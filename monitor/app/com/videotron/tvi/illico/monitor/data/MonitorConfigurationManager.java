package com.videotron.tvi.illico.monitor.data;

import java.io.File;
import java.io.FileInputStream;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.log.Log;

public class MonitorConfigurationManager implements DataUpdateListener {

	private static MonitorConfigurationManager instance = null;

	private String aodServerIp;
	private int aodPort;
	private String contextRoot;
	private int timeOutDelay;

	private static final String MONITOR_CONFIG_FILE_PATH = "MONITOR_CONFIG_FILE";

	/** The log header message. */
	private static final String LOG_HEADER = "[MonitorConfigManager] ";

	public synchronized static MonitorConfigurationManager getInstance() {
		if (instance == null) {
			instance = new MonitorConfigurationManager();
		}
		return instance;
	}

	public String getAodServerIP() {
		return aodServerIp;
	}

	public int getAodServerPort() {
		return aodPort;
	}
	
	public String getAodContextRoot() {
		return contextRoot;
	}
	
	public int getAodTimeoutDelay() {
		return timeOutDelay;
	}

	/**
	 * Constructor.
	 */
	private MonitorConfigurationManager() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "start monitoring " + MONITOR_CONFIG_FILE_PATH);
		}
		DataCenter.getInstance().addDataUpdateListener(MONITOR_CONFIG_FILE_PATH, this);

		refreshFile();
	}

	/**
	 * Reads version file in the OOB.
	 */
	private synchronized void refreshFile() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "refreshVersionFile()");
		}
		File file = (File) DataCenter.getInstance().get(MONITOR_CONFIG_FILE_PATH);
		if (file == null) {
			if (Log.WARNING_ON) {
				Log.printWarning(LOG_HEADER + "refreshVersionFile() Not found MONITOR_CONFIG_FILE_PATH");
			}
			return;
		}
		FileInputStream inputStream = null;

		try {
			inputStream = new FileInputStream(file);
			byte[] data = new byte[1024];
			int readLength = inputStream.read(data);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "refreshVersionFile() readLength=" + readLength);
			}

			int count = 0;
			int version = oneByteToInt(data, count);
			count ++;
			int serverIpLength = oneByteToInt(data, count);
			count ++;
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "refreshVersionFile() version:" + version + ",Ip length=" + serverIpLength);
			}
			aodServerIp = byteArrayToString(data, count, serverIpLength);
			count += serverIpLength;

			aodPort = twoBytesToInt(data, count, false);
			count += 2;
			
			/*
			proxyListLength = oneByteToInt(data, count);
			count ++;
			proxyIps = new String[proxyListLength];
			proxyPorts = new int[proxyListLength];
			for (int i = 0; i < proxyListLength; i++) {
				int proxyIpLength = oneByteToInt(data, count);
				count ++;
				proxyIps[i] = byteArrayToString(data, count, proxyIpLength);
				count += proxyIpLength;
				proxyPorts[i] = twoBytesToInt(data, count, false);
				count += 2;
			}
			*/
			
			int contextRootLength = oneByteToInt(data, count);
			count ++;
			contextRoot = byteArrayToString(data, count, contextRootLength);
			count += contextRootLength;
			
			timeOutDelay = twoBytesToInt(data, count, false);

			if (Log.INFO_ON) {
				Log.printInfo("MonitorConfigurationManager:() \n" + toString());
			}

		} catch (Exception e) {
			Log.print(e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
					Log.print(e);
				}
			}
		}

	}

	// ////////////////////////////////////////////////////////
	// Following is the implementation of DataUpdateListener.
	// ////////////////////////////////////////////////////////

	/**
	 * Called when data has been removed.
	 *
	 * @param key
	 *            key of data.
	 */
	public void dataRemoved(String key) {

	}

	/**
	 * Called when data has been updated.
	 *
	 * @param key
	 *            key of data.
	 * @param old
	 *            old value of data.
	 * @param value
	 *            new value of data.
	 */
	public void dataUpdated(String key, Object old, Object value) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "dataUpdated() key=" + key);
		}
		if (MONITOR_CONFIG_FILE_PATH.equals(key)) {
			refreshFile();
		}
	}

	public static int oneByteToInt(byte[] data, int offset) {
		return ((int) data[offset]) & 0xFF;
	}

	public static int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
		return ((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF);
	}

	public static String byteArrayToString(byte[] data, int offset, int length) {
		return new String(data, offset, length);
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer("<<<<< Monitor configurations >>>>>>\n");
		buffer.append("\tAOD Server IP:Port=" + aodServerIp + ":" + aodPort + ",contextRoot=" + contextRoot + ",timeout=" + timeOutDelay + "\n");
		buffer.append("=================================================\n");
		return buffer.toString();
	}

}

package com.videotron.tvi.illico.monitor.data;

/**
 * The IPPV deactivation package name class.
 * @author Woosik Lee
 */
public class IPPVDeactivationPackage extends Package {
    /**
     * Constructor.
     * @param name the IPPV deactivation package name.
     */
    public IPPVDeactivationPackage(final String name) {
        super(name);
    }
}

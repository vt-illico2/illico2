package com.videotron.tvi.illico.monitor.data;

public class KeyNames {

	public static final String BTN_OK = "popup.btn.ok";
	public static final String BTN_CANCEL = "popup.btn.cancel";
	public static final String POPUP_TITLE_NOTIFICATION = "popup.title.notification";
	
	public static final String POPUP_IBUPDATE_TITLE = "popup.title.ibupdate.dialog";
	public static final String POPUP_IBUPDATE_DIALOG_MSG = "popup.message.ibupdate.dialog";
	public static final String POPUP_IBUPDATE_POPUP_MSG = "popup.message.ibupdate.notification";
	
	public static final String POPUP_XAIT_TITLE = "popup.title.xait";
	public static final String POPUP_XAIT_MSG = "popup.message.xait";
	
	public static final String POPUP_REBOOT_AT_STANDALONE_MODE = "popup.standalone.reboot";
	
	public static final String POPUP_NO_SIGNAL_MODE = "popup.nosignal";
	
	public static final String STANDALONE_TITLE = "popup.standalone.title";
	public static final String STANDALONE_MESSAGE1 = "popup.standalone.message1";
	public static final String STANDALONE_MESSAGE2 = "popup.standalone.message2";
	public static final String STANDALONE_MESSAGE3 = "popup.standalone.message3";
	public static final String STANDALONE_MESSAGE4 = "popup.standalone.message4";
	public static final String STANDALONE_BUTTON = "popup.standalone.button";
	
	public static final String TIMEBLOCKED_TITLE = "popup.timeblocked.title";
	public static final String TIMEBLOCKED_MESSAGE = "popup.timeblocked.message";
	public static final String TIMEBLOCKED_MESSAGETO = "popup.timeblocked.messageto";
	public static final String TIMEBLOCKED_UNLOCK = "popup.timeblocked.unblock";
	
	public static final String TIMEBLOCKED_UNBLOCKED_MSG = "popup.press_b_unblock";
	
    //->Kenneth[2015.6.6] VDTRMASTER-5428 쥴리 요구에 의한 improvement
	public static final String NOSIGNAL_TITLE = "popup.nosignal_title";
	public static final String NOSIGNAL_MSG = "popup.nosignal_msg";
	public static final String NOSIGNAL_CLOSE = "popup.nosignal_close";
	public static final String NOSIGNAL_RESTART = "popup.nosignal_restart";
    //<-
}

package com.videotron.tvi.illico.monitor.data;

public class PvrStandalonePackage extends Package {

    /**
     * Constructor.
     * @param packageName the standalone package name.
     */
    public PvrStandalonePackage(final String packageName) {
        super(packageName);
    }
}

package com.videotron.tvi.illico.monitor.data;

/**
 * The package class.
 * @author Woosik Lee
 */
public class Package {
    /** The package name. */
    private String packageName;

    /**
     * Constructor.
     * @param pPackageName package name.
     */
    public Package(final String pPackageName) {
        this.packageName = pPackageName;
    }

    /**
     * Gets the package name.
     * @return package name.
     */
    public String getPackageName() {
        return packageName;
    }
}

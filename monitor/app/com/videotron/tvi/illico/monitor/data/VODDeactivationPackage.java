package com.videotron.tvi.illico.monitor.data;

/**
 * The VOD deactivation package name class.
 * @author Kenneth
 */
public class VODDeactivationPackage extends Package {
    /**
     * Constructor.
     * @param name the VOD deactivation package name.
     */
    public VODDeactivationPackage(final String name) {
        super(name);
    }
}

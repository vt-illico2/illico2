package com.videotron.tvi.illico.monitor.data;

/**
 * The UHD availability package name class.
 * @author Kenneth
 */
public class UhdAvailabilityPackage extends Package {
    /**
     * Constructor.
     * @param name the UHD availability package name.
     */
    public UhdAvailabilityPackage(final String name) {
        super(name);
    }
}

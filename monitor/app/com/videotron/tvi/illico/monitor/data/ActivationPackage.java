package com.videotron.tvi.illico.monitor.data;

/**
 * The Activation package.
 * @author Woosik Lee
 */
public class ActivationPackage extends Package {
    /**
     * The constructor.
     * @param name package name.
     */
    public ActivationPackage(final String name) {
        super(name);
    }
}

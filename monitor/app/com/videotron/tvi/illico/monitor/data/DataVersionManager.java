package com.videotron.tvi.illico.monitor.data;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.core.InbandDataUpdateController;

/**
 * This class manages the Comm_DataOOB/version.txt file.
 * @author Woosik Lee
 *
 */
public class DataVersionManager implements DataUpdateListener {

    /** The Hashtable that has version informations. */
    private Hashtable versionTable;

    /** Version File 의 변경이 있는지 여부를 가지고 있는 flag 이다. Standby 로 가면 이 Flag 를 보고 Data를 받을지 여부를 판단 한다. */
    private boolean hasUpdatedVersion;

    private static final String DATA_VERSION_FILE = "DATA_VERSION_FILE";

    /** The signleton instance of DataVersionManager. */
    private static DataVersionManager instance;

    /** The log header message. */
	private static final String LOG_HEADER = "[DataVersionManager] ";

    /**
     * Gets the singleton instance.
     * @return instance of DataVersionManager.
     */
    public static synchronized DataVersionManager getInstance() {
        if (instance == null) {
            instance = new DataVersionManager();
        }
        return instance;
    }

    /**
     * Constructor.
     */
    private DataVersionManager() {
        versionTable = new Hashtable();
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "start monitoring " + DATA_VERSION_FILE);
        }
        DataCenter.getInstance().addDataUpdateListener(DATA_VERSION_FILE, this);

        refreshVersionFile();
    }

    /**
     * Returns the version of the Inband data file.
     * @param fileName the file name.
     * @return version. if not found the file name, returns {@link MonitorService#VERSION_NOT_FOUND}
     */
    public int getInbandDataVersion(String fileName) {
        synchronized (this) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "getVersion " + fileName);
            }
            Integer value = (Integer) versionTable.get(fileName);
            if (value == null) {
                return MonitorService.VERSION_NOT_FOUND;
            }
            return value.intValue();
        }
    }

    /**
     *
     * @return if updated file, return true.
     */
    public boolean hasUpdatedFile() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "hasUpdatedFile : " + hasUpdatedVersion);
        }
        return hasUpdatedVersion;
    }

    /**
     * Standby 에서 혹은 부팅 중 Inband data 수신을 모두 끝내면 여기를 반드시 호출 해 줘야 한다.
     */
    public void clearHasUpdatedFile() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "clearHasUpdatedFile()");
        }
        hasUpdatedVersion = false;
    }

    /**
     * Prints version informations.
     */
    private void printVersionInformation() {
        if(Log.DEBUG_ON) {
            Enumeration enum1 = versionTable.keys();
            Log.printDebug(LOG_HEADER + "printVersionInformation() START ===========");
            while(enum1.hasMoreElements()) {
                String key = (String)enum1.nextElement();
                Integer value = (Integer) versionTable.get(key);
                Log.printDebug("\t" + key + ":\t\t\t" + value);
            }
            Log.printDebug(LOG_HEADER + "printVersionInformation() END ===========");
        }
    }

    /**
     * Reads version file in the OOB.
     */
    private synchronized void refreshVersionFile() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "refreshVersionFile()");
        }
        File file = (File) DataCenter.getInstance().get(DATA_VERSION_FILE);
        if (file == null) {
            if (Log.WARNING_ON) {
                Log.printWarning(LOG_HEADER + "refreshVersionFile() Not found DATA_VERSION_FILE");
            }
            versionTable.clear();
            return;
        }
        
        Hashtable oldTable = (Hashtable) versionTable.clone();
        
        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream(file);
            byte[] data = new byte[1024];
            int readLength = inputStream.read(data);
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "refreshVersionFile() readLength=" + readLength);
            }

            versionTable.put(MonitorService.mini_menu_items, new Integer(data[0] & 0xFF));
            versionTable.put(MonitorService.full_menu_items, new Integer(data[1] & 0xFF));
            versionTable.put(MonitorService.menu_config, new Integer(data[2] & 0xFF));

            versionTable.put(MonitorService.channel_logos, new Integer(data[3] & 0xFF));
            versionTable.put(MonitorService.galaxie_wallpapers, new Integer(data[4] & 0xFF));
            versionTable.put(MonitorService.epg_logos, new Integer(data[5] & 0xFF));
            versionTable.put(MonitorService.screen_saver_image, new Integer(data[6] & 0xFF));
            versionTable.put(MonitorService.loading_animation_config, new Integer(data[7] & 0xFF));
            versionTable.put(MonitorService.booting_animation_config, new Integer(data[8] & 0xFF));
            versionTable.put(MonitorService.pvr_config, new Integer(data[9] & 0xFF));
            versionTable.put(MonitorService.search_config, new Integer(data[10] & 0xFF));
            versionTable.put(MonitorService.vod_server_config, new Integer(data[11] & 0xFF));
            versionTable.put(MonitorService.widget_list, new Integer(data[12] & 0xFF));
            versionTable.put(MonitorService.dashboard_config_ib, new Integer(data[13] & 0xFF));
            versionTable.put(MonitorService.postal_code, new Integer(data[14] & 0xFF));
            versionTable.put(MonitorService.notification, new Integer(data[15] & 0xFF));

            versionTable.put(MonitorService.stc_server, new Integer(data[16] & 0xFF));
            versionTable.put(MonitorService.dtaa_config, new Integer(data[17] & 0xFF));
            versionTable.put(MonitorService.general_config, new Integer(data[18] & 0xFF));
            versionTable.put(MonitorService.error_codes, new Integer(data[19] & 0xFF));
            versionTable.put(MonitorService.cache_management, new Integer(data[20] & 0xFF));


            // Following is updated by MS version 1.5
            versionTable.put(MonitorService.itv_server, new Integer(data[21] & 0xFF));
            versionTable.put(MonitorService.cyo, new Integer(data[22] & 0xFF));
            versionTable.put(MonitorService.callerid_server_config, new Integer(data[23] & 0xFF));
            // Following is updated by MS version 1.6
            versionTable.put(MonitorService.help_topic, new Integer(data[24] & 0xFF));

            int count = 25;

            // Reads the application name and version
            int applicationSize = data[count];
            count++;
            for (int i = 0; i < applicationSize; i++) {
                int appNameLength = data[count];
                count++;
                String appName = new String(data, count, appNameLength);
                count += appNameLength;
                int appNameVersion = data[count];
                versionTable.put(appName, new Integer(appNameVersion));
                count++;
            }
            
            
            try {
            	// 20110824 Add following item 
            	versionTable.put(MonitorService.virtual_channels, new Integer(data[count] & 0xFF));
            	count ++;
            	// 20110829 Add ppv banner
            	versionTable.put(MonitorService.ppv_banner, new Integer(data[count] & 0xFF));
            	count ++;
            } catch (Exception ee) {
            	Log.print(ee);
            }
            
            try {
            	// 20111014 Added following item (pcs_teaser) - MS document v3.8
            	versionTable.put(MonitorService.pcv_teaser, new Integer(data[count] &0xFF));
            	count ++;
            } catch (Exception e) {
            	Log.print(e);
            }

            /////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////
            // Kenneth [2015.7.17] : MS 의 version.txt 와 불일치 발견. 
            // 현재의 version.txt 와 맞춤
            /////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////
            try {
            	versionTable.put(MonitorService.list_behavior, new Integer(data[count] &0xFF));
            	count ++;
            } catch (Exception e) {
            	Log.print(e);
            }
            try {
            	versionTable.put(MonitorService.vod_common, new Integer(data[count] &0xFF));
            	count ++;
            } catch (Exception e) {
            	Log.print(e);
            }
            try {
            	versionTable.put(MonitorService.tutorial_images, new Integer(data[count] &0xFF));
            	count ++;
            } catch (Exception e) {
            	Log.print(e);
            }
            try {
            	versionTable.put(MonitorService.portal_brand_images, new Integer(data[count] &0xFF));
            	count ++;
            } catch (Exception e) {
            	Log.print(e);
            }
            /*
            try {
            	// Kenneth : 2014.9.26 : R4.1
            	versionTable.put(MonitorService.list_behavior, new Integer(data[count] &0xFF));
            	count ++;
            	versionTable.put(MonitorService.list_behavior_images, new Integer(data[count] &0xFF));
            	count ++;
            } catch (Exception e) {
            	Log.print(e);
            }

            //->Kenneth[2015.7.15] : Tank : brand image 업데이트 때문에 추가됨
            try {
            	versionTable.put(MonitorService.portal_brand_images, new Integer(data[count] &0xFF));
            	count ++;
            } catch (Exception e) {
            	Log.print(e);
            }
            //<-
            */
            //<-

            //->Kenneth[2015.7.17] : 업데이트 없어도 버전찍게 하자.
            printVersionInformation();
            
            // compare old version and new version
            boolean isEquals = checkVersionsWithOldVersion(oldTable, versionTable);
            if(isEquals) {
                if(Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "Version file is updated but all version of contents are same. So do not anything.");
                }
                return;
            }

            hasUpdatedVersion = true;

            //printVersionInformation();

        } catch (Exception e) {
            Log.print(e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }

        String configImmediateUpdate = DataManager.getInstance().getIbImmediateUpdate();
		if(configImmediateUpdate.equals("NO")) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "ImmediateUpdate is NO so ignore this update.");
			}
			return;
		}

		InbandDataUpdateController ibUpdater = InbandDataUpdateController.getInstance();
        if(ibUpdater != null) {
        	ibUpdater.dataUpdated(true);
        }
        else {
        	if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "InbandDataUpdateController is not prepared.");
            }
        }
    }
    
    private boolean checkVersionsWithOldVersion(Hashtable oldTable, Hashtable newTable) {
        
        int sizeOld = oldTable.size();
        int sizeNew = newTable.size();
        if(sizeOld != sizeNew) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "equalsVersion() not equals size of version. sizeOld=" + sizeOld + ", sizeNew=" + sizeNew);
            }
            return false;
        }
        
        Enumeration enumOld = oldTable.keys();
        while(enumOld.hasMoreElements()) {
            String key = (String)enumOld.nextElement();
            Integer versionOld = (Integer)oldTable.get(key);
            if(versionOld == null) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "equalsVersion() versionOld is null key=" + key);
                }
                return false;
            }
            
            Integer versionNew = (Integer)newTable.get(key);
            if(versionNew == null || !versionOld.equals(versionNew)) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "equalsVersion() found version deference key=" + key + ", versionOld=" + versionOld + ", versionNew=" + versionNew);
                }
                return false;
            }
        }
        
        if(Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "equalsVersion return true");
        }
        
        return true;
    }
    

    // ////////////////////////////////////////////////////////
    // Following is the implementation of DataUpdateListener.
    // ////////////////////////////////////////////////////////

    /**
     * Called when data has been removed.
     * @param key key of data.
     */
    public void dataRemoved(String key) {

    }

    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "dataUpdated() key=" + key);
        }
        if (DATA_VERSION_FILE.equals(key)) {
            refreshVersionFile();
        }
    }

}

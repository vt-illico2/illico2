package com.videotron.tvi.illico.monitor.data;

import java.util.StringTokenizer;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;

/**
 * This class manages the monitor's data.
 * @author Woosik Lee
 */
public final class DataManager {

    /** The signleton instance of DataManager. */
    private static DataManager instance;
    /** The application name for finding vod. */
    private String vodName;
    /** The application name for finding menu. */
    private String menuName;
    /** The application name for finding epg. */
    private String epgName;
    /** The application name for finding upp. */
    private String uppName;
    /** The application name for finding screen-saver. */
    private String screenSaverName;
    /** The application name for loading animation. */
    private String loadingName;
    /** The application name for search. */
    private String searchName;
    /** The application name for PVR. */
    private String pvrName;
    /** The application name for Wizard. */
    private String wizardName;
    /** The application name for Dashboard. */
    private String dashboardName;
    /** The application name for ErrorMessage. */
    private String errorMessageName;
    /** The application name for Notification. */
    private String notificationName;
    /** The application name for STC. */
    private String stcName;
    /** The application name for VBM. */
    private String vbmName;
    /** The application name for ISA. */
    private String isaName;

    /** The source id for inband data channel. */
    private String sourceIdForInbandData;
    /** The flag for receiving inband data. If you want to receive data at once, you must set 'YES'. Otherwise set 'NO' .*/
    private String receivingInbandDataAtOnce;
    private String IbUpdateMaxDelayCount;
    private String IbUpdateDelayTime;
    private String IbUpdateIgnoreTimeNextUpdate;
    private String IBUpdateTimeOut;
    private String IbImmediateUpdate;
    private String IbUpdateTime;

    /** Holding time of unbound application update popup. */
    private String XAITUPdatePopupCountdownTime;
    private String XAITUpdateMaxDelayCount;
    private String XAITUpdateDelayTime;
    private String XAITUpdateIgnoreTimeNextUpdate;

    /** Waiting time of deffered download. */
    private String defferedDownloadWaitingTime;

    /** The repeat time for STB's package authority check. Unit is second. Default is 1 hour. */
    private String StbAuthorityCheckRepeatTime;

    private String inbandDataDialogPopupCountdownTime;

    private String TestForAOD;

    private String NoNeedToCheckAuthorizationApplicationList;

    private String[] noNeedToCheckAuthAppList;

    private String DsgCheckingUrl;

    private String TestInKorea;

    private String UnboundApplicationsInitializeTimeout;

    private String YearShift;

    /** The log header message. */
	private static final String LOG_HEADER = "[DataManager] ";

    /**
     * Gets the singleton instance.
     * @return instance of DataManager.
     */
    public static synchronized DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    /**
     * Constructor.
     */
    private DataManager() {
        refreshApplicationName();
    }

    /**
     * Gets the vod application name in application.prop file.
     * @return name of vod application.
     */
    public String getVodName() {
        return vodName;
    }

    /**
     * Gets the menu application name in application.prop file.
     * @return name of menu application.
     */
    public String getMenuName() {
        return menuName;
    }

    /**
     * Gets the epg application name in application.prop file.
     * @return name of epg application.
     */
    public String getEPGName() {
        return epgName;
    }

    /**
     * Gets the UPP application name in application.prop file.
     * @return name of upp application.
     */
    public String getUPPName() {
        return uppName;
    }

    /**
     * Gets the screen-saver application name in application.prop file.
     * @return name of screen-saver application.
     */
    public String getScreenSaverName() {
        return screenSaverName;
    }

    /**
     * Gets the loading-animation application name in application.prop file.
     * @return name of loading-animation.
     */
    public String getLoadingName() {
        return loadingName;
    }

    /**
     * Gets the Search application name in application.prop file.
     * @return name of Search.
     */
    public String getSearchName() {
        return searchName;
    }

    /**
     * Gets the Search application name in application.prop file.
     * @return name of PVR.
     */
    public String getPVRName() {
        return pvrName;
    }

    /**
     * Gets the Dashboard application name in application.prop file.
     * @return name of Dashboard.
     */
    public String getDashboardName() {
        return dashboardName;
    }

    /**
     * Gets the Search application name in application.prop file.
     * @return name of Wizard.
     */
    public String getWizardName() {
        return wizardName;
    }

    /**
     * Gets the ErrorMessage application name in application.prop file.
     * @return name of ErrorMessage.
     */
    public String getErrorMessageName() {
        return errorMessageName;
    }
    /**
     * Gets the Notification application name in application.prop file.
     * @return
     */
    public String getNotificationName() {
        return notificationName;
    }

    /**
     * Gets the STC application name in application.prop file.
     * @return name of STC.
     */
    public String getStcName() {
        return stcName;
    }

    /**
     * Gets the VBM application name in application.prop file.
     * @return name of VBM.
     */
    public String getVbmName() {
        return vbmName;
    }

    /**
     * Gets the ISA application name in application.prop file.
     * @return name of VBM.
     */
    public String getIsaName() {
        return isaName;
    }

    /**
     * Gets the source id fot tune to inband data channel.
     * @return source id.
     */
    public String getSourceIdForInbandData() {
        return sourceIdForInbandData;
    }

    public String getReceivingInbandDataAtOnce() {
        return receivingInbandDataAtOnce;
    }

    public String getIbUpdateMaxDelayCount() {
    	return IbUpdateMaxDelayCount;
    }

    public String getIbUpdateDelayTime() {
    	return IbUpdateDelayTime;
    }

    public String getIbUpdateIgnoreTimeNextUpdate() {
    	return IbUpdateIgnoreTimeNextUpdate;
    }

    public String getIBUpdateTimeOut() {
    	return IBUpdateTimeOut;
    }

    public String getIbImmediateUpdate() {
    	return IbImmediateUpdate;
    }

    public String getIbUpdateTime() {
    	return IbUpdateTime;
    }

    /**
     * Gets the XAIT update popup's shown time. unit is second.
     * @return time.
     */
    public String getXAITUPdateCountdownTime() {
        return XAITUPdatePopupCountdownTime;
    }

    public String getXAITUpdateMaxDelayCount() {
    	return XAITUpdateMaxDelayCount;
    }

    public String getXAITUpdateDelayTime() {
    	return XAITUpdateDelayTime;
    }

    public String getXAITUpdateIgnoreTimeNextUpdate() {
    	return XAITUpdateIgnoreTimeNextUpdate;
    }

    /**
     * Gets the repeat time for checking STB' authority (Mandatory and Activation).
     * @return repeat time. unit is second.
     */
    public String getStbAuthorityCheckRepeatTime() {
        return StbAuthorityCheckRepeatTime;
    }

    public String getDefferedDownloadWaitingTime() {
        return defferedDownloadWaitingTime;
    }

    public String getTestForAOD() {
        return TestForAOD;
    }

    public String getInbandDataDialogPopupTime() {
    	return inbandDataDialogPopupCountdownTime;
    }

    public String getDsgCheckingUrl() {
    	return DsgCheckingUrl;
    }

    public String[] getNoNeedToCheckAuthAppList() {
    	return noNeedToCheckAuthAppList;
    }

    public String getTestInKorea() {
    	return TestInKorea;
    }

    public String getUnboundApplicationsInitializeTimeout() {
    	return UnboundApplicationsInitializeTimeout;
    }

    public String getYearShift() {
        return YearShift;
    }

    /**
     * Reads the application name matched the XAIT's application name in the application.prop file.
     */
    private void refreshApplicationName() {
        DataCenter dc = DataCenter.getInstance();
        vodName = dc.getString("VOD");
        menuName = dc.getString("MainMenu");
        epgName = dc.getString("EPG");
        screenSaverName = dc.getString("Screensaver");
        sourceIdForInbandData = dc.getString("InbandDataChannel");
        receivingInbandDataAtOnce = dc.getString("ReceivingInbandDataAtOnce");
        IbUpdateMaxDelayCount = dc.getString("IbUpdateMaxDelayCount");
        IbUpdateDelayTime = dc.getString("IbUpdateDelayTime");
        IbUpdateIgnoreTimeNextUpdate = dc.getString("IbUpdateIgnoreTimeNextUpdate");
        IBUpdateTimeOut = dc.getString("IBUpdateTimeOut");
        IbImmediateUpdate = dc.getString("IbImmediateUpdate");
        IbUpdateTime = dc.getString("IbUpdateTime");

        uppName = dc.getString("UserProfile");
        loadingName = dc.getString("LoadingAnimation");
        searchName = dc.getString("Search");
        pvrName = dc.getString("PVR");
        wizardName = dc.getString("Wizard");
        dashboardName = dc.getString("Dashboard");
        errorMessageName = dc.getString("ErrorMessage");
        notificationName = dc.getString("Notification");
        stcName = dc.getString("STC");
        vbmName = dc.getString("VBM");
        isaName = dc.getString("ISA");

        XAITUPdatePopupCountdownTime = dc.getString("XAITUPdatePopupCountdownTime");
        XAITUpdateMaxDelayCount = dc.getString("XAITUpdateMaxDelayCount");
        XAITUpdateDelayTime = dc.getString("XAITUpdateDelayTime");
        XAITUpdateIgnoreTimeNextUpdate = dc.getString("XAITUpdateIgnoreTimeNextUpdate");

        StbAuthorityCheckRepeatTime = dc.getString("StbAuthorityCheckRepeatTime");

        defferedDownloadWaitingTime = dc.getString("DefferedDownloadWaitingTime");

        TestForAOD = dc.getString("TestForAOD");
        inbandDataDialogPopupCountdownTime = dc.getString("InbandDataDialogPopupCountdownTime");

        DsgCheckingUrl = dc.getString("DsgCheckingUrl");

        TestInKorea = dc.getString("TestInKorea");

        UnboundApplicationsInitializeTimeout = dc.getString("UnboundApplicationsInitializeTimeout");

        if(Log.INFO_ON) {
        	Log.printInfo(LOG_HEADER + "TestInKorea=" + TestInKorea + ", UnboundApplicationsInitializeTimeout=" + UnboundApplicationsInitializeTimeout);
        }

        YearShift = dc.getString("YearShift");
        if (Log.DEBUG_ON) Log.printDebug("YearShift in Monitor = "+YearShift);

        NoNeedToCheckAuthorizationApplicationList = dc.getString("NoNeedToCheckAuthorizationApplicationList");

        if(NoNeedToCheckAuthorizationApplicationList != null && NoNeedToCheckAuthorizationApplicationList.length() > 1) {
        	StringTokenizer stk = new StringTokenizer(NoNeedToCheckAuthorizationApplicationList, ",");
			Vector v = new Vector();
			while (stk.hasMoreElements()) {
				v.add(stk.nextToken());
			}
			noNeedToCheckAuthAppList = new String[v.size()];
			for (int i = 0; i < noNeedToCheckAuthAppList.length; i++) {
				noNeedToCheckAuthAppList[i] = (String) v.get(i);
			}

			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "noNeedToCheckAuthAppList");
				for(int i=0; i<noNeedToCheckAuthAppList.length; i++) {
					Log.printDebug(LOG_HEADER + "[" + i + "]=" + noNeedToCheckAuthAppList[i]);
				}
			}
        }

    }

}

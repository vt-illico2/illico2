package com.videotron.tvi.illico.monitor.data;

/**
 * The entitlement package name class.
 * @author Woosik Lee
 */
public class ApplicationPackage extends Package {
    /** The application names that mapping by package name. */
    private String[] packageIds;

    /**
     * Constructor.
     * @param applicationName the application name.
     * @param pIds the package id names.
     */
    public ApplicationPackage(final String applicationName, final String[] pIds) {
        super(applicationName);
        this.packageIds = pIds;
    }

    /**
     * Gets the application names.
     * @return application names (ids).
     */
    public String[] getPackageIDs() {
        return packageIds;
    }
}

// $Header: /home/cvs/illico2/monitor/app/com/videotron/tvi/illico/monitor/data/MocaPackage.java,v 1.5 2017/01/09 21:12:13 zestyman Exp $

/*
 *  MocaPackage.java	$Revision: 1.5 $ $Date: 2017/01/09 21:12:13 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.monitor.data;

public class MocaPackage extends Package {

	/**
	 * Constructor.
	 *
	 * @param packageName
	 *            the MocaPackage package name.
	 */
	public MocaPackage(final String packageName) {
		super(packageName);
	}
}

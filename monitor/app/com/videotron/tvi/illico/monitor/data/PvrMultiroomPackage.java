package com.videotron.tvi.illico.monitor.data;

public class PvrMultiroomPackage extends Package {

    /**
     * Constructor.
     * @param packageName the PVR's multiroom package name.
     */
    public PvrMultiroomPackage(final String packageName) {
        super(packageName);
    }
}
package com.videotron.tvi.illico.monitor.data;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.awt.Image;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.util.Environment;
/**
 * Parsing data over IP, list_behavior.txt/list_behavior_image.zip
 * @author Kenneth
 */
public class ListBehavior {
    private static ListBehavior instance = null;
    private static final String HEAD = "Kenneth [ListBehavior] : ";

    private int version;
    private int langSize;
    private String[] langType;
    private String[] title;
    private String[] bannerImageName;
    private String[] functionalText;
    private String[] description;
    private String[] buttonName;

    protected ListBehavior() {
    }

    public static ListBehavior getInstance() {
        if (instance == null) {
            instance = new ListBehavior();
        }
        return instance;
    }

    public void resetTexts(Object data) {
        File file = (File)data;
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            // File
            fis = new FileInputStream(file);
            baos = new ByteArrayOutputStream();
            int len = 0;
            while ((len = fis.read()) != -1) {
                baos.write(len);
            }
            byte[] arr = baos.toByteArray();
            len = arr.length;

            int index = 0;
            // version
            version = oneByteToInt(arr, index++);
            Log.printDebug(HEAD+"version = "+version);
            // language type size
            langSize = oneByteToInt(arr, index++);
            Log.printDebug(HEAD+"langSize = "+langSize);

            langType = new String[langSize];
            title = new String[langSize];
            bannerImageName = new String[langSize];
            functionalText = new String[langSize];
            description = new String[langSize];
            buttonName = new String[langSize];
            for (int i = 0 ; i < langSize ; i++) {
                // language type
                langType[i] = byteArrayToString(arr, index, 2);
                Log.printDebug(HEAD+"langType = "+langType[i]);
                index += 2;

                // title
                int titleLength = oneByteToInt(arr, index++);
                title[i] = byteArrayToString(arr, index, titleLength);
                Log.printDebug(HEAD+"title = "+title[i]);
                index += titleLength;

                // banner image name
                int bannerImageNameLength = oneByteToInt(arr, index++);
                bannerImageName[i] = byteArrayToString(arr, index, bannerImageNameLength);
                Log.printDebug(HEAD+"bannerImageName = "+bannerImageName[i]);
                index += bannerImageNameLength;

                // functional text
                int functionalTextLength = twoBytesToInt(arr, index, false);
                index += 2;
                functionalText[i] = byteArrayToString(arr, index, functionalTextLength);
                Log.printDebug(HEAD+"functionalText = "+functionalText[i]);
                index += functionalTextLength;

                // description
                int descriptionLength = twoBytesToInt(arr, index, false);
                index += 2;
                description[i] = byteArrayToString(arr, index, descriptionLength);
                Log.printDebug(HEAD+"description = "+description[i]);
                index += descriptionLength;

                // button name
                int buttonNameLength = oneByteToInt(arr, index++);
                buttonName[i] = byteArrayToString(arr, index, buttonNameLength);
                Log.printDebug(HEAD+"buttonName = "+buttonName[i]);
                index += buttonNameLength;
            }
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                    fis = null;
                }
                if (baos != null) {
                    baos.close();
                    baos = null;
                }
            } catch (Exception ex2) {
                Log.print(ex2);
            }
        }
    }

    public void resetImage(Object data) {
        //->Kenneth [2015.1.27] : VDTRMASTER-5355 : list_behavior.txt 보다 이미지가 먼저 로딩되는 경우 발생
        // 그러므로 image 처리하는 부분에서 text 데이터 파일과 무관하게 하도록 처리한다.
        try {
            Hashtable table = ZipFileReader.read((File) data);
            if (table != null) {
                int size = table.size();
                if (Log.DEBUG_ON) Log.printDebug("resetImage() : size = "+size);
                if (size == 0) return;
                Enumeration keys = table.keys();
                while (keys.hasMoreElements()) {
                    try {
                        String imageName = (String)keys.nextElement();
                        byte[] imageByte = (byte[]) table.get(imageName);
                        Log.printDebug(HEAD + "imageName = "+imageName);
                        Log.printDebug(HEAD + "imageByte = "+imageByte.length);
                        FrameworkMain.getInstance().getImagePool().createImage(imageByte, imageName);
                        FrameworkMain.getInstance().getImagePool().waitForAll();
                        Log.printDebug(HEAD+FrameworkMain.getInstance().getImagePool().getImage(imageName));
                    } catch (Exception e) {
                        Log.print(e);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        /* -> VDTRMASTER-5355 이전 버전
        Hashtable table = ZipFileReader.read((File) data);
        if (table != null) {
            for (int i = 0; i < bannerImageName.length; i++) {
                try {
                    byte[] imageByte = (byte[]) table.get(bannerImageName[i]);
                    Log.printDebug(HEAD + "imageByte = "+imageByte.length);
                    FrameworkMain.getInstance().getImagePool().createImage(imageByte, bannerImageName[i]);
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
        Log.printDebug(HEAD+FrameworkMain.getInstance().getImagePool().getImage(bannerImageName[0]));
        Log.printDebug(HEAD+FrameworkMain.getInstance().getImagePool().getImage(bannerImageName[1]));
        */
    }

    private int findLangIndex() {
        String currentLang = FrameworkMain.getInstance().getCurrentLanguage();
        if (langType == null || langType.length == 0) {
            return 0;
        }
        int index = 0;
        for (int i = 0 ; i < langType.length ; i++) {
            if (langType[i].equalsIgnoreCase(currentLang)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public String getTitle() {
		if (Environment.EMULATOR) {
            return "Title";
        }
        if (title == null || title.length == 0) return "";
        return title[findLangIndex()];
    }

    public String getFunctionalText() {
		if (Environment.EMULATOR) {
            return "Functional Text";
        }
        if (functionalText == null || functionalText.length == 0) return "";
        return functionalText[findLangIndex()];
    }

    public String getDescription() {
		if (Environment.EMULATOR) {
            //return "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            return "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\nBBB\nCCC\nDDD\nEEE\nFFF\n\n\nGGG\nHHH\n\n\n\nIII\nJJJ";
        }
        if (description == null || description.length == 0) return "";
        return description[findLangIndex()];
    }

    public String getButtonName() {
		if (Environment.EMULATOR) {
            return "ButtonName";
        }
        if (buttonName == null || buttonName.length == 0) return "";
        return buttonName[findLangIndex()];
    }

    public Image getBannerImage() {
        if (bannerImageName == null || bannerImageName.length == 0) return null;
        return FrameworkMain.getInstance().getImagePool().getImage(bannerImageName[findLangIndex()]);
    }

    // parsing codes are from EPG
    private int oneByteToInt(byte[] data, int offset) {
        return ((int) data[offset]) & 0xFF;
    }

    private int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
        return ((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF);
    }

    private int threeBytesToInt(byte[] data, int offset) {
        return ((0xFF & data[offset]) << 16)
             | ((0xFF & data[offset + 1]) << 8)
             | ((0xFF & data[offset + 2]));
    }

    private int fourBytesToInt(byte[] data, int offset) {
        return ((0xFF & data[offset]) << 24)
             | ((0xFF & data[offset + 1]) << 16)
             | ((0xFF & data[offset + 2]) << 8)
             | ((0xFF & data[offset + 3]));
    }

    private String byteArrayToString(byte[] data, int offset, int length) {
        return new String(data, offset, length);
    }
}

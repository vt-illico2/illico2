// $Header: /home/cvs/illico2/monitor/app/com/videotron/tvi/illico/monitor/data/AodApplication.java,v 1.6 2017/01/09 21:12:13 zestyman Exp $

/*
 *  AodApplication.java	$Revision: 1.6 $ $Date: 2017/01/09 21:12:13 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.monitor.data;

/**
 * AOD application.
 * It is made by management system (package_config.txt)
 * <code>AodApplication</code>
 *
 * @author $Author: zestyman $
 * @version $Revision: 1.6 $
 * @since Woosik Lee, 2012. 7. 31.
 */
public class AodApplication {

	private String appName;
	private String appPath;

	public AodApplication(String appName, String appPath) {
		this.appName = appName;
		this.appPath = appPath;
	}

	public String getAppName() {
		return appName;
	}

	public String getAppPath() {
		return appPath;
	}
}

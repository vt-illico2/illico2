package com.videotron.tvi.illico.monitor.data;

/**
 * The mandatory package class.
 * @author Woosik Lee
 */
public class MandatoryPackage extends Package {

    /**
     * Constructor.
     * @param packageName the mandatory name.
     */
    public MandatoryPackage(final String packageName) {
        super(packageName);
    }
}

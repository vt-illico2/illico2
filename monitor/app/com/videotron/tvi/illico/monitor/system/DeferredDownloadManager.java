package com.videotron.tvi.illico.monitor.system;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;
import org.ocap.system.event.DeferredDownloadEvent;
import org.ocap.system.event.SystemEvent;
import org.ocap.system.event.SystemEventListener;
import org.ocap.system.event.SystemEventManager;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.DataManager;
/**
 * This class manages the deferred download signaling.
 * @author Woosik Lee
 *
 */
public class DeferredDownloadManager implements SystemEventListener, PowerModeChangeListener, TVTimerWentOffListener {

    /** The state that is not detected deffered download. */
    private static final int DD_STATE_NONE = 0;
    /** The state that is detected deffered download. */
    private static final int DD_STATE_DETECTED_DOWNLOAD = 1;
    /** The current state. */
    private int currentState = DD_STATE_NONE;

    /** The instance of TVTimer. */
    private TVTimer timer = TVTimer.getTimer();
    /** The instance of TVTimerSpec. */
    private TVTimerSpec timerSpec = new TVTimerSpec();

    /** The log header message. */
	private static final String LOG_HEADER = "[DeferredDownloadManager] ";

    /**
     * The constructor.
     */
    public DeferredDownloadManager() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "DeferredDownloadManager()");
        }
        SystemEventManager sem = SystemEventManager.getInstance();
        sem.setEventListener(SystemEventManager.DEFERRED_DOWNLOAD_EVENT_LISTENER, this);

        Host.getInstance().addPowerModeChangeListener(this);
    }

    /**
     * Implements the org.ocap.system.event.SystemEventListener.
     * @param evt the deffered download event.
     */
    public synchronized void notifyEvent(SystemEvent evt) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "notifyEvent event=" + evt + ", currentState="
                    + currentState);
        }
        if (evt instanceof DeferredDownloadEvent) {
            DeferredDownloadEvent event = (DeferredDownloadEvent) evt;
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "typeCode=" + event.getTypeCode());
            }
            if (Host.getInstance().getPowerMode() == Host.LOW_POWER) {
                startDownload();
            } else {
                currentState = DD_STATE_DETECTED_DOWNLOAD;
            }
        }
    }

    /**
     * Starts the download.
     */
    private void startDownload() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startDownload()");
        }
        Thread downThread = new Thread() {
            public void run() {
                Host.getInstance().codeDownload();
            }
        };
        downThread.start();
    }

    /**
     * Implements the org.ocap.hardware.PowerModeChangeListener.
     * @param newPowerMode the new power mode.
     */
    public synchronized void powerModeChanged(int newPowerMode) {
        if (currentState == DD_STATE_NONE) {
            return;
        }
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "powerModeChanged: New power mode : " + newPowerMode + ", currentState : "
                    + currentState);
        }

        if (newPowerMode == Host.LOW_POWER) {
            startTimer();
        } else {
            stopTimer();
        }
    }

    /**
     * Starts the timer.
     */
    private synchronized void startTimer() {
        try {
            String waitingTimeS = DataManager.getInstance().getDefferedDownloadWaitingTime();
            long waitingTime = Integer.parseInt(waitingTimeS) * 1000L;
            timerSpec.setDelayTime(waitingTime);
            timerSpec.setRepeat(false);
            timerSpec.addTVTimerWentOffListener(this);
            timer.scheduleTimerSpec(timerSpec);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    /**
     * Stops the timer.
     */
    private synchronized void stopTimer() {
        timerSpec.removeTVTimerWentOffListener(this);
        TVTimer.getTimer().deschedule(timerSpec);
    }

    /**
     * This is timer call back..
     * @param event TVTimer event.
     */
    public void timerWentOff(TVTimerWentOffEvent paramTVTimerWentOffEvent) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "timerWentOff()");
        }

        startDownload();
    }

}

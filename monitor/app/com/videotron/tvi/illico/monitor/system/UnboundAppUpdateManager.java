package com.videotron.tvi.illico.monitor.system;

import java.awt.event.KeyEvent;
import java.lang.reflect.Method;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.application.AppID;
import org.dvb.application.AppsDatabase;
import org.dvb.application.AppsDatabaseEvent;
import org.dvb.application.AppsDatabaseEventListener;
import org.dvb.event.UserEvent;
import org.havi.ui.event.HRcEvent;
import org.ocap.application.AppManagerProxy;
import org.ocap.application.AppSignalHandler;
import org.ocap.application.OcapAppAttributes;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.PreferenceServiceProxy;
import com.videotron.tvi.illico.monitor.core.ScreenSaverManager;
import com.videotron.tvi.illico.monitor.core.UnboundAppProxy;
import com.videotron.tvi.illico.monitor.core.UnboundAppsManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.data.KeyNames;
import com.videotron.tvi.illico.monitor.ui.popup.BasicConfirm;
import com.videotron.tvi.illico.monitor.ui.popup.BasicDialog;
import com.videotron.tvi.illico.monitor.ui.popup.BasicPopup;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class monitors the XAIT update. And if the XAIT update is signaled,
 * immediately shows the notification popup and reboot for update unbound
 * applications.
 *
 * @author Woosik Lee
 */
public class UnboundAppUpdateManager implements AppSignalHandler, LayeredKeyHandler, TVTimerWentOffListener,
		AppsDatabaseEventListener {

	/** The instance of TVTimer. It is used to wait popup's continual time. */
	private TVTimer timer = TVTimer.getTimer();
	/** The instance of TVTimerSpec. It is used to wait popup's continual time. */
	private TVTimerSpec timerSpec = new TVTimerSpec();
	/** Retry timer every 4 hour. */
	private TVTimerSpec retryTimerSpec = new TVTimerSpec();

	/** The update notification popup. */
	private BasicPopup dialog = null;

	private boolean updateForcedPopup = false;
	/** Allowed delay time. */
	private static String MAX_RETRY_COUNT = "6";
	/**
	 * If XAIT update is canceled, the next XAIT update popup will be appearing
	 * after following delay time.
	 */
	private static String RETRY_TIME = "14400"; // 60*60*4 (4hour)
	/** Popup continues time. */
	private static String POPUP_COUNT_DOWN_TIME = "120"; // 2 min

	private static String IGNORE_TIME_NEXT_UPDATE = "300"; // 5min

	private int updateRetryCount = 0;

	// Cancel 한 이후에 특정 시간 안에 올라오는 notifyXAITUpdate() 에 대해 무시 하기 위해 cancel 한 시간을
	// 저장 한 값이다.
	private long lastCanceledTime = -1;

	/** The log header message. */
	private static final String LOG_HEADER = "[UnboundAppUpdateManager] ";
	
	private OcapAppAttributes[] lastNotifiedApps = null;

	/**
	 * The class constructor.
	 */
	public UnboundAppUpdateManager() {
		super();
		MAX_RETRY_COUNT = DataManager.getInstance().getXAITUpdateMaxDelayCount();
		RETRY_TIME = DataManager.getInstance().getXAITUpdateDelayTime();
		POPUP_COUNT_DOWN_TIME = DataManager.getInstance().getXAITUPdateCountdownTime();
		IGNORE_TIME_NEXT_UPDATE = DataManager.getInstance().getXAITUpdateIgnoreTimeNextUpdate();

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "MAX_RETRY_COUNT=" + MAX_RETRY_COUNT);
			Log.printInfo(LOG_HEADER + "RETRY_TIME=" + RETRY_TIME);
			Log.printInfo(LOG_HEADER + "POPUP_COUNT_DOWN_TIME=" + POPUP_COUNT_DOWN_TIME);
			Log.printInfo(LOG_HEADER + "IGNORE_TIME_NEXT_UPDATE=" + IGNORE_TIME_NEXT_UPDATE);
			Log.printInfo("AppsDatabase#addListener()");
		}

		AppsDatabase.getAppsDatabase().addListener(this);
		
		lastNotifiedApps = null;
	}

	/**
	 * Starts this class. registers the AppSignalHandler in this method.
	 */
	public void start() {
		AppManagerProxy appManagerProxy = AppManagerProxy.getInstance();
		appManagerProxy.setAppSignalHandler(this);
	}
	
	/**
	 * Compare new OcapAppAttributes with last notified OcapAppAttributes.
	 * @param newApps
	 * @return
	 */
	private boolean hasUpdateFromLastNotify(OcapAppAttributes[] newApps) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "hasUpdateFromLastNotify() ");
		}
		int[] oldAppsVersion = new int[lastNotifiedApps.length];
		for(int i=0; i<lastNotifiedApps.length; i++) {
			try {
				Class cls = Class.forName("com.alticast.am.AMOCAPJProxy");
				Method method = cls.getMethod("getVersion", null);
				if(!method.isAccessible()) {
					method.setAccessible(true);
				}
				Object obj = method.invoke(lastNotifiedApps[i], null);
				Integer intObj = (Integer)obj;
				oldAppsVersion[i] = intObj.intValue();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		int[] newAppsVersion = new int[newApps.length];
		for(int i=0; i<newApps.length; i++) {
			try {
				Class cls = Class.forName("com.alticast.am.AMOCAPJProxy");
				Method method = cls.getMethod("getVersion", null);
				if(!method.isAccessible()) {
					method.setAccessible(true);
				}
				Object obj = method.invoke(newApps[i], null);
				Integer intObj = (Integer)obj;
				newAppsVersion[i] = intObj.intValue();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (Log.INFO_ON) {
			Log.printInfo("===========================================");
			Log.printInfo("======== Updated application info =========");
			Log.printInfo("Number of new app = " + newApps.length);
			for (int i = 0; i < newApps.length; i++) {
				AppID id = newApps[i].getIdentifier();
				String name = newApps[i].getName();
				Log.printInfo("[" + i + "] Name=" + name + ", AppId=" + id + ", new version="
						+ newAppsVersion[i]);
			}
			Log.printInfo("===========================================");

			//UnboundAppProxy[] unboundApps = UnboundAppsManager.getInstance().getAllUnboundAppProxies();
			Log.printInfo("========== Last notified application info ===========");
			Log.printInfo("Number of last notified app = " + lastNotifiedApps.length);
			for (int i = 0; i < lastNotifiedApps.length; i++) {
				AppID id = lastNotifiedApps[i].getIdentifier();
				String name = lastNotifiedApps[i].getName();
				Log.printInfo("[" + i + "] Name=" + name + ", AppId=" + id + ", new version="
						+ oldAppsVersion[i]);
			}
			Log.printInfo("===========================================");
		}
		
		// Compare old and new apps.
		for(int i=0; i<newApps.length; i++) {
			OcapAppAttributes newApp = newApps[i];
			AppID newId = newApp.getIdentifier();
			boolean isFoundOldId = false;
			for(int j=0; j<lastNotifiedApps.length; j++) {
				AppID oldId = lastNotifiedApps[j].getIdentifier();
				if(newId.equals(oldId)) {
					isFoundOldId = true;
					if(oldAppsVersion[j] != newAppsVersion[i]) {
						if (Log.INFO_ON) {
							Log.printInfo("Found same id and not equals version application is " + newId);
						}
						return true;
					}
					break;
				}
			}
			
			if (!isFoundOldId) {
				if (Log.INFO_ON) {
					Log.printInfo("Not found same id application " + newId + " update is true");
				}
				return true;
			}
		}
		
		if (Log.INFO_ON) {
			Log.printInfo("Update is not necessary, return false.");
		}
		return false;
	}

	/**
	 * Note : This function use private API (com.alticast.am.AMOCAPJProxy#getVersion()) for checking updated
	 * application's version because only XAIT version may update in AXAIT.
	 * This private API is allowed from Videotron.
	 * @param newApps new application list.
	 * @return only XAIT version is updated return false.
	 */
	private boolean hasUpdated(OcapAppAttributes[] newApps) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "hasUpdated() setAccessible true version.");
		}
		// 1. Find new application's version and put to cache.
		int[] newAppsVersion = new int[newApps.length];
		for(int i=0; i<newApps.length; i++) {
			try {
				Class cls = Class.forName("com.alticast.am.AMOCAPJProxy");
				Method method = cls.getMethod("getVersion", null);
				if(!method.isAccessible()) {
					method.setAccessible(true);
				}
				Object obj = method.invoke(newApps[i], null);
				Integer intObj = (Integer)obj;
				newAppsVersion[i] = intObj.intValue();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (Log.INFO_ON) {
			Log.printInfo("===========================================");
			Log.printInfo("======== Updated application info =========");
			Log.printInfo("Number of new app = " + newApps.length);
			for (int i = 0; i < newApps.length; i++) {
				AppID id = newApps[i].getIdentifier();
				String name = newApps[i].getName();
				Log.printInfo("[" + i + "] Name=" + name + ", AppId=" + id + ", new version="
						+ newAppsVersion[i]);
			}
			Log.printInfo("===========================================");

			UnboundAppProxy[] unboundApps = UnboundAppsManager.getInstance().getAllUnboundAppProxies();
			Log.printInfo("========== Old application info ===========");
			Log.printInfo("Number of new app = " + unboundApps.length);
			for (int i = 0; i < unboundApps.length; i++) {
				AppID id = unboundApps[i].getAppID();
				String name = unboundApps[i].getName();
				Log.printInfo("[" + i + "] Name=" + name + ", AppId=" + id + ", old version=" + unboundApps[i].getAppVersion());
			}
			Log.printInfo("===========================================");
		}

		UnboundAppProxy[] unboundApps = UnboundAppsManager.getInstance().getAllUnboundAppProxies();
		if (unboundApps.length != newApps.length) {
			return true;
		}

		// Compare old and new apps.
		for(int i=0; i<newApps.length; i++) {
			OcapAppAttributes newApp = newApps[i];
			AppID newId = newApp.getIdentifier();
			boolean isFoundOldId = false;
			for(int j=0; j<unboundApps.length; j++) {
				AppID oldId = unboundApps[j].getAppID();
				if(newId.equals(oldId)) {
					isFoundOldId = true;
					if(unboundApps[j].getAppVersion() != newAppsVersion[i]) {
						if (Log.INFO_ON) {
							Log.printInfo("Found same id and not equal version application is " + newId);
						}
						return true;
					}
					break;
				}
			}

			if (!isFoundOldId) {
				if (Log.INFO_ON) {
					Log.printInfo("Not found same id application " + newId + " update is true");
				}
				return true;
			}
		}

		if (Log.INFO_ON) {
			Log.printInfo("Update is not necessary, return false.");
		}
		return false;
	}

	/**
	 * Implements the AppSignalHandler.
	 *
	 * @param newApps
	 *            The new XAIT application list.
	 * @return Always return false, because the OCAP specification defines that
	 *         ignores the return value.
	 */
	public boolean notifyXAITUpdate(OcapAppAttributes[] newApps) {
		if (Log.INFO_ON) {
			if(lastNotifiedApps == null) {
				Log.printInfo(LOG_HEADER + "notifyXAITUpdate() lastNotifiedApps is null");
			}
			else {
				Log.printInfo(LOG_HEADER + "notifyXAITUpdate() lastNotifiedApps is not null");
			}
		}

		boolean hasUpdated = false;
		if(lastNotifiedApps == null) {
			hasUpdated = hasUpdated(newApps);
		}
		else {
			hasUpdated = hasUpdateFromLastNotify(newApps);
		}
		
		if(hasUpdated) {
			int updateResult = decideUpdate();
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "notifyXAITUpdate() updateResult is " + updateResult);
			}
			switch (updateResult) {
			case UPDATE_RESULT_IGNORE_BY_TIME :
			case UPDATE_RESULT_IGNORE_BY_POPUP :
				// 여기서 lastNotifiedApps 를 newApps로 갱신 하면 안 된다. 5분 룰에 걸렸거나 현재 팝업이 떠서 무시 된 것이기 때문에
				// 다음에 올라올 notify 에서 업데이트를 수행 해야 한다. 5분이 지난 후에.
				break;
			case UPDATE_RESULT_PROCESS :
				lastNotifiedApps = newApps;
				break;
			}
		}
		return false;
	}

	private static final int UPDATE_RESULT_REBOOT = 1;
	private static final int UPDATE_RESULT_IGNORE_BY_TIME = 2;
	private static final int UPDATE_RESULT_IGNORE_BY_POPUP = 3;
	private static final int UPDATE_RESULT_PROCESS = 4;

	/**
	 * 
	 * @return {@see UnboundAppUpdateManager#UPDATE_RESULT_REBOOT}, {@see
	 *         UnboundAppUpdateManager#UPDATE_RESULT_IGNORE_BY_TIME} {@see
	 *         UnboundAppUpdateManager#UPDATE_RESULT_IGNORE_BY_POPUP}, {@see
	 *         UnboundAppUpdateManager#UPDATE_RESULT_PROCESS}.
	 */
	private synchronized int decideUpdate() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "decideUpdate() ");
		}
		// Standby 에서 업데이트 되는 경우에는 무조건 업데이트 하자.
		if (Host.getInstance().getPowerMode() == Host.LOW_POWER) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Standby mode");
			}
			reboot();
			return UPDATE_RESULT_REBOOT;
		}

		if (lastCanceledTime > 0) {
			// 한번 이상 Cancel 한 상태이면 lastCanceledTime 가 -1 이 아니다.
			long currentTime = System.currentTimeMillis();
			long tempTime = Long.parseLong(IGNORE_TIME_NEXT_UPDATE) * 1000L;
			long sumTime = tempTime + lastCanceledTime;
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "lastCanceledTime=" + lastCanceledTime + ", sumTime="
						+ sumTime);
			}
			if (currentTime < sumTime) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Ignore this update because Time is not enough from canceled time");
				}
				return UPDATE_RESULT_IGNORE_BY_TIME;
			}
		}

		if (dialog != null) {
			// 현재 Popup 이 보이고 있는 상황이기 때문에 무시 한다.
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Now shown popup on screen. so ignore this update");
			}
			return UPDATE_RESULT_IGNORE_BY_POPUP;
		}

		stopTimer();
		stopRetryTimer();

		updateRetryCount++;
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "decideUpdate() updateRetryCount=" + updateRetryCount
					+ ", P=" + Host.getInstance().getPowerMode());
		}

		ScreenSaverManager.getInstance().resetTimer();
		if (Host.getInstance().getPowerMode() == Host.LOW_POWER) {
			reboot();
			return UPDATE_RESULT_REBOOT;
		}
		int maxRetryCount = Integer.parseInt(MAX_RETRY_COUNT);
		if (updateRetryCount >= maxRetryCount) {
			// 24시간까지 Delay를 했기 때문에 강제로 업데이트가 되어야 한다.
			showPopup(true);
			startTimer();
		} else {
			showPopup(false);
			startTimer();
		}
		
		return UPDATE_RESULT_PROCESS;
	}

	/**
	 * Disposes this class.
	 * <P>
	 * When the application's Xlet#destroyXlet() is called, It is called.
	 */
	public void dispose() {
		hidePopup();
	}

	/**
	 * Shows the confirm popup for XAIT update.
	 */
	private void showPopup(boolean updateForced) {
		synchronized (this) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "showPopup() updateForced=" + updateForced);
			}

			this.updateForcedPopup = updateForced;

			String msg = DataCenter.getInstance().getString(KeyNames.POPUP_XAIT_MSG);
			StringTokenizer stk = new StringTokenizer(msg, "|");
			Vector v = new Vector();
			while (stk.hasMoreElements()) {
				v.add(stk.nextToken());
			}
			String[] message = new String[v.size()];
			for (int i = 0; i < message.length; i++) {
				message[i] = (String) v.get(i);
			}

			String title = DataCenter.getInstance().getString(KeyNames.POPUP_XAIT_TITLE);

			if (!updateForced) {
				dialog = new BasicDialog(title, WindowProperty.UNBOUND_APP_UPDATE_POPUP.getName(),
						WindowProperty.UNBOUND_APP_UPDATE_POPUP.getPriority(), message, this);
			} else {
				dialog = new BasicConfirm(title, WindowProperty.UNBOUND_APP_UPDATE_POPUP.getName(),
						WindowProperty.UNBOUND_APP_UPDATE_POPUP.getPriority(), message, this);
			}
			dialog.showPopup();
		}
	}

	/**
	 * Hides the popup.
	 */
	public void hidePopup() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "hidePopup()");
		}
		synchronized (this) {
			if (dialog != null) {
				dialog.hidePopup();
				dialog.dispose();
				dialog = null;
			}
		}
	}

	/**
	 * Implements the Layered UI's key event handler.
	 *
	 * @param event
	 *            the key event.
	 * @return Always return true because this is system popup.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "userEventReceived : " + updateForcedPopup);
		}
		if (event.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}

		int keyCode = event.getCode();

		if (keyCode == HRcEvent.VK_POWER) {
			return false;
		}

		if (updateForcedPopup) {
			// 24 시간 동안 delay 했기 때문에 강제로 업데이트 한다.
			if (keyCode == OCRcEvent.VK_ENTER) {
				reboot();
			}
			return true;
		}

		switch (keyCode) {
		case OCRcEvent.VK_LEFT:
			dialog.setFocusBtn(true);
			break;
		case OCRcEvent.VK_RIGHT:
			dialog.setFocusBtn(false);
			break;
		case OCRcEvent.VK_ENTER:
			if (dialog.getFocusBtn()) {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "start reboot()");
				}
				reboot();
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "cancel update()");
				}
				hidePopup();
				stopTimer();
				// start retry timer (4 hour)
				startRetryTimer();

				// Cancel 한 경우 특정 시간 (Value for the next pop-up display) 내에 들어오는
				// notifyXAITUpdate() 를 무시하기 위해
				// Cancel 한 시간을 기록 한다.
				lastCanceledTime = System.currentTimeMillis();
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "cancel update time=" + lastCanceledTime);
				}
			}
			break;
		default:
		}

		return true;
	}

	private void startRetryTimer() {
		stopRetryTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "startRetryTimer() " + RETRY_TIME);
		}
		try {
			long delayTime = Integer.parseInt(RETRY_TIME) * 1000L;
			retryTimerSpec.setDelayTime(delayTime);
			retryTimerSpec.setRepeat(false);
			retryTimerSpec.addTVTimerWentOffListener(this);
			timer.scheduleTimerSpec(retryTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	private void stopRetryTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "stopRetryTimer()");
		}
		retryTimerSpec.removeTVTimerWentOffListener(this);
		timer.deschedule(retryTimerSpec);
	}

	private void startTimer() {
		stopTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "startTimer()");
		}
		try {

			long waitTime = Integer.parseInt(POPUP_COUNT_DOWN_TIME) * 1000L;
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "waitTime=" + waitTime);
			}
			timerSpec.setDelayTime(waitTime);
			timerSpec.setRepeat(false);
			timerSpec.addTVTimerWentOffListener(this);
			timer.scheduleTimerSpec(timerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	private void stopTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "stopTimer()");
		}
		timerSpec.removeTVTimerWentOffListener(this);
		timer.deschedule(timerSpec);
	}

	/**
	 * This is timer call back.
	 *
	 * @param event
	 *            TVTimer event.
	 */
	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "timerWentOff() : " + event.getTimerSpec());
		}

		if (event.getTimerSpec().equals(timerSpec)) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "reboot()");
			}
			reboot();
		} else {
			// Timer for retry update (4 hour)
			decideUpdate();
		}
	}

	private void reboot() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "reboot() is called");
		}
		if (Host.getInstance().getPowerMode() == Host.LOW_POWER) {
			PreferenceServiceProxy preManager = PreferenceServiceProxy.getInstance();
			if (preManager.isReadyPreferenceService()) {
				preManager.setPreferenceValue(PreferenceServiceProxy.RUNNING_STATE_FLAG_FOR_XAIT_UPDATE,
						PreferenceServiceProxy.RUNNING_STATE_FLAG_STANDBY);
			}
		}
		Host.getInstance().reboot();
	}

	/**
	 * Implements AppsDatabaseEventListener.
	 */
	public void newDatabase(AppsDatabaseEvent paramAppsDatabaseEvent) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "newDatabase() What can I do?");
		}
	}
	/**
	 * Implements AppsDatabaseEventListener.
	 */
	public void entryAdded(AppsDatabaseEvent paramAppsDatabaseEvent) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "entryAdded()");
		}
		//decideUpdate();
	}
	/**
	 * Implements AppsDatabaseEventListener.
	 */
	public void entryRemoved(AppsDatabaseEvent paramAppsDatabaseEvent) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "entryRemoved()");
		}
		//decideUpdate();
	}
	/**
	 * Implements AppsDatabaseEventListener.
	 */
	public void entryChanged(AppsDatabaseEvent paramAppsDatabaseEvent) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "entryChanged()");
		}
		//decideUpdate();
	}
}

package com.videotron.tvi.illico.monitor.system;

import org.ocap.system.EASHandler;
import org.ocap.system.EASModuleRegistrar;

import com.videotron.tvi.illico.log.Log;

/**
 * The EAS (emergency alert system) manager.
 * @author Woosik Lee
 */
public class EASManager implements EASHandler {

    /**
     * The constructor.
     */
    public EASManager() {
        if (Log.INFO_ON) {
            Log.printInfo("EASManager: ");
        }
        EASModuleRegistrar easmr = EASModuleRegistrar.getInstance();
        easmr.registerEASHandler(this);
    }

    /**
     * It implements the EASHandler interface.
     * @param descriptor the EAS descriptor.
     * @return If tune the emergency audio channel return true. otherwise return false.
     */
    public boolean notifyPrivateDescriptor(byte[] descriptor) {
        // Play audio according to the descriptor.

        // Find audio_OOB_source_ID in the descriptor.
        // refer to SCTE18 document.

        return (true);
    }

    /**
     * Defined in EASHandler.
     */
    public void stopAudio() {
        // What can i do?
    }

}

package com.videotron.tvi.illico.monitor.system;

import org.ocap.media.ClosedCaptioningAttribute;

import com.videotron.tvi.illico.log.Log;

/**
 * This class manages the closed captioning text representation.
 * @author Woosik Lee
 */
public class ClosedCaptioningManager {

    /**
     * The constructor.
     */
    public ClosedCaptioningManager() {
        if (Log.INFO_ON) {
            Log.printInfo("EASManager: ");
        }

    }

    /**
     * Sets the preference of closed captioning text representation.
     */
    public void setPreference() {
        ClosedCaptioningAttribute attr = ClosedCaptioningAttribute.getInstance();
    }
}

package com.videotron.tvi.illico.monitor.system;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.opencable.handler.cahandler.CAHandler;
import com.opencable.handler.cahandler.CAHandlerStateEvent;
import com.opencable.handler.cahandler.CAHandlerStateListener;
import com.opencable.handler.nethandler.NetHandler;
import com.sciatl.ca.handler.CAHandlerImpl;
import com.sciatl.ca.handler.NetHandlerImpl;
import com.sciatl.ca.util.LogController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.monitor.main.MonitorMain;

/**
 * This class binds the CANH handler through the IXC.
 * @author Woosik Lee
 */
public class CANHManager implements CAHandlerStateListener, DataUpdateListener {
    /** The XletContext class's instance. */
    private XletContext xletContext = null;
    /** The CANH CAHandler's instance. */
    private CAHandlerImpl caHandlerImpl;
    /** The CANH NetHandler's instance. */
    private NetHandlerImpl netHandlerImpl;
    /** The singleton instance of CANHManager. */
    private static CANHManager instance;

    /** The log header message. */
	private static final String LOG_HEADER = "[CANHManager] ";

    /**
     * Gets the singleton instance.
     * @return instance of CANHManager.
     */
    public static synchronized CANHManager getInstance() {
        if (instance == null) {
            instance = new CANHManager();
        }
        return instance;
    }

    /**
     * Starts the class. This method bind the CANH's IXC interface.
     * @param pXlet instance of XletContext.
     */
    public void start(XletContext pXlet) {
        this.xletContext = pXlet;

        resetLogLevel();
        DataCenter.getInstance().addDataUpdateListener(Log.CONSOLE_LOG_LEVEL_DATA_KEY, this);
        try {
            caHandlerImpl = new CAHandlerImpl();
            netHandlerImpl = new NetHandlerImpl();

            synchronized (this) {
                int listenerId = caHandlerImpl.addStateListener(this);
                int state = caHandlerImpl.getState();
                if(state != CAHandler.CAH_READY) {

                    long waitTime = 60 * 1000L;
                    if (MonitorMain.CA_EMULATION) {
                        waitTime = 1L;
                    }
                    if (Log.WARNING_ON) {
                        Log.printWarning(LOG_HEADER + "CANH state =" + state + ", so wait 1 min");
                    }
                    this.wait(waitTime);
                    if (Log.DEBUG_ON) {
                        Log.printDebug(LOG_HEADER + "End of wait");
                    }

                }
                caHandlerImpl.removeStateListener(listenerId);
            }

            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "bind to IxcRegistry : CAHandler");
            }
            IxcRegistry.bind(xletContext, CAHandler.IXC_NAME, caHandlerImpl);
            IxcRegistry.bind(xletContext, NetHandler.IXC_NAME, netHandlerImpl);

        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void resetLogLevel() {
        if (Log.ALL_ON) {
            LogController.startLog(LogController.LOG_LEVEL_DEBUG);
        } else {
            LogController.stopLog();
        }
    }

	public void dataUpdated(String key, Object old, Object value) {
        resetLogLevel();
    }

	public void dataRemoved(String key) {
    }

    /**
     * Disposes this class. This method unbind the CANH's IXC interface.
     * <P>
     * When the application's Xlet#destroyXlet() is called, it is called.
     */
    public void dispose() {
        try {
            IxcRegistry.unbind(xletContext, CAHandler.IXC_NAME);
        } catch (Exception e) {
            Log.print(e);
        }
        DataCenter.getInstance().removeDataUpdateListener(Log.CONSOLE_LOG_LEVEL_DATA_KEY, this);
    }

    /**
     * Gets the CAHandler instance.
     * @return the CAHandler instance.
     */
    public CAHandler getCAHandler() {
        return caHandlerImpl;
    }

    public void deliverEvent(CAHandlerStateEvent event) {
        synchronized (this) {
            int newState = event.getNewState();
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "deliverEvent event=" + event);
            }
            if(newState == CAHandler.CAH_READY) {
                this.notifyAll();
            }
        }
    }
}

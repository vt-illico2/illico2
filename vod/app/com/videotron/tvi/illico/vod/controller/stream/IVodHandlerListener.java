package com.videotron.tvi.illico.vod.controller.stream;

import java.util.EventListener;

/**
 * IVodHandlerListener must be implemented by Vod applications in order to receive VodHandler connection and
 * disconnection events.
 * 
 * @see VodHandlerFactory
 * @see IVodHandler
 * @see VodHandlerEvent
 */
public interface IVodHandlerListener extends EventListener {
    /**
     * <code>vodStreamEventUpdate</code> This method receives connection and disconnection events. The event
     * <code>VodHandlerDisconnected</code> is generated in case of abnormal connection failure and is not generated as a
     * completion event for method IVodHandler.disconnect.
     * 
     * @param event
     *            sent by IVodHandler to indicate a connected or unconnected state.
     * @see VodHandlerEvent
     */
    public void vodHandlerEventUpdate(VodHandlerEvent event);
}

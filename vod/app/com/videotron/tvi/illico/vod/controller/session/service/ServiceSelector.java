package com.videotron.tvi.illico.vod.controller.session.service;

import java.util.Vector;

import javax.tv.locator.InvalidLocatorException;
import javax.tv.service.Service;
import javax.tv.service.selection.InsufficientResourcesException;
import javax.tv.service.selection.InvalidServiceComponentException;
import javax.tv.service.selection.NormalContentEvent;
import javax.tv.service.selection.SelectionFailedEvent;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.service.selection.ServiceContextFactory;
import javax.tv.service.selection.ServiceContextListener;

import org.ocap.net.OcapLocator;
import org.ocap.service.AbstractService;
import org.ocap.service.AlternativeContentErrorEvent;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtils;

public class ServiceSelector implements ServiceContextListener {
    private Vector listeners;

    private static ServiceSelector vodService;

    byte sessionId[] = null;

    public static ServiceSelector getInstance() {
        if (vodService == null) {
            vodService = new ServiceSelector();
        }
        return vodService;
    }

    public void selectService(OcapLocator[] locator) {
    	MenuController.getInstance().debugList[3].add("selectService : " + locator[0].toExternalForm());
        try {
            ServiceContext sc = Environment.getServiceContext(0);
            sessionId = null;
            sc.addListener(this);
            sc.select(locator);
        } catch (InvalidServiceComponentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (InvalidLocatorException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {}
    }

    public void stopService() {
        try {
            ServiceContext sc = Environment.getServiceContext(0);
            sc.stop();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {}
    }

    public void addSelectionListener(ServiceSelectionListener li) {
        if (listeners == null) {
            listeners = new Vector();
        }
        listeners.add(li);
    }

    public void removeSelectionlListener(ServiceSelectionListener li) {
        listeners.removeElement(li);
    }

    /**
     * Channel selection ServiceContext.
     */
//    public static ServiceContext getServiceContext() {
//        ServiceContextFactory scf = ServiceContextFactory.getInstance();
//        ServiceContext marked = null;
//        ServiceContext[] contexts = scf.getServiceContexts();
//        if (Log.DEBUG_ON) {
//            Log.printDebug("contexts.length = " + contexts.length);
//        }
//        if (contexts != null) {
//            for (int i = 0; i < contexts.length; i++) {
//                if (contexts[i] == null) {
//                    continue;
//                }
//                Service svc = contexts[i].getService();
//                if (svc == null) {
//                    if (marked == null) {
//                        // save first service context.
//                        marked = contexts[i];
//                    }
//                } else if (!(svc instanceof AbstractService)) {
//                    return contexts[i];
//                }
//            }
//        }
//        ServiceContext sc = null;
//        try {
//            sc = scf.createServiceContext();
//        } catch (InsufficientResourcesException ex) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("can't create service context. use first one.");
//            }
//            sc = marked;
//        }
//        return sc;
//    }

    public void receiveServiceContextEvent(ServiceContextEvent servicecontextevent) {
//        int result = ServiceSelectionListener.SELECT_FAIL;
//        if (Log.DEBUG_ON) {
//            Log.printDebug("receiveServiceContextEvent : " + servicecontextevent);
//        }
//        MenuController.getInstance().debugList[3].add("svcCtxEvt : " + StringUtils.toString(servicecontextevent));
//        if ((servicecontextevent instanceof NormalContentEvent)) {
//            result = ServiceSelectionListener.SELECT_SUCCESS;
//        } else if (servicecontextevent instanceof AlternativeContentErrorEvent) {
//        	AlternativeContentErrorEvent ae = (AlternativeContentErrorEvent) servicecontextevent;
//        	if (ae.getReason() == 102) {
//        		result = ServiceSelectionListener.SELECT_BUT_CONTENT_NOT_FOUND;
//        	}
//        } else if (servicecontextevent instanceof SelectionFailedEvent) { // for Cisco
//        	SelectionFailedEvent se = (SelectionFailedEvent) servicecontextevent;
//        	if (se.getReason() == 102) {
//        		result = ServiceSelectionListener.SELECT_BUT_CONTENT_NOT_FOUND;
//        	}
//        }
    	boolean needRemoveListener = false;
    	int result = ServiceSelectionListener.SELECT_SUCCESS;
    	
    	if (servicecontextevent instanceof SelectionFailedEvent) {
    		SelectionFailedEvent se = (SelectionFailedEvent) servicecontextevent;
    		if (se.getReason() == SelectionFailedEvent.INSUFFICIENT_RESOURCES) {
    			result = ServiceSelectionListener.SELECT_FAIL;
    			needRemoveListener = true;
    		}
    	} else if (servicecontextevent instanceof NormalContentEvent) {
    		needRemoveListener = true;
    	}
    	MenuController.getInstance().debugList[3].add("svcCtxEvt : " + StringUtils.toString(servicecontextevent));
        if (Log.DEBUG_ON) {
        	Log.printDebug("receiveServiceContextEvent : " + servicecontextevent);
            Log.printDebug("receiveServiceContextEvent result : " + result);
        }
        notifySelectionResult(result);
        if (needRemoveListener) {
        	try {
        		Environment.getServiceContext(0).removeListener(this);
        	} catch (NullPointerException npe) {}
        }
    }

    private void notifySelectionResult(int result) {
        ServiceSelectionListener li = null;
        for (int i = 0; i < listeners.size(); i++) {
            li = (ServiceSelectionListener) listeners.get(i);
            li.notifyChannelSelectResult(result);
        }
    }
}

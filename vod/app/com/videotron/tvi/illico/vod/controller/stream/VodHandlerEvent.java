package com.videotron.tvi.illico.vod.controller.stream;

import java.util.EventObject;

/**
 * The base class for all VodHandler events.
 */
public class VodHandlerEvent extends EventObject {
    private static final long serialVersionUID = 2441453631153565213L;
    private Object m_act = null;

    /**
     * Constructs a VodHandlerEvent object.
     * 
     * @param objSrc
     *            The source of the event.
     * 
     * @param act
     *            Asynchronous completion token.
     * 
     * @see VodStreamPlayEvent
     * @see VodStreamPauseEvent
     * @see VodStreamFastForwardEvent
     * @see VodStreamRewindEvent
     * @see IVodHandlerListener
     * @see IVodHandler
     * @see VodHandlerFactory
     */
    public VodHandlerEvent(IVodHandler objSrc, Object act) {
        super(objSrc);
        m_act = act;
    }

    /**
     * Returns application data unmodified.
     * 
     * @return asynchronous completion token passed in via the connect method.
     */
    public Object getAct() {
        return m_act;
    }
}

package com.videotron.tvi.illico.vod.controller.session.dsmcc;

/**
 * ReasonCodes An object for representing 4.4 of ISO/IEC 13818-6:1998(E).
 */
public class ReasonCodes {
    /**
     * Refer to ISOP/IEC13818 Section 4.4 Reason Codes for a detailed explanation.
     */
    public static final int RSN_OK = 0x0000;

    public static final int RSN_NORMAL = 0x0001;

    public static final int RSN_CL_PROC_ERROR = 0x0002;

    public static final int RSN_NE_PROC_ERROR = 0x0003;

    public static final int RSN_SE_PROC_ERROR = 0x0004;

    public static final int RSN_CL_FORMAT_ERROR = 0x0005;

    public static final int RSN_NE_FORMAT_ERROR = 0x0006;

    public static final int RSN_SE_FORMAT_ERROR = 0x0007;

    public static final int RSN_NE_CONFIG_CNF = 0x0008;

    public static final int RSN_SE_TRAN_REFUSE = 0x0009;

    public static final int RSN_SE_FORWARD_OVL = 0x000A;

    public static final int RSN_SE_FORWARD_MNT = 0x000B;

    public static final int RSN_SE_FORWARD_UNCOND = 0x000C;

    public static final int RSN_SE_REJ_RESOURCE = 0x000D;

    public static final int RSN_NE_BROADCAST = 0x000E;

    public static final int RSN_SE_SERVICE_TRANSFER = 0x000F;

    public static final int RSN_CL_NO_SESSION = 0x0010;

    public static final int RSN_SE_NO_SESSION = 0x0011;

    public static final int RSN_NE_NO_SESSION = 0x0012;

    public static final int RSN_RETRANS = 0x0013;

    public static final int RSN_NO_TRANSACTION = 0x0014;

    public static final int RSN_CL_NO_RESOURCE = 0x0015;

    public static final int RSN_CL_REJECT_RESOURCE = 0x0016;

    public static final int RSN_NE_REJECT_RESOURCE = 0x0017;

    public static final int RSN_NE_TIMER_EXPIRED = 0x0018;

    public static final int RSN_CL_SESSION_RELEASE = 0x0019;

    public static final int RSN_SE_SESSION_RELEASE = 0x001A;

    public static final int RSN_NE_SESSION_RELEASE = 0x001B;

    // User(Server) Defined Codes
    public static final int RSN_UD_NO_CREDIT_RELEASE = 0x8100;

    public static final int RSN_UD_BTM_STRINGENT = 0x8101;

    /**
     * 
     */
    public ReasonCodes() {

    }

    /**
     * 
     * Convert the reason code to a string.
     * 
     * @param code
     * @return A String representation of the reason code.
     */
    public static String reasonCodeToString(int code) {
        String rc = "";

        switch (code) {
        case RSN_OK:
            rc = "RsnOK";
            break;

        case RSN_NORMAL:
            rc = "RsnNormal";
            break;

        case RSN_CL_PROC_ERROR:
            rc = "RsnClProcError";
            break;

        case RSN_NE_PROC_ERROR:
            rc = "RsnNeProcError";
            break;

        case RSN_SE_PROC_ERROR:
            rc = "RsnSeProcError";
            break;

        case RSN_CL_FORMAT_ERROR:
            rc = "RsnClFormatError";
            break;

        case RSN_NE_FORMAT_ERROR:
            rc = "RsnNeFormatError";
            break;

        case RSN_SE_FORMAT_ERROR:
            rc = "RsnSeFormatError";
            break;

        case RSN_NE_CONFIG_CNF:
            rc = "RsnNeConfigCnf";
            break;

        case RSN_SE_TRAN_REFUSE:
            rc = "RsnSeTranRefuse";
            break;

        case RSN_SE_FORWARD_OVL:
            rc = "RsnSeForwardOvl";
            break;

        case RSN_SE_FORWARD_MNT:
            rc = "RsnSeForwardMnt";
            break;

        case RSN_SE_FORWARD_UNCOND:
            rc = "RsnSeForwardUncond";
            break;

        case RSN_SE_REJ_RESOURCE:
            rc = "RsnSeRejResource";
            break;

        case RSN_NE_BROADCAST:
            rc = "RsnNeBroadcast";
            break;

        case RSN_SE_SERVICE_TRANSFER:
            rc = "RsnSeServiceTransfer";
            break;

        case RSN_CL_NO_SESSION:
            rc = "RsnClNoSession";
            break;

        case RSN_SE_NO_SESSION:
            rc = "RsnSeNoSession";
            break;

        case RSN_NE_NO_SESSION:
            rc = "RsnNeNoSession";
            break;

        case RSN_RETRANS:
            rc = "RsnRetrans";
            break;

        case RSN_NO_TRANSACTION:
            rc = "RsnNoTransaction";
            break;

        case RSN_CL_NO_RESOURCE:
            rc = "RsnClNoResource";
            break;

        case RSN_CL_REJECT_RESOURCE:
            rc = "RsnClRejectResource";
            break;

        case RSN_NE_REJECT_RESOURCE:
            rc = "RsnNeRejectResource";
            break;

        case RSN_NE_TIMER_EXPIRED:
            rc = "RsnNeTimerExpired";
            break;

        case RSN_CL_SESSION_RELEASE:
            rc = "RsnClSessionRelease";
            break;

        case RSN_SE_SESSION_RELEASE:
            rc = "RsnSeSessionRelease";
            break;

        case RSN_NE_SESSION_RELEASE:
            rc = "RsnNeSessionRelease";
            break;

        default:
            rc = "Illegal reason code";
            break;

        }

        return rc;

    }

}

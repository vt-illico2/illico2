package com.videotron.tvi.illico.vod.controller.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.vod.app.Resources;
import org.ocap.hardware.Host;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtils;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.backend.BookmarkServerConfig;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.ListViewUI;

public class BookmarkManager implements Runnable, TVTimerWentOffListener {
	public static final int NOT_COMPLETE = -1;
	public static final int NO_ERROR = 0;
	public static final int LOST_CONNECTION_MOD = 1000;
	public static final int WRONG_SSP = 1102;
	public static final int INVALID_BOOKMARK = 1108;
	public static final int TITLE_NOT_FOUND = 1111;
	public static final int WISHLIST_EXISTS = 1112;
	public static final int INVALID_ACCOUNT = 1113;
	public static final int MAXIMUM_ITEM_EXCEED = 1115;
	public static final int BROKEN_PIPE = 1116;
	public static final int INCLUDED_TITLE_MISSING = 1118;
	public static final int RENTAL_WINDOW_MISSING = 1119;
	public static final int INVALID_CURRENT_NPT = 1121;
	public static final int INVALID_PURCHASE_ID = 1125;
	public static final int INVALID_DATE_FORMAT = 1127;
	public static final int INVALID_PARAM = 1131;
	public static final int INVALID_VALUE = 1132;
	public static final int INCLUDED_TITLE_NOT_FOUND = 1133;
	public static final int LOST_CONNECTION = 1139;
	public static final int INVALID_REQUEST = 1999;

	private static BookmarkManager instance;
	private BookmarkServerConfig serverConfig;
	// private Hashtable bookMarkPool = new Hashtable();
	// private Vector bookMarkOrdering = new Vector();

	private String[] wishlistCache = new String[0];
	private String[] wishlistCacheAdult;
	
	private Bookmark[] activePurchaseCache = new Bookmark[0];
	private Bookmark[] activePurchaseCacheWithBundle = new Bookmark[0];
	private Bookmark[] activePurchaseForRight = new Bookmark[0];
	
	private Bookmark[] activePurchaseCacheAdult;
	private Bookmark[] activePurchaseCacheWithBundleAdult;
	private Bookmark[] activePurchaseForRightAdult;
	

	private TVTimerSpec timer;

	private int numberActivePurchase;
	private int numberActivePurchaseAdult;
	
	private String macAddr;
	private Hashtable sspIdToBid = new Hashtable();
	private int lastEcode;

	public String debug = "", title = "";
	
	private BookmarkManager() {

	}

	public BookmarkServerConfig getConfig() {
		return serverConfig;
	}

	public String macAddr() {
		if (macAddr == null || "00-00-00-00-00-00".equals(macAddr)) {
			byte[] mac = null;
			if (Environment.HW_VENDOR != Environment.HW_VENDOR_SAMSUNG) {
				String hostMAC = Host.getInstance().getReverseChannelMAC();
				mac = StringUtils.macAddressFromStringToByte(hostMAC);
			} else { // samsung
				mac = CommunicationManager.getInstance()
						.getCableCardMacAddress();
			}
			if (mac != null) {
				macAddr = StringUtils.getHexStringBYTEArray(mac).replace(' ',
						'-');
			}
		}
		if (macAddr == null || macAddr.length() == 0 || "00-00-00-00-00-00".equals(macAddr)) {
			CommunicationManager.getInstance().errorMessage("VOD503", null);
		}
		if (Environment.EMULATOR) {
//			macAddr = "00-21-BE-79-BD-43"; // mustapha's
			macAddr = "00-21-BE-7B-1C-F8"; // mustapha's
//			macAddr = "00-21-BE-81-A1-86"; // conskim's
//			macAddr = "00-21-BE-72-97-EB"; // simon's
//			macAddr = "48-44-87-E3-AB-C2";
//			macAddr = "C0-C6-87-1E-5D-E4"; // mustapha's cisco
			macAddr = "10-EA-59-A6-74-8B";
			macAddr = "10-EA-59-9B-38-75"; // Karl
			macAddr = "00-21-BE-79-76-EB"; // QA- SAMSUNG HD
		}
		return macAddr;
	}

	public static BookmarkManager getInstance() {
		if (instance == null) {
			instance = new BookmarkManager();
		}
		return instance;
	}

	public void start() {
		run();
		UIComponent ui = MenuController.getInstance().getCurrentScene();
		if (ui instanceof BaseUI) {
			((BaseUI) ui).updateButtons();
		}
	}

	public void organize() {
		numberActivePurchase = 0;
		if (activePurchaseCache.length > 0 || wishlistCache.length > 0) {
			ArrayList list = new ArrayList();
			for (int i = 0; i < activePurchaseCache.length; i++) {
				Object cached = CatalogueDatabase.getInstance().getCachedBySspId(activePurchaseCache[i].getOrderableName());
				if (cached == null) {
					list.add(activePurchaseCache[i].getOrderableName());
				}
			}
			for (int i = 0; i < wishlistCache.length; i++) {
				Object cached = CatalogueDatabase.getInstance().getCachedBySspId(wishlistCache[i]);
				if (cached == null) {
					list.add(wishlistCache[i]);
				}
			}
			if (list.size() > 0) {
				synchronized (CatalogueDatabase.getInstance()) {
					CatalogueDatabase.getInstance().retrieveItems(null, list, null);
					try {
						CatalogueDatabase.getInstance().wait(
								Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {}
				}
				synchronized (CatalogueDatabase.getInstance()) {
					if (CatalogueDatabase.getInstance().retrieveServices(null)) {
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {}
					}
				}
			}
			splitAdultAndOthers();
			
			removeBundleAndNoExists();
			removeBundleAndNoExistsAdult();
			
			removeDeletedWishlist();
			removeDeletedWishlistAdult();
			ArrayList sspIds = new ArrayList();
			for (int i = 0; i < wishlistCache.length; i++) {
				sspIds.add(wishlistCache[i]);
			}
			for (int i = 0; i < wishlistCacheAdult.length; i++) {
				sspIds.add(wishlistCacheAdult[i]);
			}
			PreferenceProxy.getInstance().setPreference(
					PreferenceNames.VOD_WISH_LIST, sspIds.toString());
		}
	}
	
	private void splitAdultAndOthers() {
		ArrayList normal = new ArrayList();
		ArrayList adult = new ArrayList();
		
		if (activePurchaseCacheAdult == null) {
			Log.printDebug("BookmarkManager, splitAdultAndOthers, stage1, original = " + activePurchaseCache.length);
			for (int i = 0; i < activePurchaseCache.length; i++) {
				Object cached = CatalogueDatabase.getInstance().getCachedBySspId(activePurchaseCache[i].getOrderableName());
				if (cached instanceof Orderable) {
					Orderable o = (Orderable) cached;
					if (isAdult(o)) {
						adult.add(activePurchaseCache[i]);
					} else {
						normal.add(activePurchaseCache[i]);
					}
				} else if (cached instanceof CategorizedBundle) { // karaoke
					normal.add(activePurchaseCache[i]);
				}
			}
			Log.printDebug("BookmarkManager, splitAdultAndOthers, stage1, normal = " + normal.size() + ", adult = " + adult.size());
			
			activePurchaseCache = new Bookmark[normal.size()];
			normal.toArray(activePurchaseCache);
			activePurchaseCacheWithBundle = activePurchaseCache;
			
			activePurchaseCacheAdult = new Bookmark[adult.size()];
			adult.toArray(activePurchaseCacheAdult);
			activePurchaseCacheWithBundleAdult = activePurchaseCacheAdult;
			
			normal.clear();
			adult.clear();
		}
		
		if (wishlistCacheAdult == null) {
			Log.printDebug("BookmarkManager, splitAdultAndOthers, stage2, original = " + wishlistCache.length);
			for (int i = 0; i < wishlistCache.length; i++) {
				Object cached = CatalogueDatabase.getInstance().getCachedBySspId(wishlistCache[i]);
				if (cached instanceof Orderable) {
					Orderable o = (Orderable) cached;
					if (isAdult(o)) {
						adult.add(wishlistCache[i]);
					} else {
						normal.add(wishlistCache[i]);
					}
				}
			}
			Log.printDebug("BookmarkManager, splitAdultAndOthers, stage2, normal = " + normal.size() + ", adult = " + adult.size());
			
			wishlistCache = new String[normal.size()];
			normal.toArray(wishlistCache);
			
			wishlistCacheAdult = new String[adult.size()];
			adult.toArray(wishlistCacheAdult);
			
			normal.clear();
			adult.clear();
		}
	}

	public void run() {
		numberActivePurchase = 0;
		numberActivePurchaseAdult = 0;
		retrieveWishlistTitles(false);
		if (lastEcode == NO_ERROR) {
			retrieveActivePurchases(false);
			debug = macAddr;
		} else {
			if (lastEcode == INVALID_ACCOUNT) {
				debug = "(!)" + macAddr;
			}
			wishlistCache = new String[0];
			wishlistCacheAdult = new String[0];
			activePurchaseCache = new Bookmark[0];
			activePurchaseCacheWithBundle = new Bookmark[0];
			activePurchaseCacheAdult = new Bookmark[0];
			activePurchaseCacheWithBundleAdult = new Bookmark[0];
		}
		organize();
		synchronized (instance) {
			instance.notifyAll();
		}
	}
	
	public int numberActivePurchase(boolean adult) {
		return adult ? numberActivePurchaseAdult : numberActivePurchase;
	}
	
	public void removeDeletedWishlist() {
		Log.printDebug("removeDeletedWishlist, before, "
				+ wishlistCache.length);
		ArrayList sspIds = new ArrayList();
		for (int i = 0; i < wishlistCache.length; i++) {
			BaseElement cached = CatalogueDatabase.getInstance()
					.getCachedBySspId(wishlistCache[i]);
			boolean subs = true;
			if (cached instanceof Video) {
				// R7.3
				subs = CommunicationManager.getInstance().checkPackageForWishList(((Video) cached).service);
			} else if (cached instanceof Bundle) {
				// R7.3
				subs = CommunicationManager.getInstance().checkPackageForWishList(((Bundle) cached).service);
			}
			
			if (cached != null && subs) {
				sspIds.add(wishlistCache[i]);
			}
		}
		if (sspIds.size() == wishlistCache.length) {
			Log.printDebug("removeDeletedWishlist, no change");
			return;
		}
		String[] after = new String[sspIds.size()];
		sspIds.toArray(after);
		wishlistCache = after;
//		PreferenceProxy.getInstance().setPreference(
//				PreferenceNames.VOD_WISH_LIST, sspIds.toString());
		Log.printDebug("removeDeletedWishlist, after, "
				+ wishlistCache.length);
	}
	
	public void removeDeletedWishlistAdult() {
		Log.printDebug("removeDeletedWishlistAdult, before, "
				+ wishlistCacheAdult.length);
		ArrayList sspIds = new ArrayList();
		for (int i = 0; i < wishlistCacheAdult.length; i++) {
			BaseElement cached = CatalogueDatabase.getInstance()
					.getCachedBySspId(wishlistCacheAdult[i]);
			boolean subs = true;
			if (cached instanceof Video) {
				// R7.3
				subs = CommunicationManager.getInstance().checkPackageForWishList(((Video) cached).service);
			} else if (cached instanceof Bundle) {
				// R7.3
				subs = CommunicationManager.getInstance().checkPackageForWishList(((Bundle) cached).service);
			}
			
			if (cached != null && subs) {
				sspIds.add(wishlistCacheAdult[i]);
			}
		}
		if (sspIds.size() == wishlistCacheAdult.length) {
			Log.printDebug("removeDeletedWishlistAdult, no change");
			return;
		}
		String[] after = new String[sspIds.size()];
		sspIds.toArray(after);
		wishlistCacheAdult = after;
//		PreferenceProxy.getInstance().setPreference(
//				PreferenceNames.VOD_WISH_LIST, sspIds.toString());
		Log.printDebug("removeDeletedWishlistAdult, after, "
				+ wishlistCacheAdult.length);
	}
	
	public void removeBundleAndNoExists() {
		activePurchaseForRight = new Bookmark[0];
		if (activePurchaseCache == null || activePurchaseCache.length == 0) {
			return;
		}
		Log.printDebug("removeBundleAndNoExists, before, "
				+ activePurchaseCache.length);
		ArrayList list = new ArrayList();
		ArrayList bundleList = new ArrayList();
		for (int i = 0; i < activePurchaseCache.length; i++) {
			BaseElement cached = CatalogueDatabase.getInstance()
					.getCachedBySspId(activePurchaseCache[i].getOrderableName());
			if (cached == null) {
				continue;
			}
			list.add(activePurchaseCache[i]);
			if (cached instanceof Bundle) {
				bundleList.add(cached);
			}
		}
		numberActivePurchase = list.size();
		for (int i = 0; i < bundleList.size(); i++) {
			if (bundleList.get(i) instanceof Bundle) {
				Bundle bundle = (Bundle) bundleList.get(i);
				MoreDetail[] md = bundle.getTitles();
				for (int j = 0; j < md.length; j++) {
					if (isPurchased((Video) md[j]) != null) {
						numberActivePurchase--;
					}
				}
			}
		}
		ArrayList forRight = new ArrayList();
		item:
		for (int i = 0; i < list.size(); i++) {
			Bookmark bookmark = (Bookmark) list.get(i);
			BaseElement cached = CatalogueDatabase.getInstance().getCachedBySspId(bookmark.getOrderableName());
			if (cached instanceof Bundle) {
				forRight.add(bookmark);
			} else if (cached instanceof Video) {
				Video v = (Video) cached;
				if (v.inBundle()) {
					for (int j = 0; j < v.inBundlesAR.length; j++) {
						String bundleId = v.inBundlesAR[j];
						for (int k = 0; k < bundleList.size(); k++) {
							if (((Bundle) bundleList.get(k)).id.equals(bundleId)) {
								continue item;
							}
						}
					}
				}
				forRight.add(bookmark);
			} else if (cached instanceof CategorizedBundle) {
				forRight.add(bookmark);
			}
		}
		activePurchaseForRight = new Bookmark[forRight.size()];
		forRight.toArray(activePurchaseForRight);
		
		if (list.size() == activePurchaseCache.length) {
			Log.printDebug("removeBundleAndNoExists, no change");
			return;
		}
		Bookmark[] after = new Bookmark[list.size()];
		list.toArray(after);
		activePurchaseCache = after;
		Log.printDebug("removeBundleAndNoExists, after, "
				+ activePurchaseCache.length + " " + list);
	}
	
	public void removeBundleAndNoExistsAdult() {
		activePurchaseForRightAdult = new Bookmark[0];
		if (activePurchaseCacheAdult == null || activePurchaseCacheAdult.length == 0) {
			return;
		}
		Log.printDebug("removeBundleAndNoExistsAdult, before, "
				+ activePurchaseCacheAdult.length);
		ArrayList list = new ArrayList();
		ArrayList bundleList = new ArrayList();
		for (int i = 0; i < activePurchaseCacheAdult.length; i++) {
			BaseElement cached = CatalogueDatabase.getInstance()
					.getCachedBySspId(activePurchaseCacheAdult[i].getOrderableName());
			if (cached == null) {
				continue;
			}
			list.add(activePurchaseCacheAdult[i]);
			if (cached instanceof Bundle) {
				bundleList.add(cached);
			}
		}
		numberActivePurchaseAdult = list.size();
		for (int i = 0; i < bundleList.size(); i++) {
			if (bundleList.get(i) instanceof Bundle) {
				Bundle bundle = (Bundle) bundleList.get(i);
				MoreDetail[] md = bundle.getTitles();
				for (int j = 0; j < md.length; j++) {
					if (isPurchased((Video) md[j]) != null) {
						numberActivePurchaseAdult--;
					}
				}
			}
		}
		ArrayList forRight = new ArrayList();
		item:
		for (int i = 0; i < list.size(); i++) {
			Bookmark bookmark = (Bookmark) list.get(i);
			BaseElement cached = CatalogueDatabase.getInstance().getCachedBySspId(bookmark.getOrderableName());
			if (cached instanceof Bundle) {
				forRight.add(bookmark);
			} else if (cached instanceof Video) {
				Video v = (Video) cached;
				if (v.inBundle()) {
					for (int j = 0; j < v.inBundlesAR.length; j++) {
						String bundleId = v.inBundlesAR[j];
						for (int k = 0; k < bundleList.size(); k++) {
							if (((Bundle) bundleList.get(k)).id.equals(bundleId)) {
								continue item;
							}
						}
					}
				}
				forRight.add(bookmark);
			} else if (cached instanceof CategorizedBundle) {
				forRight.add(bookmark);
			}
		}
		activePurchaseForRightAdult = new Bookmark[forRight.size()];
		forRight.toArray(activePurchaseForRightAdult);
		
		if (list.size() == activePurchaseCacheAdult.length) {
			Log.printDebug("removeBundleAndNoExistsAdult, no change");
			return;
		}
		Bookmark[] after = new Bookmark[list.size()];
		list.toArray(after);
		activePurchaseCacheAdult = after;
		Log.printDebug("removeBundleAndNoExistsAdult, after, "
				+ activePurchaseCacheAdult.length + " " + list);
	}

//	public static void main(String args[]) throws Exception {
//		BookmarkServerConfig conf = new BookmarkServerConfig();
//		conf.setIp("10.247.56.146");
//		conf.setPort(8080);
//		conf.setRootContext("controlsvr");
//		getInstance().setServerConfig(conf);
//
//		DataCenter.getInstance().put("MESSAGE_RETRY_COUNT", new Integer(2));
//
//		// int ret = getInstance().storeWishlist("test");
//		// System.out.println("RET = " + ret);
//
//		// String[] sRet = getInstance().retrieveWishlistTitles();
//		// for (int i = 0; i < sRet.length; i++)
//		// System.out.println("sRet = " + sRet[i]);
//		// Bookmark[] bm = getInstance().retrieveActivePurchases(false);
//		// for (int i = 0; i < bm.length; i++)
//		// System.out.println("bm = " + bm[i]);
//
//		// getInstance().storeWishlist("district_9_va");
//		// sRet = getInstance().retrieveWishlistTitles();
//		// for (int i = 0; i < sRet.length; i++)
//		// System.out.println("sRet = " + sRet[i]);
//		//
//		// getInstance().deleteWishlist("district_9_va");
//		// sRet = getInstance().retrieveWishlistTitles();
//		// for (int i = 0; i < sRet.length; i++)
//		// System.out.println("sRet = " + sRet[i]);
//
//		// getInstance().purchaseAsset("district_9_va", "0.00", 60000 * 5);
//
//		// boolean ret = instance.setNPT("33103", 123);
//		// System.out.println("ret = " + ret + ", l = " + instance.lastEcode);
//		// int npt = instance.getNPT("district_9_va");
//		// System.out.println("npt = " + npt);
//
//		String[] names = new String[] { "smq2010_apres_match_08_vf",
//				"belle_famille_vf", "meet_the_parents_va",
//				"destinees_3_19_pub30_vf", "camera_cafe_8_13_pub30_vf",
//				"becoming_jane_va", "garcons_epatants_vf",
//				"2_filles_matin_88_vf", "razmoket_film_vf",
//				"razmoket_paris_vf", "if_lucy_fell_vf",
//				"pieds_dans_le_vide_hd_vf", "promesse_5_19_pub30_vf",
//				"sacre_bordel_vf", "fin_des_temps_vf", "shaft_vf",
//				"lignes_interdites_vf", "district_9_hd_vf", "district_9_hd_va",
//				"district_9_va", };
//		for (int i = 0; i < names.length; i++) {
//			instance.purchaseAsset(names[i], "0.0", 3600000L);
//			System.out.println("");
//			Thread.sleep(1500);
//		}
//	}

	public int getLastEcode() {
		return lastEcode;
	}

	public String makeRequest(String command) {
		if (serverConfig == null) {
			Log.printError("Bookmark, serverConfig is null, use default : "
							+ DataCenter.getInstance().getString("BOOKMARK_URL"));
			return DataCenter.getInstance().getString("BOOKMARK_URL") + command
					+ "?macAddress=" + macAddr();
		}
		if (serverConfig.getIp() == null
				|| serverConfig.getRootContext() == null) {
			Log.printError("Bookmark, serverConfig is invalid, "
					+ serverConfig + ", use default : "
					+ DataCenter.getInstance().getString("BOOKMARK_URL"));
			return DataCenter.getInstance().getString("BOOKMARK_URL") + command
					+ "?macAddress=" + macAddr();
		}
		String ctx = serverConfig.getRootContext();
		if (!ctx.startsWith("/")) {
			ctx = "/" + ctx;
		}
		if (!ctx.endsWith("/")) {
			ctx = ctx + "/";
		}
		serverConfig.setRootContext(ctx);
		String request = "http://" + serverConfig.getIp() + ":"
				+ serverConfig.getPort() + serverConfig.getRootContext()
				+ command + "?macAddress=" + macAddr();
		return request;
	}

	private Vector commWithCS(String request, String method) throws IOException {
		MenuController.getInstance().debugList[2].add(request.substring(request.indexOf('?')));
		MenuController.getInstance().debugList[2].add(CatalogueDatabase.SDF.format(new Date()) + ", " + request.substring(0, request.indexOf('?')));
		try {
			Vector ret = URLRequestor.getLines(request, null, null);
			for (int i = 1; i < ret.size(); i++) {
				StringTokenizer tok = new StringTokenizer(
						(String) ret.get(i), "<>");
				while (tok.hasMoreTokens()) {
					String token = tok.nextToken();
					MenuController.getInstance().debugList[2].add(CatalogueDatabase.SDF.format(new Date()) + ", " + "≤"+token+"≥");
					Log.printDebug("<" + token + ">");
				}
			}
			return ret;
		} catch (IOException ioe) {
			if ("Timeout".equals(ioe.getMessage())) {
				MenuController.getInstance().debugList[2].add(CatalogueDatabase.SDF.format(new Date()) + ", TIME OUT");
				CommunicationManager.getInstance().errorMessage("VOD302", null);
			} else {
				MenuController.getInstance().debugList[2].add(CatalogueDatabase.SDF.format(new Date()) + ", IOException");
				CommunicationManager.getInstance().errorMessage("VOD301", null);
			}
			throw ioe;
		}
	}

	private boolean checkErrorCode(String method) {
		String errorCode = "";
		switch (lastEcode) {
		case NO_ERROR:
			return false;
		case LOST_CONNECTION_MOD:// = 1000;
			errorCode = "VOD320";
			break;
		case WRONG_SSP:// = 1102;
			errorCode = "VOD319";
			break;
		case INVALID_BOOKMARK:// = 1108;
			errorCode = "VOD318";
			break;
		case TITLE_NOT_FOUND:// = 1111;
			errorCode = "VOD317";
			break;
		case WISHLIST_EXISTS:// = 1112;
//			errorCode = "VOD316";
			break; // ignore it
		case INVALID_ACCOUNT:// = 1113;
			errorCode = "VOD206";
			break;
		case MAXIMUM_ITEM_EXCEED:// = 1115;
			errorCode = "VOD315";
			break;
		case BROKEN_PIPE:// = 1116;
			errorCode = "VOD314";
			break;
		case INCLUDED_TITLE_MISSING:// = 1118;
			errorCode = "VOD313";
			break;
		case RENTAL_WINDOW_MISSING:// = 1119;
			errorCode = "VOD312";
			break;
		case INVALID_CURRENT_NPT:// = 1121;
			errorCode = "VOD311";
			break;
		case INVALID_PURCHASE_ID:// = 1125;
			errorCode = "VOD310";
			break;
		case INVALID_DATE_FORMAT:// = 1127;
			errorCode = "VOD309";
			break;
		case INVALID_PARAM:// = 1131;
			errorCode = "VOD308";
			break;
		case INVALID_VALUE:// = 1132;
			errorCode = "VOD307";
			break;
		case INCLUDED_TITLE_NOT_FOUND:// = 1133;
			errorCode = "VOD306";
			break;
		case LOST_CONNECTION:// = 1139;
			errorCode = "VOD305";
			break;
		case INVALID_REQUEST:// = 1999;
			errorCode = "VOD304";
			break;
		case NOT_COMPLETE:
			return true; // it may display time-out error
		default:
			errorCode = "VOD321";
			break;
		}
		if (errorCode.length() > 0 && method.length() > 0) {
			if (errorCode.charAt(3) == '3') {
				errorCode += "-" + method;
			}
		}
		if (errorCode.length() > 0) {
			CommunicationManager.getInstance().errorMessage(errorCode, null);
			return true;
		}
		return false;
	}

	public boolean storeWishlist(String sspId, boolean bundle) {
		title = sspId;
		String request = makeRequest(bundle ? "storeWishlistBundle" : "storeWishlistTitle") + "&sspId=" + sspId;
		Log.printInfo("BookmarkManager, storeWishlist(" + (bundle ? "bundle)" : "normal)"));
		if (Log.DEBUG_ON) {
			Log.printDebug("storeWishlist(), request = " + request);
		}
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Vector ret = commWithCS(request, "07");
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							if (Log.DEBUG_ON) {
								Log.printDebug("storeWishlist(), ecode = "
												+ lastEcode);
							}
						}
					}
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
		} catch (Exception e) {
			Log.print(e);
			return false;
		}
		checkErrorCode("07");
		return lastEcode == NO_ERROR || lastEcode == WISHLIST_EXISTS;
	}
	
	public String isInWishlist(MoreDetail md) {
		if (md instanceof Video) {
			return isInWishlist((Video) md);
		} else if (md instanceof Bundle) {
			return isInWishlist((Bundle) md);
		}
		return null;
	}

	public String isInWishlist(Video title) {
		if (title == null || title.detail == null) {
			return null;
		}
		for (int i = 0; i < title.detail.length; i++) {
			if (isInWishlist(title.detail[i].orderableItem)) {
				String bid = (String) sspIdToBid
						.get(title.detail[i].orderableItem);
				if (Log.DEBUG_ON) {
					Log.printDebug("isInWishlist()" + ", title = "
							+ title.toString(true) + ", sspId = "
							+ title.detail[i].orderableItem + ", bid = " + bid);
				}
				return bid;
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("isInWishlist()" + ", title = "
					+ title.toString(true) + ", FALSE");
		}
		return null;
	}
	
	public String isInWishlist(Bundle bundle) {
		if (bundle == null || bundle.detail == null) {
			return null;
		}
		for (int i = 0; i < bundle.detail.length; i++) {
			if (isInWishlist(bundle.detail[i].orderableItem)) {
				String bid = (String) sspIdToBid
						.get(bundle.detail[i].orderableItem);
				if (Log.DEBUG_ON) {
					Log.printDebug("isInWishlist()" + ", bundle = "
							+ bundle.toString(true) + ", sspId = "
							+ bundle.detail[i].orderableItem + ", bid = " + bid);
				}
				return bid;
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("isInWishlist()" + ", bundle = "
					+ bundle.toString(true) + ", FALSE");
		}
		return null;
	}

	public boolean isInWishlist(String sspId) {
		if (sspId == null)
			return false;
		for (int i = 0; wishlistCache != null && i < wishlistCache.length; i++) {
			if (sspId.equals(wishlistCache[i])) {
				return true;
			}
		}
		for (int i = 0; wishlistCacheAdult != null && i < wishlistCacheAdult.length; i++) {
			if (sspId.equals(wishlistCacheAdult[i])) {
				return true;
			}
		}
		return false;
	}

	public String[] retrieveWishlistTitlesAdult() {
		return wishlistCacheAdult;
	}
	public String[] retrieveWishlistTitles(boolean fromCache) {
		if (wishlistCache != null && fromCache) {
			if (Log.DEBUG_ON) {
				Log.printDebug("retrieveWishlistTitles(), wishlistCache = "
								+ wishlistCache.length);
				StringBuffer b = new StringBuffer("list = ");
				for (int i = 0; i < wishlistCache.length; i++) {
					b.append(wishlistCache[i]);
					b.append(",");
				}
				Log.printDebug(b.toString());
			}
			return wishlistCache;
		}

		ArrayList sspIds = new ArrayList();
		String request = makeRequest("retrieveWishlistTitles");
		Log.printInfo("BookmarkManager, retrieveWishlistTitles()");
		if (Log.DEBUG_ON) {
			Log.printDebug("retrieveWishlistTitles(), request = "
					+ request);
		}
		try {
			sspIdToBid.clear();
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				//String sspId = null;
				Vector ret = commWithCS(request, "08");
				if (Environment.EMULATOR) {
//					ret.clear();
//					ret.add("<WishlistTitle sspID=ddc10070_ingest_mp4_exclusif_i2_v2_hd_va bid=6539 hd=true ey=2013 psspID=ddc0070_ingest_mp4_exclusif_i2_v2_hd_va_Preview pri=4.99 ed=11 />");
//					ret.add("<WishlistTitle sspID=film_43_for_chris_vf_Pub40 bid=6967 hd=false ey=2018 psspID= pri=0 ed=25 />");
//					ret.add("<WishlistTitle sspID=film_test_06_vf bid=6630 hd=false ey=2018 psspID= pri=0 ed=25 />");
//					ret.add("<WishlistTitle sspID=karate_kid_hd_vf bid=7319 hd=true ey=2020 psspID=karate_kid_hd_vf_Preview pri=0 ed=31 />");
					
//					ret.add("<retrieveWishlistTitles ecode=\"0\">");
//					ret.add("<WishlistTitle sspID=\"dexter_hd_forfait_3_vf\" bid=\"36770\" hd=\"true\" ey=\"2020\" psspID=\"\" pri=\"0\" ed=\"7\" />");
//					ret.add("<WishlistTitle sspID=\"dexter_1_07_hd_vf\" bid=\"36771\" hd=\"true\" ey=\"2020\" psspID=\"\" pri=\"0\" ed=\"7\" />");
//					ret.add("<WishlistTitle sspID=\"dexter_1_08_hd_vf\" bid=\"36772\" hd=\"true\" ey=\"2020\" psspID=\"\" pri=\"0\" ed=\"7\" />");
//					ret.add("<WishlistTitle sspID=\"dexter_1_09_hd_vf\" bid=\"36773\" hd=\"true\" ey=\"2020\" psspID=\"\" pri=\"0\" ed=\"7\" />");
//					ret.add("</retrieveWishlistTitles>");	
				}
/*
<retrieveWishlistTitles ecode="0">
<WishlistTitle ed="29" pri="6.99" em="12" ey="2025" hd="false" bid="7884036" sspID="pac_revenant_le_hd_vf"/>
<WishlistTitle ed="29" pri="3.49" em="12" ey="2025" hd="false" bid="7884652" sspID="PACface_cachee_de_marg_lib_vf"/>
<WishlistTitle ed="29" pri="6.99" em="12" ey="2025" hd="false" bid="7884653" sspID="PACendorphine_hd_vf"/>
</retrieveWishlistTitles>
*/
                //->Kenneth : VDTRMASTER-6255
				for (int i = 1; i < ret.size(); i++) {
                    String aLine = (String)ret.get(i);
                    if (aLine.indexOf("retrieveWishlistTitles") > 0 && aLine.indexOf("ecode") > 0) {
                        // ecode
                        StringTokenizer st = new StringTokenizer(aLine, "\"");
                        st.nextToken();
                        lastEcode = Integer.parseInt(st.nextToken());
                        Log.printDebug("KEN : lastEcode = "+lastEcode);
                        if (lastEcode == LOST_CONNECTION) {
                            break;
                        }
                        if (lastEcode != NO_ERROR) {
                            if (Log.WARNING_ON) {
                                Log.printWarning("retrieveWishlistTitles(), ecode = "
                                                + lastEcode);
                            }
                            wishlistCache = new String[0];
                            wishlistCacheAdult = new String[0];
                            checkErrorCode("08");
                            return wishlistCache;
                        }
                        continue;
                    }
                    if (aLine.indexOf("WishlistTitle") > 0 && aLine.indexOf("sspID") > 0 && aLine.indexOf("bid") > 0) {
                        // sspId And bid
                        String sspId = "";
                        String bid = "";
                        StringTokenizer st = new StringTokenizer(aLine, " =\"");
                        while (st.hasMoreTokens()) {
                            String token = st.nextToken();
                            if (token.equals("sspID")) {
                                sspId = st.nextToken();
                            } else if (token.equals("bid")) {
                                bid = st.nextToken();
                            }
                        }
                        Log.printDebug("KEN : sspID = "+sspId);
                        Log.printDebug("KEN : bid = "+bid);
                        sspIds.add(sspId);
                        sspIdToBid.put(sspId, bid);
                    }
                }

                /*
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							if (lastEcode == LOST_CONNECTION) {
								break;
							}
							if (lastEcode != NO_ERROR) {
								if (Log.WARNING_ON) {
									Log.printWarning("retrieveWishlistTitles(), ecode = "
													+ lastEcode);
								}
								wishlistCache = new String[0];
								wishlistCacheAdult = new String[0];
								checkErrorCode("08");
								return wishlistCache;
							}
						} else if ("sspID".equals(token)) {
							sspId = tok.nextToken();
                            Log.printDebug("sspIds.add("+sspId+")");
							sspIds.add(sspId);
						} else if ("bid".equals(token)) {
                            String bidStr = tok.nextToken();
							if (sspId != null) {
                                Log.printDebug("sspIdToBid.put("+sspId+", "+bidStr+")");
								sspIdToBid.put(sspId, bidStr);
								//sspIdToBid.put(sspId, tok.nextToken());
								sspId = null;
							} else {
                                Log.printDebug("bid but sspId is null. So do nothing : bid = "+bidStr);
                            }
						}
					}
				}
                */
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
		} catch (Exception e) {
			Log.print(e);
		}
		
		request = makeRequest("retrieveWishlistBundles");
		Log.printInfo("BookmarkManager, retrieveWishlistBundles()");
		if (Log.DEBUG_ON) {
			Log.printDebug("retrieveWishlistBundles(), request = "
					+ request);
		}
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				//String sspId = null;
				Vector ret = commWithCS(request, "10");
//				if (Environment.EMULATOR) {
//					ret.clear();
//					ret.add("<PurchaseItem cnpt=0 sspID=19_2_forfait_01_vf run=1 bid=9968 lr=604800 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=forfait_gym_3d_vf run=1 bid=9969 lr=554940 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=xanadu_hd_forfait_01_vf run=88 bid=8830 lr=3539 />");
//				}

                //->Kenneth : VDTRMASTER-6255
				for (int i = 1; i < ret.size(); i++) {
                    String aLine = (String)ret.get(i);
                    if (aLine.indexOf("retrieveWishlistBundles") > 0 && aLine.indexOf("ecode") > 0) {
                        // ecode
                        StringTokenizer st = new StringTokenizer(aLine, "\"");
                        st.nextToken();
                        lastEcode = Integer.parseInt(st.nextToken());
                        Log.printDebug("KEN : lastEcode = "+lastEcode);
                        if (lastEcode == LOST_CONNECTION) {
                            break;
                        }
                        if (lastEcode != NO_ERROR) {
                            if (Log.WARNING_ON) {
                                Log.printWarning("retrieveWishlistBundles(), ecode = "
                                                + lastEcode);
                            }
                            wishlistCache = new String[0];
                            checkErrorCode("10");
                            return wishlistCache;
                        }
                        continue;
                    }
                    if (aLine.indexOf("WishlistBundle") > 0 && aLine.indexOf("sspID") > 0 && aLine.indexOf("bid") > 0) {
                        // sspId And bid
                        String sspId = "";
                        String bid = "";
                        StringTokenizer st = new StringTokenizer(aLine, " =\"");
                        while (st.hasMoreTokens()) {
                            String token = st.nextToken();
                            if (token.equals("sspID")) {
                                sspId = st.nextToken();
                            } else if (token.equals("bid")) {
                                bid = st.nextToken();
                            }
                        }
                        Log.printDebug("KEN : sspID = "+sspId);
                        Log.printDebug("KEN : bid = "+bid);
                        sspIds.add(sspId);
                        sspIdToBid.put(sspId, bid);
                    }
                }

                /*
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							if (lastEcode == LOST_CONNECTION) {
								break;
							}
							if (lastEcode != NO_ERROR) {
								if (Log.WARNING_ON) {
									Log.printWarning("retrieveWishlistBundles(), ecode = "
													+ lastEcode);
								}
								wishlistCache = new String[0];
								checkErrorCode("10");
								return wishlistCache;
							}
						} else if ("sspID".equals(token)) {
							sspId = tok.nextToken();
							sspIds.add(sspId);
						} else if ("bid".equals(token)) {
							if (sspId != null) {
								sspIdToBid.put(sspId, tok.nextToken());
								sspId = null;
							}
						}
					}
				}
                */
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
		} catch (Exception e) {
			Log.print(e);
		}
//		sspIds.add("dexter_1_01_hd_vf");
//		sspIds.add("fast_five_unrtd_hd_va");//30_vagins_orgies_vf");
//		sspIds.add("tendresse_ordinaire_hd_vf");
//		sspIds.add("rio_making_a_scene_va");
//		sspIdToBid.put("dexter_1_01_hd_vf", "22222");
//		sspIdToBid.put("fast_five_unrtd_hd_va", "33333");//30_vagins_orgies_vf", "33333");
//		sspIdToBid.put("tendresse_ordinaire_hd_vf", "11111");
//		sspIdToBid.put("rio_making_a_scene_va", "11111");
		
		String[] ret = new String[sspIds.size()];
		sspIds.toArray(ret);
		wishlistCache = ret;
		wishlistCacheAdult = null;
		
		PreferenceProxy.getInstance().setPreference(
			PreferenceNames.VOD_WISH_LIST, sspIds.toString());
		if (Log.DEBUG_ON) {
			Log.printDebug("retrieveWishlistTitles(), ret = "
					+ ret.length + ", list = " + sspIds);
		}
		checkErrorCode("08");
		return ret;
	}
	
	public String deleteWishlist(String[] sspId) {
		for (int i = 0; i < sspId.length; i++) {
			if (sspIdToBid.containsKey(sspId[i])) {
				return deleteWishlist(sspId[i]);
			}
		}
		return null;
	}

	public String deleteWishlist(String sspId) {
		String bId = (String) sspIdToBid.get(sspId);
		Log.printInfo("BookmarkManager, deleteWishlist()");
		if (bId == null || bId.length() == 0) {
			Log.printWarning("deleteWishlist(), could not found 'bookmark ID' for <"
							+ sspId + ">");
			return null;
		}
		String request = makeRequest("deleteWishlist") + "&bookmarkId=" + bId
				+ "&returnList=false";
		if (Log.DEBUG_ON) {
			Log.printDebug("deleteWishlist(), request = " + request);
		}
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Vector ret = commWithCS(request, "09");
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							if (Log.DEBUG_ON) {
								Log.printDebug("deleteWishlist(), ecode = "
												+ lastEcode);
							}
							if (lastEcode == LOST_CONNECTION) {
								break;
							}
							if (lastEcode == NO_ERROR) {
								ArrayList list = new ArrayList();
								for (int j = 0; j < wishlistCache.length; j++) {
									if (wishlistCache[j].equals(sspId))
										continue;
									list.add(wishlistCache[j]);
								}
								wishlistCache = new String[list.size()];
								list.toArray(wishlistCache);
								
								list.clear();
								// R5 - handle in the case of wishlistCacheAdult is null
								for (int j = 0; wishlistCacheAdult != null && j < wishlistCacheAdult.length; j++) {
									if (wishlistCacheAdult[j].equals(sspId))
										continue;
									list.add(wishlistCacheAdult[j]);
								}
								wishlistCacheAdult = new String[list.size()];
								list.toArray(wishlistCacheAdult);
							}
							checkErrorCode("09");
							return lastEcode == NO_ERROR ? sspId : null;
						}
					}
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
		} catch (Exception e) {
			Log.print(e);
		}
		checkErrorCode("09");
		return null;
	}

	public boolean purchaseBundle(String sspId, String price,
			long leaseDuration, String[] titles) {
		title = sspId;
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < titles.length; i++) {
			if (titles[i] != null && titles[i].length() == 0) continue;
			buf.append(titles[i]).append(":");
		}
		String inclTitles = buf.length() == 0 ? "" : ("&inclTitles=" + buf
				.substring(0, buf.length() - 1));
		int rental = (int) (leaseDuration / Constants.MS_PER_HOUR);
		String request = makeRequest("purchaseBundle") + "&sspId=" + sspId
				+ "&offerPrice=" + price + "&rentalWindow=" + rental
				+ inclTitles;
		Log.printInfo("BookmarkManager, purchaseBundle()");
		if (Log.DEBUG_ON) {
			Log.printDebug("purchaseBundle(), request = " + request);
		}
		String bid = null;
		boolean appear = false;
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Vector ret = commWithCS(request, "02");
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
						} else if ("bid".equals(token)) {
							appear = true;
							bid = tok.nextToken();
						}
					}
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
			if (Log.INFO_ON) {
				Log.printInfo("purchaseBundle(), " + lastEcode
						+ ", bid = " + bid);
			}
			if (lastEcode == NO_ERROR) {
				retrieveActivePurchases(false);
				organize();
				//removeBundleAndNoExists();
				VbmController.getInstance().writePurchase(sspId);
				return true;
			}
		} catch (Exception e) {
			Log.print(e);
		}
		if (checkErrorCode("02") == false && appear == false) {
			CommunicationManager.getInstance().errorMessage("VOD303", null);
		}
		return false;
	}

	public Bookmark purchaseAsset(String sspId, String price, long leaseDuration) {
		title = sspId;
		String request = makeRequest("purchaseAsset") + "&sspId=" + sspId
				+ "&offerPrice=" + price;
		Log.printInfo("BookmarkManager, purchaseAsset()");
		if (Log.DEBUG_ON) {
			Log.printDebug("purchaseAsset(), request = " + request
					+ ", dur = " + leaseDuration);
		}
		Bookmark bookmark = new Bookmark();
		boolean appear = false;
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Vector ret = commWithCS(request, "01");
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							bookmark.setErrorCode(lastEcode);
						} else if ("bid".equals(token)) {
							appear = true;
							bookmark.setBookmarkID(tok.nextToken());
						}
					}
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
			if (lastEcode == NO_ERROR) {
				retrieveActivePurchases(false);
				organize();
				if (Log.INFO_ON) {
					Log.printInfo("purchaseAsset(), " + bookmark);
				}
				VbmController.getInstance().writePurchase(sspId);
				return bookmark;
			}
		} catch (Exception e) {
			Log.print(e);
		}
		if (checkErrorCode("01") == false && appear == false) {
			CommunicationManager.getInstance().errorMessage("VOD303", null);
		}
		return bookmark;
	}

	public Bookmark getPurchased(String sspId) {
		long current = System.currentTimeMillis();
		for (int j = 0; j < activePurchaseCacheWithBundle.length; j++) {
			if (sspId.equals(activePurchaseCacheWithBundle[j].getOrderableName())
					&& current < activePurchaseCacheWithBundle[j].getLeaseEnd()) {
				return activePurchaseCacheWithBundle[j];
			}
		}
		for (int j = 0; activePurchaseCacheWithBundleAdult != null && j < activePurchaseCacheWithBundleAdult.length; j++) {
			if (sspId.equals(activePurchaseCacheWithBundleAdult[j].getOrderableName())
					&& current < activePurchaseCacheWithBundleAdult[j].getLeaseEnd()) {
				return activePurchaseCacheWithBundleAdult[j];
			}
		}
		return null;
	}

	public Bookmark getPurchased(Bundle title) {
		long current = System.currentTimeMillis();
		for (int i = 0; i < title.detail.length; i++) {
			String sspId = title.detail[i].orderableItem;
			for (int j = 0; j < activePurchaseCacheWithBundle.length; j++) {
				if (sspId.equals(activePurchaseCacheWithBundle[j].getOrderableName())
						&& current < activePurchaseCacheWithBundle[j]
								.getLeaseEnd()) {
					return activePurchaseCacheWithBundle[j];
				}
			}
			for (int j = 0; activePurchaseCacheWithBundleAdult != null && j < activePurchaseCacheWithBundleAdult.length; j++) {
				if (sspId.equals(activePurchaseCacheWithBundleAdult[j].getOrderableName())
						&& current < activePurchaseCacheWithBundleAdult[j]
								.getLeaseEnd()) {
					return activePurchaseCacheWithBundleAdult[j];
				}
			}
		}
		return null;
	}

	public Bookmark getPurchased(Video title) {
		long current = System.currentTimeMillis();
		for (int i = 0; i < title.detail.length; i++) {
			String sspId = title.detail[i].orderableItem;
			for (int j = 0; j < activePurchaseCache.length; j++) {
				if (sspId.equals(activePurchaseCache[j].getOrderableName())
						&& current < activePurchaseCache[j].getLeaseEnd()) {
					return activePurchaseCache[j];
				}
			}
			for (int j = 0; activePurchaseCacheAdult != null && j < activePurchaseCacheAdult.length; j++) {
				if (sspId.equals(activePurchaseCacheAdult[j].getOrderableName())
						&& current < activePurchaseCacheAdult[j].getLeaseEnd()) {
					return activePurchaseCacheAdult[j];
				}
			}
		}
		return null;
	}

	public Bookmark getPurchased(CategorizedBundle title) {
		long current = System.currentTimeMillis();
		for (int i = 0; i < title.detail.length; i++) {
			String sspId = title.detail[i].orderableItem;
			for (int j = 0; j < activePurchaseCache.length; j++) {
				if (sspId.equals(activePurchaseCache[j].getOrderableName())
						&& current < activePurchaseCache[j].getLeaseEnd()) {
					return activePurchaseCache[j];
				}
			}
			for (int j = 0; activePurchaseCacheAdult != null &&  j < activePurchaseCacheAdult.length; j++) {
				if (sspId.equals(activePurchaseCacheAdult[j].getOrderableName())
						&& current < activePurchaseCacheAdult[j].getLeaseEnd()) {
					return activePurchaseCacheAdult[j];
				}
			}
		}
		return null;
	}

	public boolean isPurchased(Bundle title) {
		for (int i = 0; i < title.detail.length; i++) {
			if (isPurchased(title.detail[i].orderableItem, true)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("isPurchased()" + ", title = "
							+ title.toString(true) + ", sspId = "
							+ title.detail[i].orderableItem);
				}
				return true;
			}
		}
		// or check all titles.. backend 286
		MoreDetail[] md = title.getTitles();
		boolean purchased = true;
		for (int i = 0; i < md.length; i++) {
			if (isPurchased((Video) md[i]) == null) {
				purchased = false;
				break;
			}
		}
		if (purchased && md.length > 0 && ((Video) md[0]).inBundle()) { // possibility from web-purchasing
			String bundleId = ((Video) md[0]).inBundlesAR[0];
			Bundle bundle = (Bundle) CatalogueDatabase.getInstance().getCached(bundleId);
			if (bundle != null) {
				if (getPurchased(bundle) == null) {
					Bookmark dummy = new Bookmark();
					dummy.setOrderableName(bundle.getSspId());
					dummy.setLeaseEnd(System.currentTimeMillis()+Constants.MS_PER_HOUR);
					dummy.setLeaseRemaing((int) Constants.MS_PER_HOUR);
					if (isAdult(bundle)) {
						Bookmark[] newArray = new Bookmark[activePurchaseCacheWithBundleAdult.length + 1];
						System.arraycopy(activePurchaseCacheWithBundleAdult, 0, newArray, 1, activePurchaseCacheWithBundleAdult.length);
						newArray[0] = dummy;
						activePurchaseCacheWithBundleAdult = newArray;
					} else {
						Bookmark[] newArray = new Bookmark[activePurchaseCacheWithBundle.length + 1];
						System.arraycopy(activePurchaseCacheWithBundle, 0, newArray, 1, activePurchaseCacheWithBundle.length);
						newArray[0] = dummy;
						activePurchaseCacheWithBundle = newArray;
					}
				}
			}
		}
		return purchased;
	}
	
	public boolean isAdult(Orderable o) {
		boolean adultRating = Definition.RATING_OVER_18.equals(o.getRating()) && Definition.EXPLICIT_SEXUALITY.equals(o.getRatingInfo());
		if (o instanceof Video) {
			Video v = (Video) o;
			if (v.channel != null) {
				return false;
			}
		}
		return adultRating;
	}

	public String isPurchased(Video title) {
		for (int i = 0; title.detail != null && i < title.detail.length; i++) {
			if (isPurchased(title.detail[i].orderableItem, false)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("isPurchased()" + ", title = "
							+ title.toString(true) + ", sspId = "
							+ title.detail[i].orderableItem);
				}
				return title.detail[i].orderableItem;
			}
		}
		return null;
	}

	public boolean isPurchased(String sspId, boolean withBundle) {
		if (sspId == null) {
			return false;
		}
		
		long cur = System.currentTimeMillis();
		Bookmark[] bookmarks = withBundle ? activePurchaseCacheWithBundle
				: activePurchaseCache;
		for (int i = 0; bookmarks != null && i < bookmarks.length; i++) {
			if (sspId.equals(bookmarks[i].getOrderableName())
					&& cur < bookmarks[i].getLeaseEnd()) {
				return true;
			}
		}
		bookmarks = withBundle ? activePurchaseCacheWithBundleAdult
				: activePurchaseCacheAdult;
		for (int i = 0; bookmarks != null && i < bookmarks.length; i++) {
			if (sspId.equals(bookmarks[i].getOrderableName())
					&& cur < bookmarks[i].getLeaseEnd()) {
				return true;
			}
		}
		return false;
	}
	
	public Bookmark[] getActivePurchaseForRight(boolean adult) {
		return adult ? activePurchaseForRightAdult : activePurchaseForRight;
	}
	
	public Bookmark[] retrieveActivePurchasesAdult() {
		return activePurchaseCacheAdult;
	}
	
	public Bookmark[] retrieveActivePurchases(boolean fromCache) {
		if (activePurchaseCache != null && fromCache) {
			return activePurchaseCache;
		}
		String request = makeRequest("retrieveActivePurchases");
		Log.printInfo("BookmarkManager, retrieveActivePurchases()");
		if (Log.DEBUG_ON) {
			Log.printDebug("retrieveActivePurchases(), request = "
					+ request);
		}
		ArrayList bookmarks = new ArrayList();
		// Calendar cal = Calendar.getInstance();
		long cur = System.currentTimeMillis();
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			int remain = 10 * 60 * 60; // 10 hour
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Bookmark bookmark = null;
				Vector ret = commWithCS(request, "04");
				if (Environment.EMULATOR) {
//					ret.clear(); 
//					ret.add("<retrieveActivePurchasesResp ecode=0>");
//					ret.add("<PurchaseItem cnpt=73 sspID=tudors_1_04_vf run=50 bid=10145 lr=107038 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=tudors_1_05_vf run=50 bid=10146 lr=107038 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=tudors_1_06_vf run=50 bid=10147 lr=107038 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=tudors_les_1_forfait_02_vf run=1 bid=10148 lr=161804 />");
//					ret.add("</retrieveActivePurchasesResp>");
					
//					ret.add("<retrieveActivePurchasesResp ecode=0>");
//					ret.add("<PurchaseItem cnpt=0 sspID=dix_neuf_deux_1_01_vf run=44 bid=9965 lr=554940 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=dix_neuf_deux_1_02_vf run=44 bid=9966 lr=554940 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=dix_neuf_deux_1_03_vf run=43 bid=9967 lr=554940 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=19_2_forfait_01_vf run=1 bid=9968 lr=604800 />");
//					ret.add("</retrieveActivePurchasesResp>");
					
//					ret.add("<retrieveActivePurchasesResp ecode=0>");
//					ret.add("<PurchaseItem cnpt=40 sspID=xanadu_01_hd_vf run=88 bid=8831 lr=3539 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=xanadu_02_hd_vf run=88 bid=8832 lr=3539 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=xanadu_03_hd_vf run=88 bid=8833 lr=3539 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=xanadu_hd_forfait_01_vf run=88 bid=8830 lr=3539 />");
//					ret.add("</retrieveActivePurchasesResp>");
					
//					ret.add("<PurchaseItem cnpt=0 sspID=fitz_1_forfait_1_vf run=1 bid=7296 lr=3374 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=fitz_1_03_vf run=26 bid=7295 lr=3374 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=fitz_1_02_vf run=26 bid=7294 lr=3374 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=bidon_karaoke_vf run=1 bid=7287 lr=85715 />");
//					ret.add("<PurchaseItem cnpt=18 sspID=fitz_1_01_vf run=26 bid=7293 lr=86173 />");
//					ret.add("<PurchaseItem cnpt=17 sspID=lance_compte_tva_7_01_vf run=43 bid=7301 lr=86338 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=dix_neuf_deux_1_03_vf run=43 bid=7249 lr=459858 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=dix_neuf_deux_1_02_vf run=44 bid=7248 lr=459858 />");
//					ret.add("<PurchaseItem cnpt=2631 sspID=dix_neuf_deux_1_01_vf run=44 bid=7247 lr=459858 />");
//					ret.add("<PurchaseItem cnpt=0 sspID=19_2_forfait_01_vf run=1 bid=7250 lr=535745 />");
					
//					ret.add("<retrieveActivePurchases ecode=\"0\">");
//					long exp = App.timeOfInit + 1 * 40 * 1000;
//					long t = (exp - System.currentTimeMillis()) / 1000;
//					if (t > 0) {
//					ret.add("<PurchaseItem sspID=\"dexter_3_hd_forfait_3_vf\" bid=\"36770\" lr="+t+" />");
//					ret.add("<PurchaseItem sspID=\"fitz_1_01_hd_vf\" bid=\"36771\" lr=700 />");
//					ret.add("<PurchaseItem sspID=\"fitz_1_02_hd_vf\" bid=\"36772\" lr=600 />");
//					ret.add("<PurchaseItem sspID=\"fitz_1_03_hd_vf\" bid=\"36773\" lr=500 />");
//					}
//					ret.add("<PurchaseItem sspID=\"dexter_3_01_hd_vf\" bid=\"36761\" lr=\"900000\" />");
//					ret.add("<PurchaseItem sspID=\"dexter_3_02_hd_vf\" bid=\"36762\" lr=\"900000\" />");
//					ret.add("<PurchaseItem sspID=\"dexter_3_03_hd_vf\" bid=\"36763\" lr=\"900000\" />");
//					ret.add("<PurchaseItem sspID=\"dexter_3_hd_forfait_1_vf\" bid=\"36760\" lr=\"900000\" />");
//					ret.add("<PurchaseItem sspID=\"dexter_3_05_hd_vf\" bid=\"36769\" lr=\"790000\" />");
//					ret.add("<PurchaseItem sspID=\"dexter_3_06_hd_vf\" bid=\"36783\" lr=\"800000\" />");
//					ret.add("<PurchaseItem sspID=\"dexter_3_04_hd_vf\" bid=\"36753\" lr=\"1100000\" />");
//					ret.add("</retrieveActivePurchases>");
//					System.out.println("leeksnet " + ret);	
					
				}

				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							if (lastEcode == LOST_CONNECTION) {
								break;
							}
							if (lastEcode != NO_ERROR) {
								if (Log.WARNING_ON) {
									Log.printWarning("retrieveActivePurchases(), ecode = "
													+ lastEcode);
								}
								activePurchaseCache = new Bookmark[0];
								activePurchaseCacheWithBundle = activePurchaseCache;
								activePurchaseCacheAdult = new Bookmark[0];
								activePurchaseCacheWithBundleAdult = activePurchaseCacheAdult;
								checkErrorCode("04");
								return activePurchaseCache;
							}
						} else if ("<PurchaseItem".equals(token)) {
							if (bookmark != null) {
								bookmarks.add(bookmark);
							}
							bookmark = new Bookmark();
						} else if ("cnpt".equals(token)) {
							if (bookmark != null) {
								bookmark.setCurrentNPT(Integer.parseInt(tok
										.nextToken()));
							}
						} else if ("sspID".equals(token)) {
							if (bookmark != null) {
								bookmark.setOrderableName(tok.nextToken());
							}
						} else if ("bid".equals(token)) {
							if (bookmark != null) {
								bookmark.setBookmarkID(tok.nextToken());
							}
						} else if ("lr".equals(token)) {
							if (bookmark != null) {
								bookmark.setLeaseRemaing(Integer.parseInt(tok
										.nextToken()));
								bookmark.setLeaseEnd(cur
										+ bookmark.getLeaseRemaing()
										* Constants.MS_PER_SECOND);
								if (remain > bookmark.getLeaseRemaing()) {
									remain = bookmark.getLeaseRemaing();
								}
							}
						}
					}
				}
				if (bookmark != null) {
					bookmarks.add(bookmark);
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
			if (timer != null) {
				timer.removeTVTimerWentOffListener(this);
				TVTimer.getTimer().deschedule(timer);
			}
			long timerMS = (remain + 2) * Constants.MS_PER_SECOND;
			Log.printDebug("retrieveActivePurchases(), setup timer after " + timerMS + " msec");
			timer = new TVTimerSpec();
			timer.addTVTimerWentOffListener(this);
			timer.setDelayTime(timerMS);
			timer = TVTimer.getTimer().scheduleTimerSpec(timer);
		} catch (Exception e) {
			Log.print(e);
		}

		Bookmark[] ret = new Bookmark[bookmarks.size()];
		bookmarks.toArray(ret);
		activePurchaseCache = ret;
		activePurchaseCacheWithBundle = activePurchaseCache;
		
		activePurchaseCacheAdult = null;
		activePurchaseCacheWithBundleAdult = null;
		if (Log.DEBUG_ON) {
			Log.printDebug("retrieveActivePurchases(), ret = "
					+ ret.length + ", bookmarks = " + bookmarks);
		}
		checkErrorCode("04");
		return ret;
	}

	synchronized public boolean setNPT(String bookmarkId, int npt) {
		title = bookmarkId;
		String request = makeRequest("setNPT") + "&bookmarkId=" + bookmarkId
				+ "&currentNPT=" + ((npt == 1 ? 1000 : npt) / 1000); // mili-problem
		Log.printInfo("BookmarkManager, setNPT()");
		if (Log.DEBUG_ON) {
			Log.printDebug("setNPT(), request = " + request);
		}
		if (bookmarkId == null) {
			return false;
		}
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Vector ret = commWithCS(request, "05");
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							if (lastEcode == LOST_CONNECTION) {
								break;
							}
							if (Log.DEBUG_ON) {
								Log.printDebug("setNPT(), ecode = "
										+ lastEcode);
							}
							checkErrorCode("05");
							return lastEcode == NO_ERROR;
						}
					}
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
		} catch (Exception e) {
			Log.print(e);
		}
		checkErrorCode("05");
		return false;
	}

	synchronized public int getNPT(String sspId) {
		String request = makeRequest("getNPT") + "&sspId=" + sspId;
		Log.printInfo("BookmarkManager, getNPT()");
		if (Log.DEBUG_ON) {
			Log.printDebug("getNPT(), request = " + request);
		}
		boolean appear = false;
		int npt = 0;
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Vector ret = commWithCS(request, "06");
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							if (lastEcode == LOST_CONNECTION) {
								break;
							}
							if (lastEcode != NO_ERROR) {
								checkErrorCode("06");
								return -lastEcode;
							}
						} else if ("cnpt".equals(token)) {
							appear = true;
							npt = Integer.parseInt(tok.nextToken()) * 1000; // mili-problem
							break;
						}
					}
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
		} catch (Exception e) {
			Log.print(e);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("getNPT(), npt = " + npt);
		}
		if (checkErrorCode("06") == false && appear == false) {
			CommunicationManager.getInstance().errorMessage("VOD303", null);
		}

		return npt;
	}

	public int validateSTB() {
		String request = makeRequest("validateSettop");
		Log.printInfo("BookmarkManager, validateSTB()");
		if (Log.DEBUG_ON) {
			Log.printDebug("validateSTB(), request = " + request);
		}
		try {
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			lastEcode = NOT_COMPLETE;
			while (chance > 0 && (lastEcode == NOT_COMPLETE || lastEcode == LOST_CONNECTION)) {
				Vector ret = commWithCS(request, "");
				for (int i = 1; i < ret.size(); i++) {
					StringTokenizer tok = new StringTokenizer(
							(String) ret.get(i), " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							lastEcode = Integer.parseInt(tok.nextToken());
							break;
						}
					}
				}
				chance--;
				if (lastEcode == LOST_CONNECTION) {
					Log.printWarning("receive 1139, try again, remain chance = "
									+ chance);
				}
			}
		} catch (Exception e) {
			Log.print(e);
			lastEcode = NOT_COMPLETE;
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("validateSTB(), ecode = " + lastEcode);
		}
		return lastEcode;
	}

	public void setServerConfig(BookmarkServerConfig config) {
		serverConfig = config;
	}

	public void timerWentOff(TVTimerWentOffEvent arg0) {
		if (Log.DEBUG_ON) {
			Log.printDebug("timerWentOff(), update purchased list");
		}
		if (timer != null) {
			timer.removeTVTimerWentOffListener(this);
			TVTimer.getTimer().deschedule(timer);
		}

		// VDTRMASTER-6062
		if (Resources.appStatus != VODService.APP_PAUSED) {
            new Thread("timerWentOff(), update purchased list") {
                public void run() {
                    retrieveActivePurchases(false);
                    organize();
                    BaseUI cur = (BaseUI) MenuController.getInstance().getCurrentScene();
                    if (cur != null) {
                        if (cur instanceof ListViewUI && ((ListViewUI) cur).isResumeViewing()) {
                            int len = ((ListViewUI) cur).setData(null);
                            if (len == 0) {
                                MenuController.getInstance().back();
                            }
                        } else {
                            cur.updateButtons();
                            cur.repaint();
                            if (cur instanceof CategoryUI) {
                                if (((CategoryUI) cur).isFocusOnResumeViewing()) {
                                    ((CategoryUI) cur).refresh();
                                }
                            }
                        }
                    }
                }
            }.start();
        }
	}
}

/**
 * Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * @author LSC
 * @version 1.0 ClientSessionReleaseRequest. This class handles the message that is sent a Client to the Network to
 *          request that a session be torn-down.
 */
public class ClientSessionReleaseRequest extends SessionMessage implements ClientToNetworkMessage {

    /**
     * Contains the response code.
     */
    private short response;

    /**
     * Contains the private data.
     */
    private byte[] privateData;

    /**
     * ClientSessionReleaseRequest Default Constructor.
     */
    public ClientSessionReleaseRequest() {
        super();
        setMessageID(SessionMessage.CLIENT_SESSION_RELEASE_REQUEST);
    }

    /**
     * Sets the user defined response for why the session is to be torn down. Ususally just 0 (Ok). Called
     * byDSMCCMessenger sessionRelease and timerWentOff.
     *
     * @param rsp
     *            response
     */
    public void setResponse(short rsp) {
        this.response = rsp;
    }

    /**
     * Sets the private data (UserData) field of the message. Called by DSMCCMessenger sessionRelease.
     *
     * @param pd
     *            privatedata
     */
    public void setPrivateData(byte[] pd) {
        this.privateData = pd;
    }

    /**
     *Serialies the message. Called by DSMCCMessenger sessionRelease.
     *
     * @return A byte array with the message to be sent.
     */
    public byte[] toByteArray() {
        byte[] rb = null;

        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(bo);

        try {
            // Set the length of this message + padding bytes to make a
            // multiple
            // of 4;
            // setMessageLength(size() + 2);
            setMessageLength(16);

            byte[] ba = super.toByteArray();
            out.write(ba);
            out.writeShort(response);

            // Write the UU Data length.
            byte[] temp = new byte[2];
            out.write(temp);

            // Write the private data length padded out to a multiple of 4
            // bytes.
            if (privateData != null) {
                short length = (short) (privateData.length + 2);

                temp[1] = (byte) (length & (short) 0x00FF);
                temp[0] = (byte) ((length & (short) 0xFF00) >> 8);
                out.write(temp);

                out.write(privateData);
            } else {
                out.write(temp);
            }
            rb = bo.toByteArray();
            out.close();
            out = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (out != null) {
                try {
                    out.close();
                    out = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }
        return rb;
    }

}

package com.videotron.tvi.illico.vod.controller.stream.event;

import com.videotron.tvi.illico.vod.controller.stream.IVodStreamControl;

/**
 * The completion event for the queryStatus asynchronous stream request.
 * 
 * @see VodStreamControlEvent
 * @see VodStreamPlayEvent
 * @see VodStreamPauseEvent
 * @see VodStreamFastForwardEvent
 * @see VodStreamRewindEvent
 * @see VodStreamStatusEvent
 * @see VodStreamEndOfStreamEvent
 * @see VodStreamResetEvent
 * @see IVodStreamControl
 * @see IVodStreamControlListener
 */
public class VodStreamStatusEvent extends VodStreamControlEvent {

    private static final long serialVersionUID = -5512796941487580811L;

    /**
     * @param objSrc
     *            the source of the event.
     * @param act
     *            Asynchronous completion token.
     * @param streamHandle
     *            handle of the stream as seen by the media server.
     * @param streamState
     *            The status of the stream.
     * @param statusCode
     *            the outcome code of this operation.
     * @param lastNpt
     *            the npt of the stream reported by the media server at the time the operation was processed.
     * @param scaleNum
     *            the numerator portion of the stream's current rate.
     * @param scaleDenom
     *            the denominator portion of the stream's current rate
     * 
     * @see VodStreamControlEvent
     * @see VodStreamPlayEvent
     * @see VodStreamPauseEvent
     * @see VodStreamFastForwardEvent
     * @see VodStreamRewindEvent
     * @see VodStreamStatusEvent
     * @see VodStreamEndOfStreamEvent
     * @see VodStreamResetEvent
     * @see IVodStreamControl
     * @see IVodStreamControlListener
     */
    public VodStreamStatusEvent(IVodStreamControl objSrc, Object act, int streamHandle, int streamState,
            int statusCode, int lastNpt, short scaleNum, short scaleDenom) {
        super(objSrc, act, streamHandle, streamState, statusCode, lastNpt, scaleNum, scaleDenom);
        // TODO Auto-generated constructor stub
    }
}

package com.videotron.tvi.illico.vod.controller.stream;

/**
 * Generated if the service gateway parameter is absent.
 * 
 */
public class VodNoServGatewayException extends Exception {

    private static final long serialVersionUID = -7787979427288900047L;

    /**
     * 
     */
    public VodNoServGatewayException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     *            The detailed reason
     */
    public VodNoServGatewayException(String arg0) {
        super(arg0);
    }
}

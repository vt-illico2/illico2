package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.IOException;
import java.io.DataInputStream;

import com.videotron.tvi.illico.log.Log;

/**
 * This class represents a session transport stream resource.
 *
 * @see MPEGProgramResource
 * @see ATSCModulationModeResource
 * @see IPResource
 * @see SessionResource
 * @see PhysicalChannelResource
 * @see TSIDResource
 */
public class TSIDResource extends SessionResource {
    private short bwResourceType;

    private int downstream_bw; // /**< Defines the amount of bandwidth reserved
    // for the session.*/

    private short tsidResourceType;

    private int downstream_tsid;

    /**
     * @param rba
     *            The resource data.
     */
    public TSIDResource(DataInputStream rba, SessionResource sr) {
        this.copy(sr);

        int messageLength = this.getFieldLength();

        try {
            bwResourceType = rba.readShort();
            downstream_bw = rba.readInt();
            tsidResourceType = rba.readShort();
            downstream_tsid = rba.readInt();

            // Peel off the leftovers in case I missed something.

            messageLength -= 12;
            if (messageLength > 0 && messageLength < rba.available()) {
                rba.skipBytes(messageLength);
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("downstream_bw : " + downstream_bw);
                Log.printDebug("downstream_tsid : " + downstream_tsid);
            }
        } catch (IOException ex) {
            Log.print(ex);
        }

    }

    /**
     * Responds with the transport stream Id.
     * @return The tranpsort stream identifier.
     */
    public int getTransportStreamId() {
        return downstream_tsid;
    }

    /**
     * Responds with the transport stream bandwidth.
     * @return The transport stream bandwidth.
     */
    public int getTSBandwidth() {
        return downstream_bw;
    }
}

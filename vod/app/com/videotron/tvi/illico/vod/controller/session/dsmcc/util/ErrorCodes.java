package com.videotron.tvi.illico.vod.controller.session.dsmcc.util;

/**
 *Codes.
 */
public class ErrorCodes {
    /** The version of the Codes file. */
    public static final int VERSION = 1;

    /** The error code translations. */
    public static final int SUCCESS = 0;
    public static final int FAILURE = -1;
    public static final int ALREADY_IN_PROGRESS = 2;
    public static final int NETWORK_CONFIGURATION_FAILURE = 3;
    public static final int BAD_DNCS_IP = 4;
    public static final int TUNE_FAILURE = 5;
}

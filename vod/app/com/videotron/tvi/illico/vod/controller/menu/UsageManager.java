package com.videotron.tvi.illico.vod.controller.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.log.Log;

public class UsageManager implements Runnable {

	private UsageManager() {
	}

	private static UsageManager instance = new UsageManager();

	public static UsageManager getInstance() {
		return instance;
	}

	private ArrayList usageList = new ArrayList();
	private StringBuffer buf = new StringBuffer(200);

	public void addUsageLog(String pageId, String pageAttr, String funcId,
			String funcAttr) {
		// VDTRMASTER-5320
		return;
		
//		if ("1101".equals(pageId) == false/* && "1111".equals(pageId) == false*/) {
//			return;
//		}
//		buf.setLength(0);
//		buf.append("<event pageID=\"").append(pageId).append("\"");
//		buf.append(" pageAttrID=\"").append(pageAttr).append("\"");
//		buf.append(" funcID=\"").append(funcId).append("\"");
//		buf.append(" funcAttrID=\"").append(funcAttr).append("\"/>");
//		usageList.add(buf.toString());
//		if (usageList.size() > 20) {
//			upload();
//		}
	}

	public void upload() {
		// VDTRMASTER-5320
		return;
//		new Thread(this, "UsageManager, upload").start();
	}

	public void run() {
		synchronized (usageList) {
			if (usageList.size() == 0)
				return;
			StringBuffer large = new StringBuffer(20 * 100);
			String macAddr = BookmarkManager.getInstance().macAddr();
			large.append("report=<usagereport mac=\"").append(macAddr).append(
					"\">");
			for (int i = 0; i < usageList.size(); i++) {
				large.append(usageList.get(i));
			}
			large.append("</usagereport>");

			if (Log.DEBUG_ON) {
				Log.printDebug("usage, " + large.toString());
			}

			String url = BookmarkManager.getInstance().makeRequest("usagereport");
			try {
				String[] ret = URLRequestor.getLinesWithPost(url, large
						.toString());
				int ecode = -1;
				for (int i = 1; i < ret.length; i++) {
					StringTokenizer tok = new StringTokenizer(ret[i], " =\">");
					while (tok.hasMoreTokens()) {
						String token = tok.nextToken();
						if ("ecode".equals(token)) {
							ecode = Integer.parseInt(tok.nextToken());
							if (Log.DEBUG_ON) {
								Log.printDebug("usagereport(), ecode = " + ecode);
							}
							break;
						}
					}
				}
//				if (ecode == 0) {
					usageList.clear();
//				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
}

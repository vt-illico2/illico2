package com.videotron.tvi.illico.vod.controller.stream.event;

import com.videotron.tvi.illico.vod.controller.stream.IVodHandler;
import com.videotron.tvi.illico.vod.controller.stream.VodHandlerEvent;

/**
 * <code>VodHandlerConnectFailedEvent</code> event is generated when a connect operation fails.
 */
public class VodHdlrConnFailedEvent extends VodHandlerEvent {

    private static final long serialVersionUID = 2847606027557002636L;

    /**
     * the connect request timed out.
     */
    public static final int TIMEDOUT_RESPONSE_CODE = 100;

    /**
     * Constructor for event.
     * 
     * @param objSrc
     *            Source of the event.
     * @param act
     *            The asynchronous completion token.
     * @param responseCode
     */
    public VodHdlrConnFailedEvent(IVodHandler objSrc, Object act, int responseCode) {
        super(objSrc, act);
        m_responseCode = responseCode;
    }

    /**
     * Reports the response code. The response code should used only if the status is
     * STATUS_RESPONSE_AVAILABLE. Otherwise, it should be ignored.
     */
    public int getResponseCode() {
        return m_responseCode;
    }

    /**
     * Response code. This will have a valid value only if the status code is STATUS_RESPONSE_AVAILABLE
     */
    private int m_responseCode;
}

package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.IOException;
import java.io.DataInputStream;

import com.videotron.tvi.illico.log.Log;

/**
 * Used to specify many of the tuning parameters required by the set top. This is a custom type (0xF001) and is
 * described in sspVersion V2.3 PP 17 - 19
 *
 * @see MPEGProgramResource
 * @see ATSCModulationModeResource
 * @see IPResource
 * @see SessionResource
 * @see PhysicalChannelResource
 * @see TSIDResource
 */
public class ATSCModulationModeResource extends SessionResource {

    // _QAMModulationFormat
    private static short atscQamUnknown = 0x00; // Unknown Modulation Mode

    private static short atscQam16 = 0x06; //

    private static short atscQam32 = 0x07; //

    private static short atscQAM_64 = 0x08; //

    private static short atscQam128 = 0x0C; //

    private static short atscQam256 = 0x10; //

    // QAMModulationMode
    private static short qamModNone = 0x00;

    /**  No modulation mode. */
    private static short qamMod64 = 0x19;

    /** transmission system */
    private byte transmissionSystem;

    /** defines the type of system used to deliver the service. */
    private byte innerCodingMode;

    /** a fixed value with a value of 0x0F. */
    private byte splitBitStreamMode;

    /** a fixed value with a value of 0x00. */
    private byte modulationFormat;

    /** modulation mode for this channel. */
    private int symbolRate;

    /** a fixed value and shall be set to 0x004C4B40. */
    private byte reserved;

    private byte interleaveDepth;

    /**
     * interleaving used for this channel. This field shall have a value in the range from 0x00-0x0F.
     */
    private byte modulationMode;

    /** modulation mode; a value of No modulation mode shall be used. */
    private byte forwardErrorCorrection; // **value for FEC Transmission, system shall be used.

    /**
     * Creates the ATSCModulationModeResource resource.
     *
     * @param rba
     *            The underlying raw resource data.
     */
    public ATSCModulationModeResource(DataInputStream rba, SessionResource sr) {
        this.copy(sr);

        try {
            // m_dis.skipBytes(2);
            transmissionSystem = rba.readByte();

            // m_dis.skipBytes(2);
            innerCodingMode = rba.readByte();

            // m_dis.skipBytes(2);
            splitBitStreamMode = rba.readByte();

            // m_dis.skipBytes(2);
            modulationFormat = rba.readByte();

            // m_dis.skipBytes(2);
            symbolRate = rba.readInt();

            reserved = rba.readByte();

            interleaveDepth = rba.readByte();

            modulationMode = rba.readByte();

            forwardErrorCorrection = rba.readByte();

            if (Log.DEBUG_ON) {
                StringBuffer buff = new StringBuffer();
                buff.append("ATSCModulationModeResource.transmissionSystem = ");
                buff.append(Integer.toHexString(transmissionSystem & 0xFF));
                buff.append("\nATSCModulationModeResource.inner_coding_mode = ");
                buff.append(Integer.toHexString(innerCodingMode & 0xFF));
                buff.append("\nATSCModulationModeResource.split_bit_stream_mode = ");
                buff.append(Integer.toHexString(splitBitStreamMode & 0xFF));
                buff.append("\nATSCModulationModeResource.modulation_format = ");
                buff.append(Integer.toHexString(modulationFormat & 0xFF));
                buff.append("\nATSCModulationModeResource.symbol_rate = ");
                buff.append(Integer.toHexString(symbolRate & 0xFFFFFFFF));
                buff.append("\nATSCModulationModeResource.resered = ");
                buff.append(Integer.toHexString(reserved & 0xff));
                buff.append("\nATSCModulationModeResource.interleave_depth = ");
                buff.append(Integer.toHexString(interleaveDepth & 0xff));
                buff.append("\nATSCModulationModeResource.modulation_mode = ");
                buff.append(Integer.toHexString(modulationMode & 0xff));
                buff.append("\nATSCModulationModeResource.forward_error_correction = ");
                buff.append(Integer.toHexString(forwardErrorCorrection & 0xff));

                Log.printDebug(buff.toString());
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Responds with the transmission system.
     *
     * @return The transmission system.
     */
    public int getTransmissionSystem() {
        return transmissionSystem;
    }

    /**
     * Responds with the inner coding mode.
     *
     * @return The inner coding mode.
     */
    public int getInnerCodingMode() {
        return innerCodingMode;
    }

    /**
     * Responds with the split bitstream mode.
     *
     *
     * @return The split bitstream mode.
     */
    public int getSplitBitStreamMode() {
        return splitBitStreamMode;
    }

    /**
     * Responds with the modulation format.
     *
     * @return The modulation format.
     */
    public int getModulationFormat() {
        return modulationFormat;
    }

    /**
     * Responds with the symbol rate.
     *
     * @return The symbol rate.
     */
    public int getSymbolRate() {
        return symbolRate;
    }

    /**
     * Responds with the the interleave depth.
     *
     * @return The interleave depth.
     */
    public int getInterleaveDepth() {
        return interleaveDepth;
    }

    /**
     * Responds with the modulation mode.
     *
     * @return The modulation mode.
     */
    public int getModulationMode() {
        return modulationMode;
    }

    /**
     * Responds with the error correction.
     *
     * @return The error correction value.
     */
    public int getForwardErrorCorrection() {
        return forwardErrorCorrection;
    }
}

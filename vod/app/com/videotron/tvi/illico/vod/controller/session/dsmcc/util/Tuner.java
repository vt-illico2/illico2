package com.videotron.tvi.illico.vod.controller.session.dsmcc.util;

import java.util.Hashtable;

import org.davic.mpeg.TransportStream;
import org.davic.net.tuning.NetworkInterface;
import org.davic.net.tuning.NetworkInterfaceController;
import org.davic.net.tuning.NetworkInterfaceEvent;
import org.davic.net.tuning.NetworkInterfaceListener;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterfaceReleasedEvent;
import org.davic.net.tuning.NetworkInterfaceTuningEvent;
import org.davic.net.tuning.NetworkInterfaceTuningOverEvent;
import org.davic.net.tuning.NoFreeInterfaceException;
import org.davic.resources.ResourceClient;
import org.davic.resources.ResourceProxy;
import org.davic.resources.ResourceStatusEvent;
import org.davic.resources.ResourceStatusListener;
import org.ocap.net.OcapLocator;

import com.videotron.tvi.illico.log.Log;

/**
 * Tuner.
 */
public class Tuner implements NetworkInterfaceListener, ResourceClient, ResourceStatusListener {

    private static int tuningTimeOut = 3000; // Milliseconds to wait for a

    // tune before announcing a fail

    /** The place to obtain interfaces. */
    private NetworkInterfaceManager netManager;

    /** A Controller object for the NetworkInterface being used. */
    private NetworkInterfaceController netController;

    /** The NetworkInterface to be used. */
    private NetworkInterface netInterface;

    /** The object used for tuner event synchronization. */
    private Object tuningEvent = new Object();

    /** The Locator to attempt to tune to. */
    private OcapLocator ocLocator;

    /** Set to true if the Tuner is tuned. */
    private boolean isTuned = false;

    /** Set to true if the tuner has been setup. */
    private boolean isInitialized = false;

    /** Set to true when a network interface tune over event is received. */
    private boolean receivedNIEvent = false;

    /** The status of the tuner. */
    private int tuneStatus = NetworkInterfaceTuningOverEvent.FAILED;

    private Object statusChangedEvent = new Object();

    private boolean receivedStatusChangedEvent;

    /**
     * A cache of TSIDs from already tuned frequencies. Note this will include entries for successful tunes and the
     * associated TSID as well as failed tunes where the STB was unable to tune to the frequency
     */
    private Hashtable tsidCache = new Hashtable(20);

    /**
     * The number of milliseconds to wait between tests for the acquisition of the PAT from the SI.
     */
    public static int SI_EVENT_TIMEOUT_MS = 500;

    /** Maximum number of times to retry getting the TSID. */

    public static int MAX_TSID_RETRY = 6;

    /**
     *
     */
    public Tuner() {
        int good = initNetwork();
        if (good == ErrorCodes.SUCCESS) {
            isInitialized = true;
        }
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    /**
     * Allocate the tuner from the network controller.
     *
     * @return -1 if fail else 0.
     */
    private int initNetwork() {
        int rc = ErrorCodes.SUCCESS;

        netManager = NetworkInterfaceManager.getInstance();

        if (netManager == null) {
            return ErrorCodes.FAILURE;
        }

        netController = new NetworkInterfaceController(this);

        return rc;
    }

    /**
     * Receives an event from the NetworkInterface indicating that tuning is complete.
     */
    public void receiveNIEvent(NetworkInterfaceEvent ev) {

        if (ev instanceof NetworkInterfaceTuningEvent) {
            if (Log.DEBUG_ON) {
                NetworkInterfaceTuningEvent nite = (NetworkInterfaceTuningEvent) ev;
                Log.printDebug("Tuner.receiveNIEvent - Event=" + nite.toString());
            }
        }

        if (ev instanceof NetworkInterfaceTuningOverEvent) {

            NetworkInterfaceTuningOverEvent nitoe = (NetworkInterfaceTuningOverEvent) ev;
            tuneStatus = nitoe.getStatus();
            if (Log.DEBUG_ON) {
                Log.printDebug("Tuner.receiveNIEvent - Tuning Over Event status = " + tuneStatus);
            }
            receivedNIEvent = true;

            synchronized (tuningEvent) {
                tuningEvent.notify();
            }
        }

    }

    // ResourceClient requirements
    public boolean requestRelease(ResourceProxy px, Object data) {
        // ServiceGroupDiscovery.getInstance().stopSGD();
        if (Log.DEBUG_ON) {
            Log.printDebug("requestRelease : " + data + " isTuned : " + isTuned);
        }
        boolean rc = true;
        if (isTuned) {
            deTune();
        }
        return rc;
    }

    public void notifyRelease(ResourceProxy px) {
        if (Log.DEBUG_ON) {
            Log.printDebug("notifyRelease : " + px);
        }
        // ServiceGroupDiscovery.getInstance().stopSGD();
        // Asked to release so we comply.
        if (isTuned) {
            deTune();
        }
    }

    public void release(ResourceProxy px) {
        if (Log.DEBUG_ON) {
            Log.printDebug("release : " + px);
        }
        // ServiceGroupDiscovery.getInstance().stopSGD();
        // Also asked to release so we comply.
        if (isTuned) {
            deTune();
        }
    }

    /*  ********************** Tune the Tuner ************************ */

    /**
     * tune ==== Uses the network controller to tune to a locator. Though tuning is asynchronous, we make it synchronous
     * by waiting here until it completes.
     *
     * @return -1 if fail, 0 if success.
     */
    public int tune(OcapLocator oCLocator) {
        ocLocator = oCLocator;
        receivedStatusChangedEvent = false;

        if (Log.DEBUG_ON) {
            StringBuffer sb = new StringBuffer("Tuner.tune - Tuning to: Freq - ");
            sb.append(ocLocator.getFrequency());
            sb.append(" Prog# - ");
            sb.append(ocLocator.getProgramNumber());
            sb.append(" Mod Format - ");
            // sb.append(ocLocator.getModulationFormat()); //
            Log.printDebug(sb.toString());
        }

        // Fail if the network failed to initialize.

        if (!isInitialized()) {
            return ErrorCodes.FAILURE;
        }
        /*
         * register as a lister to be notified of network interfaces being reserved / released
         */
        NetworkInterfaceManager.getInstance().addResourceStatusEventListener(this);

        // Request the asynchronous tune.
        int tryCount = 0;
        while (tryCount < 2) {
            if (Log.DEBUG_ON) {
                Log.printDebug("Tuner.tune - See if Net interface available. Attempt (" + tryCount + ")");
            }
            if (true) {
                try {
                    if (isTuned()) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("Tuner.tune - Already tuned do not need to attempt to reserve again");
                        }
                        break; // out of the while loop
                    }
                    netController.reserveFor(oCLocator, null);
                    netInterface = netController.getNetworkInterface();
                    netInterface.addNetworkInterfaceListener(this);

                    if (Log.DEBUG_ON){
                        Log.printDebug("Tuner.tune - Tune the tuner");
                    }
                    // Ensure we start with a false indication of a
                    // receivee Network Interface Event;
                    receivedNIEvent = false;
                    netController.tune(oCLocator);
                    isTuned = true;
                    break; /* out of the while loop */
                } catch (Exception ex) {
                    if (ex instanceof NoFreeInterfaceException) {
                        // fall through and wait for a NetworkInterface to
                        // become available
                        Log.printDebug("Tuner.tune - No Free Interface, wait for a free one.");
                    } else {
                        Log.print(ex);
                        ex.printStackTrace();
                        return ErrorCodes.TUNE_FAILURE;
                    }
                }
            }
            /* else, no tuner free */
            /* Wait for a tuner to become free */
            synchronized (statusChangedEvent) {
                try {
                    if (!receivedStatusChangedEvent) {
                        statusChangedEvent.wait(tuningTimeOut);
                    }

                    if (Log.DEBUG_ON) {
                        /* Timed out. */
                        if (!receivedStatusChangedEvent) {
                            Log.printDebug("Tuner.tune - Timedout waiting for network interface to become available");
                        } else {
                            Log.printDebug("Tuner.tune - Received the proper event");
                        }
                    }
                } catch (Exception ex) {
                    Log.printDebug("Tuner.tune - Waiting for tuner exception: " + ex.getMessage());
                    ex.printStackTrace();
                    /* keep trying */
                }
            }
            tryCount++;
        } // end of while loop

        /* Check to see if the interface was gotten. */
        if (netInterface == null) {
            Log.printDebug("Tuner.tune - Failed to attain a Network Interface. Tune failed.");
            return ErrorCodes.FAILURE;
        }
        synchronized (tuningEvent) {
            try {
                if (Log.DEBUG_ON){
                    Log.printDebug("Tuner.tune - Wait for tuning event");
                }
                // receivedNIEvent is used to signal whether the tune() has
                // completed or not.
                if (!receivedNIEvent) {
                    tuningEvent.wait(tuningTimeOut);
                }
                // At this point check again whether the tune completed or not.
                if (!receivedNIEvent) {
                    Log.printDebug("Tuner.tune - Didn't receive a tuning event");

                    // if the frequency doesn't exist add a cache entry with -1
                    // so we don't try and tune to it over and over again.
                    tsidCache.put(new Integer(ocLocator.getFrequency()), new Integer(-1));
                    return ErrorCodes.FAILURE;
                }

                if (tuneStatus != NetworkInterfaceTuningOverEvent.SUCCEEDED) {
                    Log.printDebug("Tuner.tune - Failed: status = " + tuneStatus);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("Tuner.tune - adding cache entry for freq = "
                                + ocLocator.getFrequency() + ", tsid = 0xFFFF");
                    }
                    tsidCache.put(new Integer(ocLocator.getFrequency()), new Integer(0xFFFF));
                    return ErrorCodes.FAILURE;
                }
            } catch (Exception ex) {
                // See if we simply missed the event.
                if (!receivedNIEvent) {
                    Log.printDebug("Tuner.tune - Exception and no event");
                    ex.printStackTrace();
                    return ErrorCodes.FAILURE;
                }
                receivedNIEvent = false;
            }
        }
        isTuned = true;
        return ErrorCodes.SUCCESS;
    }

    /**
     * tuneCleanup
     *
     * Unreserves an interface and releases the controller after a tune failure. This should only be called from
     * deTune() because we don't want to release our listeners so we can receive resource contention events even while
     * we are just processing entries in the servicegroupmap file. Even if we have a tune failure in the tune() code we
     * don't want to clean up right away.
     */
    private void tuneCleanup() {
        isTuned = false;
        if (Log.DEBUG_ON) {
            Log.printDebug("Tuner.cleanup - Checking reserved before releasing network interface");
        }
        NetworkInterface ni = null;
        if (netController != null) {
            ni = netController.getNetworkInterface();
        }
        if (ni != null && ni.isReserved()) {
            try {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Tuner.cleanup- Releasing network interface");
                }
                netController.release();
            } catch (Throwable t) {
                Log.printDebug("Tuner.cleanup - Got error when releasing");
            }
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("Tuner.cleanup - Removing network listener");
        }
        if (netInterface != null) {
            netInterface.removeNetworkInterfaceListener(this);
            netInterface = null;
        }

    }

    /**
     *
     * Release the network Tuner resource back to the system. This process is asynchronous.
     *
     */
    public void deTune() {
        // Release the network interface that we have reserved.
        // This can throw an exception, so we have to enclose it in a 'try'
        // block.

        NetworkInterfaceManager.getInstance().removeResourceStatusEventListener(this);

        try {
            tuneCleanup();
        } catch (Exception e) {
            // Normally, we don't care because we are just cleaning up.
            if (Log.DEBUG_ON) {
                e.printStackTrace();
            }
        }

        // Tell everything that tuning has finished, even though it didn't. This
        // avoids any chance of deadlocks.
        synchronized (tuningEvent) {
            tuningEvent.notify();
        }

        isTuned = false;
    }

    /**
     * Returns the OcapLocator that we are currently tuned to.
     *
     * @return The currently tuned OcapLocator.
     */
    public OcapLocator getOCAPLocator() {
        return ocLocator;
    }

    /**
     * A way to determine if tuning was successful or not.
     *
     * @return true = is tuned, else not tuned.
     */
    public boolean isTuned() {
        return isTuned;
    }

    /**
     * Retrieves the MPEG Transport Stream ID (TSID) from the currently tuned MPEG stream.
     *
     * @return An integer value representing the current TSID.
     */
    public int getTSID() {
        TransportStream ts = null;
        int tsid = 0xFFFF;
        int attempt = 0;

        do {
            try {
                // try to get the currently tuned transport-stream from the
                // interface
                ts = netController.getNetworkInterface().getCurrentTransportStream();
            } catch (Exception e) {
                // fall through because we most likely had the tuner taken
            }

            if (ts != null) {
                // get the transport-stream id
                tsid = ts.getTransportStreamId();
            } else {
                Log.printDebug("Tuner.getTSID() - TRANSPORT STREAM IS NULL");
            }
            if (tsid == 0xFFFF || tsid == -1) {
                // Delay for SI data to become available.
                try {
                    Thread.sleep(SI_EVENT_TIMEOUT_MS);
                } catch (Exception ex) {
                }
            }

        } while (((tsid == 0xFFFF) || (tsid == -1)) && (++attempt < MAX_TSID_RETRY));

        if (Log.DEBUG_ON) {
            Log.printDebug("Tuner.getTSID: adding cache entry for freq,tsid = " + ocLocator.getFrequency()
                    + "," + tsid);
        }
        tsidCache.put(new Integer(ocLocator.getFrequency()), new Integer(tsid));
        return tsid;
    }

    /**
     * findTSID.
     *
     */
    public Integer findCachedTSID(int frequency) {
        Integer freq = new Integer(frequency);
        Integer tsid = (Integer) tsidCache.get(freq);
        return tsid;
    }

    /**
     * clearTSIDcache.
     */
    public void clearTSIDcache() {
        tsidCache.clear();
    }

    /**
     * Make sure that we have released the controller.
     *
     */
    public void destroy() {
        if (netInterface != null) {
            netInterface.removeNetworkInterfaceListener(this);
        }

        try {
            if (netController != null) {
                NetworkInterface ni = netController.getNetworkInterface();

                if (ni != null && ni.isReserved()) {
                    netController.release();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void statusChanged(ResourceStatusEvent rse) {
        if (Log.DEBUG_ON) {
            Log.printDebug("Tuner.statusChanged(): Event received " + rse.toString());
        }

        if (rse instanceof NetworkInterfaceReleasedEvent) {
            synchronized (statusChangedEvent) {
                receivedStatusChangedEvent = true;
                /* wake up blocked thread */
                statusChangedEvent.notify();
            }
        }
    }

}

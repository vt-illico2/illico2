package com.videotron.tvi.illico.vod.controller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;


public class ComponentPool {
	static ArrayList pool = new ArrayList(203);
	static HashSet classes = new HashSet();

	public static void addComponent(UIComponent ui) {
		pool.add(ui);
		classes.add(ui.getClass());
	}

	public static void cleanUp() {
		Log.printInfo("ComponentPool, cleanUp()");
		Iterator i = pool.iterator();
		while (i.hasNext()) {
			((UIComponent) i.next()).stop();
		}
		pool.clear();

		i = classes.iterator();
		while (i.hasNext()) {
			Class cls = (Class) i.next();
			try {
				int cnt = cls.getField("instanceCounter").getInt(cls);
				if (cnt > 0) {
					Log.printWarning("Check, " + cls + " = " + cnt);
				}
			} catch (Exception e) {}
		}
	}

	public static void dump() {
		Log.printDebug("ComponentPool, dump(), size = " + classes.size());
		Iterator i = classes.iterator();
		while (i.hasNext()) {
			Class cls = (Class) i.next();
			try {
				Field f = cls.getField("instanceCounter");
				int cnt = f.getInt(cls);
				Log.printDebug(cls + " = " + cnt);
			} catch (Exception e) {}
		}
	}
}

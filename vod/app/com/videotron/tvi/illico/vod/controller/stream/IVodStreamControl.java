package com.videotron.tvi.illico.vod.controller.stream;

/**
 * IVodStreamControl</code> provides a VCR style interface for controlling a remote media stream.
 * 
 */
public interface IVodStreamControl {

    /** The stream is allocated but not request has been performed.*/
    public static final int STREAM_STATE_INIT = 0x00;

    /** The media server is transporting the stream.*/
    public static final int STREAM_STATE_PLAYING = 0x01;

    /** The media server has paused the stream the stream.*/
    public static final int STREAM_STATE_PAUSED = 0x02;

    /** The stream endpoint was reached. The stream will remain in this mode as if it was paused with the maximum NPT time. */
    public static final int STREAM_STATE_END = 0x03;

    /** The state of the stream is unknown.*/
    public static final int STREAM_STATE_UNDEFINED = 0x04;

    /** The current stream position as seen by the media server.*/
    public static final int NPT_NOW = 0x80000000;

    /** The maximum stream position.*/
    public static final int NPT_END = 0x7FFFFFFF;

    /** A normal condition.*/
    public static final int STATUS_OK = 0x00;

    /** The requested operation was bad.*/
    public static final int STATUS_BAD_REQUEST = 0x01;

    /** The operation refers to an invalid stream. */
    public static final int STATUS_BAD_STREAM = 0x02;

    /** The operation was illegal for the current stream state. */
    public static final int STATUS_WRONG_STATE = 0x03;

    /** The cause of error could not be determined. */
    public static final int STATUS_UNKNOWN_ERROR = 0x04;

    /** The client does not have permission for this request. */
    public static final int STATUS_PERMISSION_DENIED = 0x05;

    /** A bad parameter was specified. */
    public static final int STATUS_BAD_PARAMETER = 0x06;

    /** Memory allocation failure. */
    public static final int STATUS_NO_MEMORY = 0x07;

    /** The implementation limit has been exceeded. */
    public static final int STATUS_IMP_LIMIT = 0x08;

    /** A transient error occurred, reissue the request. */
    public static final int STATUS_TRANSIENT_ERROR = 0x09;

    /** Not enough resources available. */
    public static final int STATUS_NO_RESOURCES = 0x0a;

    /** The server experience an error. */
    public static final int STATUS_SERVER_ERROR = 0x0b;

    /** The server experience failure. */
    public static final int STATUS_SERVER_FAILURE = 0x0c;

    /** A bad scale parameter was specified. */
    public static final int STATUS_BAD_SCALE = 0x0d;

    /** The start NPT value was not acceptable. */
    public static final int STATUS_BAD_START = 0x0e;

    /** The stop NPT value was not acceptable. */
    public static final int STATUS_BAD_STOP = 0x0f;

    /** MPEG stream delivery problems. */
    public static final int STATUS_MPEG_DELIVERY = 0x10;

    /** A response has not been received in the specified time. */
    public static final int STATUS_OPERATION_TIMEDOUT = 0x11;

    /**
     * returns the IVodHandlerobject.
     * 
     * @return the IVodeHandler
     */
    public IVodHandler getVodHandler();

    /**
     * returns the string handle as seen identified by the media server.
     * 
     * @return an integer value representing the stream handle as seen by the media server.
     */
    public int getStreamHandle();

    /**
     * Sets the maximum time to wait for a completion event.
     * 
     * @param timeout
     *            the timeout in millisecond units.
     */
    public void setRequestTimeout(int timeout);

    /**
     * Requests the stream to be played.
     * 
     * @param startNpt
     *            The stream position to begin playing.
     * @param stopNpt
     *            The stream position time to stop.
     * @param scaleNumerator
     *            Numerator portion of the desired stream rate.
     * @param scaleDenominator
     *            Denominator portion of the desired stream rate.
     * @param act
     *            asynchronous completion token.
     * @throws VodStrmInvdScaleException
     * @throws VodStrmInvaldNptException
     */
    public void play(int startNpt, int stopNpt, short scaleNumerator, short scaleDenominator, Object act)
            throws VodStrmInvdScaleException, VodStrmInvaldNptException;

    /**
     * Requests the stream to be stopped.
     * 
     * @param stopNpt
     *            The desired stream position time to pause.
     * @param act
     *            asynchronous completion token.
     * @throws VodStrmInvaldNptException
     */
    public void pause(int stopNpt, Object act) throws VodStrmInvaldNptException;

    /**
     * Requests the stream to be played in fastforward mode.
     * 
     * @param startNpt
     *            Stream start position.
     * @param stopNpt
     *            Stream stop position.
     * @param scaleNumerator
     *            Numerator portion of the desired stream rate.
     * @param scaleDenominator
     *            Denominator portion of the desired stream rate.
     * @param act
     *            asynchronous completion token.
     * @throws VodStrmInvaldNptException
     * @throws VodStrmInvdScaleException
     */
    public void fastForward(int startNpt, int stopNpt, short scaleNumerator, short scaleDenominator, Object act)
            throws VodStrmInvaldNptException, VodStrmInvdScaleException;

    /**
     * Causes the stream to play back at the specified rate. The rate is specified by the numerator and denominator.
     * 
     * @param startNpt
     *            Stream start position.
     * @param stopNpt
     *            Stream stop position.
     * @param scaleNumerator
     *            Numerator portion of the desired stream rate.
     * @param scaleDenominator
     *            Denominator portion of the desired stream rate.
     * @param act
     *            asynchronous completion token.
     * 
     * @throws VodStrmInvaldNptException
     * @throws VodStrmInvdScaleException
     */
    public void rewind(int startNpt, int stopNpt, short scaleNumerator, short scaleDenominator, Object act)
            throws VodStrmInvaldNptException, VodStrmInvdScaleException;

    /**
     * Resets the stream to it's initial state.
     * 
     * @param act
     *            asynchronous completion token.
     */
    public void reset(Object act);

    /**
     * Subscribes a listener to receive events related to this stream control object. If the specified listener is
     * currently subscribed, no action is performed.
     * 
     * @param listener
     *            - The listener of stream events to subscribe.
     */
    public void addListener(IVodStreamControlListener listener);

    /**
     * Unsubscribes a listener from receiving events related to this stream control object. If the specified listener is
     * not currently subscribed no action is performed.
     * 
     * @param listener
     * @throws NullPointerException
     */
    public void removeListener(IVodStreamControlListener listener);

    /**
     * Obtains the latest status information of this stream. This method completes
     * asynchronously indicated by event VodStreamStatusEvent</code>.
     * 
     * @param act
     *            The asynchronous completion token.
     * 
     * @see VodStreamStatusEvent
     * @see VodStreamControlEvent#getStreamNpt()
     * @see VodStreamControlEvent#getStreamScaleNum()
     * @see VodStreamControlEvent#getStreamScaleDenom()
     * @see VodStreamControlEvent#getStreamState()
     */
    public void queryStatus(Object act);
}

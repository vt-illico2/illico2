package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import org.ocap.hardware.Host;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtils;

public class SessionProperties {

    // Client MAC address
    public static byte[] clientMac;
    // Message Time out
    public static int msgTimeOut = 10;
    // Session in progress Time out
    public static int SESSION_IN_PROGRESS_TIME_OUT = 10;
    // Message retry count;
    public static int MESSAGE_RETRY_COUNT;
    // sessionIdAssignor (See ISO/IEC 13818=6:1998(E) 3.2.1)
    public static int SESSION_ID_ASSIGNOR = 0x01;;
    // resourceIdAssignor
    public static int RESOURCE_ID_ASSIGNOR = 0x00;;
    // maxForwardCount
    public static int MAX_FORWARD_COUNT =3;
    // dncsIP
    public static String DNCS_IP;
    // dncsPort
    public static int DNCS_PORT;
    // GS ( global server ) IP
    public static int GS_IP;
    public static String GS_IP_STR;
    public static String SERVICE_GW;
    public static String SERVICE_NAME;

    public static void initProperties() {
    	Log.printDebug("SessionProperties.initProperties()");
    	
        String hostMAC = null;
        if (Environment.HW_VENDOR != Environment.HW_VENDOR_SAMSUNG) {
            hostMAC = Host.getInstance().getReverseChannelMAC();
            clientMac = StringUtils.macAddressFromStringToByte(hostMAC); // dataCenter.getString("clientMac")
        } else{
            // get cable card mac from Monitor;
            clientMac = CommunicationManager.getInstance().getCableCardMacAddress();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("SessionProperties initProperties HW_VENDOR : " + Environment.HW_VENDOR);
            Log.printDebug("SessionProperties initProperties MAC : " + StringUtils.getHexStringBYTEArray(clientMac));
        }

        DataCenter dataCenter = DataCenter.getInstance();
        
        msgTimeOut = dataCenter.getInt("MSG_TIME_OUT");
        SESSION_IN_PROGRESS_TIME_OUT = dataCenter.getInt("SESSION_IN_PROGRESS_TIME_OUT");
        MESSAGE_RETRY_COUNT = dataCenter.getInt("MESSAGE_RETRY_COUNT");

        DNCS_IP = dataCenter.getString("DNCS_IP");
        try {
        	DNCS_PORT = Integer.parseInt(dataCenter.getString("DNCS_PORT"));
        } catch (NumberFormatException e) {}

        GS_IP_STR = dataCenter.getString("GW_IP");
        GS_IP = StringUtil.stringIPtoInt(dataCenter.getString("GW_IP"));
        SERVICE_GW = dataCenter.getString("SERVICE_GW");
        SERVICE_NAME = dataCenter.getString("SERVICE_NAME");

//        if (Log.DEBUG_ON) {
//            Log.printDebug("DNCS_IP : " + DNCS_IP);
//        }
    }

}

package com.videotron.tvi.illico.vod.controller.session.autodiscovery;

/*
 *  TSIDResolver.java	$Revision: 1.5 $ $Date: 2010/05/31 21:13:33 $
 *
 *  Copyright (c) 2001-2005 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

import java.io.File;
import java.util.Date;
import java.util.Vector;

import org.davic.net.InvalidLocatorException;
import org.ocap.net.OcapLocator;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.ErrorCodes;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.Tuner;
import com.videotron.tvi.illico.vod.data.config.ConfigurationFileReceiver;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

/**
 * <code>ServiceGroupController</code>.
 *
 * @author $Author: lsc $
 * @version $Revision: 1.5 $
 * @since Woo-Sik Lee, 2007. 4. 23
 */
public class ServiceGroupController{

    public boolean sgReady = false;

    private int[] frequencies;
    private int[] tsIds;
    private int[] serviceGroupIds;
    private int[] mods;

    private boolean changedSGMVersion = false;

//    private boolean firstCheck = true;
//
//    private int curFreq = 0;
//
//    private ArrayList ids;
//
//    private boolean interrupted = false;
//
//    private NetworkInterfaceController nic = null;
//
//    private NetworkInterface[] nis = null;

    private ServiceGroupMap serviceGroupMap;

    private int serviceGroupMapVersion = -1;

    private int rowCnt = 0;

    private int totalRow;

    private int validSGIDIndex = -1;

    private boolean serachComplete;

    private static ServiceGroupController autoDiscovery;

    private Vector sgEventListener = new Vector();

    private Tuner tuner;

    public static ServiceGroupController getInstance() {
        if (autoDiscovery == null) {
            autoDiscovery = new ServiceGroupController();
        }
        return autoDiscovery;
    }

    private ServiceGroupController() {
        if (Log.DEBUG_ON) {
            Log.printDebug("constructs ServiceGroupController");
        }
    }

    /**
     * @return the sgReady
     */
    public boolean isSgReady() {
        return sgReady;
    }
    
    public static void main(String[] a) {
    	File file = new File("d://servicegroupmap.dat");
    	getInstance().setSGMapFile(file);
    }

    public void setSGMapFile(File file) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ServiceGroupControl.setSGMapFile file : " + file);
        }
        ServiceGroupMap sgFile = new ServiceGroupMap();
        boolean ret = sgFile.makeServiceGroupData(file);
        if (ret == false) {
        	Log.printWarning("ServiceGroupControl.setSGMapFile fail");
        	return;
        }
        sgReady = false;
        int oldVer = serviceGroupMapVersion;
        String log = "";
        if (sgFile.getVersion() != serviceGroupMapVersion) { // even if same version
        	serviceGroupMapVersion = sgFile.getVersion();
            serviceGroupMap = sgFile;
            tsIds = serviceGroupMap.getTsIDs();
            frequencies = serviceGroupMap.getFrequencies();
            totalRow = serviceGroupMap.getEntryLength();
            serviceGroupIds = serviceGroupMap.getServiceGroup();
            mods = serviceGroupMap.getMod();
            rowCnt = 0;
            changedSGMVersion = true;
            
            log = CatalogueDatabase.SDF.format(new Date()) 
	    		+ ", ServiceGroupControl.setSGMapFile is called. "
	    		+ "v = " + oldVer + " > " + serviceGroupMapVersion 
	    		+ ", totalRow = " + totalRow
	    		+ ", uniq freq = " + serviceGroupMap.getUniqueFrequencies();
            MenuController.getInstance().debugList[4].add(log);
        } else {
        	log = CatalogueDatabase.SDF.format(new Date()) 
	     		+ ", ServiceGroupControl.setSGMapFile is called. NO CHANGE, " 
	     		+ "v = " + oldVer + " > " + serviceGroupMapVersion;
        	MenuController.getInstance().debugList[4].add(log);
        }
        Log.printDebug("ServiceGroupControl.setSGMapFile, " + log);
        sgReady = true;
    }

//    public void run() {
//        serviceGroupMap = new ServiceGroupMap();
//        int retry = 0;
//        while (true) {
//            try {
//                if (retry > maximumRetry) {
//                    serviceGroupMap.startOOB(true);
//                } else {
//                    serviceGroupMap.startOOB(false);
//                }
//                retry++;
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("ServiceGroupController attach service group map file, retry : " + retry);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            if (sgReady) {
//                return;
//            }
//            try {
//                Thread.sleep(1000);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

    public void searchServiceGroup() {
    	MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) 
    			+ ", ServiceGroupControl.searchServiceGroup() called, row = " + totalRow);
    	SGEvent sg = new SGEvent();
        sg.setEventType(ErrorCodes.FAILURE);
        
        if (Environment.EMULATOR) {
            setSGMapFile(null);
            serachComplete = true;
            changedSGMVersion = false;
            sendSGEvent(sg);
            return;
        }
        int count = 0;
        while (!isSgReady()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("Service group map file is not ready " + count);
            }
            if (count == 30) {
//                File file = (File) DataCenter.getInstance().get("meta_SGM");
//                setSGMapFile(file);
            	CommunicationManager.getInstance().errorMessage("VOD502", null);
                break;
            }
            count++;
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("Service group map file is ready = " + isSgReady());
        }
        if (tuner == null) {
            tuner = new Tuner();
        } else {
    		tuner.clearTSIDcache();
        }

//        if (totalRow == 0 || Environment.EMULATOR) {
//            serachComplete = true;
//            sendSGEvent(sg);
//            return;
//        }
        if (totalRow == 0) {
        	File serviceGropupMap = (File) DataCenter.getInstance().get(ConfigurationFileReceiver.serviceGroupMapKey);
        	MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) + ", Load file again. f = " + serviceGropupMap);
        	
            if (serviceGropupMap != null) {
            	serviceGroupMapVersion = -1;
            	setSGMapFile(serviceGropupMap);
            }
        }
        Vector tunedInfos = new Vector();
        outer: for (int i = 0; i < totalRow; i++) {
            rowCnt = i;
            String tuneKey = String.valueOf(serviceGroupMap.getFrequencies()[rowCnt]);
            boolean isTunedBefore = tunedInfos.contains(tuneKey);
            Log.printDebug("searchServiceGroup(), row = " + i + ", freq = " + tuneKey + ", tuned = " + isTunedBefore);
            if (isTunedBefore) {
                continue;
            }else {
                tunedInfos.add(tuneKey);
            }
            
            int result = handleTuneState();
            if (result == ErrorCodes.SUCCESS) {
                int tsId = tuner.getTSID();
                Log.printDebug("searchServiceGroup(), row = " + i + ", freq = " + tuneKey + ", return tsId = " + tsId);
                for (int j = 0; j < tsIds.length; j++) {
                    if (tsId == tsIds[j]) {
                        rowCnt = j;
                        validSGIDIndex = rowCnt;
                        serachComplete = true;
                        changedSGMVersion = false;
                        sg.setEventType(ErrorCodes.SUCCESS);
                        if (Log.DEBUG_ON) {
                            Log.printDebug("Discovery completed Service Group : " + serviceGroupIds[j] + ", idx = " + j);
                        }
                        MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) 
                        		+ ", ServiceGroupControl, discovery completed. ID = 0x " + 
                        		StringUtil.byteArrayToHexString(
                        				ServiceGroupController.getInstance()
                        				.getServiceGroupData()).replace('\n', ' '));
                        sg.setValidSGIndex(validSGIDIndex);
                        break outer;
                    }
                }
                Log.printDebug("searchServiceGroup(), row = " + i + ", freq = " + tuneKey + ", return tsId = " + tsId + ", NO MATCH");
            } else if (result == ErrorCodes.TUNE_FAILURE) {
            	CommunicationManager.getInstance().errorMessage("VOD401", null);
            	break outer;
            }
        } // end of for outer:
        serachComplete = true;
        sendSGEvent(sg);
        
        Log.printDebug("ServiceGroupController, searchServiceGroup(), tunner.isTuned()=" + tuner.isTuned());
        
        if (tuner.isTuned()) {
        	tuner.deTune();
        	Log.printDebug("ServiceGroupController, searchServiceGroup(), tunner.deTune called");
        }
        
        MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) + ", ServiceGroupControl.searchServiceGroup() is done");
    }

//    /**
//     * @return the changedSGMVersion
//     */
//    public boolean isChangedSGMVersion() {
//        if (firstCheck) {
//            firstCheck = false;
//            return false;
//        }
//        return changedSGMVersion;
//    }

    private int handleTuneState() {
        int result = -1;
        int freq = frequencies[rowCnt];
        int tsid = tsIds[rowCnt];
        int sg = serviceGroupIds[rowCnt];
        int mod = mods[rowCnt];
        if (Log.DEBUG_ON) {
            Log.printDebug("sgmap file entry: freq=" + freq + ", tsid=" + tsid + ", sg=" + sg);
        }

        // See if the tuner already has TSID info for this frequency cached
        Integer tmpTsid = tuner.findCachedTSID(freq);
        if (tmpTsid != null) {
            // We already have the TSID for this frequency, does it match?
            if (tmpTsid.intValue() == tsid) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("found matching cached tsid: " + tsid);
                }
                result = ErrorCodes.SUCCESS;
            }
        } else {
            // No cached TSID, must tune
            OcapLocator loc = null;
            try {
                loc = new OcapLocator(freq, mod);
            } catch (InvalidLocatorException ex) {
                ex.printStackTrace();
            }
            tuner.deTune();
            // Call our Tuner object.
            result = tuner.tune(loc);
            MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) + ", Tune to " + loc.toExternalForm());
        }
        return result;
    }

//    public void interrupt() {
//        interrupted = true;
//    }

    public synchronized int[] getTSIDs() {
        if (tsIds == null) {
            return new int[0];
        }
        return tsIds;
    }

//    public boolean isSerachComplete() {
//        return serachComplete;
//    }

//    public int getValidSGIDIndex() {
//        return validSGIDIndex;
//    }

    public void addSGEventListener(SGEventListener listener) {
        sgEventListener.add(listener);
    }

    public void removeSGEventListener(SGEventListener listener) {
        sgEventListener.remove(listener);
    }

    public void sendSGEvent(SGEvent event) {
        if (sgEventListener != null && sgEventListener.size() > 0) {
            Object listeners[];
            synchronized (sgEventListener) {
                listeners = new Object[sgEventListener.size()];
                sgEventListener.copyInto(listeners);
            }
            for (int i = 0; i < listeners.length; i++) {
                SGEventListener listener = (SGEventListener) listeners[i];
                listener.receiveSGEvent(event);
            }
        }
        CommunicationManager.getInstance().sendReadyMSGtoMonitor();
    }

    public void eraseServiceGroup() {
    	MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) 
    			+ ", ServiceGroupControl.eraseServiceGroup() called"); 
    	validSGIDIndex = -1;
    	if (tuner != null) {
    		tuner.clearTSIDcache();
    	}
    }
    
    public int getServcieGroupID() {
    	MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) 
    			+ ", ServiceGroupControl.getServcieGroupID() called");
    	if (!serachComplete) {
        	if (Log.WARNING_ON) {
        		Log.printWarning("getServcieGroupID(), searchComplete is FALSE");
        	}
            return -1;
        }
        if (changedSGMVersion) {
        	if (Log.WARNING_ON) {
        		Log.printWarning("getServcieGroupID(), changedSGMVersion is TRUE");
        	}
            return -1;
        }
        try {
        	return serviceGroupIds[validSGIDIndex];
        } catch (ArrayIndexOutOfBoundsException e) {
        	return -1;
        }
    }
    
    public byte[] getServiceGroupData() {
        if (!serachComplete) {
        	if (Log.WARNING_ON) {
        		Log.printWarning("getserviceGroupData(), searchComplete is FALSE");
        	}
            return null;
        }
        if (changedSGMVersion) {
        	if (Log.WARNING_ON) {
        		Log.printWarning("getserviceGroupData(), changedSGMVersion is TRUE");
        	}
            return null;
        }

        byte[] sgarray = new byte[6];
        int sg = -1;
        if (Environment.EMULATOR) {
            validSGIDIndex = 0;
        }
        if (serviceGroupIds == null) {
        	if (Log.WARNING_ON) {
        		Log.printWarning("getserviceGroupData(), serviceGroupIds is NULL");
        	}
            return null;
        }
        try {
	        sg = serviceGroupIds[validSGIDIndex];
	        long temp = sg;
	        temp &= 0xFFFFFFFFL; // Mask to 32 bits.

	        for (int i = sgarray.length - 1; i >= 0; i--) {
	            sgarray[i] = (byte) (temp & 0xFF);
	            temp >>= 8;
	        }
	        if (Log.DEBUG_ON) {
	        	Log.printDebug("getServiceGroupData(), index = " + validSGIDIndex
	        			+ ", SG ID = " + sg
	        			+ ", HEX = " + StringUtil.byteArrayToHexString(sgarray));
	        }
	        return sgarray;
        } catch (ArrayIndexOutOfBoundsException ae) {
        	return null;
        } catch (Exception e) {
        	Log.printError("getServiceGroupData(), catch EXCEPTION");
        	Log.print(e);
        	return null;
        }
    }

//    public ServiceGroupMap getServiceGroupMapFile() {
//        return serviceGroupMap;
//    }
}

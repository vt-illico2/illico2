/*
 * 
 * 
 * To change the template for this generated file go to Window - Preferences -
 * Java - Code Generation - Code and Comments
 */
package com.videotron.tvi.illico.vod.controller.stream.event;

import com.videotron.tvi.illico.vod.controller.stream.IVodStreamControl;

/**
 * VodStreamResetEvent completes the reset request operation.
 * 
 */
public class VodStreamResetEvent extends VodStreamControlEvent {

    private static final long serialVersionUID = -1548273154815561881L;

    /**
     * Constructor for the event.
     * 
     * @param objSrc
     *            The source of this event.
     * @param act
     *            Asynchronous completion token.
     * @param streamHandle
     *            The handle of the stream.
     * @param streamState
     *            The state of the stream.
     * @param statusCode
     *            Identifies the outcome of the requested operation.
     * @param lastNpt
     *            the npt of the stream reported by the media server at the time the operation was processed.
     * @param scaleNum
     *            the numerator portion of the stream's current rate.
     * @param scaleDenom
     *            the denominator portion of the stream's current rate
     * 
     * @see VodStreamControlEvent
     * @see VodStreamPlayEvent
     * @see VodStreamPauseEvent
     * @see VodStreamFastForwardEvent
     * @see VodStreamRewindEvent
     * @see VodStreamStatusEvent
     * @see VodStreamEndOfStreamEvent
     * @see VodStreamResetEvent
     * @see IVodStreamControl
     * @see IVodStreamControlListener
     */
    public VodStreamResetEvent(IVodStreamControl objSrc, java.lang.Object act, int streamHandle, int streamState,
            int statusCode, int lastNpt, short scaleNum, short scaleDenom) {
        super(objSrc, act, streamHandle, streamState, statusCode, lastNpt, scaleNum, scaleDenom);
        // TODO Auto-generated constructor stub
    }

}

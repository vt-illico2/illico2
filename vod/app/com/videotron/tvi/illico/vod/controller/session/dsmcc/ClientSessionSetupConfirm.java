/*  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;

/**
 * @author LSC
 * @version 1.0 ClientSessionSetupConfirm. This class handles the message that is sent from the Network to a Client in
 *          response to a ClientSessionRequest message.
 */
public class ClientSessionSetupConfirm extends SessionMessage implements NetworkToClientMessage {
    /**
     * Contains the response code.
     */
    private short response;

    /**
     * Contains the value of the serverId.
     */
    private byte[] serverID = new byte[20];

    /**
     * Contains Resources and UserData.
     * */
    private byte[] resourceAndPrivateData;

    /**
     * ClientSessionSetupConfirm Default Constructor.
     */
    public ClientSessionSetupConfirm() {
        super();
    }

    /**
     * Returns the response code from the DSMCC message. The response codes are as defined in ISO/IEC 13818-6:1998(E)
     * Table 4-60 Non-user defined codes are always positive numbers. A negative number indicates a failure in parsing.
     * 
     * @return response code
     */
    public short getResponse() {
        return response;
    }

    /**
     * Returns the resources and private data (UserData) from the message. Called by DSMCCMessenger
     * parseClientSessinSetupConfirm
     * 
     * @return Byte array containing the UserData.
     */
    public byte[] getResourceAndPrivateDataBytes() {
        return resourceAndPrivateData;
    }

    /**
     * Returns the server ID field of the message. Called by DSMCCMessenger parseClietnSessionSetupConfirm.
     * 
     * @return byte[] byte array of server id.
     */
    public byte[] getServerID() {
        return serverID;
    }

    /**
     * Parses a raw message into a ClientSessionSetupConfirm message.
     * 
     * @param data
     *            Raw message to be parsed.
     */
    public void parse(byte[] data) {
        parse(data, 0);
    }

    /**
     * Description of method... Parses a raw message into a ClientSessionSetupConfirm message.
     * 
     * @param data
     *            Raw message to be parsed.
     * @param offset
     *            Offset into the raw data array to the beginning of the message.
     */
    public void parse(byte[] data, int offset) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data, offset, data.length - offset);
        DataInputStream in = new DataInputStream(bais);

        try {
            // Parse in the DSMCC header and SessionID
            super.parse(in);

            response = in.readShort();
            in.readFully(serverID);

            // At this point, in is pointing to the resource structure.
            // The caller will parse the remaining data.
            resourceAndPrivateData = new byte[in.available()];
            in.readFully(resourceAndPrivateData);

            in.close();
            in = null;
        } catch (Exception ex) {
            System.out.println("ERROR parse " + ex.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                    in = null;
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

    /**
     * toString Gives human readable version of the message.
     * 
     * @return String
     */
    public String toString() {
        String rc = super.toString();
        StringBuffer sb = new StringBuffer(rc);
        sb.append("\nServer ID = ");
        sb.append(StringUtil.byteArrayToHexString(serverID));
        sb.append("\nResponse = ");
        sb.append(Integer.toHexString(response));
        rc = sb.toString();
        return rc;
    }
}

package com.videotron.tvi.illico.vod.controller.session.service;

import org.davic.net.InvalidLocatorException;
import org.ocap.net.OcapLocator;

public class ServiceLocator extends OcapLocator {
    private byte[] ca = null;

    private byte[] sessionId = null;

    public ServiceLocator(int qamFrequency, int mpegProgramNumber, int qamMode) throws InvalidLocatorException {
        super(qamFrequency, mpegProgramNumber, qamMode);
    }

    public ServiceLocator(int qamFrequency, int mpegProgramNumber, int qamMode, byte[] ca, byte[] sId)
            throws InvalidLocatorException {
        super(qamFrequency, mpegProgramNumber, qamMode);
        this.ca = ca;
        sessionId = sId;
    }

    public ServiceLocator(int qamFrequency, int mpegProgramNumber) throws InvalidLocatorException {
        super(qamFrequency, mpegProgramNumber, 0x10);
    }

    public byte[] getCaData() {
        return ca;
    }

    /**
     * @return the sessionId
     */
    public byte[] getSessionId() {
        return sessionId;
    }
}

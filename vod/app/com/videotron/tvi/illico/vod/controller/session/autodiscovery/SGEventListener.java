package com.videotron.tvi.illico.vod.controller.session.autodiscovery;

public interface SGEventListener {
    public abstract void receiveSGEvent(SGEvent result);
}

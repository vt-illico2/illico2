package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.IOException;
import java.io.DataInputStream;

import com.videotron.tvi.illico.log.Log;

/**
 * <code>MPEGProgramNumberResource</code> This class represents an MPEG program session resource.
 *
 * From sspVersion V2.3 Page 32: " Used to convey the MPEG program number that will be sent from the video server. This
 * resource descriptor is optional and is sent only when the particular Netwrok implementation requires a specific MPEG
 * Program to be associated with a session. If the server provides an MPEG program number (PN)using the MPEG Program
 * resource descriptor, then the SRM will assume that the program is part of a multi-program transport stream (MPTS). If
 * the server does not provide an MPEGProgram resource descriptor, then the SRM will assume that the program represents
 * a single-program transport stream (SPTS). The latter is the normal cae for Gigabit Ethernet devices. This is a
 * standard DSM-CC resource descriptor. It contains fields that do not apply to this implementation and are not used.
 * Only the mpegProgramNum field is used in this descriptor.
 *
 * @see MPEGProgramResource
 * @see ATSCModulationModeResource
 * @see IPResource
 * @see SessionResource
 * @see PhysicalChannelResource
 * @see TSIDResource
 */
public class MPEGProgramResource extends SessionResource {
    // See pages 32 - 34 of the sspVersion V2.3 spec for an explanation of the
    // following.
    private short mpeg_resourceValueType;

    private short mpeg_prog_num = 0; // /** For SSP2.3 single value encoding
    // only */

    private short mpeg_pmt_pid_resourceValueType;

    private short mpeg_pmt_pid = 0xff; // /** For SSP2.3 not used. Values of
    // all 1's shall be used*/

    private short mpeg_ca_pid = 0xff; // /** For SSP2.3 not used. Values of
    // all 1's shall be used*/

    private short elem_stream_cnt = 0; // /** For SSP2.3 this value is 0x00*/

    private short mpeg_pcr_resourceValueType; // Per sspVersion 2.3 this is 0001

    private short mpeg_pcr = 0xff; // /** Program Clock Reference, most likely

    // not used and all 1's */

    public MPEGProgramResource(byte[] b, SessionResource sres) {

    }

    /**
     * Constructors for this class.
     *
     * @param rba
     *            Resource specific data.
     */
    public MPEGProgramResource(DataInputStream rba, SessionResource sRes) {
        super.copy(sRes);
        try {
            int messageCount = super.getFieldLength();

            // We are parsing through a Table 4-7 mpegProgramDescriptor

            // I'm guessing resource value should be 0x0001 for client
            // though, can be 0x0002 for server and indicate a list.
            // So, if this give you grief, try changing it.
            mpeg_resourceValueType = rba.readShort();
            mpeg_prog_num = rba.readShort();

            // Resource value will always be 0x0001
            mpeg_pmt_pid_resourceValueType = rba.readShort();
            mpeg_pmt_pid = rba.readShort();

            // Per sspVersion V2.3 ca pid is not used.
            mpeg_ca_pid = rba.readShort();

            // Per sspVersion V2.3 elem_stream_cnt shall always be 0.
            elem_stream_cnt = rba.readShort();
            for (int i = 0; i < elem_stream_cnt; i++) {
                rba.skipBytes(6);
            }

            // Resource type should always be 0x0001.
            // PCR is supposed to always be 0xFFFF but
            // seems to show up as 0x1FFF. Go figure.
            mpeg_pcr_resourceValueType = rba.readShort();
            mpeg_pcr = rba.readShort();

            // Strip off any junk just in case I missed something.
            messageCount -= (16 + elem_stream_cnt * 6);

            // For some reason, there are additional bytes here. So, read and
            // discard them.
            if (messageCount > 0) {
                rba.skip(messageCount);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("mpeg_resourceValueType : " + mpeg_resourceValueType);
                Log.printDebug("mpeg_prog_num : " + mpeg_prog_num);
                Log.printDebug("mpeg_pmt_pid_resourceValueType : " + mpeg_pmt_pid_resourceValueType);
                Log.printDebug("mpeg_pmt_pid : " + mpeg_pmt_pid);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * <code>getMpegProgramNumber</code> Returns the MPEG program number associated with the session.
     *
     * @return An integer storing the MPEG program number.
     */
    public int getMPEGProgramNumber() {
        return mpeg_prog_num;
    }

}

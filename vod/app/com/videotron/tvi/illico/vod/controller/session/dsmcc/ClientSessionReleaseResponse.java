package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * UNClientSessionReleaseResponse.
 */
public class ClientSessionReleaseResponse extends SessionMessage implements ClientToNetworkMessage {

    byte[] uuData;

    short response;

    public ClientSessionReleaseResponse() {
        setMessageID(SessionMessage.CLIENT_SESSION_RELEASE_RESPONSE);
    }

    /**
     * A setter for the UU data of the message.
     *
     * @param uudata
     */
    public void setUUData(byte[] uudata) {
        uuData = uudata;
    }

    /**
     * Message response setter. Per the DSM-CC spec: "The response field shall be set by the Client to a value which
     * indicates the Client's response to the ClientSessionReleaseIndication message.
     *
     * @param response
     */
    public void setResponse(int response) {
        this.response = (short) response;
    }

    /**
     * Serialize this object into a byte array.
     *
     * @return the byte array.
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(bo);

        try {
            out.write(super.toByteArray());
            out.writeShort(response);
            out.write(uuData);

            rc = bo.toByteArray();

            out.close();
            out = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (out != null) {
                try {
                    out.close();
                    out = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }

        return rc;
    }

}

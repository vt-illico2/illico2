package com.videotron.tvi.illico.vod.controller.communication;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.UsageManager;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.ui.BaseUI;

public class PreferenceProxy implements PreferenceListener {

	private static PreferenceProxy instance;

	private XletContext xletContext;

	private PreferenceService preferenceService;

	private FilterUpdateListner filterListener = new FilterUpdateListner();
	private String[] preferenceKeys;
	private ArrayList wishList = new ArrayList();
	private String[] ratings = new String[] { Definitions.RATING_G,
			Definitions.RATING_8, Definitions.RATING_13, Definitions.RATING_16,
			Definitions.RATING_18 };

	public static final int ADD_WISHLIST_SUCCESS = 0;
	public static final int ADD_WISHLIST_FAILED = 1;
	public static final int ADD_WISHLIST_DUPLICATED = 2;
	
	private PreferenceProxy() {
		preferenceKeys = new String[] {
				PreferenceNames.LANGUAGE,
				PreferenceNames.VOD_WISH_LIST,
				PreferenceNames.WISHLIST_DISPLAY,
				PreferenceNames.PREFERENCES_ON_FORMAT,
				PreferenceNames.PREFERENCES_ON_LANGUAGE,
				PreferenceNames.VOD_1080P_SUPPORT,
				RightFilter.PARENTAL_CONTROL,
				RightFilter.BLOCK_BY_RATINGS,
				RightFilter.HIDE_ADULT_CONTENT,
				RightFilter.SUSPEND_DEFAULT_TIME,
				RightFilter.PROTECT_SETTINGS_MODIFICATIONS,
				MenuController.LAST_VIEW,
				MenuController.KARA_LIST,
				MonitorService.SUPPORT_UHD,
				RightFilter.CHANNEL_RESTRICTION,
				PreferenceNames.FAVOURITE_CHANNEL,
				Log.LOG_CAPTURE_APP_NAME, // Logging tools, Bundling
		};
	}

	public static synchronized PreferenceProxy getInstance() {
		if (instance == null) {
			instance = new PreferenceProxy();
		}
		return instance;
	}
	
	public PreferenceService getService() {
		return preferenceService;
	}

	public void init(XletContext xletContext) {
		this.xletContext = xletContext;
		lookUpService();
	}

	private void lookUpService() {
		/** IXC Binding */
		try {
			new Thread("lookUpService - PreferenceService.IXC_NAME") {
				public void run() {
					String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
					while (true) {
						if (preferenceService == null) {
							try {
								preferenceService = (PreferenceService) IxcRegistry
										.lookup(xletContext, lookupName);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if (preferenceService != null) {
							if (Log.DEBUG_ON) {
								Log.printDebug("PreferenceProxy.lookUpService: preferenceService ");
							}
							try {
								String[] preferences = preferenceService
										.addPreferenceListener(
												PreferenceProxy.this,
												Resources.APP_NAME,
												preferenceKeys,
												null); // null array for default values
//												new String[] {
//														Definitions.LANGUAGE_ENGLISH,
//														null, null, null });
								setPreference(preferenceKeys, preferences);
							} catch (Exception ignore) {
								ignore.printStackTrace();
							}
							break;
						}
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setPreference(String[] keys, String[] preference) {
		for (int i = 0; i < keys.length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("setPreference key[" + keys[i]
						+ "] ,  preference : " + preference[i]);
			}
			if (preference[i] == null) {
				continue;
			}
			if (keys[i].equals(PreferenceNames.LANGUAGE)) {
				if (preference[i].equals(Definitions.LANGUAGE_FRENCH)) {
					Resources.currentLanguage = Resources.LANGUAGE_FR;
				} else {
					Resources.currentLanguage = Resources.LANGUAGE_EN;
				}
				DataCenter.getInstance().put(PreferenceNames.LANGUAGE,
						preference[i]);
				
				// IMPROVEMENT
				CatalogueDatabase.getInstance().resetLastCatalogueVersion();
				
			} else if (keys[i].equals(PreferenceNames.VOD_WISH_LIST)) {
				wishList.clear();
				StringTokenizer t = new StringTokenizer(preference[i], "[], ");
				while (t.hasMoreTokens()) {
					wishList.add(t.nextToken());
				}
			} else if (keys[i].equals(PreferenceNames.WISHLIST_DISPLAY)) {
				String opt = preference[i].equals(Definitions.WISHLIST_DISPLAY_ALPHABETICAL) ? Resources.OPT_NAME : Resources.OPT_DATE;
				MenuController.getInstance().setSortWishlist(opt);
				DataCenter.getInstance().put(keys[i], preference[i]);
			} else if (keys[i].equals(RightFilter.CHANNEL_RESTRICTION)) {
				ChannelInfoManager.getInstance().updateBlockedChannels(preference[i]);
			} else if (keys[i].equals(PreferenceNames.FAVOURITE_CHANNEL)) {
				ChannelInfoManager.getInstance().updateFavoriteChannels(preference[i]);
			} else {
				DataCenter.getInstance().put(keys[i], preference[i]);
			}
		}
	}

	public void receiveUpdatedPreference(String preferenceName, String value)
			throws RemoteException {
		setPreference(new String[] { preferenceName }, new String[] { value });
		synchronized (instance) {
			instance.notifyAll();
		}
	}
	
	public void setNormalZoomMode() {
		Log.printDebug("called setNormalZoomMode()");
		if (Environment.EMULATOR) {
			return;
		}
		try {
			preferenceService.setNormalZoomMode();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setAspect(boolean save) {
		Log.printDebug("called setAspect(), save = " + save);
		if (Environment.EMULATOR) {
			return;
		}
		try {
			if (save) {
				preferenceService.saveZoomMode();
			} else {
				preferenceService.restoreZoomMode();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setFullHdVideoOutput() {
		Log.printDebug("called setFullHdVideoOutput()");
		if (Environment.EMULATOR) {
			return;
		}
		try {
			preferenceService.setFullHDTVPictureFormat();
		} catch (AbstractMethodError e) {
			Log.printError("UPP upgrade required");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setVideoOutput(boolean save) {
		Log.printDebug("called setVideoOutput(), save = " + save);
		if (Environment.EMULATOR) {
			return;
		}
		try {
			if (save) {
				preferenceService.saveTVPictureFormat();
			} else {
				preferenceService.restoreTVPictureFormat();
			}
		} catch (AbstractMethodError e) {
			Log.printError("UPP upgrade required");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void storePlaylist() {
		ArrayList playlist = MenuController.getInstance().getPlaylist();
		ArrayList list = new ArrayList();
		for (int i = 0; i < playlist.size(); i++) {
			list.add(((MoreDetail) playlist.get(i)).getId());
		}
		String val = list.toString();
		try {
			preferenceService.setPreferenceValue(MenuController.KARA_LIST, val);
		} catch (RemoteException e) {}
		Log.printDebug("called storePlaylist(), list = " + val);
	}
	
	boolean loading = false;
	synchronized public void loadPlaylist(boolean first) {
		ArrayList playlist = new ArrayList();
		String val = DataCenter.getInstance().getString(MenuController.KARA_LIST);
		StringTokenizer t = new StringTokenizer(val, "[], ");
		ArrayList listToRetrieve = new ArrayList();
		while (t.hasMoreTokens()) {
			String id = t.nextToken();
			Object cached = CatalogueDatabase.getInstance().getCached(id);
			if (cached != null) {
				playlist.add(cached);
			} else {
				listToRetrieve.add(id);
			}
		}
		if (listToRetrieve.size() > 0 && first && loading == false) {
			loading = true;
			final boolean ori = CommunicationManager.getInstance().turnOnErrorMessage(false);
			CatalogueDatabase.getInstance().retrieveVideos(new BaseUI() {
				public void reflectVCDSResult(Object cmd) {
					loading = false;
					CommunicationManager.getInstance().turnOnErrorMessage(ori);
					loadPlaylist(false);
				}
			}, listToRetrieve, null);
			return;
		}
		Log.printDebug("called loadPlaylist(), list = " + val + ", " + playlist.size() + " items");
		MenuController.getInstance().setPlaylist(playlist);
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		if (cur != null) {
			((BaseUI) cur).updateButtons();
		}
	}

//	public void changeCheckOutFormat(String language, String format) {
//		setPreference(PreferenceNames.PREFERENCES_ON_FORMAT, format);
//		setPreference(PreferenceNames.PREFERENCES_ON_LANGUAGE, language);
//		// TODO: listener로 오는지 확인
//	}

	public ArrayList getWishList() {
		return wishList;
	}

	public int wishListSize() {
		return wishList.size();
	}

	public boolean wishListClear() {
		wishList.clear();
		return setPreference(PreferenceNames.VOD_WISH_LIST, wishList.toString());
	}

	public boolean wishListRemove(String id) {
		wishList.remove(id);
		return setPreference(PreferenceNames.VOD_WISH_LIST, wishList.toString());
	}

	public int wishListAdd(String id) {
		if (wishList.contains(id)) {
			return ADD_WISHLIST_DUPLICATED;
		}
		wishList.add(id);
		return setPreference(PreferenceNames.VOD_WISH_LIST, wishList.toString()) ? ADD_WISHLIST_SUCCESS
				: ADD_WISHLIST_FAILED;
	}

	public boolean setPreference(String key, String value) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceProxy setPreference :" + key + ", value = " + value);
		}
		boolean result = false;
		try {
			preferenceService.setPreferenceValue(key, value);
			result = true;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean wishListContains(String id) {
		return wishList.contains(id);
	}

	// public boolean isBlockAdultTitle(String rating){
	// // if (Log.DEBUG_ON) {
	// // Log.printDebug("PreferenceProxy : isBlockAdultTitle : " +
	// rating);
	// // }
	// boolean checkResult = false;
	// if (rating != null) {
	// String parentalControl =
	// DataCenter.getInstance().getString(RightFilter.PARENTAL_CONTROL);
	// if (Definitions.OPTION_VALUE_YES.equals(parentalControl)) {
	// if (rating.equals(Definitions.RATING_18)) {
	// checkResult = true;
	// }
	// }
	// }
	// return checkResult;
	// }

	public boolean isBlockedByRating(String rating) {
		boolean checkResult = false;
		if (rating != null) {
			String parentalControl = DataCenter.getInstance().getString(
					RightFilter.PARENTAL_CONTROL);
			String blockedRating = DataCenter.getInstance().getString(
					RightFilter.BLOCK_BY_RATINGS);
//			if (Log.DEBUG_ON) {
//				Log.printDebug("PreferenceProxy : "
//						+ "isBlockedByRating = " + rating
//						+ ", parentalControl = " + parentalControl
//						+ ", blockedRating = " + blockedRating);
//			}
			if (Definitions.OPTION_VALUE_ON.equals(parentalControl)
					&& blockedRating != null
					&& !Definitions.RATING_G.equals(blockedRating)) {
				int ratingIndex = 0;
				int blockIndex = 0;
				for (int i = 0; i < ratings.length; i++) {
					if (ratings[i].equals(rating)) {
						ratingIndex = i;
					}
					if (ratings[i].equals(blockedRating)) {
						blockIndex = i;
					}
				}
				if (ratingIndex >= blockIndex) {
					checkResult = true;
				}
			}
		}
		return checkResult;
	}

	public boolean isBlockedByAudltCategory() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceProxy : isBlockedByAudltCategory");
		}
		boolean checkResult = false;
		String parentalControl = DataCenter.getInstance().getString(
				RightFilter.PARENTAL_CONTROL);
		String allowAdultCategory = DataCenter.getInstance().getString(
				RightFilter.HIDE_ADULT_CONTENT);
		if (Definitions.OPTION_VALUE_ON.equals(parentalControl)
				&& allowAdultCategory != null) {
			if (Definitions.OPTION_VALUE_NO.equals(allowAdultCategory)) {
				checkResult = true;
			}
		}
		return checkResult;
	}

	public void checkRightFilter(String filter, String rating, BaseUI listener) {
		checkRightFilter(filter, rating, listener, null);
	}
	public void checkRightFilter(String filter, String rating, BaseUI listener, String[] message) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceProxy checkRightFilter" +
				", filter = " + filter + ", rating = " + rating + ", msg = " + message);
		}
		String callLetter = null;
		if (RightFilter.ALLOW_PURCHASE.equals(filter)) {
//			rating = Definitions.RATING_G;
			rating = MenuController.getInstance().openAdultCategory() ? Definitions.RATING_18 : rating;
		}
		String filters[] = new String[] { filter };
		try {
			UsageManager.getInstance().addUsageLog("1110", "", "", "");
			filterListener.setMessage(message);
			filterListener.setListener(listener, filter);
			preferenceService.checkRightFilter(filterListener,
					Resources.APP_NAME, filters, callLetter, rating);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public void addFavoriteChannel(String callLetter) {
		try {
			preferenceService.addFavoriteChannel(callLetter);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}
	
	public void addFavoriteChannels(String[] callLetters) {
		try {
			preferenceService.addFavoriteChannels(callLetters);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}
	
	public void removeFavoriteChannel(String callLetter) {
		try {
			preferenceService.removeFavoriteChannel(callLetter);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}
	
	public void removeFavoriteChannels(String[] callLetters) {
		try {
			preferenceService.removeFavoriteChannels(callLetters);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}
	
	public void addBlockChannel(String callLetter) {
		try {
			preferenceService.addBlockChannel(callLetter);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}
	
	public void addBlockChannels(String[] callLetters) {
		try {
			preferenceService.addBlockChannels(callLetters);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}
	
	public void removeBlockChannel(String callLetter) {
		try {
			preferenceService.removeBlockChannel(callLetter);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}
	
	public void removeBlockChannels(String[] callLetters) {
		try {
			preferenceService.removeBlockChannels(callLetters);
		} catch (RemoteException e) {
			Log.print(e);
		}
	}

	class FilterUpdateListner implements RightFilterListener {

		BaseUI listener;
		String filter;
		String[] message;

		public void setListener(BaseUI listener, String filter) {
			this.listener = listener;
			this.filter = filter;
		}

		public void setMessage(String[] str) {
			message = str;
		}

		public String[] getPinEnablerExplain() throws RemoteException {
			return message;
		}

		public void receiveCheckRightFilter(int response)
				throws RemoteException {
			if (Log.DEBUG_ON) {
				Log.printDebug("FilterUpdateListner checkRightFilter response : "
								+ response);
			}
			listener.receiveCheckRightFilter(new CheckResult(filter, response));
		}
	}
}

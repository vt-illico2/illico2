package com.videotron.tvi.illico.vod.controller.session.dsmcc;

/**
 * ClientToNetworkMessage.
 */
public interface ClientToNetworkMessage {
    public byte[] toByteArray();

}

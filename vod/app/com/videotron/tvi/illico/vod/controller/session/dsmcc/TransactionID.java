package com.videotron.tvi.illico.vod.controller.session.dsmcc;

public class TransactionID {

    public static final int TRANSACTION_ID_CLIENT_ORIGINATOR = 0x0000;

    public static final int TRANSACTION_ID_SERVER_ORIGINATOR = 0x4000;

    public static final int TRANSACTION_ID_NETWORK_ORIGINATOR = 0x8000;

    public static final int TRANSACTION_ID_MASK = 0x3FFF;

    public static final int TRANSACTION_ID_ORIGINATOR_MASK = 0xC000;

    private int transactionID;

    private static int newTransactionID;

    static {
        long time = System.currentTimeMillis();
        time &= 0xFFF;
        newTransactionID = (int) time;
    }

    /**
     * Contains the unique TID for a DSMCC message from a particular client.
     * 
     */
    public TransactionID() {

    }

    /**
     * Constructor with initialization for parsing. Called by DSMCCMessage parse and setTransactinID
     * 
     * @param id
     */
    public TransactionID(int id) {
        transactionID = id;
    }

    /**
     * Creates a new unique TID.
     * 
     * @return The new ID.
     */
    public synchronized static int createID() {
        ++newTransactionID;

        if (newTransactionID > TRANSACTION_ID_MASK) {
            newTransactionID = 0;
        }

        return newTransactionID;
    }

    /**
     * Returns the TID for this message. Called by DSMCCMessage DSMCCMessenger
     * 
     * @return The TID.
     */
    public int getID() {
        return transactionID;
    }

    /**
     * toString.
     * 
     * @return A human readable version of this object.
     */
    public String toString() {
        return "TransactionID = " + transactionID + "\n";
    }

    /**
     * 
     * Serializes the TID and returns the four byte representation of the TID. Called by
     * UNClientSesssionSetupRequest.
     * 
     * @return 4 byte array containing the TID.
     */
    public byte[] toByteArray() {
        byte[] id = new byte[4];
        int tid = transactionID;

        for (int i = 0; i < 4; i++) {
            id[3 - i] = (byte) (tid & 0xFF);
            tid >>= 8;
        }

        return id;
    }

}

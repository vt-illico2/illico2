package com.videotron.tvi.illico.vod.controller.session.dsmcc;

/**
 * NetworkToClientMessage.
 */
public interface NetworkToClientMessage {
    public void parse(byte[] in);
}

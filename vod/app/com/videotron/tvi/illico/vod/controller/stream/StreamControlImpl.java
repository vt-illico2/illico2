package com.videotron.tvi.illico.vod.controller.stream;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamControlEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamEndOfStreamEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamFastForwardEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamPauseEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamPlayEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamResetEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamRewindEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamStatusEvent;

class StreamControlImpl implements IVodStreamControl, TVTimerWentOffListener {
	
	long lastRequest = 0L;
	long delay = 700L;

    StreamControlImpl(VodHandlerImpl handler, java.net.InetAddress addr, int port, int streamD) throws IOException {
        m_handler = handler;
        m_transactionTimer = TVTimer.getTimer();
        m_transactionMap = new Hashtable();
        m_lscpSocket = new LscpSocket(addr, port, this);

        m_streamHandle = streamD;
        m_listeners = new Vector();
    }

    /**
     * Returns the <code>IVodHandler</code> object.
     *
     * @return the <code>IVodeHandler</code>
     */
    public IVodHandler getVodHandler() {
        return m_handler;
    }

    /**
     * Returns the string handle as seen identified by the media server.
     *
     * @return an integer value representing the stream handle as seen by the media server.
     */
    public int getStreamHandle() {
        return m_streamHandle;
    }

    /**
     * Sets the maximum time to wait for a completion event.
     *
     * @param timeout
     *            the timeout in millisecond units.
     * @throws IllegalArgumentException
     */
    public void setRequestTimeout(int timeout) {
        if (timeout <= 0) {
            throw new IllegalArgumentException();
        }

        m_delayTime = timeout;
    }

    /**
     * Requests the stream to be played.
     *
     * @param startNpt
     *            The stream position to begin playing.
     * @param stopNpt
     *            The stream position time to stop.
     * @param scaleNumerator
     *            Numerator portion of the desired stream rate.
     * @param scaleDenominator
     *            Denominator portion of the desired stream rate.
     * @param act
     *            asynchronous completion token.
     * @throws VodStrmInvdScaleException
     * @throws VodStrmInvaldNptException
     */
    synchronized public void play(int startNpt, int stopNpt, short scaleNumerator, short scaleDenominator, Object act)
            throws VodStrmInvdScaleException, VodStrmInvaldNptException {
        // validate scale
        if (scaleDenominator == 0) {
            throw new VodStrmInvdScaleException();
        }
        // validate Npts
        if (startNpt != 0x80000000 && startNpt < 0) {
            throw new VodStrmInvaldNptException();
        }
        if (stopNpt != 0x80000000 && stopNpt < 0) {
            throw new VodStrmInvaldNptException();
        }
        
//        long cur = System.currentTimeMillis();
//        long gap = cur - lastRequest;
//        if (gap < delay) {
//        	Log.printDebug("StreamControl.play() needs delay - " + (delay - gap));
//        	try {
//				Thread.sleep(delay - gap);
//			} catch (InterruptedException e) {}
//        }

        // map this request to a transactionID
        LscTransaction trans = mapTransaction(act, m_delayTime, REQUEST_PLAY);

        // perform the transaction
        // if we are out of transactionIDs, we've got problems.
        // todo: provide notification of failure.
        if (trans != null) {
            m_lscpSocket.sendPlayRequest(trans.m_transactionID, m_streamHandle, startNpt, stopNpt, scaleNumerator,
                    scaleDenominator);
//            lastRequest = cur;
        } else {
            Log.printDebug(" StreamControl Play Error! null transaction!\n");
        }
    }

    /**
     * Requests the stream to be stopped.
     *
     * @param stopNpt
     *            The desired stream position time to pause.
     * @param act
     *            asynchronous completion token.
     * @throws VodStrmInvaldNptException
     */
    synchronized public void pause(int stopNpt, Object act) throws VodStrmInvaldNptException {
        if (stopNpt != 0x80000000 && stopNpt < 0) {
            throw new VodStrmInvaldNptException();
        }
        
//        long cur = System.currentTimeMillis();
//        long gap = cur - lastRequest;
//        if (gap < delay) {
//        	Log.printDebug("StreamControl.pause() needs delay - " + (delay - gap));
//        	try {
//				Thread.sleep(delay - gap);
//			} catch (InterruptedException e) {}
//        }
        
        // map this request to a transactionID
        LscTransaction trans = mapTransaction(act, m_delayTime, REQUEST_PAUSE);

        // perform the transaction
        // if we are out of transactionIDs, we've got problems.
        // todo: provide notification of failure.
        if (trans != null) {
            m_lscpSocket.sendPauseRequest(trans.m_transactionID, m_streamHandle, stopNpt);
//            lastRequest = cur;
        }
    }

    /**
     * Requests the stream to be played in fastforward mode.
     *
     * @param startNpt
     *            Stream start position.
     * @param stopNpt
     *            Stream stop position.
     * @param scaleNumerator
     *            Numerator portion of the desired stream rate.
     * @param scaleDenominator
     *            Denominator portion of the desired stream rate.
     * @param act
     *            asynchronous completion token.
     * @throws VodStrmInvaldNptException
     * @throws VodStrmInvdScaleException
     */
    synchronized public void fastForward(int startNpt, int stopNpt, short scaleNumerator, short scaleDenominator, Object act)
            throws VodStrmInvaldNptException, VodStrmInvdScaleException {
        // validate scale
        if (scaleDenominator == 0) {
            throw new VodStrmInvdScaleException();
        }
        // validate Npts
        if (startNpt != 0x80000000 && startNpt < 0) {
            throw new VodStrmInvaldNptException();
        }
        if (stopNpt != 0x80000000 && stopNpt < 0) {
            throw new VodStrmInvaldNptException();
        }
        
//        long cur = System.currentTimeMillis();
//        long gap = cur - lastRequest;
//        if (gap < delay) {
//        	Log.printDebug("StreamControl.fastForward() needs delay - " + (delay - gap));
//        	try {
//				Thread.sleep(delay - gap);
//			} catch (InterruptedException e) {}
//        }
        
        // map this request to a transactionID
        LscTransaction trans = mapTransaction(act, m_delayTime, REQUEST_FASTFORWARD);

        // perform the transaction
        // if we are out of transactionIDs, we've got problems.
        // todo: provide notification of failure.
        if (trans != null) {
            m_lscpSocket.sendPlayRequest(trans.m_transactionID, m_streamHandle, startNpt, stopNpt, scaleNumerator,
                    scaleDenominator);
//            lastRequest = cur;
        }
    }

    /**
     * Causes the stream to play back at the specified rate. The rate is specified by the numerator and denominator.
     *
     * @param startNpt
     *            Stream start position.
     * @param stopNpt
     *            Stream stop position.
     * @param scaleNumerator
     *            Numerator portion of the desired stream rate.
     * @param scaleDenominator
     *            Denominator portion of the desired stream rate.
     * @param act
     *            asynchronous completion token.
     *
     * @throws VodStrmInvaldNptException
     * @throws VodStrmInvdScaleException
     */
    synchronized  public void rewind(int startNpt, int stopNpt, short scaleNumerator, short scaleDenominator, Object act)
            throws VodStrmInvaldNptException, VodStrmInvdScaleException {
        // validate scale
        if (scaleDenominator == 0) {
            throw new VodStrmInvdScaleException();
        }
        // validate Npts
        if (startNpt != 0x80000000 && startNpt < 0) {
            throw new VodStrmInvaldNptException();
        }
        if (stopNpt != 0x80000000 && stopNpt < 0) {
            throw new VodStrmInvaldNptException();
        }
        
//        long cur = System.currentTimeMillis();
//        long gap = cur - lastRequest;
//        if (gap < delay) {
//        	Log.printDebug("StreamControl.rewind() needs delay - " + (delay - gap));
//        	try {
//				Thread.sleep(delay - gap);
//			} catch (InterruptedException e) {}
//        }
        
        // map this request to a transactionID
        LscTransaction trans = mapTransaction(act, m_delayTime, REQUEST_REWIND);

        // perform the transaction
        // if we are out of transactionIDs, we've got problems.
        // todo: provide notification of failure.
        if (trans != null) {
            m_lscpSocket.sendPlayRequest(trans.m_transactionID, m_streamHandle, startNpt, stopNpt, scaleNumerator,
                    scaleDenominator);
//            lastRequest = cur;
        }
    }

    /**
     * Resets the stream to it's initial state.
     *
     */
    synchronized public void reset(Object act) {
//    	long cur = System.currentTimeMillis();
//        long gap = cur - lastRequest;
//        if (gap < delay) {
//        	Log.printDebug("StreamControl.reset() needs delay - " + (delay - gap));
//        	try {
//				Thread.sleep(delay - gap);
//			} catch (InterruptedException e) {}
//        }
        
        // map this request to a transactionID
        LscTransaction trans = mapTransaction(act, m_delayTime, REQUEST_RESET);

        // perform the transaction
        // if we are out of transactionIDs, we've got problems.
        // todo: provide notification of failure.
        if (trans != null) {
            m_lscpSocket.sendResetRequest(trans.m_transactionID, m_streamHandle);
//            lastRequest = cur;
        }
    }

    /**
     * <code>queryStatus</code> Obtains the latest status information of this stream. This method completes
     * asynchronously indicated by event <code>VodStreamStatusEvent</code>.
     *
     * @param act
     *            The asynchronous completion token.
     *
     * @see VodStreamStatusEvent
     * @see VodStreamControlEvent#getStreamNpt()
     * @see VodStreamControlEvent#getStreamScaleNum()
     * @see VodStreamControlEvent#getStreamScaleDenom()
     * @see VodStreamControlEvent#getStreamState()
     */
    synchronized public void queryStatus(Object act) {
//    	long cur = System.currentTimeMillis();
//        long gap = cur - lastRequest;
//        if (gap < delay) {
//        	Log.printDebug("StreamControl.queryStatus() needs delay - don't query");
//        	return;
////        	try {
////				Thread.sleep(delay - gap);
////			} catch (InterruptedException e) {}
//        }
        
        // map this request to a transactionID
        LscTransaction trans = mapTransaction(act, m_delayTime, REQUEST_STATUS);

        // perform the transaction
        // if we are out of transactionIDs, we've got problems.
        if (trans != null) {
            m_lscpSocket.sendStatusRequest(trans.m_transactionID, m_streamHandle);
//            lastRequest = cur;
        }
    }

    /**
     * Release all connection resources associated with this stream control device.
     *
     */
    public void disconnect() {
    	if (m_lscpSocket != null) {
    		m_lscpSocket.disconnect();
    	}
    }

    /**
     * Subscribes a listener to receive events related to this stream control object. If the specified listener is
     * currently subscribed, no action is performed.
     *
     * @param listener
     *            - The listener of stream events to subscribe.
     * @throws NullPointerException
     */
    public void addListener(IVodStreamControlListener listener) {
        if (!m_listeners.contains(listener)) {
            m_listeners.addElement(listener);
        }
    }

    /**
     * Unsubscribes a listener from receiving events related to this stream control object. If the specified listener is
     * not currently subscribed no action is performed.
     *
     * @param listener
     * @throws NullPointerException
     */
    public void removeListener(IVodStreamControlListener listener) {
        m_listeners.removeElement(listener);
    }

    /**
     * Deliver event response to all stream event listeners.
     */
    private void notifyListeners(VodStreamControlEvent ev) {
        if (m_listeners != null) {
            Object listeners[];
            synchronized (m_listeners) {
                listeners = new Object[m_listeners.size()];
                m_listeners.copyInto(listeners);
            }

            for (int i = 0; i < listeners.length; i++) {
                IVodStreamControlListener listener = (IVodStreamControlListener) listeners[i];
                listener.streamControlUpdateEvent(ev);
            }
        } else {
            Log.printDebug("StreamCtrlImpl: notifyListeners - no listeners found\n");
        }

    }

    /**
     * This method is called by the LscpSocket class to indicate that an LSC response has been received.
     *
     * @param transactionId
     * @param opCode
     * @param status
     * @param stream
     * @param currentNpt
     * @param scaleNum
     * @param scaleDen
     * @param serverMode
     */
    void notifyLscpResponse(int transactionId, int opCode, int status, int stream, int currentNpt, short scaleNum,
            short scaleDen, int serverMode) {
        int serverState = mapServerMode(serverMode);
        int serverStatus = status;//mapServerStatus(status);

        // retrieve pending transaction associated with this id
        LscTransaction transaction = (LscTransaction) m_transactionMap
                .remove(new Integer((transactionId & 0x000000ff)));

        // if no transaction is mapped, this transaction must have timed out and
        // been removed. do nothing.
        if (transaction == null && opCode != LscpSocket.LSC_DONE) {
            Log.printDebug("StreamCtrlImpl notifyLscpResponse: transaction not mapped.\n");
            return;
        }

        // if this is an unsolicited end of stream indication
        if (opCode == LscpSocket.LSC_DONE) {
            notifyListeners(new VodStreamEndOfStreamEvent(this, null, m_streamHandle, serverState, serverStatus,
                    currentNpt, scaleNum, scaleDen));
            return;
        }

        // if there is a request/response pair, signal event as appropriate
        switch (transaction.m_request) {
        case REQUEST_PAUSE:
            notifyListeners(new VodStreamPauseEvent(this, transaction.m_act, m_streamHandle, serverState, serverStatus,
                    currentNpt, scaleNum, scaleDen));
            break;
        case REQUEST_PLAY:
            notifyListeners(new VodStreamPlayEvent(this, transaction.m_act, m_streamHandle, serverState, serverStatus,
                    currentNpt, scaleNum, scaleDen));
            break;
        case REQUEST_STATUS:
            notifyListeners(new VodStreamStatusEvent(this, transaction.m_act, m_streamHandle, serverState,
                    serverStatus, currentNpt, scaleNum, scaleDen));
            break;
        case REQUEST_RESET:
            notifyListeners(new VodStreamResetEvent(this, transaction.m_act, m_streamHandle, serverState, serverStatus,
                    currentNpt, scaleNum, scaleDen));
            break;
        case REQUEST_REWIND:
            notifyListeners(new VodStreamRewindEvent(this, transaction.m_act, m_streamHandle, serverState,
                    serverStatus, currentNpt, scaleNum, scaleDen));
            break;
        case REQUEST_FASTFORWARD:
            notifyListeners(new VodStreamFastForwardEvent(this, transaction.m_act, m_streamHandle, serverState,
                    serverStatus, currentNpt, scaleNum, scaleDen));
            break;
        default:
            break;
        }
    }

    /**
     * Map an LSCP status codes to our external status codes.
     */
    int mapServerStatus(int status) {
        int ret = STATUS_UNKNOWN_ERROR;

        switch (status) {

        case LSC_OK:
            ret = STATUS_OK;
            break;
        case LSC_BAD_REQUEST:
            ret = STATUS_BAD_REQUEST;
            break;
        case LSC_BAD_STREAM:
            ret = STATUS_BAD_STREAM;
            break;
        case LSC_WRONG_STATE:
            ret = STATUS_WRONG_STATE;
            break;
        case LSC_NO_PERMISSION:
            ret = STATUS_PERMISSION_DENIED;
            break;
        case LSC_BAD_PARAM:
            ret = STATUS_BAD_PARAMETER;
            break;
        case LSC_NO_IMPLEMENT:
            ret = STATUS_UNKNOWN_ERROR;
            break;
        case LSC_NO_MEMORY:
            ret = STATUS_NO_MEMORY;
            break;
        case LSC_IMP_LIMIT:
            ret = STATUS_IMP_LIMIT;
            break;
        case LSC_TRANSIENT:
            ret = STATUS_TRANSIENT_ERROR;
            break;
        case LSC_NO_RESOURCES:
            ret = STATUS_NO_RESOURCES;
            break;
        case LSC_SERVER_ERROR:
            ret = STATUS_SERVER_ERROR;
            break;
        case LSC_SERVER_FAILURE:
            ret = STATUS_SERVER_FAILURE;
            break;
        case LSC_BAD_SCALE:
            ret = STATUS_BAD_SCALE;
            break;
        case LSC_BAD_START:
            ret = STATUS_BAD_START;
            break;
        case LSC_BAD_STOP:
            ret = STATUS_BAD_STOP;
            break;
        case LSC_MPEG_DELIVERY:
            ret = STATUS_MPEG_DELIVERY;
            break;
        default:
            break;
        }

        return ret;
    }

    /**
     * Map an LSCP server mode state to our external server state.
     */
    int mapServerMode(int mode) {
        int ret = STREAM_STATE_UNDEFINED;

        switch (mode) {
        case LSC_O_MODE:
            ret = STREAM_STATE_INIT;
            break;
        case LSC_P_MODE:
            ret = STREAM_STATE_PAUSED;
            break;
        case LSC_ST_MODE:
            ret = STREAM_STATE_PLAYING;
            break;
        case LSC_T_MODE:
            ret = STREAM_STATE_PLAYING;
            break;
        case LSC_TP_MODE:
            ret = STREAM_STATE_PLAYING;
            break;
        case LSC_STP_MODE:
            ret = STREAM_STATE_PLAYING;
            break;
        case LSC_PST_MODE:
            ret = STREAM_STATE_PAUSED;
            break;
        case LSC_MODE_EOS:
            ret = STREAM_STATE_END;
            break;
        default:
            break;
        }

        return ret;
    }

    /**
     * Find an unused transactionID and create an LscTransaction into the trans map and timer.
     */
    private LscTransaction mapTransaction(Object act, long timeout, int request) {
        synchronized (m_transactionMap) {
            // Search for an unused transactionID
            int initTransID = m_curTransactionID;
            m_curTransactionID = ++m_curTransactionID & 0x000000ff;
            while (initTransID != m_curTransactionID) // have we examined all
            // 256 values?
            {
                Integer tid = new Integer(m_curTransactionID);
                if (!m_transactionMap.containsKey(tid)) {
                    // We've found an empty transaction id. Create the trans
                    // object,
                    // and schedule it in the timer
                    LscTransaction transaction = new LscTransaction(m_curTransactionID, act, timeout, request);
                    m_transactionMap.put(tid, transaction);
                    try {
                    	transaction.addTVTimerWentOffListener(this);
                        m_transactionTimer.scheduleTimerSpec(transaction);
                        Log.printDebug("LSC Transaction scheduled, tid = " + tid + ", request = " + request);
                    } catch (TVTimerScheduleFailedException e) {
                        // A failed schedule will result in no timeout
                        // notification!
                        Log.printDebug("LSC Transaction timer Exception caught!");
                        e.printStackTrace();
                    }
                    return transaction;
                }

                m_curTransactionID = ++m_curTransactionID & 0x000000ff;
            }
        }

        // Unable to find unused transaction ID! should result in a
        // STATUS_IMP_LIMIT event being delivered for this request?
        Log.printDebug("Unable to map LscTransaction\n");
        return null;
    }

    /**
     * Process the TV Timer events associated with LSC transactions Remove the associated transaction from the map.
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
        LscTransaction transaction = (LscTransaction) event.getTimerSpec();
        transaction.removeTVTimerWentOffListener(this);
        m_transactionTimer.deschedule(transaction);

        Object obj = m_transactionMap.remove(new Integer(transaction.m_transactionID));
        if (obj != null) { // it doen't response in duration
        	Log.printDebug("LSC Transaction is time out, tid = " + transaction.m_transactionID + ", request = " + transaction.m_request);
        }
    }

    /**
     * The <code>LscTransaction</code> class is used to track outstanding LSC request. By extending the TVTimerSpec, we
     * can handle user defined timeouts. Additionally, the class contains the transactionID and act for this request
     */
    private class LscTransaction extends TVTimerSpec {
        LscTransaction(int transactionID, Object act, long delay, int request) {
            m_transactionID = transactionID;
            m_act = act;
            m_request = request;
            setDelayTime(delay);
        }

        // Transaction ID sent to server for this transaction
        int m_transactionID;

        // type of request for this transaction
        int m_request;

        // Async completion token for this transaction
        Object m_act;
    }

    /**
     * According to the LSCP spec, LSCP suggests that a lossless protocol should be used for communicating messages to
     * the stream server. However, it recoginizes that messages may still be lost, so it includes an 8 bit transaction
     * ID as part of it's messaging header. 256 values may not be enough, so lets use a full int, and track internally
     */
    private int m_curTransactionID = 1;

    /**
     * This hash contains all outstanding LSC requests, mapped by transactionID.
     */
    private Hashtable m_transactionMap = null;

    /**
     * The TVTimer is used to handle all LSC timeout conditions.
     */
    private TVTimer m_transactionTimer = null;

    /**
     * Transaction timeout value - default to arbitrary 10 seconds.
     */
    private long m_delayTime = 10000;

    /**
     * The VOD handler which instantiated this stream control.
     */
    private IVodHandler m_handler = null;

    /**
     * Separate class for handling all LSC communication to the server.
     */
    private LscpSocket m_lscpSocket = null;

    /**
     * contains all VOD Stream Control listeners.
     */
    private Vector m_listeners;

    /**
     * Handle to the stream associated with this control.
     */
    private int m_streamHandle;

    // private event codes - stored in transactions to facilitate event mapping
    private static final int REQUEST_FASTFORWARD = 1;

    private static final int REQUEST_REWIND = 2;

    private static final int REQUEST_STATUS = 3;

    private static final int REQUEST_PLAY = 4;

    private static final int REQUEST_PAUSE = 5;

    private static final int REQUEST_RESET = 6;

    // private LSCP server mode codes see: LSC10.pdf
    private static final int LSC_O_MODE = 0;

    private static final int LSC_P_MODE = 1;

    private static final int LSC_ST_MODE = 2;

    private static final int LSC_T_MODE = 3;

    private static final int LSC_TP_MODE = 4;

    private static final int LSC_STP_MODE = 5;

    private static final int LSC_PST_MODE = 6;

    private static final int LSC_MODE_EOS = 7;

    // private LSCP status codes
    private static final int LSC_OK = 0;

    private static final int LSC_BAD_REQUEST = 0x10;

    private static final int LSC_BAD_STREAM = 0x11;

    private static final int LSC_WRONG_STATE = 0x12;

    private static final int LSC_UNKNOWN = 0x13;

    private static final int LSC_NO_PERMISSION = 0x14;

    private static final int LSC_BAD_PARAM = 0x15;

    private static final int LSC_NO_IMPLEMENT = 0x16;

    private static final int LSC_NO_MEMORY = 0x17;

    private static final int LSC_IMP_LIMIT = 0x18;

    private static final int LSC_TRANSIENT = 0x19;

    private static final int LSC_NO_RESOURCES = 0x1a;

    private static final int LSC_SERVER_ERROR = 0x20;

    private static final int LSC_SERVER_FAILURE = 0x21;

    private static final int LSC_BAD_SCALE = 0x30;

    private static final int LSC_BAD_START = 0x31;

    private static final int LSC_BAD_STOP = 0x32;

    private static final int LSC_MPEG_DELIVERY = 0x40;

}

package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ISessionHandleType;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.SessionDescriptor;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.Playout;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;

public class UserPrivateData {

    private Pegasus23Writer m_out;
    private String m_serviceGW;
    private String m_serviceName;

    private Vector m_setupReqDescriptors;
    private Vector m_setupCompleteDescriptors;
    private Vector m_releaseReqDescriptors;
    private Vector m_relCompleteDescriptors;
    private Vector m_notificationDescriptors;

    public static final int ASSET_ID_TAG = 0x01;
    public static final int NODEGROUP_ID_TAG = 0x02;
    public static final int IP_TAG = 0x03;
    public static final int STREAM_HANDLE_TAG = 0x04;
    public static final int COPY_PROTECTION_TAG = 0x91;

    /**
     * Used in simulator.
     */
    private byte[] serviceGroupData = { 0, 0, 0, 0, 0, 1 };

    public byte[] setSetUpUserData(String sspId, byte[] sgData) {
    	Log.printDebug("UserPrivateData, sspId = " + sspId);
        byte[] setupPrivateData = null;

        try {
            if (sspId != null) {
                String assetName = sspId;
                addDescriptor(ISessionHandleType.SETUP_REQUEST_DESCRIPTOR_TYPE, ASSET_ID_TAG, assetName.length(), 0,
                        assetName.getBytes());
            }

            byte encrypted[] = new byte[2];
            Video video = (Video) CatalogueDatabase.getInstance().getCachedBySspId(sspId);
            if (video != null) {
            	Playout playout = video.getPlayoutByName(sspId);
            	if (playout != null) {
            		if (playout.isEncrypted) {
            			encrypted[0] = (byte) ((1) & 0xFF);
            		}
            		String bits = "0x00";
            		switch (playout.copyProtectionType) {
            		case 2: // copy freely
            			bits = DataCenter.getInstance().getString("CCI_BITS_COPY_FREELY");
            			break;
            		case 1: // copy once
            			bits = DataCenter.getInstance().getString("CCI_BITS_COPY_ONCE");
            			break;
            		case 0: // copy never
            			bits = DataCenter.getInstance().getString("CCI_BITS_COPY_NEVER");
            			break;
            		}
            		if (bits.length() == 4) {
            			encrypted[1] = Byte.parseByte(bits.substring(2), 16);	
            		} else if (bits.length() == 2) {
            			encrypted[1] = Byte.parseByte(bits, 16);	
            		}
            		Log.printDebug("UserPrivateData" +
            				", " + video.toString(true) +
            				", isEnc = " + playout.isEncrypted +
            				", copyProtect = " + playout.copyProtectionType +
            				", cci = 0x" + Integer.toHexString(encrypted[1]));
            	}
            } else {
            	Log.printDebug("UserPrivateData can't found video element");
            	encrypted[0] = (byte) ((0) & 0xFF);
            	encrypted[1] = (byte) ((0) & 0xFF);
            }
            Log.printDebug("UserPrivateData, encrypted = " + StringUtil.byteArrayToHexString(encrypted));

            addDescriptor(ISessionHandleType.SETUP_REQUEST_DESCRIPTOR_TYPE, COPY_PROTECTION_TAG, // tag - Copy protection
                    2, // data length
                    0, // data offset
                    encrypted); // data bytes

            serviceGroupData = sgData;
            addDescriptor(ISessionHandleType.SETUP_REQUEST_DESCRIPTOR_TYPE, NODEGROUP_ID_TAG, // tag - NodeGropID = 2
                    // according to
                    // DN-DESC-SP-2.03
                    serviceGroupData.length, // data length
                    0, // data offset
                    serviceGroupData); // data bytes

            setupPrivateData = this.buildPrivateData(m_setupReqDescriptors);

        } catch (Exception e) {
            Log.printDebug(e.getMessage());
        }
        return setupPrivateData;
    }

    /**
     *
     * setServers
     *
     * Sets server parameters needed to establish the connection.
     * @param serviceGW
     *            The service gateway name.
     * @param serviceName
     *            The service name.
     *
     */
    public void setServers(String serviceGW, String serviceName, int appServer) {
        m_serviceGW = serviceGW;
        if (serviceName != null) {
            m_serviceName = serviceName;
        }
    }

    /**
     * Sends a request to the Head-end to establish a session. Event
     * SessionSetupSucceededEvent is sent to the ISessionHandlerListener object to when this
     * call completes. If the call fails SessionSetupFailed or SessionSetupTimeoutEvent is
     * sent to the registered listeners.
     *
     *
     * @see SessSetupSucceededEvent
     * @see SessionSetupFailedEvent
     * @see SessionSetupTimeOutEvent
     */
    public void setup() {

    }

    public int addDescriptor(int type, int tag, int length, int offset, byte[] src) {
        int i = 0;
        if ((length + offset) > src.length) {
            // Validate length + offset doesn't result in a boundary error.
            Log.printDebug("ISH:addDescriptor - array boundary error");
            return 0;
        }

        byte[] newDestArray = new byte[length];

        System.arraycopy(src, offset, newDestArray, 0, length);

        SessionDescriptor descriptor = new SessionDescriptor(tag, length, newDestArray);
        switch (type) {

        case ISessionHandleType.SETUP_REQUEST_DESCRIPTOR_TYPE:
            try {
                if (m_setupReqDescriptors == null) {
                    m_setupReqDescriptors = new Vector();
                }
                m_setupReqDescriptors.addElement(descriptor);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            break;
        case ISessionHandleType.RELEASE_REQUEST_DESCRIPTOR_TYPE:
            try {
                if (m_releaseReqDescriptors == null) {
                    m_releaseReqDescriptors = new Vector();
                }
                m_releaseReqDescriptors.addElement(descriptor);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            break;
        default:
            Log.printDebug("UserPrivateData.addDescriptor: Invalid type");
        }
        return 0;
    }

    public SessionDescriptor[] getDescriptors(int type) {
        // For the descriptor type, create an array equal to the size of
        // the vector, store a reference of each element of vector into
        // the array.
        Vector localRef = null;
        switch (type) {
        case ISessionHandleType.SETUP_COMPLETE_DESCRIPTOR_TYPE:
            localRef = m_setupCompleteDescriptors;
            break;
        case ISessionHandleType.SETUP_REQUEST_DESCRIPTOR_TYPE:
            localRef = m_setupReqDescriptors;
            break;
        case ISessionHandleType.RELEASE_COMPLETE_DESCRIPTOR_TYPE:
            localRef = m_relCompleteDescriptors;
            break;
        case ISessionHandleType.RELEASE_NOTIFICATION_DESCRIPTOR_TYPE:
            localRef = m_notificationDescriptors;
            break;
        case ISessionHandleType.RELEASE_REQUEST_DESCRIPTOR_TYPE:
            localRef = m_releaseReqDescriptors;
            break;
        default:
            throw new IllegalArgumentException();
        }

        int len = localRef.size();
        SessionDescriptor[] result = new SessionDescriptor[len];

        for (int elemNdx = 0; elemNdx < len; elemNdx++) {
            result[elemNdx] = (SessionDescriptor) localRef.elementAt(elemNdx);
        }
        return result;
    }

    /**
     * buildPrivateData
     *
     * This method formats request descriptors into a byte array.
     *
     * Memory layout of user private data
     *
     * ProtocolID ......................... uint8 Version ........................... uint8 ServiceGateway
     * ................... char[16] ServiceGatewayDataLength ........... uint32 for (ServiceGatewayDataLength) { Service
     * ............... char[16] ServiceDataLength ............... uint32 for(ServiceDataLength){ ServiceData } }
     *
     *
     *
     *
     * --------------------------------------- ServiceData{ descriptorCount.................... uint8 List of Descriptor
     * } --------------------------------------- Descriptor{ tag................................ uint8 length
     * ............................ uint8 data }
     *
     */
    private byte[] buildPrivateData(Vector vectPrivateDescs) {
        int totalPrivateDataSize;

        int ServiceGatewayDataLength = 0;
        int ServiceDataLength = 0;
        int protocolHeaderLength = 0;
        int rem;
        byte[] ba = null;

        /**
         * ServiceData Length in bytes ServiceData{ descriptorCount................ uint8 List of Descriptor }
         */
        ServiceDataLength = 1 + getSizeOfAllDescriptors(vectPrivateDescs);
        Log.printDebug("=========================");
        Log.printDebug("buildPrivateData ServiceDataLength : " + ServiceDataLength);
        Log.printDebug("=========================");

        /**
         * ProtocolHeader Length in bytes
         *
         * ProtocolID ....................... uint8 Version ....................... uint8 ServiceGateway
         * ................... char[16] ServiceGatewayDataLength ......... uint32
         */
        protocolHeaderLength = 22;

        /**
         * ServiceGatewayDataLength = Service + memsize of ServiceDataLength + ServiceDataLength
         *
         */
        ServiceGatewayDataLength = 16 + 4 + ServiceDataLength;

        /**
         * Rounding to a 4 Byte boundary. reference: ssp23.pdf
         */
        totalPrivateDataSize = protocolHeaderLength + ServiceGatewayDataLength;
        rem = totalPrivateDataSize % 4;
        if (rem > 0) {
            totalPrivateDataSize = totalPrivateDataSize + 4 - rem;
        }

        // System.out.println("ISH:buildPrivateData- Calculated UserPrivateData size is = " + totalPrivateDataSize);
        // Allocate memory
        try {
            m_out = new Pegasus23Writer(totalPrivateDataSize);

            if (Resources.sspVersion == Resources.SSP23) {
                m_out.write_ProtocolId(Pegasus23Writer.PROTOCOLID);
            } else {
                m_out.write_ProtocolId(1);
//                m_out.write_ProtocolId(2);
            }
            m_out.write_Version(Pegasus23Writer.VERSION);

            if (Resources.sspVersion == Resources.SSP23) {
                m_out.write_ServiceGateway(m_serviceGW);
                m_out.write_ServiceDataLength(ServiceGatewayDataLength);
                m_out.write_Service(m_serviceName);
                m_out.write_ServiceDataLength(ServiceDataLength);
            }
            m_out.write_ServiceData(vectPrivateDescs);

            ba = m_out.getByteArray();
        } catch (Exception ex) {
            Log.printDebug("\nISH::buildPrivateData Exception.");
            ex.printStackTrace();
        }

        return ba;

    }

    private int getSizeOfAllDescriptors(Vector vectPrivateDescs) {
        int total = 0;
        int dcount = 0;
        SessionDescriptor sd = null;

        // For each descriptor account for the tag, length and length of data.
        dcount = vectPrivateDescs.size();

        // now add the length of the data field of each
        for (int ndx = 0; ndx < dcount; ndx++) {

            sd = (SessionDescriptor) vectPrivateDescs.elementAt(ndx);
            total += (2 + sd.getLength());
        }

        // System.out.println("ISH::getSizeofAllDescriptors decimal size = " + total);

        return total;
    }

    public class Pegasus23Writer {
        static final int PROTOCOLID = 2;
        static final int VERSION = 1;

        private ByteArrayOutputStream m_baOS = null;
        private DataOutputStream m_dOS = null;

        /**
	    *
	    */
        public Pegasus23Writer(int size) {
            // TODO Auto-generated constructor stub
            m_baOS = new ByteArrayOutputStream(size);

            m_dOS = new DataOutputStream(m_baOS);

            if (Log.DEBUG_ON) {
                Log.printDebug("Pegasus23Writer created byteArrayOutputStream of " + size + " bytes");
            }
        }

        public void write_ProtocolId(int protocolId) {
            try {
                m_dOS.writeByte(protocolId);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public void write_Version(int version) {
            try {
                m_dOS.writeByte(version);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public void write_ServiceGateway(String sgw) {
            try {
                byte[] ba;
                int padding = 0;
                int len = 0;
                int i = 0;
                ba = sgw.getBytes();
                len = ba.length;
                if (Log.DEBUG_ON) {
                    Log.printDebug("Pegasus23Writer.write_ServiceGateway [String sgw.byteArray] returned "
                            + len + " bytes , " + sgw);
                }
                if (ba.length < 16) {
                    padding = 16 - ba.length;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("Pegasus23Writer.write_ServiceGateway padding size = " + padding);
                    }
                }
                // write the byteArray and padding if any.
                m_dOS.write(ba, 0, len);
                if (padding > 0) {
                    for (i = 0; i < padding; i++) {
                        m_dOS.writeByte(0x00);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        public void write_ServiceGatewayDataLength(int length) {
            try {
                m_dOS.writeInt(length);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public void write_Service(String service) {
            try {
                byte[] ba;
                int padding = 0;
                int len = 0;
                int i = 0;
                ba = service.getBytes();
                len = ba.length;
                if (ba.length < 16) {
                    padding = 16 - ba.length;
                }
                m_dOS.write(ba, 0, len);
                if (padding > 0) {
                    for (i = 0; i < padding; i++) {
                        m_dOS.writeByte(0x00);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public void write_ServiceDataLength(int length) {
            try {
                m_dOS.writeInt(length);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public void write_ServiceData(Vector sda) {
            SessionDescriptor cd;
            int tag;
            int length;
            byte[] ba;
            int num = 0;

            try {
                num = sda.size();
                // write descriptorCount
                m_dOS.writeByte(num);

                // write each descriptor
                // write in reverse order to handle appearent mystro descriptor order bug
                for (int ndx = num - 1; ndx >= 0; ndx--) {
                    cd = (SessionDescriptor) sda.elementAt(ndx);
                    tag = cd.getTag();
                    length = cd.getLength();
                    ba = cd.getData();
                    m_dOS.writeByte(tag);
                    m_dOS.writeByte(length);
                    m_dOS.write(ba);

                    Log.printDebug("write_ServiceData, tag = " + tag + ", len = " + length + ", data = " + StringUtil.byteArrayToHexString(ba));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public byte[] getByteArray() {
            int i = 0;
            byte[] ba = m_baOS.toByteArray();
            return ba;
        }
    }
}

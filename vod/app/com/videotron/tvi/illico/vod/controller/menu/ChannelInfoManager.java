/**
 *
 */
package com.videotron.tvi.illico.vod.controller.menu;

import java.io.File;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.text.Collator;
import java.util.*;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.data.config.FreePreviews;
import com.videotron.tvi.illico.vod.data.vcds.type.*;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

/**
 * @author zestyman
 */
public class ChannelInfoManager implements DataUpdateListener {

    public static final String FREE_PREVIEWS = "FREE_PREVIEWS";

    private static ChannelInfoManager instance;

    private String[] blockedChannels;
    private String[] favoriteChannels;
    private String paramFavoriteChannels;
    private String paramSubscribedChannels;


    private CategoryContainer[] favoriteCategoryContainers;
    private CategoryContainer[] allChannelCategoryContainers;
    private CategoryContainer[] subscribedCategoryContainers;

    private CategoryContainer favoriteTabContainer;
    private CategoryContainer allChannelTabContainer;
    private CategoryContainer subscribedTabContainer;

    public HashSet freePreviews = new HashSet();

    private Collator collator = Collator.getInstance();

    public static boolean needToUpdate = false;

    private Vector tempUnblockChannelVec = new Vector();

    // R7.3
    private CategoryContainer treeRoot;
    private Favourites favorites;
    private Subscribed subscribed;
    private Unsubscribed unsubscribed;
    private CategoryContainer unsubscribedTabContainer;
    // VDTRMASTER-6091
    private CategoryContainer myPackageTabContainer;
    private int favoritesCount;
    private int subscribedChannelsOnlyCount;
    private int subscribedChannelsCount;
    private int unsubscribedChannelCount;
    private int unsubscribedTabIndex;

    private ChannelInfoManager() {
    }

    public static ChannelInfoManager getInstance() {
        if (instance == null) {
            instance = new ChannelInfoManager();
            instance.init();
        }

        return instance;
    }

    private void init() {
        // R7.3
        unsubscribedTabContainer = new CategoryContainer();
        unsubscribedTabContainer.type = MenuController.MENU_UNSUBSCRIBED_CHANNELS;

        // VDTRMASTER-6091
        myPackageTabContainer = new CategoryContainer();
        myPackageTabContainer.type = MenuController.MENU_SUBSCRIBED_CHANNELS;

        DataCenter.getInstance().addDataUpdateListener(FREE_PREVIEWS, this); // free
        // preview
        // channels
    }

    public void flushData() {
        for (int i = 0; favoriteCategoryContainers != null && i < favoriteCategoryContainers.length; i++) {
            if (favoriteCategoryContainers[i] != null)
                favoriteCategoryContainers[i].dispose();
            favoriteCategoryContainers[i] = null;
        }
        favoriteCategoryContainers = null;
        for (int i = 0; allChannelCategoryContainers != null && i < allChannelCategoryContainers.length; i++) {
            if (allChannelCategoryContainers[i] != null)
                allChannelCategoryContainers[i].dispose();
            allChannelCategoryContainers[i] = null;
        }
        allChannelCategoryContainers = null;

        tempUnblockChannelVec.clear();

        if (favorites != null) {
            for (int i = 0; favorites.categoryContainer != null && i < favorites.categoryContainer.length; i++) {
                if (favorites.categoryContainer[i] != null)
                    favorites.categoryContainer[i].dispose();
                favorites.categoryContainer[i] = null;
            }
            favorites = null;
        }
        favoritesCount = 0;

        if (subscribed != null) {
            for (int i = 0; subscribed.categoryContainer != null && i < subscribed.categoryContainer.length; i++) {
                if (subscribed.categoryContainer[i] != null)
                    subscribed.categoryContainer[i].dispose();
                subscribed.categoryContainer[i] = null;
            }
            subscribed = null;
        }
        subscribedChannelsCount = 0;

        if (unsubscribed != null) {
            for (int i = 0; unsubscribed.categoryContainer != null && i < unsubscribed.categoryContainer.length; i++) {
                if (unsubscribed.categoryContainer[i] != null)
                    unsubscribed.categoryContainer[i].dispose();
                unsubscribed.categoryContainer[i] = null;
            }
            unsubscribed = null;
        }
        unsubscribedChannelCount = 0;
        subscribedChannelsOnlyCount = 0;
    }

    public void updateBlockedChannels(String list) {
        Log.printDebug("ChannelInfoManager, updateBlockChannels, list=" + list);
        blockedChannels = TextUtil.tokenize(list, PreferenceService.PREFERENCE_DELIMETER);

        if (Resources.appStatus == VODService.APP_STARTED) {

            BaseUI curUI = (BaseUI) MenuController.getInstance().getCurrentScene();

            if (curUI instanceof CategoryUI) {
                ((CategoryUI) curUI).updateBlockChannel();
            } else {
                needToUpdate = true;
            }
            // VDTRMASTER-5507
            MenuController.getInstance().hideLoadingAnimation();
        }
    }

    public boolean isBlockedChannel(Channel ch) {
        // VDTRMASTER-5520
        String parentalControl = DataCenter.getInstance().getString(RightFilter.PARENTAL_CONTROL);
        Log.printDebug("ChannelInfoManager, isBlockChannel, ch=" + ch + ", Parental Controll=" + parentalControl);

        if (Definitions.OPTION_VALUE_ON.equals(parentalControl)) {
            if (blockedChannels != null && ch != null) {
                String callLetter = ch.callLetters;

                if (tempUnblockChannelVec.contains(callLetter)) {
                    Log.printDebug("ChannelInfoManager, included in tempUnblockChannelVec, ch=" + ch);
                    return false;
                }

                for (int i = 0; i < blockedChannels.length; i++) {
                    if (callLetter != null && callLetter.equals(blockedChannels[i])) {
                        return true;
                    }
                }
                // R7.3
                if (ch.mplexChannels != null && ch.mplexChannels.channel.length > 0) {
                    return isBlockedChannel(ch.mplexChannels.channel);
                }
            }
        }

        return false;
    }

    public boolean isBlockedChannel(Channel[] channels) {
        // VDTRMASTER-5520
        String parentalControl = DataCenter.getInstance().getString(RightFilter.PARENTAL_CONTROL);
        Log.printDebug("ChannelInfoManager, isBlockChannel, channels=" + channels + ", Parental Controll="
                + parentalControl);

        if (Definitions.OPTION_VALUE_ON.equals(parentalControl)) {
            if (blockedChannels != null && channels != null) {
                for (int i = 0; i < blockedChannels.length; i++) {
                    for (int j = 0; j < channels.length; j++) {
                        String callLetter = channels[j].callLetters;

                        if (tempUnblockChannelVec.contains(callLetter)) {
                            Log.printDebug("ChannelInfoManager, included in tempUnblockChannelVec, ch=" + channels[j]);
                            return false;
                        }
                        //					Log.printDebug("ChannelInfoManager, callLetter=" + callLetter);
                        if (callLetter != null && callLetter.equals(blockedChannels[i])) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public boolean isBlockedChannel(Topic topic) {
        Log.printDebug("ChannelInfoManager, isBlockChannel, topic=" + topic);
        if (topic != null) {
            if (topic.channel != null) {
                return isBlockedChannel(topic.channel);
            } else if (topic.network != null) {
                return isBlockedChannel(topic.network.channel);
            }
        }

        return false;
    }

    // VDTRMASTER-5634
    public boolean isBlockedChannelForOption(Channel ch) {
        Log.printDebug("ChannelInfoManager, isBlockedChannelForOption, ch=" + ch);

        if (blockedChannels != null && ch != null) {
            String callLetter = ch.callLetters;

            for (int i = 0; i < blockedChannels.length; i++) {
                if (callLetter != null && callLetter.equals(blockedChannels[i])) {
                    return true;
                }
            }
            // R7.3
            if (ch.mplexChannels != null && ch.mplexChannels.channel.length > 0) {
                return isBlockedChannelForOption(ch.mplexChannels.channel);
            }
        }

        return false;
    }

    // VDTRMASTER-5634
    public boolean isBlockedChannelForOption(Channel[] channels) {
        Log.printDebug("ChannelInfoManager, isBlockedChannelForOption, channels=" + channels);

        if (blockedChannels != null && channels != null) {
            for (int i = 0; i < blockedChannels.length; i++) {
                for (int j = 0; j < channels.length; j++) {
                    String callLetter = channels[j].callLetters;

                    if (callLetter != null && callLetter.equals(blockedChannels[i])) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    // VDTRMASTER-5634
    public boolean isBlockedChannelForOption(Topic topic) {
        Log.printDebug("ChannelInfoManager, isBlockedChannelForOption, topic=" + topic);
        if (topic != null) {
            if (topic.channel != null) {
                return isBlockedChannelForOption(topic.channel);
            } else if (topic.network != null) {
                return isBlockedChannelForOption(topic.network.channel);
            }
        }

        return false;
    }

    public void addBlockedChannel(Topic topic) {
        Log.printDebug("ChannelInfoManager, addBlockChannel, topic=" + topic);
        if (topic != null) {
            // VDTRMASTER-5507
            MenuController.getInstance().showLoadingAnimation();
            // VDTRMASTER-5520
            String parentalControl = DataCenter.getInstance().getString(RightFilter.PARENTAL_CONTROL);
            if (Definitions.OPTION_VALUE_OFF.equals(parentalControl)) {
                try {
                    PreferenceProxy.getInstance().getService().setPreferenceValue(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
                } catch (RemoteException e) {
                    Log.print(e);
                }
            }
            if (topic.channel != null) {
                // R7.3
                if (topic.channel.mplexChannels != null && topic.channel.mplexChannels.channel.length > 0) {
                    Vector callVec = new Vector();

                    if (topic.channel.callLetters != null) {
                        callVec.add(topic.channel.callLetters);
                    }

                    for (int i = 0; i < topic.channel.mplexChannels.channel.length; i++) {
                        if (topic.channel.mplexChannels.channel[i].callLetters != null) {
                            callVec.add(topic.channel.mplexChannels.channel[i].callLetters);
                        }
                    }

                    String[] callArr = new String[callVec.size()];
                    callVec.toArray(callArr);

                    PreferenceProxy.getInstance().addBlockChannels(callArr);

                } else if (topic.channel.callLetters != null) {
                    PreferenceProxy.getInstance().addBlockChannel(topic.channel.callLetters);
                }
            } else if (topic.network != null) {
                Vector callVec = new Vector();

                for (int i = 0; i < topic.network.channel.length; i++) {
                    if (topic.network.channel[i].callLetters != null) {
                        callVec.add(topic.network.channel[i].callLetters);
                    }
                }

                String[] callArr = new String[callVec.size()];
                callVec.toArray(callArr);

                PreferenceProxy.getInstance().addBlockChannels(callArr);
            }
        }
    }

    public void addTemporaliryUnblockChannel(Topic topic) {
        Log.printDebug("ChannelInfoManager, addTemporaliryUnblockChannel, topic=" + topic);

        if (topic != null) {
            if (topic.channel != null) {
                // R7.3
                if (topic.channel.mplexChannels != null && topic.channel.mplexChannels.channel.length > 0) {

                    if (topic.channel.callLetters != null) {
                        tempUnblockChannelVec.add(topic.channel.callLetters);
                    }

                    for (int i = 0; i < topic.channel.mplexChannels.channel.length; i++) {
                        if (topic.channel.mplexChannels.channel[i].callLetters != null) {
                            tempUnblockChannelVec.add(topic.channel.mplexChannels.channel[i].callLetters);
                        }
                    }

                } else if (topic.channel.callLetters != null) {
                    tempUnblockChannelVec.add(topic.channel.callLetters);
                }
            } else if (topic.network != null) {
                for (int i = 0; i < topic.network.channel.length; i++) {
                    tempUnblockChannelVec.add(topic.network.channel[i].callLetters);
                }
            }
        }

    }

    public void removeBlockedChannel(Topic topic) {
        Log.printDebug("ChannelInfoManager, removeBlockChannel, topic=" + topic);

        if (topic != null) {
            // VDTRMASTER-5507
            MenuController.getInstance().showLoadingAnimation();
            if (topic.channel != null) {
                // R7.3
                if (topic.channel.mplexChannels != null && topic.channel.mplexChannels.channel.length > 0) {
                    Vector callVec = new Vector();

                    if (topic.channel.callLetters != null) {
                        callVec.add(topic.channel.callLetters);
                    }

                    for (int i = 0; i < topic.channel.mplexChannels.channel.length; i++) {
                        if (topic.channel.mplexChannels.channel[i].callLetters != null) {
                            callVec.add(topic.channel.mplexChannels.channel[i].callLetters);
                        }
                    }

                    String[] callArr = new String[callVec.size()];
                    callVec.toArray(callArr);

                    PreferenceProxy.getInstance().removeBlockChannels(callArr);

                } else if (topic.channel.callLetters != null) {
                    // VDTRMASTER-5813
                    PreferenceProxy.getInstance().removeBlockChannel(topic.channel.callLetters);

                }
            } else if (topic.network != null) {
                Vector callVec = new Vector();

                for (int i = 0; i < topic.network.channel.length; i++) {
                    callVec.add(topic.network.channel[i].callLetters);
                }

                String[] callArr = new String[callVec.size()];
                callVec.toArray(callArr);

                PreferenceProxy.getInstance().removeBlockChannels(callArr);
            }
        }
    }

    public void updateFavoriteChannels(String list) {
        Log.printDebug("ChannelInfoManager, updateFavoriteChannels, list=" + list);
        favoriteChannels = TextUtil.tokenize(list, PreferenceService.PREFERENCE_DELIMETER);

        if (Log.DEBUG_ON) {
            for (int i = 0; favoriteChannels != null && i < favoriteChannels.length; i++) {
                Log.printDebug("ChannelInfoManager, updateFavoriteChannels,favoriteChannels[" + i + "]="
                        + favoriteChannels[i]);
            }
        }

        // VDTRMASTER-5787
        if (favoriteChannels != null) {
            Vector chVector = new Vector();
            for (int i = 0; i < favoriteChannels.length; i++) {
                chVector.add(favoriteChannels[i]);
            }

            Object[][] channelData = (Object[][]) SharedMemory.getInstance().get(SharedDataKeys.EPG_CHANNELS);
            final int INDEX_CHANNEL_NAME = 1;
            final int INDEX_CHANNEL_SISTER_NAME = 7;
            for (int i = 0; i < channelData.length; i++) {
                String callLetterStr = ((String) channelData[i][INDEX_CHANNEL_NAME]);

                if (chVector.contains(callLetterStr)) {
                    String sisterCallLetterStr = (String) channelData[i][INDEX_CHANNEL_SISTER_NAME];
                    if (sisterCallLetterStr != null && !chVector.contains(sisterCallLetterStr)) {
                        chVector.add(sisterCallLetterStr);
                    }
                }
            }

            favoriteChannels = new String[chVector.size()];
            chVector.copyInto(favoriteChannels);
        }

        StringBuffer sf = new StringBuffer();
        for (int i = 0; favoriteChannels != null && i < favoriteChannels.length; i++) {
            if (i > 0) {
                sf.append(",");
            }
            sf.append(URLEncoder.encode(favoriteChannels[i]));
        }

        paramFavoriteChannels = sf.toString();

        if (Resources.appStatus == VODService.APP_STARTED) {

            BaseUI curUI = (BaseUI) MenuController.getInstance().getCurrentScene();

            if (curUI instanceof CategoryUI) {
                ((CategoryUI) curUI).updateFavoriteChannel();
            } else {
                needToUpdate = true;
            }

            // VDTRMASTER-5507
            MenuController.getInstance().hideLoadingAnimation();
        }
    }

    public boolean isFavoriteChannel(Channel ch) {
        if (Log.ALL_ON) {
            Log.printDebug("ChannelInfoManager, isFavoriteChannel, ch=" + ch);
        }
        if (favoriteChannels != null && ch != null) {
            String callLetter = ch.callLetters;
            for (int i = 0; i < favoriteChannels.length; i++) {
                if (callLetter != null && callLetter.equals(favoriteChannels[i])) {
                    return true;
                }
            }
            // R7.3
            if (ch.mplexChannels != null && ch.mplexChannels.channel.length > 0) {
                return isFavoriteChannel(ch.mplexChannels.channel);
            }
        }

        return false;
    }

    public boolean isFavoriteChannel(Channel[] channels) {
        if (Log.ALL_ON) {
            Log.printDebug("ChannelInfoManager, isFavoriteChannel, channels=" + channels);
        }
        if (favoriteChannels != null && channels != null) {
            for (int i = 0; i < favoriteChannels.length; i++) {
                for (int j = 0; j < channels.length; j++) {
                    String callLetter = channels[j].callLetters;
                    if (callLetter != null && callLetter.equals(favoriteChannels[i])) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean isFavoriteChannel(Topic topic) {
        if (Log.ALL_ON) {
            Log.printDebug("ChannelInfoManager, isFavoriteChannel, topic=" + topic);
        }

        if (topic != null) {
            if (topic.channel != null) {
                return isFavoriteChannel(topic.channel);
            } else if (topic.network != null) {
                return isFavoriteChannel(topic.network.channel);
            }
        }

        return false;
    }

    public void addFavoritehannel(Topic topic, boolean fromOption) {
        Log.printDebug("ChannelInfoManager, addFavoritehannel, topic=" + topic);

        if (topic != null) {
            // VDTRMASTER-5507
            MenuController.getInstance().showLoadingAnimation();
            // R7
            String subfix = "_P";
            if (fromOption) {
                subfix = "_D";
            }

            if (topic.channel != null) {
                // R7.3
                if (topic.channel.mplexChannels != null && topic.channel.mplexChannels.channel.length > 0) {
                    Vector callVec = new Vector();
                    Vector callSubfixVec = new Vector();

                    if (topic.channel.callLetters != null) {
                        callVec.add(topic.channel.callLetters);
                        callSubfixVec.add(topic.channel.callLetters + subfix);
                    }

                    for (int i = 0; i < topic.channel.mplexChannels.channel.length; i++) {
                        if (topic.channel.mplexChannels.channel[i].callLetters != null) {
                            callVec.add(topic.channel.mplexChannels.channel[i].callLetters);
                            callSubfixVec.add(topic.channel.mplexChannels.channel[i].callLetters + subfix);
                        }
                    }

                    String[] callArr = new String[callVec.size()];
                    String[] callSubfixArr = new String[callSubfixVec.size()];
                    callVec.toArray(callArr);
                    callSubfixVec.toArray(callSubfixArr);

                    PreferenceProxy.getInstance().addFavoriteChannels(callArr);

                    VbmController.getInstance().writeAddFavoriteChannel(callSubfixArr);
                } else if (topic.channel.callLetters != null) {
                    // VDTRMASTER-5813
                    PreferenceProxy.getInstance().addFavoriteChannels(new String[]{topic.channel.callLetters});

                    VbmController.getInstance().writeAddFavoriteChannel(topic.channel.callLetters + subfix);
                }
            } else if (topic.network != null) {
                Vector callVec = new Vector();
                Vector callSubfixVec = new Vector();
                for (int i = 0; i < topic.network.channel.length; i++) {
                    if (topic.network.channel[i].callLetters != null) {
                        callVec.add(topic.network.channel[i].callLetters);
                        callSubfixVec.add(topic.network.channel[i].callLetters + subfix);
                    }
                }

                String[] callArr = new String[callVec.size()];
                String[] callSubfixArr = new String[callSubfixVec.size()];
                callVec.toArray(callArr);
                callSubfixVec.toArray(callSubfixArr);

                PreferenceProxy.getInstance().addFavoriteChannels(callArr);

                VbmController.getInstance().writeAddFavoriteChannel(callSubfixArr);
            }
        }
    }

    public void removeFavoritehannel(Topic topic) {
        Log.printDebug("ChannelInfoManager, removeFavoritehannel, topic=" + topic);

        if (topic != null) {
            // VDTRMASTER-5507
            MenuController.getInstance().showLoadingAnimation();
            if (topic.channel != null) {
                // R7.3
                if (topic.channel.mplexChannels != null && topic.channel.mplexChannels.channel.length > 0) {
                    Vector callVec = new Vector();
                    if (topic.channel.callLetters != null) {
                        callVec.add(topic.channel.callLetters);
                    }
                    for (int i = 0; i < topic.channel.mplexChannels.channel.length; i++) {
                        if (topic.channel.mplexChannels.channel[i].callLetters != null) {
                            callVec.add(topic.channel.mplexChannels.channel[i].callLetters);
                        }
                    }

                    String[] callArr = new String[callVec.size()];
                    callVec.toArray(callArr);

                    PreferenceProxy.getInstance().removeFavoriteChannels(callArr);

                } else if (topic.channel.callLetters != null) {
                    PreferenceProxy.getInstance().removeFavoriteChannel(topic.channel.callLetters);
                }
            } else if (topic.network != null) {
                Vector callVec = new Vector();

                for (int i = 0; i < topic.network.channel.length; i++) {
                    callVec.add(topic.network.channel[i].callLetters);
                }

                String[] callArr = new String[callVec.size()];
                callVec.toArray(callArr);

                PreferenceProxy.getInstance().removeFavoriteChannels(callArr);
            }
        }
    }

    public boolean isPreviewChannel(Channel ch) {
        if (Log.EXTRA_ON) {
            Log.printDebug("ChannelInfoManager, isPreviewChannel, ch=" + ch);
        }
        return freePreviews.contains(ch.callLetters);
    }

    public boolean isPreviewChannel(Channel[] channels) {
        if (Log.EXTRA_ON) {
            Log.printDebug("ChannelInfoManager, isPreviewChannel, channels=" + channels);
        }
        if (channels != null) {
            for (int i = 0; i < channels.length; i++) {
                if (freePreviews.contains(channels[i].callLetters)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isPreviewChannel(Topic topic) {
        if (Log.EXTRA_ON) {
            Log.printDebug("ChannelInfoManager, isPreviewChannel, topic=" + topic);
        }

        if (topic != null) {
            if (topic.channel != null) {
                return isPreviewChannel(topic.channel);
            } else if (topic.network != null) {
                return isPreviewChannel(topic.network.channel);
            }
        }

        return false;
    }

    public boolean isSubscribed(Topic topic) {
        if (topic.channel != null) {
            if (topic.channel.hasAuth == false) {
                topic.channel.hasAuth = CommunicationManager.getInstance().checkAuth(topic.channel.callLetters);
            }

            if (topic.channel.hasAuth) {
                return topic.channel.hasAuth;
            }

            if (topic.channel.mplexChannels != null) {
                for (int i = 0; i < topic.channel.mplexChannels.channel.length; i++) {
                    if (!topic.channel.mplexChannels.channel[i].hasAuth) {
                        topic.channel.mplexChannels.channel[i].hasAuth = CommunicationManager.getInstance()
                                .checkAuth(topic.channel.mplexChannels.channel[i].callLetters);

                        if (topic.channel.mplexChannels.channel[i].hasAuth) {
                            return true;
                        }
                    }
                }
            }

        } else if (topic.network != null && topic.network.channel != null) {

            for (int i = 0; i < topic.network.channel.length; i++) {
                if (topic.network.channel[i].hasAuth == false) {
                    topic.network.channel[i].hasAuth = CommunicationManager.getInstance()
                            .checkAuth(topic.network.channel[i].callLetters);
                }

                if (topic.network.channel[i].hasAuth) {
                    return true;
                }
            }
        }

        return false;
    }

    public int getFavoriteChannelCount() {
        Log.printDebug("ChannelInfoManager, getFavoriteChannelCount");
        if (favoriteCategoryContainers != null) {
            return favoriteCategoryContainers.length;
        }

        return 0;
    }

    public int getAllChannelCount() {
        Log.printDebug("ChannelInfoManager, getAllChannelCount");
        if (allChannelCategoryContainers != null) {
            return allChannelCategoryContainers.length - getFavoriteChannelCount();
        }

        return 0;

    }

    public int getSubscribedChannelsCount() {
        Log.printDebug("ChannelInfoManager, getSubscribedChannelsCount");
        return subscribedChannelsCount;
    }

    public int getUnsubscribedChannelsCount() {
        Log.printDebug("ChannelInfoManager, getUnsubscribedChannelsCount");
        return unsubscribedChannelCount;
    }

    public String getParamFavoriteChannels() {
        Log.printDebug("ChannelInfoManager, getParamFavoriteChannels");
        if (paramFavoriteChannels != null && favoriteChannels.length > 0) {
            return paramFavoriteChannels;
        }
        return null;
    }

    public void setAllChannels(CategoryContainer[] categories) {
        Log.printDebug("ChannelInfoManager, setAllChannels, categories=" + categories);
        allChannelCategoryContainers = categories;

        ArrayList list = new ArrayList();

        for (int i = 0; i < allChannelCategoryContainers.length; i++) {
            boolean addFavoriteChanel = false;
            Channel ch = allChannelCategoryContainers[i].getTopic().channel;
            Log.printDebug("ChannelInfoManager, setAllChannels, ch=" + ch);
            if (ch != null) {
                if (isFavoriteChannel(ch)) {
                    list.add(allChannelCategoryContainers[i]);
                    addFavoriteChanel = true;
                }

            } else {
                Network nt = allChannelCategoryContainers[i].getTopic().network;
                Log.printDebug("ChannelInfoManager, setAllChannels, nt=" + nt);
                if (nt != null) {
                    Log.printDebug("ChannelInfoManager, setAllChannels, nt.channel=" + nt.channel);
                    if (isFavoriteChannel(nt.channel)) {
                        list.add(allChannelCategoryContainers[i]);
                        addFavoriteChanel = true;
                    }
                }
            }

            if (!addFavoriteChanel) {
                break;
            }
        }

        // for (int i = 0; i < allChannelCategoryContainers.length; i++) {
        // Channel ch = allChannelCategoryContainers[i].getTopic().channel;
        // Log.printDebug("ChannelInfoManager, setAllChannels, ch=" + ch);
        // if (ch != null) {
        // if (isPreviewChannel(ch)) {
        // previewVec.add(allChannelCategoryContainers[i].getTopic().id);
        // }
        // } else {
        // Network nt = allChannelCategoryContainers[i].getTopic().network;
        // Log.printDebug("ChannelInfoManager, setAllChannels, nt=" + nt);
        // if (nt != null) {
        // Log.printDebug("ChannelInfoManager, setAllChannels, nt.channel=" +
        // nt.channel);
        // if (isPreviewChannel(nt.channel)) {
        // previewVec.add(allChannelCategoryContainers[i].getTopic().id);
        // }
        // }
        // }
        // }

        favoriteCategoryContainers = new CategoryContainer[list.size()];
        list.toArray(favoriteCategoryContainers);

        // VDTRMASTER-5817
        CategoryContainer tmp;
        for (int i = 0; i < favoriteCategoryContainers.length; i++) {
            for (int j = i + 1; j < favoriteCategoryContainers.length; j++) {

                if (collator.compare(favoriteCategoryContainers[i].getTitle(),
                        favoriteCategoryContainers[j].getTitle()) > 0) {
                    tmp = favoriteCategoryContainers[i];
                    favoriteCategoryContainers[i] = favoriteCategoryContainers[j];
                    favoriteCategoryContainers[j] = tmp;
                }
            }
        }

        Log.printDebug("ChannelInfoManager, setAllChannels, favoriteCategoryContainers" + favoriteCategoryContainers.length);
    }

    public void buildParamSubscribedChannels(CategoryContainer[] categories) {
        Log.printDebug("ChannelInfoManager, buildParamSubscribedChannels, categories=" + categories);
        allChannelCategoryContainers = categories;

        ArrayList list = new ArrayList();
        ArrayList cList = new ArrayList();
        Channel ch;
        Network nw;
        MenuController menu = MenuController.getInstance();
        for (int i = 0; i < categories.length; i++) {

            if (!isFavoriteChannel(categories[i].getTopic())) {

                nw = categories[i].getTopic().network;
                if (nw != null) {
                    for (int j = 0; j < nw.channel.length; j++) {
                        if (nw.channel[j].hasAuth == false) {
                            nw.channel[j].hasAuth = CommunicationManager.getInstance().checkAuth(nw.channel[j].callLetters);
                        }
                        if (nw.channel[j].hasAuth) {
                            list.add(nw.channel[j].callLetters);
                            cList.add(categories[i]);
                            menu.debugList[0].add("Item[" + i + "/" + categories.length + "] = " + nw.toString(true) + ", "
                                    + nw.channel[j].toString(true) + " is subscribed.");
                            Log.printDebug("Item[" + i + "/" + categories.length + "] = " + nw.toString(true) + ", "
                                    + nw.channel[j].toString(true) + " is subscribed.");
                            break;
                        }
                    }
                }
                ch = ((CategoryContainer) categories[i]).getTopic().channel;
                if (ch == null) {
                    continue;
                }
                if (ch.hasAuth == false) {
                    ch.hasAuth = CommunicationManager.getInstance().checkAuth(ch.callLetters);
                }
                if (ch.hasAuth) {
                    list.add(ch.callLetters);
                    cList.add(categories[i]);
                    menu.debugList[0].add("Item[" + i + "/" + categories.length + "] = " + ch.toString(true)
                            + " is subscribed.");
                    Log.printDebug("Item[" + i + "/" + categories.length + "] = " + ch.toString(true) + " is subscribed.");
                }
            }
        }

        //
        StringBuffer sf = new StringBuffer();
        subscribedChannelsCount = list.size();
        for (int i = 0; i < list.size(); i++) {
            if (i > 0) {
                sf.append(",");
            }
            sf.append(URLEncoder.encode((String) list.get(i)));
        }
        paramSubscribedChannels = sf.toString();

        subscribedCategoryContainers = new CategoryContainer[cList.size()];
        subscribedChannelsCount = cList.size();
        cList.toArray(subscribedCategoryContainers);
    }

    public String getParamSubscribedChannels() {
        return paramSubscribedChannels;
    }

    public CategoryContainer[] getDataForAllChannelMode() {
        Log.printDebug("ChannelInfoManager, getDataForAllChannelMode");
        CategoryContainer[] newCategoryContainers;

        ArrayList list = new ArrayList();

        // add favorite tab
        list.add(favoriteTabContainer);

        // add favorite channels
        for (int i = 0; i < favoriteCategoryContainers.length; i++) {
            list.add(favoriteCategoryContainers[i]);
        }

        // add All Channel tab
        list.add(allChannelTabContainer);

        for (int i = favoriteCategoryContainers.length; i < allChannelCategoryContainers.length; i++) {
            list.add(allChannelCategoryContainers[i]);
        }

        newCategoryContainers = new CategoryContainer[list.size()];
        list.toArray(newCategoryContainers);

        return newCategoryContainers;
    }

    public CategoryContainer[] getDataForAllChannelMode(CategoryContainer ca, int menuIdx) {
        Log.printDebug("ChannelInfoManager, getDataForAllChannelMode, ca=" + ca.getTopic().id + ", menuIdx=" + menuIdx);
        CategoryContainer[] newAllContainers;

        // find in favorite
        boolean isInFavoriteList = false;
        if (menuIdx <= getFavoriteChannelCount()) {
            isInFavoriteList = true;
        }

        ArrayList list = new ArrayList();

        if (isInFavoriteList) {
            // remove from favorite list
            int i = 0;
            for (; !allChannelCategoryContainers[i].getTopic().id.equals(ca.getTopic().id); i++) {
                list.add(allChannelCategoryContainers[i]);
            }

            CategoryContainer targetContainer = allChannelCategoryContainers[i];

            if (Log.DEBUG_ON) {
                Log.printDebug("ChannelInfoManager, getDataForAllChannelMode, targetContainer="
                        + targetContainer.getTitle());
            }

            i++;
            for (; i < getFavoriteChannelCount()
                    || collator.compare(allChannelCategoryContainers[i].getTitle(), targetContainer.getTitle()) < 0; i++) {
                list.add(allChannelCategoryContainers[i]);
            }

//			list.add(targetContainer);
            boolean needToAdd = true;

            for (; i < allChannelCategoryContainers.length; i++) {
                if (!needToAdd || collator.compare(allChannelCategoryContainers[i].getTitle(), targetContainer.getTitle()) < 0) {
                    list.add(allChannelCategoryContainers[i]);
                } else {
                    list.add(targetContainer);
                    // VDTRMASTER-5624
                    list.add(allChannelCategoryContainers[i]);
                    needToAdd = false;
                }
            }

        } else {
            // add from favorite list
            int targetIdx = 0;

            for (int tIdx = 0; tIdx < allChannelCategoryContainers.length; tIdx++) {
                if (ca.getTopic().id.equals(allChannelCategoryContainers[tIdx].getTopic().id)) {
                    targetIdx = tIdx;
                    break;
                }
            }

            int i = 0;
            for (; collator.compare(allChannelCategoryContainers[i].getTitle(), ca.getTitle()) < 0
                    && i < getFavoriteChannelCount(); i++) {
                list.add(allChannelCategoryContainers[i]);
            }

            CategoryContainer targetContainer = allChannelCategoryContainers[targetIdx];
            list.add(targetContainer);
            for (; i < allChannelCategoryContainers.length; i++) {
                if (!ca.getTopic().id.equals(allChannelCategoryContainers[i].getTopic().id)) {
                    list.add(allChannelCategoryContainers[i]);
                }
            }
        }

        newAllContainers = new CategoryContainer[list.size()];
        list.toArray(newAllContainers);

        setAllChannels(newAllContainers);
        CategoryContainer[] newCate = getDataForAllChannelMode();

        // R7
        // VDTRMASTER-5817
        // To handle special case for The Movie network and HBO (recursive category);
        for (int idx = 0; idx < allChannelCategoryContainers.length; idx++) {

            if (allChannelCategoryContainers[idx].getTopic().id != ca.getTopic().id
                    && ((isFavoriteChannel(allChannelCategoryContainers[idx].getTopic()) && idx > getFavoriteChannelCount())
                    || (!isFavoriteChannel(allChannelCategoryContainers[idx].getTopic()) && idx <= getFavoriteChannelCount()))) {
                newCate = getDataForAllChannelMode(allChannelCategoryContainers[idx], idx);
                break;
            }
        }

        return newCate;
    }

    public CategoryContainer[] getDataForSubscribedMode() {
        CategoryContainer[] newCategoryContainers;

        ArrayList list = new ArrayList();

        if (allChannelCategoryContainers != null) {
            buildParamSubscribedChannels(allChannelCategoryContainers);
        }

        // add favorite tab
        list.add(favoriteTabContainer);

        // add favorite channels
        for (int i = 0; i < favoriteCategoryContainers.length; i++) {
            list.add(favoriteCategoryContainers[i]);
        }

        // add subscribed tab
        list.add(subscribedTabContainer);

        for (int i = 0; i < subscribedCategoryContainers.length; i++) {
            list.add(subscribedCategoryContainers[i]);
        }

        newCategoryContainers = new CategoryContainer[list.size()];
        list.toArray(newCategoryContainers);

        return newCategoryContainers;
    }

    public CategoryContainer[] getDataForSubscribedMode(CategoryContainer ca, int menuIdx) {
        Log.printDebug("ChannelInfoManager, getDataForSubscribedMode, ca=" + ca.getTitle() + ", menuIdx=" + menuIdx);
        CategoryContainer[] newAllContainers;

        // find in favorite
        boolean isInFavoriteList = false;
        if (menuIdx <= getFavoriteChannelCount()) {
            isInFavoriteList = true;
        }

        ArrayList list = new ArrayList();

        if (isInFavoriteList) {
            // remove from favorite list
            int i = 0;
            for (; !allChannelCategoryContainers[i].getTopic().id.equals(ca.getTopic().id); i++) {
                list.add(allChannelCategoryContainers[i]);
                if (Log.DEBUG_ON) {
                    Log.printDebug("ChannelInfoManager, getDataForSubscribedMode, 1. allChannelCategoryContainers[" + i
                            + "]=" + allChannelCategoryContainers[i].getTopic().getTitle());
                }
            }

            CategoryContainer targetContainer = allChannelCategoryContainers[i];

            if (Log.DEBUG_ON) {
                Log.printDebug("ChannelInfoManager, getDataForSubscribedMode, targetContainer="
                        + targetContainer.getTopic().id);
                Log.printDebug("ChannelInfoManager, getDataForSubscribedMode, index=" + i);
            }

            i++;
            for (; i < getFavoriteChannelCount()
                    || collator.compare(allChannelCategoryContainers[i].getTitle(), targetContainer.getTitle()) < 0; i++) {
                list.add(allChannelCategoryContainers[i]);
                if (Log.DEBUG_ON) {
                    Log.printDebug("ChannelInfoManager, getDataForSubscribedMode, 2. allChannelCategoryContainers[" + i
                            + "]=" + allChannelCategoryContainers[i].getTopic().getTitle());
                }
            }

//			list.add(targetContainer);
            boolean needToAdd = true;
            for (; i < allChannelCategoryContainers.length; i++) {
                if (!needToAdd || collator.compare(allChannelCategoryContainers[i].getTitle(), targetContainer.getTitle()) < 0) {
                    list.add(allChannelCategoryContainers[i]);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ChannelInfoManager, getDataForSubscribedMode, 3. allChannelCategoryContainers[" + i
                                + "]=" + allChannelCategoryContainers[i].getTitle());
                    }
                } else {
                    list.add(targetContainer);
                    // VDTRMASTER-5624
                    list.add(allChannelCategoryContainers[i]);
                    needToAdd = false;
                }
            }
        } else {
            // add into favorite list
            int targetIdx = 0;

            for (int tIdx = 0; tIdx < allChannelCategoryContainers.length; tIdx++) {
                if (ca.getTopic().id.equals(allChannelCategoryContainers[tIdx].getTopic().id)) {
                    targetIdx = tIdx;
                    break;
                }
            }

            int i = 0;
            for (; collator.compare(allChannelCategoryContainers[i].getTitle(), ca.getTitle()) < 0
                    && i < getFavoriteChannelCount(); i++) {
                list.add(allChannelCategoryContainers[i]);
            }

            CategoryContainer targetContainer = allChannelCategoryContainers[targetIdx];
            list.add(targetContainer);
            for (; i < allChannelCategoryContainers.length; i++) {
                if (!ca.getTopic().id.equals(allChannelCategoryContainers[i].getTopic().id)) {
                    list.add(allChannelCategoryContainers[i]);
                }
            }
        }

        newAllContainers = new CategoryContainer[list.size()];
        list.toArray(newAllContainers);

        setAllChannels(newAllContainers);
        CategoryContainer[] newCate = getDataForSubscribedMode();

        // R7
        // VDTRMASTER-5817
        // To handle special case for The Movie network and HBO (recursive category);
        for (int idx = 0; idx < allChannelCategoryContainers.length; idx++) {

            if (allChannelCategoryContainers[idx].getTopic().id != ca.getTopic().id
                    && ((isFavoriteChannel(allChannelCategoryContainers[idx].getTopic()) && idx > getFavoriteChannelCount())
                    || (!isFavoriteChannel(allChannelCategoryContainers[idx].getTopic()) && idx <= getFavoriteChannelCount()))) {
                newCate = getDataForSubscribedMode(allChannelCategoryContainers[idx], idx);
                break;
            }
        }

        return newCate;
    }

    public void updateFavoriteData(Topic topic) {
        Log.printDebug("ChannelInfoManager, updateFavoriteData, topic.id=" + topic.id
                + ", allChannelCategoryContainers=" + allChannelCategoryContainers);
        int tartgetIdx = 0;
        CategoryContainer foundCategory = null;

        if (allChannelCategoryContainers != null) {
            for (int i = 0; i < allChannelCategoryContainers.length; i++) {
                Log.printDebug("ChannelInfoManager, updateFavoriteData, allChannelCategoryContainers[" + i + "].id="
                        + allChannelCategoryContainers[i].id);
                if (topic.id.equals(allChannelCategoryContainers[i].getTopic().id)) {
                    tartgetIdx = i;
                    foundCategory = allChannelCategoryContainers[i];
                    break;
                }
            }
        }

        Log.printDebug("ChannelInfoManager, updateFavoriteData, foundCategory=" + foundCategory);

        if (foundCategory != null) {
            getDataForAllChannelMode(foundCategory, tartgetIdx);
        }
    }

    // R7.3
    public CategoryContainer[] getChannelCategoryContainers(CategoryContainer treeRoot) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, treeRoot=" + treeRoot);
            if (treeRoot.wrappedTopicPointer != null) {
                Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, unsubscribed=" + treeRoot.wrappedTopicPointer.unsubscribed);
            }
        }
        this.treeRoot = treeRoot;

        //R7.3 sykim VDTRMASTER-6160
        //if (treeRoot.wrappedTopicPointer.unsubscribed != null) {
        favorites = treeRoot.wrappedTopicPointer.favourites;
        subscribed = treeRoot.wrappedTopicPointer.subscribed;
        unsubscribed = treeRoot.wrappedTopicPointer.unsubscribed;
        if (favorites != null) {
            favoritesCount = favorites.count;
        }
        // VDTRMASTER-6100
        if (subscribed != null) {
            subscribedChannelsOnlyCount = subscribed.count;
        }
        if (unsubscribed != null) {
            unsubscribedChannelCount = unsubscribed.count;            
        } else
        	unsubscribedChannelCount = 0;
        return getChannelCategoryContainers();
//        } else {
//            return treeRoot.categoryContainer;
//        }
    }

    public CategoryContainer[] getChannelCategoryContainers() {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers");
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, favorites=" + favorites);
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, subscribed=" + subscribed);
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, unsubscribed=" + unsubscribed);
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, favoritesCount=" + favoritesCount);
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, subscribedChannelsOnlyCount=" + subscribedChannelsOnlyCount);
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, unsubscribedChannelCount=" + unsubscribedChannelCount);
        }

        Vector favoriteVec = new Vector();
        Vector subscribedVec = new Vector();
        Vector unsubscribedVec = new Vector();
        Vector completedListVec = new Vector();

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, handle a favorites list");
        }

        for (int i = 0; i < favoritesCount; i++) {
            CategoryContainer caContainer = favorites.categoryContainer[i];
            Topic topic = caContainer.getTopic();

            if (isFavoriteChannel(topic)) {
                favoriteVec.add(caContainer);
            } else {
                if (isSubscribed(topic)) {
                    subscribedVec.add(caContainer);
                } else {
                    unsubscribedVec.add(caContainer);
                }
            }
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, handle a subscribed list");
        }

        for (int i = 0; i < subscribedChannelsOnlyCount; i++) {
            CategoryContainer caContainer = subscribed.categoryContainer[i];
            Topic topic = caContainer.getTopic();

            if (isFavoriteChannel(topic)) {
                favoriteVec.add(caContainer);
            } else {
                subscribedVec.add(caContainer);
            }
        }
        sortWithTitle(favoriteVec);

        sortWithTitle(subscribedVec);

        // VDTRMASTER-6092
        subscribedChannelsCount = favoriteVec.size() + subscribedVec.size();
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, subscribedChannelsCount=" + subscribedChannelsCount);
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, handle an unsubscribed list");
        }

        for (int i = 0; i < unsubscribedChannelCount; i++) {
            CategoryContainer caContainer = unsubscribed.categoryContainer[i];
            unsubscribedVec.add(caContainer);
        }

        sortWithTitle(unsubscribedVec);

        // VDTRMASTER-6091
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, add myChannelContainer in completed list");
        }
        completedListVec.add(myPackageTabContainer);

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, add favorites in completed list");
        }
        for (int i = 0; i < favoriteVec.size(); i++) {
            completedListVec.add(favoriteVec.get(i));
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, add subscribed list in completed list");
        }
        for (int i = 0; i < subscribedVec.size(); i++) {
            completedListVec.add(subscribedVec.get(i));
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, add unsubscribedTabContainer in completed list");
        }
        completedListVec.add(unsubscribedTabContainer);
        unsubscribedTabIndex = completedListVec.size() - 1;

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, add unsubscribed list in completed list");
        }

        for (int i = 0; i < unsubscribedVec.size(); i++) {
            completedListVec.add(unsubscribedVec.get(i));
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getChannelCategoryContainers, completedListVec.size=" + completedListVec.size());
        }

        CategoryContainer[] completedCaContainers = new CategoryContainer[completedListVec.size()];
        completedListVec.copyInto(completedCaContainers);

        return completedCaContainers;

    }

    public CategoryContainer[] sortWithTitle(CategoryContainer[] caContainers) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, sortWithTitle(Categorycontainer[])");
        }
        CategoryContainer tmp;
        for (int i = 0; i < caContainers.length; i++) {
            for (int j = i + 1; j < caContainers.length; j++) {

                if (collator.compare(caContainers[i].getTitle(),
                        caContainers[j].getTitle()) > 0) {
                    tmp = caContainers[i];
                    caContainers[i] = caContainers[j];
                    caContainers[j] = tmp;
                }
            }
        }

        return caContainers;
    }

    public Vector sortWithTitle(Vector caContainerVec) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, sortWithTitle(Vector)");
        }
        int size = caContainerVec.size();
        for (int i = 0; i < size; i++) {
            for (int j = i + 1; j < size; j++) {
                CategoryContainer curContainer = (CategoryContainer) caContainerVec.get(i);
                CategoryContainer nextContainer = (CategoryContainer) caContainerVec.get(j);

                if (collator.compare(curContainer.getTitle(),
                        nextContainer.getTitle()) > 0) {

                    caContainerVec.remove(j);
                    caContainerVec.remove(i);
                    caContainerVec.add(i, nextContainer);
                    caContainerVec.add(j, curContainer);
                }
            }
        }

        return caContainerVec;
    }

    public int getUnsubscribedTabIndex() {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getUnsubscribedTabIndex=" + unsubscribedTabIndex);
        }
        return unsubscribedTabIndex;
    }

    // R7.3
    public void updateSubscribedList() {
        long startTime = System.currentTimeMillis();
        Object[][] epgChannels = (Object[][]) SharedMemory.getInstance().get(SharedDataKeys.EPG_CHANNELS);
        Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(SharedDataKeys.EPG_CA_MAP);

//		if (Log.DEBUG_ON) {
//			Log.printDebug("ChannelInfoManager, getSubscribedList, epgChannels=" + epgChannels);
//			Log.printDebug("ChannelInfoManager, getSubscribedList, caTable=" + caTable);
//            Log.printDebug("ChannelInfoManager, getSubscribedList, paramSubscribedChannels=" + paramSubscribedChannels);
//		}

        StringBuffer channelStringBuffer = new StringBuffer();

        if (epgChannels != null) {
            for (int i = 0; i < epgChannels.length; i++) {
                int type = ((Integer) epgChannels[i][4]).intValue();
                if (type >= TvChannel.TYPE_NORMAL && type != TvChannel.TYPE_VIRTUAL && type != TvChannel.TYPE_PPV) {
                    Integer sourceId = (Integer) epgChannels[i][5];
                    Boolean subscribed = (Boolean) caTable.get(sourceId);

                    if (subscribed != null && subscribed.booleanValue()) {
                        if (channelStringBuffer.length() > 0) {
                            channelStringBuffer.append(",");
                        }
                        String callLetter = (String) epgChannels[i][1];
                        if (callLetter != null) {
                            // VDTRMASTER-6073
                            channelStringBuffer.append(URLEncoder.encode(callLetter));
                        }
                    }
                }
            }
            paramSubscribedChannels = channelStringBuffer.toString();

            if (Environment.EMULATOR) {
                paramSubscribedChannels = "TOONF,DISFR,YOOPA,SE";
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("ChannelInfoManager, getSubscribedList, paramSuscribedChannels=" + paramSubscribedChannels);
            }

        }
        long endTime = System.currentTimeMillis();

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelInfoManager, getSubscribedList, time=" + (endTime - startTime));
        }
    }

    public void dataRemoved(String key) {
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (FREE_PREVIEWS.equals(key)) {
            File file = (File) value;
            FreePreviews config = FreePreviews.create(file);
            if (config == null) {
                return;
            }
            Log.printDebug(config);
            this.freePreviews = config.getChannelNames();
        }
    }
}

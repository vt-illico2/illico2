package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;

/**
 * UNClientSessionInProgress.
 */
public class ClientSessionInProgress extends DSMCCMessage implements ClientToNetworkMessage {
    Vector m_sessions;

    /**
     * This is a watchdog message sent from the client to the server to keep it from tearing down the session.
     * Interestingly enough, when sniffing data on a live session, this message was never seen from the native code over
     * a 30 minute period.
     */
    public ClientSessionInProgress() {
        setAdaptationLength((byte) 0);
        setMessageID(SessionMessage.CLIENT_SESSION_IN_PROGRESS);
        setTransactionID();
        setDSMCCType(SessionMessage.UN_SESSION_MESSAGE);
    }

    /**
     *
     * The ClientSessionInProgress message contains a list of all of the sessions currently in
     * progress. The Client can have more than 100 sessions running by spec. So, you hit the watchdog with a single
     * message for all of them at once.
     *
     * @param sess
     *            Vector of all current sessions.
     */
    public void setSessions(Vector sess) {
        m_sessions = sess;
        // All session fields are 10 bytes long.
        // The session count field is 2 bytes long.
        int size = 2 + 10 * m_sessions.size();
        // We now know the size of the message.
        // Fill it in.
        setMessageLength(size);
    }

    /**
     * toByteArray, Used to serialize the message.
     *
     * @return A byte array containing the serialized message.
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        byte[] s = super.toByteArray();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();

        try {
            bo.write(s);
            int size = m_sessions.size();
            byte tempb = (byte) (size & 0xFF);
            byte tempa = (byte) ((size >> 8) & 0xFF);
            bo.write(tempa);
            bo.write(tempb);

            for (Enumeration e = m_sessions.elements(); e.hasMoreElements();) {
                byte[] b = (byte[]) e.nextElement();
                bo.write(b);
            }

            rc = bo.toByteArray();

            bo.close();
            bo = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (bo != null) {
                try {
                    bo.close();
                    bo = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }

        return rc;
    }

}

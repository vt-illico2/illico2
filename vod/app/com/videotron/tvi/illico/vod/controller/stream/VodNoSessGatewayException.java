package com.videotron.tvi.illico.vod.controller.stream;

/**
 * Generated if the session gateway server is not specified.
 */
public class VodNoSessGatewayException extends Exception {

    private static final long serialVersionUID = -1164867650753626282L;

    public VodNoSessGatewayException() {
        super();
    }

    /**
     * @param arg0
     *            The detailed reason.
     */
    public VodNoSessGatewayException(String arg0) {
        super(arg0);
    }
}

package com.videotron.tvi.illico.vod.controller.session.autodiscovery;

public class SGEvent {
    int eventType;

    int validSGIndex;

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    /**
     * @return the validSGIndex
     */
    public int getValidSGIndex() {
        return validSGIndex;
    }

    /**
     * @param validSGIndex
     *            the validSGIndex to set
     */
    public void setValidSGIndex(int validSGIndex) {
        this.validSGIndex = validSGIndex;
    }

}

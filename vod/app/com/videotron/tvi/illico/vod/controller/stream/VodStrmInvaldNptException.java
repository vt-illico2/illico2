package com.videotron.tvi.illico.vod.controller.stream;

/**
 * This exception is thrown when an invalid nptStart or nptStop parameter is
 * detected.
 */
public class VodStrmInvaldNptException extends VodStrmIllegalArguException {

    private static final long serialVersionUID = -6450255861832648221L;

    public VodStrmInvaldNptException() {
    }

    public VodStrmInvaldNptException(String reason) {
        super(reason);
    }
}

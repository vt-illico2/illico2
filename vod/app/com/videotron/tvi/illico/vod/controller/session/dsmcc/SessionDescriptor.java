/* Generated by Together */

package com.videotron.tvi.illico.vod.controller.session.dsmcc;

/**
 * A container class for generic descriptors.
 * 
 * */
public class SessionDescriptor {
    private int tag = 0;

    private byte[] ba = null;

    private int length = 0;

    private int type = 0;

    /** identifies the descriptors added to the setup request method.*/
    public static final int SETUP_REQUEST_DESCRIPTOR_TYPE = 0;

    /** identifies the descriptors returned from the server when the setup completes. */
    public static final int SETUP_COMPLETE_DESCRIPTOR_TYPE = 1;

    /** identifies the descriptors added by the application and sent via the release request method. */
    public static final int RELEASE_REQUEST_DESCRIPTOR_TYPE = 2;

    /** identifies the descriptors added by the server in response to the release request.*/
    public static final int RELEASE_COMPLETE_DESCRIPTOR_TYPE = 3;

    /**
     * specifies the descriptors sent by the server when the server
     * initiates the release.
     */
    public static final int RELEASE_NOTIFICATION_DESCRIPTOR_TYPE = 4;

    /**
     * 
     * @param tag
     *            the descriptor's tag.
     * @param length
     *            the descriptor's length
     * @param ba
     *            the data
     */
    public SessionDescriptor(int tag, int length, byte[] ba) {
        this.ba = ba;
        this.length = length;
        this.tag = tag;
    }

    /**
     * Responds with the descriptor type that identifies this descriptor.
     * 
     * @return the descriptor type.
     * 
     * @see ISessionHandler#SETUP_COMPLETE_DESCRIPTOR_TYPE
     * @see ISessionHandler#SETUP_REQUEST_DESCRIPTOR_TYPE
     * @see ISessionHandler#RELEASE_COMPLETE_DESCRIPTOR_TYPE
     * @see ISessionHandler#RELEASE_REQUEST_DESCRIPTOR_TYPE
     * @see ISessionHandler#RELEASE_NOTIFICATION_DESCRIPTOR_TYPE
     */
    public int getType() {
        return type;
    }

    /**
     * Responds with the descriptor's tag.
     * 
     * @return the value of the descriptor tag.
     */
    public int getTag() {
        return tag;
    }

    /**
     * Responds with the descriptor's length.
     * 
     * @return the number of bytes in the descriptor data.
     */
    public int getLength() {
        return length;
    }

    /**
     * Responds with the descriptors data stored in a byte array.
     * 
     * @return the data in byte array format.
     */
    public byte[] getData() {
        return ba;
    }

}

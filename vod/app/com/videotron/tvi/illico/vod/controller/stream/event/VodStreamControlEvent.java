package com.videotron.tvi.illico.vod.controller.stream.event;

import java.util.EventObject;

import com.videotron.tvi.illico.vod.controller.stream.IVodStreamControl;

/**
 * VodStreamControlEvent The base class for all stream related events.
 * 
 * @see VodStreamControlEvent
 * @see VodStreamPlayEvent
 * @see VodStreamPauseEvent
 * @see VodStreamFastForwardEvent
 * @see VodStreamRewindEvent
 * @see VodStreamStatusEvent
 * @see VodStreamEndOfStreamEvent
 * @see IVodStreamControl
 * @see IVodStreamControlListener
 */
public class VodStreamControlEvent extends EventObject {

    private static final long serialVersionUID = -284562799261110279L;

    /**
     * Contructs a VodStreamControlEvent object.
     * 
     * @param objSrc
     *            The source of the event.
     * @param act
     *            Asynchronous completion token.
     * @param streamHandle
     *            the streamHandle as seen by the media server.
     * @param streamState
     *            the server state for this stream
     * @param statusCode
     *            the outcome code of this operation.
     * @param lastNpt
     *            the npt of the stream reported by the media server at the time the operation was processed.
     * @param scaleNum
     *            the numerator portion of the stream's current rate.
     * @param scaleDenom
     *            the denominator portion of the stream's current rate
     * 
     * @see VodStreamControlEvent
     * @see VodStreamPlayEvent
     * @see VodStreamPauseEvent
     * @see VodStreamFastForwardEvent
     * @see VodStreamRewindEvent
     * @see VodStreamStatusEvent
     * @see VodStreamEndOfStreamEvent
     * @see IVodStreamControl
     * @see IVodStreamControlListener
     */
    public VodStreamControlEvent(IVodStreamControl objSrc, Object act, int streamHandle, int streamState,
            int statusCode, int lastNpt, int scaleNum, int scaleDenom) {
        super(objSrc);

        m_streamHandle = streamHandle;
        m_streamState = streamState;
        m_statusCode = statusCode;
        m_lastNpt = lastNpt;
        m_scaleNum = scaleNum;
        m_scaleDenom = scaleDenom;
    }

    /**
     * Responds with the act passed into the corresponding request. If the subclassed event is not
     * solicited response, the act will be null. It may also be null if the associated request event was solicited and
     * null was passsed in.
     * 
     * @return The asynchronous completion token.
     */
    public Object getAct() {
        return null;
    }

    /**
     * Responds with the current NPT for this stream object as indicated by the VOD server.
     * The current NPT is updated by the most recent completion or notification event.
     * 
     * @return the last NPT received.
     */
    public int getStreamNpt() {
        return m_lastNpt;
    }

    /**
     * Responds with the current state for this stream object as indicated by the VOD
     * server.
     * 
     * @return the last NPT received.
     */
    public int getStreamState() {
        return m_streamState;
    }

    /**
     * Responds with the outcome of the requested operation, identifying success or a
     * detailed description of why the request failed.
     * 
     * @return the success or failure code corresponding to the associated requested.
     */
    public int getEventStatus() {
        return m_statusCode;
    }

    /**
     * Responds with the handle of the stream supplied by the video server.
     * 
     * @return the handle used by the media server to associate stream requests.
     */
    public int getStreamHandle() {
        return m_streamHandle;
    }

    /**
     * Responds with the stream's current scale numerator portion.
     * 
     * @return the scale numerator.
     */
    public int getStreamScaleNum() {
        return m_scaleNum;
    }

    /**
     * Responds with the stream's current scale denominator portion.
     * 
     * @return the scale denominator.
     */
    public int getStreamScaleDenom() {
        return m_scaleDenom;
    }

    /** handle of the stream control object. */
    private IVodStreamControl m_streamControl;

    /** ACT for this event notification. */
    private Object m_act;

    /** LSC stream handle. */
    private int m_streamHandle;

    /** status code returned in this response. */
    private int m_statusCode;

    /** NPT returned in this response. */
    private int m_lastNpt;

    /** rate numerator for this response. */
    private int m_scaleNum;

    /** rate denominator for this response. */
    private int m_scaleDenom;

    /** state of this stream as indicated by server. */
    private int m_streamState;
}

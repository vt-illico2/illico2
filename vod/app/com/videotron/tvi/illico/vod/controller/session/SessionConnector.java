/*  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session;

import java.io.IOException;
import java.net.UnknownHostException;

import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionEventListener;

/**
 * @author LSC
 * @version 1.0 SessionConnector. Abstract layer between the Application and sspVersion implementation. It configures
 *          data needed for session setup and transmits the data to DNCS.
 */
public interface SessionConnector {

    /**
     * This method is called when one wishes to redefine the system UU configuration parameters such as messageTimeout
     * and the servers in which to communicate with.
     * 
     * @throws UnknownHostException
     *             Thrown to indicate that the IP address of a host could not be determined.
     */
    public void initialize() throws UnknownHostException;

    /**
     * sends ClientSessionSetupRequest message. The message is sent to the SRM.
     * 
     * @param sspId
     *            A unique id of Video. It is used as session name.
     * @return - Returns null if successful otherwise, an error String.
     */
    public String sessionRequest(String sspId) throws IOException;

    /**
     * sends ClientSessionReleaseRequest message.
     * 
     * @param sspId
     *            A unique id of Video. It is used as session name.
     * @param reasonCode
     *            A reason code as defined by ISO/IEC 13818-6:1998(E) table 4-59.
     * @return Returns null if successful otherwise, an error String.
     */
    public String releaseRequest(String sspId, int reasonCode);

    /**
     * Adds a listener to the distribution list.
     * 
     * @param de
     *            The listener to add.
     */
    public void addDSMCCSessionEventListener(DSMCCSessionEventListener de);

    /**
     * Removes a listener from the list.
     * 
     * @param de
     *            The listener to remove
     */
    public void removeDSMCCSessionEventListener(DSMCCSessionEventListener de);

    /**
     * Translate a String session name into an array[10] SRM name.
     * 
     * @param sessID
     *            - The BMS name of the session
     * @return the 10 byte SRM name of the session.
     */
    public byte[] getSessionID(String sessID);
}

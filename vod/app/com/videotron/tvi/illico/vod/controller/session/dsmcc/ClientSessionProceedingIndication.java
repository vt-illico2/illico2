package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * UNClientSessionProceedingIndication See 4.2.11.1 Of ISO/IEC 13818-6:1998(E).
 */
public class ClientSessionProceedingIndication extends SessionMessage implements NetworkToClientMessage {

    private short m_reason;

    /**
     *
     */
    public ClientSessionProceedingIndication() {
        super();
    }

    /**
     *
     * Returns the reason field specified in 4.2.11.1 of the ISO/IEC 13818-6 1998(E) spec. Reason
     * codes are not specified. Great eh?
     *
     * @return reason - The reason code for the delay
     */
    public int getReason() {
        return m_reason;
    }

    /**
     *
     * Parse the input byte array into a UNClientSessionProceedingIndication.
     *
     * @param data
     */
    public void parse(byte[] data) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data, 0, data.length);
        DataInputStream in = new DataInputStream(bais);

        try {
            // Parse in the DSMCC header and SessionID
            super.parse(in);

            // Parse in the reason for this delay
            m_reason = in.readShort();

            in.close();
            in = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (in != null) {
                try {
                    in.close();
                    in = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }

    }

}

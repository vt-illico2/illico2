package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * CAResource.
 */
public class CAResource extends SessionResource {

    private short m_CASystemID;

    private short m_CAInfoLength;

    private byte[] m_CAInfo;

    private short m_fieldType;

    private int m_originalMessageLength;

    /**
     *
     * @param rba
     *            - DataInputStream to read data from.
     * @param sRes
     *            - SessionResource that contains the initial parsing.
     */
    public CAResource(DataInputStream rba, SessionResource sRes) {

        // Copy the initial parse into this object.
        super.copy(sRes);

        // The VM must support marks for this DataInputStream.
        if (!rba.markSupported()) {
            Log.printDebug("MARK is NOT supported!");
            return;
        }

        // Note that the SA PodHandler ACPackage BLOB
        // is not the same as the caInfoByte from the sspVersion spec.
        // The ACPackage requires the BLOB to start from
        // the CaSystemId and run to the last byte of caInfoByte

        try {
            int messageCount = super.getFieldLength();

            m_originalMessageLength = messageCount;

            m_fieldType = rba.readShort();

            rba.mark(10);

            m_CASystemID = rba.readShort();

            m_CAInfoLength = rba.readShort();

            rba.reset();

            if (m_CAInfoLength < 5000) {
                m_CAInfo = new byte[m_CAInfoLength + 4];
            } else {
                m_CAInfo = new byte[0];
            }

            rba.readFully(m_CAInfo);

            // Strip off any junk just in case I missed something.
            messageCount -= (6 + m_CAInfo.length);

            // For some reason, there are additional bytes here. So, read and
            // discard them.
            if (messageCount > 0) {
                rba.skip(messageCount);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * getCASystemID, Returns the Conditional Access System ID.
     *
     * @return
     */
    public short getCASystemID() {
        return m_CASystemID;
    }

    /**
     *
     * getCAInfoLength, Returns the Conditional Access BLOB length.
     *
     * @return
     */
    public short getCAInfoLength() {
        return m_CAInfoLength;
    }

    /**
     *
     * getCAInfo, Returns an array containing the Conditional Access BLOB.
     *
     * @return
     */
    public byte[] getCAInfo() {
        if (Log.DEBUG_ON) {
            Log.printDebug(toString());
        }
        return m_CAInfo;
    }

    /**
     * This is strictly debugging code.
     */
    public String toString() {
        StringBuffer sb = null;

        sb = new StringBuffer("CA Resource Data \n");
        sb.append("FieldType = ");
        sb.append(m_fieldType);
        sb.append("\nCA System ID = ");
        sb.append(m_CASystemID);
        sb.append("\nCA Info Length = ");
        sb.append(m_CAInfoLength);
        sb.append("\nCA Message Count = ");
        sb.append(m_originalMessageLength);
        sb.append("\n");
        String s2 = hexDump(m_CAInfo);
        sb.append(s2);

        return sb.toString();
    }

    /**
     * This is strictly debugging code.
     *
     * @param data
     * @return
     */
    private static String hexDump(byte[] data) {
        String rc = null;
        if (Log.DEBUG_ON) {
            if (data == null || data.length == 0) {
                return "[]";
            }

            String[] tmpl = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e",
                    "f" };
            int lc = 208;
            StringBuffer sb = new StringBuffer();
            /*
             * try to line up for easy comparison with sniffer dump
             */
            sb.append(lc);
            sb.append(": 0e00 012d ");
            for (int i = 0, j = 5; i < data.length; i++, j++) {
                byte hi = (byte) ((data[i] & 0xF0) >>> 4);
                byte low = (byte) (data[i] & 0x0F);

                sb.append(tmpl[hi]);
                sb.append(tmpl[low]);

                if ((j % 16) == 0) {
                    sb.append("\n");
                    sb.append(lc + j);
                    sb.append(": ");
                } else {
                    if ((j & 1) == 0) {
                        sb.append(" ");
                    }
                }
            }
            sb.append("\n");

            rc = sb.toString();
        }
        return rc;
    }

}

package com.videotron.tvi.illico.vod.controller.stream;

import java.net.InetAddress;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCResultParser;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.SessionDescriptor;

public class VodHandlerImpl implements IVodHandler {

    /**
     * Constructor: retrieve a SessionHandler for this VOD Handler.
     */
    public VodHandlerImpl() {
        try {
            // SessionManager sm = SessionManager.getInstance();
            // m_session = sm.newSessionHandler();
            m_listeners = new Vector();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnect(Object act) {
    	if (m_streamControl != null) {
    		m_streamControl.disconnect();
    	}
    }

    /**
     * Subscribes a listener to Vod events from the associated <code>IVodHandler</code>
     * object.
     *
     * @param listener
     *            The <code>IVodHandlerListener</code> to subscribe receive Vod events.
     */
    public void addVodListener(IVodHandlerListener listener) {
        if (!this.m_listeners.contains(listener)) {
            this.m_listeners.addElement(listener);
        }
    }

    /**
     * Unsubscribes the listener to receiving Vod events from this VodHandler.
     *
     * @param listener
     *            The <code>IVodHandlerListener</code> to unsubscribe.
     */
    public void removeVodListener(IVodHandlerListener listener) {
        this.m_listeners.removeElement(listener);
    }

    /**
     * Called to acquire the stream control object used to control the Vod stream.
     *
     * @return <code> One or more IVodStreamControl</code> objects or null if connection failed.
     */
    public IVodStreamControl[] getStreams() {
        IVodStreamControl[] controlArray = new IVodStreamControl[1];

        if (m_streamControl == null) {
            return null;
        }

        controlArray[0] = m_streamControl;
        return controlArray;
    }

    /**
     * returns the LSCP IP (to diags).
     *
     * @return a InetAddress containing the LSCP IP address
     */
    public static InetAddress getLscpIP() {
        return m_lscpIP;
    }

    /**
     * returns the LSCP port (to diags).
     *
     * @return a int containing the LSCP port
     */
    public static int getLscpPort() {
        return m_lscpPort;
    }

    public void setServers(int sessionGW, int secondarySessionGW, String serviceGW, String serviceName, int appServer)
            throws Exception {

    }

    public int addDescriptor(int type, int tag, int length, int offset, byte[] data) {
        return -1;
    }

    /**
     * Responds with the connection status of this object.
     *
     * @see IVodHandler#STATE_CONNECTED
     * @see IVodHandler#STATE_DISCONNECTED
     *
     * @return The state of the connection.
     */
    public int getConnectionState() {
        return m_connectionState;
    }

    /**
     * notify listeners of VodHandler event.
     */
    private void notifyListeners(VodHandlerEvent event) {
        if (this.m_listeners != null) {
            Object tmpListeners[];
            synchronized (this.m_listeners) {
                tmpListeners = new Object[this.m_listeners.size()];
                this.m_listeners.copyInto(tmpListeners);
            }

            for (int i = 0; i < tmpListeners.length; i++) {
                /**
                 * Note: because the VOD events are practically immutable, it should be ok to distribute this single
                 * event instance to all listeners.
                 */
                IVodHandlerListener listener = (IVodHandlerListener) tmpListeners[i];
                listener.vodHandlerEventUpdate(event);
            }
        }
    }

    /**
     * Create the stream control object(s) in response to a successful session setup returns false on error.
     */
    public boolean createStreamControls() {
        int streamH = -1;
        java.net.InetAddress destIP = null;
        int destPort = -1;

        // jas modified
        destPort = getPortFromUserData();
        destIP = getIpFromUserData();
        streamH = getStreamHandlerFromUserData();

        // check that we found all relevant resources
        if (streamH == -1 || destIP == null || destPort == -1) {
            return false;
        }

        // create the stream control object
        try {
            m_streamControl = new StreamControlImpl(this, destIP, destPort, streamH);
        } catch (java.io.IOException e) {
            Log.printDebug("VOD Handler - IOException attempting to create stream control!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    int getPortFromUserData() {
        SessionDescriptor[] desc;
        int tag = 0;
        byte[] data;
        int port = 0;

        // desc =
        // userPrivateData.getDescriptors(ISessionHandleType.SETUP_COMPLETE_DESCRIPTOR_TYPE);
        desc = userPrivateData.getSessionDescriptor();

        for (int i = 0; i < desc.length; i++) {
            tag = desc[i].getTag();
            switch (tag) {
            case 0x03: // ip and port
                data = desc[i].getData();
                port = (((int) data[0] << 8) & 0x0000ff00) | (((int) data[1]) & 0x000000FF) & 0x0000FFFF;
                // return port;
                break;
            default:
                break;
            }
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("Video Server Port # is: " + port);
        }

        m_lscpPort = port;

        return port;
    }

    private InetAddress getIpFromUserData() {
        SessionDescriptor[] desc;
        int tag = 0;
        byte[] data;
        InetAddress lscIp = null;

        // desc =
        // userPrivateData.getDescriptors(ISessionHandleType.SETUP_COMPLETE_DESCRIPTOR_TYPE);
        desc = userPrivateData.getSessionDescriptor();

        for (int i = 0; i < desc.length; i++) {
            tag = desc[i].getTag();
            switch (tag) {
            case 0x03:// int stream handle
                data = desc[i].getData();
                try {
                    int ip1 = (((int) data[2])) & 0x000000FF;
                    int ip2 = (((int) data[3])) & 0x000000FF;
                    int ip3 = (((int) data[4])) & 0x000000FF;
                    int ip4 = (((int) data[5])) & 0x000000FF;

                    String ipAdd = ip1 + "." + ip2 + "." + ip3 + "." + ip4;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("Video Server LSCP IP addr is: " + ipAdd);
                    }
                    lscIp = java.net.InetAddress.getByName(ipAdd);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("Video Server IP lookup SUCCESS");
                    }
                    m_lscpIP = lscIp;
                    return lscIp;
                } catch (java.net.UnknownHostException e) {
                    Log.printDebug("InetAddress.getByName caught unknown host exception: " + e);
                    e.printStackTrace();
                }
                break;
            default:
                break;
            }
        }
        return lscIp;
    }

    private int getStreamHandlerFromUserData() {
        SessionDescriptor[] desc;
        int tag = 0;
        byte[] data;
        int strmH = 0;

        // desc =
        // userPrivateData.getDescriptors(ISessionHandleType.SETUP_COMPLETE_DESCRIPTOR_TYPE);
        desc = userPrivateData.getSessionDescriptor();

        for (int i = 0; i < desc.length; i++) {
            tag = desc[i].getTag();

            switch (tag) {
            case 0x04:// int stream handle
                data = desc[i].getData();
                strmH = (((int) data[0] << 24) & 0xFF000000) + (((int) data[1] << 16) & 0x00FF0000)
                        + (((int) data[2] << 8) & 0x0000FF00) + (((int) data[3]) & 0x000000FF);
                break;
            default:
                break;
            }
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("Video Server stream handle is: " + strmH + " = " + Integer.toHexString(strmH));
        }

        return strmH;
    }

    /**
     *
     * getSessionID, Used to obtain the SRM 10 byte session ID.
     *
     * @return the SRM byte[10] name of the session.
     */
    public byte[] getSessionID() {
        return null;
    }

    /**
     * VOD Handler event listeners.
     */
    private Vector m_listeners = null;

    /**
     * boolean: have the servers been set?
     */
    private boolean m_serversSet = false;

    /**
     * connection state.
     */
    private int m_connectionState = STATE_DISCONNECTED;

    /**
     * Stream Control - do we need to handle multiple for this impl?
     */
    private StreamControlImpl m_streamControl = null;

    /**
     * IP address for the LSCP server - used by diags.
     */
    private static InetAddress m_lscpIP;

    /**
     * port for the LSCP server - used by diags.
     */
    private static int m_lscpPort;

    private DSMCCResultParser userPrivateData;

    /**
     * @param userPrivateData
     *            the userPrivateData to set
     */
    public void setUserPrivateData(DSMCCResultParser userPrivateData) {
        this.userPrivateData = userPrivateData;
    }

}

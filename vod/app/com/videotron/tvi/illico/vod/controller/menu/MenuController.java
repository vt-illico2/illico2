package com.videotron.tvi.illico.vod.controller.menu;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.Vector;

import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.SweepEffect;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.PromotionBanner;
import com.videotron.tvi.illico.ui.PromotionBanner.BannerData;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.VideoOutputUtil;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.vod.app.App;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ListComp;
import com.videotron.tvi.illico.vod.comp.MenuTreeUI;
import com.videotron.tvi.illico.vod.comp.PopAddWishlist;
import com.videotron.tvi.illico.vod.comp.PopChangeView;
import com.videotron.tvi.illico.vod.comp.PopOrderResult;
import com.videotron.tvi.illico.vod.comp.PopPlayAll;
import com.videotron.tvi.illico.vod.comp.PopRapid;
import com.videotron.tvi.illico.vod.comp.PopSelectItem;
import com.videotron.tvi.illico.vod.comp.PopSelectOption;
import com.videotron.tvi.illico.vod.comp.PopSubscribe;
import com.videotron.tvi.illico.vod.comp.ShowcaseView;
import com.videotron.tvi.illico.vod.comp.SimilarContentView;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VODServiceImpl;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.backend.BookmarkServerConfig;
import com.videotron.tvi.illico.vod.data.config.ConfigurationFileReceiver;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.AvailableSource;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Category;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Playout;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Section;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Service;
import com.videotron.tvi.illico.vod.data.vcds.type.Showcase;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.TopicPointer;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.data.vcds.type.VideoDetails;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.ui.AboutChannelUI;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.BundleDetailUI;
import com.videotron.tvi.illico.vod.ui.CarouselViewUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.KaraokeListViewUI;
import com.videotron.tvi.illico.vod.ui.ListOfTitlesUI;
import com.videotron.tvi.illico.vod.ui.ListViewUI;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;
import com.videotron.tvi.illico.vod.ui.SeriesOptionUI;
import com.videotron.tvi.illico.vod.ui.TitleDetailUI;
import com.videotron.tvi.illico.vod.ui.UITemplateList;
import com.videotron.tvi.illico.vod.ui.WallofPostersFilmInfoUI;

public class MenuController implements ScreenSaverConfirmationListener,
		ClockListener, MenuListener, LayeredKeyHandler {

	public static final int EXIT_NORMAL = 0;
	public static final int EXIT_TO_MENU = 1;
	public static final int EXIT_FROM_WATCH = 2;
	public static final int EXIT_NO_ACTION = 3;
	public static final int EXIT_BY_HOTKEY = 4;
	public static final int EXIT_BY_LAST = 5;
	public static final int EXIT_TO_CHANNEL = 6;
	public static final int EXIT_BY_ERROR = 7;
	public static final String ASSET = "ASSET_";
	public static final String WISHLIST = "WISHLIST";
	public static final String LAST_VIEW = "VOD_LAST_VIEW";
	public static final String KARA_LIST = "vod.karaoke.playlist";
	public static final String BANNER_CONTENT = "banner_content";
	public static final String BANNER_CATEGORY = "banner_category";
	public static final String SEARCH = "Search";
	public static final String WIZARD = "Wizard";
	public static final String AGENT = "Agent";
	public static final String AGENT_CONTENT = "AgentContent";
	public static final String AGENT_PLAY = "AgentPlay";
	public static final String AGENT_PREVIEW = "AgentPreview";
	public static final String AGENT_ORDER = "AgentOrder";
	// R5
	public static final String EPG = "EPG";
	// R5 - TANK
	public static final String MENU = "Menu";
	public static final String MENU_PARAM_WISHLIST = "WISHLIST";
	public static final String MENU_PARAM_RESUMEVIEWING = "RESUMEVIEWING";
	public static final String MENU_PARAM_TOPIC = "TOPIC";
	public static final int SCROLL_DELAY = 175;

	private static MenuController instance = new MenuController();

	/* current UI code */
	private  BaseUI currentScene;

	/* Screen instance */
	private int chageViewType;

	private static Object[] sceneData;
	private Stack sceneStack = new Stack();

	public final static int TEMPLATE_ID_IDX = 0;
	public final static int PARAM_DATA_IDX = 1;

	public final static int MENU_CATEGORY = 0; // negative displayed with MainToolUI
	public final static int MENU_SEARCH = -1;
	public final static int MENU_RESUME = -2;
	public final static int MENU_WISHLIST = -3;
	public final static int MENU_RESUME_ADULT = -4;
	public final static int MENU_WISHLIST_ADULT = -5;
	public final static int MENU_CHANNEL_ENV = 100;
	public final static int MENU_CHANNEL = 110;
	public final static int MENU_ABOUT = -10;
	public final static int MENU_KARAOKE = -50;
	public final static int MENU_KARAOKE_PACK = -51;
	public final static int MENU_KARAOKE_CONTENTS = -52;
	public final static int MENU_VSD = 200;
	
	// R5
//	public final static int MENU_ALL_CHANNELS = -500; // R7.3 - removed
//	public final static int MENU_FAVORITE_CHANNELS = -501; // R7.3 - removed
	public final static int MENU_SUBSCRIBED_CHANNELS = -502;
	// R7.3
    public final static int MENU_UNSUBSCRIBED_CHANNELS = -503;

	private Vector navigationHistory = new Vector();
	private String timeAndDate;
	private ConfigurationFileReceiver configurationReceiver;

	// private Boolean loadingAnimationRunning = Boolean.FALSE;
	private Point loadingPoint = new Point(480, 270);

	private boolean openAdultCategory; 
	private int sizeHistoryWhenOpenAdultCategory;
	private boolean openAdultCategoryOld;
	private boolean fromCompanion;
	private boolean showAdultContent;
	private boolean ageUnblocked;
	// private HashSet unblocked = new HashSet(); // changed June 2
	private MoreDetail titleToUnblock;
	private Object objectToPlay;
	private String lastAction;
	private long lastActionTime;
	private BaseElement lastObject;
	private ArrayList playList = new ArrayList();
	// private long karaokeLeaseEnd = 0L;
	// private String karaokeName = "";

	private boolean scrollThread;
	private SweepEffect sweepEffect;

	private String from; // what service called VOD
	private boolean needSearch;
	private boolean isFromSearch;
	private boolean gotoAsset;
	private boolean pauseTemporary;
	private MenuItem lastSelect;
	// R5
	private boolean isGotoCOD;
	private boolean needToBackEPG;
	private ClickingEffect clickingEffect;
	private int displayFavorite;
	private MoreDetail lastWishContent;

	private String lastCategoryId;
	private String sortWishlist = null;
	private String sortResumeViewing = null;
	private String sortNormal = null;

	private int lastView = UITemplateList.LIST_OF_TITLES;
	
	private boolean loadingWishlist;

	private LayeredWindow mainWindow;
	private LayeredUI layeredUI;
	private LayeredUI hiddenUI;
	private boolean isShadowed;

	public String elapsed = "";
	
	private boolean isShowingLoading;
	
	// R5 - TANK
	private String paramKey;
	private boolean unlockedAdultFromSearch;
	private long keyTimeStamp = 0;
	
	// IMPROVEMENT
	long before = 0;
	long after = 0;

	// R7.3
    // VDTRMASTER-6042
    private LayeredWindow popupWindow;
    private LayeredUI popupLayeredUI;

	private MenuController() {
	}

	public static synchronized MenuController getInstance() {
//		if (instance == null) {
//			instance = new MenuController();
//		}
		return instance;
	}

	public void init() {
		Log.printDebug("MenuController, init()");
		configurationReceiver = new ConfigurationFileReceiver();
		configurationReceiver.init();
		// R5
		BaseUI.setMenuController(instance);
		
//		new Thread() {
//			public void run() {
//				try {
//					Thread.sleep(20000);
//					VODServiceImpl.getInstance().showContent("V_33318");
//				} catch (Exception e) {}
//			}
//		}.start();
		
		// VDTRMASTER-5719
		mainWindow = new LayeredWindow() {
			public void notifyClean() {
				isShadowed = false;
			}
			public void notifyShadowed() {
				isShadowed = true;
			}
		};
		mainWindow.setBounds(Resources.fullRec);
		mainWindow.setVisible(true);
		layeredUI = WindowProperty.VOD_MAIN.createLayeredDialog(mainWindow, Resources.fullRec, this);

        // VDTRMASTER-6042
		popupWindow = new LayeredWindow() {
            public void notifyClean() {
                isShadowed = false;
            }
            public void notifyShadowed() {
                isShadowed = true;
            }
        };

        popupWindow.setBounds(Resources.fullRec);
        popupWindow.setVisible(true);
        popupLayeredUI = WindowProperty.VOD_POPUP.createLayeredDialog(popupWindow, Resources.fullRec, this);
        
        
        // Touche project
        VbmController.getInstance().setMenuController(this);
	}

	synchronized public void start() {
		Log.printDebug("MenuController, start(), curScene = " + currentScene);
		
		if (currentScene == null)
			VbmController.getInstance().resetHistoryPath();

		// IMROVEMENT
		before = System.currentTimeMillis();
		
		BookmarkServerConfig conf = BookmarkManager.getInstance().getConfig();
		String ctrl = conf == null ? ("D)" + DataCenter.getInstance()
				.getString("BOOKMARK_URL")) : conf.toString();
		debugList[0].add("ControlServer : " + ctrl);
		debugList[0].add("VCDS : "
				+ DataCenter.getInstance().getString("VCDS_URL"));
		debugList[0].add("Image : "
				+ DataCenter.getInstance().getString("IMAGE_URL"));
		debugList[0].add("DNCS : "
				+ DataCenter.getInstance().getString("DNCS_IP") + ":"
				+ DataCenter.getInstance().getString("DNCS_PORT"));
		debugList[0].add("GW_IP : "
				+ DataCenter.getInstance().getString("GW_IP"));
		debugList[0].add("SVC : "
				+ StringUtil.byteArrayToHexString(
						ServiceGroupController.getInstance()
								.getServiceGroupData()).replace('\n', ' '));
		// Bunding
		debugList[0].add("DDC-090 : " + DataCenter.getInstance().getString("DDC_090"));

		// IMPROVEMENT
		after = System.currentTimeMillis();
		App.printDebug("start 1 = " + (after - before));
		before = System.currentTimeMillis();
		
		Clock.getInstance().addClockListener(this);
		timeAndDate = Formatter.getCurrent().getLongDate();

		pauseTemporary = false;
		loadingWishlist = true;

		layeredUI.activate();
		isShadowed = false;

		// IMPROVEMENT
		after = System.currentTimeMillis();
		App.printDebug("start 2 = " + (after - before));
		before = System.currentTimeMillis();
		
		lastView = DataCenter.getInstance().getInt(LAST_VIEW, lastView);
		// R7.3
        if (lastView == UITemplateList.LIST_OF_TITLES) {
            chageViewType = 0;
        } else if (lastView == UITemplateList.WALL_OF_POSTERS) {
            chageViewType = 1;
        } else if (lastView == UITemplateList.CAROUSEL_VIEW) {
            chageViewType = 2;
        } else if (lastView == UITemplateList.LIST_VIEW) {
            chageViewType = 3;
        }
		
		String[] param = CommunicationManager.getInstance().getParameter();
		
		// IMPROVEMENT
		after = System.currentTimeMillis();
		App.printDebug("start 3 = " + (after - before));
		before = System.currentTimeMillis();
		
		///////////////
//		param = new String[] {
//				"Search", "EP_26573", "VOD", //"EP_5230", "VOD"
				//"banner_category", "N_18007"
				//"banner_content", "EP_5230"
//		};
//		new Thread() {
//			public void run() {
//				needSearch = false;
//				try {
//					Thread.sleep(20000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				try {
//					CommunicationManager.getInstance().searchActionEventListener.actionRequested("VOD", new String[] {"Search", "EP_5231", "VOD"});
//				} catch (RemoteException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}.start();
		/////////////////////
		
		if (param != null && param.length > 0) {
			from = param[0];
			
//			// TEMP, due to Pastile sends "HOT_KEY", "banner_content", "V_1234", remove first one
//			if ((MonitorService.REQUEST_APPLICATION_HOT_KEY.equals(from) || "pastille".equals(from)) && param.length == 3) {
//				from = param[1];
//				param[1] = param[2];
//			}
		} else {
			from = "";
		}
		boolean needPromotion = true;
		boolean needMainScene = false;
		isFromSearch = false;
		isGotoCOD = false;
		needToBackEPG = false;
		displayFavorite = 0;
		
		// R5 - TANK
		paramKey = null;
		keyTimeStamp = 0;
		
		// IMPROVEMENT
		after = System.currentTimeMillis();
		App.printDebug("start 4 = " + (after - before));
		before = System.currentTimeMillis();
		
		if (MonitorService.REQUEST_APPLICATION_LAST_KEY.equals(from)) {
			if (currentScene != null) {
				currentScene.setVisible(true);
				currentScene.updateButtons();
				component(true, currentScene);
			}
			loadingWishlist = false;
		} else {
			
			// IMPROVEMENT
			after = System.currentTimeMillis();
			App.printDebug("start 5 = " + (after - before));
			before = System.currentTimeMillis();
			
			// IMPROVEMENT comment out
//			if (from.startsWith(AGENT) == false) {
//				CatalogueDatabase.getInstance().cleanUp();
//			}
			if (currentScene != null && currentScene instanceof CategoryUI) {
				((CategoryUI) currentScene).stop();
				component(false, currentScene);
				((CategoryUI) currentScene).flushData();
				setCurrentScene(null);
			}
			CategoryUI.setBranded(Integer.MIN_VALUE);

			openAdultCategory = false;
			showAdultContent = false;
			ageUnblocked = false;
			unlockedAdultFromSearch = false;
			// unblocked.clear();

			// CatalogueDatabase.getInstance().cleanUp();
			// lastView = UITemplateList.LIST_OF_TITLES;
			
			if (param != null && param.length >= 2) {
				if (SEARCH.equalsIgnoreCase(from)) {
					UsageManager.getInstance().addUsageLog("1101",
							"Start VOD from Search - " + param[1], "", "");
					isFromSearch = true;
					unlockedAdultFromSearch = Boolean.valueOf(param[3]).booleanValue();
					goToAsset(param[1], true, ASSET + param[1]);
				} else if (WIZARD.equalsIgnoreCase(from)) {
					UsageManager.getInstance().addUsageLog("1101",
							"Start VOD from Wizard - " + param[1], "", "");
					goToAsset(param[1], false, ASSET + param[1]);
				} else if (BANNER_CONTENT.equalsIgnoreCase(param[1])) {
					UsageManager.getInstance().addUsageLog("1101",
							"Start VOD from Promotion banner - " + param[2],
							"", "");
					goToAsset(param[2], false, ASSET + param[2]);
				} else if (BANNER_CATEGORY.equalsIgnoreCase(param[1])) {
					UsageManager.getInstance().addUsageLog("1101",
							"Start VOD from Promotion banner - " + param[2],
							"", "");
					showLoadingAnimation();
//					CatalogueDatabase.getInstance().retrieveCategory(
//							menuAdaptor, param[1], null);
					int pageSize = DataCenter.getInstance().getInt("PAGE_SIZE");
					if (pageSize > 0) {
						pageSize = Math.max(21, pageSize);
					}
					int start = -(pageSize / 2);
					if (lastView == UITemplateList.WALL_OF_POSTERS
							|| lastView == UITemplateList.LIST_VIEW) {
						pageSize += -start;
					}
					CatalogueDatabase.getInstance()
							.retrieveCategoryContents(menuAdaptor,
									param[2], null, start, pageSize, "");
					needSearch = false;
					gotoAsset = false;
				} else if (AGENT_CONTENT.equalsIgnoreCase(from)) {
//					UsageManager.getInstance().addUsageLog("1101",
//							"Start VOD from companion agent - " + param[1],
//							"", "");
//					goToAsset(param[1], false, ASSET + param[1]);
					
					final String contentId = param[1];
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								VODServiceImpl.getInstance().showContent(contentId);
							} catch (RemoteException e) {
								//needMainScene = true; // some exception
							}
						}
					});
					
				} else if (AGENT_PLAY.equalsIgnoreCase(from)) {
					StringTokenizer tok = new StringTokenizer(param[1], ",");
					ArrayList list = new ArrayList();
					while (tok.hasMoreTokens()) {
						list.add(tok.nextToken());
					}
					String[] contentId = new String[list.size()];
					list.toArray(contentId);
					list.clear();
					
					tok = new StringTokenizer(param[2], ",");
					while (tok.hasMoreTokens()) {
						list.add(tok.nextToken());
					}
					boolean[] trailer = new boolean[list.size()];
					for (int i = 0; i < trailer.length; i++) {
						trailer[i] = Boolean.valueOf((String) list.get(i)).booleanValue();
					}
					list.clear();
					
					tok = new StringTokenizer(param[3], ",");
					while (tok.hasMoreTokens()) {
						list.add(tok.nextToken());
					}
					String[] lang = new String[list.size()];
					list.toArray(lang);
					list.clear();
					
					tok = new StringTokenizer(param[4], ",");
					while (tok.hasMoreTokens()) {
						list.add(tok.nextToken());
					}
					String[] definition = new String[list.size()];
					list.toArray(definition);
					list.clear();
					
					try {
						VODServiceImpl.getInstance().playContent(contentId, trailer, lang, definition);
					} catch (RemoteException e) {
						//needMainScene = true; // some exception
					}
				} else if (AGENT_PREVIEW.equalsIgnoreCase(from)) {
					StringTokenizer tok = new StringTokenizer(param[1], ",");
					ArrayList list = new ArrayList();
					while (tok.hasMoreTokens()) {
						list.add(tok.nextToken());
					}
					String[] contentId = new String[list.size()];
					list.toArray(contentId);
					list.clear();
					
					tok = new StringTokenizer(param[2], ",");
					while (tok.hasMoreTokens()) {
						list.add(tok.nextToken());
					}
					String[] lang = new String[list.size()];
					list.toArray(lang);
					
//					try {
//						VODServiceImpl.getInstance().playPreview(contentId, lang);
//					} catch (RemoteException e) {
//						needMainScene = true; // some exception
//					}
				} else if (AGENT_ORDER.equalsIgnoreCase(from)) {
//					try {
//						VODServiceImpl.getInstance().showOrder(param[1], param[2], param[3]);
//					} catch (RemoteException e) {
//						needMainScene = true; // some exception
//					}
				} else if (EPG.equals(from)) { // R5
					needToBackEPG = true;
					if (param[1].equals("CHANNEL_ON_VOD")) {
						isGotoCOD = true;
						goToMainScene(true);
					} else if (param[1].startsWith("CALL_")) {
						String callLetter = param[1].substring("CALL_".length());
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveChannel(menuAdaptor, callLetter, null);
					} else {
						// VDTRMASTER-5579
						VbmController.getInstance().writeSimilarContent(param[1]);
						goToAsset(param[1], false, ASSET + param[1]);
					}
				} else if (MENU.equals(from)) {
					goToSceneForMenu(param);
				} else {
					needMainScene = true; // unknown param
				}
			} else {
				needMainScene = true; // no param
			}
			
			// IMPROVEMENT
			after = System.currentTimeMillis();
			App.printDebug("start 6 = " + (after - before));
			before = System.currentTimeMillis();
			
			if (needMainScene) {
				UsageManager.getInstance().addUsageLog("1101", "Start VOD!",
						"", "");
				needPromotion = false;
				goToMainScene(true);
				needSearch = false;
				gotoAsset = false;
			}

			// IMPROVEMENT
			after = System.currentTimeMillis();
			App.printDebug("start 7 = " + (after - before));
			before = System.currentTimeMillis();
		}
		
		if (MonitorService.REQUEST_APPLICATION_HOT_KEY.equals(from)) {
			VbmController.getInstance().writeParentApp("Remote");
//		} else if (MonitorService.REQUEST_APPLICATION_MENU.equalsIgnoreCase(from)) {
//			VbmController.getInstance().writeParentApp("Menu");
//		} else if (BANNER_CATEGORY.equals(from) || BANNER_CONTENT.equals(from) || "Dashboard".equalsIgnoreCase(from)) {
//			VbmController.getInstance().writeParentApp("Dashboard");
//		} else if ("".equals(from)) {
//			VbmController.getInstance().writeParentApp("TV");
		} else if (param != null && param.length == 3 && param[2].equalsIgnoreCase("TV")) {
			VbmController.getInstance().writeParentApp("TV");
		} else if (param != null && param.length > 0 && param[0] != null) {
			VbmController.getInstance().writeParentApp(param[0]);
		} else {
			VbmController.getInstance().writeParentApp("");
		}
		
		if (needPromotion) {
			PromotionBanner.getInstance().startMonitoring();
			PromotionBanner.getInstance().setRotationTimer(true);
		}
		
		// IMPROVEMENT
		after = System.currentTimeMillis();
		App.printDebug("start 8 = " + (after - before));
		before = System.currentTimeMillis();
		
		scrollThread = true;
		new Thread("scrollThread") {
			public void run() {
				while (scrollThread) {
					try {
						Thread.sleep(SCROLL_DELAY);
					} catch (InterruptedException e) {
					}
					if (currentScene != null) {
						currentScene.scroll();
					}
					
					if (displayFavorite > 0) {
						displayFavorite--;
						
						if (displayFavorite == 0) {
							if (currentScene != null) {
								currentScene.repaint();
							}
						}
					}
				}
			}
		}.start();
	}

	synchronized public boolean pause() {
		Log.printDebug("MenuController, pause(), temp = " + pauseTemporary);
		Clock.getInstance().removeClockListener(this);
		if (layeredUI != null) {
			layeredUI.deactivate();
			// VDTRMASTER-5719
//			WindowProperty.dispose(layeredUI);
		}

		// VDTRMASTER-6042
        if (popupLayeredUI != null) {
		    popupLayeredUI.deactivate();
        }

		PopChangeView.getInstance().stop();
		scrollThread = false;

		if (pauseTemporary) {
			if (currentScene != null) {
				if (currentScene instanceof PlayScreenUI) {
					back();
				}
				currentScene.setVisible(false);
			}
			return false;
		}
		
		if (pauseTemporary == false) {
			openAdultCategory = false;
			showAdultContent = false;
			ageUnblocked = false;
		}

		if (hiddenUI != null) {
			WindowProperty.dispose(hiddenUI);
			hiddenUI = null;
		}
		for (int i = 0; i < debugList.length - 1; i++) {
			debugList[i].clear();
		}
		debugIdx = 0;
		hiddenIdx = 0;

		while (currentScene != null) {
			currentScene.stop();
			component(false, currentScene);
			currentScene.flushData();
			setCurrentScene(null);
			if (sceneStack.empty() == false) {
				setCurrentScene((BaseUI) sceneStack.pop());
			}
		}
		setCurrentScene(null);
		backwardHistory(true);
		sceneData = null;
		pauseTemporary = false;

		playList.clear();

		sortWishlist = null;
		// VDTRMASTER-5655
//		sortResumeViewing = null;
		sortNormal = null;

		// VDTRMASTER-5719
//		mainWindow = null;
		
		BaseRenderer.flushBrandingBG();
		BaseRenderer.i00Vtlogo = null;
		
		ChannelInfoManager.getInstance().flushData();
		
		return true;
	}

	public void stop() {
		Log.printDebug("MenuController, stop()");
		FrameworkMain.getInstance().getHScene().setVisible(false);
		FrameworkMain.getInstance().getHScene().removeAll();

		PromotionBanner.getInstance().setRotationTimer(false);
		backwardHistory(true);
		
		hideLoadingAnimation();
		keyTimeStamp = 0;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}
	
	private void setCurrentScene(BaseUI newScene) {
		Log.printDebug("MenuController, setCurrentScene(), newScene = " + newScene);
		currentScene = newScene;
	}
	
	public boolean isShadowed() {
		return isShadowed;
	}

	private void component(boolean add, Component c) {
		if (c == null || mainWindow == null) {
			return;
		}
		if (add) {
			if (c instanceof BaseUI && c instanceof PlayScreenUI == false
			/* && c instanceof FullTrailerUI == false */) {
				Component[] child = ((BaseUI) c).getComponents();
				boolean hasBanner = false;
				for (int i = 0; i < child.length; i++) {
					if (child[i] instanceof PromotionBanner) {
						hasBanner = true;
						break;
					}
				}
				if (!hasBanner) {
					((BaseUI) c).add(PromotionBanner.getInstance());
				}
			}
			//FrameworkMain.getInstance().addComponent(c);
			mainWindow.add(c);
		} else {
			//FrameworkMain.getInstance().removeComponent(c);
			mainWindow.remove(c);
		}
	}

	// R7.3
    // VDTRMASTER-6042
    public void addPopup(UIComponent c) {
        if (c == null || popupWindow == null) {
            return;
        }

        if (c != null) {
            popupWindow.add(c);
            popupLayeredUI.activate();
        } else {
            popupLayeredUI.deactivate();
        }
    }

    public void removePopup(UIComponent c) {
        if (c == null || popupWindow == null) {
            return;
        }

        popupWindow.remove(c);
        popupLayeredUI.deactivate();
    }
	
	public void setPlaylist(ArrayList list) {
		playList = list;
	}

	public ArrayList getPlaylist() {
		return playList;
	}
	
	public BaseElement getLastObject() {
		return lastObject;
	}

	// sort - 0:none, 1:alpha, 2:by date, 3:by prod date, 4:default
	public MenuItem getOptionMenu(int sort, BaseElement element) {
		Log.printDebug("getOptionMenu, sort = " + sort + ", element = " + element);
		MenuItem optionMenu = new MenuItem("menu");
		if (sort != 0) {
			MenuItem menuSort = new MenuItem(Resources.OPT_SORT);
			MenuItem item = new MenuItem(Resources.OPT_NAME);
			if (sort == 1) {
				item.setChecked(true);
			}
			menuSort.add(item);

			boolean sortNormal = true;
			String key = Resources.OPT_DATE;
			if (currentScene instanceof ListViewUI) {
				if (((ListViewUI) currentScene).isWishList()) {
					key = Resources.OPT_DATE_WISHLIST;
					sortNormal = false;
				} else if (((ListViewUI) currentScene).isResumeViewing()) {
					key = Resources.OPT_DATE_RESUME_VIEWING;
					sortNormal = false;
				}
			}
			item = new MenuItem(key);
			if (sort == 2) {
				item.setChecked(true);
			}
			menuSort.add(item);
			
			if (sortNormal) {
				item = new MenuItem(Resources.OPT_PROD_YEAR);
				if (sort == 3) {
					item.setChecked(true);
				}
				menuSort.add(item);
				
				item = new MenuItem(Resources.OPT_DEFAULT);
				if (sort == 4) {
					item.setChecked(true);
				}
			menuSort.add(item);
			}
			if (currentScene instanceof ListViewUI == false || ((ListViewUI) currentScene).isExplored() == false) {
				optionMenu.add(menuSort);
			}
		}

        // R7.3
        // VDTRMASTER-6105
        optionMenu.add(new MenuItem(Resources.TEXT_SEARCH));

		if ((currentScene instanceof ListViewUI == false || ((ListViewUI) currentScene).isExplored() == false)) {
			if ((element instanceof Video && CommunicationManager.getInstance().checkPackageForWishList(((Video) element).service)) || element instanceof Bundle) {
				// VDTRMASTER-5602
				if (BookmarkManager.getInstance().isInWishlist((MoreDetail) element) != null) {
					MenuItem item = new MenuItem(Resources.TEXT_REMOVE_FROM_WISHLIST);
					if ((element instanceof Video && BookmarkManager.getInstance().isAdult((Video) element))
							|| (element instanceof Bundle && BookmarkManager.getInstance().isAdult((Bundle) element))) { // VDTRMASTER-5621
						item = new MenuItem(Resources.TEXT_REMOVE_FROM_WISHLIST_18);
					}
					item.setData(element);
					optionMenu.add(item);
				} else {
					MenuItem item = new MenuItem(Resources.TEXT_ADDTO_WISHLIST);
					// VDTRMASTER-5602
					if ((element instanceof Video && BookmarkManager.getInstance().isAdult((Video) element))
							|| (element instanceof Bundle && BookmarkManager.getInstance().isAdult((Bundle) element))) { // VDTRMASTER-5621
						item = new MenuItem(Resources.TEXT_ADDTO_WISHLIST_18);
					}
					item.setData(element);
					optionMenu.add(item);
				}
			}
		}
		
		// R5
		Topic topic = null;
		if (currentScene instanceof CategoryUI) {
			CategoryUI ui = (CategoryUI) currentScene;
			if (ui.getCurrentCategory().type == MenuController.MENU_CHANNEL
					&& ui.getCurrentCategory().getTopic() != null) {
				topic = ui.getCurrentCategory().getTopic();
			} else if (ui.getCurrentCategory().type == MenuController.MENU_CHANNEL_ENV 
					&& ui.getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT
					&& ui.getFocusedDataOnShowcaseView() instanceof Showcase
					&& ((Showcase)ui.getFocusedDataOnShowcaseView()).detail.getTopic() != null) {
				topic = ((Showcase)ui.getFocusedDataOnShowcaseView()).detail.getTopic();
			} else if (CategoryUI.getChannelCategory() != null 
					&& CategoryUI.getChannelCategory().type == MenuController.MENU_CHANNEL
					&& CategoryUI.getChannelCategory().getTopic() != null) {
				topic = CategoryUI.getChannelCategory().getTopic();
			}
		} else if (CategoryUI.getChannelCategory() != null 
				&& CategoryUI.getChannelCategory().type == MenuController.MENU_CHANNEL
				&& CategoryUI.getChannelCategory().getTopic() != null
				&& (currentScene instanceof ListViewUI && !((ListViewUI)currentScene).isWishList())) { // VDTRMASTER-5615 & VDTRMASTER-5616
			topic = CategoryUI.getChannelCategory().getTopic();
		}
		
		if (topic != null) {
			ChannelInfoManager chManager = ChannelInfoManager.getInstance();
			
			// VDTRMASTER-5661
			if (chManager.isFavoriteChannel(topic)) {
				optionMenu.add(new MenuItem(Resources.OPT_REMOVE_FAVORITE_CHANNEL));
			} else if (chManager.isSubscribed(topic)) {
				optionMenu.add(new MenuItem(Resources.OPT_ADD_FAVORITE_CHANNEL));
			}
			
			// VDTRMASTER-5634
			if (chManager.isBlockedChannelForOption(topic)) {
				optionMenu.add(new MenuItem(Resources.OPT_REMOVE_BLOCKED_CHANNEL));
			} else {
				optionMenu.add(new MenuItem(Resources.OPT_ADD_BLOCKED_CHANNEL));
			}
		}

		optionMenu.add(new MenuItem(Resources.OPT_GO_SETTING));
		optionMenu.add(new MenuItem(Resources.OPT_GO_HELP));
		if (DataCenter.getInstance().getString(RightFilter.PARENTAL_CONTROL)
				.equals(Definitions.OPTION_VALUE_OFF)) {
			optionMenu.add(new MenuItem(Resources.OPT_ACTIVATE_PARENTAL));
		} else if (DataCenter.getInstance()
				.getString(RightFilter.PARENTAL_CONTROL)
				.equals(Definitions.OPTION_VALUE_ON)) {
			optionMenu.add(new MenuItem(Resources.OPT_REMOVE_PARENTAL));
		} else {
			optionMenu.add(new MenuItem(Resources.OPT_APPLY_PARENTAL));
		}
		
		
		return optionMenu;
	}

	public void viewChange(Object elements, Object focused) {
		Log.printInfo("MenuController, viewChange()");

		currentScene.stop();
		component(false, currentScene);

		boolean loopData = false;
		
		int sceneCode = 0;
		switch (chageViewType) {
		case PopChangeView.VIEW_NORMAL:
			sceneCode = UITemplateList.LIST_OF_TITLES;
			loopData = true;
			break;
		case PopChangeView.VIEW_WALL:
			sceneCode = UITemplateList.WALL_OF_POSTERS;
			loopData = false;
			break;
		case PopChangeView.VIEW_CARROUSEL:
			sceneCode = UITemplateList.CAROUSEL_VIEW;
			loopData = true;
			break;
		case PopChangeView.VIEW_LIST:
			sceneCode = UITemplateList.LIST_VIEW;
			loopData = false;
			break;
		default:
			break;
		}
		
		int pageSize = DataCenter.getInstance().getInt("PAGE_SIZE");
		if (pageSize > 0 && elements instanceof BaseElement[]) {
			BaseElement[] data = (BaseElement[]) elements;
			boolean needRetrieving = false;
			for (int i = 0; i < data.length; i++) {
				if (data[i] == null) {
					needRetrieving = true;
					break;
				}
			}
			int start = 0;
			if (needRetrieving && loopData) {
				if (data[data.length - 1] == null) {
					pageSize /= 2;
					start = data.length - pageSize;
				} else {
					needRetrieving = false;
				}
			} else if (needRetrieving && loopData == false) {
				boolean inFirst = false;
				boolean inLast = false;
				for (int i = 0; focused != null && i < data.length; i++) {
					if (focused.equals(data[i])) {
						if (i < pageSize) {
							inFirst = true;
							break;
						} else if (i >= data.length - pageSize) {
							inLast = true;
							break;
						}
					}				
				}
				if (inFirst) {
					while (data[start++] != null);
					start--;
					
					if (start < pageSize) {
						pageSize -= start;
					} else {
						needRetrieving = false;
					}
				} else if (inLast) {
					start = data.length - pageSize;
					while (data[start-1+pageSize--] != null);
					pageSize++;
					if (pageSize <= 0) {
						needRetrieving = false;
					}
				}
			}
			if (needRetrieving) {
				synchronized (CatalogueDatabase.getInstance()) {
					CatalogueDatabase.getInstance().
					retrieveCategoryContents(null, null, null, start, pageSize, "");
					try {
						CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {}
				} 
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				if (cat != null) {
					elements = cat.totalContents;
				}
			}
		}

		setCurrentScene(createScene(sceneCode));
		MenuController.sceneData = new Object[] { new Integer(sceneCode),
				elements, focused };
		component(true, currentScene);
		currentScene.start(false);

		lastView = sceneCode;
		PreferenceProxy.getInstance().setPreference(LAST_VIEW, String.valueOf(lastView));
	}

	public int getLastView() {
		return lastView;
	}

	/**
	 * @return the chageViewType
	 */
	public int getChageViewType() {
		return chageViewType;
	}

	/**
	 * @param chageViewType
	 *            the chageViewType to set
	 */
	public void setChageViewType(int chageViewType) {
		this.chageViewType = chageViewType;
	}


	public void purchaseContent(final BaseElement element, final BaseUI listener) {
		if (EventQueue.isDispatchThread()) {
			new Thread("purchaseContent") {
				public void run() {
					purchaseContent(element, listener);
				}
			}.start();
			return;
		}
		showLoadingAnimation();
		int valid = BookmarkManager.getInstance().validateSTB();
		hideLoadingAnimation();
		
		if (valid != BookmarkManager.NOT_COMPLETE) {
			if (valid == BookmarkManager.NO_ERROR) {
				currentScene.setPopUp(PopSelectOption.getInstance());
				PopSelectOption.getInstance().show(listener != null ? listener : menuAdaptor, element);
			} else {
				CommunicationManager.getInstance().errorMessage("VOD206", null);
			}
		}
	}
	public void purchaseKaraoke(final boolean needPopup) {
		if (EventQueue.isDispatchThread()) {
			new Thread("purchaseKaraoke") {
				public void run() {
					purchaseKaraoke(needPopup);
				}
			}.start();
			return;
		}
		showLoadingAnimation();
		int valid = BookmarkManager.getInstance().validateSTB();
		hideLoadingAnimation();
		if (valid != BookmarkManager.NOT_COMPLETE) {
			if (valid == BookmarkManager.NO_ERROR) {
				CategoryContainer partyPack = CategoryUI.getPartyPack();
				if (partyPack != null) {
					currentScene.setPopUp(PopSelectOption.getInstance());
					PopSelectOption.getInstance().show(menuAdaptor,
							partyPack.categorizedBundle);
				}
			} else {
				CommunicationManager.getInstance().errorMessage("VOD206", null);
			}
		}
	}

	public String getFrom() {
		return from;
	}
	
	public boolean isFromSearch() {
		return isFromSearch;
	}
	
	public void setIsFromSearch(boolean fromSearch) {
		isFromSearch = fromSearch;
	}
	
	// R5
	public boolean isGotoCOD() {
		return isGotoCOD;
	}
	
	// R5
	public void isGotoCOD(boolean gotoCOD) {
		isGotoCOD = gotoCOD;
	}
	
	public boolean needToBackEPG() {
		return needToBackEPG;
	}
	
	public void setNeedToBackEPG(boolean need) {
		this.needToBackEPG = need;
	}
	
	public boolean getGotoAsset() {
		return gotoAsset;
	}
	
	public void goToBundle(Bundle bundle, String bundleId) {
		if (EventQueue.isDispatchThread()) { // loadingbar..
			final Bundle b = bundle;
			final String bId = bundleId;
			new Thread("goToBundle") {
				public void run() {
					goToBundle(b, bId);
				}
			}.start();
			return;
		}
		Log.printDebug("MenuController, goToBundle()" + ", bundle = "
				+ bundle + ", bundleId = " + bundleId);
		MoreDetail[] md = null;
		if (bundleId != null && bundle == null) {
			bundle = (Bundle) CatalogueDatabase.getInstance()
					.getCached(bundleId);
		}
		if (bundle != null) {
			md = bundle.getTitles();
			bundleId = bundle.id;
		}
		
		if (md == null || md.length == 0 || md[0] instanceof Episode) {
			// episode bundle may retrieved as empty because sibling bundle refer it
			synchronized (CatalogueDatabase.getInstance()) {
				showLoadingAnimation();
				CatalogueDatabase.getInstance().retrieveBundle(null,
						bundleId, true, null);
				try {
					CatalogueDatabase.getInstance().wait(
							Constants.MS_PER_MINUTE);
				} catch (InterruptedException e) {
				}
				hideLoadingAnimation();
			}
			bundle = (Bundle) CatalogueDatabase.getInstance()
					.getCached(bundleId);
			md = bundle.getTitles();
		}
		
		// VDTRMASTER-5516
		if (bundle == null && (SEARCH.equals(from) || isFromSearch)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuController, goToBundle, bundle=" + bundle 
						+ ", from=" + from);
				Log.printDebug("MenuController, goToBundle, may error case, so reset needSearch");
			}
			MenuController.getInstance().needSearch = false;
			return;
		}
		
		if (md.length > 0) {
			if (bundle.isMovieBundle()) {
				menuAdaptor.reflectVCDSResult(bundle.id);
				synchronized (VODServiceImpl.getInstance()) {
					VODServiceImpl.getInstance().notifyAll();
				}
			} else {
				Episode episode = (Episode) md[0];
				if (episode.season != null) {
					if (episode.season.notListed != null 
							&& episode.season.notListed.indexOf("i2") == -1) { // backend 367 - no retrieve
						// jira support 75 - enforce retrieving
						episode.seasonRef = episode.season.id;
						episode.season = null;
					}
				}
				if (episode.season == null || episode.season.episodeBundles.length == 0) {
					String seasonId = episode.season == null ? episode.seasonRef : episode.season.id;
					synchronized (CatalogueDatabase.getInstance()) {
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveSeason(
								null, seasonId, null);
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {
						}
						hideLoadingAnimation();
					}
					episode.season = (Season) CatalogueDatabase
							.getInstance().getCached(seasonId);
				}
				Season season = episode.season;
				StringBuffer buf = new StringBuffer();
				for (int i = 0; i < season.episodeBundles.length; i++) {
					if (CatalogueDatabase.getInstance().getCached(season.episodeBundles[i]) != null) {
						continue; // backend 367 - no retrieve
					}
					buf.append(season.episodeBundles[i]);
					buf.append(',');
				}
				if (buf.length() > 0) {
					showLoadingAnimation();
					synchronized (CatalogueDatabase.getInstance()) {
						CatalogueDatabase.getInstance().retrieveBundle(
								null, buf.toString(), false, null);
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {
						}
					}
					hideLoadingAnimation();
				}
				Bundle[] data = new Bundle[season.episodeBundles.length];
				for (int i = 0; i < data.length; i++) {
					data[i] = (Bundle) CatalogueDatabase.getInstance()
							.getCached(season.episodeBundles[i]);
				}
				if (data.length == 0 && season.seasonBundle != null) {
					data = new Bundle[1];
					data[0] = (Bundle) CatalogueDatabase.getInstance()
							.getCached(season.seasonBundle);
				}
				forwardHistory(season);
				goToNextScene(UITemplateList.LIST_OF_TITLES,
						new Object[] {
								new Integer(
										UITemplateList.LIST_OF_TITLES),
								data, bundle }, false);
				synchronized (VODServiceImpl.getInstance()) {
					VODServiceImpl.getInstance().notifyAll();
				}
			}
			if (SEARCH.equals(from) || BANNER_CONTENT.equals(from) || (from != null && from.startsWith(AGENT))) {
				mainScreenReady();
			}
		}
	}
	
	public void goToVSD(CategoryContainer category, BaseUI listener) {
		if (category.getTopic() == null) {
			return;
		}
		Log.printDebug("MenuController, goToVSD()" + ", cat = "
				+ category.toString(false) + ", name = " 
				+ category.getTopic().namedTopic + ", listener = " + listener);
		showLoadingAnimation();
		CatalogueDatabase.getInstance().retrieveVSD(listener, category.getTopic().namedTopic, category.getTopic().id);
	}

	// return true : going to sub-categories, false = going to list or blocked
	public boolean goToCategory(CategoryContainer category, BaseUI listener) {
		Log.printDebug("MenuController, goToCategory()" + ", cat = "
				+ category.toString(false) + ", listener = " + listener);
		UsageManager.getInstance().addUsageLog(getScreenId(),
				category.parent == null ? "" : category.parent.getId(), "1201",
				category.getId());
		if (openAdultCategory == false && category.isAdult) {
			// "A popup asks PIN code even if parental controls are off", refer
			// UI parental controls
			PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.ALLOW_ADULT_CATEGORY,
					null,
					listener,
					new String[] {
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_UNBLOCK_PIN1),
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_UNBLOCK_PIN2) });
			return false;
		}
		Log.printDebug("category.subCategories = "
				+ category.subCategories.length + ", contents = "
				+ category.totalContents.length);
		if (category.categorizedBundle != null && category.categorizedBundle.isFree() == false) {
			CategoryUI.setPartyPack(category);
		}
		if (category.subCategories.length > 0 && category.needRetrieve == false) {
			if (listener == menuAdaptor && currentScene instanceof CategoryUI) { // it may from ShowcaseView
				((CategoryUI) currentScene).moveToSubCategory(category);
			}
			return true;
		} else if (category.subCategoriesAsPoster.length > 0) {
			menuAdaptor.reflectVCDSResult(category);
			return false;
		} else if (category.leafCount == 0) {
			showLoadingAnimation();
			if (category.type == MENU_KARAOKE_PACK) {
				ArrayList list = new ArrayList();
				list.add(category.id);
				CatalogueDatabase.getInstance().retrieveCategorizedBundles(menuAdaptor, list, category);
				PreferenceProxy.getInstance().loadPlaylist(true);
			} else {
				CatalogueDatabase.getInstance().retrieveCategory(menuAdaptor,
					category.id, category);
			}
			return false;
		} else { // leaf category
			boolean allRetrieved = category.totalContents.length > 0;
			for (int i = 0; i < category.totalContents.length; i++) {
				if (category.totalContents[i] == null) {
					allRetrieved = false;
					break;
				}
			}
			if (allRetrieved && category.needRetrieve == false) {
				menuAdaptor.reflectVCDSResult(category);
			} else {
				showLoadingAnimation();
				category.needRetrieve = false;
				int pageSize = DataCenter.getInstance().getInt("PAGE_SIZE");
				if (pageSize > 0) {
					pageSize = Math.max(21, pageSize);
				}
				int start = -(pageSize / 2);
				if (lastView == UITemplateList.WALL_OF_POSTERS
						|| lastView == UITemplateList.LIST_VIEW) {
					pageSize += -start;
				}
//				if (lastView == UITemplateList.LIST_OF_TITLES 
//						|| lastView == UITemplateList.CAROUSEL_VIEW) {
//					
//					start = -(pageSize / 2);
//				}
				
				if (category.getTopic() != null) {
					CatalogueDatabase.getInstance()
					.retrieveNamedTopic(listener, category.getTopic().type, category.getTopic().namedTopic, null);
				} else {
					CatalogueDatabase.getInstance()
						.retrieveCategoryContents(menuAdaptor,
								category.id, category, start, pageSize, "");
				}
			}
			return false;
		}
	}

	private void checkOptionAndGo(Season season, BaseUI listener) {
		// check purchase option
		Log.printDebug("MenuController, checkOptionAndGoTo()" + ", season = "
				+ season.toString(true) + ", canBuyEpiosde = "
				+ season.canBuyEpisode + ", canBuyBundle = "
				+ season.canBuyBundle + ", canBuySeason = "
				+ season.canBuySeason);
		if (listener == null) {
			listener = menuAdaptor;
		}
		int optionCnt = 0;
		if (season.canBuyEpisode) {
			optionCnt++;
		}
		if (season.canBuyBundle) {
			optionCnt++;
		}
		if (season.canBuySeason) {
			optionCnt++;
		}
		if (optionCnt == 0) { // something wrong
			Log.printError("MenuController, there is no option in " + season.toString(true));
			CommunicationManager.getInstance().errorMessage("VOD104", null);
			return;
		}
		if (optionCnt == 1) { // no option
			// VDTRMASTER-5367 [CMS lite][VOD] Branding problem in channel on Demand section
//			if (season.service != null && season.service.serviceType == Service.SVOD) {
//				CategoryUI.setBrandingElement(season.service.description, true);
//			}
			if (season.canBuyEpisode) {
				forwardHistory(season);
				goToNextScene(UITemplateList.LIST_OF_TITLES, season.id);
			} else if (season.canBuyBundle) {
				StringBuffer buf = new StringBuffer();
				for (int i = 0; i < season.episodeBundles.length; i++) {
					// if (CatalogueDatabase.getInstance().getCached(
					// season.episodeBundles[i]) == null) {
					buf.append(season.episodeBundles[i]);
					buf.append(',');
					// }
				}
				if (buf.length() > 0) {
					showLoadingAnimation();
					synchronized (CatalogueDatabase.getInstance()) {
						CatalogueDatabase.getInstance().retrieveBundle(null,
								buf.toString(), false, null);
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {}
					}
					hideLoadingAnimation();
				}
				Bundle[] data = new Bundle[season.episodeBundles.length];
				for (int i = 0; i < data.length; i++) {
					data[i] = (Bundle) CatalogueDatabase.getInstance()
							.getCached(season.episodeBundles[i]);
				}
				forwardHistory(season);
				goToNextScene(UITemplateList.LIST_OF_TITLES, data);
			} else if (season.canBuySeason) {
				showLoadingAnimation();
				synchronized (CatalogueDatabase.getInstance()) {
					CatalogueDatabase.getInstance().retrieveBundle(null,
							season.seasonBundle, false, null);
					try {
						CatalogueDatabase.getInstance().wait(
								Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {}
				}
				hideLoadingAnimation();
				Bundle[] data = new Bundle[] {
						(Bundle) CatalogueDatabase.getInstance()
							.getCached(season.seasonBundle)
				};
				forwardHistory(season);
				goToNextScene(UITemplateList.LIST_OF_TITLES, data);
			}
		} else { // 2 or more options, select option
			StringBuffer buf = new StringBuffer();
			if (season.seasonBundle != null
			/*
			 * && CatalogueDatabase.getInstance().getCached(
			 * season.seasonBundle) == null
			 */) {
				buf.append(season.seasonBundle);
				buf.append(',');
			}
			for (int i = 0; i < season.episodeBundles.length; i++) {
				// if (CatalogueDatabase.getInstance().getCached(
				// season.episodeBundles[i]) == null) {
				buf.append(season.episodeBundles[i]);
				buf.append(',');
				// }
			}
			if (buf.length() > 0) {
				showLoadingAnimation();
				synchronized (CatalogueDatabase.getInstance()) {
					CatalogueDatabase.getInstance().retrieveBundle(null,
							buf.toString(), false, "repaint");
					try {
						CatalogueDatabase.getInstance().wait(
								Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {
					}
				}
				hideLoadingAnimation();
			}
			forwardHistory(season);
			MenuController.getInstance().goToNextScene(
					UITemplateList.SERIES_OPTIONS, season);
		}
	}

	public boolean fromBundleDetail() {
		if (sceneStack.isEmpty()) {
			return false;
		}
		Object lastScene = sceneStack.peek();
		return lastScene instanceof BundleDetailUI;
	}

	public void setNeedSearch() {
		needSearch = true;
	}
	
	public boolean isGoToAsset() {
		return gotoAsset;
	}

	public void goToAsset(String id, boolean needSearch, String cmd) {
		if (Log.INFO_ON) {
			Log.printInfo("MenuController, goToAsset(" + id
					+ "), needSearch = " + needSearch);
		}
		if (id == null || id.length() == 0) {
			goToMainScene(true);
			return;
		}
		if (cmd == null) {
			cmd = id;
		}
		this.needSearch = needSearch;
		if (needSearch) {
			//showAdultContent = true; // already put admin pin in Search
			// 4706
		}
		
		// VDTRMASTER-5691
		if (from != null && EPG.equals(from)) {
			gotoAsset = false;
		} else {
			gotoAsset = true;
		}
			

		id = id.toUpperCase();
		
		ArrayList list = new ArrayList();
		list.add(id);
		if (id.startsWith("EP")) {
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveEpisodes(menuAdaptor, list,
					cmd);
		} else if (id.startsWith("V")) {
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveVideos(menuAdaptor, list,
					cmd);
		} else if (id.startsWith("BU")) {
			goToBundle(null, id);
		} else if (id.startsWith("SR")) {
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveSeries(menuAdaptor, id, cmd);
		} else if (id.startsWith("SS")) {
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveSeason(menuAdaptor, id, cmd);
		} else if (id.startsWith("CE")) {
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveExtra(menuAdaptor, id, cmd);
		} else {
			CommunicationManager.getInstance().errorMessage("VOD201-04", null);
		}
	}

	public void goToKaraoke(CategoryContainer category) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, goToKaraoke(), category = "
					+ category + ", bundle = " + category.categorizedBundle
					+ ", type = " + category.type);
		}

		boolean retrieved = true;
		for (int i = 0; i < category.totalContents.length; i++) {
			if (BaseElement.NA.equals(category.totalContents[i]
					.getDescription())) {
				retrieved = false;
				break;
			}
		}
		if (retrieved) {
			forwardHistory(category);
			goToNextScene(UITemplateList.KARAOKE_CONTENTS, category);
		} else {
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveCategoryContents(
					menuAdaptor, category.id, category);
		}
	}

	public void goToChannelList(CategoryContainer category) {
		forwardHistory(category);
		goToNextScene(UITemplateList.CHANNEL_LIST, category);
	}

	public void goToChannelMenu(CategoryContainer category) {
		Log.printDebug("called goToChannelMenu(), cat = " + category.toString(true) + ", topic = " + category.getTopic());
		if (category.getTopic().channel == null
				&& category.getTopic().network == null) {
			return;
		}
		// need retrieve, 1 is "about channel"
		if (category.getTopic().treeRoot == null || category.getTopic().treeRoot.categoryContainer.length == 1) {
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveChannel(menuAdaptor,
					category.getTopic(), category);
		} else {
			menuAdaptor.reflectVCDSResult(category);
//			if (category.getTopic().channel != null) {
//				CategoryUI.setBrandingElement(category.getTopic().channel, true);
//			} else if (category.getTopic().network != null) {
//				CategoryUI.setBrandingElement(category.getTopic().network, true);
//			}
//			CategoryUI.setBrandingElementByCategory(category);
//			forwardHistory("");
//			goToNextScene(UITemplateList.CHANNEL_MENU, category);
		}
	}
	
	// R5
	public void goToChannelMenu(String callLetter) {
		Log.printDebug("called goToChannelMenu(), callLetter = " + callLetter);
		if (callLetter == null) {
			return;
		}
		showLoadingAnimation();
		CatalogueDatabase.getInstance().retrieveChannel(menuAdaptor, callLetter, null);
	}
	
	// R5 - TANK
	public void goToSceneForMenu(String[] param) {
		Log.printDebug("MenuController, goToSceneForMenu");
		
		// VDTRMASTER-5640
		if (param[1].equals(MENU_PARAM_WISHLIST) ) {
			BookmarkManager.getInstance().retrieveWishlistTitles(false);
			BookmarkManager.getInstance().organize();
			
			if (BookmarkManager.getInstance().retrieveWishlistTitles(true).length > 0) {
				loadingWishlist = false;
				goToWishlist(false);
			} else {
				paramKey = param[1];
				
				// R7
				// VDTRMASTER-5774
				CategoryUI.getInstance().startLoadingChecker();
				
				// VDTRMASTER-5640
//				EventQueue.invokeLater(new Runnable() {
//					public void run() {
//						try {
//							Thread.sleep(100);
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//						}
//						showLoadingAnimation(true);
//						goToMainScene(true);
//					}
//				});
				
				showLoadingAnimation(true);
				goToMainScene(true);
				
			}
			
		} else if (param[1].equals(MENU_PARAM_RESUMEVIEWING)) {
			//R7.4 freelife VDTRMASTER-6200
			CategoryUI.resetTrialPack();
			
			
			BookmarkManager.getInstance().retrieveActivePurchases(false);
			BookmarkManager.getInstance().organize();
			
			if (BookmarkManager.getInstance().retrieveActivePurchases(true).length > 0) {
				goToResumeViewing(null, false);
			} else {
				paramKey = param[1];
				
				// R7
				// VDTRMASTER-5774
				CategoryUI.getInstance().startLoadingChecker();
				
				// VDTRMASTER-5640
//				EventQueue.invokeLater(new Runnable() {
//					public void run() {
//						try {
//							Thread.sleep(100);
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//						}
//						showLoadingAnimation(true);
//						goToMainScene(true);
//					}
//				});
				showLoadingAnimation(true);
				goToMainScene(true);
			}
			
		} else if (param[1].equals(MENU_PARAM_TOPIC)){
			// TOPIC
			paramKey = param[3] + param[2];
			
			if (param[2].equals("Environnement des Chaines")) {
			    ChannelInfoManager.getInstance().updateSubscribedList();
				CatalogueDatabase.getInstance().retrieveChannelOnDemand2(menuAdaptor, param[3], true, 0, true);
			} else {
//				CatalogueDatabase.getInstance().retrieveNamedTopic(menuAdaptor, param[3], param[2], paramKey);
				// VDTRMASTER-5653 & VDTRMASTER-5649
				CatalogueDatabase.getInstance().retrieveNamedTopic(menuAdaptor, param[3], param[2], "", paramKey);
			}
			
		} else {
			Log.printDebug("MenuController, goToSceneForMenu, unknow param");
		}
	}
	
	// R5 - TANK
	public String getParamKey() {
		return paramKey;
	}
	
	public void resetParamKey() {
		paramKey = null;
	}
	
	public void goToResumeViewing(BaseElement focused, boolean adult) {
		BookmarkManager.getInstance().retrieveActivePurchases(false);
		BookmarkManager.getInstance().organize();
		Bookmark[] bookmarks = adult ? 
			BookmarkManager.getInstance().retrieveActivePurchasesAdult() :
			BookmarkManager.getInstance().retrieveActivePurchases(true);
		if (bookmarks == null) {
			// retrieveActivePurchasesAdult may return null
			bookmarks = new Bookmark[0];
		}
		Log.printDebug("MenuController, goToResumeViewing(), size = "
				+ bookmarks.length);
		if (bookmarks.length == 0) {
			// currentScene.setPopUp(PopAddWishlist.getInstance());
			// PopAddWishlist.getInstance().show(menuAdaptor, null,
			// PopAddWishlist.NO_RESUMEVIEWING);
			return;
		}

		BaseElement[] elements = new BaseElement[bookmarks.length];
		for (int i = 0; i < bookmarks.length; i++) {
			Object cached = CatalogueDatabase.getInstance().getCachedBySspId(
					bookmarks[i].getOrderableName());
			elements[i] = (BaseElement) cached;
		}
		goToNextScene(UITemplateList.RESUME_VIEWING, new Object[] {elements, focused, Boolean.valueOf(adult)});
	}
	
	// R5
	public void goToWishlist(boolean effect) {
		if (loadingWishlist) {
			return;
		}
		loadingWishlist = true;
		if (effect) {
			if (currentScene.getFooter() != null) {
				if (currentScene.getFooter().getParent() instanceof SimilarContentView) {
					currentScene.getFooter().clickAnimation(-1);
				} else {
					currentScene.getFooter().clickAnimation(-2);
				}
			}
		}
		int oriNum = 0;
		// R5
		// VDTRMASTER-5608
		boolean isAdult = isAdultWithSimilar();
		
		try {
			oriNum = isAdult ? 
				BookmarkManager.getInstance().retrieveWishlistTitlesAdult().length :
				BookmarkManager.getInstance().retrieveWishlistTitles(true).length;
		} catch (NullPointerException e) {
			// retrieveWishlistTitlesAdult may return null
		}
		BookmarkManager.getInstance().retrieveWishlistTitles(false);
		BookmarkManager.getInstance().organize();
		String[] names = isAdult ?
				BookmarkManager.getInstance().retrieveWishlistTitlesAdult() :
				BookmarkManager.getInstance().retrieveWishlistTitles(true);
		if (names == null) {
			names = new String[0];
		}
		if (names.length == 0) {
			// VDTRMASTER-5500
			if ((currentScene instanceof CategoryUI 
					&& ((CategoryUI) currentScene).getCurrentCategory().type != MENU_WISHLIST 
					&& ((CategoryUI) currentScene).getCurrentCategory().type != MENU_WISHLIST_ADULT)
					|| currentScene instanceof CategoryUI == false) {
				currentScene.setPopUp(PopAddWishlist.getInstance());
				PopAddWishlist.getInstance().show(menuAdaptor, null,
						PopAddWishlist.NO_WISHLIST);
			}
			loadingWishlist = false;
			if (oriNum > 0) {
				currentScene.updateButtons();
			}
		} else {
			ArrayList list = new ArrayList();
			for (int i = 0; i < names.length; i++) {
				list.add(names[i]);
			}
			showLoadingAnimation();
			CatalogueDatabase.getInstance().retrieveItems(menuAdaptor,
					list, WISHLIST);
		}

		// VDTRMASTER-6127
		if (effect) {
			// Touche:  VDTRMASTER-6245: W 소문자로 바꿈 
            VbmController.getInstance().writeHotKey("wishlist");
        } else {
        	// Touche:  VDTRMASTER-6245: W 소문자로 바꿈
        	// Touche:  VDTRMASTER-6249: addHistory
            VbmController.getInstance().writeSelectionAddHistory("wishlist");
        }
	}

	// Touche:  VDTRMASTER-6244
	public boolean goToDetail(BaseElement current, BaseUI listener) {
		return goToDetail(current, listener, false);
	}
	
	public boolean goToDetail(BaseElement current, BaseUI listener, boolean fromShowcase) {
		Log.printDebug("MenuController, goToDetail()" + ", cur = "
				+ current.toString(false) + ", listener = " + listener
				+ ", cur = " + currentScene + ", needSearch = " + needSearch + ", gotoAsset = " + gotoAsset);
		if (current instanceof MoreDetail) {
			UsageManager.getInstance().addUsageLog(getScreenId(), "", "1202",
					((MoreDetail) current).getId());
		}

		if (listener == null) {
			listener = menuAdaptor;
		}
		if (current instanceof Showcase) {
			Showcase sc = (Showcase) current;
			MoreDetail content = sc.getContent();
			if (content == null) {
				showLoadingAnimation();
				CatalogueDatabase.getInstance().retrieveShowcases(menuAdaptor,
						sc.getId(), null);
				return false;
			}
			current = content;
			Log.printDebug("MenuController, cur = " + current.toString(false));
			
			if (current instanceof CategoryContainer) {
				((CategoryContainer) current).needRetrieve = true; 
			}
		}
		
		if (current instanceof Video) {
			if (gotoAsset == false && isAdultBlocked((MoreDetail) current)) {
				titleToUnblock = (MoreDetail) current;
				PreferenceProxy.getInstance().checkRightFilter(
						RightFilter.HIDE_ADULT_CONTENT,
						null,
						listener,
						new String[] {
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN1),
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN2) });
				return false;
			} else {
				Video v = (Video) current;
				// It doesn't have detail data
				Log.printDebug(v.dumpOffer());
				if (v.detail.length == 0 && v.offer.length == 0) { // retrieve
					showLoadingAnimation();
					ArrayList list = new ArrayList();
					list.add(v.id);
					if (v.id.startsWith("V")) {
						CatalogueDatabase.getInstance().retrieveVideos(
								menuAdaptor, list, v.id);
					} else { // episode
						CatalogueDatabase.getInstance().retrieveEpisodes(
								menuAdaptor, list, v.id);
					}
				} else { // it's retrieved
					if (current instanceof Episode) {
						Episode e = (Episode) current;
						if (currentScene == null || currentScene instanceof CategoryUI || gotoAsset 
								|| (navigationHistory.isEmpty() || navigationHistory.get(navigationHistory.size()-1) instanceof Season == false)
								|| (currentScene instanceof ListViewUI && (((ListViewUI) currentScene).isWishList() || ((ListViewUI) currentScene).isResumeViewing()))) {
							if (e.inBundleOnly) { 
								if (e.inBundlesAR == null || e.inBundlesAR.length == 0) {
									CommunicationManager.getInstance().errorMessage("VOD104", null);
									Log.printError("MenuController, " + e.toString(true) + " has 'inBundleOnly' but it has no 'inBundles'");
								} else {
									goToBundle(null, ((Episode) current).inBundlesAR[0]);
								}
							} else { // it's not in bundle only
								if (e.season == null || e.season.episode.length == 0) { // not retrieved yet
									ArrayList list = new ArrayList();
									list.add(v.id);
									CatalogueDatabase.getInstance().retrieveEpisodes(
											menuAdaptor, list, v.id);
								} else { // goto episode list
									forwardHistory(e.season);
									goToNextScene(
											UITemplateList.LIST_OF_TITLES,
											new Object[] {
													new Integer(
															UITemplateList.LIST_OF_TITLES),
													e.season.episode, e },
											false);
									if (SEARCH.equals(from) || BANNER_CONTENT.equals(from) || (from != null && from.startsWith(AGENT))) {
										mainScreenReady();
									}
								}
							}
						} else { // already list scene
							String action = currentScene.getActionButton(current)[0];
							processActionMenu(action, current);
						}
					} else { // just video
						goToNextScene(UITemplateList.MOVIE_DETAIL, current);
						if (SEARCH.equals(from) || BANNER_CONTENT.equals(from) || (from != null && from.startsWith(AGENT))) {
							mainScreenReady();
						}
					}
				}
			}
		} else if (current instanceof Series) {
			String action = null;
			// R5
			if (currentScene != null) {
				try {
					action = currentScene.getActionButton(current)[0];
				} catch (ArrayIndexOutOfBoundsException e) {}
			}
			if (DataCenter.getInstance().getString(Resources.TEXT_UNBLOCK).equals(action)) {
				if (current instanceof MoreDetail) {
					titleToUnblock = (MoreDetail) current;
					String filter = isAdultBlocked(titleToUnblock) ? RightFilter.HIDE_ADULT_CONTENT : RightFilter.BLOCK_BY_RATINGS;
					PreferenceProxy.getInstance().checkRightFilter(
							filter,
							((MoreDetail) current).getRating(),
							menuAdaptor,
							new String[] {
									DataCenter.getInstance().getString(
											Resources.TEXT_MSG_UNBLOCK_PIN1),
									DataCenter.getInstance().getString(
											Resources.TEXT_MSG_UNBLOCK_PIN2) });
					return false;
				}
			} else {
				Series series = (Series) current;
				if (series.service != null && series.service.serviceType == Service.SVOD) {
					Service service = (Service) CatalogueDatabase.getInstance().getCached(series.service.id);
					if (service.description == null) {
						Log.printDebug("MenuController, gotoDetail(), there is no Service description");
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, series.service.id, series.service);
						return false;
					}
					series.service = service;
				}
				
				if (series.seasonIds.length > 0 || series.needRetrieve) { // need retrieve
					showLoadingAnimation();
					CatalogueDatabase.getInstance().retrieveSeries(listener,
							series.id, series);
				} else if (series.season.length > 0) {
					// already fetched this series
					if (series.season.length == 1) {
						goToDetail(series.season[0], listener);
					} else {// screen of choose season
						forwardHistory(current);
						goToNextScene(UITemplateList.LIST_OF_TITLES, series.id);
					}
				}
			}
			
		} else if (current instanceof AvailableSource) {
			lastObject = current;
			MoreDetail parentSeason = ((AvailableSource) current).parent;
			goToDetail(parentSeason, listener);
		} else if (current.equals(Season.MORE_CONTENT)) {
			try {
				Series series = null;
				BaseElement first = ((ListOfTitlesUI) currentScene).getData()[0];
				if (first instanceof Season) {
					series = ((Season) first).series;
				} else if (first instanceof AvailableSource) {
					series = ((AvailableSource) first).parent.series;
				}
				goToDetail(series, null);
			} catch (Exception e) {
				Log.print(e);
			}
		} else if (current instanceof Season) {
			String action = null;
			//R5
			if (currentScene != null) {
				try {
					action = currentScene.getActionButton(current)[0];
				} catch (ArrayIndexOutOfBoundsException e) {}
			}
			if (DataCenter.getInstance().getString(Resources.TEXT_UNBLOCK).equals(action)) {
				if (current instanceof MoreDetail) {
					titleToUnblock = (MoreDetail) current;
					String filter = isAdultBlocked(titleToUnblock) ? RightFilter.HIDE_ADULT_CONTENT : RightFilter.BLOCK_BY_RATINGS;
					PreferenceProxy.getInstance().checkRightFilter(
							filter,
							((MoreDetail) current).getRating(),
							menuAdaptor,
							new String[] {
									DataCenter.getInstance().getString(
											Resources.TEXT_MSG_UNBLOCK_PIN1),
									DataCenter.getInstance().getString(
											Resources.TEXT_MSG_UNBLOCK_PIN2) });
					return false;
				}
			} else {
				// TODO
				Season season = (Season) current;
				if (season.service != null && season.service.serviceType == Service.SVOD) {
					Service service = (Service) CatalogueDatabase.getInstance().getCached(season.service.id);
					if (service.description == null) {
						Log.printDebug("MenuController, gotoDetail(), there is no Service description");
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, season.service.id, season.service);
						return false;
					}
					season.service = service;
				}
				if (season.episode.length > 0) {
					// already fetched this season
					if (season.sections.length > 0) {
						forwardHistory(season);
						goToNextScene(UITemplateList.LIST_OF_TITLES, season.sections);
					} else {
						checkOptionAndGo(season, listener);
					}
				} else {
					showLoadingAnimation();
					CatalogueDatabase.getInstance().retrieveSeason(listener,
							season.id, null);
				}
			}
		} else if (current instanceof CategoryContainer) {
			CategoryContainer category = (CategoryContainer) current;
			if (category.getTopic() != null
					&& (category.getTopic().channel != null || category
							.getTopic().network != null)) {
				//R7.4 freelife VDTRMASTER-6185
				Log.printDebug("MenuController, gotoDetail() goToChannelMenu " + category.id);
				VbmController.getInstance().writeSelectionAddHistory(category.id, category.parent);
				goToChannelMenu(category);
			} else {
				if (category.service != null && category.service.serviceType == Service.SVOD) {
					Service service = (Service) CatalogueDatabase.getInstance().getCached(category.service.id);
					if (service.description == null) {
						Log.printDebug("MenuController, gotoDetail(), there is no Service description");
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, category.service.id, category.service);
						return false;
					}
					category.service = service;
				}
				if (category.type == MENU_VSD) {
					goToVSD(category, listener);
				} else {
					if (fromShowcase) {
						// Touche:  VDTRMASTER-6244
						VbmController.getInstance().writeSelectionAddDoubleHistory(category.getId(), category.parent);
					} else {
						//R7.4 freelife Add right key vbm					
						VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
					}
					goToCategory(category, listener);
				}
			}
		} else if (current instanceof Bundle) {
			Bundle bundle = (Bundle) current;
			// 4783
//			if (currentScene instanceof CategoryUI || (currentScene instanceof BundleDetailUI == false && bundle.isMovieBundle())) {
				goToBundle(bundle, null);
//			} else {
//				String action = currentScene.getActionButton(current)[0];
//				processActionMenu(action, current);
//			}
		} else if (current instanceof CategorizedBundle) {
			if (currentScene instanceof CategoryUI) {
				processActionMenu(
					DataCenter.getInstance().getString(Resources.TEXT_SELECT),
					current);
			} else {
				goToNextScene(UITemplateList.CHANNEL_MENU, current);
			}
		} else if (current instanceof Extra) {
			Extra extra = (Extra) current;
			if (extra.extraDetail == null) {
				showLoadingAnimation();
				CatalogueDatabase.getInstance().retrieveExtra(menuAdaptor, extra.id, null);
			} else {
				forwardHistory(current);
				goToNextScene(UITemplateList.LIST_OF_TITLES, ((Extra) current).id);
			}
		} else if (current instanceof Section) {
			Section section = (Section) current;
			BaseElement[] data = null;
			if (section.sections.length > 0) {
				data = section.sections;
			} else {
				data = new Episode[section.episodes.length];
				for (int i = 0; i < data.length; i++) {
					data[i] = (BaseElement) CatalogueDatabase.getInstance().getCached(section.episodes[i]);
					if (data[i] == null) {
						showLoadingAnimation(true);
						synchronized (CatalogueDatabase.getInstance()) {
							CatalogueDatabase.getInstance().retrieveSeason(null, section.parent.id, null);
							try {
								CatalogueDatabase.getInstance().wait(
										Constants.MS_PER_MINUTE);
							} catch (InterruptedException e) {
							}
						}
						hideLoadingAnimation();
						goToDetail(current, listener);
						return true;
					}
				}
			}
			forwardHistory(current);
			goToNextScene(UITemplateList.LIST_OF_TITLES, data);
		}
		
		return true;
	}

	public void goToNextScene(int sceneCode) {
		goToNextScene(sceneCode, null);
	}

	public void goToNextScene(int sceneCode, Object sceneData) {
		Object param[] = new Object[2];
		param[0] = new Integer(sceneCode);
		param[1] = sceneData;
		goToNextScene(sceneCode, param, false);
	}

	synchronized private void goToNextScene(int sceneCode, Object[] param,
			boolean back) {
		if (Log.INFO_ON) {
			Log.printInfo("MenuController, goToNextScene(" + sceneCode + ")");
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("currentScene = " + currentScene);
		}
		if (Resources.appStatus == VODService.APP_PAUSED
				|| Resources.appStatus == VODService.APP_PAUSING) {
			Log.printWarning("goToNextScene(), but VOD will be paused or called pauseXlet()");
			return;
		}
		
		boolean needReuse = false;
		if (currentScene != null) { // keep current to stack
			currentScene.stop();
			component(false, currentScene);
			
			if (sceneStack.isEmpty() || !(sceneStack.peek() instanceof TitleDetailUI 
					&& currentScene instanceof TitleDetailUI && sceneCode == UITemplateList.MOVIE_DETAIL)) {
				if (!(currentScene instanceof PlayScreenUI)) {
					sceneStack.push(currentScene);
				}
			} else if (currentScene instanceof TitleDetailUI) { // VDTRMASTER-5698
				needReuse = true;
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("needReuse = " + needReuse);
		}
		if (!needReuse) {
			setCurrentScene(createScene(sceneCode));
		}
		if (currentScene != null) {
			currentScene.setVisible(false);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("nextScene = " + currentScene + ", stack = "
					+ sceneStack.size() + ", search = " + needSearch);
		}
		sceneData = param;
		
		if (CategoryUI.isBranded() && !needReuse) { // VDTRMASTER-5535
			CategoryUI.setBranded(1);
		}
		
		component(true, currentScene);
		needRapid = false;
		currentScene.scroll = 0;
		currentScene.needSearch = needSearch;
		needSearch = false;
		
		currentScene.start(back);
		
		// R5
		// To reset a needToBackEPG
		if (!sceneStack.isEmpty()) {
			setNeedToBackEPG(false);
		}
	}

	private BaseUI createScene(int sceneCode) {
		BaseUI scene = null;

		switch (sceneCode) {
		case UITemplateList.MAIN:
			scene = CategoryUI.getInstance();
			break;
		case UITemplateList.LIST_OF_TITLES:
			scene = new ListOfTitlesUI();
			break;
		case UITemplateList.MOVIE_DETAIL:
			scene = new TitleDetailUI();
			break;
		case UITemplateList.PLAY_SCREEN:
			scene = new PlayScreenUI();
			break;
		case UITemplateList.WISH_LIST:
			scene = new ListViewUI(ListComp.TYPE_WISHLIST);
			break;
		case UITemplateList.RESUME_VIEWING:
			scene = new ListViewUI(ListComp.TYPE_RESUME);
			break;
		case UITemplateList.CAROUSEL_VIEW:
			scene = new CarouselViewUI();
			break;
		case UITemplateList.WALL_OF_POSTERS:
			scene = new WallofPostersFilmInfoUI();
			break;
		case UITemplateList.BUNDLE_DETAIL:
			scene = new BundleDetailUI();
			break;
		case UITemplateList.LIST_VIEW:
			scene = new ListViewUI(ListComp.TYPE_NORMAL);
			break;
		case UITemplateList.SERIES_OPTIONS:
			scene = new SeriesOptionUI();
			break;
		case UITemplateList.ABOUT_CHANNEL:
			scene = new AboutChannelUI();
			break;
		case UITemplateList.KARAOKE_CONTENTS:
		case UITemplateList.KARAOKE_PLAYLIST:
			scene = new KaraokeListViewUI(sceneCode);
			break;
		case UITemplateList.CHANNEL_LIST:
			scene = CategoryUI.getInstance();
			break;
		case UITemplateList.CHANNEL_MENU:
			scene = CategoryUI.newInstance();
			break;

		default:
			scene = null;
			break;

		}
		return scene;
	}

	public void back() {
		boolean needSearch = false; // will be used in finally block
		
		// VDTRMASTER-5211
		if (lastObject != null && lastObject.equals(Season.MORE_CONTENT)) {
			lastObject = null;
		}
		
		if (fromCompanion) {
			openAdultCategory = openAdultCategoryOld;
			fromCompanion = false;
		}
		
		if (from != null && from.startsWith(AGENT)/* && sceneStack.empty()*/) {
			Log.printInfo("MenuController, back(), from = " + from);
			from = null;
			goToMainScene(false);
			return;
		} else if (from != null && from.equals(EPG) && needToBackEPG) {
			VbmController.getInstance().writeExitedWithBack();
			CommunicationManager.getInstance().startUnboundApplication(EPG, 
					new String[] { MonitorService.REQUEST_APPLICATION_LAST_KEY, Resources.APP_NAME});
			return;
		}
		
		if (currentScene != null) {
			needSearch = currentScene.needSearch;
			currentScene.needSearch = false;
		}
		Log.printInfo("MenuController, back(), needSearch = " + needSearch + ", isEDT = " + EventQueue.isDispatchThread());
//		if (needSearch) {
//			CommunicationManager.getInstance().launchSearchApplication(false,
//					false);
//			needSearch = currentScene.needSearch = false;
//			keyTime = System.currentTimeMillis();
//		}
		try {
			// VDTRMASTER-5470
			if (currentScene instanceof PlayScreenUI == false) {
				gotoAsset = false;
			}
			if (sceneStack.empty()) {
				if (currentScene instanceof CategoryUI) {
					Log.printDebug("It's the main scene now.");
					return;
				} else {
					Log.printDebug("It's first screen but not the main scene.");
					goToMainScene(false);
					return;
				}
			}
			Log.printDebug("currentScene = " + currentScene + ", back = "
					+ sceneStack.peek() + ", " + needSearch);
			if (currentScene != null) { // discard current scene
				currentScene.stop();
				component(false, currentScene);
				currentScene.flushData();
			}
			CategoryUI.setBranded(-1);
			
			setCurrentScene((BaseUI) sceneStack.pop());
			
			component(true, currentScene);
			currentScene.start(true);
			currentScene.updateButtons();
		} finally {
			if (needSearch) {
				VbmController.getInstance().writeExitedWithBack();
				isFromSearch = false;
				new Thread("launchSearchApplication on back()") {
					public void run() {
						keyTime = System.currentTimeMillis();
						CommunicationManager.getInstance().launchSearchApplication(false,
								false);
					}
				}.start();
			}
		}
		return;
	}

	public void goToHelp() {
		try {
			pauseTemporary = true;
			CommunicationManager.getInstance().getMonitorService().startUnboundApplication(
					"Help", 
					new String[] {
							MonitorService.REQUEST_APPLICATION_HOT_KEY, 
							Resources.APP_NAME, null});
		} catch (RemoteException e) {
			Log.printWarning(e);
		}
	}
	public void goToSetting() {
		if (Definitions.OPTION_VALUE_ON.equals(DataCenter.getInstance()
				.getString(RightFilter.PARENTAL_CONTROL))
				&& Definitions.OPTION_VALUE_YES.equals(DataCenter.getInstance()
						.getString(RightFilter.PROTECT_SETTINGS_MODIFICATIONS))) {
			// check right filter
			PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.PROTECT_SETTINGS_MODIFICATIONS,
					null,
					menuAdaptor,
					new String[] {
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_PROTECT_SETTING1),
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_PROTECT_SETTING2) });
			return;
		}
		try {
			pauseTemporary = true;
			CommunicationManager
					.getInstance()
					.getMonitorService()
					.startUnboundApplication(
							"SETTINGS",
							new String[] { Resources.APP_NAME,
									PreferenceService.VOD });
		} catch (RemoteException e) {
			Log.printWarning(e);
		}
	}

	public void goToMainScene(boolean start) {
		Log.printInfo("MenuController, goToMainScene(), currentScene = "
				+ currentScene + ", sceneStack.empty = " + sceneStack.empty() + ", start = " + start);
		backwardHistory(true);
		
		if (from != null && from.startsWith(AGENT)) {
			from = null;
		}
		
		isFromSearch = false; // for reset
		
		// main scene now
		if (currentScene instanceof CategoryUI && CategoryUI.getInstance() == currentScene && sceneStack.empty()) {
			((CategoryUI) currentScene).fadeOutShowcase();
			((CategoryUI) currentScene).reset();
			currentScene.setVisible(true);
			return;
		}

		// or there is history
		while (currentScene != null) {
			// discard current scene;
			Log.printDebug("MenuController, goToMainScene(), stop " + currentScene);
			currentScene.stop();
			component(false, currentScene);
			currentScene.flushData();
			setCurrentScene(null);
			if (sceneStack.empty() == false) {
				setCurrentScene((BaseUI) sceneStack.pop());
			}
		}
		
		// IMPROVEMENT
		after = System.currentTimeMillis();
		App.printDebug("start 9-2 = " + (after - before));
		before = System.currentTimeMillis();
		
		Log.printDebug("MenuController, goToMainScene() after, currentScene = "
				+ currentScene + ", sceneStack.empty = " + sceneStack.empty());
		gotoAsset = false;
		if (start) {
			// R7
			// VDTRMASTER-5774
			boolean isSameVersion = CategoryUI.getInstance().reset();
			goToNextScene(UITemplateList.MAIN);
			
			// IMPROVEMENT
			after = System.currentTimeMillis();
			App.printDebug("start 9-3 = " + (after - before));
			before = System.currentTimeMillis();
			
			if (paramKey == null) {
				CategoryUI.getInstance().fadeIn();
			} else if (isSameVersion) {
				// R7
				// VDTRMASTER-5774
				currentScene.reflectVCDSResult(null);
			}

			// IMPROVEMENT
			if (BookmarkManager.getInstance().retrieveWishlistTitles(true).length > 0) {
				loadingWishlist = false;
			}
			after = System.currentTimeMillis();
			App.printDebug("start 9-4 = " + (after - before));
			before = System.currentTimeMillis();
			
			// VDTRMASTER-5640
//			else {
//				showLoadingAnimation();
//			}
		} else {
			// maybe nouse
			goToNextScene(UITemplateList.MAIN);
			if (currentScene != null) {
				((CategoryUI) currentScene).reset();
				currentScene.setVisible(true);
			}
		}
	}

	public static Object[] peekStack() {
		return sceneData;// getInstance().uiDataStack.peek();
	}

	public UIComponent getCurrentScene() {
		return currentScene;
	}
	
	public void mainScreenReady() {
		Log.printDebug("called mainScreenReady(), EDT = " + EventQueue.isDispatchThread());
		
		synchronized (VODServiceImpl.getInstance()) {
			VODServiceImpl.getInstance().notifyAll();
		}
		
		CommunicationManager.getInstance().turnOnErrorMessage(false);
		showLoadingAnimation();
		BookmarkManager.getInstance().start();
		hideLoadingAnimation();
		CommunicationManager.getInstance().turnOnErrorMessage(true);
		loadingWishlist = false;
	}

	public BaseUI getLastScene() {
		if (sceneStack.isEmpty()) {
			return null;
		}
		return (BaseUI) sceneStack.peek();
	}
	
	// R5
	// VDTRMASTER-5623
	public boolean isUnderWishList() {
		if (sceneStack.size() > 0) {
			for (int i = 0; i < sceneStack.size(); i++) {
				BaseUI ui = (BaseUI) sceneStack.get(i);
				
				if (ui instanceof ListViewUI && ((ListViewUI) ui).isWishList()) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	// R5
	// VDTRMASTER-5628
	public boolean isInWishlist() {
		if (currentScene != null) {
			if (currentScene instanceof ListViewUI && ((ListViewUI) currentScene).isWishList()) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean useBradingDeco() {
		UIComponent parent = getCurrentScene();
		
		if (parent instanceof ListOfTitlesUI) {
			return ((ListOfTitlesUI)parent).useBradingDeco();
		}
		
		return false;
	}
	
	public void forwardHistory(BaseElement history) {
		Log.printDebug("forwardHistory(), element = " + history.toString(true));
		navigationHistory.add(history);
	}

	public void forwardHistory(String history) {
		Log.printDebug("forwardHistory(), str = " + history);
		navigationHistory.add(history);
	}

	public void backwardHistory(boolean reset) {		
		if (reset) {
			navigationHistory.clear();
			CategoryUI.setChannelCategory(null);
			CategoryUI.setBrandingElement(null, false);
			CategoryUI.setChannel( null);
		} else {
			if (navigationHistory.size() > 0) {
				Object obj = navigationHistory.remove(navigationHistory.size() - 1);
				
				if (obj instanceof CategoryContainer 
						&& (((CategoryContainer) obj).type == MenuController.MENU_CHANNEL
						|| ((CategoryContainer) obj).type == MenuController.MENU_CHANNEL_ENV)
						) {
					CategoryUI.setChannelCategory(null);
				}
			}
		}		
	}

	public int getHistorySize() {
		return navigationHistory.size();
	}

	public String getLastHistory() {
		if (navigationHistory.size() == 0) {
			return DataCenter.getInstance().getString(
					Resources.TEXT_MSG_MAIN_TOP);
		}
		Object obj = navigationHistory.lastElement();
		if (obj instanceof String) {
			return (String) obj;
		} else if (obj instanceof MoreDetail) {
			return ((MoreDetail) obj).getTitle();
		} else if (obj instanceof BaseElement) {
			return ((BaseElement) obj).toString(false);
		}
		return "?";
	}
	
	// R5
	public Object getListHistoryObject() {
		if (navigationHistory.size() == 0) {
			return DataCenter.getInstance().getString(
					Resources.TEXT_MSG_MAIN_TOP);
		}
		
		Object obj = navigationHistory.lastElement();
		return obj;
	}

	public String[] getHistory() {
		String[] hisArr = new String[navigationHistory.size() + 1];
		hisArr[0] = DataCenter.getInstance().getString(
				Resources.TEXT_MSG_MAIN_TOP);
		for (int i = 0; i < navigationHistory.size(); i++) {
			Object obj = navigationHistory.get(i);
			if (obj instanceof String) {
				hisArr[i + 1] = (String) obj;
			} else if (obj instanceof MoreDetail) {
				hisArr[i + 1] = ((MoreDetail) obj).getTitle();
			} else {
				hisArr[i + 1] = "??";
			}
		}
		return hisArr;
	}
	
//	public void showCannot() {
//		currentScene.setPopUp(PopAddWishlist.getInstance());
//		PopAddWishlist.getInstance().show(menuAdaptor, null,
//				PopAddWishlist.CANT_JUMP);
//	}
	
	public void showRapid() {
		if (currentScene.getPopUp() == null) {
			currentScene.setPopUp(PopRapid.getInstance());
			int mode = PopRapid.HORIZONTAL;
			if (currentScene instanceof ListViewUI || currentScene instanceof WallofPostersFilmInfoUI) {
				mode = PopRapid.VERTICAL;
			}
			PopRapid.getInstance().show(menuAdaptor, mode);
			keyStart = -1;
		}
	}

	public void showOrderConfirm(Object object) {
		currentScene.setPopUp(PopOrderResult.getInstance());
		PopOrderResult.getInstance().show(menuAdaptor, object);
	}

	public void showSelectItem(Object object) {
		currentScene.setPopUp(PopSelectItem.getInstance());
		PopSelectItem.getInstance().show(menuAdaptor, object);
	}
	
	public void showPlayAllPopup(Object object, boolean isLast) {
		currentScene.setPopUp(PopPlayAll.getInstance());
		PopPlayAll.getInstance().show(menuAdaptor, object, isLast);
	}

//	public void showTrailer(PlayData playData) {
//		currentScene.setPopUp(PopTrailer.getInstance());
//		PopTrailer.getInstance().show(currentScene, playData);
//	}
	
	public void setLastCategoryId(String id) {
		lastCategoryId = id;
	}
	
	// R5
	// VDTRMASTER-5653 & VDTRMASTER-5649
	public String getLastCategoryId() {
		return lastCategoryId;
	}
	public void setSortWishlist(String option) {
		sortWishlist = option;
	}

	public String getSortWishlist() {
		return sortWishlist;
	}

	public void setSortResumeViewing(String option) {
		sortResumeViewing = option;
	}

	public String getSortResumeViewing() {
		return sortResumeViewing;
	}

	public void setSortNormal(String option) {
		sortNormal = option;
	}

	public String getSortNormal() {
		return sortNormal;
	}

	public void keyPressed(KeyEvent e) {
		// boolean ret = processKeyEvent(e.getKeyCode());
	}

	public void keyReleased(KeyEvent e) {

	}

	public void keyTyped(KeyEvent e) {

	}

	long keyEvent;
	long keyTime; // for block key event
	long keyStart; // for continuous event
	boolean continousKey;
	int lastKeyCode;
	boolean needRapid;
	
	public void setNeedRapid(boolean need) {
		needRapid = need;
	}

	public boolean isContinousEvent() {
		return continousKey;
	}
	public boolean handleKeyEvent(UserEvent event) { // from Layered UI
		int type = event.getType();
		int code = event.getCode();
		if (type != KeyEvent.KEY_PRESSED) {
			if (type == KeyEvent.KEY_RELEASED) {// && lastKeyCode == code) {
				lastKeyCode = -1;
				keyStart = 0;
				continousKey = false;
			}
			return false;
		}
		long t = System.currentTimeMillis();
		if (t - keyTime < 1500) { // too early event from certain time
			return true;
		}
		keyEvent = event.getWhen();
		if (t - keyEvent > 500L) { // too old event
			return true;
		}
		boolean ret;
		//synchronized (instance) {
			ret = processKeyEvent(code);
		//}
		lastKeyCode = code;
		
		// lock this routine for temporary
//		if (keyStart == 0) {
//			keyStart = t;
//		} else if (keyStart < 0) {
//			// it means there is rapid popup
//		} else {
//			long dur = t - keyStart;
//			continousKey = dur > Constants.MS_PER_SECOND;
//			if (needRapid && dur > Constants.MS_PER_SECOND * 3) {
//				showRapid();
//			}
//		}
		if (Log.DEBUG_ON) {
			elapsed = "elapsed = " + (System.currentTimeMillis() - t);
			Log.printDebug("MenuController, handleKeyEvent return " + ret
					+ ", " + elapsed + ", t = " + t + ", w = " + keyEvent);
		}
		return ret;
	}

	int hiddenIdx = 0;
	int[] hidden = new int[] { 
			KeyEvent.VK_0, 
			KeyEvent.VK_0, 
			KeyEvent.VK_0,
			KeyEvent.VK_0, 
			KeyEvent.VK_0, 
			KeyEvent.VK_PAGE_UP, 
			KeyEvent.VK_PAGE_UP, 
			KeyEvent.VK_PAGE_DOWN,};


	public ArrayList[] debugList = new ArrayList[] { new ArrayList(), // image
			new ArrayList(), // vcds
			new ArrayList(), // bookmark
			new ArrayList(), // playscreen
			new ArrayList(), // service group
			new ArrayList(), // illico core // R7.2
	};
	public int debugIdx = 0;
	LayeredWindow hiddenWindow = new LayeredWindow() {
		public void paint(Graphics g) {
			ArrayList list = debugList[debugIdx];
			g.setColor(new DVBColor(80, 170, 80, 150));
			//g.fillRoundRect(40, 50, 880, 440, 40, 40);
			g.fillRect(0, 50, 960, 440);
			g.setColor(new DVBColor(132, 96, 255, 128));
			g.fillRect(0, 30, 960, 20);
			g.fillRect(0, 490, 960, 20);

			g.setFont(FontResource.BLENDER.getFont(16));
			FontMetrics fm = g.getFontMetrics();
			int len = list.size();
			for (int i = 0; i < len && i < 25; i++) {
				String str = (String) list.get(len - 1 - i);
				str = TextUtil.shorten(str, fm, 860);
				g.setColor(Color.black);
				g.drawString(str, 57, 67 + i * 17);
				g.setColor(Color.yellow);
				g.drawString(str, 55, 65 + i * 17);
			}
			while (list.size() > 25) {
				list.remove(0);
			}
			String title = "";
			switch (debugIdx) {
			case 0:
				title = "Debug information of Image download, "
					+ CatalogueDatabase.getInstance().posterInfo();
				break;
			case 1:
				title = "Debug information of VCDS, "
					+ CatalogueDatabase.getInstance().cacheInfo();
				break;
			case 2:
				title = "Debug information of Control server";
				break;
			case 3:
				title = "Debug information of Video server, "
						+ CommunicationManager.getInstance().debugLock;
				break;
			case 4:
				title = "Debug information of Service group";
				break;
			// R7.2
			case 5:
				title = "Debug information of illico core";
				break;
			}

			g.setColor(Color.black);
			g.drawString(title, 52, 47);
			g.setColor(Color.cyan);
			g.drawString(title, 50, 45);
			super.paint(g);
		}
	};

	public boolean processKeyEvent(int keyCode) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, processKeyEvent()" + ", code = "
					+ keyCode + ", cur = " + currentScene + ", from = " + from);
		}
		boolean used = false;
		
//		Log.checkLogOnOff(keyCode, Resources.APP_NAME);

		if (hidden[hiddenIdx] == keyCode) {
			if (hiddenUI != null && hiddenUI.isActive() && keyCode == KeyEvent.VK_0) {
				debugIdx = (debugIdx + 1) % debugList.length;
				hiddenWindow.repaint();
			}
			hiddenIdx++;
			if (hiddenIdx >= hidden.length) {
				if (hiddenUI == null) {
					hiddenUI = WindowProperty.VOD_TRAILER.createLayeredWindow(
							hiddenWindow, Resources.fullRec);
					hiddenWindow.setBounds(Resources.fullRec);
					hiddenUI.activate();
				} else {
					hiddenUI.deactivate();
					WindowProperty.dispose(hiddenUI);
					hiddenUI = null;
				}
				hiddenIdx = 0;
			}
		} else {
			hiddenIdx = 0;
		}

		if (keyCode == KeyCodes.STOP) {
			if (hiddenUI != null) {
				hiddenUI.deactivate();
				WindowProperty.dispose(hiddenUI);
				hiddenUI = null;
			}
		}

		// if (errorKeys[errorKeyIdx] == keyCode) {
		// errorKeyIdx++;
		// if (errorKeyIdx >= errorKeys.length) {
		// errorMode = (errorMode + 1) % 3;
		// errorKeyIdx = 0;
		// if (currentScene != null) {
		// currentScene.repaint();
		// }
		// }
		// } else {
		// errorKeyIdx = 0;
		// }

		// Popup exists
		if (currentScene != null && currentScene.getPopUp() != null) {
			used = currentScene.getPopUp().handleKey(keyCode);
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuController, processKeyEvent(), popup used = "
						+ used);
			}
		}
		if (used) {
			return used;
		}

		// VOD Watching
		if (currentScene instanceof PlayScreenUI
		/* || currentScene instanceof FullTrailerUI */) {
			used = currentScene.handleKey(keyCode);
		} else {
			// Don't use it, pass!
			switch (keyCode) {
			case KeyCodes.LIST:
			case KeyCodes.SETTINGS:
			case OCRcEvent.VK_GUIDE:
			case KeyCodes.MENU:
			case KeyCodes.WIDGET:
			case OCRcEvent.VK_MUTE:
			case OCRcEvent.VK_VOLUME_DOWN:
			case OCRcEvent.VK_VOLUME_UP:
			case KeyCodes.PIP:
			case KeyCodes.PIP_MOVE:
			case KeyCodes.ASPECT:
				return false;
				// case KeyEvent.VK_F8:
				// FlipBar fs = FlipBarFactory.getInstance().createVodFlipBar();
				// currentScene.add(fs, 0);
				// FlipBarContent contents = new FlipBarContent("Start typing",
				// 0, 60 * Constants.MS_PER_MINUTE);
				// fs.setContent(contents);
				// fs.setMediaTime(33 * Constants.MS_PER_MINUTE);
				// fs.start();
				// return false;
//			case KeyEvent.VK_F12:
//				currentScene.setPopUp(PopSubscribe.getInstance());
//				PopSubscribe.getInstance().show(currentScene, null);

				// FlipBar f = FlipBarFactory.getInstance().createVodFlipBar();
				// currentScene.add(f, 0);
				// FlipBarContent content = new
				// FlipBarContent("Start typing to get a list of possible matches. Optional list of users who can close the issue as well as the reporter.",
				// 0, 60 * Constants.MS_PER_MINUTE);
				// f.setContent(content);
				// f.setMediaTime(33 * Constants.MS_PER_MINUTE);
				// f.start();
				// return false;
				// case KeyEvent.VK_F11:
				// FlipBar ff = (FlipBar) currentScene.getComponent(0);
				// ff.stop();
				// return false;
				// PopVODStop p = new PopVODStop();
				// currentScene.setPopUp(p);
				// p.show(currentScene, null, false, false, false, false);
				// objectToPlay = new Object();
				// showOrderConfirm(objectToPlay);
				// currentScene.setPopUp(PopSelectOption.getInstance());
				// PopSelectOption.getInstance().show(currentScene,
				// CategoryUI.getInstance().getPartyPack().categorizedBundle);
				// PopChangeView.getInstance().show(currentScene);
				// return false;
			}
		}
		if (used) {
			return used;
		}
		
		// VDTRMASTER-5575
		Log.printDebug("MenuController, processKeyEvent, isShowingLoading=" + isShowingLoading 
				+ ", MenuTreeUI.isAnimating=" + MenuTreeUI.isAnimating);
		if (keyCode != OCRcEvent.VK_LIVE && keyCode != OCRcEvent.VK_EXIT 
				&& (isShowingLoading || MenuTreeUI.isAnimating)) {
			
			// VDTRMASTER-5644
			if (keyTimeStamp == 0) {
				keyTimeStamp = System.currentTimeMillis();
				return true;
			} else if (keyTimeStamp > System.currentTimeMillis() - 3000) {
				return true;
			}
			isShowingLoading = false;
			MenuTreeUI.isAnimating = false;
		}
		keyTimeStamp = 0;
		

		used = true;
		// Else common key
		switch (keyCode) {
		// no action
		case KeyEvent.VK_0:
		case KeyEvent.VK_1:
		case KeyEvent.VK_2:
		case KeyEvent.VK_3:
		case KeyEvent.VK_4:
		case KeyEvent.VK_5:
		case KeyEvent.VK_6:
		case KeyEvent.VK_7:
		case KeyEvent.VK_8:
		case KeyEvent.VK_9:
		case KeyCodes.FAV:
		case KeyCodes.RECORD:
			// case KeyCodes.LIST:
			// case OCRcEvent.VK_FORWARD:
			// case OCRcEvent.VK_BACK:
		case OCRcEvent.VK_REWIND:
		case OCRcEvent.VK_FAST_FWD:
		case OCRcEvent.VK_PAUSE:
		case OCRcEvent.VK_PLAY:
		case OCRcEvent.VK_STOP:
		case OCRcEvent.VK_CHANNEL_DOWN:
		case OCRcEvent.VK_CHANNEL_UP:
			break;

		case KeyCodes.SEARCH:
			if (currentScene.getFooter() != null) {
				currentScene.getFooter().clickAnimation(Resources.TEXT_SEARCH);
			}
//			boolean karaoke = currentScene.isExistPlayListBtn()
//					|| currentScene instanceof KaraokeListViewUI;
//			if (karaoke == false) {
//				if (currentScene instanceof CategoryUI) {
//					CategoryContainer category = ((CategoryUI) currentScene)
//							.getCurrentCategory();
//					
//					karaoke = category != null && (category.type == MENU_KARAOKE_PACK
//							|| category.type == MENU_KARAOKE_CONTENTS);
//				}
//			}
//			if (karaoke
//					&& CategoryUI.getPurchasedPartyPack() == null) {
//				purchaseKaraoke(false);
//			} else {
//				if (needSearch) {
//					back();
//				} else {
//					if (currentScene != null && currentScene.getPopUp() != null) {
//						currentScene.getPopUp().stop();
//						currentScene.setPopUp(null);
//					}
//					VbmController.getInstance().writeHotKey(SEARCH);
//					CommunicationManager.getInstance().launchSearchApplication(
//						true, karaoke);
////					try {
////						CommunicationManager.getInstance().searchActionEventListener.actionRequested("VOD", new String[] {"Search", "V_28871", "VOD"});
////					} catch (RemoteException e) {
////						e.printStackTrace();
////					}
//				}
//			}
			
			// R5 - because new Search don't has a karaoke search
			if (needSearch) {
				back();
			} else {
				if (currentScene != null && currentScene.getPopUp() != null) {
					currentScene.getPopUp().stop();
					currentScene.setPopUp(null);
				}
				VbmController.getInstance().writeHotKey(SEARCH);
				CommunicationManager.getInstance().launchSearchApplication(
					true, false);
			}
			break;
		case KeyCodes.VOD:
			if (sceneStack.isEmpty()
					&& currentScene instanceof CategoryUI
					&& CategoryUI.getInstance().isTop()) {
				exit(EXIT_BY_HOTKEY, null);
			} else {
				needSearch = false; // jira 3243
				goToMainScene(false);
			}
			break;
		case KeyEvent.VK_BACK_SPACE:
		case KeyCodes.LAST:
			Log.printDebug("MenuController, processKeyEvent, isShowingLoading=" + isShowingLoading 
					+ ", MenuTreeUI.isAnimating=" + MenuTreeUI.isAnimating);
			if (isShowingLoading || MenuTreeUI.isAnimating) {
				break;
			}
			
			if (clickingEffect == null) {
				clickingEffect = new ClickingEffect(currentScene);
			}
			
			// VDTRMASTER-5501
			if (!CategoryUI.getInstance().isTop()) {
				clickingEffect.start(52, 488, 34, 24);
			}
			
			if (currentScene instanceof CategoryUI
					&& CategoryUI.getInstance().isTop()
					&& "Menu".equalsIgnoreCase(from)) {
				// exit(EXIT_TO_MENU);
				// VDTRMASTER-5557
				used = false;
			} else if (currentScene instanceof TitleDetailUI
					&& SEARCH.equalsIgnoreCase(from) && needSearch) {
				CommunicationManager.getInstance().launchSearchApplication(
						false, false);
			} else if (currentScene instanceof TitleDetailUI
					&& "Wizard".equalsIgnoreCase(from)) {
				exit(EXIT_BY_LAST, null);
			} else {
				used = false;
			}
			
			// Touche project
			// VDTRMASTER-6252, VDTRMASTER-6253
			if (currentScene instanceof CategoryUI && 
					((CategoryUI)currentScene).isFocusOnShowcase()) {
				VbmController.getInstance().backwardHistory();
			}
			break;
		case OCRcEvent.VK_LIVE:
		case OCRcEvent.VK_EXIT:
			exit(EXIT_NORMAL, null);
			break;
		case KeyCodes.COLOR_A:
			// R5
			if (needToBlockKeyForSimilarContents()) {
				break;
			}
			boolean exists = currentScene.isExistChangeViewBtn();
			Log.printDebug("ColorKey A, exists = " + exists);
			if (exists) {
				if (currentScene.getFooter() != null) {
					currentScene.getFooter().clickAnimation(
							Resources.TEXT_VIEW_CHANGE);
				}
				PopChangeView.getInstance().show(currentScene);
				VbmController.getInstance().writeHotKey("View");
				used = true;
			} else if (currentScene.getChannelBtn() != 0) {
				if (currentScene.getFooter() != null) {
					int btn = currentScene.getChannelBtn();
					currentScene.getFooter().clickAnimation(btn == BaseUI.BTN_ALL_CHANNEL ? Resources.TEXT_VIEW_ALL_CHANNEL : Resources.TEXT_VIEW_MY_CHANNEL);
				}
				VbmController.getInstance().writeHotKey("View");
				used = false;
			} else {
				if (Environment.EMULATOR) {
				try {
					CommunicationManager.getInstance().searchActionEventListener.actionRequested("VOD", new String[] {"Search", "EP_26573"/*"V_14409"*/, "VOD"});
				} catch (RemoteException e) {}
				}
				used = true; // ignore color key A;
			}
			break;
		case KeyCodes.COLOR_B:
			Log.printDebug("MenuController, processKeyEvent, isShowingLoading=" + isShowingLoading 
					+ ", MenuTreeUI.isAnimating=" + MenuTreeUI.isAnimating);
			if (isShowingLoading || MenuTreeUI.isAnimating) {
				break;
			}
			
			// R5
			if (needToBlockKeyForSimilarContents()) {
				used = false;
				break;
			}
			
			if (currentScene.isExistPlayListBtn()) {
				if (currentScene.getFooter() != null) {
					currentScene.getFooter().clickAnimation(-2);
				}
				if (CategoryUI.getPurchasedPartyPack() != null) {
					if (playList.size() > 0) {
						goToNextScene(UITemplateList.KARAOKE_PLAYLIST);
					} else {
						currentScene.setPopUp(PopAddWishlist.getInstance());
						PopAddWishlist.getInstance().show(menuAdaptor, null,
								PopAddWishlist.NO_PLAYLIST);
					}
					VbmController.getInstance().writeHotKey("PlayList");
				} else {
					purchaseKaraoke(true);
				}
			} else if (currentScene.isExistWishListBtn()) {
				// VDTRMASTER-5500
				loadingWishlist = false;
				goToWishlist(true);
			}
			break;
		case KeyCodes.COLOR_C:
			// R5
			if (needToBlockKeyForSimilarContents()) {
				break;
			}
			BannerData banner = PromotionBanner.getInstance().getBannerData();
			if (banner != null) {
				if (banner.getLinkType() == 6) { // category
					showLoadingAnimation();
					int pageSize = DataCenter.getInstance().getInt("PAGE_SIZE");
					if (pageSize > 0) {
						pageSize = Math.max(21, pageSize);
					}
					int start = -(pageSize / 2);
					if (lastView == UITemplateList.WALL_OF_POSTERS
							|| lastView == UITemplateList.LIST_VIEW) {
						pageSize += -start;
					}
					CatalogueDatabase.getInstance()
							.retrieveCategoryContents(menuAdaptor,
									banner.getCategoryID(), null, start, pageSize, "");

				} else if (banner.getLinkType() == 7) { // content
					goToAsset(banner.getContentsID(), false, null);
				} else {
					PromotionBanner.getInstance().processKeyEvent();
				}
			}
			break;
		case KeyCodes.COLOR_D:
			// R5
			if (needToBlockKeyForSimilarContents()) {
				break;
			}
			if (currentScene.getFooter() != null) {
				currentScene.getFooter().clickAnimation(Resources.TEXT_OPTION);
			}
			VbmController.getInstance().writeHotKey("Options");
			used = false;
			break;
		case KeyCodes.LITTLE_BOY:
		case OCRcEvent.VK_F4:
			Log.printDebug("MenuController, processKeyEvent, LITTLE_BOY, isShowingLoading=" + isShowingLoading 
					+ ", MenuTreeUI.isAnimating=" + MenuTreeUI.isAnimating);
			
			if (isShowingLoading || MenuTreeUI.isAnimating) {
				break;
			}
			
			// R5
			if (needToBlockKeyForSimilarContents()) {
				used = false;
				break;
			}
						
			if (currentScene instanceof PlayScreenUI == false && currentScene instanceof CategoryUI == false) {
				BaseElement element = getCurrentElement();
				 
				if (element != null && currentScene != null) {
					String btnName = currentScene.getWishButtonName(element);
					Log.printDebug("MenuController, processKeyEvent, LITTLE_BOY, btnName=" + btnName);
					if (btnName != null) {
						currentScene.getFooter().clickAnimation(BaseUI.imgProfile);
						processActionMenu(btnName, element);
					}
				} else {
					// R5
					used = false;
				}
			} else {
				used = false;
			}
			break;
		default:
			used = false;
		}
		if (used) {
			return true;
		}
		if (currentScene != null) {
			return currentScene.handleKey(keyCode);
		}
		return false;
	}
	
	// R5
	public BaseElement getCurrentElement() {
		BaseElement element = null;
		
		if (currentScene instanceof BundleDetailUI) {
			if (currentScene.getCurrentFocusUI() == BundleDetailUI.FOCUS_BUNDLE) {
				element = ((BundleDetailUI) currentScene).getBundle();
			} else {
				element = ((BundleDetailUI) currentScene).getCurrentCatalogue();
			}
		} else if (currentScene instanceof CarouselViewUI) {
			element = ((CarouselViewUI) currentScene).getCurrentCatalogue();
		} else if (currentScene instanceof ListOfTitlesUI) {
			element = ((ListOfTitlesUI) currentScene).getCurrentCatalogue();
		} else if (currentScene instanceof ListViewUI) {
			element = ((ListViewUI) currentScene).getCurrentCatalogue();
		} else if (currentScene instanceof TitleDetailUI 
				&& !((TitleDetailUI) currentScene).needKeyBlockForSimilarContent()) {
			element = ((TitleDetailUI) currentScene).getTitle();
		} else if (currentScene instanceof WallofPostersFilmInfoUI) {
			element = ((WallofPostersFilmInfoUI) currentScene).getCurrentCatalogue();
		}
		
		return element;
	}
	
	public boolean needToBlockKeyForSimilarContents() {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, needToBlockKeyForSimilarContents()");
		}
		
		if (currentScene != null && currentScene instanceof TitleDetailUI 
				&& ((TitleDetailUI) currentScene).needKeyBlockForSimilarContent()) {
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuController, needToBlockKeyForSimilarContents() return true");
			}
			return true;
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, needToBlockKeyForSimilarContents() return false");
		}
		return false;
	}

	public String getScreenId() {
		if (currentScene instanceof CategoryUI) {
			return "1103";
		}
		if (currentScene instanceof ListOfTitlesUI) {
			return "1104";
		}
		if (currentScene instanceof WallofPostersFilmInfoUI) {
			return "1104";
		}
		if (currentScene instanceof CarouselViewUI) {
			return "1104";
		}
		if (currentScene instanceof ListViewUI) {
			if (((ListViewUI) currentScene).isWishList()) {
				return "1115";
			}
			if (((ListViewUI) currentScene).isResumeViewing()) {
				return "1116";
			}
			return "1104";
		}
		if (currentScene instanceof TitleDetailUI) {
			return "1105";
		}
		if (currentScene instanceof BundleDetailUI) {
			return "1105";
		}
		if (currentScene instanceof PlayScreenUI) {
			return "1108";
		}
		return "1199";
	}

	public void exit(int reason, Channel channel) {
		if (Log.INFO_ON) {
			Log.printInfo("MenuController, exit(), reason = " + reason
					+ ", ch = " + channel);
		}
		int appStatus = Resources.appStatus;
		Resources.appStatus = VODService.APP_PAUSING;
		try {
			UsageManager.getInstance().addUsageLog(getScreenId(), "", "1210",
					"");
			if (reason == EXIT_TO_MENU) {
				CommunicationManager.getInstance().getMonitorService()
						.startUnboundApplication("Menu", null);
			} else if (channel == null) {
				CommunicationManager.getInstance().getMonitorService()
						.exitToChannel();
			} else if (channel != null) {
				TvChannel tvCh = CommunicationManager.getInstance().getChannel(
						channel.callLetters);
				// VDTRMASTER-5521
				if (tvCh != null) {
					TvChannel sisterTvCh = tvCh.getSisterChannel();
					if (Log.DEBUG_ON) {
						Log.printDebug("MenuController, exit(), tvCh.definition=" + tvCh.getDefinition());
					}
					if (sisterTvCh != null && sisterTvCh.getDefinition() > tvCh.getDefinition()) {
						if (Log.DEBUG_ON) {
							Log.printDebug("MenuController, exit(), sisterTvCh.definition="
									+ sisterTvCh.getDefinition());
						}
						CommunicationManager.getInstance().getMonitorService()
						.exitToChannel(sisterTvCh.getSourceId());
					} else {
						CommunicationManager.getInstance().getMonitorService()
								.exitToChannel(tvCh.getSourceId());
					}
				} else {
					Resources.appStatus = appStatus;
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * As need screen-saver, the monitor asks other applications to confirm.
	 * <P>
	 * If any application return false, screen-saver is not launching. In other
	 * words All applications return true, screen-saver can launch.
	 * 
	 * @return if true, screen-saver will be run, if false, will be cancel.
	 */
	public boolean confirmScreenSaver() {
		boolean watching = Resources.appStatus == VODService.APP_WATCHING;
		boolean showing = currentScene != null && currentScene.isVisible();
		boolean paused = Resources.appStatus == VODService.APP_PAUSED
				|| Resources.appStatus == VODService.APP_PAUSING;
		Log.printDebug("MenuController, confirmScreenSaver(), watching = "
				+ watching + ", showing = " + showing + ", paused = " + paused);
		if (paused || showing == false) {
			return true;
		}
		return watching == false;
	}

	public void clockUpdated(Date date) {
		timeAndDate = Formatter.getCurrent().getLongDate();
		if (currentScene != null) {
			currentScene.repaint();
		}
	}

	/**
	 * @return the timeAndDate
	 */
	public String getTimeAndDate() {
		return timeAndDate;
	}

	// /**
	// * @return the wishListUI
	// */
	// public WishlistDockUI getWishListUI() {
	// return wishListUI;
	// }

	public boolean isBlocked(MoreDetail title) {
		if (title == null)
			return false;
		
		// not under parental control
		if (Definitions.OPTION_VALUE_ON.equals(DataCenter.getInstance()
				.getString(RightFilter.PARENTAL_CONTROL)) == false) {
			return false;
		}
		
		// already put admin pin for adult category
		if (openAdultCategory) {
			return false;
		}
		
		if (isFromSearch && unlockedAdultFromSearch && Definition.RATING_OVER_18.equals(title.getRating())) {
			return false;
		}
		
		// this title is an adult content and user have not input pin yet
		if (Definition.RATING_OVER_18.equals(title.getRating())
				&& showAdultContent == false) {
			return true;
		}
		
		if (ageUnblocked) {
			return false;
		}
		
		// R5
		// VDTRMASTER-5709
		if (title instanceof Video && (((Video) title).channel != null || ((Video) title).network != null)) {
			Video v = (Video) title;
			if (v.channel != null 
					&& (ChannelInfoManager.getInstance().isBlockedChannel(v.channel)
							|| (v.channel.network != null && ChannelInfoManager.getInstance().isBlockedChannel(v.channel.network.channel)))) {
				return true;
			} else if (v.network != null && ChannelInfoManager.getInstance().isBlockedChannel(v.network.channel)) {
				return true;
			}
		} else if (title instanceof Bundle && (((Video)((Bundle) title).getTitles()[0]).channel != null || ((Video)((Bundle) title).getTitles()[0]).network != null)) {
			Bundle b = (Bundle) title;
			if (((Video)b.getTitles()[0]).channel != null 
					&& (ChannelInfoManager.getInstance().isBlockedChannel(((Video)b.getTitles()[0]).channel)
							|| (((Video)b.getTitles()[0]).channel.network != null && ChannelInfoManager.getInstance().isBlockedChannel(((Video)b.getTitles()[0]).channel.network.channel)))) {
				return true;
			} else if (((Video)b.getTitles()[0]).network != null 
					&& ChannelInfoManager.getInstance().isBlockedChannel(((Video)b.getTitles()[0]).network.channel)) {
				return true;
			}
		} else if (currentScene instanceof CategoryUI 
				&& ((CategoryUI) currentScene).getCurrentCategory().getTopic() != null) {
			if (ChannelInfoManager.getInstance().isBlockedChannel(((CategoryUI) currentScene)
						.getCurrentCategory().getTopic())) {
				return true;
			} else {
				return false;
			}
		// VDTRMASTER-5633 && VDTRMASTER-5683
		} else if (CategoryUI.getChannelCategory() != null 
				&& ChannelInfoManager.getInstance().isBlockedChannel(CategoryUI.getChannelCategory().getTopic())) {
			return true;
		}

//		// if this is a bundle, check all elements in bundle
//		// it returns true if any one element is blocked
//		if (title instanceof Bundle) {
//			if (PreferenceProxy.getInstance().isBlockedByRating(
//					title.getRating()) == false) {
//				return false;
//			}
//			// if (unblocked.contains(title.getId())) {
//			// return false;
//			// }
//
//			MoreDetail[] md = ((Bundle) title).getTitles();
//			for (int i = 0; i < md.length; i++) {
//				// if (unblocked.contains(md[i].getId()) == false) {
//				// return true;
//				// }
//			}
//			return false;
//		}

		// check rating and check unblock this title
		return PreferenceProxy.getInstance().isBlockedByRating(
				title.getRating());
		// && unblocked.contains(title.getId()) == false;
	}

	public boolean isAdultBlocked(MoreDetail title) {
		if (title == null) {
			return false;
		}

		// not under parental control
		if (Definitions.OPTION_VALUE_ON.equals(DataCenter.getInstance()
				.getString(RightFilter.PARENTAL_CONTROL)) == false) {
			return false;
		}
		// user doesn't want to hide adult content
		if (Definitions.OPTION_VALUE_NO.equals(DataCenter.getInstance()
				.getString(RightFilter.HIDE_ADULT_CONTENT))) {
			return false;
		}

		// this title is not an adult content
		if (Definition.RATING_OVER_18.equals(title.getRating()) == false) {
			return false;
		}
		
		if (isFromSearch && unlockedAdultFromSearch && Definition.RATING_OVER_18.equals(title.getRating())) {
			return false;
		}
		
		return showAdultContent == false && openAdultCategory == false;
	}

	public boolean openAdultCategory() {
		return openAdultCategory;
	}

	public void setOpenAdultCategory(boolean open, boolean fromCompanion) {
		boolean old = openAdultCategory;
		if (fromCompanion) {
			this.fromCompanion = true;
			openAdultCategoryOld = openAdultCategory;
		}
		openAdultCategory = open;
		sizeHistoryWhenOpenAdultCategory = navigationHistory.size();
		if (open) {
			
		} else {
			showAdultContent = false;
		}
		if (currentScene != null && old != open) { // DDC 76.4
			currentScene.updateButtons();
		}
	}
	
	public void showLoadingAnimation() {
		showLoadingAnimation(false);
	}

	public void showLoadingAnimation(boolean instant) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, showLoadingAnimation(), instant = " + instant);
		}
		
		// VDTRMASTER-5640
		if (isShowingLoading && !instant) return;
		
		synchronized(this.instance) {
			isShowingLoading = true;
			try {
				if (instant) {
					((LoadingAnimationService) DataCenter.getInstance().get(
							LoadingAnimationService.IXC_NAME))
							.showNotDelayLoadingAnimation(loadingPoint);
				} else {
					((LoadingAnimationService) DataCenter.getInstance().get(
							LoadingAnimationService.IXC_NAME))
							.showLoadingAnimation(loadingPoint);
				}
			} catch (Exception e) {
				Log.print(e);
			}
		}
	}

	public void hideLoadingAnimation() {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, hideLoadingAnimation()");
		}
		synchronized(this.instance) {
			isShowingLoading = false;
			try {
				((LoadingAnimationService) DataCenter.getInstance().get(
						LoadingAnimationService.IXC_NAME)).hideLoadingAnimation();
			} catch (Exception e) {
				Log.print(e);
			}
		}
	}
	
	private void playAll(BaseElement element, int selected, int opt) {
		Log.printDebug("PlayAll, element = " + element.toString(true) + ", selected = " + selected + ", opt = " + opt);
		Vector playAll = new Vector();
		Vector loop = new Vector();
		
		boolean parentalBlocked = false;
		boolean needLoop = false;
		boolean include = opt == PopPlayAll.PLAY_1_TO_LAST;
		
		BaseElement[] data = null;
		Object cachedData = null;
		BaseUI scene = currentScene instanceof TitleDetailUI ? getLastScene() : currentScene;
		if (element instanceof Season) {
			Season season = (Season) element;
			if (season.episodeBundles.length > 0) {
				data = new BaseElement[season.episodeBundles.length];
				for (int i = 0; i < data.length; i++) {
					data[i] = (BaseElement) CatalogueDatabase.getInstance().getCached(season.episodeBundles[i]);
				}
			} else {
				data = ((Season) element).episode;
			}
			include = true;
		} else if (element instanceof Bundle) {
			data = ((Bundle) element).getTitles();
		} else if (scene instanceof ListOfTitlesUI) {
			data = ((ListOfTitlesUI) scene).getData();
			cachedData = ((ListOfTitlesUI) scene).getCachedData();
		} else if (scene instanceof ListViewUI) {
			data = ((ListViewUI) scene).getData();
		} else if (scene instanceof BundleDetailUI) {
			data = ((BundleDetailUI) scene).getData();
			cachedData = ((BundleDetailUI) scene).getBundle();
		} else if (scene instanceof WallofPostersFilmInfoUI) {
			data = ((WallofPostersFilmInfoUI) scene).getData();
		} else if (scene instanceof CarouselViewUI) {
			data = ((CarouselViewUI) scene).getData();
		}
		
		for (int i = 0; i < data.length; i++) {
			if (data[i] == element) {
				include = true;
			}
			if (data[i] instanceof Bundle) {
				boolean purchased = BookmarkManager.getInstance().isPurchased((Bundle) data[i]);
				if (purchased) {
					MoreDetail[] titles = ((Bundle) data[i]).getTitles();
					for (int j = 0; j < titles.length; j++) {
						if (playableType(include ? playAll : loop, titles[j], selected, include)) {
							if (isBlocked((MoreDetail) data[i])) {
								parentalBlocked = true;
								Log.printDebug("PlayAll, there is parental blocked contents");
							}
						}
					}
				} else {
					Log.printDebug("PlayAll, not a purhcased bundle " + data[i].toString(true));
					playAll.add(data[i]); // to propose
				}
			} else {
				if (playableType(include ? playAll : loop, data[i], selected, include)) {
					if (isBlocked((MoreDetail) data[i])) {
						parentalBlocked = true;
						Log.printDebug("PlayAll, there is parental blocked contents");
					}
					
					//if ("VT_124".equals(((Video) data[i]).getTypeID())) {
					if (data[i] instanceof Episode == false && opt != PopPlayAll.PLAY_SELECTED_ONLY) {
						needLoop = true;
					}
					
					// VDTRMASTER-5204
					if (cachedData instanceof Extra || (cachedData instanceof Bundle && ((Bundle)cachedData).isMovieBundle())) {
						needLoop = false;
					}
				}
			}
		}
		Log.printDebug("PlayAll, after check, needLoop = " + needLoop + ", playAll = " + playAll.size() + ", loop = " + loop.size());
		if (needLoop) {
			playAll.addAll(loop);
			loop.clear();
			Log.printDebug("PlayAll, after add, playAll = " + playAll.size() + ", loop = " + loop.size());
		}
		
		if (playAll.size() == 0) {
			Log.printDebug("PlayAll, there is no playable contents now");
		} else {
			if (parentalBlocked) {
				final Vector playAll2 = playAll;
				PreferenceProxy.getInstance().checkRightFilter(
						RightFilter.BLOCK_BY_RATINGS,
						((MoreDetail) element).getRating(),
						new BaseUI() {
							public void receiveCheckRightFilter(CheckResult result) {
								if (result.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
									goToNextScene(UITemplateList.PLAY_SCREEN, playAll2);
								}
							}
						},
						new String[] {
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN1),
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN2) });
			} else {
				goToNextScene(UITemplateList.PLAY_SCREEN, playAll);
			}
		}
	}
	
	/*
	 * VOD should show a button ‘Watch all’ with type. 
 		These are types to determine: 
		-       Movie --> <video> with videoType VT_102 
		-       Movie Bundle  --> <bundle> with <video> inside 
		-       TV Show --> <video> with videoType VT_100 
		-       Episode --> <episode> 
		-       Series Bundle --> <bundle> with all episodes of a season (for season bundle, this is already known to the STB), <bundle> with all episodes of all seasons of a series 
		-       Trailer --> <video> with videoType VT_124 
		-       Extra --> <video> with videoType VT_104 

	 */
	private boolean playableType(Vector playAll, BaseElement element, int selected, boolean include) {
		boolean ret = false;
		if (element == null) {
			Log.printDebug("PlayAll, playableType, element is null");
			return ret;
		}
		if (element instanceof Video) {
			Video v = (Video) element;
			
			if (v instanceof Episode || "VT_100".equals(v.getTypeID()) || "VT_102".equals(v.getTypeID()) || "VT_103".equals(v.getTypeID()) || "VT_104".equals(v.getTypeID()) || "VT_124".equals(v.getTypeID())) {
				v.selected = selected;
				playAll.add(v);
				ret = true;
				Log.printDebug("PlayAll, playableType, added " + (include ? " in list " : " in loop ") + element.toString(true) + "/" + v.getType());
			} else {
				Log.printDebug("PlayAll, playableType, not a type " + v.toString(true) + ", type = " + v.getTypeID() + "/" + v.getType());
			}
		} else {
			Log.printDebug("PlayAll, playableType, not a type " + element.toString(true));
		}
		return ret;
	}

	public void processActionMenu(String actionMenu, BaseElement element) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, processActionMenu()" + ", cur = "
					+ currentScene + ", menu = " + actionMenu + ", element = "
					+ (element == null ? "NULL" : element.toString(false))
					+ ", isEDT = " + EventQueue.isDispatchThread());
		}
		if (currentScene == null)
			return;

		if (EventQueue.isDispatchThread() 
				&& actionMenu.equals(lastAction) 
				&& System.currentTimeMillis() - lastActionTime < 1000) {
			Log.printDebug("MenuController, processActionMenu(), return only because it's duplicated action");
			return;
		}

		lastAction = actionMenu;
		lastActionTime = System.currentTimeMillis();
		lastObject = element;
		
		DataCenter dataCenter = DataCenter.getInstance();
		if (dataCenter.getString(Resources.TEXT_VIEW_BUNDLE_ITEMS).equals(actionMenu)
				|| dataCenter.getString(Resources.TEXT_VIEW_EPISODES).equals(actionMenu)) {
			if (currentScene instanceof ListViewUI && ((ListViewUI) currentScene).isResumeViewing()) {
				((ListViewUI) currentScene).exploreBundle();
			}
		} else if (dataCenter.getString(Resources.TEXT_UNBLOCK).equals(actionMenu)) {
			VbmController.getInstance().writeSelectionDummy();
			VbmController.getInstance().writeSelection("Unblock");
			if (element instanceof MoreDetail) {
				titleToUnblock = (MoreDetail) element;
				// PreferenceProxy.getInstance().checkRightFilter(
				// RightFilter.BLOCK_BY_RATINGS, ((MoreDetail)
				// element).getRating(), menuAdaptor);
				PreferenceProxy.getInstance().checkRightFilter(
						RightFilter.BLOCK_BY_RATINGS,
						((MoreDetail) element).getRating(),
						menuAdaptor,
						new String[] {
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN1),
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN2) });
			}
		} else if (actionMenu.startsWith(dataCenter.getString(Resources.TEXT_PLAY_ALL))
				|| actionMenu.startsWith(dataCenter.getString(Resources.TEXT_WATCH_ALL))) {
			
			// 4K
			// DDC-107
			BaseElement[] elements = null;
			BaseUI baseScene = currentScene instanceof TitleDetailUI ? getLastScene() : currentScene;
			if (baseScene instanceof ListOfTitlesUI) {
				elements = ((ListOfTitlesUI) baseScene).getData();
			} else if (baseScene instanceof ListViewUI) {
				elements = ((ListViewUI) baseScene).getData();
			} else if (baseScene instanceof BundleDetailUI) {
				elements = ((BundleDetailUI) baseScene).getData();
			} else if (baseScene instanceof CarouselViewUI) {
				elements = ((CarouselViewUI) baseScene).getData();
			} else if (baseScene instanceof WallofPostersFilmInfoUI) {
				elements = ((WallofPostersFilmInfoUI) baseScene).getData();
			}
			
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuController, processActionMenu(String, BaseElement), elements=" + elements);
			}
			if (elements != null) {
				for (int i = 0; i < elements.length; i++) {
					BaseElement e = elements[i];
					if (e instanceof Video) {
						Video v = (Video) e;
						// VDTRMASTER-5517
						if (v.getOfferHD(null) != null) {
							if (Log.DEBUG_ON) {
								Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
										+ v.getOfferHD(null).definition + ", canPlayUHDContent=" 
										+ VideoOutputUtil.getInstance().canPlayUHDContent());
							}
							if (v.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
								if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
									// DDC-114
									Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
											+ "Error Code : VOD850");
									CommunicationManager.getInstance().commonMessage("VOD850", null);
									VbmController.getInstance().writeSelection("WatchALL");
									return;
								} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
										|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
									// DDC-114
									Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
											+ "Error Code : VOD851");
									CommunicationManager.getInstance().commonMessage("VOD851", null);
									VbmController.getInstance().writeSelection("WatchALL");
									return;
								}
							}
						}
					} else if (e instanceof Bundle) {
						Bundle bundle = (Bundle) e;
						// VDTRMASTER-5517
						if (bundle.getOfferHD(null) != null) {
							if (Log.DEBUG_ON) {
								Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
										+ bundle.getOfferHD(null).definition + ", canPlayUHDContent=" 
										+ VideoOutputUtil.getInstance().canPlayUHDContent());
							}
							if (bundle.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
								if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
									// DDC-114
									Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
											+ "Error Code : VOD850");
									CommunicationManager.getInstance().commonMessage("VOD850", null);
									VbmController.getInstance().writeSelection("WatchALL");
									return;
								}  else if (!VideoOutputUtil.getInstance().existHdcpApi() 
										|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
									// DDC-114
									Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
											+ "Error Code : VOD851");
									CommunicationManager.getInstance().commonMessage("VOD851", null);
									VbmController.getInstance().writeSelection("WatchALL");
									return;
								}
							} 
						}
					}
				}
			}
			
			if (EventQueue.isDispatchThread()) {
				try {
					CommunicationManager.getInstance().requestTuner(actionMenu, element, 0);
					return;
				} catch (NullPointerException npe) {}
			}
			VbmController.getInstance().writeSelection("WatchALL");
			
			// VDTRMASTER-5166 [CQ][PO1/2/3][R4][VOD] 'Play all' confirmation popup should not be shown
			if (element instanceof Bundle) { // click on Bundle, just play 1 to last its bundle
				menuAdaptor.popUpClosed(PopPlayAll.getInstance(), new Object[] { element, new Integer(PopPlayAll.PLAY_1_TO_LAST) });
				return;
			} else if (actionMenu.equals(dataCenter.getString(Resources.TEXT_PLAY_ALL) + " " + dataCenter.getString(Resources.TEXT_LBL_TRAILERS))
					|| actionMenu.equals(dataCenter.getString(Resources.TEXT_WATCH_ALL) + dataCenter.getString(Resources.TEXT_LBL_TRAILERS))){
				menuAdaptor.popUpClosed(PopPlayAll.getInstance(), new Object[] { element, new Integer(PopPlayAll.PLAY_SEL_TO_LAST) });
				return;
			}
			
			boolean isFirst = false, isLast = false;
			BaseUI scene = currentScene instanceof TitleDetailUI ? getLastScene() : currentScene;
			if (scene instanceof ListOfTitlesUI) {
				isFirst = ((ListOfTitlesUI) scene).focusOnFirst();
				isLast = ((ListOfTitlesUI) scene).focusOnLast();
			} else if (scene instanceof ListViewUI) {
				isFirst = ((ListViewUI) scene).focusOnFirst();
				isLast = ((ListViewUI) scene).focusOnLast();
			} else if (scene instanceof BundleDetailUI) {
				isFirst = ((BundleDetailUI) scene).focusOnFirst();
				isLast = ((BundleDetailUI) scene).focusOnLast();
			} else if (scene instanceof CarouselViewUI) {
				isFirst = ((CarouselViewUI) scene).focusOnFirst();
				isLast = ((CarouselViewUI) scene).focusOnLast();
			} else if (scene instanceof WallofPostersFilmInfoUI) {
				isFirst = ((WallofPostersFilmInfoUI) scene).focusOnFirst();
				isLast = ((WallofPostersFilmInfoUI) scene).focusOnLast();
			}

			// R7.3
			// Watch all start from selected title to end for episode.
            // VDTRMASTER-6083
			if (actionMenu.startsWith(dataCenter.getString(Resources.TEXT_WATCH_ALL))) {
				menuAdaptor.popUpClosed(PopPlayAll.getInstance(), new Object[]{element, new Integer(PopPlayAll.PLAY_SEL_TO_LAST)});
			} else {

				if (isFirst) {
					menuAdaptor.popUpClosed(PopPlayAll.getInstance(), new Object[]{element, new Integer(PopPlayAll.PLAY_1_TO_LAST)});
				} else {
					showPlayAllPopup(element, isLast);
				}
			}
			
			
		} else if (dataCenter.getString(Resources.TEXT_WATCH).equals(actionMenu)
				|| dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE).equals(actionMenu)
				|| dataCenter.getString(Resources.TEXT_SELECT_A_MOVIE).equals(actionMenu)
				|| dataCenter.getString(Resources.BTN_FREE_WATCH_NOW).equals(actionMenu)
				|| dataCenter.getString(Resources.BTN_FREE_WATCH_NOW).indexOf(actionMenu) > -1) { // VDTRMASTER-5540
			// 4K
			// DDC-107
			if (element instanceof Video) {
				Video v = (Video) element;
				// VDTRMASTER-5517
				if (v.getOfferHD(null) != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
								+ v.getOfferHD(null).definition + ", canPlayUHDContent=" 
								+ VideoOutputUtil.getInstance().canPlayUHDContent());
					}
					if (v.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
						if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD850");
							CommunicationManager.getInstance().commonMessage("VOD850", null);
							
							// Touche:  VDTRMASTER-6248
							if (currentScene instanceof TitleDetailUI)
								VbmController.getInstance().writeSelection("A_Watch Now");
							else
								VbmController.getInstance().writeSelection("Watch Now");
							
							return;
						} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
								|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD851");
							CommunicationManager.getInstance().commonMessage("VOD851", null);
							
							// Touche:  VDTRMASTER-6248
							if (currentScene instanceof TitleDetailUI)
								VbmController.getInstance().writeSelection("A_Watch Now");
							else
								VbmController.getInstance().writeSelection("Watch Now");
							return;
						}
					}
				}
			} else if (element instanceof Bundle) {
				Bundle bundle = (Bundle) element;
				// VDTRMASTER-5517
				if (bundle.getOfferHD(null) != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
								+ bundle.getOfferHD(null).definition + ", canPlayUHDContent=" 
								+ VideoOutputUtil.getInstance().canPlayUHDContent());
					}
					if (bundle.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
						if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD850");
							CommunicationManager.getInstance().commonMessage("VOD850", null);
							
							// Touche:  VDTRMASTER-6248
							if (currentScene instanceof TitleDetailUI)
								VbmController.getInstance().writeSelection("A_Watch Now");
							else
								VbmController.getInstance().writeSelection("Watch Now");
							
							return;
						} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
								|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD851");
							CommunicationManager.getInstance().commonMessage("VOD851", null);
							
							// Touche:  VDTRMASTER-6248
							if (currentScene instanceof TitleDetailUI)
								VbmController.getInstance().writeSelection("A_Watch Now");
							else
								VbmController.getInstance().writeSelection("Watch Now");
							
							return;
						}
					} 
				}
			}
			
			if (EventQueue.isDispatchThread()) {
				try {
					CommunicationManager.getInstance().requestTuner(actionMenu, element, 0);
					return;
				} catch (NullPointerException npe) {}
			}
			// VDTRMASTER-6248
			if (currentScene instanceof TitleDetailUI)
				VbmController.getInstance().writeSelection("A_Watch Now");
			else
				VbmController.getInstance().writeSelection("Watch Now");
			
			if (element instanceof Video) {
				if (BookmarkManager.getInstance().isPurchased(
						(Video) element) != null) {
					Bookmark purchased = BookmarkManager.getInstance()
							.getPurchased((Video) element);
					if (purchased != null) {
						CatalogueDatabase.getInstance().registerBySspId(
								purchased.getOrderableName(), element);
						goToNextScene(UITemplateList.PLAY_SCREEN, purchased);
					}
				} else if (((Video) element).isFree()) {
					currentScene.setPopUp(PopSelectOption.getInstance());
					PopSelectOption.getInstance().show(menuAdaptor, element);
				}
			} else if (element instanceof Bundle) {
				Bundle bundle = (Bundle) element;
				if (BookmarkManager.getInstance().isPurchased(bundle)) {
					Bookmark purchased = BookmarkManager.getInstance()
							.getPurchased(bundle);
					if (purchased != null) {
						CatalogueDatabase.getInstance().registerBySspId(
								purchased.getOrderableName(), element);
						//goToNextScene(UITemplateList.PLAY_SCREEN, purchased);
						
						if (bundle.getTitles().length == 0) {
							CommunicationManager.getInstance().errorMessage("VOD105-010", null);
							return;
						}
						
						objectToPlay = purchased;
						showSelectItem(new Object[] {
								new Integer(bundle.getType(purchased.getOrderableName())),
								bundle.getId()	
						});
					}
				} else if (bundle.isFree()) {
					currentScene.setPopUp(PopSelectOption.getInstance());
					PopSelectOption.getInstance().show(menuAdaptor, element);
				}
			}
		} else if (dataCenter.getString(Resources.TEXT_ORDER).equals(actionMenu) 
				|| dataCenter.getString(Resources.TEXT_ORDER_TITLE).equals(actionMenu)) {
			
			// 4K
			// DDC-107
			if (element instanceof Video) {
				Video v = (Video) element;
				// VDTRMASTER-5517
				if (v.getOfferHD(null) != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
								+ v.getOfferHD(null).definition + ", canPlayUHDContent=" 
								+ VideoOutputUtil.getInstance().canPlayUHDContent());
					}
					if (v.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
						if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD850");
							CommunicationManager.getInstance().commonMessage("VOD850", null);
							// R7.3
                            if (currentScene instanceof TitleDetailUI) {
                                VbmController.getInstance().writeSelection("A_Order");
                            } else {
                                VbmController.getInstance().writeSelection("Order");
                            }
							return;
						} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
								|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD851");
							CommunicationManager.getInstance().commonMessage("VOD851", null);
							// R7.3
                            if (currentScene instanceof TitleDetailUI) {
                                VbmController.getInstance().writeSelection("A_Order");
                            } else {
                                VbmController.getInstance().writeSelection("Order");
                            }
							return;
						}
					}
				}
			} else if (element instanceof Bundle) {
				Bundle bundle = (Bundle) element;
				// VDTRMASTER-5517
				if (bundle.getOfferHD(null) != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
								+ bundle.getOfferHD(null).definition + ", canPlayUHDContent=" 
								+ VideoOutputUtil.getInstance().canPlayUHDContent());
					}
					if (bundle.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
						
						if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD850");
							CommunicationManager.getInstance().commonMessage("VOD850", null);
							VbmController.getInstance().writeSelection("Order Bundle");
							return;
						} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
								|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD851");
							CommunicationManager.getInstance().commonMessage("VOD851", null);
							VbmController.getInstance().writeSelection("Order Bundle");
							return;
						}
					}
				}
			}
			
			if (EventQueue.isDispatchThread()) {
				try {
					CommunicationManager.getInstance().requestTuner(actionMenu, element, 0);
					return;
				} catch (NullPointerException npe) {}
			}
			// R7.3
			if (currentScene instanceof TitleDetailUI) {
                VbmController.getInstance().writeSelection("A_Order");
            } else {
                VbmController.getInstance().writeSelection("Order");
            }
			if (element instanceof Season) {
				Season season = (Season) element;
				if (season.sections.length > 0) {
					forwardHistory(season);
					goToNextScene(UITemplateList.LIST_OF_TITLES, season.sections);
				} else {
					checkOptionAndGo(season, menuAdaptor);
				}
			} else {
				purchaseContent(element, null);
			}
		} else if (dataCenter.getString(Resources.TEXT_ORDER_BUNDLE).equals(
				actionMenu)
				|| actionMenu.startsWith(DataCenter.getInstance().getString(
						Resources.TEXT_ORDER_EPISODES))) {
			// 4K
			// DDC-107
			if (element instanceof Bundle) {
				Bundle bundle = (Bundle) element;
				// VDTRMASTER-5517
				if (bundle.getOfferHD(null) != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
								+ bundle.getOfferHD(null).definition + ", canPlayUHDContent=" 
								+ VideoOutputUtil.getInstance().canPlayUHDContent());
					}
					if (bundle.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
						if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
							// DDC-114
							Log.printDebug("MenuController, playContent(String[], boolean[], String[], String[]), "
									+ "Error Code : VOD850");
							CommunicationManager.getInstance().commonMessage("VOD850", null);
                            VbmController.getInstance().writeSelection("Order Bundle");
							return;
							// VDTRMASTER-5560
						} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
								|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD851");
							CommunicationManager.getInstance().commonMessage("VOD851", null);
							VbmController.getInstance().writeSelection("Order Bundle");
							return;
						}
					}
				}
			}
			
			if (EventQueue.isDispatchThread()) {
				try {
					CommunicationManager.getInstance().requestTuner(actionMenu, element, 0);
					return;
				} catch (NullPointerException npe) {}
			}
			VbmController.getInstance().writeSelection("Order Bundle");
			purchaseContent(element, null);
		} else if (dataCenter.getString(Resources.TEXT_SEE_BUNDLE).equals(
				actionMenu)) {
			VbmController.getInstance().writeSelection("Explore Bundle");
			if (fromBundleDetail()) {
				back();
			} else {
				Bundle bundle = null;
				String bundleId = null;
				if (element instanceof Video) {
					bundleId = ((Video) element).inBundlesAR[0];
					bundle = (Bundle) CatalogueDatabase.getInstance()
							.getCached(bundleId);
				} else if (element instanceof Bundle) {
					bundle = (Bundle) element;
					bundleId = bundle.id;
				}
				if (bundle == null || bundle.bundleDetail == null
						|| bundle.hasAllData() == false) {
					showLoadingAnimation();
					CatalogueDatabase.getInstance().retrieveBundle(menuAdaptor,
							bundleId, true, null);
				} else {
					menuAdaptor.reflectVCDSResult(bundle);
				}
			}
		} else if (dataCenter.getString(Resources.TEXT_SEE_TRAILER).equals(
				actionMenu)) {
			
			// 4K
			// DDC-107
			if (element instanceof Video) {
				Video v = (Video) element;
				// VDTRMASTER-5517
				if (v.getOfferHD(null) != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("MenuController, processActionMenu(String, BaseElement), video.definition=" 
								+ v.getOfferHD(null).definition + ", canPlayUHDContent=" 
								+ VideoOutputUtil.getInstance().canPlayUHDContent());
					}
					if (v.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
						if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD850");
							CommunicationManager.getInstance().commonMessage("VOD850", null);
							// R7.3
							if (currentScene instanceof TitleDetailUI) {
                                VbmController.getInstance().writeSelection("A_Watch Trailer");
                            } else {
                                VbmController.getInstance().writeSelection("Watch Trailer");
                            }
							return;
						} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
								|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
							// DDC-114
							Log.printDebug("MenuController, processActionMenu(String, BaseElement), "
									+ "Error Code : VOD851");
							CommunicationManager.getInstance().commonMessage("VOD851", null);
                            // R7.3
                            if (currentScene instanceof TitleDetailUI) {
                                VbmController.getInstance().writeSelection("A_Watch Trailer");
                            } else {
                                VbmController.getInstance().writeSelection("Watch Trailer");
                            }
							// VDTRMASTER-5556
							return;
						}
					}
				}
			} 
						
			if (EventQueue.isDispatchThread()) {
				try {
					CommunicationManager.getInstance().requestTuner(actionMenu, element, 0);
					return;
				} catch (NullPointerException npe) {}
			}
            // R7.3
            if (currentScene instanceof TitleDetailUI) {
                VbmController.getInstance().writeSelection("A_Watch Trailer");
            } else {
                VbmController.getInstance().writeSelection("Watch Trailer");
            }
			if (element instanceof Video) {
				UsageManager.getInstance().addUsageLog(getScreenId(), "",
						"1203", ((MoreDetail) element).getId());
				// goToNextScene(UITemplateList.FULL_TRAILER,
				// ((Video) element).getTrailer());
				Playout p = ((Video) element).getTrailer();
				VODServiceImpl.getInstance().showPopupVideo(p.name, p.duration,
						null, (Video) element, p);
			}
		} else if (dataCenter.getString(Resources.TEXT_SELECT).equals(
				actionMenu)) {
			VbmController.getInstance().writeSelection("Select");
			if (element instanceof CategoryContainer) {
				try {
				//R7.4 sykim VDTRMASTER-6186
				if (((CategoryContainer) element).category != null &&
						((CategoryContainer) element).category.id != null)
					VbmController.getInstance().writeSelectionAddHistory(((CategoryContainer) element).category.id, 
							((CategoryContainer) element).parent);
				} catch (Exception e) {
					e.printStackTrace();
				}
				goToCategory((CategoryContainer) element, menuAdaptor);
			} else if (element instanceof CategorizedBundle) {
				if (currentScene instanceof CategoryUI) {
					CategoryUI.getInstance().moveToKaraokePackage(true);
				} else {
					goToDetail(element, null);
				}
//				backwardHistory(false);
//				back();
//				CategoryUI.getInstance().moveToKaraokePackage(true);
			}
		} else if (dataCenter.getString(Resources.TEXT_SELECT_THIS_CONTENT).equals(actionMenu)) {
			goToDetail(element, null);
		} else if (dataCenter.getString(Resources.TEXT_SELECT_SERIES).equals(
				actionMenu)) {
			VbmController.getInstance().writeSelectionAddHistory("Select Series", null);
			goToDetail(element, null);
		} else if (dataCenter.getString(Resources.TEXT_SELECT_SEASON).equals(
				actionMenu)) {
			VbmController.getInstance().writeSelectionAddHistory("Select Season", null);
			goToDetail(element, null);
		} else if (actionMenu.startsWith(dataCenter.getString(Resources.TEXT_SELECT_SEASON))) {
			VbmController.getInstance().writeSelectionAddHistory("Select Season", null);
			goToDetail(element, null);
		} else if (dataCenter.getString(Resources.TEXT_GOTO_CLUB).equals(actionMenu)) {
			VbmController.getInstance().writeSelectionAddHistory("Go to Club Unlimited", null);
//			// TODO
//			Service service = ((AvailableSource) element).service;
//			
//			if (CommunicationManager.getInstance().isaSupported(service.getId2())) {
//				CommunicationManager.getInstance().showISA(service.getId2(), ISAService.ENTRY_POINT_VOD_E04);
//			} else {
//				currentScene.setPopUp(PopSubscribe.getInstance());
//				PopSubscribe.getInstance().show(currentScene, service);
//			}
			
			goToDetail(element, null);
			
		} else if (dataCenter.getString(Resources.TEXT_WATCH_IN_CLUB).equals(actionMenu)) {
			VbmController.getInstance().writeSelectionAddHistory("Watch in Club Unlimited", null);
			goToDetail(element, null);
		} else if (dataCenter.getString(Resources.TEXT_GOTO_CHANNEL).equals(actionMenu)) {
			VbmController.getInstance().writeSelectionAddHistory("Go to Channel", null);
			goToDetail(element, null);
		} else if (dataCenter.getString(Resources.TEXT_REMOVE_FROM_WISHLIST).equals(actionMenu)
				|| dataCenter.getString(Resources.TEXT_REMOVE_FROM_WISHLIST_18).equals(actionMenu)) {
			VbmController.getInstance().writeSelection("Remove from Wishlist");
			if (element instanceof Video) {
				String sspId = BookmarkManager.getInstance().deleteWishlist(
						((Video) element).getSspIds());
				if (sspId != null) {
					UsageManager.getInstance().addUsageLog(getScreenId(), "",
							"1208", sspId);
				
					BookmarkManager.getInstance().retrieveWishlistTitles(false);
					BookmarkManager.getInstance().organize();
					menuAdaptor.popUpClosed(PopAddWishlist.getInstance(),
							Boolean.TRUE);
				}
			} else if (element instanceof Bundle) {
				String sspId = BookmarkManager.getInstance().deleteWishlist(((Bundle) element).getSspId());
				if (sspId != null) {
					UsageManager.getInstance().addUsageLog(getScreenId(), "",
							"1208", sspId);
					BookmarkManager.getInstance().retrieveWishlistTitles(false);
					BookmarkManager.getInstance().organize();
					menuAdaptor.popUpClosed(PopAddWishlist.getInstance(),
							Boolean.TRUE);
				}
			}

		} else if (dataCenter.getString(Resources.TEXT_ADDTO_WISHLIST).equals(actionMenu)
				|| dataCenter.getString(Resources.TEXT_ADDTO_WISHLIST_18).equals(actionMenu)) {
		    // R7.3
		    if (currentScene instanceof  TitleDetailUI) {
                VbmController.getInstance().writeSelection("A_Add to Wishlist");
            } else {
                VbmController.getInstance().writeSelection("Add to Wishlist");
            }
			if (element instanceof Video) {
				Video video = (Video) element;
				String sspId = video.getSspId();
				if (sspId.length() == 0) { // it maybe before retrieving
					synchronized (CatalogueDatabase.getInstance()) {
						ArrayList list = new ArrayList();
						list.add(video.getId());
						if (video.getId().startsWith("V")) {
							CatalogueDatabase.getInstance().retrieveVideos(
									null, list, null);
						} else {
							CatalogueDatabase.getInstance().retrieveEpisodes(
									null, list, null);
						}

						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {
						}
					}
					sspId = video.getSspId();
				}
				boolean ret = BookmarkManager.getInstance()
						.storeWishlist(sspId, false);
				if (ret) {
					UsageManager.getInstance().addUsageLog(getScreenId(), "",
							"1207", sspId);
					BookmarkManager.getInstance().retrieveWishlistTitles(false);
					BookmarkManager.getInstance().organize();

					menuAdaptor.popUpClosed(PopAddWishlist.getInstance(),
							Boolean.TRUE);
					setLastWishContent((Video)element);
				}
			} else if (element instanceof Bundle) {
				String sspId = ((Bundle) element).getSspId();
				boolean ret = BookmarkManager.getInstance().storeWishlist(sspId, true);
				if (ret) {
					UsageManager.getInstance().addUsageLog(getScreenId(), "",
							"1207", sspId);
					BookmarkManager.getInstance().retrieveWishlistTitles(false);
					BookmarkManager.getInstance().organize();

					menuAdaptor.popUpClosed(PopAddWishlist.getInstance(),
							Boolean.TRUE);
					setLastWishContent((Bundle)element);
				}
			}
		} else if (dataCenter.getString(Resources.TEXT_SUBSCRIBE).equals(actionMenu) 
				|| dataCenter.getString(Resources.TEXT_CHANGE_MY_PKG).equals(actionMenu)
				|| dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG).equals(actionMenu)) {
			//R7.4 VBM measurement changed
			VbmController.getInstance().writeSelection("Content Subscribe");
			//VbmController.getInstance().writeSelection("Subscribe");
			
			//currentScene.setPopUp(PopSubscribe.getInstance());
			
			if (currentScene instanceof ListOfTitlesUI && ((ListOfTitlesUI)currentScene).isFromAvailableSource()) {
				Log.printDebug("isFromAvailableSource is true");
				BaseElement lastObject = ((ListOfTitlesUI)currentScene).getLastObject();
				AvailableSource available = (AvailableSource) lastObject;
				
				if (available.channel != null) {
					String watch = null;
					if (available.channel.hasAuth == false) {
						available.channel.hasAuth = CommunicationManager.getInstance().checkAuth(available.channel.callLetters);
					}
					if (available.channel.hasAuth) {
						watch = available.channel.callLetters;
					}
					
					if (watch == null) {
						String entry = element instanceof Episode ? ISAService.ENTRY_POINT_VOD_E03 : ISAService.ENTRY_POINT_VOD_E02;
						CommunicationManager.getInstance().showISA(available.channel.callLetters, entry);
					}
				} else if (available.network != null) {
					String watch = null;
					for (int i = 0; i < available.network.channel.length; i++) {
						if (available.network.channel[i].hasAuth == false) {
							available.network.channel[i].hasAuth = CommunicationManager.getInstance().checkAuth(available.network.channel[i].callLetters);
						}
						if (available.network.channel[i].hasAuth) {
							watch = available.network.channel[i].callLetters;
							break;
						}
					}
					
					if (watch == null) {
						String entry = element instanceof Episode ? ISAService.ENTRY_POINT_VOD_E03 : ISAService.ENTRY_POINT_VOD_E02;
						CommunicationManager.getInstance().showISA(available.network.getId(), entry);
					}
				}
			} else {
				Channel ch = CategoryUI.getChannel();
				if (ch == null || (currentScene instanceof ListViewUI && ((ListViewUI) currentScene).isWishList())) { // from search or banner
					MoreDetail md = null;
					if (currentScene instanceof TitleDetailUI) {
						ch = ((TitleDetailUI) currentScene).getTitle().channel;
						//ch.loadLogo(false);
					} else if (currentScene instanceof ListOfTitlesUI) {
						md = (MoreDetail) ((ListOfTitlesUI) currentScene).getCurrentCatalogue();
					} else if (currentScene instanceof WallofPostersFilmInfoUI) {
						md = (MoreDetail) ((WallofPostersFilmInfoUI) currentScene).getCurrentCatalogue();
					} else if (currentScene instanceof CarouselViewUI) {
						md = (MoreDetail) ((CarouselViewUI) currentScene).getCurrentCatalogue();
					} else if (currentScene instanceof ListViewUI) {
						md = (MoreDetail) ((ListViewUI) currentScene).getCurrentCatalogue();
					}
					if (md != null) {
						if (md instanceof Bundle && ((Bundle) md).getTitles().length > 0) {
							md = ((Bundle) md).getTitles()[0];
						}
						if (md instanceof Video) {
							ch = ((Video) md).channel;
						}
					}
					if (ch != null) {
						if (ch.network != null && ch.network.imageGroup.length > 0 && ch.imageGroup.length == 0) {
							ch.imageGroup = ch.network.imageGroup;
						}
						ch.loadLogo(false, false);
					}
				}
				//PopSubscribe.getInstance().show(currentScene, ch);
				String entry = element instanceof Episode ? ISAService.ENTRY_POINT_VOD_E03 : ISAService.ENTRY_POINT_VOD_E02;
				CommunicationManager.getInstance().showISA(ch.callLetters, entry);
			}
//		} else if (dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG).equals(actionMenu)) {
//			CommunicationManager.getInstance().launchCYO();
		} else if (actionMenu.startsWith(dataCenter.getString(Resources.TEXT_SUBSCRIBE_SVOD))) {
			VbmController.getInstance().writeSelection("SubscribeSVOD");
			
			Service service = null;
			if (currentScene instanceof ListOfTitlesUI && ((ListOfTitlesUI)currentScene).isFromAvailableSource()) {
				Log.printDebug("isFromAvailableSource is true");
				BaseElement lastObject = ((ListOfTitlesUI)currentScene).getLastObject();
				AvailableSource available = (AvailableSource) lastObject;
				
				if (available.service != null) {
					service = available.service;
				} 
			} else {
			
				if (element instanceof Video) {
					Video video = (Video) element;
					service = video.service[0];
					if (service.description == null) {
						service = (Service) CatalogueDatabase.getInstance().getCached(service.id);
						video.service[0] = service;
					}
					
					final Service serviceForThread = service;
					if (service.description.logos[BrandingElement.LOGO_LG] == null) {
						new Thread("Branding image loading") {
							public void run() {
								serviceForThread.description.loadLogo(false, true);
							}
						}.start();
					}
				} else if (element instanceof Bundle) {
					Bundle bundle = (Bundle) element;
					service = bundle.service[0];
					if (service.description == null) {
						service = (Service) CatalogueDatabase.getInstance().getCached(service.id);
						bundle.service[0] = service;
					}
					
					final Service serviceForThread = service;
					if (service.description.logos[BrandingElement.LOGO_LG] == null) {
						new Thread("Branding image loading") {
							public void run() {
								serviceForThread.description.loadLogo(false, true);
							}
						}.start();
					}
				}
			}

			if (CommunicationManager.getInstance().isaSupported(service.getId2())
					|| !CommunicationManager.getInstance().isIncludedInExcludedData(service.getId2())) {
				String entry = element instanceof Episode ? ISAService.ENTRY_POINT_VOD_E05 : ISAService.ENTRY_POINT_VOD_E04;
				CommunicationManager.getInstance().showISA(service.getId2(), entry);
			} else {
				currentScene.setPopUp(PopSubscribe.getInstance());
				PopSubscribe.getInstance().show(currentScene, service);
			}
			
		} else if (dataCenter.getString(Resources.TEXT_SEE_MORE_CONTENT).equals(actionMenu)) {
			VbmController.getInstance().writeSelectionAddHistory("Other seasons", null);
			goToDetail(element, null);
		}
		currentScene.repaint();
	}
	
	BaseElement next;
	public void setNext(BaseElement next) {
		this.next = next;
	}
	
	public BaseElement getNext() {
		BaseElement ret = next;
		next = null;
		return ret;
	}
	
	boolean moveToNext;
	public void setMoveToNext(boolean move) {
		moveToNext = move;
	}
	
	public boolean isMoveToNext() {
		return moveToNext;
	}
	
	public BaseElement getNext(BaseElement current) {
		if (current == null) return null;
		if (sceneStack.isEmpty()) return null;
		
		BaseUI parent = getParentUI();
		Log.printDebug("MenuController, getNext(), parent = " + parent);
		BaseElement[] data = null;
		if (parent instanceof ListOfTitlesUI) {
			data = ((ListOfTitlesUI) parent).getData();
		} else if (parent instanceof BundleDetailUI) {
			data = ((BundleDetailUI) parent).getData();
		} else if (parent instanceof ListViewUI) {
			data = ((ListViewUI) parent).getData();
		//R7.4 sykim VDTRMASTER-6155	
		} else if (parent instanceof CarouselViewUI) {
			data = ((CarouselViewUI) parent).getData();
		} else if (parent instanceof WallofPostersFilmInfoUI) {
			data = ((WallofPostersFilmInfoUI) parent).getData();
		}
			
			
		if (data == null) return null;
		
		BaseElement next = null;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null && data[i].equals(current) && i < data.length - 1) {
				next = data[i+1];
				break;
			}
		}
		// in case of bundle list
		for (int i = 0; next == null && i < data.length; i++) {
			if (data[i] != null && data[i] instanceof Bundle && ((Bundle) data[i]).hasContent(current)) {
				MoreDetail[] details = ((Bundle) data[i]).getTitles();
				
				int index = -1;
				if (details != null) {
					for (int j = 0; j < details.length && details[j] != null; j++) {
						if (((MoreDetail)current).getId().equals(details[j].getId())) {
							index = j;
							break;
						}
					}
				}
				
				if (index + 1 < details.length) {
					next = details[index + 1];
				} else {
					next = data[i+1];
				}
				break;
			}
		}
		Log.printDebug("MenuController, getNext(), next = " + (next == null ? "NULL" : next.toString(true)));
		return next;
	}
	
	public BaseElement getNextBundle(BaseElement current) {
		
		if (current == null) return null;
		if (sceneStack.isEmpty()) return null;
		
		BaseUI parent = getParentUI();
		Log.printDebug("MenuController, getNext(), parent = " + parent);
		BaseElement[] data = null;
		if (parent instanceof BundleDetailUI) {
			parent = (BaseUI) sceneStack.get(sceneStack.size() - 2);
		}
		
		if (parent instanceof ListOfTitlesUI) {
			data = ((ListOfTitlesUI)parent).getData();
		} else if (parent instanceof ListViewUI) {
			data = ((ListViewUI)parent).getData();
		} else if (parent instanceof CarouselViewUI) {
			data = ((CarouselViewUI)parent).getData();
		} else if (parent instanceof WallofPostersFilmInfoUI) {
			data = ((WallofPostersFilmInfoUI)parent).getData();
		}
		
		
		if (data == null) return null;
		
		BaseElement next = null;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null && data[i].equals(current) && i < data.length - 1) {
				next = data[i+1];
				break;
			}
		}
		// in case of bundle list
		for (int i = 0; next == null && i < data.length; i++) {
			if (data[i] != null && data[i] instanceof Bundle && ((Bundle) data[i]).hasContent(current) && i < data.length - 1) {
				next = data[i+1];
				break;
			}
		}
		Log.printDebug("MenuController, getNextBundle(), next = " + (next == null ? "NULL" : next.toString(true)));
		return next;
		
	}
	
	public BaseUI getParentUI() {
		
		if (sceneStack.size() == 0) {
			return null;
		}
		
		BaseUI parent = (BaseUI) sceneStack.peek();
		if (parent instanceof TitleDetailUI && sceneStack.size() >= 2) {
			parent = (BaseUI) sceneStack.get(sceneStack.size() - 2);
		}
		
		return parent;
	}
	
	public boolean isFromTitleDetailUI() {
		if (sceneStack.size() == 0) {
			return false;
		}
		
		BaseUI parent = (BaseUI) sceneStack.peek();
		if (parent instanceof TitleDetailUI ) {
			return true;
		}
		return false;
	}
	
	public void refreshEffect() {
		Rectangle b = null;
		
		if (currentScene.getFooter().getParent() instanceof SimilarContentView) {
			b = currentScene.getFooter().getBoundsOfLabel(-1);
		} else {
			b = currentScene.getFooter().getBoundsOfLabel(-2);
		}
		
		b.x += currentScene.getFooter().getX();
		b.y += currentScene.getFooter().getY();
		if (sweepEffect == null) {
			sweepEffect = new SweepEffect(menuAdaptor, 10);
		}
		sweepEffect.startExpand(b.x, b.y, b.width, b.height);
	}

	private BaseUI menuAdaptor = new BaseUI() {
		public void reflectVCDSResult(Object cmd) {
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuAdaptor, reflectVCDSResult(), cmd = " + cmd);
			}
			synchronized (instance) {
				hideLoadingAnimation();
				if (CatalogueDatabase.FAIL.equals(cmd)) {
					if (currentScene == null) {
						goToMainScene(true);
					} else if (currentScene instanceof CategoryUI) {
						((CategoryUI) currentScene).refresh();
					}
					CommunicationManager.getInstance().errorMessage("VOD101", null);
				} else if (cmd != null
						&& cmd.toString().startsWith(CatalogueDatabase.FAIL)) {
					CommunicationManager.getInstance().errorMessage(cmd.toString().substring(CatalogueDatabase.FAIL.length()), null);
				} else if (paramKey != null) { // R5 - TANK
					
					Topic topic = CatalogueDatabase.getInstance().getLastTopic();
					
					if (topic.svodService != null && topic.svodService.description == null) {
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, topic.svodService.id, cmd);
						return;
					}
					
					CategoryContainer wrap = new CategoryContainer();
					wrap.isAdult = topic.treeRoot.isAdult;
					wrap.parent = wrap;
					wrap.type = MenuController.MENU_CATEGORY;
					if ("Karaoke".equals(topic.type)) {
						wrap.type = MenuController.MENU_KARAOKE;
					} else if ("Chaîne".equals(topic.type) || "Chaine".equals(topic.type)) {
						wrap.type = MenuController.MENU_CHANNEL;
					} else if ("Groupe de Chaînes".equals(topic.type) || "Groupe de Chaines".equals(topic.type)) {
						wrap.type = MenuController.MENU_CHANNEL;
					} else if ("Environnement des Chaînes".equals(topic.type) || "Environnement des Chaines".equals(topic.type)) {
						wrap.type = MenuController.MENU_CHANNEL_ENV;
					} else if ("VSD".equals(topic.type)) {
						wrap.type = MenuController.MENU_VSD;
					}
					
					TopicPointer tp = new TopicPointer();
					tp.topic = topic;
					wrap.topicPointer = new TopicPointer[] {
							tp
					};
					wrap.category = new Category();
					wrap.category.sample = topic.sample;
					wrap.platformService = wrap.topicPointer[0].platformService;
					wrap.platformValidity = wrap.topicPointer[0].platformValidity;
					
					paramKey = null;
					if (topic.type.equals("Environnement des Chaines")) {
						goToChannelList(wrap);
					} else {
						if (topic.leafCount > 0) {
							forwardHistory(topic.getTitle());
							CategoryUI.setBrandingElementByCategory(wrap);
							// TODO
							// VDTRMASTER-5653
							setLastCategoryId(topic.id);
							// VDTRMASTER-5801
							DataCenter.getInstance().put("VIEW_PARAM", topic.treeRoot.totalContents);
							goToNextScene(lastView, topic.treeRoot.totalContents);
						} else {
							// VDTRMASTER-5638
							CategoryUI.setChannelCategory(wrap);
							CategoryUI.setBrandingElementByCategory(wrap);
							goToNextScene(UITemplateList.CHANNEL_MENU, wrap);
						}
					}
					// VDTRMASTER-5500
					loadingWishlist = false;
				} else if (cmd instanceof String && ((String)cmd).equals(MenuTreeUI.MENU_CHANNEL)) { // R5
					Topic topic = CatalogueDatabase.getInstance().getChannelTopic();
					
					CategoryContainer wrap = new CategoryContainer();
					wrap.isAdult = topic.treeRoot.isAdult;
					wrap.parent = wrap;
					wrap.type = MenuController.MENU_CATEGORY;
					if ("Karaoke".equals(topic.type)) {
						wrap.type = MenuController.MENU_KARAOKE;
					} else if ("Chaîne".equals(topic.type) || "Chaine".equals(topic.type)) {
						wrap.type = MenuController.MENU_CHANNEL;
					} else if ("Groupe de Chaînes".equals(topic.type) || "Groupe de Chaines".equals(topic.type)) {
						wrap.type = MenuController.MENU_CHANNEL;
					} else if ("Environnement des Chaînes".equals(topic.type) || "Environnement des Chaines".equals(topic.type)) {
						wrap.type = MenuController.MENU_CHANNEL_ENV;
					} else if ("VSD".equals(topic.type)) {
						wrap.type = MenuController.MENU_VSD;
					}
					
					TopicPointer tp = new TopicPointer();
					tp.topic = topic;
					
					wrap.topicPointer = new TopicPointer[] {
							tp
					};
					wrap.category = new Category();
					wrap.category.sample = topic.sample;
					
					if (wrap.getTopic() != null) {
						CategoryUI.setChannelCategory(wrap);
						if (wrap.getTopic().channel != null) {
							CategoryUI.setBrandingElement(wrap.getTopic().channel, true);
							CategoryUI.filterElement = wrap.getTopic().channel;
						} else if (wrap.getTopic().network != null) {
							CategoryUI.setBrandingElement(wrap.getTopic().network, true);
							CategoryUI.filterElement = wrap.getTopic().network;
						}
						
						CategoryUI.setBrandingElementByCategory(wrap);
						if (CategoryUI.isBranded()) {
							CategoryUI.setBranded(1);
						}
						
						forwardHistory("");
						goToNextScene(UITemplateList.CHANNEL_MENU, wrap);
					}
				} else if (cmd instanceof String && ((String)cmd).equals(MenuTreeUI.MENU_CHANNEL_ON_DEMAND)) { // R5
					
				} else if (cmd instanceof CategoryContainer
						|| cmd.toString().startsWith("N_")) {
					CategoryContainer category = null;
					if (cmd instanceof CategoryContainer) {
						category = (CategoryContainer) cmd;
					} else {
						category = (CategoryContainer) CatalogueDatabase
								.getInstance().getCached(cmd);
					}
					Log.printDebug("MenuAdaptor, reflectVCDSResult(), retrieved category = "
							+ category.toString(true) 
							+ (category.getTopic() != null ? ", ch = " + category.getTopic().channel + ", nw = " + category.getTopic().network : ", noTopic"));
					
					// R5 - TANK
					if (category.getTopic() != null && category.getTopic().leafCount > 0) {
						forwardHistory(category.getTopic().getTitle());
						goToNextScene(lastView, category.id);
					} else if (category.getTopic() != null) {
						if (category.getTopic().channel != null) {
							CategoryUI.setBrandingElement(category.getTopic().channel, true);
							CategoryUI.filterElement = category.getTopic().channel;
						} else if (category.getTopic().network != null) {
							CategoryUI.setBrandingElement(category.getTopic().network, true);
							CategoryUI.filterElement = category.getTopic().network;
						}
						goToNextScene(UITemplateList.CHANNEL_MENU, category);
					} else if (category.subCategories.length > 0
							&& currentScene instanceof CategoryUI) {
						category.needRetrieve = false;
						goToCategory(category, menuAdaptor);
					} else { // has contents
						if (category.needRetrieve == false || "".equals(from) == false) {

							forwardHistory(category);
							CategoryUI.setBrandingElementByCategory(category);
							if (category.type == MENU_KARAOKE_CONTENTS) {
								goToNextScene(UITemplateList.KARAOKE_CONTENTS,
										category);
							} else {
								goToNextScene(lastView, category.id);
								
								if (BANNER_CATEGORY.equals(from)) {
									mainScreenReady();
								}
							}
						}
					}
				} else if (cmd instanceof Service) {
					// from showcase
					if (((Service) cmd).description != null) {
						if (currentScene != null) {
							MenuController.this.processKeyEvent(OCRcEvent.VK_ENTER);
						} else if (from.equals(EPG)) {
							// R5
							String[] param = CommunicationManager.getInstance().getParameter();
							
							Series series = (Series) CatalogueDatabase.getInstance()
									.getCached(param[1]);
							
							goToDetail(series, null);
							
						}
					} else {
						CommunicationManager.getInstance().errorMessage("VOD104", null);
					}
				} else if (cmd instanceof Series
						|| cmd.toString().startsWith("SR")
						|| cmd.toString().startsWith(ASSET + "SR")) {
					Series series = null;
					if (cmd instanceof Series) {
						series = (Series) cmd;
					} else {
						String id = cmd.toString();
						if (id.startsWith(ASSET)) {
							id = id.substring(ASSET.length());
						}
						series = (Series) CatalogueDatabase.getInstance()
								.getCached(id);
					}
					series.needRetrieve = false;
					goToDetail(series, null);
				} else if (cmd instanceof Season
						|| cmd.toString().startsWith("SS")
						|| cmd.toString().startsWith(ASSET + "SS")) {
					// goToNextScene(UITemplateList.LIST_OF_TITLES, cmd);
					Season season = null;
					if (cmd instanceof Season) {
						season = (Season) cmd;
					} else {
						String id = cmd.toString();
						if (id.startsWith(ASSET)) {
							id = id.substring(ASSET.length());
						}
						season = (Season) CatalogueDatabase.getInstance()
								.getCached(id);
					}
					
					// VDTRMASTER-5676
					if (season.service != null && season.service.serviceType == Service.SVOD) {
						Service service = (Service) CatalogueDatabase.getInstance().getCached(season.service.id);
						if (service.description == null) {
							Log.printDebug("MenuController, gotoDetail(), there is no Service description");
							showLoadingAnimation();
							CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, season.service.id, cmd);
							return;
						}
						season.service = service;
					}
					
					if (season.sections.length > 0) {
						forwardHistory(season);
						goToNextScene(UITemplateList.LIST_OF_TITLES, season.sections);
					} else {
						checkOptionAndGo(season, null);
					}
				} else if (cmd instanceof Showcase
						|| cmd.toString().startsWith("SC")) {
					Showcase sc = null;
					if (cmd instanceof Showcase) {
						sc = (Showcase) cmd;
					} else {
						sc = (Showcase) CatalogueDatabase.getInstance()
								.getCached(cmd);
					}
					MoreDetail content = sc.getContent();
					if (content != null) {
						goToDetail(content, null);
					} else {
						// error
					}
				} else if (cmd instanceof Bundle
						|| cmd.toString().startsWith("BU")) {
					goToNextScene(UITemplateList.BUNDLE_DETAIL, cmd);
				} else if (cmd instanceof Extra 
						|| cmd.toString().startsWith("CE")
						|| cmd.toString().startsWith(ASSET + "CE")) {
					String id = cmd.toString();
					if (id.startsWith(ASSET)) {
						id = id.substring(ASSET.length());
					}
					forwardHistory((BaseElement) CatalogueDatabase.getInstance().getCached(id));
					goToNextScene(UITemplateList.LIST_OF_TITLES, id);
				} else if (cmd instanceof Episode
						|| cmd.toString().startsWith("EP")
						|| cmd.toString().startsWith(ASSET + "EP")) {
					Episode episode = null;
					if (cmd instanceof Episode) {
						episode = (Episode) cmd;
					} else {
						String id = cmd.toString();
						if (id.startsWith(ASSET)) {
							id = id.substring(ASSET.length());
						}
						episode = (Episode) CatalogueDatabase.getInstance()
								.getCached(id);
					}
					
					// VDTRMASTER-5516
					if (episode == null && (SEARCH.equals(from) || isFromSearch)) {
						if (Log.DEBUG_ON) {
							Log.printDebug("MenuController, menuAdaptor, reflectVCDSResult(Object), episode=" + episode 
									+ ", from=" + from);
							Log.printDebug("MenuController, menuAdaptor, reflectVCDSResult(Object), may error case, "
									+ "so reset needSearch");
						}
						MenuController.getInstance().needSearch = false;
						return;
					}
					
					if (episode.service.length > 0 && episode.service[0].serviceType == Service.SVOD && episode.service[0].description == null) {
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, episode.service[0].id, cmd);
						return;
					} // 4452
					if (episode.season == null || episode.season.episode.length == 0) {
						String seasonId = episode.season != null ? episode.season.id : episode.seasonRef;
						synchronized (CatalogueDatabase.getInstance()) {
							showLoadingAnimation();
							CatalogueDatabase.getInstance().retrieveSeason(
									null, seasonId, cmd);
							try {
								CatalogueDatabase.getInstance().wait(
										Constants.MS_PER_MINUTE);
							} catch (InterruptedException e) {
							}
							hideLoadingAnimation();
						}
						episode.season = (Season) CatalogueDatabase
								.getInstance().getCached(seasonId);
					}

					if (episode.inBundleOnly) {
						goToDetail(episode, null);
					} else {
						forwardHistory(episode.season);
						// VDTRMASTER-5576
//						if (episode.channel != null) {
//							if (episode.channel.network != null && episode.channel.network.imageGroup.length > 0) {
//								CategoryUI.setBrandingElement(episode.channel.network, true);
//							} else if (episode.channel.imageGroup.length > 0) {
//								CategoryUI.setBrandingElement(episode.channel, true);
//							}
//						}
						goToNextScene(UITemplateList.LIST_OF_TITLES, new Object[] {
							new Integer(UITemplateList.LIST_OF_TITLES),
							episode.season.episode, episode }, false);
						if (SEARCH.equals(from) || BANNER_CONTENT.equals(from) || (from != null && from.startsWith(AGENT))) {
							mainScreenReady();
						}
					}
				} else if (cmd instanceof Video
						|| cmd.toString().startsWith("V")) {
					Video video = null;
					if (cmd instanceof Video) {
						video = (Video) cmd;
					} else {
						video = (Video) CatalogueDatabase.getInstance().getCached(cmd);
					}
					if (video.service.length > 0 && video.service[0].serviceType == Service.SVOD && video.service[0].description == null) {
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, video.service[0].id, cmd);
						return;
					}
					goToNextScene(UITemplateList.MOVIE_DETAIL, cmd);
					if (SEARCH.equals(from) || BANNER_CONTENT.equals(from) || (from != null && from.startsWith(AGENT))) {
						mainScreenReady();
					}
				} else if (cmd.toString().startsWith(ASSET)) { // from Search
					String videoId = cmd.toString().substring(ASSET.length());
					Video video = (Video) CatalogueDatabase.getInstance().getCached(videoId);
					
					// VDTRMASTER-5516
					if (video == null && (SEARCH.equals(from) || isFromSearch)) {
						if (Log.DEBUG_ON) {
							Log.printDebug("MenuController, menuAdaptor, reflectVCDSResult(Object), video=" + video 
									+ ", from=" + from);
							Log.printDebug("MenuController, menuAdaptor, reflectVCDSResult(Object), may error case, "
									+ "so reset needSearch");
						}
						MenuController.getInstance().needSearch = false;
						return;
					}
					if (video.service.length > 0 && video.service[0].serviceType == Service.SVOD && video.service[0].description == null) {
						showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveServices(menuAdaptor, video.service[0].id, cmd);
						return;
					}
					if (SEARCH.equals(from) || BANNER_CONTENT.equals(from) || (from != null && from.startsWith(AGENT))) {
						mainScreenReady();
					}
					goToNextScene(UITemplateList.MOVIE_DETAIL, videoId);
				} else if (WISHLIST.equals(cmd)) {
					String[] names = isAdultWithSimilar() ?
							BookmarkManager.getInstance().retrieveWishlistTitlesAdult() :
							BookmarkManager.getInstance().retrieveWishlistTitles(true);
					ArrayList list = new ArrayList();
					boolean needReload = false;
					for (int i = 0; names != null && i < names.length; i++) {
						Object cached = CatalogueDatabase.getInstance()
								.getCachedBySspId(names[i]);
						if (cached != null) {
							list.add(cached);
						}
					}

					if (names.length != list.size()) {
                        ArrayList list2 = new ArrayList();
                        for (int i = 0; i < names.length; i++) {
                            Object cached = CatalogueDatabase.getInstance()
                                    .getCachedBySspId(names[i]);
                            if (cached != null) {
                                list2.add(cached);
                            }
                        }
                        showLoadingAnimation();
                        CatalogueDatabase.getInstance().retrieveItems(menuAdaptor,
                                list2, WISHLIST);
                        return;
                    }

					BaseElement[] elements = new BaseElement[list.size()];
					list.toArray(elements);
					goToNextScene(UITemplateList.WISH_LIST, elements);
					loadingWishlist = false;
					// } else if ("ResumeViewing".equals(cmd)) {
					// BookmarkManager.getInstance().removeBundleAndNoExists();
					// Bookmark[] bookmarks = BookmarkManager.getInstance()
					// .retrieveActivePurchases(true);
					// ArrayList list = new ArrayList();
					// for (int i = 0; i < bookmarks.length; i++) {
					// Object cached = CatalogueDatabase.getInstance()
					// .getCachedBySspId(bookmarks[i].getSspID());
					// if (cached != null) {
					// list.add(cached);
					// }
					// }
					// BaseElement[] elements = new BaseElement[list.size()];
					// list.toArray(elements);
					// goToNextScene(UITemplateList.RESUME_VIEWING, elements);
				} else if (MenuTreeUI.MENU_KARAOKE.equals(cmd)) {
					CategoryContainer category = ((CategoryUI) currentScene)
							.getCurrentCategory();
					for (int i = 0; i < category.subCategories.length; i++) {
						if (category.subCategories[i].type == MENU_KARAOKE) {
							String topicId = category.subCategories[i]
									.getTopic().id;
							category.subCategories[i].getTopic().dispose();
							category.subCategories[i].topicPointer[0].topic = (Topic) CatalogueDatabase
									.getInstance().getCached(topicId);
							break;
						}
					}
					((CategoryUI) currentScene)
							.reflectVCDSResult(((CategoryUI) currentScene)
									.getCurrentCategory().subCategories);
				} else if (cmd.toString().startsWith("TO_")) {
					Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(cmd);
					forwardHistory(topic.getTitle());
					goToNextScene(UITemplateList.LIST_OF_TITLES, topic.treeRoot.totalContents);
				}
			}
		}

		public void animationEnded(Effect effect) {
			if (effect == sweepEffect
					&& sweepEffect.getMode() == SweepEffect.MODE_EXPAND) {
				currentScene.updateButtons();
				sweepEffect.startCollapse();
			}
		}

		public void receiveCheckRightFilter(CheckResult checkResult) {
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuAdaptor, receiveCheckRightFilter()"
						+ ", checkResult = " + checkResult + ", filter = "
						+ checkResult.getFilter() + ", response = "
						+ checkResult.getResponse());
			}
			if (PreferenceService.RESPONSE_SUCCESS != checkResult.getResponse()) {
				
				//Touche, VDTRMASTER-6250, VDTRMASTER-6254
				if (checkResult.getFilter() == RightFilter.BLOCK_BY_RATINGS ||
					checkResult.getFilter() == RightFilter.HIDE_ADULT_CONTENT) {
					
					Log.printInfo("receiveCheckRightFilter cancel currentScene " + currentScene);
					
					if (currentScene instanceof CategoryUI &&
							((CategoryUI) currentScene).getShowcaseView() != null) {
						VbmController.getInstance().backwardHistory();
					} else {
						VbmController.getInstance().removeDummy();
					}
				}
				
				return;
			// Touche, VDTRMASTER-6250, VDTRMASTER-6254				
			} else if (PreferenceService.RESPONSE_SUCCESS == checkResult.getResponse()) {
				if (checkResult.getFilter() == RightFilter.BLOCK_BY_RATINGS ||
						checkResult.getFilter() == RightFilter.HIDE_ADULT_CONTENT) {						
					Log.printInfo("receiveCheckRightFilter success currentScene " + currentScene);						
					if (currentScene instanceof CategoryUI &&
							((CategoryUI) currentScene).getShowcaseView() != null) {
						;
					} else {
						VbmController.getInstance().removeDummy();
					}
				}
			}
			
			if (RightFilter.ALLOW_ADULT_CATEGORY
					.equals(checkResult.getFilter())) {
				openAdultCategory = true;
				if (currentScene != null) {
					currentScene.receiveCheckRightFilter(checkResult);
				}
			} else if (RightFilter.HIDE_ADULT_CONTENT.equals(checkResult
					.getFilter())) {
				showAdultContent = true;
				ageUnblocked = true;
				if (titleToUnblock != null && titleToUnblock instanceof Video) {
					// unblocked.add(titleToUnblock.getId());

					Video v = (Video) titleToUnblock;
					goToDetail(v, null);
//					if (v.detail.length == 0 && v.offer.length == 0) {
//						showLoadingAnimation();
//						ArrayList list = new ArrayList();
//						list.add(v.id);
//						if (v.id.startsWith("V")) {
//							CatalogueDatabase.getInstance().retrieveVideos(
//									menuAdaptor, list, v.id);
//						} else {
//							CatalogueDatabase.getInstance().retrieveEpisodes(
//									menuAdaptor, list, v.id);
//						}
//
//					} else { // alreay retrieved
//						goToNextScene(UITemplateList.MOVIE_DETAIL,
//								titleToUnblock);
//					}
				}
				titleToUnblock = null;
				if (currentScene != null) {
					currentScene.receiveCheckRightFilter(checkResult);
				}
			} else if (RightFilter.BLOCK_BY_RATINGS.equals(checkResult
					.getFilter())) {
				
				
				if (currentScene instanceof CategoryUI) {
					Log.printDebug("currentCategory=" +((CategoryUI)currentScene).getCurrentCategory());
				}
				
				// R5
				// VDTRMASTER-5692
				if (CategoryUI.getChannelCategory() != null 
						&& ChannelInfoManager.getInstance().isBlockedChannel(CategoryUI.getChannelCategory().getTopic())) {
					ChannelInfoManager.getInstance().addTemporaliryUnblockChannel(CategoryUI.getChannelCategory().getTopic());
				} else if (currentScene instanceof CategoryUI
								&& ((CategoryUI)currentScene).getCurrentCategory().getTopic() != null
								&& ChannelInfoManager.getInstance().isBlockedChannel(((CategoryUI)currentScene).getCurrentCategory().getTopic())) {
					ChannelInfoManager.getInstance().addTemporaliryUnblockChannel(((CategoryUI)currentScene).getCurrentCategory().getTopic());
				} else {
					ageUnblocked = true;
					if (titleToUnblock != null) {
						// unblocked.add(titleToUnblock.getId());
	
						if (Definitions.RATING_18
								.equals(titleToUnblock.getRating())) {
							showAdultContent = true;
						} else if (titleToUnblock instanceof AvailableSource && Definitions.RATING_18
								.equals(((AvailableSource)titleToUnblock).parent.getRating())) { // VDTRMASTER-5818
							showAdultContent = true;
						}
	
						titleToUnblock = null;
					}
				}
				
				if (currentScene != null) {
					currentScene.receiveCheckRightFilter(checkResult);
				}
			} else if (RightFilter.PARENTAL_CONTROL.equals(checkResult
					.getFilter())) {
				if (Resources.OPT_ACTIVATE_PARENTAL.equals(lastSelect.getKey())) {
					try {
						pauseTemporary = true;
						CommunicationManager
								.getInstance()
								.getMonitorService()
								.startUnboundApplication(
										"SETTINGS",
										new String[] {
												Resources.APP_NAME,
												PreferenceService.PARENTAL_CONTROLS });
					} catch (RemoteException e) {
						Log.printWarning(e);
					}
				} else if (Resources.OPT_REMOVE_PARENTAL.equals(lastSelect
						.getKey())) {
					PreferenceProxy.getInstance().setPreference(
							RightFilter.PARENTAL_CONTROL,
							DataCenter.getInstance().getString(
									RightFilter.SUSPEND_DEFAULT_TIME));
					// String title = DataCenter.getInstance().getString(
					// lastSelect.getKey());
					// String h = DataCenter
					// .getInstance()
					// .getString(RightFilter.SUSPEND_DEFAULT_TIME)
					// .equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS) ?
					// " 4 "
					// : " 8 ";
					// String message = DataCenter.getInstance().getString(
					// Resources.TEXT_MSG_PARENTAL_SUSPENDED)
					// + h
					// + DataCenter.getInstance().getString(
					// Resources.TEXT_LBL_HOURS);
					// ParentalResultPopup.getInstance()
					// .showNotice(title, message);
				} else if (Resources.OPT_APPLY_PARENTAL.equals(lastSelect
						.getKey())) {
					openAdultCategory = false;
					showAdultContent = false;
					ageUnblocked = false;
					// unblocked.clear();
					PreferenceProxy.getInstance().setPreference(
							RightFilter.PARENTAL_CONTROL,
							Definitions.OPTION_VALUE_ON);
					// String title = DataCenter.getInstance().getString(
					// lastSelect.getKey());
					// String message = DataCenter.getInstance().getString(
					// Resources.TEXT_MSG_PARENTAL_ENABLED);
					// ParentalResultPopup.getInstance()
					// .showNotice(title, message);
				}
				synchronized (PreferenceProxy.getInstance()) {
					try {
						PreferenceProxy.getInstance().wait(
								Constants.MS_PER_SECOND);
					} catch (InterruptedException e) {
					}
				}
				if (currentScene != null) {
					currentScene.receiveCheckRightFilter(checkResult);
					// currentScene.repaint();
				}
			} else if (RightFilter.ALLOW_PURCHASE.equals(checkResult
					.getFilter())) {
				if (objectToPlay != null) {
					String id = (String) ((Object[]) objectToPlay)[1];
					Object cached = CatalogueDatabase.getInstance().getCached(
							id);
					if (cached instanceof Video) {
						menu.goToNextScene(UITemplateList.PLAY_SCREEN,
								objectToPlay);
					} else {
						showOrderConfirm(objectToPlay);
					}
				} else { // karaoke purchase
					CategoryContainer category = CategoryUI.getPartyPack();
					if (category == null) {
						Log.printWarning("purchaeKaraoke(), NO party pack data");
						return;
					}
					Offer offer = category.categorizedBundle.offer[0];
					VideoDetails detail = category.categorizedBundle.detail[0];
					if (Log.INFO_ON) {
						Log.printInfo("purchaseKaraoke(), offer = " + offer
								+ ", detail = " + detail);
					}
					boolean ret = BookmarkManager.getInstance().purchaseBundle(
							detail.orderableItem, offer.amount,
							offer.leaseDuration, new String[0]);
					if (ret) {
						CategoryUI.setPartyPack(category);
						if (CategoryUI.getInstance().isVisible()) {
							CategoryUI.getInstance().refresh();
						}
						showOrderConfirm(null);
					}
				}
			} else if (RightFilter.PROTECT_SETTINGS_MODIFICATIONS
					.equals(checkResult.getFilter())) {
				try {
					pauseTemporary = true;
					CommunicationManager
							.getInstance()
							.getMonitorService()
							.startUnboundApplication(
									"SETTINGS",
									new String[] { Resources.APP_NAME,
											PreferenceService.VOD });
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
		}

		public void popUpClosed(UIComponent popup, Object msg) {
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuAdaptor, popUpClosed()" + ", popup = "
						+ popup + ", msg = " + msg);
			}
			if (currentScene != null) {
				currentScene.setPopUp(null);
				currentScene.remove(popup);
			}
			if (popup == PopAddWishlist.getInstance()
					&& Boolean.TRUE.equals(msg)) {
				if (currentScene instanceof ListViewUI
						&& ((ListViewUI) currentScene).isWishList()) {
					currentScene.updateButtons();
				} else {
					refreshEffect();
				}
			} else if (popup == PopPlayAll.getInstance()) {
				if (msg instanceof Object[]) {
					Object[] obj = (Object[]) msg;
					BaseElement element = (BaseElement) obj[0];
					final int opt = ((Integer) obj[1]).intValue();
					if (opt == PopPlayAll.CANCEL) {
						return;
					}
					
					Bookmark purchased = null;
					BaseElement candidate = null;
					if (element instanceof Video) {
						purchased = BookmarkManager.getInstance().getPurchased((Video) (candidate = element));
					} else if (element instanceof Bundle) {
						purchased = BookmarkManager.getInstance().getPurchased((Bundle) (candidate = element));
					} else if (element instanceof Season) {
						Season season = (Season) element;
						if (season.episode.length == 0) {
							showLoadingAnimation();
							synchronized (CatalogueDatabase.getInstance()) {
								CatalogueDatabase.getInstance().retrieveSeason(
										null, season.id, null);
								try {
									CatalogueDatabase.getInstance().wait(
											Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
							}
							StringBuffer buf = new StringBuffer();
							for (int i = 0; i < season.episodeBundles.length; i++) {
								buf.append(season.episodeBundles[i]);
								buf.append(',');
							}
							if (buf.length() > 0) {
								synchronized (CatalogueDatabase.getInstance()) {
									CatalogueDatabase.getInstance().retrieveBundle(null,
											buf.toString(), false, null);
									try {
										CatalogueDatabase.getInstance().wait(
												Constants.MS_PER_MINUTE);
									} catch (InterruptedException e) {}
								}
								Bundle bundle = (Bundle) CatalogueDatabase.getInstance().getCached(season.episodeBundles[0]);
								purchased = BookmarkManager.getInstance().getPurchased(((Bundle) (candidate = bundle)));
							} else {
								purchased = BookmarkManager.getInstance().getPurchased((Video) (candidate = season.episode[0]));
							}
							hideLoadingAnimation();
						}
					}
					
					if (purchased == null) { // not purchased/watched yet
						Log.printDebug("PlayAll, candidate = " + candidate);
						currentScene.setPopUp(PopSelectOption.getInstance());
						final BaseElement element2 = element;
						PopSelectOption.getInstance().show(new BaseUI() {
							public void popUpClosed(UIComponent popup, Object msg) {
								if (currentScene != null) {
									currentScene.setPopUp(null);
									currentScene.remove(popup);
								}
								int select = ((Integer) ((Object[]) msg)[0]).intValue();
								if (select == PopSelectOption.BTN_CANCEL) {
									return;
								}
								playAll(element2, select, opt);
							}
						}, candidate);
						return;
					}
					
					// selected purchased one, follow it's option
					Log.printDebug("PlayAll, purchased = " + purchased);
					int selected = Resources.HD_FR;
					if (candidate instanceof Video) {
						selected = ((Video) candidate).getPlayout(purchased.getOrderableName()).type;
					} else if (candidate instanceof Bundle) {
						Video v = (Video) ((Bundle) candidate).getTitles()[0];
						Log.printDebug("PlayAll, first item in selected bundle = " + v.toString(true));
						Bookmark videoPurchased = BookmarkManager.getInstance().getPurchased(v);
						if (videoPurchased != null) {
							Playout p = v.getPlayout(videoPurchased.getOrderableName());
							Log.printDebug("PlayAll, first playout = " + p);
							if (p != null) {
								selected = p.type;
							}
						}
					}
					playAll(element, selected, opt);
				}
			} else if (popup == PopSelectOption.getInstance()) {
				if (msg instanceof CategorizedBundle) {
					objectToPlay = null; // Karaoke
					// PreferenceProxy.getInstance().checkRightFilter(
					// RightFilter.ALLOW_PURCHASE, null, menuAdaptor);
					PreferenceProxy
							.getInstance()
							.checkRightFilter(
									RightFilter.ALLOW_PURCHASE,
									null,
									menuAdaptor,
									new String[] {
											DataCenter
													.getInstance()
													.getString(
															Resources.TEXT_MSG_PURCHASE_PIN1),
											DataCenter
													.getInstance()
													.getString(
															Resources.TEXT_MSG_PURCHASE_PIN2) });
					return;
				}
				int select = ((Integer) ((Object[]) msg)[0]).intValue();
				if (select == PopSelectOption.BTN_CANCEL) {
					return;
				}
				String titleId = (String) ((Object[]) msg)[1];
				Object cached = CatalogueDatabase.getInstance().getCached(
						titleId);
				String rating = null;
				//R7.4 freelife VOD ordering improvement
				/*
				if (select == PopSelectOption.BTN_BUNDLE) {
					if (cached instanceof Video) {
						String bId = ((Video) cached).inBundlesAR[0];
						Bundle bundle = (Bundle) CatalogueDatabase
								.getInstance().getCached(bId);
						if (bundle != null) {
							menuAdaptor.reflectVCDSResult(bundle);
						} else {
							showLoadingAnimation();
							CatalogueDatabase.getInstance().retrieveBundle(
									menuAdaptor, bId, true, null);
						}
					}
					return;
				}
				*/
				if (cached instanceof Video 
						&& ((Video) cached).isFree(select)) { // support 147
					menu.goToNextScene(UITemplateList.PLAY_SCREEN, msg);
					return;
				} else if (cached instanceof Bundle
						&& ((Bundle) cached).isFree(select)) { // support 147
					menu.goToNextScene(UITemplateList.PLAY_SCREEN, msg);
					return;
				} 
				
				rating = ((MoreDetail) cached).getRating();
				objectToPlay = msg;
				// PreferenceProxy.getInstance().checkRightFilter(
				// RightFilter.ALLOW_PURCHASE, rating, menuAdaptor);
				PreferenceProxy.getInstance().checkRightFilter(
						RightFilter.ALLOW_PURCHASE,
						rating,
						menuAdaptor,
						new String[] {
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_PURCHASE_PIN1),
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_PURCHASE_PIN2) });

			} else if (popup == PopOrderResult.getInstance()) {
				if (msg instanceof Integer) {
					int focus = ((Integer) msg).intValue();
					switch (focus) {
					case PopOrderResult.CLOSE: // karaoke
						if (currentScene instanceof CategoryUI == false) {
							backwardHistory(false);
							back();
							// 20190222 Touche project
							VbmController.getInstance().backwardHistory();		
						}
						CategoryUI.getInstance().moveToKaraokePackage(true);
						break;
					case PopOrderResult.WATCH_NOW:
						menu.goToNextScene(UITemplateList.PLAY_SCREEN,
								objectToPlay);
						break;
					case PopOrderResult.PLAY_ALL:
						objectToPlay = new Object[] {
								((Object[]) objectToPlay)[0],
								((Object[]) objectToPlay)[1],
								((Object[]) objectToPlay)[2],
								new Integer(-1), // means play all
								};
						menu.goToNextScene(UITemplateList.PLAY_SCREEN,
								objectToPlay);
						break;
					case PopOrderResult.SELECT_ANOTHER:
						showSelectItem(objectToPlay);
						break;
					}
				}
			} else if (popup == PopSelectItem.getInstance()) {
				if (msg instanceof Integer) {
					int focus = ((Integer) msg).intValue();
					boolean isPurchasing = dataCenter.getString(Resources.TEXT_WATCH).equals(lastAction) == false 
							&& dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE).equals(lastAction) == false
							&& dataCenter.getString(Resources.TEXT_SELECT_A_MOVIE).equals(lastAction) == false; // VDTRMASTER-5540
					if (focus < 0) {
						if (isPurchasing) {
							showOrderConfirm(objectToPlay);
						}
					} else {
						if (isPurchasing) {
							objectToPlay = new Object[] {
									((Object[]) objectToPlay)[0],
									((Object[]) objectToPlay)[1],
									((Object[]) objectToPlay)[2], msg };
							menu.goToNextScene(UITemplateList.PLAY_SCREEN,
									objectToPlay);
						} else {
							((Bookmark) objectToPlay).idxOfBundle = ((Integer) msg).intValue();
							menu.goToNextScene(UITemplateList.PLAY_SCREEN,
									objectToPlay);
						}
					}
				}
			}
		}
	};

	public void canceled() {
		if (currentScene != null) {
			currentScene.setPopUp(null);
		}
	}

	public void selected(final MenuItem item) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, selected(), item = "
					+ item.getKey());
		}
		if (currentScene != null) {
			currentScene.setPopUp(null);
		}
		lastSelect = item;
		if (Resources.OPT_ACTIVATE_PARENTAL.equals(item.getKey())) {
			// PreferenceProxy.getInstance().checkRightFilter(
			// RightFilter.PARENTAL_CONTROL, null, menuAdaptor);
			PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.PARENTAL_CONTROL,
					null,
					menuAdaptor,
					new String[] {
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_RESTORE_PIN1),
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_RESTORE_PIN2) });
		} else if (Resources.OPT_APPLY_PARENTAL.equals(item.getKey())) {
			// PreferenceProxy.getInstance().checkRightFilter(
			// RightFilter.PARENTAL_CONTROL, null, menuAdaptor);
			PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.PARENTAL_CONTROL,
					null,
					menuAdaptor,
					new String[] {
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_RESTORE_PIN1),
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_RESTORE_PIN2) });
		} else if (Resources.OPT_REMOVE_PARENTAL.equals(item.getKey())) {
			// PreferenceProxy.getInstance().checkRightFilter(
			// RightFilter.PARENTAL_CONTROL, null, menuAdaptor);
			PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.PARENTAL_CONTROL,
					null,
					menuAdaptor,
					new String[] {
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_SUSPEND_PIN1),
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_SUSPEND_PIN2) });
		} else if (Resources.OPT_GO_HELP.equals(item.getKey())) {
			goToHelp();
		} else if (Resources.OPT_GO_SETTING.equals(item.getKey())) {
			goToSetting();
		} else if (Resources.TEXT_REMOVE_FROM_WISHLIST.equals(item.getKey()) 
				|| Resources.TEXT_REMOVE_FROM_WISHLIST_18.equals(item.getKey())) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					processActionMenu(
							DataCenter.getInstance().getString(item.getKey()),
							(BaseElement) item.getData());
				}
			});
		} else if (Resources.TEXT_ADDTO_WISHLIST.equals(item.getKey()) 
				|| Resources.TEXT_ADDTO_WISHLIST_18.equals(item.getKey())) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					processActionMenu(
							DataCenter.getInstance().getString(item.getKey()),
							(BaseElement) item.getData());
				}
			});
		} else if (Resources.OPT_NAME.equals(item.getKey()) 
				|| Resources.OPT_DATE.equals(item.getKey())
				|| Resources.OPT_PROD_YEAR.equals(item.getKey())
				|| Resources.OPT_DEFAULT.equals(item.getKey())) {
			
//			if (CatalogueDatabase.getInstance().getLastSortType().equals(
//					CatalogueDatabase.getInstance().getSortType(item.getKey()))) {
//				return; // same sort option
//			}
			if (currentScene instanceof ListViewUI == false || ((ListViewUI) currentScene).isNormal()) {
				showLoadingAnimation();
				int pageSize = DataCenter.getInstance().getInt("PAGE_SIZE");
				if (pageSize > 0) {
					pageSize = Math.max(21, pageSize);
				}
				int start = 0;
				if (currentScene instanceof ListOfTitlesUI
						|| currentScene instanceof CarouselViewUI) {
					start = -(pageSize / 2);
				}
				String sortType = CatalogueDatabase.getInstance().getSortType(item.getKey());
				
				// VDTRMASTER-5653 & VDTRMASTER-5649
				if (lastCategoryId.startsWith("TO_")) {
					Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(lastCategoryId);
					CatalogueDatabase.getInstance().retrieveNamedTopic(currentScene, topic.type, topic.namedTopic, 
							sortType, CatalogueDatabase.SORT);
				} else {
					CatalogueDatabase.getInstance().retrieveCategoryContents(currentScene, lastCategoryId, 
							CatalogueDatabase.SORT,	start, pageSize, sortType);
				}
			}
		} else if (Resources.TEXT_SEARCH.equals(item.getKey())) {
            if (needSearch) {
                back();
            } else {
                if (currentScene != null && currentScene.getPopUp() != null) {
                    currentScene.getPopUp().stop();
                    currentScene.setPopUp(null);
                }
                VbmController.getInstance().writeHotKey(SEARCH);
                CommunicationManager.getInstance().launchSearchApplication(
                        true, false, "D_VOD");
            }
        } else if (currentScene instanceof CategoryUI) {
			CategoryContainer ca = ((CategoryUI) currentScene).getCurrentCategory();
			
			// VDTRMASTER-5713
			if (ca.type == MenuController.MENU_CHANNEL_ENV && ((CategoryUI) currentScene).getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT) {
				ShowcaseView scView = ((CategoryUI) currentScene).getShowcaseView();
				
				if (scView.getCurrentCatalogue() instanceof Showcase) {
					Showcase sc = (Showcase) scView.getCurrentCatalogue();
					Log.printDebug("MenuController, selected(), showcase=" + sc);
					CategoryContainer wrap = new CategoryContainer();
					wrap.isAdult = sc.detail.getTopic().treeRoot.isAdult;
					wrap.type = MenuController.MENU_CHANNEL;
					wrap.topicPointer = new TopicPointer[] { new TopicPointer() };
					wrap.topicPointer[0].topic = sc.detail.getTopic();
					
					proccessChannelOption(wrap, item);
				}

			} else if (ca.type == MenuController.MENU_CHANNEL) {
				proccessChannelOption(ca, item);
			} else if (CategoryUI.getChannelCategory() != null) {
				ca = CategoryUI.getChannelCategory();
				proccessChannelOption(ca, item);
			}
		} else if (CategoryUI.getChannelCategory() != null) {
			CategoryContainer ca = CategoryUI.getChannelCategory();
			proccessChannelOption(ca, item);
		}
		
		if (currentScene != null) {
			currentScene.selected(item);
		}
	}
	
	private void proccessChannelOption(final CategoryContainer ca, MenuItem item) {
		if (ca.type == MenuController.MENU_CHANNEL) {
			if (Resources.OPT_ADD_FAVORITE_CHANNEL.equals(item.getKey())) {
				ChannelInfoManager.getInstance().addFavoritehannel(ca.getTopic(), true);
			} else if (Resources.OPT_REMOVE_FAVORITE_CHANNEL.equals(item.getKey())) {
				ChannelInfoManager.getInstance().removeFavoritehannel(ca.getTopic());
			} else if (Resources.OPT_ADD_BLOCKED_CHANNEL.equals(item.getKey())) {
				String src = DataCenter.getInstance().getString(Resources.TEXT_MSG_BLOCK_CHANNEL);
				src = TextUtil.replace(src, "[channel name]", ca.getTopic().getTitle());
				String[] message = TextUtil.tokenize(src, PreferenceService.PREFERENCE_DELIMETER);
				PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.CHANNEL_RESTRICTION,
					null,
					new BaseUI() {
						public void receiveCheckRightFilter(CheckResult result) {
							if (result.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
								ChannelInfoManager.getInstance().addBlockedChannel(ca.getTopic());
							}
						}
					},
					message
				);
				
			} else if (Resources.OPT_REMOVE_BLOCKED_CHANNEL.equals(item.getKey())) {
				String src = DataCenter.getInstance().getString(Resources.TEXT_MSG_UNBLOCK_CHANNEL);
				src = TextUtil.replace(src, "[channel name]", ca.getTopic().getTitle());
				String[] message = TextUtil.tokenize(src, PreferenceService.PREFERENCE_DELIMETER);
				PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.CHANNEL_RESTRICTION,
					null,
					new BaseUI() {
						public void receiveCheckRightFilter(CheckResult result) {
							if (result.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
								ChannelInfoManager.getInstance().removeBlockedChannel(ca.getTopic());
							}
						}
					},
					message
				);
			}
		} else if (ca.type == MenuController.MENU_CHANNEL_ENV) {
			// in the case of showcase of channel on demand in Main
			final BaseElement be = ((CategoryUI) currentScene).getShowcaseView().getCurrentCatalogue();
			if (be instanceof Showcase && ((Showcase)be).detail.getTopic() != null) {
				if (Resources.OPT_ADD_FAVORITE_CHANNEL.equals(item.getKey())) {
					ChannelInfoManager.getInstance().addFavoritehannel(((Showcase)be).detail.getTopic(), true);
				} else if (Resources.OPT_REMOVE_FAVORITE_CHANNEL.equals(item.getKey())) {
					ChannelInfoManager.getInstance().removeFavoritehannel(((Showcase)be).detail.getTopic());
					
				} else if (Resources.OPT_ADD_BLOCKED_CHANNEL.equals(item.getKey())) {
					String src = DataCenter.getInstance().getString(Resources.TEXT_MSG_BLOCK_CHANNEL);
					src = TextUtil.replace(src, "[channel name]", ca.getTopic().getTitle());
					String[] message = TextUtil.tokenize(src, PreferenceService.PREFERENCE_DELIMETER);
					PreferenceProxy.getInstance().checkRightFilter(
						RightFilter.CHANNEL_RESTRICTION,
						null,
						new BaseUI() {
							public void receiveCheckRightFilter(CheckResult result) {
								if (result.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
									ChannelInfoManager.getInstance().addBlockedChannel(((Showcase)be).detail.getTopic());
								}
							}
						},
						message
					);
					
				} else if (Resources.OPT_REMOVE_BLOCKED_CHANNEL.equals(item.getKey())) {
					String src = DataCenter.getInstance().getString(Resources.TEXT_MSG_UNBLOCK_CHANNEL);
					src = TextUtil.replace(src, "[channel name]", ca.getTopic().getTitle());
					String[] message = TextUtil.tokenize(src, PreferenceService.PREFERENCE_DELIMETER);
					PreferenceProxy.getInstance().checkRightFilter(
						RightFilter.CHANNEL_RESTRICTION,
						null,
						new BaseUI() {
							public void receiveCheckRightFilter(CheckResult result) {
								if (result.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
									ChannelInfoManager.getInstance().removeBlockedChannel(((Showcase)be).detail.getTopic());
								}
							}
						},
						message
					);
				}
			}
		}
	}
	
	public boolean isAdult() {
		
		boolean isAdult = false;
		// VDTRMASTER-5681
		if (openAdultCategory && sizeHistoryWhenOpenAdultCategory == 0 
				&& ((currentScene instanceof TitleDetailUI == false && currentScene instanceof ListViewUI == false) 
						|| (currentScene instanceof ListViewUI 
						&& ((ListViewUI)currentScene).isWishList()) && getLastScene() instanceof TitleDetailUI == false)) {
			isAdult = true;
		} else if (isFromSearch && currentScene instanceof TitleDetailUI) {
			
			Video title = ((TitleDetailUI) MenuController.getInstance().getCurrentScene()).getTitle();
			
			
			if (CommunicationManager.getInstance().hasAnyPackage() && 
					CommunicationManager.getInstance().checkPackage(title.service)) {
				if (BookmarkManager.getInstance().isAdult(title)) {
					isAdult = true;
				}
			}
			
		} else {
			BaseUI curUI = (BaseUI) MenuController.getInstance().getCurrentScene();
			
			BaseElement curElement = null;
			
			if (curUI instanceof ListOfTitlesUI) {
				curElement = ((ListOfTitlesUI) curUI).getCurrentCatalogue();
			} else if (curUI instanceof ListViewUI) {
				curElement = ((ListViewUI) curUI).getCurrentCatalogue();
			} else if (curUI instanceof CarouselViewUI) {
				curElement = ((CarouselViewUI) curUI).getCurrentCatalogue();
			} else if (curUI instanceof WallofPostersFilmInfoUI) {
				curElement = ((WallofPostersFilmInfoUI) curUI).getCurrentCatalogue();
			} else { // VDTRMASTER-5608
				if (currentScene.getFooter().getParent() instanceof SimilarContentView) {
					curElement = ((SimilarContentView)currentScene.getFooter().getParent()).getFocusedContent();
				} else if (curUI instanceof TitleDetailUI) { // VDTRMASTER-5637
					curElement = ((TitleDetailUI) curUI).getTitle();
					
					if (curElement == null) {
						curElement = ((TitleDetailUI) curUI).getBundle();
					}
				}
			}
			
			if (curElement != null && curElement instanceof Orderable) {
				return BookmarkManager.getInstance().isAdult((Orderable)curElement);
			}
		}
		
		return isAdult;
	}
	
	
	// R5
	private void setLastWishContent(MoreDetail md) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, setLastWishContent, md=" + md);
		}
		lastWishContent = md;
		displayFavorite = 18;
	}
	
	// R5
	public boolean isLastWishContent(MoreDetail md) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, isLastWishContent, md=" + md + ", lastWishContent=" + lastWishContent);
		}
		if (lastWishContent != null && md != null && lastWishContent.equals(md)) {
			return true;
		}
		
		return false;
	}
	
	// R5
	public boolean needToDisplayFavorite() {
		if (displayFavorite > 0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuController, needToDisplayFavorite return true");
			}
			return true;
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuController, needToDisplayFavorite return false");
		}
		return false;
	}
	
	// R5
	public boolean isAdultWithSimilar() {
		boolean isAdult = false;
		if (currentScene != null && currentScene.getFooter() != null) {
			if (currentScene.getFooter().getParent() instanceof SimilarContentView) {
				BaseElement element = ((SimilarContentView)currentScene.getFooter().getParent()).getFocusedContent();
				if (element != null && element instanceof Orderable) {
					isAdult = BookmarkManager.getInstance().isAdult((Orderable)element);
				}
				
				if (((SimilarContentView)currentScene.getFooter().getParent()).hasChannelName()) {
					isAdult = false;
				}
			} else {
				isAdult = isAdult();
			}
		}
		
		return isAdult;
	}
}

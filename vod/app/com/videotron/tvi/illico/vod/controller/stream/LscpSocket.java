package com.videotron.tvi.illico.vod.controller.stream;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * LSC message packaging utility class Provide facility for delivering LSC messages, and for receiving LSC responses and
 * delivering them to a stream control implementation.
 *
 */
class LscpSocket {

    /** LSC OpCode indicating a pause request. */
    public static final int LSC_PAUSE = 1;

    /** LSC OpCode indicating a resume request. */
    public static final int LSC_RESUME = 2;

    /** LSC OpCode indicating a status request. */
    public static final int LSC_STATUS = 3;

    /** LSC OpCode indicating a reset request. */
    public static final int LSC_RESET = 4;

    /** LSC OpCode indicating a jump request. */
    public static final int LSC_JUMP = 5;

    /** LSC OpCode indicating a play request. */
    public static final int LSC_PLAY = 6;

    /** LSC OpCode indicating a done notice. */
    public static final int LSC_DONE = 0x40;

    /** LSC OpCode indicating a pause request. */
    public static final int LSC_PAUSE_REPLY = 0x81;

    /** LSC OpCode indicating a pause request. */
    public static final int LSC_RESUME_REPLY = 0x82;

    /** LSC OpCode indicating a pause request. */
    public static final int LSC_STATUS_REPLY = 0x83;

    /** LSC OpCode indicating a pause request. */
    public static final int LSC_RESET_REPLY = 0x84;

    /** LSC OpCode indicating a pause request. */
    public static final int LSC_JUMP_REPLY = 0x85;

    /** LSC OpCode indicating a pause request. */
    public static final int LSC_PLAY_REPLY = 0x86;

    /**
     * The LscpSocket class is a utility class for the StreamControlImpl class. It handles all socket communication with
     * the VOD server for stream control, packing request messages for the StreamControlImpl, and listening for and
     * unpacking LSC replies from the VOD server.
     *
     * @param addr
     * @param port
     * @param control
     * @throws IOException
     */
    LscpSocket(InetAddress addr, int port, StreamControlImpl control) throws IOException {
        m_socket = new Socket(addr, port);
        m_socket.setSoTimeout((int) Constants.MS_PER_HOUR);
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("LSCPSocket called, socket = " + m_socket);
        }

        // create the output stream for sending LSC commands
        // ByteArrayOutputStream bos = new ByteArrayOutputStream(20);
        m_outputStream = m_socket.getOutputStream();

        // create input stream for receiving LSC responses
        m_inputStream = new DataInputStream(m_socket.getInputStream());

        m_lscThread = new LscReaderThread(m_socket, control);
        m_lscThread.setName("LscThread");
        m_lscThread.start();
    }

    /**
     * Disconnect this LscpSocket from the VOD server.
     *
     */
    void disconnect() {
        if (Log.DEBUG_ON) {
        	Log.printDebug("LSCPSocket Disconnect called, socket = " + m_socket + ", thread = " + m_lscThread);
        }
        try {
            if (m_socket != null) {
                m_socket.close();
                m_socket = null;
            }
            if (m_outputStream != null) {
            	m_outputStream.close();
            	m_outputStream = null;
            }
            if (m_lscThread != null) {
            	m_lscThread.finish();
//	            // Wait upto 5 seconds for the lsc reader thread to complete
//	            synchronized (m_lscThread) {
//	                m_lscThread.wait(5000);
//	            }
            }
            m_lscThread = null;
        } catch (Exception e) {
            m_lscThread = null;
            Log.printDebug("LscpSocket: Exception in disconnect method!");
            e.printStackTrace();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("LSCPSocket Disconnect completed");
        }
    }

    /**
     * The LscReaderThread has the responsibility of listening on the given socket for LSC replies from the VOD server,
     * and communicating these replys to the StreamControlImpl reference.
     *
     */
    private class LscReaderThread extends Thread {

        LscReaderThread(Socket socket, StreamControlImpl streamControl) {
            m_socket = socket;
            m_streamControl = streamControl;
        }

        /**
         * Listen to the LSC Socket, reading messages and passing them into the StreamControlImpl. Expect
         * VodStreamControl events to be delivered on this thread.
         */
        public void run() {
            // LSC Response message variables
            byte version;
            byte transaction;
            byte status;
            byte opcode;
            int streamHandle;
            int npt;
            short scaleNum, scaleDen;
            byte mode;

            // Place the try block outside the while loop, as a single
            // IOException should
            // indicate a socket closure
            try {
                Thread thread = Thread.currentThread();

                while ((!isInterrupted() & !m_finished) && (thread == m_lscThread)) {
                    /**
                     * Buffered input check: standard LSC responses are 17 bytes long. Some VOD servers will pad this
                     * out to 20, others will not. We will read the first 3 bytes and check for valid OpCode at byte 3.
                     * If it is not valid we assume it is the padding for the previous message, and will read the next 3
                     * bytes as from the beginning of the next message.
                     */

                	try {
	                    version = m_inputStream.readByte();
	                    transaction = m_inputStream.readByte();
	                    opcode = m_inputStream.readByte();
	
	                    int opcodeVal = opcode & 0x000000FF;
	                    // check if this is a valid opcode
	                    if ((opcodeVal != LscpSocket.LSC_DONE)
	                            && ((opcodeVal < LscpSocket.LSC_PAUSE_REPLY) || (opcodeVal > LscpSocket.LSC_PLAY_REPLY))) {
	                        // not a valid opcode: treat them as padding bytes, and
	                        // start over
	                        version = m_inputStream.readByte();
	                        transaction = m_inputStream.readByte();
	                        opcode = m_inputStream.readByte();
	                    }
	
	                    status = m_inputStream.readByte();
	                    streamHandle = m_inputStream.readInt();
	                    npt = m_inputStream.readInt();
	                    scaleNum = m_inputStream.readShort();
	                    scaleDen = m_inputStream.readShort();
	                    mode = m_inputStream.readByte();
	
	                    // Setting stream ID and status for Diags
	                    LscpUtils.setStreamID(streamHandle);
	                    LscpUtils.setStatus(status);
	
	                    // send message to stream control impl
	                    if (Log.DEBUG_ON) {
	                        Log.printDebug("---***LscpSocket: NotifyReponse start ***---");
	                        Log.printDebug("status = " + status +
	                        		", streamHandle = " + streamHandle +
	                        		", npt = " + npt +
	                        		", scaleNum = " + scaleNum +
	                        		", scaleDen = " + scaleDen +
	                        		", mode = " + mode);
	                    }
	                    m_streamControl.notifyLscpResponse(transaction, opcode, status, streamHandle, npt, scaleNum,
	                            scaleDen, mode);
	
	                    if (Log.DEBUG_ON) {
	                        Log.printDebug("---***LscpSocket: NotifyReponse end   ***---");
	                    }
	
	                    // Increment diags counter:
	                    LscpUtils.incReceivedTransactions();
                	} catch (SocketTimeoutException ex) { // handle for SO_TIMEOUT - VDTRMASTER-5387
                		if (Log.DEBUG_ON) {
                            Log.printDebug("LscReaderThread, SocketTimeoutException received");
                        }
                	}

                }
                if (Log.DEBUG_ON) {
                    Log.printDebug("LSC while loop exit");
                }
            } catch (EOFException e) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("EOFException Expected");
                }
                // we expect this exception - do nothing
            } catch (IOException e) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("LscpSocket Socket Closed - receive expected IOException.");
                }
                // EOFException expected as the result of socket closure - other
                // received
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("LSCP Thread - last message!");
            }
            
            if (m_inputStream != null) {
            	try {
					m_inputStream.close();
				} catch (IOException e) {
				}
            	m_inputStream = null;
            }

            // Provide notification that we are done
            synchronized (this) {
                notifyAll();
            }
        }

        /**
         * terminate the execution of this thread.
         *
         */
        public void finish() {
            m_finished = true;
            this.interrupt();
        }

        /*
         * Referense to the stream control utilizing this socket thread
         */
        private StreamControlImpl m_streamControl = null;

        /*
         * Are we done yet?
         */
        private boolean m_finished = false;

        /*
         * Socket on which to listen
         */
        private Socket m_socket = null;
    }

    /**
     * Utility - dump byte array to sysout.
     *
     * @param buf
     */

    private void dumpBytes(byte buf[]) {
        int index = 0;
        int len = buf.length;

        if (Log.DEBUG_ON) {

            while (index < len) {
                Log.printDebug("<" + Integer.toString(index, 16) + ">");

                if (index % 16 < 16) {
                    Log.printDebug(" " + Integer.toString(buf[index]) + " ");
                }
                Log.printDebug("\n");
                index++;
            }
        }
    }

    /**
     * Utility - pack an integer into a byte array - network byte order.
     *
     * @param buf
     * @param offset
     * @param data
     */
    private void packInt(byte buf[], int offset, int data) {
        buf[offset] = (byte) ((data >> 24) & 0x000000ff);
        buf[offset + 1] = (byte) ((data >> 16) & 0x000000ff);
        buf[offset + 2] = (byte) ((data >> 8) & 0x000000ff);
        buf[offset + 3] = (byte) ((data) & 0x000000ff);
    }

    /**
     * Utility - pack a short into a byte array - network byte order.
     *
     * @param buf
     * @param offset
     * @param data
     */
    private void packShort(byte buf[], int offset, short data) {
        buf[offset] = (byte) ((data >> 8) & 0x000000ff);
        buf[offset + 1] = (byte) ((data) & 0x000000ff);
    }

    /**
     * Pack the basic LSC request header into buf.
     *
     * @param transactionID
     * @param opcode
     * @param stream
     * @param buf
     */
    private void packHeader(int transactionID, int opcode, int stream, byte buf[]) {
        buf[0] = (byte) 0; // version
        buf[1] = (byte) (transactionID & 0x000000ff); // transactionID
        buf[2] = (byte) (opcode); // opcode
        buf[3] = (byte) 0; // status
        packInt(buf, 4, stream);
    }

    /**
     * Write this buffer to the LSC Socket.
     *
     * @param buf
     */
    private void writeNetworkBytes(byte buf[], int code) {

    	if (m_outputStream == null) {
    		return;
    	}
    	
        try {
            m_outputStream.write(buf);

            // Increment the diags counter:
            LscpUtils.incSentTransactions();
        } catch (IOException e) {
            Log.printDebug(" LscpSocket -- IOException while writing lsc request");
            e.printStackTrace();
            Log.printWarning("IOException on LSCP, c = VOD604-0" + code);
        }
    }

    /**
     * Send an LSC Status request.
     *
     * @param transactionID
     * @param stream
     */
    void sendStatusRequest(int transactionID, int stream) {
        // Status requests are *8* bytes long
        byte buf[] = new byte[8];

        packHeader(transactionID, LSC_STATUS, stream, buf);

        writeNetworkBytes(buf, LSC_STATUS);
    }

    /**
     * Send an LSC pause request.
     *
     * @param transactionID
     * @param stream
     * @param stopNpt
     */
    void sendPauseRequest(int transactionID, int stream, int stopNpt) {
        // pause requests are *12* bytes long
        byte buf[] = new byte[12];

        packHeader(transactionID, LSC_PAUSE, stream, buf); // Header
        packInt(buf, 8, stopNpt); // Npt

        writeNetworkBytes(buf, LSC_PAUSE);
    }

    /**
     * Send an LSC resumre request.
     *
     * @param transactionID
     * @param stream
     * @param startNpt
     * @param scaleNum
     * @param scaleDen
     */
    void sendResumeRequest(int transactionID, int stream, int startNpt, short scaleNum, short scaleDen) {
        // Resume requests are *16* bytes long
        byte buf[] = new byte[16];

        packHeader(transactionID, LSC_RESUME, stream, buf); // header
        packInt(buf, 8, startNpt); // start Npt
        packShort(buf, 12, scaleNum); // rate numerator
        packShort(buf, 14, scaleDen); // rate den

        writeNetworkBytes(buf, LSC_RESUME);
    }

    /**
     * Send an LSC reset request.
     *
     * @param transactionID
     * @param stream
     */
    void sendResetRequest(int transactionID, int stream) {
        // reset requests are *8* bytes long
        byte buf[] = new byte[8];

        packHeader(transactionID, LSC_RESET, stream, buf);

        writeNetworkBytes(buf, LSC_RESET);
    }

    /**
     * Send an LSC play request.
     *
     * @param transactionID
     * @param stream
     * @param startNpt
     * @param stopNpt
     * @param scaleNum
     * @param scaleDen
     */
    void sendPlayRequest(int transactionID, int stream, int startNpt, int stopNpt, short scaleNum, short scaleDen) {
        // play requests are *20* bytes long
        byte buf[] = new byte[20];

        packHeader(transactionID, LSC_PLAY, stream, buf); // header
        packInt(buf, 8, startNpt); // start Npt
        packInt(buf, 12, stopNpt); // stop Npt
        packShort(buf, 16, scaleNum); // rate numerator
        packShort(buf, 18, scaleDen); // rate den

        int code = LSC_PLAY;
        if (scaleNum < 0) {
        	code = 7; // rew
        } else if (scaleNum > 0){
        	code = 8; // ff
        } else if (scaleDen > 1) {
        	code = 9; // slow
        }
        writeNetworkBytes(buf, code);

        if (Log.DEBUG_ON) {
        	Log.printDebug("sendPlayRequest, " + scaleNum + "/" + scaleDen);
        }
    }

    /**
     * VOD stream control socket connection.
     */
    private Socket m_socket;

    /**
     * out going lsc data socket stream.
     */
    private OutputStream m_outputStream;

    /**
     * incoming lsc data socket stream.
     */
    private DataInputStream m_inputStream;

    private LscReaderThread m_lscThread;

}

package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.ErrorCodes;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;

/**
 * DSMCCUDPSocket.
 */
public class DSMCCUDPSocket implements Runnable, TVTimerWentOffListener {
    /** List of listeners to dsmcc messages. */
    private Vector listenerList = new Vector();

    private boolean clientSocketListenerRunning;

    /**
     * Stack does not properly handle two-directional messaging on a single socket. So- use 2 as a work around until
     * they fix this.
     */

    private DatagramSocket receiveSocket;

    private DatagramSocket sendSocket;

    /**
     * @throws java.net.SocketException
     */
//    public DSMCCUDPSocket() throws SocketException {
//        receiveSocket = new DatagramSocket();
//        sendSocket = new DatagramSocket();
//    }

    /**
     * @param port
     * @throws java.net.SocketException
     */
    public DSMCCUDPSocket(int port) throws SocketException {
    	SocketException exc = null;
    	try {
	        Log.printDebug(" create receive DSMCCUDPSocket port " + port);
	        receiveSocket = new DatagramSocket(port);
    	} catch (Exception e) {
    		e.printStackTrace();
    		if (e instanceof SocketException) {
    			exc = (SocketException) e;
    		}
    	}
    	try {
	        Log.printDebug(" create send DSMCCUDPSocket ");
	        sendSocket = new DatagramSocket();
    	} catch (Exception e) {
    		e.printStackTrace();
    		if (e instanceof SocketException) {
    			exc = (SocketException) e;
    		}
    	}
    	if (exc != null) {
    		throw exc;
    	}
    }

//    /**
//     * @param port
//     * @param laddr
//     * @throws java.net.SocketException
//     */
//    public DSMCCUDPSocket(int port, InetAddress laddr) throws SocketException {
//        receiveSocket = new DatagramSocket(port, laddr);
//        sendSocket = new DatagramSocket();
//    }

    /**
     * start ======== Performs any needed initialization.
     *
     * @return
     */
    public int start() {
        // This was originally used to start up the receive thread in this
        // object. However, the PowerTV DatagramSocket blocked sends
        // on a blocked receive and hence two sockets had to be used.
        // So - if we ever get back to 1 socket then you can move the
        // thread inside of this class.
        int rc = ErrorCodes.SUCCESS;

        return rc;
    }

    /**
     *
     * Called as part of the cleanup process of this object. This will shutdown the listener thread
     * and drop any outstanding state data.
     */
    public void shutdown() {
        Log.printDebug("DSMCCUDPSocket Shutdown called, this = " + toString() + ", rcv = " + receiveSocket + ", send = " + sendSocket);
        
        if (clientSocketListenerRunning) {
            try {
                clientSocketListenerRunning = false;
                
                receiveSocket.close();
                sendSocket.close();
                
            } catch (Exception ex) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Exception caught on shutdown : " + ex.getMessage());
                }
//            } catch (Throwable th) {
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("Throwable caught on shutdown : " + th.getMessage());
//                }
            }
        }
    }

    /**
     *
     * Sends a UDP packet to the SRM.
     *
     * @param msg
     *            The DSM-CC message stored as a byte array.
     * @throws IOException
     *             Should the socket fail.
     */
    public void send(byte[] msg, InetAddress addr, int port) throws IOException {
        if (Log.DEBUG_ON) {
        	String hex = StringUtil.byteArrayToHexString(msg);
            Log.printDebug("Send message DSMCC ->> " + hex);
            String[] lines = StringUtil.tokenize(hex, "\n");
            ArrayList l = MenuController.getInstance().debugList[3];
            for (int i = lines.length-1; i > 0; i--) {
            	l.add(">> " + lines[i]);
            }
        }

        if (clientSocketListenerRunning) {
            DatagramPacket pkt = new DatagramPacket(msg, msg.length, addr, port);
            if (Log.DEBUG_ON){
                Log.printDebug("Sending DSMCC Packet " + addr.toString());
            }
            try {
                sendSocket.send(pkt);
                if (Log.DEBUG_ON){
                    Log.printDebug("Sent DSMCC Packet");
                }
                // Increment the diags counter:
                SspUtils.incTransactionsSent();
            } catch (Throwable t) {
                if (Log.DEBUG_ON){
                    Log.printDebug("DSMCCUDPSocket.send() exception: " + t);
                }
                // Throw IOException so the caller knows something went bad.
                throw new IOException();
            }
        } else {
            if (Log.DEBUG_ON){
                Log.printDebug("DSMCC ClientSocket not running");
            }
        }
    }

    /**
     * Runs the socket listener thread on a non-pre-determined port.
     */
    public void run() {
        clientSocketListenerRunning = true;
        DatagramPacket pkt = null;
        byte[] messageBuffer = new byte[2000];;

        if (Log.DEBUG_ON) {
            Log.printDebug("DSMCCUDPSocket Starting listenerThread Version 1.0");
        }

        while (clientSocketListenerRunning) {
            // Read in a message.
            if (Log.DEBUG_ON) {
                Log.printDebug("Wait on new message...");
            }

            try {
                pkt = new DatagramPacket(messageBuffer, messageBuffer.length);
            } catch (Exception ex) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Couldn't create new packet: " + ex.getMessage());
                }
                ex.printStackTrace();
            }

            try {
            	receiveSocket.setSoTimeout(5000);
                receiveSocket.receive(pkt);
                // Strip off excess buffer and copy for processing.
                byte msg[] = new byte[pkt.getLength()];
                System.arraycopy(messageBuffer, 0, msg, 0, msg.length);

                if (Log.DEBUG_ON) {
                    String hex = StringUtil.byteArrayToHexString(msg);
                    Log.printDebug("Received message DSMCC <<- " + hex);
                    String[] lines = StringUtil.tokenize(hex, "\n");
                    ArrayList l = MenuController.getInstance().debugList[3];
                    for (int i = lines.length-1; i > 0; i--) {
                    	l.add("<< " + lines[i]);
                    }
                }

                // Increment diags counter:
                SspUtils.incTransactionsReceived();

                // Send off for digestion.
                sendEvents(msg);
            } catch (SocketTimeoutException ste) {
            	if (Log.DEBUG_ON) {
                    Log.printDebug("Timeout on receive socket, it's ok");
                }
            } catch (IOException ex) {
                // We received an exception. Could be someone trying to
                // terminate our thread. Report it and swing around again.
                if (Log.DEBUG_ON) {
                    Log.printDebug("Exception on socket receive: " + ex.getMessage());
                }
                ex.printStackTrace();
                continue;
            }
        } // While m_ClientSocketListenerRunning

        if (Log.DEBUG_ON) {
            Log.printDebug("Recieving finished from " + receiveSocket);
        }
    }

    /**
     *
     * Distribute events to any listeners.
     *
     * @param event
     *            The DSMCCSessionEvent to distribute.
     */
    private synchronized void sendEvents(byte[] event) {
        for (Enumeration e = listenerList.elements(); e.hasMoreElements();) {
            DSMCCSocketEventListener dl = (DSMCCSocketEventListener) e.nextElement();
            dl.receiveDSMCCEvent(event);
        }
    }

    /**
     *
     * Adds a listener to the distribution list.
     *
     * @param de
     *            The listener to add.
     */
    public synchronized void addDSMCCSocketEventListener(DSMCCSocketEventListener de) {
        listenerList.addElement(de);
    }

    /**
     *
     * Removes a listener from the list.
     *
     * @param de
     *            The listener to remove
     */
    public synchronized void removeDSMCCSocketEventListener(DSMCCSocketEventListener de) {
        listenerList.removeElement(de);
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
    }
}

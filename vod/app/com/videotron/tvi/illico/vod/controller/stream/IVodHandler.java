package com.videotron.tvi.illico.vod.controller.stream;

/**
 * Represents a video on demand session that must be connected to obtain the required network
 * resources for Vod operation.
 */
public interface IVodHandler {
    /** identifies the descriptors added to the connect request method.*/
    public static final int CONNECT_REQUEST_DESCRIPTOR_TYPE = 0;

    /** identifies the descriptors returned from the server when the connection completes.*/
    public static final int CONNECT_COMPLETE_DESCRIPTOR_TYPE = 1;

    /** identifies the descriptors added by the application and sent via  the disconnect request method.*/
    public static final int DISCONNECT_REQUEST_DESCRIPTOR_TYPE = 2;

    /** identifies the descriptors added by the server in response to the VodHandlerDisconnectEvent*/
    public static final int DISCONNECT_COMPLETE_DESCRIPTOR_TYPE = 3;

    /** specifies the descriptors sent by the server when the server initiates the disconnection.*/
    public static final int DISCONNECT_NOTIFICATION_DESCRIPTOR_TYPE = 4;

    /** The state of this object when it is not connected. */
    public static final int STATE_DISCONNECTED = 0;

    /** The state of this object when it is connected. */
    public static final int STATE_CONNECTED = 1;

    /**
     * Subscribes a listener to Vod events from the associated  IVodHandler object.
     * 
     * @param listener
     *            The IVodHandlerListener to subscribe receive Vod events.
     */
    public void addVodListener(IVodHandlerListener listener);

    /**
     * Unsubscribes the listener to receiving Vod events from this VodHandler.
     * 
     * @param listener
     *            The IVodHandlerListener</code> to unsubscribe.
     */
    public void removeVodListener(IVodHandlerListener listener);

    /**
     * Terminates the IVodHandler</code> connection from the video server. All resources
     * are terminated when this call returns. This call does not generate the event
     * VodHandlerDisconnectedEvent.
     * 
     * @param act
     *            asynchronous completion token.
     */
    public void disconnect(Object act);

    /**
     * Called to acquire the stream control object used to control the Vod stream.
     * 
     * @return  One or more IVodStreamControl objects or null if connection failed.
     */
    public IVodStreamControl[] getStreams();

    /**
     * This method sets server parameters needed to establish a connection. This method must be
     * performed before calling connect. Using this method with other setPrivate or addPrivate methods could be
     * dangerous.
     * 
     * @param sessionGW
     *            the integer ip address of the session gateway server.
     * @param secondarySessionGW
     *            the integer ip address of the secondary gateway server.
     * @param serviceGW
     *            the name of the service gateway server.
     * @param serviceName
     *            the name of the vod service.
     * @throws Exception
     */
    public void setServers(int sessionGW, int secondarySessionGW, String serviceGW, String serviceName, int appServer)
            throws Exception;

    /**
     * Adds a descriptor to this object.
     * <p>
     * Use CONNECT_REQUEST_DESCRIPTOR_TYPE when adding descriptors to accompany 
     * the IVodHandler.connect request method.
     * <p>
     * Use DISCONNECT_REQUEST_DESCRIPTOR_TYPE when adding descriptors to accompany 
     * the IVodHandler.disconnect request
     * method.
     * 
     * @param type
     *            specifies the descriptor associated with a msg type.
     * @param tag
     *            the descriptor tag identifier
     * @param length
     *            length of bytes in the array
     * @param offset
     *            a pointer to the specific descriptor bytes within the array. If this value is zero, the first byte of
     *            the array is used.
     * @param data
     *            array of formatted bytes
     * @return the current descriptor count once the function returns.
     * 
     * @see IVodHandler#CONNECT_REQUEST_DESCRIPTOR_TYPE
     * 
     * @see IVodHandler#DISCONNECT_REQUEST_DESCRIPTOR_TYPE
     * 
     * @see IVodHandler#DISCONNECT_NOTIFICATION_DESCRIPTOR_TYPE
     */
    public int addDescriptor(int type, int tag, int length, int offset, byte[] data);

     /**
     * Responds with the connection status of this object.
     * 
     * @see IVodHandler#STATE_CONNECTED
     * @see IVodHandler#STATE_DISCONNECTED
     * 
     * @return The state of the connection.
     */
    int getConnectionState();

    /**
     * 
     * getSessionID , Used to obtain the SRM 10 byte session ID.
     * 
     * @return the SRM byte[10] name of the session.
     */
    public byte[] getSessionID();
}

package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;

import org.ocap.net.OcapLocator;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.VideoControllerImpl;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.ATSCModulationModeResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.CAResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.IPResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.MPEGProgramResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.PhysicalChannelResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.SessionResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.TSIDResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.controller.session.service.ServiceLocator;

public class DSMCCResultParser {

    private Vector resources;

    private Vector setupReqDescriptors;

    private Vector setupCompleteDescriptors;

    private Vector releaseReqDescriptors;

    private Vector relCompleteDescriptors;

    private Vector notificationDescriptors;

    private static final int STATE_IDLE = 0;

    private static final int STATE_ACTIVE = 1;

    private static final int STATE_PENDING_SETUP = 2;

    private static final int STATE_PENDING_RELEASE = 3;

    private static final int STATE_PENDING_SG_COMPLETE = 3;

    private static final int STATE_DONE = 4;

    private int state = STATE_IDLE;

    public DSMCCResultParser() {
        resources = new Vector();
        setupReqDescriptors = new Vector();
        setupCompleteDescriptors = new Vector();
        releaseReqDescriptors = new Vector();
        relCompleteDescriptors = new Vector();
        notificationDescriptors = new Vector();
    }

    public SessionResource[] getResources() {
        int len = resources.size();
        SessionResource[] result = new SessionResource[len];

        for (int elemNdx = 0; elemNdx < len; elemNdx++) {
            result[elemNdx] = (SessionResource) resources.elementAt(elemNdx);
        }
        return result;
    }

    public SessionDescriptor[] getSessionDescriptor() {
        int len = setupCompleteDescriptors.size();
        SessionDescriptor[] result = new SessionDescriptor[len];

        for (int elemNdx = 0; elemNdx < len; elemNdx++) {
            result[elemNdx] = (SessionDescriptor) setupCompleteDescriptors.elementAt(elemNdx);
        }
        return result;
    }

    /*
     *
     * Memory layout of user private data
     *
     */
    public void doSetupSucceeded(DataInputStream disRef) {
        // Data now comes in as defined in 4.2.4.2 ClientSessionSetupConfirm of
        // DSMCC spec.
        if (Log.DEBUG_ON) {
            Log.printDebug("ISessionHandlerImpl::DoSetupSucceeded ENTRY");
        }

        try {
            state = STATE_ACTIVE;

         // see 4.2.2.4 for data format
            // read server ID
            // byte[] serverId = new byte[20];
            // disRef.readFully(serverId);

            // read number or raw resource descriptors
            // We are at the head of table 4-7 Resources()

            int nrds = disRef.readUnsignedShort();

            // Do only if resources exist.
            if (nrds > 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("nrds : " + nrds);
                }

                for (int i = 0; i < nrds; i++) {

                    // Now read the data structure of Table 4-62.
                    // Start with Table 4-63 common Descriptor Header.

                    SessionResource sr = new SessionResource(disRef);

                    int type = sr.getResourceDescriptorType();

                    if (Log.DEBUG_ON) {
                        Log.printDebug("DoSetupSucceeded- Resource type = [ " + i + " , "
                                + SessionResource.sessionResourceTypeToString(type) + " ]");
                    }

                    // SessionResource subclass parses and copies fields of
                    // the ba.
                    // If the type is not known, the class will know how to
                    // read the
                    // header, but not the actual data.. Define a new class
                    // called PrivateResource
                    switch (type) {
                    case SessionResource.MPEGProgram:
                        MPEGProgramResource rMPEGProgram = new MPEGProgramResource(disRef, sr);
                        resources.addElement(rMPEGProgram);
                        break;

                    case SessionResource.PhysicalChannel:
                        PhysicalChannelResource rPhysicalChannel = new PhysicalChannelResource(disRef, sr);
                        resources.addElement(rPhysicalChannel);
                        break;

                    case SessionResource.TSDownStream:
                        TSIDResource rTSDownStream = new TSIDResource(disRef, sr);
                        resources.addElement(rTSDownStream);
                        break;

                    case SessionResource.IPDescriptor:
                        IPResource rIPDescriptor = new IPResource(disRef, sr);
                        resources.addElement(rIPDescriptor);
                        break;

                    case SessionResource.ATSCModulationMode:
                        ATSCModulationModeResource rATSCModulationMode = new ATSCModulationModeResource(disRef, sr);
                        resources.addElement(rATSCModulationMode);
                        break;

                    case SessionResource.ClientConditionalAccess:
                        CAResource rClientConditionalAccess = new CAResource(disRef, sr);
                        resources.addElement(rClientConditionalAccess);
                        break;

                    default:
                    }
                }
            }

            // Iff private data
            int udpLength = 0;

            // If there isn't at least 4 bytes we can't go forward.
            // Releases have no private data?
            if (disRef.available() > 4) {
                udpLength = disRef.readInt();
            }

            if (udpLength > 0) {
                readAndStorePrivateDataDescriptors(ISessionHandleType.SETUP_COMPLETE_DESCRIPTOR_TYPE, disRef,
                        udpLength);
            }
        } catch (IOException ex) {
            Log.printDebug("DSMCCResultParser.doSetupSucceeded- Exception caught during resource processing");
            ex.printStackTrace();
        }
    }

    /**
     * doSetupFailed
     *
     * @param disRef
     */
    public void doSetupFailed(DataInputStream disRef, int responseCode) {
        Log.printDebug("DSMCCResultParser.doSetupFailed, rc = " + responseCode);

        state = STATE_DONE;
        // Iff private data
        try {
            // Upon entering this method, disRef is at the head of table 4-7
            // Resources() structure
            // First thing is to read number of resource descriptors
            int nrds = disRef.readUnsignedShort();

            // In the case of a failed setup we don't expect any resources to be
            // attached
            // to the message. If there are any, log an error.
            if (nrds > 0) {
                Log.printWarning("DSMCCResultParser.doSetupFailed- unexpected resources in failed setup message");
            }

            // Now we are correctly positioned to read in the UUData/private
            // data stuff
            int udpLength = disRef.readInt();
            if (udpLength > 0) {
                readAndStorePrivateDataDescriptors(
                        ISessionHandleType.SETUP_COMPLETE_DESCRIPTOR_TYPE, disRef, udpLength);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        // SessionSetupFailedEvent ev = new SessionSetupFailedEvent(this,
        // responseCode, m_setupAct);
        // notifyListeners(ev);
    }

    /**
     * doSetupTimedout
     *
     * @param disRef
     */
    public void doSetupTimedout(DataInputStream disRef, int responseCode) {
        Log.printDebug("DSMCCResultParser.doSetupTimeout- ");

        state = STATE_DONE;

        // SessionSetupTimeOutEvent ev = new SessionSetupTimeOutEvent(this,
        // m_setupAct);
        // notifyListeners(ev);
    }

    /**
     * doReleaseSucceeded
     *
     * @param disRef
     */
    public void doReleaseSucceeded(DataInputStream disRef) {
        try {
            state = STATE_DONE;

            if (Log.DEBUG_ON) {
                Log.printDebug("DSMCCResultParser.doReleaseSucceeded- ");
            }
            // UDPLENGTH
            int udpLength = disRef.readInt();
            if (udpLength > 0) {
                readAndStorePrivateDataDescriptors(ISessionHandleType.RELEASE_COMPLETE_DESCRIPTOR_TYPE, disRef,
                        udpLength);
            }

        } catch (Exception ex) {
            Log.printDebug("DSMCCResultParser.doReleaseSucceeded- Exception caught during resource processing");
            ex.printStackTrace();
        }
        // SessRelSucceededEvent ev = new SessRelSucceededEvent(this,
        // m_setupAct, null, SessionEvent.RspOk);
        // notifyListeners(ev);
    }

    /**
     * readAndStorePrivateDataDescriptors
     * <b>Start of upd descriptors</b> ......| descriptor cnt | tag_8 | dataLen_8 | n bytes | tag_8 | dataLen_8 | .... |
     */
    public void readAndStorePrivateDataDescriptors(int type, DataInputStream disRef, int udpLength) {
        byte tag;
        byte length;
        byte[] ba;
        int countdown;

        if (Resources.sspVersion == Resources.SSP23) {
            countdown = udpLength - 42;
        } else {
            countdown = udpLength - 2;
        }
        try {
            // Add the size of the Pegasus header to index to skip over the
            // Pegasus header information
            // Table 24 of sspVersion 2.3
            if (Resources.sspVersion == Resources.SSP23) {
                disRef.skipBytes(42);
            } else {
            	
            	int protocolId = disRef.read();
            	int version = disRef.read();
            	if (Log.DEBUG_ON) {
                    Log.printDebug("DSMCCResultParser.readAndStorePrivateDataDescriptors-protocolId=" + protocolId);
                    Log.printDebug("DSMCCResultParser.readAndStorePrivateDataDescriptors-version=" + version);
                }
            	
//                disRef.skipBytes(2);
            	
            }
            byte pdDescrCnt = disRef.readByte();
            --countdown;

            if (Log.DEBUG_ON) {
                Log.printDebug("DSMCCResultParser.readAndStorePrivateDataDescriptors- private data descriptor count = "
                        + pdDescrCnt);
            }

            if (pdDescrCnt > 0) {
                for (byte i = 0; i < pdDescrCnt; i++) {
                    tag = disRef.readByte();
                    length = disRef.readByte();
                    ba = new byte[length];

                    disRef.readFully(ba, 0, (int) length);
                    countdown -= (2 + length);

                    SessionDescriptor sd = new SessionDescriptor(tag, length, ba);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("tag : " + tag);
                        Log.printDebug("length : " + length);
                        Log.printDebug("ba : " + StringUtil.byteArrayToHexString(ba));
                    }

                    switch (type) {
                    case ISessionHandleType.SETUP_COMPLETE_DESCRIPTOR_TYPE:
                        setupCompleteDescriptors.addElement(sd);
                        break;
                    case ISessionHandleType.RELEASE_COMPLETE_DESCRIPTOR_TYPE:
                        relCompleteDescriptors.addElement(sd);
                        break;
                    case ISessionHandleType.RELEASE_NOTIFICATION_DESCRIPTOR_TYPE:
                        notificationDescriptors.addElement(sd);
                        break;
                    default:
                        break;
                    }
                    // System.out.println("ISH:readAndStorePrivateDataDescriptors-
                    // adding completion descr["+i+"] tag = "+tag);
                }
                disRef.skipBytes(countdown);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * DoReleaseFailed | ET_32 | RESP_32 | sessNameCnt_32 | sessName |.
     *
     * @param disRef
     */
    public void doReleaseFailed(DataInputStream disRef, int responseCode) {
        try {
            Log.printDebug("DSMCCResultParser.doReleaseFailed- ");
            state = STATE_DONE;

            // UDPLENGTH
            int udpLength = disRef.readInt();
            if (udpLength > 0) {
                readAndStorePrivateDataDescriptors(ISessionHandleType.RELEASE_COMPLETE_DESCRIPTOR_TYPE, disRef,
                        udpLength);
            }
        } catch (Exception ex) {
            Log.printDebug("DSMCCResultParser.doReleaseFailed- Exception caught during resource processing");
            ex.printStackTrace();
        }

        // SessionReleaseFailedEvent ev = new SessionReleaseFailedEvent(this,
        // m_setupAct, responseCode);
        // notifyListeners(ev);
    }

    /**
     *
     * DoReleaseTimedout
     *
     * @param disRef
     */
    public void doReleaseTimedout(DataInputStream disRef, int responseCode) {
        try {
            Log.printDebug("DSMCCResultParser.doReleaseTimedout- ");
            state = STATE_DONE;

            // SessRelTimeOutEvent ev = new SessRelTimeOutEvent(this,
            // m_setupAct);
            // notifyListeners(ev);
        } catch (Exception ex) {
            Log.printDebug("DSMCCResultParser.doReleaseTimedout- Exception caught during resource processing");
            ex.printStackTrace();
        }
    }

    public void doReleaseRequired(DataInputStream disRef, int responseCode) {
        try {
            if (Log.DEBUG_ON) {
                Log.printDebug("DSMCCResultParser.doReleaseRequiredNotificationEvent- ");
            }

            state = STATE_DONE;
            // Add the size of the Pegasus header to index to skip over the
            // Pegasus header information

            // Upon entering this method, disRef is at the head of table 4-7
            // Resources() structure
            // First thing is to read number of resource descriptors
            int nrds = disRef.readUnsignedShort();

            // In the case of a requested teardown we don't expect any resources
            // to be attached
            // to the message. If there are any, log an error.
            if (nrds > 0) {
                Log.printDebug("DSMCCResultParser.doSetupFailed- unexpected resources in teardown request message");
            }

            // SessRelReqdNotifyEvent ev = new SessRelReqdNotifyEvent(this,
            // responseCode, null);
            // notifyListeners(ev);
        } catch (Exception ex) {
            Log
                    .printDebug("DSMCCResultParser.doReleaseRequiredNotificationEvent- Exception caught during resource processing");
            ex.printStackTrace();
        }
    }

    public OcapLocator[] getLocatorInformation(String sessionName) {
        ServiceLocator locator = null;
        OcapLocator[] ocapLocator = null;
        SessionResource sr[] = getResources();
        if (sr.length == 0){
            return null;
        }
        int freq = 0;
        int prognum = -1;
        int qam = -1;
        byte ca[] = null;

        if (sr != null) {
            for (int i = 0; i < sr.length; i++) {
                if (sr[i] instanceof PhysicalChannelResource) {
                    PhysicalChannelResource pcr = (PhysicalChannelResource) sr[i];
                    freq = pcr.getChannelId();

                    if (Log.DEBUG_ON) {
                        Log.printDebug("PhysicalChannelResource is: " + freq);
                    }
                }
                if (sr[i] instanceof MPEGProgramResource) {
                    MPEGProgramResource mpr = (MPEGProgramResource) sr[i];
                    prognum = mpr.getMPEGProgramNumber();
                    if (Log.DEBUG_ON) {
                        Log.printDebug("MPEGProgramResource Prog# is: " + prognum);
                    }
                }
                if (sr[i] instanceof ATSCModulationModeResource) {
                    ATSCModulationModeResource amr = (ATSCModulationModeResource) sr[i];
                    qam = amr.getModulationFormat();

                    if (Log.DEBUG_ON) {
                        Log.printDebug("ATSCModulationModeResource QAM Format # is: " + qam);
                    }
                }
                if (sr[i] instanceof CAResource) {
                    CAResource amr = (CAResource) sr[i];
                    ca = amr.getCAInfo();

                    if (Log.DEBUG_ON) {
                        Log.printDebug("CAResource is: " + StringUtil.byteArrayToHexString(ca));
                    }
                }
            }

            // confirm that we received all relevant resources
            if (freq == 0 || prognum == -1) {
                Log.printDebug(" Error creating VOD locator! freq=" + freq + " prognum" + prognum + "qam"
                        + qam + "\n");
                return null;
            }

            if (qam == -1) {
                // According to the sspVersion spec, in the absence of an ATSCMod
                // resource,
                // default to QAM256 (ie, 16)
                qam = 16;
            }

            if (Log.DEBUG_ON) {
                Log.printDebug(" CA = " + StringUtil.byteArrayToHexString(ca));
            }

            // try to create the locator
            try {
                if (Log.DEBUG_ON) {
                    Log.printDebug(" getLocatorInformation : sessionName : " + sessionName);
                }
                byte SessionId[] = VideoControllerImpl.getInstance().getSessionConnector().getSessionID(sessionName);
                locator = new ServiceLocator(freq, prognum, qam, ca, SessionId);
                MenuController.getInstance().debugList[3].add("getLocator : f=" + freq + ", p=" + prognum + ", q=" + qam);
                ocapLocator = new OcapLocator[1];
                ocapLocator[0] = locator;
            } catch (Exception e) {
                Log.printDebug(" Invalid locator constructed parsing session resources: VodHandlerImpl\n");
                e.printStackTrace();
                return null;
            }
        }
        return ocapLocator;
    }
}

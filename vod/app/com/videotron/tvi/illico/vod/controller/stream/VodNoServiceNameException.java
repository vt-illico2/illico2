package com.videotron.tvi.illico.vod.controller.stream;

/**
 * Generated if the service name has not been set.
 * 
 */
public class VodNoServiceNameException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 550976321979716220L;

    public VodNoServiceNameException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     *            The detailed reason.
     */
    public VodNoServiceNameException(String arg0) {
        super(arg0);
    }
}

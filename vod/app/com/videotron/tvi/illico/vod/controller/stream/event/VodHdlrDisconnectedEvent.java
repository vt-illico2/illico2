package com.videotron.tvi.illico.vod.controller.stream.event;

import com.videotron.tvi.illico.vod.controller.stream.IVodHandler;
import com.videotron.tvi.illico.vod.controller.stream.VodHandlerEvent;

/**
 * VodHandlerDisconnectedEvent Sent by IVodHandler if the it is abnormally disconnected. This event is not generated as
 * a result of IVodHandler.disconnect.
 */
public class VodHdlrDisconnectedEvent extends VodHandlerEvent {

    private static final long serialVersionUID = 5517425926604735665L;

    private int m_reason;

    /** the server detected an error. */
    public static final int SERVER_ERROR = 0x00;

    /** the server detected a format error. */
    public static final int SERVER_FORMAT_ERROR = 0x01;

    /** the connection was dropped for unknown reasons. */
    public static final int UNKNOWN = 0x02;

    /** if the client disconnects because of a communications error. */
    public static final int COMMUNICATIONS_ERROR = 0x03;

    /** if the client could not maintain resources for the connection. */
    public static final int NO_RESOURCES = 0x04;

    /**
     * Constructs the event.
     * 
     * @param objSrc
     *            Source of the event.
     * @param act
     *            Asynchronous completion token.
     * @param reason
     *            Reason code of success or failure.
     */
    public VodHdlrDisconnectedEvent(IVodHandler objSrc, Object act, int reason) {
        super(objSrc, act);
        m_reason = reason;
    }

    /**
     * Responds with the reason for disconnecting.
     * 
     * @return the reason for disconnecting.
     */
    public int getReason() {
        return m_reason;
    }
}

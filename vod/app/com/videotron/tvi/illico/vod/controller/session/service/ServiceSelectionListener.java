package com.videotron.tvi.illico.vod.controller.session.service;

public interface ServiceSelectionListener {
	public static int SELECT_FAIL = -1;
    public static int SELECT_SUCCESS = 100;
    public static int SELECT_BUT_CONTENT_NOT_FOUND = 200;

    public abstract void notifyChannelSelectResult(int result);
}

package com.videotron.tvi.illico.vod.controller.session.dsmcc;

public class SspUtils {
    private static int transactionsReceived = 0;

    private static int transactionsSent = 0;

    public static void incTransactionsReceived() {
        transactionsReceived++;
    }

    public static int getTransactionsReceived() {
        return transactionsReceived;
    }

    public static void cleanTransactionsReceived() {
        transactionsReceived = 0;
    }

    public static void incTransactionsSent() {
        transactionsSent++;
    }

    public static int getTransactionsSent() {
        return transactionsSent;
    }

    public static void cleanTransactionsSent() {
        transactionsSent = 0;
    }
}

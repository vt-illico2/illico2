/*  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.AdaptationHeader;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionInProgress;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionProceedingIndication;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionReleaseConfirm;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionReleaseIndication;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionReleaseRequest;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionReleaseResponse;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionSetupConfirm;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ClientSessionSetupRequest;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCMessage;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionEvent;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionEventListener;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionState;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSocketEventListener;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCUDPSocket;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ReasonCodes;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ResRspCode;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.SessionMessage;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.SessionProperties;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.TransactionID;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.UserPrivateData;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.SynchronizedHash;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;

/**
 * @author LSC
 * @version 1.0 SessionConnectorImpl This class handles low level DSMCC message communication with the SRM of the DNCS.
 */
public class SessionConnectorImpl implements SessionConnector, TVTimerWentOffListener, DSMCCSocketEventListener {

    /** The socket in which SRM setup and teardown messges are sent. */
    private DSMCCUDPSocket datagramSocket;

    /** Hash the session Name to SessionID (Array[10]) and to State. */
    private SynchronizedHash sessionIdAToNameHashB = new SynchronizedHash();

    /** List of listeners to session events. */
    private Vector listenerList = new Vector();

    /**
     * Initialization parameters The messenger requires several parameters in order to set up sessions. the MAC address
     * specifies the Media Access Control number used by this set top box for identification.
     */
    private byte[] macAddr;

    /**
     * tMSGTimeOut is the maximum amount of time to wait for a response to a message from the server.
     */
    private int tMSGTimeOut;

    /**
     * The client must send periodic SessionInProgress messages in order to keep the session alive on the server. This
     * is the time period between client messages. (See ISO/IEC 13818=6:1998(E) 3.2.1)
     */
    private int tSessionInProgTimeout;

    /** tSessionInProgTimeout/1000. */
    private int tSessionInProgTime;

    /**
     * This is the maximum number of times to retry a message after a timeout failure. (See ISO/IEC 13818=6:1998(E)
     * 3.2.1)
     */
    private int messageRetryCount;

    /**
     * Indicates whehter the sessionId is assigned by the User requesting the session or by the Network. A value of 0
     * indicates that the Network will assign the sessionId. A value of 1 indicates that ht eUser shall assign the
     * sessionId. In our case, this should always be 1. (See ISO/IEC 13818=6:1998(E) 3.2.1)
     */
    private int sessionIdAssignor;

    /**
     * Indicates whether the resource Id is assigned by the User requesting the resource or by the Network. A value of 0
     * indicates that the Network will assign the resourceId. A value of 1 indicatest that the User shall assign athe
     * resourceId. (See ISO/IEC 13818=6:1998(E) 3.2.1)
     */
    private int resourceIdAssignor;

    /**
     * Indicates the maximum number of times that a session request may be forwarded before it is rejected. If this
     * count is set to 0, this is an indication that session requests shall not be forwarded. In our case, it is always
     * 0. (See ISO/IEC 13818=6:1998(E) 3.2.1)
     */
    private int maxForwardCount;

    /**
     * Contains the Java representation of an IP address.
     */
    private InetAddress dncsIPObj;

    /**
     * Contains the target port to send session setup messages to. Note that the default is 13819. The older client OS
     * binds to this port and hence if you don't disable it, opening the port will fail.
     */
    private int dncsPort;

    /**
     * Contains the byte array of an IP address.
     */
    private byte[] byteServerAddr = new byte[4];

    /**
     * Thread for communication with DNCS.
     */
    private Thread socketThread;
    /**
     * TVTimer for maximum allowed delay before timing out for communication with DNCS.
     */
    private TVTimer timer;
    /**
     * TVTimerSpec for communication with DNCS.
     */
    private TVTimerSpec timerSpec;

    /**
     * Default constructor.
     */
    public SessionConnectorImpl() {

    }

    /**
     * This method is called when one wishes to redefine the system UU configuration parameters such as messageTimeout
     * and the servers in which to communicate with.
     *
     * @throws UnknownHostException
     *             Thrown to indicate that the IP address of a host could not be determined.
     */
    public void initialize() throws UnknownHostException {
        this.macAddr = SessionProperties.clientMac;
        this.tMSGTimeOut = SessionProperties.msgTimeOut;
        this.tSessionInProgTimeout = SessionProperties.SESSION_IN_PROGRESS_TIME_OUT;
        this.tSessionInProgTime = tSessionInProgTimeout / 1000;
        this.messageRetryCount = SessionProperties.MESSAGE_RETRY_COUNT;
        this.sessionIdAssignor = SessionProperties.SESSION_ID_ASSIGNOR;
        this.resourceIdAssignor = SessionProperties.RESOURCE_ID_ASSIGNOR;
        this.maxForwardCount = SessionProperties.MAX_FORWARD_COUNT;
        this.dncsPort = SessionProperties.DNCS_PORT;
        int[] serverAddr = new int[4];
        int serverIP = SessionProperties.GS_IP;
        serverAddr[3] = (serverIP & 0x000000FF);
        byteServerAddr[3] = (byte) serverAddr[3];
        serverAddr[2] = ((serverIP >> 8) & 0x000000FF);
        byteServerAddr[2] = (byte) serverAddr[2];
        serverAddr[1] = ((serverIP >> 16) & 0x000000FF);
        byteServerAddr[1] = (byte) serverAddr[1];
        serverAddr[0] = ((serverIP >> 24) & 0x000000FF);
        byteServerAddr[0] = (byte) serverAddr[0];

        String dNCSAddress = SessionProperties.DNCS_IP;
        dncsIPObj = InetAddress.getByName(dNCSAddress);
        if (Log.DEBUG_ON) {
            StringBuffer buff = new StringBuffer("DSMCC Messenger.initialize()");
            buff.append("\nMAC: ");
            buff.append(StringUtil.byteArrayToHexString(macAddr));
            buff.append("\nMSG Timeout: ");
            buff.append(tMSGTimeOut);
            buff.append("\nSession In Progress Timeout (sec): ");
            buff.append(tSessionInProgTimeout);
            buff.append("\nMessage Retry Count: ");
            buff.append(messageRetryCount);
            buff.append("\nSession Id Assignor: ");
            buff.append(sessionIdAssignor);
            buff.append("\nResource Id Assignor: ");
            buff.append(resourceIdAssignor);
            buff.append("\nMax Forward Count: ");
            buff.append(maxForwardCount);
            buff.append("\nDNCS IP:");
            buff.append(dNCSAddress);
            buff.append("\nDNCS port:");
            buff.append(dncsPort);
            buff.append("\nDNCSIPObj: ");
            buff.append(dncsIPObj.toString());
            buff.append("\nGSIP: ");
            buff.append(serverAddr[0]).append('.');
            buff.append(serverAddr[1]).append('.');
            buff.append(serverAddr[2]).append('.');
            buff.append(serverAddr[3]);
            Log.printDebug(buff.toString());
        }
        if (datagramSocket != null) {
        	datagramSocket.shutdown();
        	datagramSocket = null;
        }
        if (datagramSocket == null) {
            try {
                datagramSocket = new DSMCCUDPSocket(dncsPort);
                datagramSocket.addDSMCCSocketEventListener(this);
            } catch (SocketException ex) {
                Log.printDebug("FATAL SOCKET Exception: " + ex.getMessage());
                ex.printStackTrace();
            }
        }

        timer = TVTimer.getTimer();
        
        if (timerSpec != null) {
        	timerSpec.removeTVTimerWentOffListener(this);
        	timer.deschedule(timerSpec);
        }

        timerSpec = new TVTimerSpec();
        timerSpec.setDelayTime(1000);
        timerSpec.setRepeat(true);
        timerSpec.addTVTimerWentOffListener(this);

        try {
            timerSpec = timer.scheduleTimerSpec(timerSpec);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // start();
    }

    /**
     * Sends a UDP packet to the SRM.
     *
     * @param msg
     *            The DSM-CC message stored as a byte array.
     * @param addr
     *            SRM net address
     * @param port
     *            SRM communication port
     * @throws IOException
     *             Should the socket fail.
     */
    private void send(byte[] msg, InetAddress addr, int port) throws IOException {
        datagramSocket.send(msg, addr, port);
    }

    /**
     * sends ClientSessionSetupRequest message. The message is sent to the SRM.
     *
     * @param sspId
     *            A unique id of Video. It is used as session name.
     * @return - Returns null if successful otherwise, an error String.
     */
    public String sessionRequest(String sspId) throws IOException {
        String rs = null;
        byte[] privateData = makePrivateData(sspId);
        if (privateData == null) {
        	Log.printDebug("makePrivateData returned null array");
            throw new IOException();
        }
        synchronized (this) {
        	rs = sessionSetup(sspId, privateData);
        }
        return rs;
    }

    /**
     * makes a part of the DSM-CC session messages in the UUData portion of the messages.
     *
     * @param sspId
     *            A unique id of Video. It is used as session name.
     * @return Returns Bytes array of UUData.
     */
    private byte[] makePrivateData(String sspId) {
        UserPrivateData userData = new UserPrivateData();

        int iAppServerIP = SessionProperties.GS_IP;
        String serviceName = SessionProperties.SERVICE_NAME;
        String serviceGW = SessionProperties.SERVICE_GW;
        userData.setServers(serviceGW, serviceName, iAppServerIP);
        byte[] serviceGroup = ServiceGroupController.getInstance().getServiceGroupData();
        if (serviceGroup == null) {
        	Log.printDebug("makePrivateData, serviceGroup is not configured.");
            return null;
        }
        byte[] setupPrivateData = userData.setSetUpUserData(sspId, serviceGroup);
        return setupPrivateData;
    }

    /**
     * Builds and sends a DSM-CC. ClientSessionSetupRequest message. The message is sent to the SRM.
     *
     * @param sessionName
     *            An ASCII name for this session.
     * @param data
     *            The private data section of the message as provided by the upper levels of code.
     * @return Returns null if successful otherwise, an error String.
     */
    public String sessionSetup(String sessionName, byte[] data) throws IOException {
        String rs = null;
        ClientSessionSetupRequest msg = new ClientSessionSetupRequest();
        AdaptationHeader adpHeader = new AdaptationHeader();
        msg.setAdaptationHeader(adpHeader);

        try {
            msg.setTransactionID();
            msg.setMAC(macAddr);
            msg.setServerIP(byteServerAddr);
            msg.setUserData(data);
        } catch (Exception ex) {
            return (ex.getMessage());
        }

        TransactionID tid = msg.getTransactionID();
        int itid = tid.getID();
        DSMCCSessionState sstate = new DSMCCSessionState(tMSGTimeOut, messageRetryCount, itid);
        sstate.setSessionName(sessionName);
        sstate.setMessage(msg);
        sessionIdAToNameHashB.putMessageHash(sessionName, msg.getSessionID(), sstate);

        send(msg.toByteArray(), dncsIPObj, dncsPort);

        // try {
        // send(msg.toByteArray(), dncsIPObj, dncsPort);
        // } catch (IOException ex) {
        // rs = ex.getMessage();
        // ex.printStackTrace();
        // }
        if (rs != null) {
            // Failure - remove state.
            sessionIdAToNameHashB.removeMessageHashWithArrayKey(msg.getSessionID());
        }
        int state = sstate.nextState(DSMCCSessionState.SENT_SETUP);
        if (rs == null) {
            rs = DSMCCSessionState.stateToString(state);
        }

//        final byte[] messageArr = msg.toByteArray();
//
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    try {
//                        Thread.sleep(10000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    send(messageArr, dncsIPObj, dncsPort);
//                } catch (IOException e) {
//
//                    e.printStackTrace();
//                }
//            }
//        }).start();

        return rs;
    }

    /**
     * sends ClientSessionReleaseRequest message.
     *
     * @param sspId
     *            A unique id of Video. It is used as session name.
     * @param reasonCode
     *            A reason code as defined by ISO/IEC 13818-6:1998(E) table 4-59.
     * @return Returns null if successful otherwise, an error String.
     */
    public String releaseRequest(String sspId, int reasonCode) {
        String rs = null;
        
        synchronized (this) {
        	rs = sessionRelease(sspId, reasonCode);
        }
        return rs;
    }

    /**
     * Builds and sends a DSMCC ClientSessionReleaseRequest message and starts the tracking.
     *
     * @param sessionName
     *            - The name of the session. (Vestigial)
     * @param reason
     *            - A reason code as defined by ISO/IEC 13818-6:1998(E) table 4-59.
     * @return Returns null if successful otherwise, an error String.
     */
    public String sessionRelease(String sessionName, int reason) {
        String result;
        ClientSessionReleaseRequest msg = new ClientSessionReleaseRequest();
        msg.setTransactionID();
        msg.setResponse((short) reason);
        byte[] sessionID = sessionIdAToNameHashB.getArrayGivenSessionName(sessionName);
        if (sessionID == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("sessionID : " + sessionID);
            }
            return null;
        }
        msg.setSessionID(sessionID, 0);
        if (Log.DEBUG_ON) {
            Log.printDebug("sessionID : " + StringUtil.byteArrayToHexString(sessionID));
        }
        try {
            if (Log.DEBUG_ON) {
                byte[] msgByteArray = msg.toByteArray();
            }
            send(msg.toByteArray(), dncsIPObj, dncsPort);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        DSMCCSessionState sstate = (DSMCCSessionState) sessionIdAToNameHashB.getStateGivenSessionName(sessionName);
        // Save for a resend if we have to.
        sstate.setMessage(msg);

        int state = sstate.nextState(DSMCCSessionState.SENT_RELEASE);
        result = DSMCCSessionState.stateToString(state);

        sessionIdAToNameHashB.putMessageHash(sessionName, sessionID, sstate);
        return result;
    }

    /**
     * Constructs and sends a UNClientSessionInProgress message ISO/IEC13818-61998(E) Section 2.4.14.1. Note that upon
     * sniffing the laboratory network, these messages were never seen. However, they are required under sspVersion 2.3
     * section 7.1
     *
     * @param sessions
     *            Vector containing all of the currently open session IDs
     */
    private void sendClientSessionInProgressReq(Vector sessions) {
        if (Log.DEBUG_ON) {
            Log.printDebug("sendClientSessionInProgressReq  sessions.size : " + sessions.size());
        }
        ClientSessionInProgress msg = new ClientSessionInProgress();
        msg.setSessions(sessions);
        byte[] bamsg = msg.toByteArray();

        try {
            send(bamsg, dncsIPObj, dncsPort);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Per sspVersion V2.3 section 7.1, this message is required of the client but optional for the server. When
     * received, it causes the timeout timer for a message response wait to be reset to zero. It signals from the DNCS
     * to the client that the DNCS is delayed in processing the request and that the client should wait a little longer.
     *
     * @param in
     *            The received byte array.
     */
    private void parseClientSessionProceedingIndication(byte[] in) {
        if (Log.DEBUG_ON) {
            Log.printDebug("parseClientSessionProceedingIndication ");
        }
        ClientSessionProceedingIndication msg = new ClientSessionProceedingIndication();

        msg.parse(in);
        // Need to reset the setup timeout timer.
        byte[] sessionID = msg.getSessionID();

        // Get the name of the session.
        String name = sessionIdAToNameHashB.getSessionNameGivenSessionArray(sessionID);

        // Get the session state of the session
        DSMCCSessionState ss = (DSMCCSessionState) sessionIdAToNameHashB.getStateGivenSessionName(name);
        // reset the timeout.
        ss.resetMessageTimeout();

    }

    /**
     * Parses and processes a ClientSessionSetupConfirm message from a raw byte array. Called by parseResponse
     *
     * @param raw
     *            The received byte array.
     */
    private void parseClientSessionSetupConfirm(byte[] raw) {

        ClientSessionSetupConfirm msg = new ClientSessionSetupConfirm();
        msg.parse(raw);

        byte[] sessID = msg.getSessionID();

        if (Log.DEBUG_ON) {
            Log.printDebug("parseClientSessionSetupConfirm - SessionID = "
                    + StringUtil.byteArrayToHexString(sessID));
        }

        DSMCCSessionState state = (DSMCCSessionState) sessionIdAToNameHashB.getStateGivenSessionIdArray(sessID);

        // If sessionID is not our sessionID then we don't know what the server
        // is talking about and should just drop the message.
        if (state == null) {
            Log.printError("ERROR - unknown TID " + msg.toString());
            return;
        }

        // Make an event to send to the listeners.
        DSMCCSessionEvent msgEvent = new DSMCCSessionEvent(this);

        int rc = msg.getResponse();
        String name = state.getSessionName();

        if (Log.DEBUG_ON) {
        	Log.printDebug("parseClientSessionSetupConfirm - name = " + name + ", rc = 0x" + Integer.toHexString(rc));
            MenuController.getInstance().debugList[3].add("parseClientSessionSetupConfirm, rc = 0x" + Integer.toHexString(rc));
        }

        msgEvent.setResponseCode(rc);
        // See SessionManagerImpl for full list of event codes this class
        // provides.
        if (rc != ResRspCode.RspOk) {
            // Something failed. Send back failure response.
            msgEvent.setEventType(SessionEventType.SETUP_FAILED);
            
            // No more need for state info on this session. Caller
            // retains high-level state information.
            // Removing from hash stops timer based messages.
            sessionIdAToNameHashB.removeMessageHashBySessionName(name);
        } else {
            msgEvent.setEventType(SessionEventType.SETUP_SUCCEEDED);
        }
        
        msgEvent.setSessionName(name.getBytes());
        // Fill in the private data for listeners in case they want it.
        msgEvent.setPrivateData(msg.getResourceAndPrivateDataBytes());

        // VDTRMASTER-6051
        int nstate = state.nextState(DSMCCSessionState.RECEIVED_SETUP_CONF);
        if (Log.DEBUG_ON) {
            Log.printDebug("CurrentState = " + DSMCCSessionState.stateToString(nstate));
        }
        sendEvents(msgEvent);

    }

    /**
     * This message shows up when there has been a problem and the server has torn down the session.
     *
     * @param rawmsg
     *            Raw data bytes received from the socket.
     */
    private void parseClientSessionReleaseIndication(byte[] rawmsg) {
        ClientSessionReleaseIndication msg = new ClientSessionReleaseIndication();
        msg.parse(rawmsg);
        int rc = msg.getReason();
        byte[] sessID = msg.getSessionID();

        if (Log.DEBUG_ON) {
            Log.printDebug("parseClientSessionReleaseIndication - SessionID = "
                    + StringUtil.byteArrayToHexString(sessID));
            Log.printDebug("parseClientSessionReleaseIndication - Reason = "
                    + ReasonCodes.reasonCodeToString(rc));
        }

        DSMCCSessionState state = (DSMCCSessionState) sessionIdAToNameHashB.getStateGivenSessionIdArray(sessID);

        // If sessionID is not our sessionID then we don't know what the server
        // is
        // talking about and should just drop the message.

        if (state == null) {
            Log.printDebug("No Session State at DSMCC Level");
            return;
        }

        int nstate = state.nextState(DSMCCSessionState.RECEIVED_SERVER_TEARDOWN_MSG);

        if (Log.DEBUG_ON) {
            Log.printDebug("CurrentState = " + DSMCCSessionState.stateToString(nstate));
        }

        // Make an event to send to the listeners.
        DSMCCSessionEvent msgEvent = new DSMCCSessionEvent(this);

        String name = state.getSessionName();
        msgEvent.setResponseCode(rc);
        msgEvent.setEventType(SessionEventType.RELEASE_REQUIRED);
        msgEvent.setSessionName(name.getBytes());

        // Fill in the private data for listeners in case they want it.
        msgEvent.setPrivateData(msg.getUserData());

        sendEvents(msgEvent);

        // Send a response back to the server saying that we've taken the
        // session down.

        sendUNClientSessionReleaseResponse(msg);

    }

    /**
     * In response to a UNClientSessionReleaseIndication this message is sent.
     *
     * @param msg
     *            The UNClientSessionReleaseIndication object.
     */
    private void sendUNClientSessionReleaseResponse(ClientSessionReleaseIndication msg) {
        ClientSessionReleaseResponse outmsg = new ClientSessionReleaseResponse();
        outmsg.setResponse(ReasonCodes.RSN_OK);
        outmsg.setSessionID(msg.getSessionID(), 0);
        outmsg.setTransactionID(msg.getTransactionID());
        outmsg.setUUData(msg.getUserData());

        try {
            send(outmsg.toByteArray(), dncsIPObj, dncsPort);
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.printDebug("sendUNClientSessionReleaseResponse IO Exception");
        }
    }

    /**
     * Parses a raw byte array received from the SRM into a UN-ClientReleaseConfirm message and processes it.
     *
     * @param in
     *            The byte array from the SRM listener socket.
     */
    private void parseClientReleaseConfirm(byte[] in) {
        if (Log.DEBUG_ON) {
            Log.printDebug("parseClientReleaseConfirm");
        }
        ClientSessionReleaseConfirm msg = new ClientSessionReleaseConfirm();
        msg.parse(in);

        byte[] sessID = msg.getSessionID();
        DSMCCSessionState state = (DSMCCSessionState) sessionIdAToNameHashB.getStateGivenSessionIdArray(sessID);

        // The caller other side of the API uses the Session Name string to
        // reference sessions.
        String sessionName = sessionIdAToNameHashB.getSessionNameGivenSessionArray(sessID);

        if (Log.DEBUG_ON) {
            Log.printDebug(msg.toString());
        }

        if (state == null) {
            // Problems! We have lost track of this transaction. Dump it.
            Log.printDebug("ERROR Client Release msg: " + msg.toString());
            return;
        }

        int nstate = state.nextState(DSMCCSessionState.RECEIVED_RELEASE_CONF);

        if (Log.DEBUG_ON) {
            Log.printDebug("CurrentState = " + DSMCCSessionState.stateToString(nstate));
        }

        // Make an event to send to the listeners.
        DSMCCSessionEvent msgEvent = new DSMCCSessionEvent(this);
        msgEvent.setSessionName(sessionName.getBytes());
        int rc = msg.getResponse();
        msgEvent.setResponseCode(rc);

        // See SessionManagerImpl for full list of event codes this class
        // provides.
        if (rc != ResRspCode.RspOk) {
            // Something failed. Send back failure response.
            msgEvent.setEventType(SessionEventType.RELEASE_FAILED);
        } else {
            msgEvent.setEventType(SessionEventType.RELEASE_SUCCEEDED);
        }

        msgEvent.setPrivateData(msg.getResourceAndPrivateDataBytes());
        // Remove from hash
        if (Log.DEBUG_ON) {
            Log.printDebug("parseClientReleaseConfirm : " + sessID);
        }
        sessionIdAToNameHashB.removeMessageHashWithArrayKey(sessID);
        // Notify listeners.
        sendEvents(msgEvent);

    }

    /**
     * Processes the raw byte array received from the SRM listener socket.
     *
     * @param rawmsg
     *            The received byte array.
     */
    private void parseResponse(byte[] rawmsg) {
        // Verify that this is a good DSM-CC message
        if (rawmsg == null || rawmsg[0] != DSMCCMessage.PROTOCOL_DISCRIMINATOR) {
            Log.printWarning("Bad raw message. " + StringUtil.byteArrayToHexString(rawmsg));
            return;
        }

        if (rawmsg[1] == DSMCCMessage.UN_SESSION_MESSAGE) {
            switch (getMessageID(rawmsg)) {

            case SessionMessage.CLIENT_SESSION_SET_UP_CONFIRM:
                parseClientSessionSetupConfirm(rawmsg);
                break;

            case SessionMessage.CLIENT_SESSION_RELEASE_CONFIRM:
                parseClientReleaseConfirm(rawmsg);
                break;

            case SessionMessage.CLIENT_SESSION_PROCEEDING_IND:
                parseClientSessionProceedingIndication(rawmsg);
                break;

            case SessionMessage.CLIENT_SESSION_RELEASE_INDICATION:
                parseClientSessionReleaseIndication(rawmsg);
                break;

            default:

                break;

            }
        } else {
            // Message we currently don't handle
            Log.printDebug("Received unknown message type:" + StringUtil.byteArrayToHexString(rawmsg));
        }

    }

    /**
     * Pulls an MPEG message ID out of an MPEG DSM-CC message.
     *
     * @param mpegFrame
     *            MPEG DSM-CC message
     * @return MPEG message ID
     */
    private short getMessageID(byte[] mpegFrame) {
        short rc = mpegFrame[2];
        rc <<= 8;
        rc += mpegFrame[3];
        return rc;
    }

    /**
     * Starts the listener threads.
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("DSMCCMessenger start called ");
        }

        if (datagramSocket != null) {
            datagramSocket.start();
            socketThread = new Thread(datagramSocket, "DSMCCListenerThread");
            socketThread.setPriority(Thread.MAX_PRIORITY);
            socketThread.start();
        }
    }

    /**
     *Shuts down and cleans up DSMCC messaging.
     */
    public void shutdown() {
        if (Log.DEBUG_ON) {
            Log.printDebug("DSMCCMessenger shutdown called!");
        }
        // datagramSocket.removeDSMCCSocketEventListener(this);
        if (datagramSocket != null) {
            datagramSocket.shutdown();
        }
        if (datagramSocket != null) {
            socketThread.interrupt();
        }

        // Wait a second for it to die.
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ie) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // Empty the maps.
        sessionIdAToNameHashB.clear();

        if (timerSpec != null) {
            // Shut down the timer
            timerSpec.removeTVTimerWentOffListener(this);
            try {
                timer.deschedule(timerSpec);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            timerSpec = null;
        }

    }

    /**
     * Distribute events to any listeners.
     *
     * @param event
     *            The DSMCCSessionEvent to distribute.
     */
    private synchronized void sendEvents(DSMCCSessionEvent event) {
        for (Enumeration e = listenerList.elements(); e.hasMoreElements();) {
            DSMCCSessionEventListener dl = (DSMCCSessionEventListener) e.nextElement();
            dl.receiveSRMEvent(event);
        }
    }

    /**
     * Adds a listener to the distribution list.
     *
     * @param de
     *            The listener to add.
     */
    public synchronized void addDSMCCSessionEventListener(DSMCCSessionEventListener de) {
        listenerList.addElement(de);
    }

    /**
     * Removes a listener from the list.
     *
     * @param de
     *            The listener to remove
     */
    public synchronized void removeDSMCCSessionEventListener(DSMCCSessionEventListener de) {
        listenerList.removeElement(de);
    }

    /**
     * This listener is used to update the communications timeout timer.
     *
     * @see implements javax.tv.util.TVTimerWentOffListener.timerWentOff
     * @param e
     *            TVTimerWentOffEvent
     */
    public void timerWentOff(TVTimerWentOffEvent e) {
        if (e.getTimerSpec() != timerSpec) {
            return;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("SessionConnectorImpl timerWentOff, timerSpec = " + timerSpec);
        }
        DSMCCSessionState state;
        Vector sessionIDs = new Vector();

        for (Enumeration en = sessionIdAToNameHashB.elements(); en.hasMoreElements();) {
            state = (DSMCCSessionState) en.nextElement();
            int action = state.updateTimer();

            if ((action & DSMCCSessionState.TIMER_RESEND) == DSMCCSessionState.TIMER_RESEND) {
                DSMCCMessage msg = state.getMessage();

                if (Log.DEBUG_ON) {
                    Log.printDebug("Timer - Resending message");
                }

                // Reload the timer to prevent coming here over and over.
                state.resetMessageTimeout();

                try {
                    send(msg.toByteArray(), dncsIPObj, dncsPort);
                } catch (IOException ex) {
                    Log.printDebug(ex.getMessage());
                    ex.printStackTrace();
                }

            }
            // Number of retries has been exceeded. Give up on this session
            // attempt
            if ((action & DSMCCSessionState.TIMER_FAIL) == DSMCCSessionState.TIMER_FAIL) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Timer - Giving up on session establishment");
                }

                // Make an event to send to the listeners.
                DSMCCSessionEvent msgEvent = new DSMCCSessionEvent(this);

                String name = state.getSessionName();
                // Something failed. Send back failure response.
                msgEvent.setEventType(SessionEventType.SETUP_TIMEDOUT);
                // No more need for state info on this session. Caller
                // retains high-level state information.
                // Removing from hash stops timer based messages.
                sessionIdAToNameHashB.removeMessageHashBySessionName(name);
                msgEvent.setSessionName(name.getBytes());
                sendEvents(msgEvent);
                state.nextState(DSMCCSessionState.RECEIVED_SETUP_CONF);
            }

            // A timeout of zero indicates no timeout - wait forever.
            if (state.getState() == DSMCCSessionState.SESSION_ACTIVE && tSessionInProgTime <= 0) {
                SessionMessage umsg = (SessionMessage) state.getMessage();
                byte[] sessionID = umsg.getSessionID();
                sessionIDs.addElement(sessionID);
                // Restart the in progress timer
                tSessionInProgTime = tSessionInProgTimeout / 1000;
            }
        }
        tSessionInProgTime--;
        // If there are active sessions, send out the watchdog timeout.
        if (!sessionIDs.isEmpty()) {
            sendClientSessionInProgressReq(sessionIDs);
        }
    }

    /**
     * receiveDSMCCEvent A message has been received from the SRM.
     *
     * @param msg
     *            A message has been received from the SRM
     */
    public void receiveDSMCCEvent(byte[] msg) {
        if (Log.DEBUG_ON) {
            Log.printDebug("receiveDSMCCEvent");
        }
        synchronized (this) {
        	parseResponse(msg);
        }
    }

    /**
     * Translate a String session name into an array[10] SRM name.
     *
     * @param sessID
     *            - The BMS name of the session
     * @return the 10 byte SRM name of the session.
     */
    public byte[] getSessionID(String sessID) {
        return sessionIdAToNameHashB.getArrayGivenSessionName(sessID);
    }

}

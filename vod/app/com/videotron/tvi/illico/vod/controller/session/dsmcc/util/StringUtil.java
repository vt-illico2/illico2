package com.videotron.tvi.illico.vod.controller.session.dsmcc.util;

import java.util.Vector;

/**
 *StringUtil.
 */

public class StringUtil

{

    public static String[] tokenize(String str, String delim) {
        Vector v = new Vector();
        String s = str;
        int p;
        while ((p = s.indexOf(delim)) != -1) {
            if (p == 0) {
                v.addElement("");
            } else {
                v.addElement(s.substring(0, p));
            }
            s = s.substring(p + 1);
        }
        v.add(s);
        String[] ret = new String[v.size()];
        ret = (String[]) v.toArray(ret);
        return ret;
    }

    public static String byteArrayToHexString(byte[] array) {
        StringBuffer buf = new StringBuffer();
        String tmp2 = "";
        try {
            if (array == null) {
                return "NULL ARRAY";
            }
            int count = 0;
            buf.append("\n");

            for (int i = 0; i < array.length; i++) {
                count++;
                int num = array[i];
                num &= 0xFF;
                if (num > 47 && num < 127) {
                    tmp2 += (char) num;
                } else {
                    tmp2 += ".";
                }
                String tmp = Integer.toHexString(num).toUpperCase();
                if (tmp != null && tmp.length() == 1) {
                    tmp = "0" + tmp;
                }
                buf.append(tmp);
                buf.append(" ");
                if (count == 16) {
                    buf.append(":  " + tmp2);
                    buf.append("\n");
                    count = 0;
                    tmp2 = "";
                }
            }
            if (tmp2 != null && tmp2.length() > 0) {
                buf.append(":  " + tmp2);
                buf.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return buf.toString();
    }

    /**
     * 
     * intIPtoString, Utility to convert an integer representation of an IP address to a string.
     * 
     * @param ip
     * @return String representation of IP address. i.e. "192.168.132.1"
     */

    public static String intIPtoString(int ip)

    {

        int convArray[] = new int[4];

        convArray[0] = ip & 0x000000FF;

        convArray[1] = (ip >> 8) & 0x000000FF;

        convArray[2] = (ip >> 16) & 0x000000FF;

        convArray[3] = (ip >> 24) & 0x000000FF;

        StringBuffer sbAddress = new StringBuffer();

        sbAddress.append(convArray[3]);

        sbAddress.append(".");

        sbAddress.append(convArray[2]);

        sbAddress.append(".");

        sbAddress.append(convArray[1]);

        sbAddress.append(".");

        sbAddress.append(convArray[0]);

        return sbAddress.toString();

    }

    /**
     * 
     * StringIPtoInt ======== Convert a dotted decimpal string representation of an IP address to an integer.
     * 
     * @param ip
     * @return
     */

    public static int stringIPtoInt(String ip) {

        int rc = 0;
        char[] data = ip.toCharArray();
        for (int i = 0; i < data.length; i++) {
            char c = data[i];
            int b = 0;
            while (c != '.') {
                // Apply Cramer's rule for building a decimal int.
                b = b * 10 + c - '0';
                if (++i >= data.length) {
                    break;
                }
                c = data[i];
            }
            rc = (rc << 8) + b;
        }
        return rc;
    }

}

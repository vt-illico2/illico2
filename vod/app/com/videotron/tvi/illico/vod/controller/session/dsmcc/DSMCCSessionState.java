package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import com.videotron.tvi.illico.log.Log;

/**
 * DSMCCSessionState.
 */
public class DSMCCSessionState {
    /**
     * Session States.
     */

    public static final int IDLE = 0;

    public static final int SENT_SETUP_WAITING = 1;

    public static final int SESSION_ACTIVE = 2;

    public static final int SENT_RELEASE_WAITING = 3;

    public static final int MAX_STATE = 4;

    public static final int ILLEGAL_STATE = 0xFF;

    /**
     * Session Events.
     */

    public static final int SENT_SETUP = 0;

    public static final int RECEIVED_SETUP_CONF = 1;

    public static final int SENT_RELEASE = 2;

    public static final int RECEIVED_RELEASE_CONF = 3;

    public static final int RECEIVED_SETUP_FAIL = 4;

    public static final int RECEIVED_SERVER_TEARDOWN_MSG = 5;

    /**
     * Timer Update event flags.
     */
    public static final int TIMER_IDLE = 0;

    public static final int TIMER_RESEND = 1;

    public static final int TIMER_FAIL = 2;

    /**
     * Q to Q+ Map Rows are by current state. Columns by event.
     */

    private static final int NEXT_STATE[][] =

    { // SENT_SETUP RECEIVE CONF SENT RELEASE RECEIVE REL CONF
            // RECEIVED_SETUP_FAIL, RECEIVED_SERVER_TEARDOWN_MSG
            /* IDLE */
            { SENT_SETUP_WAITING, ILLEGAL_STATE, ILLEGAL_STATE, IDLE, IDLE, IDLE },
            /* SENT_SETUP_WAITING */
            { SENT_SETUP_WAITING, SESSION_ACTIVE, ILLEGAL_STATE, ILLEGAL_STATE, IDLE, IDLE },
            /* SESSION_ACTIVE */
            { ILLEGAL_STATE, ILLEGAL_STATE, SENT_RELEASE_WAITING, ILLEGAL_STATE, ILLEGAL_STATE, IDLE },
            /* SENT_RELEASE_WAITING */
            { ILLEGAL_STATE, ILLEGAL_STATE, SENT_RELEASE_WAITING, IDLE, ILLEGAL_STATE, IDLE } };

    private int state = IDLE;

    private int responseTimeoutReload;

    private int responseTimeout;

    private int retryCountReload;

    private int retryCount;

    private int trackingID;

    private int tid;

    private DSMCCMessage msg;

    private String sessionName;

    /**
     * Default constructor.
     */
    public DSMCCSessionState() {

    }

    /**
     * Constructor which initializes the response timeout, retry count, and transaction ID.
     *
     * @param rspTimeout
     *            Number of milliseconds until a retry is sent.
     * @param retryCnt
     *            Number of times to retry
     * @param tid
     *            Transaction ID for the last message.
     */
    public DSMCCSessionState(int rspTimeout, int retryCnt, int tid) {
        responseTimeout = rspTimeout / 1000;
        responseTimeoutReload = rspTimeout / 1000;
        retryCount = retryCnt;
        retryCountReload = retryCnt;
        this.tid = tid;
    }

    /**
     *
     * nextState ,Calculate the next state.
     *
     * @param event
     *            State machine input.
     * @return
     */
    public int nextState(int event) {
        int length = NEXT_STATE[state].length;
        int currentState = state;

        if (event < length) {
            state = NEXT_STATE[state][event];
        } else {
            state = ILLEGAL_STATE;
        }

        if (Log.DEBUG_ON) {
            StringBuffer sb = new StringBuffer("DSMCC State Transition - Q = ");
            sb.append(stateToString(currentState));
            sb.append(" event = ");
            sb.append(eventToString(event));
            sb.append(" Q+ = ");
            sb.append(stateToString(state));
            Log.printDebug(sb.toString());
        }

        return state;
    }

    /**
     *
     * getState ,Retrieve the current state.
     *
     * @return current state.
     */
    public int getState() {
        return state;
    }

    /**
     *
     * setMessage ,Store the message just sent.
     *
     * @param msg
     */
    public void setMessage(DSMCCMessage msg) {
        // TODO - This is currently not used but needs to be.
        this.msg = msg;
    }

    /**
     *
     * getMessage ,return the last message sent.
     *
     * @return last message sent
     */
    public DSMCCMessage getMessage() {
        return msg;
    }

    /**
     *
     * updateTimer ,Receives a timer event and handles response timeouts, retries by forwarding the state.
     *
     * @return
     */
    public int updateTimer() {
        int rc = TIMER_IDLE;

        // 0 indicates the timer is off.
        if (responseTimeout != 0) {
            if (responseTimeout == 1) {
                switch (state) {
                case SENT_SETUP_WAITING:
                case SENT_RELEASE_WAITING:

                    if (retryCount > 0) {
                        retryCount--;
                        rc |= TIMER_RESEND;
                    } else {
                        // We've exhausted the number of retries and still
                        // haven't
                        // gotten a response. Send a failure back.
                        rc |= TIMER_FAIL;
                    }

                    break;

                default:
                    retryCount = retryCountReload;
                    break;
                }
            }
            responseTimeout--;
        }

        return rc;
    }

    /**
     *
     * sessionName ,Setter for session name. The session name is how the VodHandler refers to sessions. It knows
     * nothing of session IDs or states. Called by DSMCCMessenger parseClientSessionSetupConfirm
     *
     * @return the Session Name
     */
    public String getSessionName() {
        return sessionName;
    }

    /**
     *
     * setSessionName ,setter for the session name. The session name is how the VodHandler refers to sessions.
     * It knows nothing of session IDs or states. Called by DSMCCMessenger sessionSetup
     *
     * @param name
     */
    public void setSessionName(String name) {
        sessionName = name;
    }

    /**
     *
     * resetMessageTimeout ,Resets the response message timeout timer. This is usually due to the receipt of a
     * message from the SRM. Called by DSMCCMessenger parseClientSessionProceedingIndication.
     */
    public void resetMessageTimeout() {
        responseTimeout = responseTimeoutReload;
    }

    /**
     *
     * stateToString ,Convert state variable to a printable string.
     *
     * @param state
     * @return String version of the state variable.
     */
    public static String stateToString(int state) {
        switch (state) {
        case IDLE:
            return "IDLE";

        case SENT_SETUP_WAITING:
            return "SENT_SETUP_WAITING";

        case SESSION_ACTIVE:
            return "SESSION_ACTIVE";

        case SENT_RELEASE_WAITING:
            return "SENT_RELEASE_WAITING";

        case MAX_STATE:
            return "MAX_STATE";

        case ILLEGAL_STATE:
            return "ILLEGAL_STATE";

        default:
            return "REALLY ILLEGAL STATE: " + state;

        }
    }

    /**
     *
     * eventToString ,Convert a DSMCC message event to a human readable String.
     *
     * @param event
     *            as defined in DSMCCSessionState
     * @return - A human readable String representing the event.
     */
    public static String eventToString(int event) {
        switch (event) {
        case SENT_SETUP:
            return "SENT_SETUP";

        case RECEIVED_SETUP_CONF:
            return "RECEIVED_SETUP_CONF";

        case SENT_RELEASE:
            return "SENT_RELEASE";

        case RECEIVED_RELEASE_CONF:
            return "RECEIVED_RELEASE_CONF";

        case RECEIVED_SETUP_FAIL:
            return "RECEIVED_SETUP_FAIL";

        case RECEIVED_SERVER_TEARDOWN_MSG:
            return "RECEIVED_SERVER_TEARDOWN_MSG";

        default:
            return "ILLEGAL EVENT";
        }
    }
}

package com.videotron.tvi.illico.vod.controller.session.dsmcc.util;

import java.util.StringTokenizer;
import java.util.Vector;

import javax.tv.service.selection.PresentationTerminatedEvent;
import javax.tv.service.selection.SelectionFailedEvent;
import javax.tv.service.selection.ServiceContextEvent;

import org.ocap.service.AlternativeContentErrorEvent;

public class StringUtils {
    
    /**
     * Returns a String with all occurrences of <code>from</code> within <code>orig</code> replaced with <code>to</code>
     * . If <code>orig</code> contains no occurrences of <code>from</code>, or if <code>from</code> is equal to
     * <code>to</code>, <code>orig</code> itself is returned rather than a copy being made. None of the parameters
     * should be null.
     * 
     * @param orig
     *            the original String. Must not be null.
     * @param from
     *            the String to replace within <code>orig</code>. Must not be null.
     * @param to
     *            the String to replace <code>from</code> with. Must not be null.
     * 
     * @returns a version of <code>orig</code> with all occurrences of <code>from</code> being replaced with
     *          <code>to</code>.
     * 
     */
    public static String replace(String orig, String from, String to) {
        int fromLength = from.length();

        if (fromLength == 0) {
            throw new IllegalArgumentException("String to be replaced must not be empty");
        }            

        int start = orig.indexOf(from);
        if (start == -1) {
            return orig;
        }

        boolean greaterLength = (to.length() >= fromLength);

        StringBuffer buffer;
        // If the "to" parameter is longer than (or
        // as long as) "from", the final length will
        // be at least as large
        if (greaterLength) {
            if (from.equals(to)) {
                return orig;
            }                
            buffer = new StringBuffer(orig.length());
        } else {
            buffer = new StringBuffer();
        }

        char[] origChars = orig.toCharArray();

        int copyFrom = 0;
        while (start != -1) {
            buffer.append(origChars, copyFrom, start - copyFrom);
            buffer.append(to);
            copyFrom = start + fromLength;
            start = orig.indexOf(from, copyFrom);
        }
        buffer.append(origChars, copyFrom, origChars.length - copyFrom);

        return buffer.toString();
    }

    /** 
     * converts a char to a HEX value two character string. ie char:0 = "00" char:255 = "FF" 
     * 
     */
    public static String getHexStringCHAR(char c) {
        String str = "";

        // handle the case of -1
        if (c == -1) {
            str = "FF";
            return str;
        }

        if (c < 16) {
            // pad out string to make it look nice
            str = "0";
        }

        str += (Integer.toHexString((int) c)).toUpperCase();
        return str;
    }

    /**
     * converts a char to a HEX value two character string. ie char:0 = "00" char:255 "FF"
     */
    public static String getHexStringBYTE(byte b) {
        String str = "";

        // handle the case of -1
        if (b == -1) {
            str = "FF";
            return str;
        }

        int value = (((int) b) & 0xFF);

        if (value < 16) {
            // pad out string to make it look nice
            str = "0";
        }

        str += (Integer.toHexString(value)).toUpperCase();
        return str;
    }

    /**
     * converts a int to a HEX value two character string. ie int:0 = "0000" int:65356 = "FFFF"
     */
    public static String getHexStringWORD(short w) {
        String str = "";

        // handle the case of -1
        if (w == -1) {
            str = "FFFF";
            return str;
        }

        int value = (((int) w) & 0xFFFF);

        if (value < 16) {
            // pad out string to make it look nice
            str = "000";
        } else if (value < 256) {
            // pad out string to make it look nice
            str = "00";
        } else if (value < 4096) {
            // pad out string to make it look nice
            str = "0";
        }

        str += (Integer.toHexString(value)).toUpperCase();
        return str;
    }

    /**
     * converts a int to a HEX long into two-byte character string. ie 0=="00000000" -1=="FFFFFFFF"
     */
    public static String getHexStringDWORD(int dw) {
        String str = "";

        // handle the case of -1
        if (dw == -1) {
            str = "FFFFFFFF";
            return str;
        }

        long value = (((long) dw) & 0xFFFFFFFFL);
        if (value < 0) {
            System.out.println("failed!!!:" + value);
        }

        if (value < 16) {
            // pad out string to make it look nice
            str = "0000000";
        } else if (value < 256) {
            // pad out string to make it look nice
            str = "000000";
        } else if (value < 4096) {
            // pad out string to make it look nice
            str = "00000";
        } else if (value < 65536) {
            // pad out string to make it look nice
            str = "0000";
        } else if (value < 1048576) {
            // pad out string to make it look nice
            str = "000";
        } else if (value < 16777216) {
            // pad out string to make it look nice
            str = "00";
        } else if (value < 268435456) {
            // pad out string to make it look nice
            str = "0";
        }

        str += (Long.toHexString(value)).toUpperCase();
        return str;
    }

    public static String getHexStringBYTEArray(byte[] data) {
        if (data == null) {
            return "";
        }
        return getHexStringBYTEArray(data, 0, data.length);
    }

    /**
     * returns a String from the byte array
     */
    public static String getStringBYTEArray(byte[] data) {
        return getStringBYTEArrayOffset(data, 0);
    }

    /**
     * returns a String from the byte array
     */
    public static String getStringBYTEArrayOffset(byte[] data, int offset) {
        if (data == null) {
            return "";
        }

        StringBuffer strBf = new StringBuffer();;
        for (int x = 0 + offset; x < data.length; x++) {
            if (data[x] == 0) {
                return strBf.toString();
            }
            strBf.append((char) data[x]);
        }
        return strBf.toString();
    }

    /**
     * returns a String from the byte array
     */
    public static byte[] getBYTEArrayString(String str) {
        if (str == null) {
            return new byte[0];
        }

        byte[] data = new byte[str.length()];
        for (int x = 0; x < str.length(); x++) {
            data[x] = (byte) str.charAt(x);
        }
        return data;
    }

    public static String getHexStringBYTEArray(byte[] data, int offset, int length) {
        if (data == null) {
            return "";
        }

        StringBuffer buff = new StringBuffer(4096);
        // String str = new String();
        for (int i = offset; i < offset + length; i++) {
            // int val = data[i];
            // str += getHexStringBYTE(data[i]);
            buff.append(getHexStringBYTE(data[i]));
            if (i != data.length - 1) {
                buff.append(" ");
            }
        }
        return buff.toString();
    }

    private static int traverseString(String hexString, byte[] data) {
        int ct = 0;

        hexString = hexString.trim();
        // System.out.println("traverseString: '" + str + "'");
        StringBuffer numStr = new StringBuffer();;
        while (!hexString.equals("")) {
            int i = 0;
            numStr = new StringBuffer();

            // try to build up value
            try {
                char c = hexString.charAt(i);
                i++;
                while (c != ' ' && c != '\t') {
                    numStr.append(c);
                    c = hexString.charAt(i);
                    i++;
                }
            } catch (Exception e) {
                // this is okay. really...
            }

            int val = Integer.parseInt(numStr.toString(), 16);

            if (data != null) {
                try {
                    data[ct] = (byte) val;
                } catch (Exception e) {
                    // failed to save value into data array.
                    // most likely, the array isn't big enough
                    return -1;
                }
            }

            hexString = hexString.substring(i, hexString.length());
            hexString = hexString.trim();
            ct++;
        }

        return ct;
    }

    /**
     * Converts a String of the format "00 11 FF" to a byte array: { 00, 17 , 255 }
     */
    public static byte[] getBYTEArrayFromHexString(String hexString) {
        // call it first to determine the size the array should be
        int dataCt = traverseString(hexString, null);
        byte[] data = new byte[dataCt];
        // now do it again putting the data into the array
        traverseString(hexString, data);

        return data;
    }

    /**
     * convert time in ms into user-friendly string
     */
    public static String getTimeString(long ms) {
        String str = "";

        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;

        long s = (int) (ms / 1000);

        if (s == 0) {
            str = "0 seconds";
            return str;
        }

        if (s >= 60 * 60 * 24) {
            days = (int) (s / (60 * 60 * 24));
            s -= days * 60 * 60 * 24;
        }

        if (s >= 60 * 60) {
            hours = (int) (s / (60 * 60));
            s -= hours * 60 * 60;
        }

        if (s >= 60) {
            minutes = (int) (s / 60);
            s -= minutes * 60;
        }

        seconds = (int) s;

        // now build the string

        if (days > 1) {
            str += days + " days ";
        } else if (days > 0) {
            str += days + " day ";
        }

        if (hours > 1) {
            str += hours + " hours ";
        } else if (hours > 0) {
            str += hours + " hour ";
        }

        if (minutes > 1) {
            str += minutes + " minutes ";
        } else if (minutes > 0) {
            str += minutes + " minute ";
        }

        if (seconds > 1) {
            str += seconds + " seconds ";
        } else if (seconds > 0) {
            str += seconds + " second ";
        }

        // remove the trailing space
        if (str.length() > 0) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    /**
     * convert time in ms into user-friendly string
     */
    public static String getTimeWithMsString(long ms) {
        String str = "";
        if (ms >= 1000) {
            str = getTimeString(ms) + " ";
        }
        int milliseconds = (int) (((float) ms) % 1000);

        if (milliseconds == 1) {
            str += milliseconds + " millisecond";
        } else {
            str += milliseconds + " milliseconds";
        }

        return str;
    }

    /**
     * parse a line.
     * 
     * @param aLine
     * @param delimiter
     * @return parsed field array
     */
    public static String[] parseStringLine(String aLine, String delimiter) {
        StringTokenizer st = new StringTokenizer(aLine, delimiter, true);
        Vector rc = new Vector();
        String lastToken = delimiter;

        while (st.hasMoreTokens()) {
            String aToken = st.nextToken();
            if (aToken == null) {
                rc.addElement("");
            } else if (aToken.equals(delimiter)) {
                if (lastToken.equals(delimiter)) {
                    rc.addElement("");
                }                    
            } else {
                rc.addElement(aToken);
            }
            lastToken = aToken;
        }

        // Create string array and populate it
        String[] retstr = new String[rc.size()];
        for (int j = 0; j < rc.size(); j++) {
            retstr[j] = (String) rc.elementAt(j);
        }
        return retstr;

    } 

    /**
     * Converts a MAC address in long integer format to one in string format.
     * 
     * @param macAddress
     *            The long integer value to be converted to a MAC address hex string.
     * @return The hex string representing the MAC address.
     */
    public static String macAddressToString(long macAddress) {
        char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', };
        StringBuffer sb = new StringBuffer("00:00:00:00:00:00");

        // Convert to byte array
        byte[] b = new byte[6];
        b[0] = (byte) (macAddress >> 40);
        b[1] = (byte) (macAddress >> 32);
        b[2] = (byte) (macAddress >> 24);
        b[3] = (byte) (macAddress >> 16);
        b[4] = (byte) (macAddress >> 8);
        b[5] = (byte) (macAddress);

        for (int i = 5; i >= 0; i--) {
            char high = hexChar[(b[i] & 0xf0) >>> 4];
            char low = hexChar[(b[i] & 0x0f)];

            int idx = i * 3;
            sb.setCharAt(idx + 1, low);
            sb.setCharAt(idx, high);
        }

        return sb.toString();
    } // macAddressToString()

    /**
     * Convert a MAC address in hex string format to one in long integer format. Permits upper or lower case hex.
     * 
     * @param s
     *            The hex string representing the MAC address; eg, "00:0A:73:3C:27:C5". It must be formed only of digits
     *            0-9, A-F or a-f, and :. No spaces, minus or plus signs are allowed.
     * @return corresponding long integer value.
     * @throws IllegalArgumentException
     */
    public static long macAddressFromString(String s) {
        long macAddress = 0L;
        try {
            int stringLength = s.length();
            byte[] b = new byte[(stringLength + 1) / 3];

            for (int i = 0, j = 0; i < stringLength; i += 3, j++) {
                int high = charToNibble(s.charAt(i));
                int low = charToNibble(s.charAt(i + 1));
                b[j] = (byte) ((high << 4) | low);
            }

            // Convert from byte array
            macAddress = ((b[0] & 0xFFL) << 40) | ((b[1] & 0xFFL) << 32) | ((b[2] & 0xFFL) << 24)
                    | ((b[3] & 0xFFL) << 16) | ((b[4] & 0xFFL) << 8) | (b[5] & 0xFFL) & 0x0000FFFFFFFFFFFFL;
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    "StringUtils.macAddressFromString() equires rg like 00:0A:73:3C:27:C5 not " + s);
        }

        return macAddress;
    } // macAddressFromString()

    public static byte[] macAddressFromStringToByte(String s) {
        System.out.println("macAddressFromStringToByte  : " + s);
        byte[] bMacAddr = null;
        try {
            int stringLength = s.length();
            bMacAddr = new byte[(stringLength + 1) / 3];

            for (int i = 0, j = 0; i < stringLength; i += 3, j++) {
                int high = charToNibble(s.charAt(i));
                int low = charToNibble(s.charAt(i + 1));

                bMacAddr[j] = (byte) ((high << 4) | low);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    "StringUtils.macAddressFromStringToByte() equires rg like 00:0A:73:3C:27:C5 not " + s);
        }
        return bMacAddr;
    }

    /**
     * Converts a single char to corresponding nibble.
     * 
     * @param c
     *            char to convert. must be 0-9 a-f A-F, no spaces, plus or minus signs.
     * 
     * @return corresponding integer
     */
    private static int charToNibble(char c) {
        int nibble = 0;
        if (('0' <= c) && (c <= '9')) {
            nibble = c - '0';
        } else if (('a' <= c) && (c <= 'f')) {
            nibble = c - 'a' + 0xa;
        } else if (('A' <= c) && (c <= 'F')) {
            nibble = c - 'A' + 0xa;
        }
        return nibble;
    } // charToNibble()

    /**
     * Returns a String with all occurrences of <code>placeholder</code> within <code>orig</code> replaced with
     * <code>substitutes</code> strings which coorespond to the supplied array. If <code>orig</code> contains no
     * occurrences of <code>from</code>, <code>orig</code> itself is returned. None of the parameters should be null and
     * it is assumed the token delimiter is a " ".
     * 
     * @param orig
     *            replace placeholders with supplied substitutes
     * @param placeholder
     *            temporary value to be replaced
     * @param substitutes
     *            actual value to use
     * @return string with placeholders substituted with supplied strings
     */
    public static String substitutePlaceholders(String orig, String delimiter, 
            String placeholder, String[] substitutes) {
        // Tokenize the original string
        String tokens[] = parseStringLine(orig, delimiter);

        // Get ready to create new string with placeholder replaced by
        // substitues
        StringBuffer sb = new StringBuffer();
        int placeholderIdx = 0;
        int startPos;
        int endPos;
        int subLocation;

        // Look for placeholder and substitute with supplied values
        for (int i = 0; i < tokens.length; i++) {
            // This replacement while loop requires that the case of
            // the placeholder string MATCH the case of the placeholder
            // substring found in the tokens[] string.
            // Prior code made use of:
            // if (tokens[i].equalsIgnoreCase(placeholder))
            // under the circumstance token == placeholder & we wish
            // to ignore the case of these items.
            startPos = 0;
            endPos = 0;
            while ((subLocation = (tokens[i].substring(startPos)).indexOf(placeholder)) != -1) {
                // subLocation is the offset in the substring NOT the orig
                // string

                if (subLocation != 0) {
                    endPos = startPos + subLocation;
                    sb.append(tokens[i].substring(startPos, endPos));
                }

                // Replace placeholder in new string
                sb.append(substitutes[placeholderIdx]);

                // Increment placeholder count in order to
                // traverse through the list of substitutes
                if ((placeholderIdx + 1) < substitutes.length) {
                    placeholderIdx++;
                }

                // New starting position
                startPos = endPos + placeholder.length();
            }

            // Append any substring to string under construction
            // NOTE: startPos == 0 indicates append entire string
            if (startPos == 0) {
                sb.append(tokens[i]);
            } else if (startPos < tokens[i].length()) {
                sb.append(tokens[i].substring(startPos, tokens[i].length()));
            }

            // Put back in the delimiter if not at end of tokens
            if ((i + 1) < tokens.length) {
                sb.append(delimiter);
            }
        } // endfor (int i = 0; i < tokens.length; i++)

        // Return the recreated string
        return sb.toString();
    }

    /**
     * Creates a single comma separated list from the elements in the supplied string array.
     * 
     * @param strArray
     *            generate list based on these elements
     * @return single comma separated list of supplied array elements
     */
    public static String getArrayStr(String strArray[]) {
        StringBuffer sb = new StringBuffer(32);
        for (int i = 0; i < strArray.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(strArray[i]);
        }
        return sb.toString();
    }

    /**
     * Replaces a single occurance of a string within a string.
     */
    public static String replaceString(String target, String original, String replacement) {
        String result = target;

        int originalLength = original.length();
        int originalIndex = target.indexOf(original);
        if (originalIndex != -1) {
            result = target.substring(0, originalIndex) + replacement
                    + target.substring(originalIndex + originalLength);
        }
        return result;
    }

    /**
     * Another variation on the replacement theme. This one works by characters...
     * 
     * @param source
     *            Source string to do the replacement in
     * @param originalChar
     *            Original character to look for and replace
     * @param newChar
     *            New (replacement) character
     * @return source string with appropriate replacements
     */
    public static String replaceChar(String source, char originalChar, char newChar) {
        char[] str = source.toCharArray();
        for (int i = 0; i < str.length; i++) {
            if (str[i] == originalChar) {
                str[i] = newChar;
            }                
        }

        return new String(str);
    }

    /**
     * Splits a string at each occurance of delimiter character. If last character is the delimiter then final elements
     * will be an empty string. Adjacent delimiters produce empty strings. Required because StringTokenizer does not
     * allow empty tokens.
     * 
     * @param input - The string to be split.
     * @param delimiter - The character at which splits will occure.
     */
    public static String[] splitString(String input, char delimiter) {
        char[] charArray = input.toCharArray();

        // Count number of tokens
        int tokenCount = 1;
        for (int charIndex = 0; charIndex < charArray.length; charIndex++) {
            if (charArray[charIndex] == delimiter) {
                tokenCount++;
            }
        }

        // Allocate array for tokens
        String[] tokenArray = new String[tokenCount];

        // Split string into tokens are delimiters
        int tokenIndex = 0;
        int tokenStartCharIndex = 0;
        int charIndex = 0;
        for (charIndex = 0; charIndex < charArray.length; charIndex++) {
            if (charArray[charIndex] == delimiter) {
                int length = charIndex - tokenStartCharIndex;
                if (length > 0) {
                    tokenArray[tokenIndex] = new String(charArray, tokenStartCharIndex, length);
                } else {
                    tokenArray[tokenIndex] = "";
                }
                tokenIndex++;
                tokenStartCharIndex = charIndex + 1;
            }
        }
        // Deal with token that follows last delimiter
        int length = charIndex - tokenStartCharIndex;
        if (length > 0) {
            tokenArray[tokenIndex] = new String(charArray, tokenStartCharIndex, length);
        } else {
            tokenArray[tokenIndex] = "";
        }
        return tokenArray;
    }

    /**
     * Appends a String to a buffer right aligned within column.
     * 
     * @param buf
     *            Buffer to append to
     * @param field
     *            String to be appended
     * @param width
     *            With of column in which to right align the field.
     */
    public static void appendRightAligned(StringBuffer buf, String field, int width) {
        int len = field.length();
        while (len < width) {
            buf.append(" ");
            len++;
        }
        buf.append(field);
    }

    /**
     * Appends a String to a buffer left aligned within column.
     * 
     * @param buf
     *            Buffer to append to
     * @param field
     *            String to be appended
     * @param width
     *            With of column in which to left align the field.
     */
    public static void appendLeftAligned(StringBuffer buf, String field, int width) {
        int len = field.length();
        buf.append(field);
        while (len < width) {
            buf.append(" ");
            len++;
        }
    }

    private static final String[] SELECTION_FAILED_REASON = {
        "UNKNOWN",
        "INTERRUPTED",
        "CA_REFUSAL",
        "CONTENT_NOT_FOUND",
        "MISSING_HANDLER",
        "TUNING_FAILURE",
        "INSUFFICIENT_RESOURCES",
    };

    private static final String[] PRESENTATION_TERMINATED_REASON = {
        "UNKNOWN",
        "SERVICE_VANISHED",
        "TUNED_AWAY",
        "RESOURCES_REMOVED",
        "ACCESS_WITHDRAWN",
        "USER_STOP",
    };

    private static final String[] ALTERNATIVE_CONTENT_ERROR_REASON = {
        "RATING_PROBLEM",
        "CA_REFUSAL",
        "CONTENT_NOT_FOUND",
        "MISSING_HANDLER",
        "TUNING_FAILURE"
    };

    public static String toString(ServiceContextEvent e) {
        String str = null;
        if (e instanceof SelectionFailedEvent) {
            SelectionFailedEvent fe = (SelectionFailedEvent) e;
            int reason = fe.getReason();
            if (reason >= 0 && reason < SELECTION_FAILED_REASON.length) {
                str = SELECTION_FAILED_REASON[reason];
            }
        } else if (e instanceof PresentationTerminatedEvent) {
            PresentationTerminatedEvent te = (PresentationTerminatedEvent) e;
            int reason = te.getReason();
            if (reason >= 0 && reason < PRESENTATION_TERMINATED_REASON.length) {
                str = PRESENTATION_TERMINATED_REASON[reason];
            }
        } else if (e instanceof AlternativeContentErrorEvent) {
            AlternativeContentErrorEvent ae = (AlternativeContentErrorEvent) e;
            int reason = ae.getReason() - 100;
            if (reason >= 0 && reason < ALTERNATIVE_CONTENT_ERROR_REASON.length) {
                str = ALTERNATIVE_CONTENT_ERROR_REASON[reason];
            }
        }
        if (str != null) {
            return e + ", r = " + str;
        }
        return e.toString();
    }
}


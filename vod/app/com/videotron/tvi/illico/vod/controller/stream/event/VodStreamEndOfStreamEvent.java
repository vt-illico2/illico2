package com.videotron.tvi.illico.vod.controller.stream.event;

import com.videotron.tvi.illico.vod.controller.stream.IVodStreamControl;

/**
 * Sent to <code>IVodStreamControlListener</code> when the stream reaches end of
 * playout.
 * 
 * @see VodStreamControlEvent
 * @see VodStreamPlayEvent
 * @see VodStreamPauseEvent
 * @see VodStreamFastForwardEvent
 * @see VodStreamRewindEvent
 * @see VodStreamStatusEvent
 * @see VodStreamEndOfStreamEvent
 * @see VodStreamResetEvent
 * @see IVodStreamControl
 * @see IVodStreamControlListener
 */
public class VodStreamEndOfStreamEvent extends VodStreamControlEvent {

    private static final long serialVersionUID = 5172440766560794843L;

    /**
     * Constructs an VodEndOfStreamEvent event object.
     * 
     * @param objSrc
     *            The source of the event.
     * @param act
     *            Asynchronous completion token.
     * @param streamHandle
     *            the streamHandle as seen by the media server.
     * @param streamState
     *            the state of the stream.
     * @param statusCode
     *            the outcome code of this operation.
     * @param lastNpt
     *            the npt of the stream reported by the media server at the time the operation was processed.
     * @param scaleNum
     *            the numerator portion of the stream's current rate.
     * @param scaleDenom
     *            the denominator portion of the stream's current rate
     * 
     * @see VodStreamControlEvent
     * @see VodStreamPlayEvent
     * @see VodStreamPauseEvent
     * @see VodStreamFastForwardEvent
     * @see VodStreamRewindEvent
     * @see VodStreamStatusEvent
     * @see VodStreamEndOfStreamEvent
     * @see VodStreamResetEvent
     * @see IVodStreamControl
     * @see IVodStreamControlListener
     */
    public VodStreamEndOfStreamEvent(IVodStreamControl objSrc, Object act, int streamHandle, int streamState,
            int statusCode, int lastNpt, short scaleNum, short scaleDenom) {
        super(objSrc, act, streamHandle, streamState, statusCode, lastNpt, scaleNum, scaleDenom);
    }
}

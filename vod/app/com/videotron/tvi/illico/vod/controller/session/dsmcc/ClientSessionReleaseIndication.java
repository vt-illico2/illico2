package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * UNClientReleaseIndication.
 */
public class ClientSessionReleaseIndication extends SessionMessage implements NetworkToClientMessage {

    private int reason;

    private byte[] uudata;

    /**
     *
     */
    public ClientSessionReleaseIndication() {
        super();
    }

    /**
     * Internalizes the received byte array into a UNClientReleaseIndication message object.
     *
     * @param data
     */
    public void parse(byte[] data) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data, 0, data.length);
        DataInputStream in = new DataInputStream(bais);

        try {
            // Parse in the DSMCC header and SessionID
            super.parse(in);

            // Parse in the reason for this delay
            reason = in.readShort();
            // convert to unsigned
            reason &= 0xffff;

            // Get the user data.
            int uuDataLength = in.readShort();

            byte[] temp = new byte[uuDataLength];
            uudata = new byte[uuDataLength + 2];
            in.readFully(temp);
            uudata[0] = (byte) (uuDataLength & 0xFF);
            uudata[1] = (byte) (uuDataLength & 0xFF00);
            System.arraycopy(temp, 0, uudata, 2, uuDataLength);

            in.close();
            in = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (in != null) {
                try {
                    in.close();
                    in = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }
    }

    /**
     *
     * Returns the reason code supplied by the server. This code explains why the teardown was requested.
     *
     * @return int
     */
    public int getReason() {
        return reason;
    }

    /**
     *
     * Returns the sspVersion V2.3 UserData section of the message.
     *
     * @return byte[]
     */
    public byte[] getUserData() {
        return uudata;
    }
}

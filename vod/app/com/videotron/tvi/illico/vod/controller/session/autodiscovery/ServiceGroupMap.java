package com.videotron.tvi.illico.vod.controller.session.autodiscovery;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.dvb.dsmcc.DSMCCObject;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;

public class ServiceGroupMap{

    private int version;
    private int entryLength;
    private int UniqueFrequencies;
    private int serviceGroup[];
    private int tsIDs[];
    private int frequencies[];
    private int mod[];

//    boolean readFile = false;

    public int[] getFrequencies() {
        return frequencies;
    }

    public int[] getServiceGroup() {
        return serviceGroup;
    }

    public int[] getTsIDs() {
        return tsIDs;
    }

    public int[] getMod() {
        return mod;
    }
    
//    public String toString() {
//    	return "";
//    }

    public boolean makeServiceGroupData(File oobFile){
        if (Log.DEBUG_ON) {
            Log.printDebug("makeServiceGroupData : " + oobFile);
        }
        if (Environment.EMULATOR) {
            File file = (File) DataCenter.getInstance().get("meta_SGM");
            return parseFileFromBIN(file);
        } else {
            return parseFileFromBIN(oobFile);
        }
    }

    public boolean parseFileFromBIN(File file) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ServiceGroupMap parseFileFromBIN " + file);
        }
        boolean ret = false;
        FileInputStream fis = null;
        DataInputStream dis = null;
        try {
        	if (file instanceof DSMCCObject) {
        		DSMCCObject oobFile = (DSMCCObject) file;
        		if (oobFile.isLoaded() == false) {
        			Log.printDebug("ServiceGroupMap need loading, " + oobFile);
        			oobFile.synchronousLoad();
        			Log.printDebug("ServiceGroupMap loading done");
        		}
        	}

            fis = new FileInputStream(file);
            dis = new DataInputStream(fis);
            //System.out.println("file: " + file.getName());

            int i = 0;
            int data;
            while (i < 4) {
                data = dis.readInt();
                switch (i) {
                case 0:
                    Log.printInfo("parseFileFromBIN Format     Version: " + data);
                    break;
                case 1:
                    version = data;
                    Log.printInfo("parseFileFromBIN Map        Version: " + data);
                    break;
                case 2:
                    entryLength = data;
                    Log.printInfo("parseFileFromBIN Map        Entries: " + data);
                    break;
                case 3:
                    UniqueFrequencies = data;
                    Log.printInfo("parseFileFromBIN Unique Frequencies: " + data);
                    break;
                default:
                    break;
                }
                i++;
            }
            // "TSID","SGID", "TRANS", "INNER", "SPLIT", "MODUL", "SYMBOLRATE",
            // "FREQUENCY"
            // L2C4L2
            serviceGroup = new int[entryLength];
            tsIDs = new int[entryLength];
            frequencies = new int[entryLength];
            mod = new int[entryLength];
            if (Log.DEBUG_ON) {
                Log.printDebug("ServiceGroupMap parseFileFromBIN contentsLen " + entryLength);
            }
            for (int j = 0; j < entryLength; j++) {
                tsIDs[j] = dis.readInt();
                serviceGroup[j] = dis.readInt();
                dis.readByte();
                dis.readByte();
                dis.readByte();
                mod[j] = (int) dis.readByte();
                dis.readInt();
                frequencies[j] = dis.readInt();
                Log.printDebug("tsId : " + tsIDs[j] + ", sgId : " + serviceGroup[j] + ", freq : "
                        + frequencies[j] + ", modulation : " + mod[j]);
            }
            ret = true;
        } catch (Exception eof) {
            Log.print(eof);
            CommunicationManager.getInstance().errorMessage("VOD402", null);
        } finally{
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @return the entryLength
     */
    public int getEntryLength() {
        return entryLength;
    }

    /**
     * @return the uniqueFrequencies
     */
    public int getUniqueFrequencies() {
        return UniqueFrequencies;
    }


}

package com.videotron.tvi.illico.vod.controller.communication;

/**
 * The Class CheckResult contains the result of CheckRightFilter from User Profile Application.
 */
public class CheckResult {
    
    /** The response. */
    private int response;
    
    /** The filter. */
    private String filter;
    
    /**
     * Instantiates a new check result.
     * 
     * @param filter the filter
     * @param response the response
     */
    public CheckResult(String filter, int response){
        this.filter = filter;
        this.response = response;
    }

    /**
     * Gets the response.
     * 
     * @return the response
     */
    public int getResponse() {
        return response;
    }

    /**
     * Gets the filter.
     * 
     * @return the filter
     */
    public String getFilter() {
        return filter;
    }    
}

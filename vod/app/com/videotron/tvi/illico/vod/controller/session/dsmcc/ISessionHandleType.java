package com.videotron.tvi.illico.vod.controller.session.dsmcc;

public interface ISessionHandleType {
    /** the state of the session before the setup method is invoked. This is the default state. */
    static final int STATE_IDLE = 0;

    /** the state of the session when setup successfully completes.*/
    public static final int STATE_ACTIVE = 1;

    /**
     * the state of the session after the setup method is invoked and before
     * the SessionSetupSucceededEvent arrives.
     */
    public static final int STATE_PENDING_SETUP_COMPLETE = 2;

    /**
     * the state of the session after the release method is invoked and
     * before the SessionReleaseSucceededEvent arrives.
     */
    public static final int STATE_PENDING_RELEASE_COMPLETE = 3;

    /** identifies the descriptors to be sent with the setup request. */
    public static final int SETUP_REQUEST_DESCRIPTOR_TYPE = 0x00;

    /** identifies the descriptors returned from the server when the setup completes. */
    public static final int SETUP_COMPLETE_DESCRIPTOR_TYPE = 0x01;

    /** identifies the descriptors returned from the server when the release completes. */
    public static final int RELEASE_COMPLETE_DESCRIPTOR_TYPE = 0x02;

    /** identifies the descriptors to be sent with the release request. */
    public static final int RELEASE_REQUEST_DESCRIPTOR_TYPE = 0x03;

    /** identifies the descriptors returned from the server when session release is initiated from the server. */
    public static final int RELEASE_NOTIFICATION_DESCRIPTOR_TYPE = 0x04;
}

/*  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session.dsmcc;

/**
 * @author LSC
 * @version 1.0 RspCode REASON AND RESPONSE CODES
 */
public class ResRspCode {
    /**
     * Comment for RsnSessionRelease Used in release requests to indicate a normal release request.
     */
    public static final int RsnClNormalRelease = 0x01;

    /**
     * Comment for RsnClFormatError Used in release requests to release a session because a format error was detected.
     */
    public static final int RsnClFormatError = 0x05;

    /**
     * Comment for RsnClProcError Used in release requests to release a session for the reason that a processing error
     * occurred.
     */
    public static final int RsnClProcError = 0x02;

    /**
     * Comment for RsnClSessionRelease Used in release requests to release a session for some reason other than normal.
     */
    public static final int RsnClSessionRelease = 0x19;

    /**
     * Comment for OK Indicates a successful event.
     */
    public static final int RspOk = 0x00;

    /**
     * Comment for RspSeProcError Request rejected due to a server detected error.
     */
    public static final int RspSeProcError = 0x24;

    /**
     * Comment for RspSeFormatError Request rejected due to a server detected format error.
     */
    public static final int RspSeFormatError = 0x27;

    /**
     * Comment for RspNeNoCalls Request rejected by the network because new calls could not be handled.
     */
    public static final int RspNeNoCalls = 0x02;

    /**
     * Comment for RspNeInvalidClient Request rejected by the network due to an invalid client identifier.
     */
    public static final int RspNeInvalidClient = 0x03;

    /**
     * Comment for RspNeInvalidServer Request rejected by the network due to invalid server identifier.
     */
    public static final int RspNeInvalidServer = 0x04;

    /**
     * Comment for RspNeNoSession Request rejected by the network because the session doesn't exist.
     */
    public static final int RspNeNoSession = 0x07;

    /**
     * Comment for RspSeNoSession Request rejected by the server or the server timed out.
     */
    public static final int RspSeNoSession = 0x10;

    /**
     * Comment for RspSeNoService Request rejected by the server because the service is unavailable.
     */
    public static final int RspSeNoService = 0x08;

    /**
     * Comment for RspSeNoResource The resource was not found.
     */
    public static final int RspSeNoResource = 0x20;

    /**
     * Comment for RspClUnknown The reason is unknown.
     */
    public static final int RspClUnknown = 0x40;

    /**
     * Comment for RspSeUnknown The reason is unknown.
     */
    public static final int RspSeUnknown = 0xff;
}

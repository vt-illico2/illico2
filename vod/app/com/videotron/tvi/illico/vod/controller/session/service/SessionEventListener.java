package com.videotron.tvi.illico.vod.controller.session.service;

public interface SessionEventListener {
    public void receiveSRMEvent(SessionEvent event);
}

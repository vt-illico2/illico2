package com.videotron.tvi.illico.vod.controller.communication;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;

import javax.tv.service.selection.ServiceContext;
import javax.tv.xlet.XletContext;

import org.davic.net.tuning.NetworkInterface;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.dvb.io.ixc.IxcRegistry;
import org.dvb.service.selection.DvbServiceContext;
import org.havi.ui.event.HRcEvent;
import org.ocap.diagnostics.MIBDefinition;
import org.ocap.diagnostics.MIBManager;
import org.ocap.diagnostics.MIBObject;

import com.opencable.handler.cahandler.CAHandler;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.daemon.RemoteRequestListener;
import com.videotron.tvi.illico.ixc.epg.CaUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.isa.ISAServiceDataListener;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.EIDMappingTableUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorCAAuthorizationListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.search.SearchActionEventListener;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.UsageManager;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Service;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.ListOfTitlesUI;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;
import com.videotron.tvi.illico.vod.ui.UITemplateList;

public class CommunicationManager {

	private static CommunicationManager instance;
	private MonitorService monitorService;
	private EpgService epgService;
//	private CAHandler caHandler;
	private SearchService searchService;
	private ErrorMessageService errorMessageService;
	private boolean showErrorMessage = true;
	private String lastErrorCode;
	private StcService stcService;
	private DaemonService daemonService;
	private VbmService vbmService;
	private PvrService pvrService;
	private ISAService isaService;
	private XletContext xletContext;

	private CommunicationManager() {

	}

	public void init(XletContext xletContext) {
		this.xletContext = xletContext;
		lookUpService("/1/1/", MonitorService.IXC_NAME);
		lookUpService("/1/1/", CAHandler.IXC_NAME);
		lookUpService("/1/2026/", EpgService.IXC_NAME);
		lookUpService("/1/2080/", SearchService.IXC_NAME);
		lookUpService("/1/2075/", LoadingAnimationService.IXC_NAME);
		lookUpService("/1/2025/", ErrorMessageService.IXC_NAME);
		lookUpService("/1/2100/", StcService.IXC_NAME);
		lookUpService("/1/2040/", DaemonService.IXC_NAME);
		lookUpService("/1/2085/", VbmService.IXC_NAME);
		lookUpService("/1/20A0/", PvrService.IXC_NAME);
		lookUpService("/1/2200/", ISAService.IXC_NAME);
	}
	
	public void destroy() {
		if (monitorService != null) {
			try {
				monitorService.removeInbandDataListener(
						inbandDataListener, Resources.APP_NAME);
			} catch (RemoteException ex) {
				Log.print(ex);
			}
			try {
				monitorService.removeCAResourceAuthorizationListener(
						caListener, Resources.APP_NAME);
			} catch (RemoteException ex) {
				Log.print(ex);
			}
			try {
				monitorService.removeEIDMappingTableUpdateListener(
						eidUpdateListener, Resources.APP_NAME);
			} catch (RemoteException ex) {
				Log.print(ex);
			}
		}
		if (stcService != null) {
			try {
				stcService.removeLogLevelChangeListener(
						Resources.APP_NAME);
			} catch (RemoteException ex) {
				Log.print(ex);
			}
		}
		if (daemonService != null) {
			try {
				daemonService.removeListener(Resources.APP_NAME);
			} catch (RemoteException ex) {
				Log.print(ex);
			}
		}
		if (isaService != null) {
			try {
				isaService.removeISAServiceDataListener(Resources.APP_NAME);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void lookUpService(String path, final String name) {
		final String ixcName = path + name;
		Thread thread = new Thread("VOD.lookUp." + name) {
			public void run() {
				Remote remote = null;
				while (remote == null) {
					try {
						remote = IxcRegistry.lookup(xletContext, ixcName);
					} catch (Exception ex) {
						if (Log.INFO_ON) {
							Log.printInfo("CommunicationManager: not bound - " + name);
						}
					}
					if (remote != null) {
						if (Log.INFO_ON) {
							Log.printInfo("CommunicationManager: found - " + name);
						}
						DataCenter.getInstance().put(name, remote);
						try {
							if (name.equals(MonitorService.IXC_NAME)) {
								monitorService = (MonitorService) remote;
								monitorService.addInbandDataListener(
										inbandDataListener, Resources.APP_NAME);
								monitorService.addCAResourceAuthorizationListener(
										caListener, Resources.APP_NAME);
								monitorService.addEIDMappingTableUpdateListener(
										eidUpdateListener, Resources.APP_NAME);
								if (DataCenter.getInstance().get("SVOD_SERVICE_IDS") != null) {
									Log.printDebug("will checkPackages() because monitor service looked up");
									checkPackages();
								}
							} else if (name.equals(SearchService.IXC_NAME)) {
								searchService = (SearchService) remote;
							} else if (name.equals(EpgService.IXC_NAME)) {
								epgService = (EpgService) remote;
//							} else if (name.equals(CAHandler.IXC_NAME)) {
//								caHandler = (CAHandler) remote;
							} else if (name.equals(ErrorMessageService.IXC_NAME)) {
								errorMessageService = (ErrorMessageService) remote;
							} else if (name.equals(StcService.IXC_NAME)) {
								stcService = (StcService) remote;
								try {
									int currentLevel = stcService
											.registerApp(Resources.APP_NAME);
									Log.printDebug("stc log level = "
											+ currentLevel);
									Log.setStcLevel(currentLevel);
								} catch (RemoteException ex) {
									Log.print(ex);
								}
								Object o = SharedMemory.getInstance().get(
										"stc-" + Resources.APP_NAME);
								if (o != null) {
									DataCenter.getInstance().put("LogCache", o);
								}
								try {
									stcService.addLogLevelChangeListener(
											Resources.APP_NAME,
											logLevelChangeListener);
								} catch (RemoteException ex) {
									Log.print(ex);
								}
							} else if (name.equals(DaemonService.IXC_NAME)) {
								daemonService = (DaemonService) remote;
								daemonService.addListener(Resources.APP_NAME, remoteRequestListener);
							} else if (name.equals(VbmService.IXC_NAME)) {
								vbmService = (VbmService) remote;
								VbmController.getInstance().init(vbmService);
							} else if (name.equals(PvrService.IXC_NAME)) {
								pvrService = (PvrService) remote;
							} else if (name.equals(ISAService.IXC_NAME)) {
								isaService = (ISAService) remote;
								isaService.addISAServiceDataListener(isaListener, Resources.APP_NAME);
								isaListener.updateISAServiceList(ISAService.ISA_SERVICE_TYPE_CHANNEL, 
										isaService.getISAServiceList(ISAService.ISA_SERVICE_TYPE_CHANNEL));
								isaListener.updateISAServiceList(ISAService.ISA_SERVICE_TYPE_ILLICO_SERVICE,
										isaService.getISAServiceList(ISAService.ISA_SERVICE_TYPE_ILLICO_SERVICE));

								// Bundling
								isaListener.updateISAExcludedChannelsAndTypes(
										isaService.getExcludedChannels(), isaService.getExcludedChannelTypes()
								);
							}
						} catch (RemoteException re) {
							Log.printWarning(re);
						}
						return;
					}
					try {
						Thread.sleep(2000);
					} catch (Exception e) {
					}
				}
			}
		};
		thread.start();
	}

	public void start() {
		if (monitorService != null) {
			try {
				monitorService.addScreenSaverConfirmListener(
						MenuController.getInstance(), Resources.APP_NAME);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		if (searchService != null) {
			try {
				searchService.addSearchActionEventListener(
						SearchService.PLATFORM_VOD, searchActionEventListener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		if (epgService != null) {
			try {
				epgService.addCaUpdateListener(caUpdateListener);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (AbstractMethodError e) {
				e.printStackTrace();
			}
		}
		
		if (Environment.EMULATOR) {
			packageRightsNext.put("PK_MENU_VOD", "1");
			packageRightsNext.put("PK_VOD_SERVICE1", "4821"); // 4k
			packageRightsNext.put("PK_VOD_CLUB", "1131"); // club
//			packageRightsNext.put("PK_VOD_SERVICE1", "18");
//			packageRightsNext.put("PK_VOD_SERVICE2", "21");
//			packageRightsNext.put("PK_VOD_SERVICE3", "24");
//			packageRightsNext.put("PK_VOD_SERVICE4", "33");
		}
		
		// if package updated while running, update cache once at start
		updatePackageRights();
	}
	
	public void updatePackageRights() {
		packageRights.clear();
		packageRights.putAll(packageRightsNext);
		Log.printDebug("packageRights = " + packageRights);
		Log.printDebug("packageRightsNext = " + packageRightsNext);
	}
	
	public void stop() {
		stopSearchApplication();
		if (monitorService != null) {
			try {
				monitorService.removeScreenSaverConfirmListener(
						MenuController.getInstance(), Resources.APP_NAME);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		if (searchService != null) {
			try {
				searchService
						.removeSearchAcitonEventListener(SearchService.PLATFORM_VOD);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		if (epgService != null) {
			try {
				epgService.removeCaUpdateListener(caUpdateListener);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (AbstractMethodError e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized static CommunicationManager getInstance() {
		if (instance == null) {
			instance = new CommunicationManager();
		}
		return instance;
	}
	
	public void requestTuner(final String actionMenu, final BaseElement element, final int srcId) throws NullPointerException {
		if (pvrService == null) {
			Log.printWarning("PvrService is null");
			throw new NullPointerException();
		}
		new Thread("requestTuner action = " + actionMenu + ", element = " + (element == null ? "NULL" : element.toString(true))  + ", srcId = " + srcId) {
			public void run() {
				Log.printDebug("called requestTuner(), action = " + actionMenu + ", element = " + (element == null ? "NULL" : element.toString(true)) + ", srcId = " + srcId);
				boolean ret = false;
				try {
					if (srcId > 0) {
						ret = pvrService.requestTuner(srcId);
					} else {
						ret = pvrService.requestTuner();
					}
				} catch (RemoteException e) {
					Log.print(e);
				}
				Log.printDebug("PvrService return = " + ret);
				if (ret) {
					if (actionMenu == null) {
						MenuController.getInstance().processKeyEvent(HRcEvent.VK_ENTER);
					} else {
						MenuController.getInstance().processActionMenu(actionMenu, element);
					}
				}
			}
		}.start();
		
	}

	public void sendReadyMSGtoMonitor() {
		if (Log.INFO_ON) {
			Log.printInfo("CommunicationManager: sendReadyMSGtoMonitor");
		}
		while (true) {
			if (monitorService == null) {
				Log.printDebug("sendReadyMSGtoMonitor : sleep 1 sec ");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				break;
			}
		}

		try {
			monitorService.iamReady(Resources.APP_NAME);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public byte[] getCableCardMacAddress() {
		if (Log.INFO_ON) {
			Log.printInfo("CommunicationManager: getCableCardMacAddress");
		}
		byte[] cableMac = null;
		try {
			if (monitorService != null) {
				cableMac = monitorService.getCableCardMacAddress();
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return cableMac;
	}

	/**
	 * @return the monitorService
	 */
	public MonitorService getMonitorService() {
		return monitorService;
	}

	public String[] getParameter() {
		String[] parameters = null;
		if (monitorService != null) {
			try {
				parameters = monitorService.getParameter(Resources.APP_NAME);
				if (Log.DEBUG_ON) {
					if (parameters != null) {
						Log.printDebug("CommunicationManager getParameter, " + parameters.length + " parameter(s)");
						for (int i = 0; i < parameters.length; i++) {
							Log.printDebug("CommunicationManager getParameter["+i+"/"+parameters.length+"] : "
											+ parameters[i]);
						}
					} else {
						Log.printDebug("CommunicationManager getParameter, NULL");
					}
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return parameters;
	}

	RemoteRequestListener remoteRequestListener = new RemoteRequestListener() {
		public byte[] request(String path, byte[] body) throws RemoteException {
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager request : "
								+ path + ", " + new String(body));
			}
			if ("getServiceGroup".equals(path)) {
				byte[] ret = ServiceGroupController.getInstance().getServiceGroupData();
				Log.printDebug("getServiceGroup, ret = " + StringUtil.byteArrayToHexString(ret));
				int id = ServiceGroupController.getInstance().getServcieGroupID();
				Log.printDebug("getServiceGroup, int = " + id);
				return ("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><root result=\"0\"><sg>"+id+"</sg></root>").getBytes();
			} else if ("eraseServiceGroup".equals(path)) {
				byte[] ret = ServiceGroupController.getInstance().getServiceGroupData();
				Log.printDebug("before service group data = " + StringUtil.byteArrayToHexString(ret));
				ServiceGroupController.getInstance().eraseServiceGroup();
				ret = ServiceGroupController.getInstance().getServiceGroupData();
				Log.printDebug("after service group data = " + StringUtil.byteArrayToHexString(ret));
				return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><root result=\"0\" />".getBytes();
			} else if ("forceAutoDiscovery".equals(path)) {
				new Thread("forceAutoDiscovery") {
					public void run() {
						ServiceGroupController.getInstance().searchServiceGroup();
					}
				}.start();
				return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><root result=\"0\" />".getBytes();
			}
			return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><root result=\"-1\" />".getBytes();
		}
	};
	
	InbandDataListener inbandDataListener = new InbandDataListener() {
		public void receiveInbandData(String locator) throws RemoteException {
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager receiveInbandData : "
								+ locator);
			}
			int versionServerConfig = -1;
			try {
				versionServerConfig = monitorService
						.getInbandDataVersion(MonitorService.vod_server_config);
				Log.printDebug("CommunicationManager receiveInbandData versionServerConfig : "
								+ versionServerConfig);
			} catch (Exception e) {
				if (Log.WARNING_ON) {
					e.printStackTrace();
				}
			}
			if (!Environment.EMULATOR) {
				DataAdapterManager.getInstance().getInbandAdapter()
						.asynchronousLoad(locator);
			}
			monitorService.completeReceivingData(Resources.APP_NAME);
		}
	};
	
	public void launchCYO() {
		PreferenceService prefSvc = PreferenceProxy.getInstance().getService();
		
		try {
			prefSvc.showPinEnabler(new PinEnablerListener() {
				public void receivePinEnablerResult(int response, String detail) throws RemoteException {
					if (response == PreferenceService.RESPONSE_SUCCESS) {
						if (monitorService != null) {
							try {
								monitorService.startUnboundApplication("Options", null);
							} catch (RemoteException e) {
								Log.print(e);
							}
						}
					}
				}
			}, new String[] {
					DataCenter.getInstance().getString(Resources.TEXT_MSG_ADMIN1),
					DataCenter.getInstance().getString(Resources.TEXT_MSG_ADMIN2)
			});
		} catch (RemoteException e) {
			Log.print(e);
		}
		
		
	}

	private boolean lastSearchMode;
	public void launchSearchApplication(boolean init, boolean karaoke) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager launchSearchApplication, init = " + init + ", karaoke = " + karaoke);
		}
		if (searchService != null) {
			if (init) {
				lastSearchMode = karaoke;
			} else {
				karaoke = lastSearchMode;
			}
			try {
				searchService.launchSearchApplication(
						karaoke ? SearchService.PLATFORM_KARAOKE :
						SearchService.PLATFORM_VOD, init);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		} else {
			errorMessage("VOD201-04", null);
			Log.printWarning("CommunicationManager SearchService is not looked up.");
		}
	}

    public void launchSearchApplication(boolean init, boolean karaoke, String param) {
        if (Log.DEBUG_ON) {
            Log.printDebug("CommunicationManager launchSearchApplication, init = " + init + ", karaoke = " + karaoke
            + "param" + param);
        }
        if (searchService != null) {
            if (init) {
                lastSearchMode = karaoke;
            } else {
                karaoke = lastSearchMode;
            }
            try {
                searchService.launchSearchApplicationWithParam(
                        karaoke ? SearchService.PLATFORM_KARAOKE :
                                SearchService.PLATFORM_VOD, init, param);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            errorMessage("VOD201-04", null);
            Log.printWarning("CommunicationManager SearchService is not looked up.");
        }
    }
	
	// R5
	public void startUnboundApplication(String targetApp, String[] parameters) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager startUnboundApplication, targetApp = " + targetApp
					+ ", parameters = " + parameters);
		}
		try {
			monitorService.startUnboundApplication(targetApp, parameters);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public TvChannel getChannel(String callLetter) {
		if (epgService == null) {
			errorMessage("VOD201-04", null);
			Log.printWarning("CommunicationManager EpgService is not looked up.");
		}
		TvChannel ch = null;
		try {
			ch = epgService.getChannel(callLetter);
		} catch (Exception e) {
			Log.print(e);
		}
		return ch;
	}
	
	MonitorCAAuthorizationListener caListener = new MonitorCAAuthorizationListener() {
		public void deliverEvent(long targetId, int targetType,
				boolean isAuthorized, String packageId) throws RemoteException {
			
			Log.printInfo("CommunicationManager deliverEvent, " + packageId + "(" + targetId + ") - " + isAuthorized);
			int[] svodServiceIds = (int[]) DataCenter.getInstance().get("SVOD_SERVICE_IDS");
			String[] svodPackageNames = (String[]) DataCenter.getInstance().get("SVOD_PACKAGE_NAMES");
			if (svodPackageNames == null) {
				Log.printInfo("CommunicationManager deliverEvent, no svodPackageNames");
				return;
			}
			Log.printDebug("CommunicationManager deliverEvent, before packageRightsNext = " + packageRightsNext);
			String strServiceId = null;
			for (int i = 0; i < svodPackageNames.length; i++) {
				if (svodPackageNames[i].equals(packageId)) {
					strServiceId = String.valueOf(svodServiceIds[i]);
					break;
				}
			}
			// remove service id if it exists
			for (Enumeration e = packageRightsNext.keys(); e.hasMoreElements(); ) {
				Object key = e.nextElement();
				if (strServiceId.equals(packageRightsNext.get(key))) {
					packageRightsNext.remove(key);
				}
			} 
			// put service id if only it is authorized
			if (isAuthorized) {
				packageRightsNext.put(packageId, strServiceId);
				packageRights.put(packageId, strServiceId);
			} else {
				packageRightsNext.remove(packageId);
				packageRights.remove(packageId);
			}
			if (MenuController.getInstance().getCurrentScene() != null) {
				Log.printDebug("CommunicationManager deliverEvent, curScene = " + MenuController.getInstance().getCurrentScene());
				((BaseUI) MenuController.getInstance().getCurrentScene()).updateButtons();
			}
			Log.printDebug("CommunicationManager deliverEvent, after packageRightsNext = " + packageRightsNext);
		}
	};
	
	EIDMappingTableUpdateListener eidUpdateListener = new EIDMappingTableUpdateListener() {
		public void tableUpdated() throws RemoteException {
			new Thread("CommunicationManager tableUpdated") {
				public void run() {
					Log.printDebug("CommunicationManager tableUpdated, so call checkPacakges()");
					checkPackages();
				}
			}.start();
		}
	};
	
	Hashtable packageRights = new Hashtable();
	Hashtable packageRightsNext = new Hashtable();
	
	public String getAllPackages() {
		int[] svodServiceIds = (int[]) DataCenter.getInstance().get("SVOD_SERVICE_IDS");
		if (svodServiceIds == null || svodServiceIds.length == 0) {
			if (Environment.EMULATOR) {
				return getAuthorizedPackage();
			}
			return "&serviceIds=default";
		}
		StringBuffer buf = new StringBuffer("&serviceIds=");
		for (int i = 0; i < svodServiceIds.length; i++) {
			buf.append("S_").append(svodServiceIds[i]).append(',');
		}
		return buf.toString();
	}
	
	public boolean hasAnyPackage() {
		if (Environment.EMULATOR) {
			return true;
		}
		int[] svodServiceIds = (int[]) DataCenter.getInstance().get("SVOD_SERVICE_IDS");
		return svodServiceIds != null && svodServiceIds.length > 0;
	}
	
	public String getAuthorizedPackage() {
		if (packageRights.size() == 0) {
			return "&serviceIds=default";
		}
		StringBuffer buf = new StringBuffer("&serviceIds=");
		StringBuffer bufLog = new StringBuffer();
		Enumeration e = packageRights.keys();
		while (e.hasMoreElements()) {
			Object key = e.nextElement();
			bufLog.append((String) key).append(',');
			Object val = packageRights.get(key);
			buf.append("S_").append((String) val).append(',');
		}
//		if (Log.DEBUG_ON) {
//			Log.printDebug("CommunicationManager getAuthorizedPackage, " + buf.toString() + " -> " + bufLog.toString());
//		}
		return buf.toString();
	}
	
	public String getAuthorizedPackage2() {
		if (packageRights.size() == 0) {
			return "default";
		}
		StringBuffer buf = new StringBuffer();
		StringBuffer bufLog = new StringBuffer();
		Enumeration e = packageRights.keys();
		while (e.hasMoreElements()) {
			Object key = e.nextElement();
			bufLog.append((String) key).append(',');
			Object val = packageRights.get(key);
			buf.append("S_").append((String) val).append(',');
		}
//		if (Log.DEBUG_ON) {
//			Log.printDebug("CommunicationManager getAuthorizedPackage, " + buf.toString() + " -> " + bufLog.toString());
//		}
		return buf.toString();
	}
	
	synchronized public void checkPackages() {
		int[] svodServiceIds = (int[]) DataCenter.getInstance().get("SVOD_SERVICE_IDS");
		String[] svodPackageNames = (String[]) DataCenter.getInstance().get("SVOD_PACKAGE_NAMES");
		if (svodServiceIds == null) {
			boolean o = showErrorMessage;
			showErrorMessage = false;
			errorMessage("VOD201-02", null);
			Log.printWarning("CommunicationManager service ids is null.");
			showErrorMessage = o;
			packageRightsNext.clear();
		} else {
			if (monitorService == null) {
				Log.printWarning("CommunicationManager checkPackages, there is no MonitorService");
				return;
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager checkPackages, before packageRightsNext = " + packageRightsNext);
			}
			packageRightsNext.clear();
			for (int i = 0; i < svodServiceIds.length; i++) {
				checkPackage(svodPackageNames[i], svodServiceIds[i]);
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager checkPackages, after packageRightsNext = " + packageRightsNext);
			}
			if (MenuController.getInstance().getCurrentScene() != null) {
				((BaseUI) MenuController.getInstance().getCurrentScene()).updateButtons();
				MenuController.getInstance().getCurrentScene().repaint();
			}
			
			// R5
			// It will be used by Search.
			updatePackageRights();
			String svodList = getAuthorizedPackage2();
			SharedMemory.getInstance().put(VODService.KEY_SVOD_LIST, svodList);
		}
	}
	
	public boolean checkPackage(Service service) {
		return packageRights.contains(service.id.substring(2));
	}
	
	public boolean checkPackage(Service[] service) {
		if (service == null || service.length == 0) {
			return true;
		}
		for (int i = 0; i < service.length; i++) {
			// make to ignore the service has a CONFIG for serviceType.
			// The service which has a CNOFIG for serviceType have to use checkPackage(Service) according to needs
			if (service[i].serviceType != Service.CONFIG && checkPackage(service[i])) {
				Log.printDebug("checkPackage, service[" + i + "] = " + service[i] + ", true");
				return true;
			}
			Log.printDebug("checkPackage, service[" + i + "] = " + service[i] + ", false");
		}
		Log.printDebug("checkPackage, packages = " + packageRights);
		return false;
	}

	// R7.3
	public boolean checkPackageForWishList(Service[] service) {
		if (service == null || service.length == 0) {
			return true;
		}
		for (int i = 0; i < service.length; i++) {
			// make to ignore the service has a CONFIG for serviceType.
			// The service which has a CNOFIG for serviceType have to use checkPackage(Service) according to needs
			if (service[i].serviceType != Service.CONFIG && checkPackage(service[i])) {
				Log.printDebug("checkPackageForWishList, service[" + i + "] = " + service[i] + ", true");
				return true;
			} else if (service[i].serviceType == Service.SVOD) {
				Log.printDebug("checkPackageForWishList, service[" + i + "] = " + service[i] + ", is SVOD");
				return true;
			}
			Log.printDebug("checkPackageForWishList, service[" + i + "] = " + service[i] + ", false");
		}
		Log.printDebug("checkPackageForWishList, packages = " + packageRights);
		return false;
	}
	
	public boolean checkPackage(String packageName, int serviceId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager checkPackage, " + packageName);
		}
		if (monitorService == null || packageName == null || packageName.length() == 0) {
			return false;
		}
		int ret = MonitorService.CHECK_ERROR;
		try {
			ret = monitorService.checkResourceAuthorization(packageName);
		} catch (RemoteException e) {
			boolean o = showErrorMessage;
			showErrorMessage = false;
			errorMessage("VOD505", null);
			showErrorMessage = o;
		}
		String debugLog = ", checkPackage, name = " + packageName + ", ret = ";
		switch (ret) {
		case MonitorService.NOT_AUTHORIZED:
			Log.printDebug("CommunicationManager checkPackage return = NOT_AUTHORIZED");
			debugLog += "NOT_AUTHORIZED";
			break;
		case MonitorService.AUTHORIZED:
			Log.printDebug("CommunicationManager checkPackage return = AUTHORIZED");
			debugLog += "AUTHORIZED";
			break;
		case MonitorService.NOT_FOUND_ENTITLEMENT_ID:
			Log.printDebug("CommunicationManager checkPackage return = NOT_FOUND_ENTITLEMENT_ID");
			debugLog += "NOT_FOUND_ENTITLEMENT_ID";
			break;
		case MonitorService.CHECK_ERROR:
			Log.printDebug("CommunicationManager checkPackage return = CHECK_ERROR");
			debugLog += "CHECK_ERROR";
			break;
		}
		MenuController.getInstance().debugList[4].add(CatalogueDatabase.SDF.format(new Date()) + debugLog); 
		if (ret == MonitorService.CHECK_ERROR) {
			boolean o = showErrorMessage;
			showErrorMessage = false;
			errorMessage("VOD504", null);
			showErrorMessage = o;
		} else if (ret == MonitorService.NOT_FOUND_ENTITLEMENT_ID) {
			boolean o = showErrorMessage;
			showErrorMessage = false;
			errorMessage("VOD201-03", null);
			showErrorMessage = o;
		}
		String strServiceId = String.valueOf(serviceId);
		// remove service id if it exists
		for (Enumeration e = packageRightsNext.keys(); e.hasMoreElements(); ) {
			Object key = e.nextElement();
			if (strServiceId.equals(packageRightsNext.get(key))) {
				packageRightsNext.remove(key);
			}
		} 
		// put service id if only it is authorized
		if (ret == MonitorService.AUTHORIZED) {
			packageRightsNext.put(packageName, strServiceId);
		}
		return ret == MonitorService.AUTHORIZED;
	}

	public boolean checkAuth(String callLetter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager checkAuth, " + callLetter);
		}
		if (/*caHandler == null || */epgService == null) {
			errorMessage("VOD201-04", null);
			Log.printWarning("CommunicationManager checkAuth, no epgService");
			return false;
		}
		if (callLetter == null || callLetter.length() == 0) {
			return false;
		}
		if (Environment.EMULATOR && ("CJPM".equals(callLetter) || "SE2".equals(callLetter) || "SE3".equals(callLetter)
				|| "SE4".equals(callLetter) || "TOONF".equals(callLetter) || "DISFR".equals(callLetter)
                || "YOOPA".equals(callLetter))) {
			return true;
		}
		try {
			TvChannel ch = getChannel(callLetter);
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager epgService.getChannel(), " + ch);
			}
			if (ch == null) {;
				return false;
			}
			if (Environment.EMULATOR) {
				return false;
			}
			boolean isSubscribed = ch.isSubscribed();
			boolean isAuthorizedByFreePreview = ch.isAuthorizedByFreePreview();
			boolean isSubscribedActually = isSubscribed && isAuthorizedByFreePreview == false;
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager letter = " + callLetter 
						+ ", srcid = " + ch.getSourceId() 
						+ ", isSubscribedActually = " + isSubscribedActually 
						+ ", isSubscribed = " + isSubscribed 
						+ ", isAuthorizedByFreePreview = " + isAuthorizedByFreePreview);
			}
			
			// R5
			// VDTRMASTER-5439
			return isSubscribed;
//			return isSubscribedActually;
//			CAAuthorization ret = caHandler.getSourceAuthorization((short) ch.getSourceId(), null);
//			boolean isAuth = ret.isAuthorized();
//			if (Log.DEBUG_ON) {
//				Log.printDebug("CommunicationManager src = " + ch.getSourceId() + ", isAuth = " + isAuth);
//			}
//			return isAuth;
		} catch (Exception e) {
			Log.print(e);
		}
		return false;
	}
	
	public boolean isPreviewChannel(String callLetter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager, isPreviewChannel, " + callLetter);
		}
		if (/*caHandler == null || */epgService == null) {
			errorMessage("VOD201-04", null);
			Log.printWarning("CommunicationManager, isPreviewChannel, no epgService");
			return false;
		}
		if (callLetter == null || callLetter.length() == 0) {
			return false;
		}
		if (Environment.EMULATOR && "CJPM".equals(callLetter)) {
			return true;
		}
		try {
			TvChannel ch = getChannel(callLetter);
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager, isPreviewChannel, epgService.getChannel(), " + ch);
			}
			if (ch == null) {
				return false;
			}
			if (Environment.EMULATOR) {
				return false;
			}
			boolean isAuthorizedByFreePreview = ch.isAuthorizedByFreePreview();
			if (Log.DEBUG_ON) {
				Log.printDebug("CommunicationManager letter = " + callLetter 
						+ ", srcid = " + ch.getSourceId() 
						+ ", isAuthorizedByFreePreview = " + isAuthorizedByFreePreview);
			}
			return isAuthorizedByFreePreview;
		} catch (Exception e) {
			Log.print(e);
		}
		return false;
	}
	
	public void resetLastErrorCode() {
		lastErrorCode = "";
	}
	public String getLastErrorCode() {
		return lastErrorCode;
	}

	public void errorMessage(String errorCode, ErrorMessageListener l) {
		errorMessage(errorCode, l, null, false);
	}

	public void errorMessage(String errorCode, ErrorMessageListener l,
			Hashtable param, boolean force) {
		lastErrorCode = errorCode;
		if (errorMessageService != null) {
			try {
				UsageManager.getInstance().addUsageLog("1111", errorCode, "", "");
				Log.printError("errorMessage, c = " + errorCode);
				Log.printDebug("errorMessage, s = " + showErrorMessage + ", f = " + force + ", l = " + l + ", p = " + param);
				if (showErrorMessage && (force || Resources.appStatus != VODService.APP_PAUSED)) {
					if (l == null) {
						if (MenuController.getInstance().getCurrentScene() instanceof PlayScreenUI) {
							l = ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).getErrorMessageListener();
						} else {
							l = errorListener;
						}
					}
					if (param == null) {
						errorMessageService.showErrorMessage(errorCode, l);
					} else {
						errorMessageService.showErrorMessage(errorCode, l, param);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void commonMessage(String commonCode, ErrorMessageListener l) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager, commonMessage=" + commonCode + ", listener=" + l);
		}
		try {
			if (l == null) {
				errorMessageService.showCommonMessage(commonCode, errorListener);
			} else {
				errorMessageService.showCommonMessage(commonCode, l);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public String debugLock = "";
	public void checkLock(final PlayScreenUI scene) {
		new Thread("checkLock") {
			public void run() {
				NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
				for (int i = 0; i < nis.length; i++) {
					Log.printDebug("checkLock(), nis["+i+"] = " + nis[i]);
				}
				String oid = null;
				ServiceContext sc = Environment.getServiceContext(0);
				Log.printDebug("checkLock(), sc = " + sc);
				if (sc != null && sc instanceof DvbServiceContext) {
					NetworkInterface ni = ((DvbServiceContext) sc).getNetworkInterface();
					if (ni == null) {
						debugLock = "NetworkInterface is not configured";
						Log.printDebug("checkLock(), ni is NULL");
						scene.notifyCheckLock(false);
						return;// false;
					}
					oid = ni.equals(nis[0]) ? ".1.3.6.1.4.1.4491.2.3.1.1.1.2.7.3.1.14.1" : ".1.3.6.1.4.1.4491.2.3.1.1.1.2.7.3.1.14.2";
					Log.printDebug("checkLock(), ni = " + ni + ", oid = " + oid);
				}
				MIBManager mgr = MIBManager.getInstance();
				MIBDefinition[] defs = mgr.queryMibs(oid);
				if (defs == null) {
					if (Log.DEBUG_ON) {
						debugLock = oid + ", defs is null";
						Log.printDebug("checkLock() defs is null.");
					}
					scene.notifyCheckLock(false);
					return;// false;
				}
				boolean ret;
				for (int i = 0; i < defs.length; i++) {
					MIBDefinition def = defs[i];
					if (def == null) {
						if (Log.DEBUG_ON) {
							debugLock = oid + ", def is null";
							Log.printDebug("checkLock() def-" + i + " is null.");
						}
						continue;
					}
					MIBObject obj = def.getMIBObject();
					if (obj == null) {
						if (Log.DEBUG_ON) {
							debugLock = oid + ", obj is null";
							Log.printDebug("checkLock() obj-" + i + " is null.");
						}
						continue;
					}
					byte[] mibObjectData = obj.getData();
					if (mibObjectData == null) {
						if (Log.DEBUG_ON) {
							debugLock = oid + ", mibObjectData is null";
							Log.printDebug("checkLock() mibObjectData-" + i + " is null.");
						}
						continue;
					}
					ret = mibObjectData[mibObjectData.length - 1] == 2;
					debugLock = oid + " = " + StringUtil.byteArrayToHexString(mibObjectData).replace('\n', '\t');
					Log.printDebug("checkLock() mibObjectData-" + i + " = " + StringUtil.byteArrayToHexString(mibObjectData) + ", ret = " + ret);
					scene.notifyCheckLock(ret);
					return;// ret;
				}
				scene.notifyCheckLock(false);
				return;// false;
			}
		}.start();
	}

	public boolean turnOnErrorMessage(boolean show) {
		Log.printDebug("turnOnErrorMessage, s = " + show);
//		Thread.dumpStack();
		boolean ret = showErrorMessage;
		showErrorMessage = show;
		return ret;
	}

	public void stopSearchApplication() {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager stopSearchApplication ");
		}
		if (searchService != null) {
			try {
				searchService.stopSearchApplication();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Request to the platform with Action.
	 *
	 * @param platform
	 *            platform
	 * @param action
	 *            action
	 * @throws RemoteException
	 *             The remote exception
	 */
	public SearchActionEventListener searchActionEventListener = new SearchActionEventListener() {
		public void actionRequested(String platform, String[] action)
				throws RemoteException {
			if (Log.DEBUG_ON) {
				Log.printDebug("SearchEventListener actionRequested platform, "
								+ platform);
				if (action != null) {
					for (int i = 0; i < action.length; i++) {
						Log.printDebug("action["+i+"] = " + action[i]);
					}
				}
			}
			if (action.length > 1 && "Search".equals(action[0])) {
				if (action.length < 3) {
					return;
				}
				
				if (SearchService.PLATFORM_VOD.equals(action[2])) {
					synchronized (MenuController.getInstance()) {
						MenuController.getInstance().setIsFromSearch(true);
						MenuController.getInstance()
								.goToAsset(action[1], true, MenuController.ASSET + action[1]);
						try {
							MenuController.getInstance().wait(1000);
						} catch (InterruptedException e) {
						}
					}
				} else if (SearchService.PLATFORM_KARAOKE.equals(action[2])) {
					Video song = (Video) CatalogueDatabase.getInstance().getCached(action[1]);
					if (song == null || BaseElement.NA.equals(song.getDescription())) {
						MenuController.getInstance().showLoadingAnimation();
						synchronized (CatalogueDatabase.getInstance()) {
							ArrayList list = new ArrayList();
							list.add(action[1]);
							CatalogueDatabase.getInstance().retrieveVideos(null, list, null);
							try {
								CatalogueDatabase.getInstance().wait(1000);
							} catch (InterruptedException e) {}
						}
						MenuController.getInstance().hideLoadingAnimation();
					}
					song = (Video) CatalogueDatabase.getInstance().getCached(action[1]);
					if (song != null) {
//						MenuController.getInstance().getPlaylist().add(song);
//						if (MenuController.getInstance().getCurrentScene() instanceof KaraokeListViewUI && ((KaraokeListViewUI) MenuController.getInstance().getCurrentScene()).isPlaylist()) {
//							((KaraokeListViewUI) MenuController.getInstance().getCurrentScene()).start(false);
//						} else {
//							MenuController.getInstance().refreshEffect();
//						}
						// play song, no add it to playlist
						if (CategoryUI.getPurchasedPartyPack() != null || CategoryUI.isTrialSong(song)) {
							ArrayList list = new ArrayList();
							list.add(song);
							MenuController.getInstance().goToNextScene(UITemplateList.PLAY_SCREEN, list);
							//MenuController.getInstance().setNeedSearch();
						} else {
							MenuController.getInstance().purchaseKaraoke(true);
						}
					}
				}
			}
		}
	};

	LogLevelChangeListener logLevelChangeListener = new LogLevelChangeListener() {
		public void logLevelChanged(int logLevel) throws RemoteException {
			Log.setStcLevel(logLevel);
		}
	};

	ErrorMessageListener errorListener = new ErrorMessageListener() {
		public void actionPerformed(int buttonType) throws RemoteException {
			String from = MenuController.getInstance().getFrom();
			boolean gotoAsset = MenuController.getInstance().getGotoAsset();
			
			Log.printDebug("ErrorMessageListener, button = " + buttonType + ", from = " + from + ", gotoAsset = " + gotoAsset);
			if (buttonType == ErrorMessageListener.BUTTON_TYPE_CLOSE) {
				if (gotoAsset
						&& MonitorService.REQUEST_APPLICATION_HOT_KEY.equals(from) == false 
						&& MonitorService.REQUEST_APPLICATION_LAST_KEY.equals(from) == false) {
					MenuController.getInstance().exit(MenuController.EXIT_BY_ERROR, null);
				} else if (MenuController.BANNER_CATEGORY.equals(from)){
					MenuController.getInstance().exit(MenuController.EXIT_BY_ERROR, null);
				} else if (!gotoAsset && MonitorService.REQUEST_APPLICATION_HOT_KEY.equals(from)
						&& (MenuController.getInstance().getCurrentScene() instanceof ListOfTitlesUI)) {
					MenuController.getInstance().backwardHistory(false);
					MenuController.getInstance().back();
					
				}
			}
		}
	};
	
	// id : call letter or service id
	HashSet isaChannel = new HashSet();
	HashSet isaPreview = new HashSet();
	HashSet isaSvc = new HashSet();

	String[] excludedChannels;
	int[] excludedChannelTypes;

	public boolean isaSupported(String id) {
		if (Environment.EMULATOR && "M".equals(id)) return true;
		if (isaChannel.contains(id)) {
			Log.printDebug("CommunicationManager, isaSupported(), id = " + id + ", supported CHANNEL");
			return true;
		}
		if (isaPreview.contains(id)) {
			Log.printDebug("CommunicationManager, isaSupported(), id = " + id + ", supported PREVIEW");
			return true;
		}
		if (isaSvc.contains(id)){ 
			Log.printDebug("CommunicationManager, isaSupported(), id = " + id + ", supported SERVICE");
			return true;
		}
		Log.printDebug("CommunicationManager, isaSupported(), id = " + id + ", not ISA");
		return false;
	}
	
	public void showISA(String serviceId, String entryPoint) {
		Log.printDebug("CommunicationManager, showISA, id = " + serviceId + ", entry = " + entryPoint);
		try {
			isaService.showSubscriptionPopup(serviceId, entryPoint);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	// Bundling
	public boolean isIncludedInExcludedChannels(String serviceId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager: isIncludedInExcludedChannels: serviceId=" + serviceId);
		}
		if (excludedChannels != null) {
			for (int i = 0; i < excludedChannels.length; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug(
							"CommunicationManager: setExcludedChannels: excludedChannels[" + i + "]=" + excludedChannels[i]);
				}
				if (excludedChannels[i].equals(serviceId)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isIncludedInExcludedChannelTypes(String serviceId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationManager: isIncludedInExcludedChannelTypes: serviceId=" + serviceId);
		}

		if (excludedChannelTypes != null) {
			TvChannel ch = null;
			try {
				ch = epgService.getChannel(serviceId);

				if (ch != null) {
					for (int i = 0; i < excludedChannelTypes.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug(
									"CommunicationManager: isIncludedInExcludedChannelTypes: ch.getType=" + ch.getType());
						}
						if (excludedChannelTypes[i] == ch.getType()) {
							return true;
						}
					}

				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	public boolean isIncludedInExcludedData(String serviceId) {
		if (isIncludedInExcludedChannels(serviceId) || isIncludedInExcludedChannelTypes(serviceId)) {
			return true;
		}

		return false;
	}
	
	ISAServiceDataListener isaListener = new ISAServiceDataListener() {

		public void updateISAServiceList(String isaServiceType, String[] serviceList) throws RemoteException {
			Log.printDebug("ISAServiceDataListener, isaServiceType = " + isaServiceType + ", list = " + (serviceList == null ? -1 : serviceList.length)); 
			if (ISAService.ISA_SERVICE_TYPE_CHANNEL.equals(isaServiceType)) {
				isaChannel.clear();
				for (int i = 0; serviceList != null && i < serviceList.length; i++) {
					isaChannel.add(serviceList[i]);
					Log.printDebug("ISAServiceDataListener, channel[" + i + "] = " + serviceList[i]);
				}
			} else if (ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW.equals(isaServiceType)) {
				isaPreview.clear();
				for (int i = 0; serviceList != null && i < serviceList.length; i++) {
					isaPreview.add(serviceList[i]);
					Log.printDebug("ISAServiceDataListener, preview[" + i + "] = " + serviceList[i]);
				}
			} else if (ISAService.ISA_SERVICE_TYPE_ILLICO_SERVICE.equals(isaServiceType)) {
				isaSvc.clear();
				for (int i = 0; serviceList != null && i < serviceList.length; i++) {
					isaSvc.add(serviceList[i]);
					Log.printDebug("ISAServiceDataListener, service[" + i + "] = " + serviceList[i]);
				}
			}
		}

		public void updateISAExcludedChannelsAndTypes(String[] exChannels, int[] excludedTypes)
				throws RemoteException {

			Log.printDebug("ISAServiceDataListener, updateISAExcludedChannelsAndTypes, excludedChannels = "
					+ exChannels + ", excludedTypes=" + excludedTypes);

			excludedChannels = exChannels;
			excludedChannelTypes = excludedTypes;
		}
	};
	
	CaUpdateListener caUpdateListener = new CaUpdateListener() {
		public void caUpdated(int sourceId, boolean authorized) throws RemoteException {
			Channel ch = CategoryUI.getChannel();
			Log.printDebug("CaUpdateListener, caUpdated, sourceId = " + sourceId + ", authorized = " + authorized + ", ch = " + ch);
			if (ch != null) {
				ch.hasAuth = CommunicationManager.getInstance().checkAuth(ch.callLetters);
			}
			if (MenuController.getInstance().getCurrentScene() != null) {
				Log.printDebug("CaUpdateListener, caUpdated, curScene = " + MenuController.getInstance().getCurrentScene());
				((BaseUI) MenuController.getInstance().getCurrentScene()).updateButtons();
			}
		}
	};
}

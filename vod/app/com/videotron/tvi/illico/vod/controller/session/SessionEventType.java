package com.videotron.tvi.illico.vod.controller.session;

/**
 * 
 * @author Administrator
 * 
 */
public interface SessionEventType {

    /** */
    public static final int SETUP_SUCCEEDED = 0x41;
    /** */
    public static final int SETUP_FAILED = 0x42;
    /** */
    public static final int SETUP_TIMEDOUT = 0x43;
    /** */
    public static final int SETUP_FAILED_IOE = 0x44;
    /** */
    public static final int RELEASE_SUCCEEDED = 0x47;
    /** */
    public static final int RELEASE_FAILED = 0x48;
    /** */
    public static final int RELEASE_TIMEDOUT = 0x49;
    /** */
    public static final int RELEASE_REQUIRED = 0x50;
    /** */
    public static final int SERVICEGROUP_SUCCEEDED = 0x60;
    /** */
    public static final int SERVICEGROUP_FAILED = 0x70;
    /** */
    public static final int SERVICEGROUP_CHANGED = 0x81; // 0x80 (original)
    /** */
    public static final int STREAM_CONTROL_FAILED = 0x90;
    
}

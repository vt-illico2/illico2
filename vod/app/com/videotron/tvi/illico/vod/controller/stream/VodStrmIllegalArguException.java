package com.videotron.tvi.illico.vod.controller.stream;

public class VodStrmIllegalArguException extends Exception {

    private static final long serialVersionUID = 2059397096061607677L;

    public VodStrmIllegalArguException() {

    }

    public VodStrmIllegalArguException(String reason) {
        super(reason);
    }
}

/*  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;

/**
 * @author LSC
 * @version 1.0 ClientSessionSetupRequest. This class handles the message that is sent from Client to the Network to
 *          request that a session be established with the requested serverId.
 */
public class ClientSessionSetupRequest extends SessionMessage implements ClientToNetworkMessage {

    /**
     * contains a value uniquely identifies the Client within the domain of the Network.
     * */
    private byte[] clientID = new byte[20];

    /**
     * contains a value which uniquely identifies the Server with which the Client is attempting to establish a session.
     * */
    private byte[] serverID = new byte[20];

    /**
     * contains uuData which is defined by the User-To-User portion.
     * */
    private byte[] userData;

    /**
     * to build client ID.
     * */
    private byte[] clientMAC;

    /**
     * to build server ID.
     * */
    private byte[] serverIP;

    /**
     * reserved.
     * */
    private byte[] reserved = { (byte) 0, (byte) 0 };

    /**
     * An ISO/iED 13818-6 Client Session Setup Request message. (section 4.2.4.1)
     */
    public ClientSessionSetupRequest() {
        setMessageID(SessionMessage.CLIENT_SESSION_SET_UP_REQUEST);
    }

    /**
     * Sets the MAC address of the STB client sending this message.
     * 
     * @param mac
     *            A 6 byte MAC address in an array.
     * @throws InvalidParameterException
     */
    public void setMAC(byte[] mac) {

        if (mac == null) {
            // Cannot set up session because MAC is null (POD not on line yet).
            return;
        }

        byte[] tid = getTransactionID().toByteArray();
        byte[] sessID = getSessionID();

        for (int i = 6; i < 10; i++) {
            sessID[i] = tid[i - 6];
        }

        clientMAC = mac;
        // Used to network byte order the MAC (MSB @ byte 0)
        
//        int byteNum = 5;
//
//        // The length of a mac is 6
//        for (int i = 0; i < 6 && i < mac.length; i++) {
//            clientID[13 + i] = mac[byteNum];
//            // The MAC is also used to create the sessionID. A previous or
//            // future
//            // call to setTransactinID() will or has set the other portion of
//            // the sessionID
//            sessID[i] = mac[byteNum--];
//        }
        
        int byteNum = 0;

        // The length of a mac is 6
        for (int i = 0; i < 6 && i < mac.length; i++) {
            clientID[13 + i] = mac[byteNum];
            // The MAC is also used to create the sessionID. A previous or
            // future
            // call to setTransactinID() will or has set the other portion of
            // the sessionID
            sessID[i] = mac[byteNum++];
        }
        // Per Paragraph 7.1.1.2.2 of sspVersion 2.3
        clientID[0] = 0x2D;
    }

    /**
     * Sets the IP address of the Mystro Server that will perform the session setup.
     * 
     * @param ip
     *            A 4 byte array containing the IP address of the server.
     */
    public void setServerIP(byte[] ip) {
        serverIP = ip;

        for (int i = 0; i < 4 && i < ip.length; i++) {
            serverID[i + 9] = ip[i];
        }
        // Per Paragraph 7.1.1.2.2 of sspVersion 2.3
        serverID[0] = 0x2D;

    }

    /**
     * Sets the UserData portion of the DSM-CC message as specified in the Pegasus sspVersion V2.3 Table 24.
     * 
     * @param d
     *            byte array containing the UserData
     */
    public void setUserData(byte[] d) {
        userData = d;
    }

    /**
     * Serializes this message for transmission by UDP to the SRM.
     * 
     * @return A byte array containing the entire message.
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        // Set the length of this message + padding bytes to make a multiple
        // of 4;
        setMessageLength(size() + 4);
        byte[] s = super.toByteArray();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();

        try {
            bo.write(s);
            bo.write(reserved);
            bo.write(clientID);
            bo.write(serverID);

            // Write the UU Data length.
            byte[] temp = new byte[2];

            bo.write(temp);

            // Write the private data length padded out to a multiple of 4
            // bytes.

            short length = (short) (userData.length + 4);

            temp[1] = (byte) (length & (short) 0x00FF);
            temp[0] = (byte) ((length & (short) 0xFF00) >> 8);
            bo.write(temp);

            bo.write(userData);

            temp[0] = (byte) 0x41;
            temp[1] = (byte) 0x54;
            bo.write(temp);
            temp[0] = (byte) 0x54;
            temp[1] = (byte) 0x53;
            bo.write(temp);
            rc = bo.toByteArray();

            bo.close();
            bo = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (bo != null) {
                try {
                    bo.close();
                    bo = null;
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return rc;
    }

    /**
     * Returns the size of the final message that is to be serialized.
     * 
     * @return Size of the serialized message.
     */
    public int size() {
        // Calcualte size (Only includes stuff past the DSMCC header so subtract
        // off 12 bytes
        // and add 6 more for the reserved and count fields.
        return super.size() + clientID.length + serverID.length + userData.length - 6;
    }

    /**
     * toString Gives human readable version of the message.
     * 
     * @return String
     */
    public String toString() {
        String rc = super.toString();
        StringBuffer sb = new StringBuffer(rc);
        sb.append("\nClient ID = \n");
        sb.append(StringUtil.byteArrayToHexString(clientID));
        sb.append("\nserver ID = \n");
        sb.append(StringUtil.byteArrayToHexString(serverID));
        sb.append("\nuser data = \n");
        sb.append(StringUtil.byteArrayToHexString(userData));
        rc = sb.toString();
        return rc;
    }

}

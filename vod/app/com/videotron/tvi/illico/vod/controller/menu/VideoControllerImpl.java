package com.videotron.tvi.illico.vod.controller.menu;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

import javax.tv.service.selection.ServiceContext;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.davic.net.tuning.NetworkInterface;
import org.davic.net.tuning.NetworkInterfaceEvent;
import org.davic.net.tuning.NetworkInterfaceListener;
import org.davic.net.tuning.NetworkInterfaceTuningOverEvent;
import org.dvb.service.selection.DvbServiceContext;
import org.ocap.net.OcapLocator;

import com.opencable.handler.cahandler.CAHandler;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.vod.VODEvents;
import com.videotron.tvi.illico.ixc.vod.VODServiceListener;
import com.videotron.tvi.illico.ixc.vod.VideoController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.vod.comp.PopTrailer;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.session.SessionConnector;
import com.videotron.tvi.illico.vod.controller.session.SessionConnectorImpl;
import com.videotron.tvi.illico.vod.controller.session.SessionEventType;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCResultParser;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionEvent;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionEventListener;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.ResRspCode;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.SessionProperties;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.CAResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.SessionResource;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.resource.UserPrivateData;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.controller.session.service.ServiceSelectionListener;
import com.videotron.tvi.illico.vod.controller.session.service.ServiceSelector;
import com.videotron.tvi.illico.vod.controller.stream.IVodStreamControl;
import com.videotron.tvi.illico.vod.controller.stream.IVodStreamControlListener;
import com.videotron.tvi.illico.vod.controller.stream.VodHandlerImpl;
import com.videotron.tvi.illico.vod.controller.stream.VodStrmInvaldNptException;
import com.videotron.tvi.illico.vod.controller.stream.VodStrmInvdScaleException;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamControlEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamEndOfStreamEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamFastForwardEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamPauseEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamPlayEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamResetEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamRewindEvent;
import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamStatusEvent;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;

public class VideoControllerImpl implements VideoController,
		DSMCCSessionEventListener, IVodStreamControlListener,
		ServiceSelectionListener {

	private final int STATE_INIT = 0;

	private final int STATE_SESSION_COMPLETE = 1;

	private final int STATE_VOD_CONTROL = 2;

	private int currentState = STATE_INIT;

	private static VideoControllerImpl instance;

	private SessionConnectorImpl dsmccMessenger;

	private VodHandlerImpl vodHandler;

	private SessionProperties sessionProp;

	private UserPrivateData userData;

	private int currentStreamMode = VODEvents.STOP;

	private int nextStreamMode;

	private DSMCCResultParser dsmccResultParser;

	private IVodStreamControl vodContorller;

	private VODServiceListener serviceListener;

	private int currentNPT = 0;

	private int maxNPT = 0;

	private DSMCCSessionEvent lastDSMCCSessionEvent;

	private String sessionName;

	private byte[] lastCA;
	private byte[] lastSessionID;
	private String lastLocator = "";
	private String scaleInfo = "";
	
	private long controlTime = 0;
	private boolean isActiveDDC090;
	private int lastStatus = 0;
	private boolean isPlaySlowMode;
	private VodStreamControlEvent lastStreamEvent;
	
	public static final int LSC_NO_PERMISSION = 0x14;

	private VideoControllerImpl() {
		isActiveDDC090 = DataCenter.getInstance().getBoolean("DDC_090");
	}

	public void init() {
//		sessionProp = new SessionProperties();
//		sessionProp.initProperties();
		dsmccMessenger = new SessionConnectorImpl();
		vodHandler = new VodHandlerImpl();
		dsmccMessenger.addDSMCCSessionEventListener(this);
		ServiceSelector.getInstance().addSelectionListener(this);
	}

	public void stop() {
		stop("");
//		if (Log.DEBUG_ON) {
//			Log.printDebug("VideoControllerImpl stop : "
//					+ sessionName + ", state = " + currentState);
//		}
//		if (currentState != STATE_INIT) {
//			sessionRelease(sessionName, ResRspCode.RsnClNormalRelease);
//			serviceListener = null;
//		}
//		dsmccMessenger.shutdown();
	}

	public static VideoControllerImpl getInstance() {
		if (instance == null) {
			instance = new VideoControllerImpl();
		}
		return instance;
	}

	public void sessionSetup(String sessionName) {
		if (Log.INFO_ON) {
			Log.printInfo("VideoControllerImpl sessionSetup : "
					+ sessionName);
		}
		this.currentNPT = 0;
		controlTime = 0;
		isPlaySlowMode = false;
		if (!ServiceGroupController.getInstance().isSgReady()) {
			if (Log.ERROR_ON) {
				Log.printError("VideoControllerImpl sessionSetup : stopped , Servie Group discovery isn't ready");
			}
			sendVODEvent(SessionEventType.SERVICEGROUP_FAILED);
			return;
		}
		this.sessionName = sessionName;
		try {
			SessionProperties.initProperties();
//			if (SessionProperties.clientMac == null) {
//				SessionProperties.clientMac = CommunicationManager
//						.getInstance().getCableCardMacAddress();
//			}
			if (SessionProperties.clientMac == null) {
				if (Log.ERROR_ON) {
					Log.printError("VideoControllerImpl sessionSetup : stopped , Fail to get CableCard MAC from monitor app");
				}
				sendVODEvent(VODEvents.NO_CABLE_CARD_MAC);
				return;
			}
			dsmccMessenger.initialize();
			dsmccMessenger.start();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		currentStreamMode = VODEvents.STOP;
		CAHandler caHandler = (CAHandler) DataCenter.getInstance().get(
				CAHandler.IXC_NAME);
		try {
			Log.printDebug("sessionSetup, try caHandler.clearACPackages()");
			caHandler.clearACPackages();
		} catch (Exception e) {
			Log.printWarning(e);
		}
		// CISCOGTWY-93 [ING-DEV-VID][ECLR][PHASE1][OS] TAKE LOGER TIME TO GET THE VIDEO IN THE G8+ VOD COMPARED TO G6
		if (Environment.TUNER_COUNT > 5) {
			final String sessionName2 = sessionName;
			new Thread() {
				public void run() {
					try {
						Log.printDebug("sessionSetup for G8, try dsmccMessenger.sessionRequest()");
						dsmccMessenger.sessionRequest(sessionName2);
					} catch (NullPointerException npe) { // datagramSocket can be null, SUPPORT 248
						sendVODEvent(SessionEventType.SETUP_FAILED_IOE);
						npe.printStackTrace();
					} catch (IOException e) {
						sendVODEvent(SessionEventType.SETUP_FAILED_IOE);
						e.printStackTrace();
					}
				}
			}.start();
			
		} else {
			try {
				Log.printDebug("sessionSetup, try dsmccMessenger.sessionRequest()");
				dsmccMessenger.sessionRequest(sessionName);
			} catch (NullPointerException npe) { // datagramSocket can be null, SUPPORT 248
				sendVODEvent(SessionEventType.SETUP_FAILED_IOE);
				npe.printStackTrace();
			} catch (IOException e) {
				sendVODEvent(SessionEventType.SETUP_FAILED_IOE);
				e.printStackTrace();
			}
		}
	}

	public void retrySessionRequest(String sessionName) {
		try {
			Log.printDebug("sessionSetup, retry dsmccMessenger.sessionRequest()");
			dsmccMessenger.sessionRequest(sessionName);
		} catch (NullPointerException npe) { // datagramSocket can be null, SUPPORT 248
			sendVODEvent(SessionEventType.SETUP_FAILED_IOE);
			npe.printStackTrace();
		} catch (IOException e) {
			sendVODEvent(SessionEventType.SETUP_FAILED_IOE);
			e.printStackTrace();
		}
	}

	public void sessionRelease(String sessionName, int reason) {
		String rs = dsmccMessenger.releaseRequest(sessionName, reason);
		Log.printDebug("sessionRelease, rs = " + rs);
		// dsmccMessenger.shutdown();

		byte[] sessionID = dsmccMessenger.getSessionID(sessionName);
		if (sessionID != null) {
			Log.printDebug("sessionRelease, try caHandler.freeACPackage()");
			try {
				CAHandler caHandler = (CAHandler) DataCenter.getInstance().get(
						CAHandler.IXC_NAME);
				caHandler.freeACPackage(sessionID);
			} catch (Exception e) {
				Log.printWarning(e);
			}
		}
	}
	
	public void playSlow(String sspID) {
		if (Log.DEBUG_ON) {
			Log.printDebug("playSlow vodContorller : " + vodContorller);
		}
		if (vodContorller == null) {
			return;
		}
		isPlaySlowMode = true;
		try {
			vodContorller.play(IVodStreamControl.NPT_NOW, IVodStreamControl.NPT_END,
					(short) 1, (short) 2, null);
		} catch (VodStrmInvdScaleException e) {
			e.printStackTrace();
		} catch (VodStrmInvaldNptException e) {
			e.printStackTrace();
		}
	}

	public void play(String sspID, int speed) {
		if (Log.DEBUG_ON) {
			Log.printDebug("play vodContorller = " + vodContorller + ", speed = " + speed);
		}
		if (vodContorller == null) {
			return;
		}
		isPlaySlowMode = false;
		if (speed > VODEvents.PLAY) {
			fastFoward(sspID, speed);
		} else if (speed < VODEvents.STOP) {
			rewind(sspID, speed);
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("play currentStreamMode : "
						+ currentStreamMode);
			}
			try {
				int npt;
				if (currentStreamMode == VODEvents.PAUSE) {
					npt = currentNPT;
				} else {
					npt = IVodStreamControl.NPT_NOW;
				}
				// currentStreamMode = speed;
				vodContorller.play(npt, IVodStreamControl.NPT_END,
						(short) speed, (short) 1, null);
			} catch (VodStrmInvdScaleException e) {
				e.printStackTrace();
			} catch (VodStrmInvaldNptException e) {
				e.printStackTrace();
			}
		}
	}

	public void play(String sspID, int speed, int npt) {
		if (Log.DEBUG_ON) {
			Log.printDebug("play2 currentStreamMode : "
					+ currentStreamMode + ", speed = " + speed + ", npt = "
					+ npt);
		}
		isPlaySlowMode = false;
		try {
			// currentStreamMode = speed;
			vodContorller.play(npt, IVodStreamControl.NPT_END, (short) speed,
					(short) 1, null);
		} catch (VodStrmInvdScaleException e) {
			e.printStackTrace();
		} catch (VodStrmInvaldNptException e) {
			e.printStackTrace();
		}
	}

	private void fastFoward(String sspID, int speed) {
		if (currentState != STATE_VOD_CONTROL) { // || currentStreamMode <
			// StreamConstants.STOP
			if (Log.DEBUG_ON) {
				Log.printDebug("fastFoward returned");
			}
			return;
		}
		isPlaySlowMode = false;
		nextStreamMode = speed;
		try {
			vodContorller.fastForward(IVodStreamControl.NPT_NOW,
					IVodStreamControl.NPT_END, (short) speed, (short) 1, null);
		} catch (VodStrmInvdScaleException e) {
			e.printStackTrace();
		} catch (VodStrmInvaldNptException e) {
			e.printStackTrace();
		}
	}

	private void rewind(String sspID, int speed) {
		if (currentState != STATE_VOD_CONTROL) { // || currentStreamMode >=
			// StreamConstants.PAUSE
			if (Log.DEBUG_ON) {
				Log.printDebug("rewind returned");
			}
			return;
		}
		isPlaySlowMode = false;
		nextStreamMode = speed;
		try {
			vodContorller.rewind(IVodStreamControl.NPT_NOW, 0, (short) speed,
					(short) 1, null);
		} catch (VodStrmInvdScaleException e) {
			e.printStackTrace();
		} catch (VodStrmInvaldNptException e) {
			e.printStackTrace();
		}
	}

	public void resume(String sspID, int currentNPT) {
		if (vodContorller == null) {
			return;
		}
	}

	public void jump(String sspID, int npt) {
		if (Log.DEBUG_ON) {
			Log.printDebug("jump vodContorller : " + vodContorller);
		}
		if (vodContorller == null) {
			return;
		}
		// try {
		// currentStreamMode = VODEvents.PAUSE;
		// vodContorller.play(maxNPT - beforeEnd, IVodStreamControl.NPT_END,
		// (short) 0, (short) 1, null);
		// vodContorller.pause(IVodStreamControl.NPT_NOW, null);
		// } catch (VodStrmInvaldNptException e) {
		// e.printStackTrace();
		// } catch (VodStrmInvdScaleException e) {
		// e.printStackTrace();
		// }
	}

	public void pause(String sspID) {
		if (Log.DEBUG_ON) {
			Log.printDebug("VideoControllerImpl pause(sspID) = " + sspID + ", sessionName = "
					+ sessionName + ", controller = " + vodContorller
					+ ", currentState = " + currentState);
		}
		if (vodContorller == null || currentState != STATE_VOD_CONTROL) {
			if (Log.DEBUG_ON) {
				Log.printDebug("pause returned");
			}
			return;
		}
		isPlaySlowMode = false;
		try {
			// currentStreamMode = VODEvents.PAUSE;
			vodContorller.pause(IVodStreamControl.NPT_NOW, null);
		} catch (VodStrmInvaldNptException e) {
			e.printStackTrace();
		}
	}

	public void stop(String sspID) {
		if (Log.DEBUG_ON) {
			Log.printDebug("VideoControllerImpl stop(sspID) = " + sspID + ", sessionName = "
					+ sessionName + ", controller = " + vodContorller
					+ ", currentState = " + currentState);
		}
		if (lastNetworkInterface != null) {
			lastNetworkInterface.removeNetworkInterfaceListener(networkListener);
			lastNetworkInterface = null;
		}
		isPlaySlowMode = false;
		if (sessionName == null) {
			Log.printDebug("current session is null");
			return;
		}
		try {
			if (currentState == STATE_VOD_CONTROL) {
                // currentStreamMode = VODEvents.STOP;
                if (vodContorller != null) {
                    vodContorller.pause(IVodStreamControl.NPT_NOW, null);
                }
                ServiceSelector.getInstance().stopService();
                currentState = STATE_INIT;
                sessionRelease(sessionName, ResRspCode.RsnClNormalRelease);

                // Nathalie said
                // "out time is very limited. My recommandation at this point is not to includ them in our R7.3 delivery"
                // below 3 line is commented out.
            } else if (currentState == STATE_SESSION_COMPLETE) { // VDTRMASTER-6050 & VDTRMASTER-6051
                currentState = STATE_INIT;
                sessionRelease(sessionName, ResRspCode.RsnClNormalRelease);
			} else {
				dsmccMessenger.shutdown();
				vodHandler.disconnect(null);
				sessionName = null;
			}
		} catch (VodStrmInvaldNptException e) {
			e.printStackTrace();
		}
	}

	public void queryStatus() {
		if (vodContorller == null) {
			return;
		}
		vodContorller.queryStatus(this);
	}

	public void streamTerminated() {
		if (vodContorller == null) {
			return;
		}
	}

	public void addVODServiceListener(VODServiceListener listener) {
		serviceListener = listener;
	}

	public void removeVODServiceListener(VODServiceListener listener) {
		serviceListener = null;
	}

	public void sendVODEvent(int eventCode) {
		if (serviceListener != null) {
			try {
				serviceListener.receiveVODEvent(eventCode);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * streamControlUpdateEvent Receives stream events.
	 *
	 * @param event
	 *            A subclassed a stream event.
	 *
	 * @see VodStreamPlayEvent
	 * @see VodStreamPauseEvent
	 * @see VodStreamFastForwardEvent
	 * @see VodStreamRewindEvent
	 * @see VodStreamStatusEvent
	 * @see VodStreamEndOfStreamEvent
	 * @see VodStreamResetEvent
	 */
	public void streamControlUpdateEvent(VodStreamControlEvent event) {
		int status = event.getEventStatus();
		Log.printDebug("streamControlUpdateEvent : " + event
				+ "  status : " + status);
		Log.printDebug("streamControlUpdateEvent currentStreamMode : "
						+ currentStreamMode);
		
		// DDC-090
		// correction for last streamMode
		if (controlTime != 0) {
			updateCurrentNPT();
		}
		controlTime = System.currentTimeMillis();
		lastStreamEvent = event;
		lastStatus = status;
		Log.printDebug("streamControlUpdateEvent controlTime : "
				+ controlTime);
		
		if (event instanceof VodStreamPlayEvent) {
			if (status == IVodStreamControl.STATUS_OK) {
				if (currentStreamMode != VODEvents.PLAY) {
					Log.printDebug("streamControlUpdateEvent selectService : "
									+ sessionName);
					OcapLocator[] loc = dsmccResultParser.getLocatorInformation(sessionName);
					ServiceSelector.getInstance().selectService(loc);
					if (loc != null && loc.length > 0) {
						lastLocator = loc[0].toExternalForm();
					} else {
						lastLocator = "NULL or 0-length";
					}
				}
				currentStreamMode = VODEvents.PLAY;
				changeStatus(STATE_VOD_CONTROL);
				currentNPT = event.getStreamNpt();
			} else {
				
				if (isActiveDDC090 && status != LSC_NO_PERMISSION) {
					CommunicationManager.getInstance().errorMessage("VOD603-"+status, null);
				} else if (!isActiveDDC090) {
					CommunicationManager.getInstance().errorMessage("VOD603-"+status, null);
				}
			}
		} else if (event instanceof VodStreamEndOfStreamEvent) {
			Log.printDebug(">> VodStreamEndOfStreamEvent : "
					+ "  currentStreamMode : " + currentStreamMode);
			switch (currentStreamMode) {
			case VODEvents.PLAY:
				currentStreamMode = VODEvents.END_OF_STREAM;
				break;
			case VODEvents.FF_SCALE_1:
			case VODEvents.FF_SCALE_2:
			case VODEvents.FF_SCALE_3:
			case VODEvents.FF_SCALE_4:
				currentStreamMode = VODEvents.END_OF_STREAM;
				break;
			case VODEvents.REW_SCALE_1:
			case VODEvents.REW_SCALE_2:
			case VODEvents.REW_SCALE_3:
			case VODEvents.REW_SCALE_4:
				currentStreamMode = VODEvents.RESUME;
				break;
			default:
				break;
			}
		} else if (event instanceof VodStreamPauseEvent) {
			// VDTRMASTER-5358 [VOD][R4.1+NBE][DDC-90] Vod client triggers a timeout on Control Server request
			if (currentState == STATE_INIT) {
				Log.printDebug("VideoControllerImpl, ignore a event cause currentState=STATE_INIT");
				return;
			}
			if (status == IVodStreamControl.STATUS_OK) {
				currentNPT = event.getStreamNpt();
				if (currentStreamMode == VODEvents.STOP) {
					sessionRelease(sessionName, ResRspCode.RsnClNormalRelease);
				}
				currentStreamMode = VODEvents.PAUSE;
			} else {
				if (isActiveDDC090 && status != LSC_NO_PERMISSION) {
					CommunicationManager.getInstance().errorMessage("VOD603-"+status, null);
				}
			}
		} else if (event instanceof VodStreamFastForwardEvent) {
			Log.printDebug("VodStreamFastForwardEvent, nextStreamMode = "
					+ nextStreamMode + ", scale = " + event.getStreamScaleNum() + ", denom = " + event.getStreamScaleDenom());
			if (status == IVodStreamControl.STATUS_OK) {
				currentStreamMode = nextStreamMode;
			} else {
				if (isActiveDDC090 && status != LSC_NO_PERMISSION) {
					CommunicationManager.getInstance().errorMessage("VOD603-"+status, null);
				} else if (!isActiveDDC090) {
					CommunicationManager.getInstance().errorMessage("VOD603-"+status, null);
				}
			}
		} else if (event instanceof VodStreamRewindEvent) {
			Log.printDebug("VodStreamRewindEvent, nextStreamMode = "
					+ nextStreamMode + ", scale = " + event.getStreamScaleNum() + ", denom = " + event.getStreamScaleDenom());
			if (status == IVodStreamControl.STATUS_OK) {
				currentStreamMode = nextStreamMode;
			} else {
				if (isActiveDDC090 && status != LSC_NO_PERMISSION) {
					CommunicationManager.getInstance().errorMessage("VOD603-"+status, null);
				} else if (!isActiveDDC090) {
					CommunicationManager.getInstance().errorMessage("VOD603-"+status, null);
				}
			}
		} else if (event instanceof VodStreamStatusEvent) {
			currentNPT = event.getStreamNpt();
			Log.printDebug("VodStreamStatusEvent, npt = "
					+ currentNPT + ", max = " + maxNPT);
			scaleInfo = "StreamEvent = " + event.getStreamNpt() + ", " + event.getStreamScaleNum() + "/" + event.getStreamScaleDenom() + ", " + status;
			sendVODEvent(VODEvents.STATUS);
			return;
		}
		scaleInfo = "StreamEvent = " + event.getStreamNpt() + ", " + event.getStreamScaleNum() + "/" + event.getStreamScaleDenom() + ", " + status;
		sendVODEvent(currentStreamMode);
	}

	public DSMCCSessionEvent getLastEvent() {
		return lastDSMCCSessionEvent;
	}

	public void receiveSRMEvent(DSMCCSessionEvent event) {
		parseSRMEvent(event);
	}

	public void parseSRMEvent(Object event) {
		if (event instanceof DSMCCSessionEvent) {
			DSMCCSessionEvent eventDSMCC = (DSMCCSessionEvent) event;
			lastDSMCCSessionEvent = eventDSMCC;
			if (Log.DEBUG_ON) {
				Log.printDebug("SessionManagerImpl::receiveSRMEvent ENTRY : "
								+ eventDSMCC.getResponseCode());
			}

			int responseCode = 0;
			int eventType = 0;

			DataInputStream dis = null;
			try {
				dsmccResultParser = new DSMCCResultParser();
				String name = null;
				byte[] uudat = eventDSMCC.getPrivateData();

				ByteArrayInputStream bais = null;

				// Not all messages will have private data. e.,g a session
				// timeout
				if (uudat != null) {
					bais = new ByteArrayInputStream(uudat);
					dis = new DataInputStream(bais);
				}

				responseCode = eventDSMCC.getResponseCode();
				eventType = eventDSMCC.getEventType();

				// Is this a session event?
				if ((eventType >= SessionEventType.SETUP_SUCCEEDED)
						&& (eventType <= SessionEventType.RELEASE_REQUIRED)) {

					// Next, read the length of the Session, then the Name into
					// a
					// byte array to convert it to a string.
					// byte[] temp = eventDSMCC.getSessionName();
					// name = new String(temp, 0, temp.length);

					switch (eventType) {
					case SessionEventType.SETUP_SUCCEEDED:
						dsmccResultParser.doSetupSucceeded(dis);
						// Private Data
						// Header
						changeStatus(STATE_SESSION_COMPLETE);
						if (vodContorller == null) {
							eventType = SessionEventType.STREAM_CONTROL_FAILED;
							break;
						}
						SessionResource[] resources = dsmccResultParser.getResources();
						for (int i = 0; i < resources.length; i++) {
							if (resources[i] instanceof CAResource) {
								CAResource ca = (CAResource) resources[i];
								CAHandler caHandler = (CAHandler) DataCenter
										.getInstance().get(CAHandler.IXC_NAME);
								try {
									lastSessionID = dsmccMessenger
											.getSessionID(sessionName);
									lastCA = ca.getCAInfo();
									Log.printDebug("VideoControllerImpl, try caHandler.setACPackage()"
													+ ", ca = "
													+ StringUtil
															.byteArrayToHexString(lastCA)
													+ ", sessionID = "
													+ StringUtil
															.byteArrayToHexString(lastSessionID));
									caHandler.setACPackage(lastCA,
											lastSessionID);
								} catch (Exception e) {
									Log.printWarning(e);
								}
								break;
							}
						}
						
						// commented by  https://issues.alticast.com/jira/browse/VDTRMASTER-5074 
						//VbmController.getInstance().writePlayback(true, sessionName);
						break;
					case SessionEventType.SETUP_FAILED:
						dsmccResultParser.doSetupFailed(dis, responseCode);
						break;
					case SessionEventType.SETUP_TIMEDOUT:
						dsmccResultParser.doSetupTimedout(dis, responseCode);
						break;
					/**
					 * Because the application called release, or the server
					 * released the session, an event is sent and the session is
					 * removed.
					 */
					case SessionEventType.RELEASE_SUCCEEDED:
						Log.printDebug("SessionManagerImpl::receiveSRMEvent ENTRY : RELEASE_SUCCEEDED");
						dsmccMessenger.shutdown();
						vodHandler.disconnect(null);
						dsmccResultParser.doReleaseSucceeded(dis);
						changeStatus(STATE_INIT);
						VbmController.getInstance().writePlayback(false, sessionName);
						sessionName = null;
						break;
					case SessionEventType.RELEASE_FAILED:
						Log.printDebug("SessionManagerImpl::receiveSRMEvent ENTRY : RELEASE_FAILED");
						dsmccMessenger.shutdown();
						vodHandler.disconnect(null);
						dsmccResultParser.doReleaseFailed(dis, responseCode);
						sessionName = null;
						break;
					case SessionEventType.RELEASE_TIMEDOUT:
						Log.printDebug("SessionManagerImpl::receiveSRMEvent ENTRY : RELEASE_TIMEDOUT");
						dsmccMessenger.shutdown();
						vodHandler.disconnect(null);
						dsmccResultParser.doReleaseTimedout(dis, responseCode);
						sessionName = null;
						break;
					case SessionEventType.RELEASE_REQUIRED:
						Log.printDebug("SessionManagerImpl::receiveSRMEvent ENTRY : RELEASE_REQUIRED");
						dsmccResultParser.doReleaseRequired(dis, responseCode);
						break;
					default:
						break;
					}
				}
			} catch (Exception ex) {
				Log.printError("SessionManagerImpl: Ouch!!! I am hosed.\n");
				Log.print(ex);
			} finally {
				lastStatus = IVodStreamControl.STATUS_OK;
				sendVODEvent(eventType);
				if (dis != null) {
					try {
						dis.close();
						dis = null;
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
				}
			}
		}
	}

	public String getCAInfo() {
		return "SessionID = "
				+ StringUtil.byteArrayToHexString(lastSessionID).trim()
				+ ", CA = " + StringUtil.byteArrayToHexString(lastCA).trim();
	}

	public String getLocatorInfo() {
		return lastLocator;
	}

	public String getScaleInfo() {
		return scaleInfo;
	}

	public void changeStatus(int state) {
		currentState = state;
		if (currentState == STATE_SESSION_COMPLETE) {
			boolean ready = setUserDataToVodHandler(dsmccResultParser);
			if (ready) {
				IVodStreamControl[] controllArr = getStreamConroller();
				vodContorller = controllArr[0];
				vodContorller.addListener(this);
			} else {
				// error case - vod603
			}
		}
	}

	public void notifyChannelSelectResult(int result) {
		if (Log.DEBUG_ON) {
			Log.printDebug("notifyChannelSelectResult::result : " + result + 
					", curScene = " + MenuController.getInstance().getCurrentScene());
		}
		if (result == ServiceSelectionListener.SELECT_FAIL) {
			UIComponent ui = MenuController.getInstance().getCurrentScene();
			if (ui instanceof PlayScreenUI) {
				((PlayScreenUI) ui).error("VOD214", null);
//			} else if (ui instanceof BaseUI) {
//				UIComponent popup = ((BaseUI) ui).getPopUp();
//				if (popup instanceof PopTrailer) {
//					((PopTrailer) popup).error("VOD214", null);
//				}
			} else {
				PopTrailer.getInstance().error("VOD214", null);
			}
		} else {
			// add
			ServiceContext sc = Environment.getServiceContext(0);
			Log.printDebug("notifyChannelSelectResult, sc = " + sc);
			if (sc != null && sc instanceof DvbServiceContext) {
				lastNetworkInterface = ((DvbServiceContext) sc).getNetworkInterface();
				lastNetworkInterface.addNetworkInterfaceListener(networkListener);
			}
		}
	}
	
	TVTimerSpec noSignalTimer;
	TVTimerWentOffListener noSignalTimeout = new TVTimerWentOffListener() {
		public void timerWentOff(TVTimerWentOffEvent arg0) {
			if (noSignalTimer != null) {
				TVTimer.getTimer().deschedule(noSignalTimer);
				noSignalTimer.removeTVTimerWentOffListener(noSignalTimeout);
				noSignalTimer = null;
			}
			Log.printDebug("VideoControllerImpl, noSignalTimer, try to show an error popup");
			UIComponent ui = MenuController.getInstance().getCurrentScene();
			if (ui instanceof PlayScreenUI) {
				((PlayScreenUI) ui).error("VOD218", null);
			} else {
				PopTrailer.getInstance().error("VOD218", null);
			}
		}
	};
	
	NetworkInterface lastNetworkInterface;
	NetworkInterfaceListener networkListener = new NetworkInterfaceListener() {
		public void receiveNIEvent(NetworkInterfaceEvent e) {
			Log.printDebug("VideoControllerImpl, receiveNIEvent(), " + e);
			if (e instanceof NetworkInterfaceTuningOverEvent) {
				NetworkInterfaceTuningOverEvent evt = (NetworkInterfaceTuningOverEvent) e;
				Log.printDebug("VideoControllerImpl, receiveNIEvent(), status = " + (evt.getStatus() == NetworkInterfaceTuningOverEvent.FAILED ? "FAILED" : "SUCCEEDED"));
				if (evt.getStatus() == NetworkInterfaceTuningOverEvent.FAILED) {
					if (noSignalTimer != null) {
						Log.printDebug("VideoControllerImpl, receiveNIEvent(), the timer is already scheduled");
						return;
					}
					noSignalTimer = new TVTimerSpec();
					noSignalTimer.setDelayTime(10 * Constants.MS_PER_SECOND);
					noSignalTimer.addTVTimerWentOffListener(noSignalTimeout);
					try {
						noSignalTimer = TVTimer.getTimer().scheduleTimerSpec(noSignalTimer);
					} catch (TVTimerScheduleFailedException exc) {}
					Log.printDebug("VideoControllerImpl, receiveNIEvent(), the timer is scheduled in 10 secs");
				} else {
					if (noSignalTimer != null) {
						TVTimer.getTimer().deschedule(noSignalTimer);
						noSignalTimer.removeTVTimerWentOffListener(noSignalTimeout);
						noSignalTimer = null;
						Log.printDebug("VideoControllerImpl, receiveNIEvent(), the timer is descheduled because the event is not 'failed'");
					} else {
						Log.printDebug("VideoControllerImpl, receiveNIEvent(), there is no scheduled timer");
					}
				}
			}
		}
	};

	public SessionConnector getSessionConnector() {
		return dsmccMessenger;
	}

	public boolean setUserDataToVodHandler(DSMCCResultParser data) {
		vodHandler.setUserPrivateData(data);
		return vodHandler.createStreamControls();
	}

	public IVodStreamControl[] getStreamConroller() {
		return vodHandler.getStreams();
	}

	/**
	 * @return the currentNPT
	 */
	public int getCurrentNPT() {
		if (Log.DEBUG_ON) {
			Log.printDebug("VideoControllerImpl getCurrentNPT : "
					+ currentNPT);
		}
		return currentNPT;
	}
	
	/**
	 * DDC-090
	 * This method make that VideoControllerImpl update currentNPT according to currentMode
	 */
	public void updateCurrentNPT() {
		if (Log.DEBUG_ON) {
			Log.printDebug("VideoControllerImpl, updateCurrentNPT, currentStreamMode = " + currentStreamMode + ", currentNPT=" + currentNPT + ", controlTime=" + controlTime);
		}
		long currentTime = System.currentTimeMillis();
		long gap = 0;
		int addTime = 0;
		switch (currentStreamMode) {
		case VODEvents.PLAY:
			gap = currentTime - controlTime;
			addTime = (int) gap;
			currentNPT += addTime;
			
			if (Log.DEBUG_ON) {
				Log.printDebug("VideoControllerImpl, updateCurrentNPT, gap = " + gap + ", addTime=" + addTime + ", currentNPT=" + currentNPT);
			}
			
			controlTime = currentTime;
			break;
		case VODEvents.PLAY_SLOW:
			gap = currentTime - controlTime;
			addTime = (int) (gap * 0.8);
			currentNPT += addTime;
			
			if (Log.DEBUG_ON) {
				Log.printDebug("VideoControllerImpl, updateCurrentNPT, gap = " + gap + ", addTime=" + addTime + ", currentNPT=" + currentNPT);
			}
			
			controlTime = currentTime;
			break;
		case VODEvents.FF_SCALE_1:
		case VODEvents.FF_SCALE_2:
		case VODEvents.FF_SCALE_3:
		case VODEvents.FF_SCALE_4:
		case VODEvents.REW_SCALE_1:
		case VODEvents.REW_SCALE_2:
		case VODEvents.REW_SCALE_3:
		case VODEvents.REW_SCALE_4:
			gap = currentTime - controlTime;
			addTime = (int) (gap * currentStreamMode);
			currentNPT += addTime;
			if (Log.DEBUG_ON) {
				Log.printDebug("VideoControllerImpl, updateCurrentNPT, gap = " + gap + ", addTime=" + addTime + ", currentNPT=" + currentNPT);
			}
			controlTime = currentTime;
			break;
		default:
			controlTime = currentTime;
			break;
		}
	}
	/**
	 * DDC-090
	 * @return
	 */
	
	public int getLastStatus() {
		return lastStatus;
	}
	
	public boolean isPlaySlowMode() {
		return isPlaySlowMode;
	}
	
	public VodStreamControlEvent getLastStreamEvent() {
		return lastStreamEvent;
	}
}

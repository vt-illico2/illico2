package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * UNClientReleaseConfirm.
 */
public class ClientSessionReleaseConfirm extends SessionMessage implements NetworkToClientMessage {
    private int response;

    private byte[] resourceAndPrivateData;

    /**
     *
     */
    public ClientSessionReleaseConfirm() {
        super();
    }

    /**
     * Parses a raw received DSMCC message into a UNClientReleaseConfirm object. Called by DSMCCMessenger
     * parseClientReleaseConfirm.
     *
     * @param raw
     *            byte array containing the DSMCC message received.
     */
    public void parse(byte[] raw) {
        ByteArrayInputStream bas = new ByteArrayInputStream(raw);
        DataInputStream in = new DataInputStream(bas);

        super.parse(in);

        try {
            response = in.readUnsignedShort();
            resourceAndPrivateData = new byte[in.available()];
            in.readFully(resourceAndPrivateData);

            in.close();
            in = null;
        } catch (Exception e) {
            Log.print(e);
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                    in = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }
    }

    /**
     *
     * Returns a byte array containing the resource data and private data.
     * Called by DSMCCMessenger parseClientReleaseConfirm.
     *
     * @return byte[]
     */
    public byte[] getResourceAndPrivateDataBytes() {
        return resourceAndPrivateData;
    }

    /**
     *
     * Response code getter. Called by DSMCCMessenger parseClientReleaseConfirm
     *
     * @return The respone code as defined in the DSMCC spec.
     */
    public int getResponse() {
        return response;
    }

    /**
     * toString.
     *
     * @return A String representation of the message.
     */
    public String toString() {
        StringBuffer buff = new StringBuffer("UNClientReleaseConfirm ");

        buff.append("\nResponse = ");
        buff.append(response);
        buff.append("\nPrivate Data Length = ");
        buff.append(resourceAndPrivateData.length);
        return buff.toString();
    }
}

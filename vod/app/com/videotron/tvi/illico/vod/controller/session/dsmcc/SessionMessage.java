/*
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;

/**
 * SessionMessage. This class is core structure of the protocol defined in sspVersion. Creates message type and header
 * and sets transaction id.
 *
 * @author LSC
 * @version 1.0
 */
public class SessionMessage extends DSMCCMessage {
    /**
     * These are the types of session messages that can be made. These values come from ISO/IEC 13818-6:1998(E)Table 4-5
     */
    public final static short CLIENT_SESSION_SET_UP_REQUEST = 0x4010;

    public final static short CLIENT_SESSION_SET_UP_CONFIRM = 0x4011;

    public final static short CLIENT_SESSION_RELEASE_REQUEST = 0x4020;

    public final static short CLIENT_SESSION_RELEASE_CONFIRM = 0x4021;

    public final static short CLIENT_SESSION_RELEASE_INDICATION = 0X4022;

    public final static short CLIENT_SESSION_RELEASE_RESPONSE = 0x4023;

    public final static short CLIENT_STATUS_REQUEST = 0x4060;

    public final static short CLIENT_STATUS_CONFIRM = 0x4061;

    public final static short CLIENT_STATUS_RESPONSE = 0x4063;

    public final static short CLIENT_SESSION_PROCEEDING_IND = 0x4082;

    public final static short CLIENT_SESSION_IN_PROGRESS = 0x40B0;

    private byte[] sessionID = new byte[10];

    /**
     * Default constructor.
     *
     */
    public SessionMessage() {
        setDSMCCType(UN_SESSION_MESSAGE);
    }

    /**
     * Copies the session ID from a byte array passed in. This method can also copy the session ID from an offset from
     * the beginning of the array. No bounds checking is done.
     *
     * @param id
     *            A byte array containing the serialized session ID.
     * @param offset
     *            The first location in the byte array in which the session ID starts.
     */
    public void setSessionID(byte[] id, int offset) {
        // Session ID always has 10 bytes.
        System.arraycopy(id, offset, sessionID, 0, 10);
    }

    /**
     * Get a byte array containing the session ID. The session ID consists of the MAC address concatinated with the
     * Transaction ID.
     *
     * @return A byte array containing the session ID.
     */
    public byte[] getSessionID() {
        return sessionID;
    }

    /**
     * Method to serialize this object to a byte array without using the built in Java serialization which tends to add
     * extra bytes.
     *
     * @return byte[] serialized version of this object
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        byte[] s = super.toByteArray();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();

        try {
            bo.write(s);
            bo.write(sessionID);

            rc = bo.toByteArray();

            bo.close();
            bo = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (bo != null) {
                try {
                    bo.close();
                    bo = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }
        return rc;
    }

    /**
     * Parse a UN Session message.
     *
     * @param di
     *            A DataInputStream object containing the UN Session type message.
     */
    public void parse(DataInputStream di) {
        super.parse(di);

        try {
            di.readFully(sessionID);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * @return The size of this object plus it's ancestor.
     */
    public int size() {
        return super.size() + sessionID.length;
    }

    public String toString() {
        String rc = super.toString();
        StringBuffer sb = new StringBuffer(rc);
        sb.append("\nSession ID = ");
        sb.append(StringUtil.byteArrayToHexString(sessionID));
        rc = sb.toString();
        return rc;
    }

}

package com.videotron.tvi.illico.vod.controller.session.dsmcc.util;

public class Unsigned {
    /*
     * * DESCRIPTION: converts a 4-byte int to network byte order and puts it into * the data array.
     */
    final public static int writeDWORD(char[] data, int offset, long value) {
        data[offset + 0] = (char) ((value >> 24) & 0xFF);
        data[offset + 1] = (char) ((value >> 16) & 0xFF);
        data[offset + 2] = (char) ((value >> 8) & 0xFF);
        data[offset + 3] = (char) (value & 0xFF);
        return 4;
    }

    final public static int writeMAC(char[] data, int offset, long value) {
        value = value & 0x0000FFFFFFFFFFFFL;

        data[offset + 0] = (char) ((value >> 40) & 0xFF);
        data[offset + 1] = (char) ((value >> 32) & 0xFF);
        data[offset + 2] = (char) ((value >> 24) & 0xFF);
        data[offset + 3] = (char) ((value >> 16) & 0xFF);
        data[offset + 4] = (char) ((value >> 8) & 0xFF);
        data[offset + 5] = (char) (value & 0xFF);

        /*
         * System.out.println("writeMAC: " + value + "\t" + Integer.toHexString((char)data[offset+0]) + " " +
         * Integer.toHexString((char)data[offset+1]) + " " + Integer.toHexString((char)data[offset+2]) + " " +
         * Integer.toHexString((char)data[offset+3]) + " " + Integer.toHexString((char)data[offset+4]) + " " +
         * Integer.toHexString((char)data[offset+5]) + " ");
         */
        return 6;
    }

    final public static int writeMAC(byte[] data, int offset, long value) {
        value = value & 0x0000FFFFFFFFFFFFL;

        data[offset + 0] = (byte) ((value >> 40) & 0xFF);
        data[offset + 1] = (byte) ((value >> 32) & 0xFF);
        data[offset + 2] = (byte) ((value >> 24) & 0xFF);
        data[offset + 3] = (byte) ((value >> 16) & 0xFF);
        data[offset + 4] = (byte) ((value >> 8) & 0xFF);
        data[offset + 5] = (byte) (value & 0xFF);

        /*
         * System.out.println("writeMAC: " + value + "\t" + Integer.toHexString((char)data[offset+0]) + " " +
         * Integer.toHexString((char)data[offset+1]) + " " + Integer.toHexString((char)data[offset+2]) + " " +
         * Integer.toHexString((char)data[offset+3]) + " " + Integer.toHexString((char)data[offset+4]) + " " +
         * Integer.toHexString((char)data[offset+5]) + " ");
         */
        return 6;
    }

    /*
     * * DESCRIPTION: converts a 2-byte int to network byte order and puts it into * the data array.
     */
    final public static int writeWORD(char[] data, int offset, int value) {
        data[offset + 0] = (char) ((value >> 8) & 0xFF);
        data[offset + 1] = (char) (value & 0xFF);
        return 2;
    }

    /*
     * * DESCRIPTION: adds a byte to the data array.
     */
    final public static int writeBYTE(char[] data, int offset, int value) {
        data[offset] = (char) (value & 0xFF);
        return 1;
    }

    /*
     * * DESCRIPTION: converts a 8-byte long to network byte order and puts it into * the data array.
     */
    final public static int writeQWORD(byte[] data, int offset, long value) {
        data[offset + 0] = (byte) ((value >> 56) & 0xFF);
        data[offset + 1] = (byte) ((value >> 48) & 0xFF);
        data[offset + 2] = (byte) ((value >> 40) & 0xFF);
        data[offset + 3] = (byte) ((value >> 32) & 0xFF);
        data[offset + 4] = (byte) ((value >> 24) & 0xFF);
        data[offset + 5] = (byte) ((value >> 16) & 0xFF);
        data[offset + 6] = (byte) ((value >> 8) & 0xFF);
        data[offset + 7] = (byte) ((value >> 0) & 0xFF);

        return 8;
    }

    /*
     * * DESCRIPTION: converts a 4-byte int to network byte order and puts it into * the data array.
     */
    final public static int writeDWORD(byte[] data, int offset, long value) {
        data[offset + 0] = (byte) ((value >> 24) & 0xFF);
        data[offset + 1] = (byte) ((value >> 16) & 0xFF);
        data[offset + 2] = (byte) ((value >> 8) & 0xFF);
        data[offset + 3] = (byte) (value & 0xFF);
        return 4;
    }

    /*
     * * DESCRIPTION: converts a 2-byte int to network byte order and puts it into * the data array.
     */
    final public static int writeWORD(byte[] data, int offset, int value) {
        data[offset + 0] = (byte) ((value >> 8) & 0xFF);
        data[offset + 1] = (byte) (value & 0xFF);
        return 2;
    }

    /*
     * * DESCRIPTION: adds a byte to the data array.
     */
    final public static int writeBYTE(byte[] data, int offset, int value) {
        data[offset] = (byte) (value & 0xFF);
        return 1;
    }

    /*
     * * DATE: (5/23/2002) * DESCRIPTION:
     */
    final public static short readBYTE(byte[] data, int offset) {
        short value = (short) (data[offset] & 0xFF);

        return value;
    }

    /*
     * * DATE: (5/23/2002) * DESCRIPTION:
     */
    final public static int readWORD(byte[] data, int offset) {
        int i = ((data[offset] & 0xFF) << 8) | (data[offset + 1] & 0xFF);
        return i;
    }

    /*
     * * DATE: (5/23/2002) * DESCRIPTION:
     */
    final public static long readDWORD(byte[] data, int offset) {
        long l = (((long) data[offset] & 0xFF) << 24) | ((data[offset + 1] & 0xFF) << 16)
                | ((data[offset + 2] & 0xFF) << 8) | (data[offset + 3] & 0xFF);
        return l;
    }

    /*
     * * DATE: (5/23/2002) * DESCRIPTION:
     */
    final public static long readQWORD(byte[] data, int offset) {
        long l = (((long) data[offset + 0] & 0xFF) << 56) | (((long) data[offset + 1] & 0xFF) << 48)
                | (((long) data[offset + 2] & 0xFF) << 40) | (((long) data[offset + 3] & 0xFF) << 32)
                | (((long) data[offset + 4] & 0xFF) << 24) | (((long) data[offset + 5] & 0xFF) << 16)
                | (((long) data[offset + 6] & 0xFF) << 8) | (((long) data[offset + 7] & 0xFF) << 0);
        return l;
    }

    final public static long readMAC(byte[] data, int offset) {
        // System.out.println("Called readMac with offset of " + offset);

        // System.out.println("readMAC " +
        // StringUtils.getHexStringBYTEArray(data,offset, 6));

        long low = (((long) data[offset + 2] & 0xFF) << 24) | ((data[offset + 3] & 0xFF) << 16)
                | ((data[offset + 4] & 0xFF) << 8) | (data[offset + 5] & 0xFF);

        long hi = (((long) data[offset + 0] & 0xFF) << 8) | ((data[offset + 1] & 0xFF));

        long l = ((hi << 32) | low);

        return l;
    }

}

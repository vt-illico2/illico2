package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.IOException;
import java.io.DataInputStream;

import com.videotron.tvi.illico.log.Log;

/**
 * Describes the channel used for tuning sessions. The channelId field is the QAM frequency in Hertz. The direction
 * field is Downstream.
 */
public class PhysicalChannelResource extends SessionResource {

    private short channel_id_resourceValueType; // /** For sspVersion 2.3 0x0001 */

    private int channel_id; // /**< For SSP2.3 the frequency in Hz at which the
    // stream in carried */

    private short direction; // /**< For SSP2.3 this field is always 0 */

    /**
     * @param rba
     *            The resource data
     */
    public PhysicalChannelResource(DataInputStream rba, SessionResource sr) {

        this.copy(sr);

        int messageLength = this.getFieldLength();

        try {
            channel_id_resourceValueType = rba.readShort();
            channel_id = rba.readInt();
            direction = rba.readShort();
            messageLength -= 8;

            if (messageLength > 0 && messageLength <= rba.available()) {
                rba.skip(messageLength);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("channel_id_resourceValueType : " + channel_id_resourceValueType);
                Log.printDebug("channel_id : " + channel_id);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Responds with the channel identifier.
     *
     * @return Returns the channel Id.
     */
    public int getChannelId() {
        return channel_id;
    }
}

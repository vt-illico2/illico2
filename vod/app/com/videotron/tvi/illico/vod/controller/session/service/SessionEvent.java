package com.videotron.tvi.illico.vod.controller.session.service;

public class SessionEvent {
    private int eventType;

    private int responseCode;

    private byte[] sessionName;

    private byte[] privateData;

    public int getEventType() {
        return responseCode;
    }

    public void setEventType(int t) {

    }

    public int getResponseCode() {
        return eventType;
    }

    public void setResponseCode(int r) {

    }

    public byte[] getSessionName() {
        return sessionName;
    }

    public void setSessionName(byte[] s) {

    }

    public void setPrivateData(byte[] privateData) {

    }

    public byte[] getPrivateData() {
        return privateData;
    }
}

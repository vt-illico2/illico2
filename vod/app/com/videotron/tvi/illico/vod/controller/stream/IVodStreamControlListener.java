package com.videotron.tvi.illico.vod.controller.stream;

import java.util.EventListener;

import com.videotron.tvi.illico.vod.controller.stream.event.VodStreamControlEvent;

/**
 * Implemented by Vod applications to receive stream control completion and notification events.
 * 
 * @see VodStreamControlEvent
 * @see VodStreamPlayEvent
 * @see VodStreamPauseEvent
 * @see VodStreamFastForwardEvent
 * @see VodStreamRewindEvent
 * @see VodStreamStatusEvent
 * @see VodStreamEndOfStreamEvent
 * @see VodStreamResetEvent
 * @see IVodStreamControl
 * @see IVodStreamControlListener
 */
public interface IVodStreamControlListener extends EventListener {
    
    /**
     * streamControlUpdateEvent Receives stream events.
     * 
     * @param event
     *            A sublassed a stream event.
     * 
     * @see VodStreamPlayEvent
     * @see VodStreamPauseEvent
     * @see VodStreamFastForwardEvent
     * @see VodStreamRewindEvent
     * @see VodStreamStatusEvent
     * @see VodStreamEndOfStreamEvent
     * @see VodStreamResetEvent
     */
    public void streamControlUpdateEvent(VodStreamControlEvent event);
}

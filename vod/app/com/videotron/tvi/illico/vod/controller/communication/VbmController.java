package com.videotron.tvi.illico.vod.controller.communication;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Vector;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Favourites;
import com.videotron.tvi.illico.vod.data.vcds.type.Subscribed;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.Unsubscribed;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.UITemplateList;

public class VbmController implements LogCommandTriggerListener {

    protected static VbmController instance = new VbmController();

    public static VbmController getInstance() {
        return instance;
    }

    public static boolean ENABLED = false;
    public static final String DEF_MEASUREMENT_GROUP = "-1";
    public static final String VALUE_SEPARATOR = "|";

    protected VbmService vbmService;
    protected HashSet idSet = new HashSet();
    protected Vector vbmLogBuffer;
    
    //R7.4 freelife Add right key vbm
    private String prevRightSelectionValue = null;
    
    //
    private MenuController menu;
    private String historyPath = "";
    private String previousPath = "";

    protected VbmController() {
    }

    public void init(VbmService service) {
        Log.printInfo("VbmController.init");
        vbmService = service;
        long[] ids = null;
        try {
            ids = vbmService.checkLogCommand(Resources.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        try {
            vbmService.addLogCommandChangeListener(Resources.APP_NAME, this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }

        if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmController: ids[" + i + "] = " + ids[i]);
                }
                idSet.add(new Long(ids[i]));
            }
        }

        vbmLogBuffer = (Vector) SharedMemory.getInstance().get(VbmService.VBM_SM_INTF);
        checkEmpty();
        
        historyPath = "";
        previousPath = "";
        
        if (vbmLogBuffer != null) {
            Log.printDebug("VbmController: found vbmLogBuffer");
        } else {
            Log.printDebug("VbmController: not found vbmLogBuffer.");
        }
    }

    public void dispose() {
        try {
            vbmService.removeLogCommandChangeListener(Resources.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }
    
    public void setMenuController(MenuController _menu) {
    	menu = _menu;
    }

    protected void checkEmpty() {
        ENABLED = vbmLogBuffer != null && !idSet.isEmpty();
    }

    public boolean write(long id, String group, String[] values) {
        if (!ENABLED) {
            return false;
        }
        if (!idSet.contains(new Long(id))) {
            return false;
        }
        if (group == null) {
            group = DEF_MEASUREMENT_GROUP;
        }
        StringBuffer sb = new StringBuffer(80);
        sb.append(id);
        sb.append(VbmService.SEPARATOR);
        sb.append(group);

        if (values != null && values.length > 0) {
            sb.append(VbmService.SEPARATOR);
            sb.append(values[0]);
            for (int i = 1; i < values.length; i++) {
                sb.append(VALUE_SEPARATOR);
                sb.append(values[i]);
            }
        }
        String str = sb.toString();
        if (Log.DEBUG_ON) {
            Log.printDebug("VbmController.write: " + str);
        }
        try {
            vbmLogBuffer.addElement(str);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return true;
    }


	/**
	 * event invoked when logLevel data updated.
	 * it is only applied to action measurements
	 *
	 *
	 * @param Measurement to notify measurement an frequencies
	 * @param command - true to start logging, false to stop logging
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
        Log.printInfo("VbmController.logCommandChanged: " + measurementId + ", " + command);
        if (command) {
            idSet.add(new Long(measurementId));
        } else {
            idSet.remove(new Long(measurementId));
        }
        checkEmpty();
    }

	/**
	 * event invoked when log of trigger measurement should be collected
	 *
	 * @param measurementId trigger measurement
	 *
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	public void triggerLogGathering(long measurementId) {
        Log.printInfo("VbmController.triggerLogGathering: " + measurementId);
    }
	
	/**
	 * 20190110 R7.4+Touche project
	 * Need to verify with old issues
	 * - VDTRMASTER-6185, 6186 
	 * @param value
	 * @param pCategory
	 */
	public void writeSelectionAddHistory(String value) {
		historyPath = "";
		writeSelectionAddHistory(value, null);
	}
	public void writeSelectionAddHistory(String value, CategoryContainer pCategory) {
		if (Log.INFO_ON) {
			Log.printInfo("VbmController.writeSelectionAddHistory: value " + value);
			Log.printInfo("VbmController.writeSelectionAddHistory: pCategory " + pCategory);
		}
		
		try {
			/**
			 * check history
			 */
			// 1: if it is top category, clear all history
			if (pCategory != null && pCategory.getTopic() != null && 
				"VSD".equals(pCategory.getTopic().type) && 
				"TO_1".equals((pCategory.getTopic()).id)) {		
				historyPath = "";
				doubleHistory = "";
			} else {
				// 2: if there is the same value on the last history, nothing to do
				if (historyPath.lastIndexOf(value) != -1 && 
						historyPath.lastIndexOf(value) == historyPath.length() - value.length()) {
					value = "";
				}
			}
			
			if (historyPath.length() > 0 && value.length() > 0)
				historyPath += "^" + value;
			else if (historyPath.length() == 0)
				historyPath = value;
				
			String _path = getPath();
			if (!_path.equals(previousPath)) {
				if (Log.INFO_ON) {
					Log.printInfo("VbmController.writeSelectionAddHistory: getPath() " + _path);
					//Log.printInfo("VbmController.writeSelectionAddHistory: historyPath " + historyPath);
				}
				write(1002000001, null, new String[] { _path });
			}
			
			previousPath = _path;
		} catch (Exception e) {
			
		}
	}
	
	/**
	 *  VDTRMASTER-6244
	 * ShowCaseView에서 CategoryContainer가 선택된경우 두번의 히스토리가 add 된다. 
	 * 이때 backwardHistory로 두번의 히스토리를 모두 지워주기 위한 처리 
	 */
	String doubleHistory = "";
	public void writeSelectionAddDoubleHistory(String value, CategoryContainer pCategory) {
		
		writeSelectionAddHistory(value, pCategory);
		
		doubleHistory = previousPath;
	}
	
	private String getPath() {
		int idx = historyPath.indexOf("^@@@");
        String newPath = historyPath;
        while(idx > -1) {
            newPath =  newPath.substring(0, idx) + newPath.substring(idx+4);
            idx = newPath.indexOf("^@@@");
        }
        return newPath;
	}
	
	public void resetHistoryPath() {
		if (Log.INFO_ON)
			Log.printInfo("VbmController.resetHistoryPath");
		previousPath = "";
		historyPath = "";
	}
	
	/**
	 * 현재 back키 입력 시 항상 backwardHistory를 호출하여 history를 하나씩 지워준다.
	 * 그런데 ok 키로 next depth로 이동하는 경우 writehistory를 남기지 않는 경우는 back key에 의해서 지워지지 말아야할 history가 지워지게 된다. 
	 * 때문에 ok 키로 next depth로 이동할때는 무조건 history를 만들기 위해서 이 함수를 호출한다. 
	 * 실제 vbm에 데이터를 기록할때 dummy를 제거하는 코드도 추가되었다. (getRemovedDummyPath) 
	 */
	public void writeSelectionDummy() {
		if (historyPath.length() > 0)
			historyPath += "^@@@";
		
		//if (Log.INFO_ON)
		//	Log.printInfo("VbmController.writeSelectionDummy: " + historyPath);
	}
	
	public void removeDummy() {
		int idx = historyPath.lastIndexOf("^@@@");        
        if(idx > -1) {
        	historyPath =  historyPath.substring(0, idx);            
        }
	}
	
	public void backwardHistory() {
		if (menu != null && menu.isFromSearch())
			return;
		
		if (Log.INFO_ON)
			Log.printInfo("VbmController.backwardHistory");
		
		if (historyPath.length() > 0 && historyPath.lastIndexOf("^") > -1) {
			historyPath = historyPath.substring(0,historyPath.lastIndexOf("^"));
		
			if (doubleHistory.length() > 0 && doubleHistory.equals(historyPath)) {
				backwardHistory();
				doubleHistory = "";
			}
		}
	}
	
	public void writeSelection(String value) {
				
		String path = getPath();
		
		// VDTRMASTER-6253
		if (menu != null && menu.isFromSearch())
			path = "";
		
		// VDTRMASTER-6243
		if (path != null && path.length() != 0)
			path = path + "^";
		
		if (Log.INFO_ON)
			Log.printInfo("VbmController.writeSelection: " + path + value);
		write(1002000001, null, new String[] { path + value });
	}

	public void writeSelectionNoHistory(String value) {
		if (Log.INFO_ON)
			Log.printInfo("VbmController.writeSelectionNoHistory: " + value);
		write(1002000001, null, new String[] { value });
	}
	
	/*
	//R7.4 freelife Add right key action
	public void writeRightSelection(String value) {
		if (value != null && prevRightSelectionValue != null && 
				value.equals(prevRightSelectionValue)) { 
			
			if (Log.DEBUG_ON)
				Log.printDebug("VbmController.writeRightSelection: the smae value " + value);
			return;
		}
		prevRightSelectionValue = value;
		write(1002000001, null, new String[] { value });
	}
	*/
	public void writeHotKey(String keyString) {
		write(1002000002, null, new String[] { keyString });
	}
	
	public void writeLayoutChange(int sceneCode) {
		String value = null;
		switch (sceneCode) {
		case UITemplateList.LIST_OF_TITLES:
			value = "Normal";
			break;
		case UITemplateList.WALL_OF_POSTERS:
			value = "Grid";
			break;
		case UITemplateList.CAROUSEL_VIEW:
			value = "Carousel";
			break;
		case UITemplateList.LIST_VIEW:
			value = "List";
			break;
		default:
			break;
		} 
		write(1002000003, null, new String[] { value });
	}

	public void writePurchase(String sspId) {
		write(1002000004, sessionStr, new String[] { sspId });	
	}

	long playSession = -1;
	public void writePlayback(boolean start, String sspId) {
		String value = null;
		if (start) {
			playSession = System.currentTimeMillis();
			value = /*session + "_" + playSession + "_" +*/ sspId;
			write(1002000005, sessionStr, new String[] { value });
		} else {
			if (playSession == -1) {
				return;
			}
			value = /*session + "_" + playSession + "_" +*/ sspId;
			write(1002000006, sessionStr, new String[] { value });
			playSession = -1;
		}
	}

	long session;
	String sessionStr;
	
	public void writeSession(boolean start, int way) {
		/**
	     * R7.4+Touche
	     */
		// VDTRMASTER-6253
		//historyPath = "";
		write(start ? 1002000007 : 1002000008, sessionStr, new String[] { String.valueOf(session) });	
	}
	
	public void writeParentApp(String parent) {
		session = System.currentTimeMillis();
		sessionStr = String.valueOf(session);
		write(1002000009, sessionStr, new String[] { parent });
	}
	
	// R5
	public void writeAddFavoriteChannel(String callLetter) {
		Log.printDebug("VbmController, writeAddFavoriteChannel, callLetter=" + callLetter);
		write(1002000010, null, new String[] { callLetter });
	}
	
	// R5
	public void writeAddFavoriteChannel(String[] callLetters) {
		Log.printDebug("VbmController, writeAddFavoriteChannel, callLetters=" + callLetters.length);
		if (callLetters != null) {
			for (int i = 0; i < callLetters.length; i++) {
				Log.printDebug("VbmController, writeAddFavoriteChannel, callLetters[" + i + "]=" + callLetters[i]);
			}
		}
		write(1002000010, null, callLetters);
	}
	
	// R5
	public void writeChannelSortOption(String option) {
		Log.printDebug("VbmController, writeChannelSortOption, option=" + option);
		write(1002000011, null, new String[] { option });
	}
	
	// R5
	public void writeSimilarContent(String id) {
		Log.printDebug("VbmController, writeSimilarContent, id=" + id);
		write(1002000012, null, new String[] { id });
		
		// 20192022 Touche
		writeSelectionDummy();
	}
	
	// R5
	public void writeEnterTitleDetailUI() {
		Log.printDebug("VbmController, writeEnterTitleDetailUI, sessionStr=" + sessionStr);
		write(1002000013, null, new String[] { sessionStr });
	}
	
	// R5
	public void writeExitedWithBack() {
		Log.printDebug("VbmController, writeExitedWithBack, sessionStr=" + sessionStr);
		write(1002000014, null, new String[] { sessionStr });
	}
	
	// R7
	public void writeSimilarPanelOpened(String contentId) {
		Log.printDebug("VbmController, writeSimilarPanelOpened, contentId=" + contentId);
		write(1002000015, null, new String[] { contentId });
	}
}


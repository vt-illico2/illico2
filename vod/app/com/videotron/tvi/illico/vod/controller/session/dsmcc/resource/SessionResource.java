package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * This class is the base class for all session resources classes.
 *
 * @see ATSCModulationModeResource
 * @see CAResource
 * @see IPResource
 * @see MPEGProgramResource
 * @see PhysicalChannelResource
 * @see TSIDResource
 */
public class SessionResource {
    private byte[] m_baRef = null;

    public static final int MPEGProgram = 0x0003;

    /** < Provide MPEG Program number. */
    public static final int PhysicalChannel = 0x0004;

    /** Describes the channel used for tuning sessions. The channelID field is the QAM frequency in Hertz. */
    public static final int TSDownStream = 0x0006;

    /** Connection parameters throught the network and conveys inband bandwidth and transport stream Id. */
    public static final int IPDescriptor = 0x0009;

    public static final int ATSCModulationMode = 0xF001;

    /** As specified in Session Setup Protocol Version 2.1 . */
    public static final int Reserved = 0xF002;

    /** As specified in Session Setup Protocol Version 2.1 . */
    public static final int HeadEndIdDescriptor = 0xF003;

    /** point of destribution to the access network. */
    public static final int ServerConditionalAccess = 0xF004;

    /** server conditional access resources. */
    public static final int ClientConditionalAccess = 0xF005;

    /** < Client conditional access resources. */

    private short mReqId;

    /** resource request Id assigned by headend components. */
    private short descType;

    /** indicates the type of DSM-CC or User-Defined resource. */
    private short resNum;

    /** uniquely identifies a resource in the clients view. */
    private short assocTag;

    /** Specifies which resources are grouped together in a view. */
    private short dataFldlen;

    /** as defined by ISO/IEC 13818-6. */
    private short dataFldcnt;

    /** as defined by ISO/IEC 13818-6. */
    private byte m_flags;

    /** as defined by ISO/IEC 13818-6. */
    private byte m_status;

    /** as defined by ISO/IEC 13818-6. */
    private byte[] m_typeOwnerID = new byte[3];

    /** as defined by ISO/IEC 13818-6. */
    private byte[] m_typeOwnerValue = new byte[3];

    /** as defined by ISO/IEC 13818-6 */

    /**
     * The super class for all session resource types.
     *
     * @param ba
     *            The raw resource byte array.
     */
    public SessionResource(byte[] ba) {
        m_baRef = ba;
        ByteArrayInputStream bais = new ByteArrayInputStream(m_baRef);
        DataInputStream dis = new DataInputStream(bais);

        try {
            mReqId = dis.readShort();
            descType = dis.readShort();
            resNum = dis.readShort();
            assocTag = dis.readShort();
            m_flags = dis.readByte();
            m_status = dis.readByte();
            dataFldlen = dis.readShort();
            dataFldcnt = dis.readShort();
            if (descType == 0xFFFF) {
                m_typeOwnerID[0] = dis.readByte();
                m_typeOwnerID[1] = dis.readByte();
                m_typeOwnerID[2] = dis.readByte();
                m_typeOwnerValue[0] = dis.readByte();
                m_typeOwnerValue[1] = dis.readByte();
                m_typeOwnerValue[2] = dis.readByte();
            }

            dis.close();
            dis = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (dis != null) {
                try {
                    dis.close();
                    dis = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }

    }

    /**
     * The super class for all session resource types.
     *
     * @param dis
     *            DataInputStream from which to read session resource fields
     */
    public SessionResource(DataInputStream dis) {

        try {
            mReqId = dis.readShort();
            descType = dis.readShort();
            resNum = dis.readShort();
            assocTag = dis.readShort();
            m_flags = dis.readByte();
            m_status = dis.readByte();
            dataFldlen = dis.readShort();
            dataFldcnt = dis.readShort();
            if ((descType & 0xFFFF) == 0xFFFF) {
                dis.readFully(m_typeOwnerID);
                dis.readFully(m_typeOwnerValue);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }

    }

    public void copy(SessionResource ses) {
        this.mReqId = ses.mReqId;
        this.descType = ses.descType;
        this.resNum = ses.resNum;
        this.assocTag = ses.assocTag;
        this.assocTag = ses.assocTag;
        this.m_flags = ses.m_flags;
        this.m_status = ses.m_status;
        this.dataFldlen = ses.dataFldlen;
        this.dataFldcnt = ses.dataFldcnt;
        this.m_typeOwnerID = ses.m_typeOwnerID;
        this.m_typeOwnerValue = ses.m_typeOwnerValue;
    }

    public SessionResource() {
    }

    /**
     * getResourceDescriptorType Responds with the resource descriptor type.
     *
     * @return the resource descriptor type.
     */
    public int getResourceDescriptorType() {
        return (0xFFFF & descType);
    }

    /**
     *  Responds with the resource tag.
     *
     * @return The resource tag.
     */
    public int getResourceTag() {
        return 0;
    }

    /**
     * Responds with the resource number.
     *
     * @return The resource number.
     */
    public int getResourceNum() {
        return (0xFFFF & resNum);
    }

    /**
     * Responds with the m_status of the resource.
     *
     * @return The m_status.
     */
    public int getResourceStatus() {
        return (0xFF & m_status);
    }

    /**
     * Responds with the raw resource data.
     *
     * @return the resource data.
     */
    public byte[] getData() {
        return m_baRef;
    }

    /**
     * the length of the data after the header. SessionResource
     *
     * @return length of the field
     */
    public int getFieldLength() {
        return dataFldlen;
    }

    /**
     *
     * Translate resource type to a readable string for logging.
     *
     * @param type
     * @return The string representation of the type.
     */
    public static String sessionResourceTypeToString(int type) {
        switch (type) {
        case MPEGProgram:
            return "MPEGProgram";
        case PhysicalChannel:
            return "PhysicalChannel";
        case TSDownStream:
            return "TSDownStream";
        case IPDescriptor:
            return "IPDescriptor";
        case ATSCModulationMode:
            return "ATSCModulationMode";
        case Reserved:
            return "Reserved";
        case HeadEndIdDescriptor:
            return "HeadEndIdDescriptor";
        case ServerConditionalAccess:
            return "ServerConditionalAccess";
        case ClientConditionalAccess:
            return "ClientConditionalAccess";
        default:
            return "Unknown type: " + Integer.toHexString(type);
        }
    }
}

package com.videotron.tvi.illico.vod.controller.session.dsmcc.resource;

import java.io.DataInputStream;

import com.videotron.tvi.illico.log.Log;

/**
 * <code>IPResource</code> Used to convey IP address of the service access point for stream operations.
 *
 * @see MPEGProgramResource
 * @see ATSCModulationModeResource
 * @see IPResource
 * @see SessionResource
 * @see PhysicalChannelResource
 * @see TSIDResource
 */
public class IPResource extends SessionResource {
    private int srcIPAddr;

    /** < Source IP address. For SSP21 0 is used */
    private int dstIPAddr;

    /** < Destination IP address. For SPP21, designates the stream server address */
    private int srcIPPort;

    /** < Source IP port. For SSP21 0 is used. */
    private int dstIPPort;

    /** < Destination IP port. For SSP21, designates the stream server port. */
    private short protocol;

    /**
     * The constructor for this resource.
     *
     * @param rba
     *            The raw resource data.
     */
    public IPResource(DataInputStream rba, SessionResource sr) {
        // super(rba);
        this.copy(sr);
        try {
            srcIPAddr = rba.readInt();
            srcIPPort = rba.readUnsignedShort();
            dstIPAddr = rba.readInt();
            dstIPPort = rba.readUnsignedShort();

            if (Log.DEBUG_ON) {
                Log.printDebug("dstIPAddr : " + dstIPAddr);
                Log.printDebug("dstIPPort : " + dstIPPort);
            }
        } catch (Exception ex) {

        }
    }

    /**
     * Responds with the destination ip.
     *
     *
     * @return The destination ip.
     */
    public int getDestinationIp() {
        return dstIPAddr;
    }

    /**
     * Responds with the destination port.
     *
     *
     * @return The destination port.
     */
    public int getDestinationPort() {
        return dstIPPort;
    }

    /**
     * Responds with the protocol.
     *
     *
     * @return The protocol.
     */
    public int getProtocol() {
        return 0;
    }
}

/*  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * @author LSC
 * @version 1.0 DSMCCMessage. This class is core structure of the protocol defined in sspVersion. Creates message type
 *          and header and sets transaction id.
 */
public class DSMCCMessage {
    // These are the types of messages we will be dealing with.
    // 0x06- 0x7F are reserved by ISO/IEC 13818-6
    // 0x80-0xFF are user defined message types.

    /** User-to-Network configuration message. */
    public static final byte UN_CONFIG_MESSAGE = (byte) 0x01;

    /** User-to-Network session message. */
    public static final byte UN_SESSION_MESSAGE = (byte) 0x02;

    /** Download message. */
    public static final byte DOWNLOAD_MESSAGE = (byte) 0x03;

    /** SDB Channel Change Protocol message. */
    public static final byte SDB_CHANNEL_CHANGE_PROTOCOL_MESSAGE = (byte) 0x04;

    /** User-to- Network pass-thru message. */
    public static final byte UN_PASSTHROUGH_MESSAGE = (byte) 0x05;

    /** number of bytes before the variable length adaptation field - if present. */
    public static final int INVARIANT_HEADER_SIZE = 12;

    /** This will always be 0x11 indicating this is an MPEG-2 DSMCC message. */
    public static final byte PROTOCOL_DISCRIMINATOR = (byte) 0x11;

    /** contains protocol discriminator. */
    private byte protocolDiscriminator = PROTOCOL_DISCRIMINATOR;

    /** contains message type. */
    private byte dsmccType = UN_SESSION_MESSAGE;

    /** DSMCC Message Type. */
    private short messageID;

    /** A unique number for this particular message but with a definite format. */
    private TransactionID transactionID = null;

    /** Always 0xFF per ISO/IEC1318-6:1998(E). */
    private byte reserved = (byte) 0xFF;

    /**
     * The length of the adaptation header. if it is included. Else 0.
     */
    private byte adaptationLength = (byte) 0;

    /** The length of the entire message from adaptation length forward. */
    private short messageLength;

    /** The adaptation header. */
    private AdaptationHeader adaptationHeader = null;

    /**
     * set protocol discripminator.
     *
     * @param pd
     *            The protocol descriptor to use for this message.
     */
    public void setProtocolDiscriminator(byte pd) {
        protocolDiscriminator = pd;
    }

    /**
     * set DSMCC Type.
     *
     * @param type
     *            of message we are building.
     */
    public void setDSMCCType(byte type) {
        dsmccType = type;
    }

    /**
     * Sets the adaptation header length. The default is 0 if this method is never called. Per Cisco, no adaptation data
     * is required even though the STB currently sends some.
     *
     * @param len
     *            Adaptation length
     */
    public void setAdaptationLength(byte len) {
        adaptationLength = len;
    }

    /**
     * Getter for the adaptation header length.
     *
     * @return The adaptation header length.
     */
    public byte getAdaptationLength() {
        return adaptationLength;
    }

    /**
     * Sets the message header message ID to one of those found in the DSM-CC spec table 4-5.
     *
     * @param id
     *            Message ID
     */
    public void setMessageID(short id) {
        messageID = id;
    }

    /**
     * overrides java.lang.Object.toString.
     *
     * @return A string describing the contents of this DSMCC header in human readable form.
     */
    public String toString() {
        String rc;

        rc = "Protocol Discriminator = " + protocolDiscriminator + "\n";
        rc += "DSMCC Type             = " + dsmccType + " (" + dsmccTypeToString(dsmccType) + ")\n";
        rc += "Transaction ID         = " + transactionID.toString() + "\n";
        rc += "Reserved               = " + reserved + "\n";
        rc += "Adaptation Length      = " + adaptationLength + "\n";

        if (adaptationLength != 0 && adaptationHeader != null) {
            rc += adaptationHeader.toString() + "\n";
        }

        return rc;
    }

    /**
     * Sets the adaptation header for this message. Note that the default is to have no adaptation header.
     *
     * @param he
     *            The AdaptationHeader to add to this header. See 2.1 of the DSM-CC spec.
     */
    public void setAdaptationHeader(AdaptationHeader he) {
        adaptationHeader = he;
        if (he != null) {
            adaptationLength = (byte) he.getLength();
        } else {
            adaptationLength = 0;
        }
    }

    /**
     * With no parameters, this method will generate a new internal transaction ID.
     */
    public void setTransactionID() {
        transactionID = new TransactionID(TransactionID.createID());
    }

    /**
     * Sets an external TransactionID object into this message.
     *
     * @param id
     *            The TransactionID object to use.
     */
    public void setTransactionID(TransactionID id) {
        transactionID = id;
    }

    /**
     * Get the TransactionID object for this message.
     *
     * @return TransactionID
     */
    public TransactionID getTransactionID() {
        return transactionID;
    }

    /**
     * toByteArray.
     *
     * @return A byte array containing the contents of this DSMCC message header. This may be sent to the network.
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(bo);

        try {
            out.writeByte(protocolDiscriminator);
            out.writeByte(dsmccType);
            out.writeShort(messageID);
            out.writeInt(transactionID.getID());
            out.writeByte(reserved);
            out.writeByte(adaptationLength);
            out.writeShort(messageLength);

            if (adaptationLength != 0) {
                out.write(adaptationHeader.toByteArray());
            }

            rc = bo.toByteArray();

            out.close();
            out = null;
        } catch (Exception ex) {
            Log.print(ex);
        } finally {
            if (out != null) {
                try {
                    out.close();
                    out = null;
                } catch (IOException ioe) {
                    Log.print(ioe);
                }
            }
        }

        return rc;
    }

    /**
     * parses DataInputStream.
     *
     * @param in
     *            A DSMCC message represented as an array of bytes.
     */
    public void parse(DataInputStream in) {

        try {
            protocolDiscriminator = in.readByte();
            dsmccType = in.readByte();
            messageID = in.readShort();
            transactionID = new TransactionID(in.readInt());
            reserved = in.readByte();
            adaptationLength = in.readByte();
            messageLength = in.readShort();

            if (Log.DEBUG_ON) { // ((int) data[offset]) & 0xFF
                Log.printDebug("m_protocolDiscriminator : " + protocolDiscriminator);
                Log.printDebug("m_dsmccType : " + dsmccType);
                Log.printDebug("m_transactionID : " + transactionID.getID());
                Log.printDebug("m_reserved : " + ((int) reserved & 0xFF));
                Log.printDebug("m_adaptationLength : " + adaptationLength);
            }

            if (adaptationLength != 0) {
                byte[] adaptBytes = new byte[adaptationLength];
                in.read(adaptBytes, 0, adaptationLength);
                adaptationHeader = new AdaptationHeader(adaptBytes);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /**
     * Convert the message type to a human readable form.
     *
     * @param type
     *            type of DSMCC
     * @return String version of DSMCC type
     */
    public String dsmccTypeToString(byte type) {
        switch (type) {
        case UN_CONFIG_MESSAGE:
            return "UN_CONFIG_MESSAGE";

        case UN_SESSION_MESSAGE:
            return "UN_SESSION_MESSAGE";

        case DOWNLOAD_MESSAGE:
            return "DOWNLOAD_MESSAGE";

        case SDB_CHANNEL_CHANGE_PROTOCOL_MESSAGE:
            return "SDB_CHANNEL_CHANGE_PROTOCOL_MESSAGE";

        case UN_PASSTHROUGH_MESSAGE:
            return "UN_PASSTHROUGH_MESSAGE";

        default:
            return "UNKNOWN";
        }

    }

    /**
     * Used to set the total message length field of this message. This is set after the message has been extended.
     *
     * @param length
     *            message length
     */
    public void setMessageLength(int length) {
        messageLength = (short) length;
    }

    /**
     * Return the size of the DSMCC header and adaptation field.
     *
     * @return size of DSMCC header and adaptation field
     */
    public int size() {
        return INVARIANT_HEADER_SIZE + adaptationLength;
    }
}

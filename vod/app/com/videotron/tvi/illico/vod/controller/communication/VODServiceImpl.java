package com.videotron.tvi.illico.vod.controller.communication;

import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import org.dvb.io.ixc.IxcRegistry;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.ixc.vod.VODServiceListener;
import com.videotron.tvi.illico.ixc.vod.VideoController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.VideoOutputUtil;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.PopTrailer;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.VideoControllerImpl;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.data.PlayData;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Playout;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;
import com.videotron.tvi.illico.vod.ui.UITemplateList;

public class VODServiceImpl implements VODService {

	public static final String AGENT_PREF_LANG = "AGENT_PREF_LANG";
	public static final String AGENT_PREF_DEF = "AGENT_PREF_DEF";

	public static final int E25_RIGHTS_MISSING_TO_ACCESS_CONTENT = -125;
	public static final int E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION = -134;

	private XletContext xletContext;
	private BaseUI popTrailerAdaptor;

	private static VODServiceImpl instance = new VODServiceImpl();

	public static VODServiceImpl getInstance() {
		return instance;
	}

	public void start(XletContext xletContext) {
		this.xletContext = xletContext;
		bindToIxc();
	}

	private void bindToIxc() {
		try {
			if (Log.DEBUG_ON) {
				Log.printInfo("VODServiceImpl: bind to IxcRegistry");
			}
			IxcRegistry.bind(xletContext, IXC_NAME, this);
		} catch (Exception e) {
			Log.print(e);
		}
	}

	public void stop() {
		Log.printDebug("VODServiceImpl.stop(), popTrailerAdaptor = " + popTrailerAdaptor);
	}

	public void dispose() {
		try {
			IxcRegistry.unbind(xletContext, IXC_NAME);
		} catch (Exception e) {
			Log.print(e);
		}
	}

	public VideoController getVideoController() throws RemoteException {
		return VideoControllerImpl.getInstance();
	}

	public void addVODServiceListener(VODServiceListener listener) throws RemoteException {
		VideoControllerImpl.getInstance().addVODServiceListener(listener);
	}

	public void removeVODServiceListener(VODServiceListener listener) throws RemoteException {
		VideoControllerImpl.getInstance().removeVODServiceListener(listener);
	}

	public void linkToVOD(String categoryId, String sspID) throws RemoteException {

	}

	public int getVODState() throws RemoteException {
		return Resources.appStatus;
	}

	public void startAutoDiscovery() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("VODServiceImpl: startAutoDiscovery");
		}
		// Log.printInfo("NO ACTION -- startAutoDiscovery -- NO ACTION");
		// CommunicationManager.getInstance().sendReadyMSGtoMonitor();
		CommunicationManager.getInstance().turnOnErrorMessage(false);
		ServiceGroupController.getInstance().searchServiceGroup();
		CommunicationManager.getInstance().turnOnErrorMessage(true);
	}

	LayeredUI videoUI;
	PlayData playData;

	public void showPopupVideo(String sspId, int runtimeInSec, String dispTitle) {
		showPopupVideo(sspId, runtimeInSec, dispTitle, null, null);
	}

	public void showPopupVideo(String sspId, int runtimeInSec, String dispTitle, Video title, Playout playout) {
		if (Log.INFO_ON) {
			Log.printInfo("VODServiceImpl: showPopupVideo");
		}

		playoutToLog = playout;
		titleToLog = title;

		playData = new PlayData();
		playData.sspId = sspId;
		playData.duration = runtimeInSec;
		playData.titleName = dispTitle;

		if (ServiceGroupController.getInstance().getServiceGroupData() == null) {
			if (Log.WARNING_ON) {
				Log.printWarning("VODServiceImpl, Service Group data is missing");
			}
			MenuController.getInstance().showLoadingAnimation();
			ServiceGroupController.getInstance().searchServiceGroup();
			MenuController.getInstance().hideLoadingAnimation();
		}
		if (ServiceGroupController.getInstance().getServiceGroupData() == null) {
			CommunicationManager.getInstance().errorMessage("VOD502", null);
			return;
		}

		PopTrailer trailer = PopTrailer.getInstance();

		LayeredWindow videoWindow = new LayeredWindow();
		videoWindow.setBounds(Resources.fullRec);
		videoWindow.add(trailer);

		videoUI = WindowProperty.VOD_TRAILER.createLayeredDialog(videoWindow, Resources.fullRec, trailer);
		videoUI.activate();

		popTrailerAdaptor = new BaseUI() {
			public void setPopUp(UIComponent newPopUp) {
				Log.printDebug("PopTrailer, setPopUp = " + newPopUp + ", appStatus = " + Resources.appStatus
						+ ", videoUI = " + videoUI);

				if (newPopUp == null) { // it means STOP
					popTrailerAdaptor = null;
					if (videoUI == null) {
						return;
					}

					videoUI.deactivate();
					WindowProperty.dispose(videoUI);
					videoUI = null;

					logPlaybackStopped(true);
					try {
						CommunicationManager.getInstance().getMonitorService().releaseVideoContext();
					} catch (RemoteException e1) {
						Log.print(e1);
					}

					if (modeAgent) {
						Log.printDebug("PopTrailer, setPopUp(null), will call showContent()");
						modeAgent = false;
						// if (Resources.appStatus == VODService.APP_STARTED) {
						try {
							showContent(contentId[idxOfPlay]);
						} catch (RemoteException e) {
							Log.print(e);
						} catch (RuntimeException e) {
							Log.print(e);
						}
						// }
					}

					// EventQueue.invokeLater(new Runnable() {
					// public void run() {
					// // TODO exit or just reach the end
					// tryPreviewFromList();
					// }
					// });
				}
			}
		};

		trailer.show(popTrailerAdaptor, playData);
		trailer.addPowerModeChangeListener();

		VideoControllerImpl.getInstance().sessionSetup(playData.sspId);
		synchronized (trailer) {
			try {
				trailer.wait(2000);
			} catch (InterruptedException e) {
			}
		}
	}

	public void requestToStop() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, requestToStop(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.VOD);
		}
	}

	public void vodPlay() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodPlay(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.PLAY);
		}
	}

	public void vodPause() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodPause(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.PAUSE);
		}
	}

	public void vodStop() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodStop(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.STOP);
		}
	}

	public void vodFF() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodFF(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.FAST_FWD);
		}
	}

	public void vodRWD() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodRWD(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.REWIND);
		}
	}

	public void vodSkipForward() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodSkipForward(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.FORWARD);
		}
	}

	public void vodSkipBackward() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodSkipBackward(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyCodes.BACK);
		}
	}

	public void vodSkipSection(int section) throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodSkipSection(), cur = " + cur + ", status = " + Resources.appStatus
				+ ", section = " + section);

		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).handleKey(KeyEvent.VK_0 + section);
		}
	}

	public boolean vodPlayControl(int code) throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodPlayControl(), cur = " + cur + ", status = " + Resources.appStatus
				+ ", code = " + code);
		boolean ret = false;
		if (cur instanceof PlayScreenUI) {
			if (code == OCRcEvent.VK_FORWARD) {
				code = OCRcEvent.VK_CLOSE_BRACKET;
			} else if (code == OCRcEvent.VK_BACK) {
				code = OCRcEvent.VK_OPEN_BRACKET;
			}
			ret = ((PlayScreenUI) cur).handleKey(code);
		} else if (videoUI != null) {
			ret = PopTrailer.getInstance().handleKey(code);
		}
		Log.printDebug("VODServiceImpl, vodPlayControl(), ret = " + ret);
		return ret;
	}

	public boolean showHideFlipBar() {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, showHideFlipBar(), cur = " + cur + ", status = " + Resources.appStatus);

		if (cur instanceof PlayScreenUI) {
			return ((PlayScreenUI) cur).handleKey(OCRcEvent.VK_INFO);
		}
		return false;
	}

	public void vodNextContent() throws RemoteException {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		Log.printInfo("VODServiceImpl, vodNextContent(), cur = " + cur + ", status = " + Resources.appStatus);
		if (cur instanceof PlayScreenUI) {
			((PlayScreenUI) cur).playNextContent(true);
		}
	}

	private RuntimeException checkRights(Object cached) {
		RuntimeException toThrow = null;
		CommunicationManager.getInstance().updatePackageRights();
		if (cached instanceof Video) {
			Video title = (Video) cached;
			if (CommunicationManager.getInstance().checkPackage(title.service) == false) {
				toThrow = new SecurityException("svc rights - " + title);
			} else if (title.channel != null) {
				String watch = null;
				if (title.channel.network == null) { // 1 channel
					if (title.channel.hasAuth == false) {
						title.channel.hasAuth = CommunicationManager.getInstance().checkAuth(title.channel.callLetters);
					}
					if (title.channel.hasAuth) {
						watch = title.channel.callLetters;
					}
				} else {
					for (int i = 0; i < title.channel.network.channel.length; i++) {
						if (title.channel.network.channel[i].hasAuth == false) {
							title.channel.network.channel[i].hasAuth = CommunicationManager.getInstance().checkAuth(
									title.channel.network.channel[i].callLetters);
						}
						if (title.channel.network.channel[i].hasAuth) {
							watch = title.channel.network.channel[i].callLetters;
							break;
						}
					}
				}
				if (watch == null) {
					toThrow = new SecurityException("rights - " + title);
				}
			}
		} else if (cached instanceof Bundle) {
			Bundle bundle = (Bundle) cached;
			if (CommunicationManager.getInstance().checkPackage(bundle.service) == false) {
				toThrow = new SecurityException("svc rights bundle - " + bundle);
			}
		}
		return toThrow;
	}

	private RuntimeException checkBlock(Object cached) {
		RuntimeException toThrow = null;
		if (cached instanceof Video) {
			Video title = (Video) cached;
			if (MenuController.getInstance().isBlocked(title)) {
				toThrow = new IllegalStateException("blocked - " + title);
				// } else if
				// (Definition.RATING_OVER_18.equals(title.getRating()) &&
				// Definition.EXPLICIT_SEXUALITY.equals(title.getRatingInfo()))
				// {
				// toThrow = new IllegalStateException("blocked 18+ - " +
				// title);
			}
		} else if (cached instanceof Bundle) {
			Bundle bundle = (Bundle) cached;
			if (MenuController.getInstance().isBlocked(bundle)) {
				toThrow = new IllegalStateException("blocked bundle - " + bundle);
				// } else if
				// (Definition.RATING_OVER_18.equals(bundle.getRating()) &&
				// Definition.EXPLICIT_SEXUALITY.equals(bundle.getRatingInfo()))
				// {
				// toThrow = new IllegalStateException("blocked 18+ bundle - " +
				// bundle);
			}
		}
		return toThrow;
	}

	private RuntimeException checkFree(Object cached) {
		boolean isFree = false;
		if (cached instanceof Video) {
			Video title = (Video) cached;
			isFree = title.isFree() && title.inBundleOnly == false;
		} else if (cached instanceof Bundle) {
			Bundle bundle = (Bundle) cached;
			isFree = bundle.isFree();
		}

		// even it's free, bookmark is needed
		boolean ori = CommunicationManager.getInstance().turnOnErrorMessage(false);
		BookmarkManager.getInstance().retrieveActivePurchases(false);
		CommunicationManager.getInstance().turnOnErrorMessage(ori);

		if (isFree) {
			return null;
		}
		if (cached instanceof Video) {
			Video title = (Video) cached;
			if (BookmarkManager.getInstance().isPurchased(title) == null) {
				return new SecurityException("puchasable - " + title);
			}
		} else if (cached instanceof Bundle) {
			Bundle bundle = (Bundle) cached;
			if (BookmarkManager.getInstance().isPurchased(bundle) == false) {
				return new SecurityException("puchasable bundle - " + bundle);
			}
		}
		return null;
	}

	private boolean needOrder;

	public int showContent(String contentId) throws RemoteException {
		Log.printInfo("VODServiceImpl, showContent(), content = " + contentId + ", status = " + Resources.appStatus
				+ ", needOrder = " + needOrder);
		if (contentId.startsWith("V") == false && contentId.startsWith("EP") == false
				&& contentId.startsWith("BU") == false) {
			throw new IllegalArgumentException("contentId must be start with V, EP or BU. contentId = " + contentId);
		}

		boolean needRetrieve = false;
		Object cached = CatalogueDatabase.getInstance().getCached(contentId);
		try { // to sure exists
			((Orderable) cached).detail[0].toString();
		} catch (Exception e) {
			needRetrieve = true;
		}

		if (needRetrieve) {
			boolean ori = CommunicationManager.getInstance().turnOnErrorMessage(false);
			ArrayList list = new ArrayList();
			list.add(contentId);
			synchronized (CatalogueDatabase.getInstance()) {
				MenuController.getInstance().showLoadingAnimation();
				if (contentId.startsWith("V")) {
					CatalogueDatabase.getInstance().retrieveVideos(null, list, null);
				} else if (contentId.startsWith("EP")) {
					CatalogueDatabase.getInstance().retrieveEpisodes(null, list, null);
				} else if (contentId.startsWith("BU")) {
					CatalogueDatabase.getInstance().retrieveBundle(null, contentId, true, null);
				}
				try {
					CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
				} catch (InterruptedException e) {
				}
				MenuController.getInstance().hideLoadingAnimation();
				String error = CommunicationManager.getInstance().getLastErrorCode();
				if (error.startsWith("VOD102")) { // time out
					throw new UnsupportedOperationException("timeout on VCDS");
				} else if (error.startsWith("VOD10") && error.startsWith("VOD105") == false) { // connect,
																								// IO
																								// or
																								// SAX
																								// exceptions
					throw new RuntimeException(error + " occured");
				} else if (error != null && error.equals("VOD105-023")) { // VDTRMASTER-5410
					com.videotron.tvi.illico.vod.data.vcds.type.Error errorObj = CatalogueDatabase.getInstance()
							.getLastError();
					Log.printDebug("VODServiceImpl, showContent(), errorObj=" + errorObj);
					if (Log.DEBUG_ON) {
						Log.printDebug("VODServiceImpl, showContent(), errorObj.errorCode="
								+ (errorObj != null ? errorObj.errorCode : "null"));
					}

					if (errorObj != null && errorObj.errorCode.equals("VCDS023")) {
						CommunicationManager.getInstance().turnOnErrorMessage(ori);
						return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
					}
				}
			}
			CommunicationManager.getInstance().turnOnErrorMessage(ori);
		}

		cached = CatalogueDatabase.getInstance().getCached(contentId);
		Log.printDebug("VODServiceImpl, showContent(), cached = " + cached);
		if (cached == null) {
			throw new IllegalArgumentException("contentId not found on VCDS");
		}

		RuntimeException toThrow = checkRights(cached);
		if (toThrow == null) {
			toThrow = checkBlock(cached);
		}

		// 4K
		// remove acoording to request of SangSoo(QA)
//		if (Log.DEBUG_ON) {
//			Log.printDebug("VodServiceImpl, showContent, SUPPORT_UHD" + Environment.SUPPORT_UHD);
//		}
//		if (!Environment.SUPPORT_UHD) {
//			if (cached instanceof Video) {
//				Video v = (Video) cached;
//
//				for (int i = 0; i < v.detail.length; i++) {
//					if (v.detail[i].definition.equalsIgnoreCase(Video.DEFNITION_UHD)) {
//						if (Log.DEBUG_ON) {
//							Log.printDebug("VodServiceImpl, showContent, found UHD content in video");
//						}
//						return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
//					}
//				}
//
//			} else if (cached instanceof Bundle) {
//				MoreDetail[] details = ((Bundle) cached).getTitles();
//
//				if (details != null) {
//					for (int i = 0; i < details.length; i++) {
//						Video v = (Video) details[i];
//						for (int j = 0; j < v.detail.length; j++) {
//							if (v.detail[j].definition.equalsIgnoreCase(Video.DEFNITION_UHD)) {
//								if (Log.DEBUG_ON) {
//									Log.printDebug("VodServiceImpl, showContent, found UHD content in bundle");
//								}
//								return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
//							}
//						}
//					}
//				}
//			}
//		}

		checkPlaying();
		if (Resources.appStatus == VODService.APP_PAUSED) {
			CommunicationManager.getInstance().getMonitorService()
					.startUnboundApplication("VOD", new String[] { MenuController.AGENT_CONTENT, contentId });
		} else {
			MenuController.getInstance().setFrom(MenuController.AGENT_CONTENT);
			if (BookmarkManager.getInstance().isAdult((Orderable) cached)) {
				MenuController.getInstance().setOpenAdultCategory(true, true);
			}
			MenuController.getInstance().goToAsset(contentId, false, null);
			Log.printDebug("VODServiceImpl, showContent(), needOrder = " + needOrder + ", toThrow = " + toThrow);
			if (needOrder && (toThrow == null || toThrow instanceof IllegalStateException)) { // no
																								// exc
																								// or
																								// blocked
				needOrder = false;
				synchronized (instance) {
					try {
						instance.wait(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				EventQueue.invokeLater(new Runnable() { // should be in EDT
							public void run() {
								Log.printDebug("VODServiceImpl, showContent(), cur = "
										+ MenuController.getInstance().getCurrentScene());
								MenuController.getInstance().getCurrentScene().handleKey(KeyEvent.VK_ENTER);
							}
						});
			}
		}
		if (toThrow != null) {
			throw toThrow;
		}
		return 0;
	}

	private boolean modeAgent;
	private int idxOfPlay;
	private String[] contentId;
	private boolean[] trailer;
	private String[] lang;
	private String[] definition;

	public int playContent(String[] contentId, boolean[] trailer, String[] lang, String[] definition)
			throws RemoteException {
		for (int i = 0; i < contentId.length; i++) {
			Log.printDebug("VODServiceImpl, playContent(), i = " + i + ", contentId = " + contentId[i] + ", trailer = "
					+ trailer[i] + ", lang = " + lang[i] + ", definition = " + definition[i]);
		}
		StringBuffer buf = new StringBuffer("VODServiceImpl, playContent(), id = ");
		ArrayList videoList = new ArrayList();
		ArrayList episodeList = new ArrayList();
		ArrayList bundleList = new ArrayList();
		for (int i = 0; i < contentId.length; i++) {
			buf.append(contentId[i]).append(',');
			Object cached = CatalogueDatabase.getInstance().getCached(contentId[i]);
			try { // to sure exists
				((Orderable) cached).detail[0].toString();
			} catch (Exception e) {
				// need retrieve
				if (contentId[i].startsWith("V")) {
					videoList.add(contentId[i]);
				} else if (contentId[i].startsWith("EP")) {
					episodeList.add(contentId[i]);
				} else if (contentId[i].startsWith("BU")) {
					bundleList.add(contentId[i]);
				}
			}
		}
		buf.append(" vL = " + videoList.size() + ", eL = " + episodeList.size());
		Log.printInfo(buf.toString());

		boolean ori = CommunicationManager.getInstance().turnOnErrorMessage(false);
		if (videoList.size() > 0) {
			synchronized (CatalogueDatabase.getInstance()) {
				MenuController.getInstance().showLoadingAnimation();
				CatalogueDatabase.getInstance().retrieveVideos(null, videoList, null);
				try {
					CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
				} catch (InterruptedException e) {
				}
				MenuController.getInstance().hideLoadingAnimation();
				String error = CommunicationManager.getInstance().getLastErrorCode();
				if (error.startsWith("VOD102")) { // time out
					throw new UnsupportedOperationException("timeout on VCDS");
				} else if (error.startsWith("VOD10") && error.startsWith("VOD105") == false) { // connect,
																								// IO
																								// or
																								// SAX
																								// exceptions
					throw new RuntimeException(error + " occured");
				} else if (error != null && error.equals("VOD105-023")) { // VDTRMASTER-5410
					com.videotron.tvi.illico.vod.data.vcds.type.Error errorObj = CatalogueDatabase.getInstance()
							.getLastError();

					if (Log.DEBUG_ON) {
						Log.printDebug("VODServiceImpl, playContent(), videoList, errorObj.errorCode="
								+ (errorObj != null ? errorObj.errorCode : "null"));
					}

					if (errorObj != null && errorObj.errorCode.equals("VCDS023")) {
						videoList.clear();
						CommunicationManager.getInstance().turnOnErrorMessage(ori);
						return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
					}
				}
			}
		}
		videoList.clear();
		if (episodeList.size() > 0) {
			synchronized (CatalogueDatabase.getInstance()) {
				MenuController.getInstance().showLoadingAnimation();
				CatalogueDatabase.getInstance().retrieveEpisodes(null, episodeList, null);
				try {
					CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
				} catch (InterruptedException e) {
				}
				MenuController.getInstance().hideLoadingAnimation();
				String error = CommunicationManager.getInstance().getLastErrorCode();
				if (error.startsWith("VOD102")) { // time out
					throw new UnsupportedOperationException("timeout on VCDS");
				} else if (error.startsWith("VOD10") && error.startsWith("VOD105") == false) { // connect,
																								// IO
																								// or
																								// SAX
																								// exceptions
					throw new RuntimeException(error + " occured");
				} else if (error != null && error.equals("VOD105-023")) { // VDTRMASTER-5410
					com.videotron.tvi.illico.vod.data.vcds.type.Error errorObj = CatalogueDatabase.getInstance()
							.getLastError();

					if (Log.DEBUG_ON) {
						Log.printDebug("VODServiceImpl, playContent(), episodeList, errorObj.errorCode="
								+ (errorObj != null ? errorObj.errorCode : "null"));
					}

					if (errorObj != null && errorObj.errorCode.equals("VCDS023")) {
						episodeList.clear();
						CommunicationManager.getInstance().turnOnErrorMessage(ori);
						return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
					}
				}
			}
		}
		episodeList.clear();
		if (bundleList.size() > 0) {
			synchronized (CatalogueDatabase.getInstance()) {
				MenuController.getInstance().showLoadingAnimation();
				for (int i = 0; i < bundleList.size(); i++) {
					CatalogueDatabase.getInstance().retrieveBundle(null, (String) bundleList.get(i), true, null);
					try {
						CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {
					}
					String error = CommunicationManager.getInstance().getLastErrorCode();
					if (error.startsWith("VOD102")) { // time out
						throw new UnsupportedOperationException("timeout on VCDS");
					} else if (error.startsWith("VOD10") && error.startsWith("VOD105") == false) { // connect,
																									// IO
																									// or
																									// SAX
																									// exceptions
						throw new RuntimeException(error + " occured");
					} else if (error != null && error.equals("VOD105-023")) { // VDTRMASTER-5410
						com.videotron.tvi.illico.vod.data.vcds.type.Error errorObj = CatalogueDatabase.getInstance()
								.getLastError();

						if (Log.DEBUG_ON) {
							Log.printDebug("VODServiceImpl, playContent(), bundleList, errorObj.errorCode="
									+ (errorObj != null ? errorObj.errorCode : "null"));
						}

						if (errorObj != null && errorObj.errorCode.equals("VCDS023")) {
							bundleList.clear();
							CommunicationManager.getInstance().turnOnErrorMessage(ori);
							return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
						}
					}
				}
				MenuController.getInstance().hideLoadingAnimation();
			}
		}
		bundleList.clear();
		CommunicationManager.getInstance().turnOnErrorMessage(ori);

		int retrievedCnt = 0;
		for (int i = 0; i < contentId.length; i++) {
			Object cached = CatalogueDatabase.getInstance().getCached(contentId[i]);
			if (cached != null) {
				if (trailer[i]) {
					Orderable orderable = (Orderable) cached;
					Playout playout = orderable.getPreview(lang[i]);
					if (playout != null) {
						retrievedCnt++;
					}
				} else {
					retrievedCnt++;
				}
			}
			Log.printDebug("VODServiceImpl, playContent(), cached = " + cached);
		}
		if (retrievedCnt == 0) {
			throw new IllegalArgumentException("contentId not found on VCDS(at all)");
		} else if (retrievedCnt < contentId.length) {
			throw new IllegalArgumentException("contentId not found on VCDS(partially)");
		}

		// 4K
		// remove according to request of SangSoo(QA)
//		if (!Environment.SUPPORT_UHD) {
//			for (int i = 0; i < contentId.length; i++) {
//				Object cached = CatalogueDatabase.getInstance().getCached(contentId[i]);
//				if (cached instanceof Video) {
//					Video v = (Video) cached;
//
//					for (int idx = 0; idx < v.detail.length; idx++) {
//						if (v.detail[idx].definition.equalsIgnoreCase(Video.DEFNITION_UHD)) {
//							if (Log.DEBUG_ON) {
//								Log.printDebug("VodServiceImpl, playContent, found UHD content in video");
//							}
//							return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
//						}
//					}
//
//				} else if (cached instanceof Bundle) {
//					MoreDetail[] details = ((Bundle) cached).getTitles();
//
//					if (details != null) {
//						for (int idx = 0; idx < details.length; idx++) {
//							Video v = (Video) details[idx];
//							for (int j = 0; j < v.detail.length; j++) {
//								if (v.detail[j].definition.equalsIgnoreCase(Video.DEFNITION_UHD)) {
//									if (Log.DEBUG_ON) {
//										Log.printDebug("VodServiceImpl, playContent, found UHD content in bundle");
//									}
//									return E25_RIGHTS_MISSING_TO_ACCESS_CONTENT;
//								}
//							}
//						}
//					}
//				}
//			}
//		}
		
		// 4K
		// DDC-107
		for (int i = 0; i < contentId.length; i++) {
			Object cached = CatalogueDatabase.getInstance().getCached(contentId[i]);
			if (cached instanceof Video) {
				Video v = (Video) cached;

				if (Log.DEBUG_ON) {
					Log.printDebug("MenuController, playContent(String[], boolean[], String[], String[]), video.definition=" 
							+ (v.getOfferHD(null) != null ? v.getOfferHD(null).definition : null)
							+ ", canPlayUHDContent=" + VideoOutputUtil.getInstance().canPlayUHDContent());
				}
				if (v.getOfferHD(null) != null && v.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
					if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
						if (Log.DEBUG_ON) {
							Log.printDebug("MenuController, playContent(String[], boolean[], String[], String[]), return "
									+ "E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION");
						}
						Log.printError("MenuController, playContent(String[], boolean[], String[], String[]), Error Code : "
								+ "VOD850");
						
						return E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION;
					} else if (!VideoOutputUtil.getInstance().existHdcpApi() || !VideoOutputUtil.getInstance().isSupportedHdcp()) {
						
						if (Log.DEBUG_ON) {
							Log.printDebug("MenuController, playContent(String[], boolean[], String[], String[]), return "
									+ "E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION");
						}
						Log.printError("MenuController, playContent(String[], boolean[], String[], String[]), Error Code : "
								+ "VOD851");
						return E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION;
					}
				}

			} else if (cached instanceof Bundle) {
				Bundle bundle = (Bundle) cached;
				if (Log.DEBUG_ON) {
					Log.printDebug("MenuController, playContent(String[], boolean[], String[], String[]), bundle.definition=" 
							+ (bundle.getOfferHD(null) != null ? bundle.getOfferHD(null).definition : null) 
							+ ", canPlayUHDContent=" + VideoOutputUtil.getInstance().canPlayUHDContent());
				}
				if (bundle.getOfferHD(null) != null && bundle.getOfferHD(null).definition.equals(Offer.DEFNITION_UHD)) {
					if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
						if (Log.DEBUG_ON) {
							Log.printDebug("MenuController, playContent(String[], boolean[], String[], String[]), return "
									+ "E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION");
						}
						Log.printError("MenuController, playContent(String[], boolean[], String[], String[]), Error Code : "
								+ "VOD850");
						
						return E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION;
					} else if (!VideoOutputUtil.getInstance().existHdcpApi() || !VideoOutputUtil.getInstance().isSupportedHdcp()) {
						
						if (Log.DEBUG_ON) {
							Log.printDebug("MenuController, playContent(String[], boolean[], String[], String[]), return "
									+ "E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION");
						}
						Log.printError("MenuController, playContent(String[], boolean[], String[], String[]), Error Code : "
								+ "VOD851");
						return E34_NOT_POSITIVE_OPTIMAL_SETUP_VALIDATION;
					}
				}
			}
		}

		if (trailer[idxOfPlay] == false) {
			RuntimeException toThrow;
			for (int i = 0; i < contentId.length; i++) {
				needOrder = false;
				Object cached = CatalogueDatabase.getInstance().getCached(contentId[i]);
				toThrow = checkRights(cached);
				if (toThrow == null) {
					toThrow = checkBlock(cached);
					if (toThrow == null) {
						toThrow = checkFree(cached);
						if (toThrow != null) {
							needOrder = true;
						}
					} else {
						needOrder = true; // need PIN to unblock
					}
				}
				if (toThrow != null) {
					Log.printDebug("VODServiceImpl, playContent(), check fail - " + toThrow.getMessage()
							+ ", needOrder = " + needOrder);
					boolean needThrow = needOrder; // needOrder will be 'false'
													// in showContent()
					if (needOrder) {
						if (lang[i] != null) {
							DataCenter.getInstance().put(AGENT_PREF_LANG, lang[i]);
						}
						if (definition[i] != null) {
							DataCenter.getInstance().put(AGENT_PREF_DEF, definition[i]);
						}
					}
					showContent(contentId[i]);
					if (needThrow) {
						Log.printDebug("VODServiceImpl, playContent(), toThrow = " + toThrow);
						throw toThrow;
					}
					return 0;
				}
			}
		}

		checkPlaying();
		if (Resources.appStatus == VODService.APP_PAUSED) {
			Log.printInfo("VODServiceImpl, playContent(), appStatus is PAUSED, try to start VOD");
			buf.setLength(0);
			for (int i = 0; i < contentId.length; i++) {
				buf.append(contentId[i]).append(',');
			}
			String contentIds = buf.toString();
			buf.setLength(0);

			for (int i = 0; i < lang.length; i++) {
				buf.append(trailer[i]).append(',');
			}
			String trailers = buf.toString();
			buf.setLength(0);

			for (int i = 0; i < lang.length; i++) {
				buf.append(lang[i]).append(',');
			}
			String langs = buf.toString();
			buf.setLength(0);

			for (int i = 0; i < definition.length; i++) {
				buf.append(definition[i]).append(',');
			}
			String definitions = buf.toString();
			buf.setLength(0);

			CommunicationManager
					.getInstance()
					.getMonitorService()
					.startUnboundApplication("VOD",
							new String[] { MenuController.AGENT_PLAY, contentIds, trailers, langs, definitions });
			return 0;
		}

		// TODO mix w/ trailer
		idxOfPlay = 0;
		this.contentId = contentId;
		this.trailer = trailer;
		this.lang = lang;
		this.definition = definition;

		tryToPlay();
		return 0;
	}

	private void checkPlaying() {
		if (Resources.appStatus == VODService.APP_WATCHING) {
			MenuController.getInstance().showLoadingAnimation(true);
			Log.printInfo("VODServiceImpl, playContent(), appStatus is WATCHING, try to stop it");
			UIComponent cur = MenuController.getInstance().getCurrentScene();
			if (cur instanceof PlayScreenUI) {
				((PlayScreenUI) cur).stop();
			} else if (videoUI != null && videoUI.isActive()) {
				modeAgent = false;
				PopTrailer.getInstance().handleKey(OCRcEvent.VK_EXIT);
			}
			try {
				Thread.sleep(2000); // for stable
			} catch (InterruptedException e) {
			}
			MenuController.getInstance().hideLoadingAnimation();
		}
	}

	private void tryToPlay() {
		if (trailer[idxOfPlay]) {
			Object cached = CatalogueDatabase.getInstance().getCached(contentId[idxOfPlay]);
			Orderable orderable = (Orderable) cached;
			Playout playout = orderable.getPreview(lang[idxOfPlay]);
			Log.printDebug("VODServiceImpl, tryToPlay(), playout = " + playout);
			if (playout != null) {
				playout.title = orderable.getTitle();
			}
			modeAgent = true;
			showPopupVideo(playout.name, playout.duration, null, (Video) cached, playout);
		} else {
			Vector list = new Vector();
			for (int i = 0; i < contentId.length; i++) {
				Object cached = CatalogueDatabase.getInstance().getCached(contentId[idxOfPlay]);
				if (cached == null) {
					continue;
				}
				if (cached instanceof Video) {
					Video v = (Video) cached;

					//R7.4 VDTRMASTER-5850					
                    String prefLang = DataCenter.getInstance().getString(PreferenceNames.PREFERENCES_ON_LANGUAGE);
                    String prefDef = DataCenter.getInstance().getString(PreferenceNames.PREFERENCES_ON_FORMAT);

                    if (lang[idxOfPlay] == null || lang[idxOfPlay].equals(TextUtil.EMPTY_STRING)) {
                        lang[idxOfPlay] = prefLang;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("tryToPlay, forcedly lang[" + i + "] set as " + prefLang);
                        }
                    }

                    if (definition[idxOfPlay] == null || definition[idxOfPlay].equals(TextUtil.EMPTY_STRING)) {
                        definition[idxOfPlay] = prefDef;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("tryToPlay, forcedly definition[" + i + "] set as " + prefDef);
                        }
                    }
                    

					v.selected = "en".equalsIgnoreCase(lang[idxOfPlay]) ? ("hd".equalsIgnoreCase(definition[idxOfPlay])
							|| "uhd".equalsIgnoreCase(definition[idxOfPlay]) ? Resources.HD_EN : Resources.SD_EN)
							: ("hd".equalsIgnoreCase(definition[idxOfPlay])
									|| "uhd".equalsIgnoreCase(definition[idxOfPlay]) ? Resources.HD_FR
									: Resources.SD_FR);
					list.add(v);
					Log.printDebug("VODServiceImpl, playContent(), added = " + v + ", selected = " + v.selected);
				} else if (cached instanceof Bundle) {
					list.add(cached);
					Log.printDebug("VODServiceImpl, playContent(), added = " + cached);
				}
			}
			MenuController.getInstance().goToNextScene(UITemplateList.PLAY_SCREEN, list);
		}

	}

	/*
	 * ArrayList listToPreview = new ArrayList(); public void
	 * playPreview(String[] contentId, String[] lang) throws RemoteException {
	 * if (Resources.appStatus == VODService.APP_PAUSED) { Log.printInfo(
	 * "VODServiceImpl, playPreview(), appStatus is PAUSED, try to start VOD");
	 * StringBuffer buf = new StringBuffer(); for (int i = 0; i <
	 * contentId.length; i++) { buf.append(contentId[i]).append(','); } String
	 * contentIds = buf.toString(); buf.setLength(0);
	 * 
	 * for (int i = 0; i < lang.length; i++) { buf.append(lang[i]).append(',');
	 * } String langs = buf.toString(); buf.setLength(0);
	 * 
	 * CommunicationManager.getInstance().getMonitorService().
	 * startUnboundApplication("VOD", new String[]
	 * {MenuController.AGENT_PREVIEW, contentIds, langs}); return; } if
	 * (Resources.appStatus == VODService.APP_WATCHING) { Log.printInfo(
	 * "VODServiceImpl, playPreview(), appStatus is WATCHING, try to stop it");
	 * UIComponent cur = MenuController.getInstance().getCurrentScene(); if (cur
	 * instanceof PlayScreenUI) { ((PlayScreenUI) cur).stop(); } } StringBuffer
	 * buf = new StringBuffer("VODServiceImpl, playPreview(), id = "); ArrayList
	 * videoList = new ArrayList(); ArrayList episodeList = new ArrayList(); for
	 * (int i = 0; i < contentId.length; i++) {
	 * buf.append(contentId[i]).append(','); Object cached =
	 * CatalogueDatabase.getInstance().getCached(contentId[i]); try { // to sure
	 * exists ((Orderable) cached).detail[0].toString(); } catch (Exception e) {
	 * // need retrieve if (contentId[i].startsWith("V")) {
	 * videoList.add(contentId[i]); } else if (contentId[i].startsWith("EP")) {
	 * episodeList.add(contentId[i]); } } } buf.append(" vL = " +
	 * videoList.size() + ", eL = " + episodeList.size());
	 * Log.printInfo(buf.toString());
	 * 
	 * if (videoList.size() > 0) { synchronized
	 * (CatalogueDatabase.getInstance()) {
	 * MenuController.getInstance().showLoadingAnimation();
	 * CatalogueDatabase.getInstance().retrieveVideos(null, videoList, null);
	 * try { CatalogueDatabase.getInstance().wait( Constants.MS_PER_MINUTE); }
	 * catch (InterruptedException e) { }
	 * MenuController.getInstance().hideLoadingAnimation(); } }
	 * videoList.clear(); if (episodeList.size() > 0) { synchronized
	 * (CatalogueDatabase.getInstance()) {
	 * MenuController.getInstance().showLoadingAnimation();
	 * CatalogueDatabase.getInstance().retrieveEpisodes(null, episodeList,
	 * null); try { CatalogueDatabase.getInstance().wait(
	 * Constants.MS_PER_MINUTE); } catch (InterruptedException e) { }
	 * MenuController.getInstance().hideLoadingAnimation(); } }
	 * episodeList.clear();
	 * 
	 * listToPreview.clear(); for (int i = 0; i < contentId.length; i++) {
	 * Object cached = CatalogueDatabase.getInstance().getCached(contentId[i]);
	 * Orderable orderable = (Orderable) cached; Playout playout =
	 * orderable.getPreview(lang[i]); if (playout != null) { playout.title =
	 * orderable.getTitle(); listToPreview.add(playout); }
	 * Log.printDebug("playPreview, ic = " + contentId[i] + ", found = " +
	 * playout); } tryPreviewFromList(); }
	 * 
	 * private void tryPreviewFromList() {
	 * Log.printInfo("VODServiceImpl, tryPreviewFromList(), list = " +
	 * listToPreview); if (listToPreview.isEmpty() == false) { Playout preview =
	 * (Playout) listToPreview.remove(0); showPopupVideo(preview.name,
	 * preview.duration, preview.title); } }
	 * 
	 * public void showOrder(String contentId, String lang, String definition)
	 * throws RemoteException { if (Resources.appStatus ==
	 * VODService.APP_PAUSED) { Log.printInfo(
	 * "VODServiceImpl, showOrder(), appStatus is PAUSED, try to start VOD");
	 * 
	 * CommunicationManager.getInstance().getMonitorService().
	 * startUnboundApplication("VOD", new String[] {MenuController.AGENT_ORDER,
	 * contentId, lang, definition}); return; } if (Resources.appStatus ==
	 * VODService.APP_WATCHING) { Log.printInfo(
	 * "VODServiceImpl, showOrder(), appStatus is WATCHING, try to stop it");
	 * UIComponent cur = MenuController.getInstance().getCurrentScene(); if (cur
	 * instanceof PlayScreenUI) { ((PlayScreenUI) cur).stop(); } }
	 * Log.printInfo("VODServiceImpl, showOrder(), contentId = " + contentId +
	 * ", lang = " + lang + ", def = " + definition);
	 * 
	 * int command = 0; ArrayList list = new ArrayList(); Object cached =
	 * CatalogueDatabase.getInstance().getCached(contentId); try { // to sure
	 * exists ((Orderable) cached).detail[0].toString(); } catch (Exception e) {
	 * // need retrieve list.add(contentId); if (contentId.startsWith("V")) {
	 * command = 1; } else if (contentId.startsWith("EP")) { command = 2; } } if
	 * (command > 0) { // need retrieve synchronized
	 * (CatalogueDatabase.getInstance()) {
	 * MenuController.getInstance().showLoadingAnimation(); if (command == 1) {
	 * CatalogueDatabase.getInstance().retrieveVideos(null, list, null); } else
	 * { CatalogueDatabase.getInstance().retrieveEpisodes(null, list, null); }
	 * try { CatalogueDatabase.getInstance().wait( Constants.MS_PER_MINUTE); }
	 * catch (InterruptedException e) { }
	 * MenuController.getInstance().hideLoadingAnimation(); } } // TODO check
	 * purchased cached = CatalogueDatabase.getInstance().getCached(contentId);
	 * Log.printDebug("VODServiceImpl, showOrder(), cached = " + cached); if
	 * (cached != null) { final BaseElement element = (BaseElement) cached;
	 * EventQueue.invokeLater(new Runnable() { // should be in EDT public void
	 * run() { MenuController.getInstance().processActionMenu(
	 * DataCenter.getInstance().getString(Resources.TEXT_ORDER), element); } });
	 * } }
	 */

	LogStateListener logStateListener;

	public void setLogStateListener(LogStateListener l) {
		Log.printInfo("VODServiceImpl, setLogStateListener(), l = " + l + ", status = " + Resources.appStatus);
		logStateListener = l;

		if (l == null) {
			return;
		}
		Hashtable logs = null;

		if (videoUI != null && videoUI.isActive()) { // trailer
			logs = getCurrentState();
		} else if (MenuController.getInstance().getCurrentScene() != null && // VOD
																				// watching
				MenuController.getInstance().getCurrentScene() instanceof PlayScreenUI) {
			logs = getCurrentState();
		}
		if (logs != null) {
			try {
				l.logStateChanged(logs);
			} catch (RemoteException e) {
				Log.print(e);
			}

			if (Resources.appStatus == APP_STARTED) { // it may be paused
				logPlaybackPaused(true);
			}
		}
	}

	public void logPlaybackPaused(boolean paused) {
		Log.printInfo("VODServiceImpl, logPlaybackPaused(), paused = " + paused + ", listener = " + logStateListener);
		if (logStateListener != null) {
			try {
				logStateListener.playbackPaused(paused);
			} catch (RemoteException e) {
				Log.print(e);
			}
		}
	}

	public void logPlaybackStopped(boolean stopped) {
		Log.printInfo("VODServiceImpl, logPlaybackStopped(), stopped = " + stopped + ", listener = " + logStateListener);
		if (logStateListener != null) {
			try {
				if (stopped) {
					logStateListener.playbackStopped();
				} else {
					Hashtable logs = getCurrentState();
					if (logs == null) {
						return;
					}
					logStateListener.logStateChanged(logs);
				}
			} catch (RemoteException e) {
				Log.print(e);
			}
		}
	}

	Playout playoutToLog;
	Video titleToLog;

	private Hashtable getCurrentState() {
		Hashtable logs = new Hashtable();
		logs.put(LogStateListener.KEY_STATE, LogStateListener.LOG_STATE_VOD);

		if (videoUI != null && videoUI.isActive()) { // trailer
			if (titleToLog != null && playoutToLog != null) {
				logs.put(LogStateListener.KEY_CONTENT_ID, titleToLog.getId());
				// logs.put(LogStateListener.KEY_TITLE, titleToLog.getTitle());
				Offer o = titleToLog.getOffer(playoutToLog);
				logs.put(LogStateListener.KEY_LANGUAGE, o == null ? "" : o.language.toUpperCase());
				logs.put(LogStateListener.KEY_DEFINITION, o == null ? "" : o.definition);
			} else if (playData != null) { // trailer request from out of VOD
				logs.put(LogStateListener.KEY_CONTENT_ID, playData.sspId);
				// logs.put(LogStateListener.KEY_TITLE, playData.titleName ==
				// null ? "" : playData.titleName);
				logs.put(LogStateListener.KEY_LANGUAGE, "");
				logs.put(LogStateListener.KEY_DEFINITION, "");
			}
			Log.printDebug("VODServiceImpl, Trailer, logs = " + logs + ", t = " + titleToLog + ", p = " + playoutToLog
					+ ", d = " + playData);
			return logs;
		} else {
			UIComponent ui = MenuController.getInstance().getCurrentScene();
			if (ui == null || ui instanceof PlayScreenUI == false) {
				return null;
			}

			Video t = ((PlayScreenUI) ui).getTitle();
			Playout p = ((PlayScreenUI) ui).getPlayout();
			Offer o = t.getOffer(p);

			logs.put(LogStateListener.KEY_CONTENT_ID, t.getId());
			// logs.put(LogStateListener.KEY_TITLE, t.getTitle());
			logs.put(LogStateListener.KEY_LANGUAGE, o == null ? "" : o.language.toUpperCase());
			logs.put(LogStateListener.KEY_DEFINITION, o == null ? "" : o.definition);

			Log.printDebug("VODServiceImpl, VOD, logs = " + logs);
			return logs;
		}
	}

	public void showCannotIcon() throws RemoteException {

		if (Resources.appStatus == APP_WATCHING) {
			UIComponent cur = MenuController.getInstance().getCurrentScene();
			if (cur instanceof PlayScreenUI) {
				((PlayScreenUI) cur).showCannot();
			}
		}
	}

    //->Kenneth[2015.7.4] Tank
    // This method is called directly by portal application when portal application's visibility is changed.
    public void portalVisibilityChanged(boolean visible) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug("VODServiceImpl.portalVisibilityChanged("+visible+")");
        PlayScreenUI.isPortalVisible = visible;
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		if (cur instanceof PlayScreenUI) {
            if (visible) {
                if (Log.DEBUG_ON) Log.printDebug("VODServiceImpl.portalVisibilityChanged() : Call hideFlipBar()");
                ((PlayScreenUI) cur).hideFlipBar();
            } else {
                if (Log.DEBUG_ON) Log.printDebug("VODServiceImpl.portalVisibilityChanged() : Call showFlipBar()");
                ((PlayScreenUI) cur).showFlipBar(false);
            }
		}
    }
    //<-
}

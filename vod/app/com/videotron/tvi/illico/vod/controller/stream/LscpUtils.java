package com.videotron.tvi.illico.vod.controller.stream;

public class LscpUtils {
    private static int s_transactionsReceived = 0;

    private static int s_transactionsSent = 0;

    private static int s_streamID = 0;

    private static byte s_lscpStatus = 0;

    public static void incReceivedTransactions() {
        s_transactionsReceived++;
    }

    public static int getTransactionsReceived() {
        return s_transactionsReceived;
    }

    public static void cleanTransactionsReceived() {
        s_transactionsReceived = 0;
    }

    public static void incSentTransactions() {
        s_transactionsSent++;
    }

    public static int getTransactionsSent() {
        return s_transactionsSent;
    }

    public static void cleanTransactionsSent() {
        s_transactionsSent = 0;
    }

    public static void setStreamID(int StreamID) {
        s_streamID = StreamID;
    }

    public static void setStatus(byte Status) {
        s_lscpStatus = Status;
    }

    public static int getLscpStreamID() {
        return s_streamID;
    }

    public static byte getLscpStatus() {
        return s_lscpStatus;
    }

}

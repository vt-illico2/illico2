package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

/**
 * DsmccAdaptationHeader DSMCC Adaptation Headers shall only appear in messages from the Server to the SRM.
 */
public class AdaptationHeader {

    /** field is used to indicate the type of adaptation header. */
    private byte adaptationType = (byte) 0x00;

    /**  */
    private byte[] adaptationData;

    /**
     * Default adaptaion header.
     */
    public AdaptationHeader() {

    }

    /**
     * Constructor with data to be placed in the adaptation header.
     *
     * @param ba
     *            data to be placed in the adaption header
     */
    public AdaptationHeader(byte[] ba) {
        adaptationData = ba;
    }

    /**
     * Sets the data of this adaptation header.
     *
     * @param ba
     *            data to be placed in the adaption header
     */
    public void setData(byte[] ba) {
        adaptationData = ba;
    }

    /**
     * getLength.
     *
     * @return length of the adaptation field should you serialize it.
     */
    public int getLength() {
        if (adaptationData != null) {
            return adaptationData.length + 1;
        } else {
            return 0;
        }
    }

    /**
     * toByteArray.
     *
     * @return byte[] serial representation of this object per DSM-CC
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        try {
            bo.write(adaptationType);
            bo.write(adaptationData);
            rc = bo.toByteArray();
            bo.close();
            bo = null;
        } catch (Exception ex) {
            if (Log.DEBUG_ON) {
                Log.print(ex);
            }
        } finally {
            if (bo != null) {
                try {
                    bo.close();
                    bo = null;
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return rc;
    }

    /**
     * toString. Dumps the contents of this object as a user readable object.
     *
     * @return String for this object
     */
    public String toString() {
        byte[] ba = this.toByteArray();
        String rc = "Adaptation Header Data = ";
        StringBuffer buf = new StringBuffer();
        buf.append(rc);
        for (int i = 0; i < ba.length; i++) {
            buf.append("0x" + Integer.toHexString(ba[i]) + " ");
        }
        return buf.toString();
    }
}

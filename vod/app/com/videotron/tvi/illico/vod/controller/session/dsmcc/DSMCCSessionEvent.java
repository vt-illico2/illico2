/*  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vod.controller.session.dsmcc;

import java.util.EventObject;

/**
 * @author LSC
 * @version 1.0 DSMCCSessionEvent It contains the results of session request.
 */
public class DSMCCSessionEvent extends EventObject {

    private static final long serialVersionUID = 3140263165843184823L;

    /**
     * contains event type. SessionEventType.SETUP_FAILED, SETUP_SUCCEEDED, etc.
     */
    private int eventType;

    /**
     * See ResRspCode for full list of event codes this class provides.
     */
    private int responseCode;

    /** the session name of the event. */
    private byte[] sessionName;

    /** the private data of the event. */
    private byte[] privateData;

    /**
     * @param arg
     *            The object which produced this event.
     */
    public DSMCCSessionEvent(Object arg) {
        super(arg);
    }

    /**
     * Event type getter.
     * 
     * @return int as defined in SessionEventType SETUP_SUCCEEDED, etc.
     */
    public int getEventType() {
        return eventType;
    }

    /**
     * Setter for event type. Called by SessionConnectorImpl parseClientReleaseConfirm, parseClientSessionSEtupConfirm.
     * 
     * @param t
     *            SessionManagerImpl.SETUP_FAILED, SETUP_SUCCEEDED, etc.
     */
    public void setEventType(int t) {
        eventType = t;
    }

    /**
     * Getter for the event response code.
     * 
     * @return Response code (server defined in ISO/IEC 13818-6:1998(E) Table 4.5.
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * Setter for the event response code. Called by SessionConnectorImpl parseClientREleaseConfirm,
     * parseClientSessionSetupConfirm.
     * 
     * @param r
     *            Response code (server defined in ISO/IEC 13818-6:1998(E) Table 4.5.
     */
    public void setResponseCode(int r) {
        responseCode = r;
    }

    /**
     * Getter of the session name.
     * 
     * @return Byte array containing the name of the session
     */
    public byte[] getSessionName() {
        return sessionName;
    }

    /**
     * Sets the session name of the event. Called by SessionConnectorImpl parseClientSessionSetupConfirm.
     * 
     * @param s
     *            Byte Array containing the session name the event is in reference to.
     */
    public void setSessionName(byte[] s) {
        sessionName = s;
    }

    /**
     * Setter for the private data within the event. Called by SessionConnectorImpl parseClientReleaseConfirm and
     * parseClientSessionSetupConfirm.
     * 
     * @param pd
     *            private UU data
     */
    public void setPrivateData(byte[] pd) {
        this.privateData = pd;
    }

    /**
     * Retrieves private data section of event.
     * 
     * @return Byte array containing the private data.
     */
    public byte[] getPrivateData() {
        return privateData;
    }
}

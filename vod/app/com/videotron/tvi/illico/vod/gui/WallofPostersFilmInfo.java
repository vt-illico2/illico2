package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.WallofPostersFilmInfoUI;

public class WallofPostersFilmInfo extends BaseRenderer {

    private Image i01Wallbg;// = dataCenter.getImage("bg.jpg");
    
    int index = 0;

    /**
     * This will be called before the related UIComponent is shown.
     * 
     * @param c
     *            UIComponent to be painted by this renderer.
     * @see UIComponent#prepare
     */
    public void prepare(UIComponent c) {
    	i01Wallbg = dataCenter.getImage("bg.jpg");
        setBackGround(i01Wallbg);
        
        super.prepare(c);
    }

    /**
     * Paints the UIComponent.
     * 
     * @param g
     *            The graphics context to use for painting.
     * @param c
     *            UIComponent to be painted by this renderer.
     */

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        WallofPostersFilmInfoUI ui = (WallofPostersFilmInfoUI) c;
        
        g.setFont(FontResource.BLENDER.getFont(26));
        g.setColor(Color.white);
        //g.drawString(MenuController.getInstance().getLastHistory(), 61, 98);
        
        int sort = ui.getSortStatus();
        if (sort > 0) {
        	String mainTitle = MenuController.getInstance().getLastHistory();
        	BaseUI last = MenuController.getInstance().getLastScene();
    		Channel channel = null;
    		if (last != null 
    				&& last instanceof CategoryUI
    				&& CategoryUI.isBranded()
    				&& (channel = CategoryUI.getChannel()) != null) {
    			
    			if (channel.network != null) {
    				mainTitle = channel.getLanguageContentData(channel.network.title) + " ≥ " + mainTitle;
    			} else {
    				mainTitle = channel.getLanguageContentData(channel.title) + " ≥ " + mainTitle;
    			}
    		} 
    		g.drawString(mainTitle, 61, 98);
    		
        	g.setFont(FontResource.BLENDER.getFont(18));
        	//String str = dataCenter.getString(sort==1?Resources.TEXT_LBL_SORT_NAME:Resources.TEXT_LBL_SORT_DATE);
        	String str = "";
			switch (sort) {
			case 1:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_NAME);
				break;
			case 2:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_DATE);
				break;
			case 3:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_PROD);
				break;
			case 4:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_DEFAULT);
				break;
			}
        	GraphicUtil.drawStringRight(g, str, 909, 95);
        } else { // channels
        	g.drawString(DataCenter.getInstance().getString(
        			ui.getMode() == WallofPostersFilmInfoUI.MODE_ALL_CHANNEL 
        			? Resources.TEXT_VIEW_ALL_CHANNEL : Resources.TEXT_VIEW_MY_CHANNEL)
        			, 61, 98);
        }
    }
}

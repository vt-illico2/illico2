package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.MenuTreeUI;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

public class MenuTreeRenderer extends BaseRenderer {

	private Rectangle menuRec = new Rectangle(51, 55, 960 - 51, 450);

	// private Image i01Menubg = dataCenter.getImage("00_menubg.png");
	private Image i01Bullet01;// = dataCenter.getImage("01_bullet01.png");
	private Image i01Menufocus;// = dataCenter.getImage("00_menufocus.png");
	private Image i01MenufocusDim;// = dataCenter.getImage("00_menufocus_dim.png");
	private Image i01ToolmenuBg;// = dataCenter.getImage("01_toolmenu_bg.png");
	private Image i_02_ars_t;// = dataCenter.getImage("02_ars_t.png");
	private Image i_02_ars_b;// = dataCenter.getImage("02_ars_b.png");
	private Image i01ToolmenuIcon01;// = dataCenter.getImage("01_toolmenu_icon01.png");
	private Image i01ToolmenuIcon02;// = dataCenter.getImage("01_toolmenu_icon02.png");
	private Image i01ToolmenuIcon03;// = dataCenter.getImage("01_toolmenu_icon03.png");
	private Image i01ToolmenuIcon01Foc;// = dataCenter.getImage("01_toolmenu_icon01_foc.png");
	private Image i01ToolmenuIcon02Foc;// = dataCenter.getImage("01_toolmenu_icon02_foc.png");
	private Image i01ToolmenuIcon03Foc;// = dataCenter.getImage("01_toolmenu_icon03_foc.png");
	// R5
	private Image iIconEvent, iIconEventFocus;
	private Image iIconFavorite;
	private Image iBtnA;
	private Image i01ToolmenuIcon04, i01ToolmenuIcon04Foc;
	private Image wishIconImg, blockIconImg, previewIconImg;
	
	// 4K
//	private Image i00TopShadow;// = dataCenter.getImage("00_top_shadow.png");
//	private Image i00BottomShadow;// = dataCenter.getImage("00_bottom_shadow.png");

    // R7.3
    private Image i01_menubox_search;

	private Color cTitle = new Color(251, 217, 89);
	private Color cMenuNormal = new Color(230, 230, 230);
	private Color cMenuFunction = new Color(182, 182, 182);
	private Color cMenufocused = new Color(3, 3, 3);
	private Color cHeader = new Color(245, 212, 6);
	private Color cHeaderGray = new Color(187, 189, 192);
	private Color cLine = new Color(38, 38, 38);
	// R5
	private Color cTabColor = new Color(82, 82, 82);
	private Color cTabLineColor = new Color(43, 43, 43);
	
	// private Color cBottomMsg = new Color(236, 211, 143);
	// private final String replaceTarget = "menu_name";

	public Rectangle getPreferredBounds(UIComponent c) {
		return menuRec;
	}

	public void prepare(UIComponent c) {
		i01Bullet01 = dataCenter.getImage("01_bullet01.png");
		i01Menufocus = dataCenter.getImage("00_menufocus.png");
		i01MenufocusDim = dataCenter.getImage("00_menufocus_dim.png");
		i01ToolmenuBg = dataCenter.getImage("01_toolmenu_bg.png");
		i_02_ars_t = dataCenter.getImage("02_ars_t.png");
		i_02_ars_b = dataCenter.getImage("02_ars_b.png");
		i01ToolmenuIcon01 = dataCenter.getImage("02_icn_search.png");
		i01ToolmenuIcon02 = dataCenter.getImage("01_toolmenu_icon02.png");
		i01ToolmenuIcon03 = dataCenter.getImage("01_toolmenu_icon03.png");
		i01ToolmenuIcon01Foc = dataCenter.getImage("02_icn_search_f.png");
		i01ToolmenuIcon02Foc = dataCenter.getImage("01_toolmenu_icon02_foc.png");
		i01ToolmenuIcon03Foc = dataCenter.getImage("01_toolmenu_icon03_foc.png");
		// R5
		i01ToolmenuIcon04 = dataCenter.getImage("01_toolmenu_icon04.png");
		i01ToolmenuIcon04Foc = dataCenter.getImage("01_toolmenu_icon04_f.png");
		
		// R5
		iIconEvent = dataCenter.getImage("icon_event.png");
		iIconEventFocus = dataCenter.getImage("icon_event_f.png");
		iIconFavorite = dataCenter.getImage("01_menu_icon_favorite.png");
		Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
		iBtnA = (Image) imgTable.get(PreferenceService.BTN_A);
		
		wishIconImg = dataCenter.getImage("01_menu_icon_favorite.png");
		blockIconImg = dataCenter.getImage("02_icon_con.png");
		previewIconImg = dataCenter.getImage("icon_event.png");
		
		// 4K
//		i00TopShadow = dataCenter.getImage("00_top_shadow.png");
//		i00BottomShadow = dataCenter.getImage("00_bottom_shadow.png");

        // R7.3
        i01_menubox_search = dataCenter.getImage("01_menubox_search.png");
		
		super.prepare(c);
	}

	protected void paint(Graphics g, UIComponent c) {
		UIComponent curScene = MenuController.getInstance().getCurrentScene();
//		if (curScene instanceof CategoryUI == false) {
//			return;
//		}

		try {
			g.translate(-menuRec.x, -menuRec.y);

			MenuTreeUI ui = (MenuTreeUI) c;

			boolean isInCOD = ui.isInCOD();

			// g.drawImage(i01Menubg, 52, 71, ui);

			CategoryContainer category;
			String titleStr;
			g.setFont(FontResource.BLENDER.getFont(20));
			Image homeIcon = null;
			int x = 66;
			boolean isBranded = CategoryUI.isBranded() && MenuTreeUI.MENU_SLOT == 10;
			if (isBranded) {
                BrandingElement br = CategoryUI.getBrandingElement();

                CategoryContainer chCategory = CategoryUI.getChannelCategory();
                int posX = 56;
                if (chCategory != null) {
                    if (ChannelInfoManager.getInstance().isBlockedChannel(chCategory.getTopic())) {
                        g.drawImage(blockIconImg, posX + 3, 79 + 26 - blockIconImg.getHeight(c) / 2, c);
                    } else if (ChannelInfoManager.getInstance().isFavoriteChannel(chCategory.getTopic())) {
                        g.drawImage(wishIconImg, posX, 79 + 22 - wishIconImg.getHeight(c) / 2, c);
                    } else if (ChannelInfoManager.getInstance().isPreviewChannel(chCategory.getTopic())) {
                        g.drawImage(previewIconImg, posX, 79 + 22 - previewIconImg.getHeight(c) / 2, c);
                    }

                    posX += 28;
                }

                if (Log.ALL_ON) {
                    Log.printDebug("MenuTreeRenderer, br=" + br);
                }

				if (br != null && br.logos[BrandingElement.LOGO_MD] != null) {
                    if (Log.ALL_ON) {
                        Log.printDebug("MenuTreeRenderer, br.logos[BrandingElement.LOGO_MD]=" + br.logos[BrandingElement.LOGO_MD]);
                    }
					g.drawImage(br.logos[BrandingElement.LOGO_MD], posX, 79, c);
				}
//				Channel ch = ((CategoryUI) curScene).getChannel();
//				if (ch != null) {
//					if (ch.network != null && ch.network.logos[Network.LOGO_MD] != null) {
//						g.drawImage(ch.network.logos[Network.LOGO_MD], 61, 79, c);
//					} else if (ch.logos[Channel.LOGO_MD] != null){
//						g.drawImage(ch.logos[Channel.LOGO_MD], 61, 79, c);
//					}
//				} else { // VSD 
//					BrandingElement br = ((CategoryUI) curScene).getBrandingElement();
//					if (br != null && br.logos[Network.LOGO_MD] != null) {
//						g.drawImage(br.logos[Network.LOGO_MD], 61, 79, c);
//					}
//				}
				// VDTRMASTER-5796
			} else if (MenuController.getInstance().getHistorySize() == 0
					|| CatalogueDatabase.getInstance().getRoot() == null) {
				if (CategoryUI.getInstance().isTop() && ((CategoryUI) curScene).isSingleton()) {
				    // R7.3 removed
//					titleStr = dataCenter.getString(Resources.TEXT_T_HOME);
					
					// R5-TANK remove
//					homeIcon = dataCenter.getImage("00_icon01.png");
//					g.drawImage(homeIcon, 58, 76, c);
//					x += homeIcon.getWidth(c);

                    // R7.3
                    titleStr = dataCenter.getString("menu.search");
                    g.drawImage(i01_menubox_search, 52, 71, c);
                    if (CategoryUI.getInstance().isFocusOnSearch()) {
                        g.drawImage(i01Menufocus, 49, 68, ui);
                        g.drawImage(i01ToolmenuIcon01Foc, 66, 81, ui);
                        g.setColor(cMenufocused);
                        g.setFont(FontResource.BLENDER.getFont(21));
                        g.drawString(titleStr, 95, 94);
                    } else {
                        g.drawImage(i01ToolmenuIcon01, 66, 81, ui);
                        g.setColor(cMenuNormal);
                        g.drawString(titleStr, 95,  94);
                        int index = ui.getMenuIndex();

                        if (index == 0 && ui.getMenuLenth() != 0) {
                            g.drawImage(i_02_ars_t, 189, 87, c);
                        }
                    }
				} else {
					titleStr = TextUtil.shorten(MenuController.getInstance()
							.getLastHistory(), g.getFontMetrics(), 230);
					
					Object obj = MenuController.getInstance().getListHistoryObject();
					CategoryContainer parent = ui.getCurrentCategory().parent;
					if (parent != null && parent.type == MenuController.MENU_KARAOKE_PACK) {
						int cnt = 0;
						for (int i = 0; i < parent.categoryContainer.length; i++) {
							cnt += parent.categoryContainer[i].leafCount;
						}
						titleStr += " (" + cnt + ")";
					} else if (ui.getCurrentCategory().type == MenuController.MENU_CHANNEL 
							&& ui.getCODCategory() != null && isInCOD) { // VDTRMASTER-5549
                        // R7.3
                        // VDTRMASTER-6091
						titleStr = dataCenter.getString(Resources.TEXT_LBL_LIST_OF_CHANNEL);
					} else if (obj instanceof CategoryContainer
							&& ((CategoryContainer) obj).type == MenuController.MENU_CHANNEL) {
						CategoryContainer chCategory = ((CategoryContainer) obj);
						if (ChannelInfoManager.getInstance().isBlockedChannel(chCategory.getTopic())) {
							g.drawImage(blockIconImg, x + 3, 92 - blockIconImg.getHeight(c)/2, c);
						} else if (ChannelInfoManager.getInstance().isFavoriteChannel(chCategory.getTopic())) {
							g.drawImage(wishIconImg, x, 87 - wishIconImg.getHeight(c)/2, c);
						} else if (ChannelInfoManager.getInstance().isPreviewChannel(chCategory.getTopic())) {
							g.drawImage(previewIconImg, x, 87 - previewIconImg.getHeight(c)/2, c);
						}
						x += 28;
					}
					// R7.3
                    g.setColor(cMenufocused);
                    g.drawString(titleStr, x, 95);
                    g.setColor(cTitle);
                    g.drawString(titleStr, x + 1, 94);
				}

			} else { // R5 - TANK
				// R7.3
				if (ui.getCurrentCategory().type == MenuController.MENU_CHANNEL && isInCOD) {
				    // VDTRMASTER-6091
					titleStr = dataCenter.getString(Resources.TEXT_LBL_LIST_OF_CHANNEL);
                } else {
					titleStr = TextUtil.shorten(MenuController.getInstance()
							.getLastHistory(), g.getFontMetrics(), 230);
				}
				g.setColor(cMenufocused);
				g.drawString(titleStr, x, 95);
				g.setColor(cTitle);
				g.drawString(titleStr, x + 1, 94);
			}

			int index = ui.getMenuIndex();
			int focus = ui.getFocus();
			int menuLen = ui.getMenuLenth();
			int k;
			int gap = 34;
			int bookmarkSize = BookmarkManager.getInstance()
					.numberActivePurchase(false);
			int bookmarkSizeAdult = BookmarkManager.getInstance()
					.numberActivePurchase(true);
			int wishListSize = BookmarkManager.getInstance().retrieveWishlistTitles(true).length;
			int wishListSizeAdult = 0;
			if (BookmarkManager.getInstance().retrieveWishlistTitlesAdult() != null) {
				wishListSizeAdult = BookmarkManager.getInstance().retrieveWishlistTitlesAdult().length;
			}
			
			int toolsIndex = -1;

			CategoryContainer[] menu = ui.getMenu();
			if (menu == null || menu.length == 0) {
				return;
			}
			if (isBranded) {
				g.translate(0, gap);
			}

			int i;
			int unsubscribedTabIdx = ChannelInfoManager.getInstance().getUnsubscribedTabIndex();
			for (i = 0; i < MenuTreeUI.MENU_SLOT; i++) {
				if (i >= menuLen) {
					break;
				}
				k = index - focus + i;
				if (k >= menuLen) {
					break;
				}
				category = menu[k];
				g.setFont(FontResource.BLENDER.getFont(18));
				// R5 -> R7.3
				boolean isSpecialTab = true;
				if (category.type == MenuController.MENU_SUBSCRIBED_CHANNELS) { // VDTRMASTER-6091
                    titleStr = dataCenter.getString(Resources.TEXT_LBL_MY_CHANNELS)
                            + " (" + ChannelInfoManager.getInstance().getSubscribedChannelsCount() + ")";
                } else if (category.type == MenuController.MENU_UNSUBSCRIBED_CHANNELS) {
					titleStr = dataCenter.getString(Resources.TEXT_LBL_UNSUBSCRIBED_CHANNELS)
							+ " (" + ChannelInfoManager.getInstance().getUnsubscribedChannelsCount() + ")";
				} else {
					titleStr = category.getTitle();
                    isSpecialTab = false;
				}
				g.setColor(cMenuFunction);
				boolean miniline = true;
				if (category.parent != null && category.parent.type == MenuController.MENU_KARAOKE_PACK) {
					titleStr = titleStr + " (" + category.leafCount + ")";
				} else if (category.type == MenuController.MENU_SEARCH) {
					toolsIndex = i;
					if (toolsIndex > 0 && ui.getStep() == 0) {
						int storey = MenuTreeUI.MENU_SLOT - toolsIndex;
						if (storey > 3) {
							storey = 3;
						}
						// R7.3 removed
//						g.drawImage(i01ToolmenuBg, 52, 104 + toolsIndex * gap,
//								296, gap * storey, ui);
						toolsIndex = 0;
					}
					g.drawImage(i01ToolmenuIcon01, 60, 110 + i * gap, ui);
					miniline = false;
				} else if (category.type == MenuController.MENU_RESUME) {
				    // R7.3
                    toolsIndex = i;

                    int storey = MenuTreeUI.MENU_SLOT - toolsIndex;
                    if (storey > 2) {
                        storey = 2;
                    }

                    g.drawImage(i01ToolmenuBg, 52, 104 + toolsIndex * gap,
                            296, gap * storey, ui);
                    toolsIndex = 0;

					titleStr = titleStr + " (" + bookmarkSize + ")";
					g.drawImage(i01ToolmenuIcon02, 60, 110 + i * gap, ui);
				} else if (category.type == MenuController.MENU_RESUME_ADULT) {
					titleStr = titleStr + " (" + bookmarkSizeAdult + ")";
					g.drawImage(i01ToolmenuIcon02, 60, 110 + i * gap, ui);
				} else if (category.type == MenuController.MENU_WISHLIST) {
					titleStr = titleStr + " (" + wishListSize + ")";
					g.drawImage(i01ToolmenuIcon04, 60, 110 + i * gap, ui);
				} else if (category.type == MenuController.MENU_WISHLIST_ADULT) {
					titleStr = titleStr + " (" + wishListSizeAdult + ")";
					g.drawImage(i01ToolmenuIcon04, 60, 110 + i * gap, ui);
				} else if (category.type == MenuController.MENU_ABOUT) {
					Channel ch = CategoryUI.getChannel();
					titleStr = ch == null ? "" 
							: DataCenter.getInstance().getString(ch.hasAuth ? Resources.TEXT_TUNE_TO 
									: CommunicationManager.getInstance().isaSupported(ch.callLetters)
							|| !CommunicationManager.getInstance().isIncludedInExcludedData(ch.callLetters)
							? Resources.TEXT_SUBSCRIBE : Resources.TEXT_ADD_THIS_CHANNEL);
					g.drawImage(i01Bullet01, 72, 119 + i * gap, ui);
				} else if (isSpecialTab) { // R5
					// background and line
					g.setColor(cTabColor);
					g.fillRect(52, 106 + i * gap, 296, 33);
					g.setColor(cTabLineColor);
					g.fillRect(52, 139 + i * gap, 296, 1);
				} else if (category.type == MenuController.MENU_CHANNEL && isInCOD && unsubscribedTabIdx > k) { // R7.3
                    if (ChannelInfoManager.getInstance().isPreviewChannel(category.getTopic())) {
                        g.drawImage(iIconEvent, 58, 110 + i * gap, ui);
                    } else if (ChannelInfoManager.getInstance().isFavoriteChannel(category.getTopic())) {
                        g.drawImage(iIconFavorite, 58, 110 + i * gap, ui);
                    }
				} else if (!isInCOD) {
					g.drawImage(i01Bullet01, 72, 119 + i * gap, ui);
				}
				g.setColor(cMenuNormal);
				g.drawString(
						TextUtil.shorten(titleStr, g.getFontMetrics(), 230),
						95, 128 + i * gap);
				if (miniline && i > 0 && i < MenuTreeUI.MENU_SLOT) {
					//g.drawImage(i00MiniLine, 66, 105 + i * gap, ui);
					g.setColor(cLine);
					g.drawLine(66, 105 + i * gap, 66 + 268, 105 + i * gap);
				}
			}
			if (i < MenuTreeUI.MENU_SLOT) {
				//g.drawImage(i00MiniLine, 66, 105 + i * gap, ui);
				g.setColor(cLine);
				g.drawLine(66, 105 + i * gap, 66 + 268, 105 + i * gap);
			}

			if (isBranded) {
				g.translate(0, -4);
			}
			// shadow and arrow
			if (index - focus > 0) {
//				g.drawImage(i00TopShadow, 53, 104, c);
				g.drawImage(i_02_ars_t, 189, 97, c);
			}
			if (isBranded) {
				g.translate(0, -gap + 4);
			}
			if (menuLen - MenuTreeUI.MENU_SLOT > index - focus) {
//				g.drawImage(i00BottomShadow, 52, 449, c);
				g.drawImage(i_02_ars_b, 189, 469, c);
			}

			// focus
			if (isBranded) {
				g.translate(0, gap);
			}

			// R7.3
			if (ui.getStep() == 0 && !CategoryUI.getInstance().isFocusOnSearch()) {
				if (((CategoryUI) curScene).getCurrentFocusUI() == CategoryUI.FOCUS_MENU) {
					g.drawImage(i01Menufocus, 49, 102 + focus * gap, ui);
				} else {
					g.drawImage(i01MenufocusDim, 49, 102 + focus * gap, ui);
				}
				category = menu[index];
                if (category.type == MenuController.MENU_SUBSCRIBED_CHANNELS) { // VDTRMASTER-6091
                    titleStr = dataCenter.getString(Resources.TEXT_LBL_MY_CHANNELS)
                            + " (" + ChannelInfoManager.getInstance().getSubscribedChannelsCount() + ")";
                } else if (category.type == MenuController.MENU_UNSUBSCRIBED_CHANNELS) {
					titleStr = dataCenter.getString(Resources.TEXT_LBL_UNSUBSCRIBED_CHANNELS)
                            + " (" + ChannelInfoManager.getInstance().getUnsubscribedChannelsCount() + ")";
				} else {
					titleStr = category.getTitle();
				}
				if (category.parent != null && category.parent.type == MenuController.MENU_KARAOKE_PACK) {
					titleStr = titleStr + " (" + category.leafCount + ")";
				} else {
					switch (category.type) {
					case MenuController.MENU_SEARCH:
						g.drawImage(i01ToolmenuIcon01Foc, 60, 110 + focus * gap, ui);
						break;
					case MenuController.MENU_RESUME:
						g.drawImage(i01ToolmenuIcon02Foc, 60, 110 + focus * gap, ui);
						titleStr = titleStr + " (" + bookmarkSize + ")";
						break;
					case MenuController.MENU_RESUME_ADULT:
						g.drawImage(i01ToolmenuIcon02Foc, 60, 110 + focus * gap, ui);
						titleStr = titleStr + " (" + bookmarkSizeAdult + ")";
						break;
					case MenuController.MENU_WISHLIST:
						g.drawImage(i01ToolmenuIcon04Foc, 60, 110 + focus * gap, ui);
						titleStr = titleStr + " (" + wishListSize + ")";
						break;
					case MenuController.MENU_WISHLIST_ADULT:
						g.drawImage(i01ToolmenuIcon04Foc, 60, 110 + focus * gap, ui);
						titleStr = titleStr + " (" + wishListSizeAdult + ")";
						break;
					case MenuController.MENU_ABOUT:
						Channel ch = CategoryUI.getChannel();
						titleStr = ch == null ? "" 
								: DataCenter.getInstance().getString(ch.hasAuth ? Resources.TEXT_TUNE_TO 
										: CommunicationManager.getInstance().isaSupported(ch.callLetters)
								|| !CommunicationManager.getInstance().isIncludedInExcludedData(ch.callLetters)
								? Resources.TEXT_SUBSCRIBE : Resources.TEXT_ADD_THIS_CHANNEL);
					}
				} 
				
				// R5
				if (category.type == MenuController.MENU_CHANNEL 
						&& ChannelInfoManager.getInstance().isPreviewChannel(category.getTopic())
                        && unsubscribedTabIdx > index) {
					g.drawImage(iIconEventFocus, 58, 110 + focus * gap, ui);
				}
				g.setColor(cMenufocused);
				g.setFont(FontResource.BLENDER.getFont(21));
				g.drawString(
						TextUtil.shorten(titleStr, g.getFontMetrics(), 237),
						95, 128 + focus * gap);
			}
			if (isBranded) {
				g.translate(0, -gap);
			}

			// title of content area
			if (ui.getCurrentCategory() != null) {
				// R5-TANK - update to come into COD from Mainmenu.
				if (CategoryUI.getInstance().isTop() && ((CategoryUI) curScene).isSingleton() && isBranded == false) {
					g.setFont(FontResource.BLENDER.getFont(28));
					g.setColor(cHeader);
					String welcomeMsg = DataCenter.getInstance().getString(
							Resources.TEXT_MSG_MAIN_TOP);
					String title = ui.getCurrentCategory().getTitle();
					x = 375;
					g.drawString(welcomeMsg, x, 97);
					x += g.getFontMetrics().stringWidth(welcomeMsg);
					g.setColor(Color.white);
					g.setFont(FontResource.BLENDER.getFont(20));
					if (ui.getCurrentCategory().type == MenuController.MENU_RESUME) {
						title += " (" + bookmarkSize + ")";
					} else if (ui.getCurrentCategory().type == MenuController.MENU_RESUME_ADULT) {
						title += " (" + bookmarkSizeAdult + ")";
					} else if (ui.getCurrentCategory().type == MenuController.MENU_WISHLIST) {
						title += " (" + wishListSize + ")";
					} else if (ui.getCurrentCategory().type == MenuController.MENU_WISHLIST_ADULT) {
						title += " (" + wishListSizeAdult + ")";
					}
					title = " - " + title;
					if (x + g.getFontMetrics().stringWidth(title) > 890) {
						title = TextUtil.shorten(title, g.getFontMetrics(),
								890 - x);
					}
					g.drawString(title, x, 97);
				} else if (ui.getCurrentCategory().type != MenuController.MENU_KARAOKE_CONTENTS) {
					g.setColor(cHeaderGray);
					g.setFont(FontResource.BLENDER.getFont(28));
					if (ui.getCurrentCategory().type == MenuController.MENU_ABOUT) {
						Channel ch = CategoryUI.getChannel();
						titleStr = ch == null ? "" 
								: ch.hasAuth ? DataCenter.getInstance().getString(Resources.TEXT_TUNE_TO) 
										: CommunicationManager.getInstance().isaSupported(ch.callLetters)
								|| !CommunicationManager.getInstance().isIncludedInExcludedData(ch.callLetters)
								? DataCenter.getInstance().getString(Resources.TEXT_SUBSCRIBE_SVOD) + ch.getLanguageContentData(ch.title)
												: DataCenter.getInstance().getString(Resources.TEXT_ADD) + ch.getLanguageContentData(ch.title);
						g.drawString(titleStr, 375, 97);
					} else {
						
						if (CategoryUI.getChannelCategory() != null
								&& ui.getCurrentCategory().type != MenuController.MENU_CHANNEL) {
							int posX = 375;
							g.drawString(CategoryUI.getChannelCategory().getTitle(), 375, 97);
							posX = 375 + FontResource.getFontMetrics(FontResource.BLENDER.getFont(28))
									.stringWidth(CategoryUI.getChannelCategory().getTitle());
							g.setFont(FontResource.BLENDER.getFont(19));
							g.drawString(" - " + ui.getCurrentCategory().getTitle(), posX, 97);
						} else {
							g.drawString(ui.getCurrentCategory().getTitle(), 375, 97);
						}
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			g.translate(menuRec.x, menuRec.y);
		}
	}
}

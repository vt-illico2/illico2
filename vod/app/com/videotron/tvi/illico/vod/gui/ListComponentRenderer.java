package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ListComp;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;

public class ListComponentRenderer extends BaseRenderer {

	private Image i01WishlistBg;// = dataCenter.getImage("01_wishlist_bg.png");
//	private Image i07Listshadow;// = dataCenter.getImage("07_listshadow.png");
	private Image i07ListLine;// = dataCenter.getImage("07_list_line.png");
//	private Image i07ListShT;// = dataCenter.getImage("07_list_sh_t.png");
//	private Image i07ListShB;// = dataCenter.getImage("07_list_sh_b.png");
	private Image i07ListFoc;// = dataCenter.getImage("07_list_foc.png");
	private Image i07ListFocDim;// = dataCenter.getImage("07_list_foc_dim.png");
	private Image i02ArsB;// = dataCenter.getImage("02_ars_b.png");
	private Image i02ArsT;// = dataCenter.getImage("02_ars_t.png");
//	private Image i_07_dayglow_t;
//	private Image i_07_dayglow_b;
//	private Image i_03_icon_minus;
//	private Image i_03_icon_plus;
//	private Image i_03_icon_minus_foc;
//	private Image i_03_icon_plus_foc;
	private Image i_07_series;
	private Image i_07_series_f;

	private Color cHeader = new Color(202, 174, 97);
	private Color c10 = new Color(10, 10, 10);

	private static ListComponentRenderer instance;

	private ListComponentRenderer() {
	}

	synchronized public static ListComponentRenderer getInstance() {
		if (instance == null) {
			instance = new ListComponentRenderer();
		}
		return instance;
	}

	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return Resources.fullRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		i01WishlistBg = dataCenter.getImage("01_wishlist_bg.png");
//		i07Listshadow = dataCenter.getImage("07_listshadow.png");
		i07ListLine = dataCenter.getImage("07_list_line.png");
//		i07ListShT = dataCenter.getImage("07_list_sh_t.png");
//		i07ListShB = dataCenter.getImage("07_list_sh_b.png");
		i07ListFoc = dataCenter.getImage("07_list_foc.png");
		i07ListFocDim = dataCenter.getImage("07_list_foc_dim.png");
		i02ArsB = dataCenter.getImage("02_ars_b.png");
		i02ArsT = dataCenter.getImage("02_ars_t.png");
//		i_07_dayglow_t = dataCenter.getImage("07_dayglow_t.png");
//		i_07_dayglow_b = dataCenter.getImage("07_dayglow_b.png");
//		i_03_icon_minus = dataCenter.getImage("03_icon_minus.png");
//		i_03_icon_plus = dataCenter.getImage("03_icon_plus.png");
//		i_03_icon_minus_foc = dataCenter.getImage("03_icon_minus_foc.png");
//		i_03_icon_plus_foc = dataCenter.getImage("03_icon_plus_foc.png");
		i_07_series = dataCenter.getImage("07_series.png");
		i_07_series_f = dataCenter.getImage("07_series_f.png");
		super.prepare(c);
	}

	int gap = 32;

	protected void paint(Graphics g, UIComponent c) {
		g.translate(-c.getX(), -c.getY());

		g.drawImage(i01WishlistBg, 53, 114, c);
//		g.drawImage(i07Listshadow, 6, 466, c);
		ListComp ui = (ListComp) c;
		BaseElement[] catalogues = ui.getData();
		long curTime = System.currentTimeMillis();

		String listHeader = ui.getListHeader();
		g.setFont(FontResource.BLENDER.getFont(20));
		g.setColor(Color.black);
		// VDTRMASTER-5682
		listHeader = TextUtil.shorten(listHeader, g.getFontMetrics(), 400);
		g.drawString(listHeader, 71, 140);
		g.setColor(cHeader);
		g.drawString(listHeader, 70, 139);
		
		// draw list
		int focus = ui.getFocus();
		int index = ui.getTitleIndex();
		int k;
		// g.drawString("f="+focus+",i="+index, 300, 40);
		MoreDetail title = null;
		g.setFont(FontResource.BLENDER.getFont(18));
		int w = ui.getListType() == ListComp.TYPE_RESUME ? 250 : 400;
		for (int i = 0; i < ListComp.MAX_SIZE; i++) {
			k = index - focus + i;
			if (k >= catalogues.length) {
				break;
			}
			title = (MoreDetail) catalogues[k];
//			if (ui.getListType() != ListComp.TYPE_NORMAL && title instanceof Bundle) {
//				g.setColor(c10);
//				g.fillRect(54, 153 + i * gap, 433, 32);
//				g.drawImage(i_07_dayglow_t, 54, 146 + i * gap, c);
//				g.drawImage(i_07_dayglow_b, 54, 186 + i * gap, c);
//			}
			g.setColor(Color.white);
			String titleStr = null;
			if (MenuController.getInstance().isAdultBlocked(title)) {
				titleStr = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
			} else {
				titleStr = getTitleWithYearSingleLine(title, g.getFontMetrics(), w);
				if (title instanceof Bundle && ui.getListType() == ListComp.TYPE_RESUME) {
					titleStr += " (" + ((Bundle) title).getTitles().length + ")";
				}
			}
//			String titleStr = MenuController.getInstance()
//					.isAdultBlocked(title) ? dataCenter
//					.getString(Resources.TEXT_LBL_BLOCKED_TITLE)
//					: getTitleWithYearSingleLine(title, g.getFontMetrics(), w);
			g.drawString(titleStr, 73, 175 + i * gap);
			// g.drawString("k="+k, 273, 175 + i * gap);

			if (ui.getListType() == ListComp.TYPE_RESUME) {
				Bookmark bookmark = null;
				if (title instanceof Video) {
					bookmark = BookmarkManager.getInstance().getPurchased(
							(Video) title);
				} else if (title instanceof CategorizedBundle) {
					bookmark = BookmarkManager.getInstance().getPurchased(
							(CategorizedBundle) title);
				} else if (title instanceof Bundle) {
					bookmark = BookmarkManager.getInstance().getPurchased(
							(Bundle) title);
				}
				
				if (bookmark != null) {
					String remain = Formatter.getCurrent().getDurationText(
							bookmark.getLeaseEnd() - curTime);
					GraphicUtil.drawStringRight(g, remain, 468, 175 + i * gap);
					if (title instanceof Bundle) {
						g.drawImage(i_07_series, 51, 157 + i * gap, c);
					}
				} else {
					//GraphicUtil.drawStringRight(g, "NULL", 468, 175 + i * gap);
				}
			} else if (ui.getListType() == ListComp.TYPE_WISHLIST) {
				if (title instanceof Bundle) {
					//g.drawImage(((Bundle) title).collapsed ? i_03_icon_plus : i_03_icon_minus, 445, 159 + i * gap, c);
					//g.drawImage(i_07_series, 53, 159 + i * gap, c);
				}
			}
			if ( i < ListComp.MAX_SIZE - 1 && k < catalogues.length - 1) {
				g.drawImage(i07ListLine, 70, 185 + i * gap, c);
			}
		}
		
		if (catalogues.length > index) { // focus
			g.setColor(Color.black);
			g.drawImage(ui.getState() == BaseUI.STATE_ACTIVATED ? i07ListFoc
					: i07ListFocDim, 52, 150 + focus * gap, c);
			title = (MoreDetail) catalogues[index];
			String titleStr = null;
			if (MenuController.getInstance().isAdultBlocked(title)) {
				titleStr = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
			} else {
				titleStr = getTitleWithYearSingleLine(title, g.getFontMetrics(), w);
				if (title instanceof Bundle && ui.getListType() == ListComp.TYPE_RESUME) {
					titleStr += " (" + ((Bundle) title).getTitles().length + ")";
				}
			}
//			String titleStr = MenuController.getInstance()
//			.isAdultBlocked(title) ? dataCenter
//			.getString(Resources.TEXT_LBL_BLOCKED_TITLE)
//			: getTitleWithYearSingleLine(title, g.getFontMetrics(), w);
			g.drawString(titleStr, 73, 175 + focus * gap);
			
			if (ui.getListType() == ListComp.TYPE_RESUME) {
				Bookmark bookmark = null;
				if (title instanceof Video) {
					bookmark = BookmarkManager.getInstance().getPurchased(
							(Video) title);
				} else if (title instanceof CategorizedBundle) {
					bookmark = BookmarkManager.getInstance().getPurchased(
							(CategorizedBundle) title);
				} else if (title instanceof Bundle) {
					bookmark = BookmarkManager.getInstance().getPurchased(
							(Bundle) title);
				}
				if (bookmark != null) {
					String remain = Formatter.getCurrent().getDurationText(
							bookmark.getLeaseEnd() - curTime);
					GraphicUtil.drawStringRight(g, remain, 468, 175 + focus * gap);
					if (title instanceof Bundle) {
						g.drawImage(i_07_series_f, 51, 157 + focus * gap, c);
					}
				} else {
//					GraphicUtil.drawStringRight(g, "NULL", 468, 175 + focus * gap);
				}
			} else if (ui.getListType() == ListComp.TYPE_WISHLIST) {
				if (title instanceof Bundle) {
					//g.drawImage(((Bundle) title).collapsed ? i_03_icon_plus_foc : i_03_icon_minus_foc, 445, 159 + focus * gap, c);
					//g.drawImage(i_07_series_f, 53, 159 + focus * gap, c);
				}
			}
		}
		if (index - focus > 0) {
//			g.drawImage(i07ListShT, 54, 153, c);
			g.drawImage(i02ArsT, 259, 145, c);
		}
		if (catalogues.length - ListComp.MAX_SIZE > index - focus) {
//			g.drawImage(i07ListShB, 54, 415, c);
			g.drawImage(i02ArsB, 259, 465, c);
		}

		g.translate(c.getX(), c.getY());
	}

}

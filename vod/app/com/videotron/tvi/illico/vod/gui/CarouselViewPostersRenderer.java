package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CarouselViewPostersUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

//separation GUI from CarouselViewRenderer for Animation
public class CarouselViewPostersRenderer extends BaseRenderer {

	private BaseElement[] catalogues;

	private Rectangle postersRec = new Rectangle(0, 70, 960, 370);

	private Image i01ArrowL;// = dataCenter.getImage("01_arrow_l.png");
	private Image i01ArrowR;// = dataCenter.getImage("01_arrow_r.png");
//	private Image i01PosterSha;// = dataCenter.getImage("01_possh_89.png");
	private Image i02_icon_con;
	private Image blockedPortrait;// = dataCenter.getImage("01_poster_block.png");
	private Image blockedLandscape;
	// R5
	private Image i02_icon_fav, i_icon_uhd;
	private int dataLen;
	
	public boolean drawnLeft;
	public boolean drawnRight;

	public static Rectangle[] bounds;
	public static Rectangle[] pBounds = new Rectangle[] {
			new Rectangle(24, 192, 90, 135),
			new Rectangle(848, 192, 90, 135),
			new Rectangle(75, 177, 110, 165),
			new Rectangle(776, 177, 110, 165),
			new Rectangle(150, 154, 144, 216),
			new Rectangle(667, 154, 144, 216),
			new Rectangle(239, 123, 176, 264),
			new Rectangle(545, 123, 176, 264),
			new Rectangle(375, 89, 210, 315)
	};
	public static Rectangle[] lBounds = new Rectangle[] {
			new Rectangle(24-5, 192+42, 80, 60),
			new Rectangle(848-5, 192+42, 80, 60),
			new Rectangle(75-5, 177+36, 120, 90),
			new Rectangle(776-5, 177+36, 120, 90),
			new Rectangle(150-8, 154+43, 160, 120),
			new Rectangle(667-8, 154+43, 160, 120),
			new Rectangle(239-12, 123+51, 200, 150),
			new Rectangle(545-12, 123+51, 200, 150),
			new Rectangle(340, 142, 280, 210)
	};

	private Image[] lastImg = new Image[pBounds.length]; 
	
	public DVBColor[] dim = new DVBColor[] {
			new DVBColor(0.0f, 0.0f, 0.0f, 0.75f),
			new DVBColor(0.0f, 0.0f, 0.0f, 0.75f),
			new DVBColor(0.0f, 0.0f, 0.0f, 0.60f),
			new DVBColor(0.0f, 0.0f, 0.0f, 0.60f),
			new DVBColor(0.0f, 0.0f, 0.0f, 0.40f),
			new DVBColor(0.0f, 0.0f, 0.0f, 0.40f),
			null, null, null
	};

	int index = 0;
	Color cTitle = new Color(252, 202, 5);

	public Image[] getLastImage() {
		return lastImg;
	}
	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return postersRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		CarouselViewPostersUI ui = (CarouselViewPostersUI) c;
		catalogues = ui.getCatalogueServiceList();
		if (catalogues != null && catalogues.length > 0) {
			dataLen = catalogues.length;
		}
		i01ArrowL = dataCenter.getImage("02_ars_l.png");
		i01ArrowR = dataCenter.getImage("02_ars_r.png");
//		i01PosterSha = dataCenter.getImage("01_possh_89.png");
		i02_icon_con = dataCenter.getImage("02_icon_con.png");
		blockedPortrait = dataCenter.getImage("01_poster_block.jpg");
		blockedLandscape = dataCenter.getImage("01_poster_block_h.jpg");
		// R5
		i02_icon_fav = dataCenter.getImage("02_icon_fav.png");
		i_icon_uhd = dataCenter.getImage("icon_uhd.png");
		
		super.prepare(c);
	}
	
	/**
	 * Paints the UIComponent.
	 * 
	 * @param g
	 *            The graphics context to use for painting.
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 */
	protected void paint(Graphics g, UIComponent c) {
		if (catalogues == null) {
			return;
		}
		g.translate(-postersRec.x, -postersRec.y);
		index = c.getFocus();
		CarouselViewPostersUI ui = (CarouselViewPostersUI) c;
		int startIdx = ui.getStartDisplayIndex();
		int[] nums = getPosterIndex(startIdx);
		g.setColor(Color.white);
		bounds = new Rectangle[pBounds.length];
		MoreDetail title = null;
		boolean adultBlocked = false;
		for (int i = 0; i < CarouselViewPostersUI.MAX_POSTER; i++) {
			bounds[i] = pBounds[i];
			lastImg[i] = null;
			if (nums[i] == -1) {
				continue;
			}
			Image poster = null;
			title = (MoreDetail) catalogues[nums[i]];
			adultBlocked = MenuController.getInstance().isAdultBlocked(title);
			if (adultBlocked) {
				poster = title.isPortrait() ? blockedPortrait
						: blockedLandscape;
				bounds[i] = title.isPortrait() ? pBounds[i] : lBounds[i];
			} else {
				if (title != null) {
					bounds[i] = title.isPortrait() ? pBounds[i] : lBounds[i];
					if (title instanceof Extra) {
						MoreDetail titleInExtra = (MoreDetail) CatalogueDatabase.getInstance().getCached(((Extra) title).extraDetail.mainContentItem);
						poster = titleInExtra.getImage(bounds[i].width, bounds[i].height);
					} else {
						poster = title.getImage(bounds[i].width, bounds[i].height);
					}
				} else {
					poster = ImageRetriever.getDefault(pBounds[i].width, pBounds[i].height);
				}
			}
			g.drawImage(poster, bounds[i].x, bounds[i].y, bounds[i].width, bounds[i].height, c);
			lastImg[i] = poster;
			
//			g.drawImage(i01PosterSha, bounds[i].x, bounds[i].y + bounds[i].height, bounds[i].width, bounds[i].height / 10, c);
			if (dim[i] != null) {
				g.setColor(dim[i]);
				g.fillRect(bounds[i].x, bounds[i].y, bounds[i].width, bounds[i].height);
			}
		}
		String tot = ui.getTotal();
		String cur = ui.getCur();
		
		if ("/00".equals(tot)) {
			g.translate(postersRec.x, postersRec.y);
			return;
		}
		g.setColor(cTitle);
		int idx = CarouselViewPostersUI.MAX_POSTER-1;
		g.drawRect(bounds[idx].x - 1, bounds[idx].y - 1, bounds[idx].width + 1, bounds[idx].height + 1);
		g.drawRect(bounds[idx].x - 2, bounds[idx].y - 2, bounds[idx].width + 3, bounds[idx].height + 3);
		g.drawRect(bounds[idx].x - 3, bounds[idx].y - 3, bounds[idx].width + 5, bounds[idx].height + 5);
		g.setFont(FontResource.BLENDER.getFont(16));
		
		int nW = g.getFontMetrics().stringWidth(cur + tot) + 10;
		g.setColor(cTitle);
		g.fillRect(bounds[idx].x + bounds[idx].width - nW, bounds[idx].y + bounds[idx].height - 17, nW, 17);
		g.setColor(new Color(74, 72, 64));
		int pX = GraphicUtil.drawStringRight(g, tot, bounds[idx].x + bounds[idx].width - 3, bounds[idx].y + bounds[idx].height - 3);
		g.setColor(Color.black);
		GraphicUtil.drawStringRight(g, cur, pX, bounds[idx].y + bounds[idx].height - 3);
		Image flag = null;
		if (title != null) {
			switch (title.getFlag()) {
			case 1: // new
				flag = dataCenter.getImageByLanguage("icon_d_new.png");
				break;
			case 2: // last chance
				flag = dataCenter.getImageByLanguage("icon_d_last.png");
				break;
			case 3: // extras
				flag = dataCenter.getImageByLanguage("icon_d_extras.png");
				break;
			}
		}
		if (flag != null) {
			g.drawImage(flag, bounds[idx].x + bounds[idx].width - flag.getWidth(ui), bounds[idx].y, ui);
		}
		
		// R5 - UHD icon
		Offer hdOffer = title.getOfferHD(null);
		if (hdOffer != null) {
			boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
			if (uhd) {
				g.drawImage(i_icon_uhd, bounds[idx].x, bounds[idx].y + bounds[idx].height - i_icon_uhd.getHeight(c), c);
			}
		}
		
		drawnLeft = nums[6] != -1;
		if (drawnLeft) {
			//g.drawImage(i01ArrowL, 354, 234, c);
			g.drawImage(i01ArrowL, bounds[idx].x - 21, 234, c);
		}
		drawnRight = nums[7] != -1;
		if (drawnRight) {
			//g.drawImage(i01ArrowR, 589, 234, c);
			g.drawImage(i01ArrowR, bounds[idx].x + bounds[idx].width + 4, 234, c);
		}
		
		// draw title
		g.setFont(FontResource.BLENDER.getFont(23));
		String titleStr = TextUtil.shorten(title.getTitle(), g.getFontMetrics(), 750);
		
		// VDTRMASTER-5307
		if (Log.DEBUG_ON) {
			if (title instanceof Season) {
				Log.printDebug("PosterArrayRenderer, fromCategory=" + ui.fromCategory() + 
						", season.getLongTitle()=" + ((Season) title).getLongTitle());
			}
		}
		if (title instanceof Season && ui.fromCategory() && !((Season) title).getLongTitle().equals("N/A")) {
			titleStr = TextUtil.shorten(((Season) title).getLongTitle(), g.getFontMetrics(), 750);
		}
		
		if (adultBlocked) {
			titleStr = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
		}
//		g.setColor(Color.darkGray);
//		GraphicUtil.drawStringCenter(g, titleStr, 481, bounds[idx].y + bounds[idx].height + 27);
		int yTitle = bounds[idx].y + bounds[idx].height + 26;
		if (CategoryUI.getInstance().getCurrentCategory() == null 
				|| "Mixed".equals(CategoryUI.getInstance().getCurrentCategory().postersFormat)) {
			yTitle = 430;
		}
		g.setColor(cTitle);
		int icon = GraphicUtil.drawStringCenter(g, titleStr, 480, yTitle);
		
		
		boolean ageBlocked = adultBlocked
				|| MenuController.getInstance().isBlocked(title);
		if (ageBlocked && !MenuController.getInstance().needToDisplayFavorite()) {
			g.drawImage(i02_icon_con, icon - i02_icon_con.getWidth(ui) - 4,
					yTitle - 20, ui);
		} else if (BookmarkManager.getInstance().isInWishlist(title) != null) {
			g.drawImage(i02_icon_fav, icon - i02_icon_fav.getWidth(ui) - 4,
					yTitle - 20, ui);
		}

//			g.drawImage(i_01_poster_sha_l, 0, 6, c);
//			g.drawImage(i_01_poster_sha_r, 856, 6, c);
			
		g.translate(postersRec.x, postersRec.y);
	}

	public int[] getPosterIndex(int start) {
		int[] reIdx = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1 };
		int k = 0;
		int y = 0;
		int[] position = null;
		int loopCnt = dataLen;
		if (loopCnt > CarouselViewPostersUI.MAX_POSTER) {
			loopCnt = CarouselViewPostersUI.MAX_POSTER;
		}
		position = new int[] { 8, 6, 7, 4, 5, 2, 3, 0, 1 };

		for (int i = 0; i < loopCnt; i++) {
			if (i == 0) {
				y = index;
			} else {
				if (i % 2 == 0) {
					k = i / 2;
				} else {
					k = -((i + 1) / 2);
				}
				y = (index + k + dataLen) % dataLen;
			}
			reIdx[position[i]] = y;
		}
		return reIdx;
	}
}

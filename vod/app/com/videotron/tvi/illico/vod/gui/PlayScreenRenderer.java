package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.VideoControllerImpl;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;

public class PlayScreenRenderer extends BaseRenderer {

    private Image i04FlipStFocB01;// = dataCenter.getImage("04_flip_st_foc_b01.png");
    private Image i04prevSkip;// = dataCenter.getImage("04_flip_st_01_foc_b.png"); //  previous skip
    private Image i04Stop;// = dataCenter.getImage("04_flip_st_03_foc_b.png"); // stop
    private Image i04Pause;// = dataCenter.getImage("04_flip_st_04_foc_b.png"); // pause
    private Image i04Play;// = dataCenter.getImage("04_flip_st_05_foc_b.png"); // play
    private Image i04fowardSkip;// = dataCenter.getImage("04_flip_st_07_foc_b.png"); // forward skip
    private Image i04Slow;
    
    private Image[] ffSpeed;// = new Image []{ 
//    		dataCenter.getImage("04_feed_ff_2.png"), 
//    		dataCenter.getImage("04_feed_ff_4.png"), 
//    		dataCenter.getImage("04_feed_ff_8.png"), 
//    		null, 
//    		dataCenter.getImage("04_feed_ff_16.png") };
    private Image[] rewSpeed;// = new Image []{ 
//    		dataCenter.getImage("04_feed_rew_2.png"), 
//    		dataCenter.getImage("04_feed_rew_4.png"), 
//    		dataCenter.getImage("04_feed_rew_8.png"), 
//    		null, 
//    		dataCenter.getImage("04_feed_rew_16.png") };
    
    private Image cannot;// = dataCenter.getImage("no_action.png"); 
    
    private Image prev_s;// = dataCenter.getImage("04_feed_prev_s.png"); 
    private Image next_s;// = dataCenter.getImage("04_feed_next_s.png"); 
    private Image pause_s;// = dataCenter.getImage("04_feed_pause_s.png"); 
    private Image play_s;// = dataCenter.getImage("04_feed_play_s.png"); 
    private Image slow_s;
    
    private Image[] ffSpeed_s;// = new Image []{ 
//    		dataCenter.getImage("04_feed_ff_2_s.png"), 
//    		dataCenter.getImage("04_feed_ff_4_s.png"), 
//    		dataCenter.getImage("04_feed_ff_8_s.png"), 
//    		null, 
//    		dataCenter.getImage("04_feed_ff_16_s.png") };
    private Image[] rewSpeed_s;// = new Image []{ 
//    		dataCenter.getImage("04_feed_rew_2_s.png"), 
//    		dataCenter.getImage("04_feed_rew_4_s.png"), 
//    		dataCenter.getImage("04_feed_rew_8_s.png"), 
//    		null, 
//    		dataCenter.getImage("04_feed_rew_16_s.png") };
    /**
     * Returns the preferred bounds of this renderer. If preferred bounds is non-null, the UIComponent's bounds will be
     * set by this method.
     * 
     * @return The Preferred Bounds for the this Renderer.
     * @see UIComponent#setRenderer
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bgRec;
    }

    /**
     * This will be called before the related UIComponent is shown.
     * 
     * @param c
     *            UIComponent to be painted by this renderer.
     * @see UIComponent#prepare
     */
    public void prepare(UIComponent c) {

        //FrameworkMain.getInstance().getImagePool().waitForAll();
        
        i04FlipStFocB01 = dataCenter.getImage("04_flip_st_foc_b01.png");
        i04prevSkip = dataCenter.getImage("04_flip_st_01_foc_b.png"); //  previous skip
        i04Stop = dataCenter.getImage("04_flip_st_03_foc_b.png"); // stop
        i04Pause = dataCenter.getImage("04_flip_st_04_foc_b.png"); // pause
        i04Play = dataCenter.getImage("04_flip_st_05_foc_b.png"); // play
        i04fowardSkip = dataCenter.getImage("04_flip_st_07_foc_b.png"); // forward skip
        i04Slow = dataCenter.getImage("04_flip_st_slow_b.png"); // slow
        
        // REW_FF
//        ffSpeed = new Image []{ 
//        		dataCenter.getImage("04_feed_ff_4.png"), 
//        		dataCenter.getImage("04_feed_ff_16.png"), 
//        		dataCenter.getImage("04_feed_ff_64.png"), 
//        		dataCenter.getImage("04_feed_ff_128.png"), 
//        		dataCenter.getImage("04_feed_ff_256.png") };
//        rewSpeed = new Image []{ 
//        		dataCenter.getImage("04_feed_rew_4.png"), 
//        		dataCenter.getImage("04_feed_rew_16.png"), 
//        		dataCenter.getImage("04_feed_rew_64.png"), 
//        		dataCenter.getImage("04_feed_rew_128.png"), 
//        		dataCenter.getImage("04_feed_rew_256.png") };
        
        ffSpeed = new Image []{ 
        		dataCenter.getImage("04_feed_ff_2.png"), 
        		dataCenter.getImage("04_feed_ff_4.png"), 
        		dataCenter.getImage("04_feed_ff_8.png"), 
        		dataCenter.getImage("04_feed_ff_16.png"), 
        		dataCenter.getImage("04_feed_ff_32.png") };
        rewSpeed = new Image []{ 
        		dataCenter.getImage("04_feed_rew_2.png"), 
        		dataCenter.getImage("04_feed_rew_4.png"), 
        		dataCenter.getImage("04_feed_rew_8.png"), 
        		dataCenter.getImage("04_feed_rew_16.png"), 
        		dataCenter.getImage("04_feed_rew_32.png") };
        
        cannot = dataCenter.getImage("no_action.png"); 
        
        prev_s = dataCenter.getImage("04_feed_prev_s.png"); 
        next_s = dataCenter.getImage("04_feed_next_s.png"); 
        pause_s = dataCenter.getImage("04_feed_pause_s.png"); 
        play_s = dataCenter.getImage("04_feed_play_s.png"); 
        slow_s = dataCenter.getImage("04_flip_st_slow_s.png");
        
        ffSpeed_s = new Image []{ 
        		dataCenter.getImage("04_feed_ff_2_s.png"), 
        		dataCenter.getImage("04_feed_ff_4_s.png"), 
        		dataCenter.getImage("04_feed_ff_8_s.png"), 
        		null, 
        		dataCenter.getImage("04_feed_ff_16_s.png") };
        rewSpeed_s = new Image []{ 
        		dataCenter.getImage("04_feed_rew_2_s.png"), 
        		dataCenter.getImage("04_feed_rew_4_s.png"), 
        		dataCenter.getImage("04_feed_rew_8_s.png"), 
        		null, 
        		dataCenter.getImage("04_feed_rew_16_s.png") };
        
        super.prepare(c);
    }

    /**
     * Paints the UIComponent.
     * 
     * @param g
     *            The graphics context to use for painting.
     * @param c
     *            UIComponent to be painted by this renderer.
     */
    protected void paint(Graphics g, UIComponent c) {
        PlayScreenUI ui = (PlayScreenUI)c;

        if (Log.EXTRA_ON) {
	        g.setFont(FontResource.BLENDER.getFont(20));
	        String debugCA = VideoControllerImpl.getInstance().getCAInfo();
	        String debugLoc = VideoControllerImpl.getInstance().getLocatorInfo();
	        String debugScale = VideoControllerImpl.getInstance().getScaleInfo();
	        String debugMIB = CommunicationManager.getInstance().debugLock;
	        g.setColor(Color.black);
	        g.drawString("PlayScreen: " + PlayScreenUI.debug, 42, 52);
	        g.drawString(debugCA, 42, 72);
	        g.drawString(debugLoc, 42, 92);
	        g.drawString(debugScale, 42, 112);
	        g.drawString(debugMIB, 42, 132);
	        g.drawString("Playout: " + ui.getPlayout(), 42, 152);
	        g.drawString("Offer: " + ui.getOffer(), 42, 172);
	        g.drawString(PlayScreenUI.debugHDMI, 42, 192);
	        g.setColor(Color.green);
	        g.drawString("PlayScreen: " + PlayScreenUI.debug, 40, 50);
	        g.drawString(debugCA, 40, 70);
	        g.drawString(debugLoc, 40, 90);
	        g.drawString(debugScale, 40, 110);
	        g.drawString(debugMIB, 40, 130);
	        g.drawString("Playout: " + ui.getPlayout(), 40, 150);
	        g.drawString("Offer: " + ui.getOffer(), 40, 170);
	        g.drawString(PlayScreenUI.debugHDMI, 40, 190);
	        
//	        String feed = "NORMAL";
//	        switch (PlayScreenUI.feedMode) {
//	        case PlayScreenUI.MODE_LARGE:
//	        	feed = "Large icon only: " + PlayScreenUI.timeLarge + " sec(s)";
//	        	break;
//	        case PlayScreenUI.MODE_SMALL:
//	        	feed = "Small icon only: " + PlayScreenUI.timeSmall + " sec(s)";
//	        	break;
//	        case PlayScreenUI.MODE_MIXED:
//	        	feed = "Large and small: " + PlayScreenUI.timeLarge + " sec(s) and " + PlayScreenUI.timeSmall + " sec(s)";
//	        	break;
//	        }
//	        g.setColor(Color.black);
//	        GraphicUtil.drawStringRight(g, feed, 922, 52);
//	        g.setColor(Color.green);
//	        GraphicUtil.drawStringRight(g, feed, 920, 50);
        }
        
        try {
	        if (PlayScreenUI.curIcon == PlayScreenUI.NO_ICON) {
	        	return;
	        }
	        if (PlayScreenUI.curIcon == PlayScreenUI.LARGE_ICON) {
	        	if (PlayScreenUI.feedMode == PlayScreenUI.MODE_NORMAL && ui.isFlipBarOn() == false) {
	        		return;
	        	}
		        int y = 232;
	//	        if (ui.isFlipBarOn()) {
		            int flipMode = ui.getFlipStatus();
		            Image flipImage = null;
		            switch (flipMode) {
		            case PlayScreenUI.FLIP_PAUSE:
		                flipImage = i04Pause;
		                break;
		            case PlayScreenUI.FLIP_PLAY:
		                flipImage = i04Play;
		                break;
		            case PlayScreenUI.FLIP_SLOW:
		                flipImage = i04Slow;
		                break;
		            case PlayScreenUI.FLIP_REW:
		            	flipImage = rewSpeed[getSpeedIndex(ui)];
		            	y = 223;
		                break;
		            case PlayScreenUI.FLIP_FF:
		                flipImage = ffSpeed[getSpeedIndex(ui)];
		                y = 223;
		                break;
		            case PlayScreenUI.FLIP_SKIP_BACK:
		                flipImage = i04prevSkip;
		                break;
		            case PlayScreenUI.FLIP_SKIP_FORWARD:
		                flipImage = i04fowardSkip;
		                break;
		            default:
		            	break;
		            }
		            if (flipImage != null) {
			            g.drawImage(i04FlipStFocB01, 429, 197, c);
			            g.drawImage(flipImage, 451, y, c);
		            }
	//	        }
	        } else if (PlayScreenUI.curIcon == PlayScreenUI.SMALL_ICON) {
	        	int flipMode = ui.getFlipStatus();
	            Image flipImage = null;
	            switch (flipMode) {
	            case PlayScreenUI.FLIP_PAUSE:
	                flipImage = pause_s;
	                break;
	            case PlayScreenUI.FLIP_PLAY:
	                flipImage = play_s;
	                break;
	            case PlayScreenUI.FLIP_SLOW:
	                flipImage = slow_s;
	                break;
	            case PlayScreenUI.FLIP_REW:
	            	int speed = Math.abs(ui.getCurrentMode());
	            	flipImage = rewSpeed_s[speed/4];
	                break;
	            case PlayScreenUI.FLIP_FF:
	                speed = Math.abs(ui.getCurrentMode());
	                flipImage = ffSpeed_s[speed/4];
	                break;
	            case PlayScreenUI.FLIP_SKIP_BACK:
	                flipImage = prev_s;
	                break;
	            case PlayScreenUI.FLIP_SKIP_FORWARD:
	                flipImage = next_s;
	                break;
	            default:
	            	break;
	            }
	            if (flipImage != null) {
		            g.drawImage(flipImage, 57, 45, c);
	            }
	        }
        } catch (Exception e) {
        	
        } finally {
	        if (ui.cannotIcon()) {
	        	g.drawImage(cannot, 60, 48, c);
	        }
        }
    }
    
    private int getSpeedIndex(PlayScreenUI ui) {
    	int speed = Math.abs(ui.getCurrentMode());
    	
    	int index = 0;
    	
    	switch (speed) {
    	// REW_FF
//    	case 4:
//    		index = 0;
//    		break;
//    	case 16:
//    		index = 1;
//    		break;
//    	case 64:
//    		index = 2;
//    		break;
//    	case 128:
//    		index = 3;
//    		break;
    	case 2:
    		index = 0;
    		break;
    	case 4:
    		index = 1;
    		break;
    	case 8:
    		index = 2;
    		break;
    	case 16:
    		index = 3;
    		break;
    	}
    	
    	return index;
    }
}

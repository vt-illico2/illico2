package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.AboutChannelUI;
import com.videotron.tvi.illico.vod.ui.BundleDetailUI;
import com.videotron.tvi.illico.vod.ui.CarouselViewUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.KaraokeListViewUI;
import com.videotron.tvi.illico.vod.ui.ListOfTitlesUI;
import com.videotron.tvi.illico.vod.ui.ListViewUI;
import com.videotron.tvi.illico.vod.ui.SeriesOptionUI;
import com.videotron.tvi.illico.vod.ui.TitleDetailUI;
import com.videotron.tvi.illico.vod.ui.WallofPostersFilmInfoUI;

public class BaseRenderer extends Renderer {

	public DataCenter dataCenter = DataCenter.getInstance();
	private Image background;
	public static Image[] brandBG = new Image[2];
	private static String[] bgUrl = new String[2];
	private static Thread imgLoading[] = new Thread[2];
	private static int typeBG; 

	protected Rectangle bgRec = new Rectangle(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

	private static Color cDimHistory = new Color(124, 124, 124);
	private static Color cHistory = new Color(239, 239, 239);
	private static Color cButton = new Color(241, 241, 241);
	private static Color cPerson = new Color(255, 255, 255);
	private static Color cPersonLbl = new Color(182, 182, 182);
	private static Font fHistory = FontResource.BLENDER.getFont(18);
	private static Font fOffer = FontResource.BLENDER.getFont(18);
	private static Font fButton = FontResource.BLENDER.getFont(17);
	private static Font fClock = FontResource.BLENDER.getFont(16);
	private static Font fAddinfo = FontResource.BLENDER.getFont(18);
	public static FontMetrics fmOffer = FontResource.getFontMetrics(fOffer);
	public static FontMetrics fmHistory = FontResource.getFontMetrics(fHistory);
	public static FontMetrics fmButton = FontResource.getFontMetrics(fButton);
	public static FontMetrics fmClock = FontResource.getFontMetrics(fClock);
	public static FontMetrics fmInfo = FontResource.getFontMetrics(fAddinfo);

//	protected Image clock;// = dataCenter.getImage("clock.png");
	protected Image hisDim;// = dataCenter.getImage("his_dim.png");
	protected Image hisOver;// = dataCenter.getImage("his_over.png");
	static public Image i00Vtlogo;
	protected Image iconG;// = dataCenter.getImage("icon_g.png");
	protected Image icon8;// = dataCenter.getImage("icon_8.png");
	protected Image icon13;// = dataCenter.getImage("icon_13.png");
	protected Image icon16;// = dataCenter.getImage("icon_16.png");
	protected Image icon18;// = dataCenter.getImage("icon_18.png");
	protected Image iconSd;// = dataCenter.getImage("icon_sd.png");
	protected Image iconHd;// = dataCenter.getImage("icon_hd.png");
	protected Image iconFhd;
	protected Image icon3d;// = dataCenter.getImage("icon_3d.png");
	protected Image slashPrice;// = dataCenter.getImage("slash_price.png");
	protected Image slashClar;// = dataCenter.getImage("slash_clar.png");
	protected Image iconFr;// = dataCenter.getImage("icon_fr.png");
	protected Image iconEn;// = dataCenter.getImage("icon_en.png");
	protected Image iconCc;// = dataCenter.getImage("icon_cc.png");
	protected Image i_01_icon_sd_s;// = dataCenter.getImage("01_icon_sd_s.png");
	protected Image i_01_icon_hd_s;// = dataCenter.getImage("01_icon_hd_s.png");
	protected Image i_01_icon_fhd_s;
    // R5
    private Image btnBack, iconUHD;
    public static final String SLASH_FIVE = "/5";
	
	// 4K
	protected Image iconUhd;
	protected Image i_01_icon_uhd_s;
	protected DVBColor c0_51 = new DVBColor(0, 0, 0, 51);
	protected DVBColor c0_90 = new DVBColor(0, 0, 0, 90);
	protected Color c50 = new Color(50, 50, 50);
	protected Color c70 = new Color(70, 70, 70);

	public void setBackGround(Image bgImg) {
		background = bgImg;
	}

	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 *
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return bgRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 *
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		if (Log.DEBUG_ON) {
			Log.printDebug("BaseRenderer prepare, " + this);
		}

		hisDim = dataCenter.getImage("his_dim.png");
		hisOver = dataCenter.getImage("his_over.png");
		iconG = dataCenter.getImage("icon_g.png");
		icon8 = dataCenter.getImage("icon_8.png");
		icon13 = dataCenter.getImage("icon_13.png");
		icon16 = dataCenter.getImage("icon_16.png");
		icon18 = dataCenter.getImage("icon_18.png");
		iconSd = dataCenter.getImage("icon_sd.png");
		iconHd = dataCenter.getImage("icon_hd.png");
		iconFhd = dataCenter.getImage("icon_fhd_long.png");
		icon3d = dataCenter.getImage("icon_3d.png");
		slashPrice = dataCenter.getImage("slash_price.png");
		slashClar = dataCenter.getImage("slash_clar.png");
		iconFr = dataCenter.getImage("icon_fr.png");
		iconEn = dataCenter.getImage("icon_en.png");
		iconCc = dataCenter.getImage("icon_cc.png");
		i_01_icon_sd_s = dataCenter.getImage("01_icon_sd_s.png");
		i_01_icon_hd_s = dataCenter.getImage("01_icon_hd_s.png");
		i_01_icon_fhd_s = dataCenter.getImage("01_icon_fhd_s.png");
		
		// R5
        Hashtable footerTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        btnBack = (Image) footerTable.get(PreferenceService.BTN_BACK);
        iconUHD = dataCenter.getImage("icon_uhd.png");        
		// 4K
		iconUhd = dataCenter.getImage("icon_uhd_long.png");
		i_01_icon_uhd_s = dataCenter.getImage("01_icon_uhd_s.png");

		FrameworkMain.getInstance().getImagePool().waitForAll();
	}

	/**
	 * Paints the UIComponent.
	 *
	 * @param g
	 *            The graphics context to use for painting.
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 */
	protected void paint(Graphics g, UIComponent c) {
		g.drawImage(background, 0, 0, c);
		if (i00Vtlogo == null) {
			i00Vtlogo = (Image) SharedMemory.getInstance().get(
				MainMenuService.BRAND_IMAGE_KEY);
		}
		if (i00Vtlogo != null) {
			g.drawImage(i00Vtlogo, 53, 21, c);
		}
		
		// 4K
		if (c instanceof ListOfTitlesUI) {
			g.setColor(c0_51);
			g.fillRect(0, 0, 960, 118);
			g.setColor(c0_90);
			g.fillRect(0, 118, 960, 197);
		} else if (c instanceof BundleDetailUI) {
			g.setColor(c0_51);
			g.fillRect(0, 0, 960, 108);
			g.setColor(c0_90);
			g.fillRect(0, 108, 960, 198);
		}
		
		if (c instanceof ListOfTitlesUI || c instanceof WallofPostersFilmInfoUI
				|| c instanceof TitleDetailUI || c instanceof SeriesOptionUI
				|| c instanceof BundleDetailUI) {
			g.setColor(c50);
			g.fillRect(0, 483, 960, 1);
		} else if (c instanceof CategoryUI) {
			g.setColor(c70);
			g.fillRect(373, 477, 587, 1);
		}
		
		// VDTRMASTER-5584
		if (typeBG >= 0 && brandBG[typeBG] != null && c instanceof TitleDetailUI == false 
				&& c instanceof BundleDetailUI == false) {
			if ((c instanceof ListViewUI || c instanceof WallofPostersFilmInfoUI) && typeBG == 1){
				g.drawImage(brandBG[typeBG], 0, 111, brandBG[typeBG].getWidth(c), brandBG[typeBG].getHeight(c) + 5,c);
			} else {
				g.drawImage(brandBG[typeBG], 0, 111, c);
			}
		}
		
		// paintButtons(g, c);
		paintHistory(g, c);
		paintClock(g, c);
		
		// R5
		if ((c instanceof CategoryUI && !((CategoryUI)c).isTop()) || c instanceof CategoryUI == false
				|| !c.equals(CategoryUI.getInstance())) { // VDTRMASTER-5645
        	g.setFont(FontResource.BLENDER.getFont(16));
	        g.setColor(new Color(236, 211, 143));
	        g.drawImage(btnBack, 52, 488, c);
	        g.drawString(dataCenter.getString(Resources.TEXT_BACK), 87, 504);
		}
		if (Log.EXTRA_ON) {
			g.setFont(FontResource.DINMED.getFont(14));

			CategoryUI.getInstance();
			String s = "Branding depth = " + CategoryUI.brandingDepth +  
			(CategoryUI.getBrandingElement() == null ? "" : ", Branding = " + CategoryUI.getBrandingElement().toString(true));
			g.setColor(Color.black);
			g.drawString(s, 21, 36);
			g.setColor(Color.green);
			g.drawString(s, 20, 35);
			
			s = "VOD v" + Resources.APP_VERSION + 
			", VCDS> " + CatalogueDatabase.getInstance().getVersion() + 
			", MAC> " + BookmarkManager.getInstance().debug + 
			", PKG> " + CommunicationManager.getInstance().getAuthorizedPackage().substring(12) +
			" XML> " + CatalogueDatabase.XML_VERSION + 
			", URL> " + DataCenter.getInstance().getString("VCDS_URL").substring(7);
			g.setColor(Color.black);
			g.drawString(s, 21, 36-18);
			g.setColor(Color.green);
			g.drawString(s, 20, 35-18);
			
			BaseElement last = MenuController.getInstance().getLastObject();
			if (last != null) {
				s = "Last = " + last.toString(true);
				g.setColor(Color.black);
				g.drawString(s, 21, 36+18);
				g.setColor(Color.green);
				g.drawString(s, 20, 35+18);
			}
			
			String screenName = MenuController.getInstance().getCurrentScene().getClass().getName();
			g.drawString(screenName.substring(screenName.lastIndexOf('.') + 1), 59, 498);
		}
	}
	
	public static void flushBrandingBG() {
		Log.printDebug("flushBrandingBG");
		typeBG = -1;
		
		if (brandBG[0] != null) {
			brandBG[0].flush();
		}
		brandBG[0] = null;
		if (brandBG[1] != null) {
			brandBG[1].flush();
		}
		brandBG[1] = null;
		bgUrl[0] = null;
		bgUrl[1] = null;
	}
	
	public static void setBrandingBg(final int type, final String url) {
		setBrandingBg(type, url, true);
		
	}
	public static boolean hasBrandingBg() {
		return brandBG[0] != null || brandBG[1] != null;
	}
	synchronized public static void setBrandingBg(final int type, final String url, final boolean update) {
		if (url == null) {
			Log.printDebug("setBrandingBg, type = " + type);
			typeBG = type;
			return;
		}
		if (url.equals(bgUrl[type]) && brandBG[type] != null) {
			Log.printDebug("setBrandingBg, type = " + type + ", same url = " + url);
			typeBG = type;
    		return;
    	}
    	if (imgLoading[type] != null) {
    		imgLoading[type].interrupt();
    	}
    	
    	if (brandBG[type] != null) {
			Log.printDebug("setBrandingBg, flush 1");
			brandBG[type].flush();
			brandBG[type] = null;
		}
    	
    	typeBG = type;
    	imgLoading[type] = new Thread("setBrandingBg, type = " + type + ", url = " + url + ", update = " + update) {
    		public void run() {
    			Log.printDebug("setBrandingBg, type = " + type + ", url = " + url + ", update = " + update);
//    			if (brandBG[type] != null) {
//    				Log.printDebug("setBrandingBg, flush 1");
//					brandBG[type].flush();
//					brandBG[type] = null;
//				}
				bgUrl[type] = url;
				long t = System.currentTimeMillis();
    			Image tmp = null;
				try {
//					tmp = Toolkit.getDefaultToolkit().createImage(new URL(url));
					byte[] src = URLRequestor.getBytes(url, null);
					tmp = Toolkit.getDefaultToolkit().createImage(src);
				} catch (MalformedURLException e1) {
					return;
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
    			MediaTracker tracker = new MediaTracker(CategoryUI.getInstance());
    			tracker.addImage(tmp, 99);
    			try {
					tracker.waitForID(99);
					if (brandBG[type] != null && brandBG[type] != tmp) {
						Log.printDebug("setBrandingBg, flush 2");
						brandBG[type].flush();
					}
					if (url.equals(bgUrl[type])) {
						brandBG[type] = tmp;
						if (update && typeBG == type) {
//							typeBG = type;
			    			MenuController.getInstance().getCurrentScene().repaint();
						}
					}
	    			t = System.currentTimeMillis() - t;
	    			Log.printDebug("setBrandingBg, elapsed = " + t);
				} catch (InterruptedException e) {
//					if (brandBG[type] != null) {
//						brandBG[type].flush();
//						brandBG[type] = null;
//					}
	    			return;
				} finally {
					tracker.removeImage(tmp);
					imgLoading[type] = null;
				}
    		}
    	};
    	
    	imgLoading[type].start();
	}

	private void paintClock(Graphics g, UIComponent c) {
		g.setFont(fClock);
		g.setColor(Color.white);
		String str = MenuController.getInstance().getTimeAndDate();
		int x = GraphicUtil.drawStringRight(g, str, 910, 57);
		Image clock = dataCenter.getImage("clock.png");
		g.drawImage(clock, x - 7 - clock.getWidth(c), 44, c);
	}

	private void paintHistory(Graphics g, UIComponent c) {
		String[] history = MenuController.getInstance().getHistory();
		int historyLen = history.length;
		int gap = 10;
		int imageWidth = 6;
		int startX = 53;
		if (i00Vtlogo != null) {
			startX += i00Vtlogo.getWidth(c) + 14;
		}
		g.setFont(fHistory);
		CategoryUI.getInstance();
		if (MenuController.getInstance().isGoToAsset() // 4761
				|| MenuController.getInstance().isUnderWishList() // VDTRMASTER-5628
				|| MenuController.getInstance().isInWishlist()) { // VDTRMASTER-5628
			g.setColor(cHistory);
			g.drawImage(hisOver, startX, 45, c);
			startX = startX + imageWidth + gap;
			g.drawString(history[0], startX, 57);
		} else if ((c instanceof CategoryUI && CategoryUI.isBranded() && CategoryUI.getChannel() != null) 
				|| c instanceof AboutChannelUI 
				|| CategoryUI.getChannel() != null
				|| CategoryUI.getInstance().getCODCategory() != null) {
			g.setColor(cHistory);
			g.drawImage(hisOver, startX, 45, c);
			startX = startX + imageWidth + gap;
			
			if (CategoryUI.getInstance().getCODCategory() != null) {
				String title = CategoryUI.getInstance().getCODCategory().getTitle();
				g.drawString(title, startX, 57);
			} else if (CategoryUI.getInstance().getCurrentCategory() != null) {
				String title = CategoryUI.getInstance().getCurrentCategory().getTitle();
				if (title != null && !title.equals("N/A")) {
					g.drawString(title, startX, 57);
				} else {
					title = dataCenter.getString(Resources.TEXT_LBL_COD);
					g.drawString(title, startX, 57);
				}
			} else { //VDTRMASTER-5638
				String title = dataCenter.getString(Resources.TEXT_LBL_COD);
				g.drawString(title, startX, 57);
			}
			//dataCenter.getString(Resources.TEXT_LBL_CHANNEL)
			//g.drawString(title, startX, 57);
		} else if (historyLen <= 2) {
			g.setColor(cHistory);
			g.drawImage(hisOver, startX, 45, c);
			startX = startX + imageWidth + gap;
			g.drawString(history[0], startX, 57);
		} else {
			g.setColor(cDimHistory);
			g.drawImage(hisDim, startX, 45, c);
			startX = startX + imageWidth + gap;
			g.drawString(history[0], startX, 57);
			startX = startX + fmHistory.stringWidth(history[0]) + gap;
			for (int i = 0; i < Math.min(2, historyLen - 2); i++) {
				g.drawImage(hisOver, startX, 45, c);
				startX += imageWidth;
			}
			startX = startX + gap;
			g.setColor(cHistory);
			int width = 496 - startX;
			g.drawString(
					TextUtil.shorten(history[historyLen - 2], fmHistory, width),
					startX, 57);
		}
	}

	public String[] getTitleWithYear(BaseElement catalogues, FontMetrics fm,
			int width) {
		String title = ((MoreDetail) catalogues).getTitle();
		String reTitle[] = TextUtil.split(title, fm, width);
		// String year = " (" + catalogues.getYear() + ")";
		String year = ((MoreDetail) catalogues).getProductionYear();
		if (year == null) {
			year = "";
		} else if ("".equals(year) == false) {
			year = " (" + year + ")";
		}
		int yearWidth = fm.stringWidth(year);

		if (reTitle.length > 1) {
			if (yearWidth > 0) {
				reTitle[1] = TextUtil
						.shorten(reTitle[1], fm, width - yearWidth) + year;
			} else {
				reTitle[1] = TextUtil
						.shorten(reTitle[1], fm, width - yearWidth);
			}

		} else {
			int titleLen = fm.stringWidth(title);
			if (titleLen + yearWidth > width) {
				reTitle = new String[2];
				reTitle[0] = title;
				if (yearWidth > 0) {
					reTitle[1] = year;
				}
			} else {
				if (yearWidth > 0) {
					reTitle[0] = title + year;
				}
			}
		}
		return reTitle;
	}

	public String getTitleWithYearSingleLine(MoreDetail movie, FontMetrics fm,
			int width) {
		if (movie == null) {
			return "";
		}
		String title = movie.getTitle();
		String year = movie.getProductionYear();
		String reTitle = null;
		if (year == null || year.length() == 0) {
			reTitle = TextUtil.shorten(title, fm, width);
		} else {
			int yearLen = 0;
			year = " (" + movie.getProductionYear() + ")";
			yearLen = fm.stringWidth(year);
			reTitle = TextUtil.shorten(title, fm, width - yearLen);
			reTitle += year;
		}
		return reTitle;
	}

//	public String getTitleWithSeasonsCount(MoreDetail series, FontMetrics fm,
//			int width) {
//		int cnt = 0;
//		if (series instanceof Series) {
//			cnt = ((Series) series).season.length;
//		}
//		if (cnt == 0) {
//			return TextUtil.shorten(series.getTitle(), fm, width);
//		}
//		String str = null;
//		if (cnt == 1) {
//			str = series.getTitle() + "(1"
//					+ dataCenter.getString(Resources.TEXT_MSG_SEASON_AVAILABLE)
//					+ ")";
//		} else {
//			str = series.getTitle()
//					+ "(1"
//					+ dataCenter
//							.getString(Resources.TEXT_MSG_SEASONS_AVAILABLE)
//					+ ")";
//		}
//		return str;
//	}

	// public String getRunTime(String runtime) {
	// String reRuntime = "";
	// if (runtime != null) {
	// try {
	// int minute = Integer.parseInt(runtime);
	// int hour = minute / 60;
	// int min = minute % 60;
	// String sHour = null;
	// if (hour < 10) {
	// sHour = "0" + hour;
	// } else {
	// sHour = "" + hour;
	// }
	// String sMin = null;
	// if (min < 10) {
	// sMin = "0" + min;
	// } else {
	// sMin = "" + min;
	// }
	// reRuntime = sHour + ":" + sMin + ":00";
	// } catch (Exception e) {
	// Log.printError(e);
	// }
	// }
	// return reRuntime;
	// }

	public Image getRatingImage(String rating) {
		Image ratingImg = null;
		if (rating != null) {
			if (rating.equalsIgnoreCase(Definition.RATING_G)) {
				ratingImg = iconG;
			} else if (rating.equals(Definition.RATING_OVER_8)) {
				ratingImg = icon8;
			} else if (rating.equals(Definition.RATING_OVER_13)) {
				ratingImg = icon13;
			} else if (rating.equals(Definition.RATING_OVER_16)) {
				ratingImg = icon16;
			} else if (rating.equals(Definition.RATING_OVER_18)) {
				ratingImg = icon18;
			}
		}

		return ratingImg;
	}

	public String getPrice(String price) {
		try {
			double p = Double.parseDouble(price);
			if (p == 0) {
				return dataCenter.getString(Resources.TEXT_LBL_FREE);
			}
		} catch (NumberFormatException nfe) {}
		// VDTRMASTER-5861
		return Formatter.getCurrent().getPrice(price);
	}

//	public String getPrice(float price) {
//		String result = "";
//		if (price == 0) {
//			result = dataCenter.getString(Resources.TEXT_LBL_FREE);
//		} else {
//			result = formatter.getPrice(price);
//		}
//		return result;
//	}

	/**
	 * Paints content offer information.
	 *
	 * @param g
	 *            the Graphics context
	 * @param catalogue
	 *            BaseCatalogue
	 * @param x
	 *            the x coordinate of start point
	 * @param y
	 *            the y coordinate of start point based on text, not image
	 * @param duration
	 *            the flag for painting lease duration
	 * @param c
	 *            the object to be notified as more of the image is converted
	 */
	public int paintOffer(Graphics g, MoreDetail catalogue, int x, int y,
			boolean duration, UIComponent c) {
		if (catalogue == null)
			return x;
		Offer sdOffer = catalogue.getOfferSD(null);
		Offer hdOffer = catalogue.getOfferHD(null);
		String offer = null;
		g.setFont(fOffer);
		if (duration) {
			long leaseDuration = 0;
			String strDuration = "";
			if (sdOffer != null) {
				leaseDuration = sdOffer.leaseDuration;
			} else if (hdOffer != null) {
				leaseDuration = hdOffer.leaseDuration;
			}
			// VDTRMASTER-5861
			strDuration = Formatter.getCurrent().getDurationText2(leaseDuration);
			g.setColor(new Color(204, 193, 171));
			g.drawString(strDuration, x, y);
			x = x + fmOffer.stringWidth(strDuration) + 5;
		}
		g.setColor(new Color(242, 231, 207));
		if (sdOffer != null) {
			g.drawImage(iconSd, x, y - 11, c);
			x = x + iconSd.getWidth(c) + 5;
			offer = getPrice(sdOffer.amount);
			g.drawString(offer, x, y);
			x = x + fmOffer.stringWidth(offer) + 9;
		}
		if (sdOffer != null && hdOffer != null) {
			g.drawImage(slashPrice, x, y - 12, c);
			x = x + slashPrice.getWidth(c) + 7;
		}
		if (hdOffer != null) {
			boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
			boolean fHd = Definition.DEFNITION_FHD.equals(hdOffer.definition);
			
			// 4K
			if (uhd) {
				g.drawImage(iconUhd, x, y - 12, c);
				x = x + (iconUhd.getWidth(c)) + 5;
			} else {
				g.drawImage(fHd ? iconFhd : iconHd, x, y - 12, c);
				x = x + (fHd ? iconFhd.getWidth(c) : iconHd.getWidth(c)) + 5;
			}
			offer = getPrice(hdOffer.amount);
			g.drawString(offer, x, y);
			x = x + fmOffer.stringWidth(offer) + 9;
		}
		return x;
	}

	public int widthOffer(Graphics g, MoreDetail catalogue,
			boolean duration, UIComponent c) {
		if (catalogue == null)
			return 0;
		int w = 0;
		Offer sdOffer = catalogue.getOfferSD(null);
		Offer hdOffer = catalogue.getOfferHD(null);
		String offer = null;
		g.setFont(fOffer);
		if (duration) {
			long leaseDuration = 0;
			String strDuration = "";
			if (sdOffer != null) {
				leaseDuration = sdOffer.leaseDuration;
			} else if (hdOffer != null) {
				leaseDuration = hdOffer.leaseDuration;
			}
			// VDTRMASTER-5861
			strDuration = Formatter.getCurrent().getDurationText2(leaseDuration);
			g.setColor(new Color(204, 193, 171));
			w = w + fmOffer.stringWidth(strDuration) + 5;
		}
		g.setColor(new Color(242, 231, 207));
		if (sdOffer != null) {
			w = w + iconSd.getWidth(c) + 5;
			offer = getPrice(sdOffer.amount);
			w = w + fmOffer.stringWidth(offer) + 9;
		}
		if (sdOffer != null && hdOffer != null) {
			w = w + slashPrice.getWidth(c) + 7;
		}
		if (hdOffer != null) {
			boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
			boolean fHd = Definition.DEFNITION_FHD.equals(hdOffer.definition);
			
			if (uhd) {
				w = w + iconUhd.getWidth(c) + 5;
			} else {
				w = w + (fHd ? iconFhd.getWidth(c) : iconHd.getWidth(c)) + 5;
			}
			offer = getPrice(hdOffer.amount);
			w = w + fmOffer.stringWidth(offer) + 9;
		}
		return w;
	}

//	public int paintOffer(Graphics g, MoreDetail catalogue, int x, int y,
//			boolean duration, int iconX, UIComponent c) {
//		if (catalogue == null)
//			return x;
//		Offer sdOffer = catalogue.getOfferSD(null);
//		Offer hdOffer = catalogue.getOfferHD(null);
//		String offer = null;
//		g.setFont(fOffer);
//		if (duration) {
//			long leaseDuration = 0;
//			String strDuration = "";
//			if (sdOffer != null) {
//				leaseDuration = sdOffer.leaseDuration;
//			} else if (hdOffer != null) {
//				leaseDuration = hdOffer.leaseDuration;
//			}
//			strDuration = formatter.getDurationText2(leaseDuration);
//			g.setColor(new Color(204, 193, 171));
//			g.drawString(strDuration, x, y);
//		}
//		x = iconX;
//		g.setColor(new Color(242, 231, 207));
//		if (sdOffer != null) {
//			g.drawImage(iconSd, x, y - 11, c);
//			x = x + iconSd.getWidth(c) + 5;
//			offer = getPrice(sdOffer.amount);
//			g.drawString(offer, x, y);
//			x = x + fmOffer.stringWidth(offer) + 9;
//		}
//		if (sdOffer != null && hdOffer != null) {
//			g.drawImage(slashPrice, x, y - 12, c);
//			x = x + slashPrice.getWidth(c) + 7;
//		}
//		if (hdOffer != null) {
//			g.drawImage(iconHd, x, y - 11, c);
//			x = x + iconHd.getWidth(c) + 5;
//			offer = getPrice(hdOffer.amount);
//			g.drawString(offer, x, y);
//			x = x + fmOffer.stringWidth(offer) + 9;
//		}
//		return x;
//	}

	// paint price information to 3 lines
	public int paintOffer(Graphics g, MoreDetail catalogue, int x, int y,
			int r, UIComponent c) {
		return paintOffer(g, catalogue, x, y, r, false, c);
	}
	public int paintOffer(Graphics g, MoreDetail catalogue, int x, int y,
			int r, boolean inBundleOnly, UIComponent c) {
		Offer sdOffer = catalogue.getOfferSD(null);
		Offer hdOffer = catalogue.getOfferHD(null);
		long mvLeaseDur = 0;
		String mvSDAmount = null;
		String mvHDAmount = null;
		boolean fHd = false;
		boolean uhd = false;
		if (sdOffer != null) {
			mvLeaseDur = sdOffer.leaseDuration;
			mvSDAmount = getPrice(sdOffer.amount);
		}
		if (hdOffer != null) {
			mvLeaseDur = hdOffer.leaseDuration;
			mvHDAmount = getPrice(hdOffer.amount);
			
			// 4K
			if (Definition.DEFNITION_UHD.equals(hdOffer.definition)) {
				uhd = true;
			} else {
				fHd = Definition.DEFNITION_FHD.equals(hdOffer.definition);
			}
		}

		// VDTRMASTER-5861
		String strMVDur = Formatter.getCurrent().getDurationText2(mvLeaseDur);

		String type = null;
		if (catalogue instanceof Episode) {
			type = dataCenter.getString(Resources.TEXT_LBL_EPISODE);
		} else if (catalogue instanceof Bundle) {
			type = dataCenter.getString(Resources.TEXT_LBL_BUNDLE);
		} else if (catalogue instanceof Season) {
			type = dataCenter.getString(Resources.TEXT_LBL_SEASON);
			strMVDur = null;
		} else if (catalogue instanceof Series) {
			type = dataCenter.getString(Resources.TEXT_LBL_SERIES);
			strMVDur = null;
		} else if (catalogue instanceof Video) {
			type = ((Video) catalogue).getType();
//			dataCenter.getString(((Video) catalogue).isSpecialEvent() 
//					? Resources.TEXT_LBL_EVENT : Resources.TEXT_LBL_MOVIE);
		}
			
		g.setFont(FontResource.BLENDER.getFont(19));
		g.setColor(new Color(242, 231, 207));
		g.drawString(type, x, y);

		if (strMVDur != null && inBundleOnly == false) {
			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(new Color(204, 193, 171));
			GraphicUtil.drawStringRight(g, strMVDur, r, y);
		}

		g.setFont(FontResource.BLENDER.getFont(18));
		g.setColor(new Color(242, 231, 207));
		// Movie - Amount (SD)
		if (mvSDAmount != null) {
			y += 5;
			g.drawImage(i_01_icon_sd_s, x - 1, y, c);
			g.setFont(FontResource.BLENDER.getFont(19));
			y += 12;
			if (inBundleOnly == false) {
				GraphicUtil.drawStringRight(g, mvSDAmount, r, y);
			}
			y -= 1;
		}
		// Movie - Amount (HD)
		if (mvHDAmount != null) {
			y += 5;
			
			// 4K
			if (uhd) {
				g.drawImage(i_01_icon_uhd_s, x - 1, y, c);
			} else {
				g.drawImage(fHd ? i_01_icon_fhd_s : i_01_icon_hd_s, x - 1, y, c);
			}
			g.setFont(FontResource.BLENDER.getFont(19));
			y += 12;
			if (inBundleOnly == false) {
				GraphicUtil.drawStringRight(g, mvHDAmount, r, y);
			}
		}

		return y;
	}

	/**
	 * Paints content additional informations like runtime, genre, language,
	 * rating and etc.
	 *
	 * @param g
	 *            the g
	 * @param catalogue
	 *            the catalogue
	 * @param runtime
	 *            the runtime
	 * @param country
	 *            the country
	 * @param genre
	 *            the genre
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param c
	 *            the c
	 */
	public int paintAdditionalInfo(Graphics g, MoreDetail catalogue,
			boolean runtime, boolean genre, boolean country, int x, int y,
			UIComponent c) {
		return paintAdditionalInfo(g, catalogue, runtime, genre, country, 3, x, y, c);
	}
	public int paintAdditionalInfo(Graphics g, MoreDetail catalogue,
			boolean runtime, boolean genre, boolean country, int countryCount, int x, int y,
			UIComponent c) {
		return paintAdditionalInfo(g, catalogue, runtime, genre, country, countryCount, x, y, 1, 0, c);
	}
	public int paintAdditionalInfo(Graphics g, MoreDetail catalogue,
			boolean runtime, boolean genre, boolean country, int countryCount, int x, int y,
			int limitWidth, int calcWidth, UIComponent c) {
		if (catalogue == null)
			return x;
		
		// R5
		int originX = x;
		// VDTRMASTER-5678
		if (catalogue instanceof Bundle && c instanceof CarouselViewUI == false) {
			g.setColor(Color.white);
			g.setFont(FontResource.BLENDER.getFont(20));
		} else {
			g.setColor(new Color(200, 200, 200));
			g.setFont(fAddinfo);
		}
		boolean isSpecial = catalogue instanceof Video
				&& ((Video) catalogue).isSpecialEvent();
		int gap = 10;
		if (runtime) {
			long runTime = catalogue.getRuntime();
			// VDTRMASTER-5861
			String strRun = Formatter.getCurrent().getDurationText(runTime);
			g.drawString(strRun, x, y);
			x = x + fmInfo.stringWidth(strRun) + gap;
			if (genre || (country && !isSpecial)) {
				g.drawImage(slashClar, x, y - 11, c);
				x = x + slashClar.getWidth(c) + gap;
			}
		}
		if (genre) {
			String genreStr = catalogue.getGenre();
			if (genreStr != null) {
				// VDTRMASTER-5678
				if (catalogue instanceof Bundle == false && catalogue instanceof Series == false) {
					genreStr = TextUtil.shorten(genreStr, fmInfo, limitWidth < calcWidth ? 90 : 130);
				}
				g.drawString(genreStr, x, y);
				x = x + fmInfo.stringWidth(genreStr) + gap;
				if (country && isSpecial == false) {
					g.drawImage(slashClar, x, y - 11, c);
					x = x + slashClar.getWidth(c) + gap;
				}
			}
		}
		
		if (country && isSpecial == false) {
			String str = catalogue.getCountry(countryCount);
			g.drawString(str, x, y);
			x = x + fmInfo.stringWidth(str) + gap;
//			g.drawImage(slashClar, x, y - 11, c);
//			x = x + slashClar.getWidth(c) + gap;
		}
		// R5 like line feed
		g.setColor(new Color(200, 200, 00));
		g.setFont(fAddinfo);
		
		if (runtime || genre || country) {
			if (c instanceof CarouselViewUI == false) {
				if (catalogue instanceof Season) {
					x = 61;
					y += 9;
				} else if (catalogue instanceof Series == false){
					x = originX;
					y += 9;
				} else if (catalogue instanceof Series) {
					y -= 11;
				}
			} else {
				y -= 11;
			}
		}
		
		if (catalogue.hasEnglish()) {
			g.drawImage(iconEn, x, y, c);
			x = x + iconEn.getWidth(c) + 3;
		}
		if (catalogue.hasFrench()) {
			g.drawImage(iconFr, x, y, c);
			x = x + iconFr.getWidth(c) + 3;
		}
		Image rating = getRatingImage(catalogue.getRating());
		if (rating != null) {
			g.drawImage(rating, x, y, c);
			x = x + rating.getWidth(c) + 3;
		}
		if (catalogue.hasClosedCaption()) {
			g.drawImage(iconCc, x, y, c);
			x = x + iconCc.getWidth(c) + 3;
		}
		return x;
	}

	public int widthAdditionalInfo(Graphics g, MoreDetail catalogue,
			boolean runtime, boolean genre, boolean country, UIComponent c) {
		return widthAdditionalInfo(g, catalogue, runtime, genre, country, 3, c);
	}
	public int widthAdditionalInfo(Graphics g, MoreDetail catalogue,
			boolean runtime, boolean genre, boolean country, int countryCount, UIComponent c) {
		if (catalogue == null)
			return 0;
		int w = 0;
		g.setFont(fAddinfo);
		int gap = 10;
		if (runtime) {
			long runTime = catalogue.getRuntime();
			// VDTRMASTER-5861
			String strRun = Formatter.getCurrent().getDurationText(runTime);
			w = w + fmInfo.stringWidth(strRun) + gap;
		}
		if (slashClar == null) {
			slashClar = dataCenter.getImage("slash_clar.png");
		}
		if (genre) {
			if (runtime) {
				w = w + slashClar.getWidth(c) + gap;
			}
			if (catalogue.getGenre() != null) {
				w = w + fmInfo.stringWidth(catalogue.getGenre()) + gap;
				w = w + slashClar.getWidth(c) + gap;
			}
		}
		boolean isSpecial = catalogue instanceof Video
				&& ((Video) catalogue).isSpecialEvent();
		if (country && isSpecial == false) {
			String str = catalogue.getCountry(countryCount);
			w = w + fmInfo.stringWidth(str) + gap;
			w = w + slashClar.getWidth(c) + gap;
		}
		if (catalogue.hasEnglish()) {
			w = w + iconEn.getWidth(c) + 3;
		}
		if (catalogue.hasFrench()) {
			w = w + iconFr.getWidth(c) + 3;
		}
		Image rating = getRatingImage(catalogue.getRating());
		if (rating != null) {
			w = w + rating.getWidth(c) + 3;
		}
		if (catalogue.hasClosedCaption()) {
			w = w + iconCc.getWidth(c) + 3;
		}
		return w;
	}

	public void paintAdditionalInfoIcon(Graphics g, MoreDetail catalogue,
			int x, int y, UIComponent c) {
		if (catalogue.hasEnglish()) {
			g.drawImage(iconEn, x, y, c);
			x = x + iconEn.getWidth(c) + 3;
		}
		if (catalogue.hasFrench()) {
			g.drawImage(iconFr, x, y, c);
			x = x + iconFr.getWidth(c) + 3;
		}
		Image rating = getRatingImage(catalogue.getRating());
		if (rating != null) {
			g.drawImage(rating, x, y, c);
			x = x + rating.getWidth(c) + 3;
		}
		if (catalogue.hasClosedCaption()) {
			g.drawImage(iconCc, x, y, c);
		}
	}

	public void paintAdditionalInfoNoIcon(Graphics g, MoreDetail catalogue,
			boolean runtime, boolean genre, boolean country, int x, int y,
			UIComponent c) {
		if (catalogue == null)
			return;
		g.setColor(new Color(200, 200, 200));
		g.setFont(fAddinfo);
		int gap = 10;
		if (runtime) {
			long runTime = catalogue.getRuntime();
			// VDTRMASTER-5861
			String strRun = Formatter.getCurrent().getDurationText(runTime);
			g.drawString(strRun, x, y);
			x = x + fmInfo.stringWidth(strRun) + gap;
		}
		if (genre) {
			if (catalogue.getGenre() != null) {
				if (runtime) {
					g.drawImage(slashClar, x, y - 11, c);
					x = x + slashClar.getWidth(c) + gap;
				}
				g.drawString(catalogue.getGenre(), x, y);
				x = x + fmInfo.stringWidth(catalogue.getGenre()) + gap;
			}
		}
		if (country) {
			String str = catalogue.getCountry(3);
			if ((runtime || genre) && str.length() > 0) {
				g.drawImage(slashClar, x, y - 11, c);
				x = x + slashClar.getWidth(c) + gap;
			}
			int w = fmInfo.stringWidth(str);
			if (c instanceof ListViewUI && x + w > 915) {
				str = catalogue.getCountry(1);
			}
			g.drawString(str, x, y);
			//x = x + w + gap;
		}
	}
}

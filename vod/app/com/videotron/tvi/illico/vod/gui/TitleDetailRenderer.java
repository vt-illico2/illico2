package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.data.backend.BookmarkServerConfig;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.TitleDetailUI;

public class TitleDetailRenderer extends BaseRenderer {

	private Image i01Assetbg;// = dataCenter.getImage("bg.jpg");
	private Image i01DtailSh;// = dataCenter.getImage("01_dtail_sh.png");
//	private Image i_01_possh_b_l;// = dataCenter.getImage("01_possh_b_l.png");
//	private Image i_01_poshigh_b_l;// = dataCenter.getImage("01_poshigh_b_l.png");
//	private Image i_01_possh_b;// = dataCenter.getImage("01_possh_b.png");
//	private Image i_01_poshigh_b;// = dataCenter.getImage("01_poshigh_b.png");
	private Image i02_icon_con;// = dataCenter.getImage("02_icon_con.png");
	private Image blockedPortrait;// = dataCenter.getImage("01_poster_block.png");
	private Image blockedLandscape;// = dataCenter.getImage("01_poster_block_h.png");
	// remove in R7 rebrand
//	private Image i_icon_3scr_v;// = dataCenter.getImage("icon_3scr_v.png");
	// R5
	private Image i_cinoche_cote, i_icon_uhd;
	private Image i02_icon_fav;
	private Color cLable = new Color(211, 202, 183);
	private Color cCreditLbl = new Color(182, 182, 182);
	private Color cTitle = new Color(255, 203, 0);
	private Color cOrderEnd = new Color(225, 195, 80);
	private Color cOTitle = new Color(125, 125, 125);

	private static TitleDetailRenderer instance;

	synchronized public static TitleDetailRenderer getInstance() {
		if (instance == null) {
			instance = new TitleDetailRenderer();
		}
		return instance;
	}

	private TitleDetailRenderer() {
	}

	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return bgRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		i01Assetbg = dataCenter.getImage("bg.jpg");
		i01DtailSh = dataCenter.getImage("01_dtail_sh.png");
//		i_01_possh_b_l = dataCenter.getImage("01_possh_b_l.png");
//		i_01_poshigh_b_l = dataCenter.getImage("01_poshigh_b_l.png");
//		i_01_possh_b = dataCenter.getImage("01_possh_b.png");
//		i_01_poshigh_b = dataCenter.getImage("01_poshigh_b.png");
		i02_icon_con = dataCenter.getImage("02_icon_con.png");
		blockedPortrait = dataCenter.getImage("01_poster_block.jpg");
		blockedLandscape = dataCenter.getImage("01_poster_block_h.jpg");
		// remove in R7 rebrand
//		i_icon_3scr_v = dataCenter.getImageByLanguage("icon_3scr_v.png");
		// R5
		i_cinoche_cote = dataCenter.getImage("cinoche_cote.png");
		i02_icon_fav = dataCenter.getImage("02_icon_fav.png");
		i_icon_uhd = dataCenter.getImage("icon_uhd.png");
		
		setBackGround(i01Assetbg);
		
		super.prepare(c);
	}

	/**
	 * Paints the UIComponent.
	 * 
	 * @param g
	 *            The graphics context to use for painting.
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 */
	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		// g.setColor(Color.white);
		// g.setFont(FontResource.BLENDER.getFont(26));
		// g.drawString(MenuController.getInstance().getLastSecondHistory(), 61,
		// 98);

		Video title = ((TitleDetailUI) c).getTitle();
		if (title == null) {
			String from = MenuController.getInstance().getFrom();
			if ("Search".equalsIgnoreCase(from)) {
//				g.setColor(Color.white);
//				g.setFont(FontResource.BLENDER.getFont(26));
//				GraphicUtil.drawStringCenter(g, ((TitleDetailUI) c).titleId
//						+ " may not exists on VCDS", 480, 400);
			}
			return;
		}
		
		// R5
//		boolean hasSimilarContent = ((TitleDetailUI) c).hasSimilarContents();
//		int similarGap = hasSimilarContent ? -20 : 0;
		int similarGap = -20; // Julie want to keep a same position.

		// poster area, title
		Image poster = null;
		boolean portrait = title.isPortrait();
		boolean adultBlocked = MenuController.getInstance().isAdultBlocked(title);
		boolean ageBlocked = MenuController.getInstance().isBlocked(title); 
		Image flag = null;
		int w = portrait ? ImageRetriever.PR_LG.width : ImageRetriever.LD_LG.width;
		int h = portrait ? ImageRetriever.PR_LG.height : ImageRetriever.LD_LG.height;
		if (adultBlocked) {
			poster = portrait ? blockedPortrait : blockedLandscape;
			
			if (portrait) {
				g.drawImage(poster, 56, 104 + similarGap, w, h, c);
			} else {
				g.drawImage(poster, 56, 104 + similarGap, 204, 153, c);
				w = 204;
				h = 153;
			}
			
		} else {
			poster = title.getImage(w, h);
			if (poster != null) {
				if (portrait) {
					g.drawImage(poster, 56, 104 + similarGap, w, h, c);
				} else {
					g.drawImage(poster, 56, 104 + similarGap, 204, 153, c);
					w = 204;
					h = 153;
				}
			}
			switch (title.getFlag()) {
			case 1: // new
				flag = dataCenter.getImageByLanguage("icon_d_new.png");
				break;
			case 2: // last chance
				flag = dataCenter.getImageByLanguage("icon_d_last.png");
				break;
			case 3: // extras
				flag = dataCenter.getImageByLanguage("icon_d_extras.png");
				break;
			}
			
			// R5 - UHD icon
			Offer hdOffer = title.getOfferHD(null);
			if (hdOffer != null) {
				boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
				if (uhd) {
					g.drawImage(i_icon_uhd, 56, 104 + similarGap + h - i_icon_uhd.getHeight(c), c);
				}
			}
		}
		if (flag != null) {
			g.drawImage(flag, 56 + (portrait ? 210 : 204) - flag.getWidth(c), 104 + similarGap, c);
		}
		if (portrait) {
//			g.drawImage(i_01_poshigh_b, 59, 120, c);
//			g.drawImage(i_01_possh_b, 59, 435, c);
		} else {
//			g.drawImage(i_01_poshigh_b_l, 59, 120, c);
//			g.drawImage(i_01_possh_b_l, 59, 330, c);
		}
//		String availableDate = title.getEndPeriod();
//		if (availableDate.length() > 0) {
//			g.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(cOrderEnd);
//			availableDate = dataCenter.getString(Resources.TEXT_LBL_AVAILABLE)
//					+ " " + availableDate;
//			GraphicUtil.drawStringCenter(g, availableDate, portrait ? 164 : 198, (portrait ? 441 : 333) + similarGap);
//		}
		// R5 - remove to keep a same position no matter with Portrait or landscases
//		if (!title.isPortrait()) {
//			g.translate(81, 0);
//		}
		
		g.setFont(FontResource.BLENDER.getFont(27));
		g.setColor(cTitle);
		if (adultBlocked && !MenuController.getInstance().needToDisplayFavorite()) {
			g.drawImage(i02_icon_con, 289, 121 - 20 + similarGap, c);
			g.drawString(
					dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE),
					289 + i02_icon_con.getWidth(c) + 4, 121 + similarGap);
		} else {
			String titleStr = TextUtil.shorten(title.getTitle(), g.getFontMetrics(), portrait ? 550 : 500);
			if (ageBlocked && !MenuController.getInstance().needToDisplayFavorite()) {
				g.drawImage(i02_icon_con, 289, 121 - 20 + similarGap, c);
				g.drawString(titleStr,289 + (ageBlocked ? i02_icon_con.getWidth(c) + 4 : 0), 121 + similarGap);
			} else if (BookmarkManager.getInstance().isInWishlist(title) != null) {
				g.drawImage(i02_icon_fav, 289, 121 - 20 + similarGap, c);
				g.drawString(titleStr,289 + i02_icon_fav.getWidth(c) + 4, 121 + similarGap);
			} else {
				g.drawString(titleStr,289, 121 + similarGap);
			}
			
			String oTitleStr = title.getOriginalTitle();
			
			if (oTitleStr != null) {
				g.setColor(cOTitle);
				g.setFont(FontResource.BLENDER.getFont(17));
				g.drawString("(" + oTitleStr + ")", 290, 142 + similarGap);
			}
		}
		
		// genre and year
		g.setFont(FontResource.BLENDER.getFont(20));
		g.setColor(Color.white);
		String str = "";
		String genreStr = title.getGenre();
		if (genreStr != null) {
			if (g.getFontMetrics().stringWidth(genreStr) > 202) {
				genreStr = TextUtil.shorten(genreStr, g.getFontMetrics(), 202);
			}
			str += genreStr;
		}
		String yearStr = title.getProductionYear();
		if (yearStr != null && !yearStr.equals(TextUtil.EMPTY_STRING)) {
			str += (" (" + yearStr + ")");
		}
		g.drawString(str, 289, 180 + similarGap);
		paintAdditionalInfo(g, title, true, false, true, 289, 200 + similarGap, c);
		
		// actor, director
		g.setFont(FontResource.BLENDER.getFont(16));
		g.setColor(cCreditLbl);
		g.drawString(dataCenter.getString(Resources.TEXT_LBL_DIRECTOR), 290,
				258 + similarGap);
		String lbl = dataCenter.getString(Resources.TEXT_LBL_ACTOR);
		if (title.isSpecialEvent()) {
			lbl = dataCenter.getString(Resources.TEXT_LBL_WITH);
		}
		int x = 0;
		g.drawString(lbl, 290, 277 + similarGap);
		x = 299 + g.getFontMetrics().stringWidth(
				dataCenter.getString(Resources.TEXT_LBL_DIRECTOR));

//		g.setFont(FontResource.BLENDER.getFont(14));
		g.setColor(Color.white);
		g.drawString(
				TextUtil.shorten(title.getDirector(2), g.getFontMetrics(), 223),
				x, 258 + similarGap);
		String[] actors = TextUtil.split(title.getActors(3), g.getFontMetrics(), 210, 2);
		if (actors != null) {
			x = 299 + g.getFontMetrics().stringWidth(lbl);
			for (int i = 0; i < actors.length; i++) {
				g.drawString(actors[i], x, 277 + similarGap + i * 15);
			}
		}
		// R5 - remove to keep a same position no matter with Portrait or landscases
//		if (!title.isPortrait()) {
//			g.translate(-81, 0);
//		}
		
		// offers
		if (title.inBundle() && ((TitleDetailUI) c).getBundle() != null) {
			// in the case of bundle
			paintOffer(g, ((TitleDetailUI) c).getBundle(), 626, 240 + similarGap, 769, c);
			paintOffer(g, title, 626, 304 + similarGap, 769, c);
			
			// remove in R7 rebrand
//			if (title.is3screen()) {
//				g.drawImage(i_icon_3scr_v, 833, 289 + similarGap, c);
//			}
		} else {
			paintOffer(g, title, 626, 240 + similarGap, 769, c);
			
			// remove in R7 rebrand
//			if (title.is3screen()) {
//				g.drawImage(i_icon_3scr_v, 833, 225 + similarGap, c);
//			}
		}
		
		// cinoche cote
		if (((Video) title).externalRef != null) {
			g.drawImage(i_cinoche_cote, 625, 173, c);
			g.setColor(Color.white);
			g.setFont(FontResource.DINMED.getFont(18));
			String value = ((Video) title).externalRef.value;
			g.drawString(value, 679, 187);
			int valueWidth = g.getFontMetrics().stringWidth(value) + 2;
			g.setFont(FontResource.DINMED.getFont(12));
			g.drawString(SLASH_FIVE, 679 + valueWidth, 187);
		}

//		// offers
//		int x = 313;
//		int r = 457;
//		int y1 = 145;
//		boolean drawOffer = false;
//		if (title.inBundleOnly == false) {
//			y1 = paintOffer(g, title, x, 145, r, c);
//			drawOffer = true;
//			x = 495;
//			r = 638;
//		}
//		int x3screen = 494;
//		int y = y1;
//		if (title.inBundle() && ((TitleDetailUI) c).getBundle() != null) {
////			if (Resources.currentLanguage == Resources.LANGUAGE_FR) {
////				r += 15;
////			}
//			int y2 = paintOffer(g, ((TitleDetailUI) c).getBundle(), x, 145, r, c);
//			x3screen = r + 20;
//			y = Math.max(y, y2);
//			if (drawOffer) {
//				g.setColor(new Color(127, 121, 110));
//				g.fillRect(475, 134, 1, y - 170 + 2);
//			}
//		}
//
//		// runtime, genre, country, etc...
//		paintAdditionalInfo(g, title, true, true, true, 312, y + 22, c);
//		g.drawImage(i01DtailSh, 289, 219, c);
//		if (title.is3screen()) {
//			g.drawImage(i_icon_3scr_v, x3screen, y - i_icon_3scr_v.getHeight(c) + 2, c);
//		}
//
//		// actor, director
//		g.setFont(FontResource.BLENDER.getFont(16));
//		g.setColor(cCreditLbl);
//		g.drawString(dataCenter.getString(Resources.TEXT_LBL_DIRECTOR), 313,
//				245);
//		String lbl = dataCenter.getString(Resources.TEXT_LBL_ACTOR);
//		if (title.isSpecialEvent()) {
//			lbl = dataCenter.getString(Resources.TEXT_LBL_WITH);
//		}
//		g.drawString(lbl, 313, 264);
//		x = 323 + g.getFontMetrics().stringWidth(
//				dataCenter.getString(Resources.TEXT_LBL_DIRECTOR));
//
//		g.setFont(FontResource.DINMED.getFont(14));
//		g.setColor(Color.white);
//		g.drawString(
//				TextUtil.shorten(title.getDirector(3), g.getFontMetrics(), 500),
//				x, 246);
//		g.drawString(
//				TextUtil.shorten(title.getActors(3), g.getFontMetrics(), 500),
//				x, 265);
//		if (!title.isPortrait()) {
//			g.translate(-81, 0);
//		}
		if (Log.EXTRA_ON /*|| Environment.EMULATOR*/) {
			g.setFont(FontResource.BLENDER.getFont(19));
			BookmarkServerConfig conf = BookmarkManager.getInstance()
					.getConfig();
			String ctrl = conf == null ? ("D)" + DataCenter.getInstance()
					.getString("BOOKMARK_URL")) : conf.toString();
			String tmp = title.dumpPlayout()
					+ "\n\nCTRL: "
					+ ctrl
					+ "\nVCDS: "
					+ DataCenter.getInstance().getString("VCDS_URL")
					+ "\nImage: "
					+ DataCenter.getInstance().getString("IMAGE_URL") + ImageRetriever.BASE
					+ "\nDNCS: "
					+ DataCenter.getInstance().getString("DNCS_IP")
					+ ":"
					+ DataCenter.getInstance().getString("DNCS_PORT")
					+ "\nGW_IP: "
					+ DataCenter.getInstance().getString("GW_IP")
					+ "\nSVC: "
					+ StringUtil.byteArrayToHexString(
							ServiceGroupController.getInstance()
									.getServiceGroupData()).trim();
			String[] dump = TextUtil.split(tmp, g.getFontMetrics(), 900);
			for (int i = 0; i < dump.length; i++) {
				g.setColor(Color.black);
				g.drawString(dump[i], 63, 102 + i * 18);
				g.setColor(Color.green);
				g.drawString(dump[i], 61, 100 + i * 18);
			}
			
			dump = title.dumpImages();
			for (int i = 0; i < dump.length; i++) {
				g.setColor(Color.black);
				GraphicUtil.drawStringRight(g, dump[i], 902, 85 + i * 18);
				g.setColor(Color.green);
				GraphicUtil.drawStringRight(g, dump[i], 900, 83 + i * 18);
			}
			int y = 90 + dump.length * 18;
			
			tmp = title.dumpOffer();
			dump = TextUtil.split(tmp, g.getFontMetrics(), 900);
			for (int i = 0; i < dump.length; i++) {
				g.setColor(Color.black);
				GraphicUtil.drawStringRight(g, dump[i], 902, y + 2 + i * 18);
				g.setColor(Color.green);
				GraphicUtil.drawStringRight(g, dump[i], 900, y + i * 18);
			}
			y += 5 + dump.length * 18;
			
			tmp = title.dumpService();
			dump = TextUtil.split(tmp, g.getFontMetrics(), 900);
			for (int i = 0; i < dump.length; i++) {
				g.setColor(Color.black);
				GraphicUtil.drawStringRight(g, dump[i], 902, y + 2 + i * 18);
				g.setColor(Color.green);
				GraphicUtil.drawStringRight(g, dump[i], 900, y + i * 18);
			}
		}
	}
}

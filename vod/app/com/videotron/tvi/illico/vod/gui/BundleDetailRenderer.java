package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.BundleDetailUI;

public class BundleDetailRenderer extends BaseRenderer {
	private Rectangle bgRec = new Rectangle(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

	private Image i01VodviewBg;// = dataCenter.getImage("bg.jpg");
//	private Image i02GrideEf04;// = dataCenter.getImage("02_gride_ef04.png");
	private Image arrRight;// = dataCenter.getImage("01_arrow_r.png");
	private Image i_icon_3scr_h;// = dataCenter.getImage("icon_3scr_h.png");
//	private Image i_01_poshigh_m;
//	private Image i_01_possh_m;
	private Image blockedPortrait;
	// R5
	private Image i_cinoche_cote;
	private Color cInfo = new Color(255, 255, 255);
	private Color cRuntime = new Color(200, 200, 200);
	private Color cPersonLbl = new Color(182, 182, 182);
	private Color cAvailUntil = new Color(225, 195, 80);
	private Color cInfoSimple = new Color(193, 185, 170);
	private Color cInfoPrice = new Color(242, 231, 207);
	private Color cLabel = new Color(211, 202, 183);
	private Color c179 = new Color(179, 179, 179);
	private Color cTitle = new Color(255, 205, 12);
	
	public Point arrowR = new Point();

	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return bgRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		FrameworkMain.getInstance().getImagePool().waitForAll();
		
		i01VodviewBg = dataCenter.getImage("bg.jpg");
//		i02GrideEf04 = dataCenter.getImage("02_gride_ef04.png");
		arrRight = dataCenter.getImage("02_ars_r.png");
		i_icon_3scr_h = dataCenter.getImageByLanguage("icon_3scr_h.png");
//		i_01_possh_m = dataCenter.getImage("01_possh_m.png");
//		i_01_poshigh_m = dataCenter.getImage("01_poshigh_m.png");
		blockedPortrait = dataCenter.getImage("01_poster_block.jpg");
		// R5
		i_cinoche_cote = dataCenter.getImage("cinoche_cote.png");
		
		setBackGround(i01VodviewBg);
		
		super.prepare(c);
	}

	/**
	 * Paints the UIComponent.
	 * 
	 * @param g
	 *            The graphics context to use for painting.
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 */
	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		// g.drawImage(i01VodPriceLine, 52, 371, c);
		BundleDetailUI ui = (BundleDetailUI) c;
		Bundle bundle = ui.getBundle();
		if (bundle == null) {
			return;
		}
		
//		g.drawImage(i02GrideEf04, 76, 302, c);
		
		// bundle poster
		int w = 144;
		int h = 216;
		boolean adultBlocked = MenuController.getInstance().isAdultBlocked(bundle);
		boolean ageBlocked = adultBlocked
				|| MenuController.getInstance().isBlocked(bundle);
		if (adultBlocked) {
			g.drawImage(blockedPortrait, 62, 81, 144, 216, c);
		} else {
			g.drawImage(bundle.getImage(144, 216), 62, 81, 144, 216, c);
			// R5 - UHD icon
			Offer hdOffer = bundle.getOfferHD(null);
			if (hdOffer != null) {
				boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
				if (uhd) {
					g.drawImage(iconUhd, 62, 81 + 216 - iconUhd.getHeight(c), c);
				}
			}
		}
//		g.drawImage(i_01_poshigh_m, 62, 81, c);
//		g.drawImage(i_01_possh_m, 62 - 3, 81 + 216, c);
		// g.drawImage(i_01_bun_show, 123, 81, c);
		MoreDetail movie = (MoreDetail) ui.getCurrentCatalogue();
		if (movie == null) {
			g.setColor(ui.getState() == BaseUI.STATE_DEACTIVATED ? cTitle : Color.lightGray);
			g.drawRect(62 - 1, 81 - 1, w + 1, h + 1);
			g.drawRect(62 - 2, 81 - 2, w + 3, h + 3);
			g.drawRect(62 - 3, 81 - 3, w + 5, h + 5);
			if (ui.getState() == BaseUI.STATE_DEACTIVATED && arrRight != null) {
				arrowR.x = 62 + w + 5;
				arrowR.y = 81 + h / 2 - arrRight.getHeight(ui) / 2;
				g.drawImage(arrRight, 
						arrowR.x,
						arrowR.y,
//						62 + w + 5,
//					81 + h / 2 - arrRight.getHeight(ui) / 2, 
					ui);
			} else {
				arrowR.x = -1;
			}
		}

		// title
		String title = bundle.getTitle();
		if (adultBlocked) {
			title = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
		}
		g.setFont(FontResource.BLENDER.getFont(26));
		g.setColor(Color.white);
		g.drawString(title, 236, 98);
		int x = g.getFontMetrics().stringWidth(title);
		g.setFont(FontResource.BLENDER.getFont(17));
		g.setColor(c179);
		g.drawString(dataCenter.getString(Resources.TEXT_MSG_BUNDLE),
				x + 236 + 14, 98);
		
		if (Log.EXTRA_ON) {
			g.setColor(Color.green);
			g.drawString("isMovieBundle=" + bundle.isMovieBundle(), 236, 114);
		}

		int r = 203;
//		if (Resources.currentLanguage == Resources.LANGUAGE_FR) {
//			r += 15;
//		}
		paintOffer(g, bundle, 60, 343, r, c);
		if (movie != null) {
			if (movie instanceof Video == false || ((Video) movie).inBundleOnly == false) {
				paintOffer(g, movie, 60, 407, r, c);
			}
			// paint movie information
			if (bundle.isMovieBundle()) {
				g.setFont(FontResource.BLENDER.getFont(22));
				g.setColor(cInfo);
				String str = "";
				String genreStr = movie.getGenre();
				if (genreStr != null) {
					if (g.getFontMetrics().stringWidth(genreStr) > 202) {
						genreStr = TextUtil.shorten(genreStr, g.getFontMetrics(), 202);
					}
					str += genreStr;
				}
				String yearStr = movie.getProductionYear();
				if (yearStr != null && !yearStr.equals(TextUtil.EMPTY_STRING)) {
					str += (" (" + yearStr + ")");
				}
				g.drawString(str, 237, 345);
				paintAdditionalInfo(g, movie, true, false, true, 237, 365, c);
				
			} else {
				paintAdditionalInfo(g, movie, true, true, false, 237, 345, c);
			}
			
			g.setFont(FontResource.DINMED.getFont(15));
			g.setColor(cInfo);
			String[] actors = TextUtil.split(movie.getActors(3), g.getFontMetrics(), 360);
			if (actors != null) {
				for(int i = 0; i < actors.length; i++) {
					g.drawString(actors[i], 237, 414 + i * 17);
				}
			}
			
//			c.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(cAvailUntil);
//			if (movie.getEndPeriod().length() > 0) {
//				String avail = dataCenter.getString(Resources.TEXT_LBL_AVAILABLE)
//						+ " " + movie.getEndPeriod();
//				g.drawString(avail, 629, 344);
//			}
			
			// cinoche cote
			if (movie instanceof Video && ((Video) movie).externalRef != null) {
				g.drawImage(i_cinoche_cote, 505, 331, c);
				g.setColor(Color.white);
				g.setFont(FontResource.DINMED.getFont(18));
				String value = ((Video) movie).externalRef.value;
				g.drawString(value, 559, 345);
				int valueWidth = g.getFontMetrics().stringWidth(value) + 2;
				g.setFont(FontResource.DINMED.getFont(12));
				g.drawString(SLASH_FIVE, 559 + valueWidth, 345);
			}
			// person ( actor, director )
//			g.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(cPersonLbl);
//			// g.drawString(dataCenter.getString(Resources.TEXT_LBL_DIRECTOR),
//			// 237,
//			// 387);
//			String str = dataCenter.getString(Resources.TEXT_LBL_ACTOR);
//			if (movie instanceof Video && ((Video) movie).isSpecialEvent()) {
//				str = dataCenter.getString(Resources.TEXT_LBL_WITH);
//			}
//			g.drawString(str, 237, 384);
//			x = 247 + g.getFontMetrics().stringWidth(str);
//			g.setFont(FontResource.DINMED.getFont(13));
//			g.setColor(cInfo);
//			g.drawString(TextUtil.shorten(movie.getActors(2),
//					g.getFontMetrics(), 300), x, 385);

		} else {
			// paint bundle information
			g.setFont(FontResource.BLENDER.getFont(22));
			g.setColor(Color.white);
			if (bundle.isMovieBundle()) {
				g.drawString(title, 237, 345);
				paintAdditionalInfoIcon(g, bundle, 236, 354, c);
			} else {
				MoreDetail[] titles = bundle.getTitles();
				String bdTitle = title + " ("
						+ titles.length + " "
						+ dataCenter.getString(Resources.TEXT_LBL_EPISODE) + ")";
				g.drawString(bdTitle, 237, 345);
			}
			
//			g.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(cAvailUntil);
//			String avail = dataCenter.getString(Resources.TEXT_LBL_AVAILABLE)
//					+ " " + bundle.getEndPeriod();
//			g.drawString(avail, 629, 344);
		}
	}
}

package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.ui.SeriesOptionUI;

public class SeriesOptionRenderer extends BaseRenderer {
	private Rectangle bgRec = new Rectangle(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

	private Image i01VodviewBg;// = dataCenter.getImage("bg.jpg");
	private Image i01VodBgline;// = dataCenter.getImage("01_vod_bgline.png");
	private Image i01IconSdS;// = dataCenter.getImage("01_icon_sd_s.png");
	private Image i01iconHdS;// = dataCenter.getImage("01_icon_hd_s.png");
	// remove in R7 rebrand
//	private Image i_icon_3scr_v;// = dataCenter.getImage("icon_3scr_v.png");
	private Image i_01_series_line;// = dataCenter.getImage("01_series_line.png");
	private Image i_01_arrow_l;// = dataCenter.getImage("01_arrow_l.png");
	private Image i_01_arrow_r;// = dataCenter.getImage("01_arrow_r.png");
//	private Image i_01_series_prixbg;
//	private Image i_01_series_opbg;
//	private Image i_01_series_opbg_f;
	private Image i_01_series_opbg_line;
	private Image i_01_series_opbg_line_2;
//	private Image i_01_poshigh_s_l;
	private Image i_01_series_foc_145;
	
	private Color cOffer = new Color(242, 231, 207);
	private Color c105 = new Color(105, 105, 105);
	private Color c224 = new Color(224, 224, 224);
	private Color c183 = new Color(183, 183, 183);
	private Color c179 = new Color(179, 179, 179);
	private Color c200 = new Color(200, 200, 200);
	private Color cTitle = new Color(255, 204, 0);
	private Color cBlack = new DVBColor(0, 0, 0, 153);

	private String[] orderLbl1 = new String[3];
	private String[] orderLbl2 = new String[3];
	private String[] rental = new String[3];
	private String[] offerSD = new String[] {BaseElement.NA, BaseElement.NA, BaseElement.NA};
	private String[] offerHD = new String[] {BaseElement.NA, BaseElement.NA, BaseElement.NA};
	private String[] infoTitle = new String[3];
	private String[] contentCnt = new String[3];

	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return bgRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		offerSD = new String[] {BaseElement.NA, BaseElement.NA, BaseElement.NA};
		offerHD = new String[] {BaseElement.NA, BaseElement.NA, BaseElement.NA};
		
		Season season = ((SeriesOptionUI) c).getCatalogueList();
		orderLbl1[0] = dataCenter
				.getString(Resources.TEXT_LBL_ORDER_COMPLETE_SEASON);
		orderLbl1[1] = dataCenter
				.getString(Resources.TEXT_LBL_ORDER_EPISODE_BUNDLE);
		orderLbl1[2] = dataCenter
				.getString(Resources.TEXT_LBL_ORDER_SINGLE_EPISODE);
		orderLbl2[0] = dataCenter.getString(Resources.TEXT_LBL_FULL) + " "
				+ season.seasonNumber;
		orderLbl2[1] = dataCenter.getString(Resources.TEXT_LBL_EPISODE_BUNDLE);
		orderLbl2[2] = dataCenter
				.getString(Resources.TEXT_LBL_INDIVIDUAL_EPISODE);
		Offer offer;
		
		Bundle bundle = (Bundle) CatalogueDatabase.getInstance().getCached(
				season.seasonBundle == null ? "" : season.seasonBundle);
		if (bundle != null) {
			rental[0] = Formatter.getCurrent().getDurationText2(
					bundle.getLeaseDuration());
			offer = bundle.getOfferSD(null);
			if (offer != null) {
				offerSD[0] = getPrice(offer.amount);
			}
			offer = bundle.getOfferHD(null);
			if (offer != null) {
				offerHD[0] = getPrice(offer.amount);
			}
			int cnt = bundle.bundleDetail.episode.length;
			infoTitle[0] = orderLbl2[0];
			// + "("
			// + cnt
			// + " "
			// + dataCenter
			// .getString(cnt == 1 ? Resources.TEXT_MSG_EPISODE
			// : Resources.TEXT_MSG_EPISODES) + ")";
			contentCnt[0] = cnt+ " "
					+ dataCenter
							.getString(cnt == 1 ? Resources.TEXT_MSG_EPISODE
									: Resources.TEXT_MSG_EPISODES);
		} else {
			rental[0] = BaseElement.NA;
		}

		if (season.episodeBundles.length > 0 && (bundle = 
				(Bundle) CatalogueDatabase.getInstance().getCached(season.episodeBundles[0])
				) != null) {
			rental[1] = Formatter.getCurrent().getDurationText2(
					bundle.getLeaseDuration());
			offer = bundle.getOfferSD(null);
			if (offer != null) {
				offerSD[1] = getPrice(offer.amount);
			}
			offer = bundle.getOfferHD(null);
			if (offer != null) {
				offerHD[1] = getPrice(offer.amount);
			}
			if (bundle.bundleDetail != null) {
				int cnt = bundle.bundleDetail.episode.length;
				infoTitle[1] = orderLbl2[1];
				// + "("
				// + cnt
				// + " "
				// + dataCenter
				// .getString(cnt == 1 ? Resources.TEXT_MSG_EPISODE
				// : Resources.TEXT_MSG_EPISODES) + ")";
				contentCnt[1] = cnt + " "
				 + dataCenter
				 .getString(cnt == 1 ? Resources.TEXT_MSG_EPISODE
				 : Resources.TEXT_MSG_EPISODES);
			}
		} else {
			rental[1] = BaseElement.NA;
		}
		Episode episode = season.episode[0];
		if (episode != null) {
			int episodeId = 0;
			try {
				while (episode.getLeaseDuration() == 0) {
					episode = season.episode[++episodeId];
				}
			} catch (ArrayIndexOutOfBoundsException e) {}
			rental[2] = Formatter.getCurrent().getDurationText2(
					episode.getLeaseDuration());
			offer = episode.getOfferSD(null);
			if (offer != null) {
				offerSD[2] = getPrice(offer.amount);
			}
			offer = episode.getOfferHD(null);
			if (offer != null) {
				offerHD[2] = getPrice(offer.amount);
			}
			infoTitle[2] = orderLbl2[2];
			contentCnt[2] = 1 + " " + dataCenter.getString(Resources.TEXT_MSG_EPISODE);
		}
		
		i01VodviewBg = dataCenter.getImage("bg.jpg");
		i01VodBgline = dataCenter.getImage("01_vod_bgline.png");
		i01IconSdS = dataCenter.getImage("01_icon_sd_s.png");
		i01iconHdS = dataCenter.getImage("01_icon_hd_s.png");
		// remove in R7 rebrand
//		i_icon_3scr_v = dataCenter.getImageByLanguage("icon_3scr_v.png");
		i_01_series_line = dataCenter.getImage("01_series_line.png");
		i_01_arrow_l = dataCenter.getImage("02_ars_l.png");
		i_01_arrow_r = dataCenter.getImage("02_ars_r.png");
//		i_01_series_prixbg = dataCenter.getImage("01_series_prixbg.png");
//		i_01_series_opbg = dataCenter.getImage("01_series_opbg.png");
//		i_01_series_opbg_f = dataCenter.getImage("01_series_opbg_f.png");
		i_01_series_opbg_line = dataCenter.getImage("01_series_opbg_line.png");
		i_01_series_opbg_line_2 = dataCenter.getImage("01_series_opbg_line_2.png");
//		i_01_poshigh_s_l = dataCenter.getImage("01_poshigh_s_l.png");
		i_01_series_foc_145 = dataCenter.getImage("01_series_foc_145.png");
		setBackGround(i01VodviewBg);
		
		super.prepare(c);
	}

	/**
	 * Paints the UIComponent.
	 * 
	 * @param g
	 *            The graphics context to use for painting.
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 */
	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		SeriesOptionUI ui = (SeriesOptionUI) c;
		Season season = ui.getCatalogueList();
		int focus = c.getFocus();

		g.setFont(FontResource.BLENDER.getFont(26));
		g.setColor(Color.white);
		String str = dataCenter.getString(Resources.TEXT_LBL_SEASON) + " " + season.seasonNumber;
		g.drawString(str, 61, 98);
		int x = 71 + g.getFontMetrics().stringWidth(str);
		g.setFont(FontResource.BLENDER.getFont(17));
		g.setColor(c179);
		g.drawString(dataCenter.getString(Resources.TEXT_MSG_CHOOSE4), x, 98);
		g.drawImage(i01VodBgline, 0, 284, c);
		
		boolean opt3 = ui.optionLen() == 3;
//		g.drawImage(i_01_series_prixbg, opt3 ? 176 : 248, 252, c);
		if (opt3) {
			g.drawImage(i_01_series_opbg_line, 170, 276, c);
		} else {
			g.drawImage(i_01_series_opbg_line_2, 242, 276, c);
		}
		
		g.setFont(FontResource.BLENDER.getFont(19));
		GraphicUtil.drawStringRight(g, dataCenter.getString(Resources.TEXT_LBL_RENTAL), opt3 ? 259 : 330, 281);
		boolean displaySD = BaseElement.NA.equals(offerSD[0]) == false 
			|| BaseElement.NA.equals(offerSD[1]) == false 
			|| BaseElement.NA.equals(offerSD[2]) == false;
		if (displaySD) {
			g.drawImage(i01IconSdS, opt3 ? 184 : 255, 295, c);
		}
		boolean displayHD = BaseElement.NA.equals(offerHD[0]) == false 
			|| BaseElement.NA.equals(offerHD[1]) == false 
			|| BaseElement.NA.equals(offerHD[2]) == false;
		if (displayHD) {
			g.drawImage(i01iconHdS, opt3 ? 184 : 255, 315, c);
		}

		int w = ImageRetriever.LD_SM.width;
		int h = ImageRetriever.LD_SM.height;
		x = opt3 ? 0 : 70;
		for (int i = 0; i < ui.optionLen(); i++) {
//			g.drawImage(i_01_series_opbg, 269 + i * 141 + x, 110, c);
			if (i == focus) {
//				g.drawImage(i_01_series_opbg_f, 269 + i * 141 + x, 219, c);
			}
			try {
				g.drawImage(season.getImage(w, h), 279 + i * 141 + x, 138, w, h, c);
//				g.drawImage(i_01_poshigh_s_l, 279 + i * 141 + x, 138, c);
				g.setColor(cBlack);
				g.fillRect(279 + i * 141 + x, 228, 120, 32);
				if (i == focus) {
					g.drawImage(i_01_series_foc_145, 276 + i * 141 + x, 135, c);
				}
				
				g.setFont(FontResource.BLENDER.getFont(16));
				g.setColor(i == focus ? Color.black : c224);
				GraphicUtil.drawStringCenter(g, orderLbl1[ui.getOptions()[i]], 340 + i * 141 + x, 242);
	
				g.setFont(FontResource.BLENDER.getFont(19));
				g.setColor(i == focus ? cTitle : cOffer);
				GraphicUtil.drawStringCenter(g, rental[ui.getOptions()[i]], 339 + i * 141
						+ x, 282);
				if (displaySD) {
					GraphicUtil.drawStringCenter(g, offerSD[ui.getOptions()[i]], 339 + i * 141
						+ x, 307);
				}
				if (displayHD) {
					GraphicUtil.drawStringCenter(g, offerHD[ui.getOptions()[i]], 339 + i * 141
						+ x, 327);
				}
			} catch (NullPointerException npe) {}

		} // end of for (options)
		if (focus > 0) {
			g.drawImage(i_01_arrow_l, 258 + focus * 141 + x, 186, c);
		}
		if (focus < ui.optionLen() - 1) {
			g.drawImage(i_01_arrow_r, 403 + focus * 141 + x, 186, c);
		}
		
		// remove in R7 rebrand
//		if (season.is3screen()) {
//			g.drawImage(i_icon_3scr_v, 150 + 47, 367, c);
//		}

		// information, description
		if (infoTitle[ui.getOptions()[focus]] != null) {
			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(Color.white);
			g.drawString(infoTitle[ui.getOptions()[focus]], 240 + 47, 378);
			g.setFont(FontResource.BLENDER.getFont(16));
			g.setColor(c200);
			g.drawString(contentCnt[ui.getOptions()[focus]], 240 + 47, 397);
			x = 240 + g.getFontMetrics().stringWidth(contentCnt[ui.getOptions()[focus]]) + 10 + 47;
			g.drawImage(slashClar, x, 397 - 11, c);
			x = x + slashClar.getWidth(c) + 10;
			paintAdditionalInfo(g, season, false, true, false, x, 397, c);
		}
	}
}

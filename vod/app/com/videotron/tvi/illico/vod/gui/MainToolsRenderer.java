package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.MainToolsUI;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Network;
import com.videotron.tvi.illico.vod.data.vcds.type.ShowcaseContainer;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

public class MainToolsRenderer extends BaseRenderer {
	private Rectangle rightBounds = new Rectangle(320, 80, 600, 400);
	private Image i01Tooliconbg;// = dataCenter.getImage("01_tooliconbg.png");
	private Image i01Resumeicon;// = dataCenter.getImage("01_resumeicon.png");
	private Image i01Searchicon;// = dataCenter.getImage("01_searchicon.png");
	private Image i01Optionicon;// = dataCenter.getImage("01_optionicon.png");
	private Image i01Adulticon;// = dataCenter.getImage("01_adulticon.png");
	private Image i01AdultPoster;
	private Image toolIcon;
	private Image blockedPortrait;// = dataCenter.getImage("01_poster_block.png");
	private Image blockedLandscape;// = dataCenter.getImage("01_poster_block_h.png");
	private Image arrLeft;// = dataCenter.getImage("01_arrow_l.png");
	private Image arrRight;// = dataCenter.getImage("01_arrow_r.png");
//	private Image i_01_karash_513;// = dataCenter.getImage("01_karash_513.png");
	private Image i_05_focus_dim;// = dataCenter.getImage("05_focus_dim.png");
	private Image i_05_focus;// = dataCenter.getImage("05_focus.png");
	private Image i_00_focus_dim;// = dataCenter.getImage("05_focus_dim.png");
	private Image i_00_focus;// = dataCenter.getImage("05_focus.png");
//	private Image i_01_possh_s_l;// = dataCenter.getImage("01_possh_s_l.png");
//	private Image i_01_poshigh_s_l;// = dataCenter.getImage("01_poshigh_s_l.png");
//	private Image i_01_possh_s;// = dataCenter.getImage("01_possh_s.png");
//	private Image i_01_poshigh_s;// = dataCenter.getImage("01_poshigh_s.png");
//	private Image i_01_possh_b;// = dataCenter.getImage("01_possh_s.png");
//	private Image i_01_poshigh_b;// = dataCenter.getImage("01_poshigh_s.png");
	private Image i_02_icon_isa_bl;
	// R5
	private Image i01WishIcon, iLittleBoyBig;
	// R7.3
    private Image i_OK;
	
	private Font fTitle = FontResource.BLENDER.getFont(28);
	private Font fMsgFirst = FontResource.BLENDER.getFont(20);
	private Font fMsgSecond = FontResource.BLENDER.getFont(17);
	private Font fMsgAdult = FontResource.BLENDER.getFont(19);
	private Font fMsgNote = FontResource.BLENDER.getFont(16);

	public FontMetrics fmMsgFirst = FontResource.getFontMetrics(fMsgFirst);
	public FontMetrics fmMsgSecond = FontResource.getFontMetrics(fMsgSecond);
	public FontMetrics fmMsgAdult = FontResource.getFontMetrics(fMsgAdult);
	public FontMetrics fmMsgNote = FontResource.getFontMetrics(fMsgNote);

	private Color cTitle = new Color(187, 189, 192);
	private Color cMsgFirst = new Color(219, 219, 219);
	private Color cMsgSecond = new Color(179, 179, 179);
	private Color c3 = new Color(3, 3, 3);
	private Color c65 = new Color(65, 65, 65);

	private String[] message1;
	private String[] message2;
	private String[] message3;

	private int resumeLength;
	private MoreDetail[] data;
	private ShowcaseContainer[] showcases; // for karaoke
	private boolean isAdult;
	
	int index = 0;
	
	public Point arrowL = new Point();
	public Point arrowR = new Point();

	public Rectangle getPreferredBounds(UIComponent c) {
		return rightBounds;
	}

	public void prepare(UIComponent c) {
		i01Tooliconbg = dataCenter.getImage("01_tooliconbg.png");
		i01Resumeicon = dataCenter.getImage("01_resumeicon.png");
		i01Searchicon = dataCenter.getImage("01_searchicon_2.png");
		i01Optionicon = dataCenter.getImage("01_optionicon.png");
		i01Adulticon = dataCenter.getImage("01_adulticon.png");
		i01AdultPoster = dataCenter.getImage("01_adult_poster.png");
		blockedPortrait = dataCenter.getImage("01_poster_block.jpg");
		blockedLandscape = dataCenter.getImage("01_poster_block_h.jpg");
		arrLeft = dataCenter.getImage("02_ars_l.png");
		arrRight = dataCenter.getImage("02_ars_r.png");
//		i_01_karash_513 = dataCenter.getImage("01_karash_513.png");
		i_05_focus_dim = dataCenter.getImage("05_focus_dim.png");
		i_05_focus = dataCenter.getImage("05_focus.png");
		i_00_focus_dim = dataCenter.getImage("00_focus_dim.png");
		i_00_focus = dataCenter.getImage("00_focus.png");
//		i_01_possh_s_l = dataCenter.getImage("01_possh_s_l.png");
//		i_01_poshigh_s_l = dataCenter.getImage("01_poshigh_s_l.png");
//		i_01_possh_s = dataCenter.getImage("01_possh_s.png");
//		i_01_poshigh_s = dataCenter.getImage("01_poshigh_s.png");
//		i_01_possh_b = dataCenter.getImage("01_possh_b.png");
//		i_01_poshigh_b = dataCenter.getImage("01_poshigh_b.png");
		i_02_icon_isa_bl = dataCenter.getImage("02_icon_isa_bl.png");
		
		// R5
		i01WishIcon = dataCenter.getImage("01_wishlisticon.png");
		iLittleBoyBig = dataCenter.getImage("b_btn_pro_big.png");

		// R7.3
        Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        i_OK = (Image) imgTable.get(PreferenceService.BTN_OK);
		
		super.prepare(c);
	}

	public MoreDetail[] getData() {
		return data;
	}

	public void setData(CategoryContainer category) {
		String m1 = "";
		String m2 = "";
		toolIcon = null;
		data = null;
		isAdult = false;
		if (category.type == MenuController.MENU_WISHLIST
				|| category.type == MenuController.MENU_WISHLIST_ADULT) {
			toolIcon = i01WishIcon;
			
			String[] wishTitles;
			
			BookmarkManager.getInstance().organize();
			if (category.type == MenuController.MENU_WISHLIST) {
				wishTitles = BookmarkManager.getInstance().retrieveWishlistTitles(true);
			} else {
				wishTitles = BookmarkManager.getInstance().retrieveWishlistTitlesAdult();
			}
			
			resumeLength = 0;
			if (wishTitles != null) {
				resumeLength = wishTitles.length;
			}
			
			switch (resumeLength) {
			case 0:
				if (category.type == MenuController.MENU_WISHLIST) {
					m1 = dataCenter.getString(Resources.TEXT_MSG_WISHLIST);
					m2 = "";
				} else {
					m1 = dataCenter.getString(Resources.TEXT_MSG_WISHLIST_ADULT_1);
					m2 = dataCenter.getString(Resources.TEXT_MSG_WISHLIST_ADULT_2);
				}
				break;
			case 1:
				if (category.type == MenuController.MENU_WISHLIST) {
					m1 = resumeLength + " " + dataCenter.getString(Resources.TEXT_MSG_WISHLIST_NO1);
					m2 = "";
				} else {
					m1 = resumeLength + " " + dataCenter.getString(Resources.TEXT_MSG_WISHLIST_ADULT_NO1);
					m2 = "";
				}
				break;
			default:
				if (category.type == MenuController.MENU_WISHLIST) {
					m1 = resumeLength + " " + dataCenter.getString(Resources.TEXT_MSG_WISHLIST_NO2);
					m2 = "";
				} else {
					m1 = resumeLength + " " + dataCenter.getString(Resources.TEXT_MSG_WISHLIST_ADULT_NO2);
					m2 = "";
				}
				break;
			}
			setWishlistData(wishTitles);
		} else if (category.type == MenuController.MENU_RESUME 
				|| category.type == MenuController.MENU_RESUME_ADULT) {
			toolIcon = i01Resumeicon;
			Bookmark[] bookmarks = BookmarkManager.getInstance()
					.getActivePurchaseForRight(category.type == MenuController.MENU_RESUME_ADULT);
			if (bookmarks == null) {
				bookmarks = new Bookmark[0];
			}
			if (bookmarks != null) {
				resumeLength = BookmarkManager.getInstance().numberActivePurchase(category.type == MenuController.MENU_RESUME_ADULT);
				switch (resumeLength) {
				case 0:
					m1 = dataCenter.getString(Resources.TEXT_MSG_RESUME_0);
					m2 = "";
					break;
				case 1:
					m1 = dataCenter.getString(Resources.TEXT_MSG_RESUME_1_);
					m2 = dataCenter.getString(Resources.TEXT_MSG_RESUME_2_);
					break;
				default:
					m1 = dataCenter.getString(Resources.TEXT_MSG_RESUME_1);
					m1 = TextUtil.replace(m1, "0", String.valueOf(resumeLength));
					m2 = dataCenter.getString(Resources.TEXT_MSG_RESUME_2);
					break;
				}
				setResumeData(bookmarks);
			}
		} else if (category.type == MenuController.MENU_SEARCH) {
			toolIcon = i01Searchicon;
			m1 = dataCenter.getString(Resources.TEXT_MSG_SEARCH_1);
			m2 = dataCenter.getString(Resources.TEXT_MSG_SEARCH_2);
		} else if (category.type == MenuController.MENU_ABOUT) {
			toolIcon = null;
			m1 = "";//dataCenter.getString(Resources.TEXT_MSG_ABOUT_CH_1);
			Channel ch = CategoryUI.getChannel();
			if (ch != null) {
				m2 = ch.getDescription();
				if (ch.network != null) {
					m2 = ch.network.getDescription();
				}
			} else {
				m2 = "";
			}
		} else if (category.type == MenuController.MENU_KARAOKE) {
			// party pack, free trial
			showcases = category.getTopic().treeRoot.showcaseContainer;
		} else if (category.isAdult) {
			
			if (MenuController.getInstance().getHistorySize() == 0) {
				m1 = dataCenter.getString(Resources.TEXT_MSG_ADULT_TITLE);
				m2 = dataCenter.getString(Resources.TEXT_MSG_ADULT_NOTE);
				isAdult = true;
				
				message1 = TextUtil.split(m1, fmMsgAdult, 300);
				message2 = TextUtil.split(m2, fmMsgNote, 300);
				
				ArrayList list = new ArrayList();
				for (int i = 0; i < 3; i++) {
					String[] temp = TextUtil.split(dataCenter.getString(Resources.TEXT_MSG_ADULT_DESC + i), fmMsgAdult, 250);
					list.add("");
					for (int j = 0; j < temp.length; j++) {
						list.add(temp[j]);
					}
				}
				message3 = new String[list.size()];
				list.toArray(message3);
			} else {
				toolIcon = i01Adulticon;
				m1 = dataCenter.getString(Resources.TEXT_MSG_TORRID_1);
				m2 = dataCenter.getString(Resources.TEXT_MSG_TORRID_2);
			}
		}
		if (isAdult == false) {
			message1 = TextUtil.split(m1, fmMsgFirst, 440, "|");
			message2 = TextUtil.split(m2, fmMsgSecond, 440, "|");
		}
		FrameworkMain.getInstance().getImagePool().waitForAll();
	}

	private void setResumeData(Bookmark[] bookmarks) {
		index = 0;
		data = new MoreDetail[bookmarks.length];
		BaseElement[] forPoster = new BaseElement[Math.min(5, bookmarks.length)];
		for (int i = 0; i < data.length && i < forPoster.length; i++) {
			Object cached = CatalogueDatabase.getInstance().getCachedBySspId(
					bookmarks[i].getOrderableName());
			data[i] = (MoreDetail) cached;
			if (i < forPoster.length) {
				forPoster[i] = data[i];
//				Log.printDebug("MainTools, setResumeData, poster[i] = " + data[i].toString(true));
			} 
		}

		CatalogueDatabase.getInstance().retrieveImages(
				(BaseUI) MenuController.getInstance().getCurrentScene(),
				forPoster);
	}
	
	// R5
	private void setWishlistData(String[] wishData) {
		if (Log.DEBUG_ON) {
			Log.printDebug("MainToolsRenderer, setWishlistData(String[]), wishData=" + wishData);
		}
		
		if (wishData == null) {
			return;
		}
		
		index = 0;
		data = new MoreDetail[wishData.length];
		BaseElement[] forPoster = new BaseElement[Math.min(5, wishData.length)];
		for (int i = 0; i < data.length && i < forPoster.length; i++) {
			Object cached = CatalogueDatabase.getInstance().getCachedBySspId(
					wishData[i]);
			data[i] = (MoreDetail) cached;
			if (i < forPoster.length) {
				forPoster[i] = data[i];
//				Log.printDebug("MainTools, setResumeData, poster[i] = " + data[i].toString(true));
			} 
		}

		CatalogueDatabase.getInstance().retrieveImages(
				(BaseUI) MenuController.getInstance().getCurrentScene(),
				forPoster);
	}

	protected void paint(Graphics g, UIComponent c) {
		g.translate(-rightBounds.x, -rightBounds.y);
		MainToolsUI ui = (MainToolsUI) c;
		g.setColor(cTitle);
		g.setFont(fTitle);
		CategoryContainer category = ((MainToolsUI) c).getCurrentCategory();
		if (category == null)
			return;
		// g.drawString(category.getTitle(), 375, 97);
		if (ui.getType() == MenuController.MENU_RESUME || ui.getType() == MenuController.MENU_RESUME_ADULT
				|| ui.getType() == MenuController.MENU_WISHLIST || ui.getType() == MenuController.MENU_WISHLIST_ADULT) {
			paintResume(g, c);
		} else if (isAdult) {
			g.drawImage(i01AdultPoster, 373, 128, c);
//			g.drawImage(i_01_poshigh_b, 373, 128, c);
//			g.drawImage(i_01_possh_b, 373, 446, c);
			
			g.setFont(fMsgAdult);
			g.setColor(cMsgFirst);
			for (int i = 0; i < message1.length; i++) {
				g.drawString(message1[i], 606, 145 + 22 * i);
			}
			int y = 0;
			int line = 0;
			for (int i = 0; i < message3.length; i++) {
				if (message3[i].length() == 0) {
					g.drawString("-", 626, 210 + 22 * line);
				} else {
					g.drawString(message3[i], 646, y = 210 + 22 * line);
					line++;
				}
			}
			y = 380;
			
			g.setFont(fMsgNote);
			g.setColor(cMsgSecond);
			for (int i = 0; i < message2.length; i++) {
				g.drawString(message2[i], 606, y + 17 * i);
			}
			
		} else if (toolIcon != null) {
			g.drawImage(i01Tooliconbg, 508, 119, c);
			g.drawImage(toolIcon, 560, 168, c);
		} else if (ui.getType() == MenuController.MENU_KARAOKE_PACK || ui.getType() == MenuController.MENU_KARAOKE) {
			g.setColor(c3);
			g.setFont(FontResource.BLENDER.getFont(18));
			if (CategoryUI.getPurchasedPartyPack() != null) { // draw subscribed large banner
				g.drawImage(((CategoryUI) c.getParent()).getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT ? i_05_focus : i_05_focus_dim, 552, 425, c);
				GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString(Resources.TEXT_SELECT), 631, 446);
				
				g.setColor(Color.white);
				g.setFont(FontResource.DINMED.getFont(15));
				GraphicUtil.drawStringCenter(g, CategoryUI.getPartyPackString(), 631, 390);
				
			} else { // draw separated banner
				g.drawImage(((CategoryUI) c.getParent()).getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT && c.getFocus() == 0 ? i_05_focus : i_05_focus_dim, 382, 425, c);
				GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString(Resources.TEXT_TRY_NOW), 461, 446);
				g.drawImage(((CategoryUI) c.getParent()).getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT && c.getFocus() == 1 ? i_05_focus : i_05_focus_dim, 562, 425, c);
				GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString(Resources.TEXT_ORDER), 641, 446);
				g.drawImage(((CategoryUI) c.getParent()).getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT && c.getFocus() == 2 ? i_05_focus : i_05_focus_dim, 722, 425, c);
				GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString(Resources.TEXT_PREVIEW), 801, 446);
			}
		} else if (ui.getType() == MenuController.MENU_ABOUT) {
			Channel ch = CategoryUI.getChannel();
			Image logo = null;
			if (ch != null) {
				if (ch.network != null) {
					logo = ch.network.logos[Network.LOGO_LG];
				} else {
					logo = ch.logos[Channel.LOGO_LG];
				}
			}
			if (logo != null) {
				g.drawImage(logo, 503, 154, c);
			}
		}
		g.setColor(cMsgFirst);
		g.setFont(fMsgFirst);
		int gap = 20;
		int yPos = 363;
		int maxLine = 8;
		if ((ui.getType() == MenuController.MENU_RESUME 
				|| ui.getType() == MenuController.MENU_RESUME_ADULT
				|| ui.getType() == MenuController.MENU_WISHLIST
				|| ui.getType() == MenuController.MENU_WISHLIST_ADULT) 
			&& resumeLength > 0) {
			yPos = 180;
		} else if (ui.getType() == MenuController.MENU_ABOUT) {
//			yPos = Math.min(370, 490 - gap * Math.min(maxLine, message2.length));
			maxLine = 5;
			yPos = 268;
		}
		
		int pkPos = 0;
		for (int i = 0; isAdult == false && message1 != null && i < message1.length; i++) {
			if (( pkPos = message1[i].indexOf("[PK]")) > -1) {
                String pre = message1[i].substring(0, pkPos);
                String sub = message1[i].substring(pkPos + 4);
                int tWidth = fmMsgFirst.stringWidth(message1[i]);
                int preWidth = fmMsgFirst.stringWidth(pre);
                int subWidth = fmMsgFirst.stringWidth(sub);
                g.drawString(pre, 632 - tWidth / 2, yPos);
                g.drawString(sub, 632 + tWidth / 2 - subWidth, yPos);

                g.drawImage(iLittleBoyBig, 632 - tWidth / 2 + preWidth + 5, yPos - 17, c);
            } else if (( pkPos = message1[i].indexOf("[OK]")) > -1) { // R7.3
                String pre = message1[i].substring(0, pkPos);
                String sub = message1[i].substring(pkPos + 4);
                int tWidth = fmMsgFirst.stringWidth(message1[i]);
                int preWidth = fmMsgFirst.stringWidth(pre);
                int subWidth = fmMsgFirst.stringWidth(sub);
                g.drawString(pre, 632 - tWidth / 2, yPos);
                g.drawString(sub, 632 + tWidth / 2 - subWidth, yPos);

                g.drawImage(i_OK, 632 - tWidth / 2 + preWidth + 5, yPos - 17, c);
			} else {
				GraphicUtil.drawStringCenter(g, message1[i], 632, yPos);
			}
			yPos += gap;
//			GraphicUtil.drawStringCenter(g, message1[i], 636, yPos);
		}
		g.setColor(cMsgSecond);
		g.setFont(fMsgSecond);
		if (ui.getType() == MenuController.MENU_ABOUT) {
			g.setColor(Color.white);
		}
		for (int i = 0; isAdult == false && i < maxLine && message2 != null && i < message2.length; i++) {
			if (message2.length > maxLine && i == maxLine - 1) {
				GraphicUtil.drawStringCenter(g, message2[i] + " ...", 632, yPos);
			} else {
				GraphicUtil.drawStringCenter(g, message2[i], 632, yPos);
			}
			yPos += gap;
//			GraphicUtil.drawStringCenter(g, message2[i], 636, yPos);
		}
		if (ui.getType() == MenuController.MENU_ABOUT) {
			g.drawImage(((CategoryUI) c.getParent()).getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT && c.getFocus() == 0 ? i_00_focus : i_00_focus_dim, 504, 370, c);
			boolean icon = false;
			
			Channel ch = CategoryUI.getChannel();
			String btn = "";
			if (ch != null) {
				if (ch.hasAuth) {
					btn = DataCenter.getInstance().getString(Resources.TEXT_TUNE_TO);
				} else if (CommunicationManager.getInstance().isaSupported(ch.callLetters)
						|| !CommunicationManager.getInstance().isIncludedInExcludedData(ch.callLetters)) {
					icon = true;
					btn = DataCenter.getInstance().getString(Resources.TEXT_SUBSCRIBE_NOW);
				} else {
					btn = DataCenter.getInstance().getString(Resources.TEXT_CHANGE_MY_PKG);
				}
			}
			g.setFont(FontResource.BLENDER.getFont(18));
			int pos = FontResource.getFontMetrics(g.getFont()).stringWidth(btn);
			if (icon) {
				pos += i_02_icon_isa_bl.getWidth(null) + 5;
				g.drawImage(i_02_icon_isa_bl, pos = 633 - pos / 2, 376, c);
				pos += i_02_icon_isa_bl.getWidth(null) + 5;
			} else {
				pos = 633 - pos / 2;
			}
			g.setColor(c3);
			g.drawString(btn, pos, 391);
			
			// VDTRMASTER-5201 [UI/GUI]][PO1/2/3][ [R4] [ISA] [VOD] URL and phone number information should not be displayed for subscribed channels 
			if (ch != null && !ch.hasAuth) {
				g.setFont(FontResource.BLENDER.getFont(20));
				g.setColor(Color.white);
				GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString(Resources.TEXT_MSG_OR2), 405, 438);
				GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString(Resources.TEXT_MSG_OR), 713, 438);
				
				g.setFont(FontResource.BLENDER.getFont(16));
	//			g.setColor(Color.darkGray);
	//			g.drawString(DataCenter.getInstance().getString(Resources.TEXT_MSG_VISIT), 430, 437);
	//			g.drawString(DataCenter.getInstance().getString(Resources.TEXT_MSG_DIAL), 738, 437);
				g.setColor(cMsgSecond);
				g.drawString(DataCenter.getInstance().getString(Resources.TEXT_MSG_VISIT), 429, 436);
				g.drawString(DataCenter.getInstance().getString(Resources.TEXT_MSG_DIAL), 737, 436);
			}
		}
		
		g.translate(rightBounds.x, rightBounds.y);
	}

	static final int LEFT_X = 373;
	static final int TOP_Y_P = 257;
	static final int TOP_Y_L = 298;

	private void paintResume(Graphics g, UIComponent c) {
		if (resumeLength == 0) {
			g.drawImage(i01Tooliconbg, 508, 119, c);
			g.drawImage(toolIcon, 560, 168, c);
		} else {
			Color cTitle = new Color(255, 203, 000);
			CategoryUI ui = (CategoryUI) c.getParent();
			boolean isFocus = ui.getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT;
			String[] splitedTitle;
			boolean focused;

			int maxCnt;
			boolean addBtn = false;
			int gap = 16;
			boolean[] portraits = new boolean[5];
			boolean allPortrait = true;
			for (int i = 0; i < portraits.length && i < data.length; i++) {
				portraits[i] = data[i].isPortrait();
				if (portraits[i] == false) {
					allPortrait = false;
					break;
				}
			}

			if (data.length <= 4) {
				maxCnt = data.length; // it's okay up to 4
			} else {
				if (allPortrait && data.length == 5) {
					maxCnt = 5;
				} else {
					maxCnt = 3;
					addBtn = true;
				}
			}
			boolean allLandscape = true;
			for (int i = 0; i < maxCnt; i++) {
				if (portraits[i]) {
					allLandscape = false;
					break;
				}
			}
			if (allLandscape) {
				gap = 11;
			}
			((MainToolsUI) c).setMaxCnt(addBtn ? maxCnt + 1 : maxCnt);
			((MainToolsUI) c).setSeeAll(addBtn);

			int x = LEFT_X, y = 0, w = 0, h = 0;
			Rectangle focusedRect = new Rectangle();
			for (int i = 0; i < maxCnt; i++) {
				boolean portrait = data[i].isPortrait();
				w = portrait ? ImageRetriever.PR_SM.width : ImageRetriever.LD_SM.width;
				h = portrait ? ImageRetriever.PR_SM.height : ImageRetriever.LD_SM.height;
				y = portrait ? TOP_Y_P : TOP_Y_L;

				boolean isAdultBlocked = MenuController.getInstance()
						.isAdultBlocked(data[i]);
				Image poster = null;
				if (isAdultBlocked) {
					poster = data[i].isPortrait() ? blockedPortrait : blockedLandscape;
				} else {
					poster = data[i].getImage(w, h);
				}
				if (poster != null) {
					g.drawImage(poster, x, y, w, h, ui);
				}
				if (w == ImageRetriever.PR_SM.width && h == ImageRetriever.PR_SM.height) {
//					g.drawImage(i_01_poshigh_s, x, y, ui);
//					g.drawImage(i_01_possh_s, x, y + h, ui);
				} else if (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) {
//					g.drawImage(i_01_poshigh_s_l, x, y, ui);
//					g.drawImage(i_01_possh_s_l, x, y + h, ui);
				}

				focused = c.getFocus() == i;
				g.setFont(FontResource.BLENDER.getFont(16));
				if (isFocus && focused) {
					g.setColor(cTitle);
					focusedRect.setBounds(x, y, w, h);
				} else {
					g.setColor(Color.white);
				}

				String title = data[i].getTitle();
				if (isAdultBlocked) {
					splitedTitle = new String[] { DataCenter.getInstance()
							.getString(Resources.TEXT_LBL_BLOCKED_TITLE) };
					title = splitedTitle[0];
				} else {
//					splitedTitle = TextUtil.split(title, g.getFontMetrics(),
//							w - 4, 2);
//					if (splitedTitle.length > 1) {
//						splitedTitle[1] = TextUtil.shorten(splitedTitle[1],
//								g.getFontMetrics(), w);
//					}
					splitedTitle = new String[] {
							TextUtil.shorten(title, g.getFontMetrics(), w)
					};
				}
				if (isFocus && focused) {
//					GraphicUtil.drawStringCenter(g, splitedTitle[0], x + w / 2,
//							y + h + 16);
//					if (splitedTitle.length > 1) {
//						GraphicUtil.drawStringCenter(g, splitedTitle[1], x + w
//								/ 2, y + h + 16 + 12);
//					}
					if (g.getFontMetrics().stringWidth(title) > w) {
						int cnt = Math.max(ui.scroll - 10, 0);
						Rectangle ori = g.getClipBounds();
						ui.scrollBounds.x = x;
						ui.scrollBounds.y = y + h;
						ui.scrollBounds.width = w;
						ui.scrollBounds.height = 20;
						g.clipRect(x, y + h, w, 20);
						try {
							g.drawString(title.substring(cnt), x + 2, y + h + 16);
						} catch (IndexOutOfBoundsException e) {
							ui.scroll = 0;
						} 
						g.setClip(ori);
					} else {
						ui.scrollBounds.width = 0;
						GraphicUtil.drawStringCenter(g, title, x + w / 2, y + h + 16);
					}
				} else {
					g.drawString(splitedTitle[0], x + 2, y + h + 16);
					if (splitedTitle.length > 1) {
						g.drawString(splitedTitle[1], x + 2, y + h + 16 + 12);
					}
				}
				x += w + gap;
			} // end of for

			if (addBtn) {
				int seeAllX, seeAllY;
				g.setColor(c65);
				g.fillRect(x, y, w, h);
				if (w == ImageRetriever.PR_SM.width) {
					seeAllX = 38;
					seeAllY = 39;
				} else {
					seeAllX = 55;
					seeAllY = 11;
				}
				g.drawImage(arrRight, x + seeAllX, y + seeAllY, c);
				g.setFont(FontResource.BLENDER.getFont(17));
				String seeAllStr = DataCenter.getInstance().getString(
						Resources.TEXT_MSG_SEE)
						+ " "
						+ DataCenter.getInstance().getString(
								Resources.TEXT_MSG_ALL);
				int seeAllW = g.getFontMetrics().stringWidth(seeAllStr);
				String seeStr = DataCenter.getInstance().getString(
						Resources.TEXT_MSG_SEE)
						+ " ";
				int seeW = g.getFontMetrics().stringWidth(seeStr);
				String allStr = DataCenter.getInstance().getString(
						Resources.TEXT_MSG_ALL);
				String titlesStr = DataCenter.getInstance().getString(
						Resources.TEXT_MSG_TITLES);
				g.setColor(cTitle);
				g.drawString(seeStr, x + w / 2 - seeAllW / 2, y + seeAllY + 25
						+ 15);
				g.setColor(Color.white);
				GraphicUtil.drawStringCenter(g, titlesStr, x + w / 2, y
						+ seeAllY + 25 + 30);
				g.drawString(allStr, x + w / 2 - seeAllW / 2 + seeW, y
						+ seeAllY + 25 + 15);
				if (w == ImageRetriever.PR_SM.width && h == ImageRetriever.PR_SM.height) {
//					g.drawImage(i_01_poshigh_s, x, y, ui);
//					g.drawImage(i_01_possh_s, x, y + h, ui);
				} else if (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) {
//					g.drawImage(i_01_poshigh_s_l, x, y, ui);
//					g.drawImage(i_01_possh_s_l, x, y + h, ui);
				}
				if (isFocus && c.getFocus() == maxCnt) {
					focusedRect.setBounds(x, y, w, h);
				}
			}

			if (isFocus) { // draw focus rim
				g.setColor(cTitle);
				g.drawRect(focusedRect.x - 1, focusedRect.y - 1,
						focusedRect.width + 1, focusedRect.height + 1);
				g.drawRect(focusedRect.x - 2, focusedRect.y - 2,
						focusedRect.width + 3, focusedRect.height + 3);
				g.drawRect(focusedRect.x - 3, focusedRect.y - 3,
						focusedRect.width + 5, focusedRect.height + 5);

				if (true) {
					arrowL.x = focusedRect.x - arrLeft.getWidth(c) - 1;
					arrowL.y = focusedRect.y + focusedRect.height / 2 - arrLeft.getHeight(c) / 2;
					g.drawImage(arrLeft,
							arrowL.x,
							arrowL.y,
//							focusedRect.x - arrLeft.getWidth(c)
//							- 1, focusedRect.y + focusedRect.height / 2
//							- arrLeft.getHeight(c) / 2, 
							c);
				}
				if (c.getFocus() < maxCnt - 1) {
					arrowR.x = focusedRect.x + focusedRect.width + 1;
					arrowR.y = focusedRect.y + focusedRect.height / 2 - arrRight.getHeight(c) / 2;
					g.drawImage(
							arrRight,
							arrowR.x,
							arrowR.y,
//							focusedRect.x + focusedRect.width + 1,
//							focusedRect.y + focusedRect.height / 2
//									- arrRight.getHeight(c) / 2, 
									c);
				} else {
					arrowR.x = -1;
				}
			}
		} // end of if (resume is exists)
	}
}

package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Showcase;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.ListViewUI;

public class ListViewRenderer extends BaseRenderer {

	private Image bg;// = dataCenter.getImage("bg.jpg");
	private Image i02_icon_con;// = dataCenter.getImage("02_icon_con.png");
	private Image blockedPortrait;// = dataCenter.getImage("01_poster_block.png");
	private Image blockedLandscape;// = dataCenter.getImage("01_poster_block_h.png");
	// remove in R7 rebrand
//	private Image i_icon_3scr_v;// = dataCenter.getImage("icon_3scr_v.png");
//	private Image i_01_possh_s_l;// = dataCenter.getImage("01_possh_s_l.png");
//	private Image i_01_poshigh_s_l;// = dataCenter.getImage("01_poshigh_s_l.png");
//	private Image i_01_possh_s;// = dataCenter.getImage("01_possh_s.png");
//	private Image i_01_poshigh_s;// = dataCenter.getImage("01_poshigh_s.png");
	// R5
	private Image i_icon_favorite, i_cinoche_cote, i_icon_uhd;
	
	private Color cTitle = new Color(255, 203, 0);
	private Color cOrderEnd = new Color(225, 195, 80);
	private Color cLblPerson = new Color(182, 182, 182);
	private Color c227_227_228 = new Color(227, 227, 228);
	private Color c255_a26 = new DVBColor(255, 255, 255, 26);

	
	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return bgRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		bg = dataCenter.getImage("bg.jpg");
		i02_icon_con = dataCenter.getImage("02_icon_con.png");
		blockedPortrait = dataCenter.getImage("01_poster_block.jpg");
		blockedLandscape = dataCenter.getImage("01_poster_block_h.jpg");
		// remove in R7 rebrand
//		i_icon_3scr_v = dataCenter.getImageByLanguage("icon_3scr_v.png");
//		i_01_possh_s_l = dataCenter.getImage("01_possh_s_l.png");
//		i_01_poshigh_s_l = dataCenter.getImage("01_poshigh_s_l.png");
//		i_01_possh_s = dataCenter.getImage("01_possh_s.png");
//		i_01_poshigh_s = dataCenter.getImage("01_poshigh_s.png");
		// R5
		i_icon_favorite = dataCenter.getImage("02_icon_fav.png");
		i_cinoche_cote = dataCenter.getImage("cinoche_cote.png");
		i_icon_uhd = dataCenter.getImage("icon_uhd.png");
		
		setBackGround(bg);
		
		super.prepare(c);
	}

	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		ListViewUI ui = (ListViewUI) c;
		MoreDetail title = (MoreDetail) ui.getCurrentCatalogue();
		if (title == null) {
			return;
		}
		MoreDetail titleInExtra = null;
		if (title instanceof Extra) {
			titleInExtra = (MoreDetail) CatalogueDatabase.getInstance().getCached(((Extra) title).extraDetail.mainContentItem);
		}
		
		int yGap = 52;

		int sort = ui.getSortStatus();
		if (sort > 0) {
			g.setFont(FontResource.BLENDER.getFont(17));
			g.setColor(c227_227_228);
			String str = "";
			switch (sort) {
			case 1:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_NAME);
				break;
			case 2:
				if (ui.isWishList()) {
					str = dataCenter
							.getString(Resources.TEXT_LBL_WISHLIST_ORDER);
				} else if (ui.isResumeViewing()) {
					str = dataCenter
							.getString(Resources.TEXT_LBL_RESUMEVIEWING_ORDER);
				} else {
					str = dataCenter.getString(Resources.TEXT_LBL_SORT_DATE);
				}
				break;
			case 3:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_PROD);
				break;
			case 4:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_DEFAULT);
				break;
			}
			GraphicUtil.drawStringRight(g, str, 486, 109);
		}

		boolean adultBlocked = MenuController.getInstance().isAdultBlocked(
				title);
		boolean portrait = title.isPortrait();

		// movie title
		boolean ageBlocked = adultBlocked
				|| MenuController.getInstance().isBlocked(title);
		int width = ageBlocked ? 350 : 370;
		int x = 517;
		g.setFont(FontResource.BLENDER.getFont(24));
		g.setColor(cTitle);
		
		
		String sourceTitle = title.getTitle();
		// VDTRMASTER-5307
		if (Log.DEBUG_ON) {
			if (title instanceof Season) {
				Log.printDebug("PosterArrayRenderer, fromCategory=" + ui.fromCategory() + 
						", season.getLongTitle()=" + ((Season) title).getLongTitle());
			}
		}
		if (title instanceof Season && ui.fromCategory() && !((Season) title).getLongTitle().equals("N/A")) {
			sourceTitle = TextUtil.shorten(((Season) title).getLongTitle(), g.getFontMetrics(), 750);
		}
		String titleStr[] = adultBlocked ? new String[] { dataCenter
				.getString(Resources.TEXT_LBL_BLOCKED_TITLE) }
				: TextUtil.split(sourceTitle, g.getFontMetrics(), width);
		if (ageBlocked && (ui.isWishList() || !MenuController.getInstance().needToDisplayFavorite())) {
			g.drawImage(i02_icon_con, x, 111, ui);
			x += i02_icon_con.getWidth(ui);
		} else if (!ui.isWishList() && BookmarkManager.getInstance().isInWishlist(title) != null) {
			g.drawImage(i_icon_favorite, x, 111, ui);
			x += i_icon_favorite.getWidth(ui);
		}

		for (int i = 0; i < titleStr.length; i++) {
			g.drawString(titleStr[i], x, 129 + i * 28);
		}
		int y = titleStr.length > 1 ? 28 : 0;
		
		// poster area
		Image poster = null;
		int w = portrait ? ImageRetriever.PR_SM.width : ImageRetriever.LD_SM.width;
		int h = portrait ? ImageRetriever.PR_SM.height : ImageRetriever.LD_SM.height;
		if (adultBlocked) {
			poster = portrait ? blockedPortrait : blockedLandscape;
		} else {
			poster = titleInExtra == null ? title.getImage(w, h) : titleInExtra.getImage(w, h);
		}
		x = 517;
		if (poster != null) {
			g.drawImage(poster, x, 144 + y, w, h, c);
		}
		if (adultBlocked == false) {
			Image flag = null;
			switch (title.getFlag()) {
			case 1: // new
				flag = dataCenter.getImageByLanguage("icon_new.png");
				break;
			case 2: // last chance
				flag = dataCenter.getImageByLanguage("icon_last.png");
				break;
			case 3: // extras
				flag = dataCenter.getImageByLanguage("icon_extras.png");
				break;
			}
			if (flag != null) {
				g.drawImage(flag, x + w - flag.getWidth(ui), 144 + y, ui);
			}
			
			// R5 - UHD icon
			Offer hdOffer = title.getOfferHD(null);
			if (hdOffer != null) {
				boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
				if (uhd) {
					g.drawImage(i_icon_uhd, x, 144 + y + h - i_icon_uhd.getHeight(ui), ui);
				}
			}
		}
		if (w == ImageRetriever.PR_SM.width && h == ImageRetriever.PR_SM.height) {
//			g.drawImage(i_01_poshigh_s, x, 144 + y, c);
//			g.drawImage(i_01_possh_s, x, 144 + h + y, c);
		} else if (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) {
//			g.drawImage(i_01_poshigh_s_l, x, 144 + y, c);
//			g.drawImage(i_01_possh_s_l, x, 144 + h + y, c);
			g.translate(8, 0);
		}
		
		if (titleInExtra != null) {
			title = titleInExtra;
		}
		
		// R5 remove
//		boolean is3screen = (ui.isNormal() == false || title instanceof Series == false) && title.is3screen();

		// price
		if (ui.isResumeViewing() == false) {
//			if (ui.isWishList()) {
//				// available date
//				String availableDate = title.getEndPeriod();
//				if (availableDate.length() > 0) {
//					g.setFont(FontResource.BLENDER.getFont(16));
//					g.setColor(cOrderEnd);
//					availableDate = dataCenter.getString(Resources.TEXT_LBL_AVAILABLE)
//							+ " " + availableDate;
//					g.drawString(availableDate, portrait ? 619
//							: 641, 155 + y);
//				}
////				y += 20;
//			}
			int r = portrait ? 770: 795;
//			if (Resources.currentLanguage == Resources.LANGUAGE_FR) {
//				r += 15;
//			}
			try {
				paintOffer(g, title, portrait ? 626 : 651, 156 + y, r, 
						ui.isWishList() && title instanceof Video && ((Video) title).inBundleOnly, c);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// R5 remove
//			if (is3screen) {
//				g.drawImage(i_icon_3scr_v, portrait ? 789 : 811, 144 + y, c);
//			}
		} else {
//			if (is3screen) {
//				g.drawImage(i_icon_3scr_v, portrait ? 618 : 640, 144 + y, c);
//			} else {
//				g.translate(0, -yGap);
//			}
		}

		boolean isVideo = false;
		if (title instanceof Episode || title instanceof Video) {
			isVideo = true;
		}
		if (title instanceof CategorizedBundle == false) {
			// grenre & year
			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(Color.white);
			String str = "";
			String genreStr = title.getGenre();
			if (genreStr != null) {
				if (g.getFontMetrics().stringWidth(genreStr) > 202) {
					genreStr = TextUtil.shorten(genreStr, g.getFontMetrics(), 202);
				}
				str += genreStr;
			}
			String yearStr = title.getProductionYear();
			if (yearStr != null && !yearStr.equals(TextUtil.EMPTY_STRING)) {
				str += (" (" + yearStr + ")");
			}
			g.drawString(str, portrait ? 626 : 651, 213 + y);
			
			paintAdditionalInfoNoIcon(g, title, isVideo, false, isVideo, portrait ? 626
					: 651, 233 + y, c);
			paintAdditionalInfoIcon(g, title, portrait ? 626 : 651, 243 + y, c);
	
			// R5
			g.setColor(c255_a26);
			g.fillRect(portrait ? 626 : 651, (portrait ? 278 : 268) + y, portrait ? 283 : 259, 1);
			// Actor, Director
			String person = title.getActors(3);
			if (person.length() > 0) {
				g.setFont(FontResource.DINMED.getFont(15));
				g.setColor(cLblPerson);
				String[] actors = TextUtil.split(person, g.getFontMetrics(), portrait ? 283 : 259, 2);
				if (actors != null) {
					for (int i = 0; i < actors.length; i++) {
						g.drawString(actors[i], portrait ? 627 : 651, (portrait ? 303 : 293) + i * 17 + y);
					}
				}
			}
			
			// cinoche cote
			if (title instanceof Video && ((Video) title).externalRef != null) {
				g.drawImage(i_cinoche_cote, 822, 147, c);
				g.setColor(Color.white);
				g.setFont(FontResource.DINMED.getFont(18));
				String value = ((Video) title).externalRef.value;
				g.drawString(value, 876, 161);
				int valueWidth = g.getFontMetrics().stringWidth(value) + 2;
				g.setFont(FontResource.DINMED.getFont(12));
				g.drawString(SLASH_FIVE, 876 + valueWidth, 161);
			}
				
//				String lbl = dataCenter.getString(Resources.TEXT_LBL_ACTOR);
//				if (isVideo && ((Video) title).isSpecialEvent()) {
//					lbl = dataCenter.getString(Resources.TEXT_LBL_WITH);
//				}
//				g.drawString(lbl,
//						portrait ? 620 : 642, 252 + y);
//				width = g.getFontMetrics().stringWidth(lbl);
//		
//				g.setFont(FontResource.DINMED.getFont(13));
//				g.setColor(Color.white);
//		
//				String str = TextUtil.shorten(person, g.getFontMetrics(),
//						(portrait ? 290 : 265) - width);
//				g.drawString(str, (portrait ? 630 : 652) + width, 253 + y);
//			}
		}
//		if (ui.isResumeViewing() && is3screen == false) {
//			g.translate(0, yGap);
//		}
		if (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) {
			g.translate(-8, 0);
		}
	}
}

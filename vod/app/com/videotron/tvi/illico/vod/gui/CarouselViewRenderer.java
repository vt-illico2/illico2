package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CarouselViewUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

public class CarouselViewRenderer extends BaseRenderer {

	private Image i01Carbg;// = dataCenter.getImage("bg.jpg");
	private Image i01VodBgline;// = dataCenter.getImage("01_vod_bgline.png");
	private Image i02_icon_con;// = dataCenter.getImage("02_icon_con.png");

	int index = 0;

	private Color cActor = new DVBColor(255, 255, 255, 230);
	private Color cInfoBg = new DVBColor(0, 0, 0, 128);

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		CarouselViewUI ui = (CarouselViewUI) c;
		
		i01Carbg = dataCenter.getImage("bg.jpg");
		i01VodBgline = dataCenter.getImage("01_vod_bgline.png");
		i02_icon_con = dataCenter.getImage("02_icon_con.png");
		
		setBackGround(i01Carbg);
		
		super.prepare(c);
	}

	/**
	 * Paints the UIComponent.
	 * 
	 * @param g
	 *            The graphics context to use for painting.
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 */
	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		// g.drawImage(i01VodBghigh, 0, 215, c);
//		g.drawImage(i01VodBgline, 0, 371, c);
		g.setColor(cInfoBg);
		g.fillRect(0, 444, 960, 38);
		
		g.setFont(FontResource.BLENDER.getFont(26));
		g.setColor(Color.white);
		String mainTitle = MenuController.getInstance().getLastHistory();
    	BaseUI last = MenuController.getInstance().getLastScene();
		Channel channel = null;
		if (last != null 
				&& last instanceof CategoryUI
				&& CategoryUI.isBranded()
				&& (channel = CategoryUI.getChannel()) != null) {
			
			if (channel.network != null) {
				mainTitle = channel.getLanguageContentData(channel.network.title) + " ≥ " + mainTitle;
			} else {
				mainTitle = channel.getLanguageContentData(channel.title) + " ≥ " + mainTitle;
			}
		} 
		g.drawString(mainTitle, 61, 98);
//		g.drawImage(i01VodBgline, 0, 250, c);

		CarouselViewUI ui = (CarouselViewUI) c;
		MoreDetail catalogue = (MoreDetail) ui.getCurrentCatalogue();

		int sort = ui.getSortStatus();
		if (sort > 0) {
			g.setFont(FontResource.BLENDER.getFont(18));
			String str = "";
			switch (sort) {
			case 1:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_NAME);
				break;
			case 2:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_DATE);
				break;
			case 3:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_PROD);
				break;
			case 4:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_DEFAULT);
				break;
			}
			GraphicUtil.drawStringRight(g, str, 909, 95);
		}

		if (ui.inAnimation() == false) {
//			// draw title
//			g.setColor(cTitle);
//			g.setFont(FontResource.BLENDER.getFont(23));
//			String titleStr = getTitleWithYearSingleLine(catalogue, g
//					.getFontMetrics(), 750);
//			boolean adultBlocked = MenuController.getInstance().isAdultBlocked(
//					catalogue);
//			if (adultBlocked) {
//				titleStr = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
//			}
//			int icon = GraphicUtil.drawStringCenter(g, titleStr, 480, 430);
//			boolean ageBlocked = adultBlocked
//					|| MenuController.getInstance().isBlocked(catalogue);
//			if (ageBlocked) {
//				g.drawImage(i02_icon_con, icon - i02_icon_con.getWidth(ui) - 4,
//						430 - 20, ui);
//			}
		}

		// draw index
		// g.setFont(fGenre);
		// g.setColor(cCurrent);
		// int curIndex = ui.getFocus() + 1;
		// GraphicUtil.drawStringRight(g, curIndex + " /",883, 431);
		// g.setColor(cTotal);
		// int total = catalogues.length;
		// GraphicUtil.drawStringRight(g, total + "", 902, 431);

		if (catalogue == null) {
			return;
		}
		if (catalogue instanceof Extra) {
			catalogue = (MoreDetail) CatalogueDatabase.getInstance().getCached(((Extra) catalogue).extraDetail.mainContentItem);
		}
		
		int x;
		int width = 0;
		
		boolean isVideo = catalogue instanceof Video;
		
		// genre and year
		String genreYearStr = "";
		if(isVideo) {
			
			String genreStr = catalogue.getGenre();
			if (genreStr != null) {
				if (g.getFontMetrics().stringWidth(genreStr) > 202) {
					genreStr = TextUtil.shorten(genreStr, g.getFontMetrics(), 202);
				}
				genreYearStr += genreStr;
			}
			String yearStr = catalogue.getProductionYear();
			if (yearStr != null && !yearStr.equals(TextUtil.EMPTY_STRING)) {
				genreYearStr += (" (" + yearStr + ")");
			}
			
			if (genreYearStr.length() > 1) {
				width = g.getFontMetrics().stringWidth(genreYearStr) + slashClar.getWidth(c) + 10; 
			}
		}
		
		width += widthAdditionalInfo(g, catalogue, isVideo, false, false, c);
		if (isVideo) {
			width += 10 + widthOffer(g, catalogue, false, c) + slashClar.getWidth(c);
		}
		int left = x = 960 - 75 - width;
		if (isVideo) {
			x = paintOffer(g, catalogue, x, 468, false, c);
			g.drawImage(slashClar, x, 457, c);
			x = x + slashClar.getWidth(c) + 10;
			if (genreYearStr.length() > 1) {
				g.drawString(genreYearStr, x, 468);
				x = x + g.getFontMetrics().stringWidth(genreYearStr) + slashClar.getWidth(c) + 10; 
			}
			paintAdditionalInfo(g, catalogue, isVideo, false, false, x, 468, c);
		} else {
			width = widthAdditionalInfo(g, catalogue, isVideo, true, false, c);
			left = x = 960 - 75 - width;
			paintAdditionalInfo(g, catalogue, isVideo, true, false, x, 468, c);
		}
		
		
		// actor
		String str = catalogue.getActors(2);
		if (str.length() > 0) {
//			g.setColor(cActorlbl);
//			g.setFont(FontResource.BLENDER.getFont(16));
//			String lbl = dataCenter.getString(Resources.TEXT_LBL_ACTOR);
//			if (catalogue instanceof Video && ((Video) catalogue).isSpecialEvent()) {
//				lbl = dataCenter.getString(Resources.TEXT_LBL_WITH);
//			}
//			g.drawString(lbl, 75, 466);
//			width = g.getFontMetrics().stringWidth(lbl);
			width = -10;
			
			g.setColor(cActor);
			g.setFont(FontResource.DINMED.getFont(14));
			x = 85 + width;
			width = left - x - 10;
			g.drawString(TextUtil.shorten(str, g.getFontMetrics(), width), x, 467);
		}
	}
}

package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.ColorUtil;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

public class CategoryBackgroundRenderer extends BaseRenderer {

    private Image iBg;// = dataCenter.getImage("07_bg_main.jpg");
    private Image btnExit;// = dataCenter.getImage("btn_exit.png");
//    private Image i00Menushadow;// = dataCenter.getImage("00_menushadow.png");
    private Image i00MainDtTopbg;// = dataCenter.getImage("00_main_dt_topbg.png");
    private Image i00MainDtTopbg_2;// = dataCenter.getImage("00_main_dt_topbg_2.png");
//    private Image i00MenuboxBg;// = dataCenter.getImage("00_menubox_bg.png");
    private Image i01HotkeybgMain;// = dataCenter.getImage("01_hotkeybg_main.png");
    
    private Color cHeader = new Color(245, 212, 6);
    private Color cHeaderGray = new Color(187, 189, 192);
    
    /**
     * Returns the preferred bounds of this renderer. If preferred bounds is non-null, the UIComponent's bounds will be
     * set by this method.
     * 
     * @return The Preferred Bounds for the this Renderer.
     * @see UIComponent#setRenderer
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bgRec;
    }

    /**
     * This will be called before the related UIComponent is shown.
     * 
     * @param c
     *            UIComponent to be painted by this renderer.
     * @see UIComponent#prepare
     */
    public void prepare(UIComponent c) {
        
        btnExit = (Image) ((Hashtable) SharedMemory.getInstance().get(
				PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_EXIT);
        iBg = dataCenter.getImage("07_bg_main.jpg");
        //i00Menushadow = dataCenter.getImage("00_menushadow.png");
        i00MainDtTopbg = dataCenter.getImage("00_main_dt_topbg.png");
        i00MainDtTopbg_2 = dataCenter.getImage("00_main_dt_topbg_2.png");
//        i00MenuboxBg = dataCenter.getImage("00_menubox_bg.png");
        i01HotkeybgMain = dataCenter.getImage("01_hotkeybg_main.png");
        
        setBackGround(iBg);
        
        super.prepare(c);
    }

    /**
     * Paints the UIComponent.
     * 
     * @param g
     *            The graphics context to use for painting.
     * @param c
     *            UIComponent to be painted by this renderer.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        //g.drawImage(i00Menushadow, 0, 477, c);
        
        CategoryUI ui = (CategoryUI) c;
        CategoryContainer category = ui.getCurrentCategory();
        if (category != null) {
        	if (MenuController.getInstance().getHistorySize() == 0) {
            	g.drawImage(i00MainDtTopbg, 355, 26, c);
            } else if (category.type != MenuController.MENU_KARAOKE_CONTENTS
					&& (ui.getBrandingElement() == null || ui.getBrandingElement().getBackground(false) == null)) { // VDTRMASTER-5834
//            	g.drawImage(i00MainDtTopbg_2, 357, 94, c);
				g.drawImage(i00MainDtTopbg, 355, 26, c);
            }
        }
        
        // R5 - display only on isTop()
        // VDTRMASTER-5645
        if (ui.isTop() && ui.equals(CategoryUI.getInstance())) {
//          g.drawImage(i00MenuboxBg, 759, 73, c);
	        g.drawImage(i01HotkeybgMain, 405, 466, c);
//	        g.setFont(FontResource.BLENDER.getFont(19));
//	        g.setColor(new Color(236, 211, 143));
//	        g.drawString(dataCenter.getString(Resources.TEXT_MSG_TO_EXIT), 64, 503);
//	        int w = g.getFontMetrics().stringWidth(dataCenter.getString(Resources.TEXT_MSG_TO_EXIT));
//	        g.drawImage(btnExit, 64+w+5, 489, c);

            // R7.3
            g.setFont(FontResource.BLENDER.getFont(17));
            g.setColor(ColorUtil.getColor(241, 241, 241));
            g.drawImage(btnExit, 64, 489, c);
            g.drawString(dataCenter.getString(Resources.TEXT_MSG_TO_EXIT), 64 + btnExit.getWidth(c) + 5, 503);
        }

        if (category != null && category.type == MenuController.MENU_KARAOKE_CONTENTS) {
	        g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(Color.white);
			GraphicUtil.drawStringRight(g, CategoryUI.getPartyPackString(), 909, 92);
        }
        
//        g.setFont(FontResource.BLENDER.getFont(18));
//		g.setColor(Color.white);
//		g.drawString(ui.getKaraokeContainer() == null ? "NULL" : ui.getKaraokeContainer().toString(true), 5, 15);
    } // end of paint()
}

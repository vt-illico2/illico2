package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.KaraokeListViewUI;

public class KaraokeListViewRenderer extends BaseRenderer {

	private Image bg;// = dataCenter.getImage("bg.jpg");
	private Image i_01_kara_list_bg;// = dataCenter.getImage("01_kara_list_bg.png");
	private Image i_01_kara_list_bg_2;// = dataCenter.getImage("01_kara_list_bg_2.png");
//	private Image i_01_kara_list_sh;// = dataCenter.getImage("01_kara_list_sh.png");
	Image music;
	Color cBase = new Color(46, 46, 45);
	Color cTitle = new Color(214, 182, 61);
	Color cSubTitle = new Color(196, 196, 196);
	Color cLine = new Color(120, 120, 120, 100);
	
	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return bgRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		bg = dataCenter.getImage("bg.jpg");
		i_01_kara_list_bg = dataCenter.getImage("01_kara_list_bg.png");
		i_01_kara_list_bg_2 = dataCenter.getImage("01_kara_list_bg_2.png");
//		i_01_kara_list_sh = dataCenter.getImage("01_kara_list_sh.png");
		music = dataCenter.getImage("music.png");
		setBackGround(bg);
		
		super.prepare(c);
	}

	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		g.setFont(FontResource.BLENDER.getFont(18));
		g.setColor(Color.white);
		GraphicUtil.drawStringRight(g, CategoryUI.getPartyPackString(), 909, 92);
		
		KaraokeListViewUI ui = (KaraokeListViewUI) c;
		
		g.drawImage(i_01_kara_list_bg, 54, 138, c);
		g.drawImage(i_01_kara_list_bg_2, 54, 340, 537, 123, c);
//		g.drawImage(i_01_kara_list_sh, 54, 463, c);
		
		// title
		if (ui.getTitle() != null) {
			int x = 70;
			if (ui.isPlaylist() || ui.isChangeOrder()) {
				g.drawImage(music, x - 8, 163 - 20, c);
				x += 20;
			}
			g.setFont(FontResource.BLENDER.getFont(20));
			g.setColor(cBase);
			g.drawString(ui.getTitle(), x + 1, 163);
			g.setColor(cTitle);
			g.drawString(ui.getTitle(), x, 162);
		}
		for (int i = 0; i < ui.getCount(); i++) {
			g.setColor(cLine);
			g.drawLine(19 + 54, 72 + 138 + i * 32, 19 + 54 + 502, 72 + 138 + i * 32);
		}

		// subtitle
//		if (subTitle != null) {
//			g.setFont(FontResource.BLENDER.getFont(18));
//			g.setColor(cSubTitle);
//			GraphicUtil.drawStringRight(g, subTitle, 573, 161);
//		} else {
//		}
		
		
	}
}

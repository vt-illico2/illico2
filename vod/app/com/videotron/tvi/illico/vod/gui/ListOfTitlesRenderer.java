package com.videotron.tvi.illico.vod.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.AvailableSource;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Section;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.ListOfTitlesUI;

public class ListOfTitlesRenderer extends BaseRenderer {

	private Rectangle bgRec = new Rectangle(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

	private Image i01VodviewBg;// = dataCenter.getImage("bg.jpg");
	private Image i01VodBgline;// = dataCenter.getImage("01_vod_bgline.png");
	// remove in R7 rebrand
//	private Image i_icon_3scr_v;// = dataCenter.getImage("icon_3scr_v.png");
	// R5
	private Image i_cinoche_cote;

	private Color c255 = new Color(255, 255, 255);
	private Color c200 = new Color(200, 200, 200);
	private Color c182 = new Color(182, 182, 182);
	private Color c179 = new Color(179, 179, 179);

	/**
	 * Returns the preferred bounds of this renderer. If preferred bounds is
	 * non-null, the UIComponent's bounds will be set by this method.
	 * 
	 * @return The Preferred Bounds for the this Renderer.
	 * @see UIComponent#setRenderer
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return bgRec;
	}

	/**
	 * This will be called before the related UIComponent is shown.
	 * 
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 * @see UIComponent#prepare
	 */
	public void prepare(UIComponent c) {
		i01VodviewBg = dataCenter.getImage("bg.jpg");
		i01VodBgline = dataCenter.getImage("01_vod_bgline.png");
		// remove in R7 rebrand
//		i_icon_3scr_v = dataCenter.getImageByLanguage("icon_3scr_v.png");
		i_cinoche_cote = dataCenter.getImage("cinoche_cote.png");
		
		setBackGround(i01VodviewBg);
		
		super.prepare(c);
	}

	/**
	 * Paints the UIComponent.
	 * 
	 * @param g
	 *            The graphics context to use for painting.
	 * @param c
	 *            UIComponent to be painted by this renderer.
	 */
	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		ListOfTitlesUI ui = (ListOfTitlesUI) c;

		MoreDetail catalogue = (MoreDetail) ui.getCurrentCatalogue();
		AvailableSource availableSource = null;
		
		if (catalogue instanceof AvailableSource) {
			availableSource = (AvailableSource) catalogue;
			catalogue = ((AvailableSource) catalogue).parent;
		}
		
		g.setFont(FontResource.BLENDER.getFont(26));
		g.setColor(Color.white);
		String mainTitle = MenuController.getInstance().getLastHistory();
		if (catalogue != null && ui.getSubTitle().length() > 0) {
			Episode titleEpisode = null;
			if (catalogue instanceof Episode) {
				titleEpisode = (Episode) catalogue;
			} else if (catalogue instanceof Bundle) {
				MoreDetail[] md = ((Bundle) catalogue).getTitles();
				if (md.length > 0 && md[0] instanceof Episode) {
					titleEpisode = (Episode) md[0];
				}
			}
			if (titleEpisode != null && titleEpisode.season != null) {
				Season titleSeason = titleEpisode.season;
				Series titleSeries = titleSeason.series;
				if (titleSeries != null && titleSeason != null) {
					mainTitle = titleSeries.getTitle() + " - " +
						dataCenter.getString(Resources.TEXT_LBL_SEASON) + " " +
						titleSeason.seasonNumber;
				}
			}
		}
		Channel channel = CategoryUI.getChannel();
		if (!ui.clearBrandingBg() && channel != null 
				&& MenuController.getInstance().isGoToAsset() == false // 4761
				&& MenuController.getInstance().isUnderWishList() == false) { // VDTRMASTER-5628
			
			if (channel.network != null) {
				mainTitle = channel.getLanguageContentData(channel.network.title) + " ≥ " + mainTitle;
			} else {
				mainTitle = channel.getLanguageContentData(channel.title) + " ≥ " + mainTitle;
			}
		} 
		g.drawString(mainTitle, 61, 98);
		int x = g.getFontMetrics().stringWidth(mainTitle);
		g.setFont(FontResource.BLENDER.getFont(17));
		g.setColor(c179);
		g.drawString(ui.getSubTitle(), 70 + x, 98);
//		g.drawImage(i01VodBgline, 0, 250, c);

		int sort = ui.getSortStatus();
		if (sort > 0) {
			g.setFont(FontResource.BLENDER.getFont(18));
//			String str = dataCenter.getString(sort == 1 ? Resources.TEXT_LBL_SORT_NAME
//					: Resources.TEXT_LBL_SORT_DATE);
			String str = "";
			switch (sort) {
			case 1:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_NAME);
				break;
			case 2:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_DATE);
				break;
			case 3:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_PROD);
				break;
			case 4:
				str = dataCenter.getString(Resources.TEXT_LBL_SORT_DEFAULT);
				break;
			}
			GraphicUtil.drawStringRight(g, str, 909, 95);
		}

		// info area -----
		if (catalogue == null || Season.MORE_CONTENT.equals(catalogue)) {
			return;
		}
		
		Extra extra = null;
		if (catalogue instanceof Extra) {
			extra = (Extra) catalogue;
			catalogue = (MoreDetail) CatalogueDatabase.getInstance().getCached(((Extra) catalogue).extraDetail.mainContentItem);
		}
		
		if (catalogue instanceof Episode) {
			int r = 203;
//			if (Resources.currentLanguage == Resources.LANGUAGE_FR) {
//				r += 15;
//			}
			if (((Episode) catalogue).inBundleOnly == false) {
				paintOffer(g, catalogue, 60, 429, r, c);
			}
			// remove in R7 rebrand
//			if (catalogue.is3screen()) {
//				g.drawImage(i_icon_3scr_v, 833, 352, c);
//			}

			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(Color.white);
			
			// R5 - remove
//			String str = dataCenter.getString(Resources.TEXT_LBL_EPISODE) + " " + 
//					((Episode) catalogue).episodeNumber + " : " + catalogue.getTitle();
//			if (MenuController.getInstance().isAdultBlocked(catalogue)) {
//				str = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
//			}
//			g.drawString(str, 238, 348);
//			paintAdditionalInfo(g, catalogue, true, true, false, 237, 366, c);
			
			String str = dataCenter.getString(Resources.TEXT_LBL_EPISODE) + " " + ((Episode) catalogue).episodeNumber;
			g.setColor(c255);
			g.setFont(FontResource.BLENDER.getFont(22));
			g.drawString(str, 61, 348);
			paintAdditionalInfo(g, catalogue, true, true, false, 61, 369, c);
			
//			if (catalogue.getEndPeriod().length() > 0) {
//				String avaliableDate = dataCenter
//						.getString(Resources.TEXT_LBL_AVAILABLE)
//						+ " "
//						+ catalogue.getEndPeriod();
//				g.setFont(FontResource.BLENDER.getFont(16));
//				g.setColor(new Color(225, 195, 80));
//				g.drawString(avaliableDate, 627, 378);
//			}
		} else if (catalogue instanceof Bundle) {
			int r = 203;
//			if (Resources.currentLanguage == Resources.LANGUAGE_FR) {
//				r += 15;
//			}
			paintOffer(g, catalogue, 60, 429, r, c);
			// remove in R7 rebrand
//			if (catalogue.is3screen()) {
//				g.drawImage(i_icon_3scr_v, 833, 352, c);
//			}
			
			// VDTRMASTER-5599
			if (catalogue.getRuntime() > 0) {
				paintAdditionalInfo(g, catalogue, true, true, false, 61, 369, c);
			} else {
				paintAdditionalInfo(g, catalogue, false, true, false, 61, 369, c);
			}
//			String avaliableDate = dataCenter
//					.getString(Resources.TEXT_LBL_AVAILABLE)
//					+ " "
//					+ catalogue.getEndPeriod();
//			g.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(new Color(225, 195, 80));
//			g.drawString(avaliableDate, 627, 378);
			
		} else if (catalogue instanceof Video) {
			int y = 381;
			if (((Video) catalogue).inBundleOnly == false) {
				y = paintOffer(g, catalogue, 60, 381, 203, c);
			}
			if (extra != null) {
				Video firstExtra = null;
				for (int i = 0; i < extra.extraDetail.video.length; i++) {
					if ("VT_104".equals(extra.extraDetail.video[i].getTypeID())) {
						firstExtra = extra.extraDetail.video[i];
						break;
					}
				}
				if (firstExtra != null) {
					paintOffer(g, firstExtra, 60, y + 23, 203, c);
				}
			}
			// R5 - remove
//			// VDTRMASTER-5361
//			if (catalogue.is3screen() && extra == null) {
//				g.drawImage(i_icon_3scr_v, 59, 434, c);
//			}
			
			// R5
			// genre and year
			g.setFont(FontResource.BLENDER.getFont(20));
			g.setColor(c255);
			String str = "";
			String genreStr = catalogue.getGenre();
			if (genreStr != null) {
				if (g.getFontMetrics().stringWidth(genreStr) > 202) {
					genreStr = TextUtil.shorten(genreStr, g.getFontMetrics(), 202);
				}
				str += genreStr;
			}
			String yearStr = catalogue.getProductionYear();
			if (yearStr != null && !yearStr.equals(TextUtil.EMPTY_STRING)) {
				str += (" (" + yearStr + ")");
			}
			g.drawString(str, 237, 382);
			
			int w = widthAdditionalInfo(g, catalogue, true, false, true, c);
			paintAdditionalInfo(g, catalogue, true, false, true, 237, 401, c);

//			g.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(c182);
//			String lbl = dataCenter.getString(Resources.TEXT_LBL_ACTOR);
//			if (((Video) catalogue).isSpecialEvent()) {
//				lbl = dataCenter.getString(Resources.TEXT_LBL_WITH);
//			}
//			g.drawString(lbl, 237, 402);
//			w = g.getFontMetrics().stringWidth(lbl);
			w = -10;
			g.setFont(FontResource.DINMED.getFont(15));
			g.setColor(c255);
			String[] actors = TextUtil.split(catalogue.getActors(3), g.getFontMetrics(), 360, 2);
			if (actors != null) {
				for (int i = 0; i < actors.length; i++) {
					g.drawString(actors[i], 237, 451 + i * 17);
				}
			}
			
			// cinoche cote
			if (((Video) catalogue).externalRef != null) {
				g.drawImage(i_cinoche_cote, 505, 368, c);
				g.setColor(Color.white);
				g.setFont(FontResource.DINMED.getFont(18));
				String value = ((Video) catalogue).externalRef.value;
				g.drawString(value, 558, 382);
				int valueWidth = g.getFontMetrics().stringWidth(value) + 2;
				g.setFont(FontResource.DINMED.getFont(12));
				g.drawString(SLASH_FIVE, 558 + valueWidth, 382);
			}

		} else if (catalogue instanceof Season) {
			Season season = (Season) catalogue;
			String title = "";
			boolean more = false;
			if (MenuController.getInstance().isAdultBlocked(catalogue)) {
				title = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
			} else {
				if (season.series != null) {
					title = season.series.getTitle();
				}
				//title = season.getTitle();
				more = true;
			}
			g.setColor(Color.white);
			g.setFont(FontResource.BLENDER.getFont(22));
			g.drawString(title, 61, 348);
			if (more) {
				x = 61 + g.getFontMetrics().stringWidth(title) + 10;
				g.drawImage(slashClar, x, 348 - 11, c);
				x += slashClar.getWidth(c) + 10;
				g.drawString(dataCenter.getString(Resources.TEXT_LBL_SEASON) + " " + season.seasonNumber, x, 348);
			}
			
			int episodeCnt = availableSource != null ? availableSource.episodeIds.length : season.episodeSize();
			String third = episodeCnt + " " + dataCenter
				.getString(episodeCnt > 1 ? Resources.TEXT_MSG_EPISODES
					: Resources.TEXT_MSG_EPISODE);
			
			g.setColor(c200);
			g.setFont(FontResource.BLENDER.getFont(18));
			g.drawString(third, 61, 369);
			x = 61 + g.getFontMetrics().stringWidth(third) + 10;
			g.drawImage(slashClar, x, 369 - 11, c);
			x += slashClar.getWidth(c) + 10;
			paintAdditionalInfo(g, catalogue, false, true, false, x, 369, c);

		} else if (catalogue instanceof Series) {
			int seasonCnt = ((Series) catalogue).seasonSize();
			String title = seasonCnt + " "
					+ dataCenter
							.getString(seasonCnt > 1 ? Resources.TEXT_MSG_SEASONS_AVAILABLE
									: Resources.TEXT_MSG_SEASON_AVAILABLE);
			if (MenuController.getInstance().isAdultBlocked(catalogue)) {
				title = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
			}
			g.setColor(Color.white);
			g.setFont(FontResource.BLENDER.getFont(19));
			g.drawString(title, 59, 348);
			x = 59 + g.getFontMetrics().stringWidth(title) + 10;
			g.drawImage(slashClar, x, 348 - 11, c);
			x += slashClar.getWidth(c) + 10;
			// g.setColor(c200);
			// g.setFont(FontResource.BLENDER.getFont(16));
			// g.drawString(catalogue.getGenre(), x, 348);
			// x += g.getFontMetrics().stringWidth(catalogue.getGenre()) + 10;
			// g.drawImage(slashClar, x, 348 - 11, c);
			// x += slashClar.getWidth(c) + 10;
			paintAdditionalInfo(g, catalogue, false, true, false, x, 348, c);

//			g.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(c182);
//			g.drawString(dataCenter.getString(Resources.TEXT_LBL_ACTOR), 60, 366);
//			x = 70 + g.getFontMetrics().stringWidth(dataCenter.getString(Resources.TEXT_LBL_ACTOR));
			x = 59;
			g.setColor(c200);
			g.setFont(FontResource.DINMED.getFont(14));
			g.drawString(catalogue.getActors(3), x, 366);

		} else if (catalogue instanceof Section) {
			Section section = (Section) catalogue;
			if (section.sections.length == 0 && section.episodes.length > 0) {
				String title = section.episodes.length + dataCenter.getString(
						section.episodes.length == 1 
						? Resources.TEXT_MSG_ITEM_AVAILABLE 
								: Resources.TEXT_MSG_ITEMS_AVAILABLE); 
				g.setColor(Color.white);
				g.setFont(FontResource.BLENDER.getFont(19));
				g.drawString(title, 59, 348);
				x = 59 + g.getFontMetrics().stringWidth(title) + 10;
				g.drawImage(slashClar, x, 348 - 11, c);
				x += slashClar.getWidth(c) + 10;
				Episode episode = (Episode) CatalogueDatabase.getInstance().getCached(section.episodes[0]);
				paintAdditionalInfo(g, episode, false, true, false, x, 348, c);
			}
			
		}
	}
}

package com.videotron.tvi.illico.vod.app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Enumeration;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.PromotionBanner;
import com.videotron.tvi.illico.util.ColorUtil;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.comp.PopTrailer;
import com.videotron.tvi.illico.vod.controller.ComponentPool;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VODServiceImpl;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.UsageManager;
import com.videotron.tvi.illico.vod.controller.menu.VideoControllerImpl;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

/**
 * This class is the initial class of VOD Xlet.
 *
 * @version $Revision$ $Date$
 * @author LSC
 */
public class App implements Xlet, ApplicationConfig {

	/**
	 * The instance of XletContext.
	 */
	private XletContext context = null;

	// IMPROVEMENT
	static Object logObj;
	static Method method;
	
	/**
	 * Signals the Xlet to initialize itself and enter the Paused state.
	 *
	 * @param ctx
	 *            The XletContext of this Xlet.
	 * @throws XletStateChangeException
	 *             If the Xlet cannot be initialized.
	 */
	public void initXlet(XletContext ctx) throws XletStateChangeException {
		context = ctx;

		FrameworkMain.getInstance().init(this);
	}
	
	public void init() {
		Log.printDebug("App, init()");

		// DDC_090
		DataCenter.getInstance().put("DDC_090", "true");
		
		/*
		 * for vcds : 

http://10.23.106.92:10080/vcds/    version 2.2 

http://10.23.106.91:10080/vcds/    version 2.1 

if you want to access them directly 

or you can access the proxy on 10.23.106.91 on port 9999 

for bookmark server: 

http://10.23.106.73:10080/controlsvr/     
or 
http://10.23.106.74:10080/controlsvr/     
or 
http://10.23.106.86:10080/controlsvr/ 
		 */
		if (Environment.EMULATOR) { // Lab or Qualif IP for Emul
			boolean qualif = true;
			DataCenter.getInstance().put("VCDS_URL", qualif ?
						"http://10.247.89.252:9996/vcds/" :
						"http://10.247.89.252:9998/vcds/");
			DataCenter.getInstance().put("IMAGE_URL", qualif ?
						"http://10.247.89.252:9997" :
						"http://10.247.89.252:9999");
			DataCenter.getInstance().put("BOOKMARK_URL", qualif ?
//						"http://10.23.120.133:8180/controlsvr/" :
//						"http://10.247.56.146:8989/controlsvr/" :
						"http://10.247.124.55:8080/controlsvr/" :
						"http://10.247.124.55:8080/controlsvr/");
		}
		
		MenuController.getInstance().init();
		CommunicationManager.getInstance().init(context);
		PreferenceProxy.getInstance().init(context);
		VODServiceImpl.getInstance().start(context);
		VideoControllerImpl.getInstance().init();

		SharedMemory.getInstance().put(SharedDataKeys.VOD_POSTER_IMAGE,
				CatalogueDatabase.getInstance());
		
		// R5
		ChannelInfoManager.getInstance(); // to receive a preview data via oob
		
		// 4K
//		ImageRetriever.BASE = DataCenter.getInstance().getString("IMAGE_DIRECTORY");
		
		Resources.preloadClasses();
		
		// IMPROVEMENT
		CategoryUI.getInstance();
		
		Log.printInfo("App, initXlet(), ver = " + Resources.APP_VERSION);
	}

	/**
	 * Signals the Xlet to start providing service and enter the Active state.
	 *
	 * @throws XletStateChangeException
	 *             is thrown if the Xlet cannot start providing service.
	 */
	synchronized public void startXlet() throws XletStateChangeException {
		MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF.format(new Date()) + ", startXlet() is called");
		Resources.appStatus = VODService.APP_STARTED;

		long time = System.currentTimeMillis();
		Log.printInfo("App, startXlet()");

		if (DataCenter.getInstance().get("VCDS_URL") == null
				|| DataCenter.getInstance().get("VCDS_URL").toString().length() == 0
				|| DataCenter.getInstance().get("IMAGE_URL") == null
				|| DataCenter.getInstance().get("IMAGE_URL").toString()
						.length() == 0) {
			Log.printWarning("App, VCDS_URL = " + DataCenter.getInstance().get("VCDS_URL") + ", IMAGE_URL = " + DataCenter.getInstance().get("IMAGE_URL"));
			CommunicationManager.getInstance().errorMessage("VOD201-04", null);
		}

		long before = System.currentTimeMillis();
		FrameworkMain.getInstance().start();
		long after = System.currentTimeMillis();
		MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF.format(new Date()) + ", FrameworkMain.start() is done, elapsed(ms) = " + (after-before));

		before = System.currentTimeMillis();
		CommunicationManager.getInstance().start();
		after = System.currentTimeMillis();
		MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF.format(new Date()) + ", CommunicationManager.start() is done, elapsed(ms) = " + (after-before));

		ImageRetriever.initThread();
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
				//CommunicationManager.getInstance().turnOnErrorMessage(true);
//				MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF.format(new Date()) + ", MenuController.start()");
				before = System.currentTimeMillis();
				MenuController.getInstance().start();
				after = System.currentTimeMillis();
				MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF.format(new Date()) + ", MenuController.start() is done, elapsed(ms) = " + (after-before));
//			}
//		});		
		VbmController.getInstance().writeSession(true, 0);
		
		time = System.currentTimeMillis() - time;
		Log.printInfo("App, startXlet(), DONE - " + time);
		MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF.format(new Date()) + ", startXlet() is done, elapsed(ms) = " + time);
	}

	/**
	 * Signals the Xlet to stop providing service and enter the Paused state.
	 */
	synchronized public void pauseXlet() {
		Resources.appStatus = VODService.APP_PAUSED;
		long time = System.currentTimeMillis();
		Log.printInfo("App, pauseXlet()");
		
		// pauseXlet() is very important for stability
		// so put it into try-catch blocks each
		
		boolean pause = true;
		try {
			pause = MenuController.getInstance().pause();
		} catch (Exception e) {
			Log.printError(e);
		}
		if (pause) {
			try {
				MenuController.getInstance().stop();
			} catch (Exception e) {
				Log.printError(e);
			}
			try {
				ComponentPool.cleanUp();
			} catch (Exception e) {
				Log.printError(e);
			}
//			try {
//				FrameworkMain.getInstance().getImagePool().clear();
//			} catch (Exception e) {
//				Log.printError(e);
//			}
			// IMPROVEMENT
			try {
				processSelectiveImage();
			} catch (Exception e) {
				Log.printError(e);
			}
			try {
				CatalogueDatabase.getInstance().cleanUp();
			} catch (Exception e) {
				Log.printError(e);
			}
		}
		
		try {
			PromotionBanner.getInstance().stopMonitoring();
		} catch (Exception e) {
			Log.printError(e);
		}
		try {
			CommunicationManager.getInstance().stop();
		} catch (Exception e) {
			Log.printError(e);
		}
		try {
			PopTrailer.getInstance().setAppStatus(Resources.appStatus);
			VideoControllerImpl.getInstance().stop();
		} catch (Exception e) {
			Log.printError(e);
		}
		try {
			VODServiceImpl.getInstance().stop();
		} catch (Exception e) {
			Log.printError(e);
		}
		try {
			FrameworkMain.getInstance().pause();
		} catch (Exception e) {
			Log.printError(e);
		}
		try {
			ImageRetriever.closeThread();
		} catch (Exception e) {
			Log.printError(e);
		}
		try {
			UsageManager.getInstance().upload();
		} catch (Exception e) {
			Log.printError(e);
		}
		
		// IMPROVEMENT
		preload();
		
		VbmController.getInstance().writeSession(false, 0);

		time = System.currentTimeMillis() - time;
		Log.printInfo("App, pauseXlet(), DONE - " + time);
	}

	static boolean needToInitialize = true;
	public static void preload() {
		
		if (needToInitialize) {
			needToInitialize = false;
	        String className = "com.alticast.debug.Log";
	        String methodName = "printDebug";
	        
	        Class clazz;
			try {
				clazz = Class.forName(className);
				logObj = clazz.newInstance();
				Class[] types = new Class[1];
				types[0] = String.class;
				method = clazz.getMethod(methodName, types);
				
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
		
		CatalogueDatabase.getInstance().retrieveCatalogueStructure(null, null);
		
//		DataCenter dataCenter = DataCenter.getInstance();
//        dataCenter.getImage("07_bg_main.jpg");
//        dataCenter.getImage("00_main_dt_topbg.png");
//        dataCenter.getImage("00_main_dt_topbg_2.png");
//        dataCenter.getImage("00_menubox_bg.png");
//        dataCenter.getImage("01_hotkeybg_main.png");
//		dataCenter.getImage("his_dim.png");
//		dataCenter.getImage("his_over.png");
//		dataCenter.getImage("icon_g.png");
//		dataCenter.getImage("icon_8.png");
//		dataCenter.getImage("icon_13.png");
//		dataCenter.getImage("icon_16.png");
//		dataCenter.getImage("icon_18.png");
//		dataCenter.getImage("icon_sd.png");
//		dataCenter.getImage("icon_hd.png");
//		dataCenter.getImage("icon_fhd_long.png");
//		dataCenter.getImage("icon_3d.png");
//		dataCenter.getImage("slash_price.png");
//		dataCenter.getImage("slash_clar.png");
//		dataCenter.getImage("icon_fr.png");
//		dataCenter.getImage("icon_en.png");
//		dataCenter.getImage("icon_cc.png");
//		dataCenter.getImage("01_icon_sd_s.png");
//		dataCenter.getImage("01_icon_hd_s.png");
//		dataCenter.getImage("01_icon_fhd_s.png");
//		dataCenter.getImage("01_tooliconbg.png");
//		dataCenter.getImage("01_resumeicon.png");
//		dataCenter.getImage("01_searchicon.png");
//		dataCenter.getImage("01_optionicon.png");
//		dataCenter.getImage("01_adulticon.png");
//		dataCenter.getImage("01_adult_poster.png");
//		dataCenter.getImage("01_poster_block.png");
//		dataCenter.getImage("01_poster_block_h.png");
//		dataCenter.getImage("02_ars_l.png");
//		dataCenter.getImage("02_ars_r.png");
//		dataCenter.getImage("05_focus_dim.png");
//		dataCenter.getImage("05_focus.png");
//		dataCenter.getImage("00_focus_dim.png");
//		dataCenter.getImage("00_focus.png");
//		dataCenter.getImage("01_possh_s_l.png");
//		dataCenter.getImage("01_poshigh_s_l.png");
//		dataCenter.getImage("01_possh_s.png");
//		dataCenter.getImage("01_poshigh_s.png");
//		dataCenter.getImage("01_possh_b.png");
//		dataCenter.getImage("01_poshigh_b.png");
//		dataCenter.getImage("02_icon_isa_bl.png");
//		dataCenter.getImage("02_ars_t.png");
//		dataCenter.getImage("02_ars_b.png");
//		dataCenter.getImage("01_kara_mainbn_high.png");
//		dataCenter.getImage("01_kara_mainbn_sh.png");
//		
//		FrameworkMain.getInstance().getImagePool().waitForAll();
	}
	
	// IMPROVEMENT
	static final String[] IMAGE_LIST = {
			"07_bg_main.jpg", "00_main_dt_topbg.png", "00_main_dt_topbg_2.png", "01_hotkeybg_main.png",
			"his_dim.png", "his_over.png", "icon_g.png", "icon_8.png", "icon_13.png", "icon_16.png",
			"icon_18.png", "icon_sd.png", "icon_hd.png", "icon_fhd_long.png", "icon_3d.png", "slash_price.png",
			"slash_clar.png", "icon_fr.png", "icon_en.png", "icon_cc.png", "01_icon_sd_s.png", "01_icon_hd_s.png",
			"01_icon_fhd_s.png", "01_tooliconbg.png", "01_resumeicon.png", "01_searchicon.png", "01_optionicon.png",
			"01_adulticon.png", "01_adult_poster.png", "01_poster_block.jpg", "01_poster_block_h.jpg",
			"02_ars_l.png", "02_ars_r.png", "05_focus_dim.png", "05_focus.png", "00_focus_dim.png", "00_focus.png",
			"01_possh_b.png", "01_poshigh_b.png", "02_icon_isa_bl.png", "02_ars_t.png", "02_ars_b.png",
			"01_kara_mainbn_high.png", "01_kara_mainbn_sh.png"
	};
	
	// IMPROVEMENT
	private void processSelectiveImage() {
		Log.printInfo("App, processSelectiveImage");
		ImagePool ip = FrameworkMain.getInstance().getImagePool();
		
		Enumeration keys = ip.keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            
            boolean isIncluded = false;
            
            for (int i = 0; i < IMAGE_LIST.length; i++) {
            	if (IMAGE_LIST.equals(key)) {
            		isIncluded = true;
            	}
            }
            
            if (!isIncluded) {
            	ip.remove(key);
            }
            
        }
	}
	
	/**
	 * Signals the Xlet to terminate and enter the Destroyed state.
	 *
	 * @param unconditional
	 *            If unconditional is true when this method is called, requests
	 *            by the Xlet to not enter the destroyed state will be ignored.
	 * @throws XletStateChangeException
	 *             is thrown if the Xlet wishes to continue to execute (Not
	 *             enter the Destroyed state). This exception is ignored if
	 *             unconditional is equal to true.
	 */
	public void destroyXlet(boolean unconditional)
			throws XletStateChangeException {
		Log.printInfo("App, destroyXlet()");
		VODServiceImpl.getInstance().dispose();
		FrameworkMain.getInstance().getHScene().dispose();
		FrameworkMain.getInstance().destroy();
		CatalogueDatabase.getInstance().monitoringSurvivalKit(false);
		CommunicationManager.getInstance().destroy();

		// Bundling
		ColorUtil.dispose();

		VbmController.getInstance().dispose();
	}

	/**
	 * implements getApplicationName of {@link ApplicationConfig}.
	 *
	 * @return the name of VOD Application
	 * @link {@link com.videotron.tvi.illico.framework.ApplicationConfig#getApplicationName()}
	 */
	public String getApplicationName() {
		return Resources.APP_NAME;
	}

	/**
	 * implements getVersion of ApplicationConfig.
	 *
	 * @return the version of VOD Application
	 * @link {@link com.videotron.illico.framework.ApplicationConfig#getVersion()}
	 */
	public String getVersion() {
		return Resources.APP_VERSION;
	}

	/**
	 * implements getXletContext of ApplicationConfig.
	 *
	 * @return the XletContext of VOD Application
	 * @link {@link com.videotron.tvi.illico.framework.ApplicationConfig#getXletContext()}
	 */
	public XletContext getXletContext() {
		return context;
	}
	
	// IMPROVEMENT
	public static void printDebug(final String str) {
		
		if (method != null) {
		
	    	try {
				method.invoke(logObj, new String[] {str});
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
    }
}

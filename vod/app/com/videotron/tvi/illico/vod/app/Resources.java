package com.videotron.tvi.illico.vod.app;

import java.awt.Rectangle;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * The Class Resources.
 */
public class Resources {
	/** The Constant Version of SSP21. */
	public static final int SSP21 = 21;
	/** The Constant Version of SSP23. */
	public static final int SSP23 = 23;
	/** The current used SSP version. */
	public static int sspVersion = SSP21;
	/** The Constant VOD APP NAME. */
	public static final String APP_NAME = "VOD";
	/** The current APP status. */
	public static int appStatus;
	/** The Constant APP VERSION. */
	public static final String APP_VERSION = "(R7.5).0.1";
	/** The full size of java graphic layer boundary. */
	public static Rectangle fullRec = new Rectangle(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
	/** The Constant LANGUAGE English. */
	public static final int LANGUAGE_EN = 0;
	/** The Constant LANGUAGE French. */
	public static final int LANGUAGE_FR = 1;
	/** The current language. */
	public static int currentLanguage = LANGUAGE_EN;
	/** The Constant HD_FR. */
	public static final int HD_FR = 0;
	/** The Constant HD_EN. */
	public static final int HD_EN = 1;
	/** The Constant SD_FR. */
	public static final int SD_FR = 2;
	/** The Constant SD_EN. */
	public static final int SD_EN = 3;

	// buttons
	/** The Constant TEXT_OK. All text are defined at /vod/resources/text/*.txt */
	public static final String TEXT_OK = "button.ok";
	/** The Constant TEXT_SELECT. */
	public static final String TEXT_SELECT = "button.select";
	/** The Constant TEXT_BACK. */
	public static final String TEXT_BACK = "button.back";
	/** The Constant TEXT_OPTION. */
	public static final String TEXT_OPTION = "button.option";
	/** The Constant TEXT_SEARCH. */
	public static final String TEXT_SEARCH = "button.search";
	/** The Constant TEXT_ORDER. */
	public static final String TEXT_ORDER = "button.order";
	/** The Constant TEXT_ORDER_PARTY_PACK. */
	public static final String TEXT_ORDER_PARTY_PACK = "button.orderPartyPack";
	/** The Constant TEXT_ADDTO_WISHLIST. */
	public static final String TEXT_ADDTO_WISHLIST = "button.addToWishlist";
	/** The Constant TEXT_REMOVE_FROM_WISHLIST. */
	public static final String TEXT_REMOVE_FROM_WISHLIST = "button.removeFromWishList";
	/** The Constant TEXT_ADDTO_WISHLIST_18. */
	public static final String TEXT_ADDTO_WISHLIST_18 = "button.addToWishlist18";
	/** The Constant TEXT_REMOVE_FROM_WISHLIST_18. */
	public static final String TEXT_REMOVE_FROM_WISHLIST_18 = "button.removeFromWishList18";
	/** The Constant TEXT_CLOSE. */
	public static final String TEXT_CLOSE = "button.close";
	/** The Constant TEXT_ORDER_TITLE. */
	public static final String TEXT_ORDER_TITLE = "button.orderTitle";
	/** The Constant TEXT_ORDER_BUNDLE. */
	public static final String TEXT_ORDER_BUNDLE = "button.orderBundle";
	/** The Constant TEXT_SEE_BUNDLE. */
	public static final String TEXT_SEE_BUNDLE = "button.seeBundle";
	/** The Constant TEXT_SEE_BUNDLE_DETAIL. */
	public static final String TEXT_SEE_BUNDLE_DETAIL = "button.seeBundleDetail";
	/** The Constant TEXT_SEE_TRAILER. */
	public static final String TEXT_SEE_TRAILER = "button.seeTrailer";
	/** The Constant TEXT_SCROLL_DESCRIPTION. */
	public static final String TEXT_SCROLL_DESCRIPTION = "button.scrollDescription";
	/** The Constant TEXT_CHANGE_PAGE. */
	public static final String TEXT_CHANGE_PAGE = "button.changePage";
	/** The Constant TEXT_VIEW_CHANGE. */
	public static final String TEXT_VIEW_CHANGE = "button.viewChange";
	/** The Constant TEXT_VIEW_ALL_CHANNEL. */
	public static final String TEXT_VIEW_ALL_CHANNEL = "button.viewAllChannel";
	/** The Constant TEXT_VIEW_MY_CHANNEL. */
	public static final String TEXT_VIEW_MY_CHANNEL = "button.viewMyChannel";
	/** The Constant TEXT_SEASON_PAGE. */
	public static final String TEXT_SEASON_PAGE = "button.seasonPage";
	/** The Constant TEXT_ORDER_EPISODE. */
	public static final String TEXT_ORDER_EPISODE = "button.orderEpisode";
	/** The Constant TEXT_ORDER_EPISODES. */
	public static final String TEXT_ORDER_EPISODES = "button.orderEpisodes";
	/** The Constant TEXT_ORDER_SEASON. */
	public static final String TEXT_ORDER_SEASON = "button.orderSeason";
	/** The Constant TEXT_CHANGE_SEASON. */
	public static final String TEXT_CHANGE_SEASON = "button.changeSeason";
	/** The Constant TEXT_REMOVE. */
	public static final String TEXT_REMOVE = "button.remove";
	/** The Constant TEXT_SEE_OPTIONS. */
	public static final String TEXT_SEE_OPTIONS = "button.seeOptions";
	/** The Constant TEXT_ORDER_AGAIN. */
	public static final String TEXT_ORDER_AGAIN = "button.orderAgain";
	/** The Constant TEXT_UNBLOCK. */
	public static final String TEXT_UNBLOCK = "button.unblock";
	/** The Constant TEXT_WATCH. */
	public static final String TEXT_WATCH = "button.watch";
	/** The Constant TEXT_RESUME_VIEW. */
	public static final String TEXT_RESUME_VIEW = "button.resumeView";
	/** The Constant TEXT_BACK_TO_DETAIL. */
	public static final String TEXT_BACK_TO_DETAIL = "button.backToDetail";
	/** The Constant TEXT_CANCEL. */
	public static final String TEXT_CANCEL = "button.cancel";	
	/** The Constant TEXT_WATCH_NOW. */
	public static final String TEXT_WATCH_NOW = "button.watchNow";
	/** The Constant TEXT_WATCH_EPISODE_NOW. */
	public static final String TEXT_WATCH_EPISODE_NOW = "button.watchEpisodeNow";
	/** The Constant TEXT_WATCH_ITEM_NOW1. */
	public static final String TEXT_WATCH_ITEM_NOW1 = "button.watchItemNow1";
	/** The Constant TEXT_WATCH_ITEM_NOW2. */
	public static final String TEXT_WATCH_ITEM_NOW2 = "button.watchItemNow2";
	// VDTRMASTER-5503
	/** The Constant TEXT_SELECT_ANOTHER_EPISODE. */
	public static final String TEXT_SELECT_ANOTHER_EPISODE = "button.selectAnotherEpisode";
	// VDTRMASTER-5503
	/** The Constant TEXT_SELECT_ANOTHER_MOVIE. */
	public static final String TEXT_SELECT_ANOTHER_MOVIE = "button.selectAnotherMovie";
	/** The Constant TEXT_VOD_MENU. */
	public static final String TEXT_VOD_MENU = "button.vodMenu";
	/** The Constant TEXT_GO_TO_DETAIL. */
	public static final String TEXT_GO_TO_DETAIL = "button.goToDetails";
	/** The Constant TEXT_PLAY_FROM_BEGIN. */
	public static final String TEXT_PLAY_FROM_BEGIN = "button.playFromBeginning";
	/** The Constant TEXT_BACK_TO_CATEGORY. */
	public static final String TEXT_BACK_TO_CATEGORY = "button.backToTheCategory";
	/** The Constant TEXT_BACK_TO_MENU. */
	public static final String TEXT_BACK_TO_MENU = "button.backToTheMenu";
	/** The Constant TEXT_VIEW_NEXT. */
	public static final String TEXT_VIEW_NEXT = "button.viewNext";
	/** The Constant TEXT_ORDER_NEXT. */
	public static final String TEXT_ORDER_NEXT = "button.orderNext";
	/** The Constant TEXT_NEXT_SONG. */
	public static final String TEXT_NEXT_SONG = "button.nextSong";
	/** The Constant TEXT_BACK_TO_TV. */
	public static final String TEXT_BACK_TO_TV = "button.backToTVMode";
	/** The Constant TEXT_VOD_MENU2. */
	public static final String TEXT_VOD_MENU2 = "button.vodMenu2";
	/** The Constant TEXT_ADDED_TO_WISHLIST. */
	public static final String TEXT_ADDED_TO_WISHLIST = "button.addedToWishList";
	/** The Constant TEXT_MY_WISHLIST. */
	public static final String TEXT_MY_WISHLIST = "button.myWishlist";
	/** The Constant TEXT_MY_WISHLIST_18. */
	public static final String TEXT_MY_WISHLIST_18 = "button.myWishlist18";
	/** The Constant TEXT_CONTINUE_WATCH. */
	public static final String TEXT_CONTINUE_WATCH = "button.continueToWatch";
	/** The Constant TEXT_EPISODE_SUMMARY. */
	public static final String TEXT_EPISODE_SUMMARY = "button.episodesSummary";
	/** The Constant TEXT_CLOSE_OPTIONS. */
	public static final String TEXT_CLOSE_OPTIONS = "button.closeOptions";
	/** The Constant TEXT_CLOSE_WISHLIST. */
	public static final String TEXT_CLOSE_WISHLIST = "button.closeWishlist";
	/** The Constant TEXT_SELECT_SERIES. */
	public static final String TEXT_SELECT_SERIES = "button.selectSeries";
	/** The Constant TEXT_SELECT_SEASON. */
	public static final String TEXT_SELECT_SEASON = "button.selectSeason";
	/** The Constant TEXT_PLAY_ALL_SEASON. */
	public static final String TEXT_PLAY_ALL_SEASON = "button.playAllSeason";
	/** The Constant TEXT_PLAY_ALL_EPISODES. */
	public static final String TEXT_PLAY_ALL_EPISODES = "button.playAllEpisodes";
	/** The Constant TEXT_SEE_DETAIL. */
	public static final String TEXT_SEE_DETAIL = "button.seeDetail";
	/** The Constant TEXT_BUNDLE_NO. */
	public static final String TEXT_BUNDLE_NO = "button.bundleNo";
	/** The Constant TEXT_SUBSCRIBE. */
	public static final String TEXT_SUBSCRIBE = "button.subscribe";
	/** The Constant TEXT_SUBSCRIBE_SVOD. */
	public static final String TEXT_SUBSCRIBE_SVOD = "button.subscribeSVOD";
	/** The Constant TEXT_SUBSCRIBE_NOW. */
	public static final String TEXT_SUBSCRIBE_NOW = "button.subscribeNow";
	/** The Constant TEXT_ADD_THIS_CHANNEL. */
	public static final String TEXT_ADD_THIS_CHANNEL = "button.addThisChannel";
	/** The Constant TEXT_CHANGE_MY_PKG. */
	public static final String TEXT_CHANGE_MY_PKG = "button.changeMyPkg";
	/** The Constant TEXT_CHANGE_MY_TV_PKG. */
	public static final String TEXT_CHANGE_MY_TV_PKG = "button.changeMyTVPkg";
	/** The Constant TEXT_ADD. */
	public static final String TEXT_ADD = "button.add";
	/** The Constant TEXT_TUNE_TO. */
	public static final String TEXT_TUNE_TO = "button.tuneTo";
	/** The Constant TEXT_PLAY_SONG. */
	public static final String TEXT_PLAY_SONG = "button.playSong";
	/** The Constant TEXT_PLAY_ALL. */
	public static final String TEXT_PLAY_ALL = "button.playAll";
	/** The Constant TEXT_PLAY_SELECTED. */
	public static final String TEXT_PLAY_SELECTED = "button.playSelected";
	/** The Constant TEXT_WATCH_ALL. */
	public static final String TEXT_WATCH_ALL = "button.watchAll";
	/** The Constant TEXT_CHANGE_ORDER. */
	public static final String TEXT_CHANGE_ORDER = "button.changeOrder";
	/** The Constant TEXT_ADD_PLAYLIST. */
	public static final String TEXT_ADD_PLAYLIST = "button.addPlaylist";
	/** The Constant TEXT_REMOVE_PLAYLIST. */
	public static final String TEXT_REMOVE_PLAYLIST = "button.removePlaylist";
	/** The Constant TEXT_CONFIRM_CHANGE. */
	public static final String TEXT_CONFIRM_CHANGE = "button.confirmChange";
	/** The Constant TEXT_ADD_TOP. */
	public static final String TEXT_ADD_TOP = "button.addTop";
	/** The Constant TEXT_MY_PLAYLIST. */
	public static final String TEXT_MY_PLAYLIST = "button.myPlaylist";
	/** The Constant TEXT_TRY_NOW. */
	public static final String TEXT_TRY_NOW = "button.tryNow";
	/** The Constant TEXT_PREVIEW. */
	public static final String TEXT_PREVIEW = "button.preview";
	/** The Constant TEXT_BACK_TO_PLAYLIST. */
	public static final String TEXT_BACK_TO_PLAYLIST = "button.backToPlaylist";
	/** The Constant TEXT_VIEW_EPISODES. */
	public static final String TEXT_VIEW_EPISODES = "button.viewEpisodes";
	/** The Constant TEXT_VIEW_BUNDLE_ITEMS. */
	public static final String TEXT_VIEW_BUNDLE_ITEMS = "button.viewBundleItems";
	/** The Constant TEXT_PLAY_1_TO_LAST. */
	public static final String TEXT_PLAY_1_TO_LAST = "button.play1ToLast";
	/** The Constant TEXT_PLAY_SEL_TO_LAST. */
	public static final String TEXT_PLAY_SEL_TO_LAST = "button.playSelToLast";
	/** The Constant TEXT_PLAY_SEL_ONLY. */
	public static final String TEXT_PLAY_SEL_ONLY = "button.playSelectedOnly";
	/** The Constant TEXT_SELECT_AN_EPISODE. */
	public static final String TEXT_SELECT_AN_EPISODE = "button.selectAnEpisode";
	// VDTRMASTER-5508
	/** The Constant TEXT_SELECT_AN_MOVIE. */
	public static final String TEXT_SELECT_A_MOVIE = "button.selectAMovie";
	/** The Constant TEXT_SELECT_THIS_CONTENT. */
	public static final String TEXT_SELECT_THIS_CONTENT = "button.selectThisContent";
	/** The Constant TEXT_SEE_MORE_CONTENT. */
	public static final String TEXT_SEE_MORE_CONTENT = "button.seeMoreContent";
	
	public static final String TEXT_GOTO_CLUB = "button.goToClub";
	public static final String TEXT_WATCH_IN_CLUB = "button.watchInClub";
	public static final String TEXT_GOTO_CHANNEL = "button.goToChannel";
	
	// R5
	public static final String TEXT_SIMILAR_CONTENT = "button.similar.contents";
	
	//R7.4
	public static final String TEXT_LANGUAGE_EN = "button.lang.en";
	public static final String TEXT_LANGUAGE_FR = "button.lang.fr";
	public static final String TEXT_LANGUAGE_EN_CONFIRM = "button.lang.en.confirm";
	public static final String TEXT_LANGUAGE_FR_CONFIRM = "button.lang.fr.confirm";
	public static final String TEXT_FORMAT_HD = "button.format.hd";
	public static final String TEXT_FORMAT_UHD = "button.format.uhd";
	public static final String TEXT_FORMAT_FHD = "button.format.hd_1080";
	public static final String TEXT_FORMAT_SD = "button.format.sd";
	public static final String TEXT_MODIFY = "button.modify";
	
	// titles
	/** The Constant TEXT_T_ADDWISHLIST_SUCCESS. */
	public static final String TEXT_T_ADDWISHLIST_SUCCESS = "title.titleAddWishlistSuccess";
	/** The Constant TEXT_T_DELWISHLIST_SUCCESS. */
	public static final String TEXT_T_DELWISHLIST_SUCCESS = "title.titleDelWishlistSuccess";
	/** The Constant TEXT_T_ORDER_COMPLETED. */
	public static final String TEXT_T_ORDER_COMPLETED = "title.titleOrderCompleted";
	/** The Constant TEXT_T_ENTER_PIN. */
	public static final String TEXT_T_ENTER_PIN = "title.titleEnterYourPin";
	/** The Constant TEXT_T_MY_WISHLIST. */
	public static final String TEXT_T_MY_WISHLIST = "title.titleWishlist";
	/** The Constant TEXT_T_MY_WISHLIST_18. */
	public static final String TEXT_T_MY_WISHLIST_18 = "title.titleWishlist18";
	/** The Constant TEXT_T_HOME. */
	public static final String TEXT_T_HOME = "title.titleHome";
	/** The Constant TEXT_T_RESUME_VIEWING. */
	public static final String TEXT_T_RESUME_VIEWING = "title.titleResumeViewing";
	/** The Constant TEXT_T_RESUME_VIEWING_18. */
	public static final String TEXT_T_RESUME_VIEWING_18 = "title.titleResumeViewing18";
	/** The Constant TEXT_T_PLAYLIST. */
	public static final String TEXT_T_PLAYLILST = "title.titlePlsylist";
	
	//R7.4
	public static final String TEXT_T_CONFIRM = "title.titleConfirm";

	// labels
	/** The Constant TEXT_LBL_MOVIE. */
	public static final String TEXT_LBL_MOVIE = "label.lblMovie";
	/** The Constant TEXT_LBL_MOVIE2. */
	public static final String TEXT_LBL_MOVIE2 = "label.lblMovie2";
	public static final String TEXT_LBL_MOVIE3 = "label.lblMovie3";
	/** The Constant TEXT_LBL_MOVIES. */
	public static final String TEXT_LBL_MOVIES = "label.lblMovies";
	/** The Constant TEXT_LBL_MOVIES2. */
	public static final String TEXT_LBL_MOVIES2 = "label.lblMovies2";
	/** The Constant TEXT_LBL_BUNDLE. */
	public static final String TEXT_LBL_BUNDLE = "label.lblBundle";
	/** The Constant TEXT_LBL_DAYS. */
	public static final String TEXT_LBL_DAYS = "label.lblDays";
	/** The Constant TEXT_LBL_HOURS. */
	public static final String TEXT_LBL_HOURS = "label.lblHours";
	/** The Constant TEXT_LBL_EPISODE. */
	public static final String TEXT_LBL_EPISODE = "label.lblEpisode";
	/** The Constant TEXT_LBL_EPISODE2. */
	public static final String TEXT_LBL_EPISODE2 = "label.lblEpisode2";
	public static final String TEXT_LBL_EPISODE3 = "label.lblEpisode3";
	/** The Constant TEXT_LBL_EPISODES. */
	public static final String TEXT_LBL_EPISODES = "label.lblEpisodes";
	/** The Constant TEXT_LBL_EPISODES2. */
	public static final String TEXT_LBL_EPISODES2 = "label.lblEpisodes2";
	/** The Constant TEXT_LBL_SEASON. */
	public static final String TEXT_LBL_SEASON = "label.lblSeason";
	/** The Constant TEXT_LBL_TRAILER. */
	public static final String TEXT_LBL_TRAILER = "label.lblTrailer";
	/** The Constant TEXT_LBL_TRAILERS. */
	public static final String TEXT_LBL_TRAILERS = "label.lblTrailers";
	/** The Constant TEXT_LBL_EXTRA. */
	public static final String TEXT_LBL_EXTRA = "label.lblExtra";
	public static final String TEXT_LBL_EXTRA3 = "label.lblExtra3";
	/** The Constant TEXT_LBL_EXTRAS. */
	public static final String TEXT_LBL_EXTRAS = "label.lblExtras";
	/** The Constant TEXT_LBL_CONTENT. */
	public static final String TEXT_LBL_CONTENT = "label.lblContent";
	public static final String TEXT_LBL_CONTENT3 = "label.lblContent3";
	/** The Constant TEXT_LBL_CONTENTS. */
	public static final String TEXT_LBL_CONTENTS = "label.lblContents";
	/** The Constant TEXT_LBL_FREE. */
	public static final String TEXT_LBL_FREE = "label.lblFree";
	/** The Constant TEXT_LBL_AVAILABLE. */
	public static final String TEXT_LBL_AVAILABLE = "label.lblAvailable";
	/** The Constant TEXT_LBL_AVAILABLE2. */
	public static final String TEXT_LBL_AVAILABLE2 = "label.lblAvailable2";
	/** The Constant TEXT_LBL_DIRECTOR. */
	public static final String TEXT_LBL_DIRECTOR = "label.lblDirector";
	/** The Constant TEXT_LBL_ACTOR. */
	public static final String TEXT_LBL_ACTOR = "label.lblActor";
	/** The Constant TEXT_LBL_NORMAL. */
	public static final String TEXT_LBL_NORMAL = "label.lblNormal";
	/** The Constant TEXT_LBL_WALL_POSTER. */
	public static final String TEXT_LBL_WALL_POSTER = "label.lblWallofPoster";
	/** The Constant TEXT_LBL_CARROUSEL. */
	public static final String TEXT_LBL_CARROUSEL = "label.lblCarrousel";
	/** The Constant TEXT_LBL_LIST. */
	public static final String TEXT_LBL_LIST = "label.lblList";
	/** The Constant TEXT_LBL_BLOCKED_TITLE. */
	public static final String TEXT_LBL_BLOCKED_TITLE = "label.lblBlockedTitle";
	/** The Constant TEXT_LBL_ORDER_COMPLETE_SEASON. */
	public static final String TEXT_LBL_ORDER_COMPLETE_SEASON = "label.lblOrderCompleteSeason";
	/** The Constant TEXT_LBL_ORDER_EPISODE_BUNDLE. */
	public static final String TEXT_LBL_ORDER_EPISODE_BUNDLE = "label.lblOrderMore1Episode";
	public static final String TEXT_LBL_ORDER_EPISODE_PACAKGE = "label.lblOrderEpisodePackage";
	
	/** The Constant TEXT_LBL_ORDER_SINGLE_EPISODE. */
	public static final String TEXT_LBL_ORDER_SINGLE_EPISODE = "label.lblOrderSingleEpisode";
	/** The Constant TEXT_LBL_EPISODE_BUNDLE. */
	public static final String TEXT_LBL_EPISODE_BUNDLE = "label.lblEpisodeBundle";
	/** The Constant TEXT_LBL_FULL. */
	public static final String TEXT_LBL_FULL = "label.lblFull";
	/** The Constant TEXT_LBL_INDIVIDUAL_EPISODE. */
	public static final String TEXT_LBL_INDIVIDUAL_EPISODE = "label.lblIndividualEpisode";
	/** The Constant TEXT_LBL_CLOSE_PREVIEW. */
	public static final String TEXT_LBL_CLOSE_PREVIEW = "label.lblClosePreview";
	/** The Constant TEXT_LBL_RENTAL. */
	public static final String TEXT_LBL_RENTAL = "label.lblRental";
	/** The Constant TEXT_LBL_RESUMEVIEWING_ORDER. */
	public static final String TEXT_LBL_RESUMEVIEWING_ORDER = "label.lblResumeViewingOrder";
	/** The Constant TEXT_LBL_WISHLIST_ORDER. */
	public static final String TEXT_LBL_WISHLIST_ORDER = "label.lblWishlistOrder";
	/** The Constant TEXT_LBL_CHANNEL. */
	public static final String TEXT_LBL_CHANNEL = "label.lblChannel";
	/** The Constant TEXT_LBL_STYLE. */
	public static final String TEXT_LBL_STYLE = "label.lblStyle";
	/** The Constant TEXT_LBL_SORT_NAME. */
	public static final String TEXT_LBL_SORT_NAME = "label.lblSortName";
	/** The Constant TEXT_LBL_SORT_DATE. */
	public static final String TEXT_LBL_SORT_DATE = "label.lblSortDate";
	/** The Constant TEXT_LBL_EVENT. */
	public static final String TEXT_LBL_EVENT = "label.lblEvent";
	/** The Constant TEXT_LBL_WITH. */
	public static final String TEXT_LBL_WITH = "label.lblWith";
	/** The Constant TEXT_LBL_SERIES. */
	public static final String TEXT_LBL_SERIES = "label.lblSeries";
	/** The Constant TEXT_LBL_SORT_PROD. */
	public static final String TEXT_LBL_SORT_PROD = "label.lblSortProd";
	/** The Constant TEXT_LBL_SORT_DEFAULT. */
	public static final String TEXT_LBL_SORT_DEFAULT = "label.lblSortDefault";
	
	//R5
	public static final String TEXT_LBL_ERROR_CODE = "label.lblErrorCode";
	public static final String TEXT_LBL_LIST_OF_CHANNEL = "label.listOfChannel";
	public static final String TEXT_LBL_MY_FAVORITE_CHANNELS = "label.myFavoriteChannels";
	public static final String TEXT_LBL_ALL_CHANNELS = "label.allChannels";
	public static final String TEXT_LBL_MY_CHANNELS = "label.myChannels";
	public static final String TEXT_LBL_COD = "label.COD";
	// R7.3
    public static final String TEXT_LBL_UNSUBSCRIBED_CHANNELS = "label.unsubscribedChannels";

	// message
	/** The Constant TEXT_MSG_ADDWISHLIST_SUCESS. */
	public static final String TEXT_MSG_ADDWISHLIST_SUCESS = "message.msgAddWishlistSuccess";
	/** The Constant TEXT_MSG_DELWISHLIST_SUCESS. */
	public static final String TEXT_MSG_DELWISHLIST_SUCESS = "message.msgDelWishlistSuccess";
	/** The Constant TEXT_MSG_STOP_POP. */
	public static final String TEXT_MSG_STOP_POP = "message.msgStopPop";
	/** The Constant TEXT_MSG_ENTER_PIN. */
	public static final String TEXT_MSG_ENTER_PIN = "message.msgEnterPin";
	/** The Constant TEXT_MSG_ORDER_TITLE_COMPLETED. */
	public static final String TEXT_MSG_ORDER_TITLE_COMPLETED = "message.msgOrderTitleSuccess";
	/** The Constant TEXT_MSG_TORRID_1. */
	public static final String TEXT_MSG_TORRID_1 = "message.msgTorrid1";
	/** The Constant TEXT_MSG_TORRID_2. */
	public static final String TEXT_MSG_TORRID_2 = "message.msgTorrid2";
	/** The Constant TEXT_MSG_SEARCH_1. */
	public static final String TEXT_MSG_SEARCH_1 = "message.msgSearch1";
	/** The Constant TEXT_MSG_SEARCH_2. */
	public static final String TEXT_MSG_SEARCH_2 = "message.msgSearch2";
	/** The Constant TEXT_MSG_RESUME_1. */
	public static final String TEXT_MSG_RESUME_1 = "message.msgResume1";
	/** The Constant TEXT_MSG_RESUME_1_. */
	public static final String TEXT_MSG_RESUME_1_ = "message.msgResume1_";
	/** The Constant TEXT_MSG_RESUME_2. */
	public static final String TEXT_MSG_RESUME_2 = "message.msgResume2";
	/** The Constant TEXT_MSG_RESUME_2_. */
	public static final String TEXT_MSG_RESUME_2_ = "message.msgResume2_";
	/** The Constant TEXT_MSG_RESUME_0. */
	public static final String TEXT_MSG_RESUME_0 = "message.msgResume0";
	/** The Constant TEXT_MSG_OPTION_1. */
	public static final String TEXT_MSG_OPTION_1 = "message.msgOption1";
	/** The Constant TEXT_MSG_OPTION_2. */
	public static final String TEXT_MSG_OPTION_2 = "message.msgOption2";
	/** The Constant TEXT_MSG_ABOUT_CH_1. */
	public static final String TEXT_MSG_ABOUT_CH_1 = "message.msgAboutCh1";
	/** The Constant TEXT_MSG_ABOUT_CH_2. */
	public static final String TEXT_MSG_ABOUT_CH_2 = "message.msgAboutCh2";
	/** The Constant TEXT_MSG_PROMO_EXPIRED. */
	public static final String TEXT_MSG_PROMO_EXPIRED = "message.msgPromoExpired";
	/** The Constant TEXT_MSG_MAIN_BOTTOM. */
	public static final String TEXT_MSG_MAIN_BOTTOM = "message.msgMainBottom";
	/** The Constant TEXT_MSG_MAIN_TOP. */
	public static final String TEXT_MSG_MAIN_TOP = "message.msgMainTop";
	/** The Constant TEXT_MSG_CHANGEVIEW1. */
	public static final String TEXT_MSG_CHANGEVIEW1 = "message.msgChangeView1";
	/** The Constant TEXT_MSG_CHANGEVIEW2. */
	public static final String TEXT_MSG_CHANGEVIEW2 = "message.msgChangeView2";
	/** The Constant TEXT_MSG_FILTER_PURCHASE. */
	public static final String TEXT_MSG_FILTER_PURCHASE = "message.msgFilterPurchse";
	/** The Constant TEXT_MSG_BLOCK_RATING. */
	public static final String TEXT_MSG_BLOCK_RATING = "message.msgBlockByRating";
	/** The Constant TEXT_MSG_ADULT_CATEGORY. */
	public static final String TEXT_MSG_ADULT_CATEGORY = "message.msgAdultCategory";
	/** The Constant TEXT_MSG_SEASON_AVAILABLE. */
	public static final String TEXT_MSG_SEASON_AVAILABLE = "message.msgSeasonAvailable";
	/** The Constant TEXT_MSG_SEASONS_AVAILABLE. */
	public static final String TEXT_MSG_SEASONS_AVAILABLE = "message.msgSeasonsAvailable";
	/** The Constant TEXT_MSG_EPISODE. */
	public static final String TEXT_MSG_EPISODE = "message.msgEpisode";
	/** The Constant TEXT_MSG_EPISODES. */
	public static final String TEXT_MSG_EPISODES = "message.msgEpisodes";
	/** The Constant TEXT_MSG_CHOOSE1. */
	public static final String TEXT_MSG_CHOOSE1 = "message.msgChoose1";
	/** The Constant TEXT_MSG_CHOOSE2. */
	public static final String TEXT_MSG_CHOOSE2 = "message.msgChoose2";
	/** The Constant TEXT_MSG_CHOOSE3. */
	public static final String TEXT_MSG_CHOOSE3 = "message.msgChoose3";
	/** The Constant TEXT_MSG_CHOOSE4. */
	public static final String TEXT_MSG_CHOOSE4 = "message.msgChoose4";
	/** The Constant TEXT_MSG_TO_EXIT. */
	public static final String TEXT_MSG_TO_EXIT = "message.msgToExit";
	/** The Constant TEXT_MSG_BUNDLE. */
	public static final String TEXT_MSG_BUNDLE = "message.msgBundle";
	/** The Constant TEXT_MSG_BUNDLE_AVAIL. */
	public static final String TEXT_MSG_BUNDLE_AVAIL = "message.msgBundleAvail";
	/** The Constant TEXT_MSG_FULL_SEASON. */
	public static final String TEXT_MSG_FULL_SEASON = "message.msgFullSeason";
	/** The Constant TEXT_MSG_ORDER_BUNDLE. */
	public static final String TEXT_MSG_ORDER_BUNDLE = "message.msgOrderBundle";
	/** The Constant TEXT_MSG_ORDER_EPISODE. */
	public static final String TEXT_MSG_ORDER_EPISODE = "message.msgOrderEpisode";
	/** The Constant TEXT_MSG_BLOCKED. */
	public static final String TEXT_MSG_BLOCKED = "message.msgBlocked";
	/** The Constant TEXT_MSG_SEE. */
	public static final String TEXT_MSG_SEE = "message.msgSee";
	/** The Constant TEXT_MSG_ALL. */
	public static final String TEXT_MSG_ALL = "message.msgAll";
	/** The Constant TEXT_MSG_TITLES. */
	public static final String TEXT_MSG_TITLES = "message.msgTitles";
	/** The Constant TEXT_MSG_SEE_ALL_CH1. */
	public static final String TEXT_MSG_SEE_ALL_CH1 = "message.msgSeeAllCh1";
	/** The Constant TEXT_MSG_SEE_ALL_CH2. */
	public static final String TEXT_MSG_SEE_ALL_CH2 = "message.msgSeeAllCh2";
	/** The Constant TEXT_MSG_SEE_ALL_CH3. */
	public static final String TEXT_MSG_SEE_ALL_CH3 = "message.msgSeeAllCh3";
	/** The Constant TEXT_MSG_RESUME. */
	public static final String TEXT_MSG_RESUME = "message.msgResume";
	/** The Constant TEXT_MSG_VIEWINGS. */
	public static final String TEXT_MSG_VIEWINGS = "message.msgViewings";
	/** The Constant TEXT_MSG_BUNDLE_AVAIL2. */
	public static final String TEXT_MSG_BUNDLE_AVAIL2 = "message.msgBundleAvail2";
	/** The Constant TEXT_MSG_THANK. */
	public static final String TEXT_MSG_THANK = "message.msgThank";
	/** The Constant TEXT_MSG_ORDER_COMPLETE. */
	public static final String TEXT_MSG_ORDER_COMPLETE = "message.msgOrderComplete";
	/** The Constant TEXT_MSG_PARENTAL_ENABLED. */
	public static final String TEXT_MSG_PARENTAL_ENABLED = "message.msgParentalEnabled";
	/** The Constant TEXT_MSG_PARENTAL_SUSPENDED. */
	public static final String TEXT_MSG_PARENTAL_SUSPENDED = "message.msgParentalSuspended";
	/** The Constant TEXT_MSG_NOT_SUBS. */
	public static final String TEXT_MSG_NOT_SUBS = "message.msgNotSubs";
	/** The Constant TEXT_MSG_TO_SUBS. */
	public static final String TEXT_MSG_TO_SUBS = "message.msgToSubs";
	/** The Constant TEXT_MSG_GOTO_WEB. */
	public static final String TEXT_MSG_GOTO_WEB = "message.msgGotoWeb";
	/** The Constant TEXT_MSG_OR. */
	public static final String TEXT_MSG_OR = "message.msgOr";
	/** The Constant TEXT_MSG_CALL. */
	public static final String TEXT_MSG_CALL = "message.msgCall";
	/** The Constant TEXT_MSG_SUBS_REQUIRED. */
	public static final String TEXT_MSG_SUBS_REQUIRED = "message.msgSubsRequired";
	/** The Constant TEXT_MSG_KARAOKE_SUBS_TIME. */
	public static final String TEXT_MSG_KARAOKE_SUBS_TIME = "message.msgKaraokeSubsTime";
	/** The Constant TEXT_MSG_KARAOKE_SUBS_TITLE. */
	public static final String TEXT_MSG_KARAOKE_SUBS_TITLE = "message.msgKaraokeSubsTitle";
	/** The Constant TEXT_MSG_KARAOKE_SUBS_WANT. */
	public static final String TEXT_MSG_KARAOKE_SUBS_WANT = "message.msgKaraokeSubsWant";
	/** The Constant TEXT_MSG_CHOOSE_TITLE. */
	public static final String TEXT_MSG_CHOOSE_TITLE = "message.msgChooseTitle";
	/** The Constant TEXT_MSG_CHOOSE_TITLE2. */
	public static final String TEXT_MSG_CHOOSE_TITLE2 = "message.msgChooseTitle2";
	/** The Constant TEXT_MSG_PROTECT_SETTING1. */
	public static final String TEXT_MSG_PROTECT_SETTING1 = "message.msgProtectSetting1";
	/** The Constant TEXT_MSG_PROTECT_SETTING2. */
	public static final String TEXT_MSG_PROTECT_SETTING2 = "message.msgProtectSetting2";
	/** The Constant TEXT_MSG_NO_WISH_TITLE. */
	public static final String TEXT_MSG_NO_WISH_TITLE = "message.msgNoWishTitle";
	/** The Constant TEXT_MSG_NO_WISH. */
	public static final String TEXT_MSG_NO_WISH = "message.msgNoWish";
	/** The Constant TEXT_MSG_NO_WISH_TITLE. */
	public static final String TEXT_MSG_NO_WISH_TITLE_18 = "message.msgNoWishTitle18";
	/** The Constant TEXT_MSG_NO_WISH. */
	public static final String TEXT_MSG_NO_WISH_18 = "message.msgNoWish18";
	/** The Constant TEXT_MSG_NO_PLAY_TITLE. */
	public static final String TEXT_MSG_NO_PLAY_TITLE = "message.msgNoPlayTitle";
	/** The Constant TEXT_MSG_NO_PLAY. */
	public static final String TEXT_MSG_NO_PLAY = "message.msgNoPlay";
	/** The Constant TEXT_MSG_UNBLOCK_PIN1. */
	public static final String TEXT_MSG_UNBLOCK_PIN1 = "message.msgUnblockPIN1";
	/** The Constant TEXT_MSG_UNBLOCK_PIN2. */
	public static final String TEXT_MSG_UNBLOCK_PIN2 = "message.msgUnblockPIN2";
	/** The Constant TEXT_MSG_PURCHASE_PIN1. */
	public static final String TEXT_MSG_PURCHASE_PIN1 = "message.msgPurchasePIN1";
	/** The Constant TEXT_MSG_PURCHASE_PIN2. */
	public static final String TEXT_MSG_PURCHASE_PIN2 = "message.msgPurchasePIN2";
	/** The Constant TEXT_MSG_SUSPEND_PIN1. */
	public static final String TEXT_MSG_SUSPEND_PIN1 = "message.msgSuspendPIN1";
	/** The Constant TEXT_MSG_SUSPEND_PIN2. */
	public static final String TEXT_MSG_SUSPEND_PIN2 = "message.msgSuspendPIN2";
	/** The Constant TEXT_MSG_RESTORE_PIN1. */
	public static final String TEXT_MSG_RESTORE_PIN1 = "message.msgRestorePIN1";
	/** The Constant TEXT_MSG_RESTORE_PIN2. */
	public static final String TEXT_MSG_RESTORE_PIN2 = "message.msgRestorePIN2";
	/** The Constant TEXT_MSG_ADULT_TITLE. */
	public static final String TEXT_MSG_ADULT_TITLE = "message.msgAdultTitle";
	/** The Constant TEXT_MSG_ADULT_DESC. */
	public static final String TEXT_MSG_ADULT_DESC = "message.msgAdultDesc";
	/** The Constant TEXT_MSG_ADULT_NOTE. */
	public static final String TEXT_MSG_ADULT_NOTE = "message.msgAdultNote";
	/** The Constant TEXT_MSG_OR. */
	public static final String TEXT_MSG_OR2 = "message.msgOR";
	/** The Constant TEXT_MSG_VISIT. */
	public static final String TEXT_MSG_VISIT = "message.msgVisit";
	/** The Constant TEXT_MSG_DIAL. */
	public static final String TEXT_MSG_DIAL = "message.msgDial";
	/** The Constant TEXT_MSG_ADMIN1. */
	public static final String TEXT_MSG_ADMIN1 = "message.msgAdmin1";
	/** The Constant TEXT_MSG_ADMIN2. */
	public static final String TEXT_MSG_ADMIN2 = "message.msgAdmin2";
	/** The Constant TEXT_MSG_ITEM_AVAILABLE. */
	public static final String TEXT_MSG_ITEM_AVAILABLE = "message.msgItemAvailable";
	/** The Constant TEXT_MSG_ITEMS_AVAILABLE. */
	public static final String TEXT_MSG_ITEMS_AVAILABLE = "message.msgItemsAvailable";
	
	// R5
	public static final String TEXT_MSG_NO_SIMILAR_CONTENT = "message.msgNoSimilarContent";
	public static final String TEXT_MSG_ALL_CONTENT = "message.msgAllContent";
	public static final String TEXT_MSG_BLOCK_CHANNEL = "message.msgBlockChannelPIN";
	public static final String TEXT_MSG_UNBLOCK_CHANNEL = "message.msgUnblockChannelPIN";
	public static final String TEXT_MSG_WISHLIST = "message.msgWishlist";
	public static final String TEXT_MSG_WISHLIST_ADULT_1 = "message.msgWishlistAdult_1";
	public static final String TEXT_MSG_WISHLIST_ADULT_2 = "message.msgWishlistAdult_2";
	public static final String TEXT_MSG_WISHLIST_NO1 = "message.msgWishlistNo1";
	public static final String TEXT_MSG_WISHLIST_NO2 = "message.msgWishlistNo2";
	// Bundling
	public static final String TEXT_MSG_WISHLIST_ADULT_NO1 = "message.msgWishlistAdultNo1";
	public static final String TEXT_MSG_WISHLIST_ADULT_NO2 = "message.msgWishlistAdultNo2";
	
	//R7.4
	public static final String TEXT_MSG_ORDER_CONFIRM	= "message.msgOrderConfirm";
	public static final String TEXT_MSG_FOR = "message.msgFor";
	public static final String TEXT_MSG_VERSION = "message.msgVersion";
	public static final String TEXT_MSG_IMPORTANT = "message.msgImportant";
	public static final String TEXT_MSG_IMPORTANT_3D1 = "message.msgImportant3DMsg1";
	public static final String TEXT_MSG_IMPORTANT_3D2 = "message.msgImportant3DMsg2";
	public static final String TEXT_MSG_SELECT_FORMAT = "message.msgSelectFormat";
	public static final String TEXT_MSG_SELECT_LANGUAGE = "message.msgSelectLanguage";
	public static final String TEXT_MSG_HIGH_DEFINITION = "message.msgHighDefinition";
	public static final String TEXT_MSG_STANDARD = "message.msgStandard";
	
	// options menu
	/** The Constant OPT_NAME. */
	public static final String OPT_NAME = "contextualOptions.optName";
	/** The Constant OPT_DATE. */
	public static final String OPT_DATE = "contextualOptions.optDate";
	/** The Constant OPT_DATE_WISHLIST. */
	public static final String OPT_DATE_WISHLIST = "contextualOptions.optDateWishlist";
	/** The Constant OPT_DATE_RESUME_VIEWING. */
	public static final String OPT_DATE_RESUME_VIEWING = "contextualOptions.optDateResumeViewing";
	/** The Constant OPT_ACTIVATE_PARENTAL. */
	public static final String OPT_ACTIVATE_PARENTAL = "contextualOptions.optActivateParental";
	/** The Constant OPT_APPLY_PARENTAL. */
	public static final String OPT_APPLY_PARENTAL = "contextualOptions.optApplyParental";
	/** The Constant OPT_REMOVE_PARENTAL. */
	public static final String OPT_REMOVE_PARENTAL = "contextualOptions.optRemoveParental";
	/** The Constant OPT_GO_HELP. */
	public static final String OPT_GO_HELP = "contextualOptions.optGoHelp";
	/** The Constant OPT_GO_SETTING. */
	public static final String OPT_GO_SETTING = "contextualOptions.optGoSetting";
	/** The Constant OPT_SORT. */
	public static final String OPT_SORT = "contextualOptions.optSort";
	/** The Constant OPT_PROD_YEAR. */
	public static final String OPT_PROD_YEAR = "contextualOptions.optProdYear";
	/** The Constant OPT_DEFAULT. */
	public static final String OPT_DEFAULT = "contextualOptions.optDefault";
	// R5
	public static final String OPT_ADD_FAVORITE_CHANNEL = "contextualOptions.addFavoriteChannel";
	public static final String OPT_REMOVE_FAVORITE_CHANNEL = "contextualOptions.removeFavoriteChannel";
	public static final String OPT_ADD_BLOCKED_CHANNEL = "contextualOptions.addBlockedChannel";
	public static final String OPT_REMOVE_BLOCKED_CHANNEL = "contextualOptions.removeBlockedChannel";
	
	public static final String MORE_CONTENT_TITLE = "data.moreContent.title";
	public static final String MORE_CONTENT_DESC = "data.moreContent.desc";

	// Bundling
	public static final String BTN_FREE_WATCH_NOW = "button.free.watch.now";
	public static final String LBL_NOW_PALING = "label.lbl.now.playing";
	public static final String LBL_FREE_EPISODE = "label.lbl.free.episode";
	public static final String LBL_NEXT_EPISODE = "label.lbl.next.episode";
	public static final String LBL_SUBSCRIBE_EXPLAIN = "label.lbl.subscribe.explain";
	public static final String LBL_VIEW_ALL_EPISODES	= "label.lbl.view.all.episodes";

	// R7.3
	public static final String LBL_WITHOUT_NEXT_EPISODE_SUBSCRIBE_EXPLAIN = "label.lbl.without.next.episode.subscribe.explain";
	public static final String LBL_EPISDOE_FOR_TEXT_WATCH = "label.episode.for.watch.now";
	public static final String LBL_EXTRA_FOR_TEXT_WATCH = "label.extra.for.watch.now";

	
	public static void preloadClasses() {
        java.util.ArrayList list = new java.util.ArrayList();
        list.add(com.videotron.tvi.illico.framework.effect.ArrowEffect.class);
        list.add(com.videotron.tvi.illico.framework.effect.FlipEffect.class);
        list.add(com.videotron.tvi.illico.framework.effect.MovingEffect.class);
        list.add(com.videotron.tvi.illico.framework.io.BinaryReader.class);
        list.add(com.videotron.tvi.illico.framework.io.URLRequestor.class);
        list.add(com.videotron.tvi.illico.ui.ImageLabel.class);
        list.add(com.videotron.tvi.illico.util.GraphicUtil.class);
        list.add(com.videotron.tvi.illico.util.WindowProperty.class);
        list.add(com.videotron.tvi.illico.vod.comp.KaraokeView.class);
        list.add(com.videotron.tvi.illico.vod.comp.MainToolsUI.class);
        list.add(com.videotron.tvi.illico.vod.comp.MenuTreeEffect.class);
        list.add(com.videotron.tvi.illico.vod.comp.MenuTreeUI.class);
        list.add(com.videotron.tvi.illico.vod.comp.ShowcaseView.class);
        list.add(com.videotron.tvi.illico.vod.controller.menu.UsageManager.class);
        list.add(com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtils.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.AvailableSources.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.BaseContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.BundleContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.BundleDetails.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Catalog.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Category.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Classification.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ContainerPointer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ContentDescription.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ContentRoot.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Country.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.EpisodeContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Error.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ExtraContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ExtraDetails.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Image.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.LanguageContent.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Offer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Period.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Person.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.PlatformService.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.PlatformServiceSpecific.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.PlatformValidity.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Playout.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Poster.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ProductionCountry.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ResizableImages.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Sample.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.SeasonContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.SeriesContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Showcase.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ShowcaseContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.ShowcaseDetails.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.StaffRef.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Topic.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.TopicPointer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Vcds.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.Versions.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.VideoContainer.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.VideoDetails.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.VideoGenreCode.class);
        list.add(com.videotron.tvi.illico.vod.data.vcds.type.VideoType.class);
        list.add(com.videotron.tvi.illico.vod.gui.BaseRenderer.class);
        list.add(com.videotron.tvi.illico.vod.gui.CategoryBackgroundRenderer.class);
        list.add(com.videotron.tvi.illico.vod.gui.MainToolsRenderer.class);
        list.add(com.videotron.tvi.illico.vod.gui.MenuTreeRenderer.class);
        if (Log.DEBUG_ON) {
            Log.printDebug("Resource.preloadClasses : " + list.size());
        }
    }
}

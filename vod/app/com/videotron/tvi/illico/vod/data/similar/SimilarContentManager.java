/**
 * 
 */
package com.videotron.tvi.illico.vod.data.similar;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.data.similar.type.SBody;
import com.videotron.tvi.illico.vod.data.similar.type.SHead;
import com.videotron.tvi.illico.vod.data.similar.type.SimilarContent;

/**
 * @author zestyman
 *
 */
public class SimilarContentManager {

	private static SimilarContentManager instance;
	private SHead lastHead;
	private SimilarContent[] similarArr;
	
	private Object lock = new Object();
	private Thread thread;

	public static String debugStr = "Similar : ";

	private SimilarContentManager() {

	}

	public static SimilarContentManager getInstance() {
		if (instance == null) {
			instance = new SimilarContentManager();
		}

		return instance;
	}

	public SHead getLastHead() {
		return lastHead;
	}

	public SimilarContent[] requestSimilarContents(final String contentId) {
		thread = new Thread() {
			public void run() {
				long serverStartTime;
				long serverEndTime;
				long parsingEndTime;
				
				lastHead = null;
				similarArr = null;
	
				final String[] proxyIPs = (String[]) SharedMemory.getInstance().get(SearchService.KEY_SEARCH_PROXY_IP);
				int[] proxyPorts = (int[]) SharedMemory.getInstance().get(SearchService.KEY_SEARCH_PROXY_PORT);
				final String backUrl = (String) SharedMemory.getInstance().get(SearchService.KEY_SEARCH_URL);
	
				if (Log.DEBUG_ON) {
					Log.printDebug("SimilarContentManager, requestSimilarContents, proxyIPs" + proxyIPs);
					if (proxyIPs != null) {
						for (int i = 0; i < proxyIPs.length; i++) {
							Log.printDebug("SimilarContentManager, requestSimilarContents, proxyIPs[" + i + "]=" + proxyIPs[i]);
						}
					}
					Log.printDebug("SimilarContentManager, requestSimilarContents, proxyPorts" + proxyPorts);
					if (proxyPorts != null) {
						for (int i = 0; i < proxyPorts.length; i++) {
							Log.printDebug("SimilarContentManager, requestSimilarContents, proxyPorts[" + i + "]="
									+ proxyPorts[i]);
						}
					}
					Log.printDebug("SimilarContentManager, requestSimilarContents, backUrl" + backUrl);
				}
				// "http://172.26.93.197/illicoservice/stateless/search/vod/similar?vodId=";
				
				// VDTRMASTER-5737
				String URL_HEADER = null;
				if (backUrl.endsWith("/")) {
					URL_HEADER = backUrl + "vod/similar?vodId=";
				} else {
					URL_HEADER = backUrl + "/vod/similar?vodId=";
				}
				String URL_SUBFIX = "&language=" + Constants.LANGUAGES[1 - Resources.currentLanguage] + "&services="
						+ CommunicationManager.getInstance().getAuthorizedPackage2();
				try {
					serverStartTime = System.currentTimeMillis();
					byte[] src = null;
					String targetURL = URL_HEADER + contentId + URL_SUBFIX;
					if (Environment.EMULATOR) {
						// src = URLRequestor.getBytes(URL_HEADER + contentId +
						// URL_SUBFIX, null);
						src = BinaryReader.read(new File("resource/similar.txt"));
					} else {
						// src = URLRequestor.getBytes("10.247.209.97", 8000, URL_HEADER
						// + contentId + URL_SUBFIX, null);

						if (proxyIPs != null && proxyPorts != null) {
							src = getBytes(proxyIPs, proxyPorts, targetURL);
						} else {
							src = getBytes(targetURL);
						}
					}
					// byte[] src = URLRequestor.getBytes("10.247.209.97", 8000,
					// URL_HEADER + contentId + URL_SUBFIX, null);
					serverEndTime = System.currentTimeMillis();
					String jsonStr = new String(src, "UTF-8");

					if (Log.DEBUG_ON) {
						Log.printDebug("SimilarContentManager, requestSimilarContents, jsonStr=" + jsonStr);
					}

					JSONParser jsonParser = new JSONParser();

					JSONObject jObj = (JSONObject) jsonParser.parse(jsonStr);

					SHead head = new SHead();
					head.parseJson(jObj.get("head"));
					lastHead = head;
					if (Log.DEBUG_ON) {
						Log.printDebug("SimilarContentManager, requestSimilarContents, head.getError()=" + head.getError());
					}
					if (head.getError() != null) {
						similarArr = null;
					} else {
						SBody body = new SBody();
						body.parseJson(jObj.get("body"));

						similarArr = body.getSimilarContents();
					}
					parsingEndTime = System.currentTimeMillis();

					debugStr = "Similar : requestTime=" + (serverEndTime - serverStartTime) + ", parsingTime="
							+ (parsingEndTime - serverEndTime);
					
				} catch (IOException e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					similarArr = new SimilarContent[0];
				} catch (ParseException e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					similarArr = new SimilarContent[0];
				} catch (InterruptedException e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					return;
				} catch (Exception e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					similarArr = new SimilarContent[0];
				}
				
				synchronized (lock) {
					lock.notifyAll();
				}
			}
		};
		
		thread.start();
		
		synchronized (lock) {
			try {
				lock.wait(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContentManager, requestSimilarContents, after wait");
		}
		
		if (thread != null && thread.isAlive() && similarArr == null && lastHead == null) {
			similarArr = new SimilarContent[0];
			thread.interrupt();
			thread = null;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContentManager, requestSimilarContents, similarArr=" + similarArr);
		}
		
		return similarArr;
	}
	
	/**
     * Gets bytes from URL.
     *
     * @param proxyHost IP or DNS of proxy server, if null don't use proxy
     * @param proxyPort port of proxy server
     * @param locator path of source file
     * @return an array of byte[] of return, or null of file not found.
     */
    public static byte[] getBytes(String proxyHost, int proxyPort, String locator) throws IOException {
        String path = locator;
        if (Log.INFO_ON) {
            Log.printInfo("SimilarContentManager, getBytes path = " + path);
        }
        URL url = null;
        if (proxyHost != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("SimilarContentManager, getBytes proxy:port = " + proxyHost + ":" + proxyPort);
            }
        	url = new URL("HTTP", proxyHost, proxyPort, path);
        } else {
        	url = new URL(path);
        }

		// R7.2
		String urlStr = CatalogueDatabase.SDF.format(new Date()) + ", " + url.toString();
		if (BaseRenderer.fmClock.stringWidth(urlStr) > 860) {
			String[] splitStr = TextUtil.split(urlStr, BaseRenderer.fmClock, 860);
			if (splitStr != null) {
				for (int i = splitStr.length - 1; i > -1; i--) {
					MenuController.getInstance().debugList[5].add(splitStr[i]);
				}
			}
		} else {
			MenuController.getInstance().debugList[5].add(urlStr);
		}

        URLConnection con = url.openConnection();
        if (Log.INFO_ON) {
            if (con instanceof HttpURLConnection) {
                HttpURLConnection hc = (HttpURLConnection) con;
                Log.printInfo("SimilarContentManager, getBytes response = " + hc.getResponseCode());
                Log.printInfo("SimilarContentManager, getBytes : " + hc.getResponseMessage());
            }
        }

        BufferedInputStream bis = null;
        if (con instanceof HttpURLConnection && ((HttpURLConnection) con).getResponseCode() != HttpURLConnection.HTTP_OK) {
        	if (Log.DEBUG_ON) {
                Log.printDebug("SimilarContentManager, getBytes, open errorStream");
            }
        	bis = new BufferedInputStream(((HttpURLConnection) con).getErrorStream());
        } else {
        	if (Log.DEBUG_ON) {
                Log.printDebug("SimilarContentManager, getBytes, open normalStream");
            }
        	bis = new BufferedInputStream(con.getInputStream());
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[2048];
        int c;
        while ((c = bis.read(buf)) != -1) {
            baos.write(buf, 0, c);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("URLRequestor.getBytes read bytes = " + baos.size());
        }
        if (con instanceof HttpURLConnection) {
            ((HttpURLConnection) con).disconnect();
        }
        bis.close();
        return baos.toByteArray();
    }

    /**
     * Gets bytes from URL.
     *
     * @param proxyHosts IP or DNS of proxy server, if null don't use proxy
     * @param proxyPorts port of proxy server
     * @param locator path of source file
     * @return an array of byte[] of return, or null of file not found.
     */
    public byte[] getBytes(String[] proxyHosts, int[] proxyPorts, String locator) throws IOException {
        if (proxyHosts == null || proxyHosts.length == 0) {
            return getBytes(null, 0, locator);
        }
        for (int i = 0; i < proxyHosts.length; i++) {
            try {
                return getBytes(proxyHosts[i], proxyPorts[i], locator);
            } catch (IOException ex) {
                Log.printWarning(ex.getClass() + " / " + proxyHosts[i] + ":" + proxyPorts[i]);
            }
        }
        throw new IOException("no avaliable proxy");
    }

    /**
     * Gets bytes from URL.
     *
     * @param locator path of source file
     * @return an array of byte[] of return, or null of file not found.
     */
    public byte[] getBytes(String locator) throws IOException {
    	return getBytes(null, 0, locator);
    }
}

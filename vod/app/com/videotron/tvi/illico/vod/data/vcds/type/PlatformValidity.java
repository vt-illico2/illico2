package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class PlatformValidity extends BaseElement {
	int position;
	public String platformName;
	public Period validityPeriod;
	
	public void setAttribute(Attributes attributes) {
		try {
			position = Integer.parseInt(attributes.getValue(AT_POSITION));
		} catch (NumberFormatException nfe) {}
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_VALIDITY_PERIOD.equals(qName)) {
			validityPeriod = (Period) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_PLATFORM_NAME.equals(qName)) {
			platformName = data;
		}
	}

	public void dispose() {
		platformName = null;
	}
}

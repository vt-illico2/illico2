package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class ExtraDetails extends BaseElement {
	public Video[] video = new Video[0]; // or ref
	public String[] videoRef = new String[0];
	
	public String mainContentItem;

	public String getGenre() {
		if (video.length > 0) {
			return video[0].getGenre();
		}
		return NA;
	}
	
	public boolean hasFrench() {
		if (video.length > 0) {
			return video[0].hasFrench();
		}
		return false;
	}
	
	public boolean hasEnglish() {
		if (video.length > 0) {
			return video[0].hasEnglish();
		}
		return false;
	}
	
	public boolean hasClosedCaption() {
		if (video.length > 0) {
			return video[0].hasClosedCaption();
		}
		return false;
	}

	public String getRating() {
		int r = -1, r2;
		if (video.length > 0) {
			for (int i = 0; i < video.length; i++) {
				r2 = ratingToInt(video[i].getRating());
				if (r < r2) {
					r = r2;
				}
			}
		}
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		mainContentItem = attributes.getValue(AT_MAIN_CONTENT_ITEM);
	}

	public void endElement(String qName, BaseElement element) {
		if (EL_VIDEO.equals(qName)) {
			video = (Video[]) addArray(video, Video.class, element);
		}
	}

	public void setDataAt(String qName, String data) {
		if (EL_VIDEO_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				video = (Video[]) addArray(video, Video.class, cached);
			} else {
				videoRef = (String[]) addArray(videoRef, String.class, data);
			}
		}
	}

	public void dispose() {
		for (int i = 0; i < video.length; i++) {
			video[i].dispose();
			video[i] = null;
		}
		// video = null;
	}
}

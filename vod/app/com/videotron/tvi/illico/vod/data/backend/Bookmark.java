package com.videotron.tvi.illico.vod.data.backend;

import java.util.Date;

public class Bookmark {
	private int responseCode;
	private int errorCode = -1;
	private String bookmarkID;
	private boolean isAssetPurchased;
	private int currentNPT;
	private String orderableName;
	private int leaseRemaing;
	private long leaseEnd;
	public int idxOfBundle; // jira 3975

	public String toString() {
		return orderableName + "|" + bookmarkID + "|" + errorCode + "|" + currentNPT
				+ "|" + (new Date(leaseEnd));
	}

	/**
	 * @return the bookmarkID
	 */
	public String getBookmarkID() {
		return bookmarkID;
	}

	/**
	 * @param bookmarkID
	 *            the bookmarkID to set
	 */
	public void setBookmarkID(String bookmarkID) {
		this.bookmarkID = bookmarkID;
	}

	/**
	 * @return the currentNPT
	 */
	public int getCurrentNPT() {
		return currentNPT;
	}

	/**
	 * @param currentNPT
	 *            the currentNPT to set
	 */
	public void setCurrentNPT(int currentNPT) {
		this.currentNPT = currentNPT;
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the isAssetPurchased
	 */
	public boolean isAssetPurchased() {
		return isAssetPurchased;
	}

	/**
	 * @param isAssetPurchased
	 *            the isAssetPurchased to set
	 */
	public void setAssetPurchased(boolean isAssetPurchased) {
		this.isAssetPurchased = isAssetPurchased;
	}

	/**
	 * @return the leaseRemaing
	 */
	public int getLeaseRemaing() {
		return leaseRemaing;
	}

	/**
	 * @param leaseRemaing
	 *            the leaseRemaing to set
	 */
	public void setLeaseRemaing(int leaseRemaing) {
		this.leaseRemaing = leaseRemaing;
	}

	/**
	 * @return the responseCode
	 */
	public int getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the orderableName
	 */
	public String getOrderableName() {
		return orderableName;
	}

	/**
	 * @param orderableName
	 *            the orderableName to set
	 */
	public void setOrderableName(String orderableName) {
		this.orderableName = orderableName;
	}

	/**
	 * @return the end time of lease
	 */
	public long getLeaseEnd() {
		return leaseEnd;
	}

	/**
	 * @param leaseEnd
	 *            the end time of lease
	 */
	public void setLeaseEnd(long leaseEnd) {
		this.leaseEnd = leaseEnd;
	}
}

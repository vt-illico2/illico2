package com.videotron.tvi.illico.vod.data.vcds.type;

import java.awt.Rectangle;
import java.util.Date;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;

public class MoreDetail extends BaseElement {
	private static Date dummyDate = new Date(0L);

	public String getId() {
		return null;
	}

	public String getTitle() {
		return null;
	}

//	public java.awt.Image getImage() {
//		return null;
//	}
	
	public java.awt.Image getImage(int width, int height) {
		return null;
	}
	
	public java.awt.Image getImage(Object desc, int width, int height) {
		return getImage(desc, width, height, null);
	}
	public java.awt.Image getImage(Object desc, int width, int height, String usage) {
		if (desc == null) { // data is not exist
			return ImageRetriever.getDefault(width, height);
		}
		ImageGroup[] imageGroups = null;
		if (desc instanceof Description) {
			imageGroups = ((Description) desc).imageGroup;
		} else if (desc instanceof ContentDescription) {
			imageGroups = ((ContentDescription) desc).imageGroup;
		}

		// data is not exist
		if (imageGroups == null || imageGroups.length == 0) {
			return ImageRetriever.getDefault(width, height);
		}
		
		// try find exact size
		Image img = chooseImage(imageGroups, width, height, usage);
		java.awt.Image ret = null;
		if (img != null) {
			// try find poster on pool
			ret = CatalogueDatabase.getInstance().getImage(img, width, height);
			if (ret != null) { // already cached
				return ret;
			}
		}
		// it has no exact data
		// or data is exist but not loaded yet
		// try find alternate size on poster pool
		Rectangle[] alter = ImageRetriever.getAlterSize(width, height);
		if (alter == null) {
			return null;
		}
		for (int i = 0; i < alter.length; i++) {
			img = chooseImage(imageGroups, alter[i].width, alter[i].height, usage);
			if (img == null) continue;
			ret = CatalogueDatabase.getInstance().getImage(img, alter[i].width, alter[i].height);
			if (ret == null) continue;
			return ret; // found it
		}
		
		// alter size isn't exist or not cached
		return ImageRetriever.getDefault(width, height);
	}

	public boolean isPortrait() {
		return true;
	}

	public Object chooseImage() {
		return null;
	}

	public String getDescription() {
		return "";
	}

	public String getProductionYear() {
		return "";
	}

	public String getDirector(int max) {
		return "";
	}

	public String getActors(int max) {
		return "";
	}

	public long getRuntime() {
		return 0L;
	}

	public long getLeaseDuration() {
		return 0L;
	}

	public String getGenre() {
		return "";
	}

	public String getCountry(int max) {
		return "";
	}

	public String getRating() {
		return "";
	}
	
	public String getRatingInfo() {
		return "";
	}

	public boolean hasEnglish() {
		return false;
	}

	public boolean hasFrench() {
		return false;
	}

	public boolean hasClosedCaption() {
		return false;
	}

	public Offer getOfferSD(String lang) {
		return null;
	} // if lang is null, it means any lang

	public Offer getOfferHD(String lang) {
		return null;
	}

	public String getEndPeriod() {
		return "";
	}

	public boolean inBundle() {
		return false;
	}

	public boolean hasTrailer() {
		return false;
	}

	public boolean is3screen() {
		return false;
	}
	
	public int getFlag() {
		return -1;
	}

	public Date getStart() {
		return dummyDate;
	}

	public void dispose() {
	}

}

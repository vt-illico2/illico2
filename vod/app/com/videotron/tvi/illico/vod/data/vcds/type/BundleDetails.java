package com.videotron.tvi.illico.vod.data.vcds.type;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class BundleDetails extends BaseElement {
	public Bundle[] bundle = new Bundle[0]; // or ref
	public String[] bundleRef = new String[0];

	public Video[] video = new Video[0]; // or ref
	public String[] videoRef = new String[0];

	public Episode[] episode = new Episode[0]; // or ref
	public String[] episodeRef = new String[0];

	public String getGenre() {
		if (episode.length > 0) {
			return episode[0].getGenre();
		}
		if (video.length > 0) {
			return video[0].getGenre();
		}
		return NA;
	}
	
	public boolean hasFrench() {
		if (episode.length > 0) {
			return episode[0].hasFrench();
		}
		if (video.length > 0) {
			return video[0].hasFrench();
		}
		return false;
	}
	
	public boolean hasEnglish() {
		if (episode.length > 0) {
			return episode[0].hasEnglish();
		}
		if (video.length > 0) {
			return video[0].hasEnglish();
		}
		return false;
	}
	
	public boolean hasClosedCaption() {
		if (episode.length > 0) {
			return episode[0].hasClosedCaption();
		}
		if (video.length > 0) {
			return video[0].hasClosedCaption();
		}
		return false;
	}

	public String getRating() {
		int r = -1, r2;
		if (episode.length > 0) {
			for (int i = 0; i < episode.length; i++) {
				r2 = ratingToInt(episode[i].getRating());
				if (r < r2) {
					r = r2;
				}
			}
			return intToRating(r);
		}
		if (video.length > 0) {
			for (int i = 0; i < video.length; i++) {
				r2 = ratingToInt(video[i].getRating());
				if (r < r2) {
					r = r2;
				}
			}
		}
		return null;
	}

	public void endElement(String qName, BaseElement element) {
		if (EL_BUNDLE.equals(qName)) {
			bundle = (Bundle[]) addArray(bundle, Bundle.class, element);
			Bundle last = (Bundle) element;
			for (int i = 0; i < last.bundleDetail.episode.length; i++) {
				episode = (Episode[]) addArray(episode, Episode.class, last.bundleDetail.episode[i]);
			}
			for (int i = 0; i < last.bundleDetail.video.length; i++) {
				video = (Video[]) addArray(video, Video.class, last.bundleDetail.video[i]);
			}
		} else if (EL_VIDEO.equals(qName)) {
			video = (Video[]) addArray(video, Video.class, element);
		} else if (EL_EPISODE.equals(qName)) {
			episode = (Episode[]) addArray(episode, Episode.class, element);
		}
	}

	public void setDataAt(String qName, String data) {
		if (EL_BUNDLE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				bundle = (Bundle[]) addArray(bundle, Bundle.class, cached);
				Bundle last = (Bundle) cached;
				for (int i = 0; i < last.bundleDetail.episode.length; i++) {
					episode = (Episode[]) addArray(episode, Episode.class, last.bundleDetail.episode[i]);
				}
				for (int i = 0; i < last.bundleDetail.video.length; i++) {
					video = (Video[]) addArray(video, Video.class, last.bundleDetail.video[i]);
				}
			} else {
				bundleRef = (String[]) addArray(bundleRef, String.class, data);
			}
		} else if (EL_VIDEO_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				video = (Video[]) addArray(video, Video.class, cached);
			} else {
				videoRef = (String[]) addArray(videoRef, String.class, data);
			}
		} else if (EL_EPISODE_ID.equals(qName) || EL_EPISODE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				episode = (Episode[]) addArray(episode, Episode.class, cached);
			} else {
				episodeRef = (String[]) addArray(episodeRef, String.class, data);
			}
		}
	}
	
	public void sort() {
		// sort episodes by episode number
		Episode temp;
		for (int i = 0; i < episode.length; i++) {
			for (int j = i+1; j < episode.length; j++) {
				if (episode[i].episodeNumber > episode[j].episodeNumber) {
					temp = episode[i];
					episode[i] = episode[j];
					episode[j] = temp;
				}
			}
		}
		temp = null;
	}

	public void dispose() {
		for (int i = 0; i < bundle.length; i++) {
			bundle[i].dispose();
			bundle[i] = null;
		}
		// bundle = null;
		for (int i = 0; i < episode.length; i++) {
			episode[i].dispose();
			episode[i] = null;
		}
		// episode = null;
		for (int i = 0; i < video.length; i++) {
			video[i].dispose();
			video[i] = null;
		}
		// video = null;
	}
}

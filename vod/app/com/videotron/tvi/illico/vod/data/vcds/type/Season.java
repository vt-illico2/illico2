package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Season extends MoreDetail implements HasDupNamed, CacheElement {
	public String id;
	public String version;
	public boolean isAllScreenType;
	public int seasonNumber;
	public int numberOfEpisodes;
	public ContentDescription description;
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	public Service service;
	public Service[] services = new Service[0];
	public Series series; // or ref
	public String seriesRef;
	public Episode[] episode = new Episode[0];
	public String episodeRefs;
	public String notListed;
	public String inBundles;
	public String[] inBundlesAR;
	public AvailableSources availableSources; 
	
	public boolean canBuyBundle;
	public boolean canBuySeason;
	public boolean canBuyEpisode;
	public String[] episodeBundles = new String[0];
	public String seasonBundle;
	
	public Section[] sections = new Section[0];
	
	private boolean reset = false;
	
	//public StaffRef[] actor = new StaffRef[0];
	//public VideoGenreCode genre; // or ref
	//public String genreRef;
	//public Classification classification;
	public String[] episodeIds = new String[0];
	
	// R5
	public Channel channel;
	public Network network;
	
	public void loadLogo() {
		if (Log.EXTRA_ON) {
			Log.printDebug("loadLogo, availableSources=" + availableSources);
			if (availableSources != null) {
				Log.printDebug("loadLogo, availableSources.availableSource=" + (availableSources.availableSource != null ? availableSources.availableSource.length : -1));
			}
		}
		
		for (int i = 0; availableSources != null && i < availableSources.availableSource.length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("loadLogo, availSource[" + i + "]=" + availableSources.availableSource[i].toString(true));
			}
			if (availableSources.availableSource[i].network != null) {
				availableSources.availableSource[i].network.loadLogo(true, false);
			} else if (availableSources.availableSource[i].channel != null) {
				availableSources.availableSource[i].channel.loadLogo(true, false);
			} else if (availableSources.availableSource[i].service != null && availableSources.availableSource[i].service.description != null) {
				availableSources.availableSource[i].service.description.loadLogo(true, false);
			}
		}
	}
	
	public int episodeSize() { return episode.length > 0 ? episode.length : episodeIds.length; }
	public String getId() { return id; }
	public String getTitle() { 
		String title = description == null ? NA : getLanguageContentData(description.title); 
//		if (Log.EXTRA_ON) {
//			title = id + "|" + title;
//		}
		return title;
	}
	
	// R5
	public String getLongTitle() {
		String title = description == null ? NA : getLanguageContentData(description.longTitle);
		
		return title;
	}

	public java.awt.Image getImage(int width, int height) {
		return getImage(description, width, height);
	}
	public Object chooseImage() { 
		if (description == null) {
			return null;
		}
		if (description.imageGroup.length > 0) {
			return chooseImageGroup(description.imageGroup);
		}
		return null;
//		return chooseImage(description.poster); 
	}
	public boolean isPortrait() { 
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		return false;
	}
	public String getDescription() { return description == null ? NA : getLanguageContentData(description.description); }
	public String getProductionYear() { return description == null ? NA : (description.isSpecialEvent() ? "" : description.productionYear); }
	public String getGenre() { return description == null ? NA : description.getGenre(); }
	public String getActors(int max) { return description == null ? "" : description.getActors(max); }
	public boolean hasEnglish() { return description.hasEnglish(); }
	public boolean hasFrench() { return description.hasFrench(); }
	public String getRating() { return description == null ? NA : description.getRating(); }
	public boolean hasClosedCaption() { return description.isClosedCaptioned; }
	public boolean is3screen() {
		return isAllScreenType;
//		if (notListed == null) return true;
//		if (notListed.length() == 0) return true;
//		if ("i1".equalsIgnoreCase(notListed)) return true;
//		return false;
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return ContentDescription.class;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class;
		}
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		version = attributes.getValue(AT_VERSION);
		try {
			seasonNumber = Integer.parseInt(attributes.getValue(AT_SEASON_NUMBER));
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		try {
			numberOfEpisodes = Integer.parseInt(attributes.getValue(AT_NUMBER_OF_EPISODES));
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		isAllScreenType = Boolean.valueOf(attributes.getValue(AT_IS_ALL_SCREEN_TYPE)).booleanValue();
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			if (description != null) {
				description.updateWith((ContentDescription) element);
			} else {
				description = (ContentDescription) element;
			}
			description.sort();
		} else if (EL_SERIES.equals(qName)) {
			series = (Series) element;
		} else if (EL_EPISODE.equals(qName)) {
			if (reset) { realReset(); }
			episode = (Episode[]) addArray(episode, Episode.class, element);
//		} else if (EL_ACTOR.equals(qName)) {
//			actor = (StaffRef[]) addArray(actor, StaffRef.class, element);
//		} else if (EL_CLASSIFICATION.equals(qName)) {
//			classification = (Classification) element;
//		} else if (EL_GENRE.equals(qName)) {
//			genre = (VideoGenreCode) element;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		} else if (EL_SERVICE.equals(qName)) {
			services = (Service[]) addArray(services, Service.class, element);
			
			if (service == null || ((Service)element).serviceType == Service.VOD) {
				service = (Service) element;
			}
		} else if (EL_SECTION.equals(qName)) {
			if (reset) { realReset(); }
			((Section) element).setParent(this);
			sections = (Section[]) addArray(sections, Section.class, element);
		} else if (EL_AVAILABLE_SOURCES.equals(qName)) {
			availableSources = (AvailableSources) element;
			for (int i = 0; i < availableSources.availableSource.length; i++) {
				availableSources.availableSource[i].parent = this;
			}
		} else if (EL_CHANNEL.equals(qName)) {
			channel = (Channel) element;
		} else if (EL_NETWORK.equals(qName)) {
			network = (Network) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_SERIES_ID.equals(qName) || EL_SERIES_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				series = (Series) cached;
			} else {
				seriesRef = data;
			}
		} else if (EL_EPISODE_REFS.equals(qName)) {
			if (reset) { realReset(); }
			String[] ref = TextUtil.tokenize(data, ' ');
			for (int i = 0; i < ref.length; i++) {
				Object cached = CatalogueDatabase.getInstance().getCached(data);
				if (cached != null) {
					episode = (Episode[]) addArray(episode, Episode.class, cached);
				}
			}
//			Object cached = CatalogueDatabase.getInstance().getCached(data);
//			if (cached != null) {
//				episode = (Episode[]) addArray(episode, Episode.class, cached);
//			} else {
//				episodeRefs = data;
//			}
		} else if (EL_IN_BUNDLES.equals(qName)) {
			inBundles = data;
			inBundlesAR = TextUtil.tokenize(data, ' ');
//		} else if (EL_GENRE_REF.equals(qName)) {
//			Object cached = CatalogueDatabase.getInstance().getCached(data);
//			if (cached != null) {
//				genre = (VideoGenreCode) cached;
//			} else {
//				genreRef = data;
//			}
		} else if (EL_EPISODE_ID.equals(qName)) {
			if (reset) { realReset(); }
			episodeIds = (String[]) addArray(episodeIds, String.class, data);
		} else if (EL_NOT_LISTED.equals(qName)) {
			notListed = data;
		} else if (EL_CHANNEL_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				channel = (Channel) cache;
			}
		} else if (EL_NETWORK_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				network = (Network) cache;
			}
		}
	}
	
	public void register() {
		// sort episodes by episode number
		Episode temp;
		for (int i = 0; i < episode.length; i++) {
			for (int j = i+1; j < episode.length; j++) {
				if (episode[i].episodeNumber > episode[j].episodeNumber) {
					temp = episode[i];
					episode[i] = episode[j];
					episode[j] = temp;
				}
			}
		}
		temp = null;
		
		// check buy option
		canBuyEpisode = false;
		canBuyBundle = episode.length > 0;
		boolean existInBundleOnly = false;
		for (int i = 0; i < episode.length; i++) {
			if (episode[i].inBundlesAR == null || episode[i].inBundlesAR.length == 0) { // not in any bundle
				canBuyBundle = false;
				//break;
			}
			if (episode[i].inBundleOnly == false) {
				canBuyEpisode = true;
			} else {
				existInBundleOnly = true;
			}
		}
		
		if (existInBundleOnly) {
			canBuyBundle = true;
		}
		
		if (canBuyBundle) {
			String bundleId;
			for (int i = 0; i < episode[0].inBundlesAR.length; i++) {
				canBuySeason = true;
				bundleId = episode[0].inBundlesAR[i];
				for (int j = 1; j < episode.length; j++) {
					boolean found = false;
					for (int k = 0; episode[j].inBundlesAR != null && k < episode[j].inBundlesAR.length; k++) {
						if (bundleId.equals(episode[j].inBundlesAR[k])) {
							found = true;
							break;
						}
					}
					if (found == false) {
						canBuySeason = false;
						break;
					}
				}
				if (canBuySeason) {
					seasonBundle = bundleId;
					break;
				}
			}
			for (int i = 0; i < episode.length; i++) {
				for (int j = 0; episode[i].inBundlesAR != null && j < episode[i].inBundlesAR.length; j++) {
					bundleId = episode[i].inBundlesAR[j];
					if (seasonBundle == null || seasonBundle.equals(bundleId) == false) {
						episodeBundles = (String[]) addArray(episodeBundles, String.class, bundleId);
					}
				}
			}
		}
		if (canBuySeason) {
			boolean allInOne = true;
			for (int i = 0; i < episode.length; i++) {
				if (episode[i].inBundlesAR != null && episode[i].inBundlesAR.length > 1) {
					allInOne = false;
					break;
				}
			}
			if (allInOne) { // every episodes are in one bundle, it means it can be purchased by season only
				canBuyBundle = false;
			}
		}
		
		CatalogueDatabase.getInstance().register(id, this);
	}
	
	public void reset() {
		reset = true;
	}
	
	private void realReset() {
		reset = false;
		episode = new Episode[0];
		episodeBundles = new String[0];
		episodeIds = new String[0];
		sections = new Section[0];
	}
	
	public String toString() {
		return id;
	}
	
	public String toString(boolean detail) {
		if (detail) {
			if (description == null) {
				return "Season(" + id + ") = NO description, ep = " + episode.length 
						+ ", sec = " + sections.length;
			}
			return "Season(" + id + ") = " + getLanguageContentData(this.description.title)
				+ ", ep = " + episode.length + ", sec = " + sections.length;
		}
		if (description == null) {
			return "Season(" + id + ") = NO description";
		}
		return "Season(" + id + ") = " + getLanguageContentData(this.description.title);
	}
	
	public String dumpContent(String prefix) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < episode.length; i++) {
			buf.append(prefix);
			buf.append(episode[i].toString(true));
			buf.append("\n");
		}
		return buf.toString();
	}
	
	private boolean dispose = false;
	public void dispose() {
		if (dispose) return;
		dispose = true;
		
		id = null;
		version = null;
		if (description != null) description.dispose();
		description = null;
		if (platformService != null) platformService.dispose();
		platformService = null;
//		if (series != null) series.dispose();
		series = null;
		for (int i = 0; episode != null && i < episode.length; i++) {
			if (episode[i] != null) episode[i].dispose();
			episode[i] = null;
		}
//		episode = null;
		inBundles = null;
		inBundlesAR = null;
	}
	
	public static Season MORE_CONTENT = new Season();
	static {
		MORE_CONTENT.description = new ContentDescription();
		MORE_CONTENT.description.title = new LanguageContent[] {
				new LanguageContent("en", DataCenter.getInstance().getString(Resources.MORE_CONTENT_TITLE + "_en")),
				new LanguageContent("fr", DataCenter.getInstance().getString(Resources.MORE_CONTENT_TITLE + "_fr")),
		};
		MORE_CONTENT.description.description = new LanguageContent[] {
				new LanguageContent("en", DataCenter.getInstance().getString(Resources.MORE_CONTENT_DESC + "_en")),
				new LanguageContent("fr", DataCenter.getInstance().getString(Resources.MORE_CONTENT_DESC + "_fr")),
		};
		
		MORE_CONTENT.id = "Other seasons";
	}
}

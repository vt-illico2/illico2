package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Person extends BaseElement implements CacheElement {
	public String id;
	public String name = "";
	
	public String getId() { return id; }
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
	}
	
	public void setData(String data) {
		this.name = data;
	}
	
	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}
//	public void regist() {
//		Person old = (Person) CatalogueDatabase.getInstance().getCached(id);
//		if (old == null) {
//			CatalogueDatabase.getInstance().regist(id, this);
//		} else {
//			old.dispose();
//			old.id = this.id;
//			old.name = this.name;
//		}
//	}
	
	public String toString(boolean detail) {
		return "Person(" + id + ") = " + name;
	}

	public void dispose() {
		id = null;
		name = null;
	}
}	

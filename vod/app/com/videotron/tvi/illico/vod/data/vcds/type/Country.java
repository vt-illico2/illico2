package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Country extends BaseElement implements CacheElement {
	public String id;
	public String code;
	public LanguageContent[] name;
	
	public String getId() { return id; }
	public Country() {
		name = new LanguageContent[NUMBER_OF_LANGUAGE];
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		code = attributes.getValue(AT_CODE);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_NAME.equals(qName)) {
			setLanguageContent(name, (LanguageContent) element);
		}
	}

	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}
//	public void regist() {
//		Country old = (Country) CatalogueDatabase.getInstance().getCached(id);
//		if (old == null) {
//			CatalogueDatabase.getInstance().regist(id, this);
//		} else {
//			old.dispose();
//			old.id = this.id;
//			old.name = this.name;
//		}
//	}

	public void dispose() {
		id = null;
		for (int i = 0; name != null && i < name.length; i++) {
			if (name[i] != null) name[i].dispose();
			name[i] = null;
		}
		//name = null;
	}
}

package com.videotron.tvi.illico.vod.data.vcds;

import java.awt.Container;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.Image;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

/**
 * Class for getting poster and background via HTTP
 */
public class ImageRetriever extends Thread {
	public static String BASE = "/images/";
	/**
	 * String constant for HTTP header content-length.
	 */
	public static final char[] CONTENT_LENGTH = "Content-Length: "
			.toCharArray();
	/**
	 * String constant for new line.
	 */
	public static final char[] NEW_LINE = "\r\n".toCharArray();
	/**
	 * String constant for new line.
	 */
	public static final char[] NEW_LINE_TWICE = "\r\n\r\n".toCharArray();
	/**
	 * String constant for HTTP header.
	 */
	public static final char[] HTTP_HEADER = "HTTP/1.1 ".toCharArray();
	/**
	 * Static instance for image loading.
	 */
	public static MediaTracker tracker = new MediaTracker(new Container());

	public static final Rectangle PR_LG = new Rectangle(0, 0, 210, 315);
	public static final Rectangle PR_2ND = new Rectangle(0, 0, 176, 264);
	public static final Rectangle PR_MD = new Rectangle(0, 0, 144, 216);
	public static final Rectangle PR_SM = new Rectangle(0, 0, 90, 135);
	public static final Rectangle LD_LG = new Rectangle(0, 0, 280, 210);
	public static final Rectangle LD_MD = new Rectangle(0, 0, 200, 150);
	public static final Rectangle LD_SM = new Rectangle(0, 0, 120, 90);
	
	public static final Rectangle LD_KA = new Rectangle(0, 0, 249, 181);
	
	public static final Rectangle PR_MD_SM = new Rectangle(0, 0, 110, 165);
	public static final Rectangle LD_LG_MD = new Rectangle(0, 0, 240, 180);
	public static final Rectangle LD_MD_SM = new Rectangle(0, 0, 160, 120);
	public static final Rectangle LD_XSM = new Rectangle(0, 0, 80, 60);

	public static final int TYPE_DOWNLOAD_MORE = 0x20;
	public static final int TYPE_NO_DELAY = 0x10;
	public static final int TYPE_LARGE = 0x04;
	public static final int TYPE_MEDIUM = 0x02;
	public static final int TYPE_SMALL = 0x01;
	
	public static final String AFFICHE = "Affiche";
	public static final String ORDER_PAK_WIDE = "Order_Pak_Wide";
	public static final String ORDER_PAK_NARROW = "Order_Pak_Narrow";
	public static final String BANNIERE_1 = "Bannière 1";
	public static final String KARAOKE_BANNER1 = "karaoke_banner1";
	public static final String KARAOKE_BANNER2 = "karaoke_banner2";
	public static final String CHANNEL_BRAND_DETAIL_BG = "CHANNEL_BRAND_DETAIL_BG";
	public static final String CHANNEL_BRAND_BG = "CHANNEL_BRAND_BG";
	public static final String CHANNEL_LISTING_LOGO = "CHANNEL_LISTING_LOGO";
	public static final String CHANNEL_TOP_MENU_LOGO = "CHANNEL_TOP_MENU_LOGO";
	public static final String CHANNEL_OVERLAY_LOGO = "CHANNEL_OVERLAY_LOGO";
	public static final String CHANNEL_ICON_LOCO = "Overlay_Provider_2";
	public static final String LINK_SMALL = "LINK-SMALL";
	public static final String LINK_LARGE = "LINK-LARGE";

	public static HashMap ALTER = new HashMap();
	static {
		ALTER.put(PR_LG, new Rectangle[] {
				PR_2ND, PR_MD, PR_SM,
		});
		ALTER.put(PR_2ND, new Rectangle[] {
				PR_LG, PR_MD, PR_SM,
		});
		ALTER.put(PR_MD, new Rectangle[] {
				PR_LG, PR_2ND, PR_SM,
		});
		ALTER.put(PR_SM, new Rectangle[] {
				PR_MD, PR_LG, PR_2ND,
		});
		ALTER.put(LD_LG, new Rectangle[] {
				LD_MD, LD_SM,
		});
		ALTER.put(LD_MD, new Rectangle[] {
				LD_LG, LD_SM,
		});
		ALTER.put(LD_SM, new Rectangle[] {
				LD_MD, LD_LG,
		});
		ALTER.put(PR_MD_SM, new Rectangle[] {
				PR_MD, PR_LG, PR_2ND, PR_SM,
		});
		ALTER.put(LD_LG_MD, new Rectangle[] {
				LD_LG, LD_MD, LD_SM,
		});
		ALTER.put(LD_MD_SM, new Rectangle[] {
				LD_MD, LD_LG, LD_SM,
		});
		ALTER.put(LD_XSM, new Rectangle[] {
				LD_SM, LD_MD, LD_LG,
		});
	}

	private static Rectangle[] largePattern = new Rectangle[] {
		PR_LG, PR_2ND, PR_2ND, PR_2ND, PR_2ND, PR_2ND, PR_LG, PR_LG
	};

	/**
	 * Screen to repaint.
	 */
	private BaseUI listener;
	/**
	 * Array of keys.
	 */
//	private Object[] key;
	private ArrayList key;
	/**
	 * Array of type.
	 */
	//private boolean[] portrait;
	/**
	 * Flag for large image.
	 */
	private boolean large;

	private int type;

	/**
	 * Default constructor.
	 */
	public ImageRetriever() {
		super();
	}

	/**
	 * Constructor.
	 *
	 * @param name
	 *            thread name
	 */
	public ImageRetriever(String name) {
		super(name);
	}
	
	public static void initThread() {
		threads = new ImageDownloader[] {
				new ImageDownloader("Thrd_0"),
				new ImageDownloader("Thrd_1"),
				new ImageDownloader("Thrd_2"),
		};
		for (int i = 0; i < threads.length; i++) {
			threads[i].start();
		}
	}
	public static void closeThread() {
		for (int i = 0; i < threads.length; i++) {
			threads[i].alive = false;
			synchronized (threads[i]) {
				threads[i].notifyAll();
			}
		}
	}
	
	private static ImageDownloader getImageDownloader() {
		ImageDownloader candidate = threads[0];
		for (int i = 1; i < threads.length; i++) {
			if (candidate.jobList.size() > threads[i].jobList.size()) {
				candidate = threads[i];
			}
		}
		return candidate;
	}
	
	private static class ImageDownloader extends Thread {
		public ArrayList jobList = new ArrayList();
		public boolean alive = true;
		public boolean wait = false;

		public ImageDownloader(String name) {
			super(name);
		}
		
		public void run() {
			Log.printDebug(getName() + " started");
			while (alive) {
				if (jobList.isEmpty()) {
					wait = true;
					boolean allWait = true;
					for (int i = 0; i < threads.length; i++) {
						if (threads[i].wait == false) {
							allWait = false;
						}
					}
					if (allWait) {
						CatalogueDatabase.getInstance().organizePosterPool();
					}
					synchronized (this) {
						try {
							this.wait();
						} catch (InterruptedException e) {}
					}
				}
				try {
					wait = false;
					DownloadJob job = (DownloadJob) jobList.remove(0);
					Log.printDebug(getName() + " gets " + job + ", job(s) " + jobList.size());
					String result = job.run();
					Log.printDebug(getName() + ": " + result);
					MenuController.getInstance().debugList[0].add(CatalogueDatabase.SDF.format(new Date()) + ", " + getName() + ": " + result + " sec, +" + jobList.size() + " job(s)");
				} catch (IndexOutOfBoundsException e) {}
			}
			Log.printDebug(getName() + " finished");
		}
		
	}
	private static ImageDownloader[] threads;
	private static class DownloadJob {
		public String title;
		public String url;
		public String cacheKey;
		public boolean large;
		public BaseUI listener;
		
		public String toString() {
			return url;
		}
		public String run() {
			String result;
//			URL httpUrl;
//			try {
				long t1 = System.currentTimeMillis();
//				httpUrl = new URL(DataCenter.getInstance().getString("IMAGE_URL") + url);
//				java.awt.Image tmp = Toolkit.getDefaultToolkit().createImage(httpUrl);
//				MediaTracker tracker = new MediaTracker(CategoryUI.getInstance());
//				tracker.addImage(tmp, 1);
//				tracker.waitForAll();
//				tracker.removeImage(tmp);
				Object[] ret = download(url);
				java.awt.Image tmp = (java.awt.Image) ret[0];
				t1 = System.currentTimeMillis() - t1;
				double elap = Math.round(t1 / 100) / 10.0; 
				if (tmp == null) {
					result = title + ", " + cacheKey + " fail to download, " + (large ? "BIG" : "NORMAL") + ", " + elap;
				} else {
					if (large) {
						CatalogueDatabase.getInstance().addLargeImage(cacheKey, tmp);
					} else {
						CatalogueDatabase.getInstance().addImage(cacheKey, tmp);
					}
					try {
						result = title + ", " + cacheKey + "(" + tmp.getWidth(null) + "x" + tmp.getHeight(null) + ", " + ret[1] + " bytes), " + (large ? "BIG" : "NORMAL") + ", " + elap;
					} catch (Exception e) {
						//R7.4 catch Exception because default iamge is not diplayed when image does not exist.
						result = title + ", " + cacheKey + " fail to download, " + (large ? "BIG" : "NORMAL") + ", " + elap;
					}
				}
				if (listener != null) {
					listener.reflectImage();
				}
				return result;
//			} catch (MalformedURLException e) {
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			return "";
		}
	}

	/**
	 * Setter.
	 *
	 * @param listener
	 *            screen to repaint
	 * @param images
	 *            array of images
	 * @param large
	 *            true if it wants large image
	 */
	public void setData(BaseUI listener, ArrayList images, int type) {
		this.listener = listener;
		key = images;//new Object[images.size()];
//		portrait = new boolean[key.length];
//		for (int i = 0; i < key.length; i++) {
//			key[i] = images.get(i);
//			if (key[i] instanceof Poster) {
//				portrait[i] = ((Poster) key[i]).portrait;
//			} else if (key[i] instanceof ImageGroup) {
//				portrait[i] = ((ImageGroup) key[i]).portrait;
//			} else {
//				portrait[i] = true;
//			}
//		}
		this.large = (TYPE_LARGE & type) > 0;
		this.type = type;
	}

	boolean willBePaused() {
		return Resources.appStatus != VODService.APP_STARTED;
	}

	public static java.awt.Image getDefault(int width, int height) {
		Rectangle size = new Rectangle(0, 0, width, height);
		if (size.equals(ImageRetriever.PR_LG)) {
			return DataCenter.getInstance().getImage("VOD_default_L_Portrait.png");
		} else if (size.equals(ImageRetriever.PR_2ND)) {
			return DataCenter.getInstance().getImage("VOD_default_ML_Portrait.png");
		} else if (size.equals(ImageRetriever.PR_MD)) {
			return DataCenter.getInstance().getImage("VOD_default_M_Portrait.png");
		} else if (size.equals(ImageRetriever.PR_SM)) {
			return DataCenter.getInstance().getImage("VOD_default_S_Portrait.png");
		} else if (size.equals(ImageRetriever.LD_LG)) {
			return DataCenter.getInstance().getImage("VOD_default_L_Landscape.png");
		} else if (size.equals(ImageRetriever.LD_MD)) {
			return DataCenter.getInstance().getImage("VOD_default_M_Landscape.png");
		} else if (size.equals(ImageRetriever.LD_SM)) {
			return DataCenter.getInstance().getImage("VOD_default_S_Landscape.png");
		} else if (size.equals(ImageRetriever.PR_MD_SM)) {
			return DataCenter.getInstance().getImage("VOD_default_M_Portrait.png");
		} else if (size.equals(ImageRetriever.LD_LG_MD)) {
			return DataCenter.getInstance().getImage("VOD_default_ML_Landscape.png");
		} else if (size.equals(ImageRetriever.LD_MD_SM)) {
			return DataCenter.getInstance().getImage("VOD_default_M_Landscape.png");
		} else if (size.equals(ImageRetriever.LD_XSM)) {
			return DataCenter.getInstance().getImage("VOD_default_S_Landscape.png");
		}
		return null;
	}

	public static Rectangle[] getAlterSize(int width, int height) {
		Rectangle r = new Rectangle(0, 0, width, height);
		if (r.equals(PR_LG)) {
			return (Rectangle[]) ALTER.get(PR_LG);
		} else if (r.equals(PR_2ND)) {
			return (Rectangle[]) ALTER.get(PR_2ND);
		} else if (r.equals(PR_MD)) {
			return (Rectangle[]) ALTER.get(PR_MD);
		} else if (r.equals(PR_SM)) {
			return (Rectangle[]) ALTER.get(PR_SM);
		} else if (r.equals(LD_LG)) {
			return (Rectangle[]) ALTER.get(LD_LG);
		} else if (r.equals(LD_MD)) {
			return (Rectangle[]) ALTER.get(LD_MD);
		} else if (r.equals(LD_SM)) {
			return (Rectangle[]) ALTER.get(LD_SM);
		} else if (r.equals(PR_MD_SM)) {
			return (Rectangle[]) ALTER.get(PR_MD_SM);
		} else if (r.equals(LD_MD_SM)) {
			return (Rectangle[]) ALTER.get(LD_MD_SM);
		} else if (r.equals(LD_LG_MD)) {
			return (Rectangle[]) ALTER.get(LD_LG_MD);
		} else if (r.equals(LD_XSM)) {
			return (Rectangle[]) ALTER.get(LD_XSM);
		}
		return null;
	}

	/**
	 * Method for run().
	 */
	public void run() {
		ArrayList urlList = new ArrayList();
		ArrayList urlList2 = new ArrayList();
		Hashtable urlToImage = new Hashtable();
		Image img = null;
		String url;
		Iterator iter = key.iterator();
		int largeCnt = 0;
		while (iter.hasNext()) {
			Object obj = iter.next();
			ImageGroup ig = (ImageGroup) obj;
			if (large) {
				img = ig.getImage(ig.portrait ? (largePattern[largeCnt++]) : ((TYPE_MEDIUM & type) > 0 ? LD_LG_MD : LD_LG));
				if (img != null && CatalogueDatabase.getInstance().containsLargeImage(img.getCacheKey()) == false) {
					url = BASE + img.imageName + "." + ig.encoding;
					if (urlList.contains(url) == false) {
						urlList.add(url);
						urlToImage.put(url, img);
					}
				}
			} else {
				if ((TYPE_MEDIUM & type) > 0) {
					img = ig.getImage(ig.portrait ? PR_MD : LD_MD);
					if (img != null && CatalogueDatabase.getInstance().containsImage(img.getCacheKey()) == false) {
						url = BASE + img.imageName + "." + ig.encoding;
						if (urlList2.contains(url) == false) {
							urlList2.add(url);
							urlToImage.put(url, img);
						}
					}
				}
				if ((TYPE_SMALL & type) > 0) {
					img = ig.getImage(ig.portrait ? PR_SM : LD_SM);
					if (img != null && CatalogueDatabase.getInstance().containsImage(img.getCacheKey()) == false) {
						url = BASE + img.imageName + "." + ig.encoding;
						if (urlList.contains(url) == false) {
							urlList.add(url);
							urlToImage.put(url, img);
						}
					}
				}
				if (ig.specialSize && CatalogueDatabase.getInstance().containsImage(ig.images[0].getCacheKey()) == false) {
					url = BASE + ig.images[0].imageName + "." + ig.encoding;
					img = ig.images[0];
					if (urlList.contains(url) == false) {
						urlList.add(url);
						urlToImage.put(url, img);
					}
				}
			}
		}
		urlList.addAll(urlList2);
		
		for (int i = 0; i < urlList.size(); i++) {
			DownloadJob job = new DownloadJob();
			job.url = (String) urlList.get(i);
			job.cacheKey = ((Image) urlToImage.get(job.url)).getCacheKey();
			job.large = large;
			job.listener  = listener;
			job.title = ((Image) urlToImage.get(job.url)).parent.title;
			ImageDownloader downloader = getImageDownloader();
			Log.printDebug(job + " on " + downloader);
			downloader.jobList.add(job);
			
			// VDTRMASTER-5706
			if (downloader.jobList.size() > 30) {
				downloader.jobList.remove(0);
				CatalogueDatabase.getInstance().organizePosterPool();
			}
			synchronized (downloader) {
				downloader.notifyAll();
			}
		}
		if (listener != null && (TYPE_DOWNLOAD_MORE & type) > 0) {
			listener.imageDownloadMore();
		}
		if (true) return;

		Log.printDebug("ImageRetriever, run()" +
				", type = 0x" + Integer.toHexString(type) +
				", key = " + key.size() +
				", urlList = " + urlList.size() +
				", " + urlList);
		if (urlList.size() == 0) {
			if (listener != null && (TYPE_NO_DELAY & type) == 0) {
				listener.reflectImage();
			}
			if (listener != null && (TYPE_DOWNLOAD_MORE & type) > 0) {
				listener.imageDownloadMore();
			}
			return;
		}
		String host = DataCenter.getInstance().getString("IMAGE_URL");
		int port = 80;
		if (host.indexOf("://") != -1) {
			host = host.substring(host.indexOf("://") + 3);
		}
		if (host.indexOf("/") != -1) {
			host = host.substring(0, host.indexOf("/"));
		}
		if (host.indexOf(":") != -1) {
			port = Integer.parseInt(host.substring(host.indexOf(":") + 1));
			host = host.substring(0, host.indexOf(":"));
		}
		Log.printDebug("ImageRetriever, run()" + ", host = " + host
				+ ", port = " + port);
		long t1;
		Socket socket = null;
		BufferedInputStream is = null;
		PrintStream ps = null;
		byte[] buf;
		String step = "STEP 0: Parsing host OK, " + host + ":" + port;
		try {
			if (urlList.size() > 1 && (TYPE_NO_DELAY & type) == 0) {
				Thread.sleep(500);
			}
			if (interrupted() || willBePaused()) {
				System.err.println("ImageRetriever, INTERRUPTED 0");
				return;
			}

			t1 = System.currentTimeMillis();
			socket = new Socket(host, port);
			socket.setSoTimeout(1000);
			ps = new PrintStream(socket.getOutputStream(), false);
			is = new BufferedInputStream(socket.getInputStream());

			StringBuffer strBuf = new StringBuffer(step);
			strBuf.append('\n');

			for (int i = 0; i < urlList.size(); i++) {
				url = (String) urlList.get(i);
				ps.println("GET " + url + " HTTP/1.1");
				ps.println("Host: " + host);
				ps.println("Connection: "
						+ ((i == urlList.size() - 1) ? "Close" : "Keep-Alive"));
				ps.println();
//				strBuf.append("URL[").append(i).append("] = ").append(url)
//						.append('\n');
			}
			ps.flush();
			step = "STEP 1: Request url(s) OK, length : " + urlList.size();

			int r;

			long t2 = System.currentTimeMillis();
			strBuf.append("elapsed after getInputStream() = ");
			strBuf.append(t2 - t1).append('\n');
			t1 = t2;

			int len;
			for (int i = 0; i < urlList.size(); i++) {
				try {
					Thread.sleep(50); // TODO
				} catch (InterruptedException ie) {}
				if (interrupted() || willBePaused()) {
					System.err.println("ImageRetriever, INTERRUPTED 1");
					return;
				}
				t1 = System.currentTimeMillis();
				len = findContent(is);
				strBuf.append("URL = ").append(urlList.get(i)).append(", size = ").append(len)
						.append(" (").append(len >> 10).append(" Kb)");
				String debug = CatalogueDatabase.SDF.format(new Date()) + ", " + host + ":" + port + urlList.get(i) + ", " + (len >> 10) + " KB, read = ";
				if (len == 0) {
					int avail = is.available();
					debug += "X, avail = " + avail; 
					MenuController.getInstance().debugList[0].add(debug);
					strBuf.append(", avail = ").append(avail).append('\n');
					if (avail == 0) {
						type |= TYPE_DOWNLOAD_MORE;
						break;
					}
					continue;
				}
				buf = new byte[len];
				r = 0;
				while (r < len) {
					r += is.read(buf, r, len - r);
				}
				if (interrupted() || willBePaused()) {
					System.err.println("ImageRetriever, INTERRUPTED 2");
					return;
				}
				t2 = System.currentTimeMillis();
				strBuf.append(", reading = ").append(t2 - t1);
				debug += (t2 - t1) + " ms, decode = ";
				t1 = t2;

				java.awt.Image tmp = Toolkit.getDefaultToolkit().createImage(
						buf);
				Rectangle imgSize = new Rectangle(0, 0, tmp.getWidth(null),
						tmp.getHeight(null));
				strBuf.append(", size = ").append(imgSize);
				Thread.yield();
				if (interrupted() || willBePaused()) {
					System.err.println("ImageRetriever, INTERRUPTED 3");
					return;
				}
				t1 = System.currentTimeMillis();
				tracker.addImage(tmp, 1);
				tracker.waitForID(1);
				tracker.removeImage(tmp, 1);
				t2 = System.currentTimeMillis();
				strBuf.append(", decoding = ");
				strBuf.append(t2 - t1).append('\n');
				debug += (t2 - t1) + " ms, size = " + imgSize.width + "x" + imgSize.height;

				if (imgSize.width != -1 && imgSize.height != -1) {
					if (large) {
						CatalogueDatabase.getInstance().addLargeImage(((Image) urlToImage.get(urlList.get(i))).getCacheKey(), tmp);
					} else {
						CatalogueDatabase.getInstance().addImage(((Image) urlToImage.get(urlList.get(i))).getCacheKey(), tmp);
					}
				}
				MenuController.getInstance().debugList[0].add(debug);
				if (listener != null) {
					listener.reflectImage();
				}
				if (interrupted() || willBePaused()) {
					System.err.println("ImageRetriever, INTERRUPTED 4");
					return;
				}
			}
			step = strBuf.toString();
		} catch (InterruptedException ie) { // interrupted exception is okay
		} catch (SocketTimeoutException ste) {
			MenuController.getInstance().debugList[0].add("SocketTimeoutException - " + ste.getMessage());
			Log.printWarning("SocketTimeoutException - " + ste.getMessage());
			type |= TYPE_DOWNLOAD_MORE;
		} catch (Exception e) {
			Log.print(e);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception ee) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ee) {
			}
			try {
				if (socket != null) {
					socket.close();
				}
			} catch (Exception ee) {
			}
			is = null;
			ps = null;
			socket = null;
			buf = null;
		}
		Log.printDebug(step);
		CatalogueDatabase.getInstance().organizePosterPool();
		if (listener != null && (TYPE_DOWNLOAD_MORE & type) > 0) {
			listener.imageDownloadMore();
		}
	}
	
	private static Object[] download(String url) {
		String host = DataCenter.getInstance().getString("IMAGE_URL");
		int port = 80;
		if (host.indexOf("://") != -1) {
			host = host.substring(host.indexOf("://") + 3);
		}
		if (host.indexOf("/") != -1) {
			host = host.substring(0, host.indexOf("/"));
		}
		if (host.indexOf(":") != -1) {
			port = Integer.parseInt(host.substring(host.indexOf(":") + 1));
			host = host.substring(0, host.indexOf(":"));
		}
		Socket socket = null;
		BufferedInputStream is = null;
		PrintStream ps = null;
		byte[] buf;
		Object[] ret = new Object[2];
		
		try {
			socket = new Socket(host, port);
			socket.setSoTimeout(1000);
			ps = new PrintStream(socket.getOutputStream(), false);
			is = new BufferedInputStream(socket.getInputStream());
			
			ps.println("GET " + url + " HTTP/1.1");
			ps.println("Host: " + host);
			ps.println("Connection: Close");
			ps.println();
			ps.flush();
			
			int len = findContent(is);
			
			if (len < 0) {
				return ret;
			}
			ret[1] = new Integer(len);
			buf = new byte[len];
			int r = 0;
			while (r < len) {
				r += is.read(buf, r, len - r);
			}
			
			java.awt.Image tmp = Toolkit.getDefaultToolkit().createImage(buf);
			int id = Thread.currentThread().hashCode();
			tracker.addImage(tmp, id);
			tracker.waitForID(id);
			tracker.removeImage(tmp, id);
			ret[0] = tmp;
		} catch (Exception e) {
			Log.print(e);
			ret[0] = null;
			Log.printDebug("ret[0] is null");
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception ee) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ee) {
			}
			try {
				if (socket != null) {
					socket.close();
				}
			} catch (Exception ee) {
			}
			is = null;
			ps = null;
			socket = null;
			buf = null;
		}
		
		return ret;
	}

	/**
	 * Returns size of next content in input stream.
	 *
	 * @param is
	 *            input stream
	 * @return size of next content
	 * @throws Exception
	 */
	private static int findContent(InputStream is) throws Exception {
		int r;
		char ch;
		int step = 0;
		int len;
		// // 0. HTTP return
		// step = 0;
		// while (step < NEW_LINE.length && (r = is.read()) != -1) {
		// ch = (char) r;
		// if (ch == NEW_LINE[step]) {
		// step++;
		// } else {
		// step = 0;
		// }
		// }
		// 1. "Content-Length: "
		
		while (step < HTTP_HEADER.length && (r = is.read()) != -1) {
			ch = (char) r;
			step = (ch == HTTP_HEADER[step]) ? step + 1 : 0;
		}
		// http code
		step = 0;
		int code = 0;
		while (step < 3 && (r = is.read()) != -1) {
			step++;
			ch = (char) r;
			code = code * 10 + (ch - '0');
		}
		
		if (code != 200) {
			if (Log.EXTRA_ON) {
				Log.printDebug("http code =" + code);
			}
			return -1;
		}
		
		step = 0;
		while (step < CONTENT_LENGTH.length && (r = is.read()) != -1) {
			ch = (char) r;
			step = (ch == CONTENT_LENGTH[step]) ? step + 1 : 0;
		}

		// 2. get length
		step = 0;
		len = 0;
		while (step < NEW_LINE.length && (r = is.read()) != -1) {
			ch = (char) r;
			if (ch == NEW_LINE[step]) {
				step++;
			} else {
				step = 0;
				len = len * 10 + (ch - '0');
			}
		}

		// 3. ready to
		step = 0;
		while (step < NEW_LINE_TWICE.length && (r = is.read()) != -1) {
			ch = (char) r;
			step = (ch == NEW_LINE_TWICE[step]) ? step + 1 : 0;
		}

		return len;
	}
}

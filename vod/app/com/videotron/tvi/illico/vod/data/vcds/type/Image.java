package com.videotron.tvi.illico.vod.data.vcds.type;

import java.awt.Rectangle;

import org.xml.sax.Attributes;

public class Image extends BaseElement {
	static String[] FORMAT = new String[] {
			"large",
			"medium",
			"small"
	};
	       
	public String language; // if this is null, it can be displayed both side 
	public String urlBase;
	public String baseName;
	public String extension;
	public int formats;
	
	public int width, height; // for R2
	public String imageName; // for R2
	
	public ImageGroup parent;
	
	public int hashCode() {
		return baseName.hashCode();
	}
	
	public String toString(boolean detail) {
		return "Image, " + width + "x" + height + ", N = " + imageName;
	}
	
	public boolean equals(Object o) {
		if (baseName != null && o instanceof Image) {
			return baseName.equals(((Image) o).baseName);
		}
		return false;
	}
	
	public boolean match(Rectangle r) {
		return r.width == width && r.height == height;
	}
	
	public String getCacheKey() {
		return imageName;// + width + height;
	}
	
	public void setAttribute(Attributes attributes) {
		language = attributes.getValue(AT_LANGUAGE);
		urlBase = attributes.getValue(AT_URL_BASE);
		if (urlBase != null && urlBase.charAt(0) != '/') {
			urlBase = '/' + urlBase;
		}
		baseName = attributes.getValue(AT_BASE_NAME);
		extension = attributes.getValue(AT_EXTENSION);
		String f = attributes.getValue(AT_FORMATS);
		if (f == null || "large".equals(f)) {
			formats = 0;
		} else if ("medium".equals(f)) {
			formats = 1;
		} else {
			formats = 2;
		}
		try {
			width = Integer.parseInt(attributes.getValue(AT_WIDTH));
		} catch (NumberFormatException nfe) {}
		try {
			height = Integer.parseInt(attributes.getValue(AT_HEIGHT));
		} catch (NumberFormatException nfe) {}
		imageName = attributes.getValue(AT_IMAGE_NAME);
	}
	
	public String getUrl(boolean large) {
		return urlBase + getUrlWithoutBase(large);
	}
	
	public String getUrlWithoutBase(boolean large) {
		String f = large ? FORMAT[formats] : FORMAT[2];
		return baseName + "_" + f + (extension == null ? ".jpg" : extension);
	}

	public void dispose() {
		language = null;
		urlBase = null;
		baseName = null;
	}
	
	public String toString() {
		if (imageName != null) {
			return "Image, " + imageName + ", " + width + "x" + height;
		}
		return "Image, " + urlBase+"|"+baseName+"|"+extension+"|"+formats; 
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;


public class EpisodeContainer extends BaseContainer {
	public Episode episode; // or ref
	public String episodeRef;
	
	public String toString(boolean detail) {
		if (episode == null) {
			return "episode is " + episodeRef;
		}
		return episode.toString(detail);
	}
	
	public void dispose() {
		super.dispose();
		if (episode != null) episode.dispose();
		episode = null;
	}
}

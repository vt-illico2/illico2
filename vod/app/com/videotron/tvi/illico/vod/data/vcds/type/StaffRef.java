package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class StaffRef extends BaseElement {
	public int priority;
	public Person person; // or ref
	public String personRef;
	
	public String getName() {
		if (person == null) {
			return "";
		}
		if (person.name == null) {
			return "";
		}
		return person.name;
	}
	
	public void setAttribute(Attributes attributes) {
		try {
			priority = Integer.parseInt(attributes.getValue(AT_PRIORITY));
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_PERSON.equals(qName)) {
			person = (Person) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_PERSON_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				person = (Person) cached;
			} else {
				personRef = data;
			}
		}
	}

	public void dispose() {
		if (person != null) person.dispose();
		person = null;
	}
}

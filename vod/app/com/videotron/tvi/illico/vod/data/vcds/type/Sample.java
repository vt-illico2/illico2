package com.videotron.tvi.illico.vod.data.vcds.type;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Sample extends BaseElement {
	public MoreDetail[] data = new MoreDetail[0];
	public Topic[] topics = new Topic[0];
	
	public void endElement(String qName, BaseElement element) {
		if (EL_CATEGORY.equals(qName)) {
		} if (EL_CATEGORY_CONTAINER.equals(qName)) {
		} else if (EL_SHOWCASE_CONTAINER.equals(qName)) {
		} else if (EL_BUNDLE_CONTAINER.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, ((BundleContainer) element).bundle);
		} else if (EL_EXTRA_CONTAINER.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, ((ExtraContainer) element).extra);
		} else if (EL_VIDEO_CONTAINER.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, ((VideoContainer) element).video);
		} else if (EL_EPISODE_CONTAINER.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, ((EpisodeContainer) element).episode);
		} else if (EL_SEASON_CONTAINER.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, ((SeasonContainer) element).season);
		} else if (EL_SERIES_CONTAINER.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, ((SeriesContainer) element).series);
		} else if (EL_SHOWCASE.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, element);
		} else if (EL_BUNDLE.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, element);
		} else if (EL_EXTRA.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, element);
		} else if (EL_VIDEO.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, element);
		} else if (EL_EPISODE.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, element);
		} else if (EL_SEASON.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, element);
		} else if (EL_SERIES.equals(qName)) {
			data = (MoreDetail[]) addArray(data, MoreDetail.class, element);
		} else if (EL_TOPIC.equals(qName)) {
			// VDTRMASTER-5646
			if (data.length > 0) {
				Showcase sc = new Showcase();
				Topic topic = (Topic) element;
				sc.id = topic.id;
				sc.description = new Description();
				sc.description.title = topic.title;
				sc.platformValidity = topic.platformValidity;
				if (topic.imageGroup != null) {
					sc.description.imageGroup = topic.imageGroup;
				} else if (topic.channel != null && topic.channel.imageGroup != null) {
					sc.description.imageGroup = topic.channel.imageGroup;
				} else if (topic.network != null && topic.network.imageGroup != null) {
					sc.description.imageGroup = topic.network.imageGroup;
				}
				
				sc.detail = new ShowcaseDetails();
				sc.detail.setTopic(topic);
				
				data = (MoreDetail[]) addArray(data, MoreDetail.class, sc);
			} else {
				topics = (Topic[]) addArray(topics, Topic.class, element);
			}
		}
	}
	
	public void setDataAt(String qName, String refId) {
		if (EL_BUNDLE_REF.equals(qName)
				|| EL_VIDEO_REF.equals(qName)
				|| EL_EPISODE_REF.equals(qName)
				|| EL_SEASON_REF.equals(qName)
				|| EL_SERIES_REF.equals(qName)
				|| EL_SHOWCASE_REF.equals(qName)
				) {
			Object cached = CatalogueDatabase.getInstance().getCached(refId);
			if (cached != null) {
				data = (MoreDetail[]) addArray(data, MoreDetail.class, cached);
			}
		}
	}
		
	public void dispose() {
		for (int i = 0; data != null && i < data.length; i++) {
			data[i] = null;
		}
		data = null;
		for (int i = 0; topics != null && i < topics.length; i++) {
			topics[i] = null;
		}
		topics = null;
	}

}

package com.videotron.tvi.illico.vod.data.vcds.type;

public interface CacheElement {
	public void register();
	public String getId();
}

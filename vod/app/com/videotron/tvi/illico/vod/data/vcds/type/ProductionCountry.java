package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class ProductionCountry extends BaseElement {
	public int priority;
	public Country country; // or ref
	public String countryRef;
	
	public void setAttribute(Attributes attributes) {
		try {
			priority = Integer.parseInt(attributes.getValue(AT_PRIORITY));
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_COUNTRY.equals(qName)) {
			country = (Country) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_COUNTRY_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				country = (Country) cached;
			} else {
				countryRef = data;
			}
		}
	}

	public void dispose() {
		if (country != null) country.dispose();
		country = null;
	}
}

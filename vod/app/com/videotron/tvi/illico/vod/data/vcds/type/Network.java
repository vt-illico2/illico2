package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Network extends BrandingElement implements CacheElement, HasDupNamed {
	public String id;
	public LanguageContent[] title = new LanguageContent[NUMBER_OF_LANGUAGE];
	public LanguageContent[] description = new LanguageContent[NUMBER_OF_LANGUAGE];
	public Channel[] channel = new Channel[0]; 
	
	public boolean has(BaseElement br) {
		for (int i = 0; i < channel.length; i++) {
			if (channel[i].equals(br)) {
				return true;
			}
		}
		return false;
	}
	
	public String getId() { return id; }
	public String getDescription() {
		return getLanguageContentData(description);
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return LanguageContent.class;
		} 
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_TITLE.equals(qName)){
			setLanguageContent(title, (LanguageContent) element);
		} else if (EL_DESCRIPTION.equals(qName)){
			setLanguageContent(description, (LanguageContent) element);
		} else if (EL_IMAGE_GROUP.equals(qName)) {
			imageGroup = (ImageGroup[]) addArray(imageGroup, ImageGroup.class, element);
		} else if (EL_CHANNEL.equals(qName)) {
			channel = (Channel[]) addArray(channel, Channel.class, element);
		}
	}
	
	public void reset() {
		channel = new Channel[0]; 
	}
	
	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_CHANNEL_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				channel = (Channel[]) addArray(channel, Channel.class, cache);
			}
		}
	}
	
	// R5
	public String getTitle() {
		return getLanguageContentData(title);
	}
	
	public String toString(boolean detail) {
		return "Network(" + id + ") = " + getLanguageContentData(title);
	}
	
	public void dispose() {
		id = null;
		for (int i = 0; title != null && i < title.length; i++) {
			if (title[i] != null) title[i].dispose();
			title[i] = null;
		}
		for (int i = 0; description != null && i < description.length; i++) {
			if (description[i] != null) description[i].dispose();
			description[i] = null;
		}
	}
}

/**
 * 
 */
package com.videotron.tvi.illico.vod.data.similar.type;

import org.json.simple.JSONObject;
import com.videotron.tvi.illico.log.Log;

/**
 * @author zestyman
 *
 */
public class SError extends BaseJson {

	String code;
	String message;
	
	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.vod.data.similar.type.BaseJson#parseJson(java.lang.Object)
	 */
	public void parseJson(Object obj) throws Exception {
		JSONObject jObj = (JSONObject) obj;
		
		code = getString(jObj, "code");
		message = getString(jObj, "message");
		
		Log.printError("Error, return error for similar content, code=" + code + ", message=" + message);
	}

}

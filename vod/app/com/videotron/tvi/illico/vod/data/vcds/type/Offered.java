package com.videotron.tvi.illico.vod.data.vcds.type;

import java.text.ParseException;
import java.util.Date;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Offered extends BaseElement {
	public Date start;
	public Date end;
	
	public Offer offer; // or ref
	public String offerRef;
	
	public void setAttribute(Attributes attributes) {
		try {
			start = df.parse(attributes.getValue(AT_START_DATE));
		} catch (ParseException e) {
			start = new Date();
		}
		try {
			end = df.parse(attributes.getValue(AT_END_DATE));
		} catch (ParseException e) {
			end = new Date();
		}
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_OFFER.equals(qName)) {
			offer = (Offer) element;
		}
	}
	
	public void setChildAttribute(String qName, Attributes attributes) {
		if (EL_OFFER_REF.equals(qName)) {
			String refId = attributes.getValue(AT_REF);
			Object cached = CatalogueDatabase.getInstance().getCached(refId);
			if (cached != null) {
				offer = (Offer) cached;
			} else {
				offerRef = refId;
			}
		}
	}

	public void dispose() {
		start = null;
		end = null;
		if (offer != null) offer.dispose();
		offer = null;
	}
}

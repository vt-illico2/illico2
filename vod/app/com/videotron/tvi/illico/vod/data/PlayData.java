package com.videotron.tvi.illico.vod.data;

public class PlayData {
	public String sspId;
	public int duration; // sec
	public int currentMode;
	public int npt;
	public String titleName;
	
	public String toString() {
		return sspId + "|" + duration + "|0x" + Integer.toHexString(currentMode) + "|" + npt + "|" + titleName;
	}
}

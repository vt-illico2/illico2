package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.util.Constants;

public class Offer extends BaseElement {
	public String amount;
	public long leaseDuration;
	public String language;
	public String definition;
	public Period period;
	public String duration;
	
	public boolean isValid() {
		return true;
//		if (period == null) return true;
//		long now = System.currentTimeMillis();
//		return period.start.getTime() < now 
//			&& now < period.end.getTime();
	}
	
	public void setAttribute(Attributes attributes) {
		amount = attributes.getValue(AT_AMOUNT);
//		try {
//			leaseDuration = df2.parse(attributes.getValue(AT_LEASE_DURATION));
//		} catch (ParseException e) {
//			leaseDuration = new Date(0L);
//		}
		duration = attributes.getValue(AT_LEASE_DURATION);
		long t = 0L;
		if (duration.charAt(0) == 'P') { // P01DT00H, P1D
			if (duration.indexOf('D') != -1) { // day
				int day = Integer.parseInt(duration.substring(1, duration.indexOf('D')));
				t += Constants.MS_PER_DAY * day;
			}
			if (duration.indexOf('T') != -1) {
				if (duration.indexOf('H') != -1) { // hour
					int hour = Integer.parseInt(duration.substring(duration.indexOf('T')+1, duration.indexOf('H')));
					t += Constants.MS_PER_HOUR * hour;
				}
				if (duration.indexOf('M') != -1) { // minute
					int idx = duration.indexOf('T')+1;
					if (duration.indexOf('H') != -1) {
						idx = duration.indexOf('H')+1;
					}
					int min = Integer.parseInt(duration.substring(idx, duration.indexOf('M')));
					t += Constants.MS_PER_MINUTE * min;
				}
			}
		}
		leaseDuration = t;
		language = attributes.getValue(AT_LANGUAGE);
		definition = attributes.getValue(AT_DEFINITION);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_PERIOD.equals(qName)) {
			period = (Period) element;
		}
	}
	
	public void dispose() {
		amount = null;
		language = null;
		definition = null;
		if (period != null) period.dispose();
		period = null;
	}
	
	public String toString() {
		return "Offer|"+definition+"|"+language+"|"+amount+"|"+duration;
	}
}

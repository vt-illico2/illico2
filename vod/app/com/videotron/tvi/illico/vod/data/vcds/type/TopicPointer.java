package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class TopicPointer extends MoreDetail implements HasDupNamed {
	public String id;
	public String layoutName;
	public Topic topic;
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class; 
		}
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		layoutName = attributes.getValue(AT_LAYOUT_NAME);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_TOPIC.equals(qName)) {
			topic = (Topic) element;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		}
	}
	
	public void dispose() {
		if (topic != null) {
			topic.dispose();
		}
		topic = null;
	}
	
	public String getId() {
		return id;
	}
	
	public String getTitle() {
		return getLanguageContentData(topic.title);
	}
	
	public String toString(boolean detail) {
		String ret = "TopicPointer(" + id + ")";
		return topic == null ? ret : ret + " = " + topic.toString(detail); 
	}

	public String toString() {
	    return toString(false);
    }
	
	public void dumpSub(String prefix) {
		System.out.println(prefix + toString(true));
		topic.treeRoot.dumpSub(prefix);
	}

}

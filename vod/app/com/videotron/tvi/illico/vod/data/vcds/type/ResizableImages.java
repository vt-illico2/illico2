package com.videotron.tvi.illico.vod.data.vcds.type;

public class ResizableImages extends BaseElement {
	public Image[] images = new Image[0];
	
	public void endElement(String qName, BaseElement element) {
		if (EL_IMAGE.equals(qName)) {
			images = (Image[]) addArray(images, Image.class, element);
		}
	}

	public void dispose() {
		for (int i = 0; images != null && i < images.length; i++) {
			images[i] = null;
		}
		images = null;
	}
	
	public String toString() {
		return "ResizableImages, " + images.length + " images";
	}
}

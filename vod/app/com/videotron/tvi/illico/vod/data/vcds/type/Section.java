package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Section extends MoreDetail implements HasDupNamed {
	public ContentDescription description;
	public Section[] sections = new Section[0];
	public String[] episodes = new String[0];
	
	public Season parent;
	public void setParent(Season season) {
		parent = season;
		for (int i = 0; i < sections.length; i++) {
			sections[i].setParent(season);
		}
	}

	public String getTitle() {
		return description == null ? NA : getLanguageContentData(description.title);
	}
	public String getDescription() { 
		return description == null ? NA : getLanguageContentData(description.description);
	}
	public boolean isPortrait() { 
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		return false;
	}
	public java.awt.Image getImage(int width, int height) {
		return getImage(description, width, height);
	}
	public Object chooseImage() {
		if (description == null) {
			return null;
		}
		if (description.imageGroup.length > 0) {
			return chooseImageGroup(description.imageGroup);
		}
		return null;
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return ContentDescription.class;
		}
		return null;
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			description = (ContentDescription) element;
		} else if (EL_SECTION.equals(qName)) {
			sections = (Section[]) addArray(sections, Section.class, element);
		}
	}

	public void setDataAt(String qName, String data) {
		if (EL_EPISODE_REF.equals(qName)) {
			episodes = (String[]) addArray(episodes, String.class, data);
		}
	}

	public String toString() {
		return "Section(" + getTitle() + ")";
	}
	
	public String toString(boolean detail) {
		if (detail) {
			StringBuffer buf = new StringBuffer();
			buf.append("Section(").append(getTitle()).append(":");
			for (int i = 0; i < episodes.length; i++) {
				buf.append(episodes[i]).append(",");
			}
			buf.append(")");
			
			return buf.toString();
		}
		return toString();
	}

	public void dispose() {
		episodes = new String[0];
		sections = new Section[0];
	}
}

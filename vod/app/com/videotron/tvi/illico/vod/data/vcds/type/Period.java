package com.videotron.tvi.illico.vod.data.vcds.type;

import java.text.ParseException;
import java.util.Date;

import org.xml.sax.Attributes;

public class Period extends BaseElement {
	public Date start;
	public Date end;
	
	public void setAttribute(Attributes attributes) {
		try {
			start = df.parse(attributes.getValue(AT_START));
		} catch (ParseException e) {
			start = new Date();
		}
		try {
			end = df.parse(attributes.getValue(AT_END));
		} catch (ParseException e) {
			end = new Date();
		}
	}
	
	public String toString(boolean detail) {
		return df.format(start) + " ~ " + df.format(end);
	}

	public void dispose() {
		start = null;
		end = null;
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;

public class CategorizedBundle extends MoreDetail implements HasDupNamed, CacheElement {
	public String id;
	public ContentDescription description;
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	public VideoDetails[] detail = new VideoDetails[0];
	public String notListed;
	public String inBundles;
	public String[] inBundlesAR;
	public Offer[] offer = new Offer[0];

	public Offer[] offerSD = new Offer[0];
	public Offer[] offerHD = new Offer[0];

	public boolean reset = false;
	
	public String getId() {
		return id;
	}

	public String getTitle() { 
		String title = description == null ? NA : getLanguageContentData(description.title); 
//		if (Log.EXTRA_ON) {
//			title = id + "|" + title;
//		}
		return title;
	}
	
	public String getSspId(int select) {
		String lang = "", definition = "";
		switch (select) {
		case Resources.HD_FR: 
			lang = LANGUAGE_FR;
			definition = "HD";
			break;
    	case Resources.HD_EN: 
    		lang = LANGUAGE_EN;
			definition = "HD";
			break;
    	case Resources.SD_FR: 
    		lang = LANGUAGE_FR;
			definition = "SD";
			break;
    	case Resources.SD_EN:
    		lang = LANGUAGE_EN;
			definition = "SD";
			break;
		}
		for (int i = 0; i < detail.length; i++) {
			if (definition.equals(detail[i].definition)
				&& lang.equals(detail[i].language)) {

				return detail[i].orderableItem;
			}
		}
		return "";
	}
	
	public boolean isFree() {
		if (offer.length == 0) {
			return false;
		}
		for (int i = 0; i < offer.length; i++) {
			double amt = Double.parseDouble(offer[i].amount);
			if (amt > 0) return false;
		}
		return true;
	}

//	public java.awt.Image getImage() {
//		if (description == null) {
//			return null;
//		}
//		if (description.imageGroup.length > 0) {
//			return getImage(description.imageGroup);
//		}
//		return null;
////		return getImage(description.poster);
//	}
	public java.awt.Image getImage(int width, int height) {
		return getImage(description, width, height, ImageRetriever.AFFICHE);
	}
	public Object chooseImage(String usage) { 
		if (description == null) {
			return null;
		}
		if (description.imageGroup.length > 0) {
			return chooseImageGroup(description.imageGroup, usage);
		}
		return null;
//		return chooseImage(description.poster); 
	}
	public boolean isPortrait() { 
//		Object img = chooseImage();
//		if (img instanceof Poster) {
//			return ((Poster) img).portrait;
//		} else if (img instanceof ImageGroup) {
//			return ((ImageGroup) img).portrait;
//		}
		return false;
	}

	public String getDescription() {
		return description == null ? NA
				: getLanguageContentData(description.description);
	}

	public long getLeaseDuration() {
		return offer.length == 0 ? 0L : offer[0].leaseDuration;
	}

	public Offer getOfferSD(String lang) {
		if (offerSD.length == 0) {
			return null;
		}
		if (lang == null) {
			return offerSD[0];
		}
		for (int i = 0; i < offerSD.length; i++) {
			if (lang.equals(offerSD[i].language))
				return offerSD[i];
		}
		return null;
	}

	public Offer getOfferHD(String lang) {
		if (offerHD.length == 0) {
			return null;
		}
		if (lang == null) {
			return offerHD[0];
		}
		for (int i = 0; i < offerHD.length; i++) {
			if (lang.equals(offerHD[i].language))
				return offerHD[i];
		}
		return null;
	}
	
	public boolean is3screen() {
		if (notListed == null) return true;
		if (notListed.length() == 0) return true;
		if ("i1".equalsIgnoreCase(notListed)) return true;
		return false;
	}
	
	public String getEndPeriod() { 
		// support 8
		try {
			return df.format(platformService.validityPeriod.end);
		} catch (NullPointerException npe) {
		}
		return "";
//		if (offer.length > 0) {
//			return df.format(offer[0].period.end);
//		}
//		return "NO offers"; 
	}

	public Class getClass(String qName, Attributes attributes) {
		if (EL_DETAIL.equals(qName)) {
			return VideoDetails.class;
		} else if (EL_DESCRIPTION.equals(qName)) {
			return ContentDescription.class;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class;
		}
		return null;
	}

	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
	}

	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			description = (ContentDescription) element;
		} else if (EL_DETAIL.equals(qName)) {
			if (reset) { realReset(); }
			detail = (VideoDetails[]) addArray(detail, VideoDetails.class, element);
		} else if (EL_OFFER.equals(qName)) {
			if (reset) { realReset(); }
			Offer o = (Offer) element;
			// if (o.isValid() == false) return;
			offer = (Offer[]) addArray(offer, Offer.class, o);
			if (DEFNITION_HD.equals(o.definition)) {
				offerHD = (Offer[]) addArray(offerHD, Offer.class, o);
			} else if (DEFNITION_SD.equals(o.definition)) {
				offerSD = (Offer[]) addArray(offerSD, Offer.class, o);
			}
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		}
	}

	public void setDataAt(String qName, String data) {
		if (EL_IN_BUNDLES.equals(qName)) {
			inBundles = data;
			inBundlesAR = TextUtil.tokenize(data, ' ');
		} else if (EL_NOT_LISTED.equals(qName)) {
			notListed = data;
		}
	}

	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}

	public void reset() {
		reset = true;
	}
	private void realReset() {
		reset = false;
		detail = new VideoDetails[0];
		offer = new Offer[0];
		offerSD = new Offer[0];
		offerHD = new Offer[0];
	}

	public String toString() {
		return toString(false);
	}
	
	public String toString(boolean detail) {
		if (description == null) {
			return "CategorizedBundle(" + id + ") = NO description";
		}
		return "CategorizedBundle(" + id + ") = "
				+ getLanguageContentData(this.description.title);
	}

	public void dispose() {
		id = null;
		inBundles = null;
		inBundlesAR = null;
		if (description != null)
			description.dispose();
		description = null;
		if (platformService != null) platformService.dispose();
		platformService = null;
		for (int i = 0; detail != null && i < detail.length; i++) {
			if (detail[i] != null) detail[i].dispose();
			detail[i] = null;
		}
		for (int i = 0; offerSD != null && i < offerSD.length; i++) {
			offerSD[i] = null;
		}
		for (int i = 0; offerHD != null && i < offerHD.length; i++) {
			offerHD[i] = null;
		}
		for (int i = 0; offer != null && i < offer.length; i++) {
			if (offer[i] != null)
				offer[i].dispose();
			offer[i] = null;
		}
		// offer = null;
	}
}

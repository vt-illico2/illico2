package com.videotron.tvi.illico.vod.data.vcds.type;


public class ExtraContainer extends BaseContainer {
	public Extra extra; // or ref
	public String extraRef;
	
	public String toString(boolean detail) {
		if (extra == null) {
			return "extra is " + extraRef;
		}
		return extra.toString(detail);
	}
	
	public void dispose() {
		super.dispose();
		if (extra != null) extra.dispose();
		extra = null;
	}
}

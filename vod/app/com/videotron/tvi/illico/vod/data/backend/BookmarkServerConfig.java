package com.videotron.tvi.illico.vod.data.backend;

public class BookmarkServerConfig {
    private String ip;
    private int port;
    private String rootContext;
    
    
    public String toString() {
    	return ip + ":" + port + rootContext;
    }
    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }
    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }
    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }
    /**
     * @return the rootContext
     */
    public String getRootContext() {
        return rootContext;
    }
    /**
     * @param rootContext the rootContext to set
     */
    public void setRootContext(String rootContext) {
        this.rootContext = rootContext;
    }
}

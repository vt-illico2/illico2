package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Error extends BaseElement {
	public String errorCode;
	public String errorDescription;
	
	public void setAttribute(Attributes attributes) {
		errorCode = attributes.getValue(AT_CODE);
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_ERROR_DESCRIPTION.equals(qName)) {
			errorDescription = data;
		}
	}
	
	public String toString(boolean detail) {
		return "Error(" + errorCode + ") = " + errorDescription;
	}

	public void dispose() {
		errorCode = null;
		errorDescription = null;
	}
}

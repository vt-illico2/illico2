package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class ContentRoot extends BaseElement {
	public String name;
	public String contentType;
	public String value;
	public Tree treeRoot; // or ref
	public String treeRootRef;
	
	public void setAttribute(Attributes attributes) {
		name = attributes.getValue(AT_NAME);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_TREE_ROOT.equals(qName)) {
			treeRoot = (Tree) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_TREE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				treeRoot = (Tree) cached;
			} else {
				treeRootRef = data;
			}
		} else if (EL_VALUE.equals(qName)) {
			value = data;
		} else if (EL_CONTENT_TYPE.equals(qName)) {
			contentType = data;
		}
	}
	
	public String toString(boolean detail) {
		return "ContentRoot<" + contentType + "> = " + name + ", value = " + value;
	}
	
	public void dispose() {
		name = null;
		contentType = null;
		value = null;
		if (treeRoot != null) {
			treeRoot.dispose();
		}
		treeRoot = null;
	}
}

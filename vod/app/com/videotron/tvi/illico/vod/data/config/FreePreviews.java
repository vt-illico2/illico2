package com.videotron.tvi.illico.vod.data.config;

import com.videotron.tvi.illico.log.Log;
import java.io.*;
import java.util.HashSet;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

public class FreePreviews extends DefaultHandler {

    HashSet idSet = new HashSet();

    public static FreePreviews create(File file) {
        try {
            FreePreviews previews = new FreePreviews();
            previews.parse(file);
            return previews;
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    public FreePreviews() {
    }

    public HashSet getChannelNames() {
        return idSet;
    }

    private void parse(File file) throws Exception {
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        FileInputStream fis = new FileInputStream(file);
        parser.parse(fis, this);
        try {
            fis.close();
        } catch (Exception ex) {
        }
    }

    public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException {
        if ("channel".equalsIgnoreCase(localName)) {
            String channelId = attr.getValue("id");
            idSet.add(channelId);
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
    }

    public String toString() {
        return "FreePreviews " + idSet;
    }

}

package com.videotron.tvi.illico.vod.data.config;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.SessionProperties;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtil;
import com.videotron.tvi.illico.vod.data.backend.BookmarkServerConfig;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;

/**
 * This class integrated with Management System and Application Property to apply the setting values of VOD
 * Application. This class receives configuration files from Inband carousel and OOB carousel.
 *
 * @author LSC
 */
public class ConfigurationFileReceiver implements DataUpdateListener {

    public static final String serverConfigKey = "BOOK_MARK_CONFIG";
    public static final String serviceCategoryKey = "SERVICE_CATEGORY_CONFIG";
    public static final String serviceGroupMapKey = "SERVICE_GROUP_MAP";
    // 4K
    public static final String COMMON_KEY= "VOD_COMMON";
    public static final String UHD_WARNING_MSG_KEY = "UHD_WARNING_MSG";
    private int commonVersion = -1;
    /**
     * Initialize.
     */
    public void init(){
        if (Log.DEBUG_ON) {
            Log.printDebug("ConfigurationFileReceiver init");
        }
        DataCenter.getInstance().addDataUpdateListener(serverConfigKey, this);
        DataCenter.getInstance().addDataUpdateListener(serviceCategoryKey, this);
        Log.printDebug("ConfigurationFileReceiver, listener added about service category information");
        DataCenter.getInstance().addDataUpdateListener(serviceGroupMapKey, this);
        
        // 4K
        DataCenter.getInstance().addDataUpdateListener(COMMON_KEY, this);

        // sometimes this method called after loading the files
        Object newValue = DataCenter.getInstance().get(serverConfigKey);
        if (newValue != null) {
        	dataUpdated(serverConfigKey, null, newValue);
        }

        File serviceGropupMap = (File) DataCenter.getInstance().get(serviceGroupMapKey);
        if (serviceGropupMap != null) {
        	dataUpdated(serviceGroupMapKey, null, serviceGropupMap);
        }
        
        File serviceCategory = (File) DataCenter.getInstance().get(serviceCategoryKey);
        Log.printDebug("ConfigurationFileReceiver found serviceCategoryFile, will parse it and checkPackages() soon");
        if (serviceCategory != null) {
        	dataUpdated(serviceCategoryKey, null, serviceCategory);
        }
    }

    /**
     * Called when data has been removed.
     *
     * @param key key of data.
     */
    public void dataRemoved(String key){

    }

    /**
     * Called when data has been updated.
     *
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value){
        if (Log.DEBUG_ON) {
            Log.printDebug("ConfigurationFileReceiver dataUpdated, key : " + key);
        }
        if (key.equals(serverConfigKey)) {
            File bookMarkFile = (File) value;
            byte[] data = getByteArrayFromFile(bookMarkFile);
            Log.printDebug(decodeServerConfig(data));

			// VDTRMASTER-5852 - remove
//            // DDC-090
//            boolean isDDC090 = DataCenter.getInstance().getBoolean("DDC_090");
//
//            if (isDDC090) {
//            	updateServerInformation();
//            }
        } else if (key.equals(serviceCategoryKey)) {
            File serviceCategory = (File) value;
            byte[] data = getByteArrayFromFile(serviceCategory);
            Log.printDebug(decodeCategoryConfig(data));
            new Thread("ConfigurationFileReceiver, delayed check packages") {
        		public void run() {
        			try {
						Thread.sleep(Constants.MS_PER_MINUTE / 3); // in 20 secs
					} catch (InterruptedException e) {}
        			Log.printDebug("will checkPackages() because configuration file updated"); 
        			CommunicationManager.getInstance().checkPackages();
        		}
        	}.start();
        } else if (key.equals(serviceGroupMapKey)) {
            ServiceGroupController.getInstance().setSGMapFile((File)value);
        } else if (key.equals(COMMON_KEY)) {
        	File commonFile = (File) value;
        	byte[] data = getByteArrayFromFile(commonFile);
        	Log.printDebug(decodeCommon(data));
        }
    }

    /**
     * Decode data.
     * @param data the data
     * @return the string
     * @throws Exception the exception
     */
    private String decodeServerConfig(byte[] data){
    	String DNCS = "";
		try {
			DNCS = CommunicationManager.getInstance().getMonitorService().getDncsName();
			Log.printDebug("decodeServerConfig(), DNCS = " + DNCS);
			if (DNCS == null) {
				DNCS = "LAB";
			}

		} catch (Exception e1) {
			Log.print(e1);
		}
		if (data == null || data.length == 0) {
			return "No data";
		}

		// // VDTRMASTER-5852 - remove
//		// DDC-090
//		boolean isDDC090 = DataCenter.getInstance().getBoolean("DDC_090");
//
//		if (isDDC090) {
//			return "isDDC090=true";
//		}
		
        BookmarkServerConfig serverConfig = new BookmarkServerConfig();
        String ip = null;
        int port = 0;
        String rootContext = null;

        int index = 0;
        int textLength = 0;
        String text = null;
        StringBuffer buf = new StringBuffer();
        textLength = oneByteToInt(data, ++index);
        buf.append("(Bookmark) ip_addr_length = " + textLength);

        text = byteArrayToString(data, ++index, textLength);
        ip = text;
        index = index + textLength - 1;
        buf.append(" | ip_addr = " + text);

        port = twoBytesToInt(data, ++index);
        buf.append(" | port = " + port);
        index++;

        textLength = oneByteToInt(data, ++index);
        buf.append(" | root_context_length = " + textLength);

        text = byteArrayToString(data, ++index, textLength);
        index = index + textLength - 1;
        buf.append(" | root_context = " + text + "\n");
        rootContext = text;

        serverConfig.setIp(ip);
        serverConfig.setPort(port);
        serverConfig.setRootContext(rootContext);
        if (ip != null && ip.length() > 0) {
        	BookmarkManager.getInstance().setServerConfig(serverConfig);
        }

        // for vcds, re-use code about bookmark
        textLength = oneByteToInt(data, ++index);
        buf.append("(VCDS) ip_addr_length = " + textLength);

        text = byteArrayToString(data, ++index, textLength);
        ip = text;
        index = index + textLength - 1;
        buf.append(" | ip_addr = " + text);

        port = twoBytesToInt(data, ++index);
        buf.append(" | port = " + port);
        index++;

        textLength = oneByteToInt(data, ++index);
        buf.append(" | root_context_length = " + textLength);

        text = byteArrayToString(data, ++index, textLength);
        index = index + textLength - 1;
        buf.append(" | root_context = " + text + "\n");
        rootContext = text;

        try {
        	if (!rootContext.startsWith("/")) {
        		rootContext = "/" + rootContext;
        	}
        	if (!rootContext.endsWith("/")) {
        		rootContext = rootContext + "/";
        	}
			String url = new URL("HTTP", ip, port, rootContext).toExternalForm();
			if (ip != null && ip.length() > 0) {
				DataCenter.getInstance().put("VCDS_URL", url);
			}
		} catch (MalformedURLException e) {}

        // for poster server, re-use code about bookmark
        textLength = oneByteToInt(data, ++index);
        buf.append("(POSTER) ip_addr_length = " + textLength);

        text = byteArrayToString(data, ++index, textLength);
        ip = text;
        index = index + textLength - 1;
        buf.append(" | ip_addr = " + text);

        port = twoBytesToInt(data, ++index);
        buf.append(" | port = " + port);
        index++;
        
        // 4K
        textLength = oneByteToInt(data, ++index);
        buf.append(" | root_context_length = " + textLength);

        text = byteArrayToString(data, ++index, textLength);
        index = index + textLength - 1;
        buf.append(" | root_context = " + text + "\n");
        rootContext = text;
//        Log.printDebug("test" + buf.toString());
        try {
        	if (!rootContext.startsWith("/")) {
        		rootContext = "/" + rootContext;
        	}
        	if (!rootContext.endsWith("/")) {
        		rootContext = rootContext + "/";
        	}
        	
        	ImageRetriever.BASE = rootContext;
        	
			String url = new URL("HTTP", ip, port, "").toExternalForm();
			if (ip != null && ip.length() > 0) {
				DataCenter.getInstance().put("IMAGE_URL", url);

				// Bundling
				SharedMemory.getInstance().put(SharedDataKeys.VOD_POSTER_URL, url + ImageRetriever.BASE);
			}
		} catch (MalformedURLException e) {}

		try { // R2 format
//			textLength = oneByteToInt(data, ++index);
//	        text = byteArrayToString(data, ++index, textLength);
//	        index = index + textLength - 1;

	        // Session server
	        int size = oneByteToInt(data, ++index);
	        for (int i = 0; i < size; i++) {
	        	textLength = oneByteToInt(data, ++index);
	        	text = byteArrayToString(data, ++index, textLength);
	        	index = index + textLength - 1;

	        	boolean match = DNCS.equals(text);
	        	if (match) {
	        		buf.append("[MATCH] " + text);
	        	} else {
	        		buf.append(text);
	        	}

		        textLength = oneByteToInt(data, ++index);
		        buf.append(", (DNCS) ip_addr_length = " + textLength);

		        text = byteArrayToString(data, ++index, textLength);
		        ip = text;
		        index = index + textLength - 1;
		        buf.append(" | ip_addr = " + text);
		        if (match) {
			        SessionProperties.DNCS_IP = text;
			        DataCenter.getInstance().put("DNCS_IP", ip);
		        }

		        port = twoBytesToInt(data, ++index);
		        index++;
		        buf.append(" | port = " + port);
		        if (match) {
			        SessionProperties.DNCS_PORT = port;
			        DataCenter.getInstance().put("DNCS_PORT", String.valueOf(port));
		        }
		        // Gateway IP
		        textLength = oneByteToInt(data, ++index);
		        buf.append(" | (Gateway) ip_addr_length = " + textLength);
		        text = byteArrayToString(data, ++index, textLength);
		        ip = text;
		        index = index + textLength - 1;
		        buf.append(" | ip_addr = " + text + "\n");
		        if (match) {
			        SessionProperties.GS_IP_STR = ip;
			        SessionProperties.GS_IP = StringUtil.stringIPtoInt(ip);
			        DataCenter.getInstance().put("GW_IP", ip);
		        }
	        }

	        // Proxy IP for bookmark
	        size = oneByteToInt(data, ++index);
	        String[] proxyBookmark = new String[size];
	        for (int i = 0; i < proxyBookmark.length; i++) {
	        	textLength = oneByteToInt(data, ++index);
		        buf.append("ip_addr_length = " + textLength);

		        text = byteArrayToString(data, ++index, textLength);
		        ip = text;
		        index = index + textLength - 1;
		        buf.append(" | ip_addr(Bookmark proxy #"+i+") = " + text);

		        port = twoBytesToInt(data, ++index);
		        buf.append(" | port = " + port  + "\n");
		        index++;

		        proxyBookmark[i] = new URL("HTTP", ip, port, "/").toExternalForm();
	        }

	        // Proxy IP for VCDS
	        size = oneByteToInt(data, ++index);
	        String[] proxyVCDS = new String[size];
	        for (int i = 0; i < proxyVCDS.length; i++) {
	        	textLength = oneByteToInt(data, ++index);
		        buf.append("ip_addr_length = " + textLength);

		        text = byteArrayToString(data, ++index, textLength);
		        ip = text;
		        index = index + textLength - 1;
		        buf.append(" | ip_addr(VCDS proxy #"+i+") = " + text);

		        port = twoBytesToInt(data, ++index);
		        buf.append(" | port = " + port  + "\n");
		        index++;

		        proxyVCDS[i] = new URL("HTTP", ip, port, "/").toExternalForm();
	        }

	        // Proxy IP for poster
	        size = oneByteToInt(data, ++index);
	        String[] proxyPoster = new String[size];
	        for (int i = 0; i < proxyPoster.length; i++) {
	        	textLength = oneByteToInt(data, ++index);
		        buf.append("ip_addr_length = " + textLength);

		        text = byteArrayToString(data, ++index, textLength);
		        ip = text;
		        index = index + textLength - 1;
		        buf.append(" | ip_addr(Poster proxy #"+i+") = " + text);

		        port = twoBytesToInt(data, ++index);
		        buf.append(" | port = " + port  + "\n");
		        index++;

		        proxyPoster[i] = new URL("HTTP", ip, port, "/").toExternalForm();
	        }

	        // Proxy IP for session
	        size = oneByteToInt(data, ++index);
	        int n = size;
	        for (int j = 0; j < n; j++) {
	        	textLength = oneByteToInt(data, ++index);
		        text = byteArrayToString(data, ++index, textLength);
		        index = index + textLength - 1;
		        String name = text;
		        boolean match = DNCS.equals(name);

	        	size = oneByteToInt(data, ++index);
		        String[] proxySession = new String[size];
		        for (int i = 0; i < proxySession.length; i++) {
		        	textLength = oneByteToInt(data, ++index);
		        	if (match) {
		        		buf.append("[MATCH] ");
		        	}
			        buf.append(name + ", ip_addr_length = " + textLength);

			        text = byteArrayToString(data, ++index, textLength);
			        ip = text;
			        index = index + textLength - 1;
			        buf.append(" | ip_addr(Session proxy #"+i+") = " + text);

			        port = twoBytesToInt(data, ++index);
			        buf.append(" | port = " + port  + "\n");
			        index++;

			        proxySession[i] = new URL("HTTP", ip, port, "/").toExternalForm();
		        }
	        }

		} catch (Exception e) {
			// R1 format
			e.printStackTrace();
		}
        return buf.toString();
    }

    /**
     * Decode data.
     * @param data the data
     * @return the string
     * @throws Exception the exception
     */
    private String decodeCategoryConfig(byte[] data){
        String[] svodPackageNames = null;
        int[] svodServiceIds = null;

        int index = -1;
        StringBuffer buf = new StringBuffer("decodeCategoryConfig() ");
        try {
	        int textLength = 0;
	
	        int ver = oneByteToInt(data, ++index);
	        buf.append("version = ").append(ver).append("\n");
	        int loop = oneByteToInt(data, ++index);
	        buf.append("svod_service_id_list_size = ").append(loop).append("\n");
	        svodServiceIds = new int[loop];
	        svodPackageNames = new String[loop];
	
	        for (int i = 0; i < loop; i++) {
	        	svodServiceIds[i] = twoBytesToInt(data, ++index);
	        	index++;
	            buf.append("\tservice_id = ").append(svodServiceIds[i]).append(" = 0x").append(Integer.toHexString(svodServiceIds[i])).append("\n");
	
	            textLength = oneByteToInt(data, ++index);
	            svodPackageNames[i] = byteArrayToString(data, ++index, textLength);
	            index = index + textLength - 1;
	            
	            buf.append("\tpackage_name = ").append(svodPackageNames[i]).append("\n");
	            
	            if (textLength == 0) {
	            	buf.append("package_name is empty. something wrong.\n");
	            	return buf.toString();
	            }
	        }
        } catch (Exception e) {
        	Log.print(e);
        	buf.append("got an exception " + e.getMessage());
        	boolean ori = CommunicationManager.getInstance().turnOnErrorMessage(false);
        	CommunicationManager.getInstance().errorMessage("VOD201-05", null);
        	CommunicationManager.getInstance().turnOnErrorMessage(ori);
        	return buf.toString();
        }
        DataCenter.getInstance().put("SVOD_SERVICE_IDS", svodServiceIds);
        DataCenter.getInstance().put("SVOD_PACKAGE_NAMES", svodPackageNames);
        return buf.toString();
    }
    
    // 4K
    private String decodeCommon(byte[] data) {
    	StringBuffer sf = new StringBuffer("decodeCommon(), ");
    	
    	int dataIdx = 0;
    	// version
    	int version = oneByteToInt(data, dataIdx++);
    	sf.append("oldVersion=" + commonVersion + ", newVersion=" + version + "\n");
    	
    	if (version != commonVersion) {
    		int langLength = oneByteToInt(data, dataIdx++);
    		sf.append(", langLength=" + langLength + "\n");
    		for (int i = 0; i < langLength; i++) {
    			String langType = new String(data, dataIdx, 2);
    			sf.append(", langType=" + langType);
    			dataIdx += 2;
    			
    			int msgLength = twoBytesToInt(data, dataIdx);
    			sf.append(", msgLength=" + msgLength);
    			dataIdx += 2;
    			
    			String msg = new String(data, dataIdx, msgLength);
    			sf.append(", msg=" + msg + "\n");
    			dataIdx += msgLength;
    			
    			DataCenter.getInstance().put(UHD_WARNING_MSG_KEY + "_" + langType, msg);
    		}
    	}
    	
    	return sf.toString();
    	
    }

    /**
     * Change one byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int oneByteToInt(byte[] data, int offset) {
    	try {
    		return (((int) data[offset]) & 0xFF);
    	} catch (ArrayIndexOutOfBoundsException e) {
    		return 0;
    	}
    }
    /**
     * Change two byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int twoBytesToInt(byte[] data, int offset) {
    	try {
    		return (((((int) data[offset]) & 0xFF) << 8) + (((int) data[offset + 1]) & 0xFF));
    	} catch (ArrayIndexOutOfBoundsException e) {
    		return 0;
    	}
    }
    /**
     * Change byte array to String.
     * @param data byte array
     * @param offset start index
     * @param length byte length
     * @return integer
     */
    public static String byteArrayToString(byte[] data, int offset, int length) {
        String result = null;
        try {
            result = new String(data, offset, length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Gets the bytes from File.
     * @param file the file
     * @return the bytes from file
     */
    public static byte[] getByteArrayFromFile(File file) {
        if (file == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (bout != null) {
                    bout.reset();
                    bout.close();
                    bout = null;
                }
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    private void updateServerInformation() {
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationFileReceiver: updateServerInformation()");
		}
		
		// load and set for bookmark.
		BookmarkServerConfig serverConfig = new BookmarkServerConfig();
		
		String ip = DataCenter.getInstance().getString("BOOKMARK_IP");
		int port = DataCenter.getInstance().getInt("BOOKMARK_PORT");
		String rootContext = DataCenter.getInstance().getString("BOOKMARK_CONTEXT");
		serverConfig.setIp(ip);
        serverConfig.setPort(port);
        serverConfig.setRootContext(rootContext);
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("BookMark URL : " + serverConfig.toString());
        }
        
        if (ip != null && ip.length() > 0) {
        	BookmarkManager.getInstance().setServerConfig(serverConfig);
        }
        
        // Image server
        ip = DataCenter.getInstance().getString("IMAGE_IP");
        port = DataCenter.getInstance().getInt("IMAGE_PORT");
        rootContext = DataCenter.getInstance().getString("IMAGE_CONTEXT");
        
        try {
        	String url = new URL("HTTP", ip, port, rootContext).toExternalForm();
        	
        	if (Log.DEBUG_ON) {
        		Log.printDebug("image url=" + url);
        	}
        	
        	if (ip != null && ip.length() > 0) {
    			DataCenter.getInstance().put("IMAGE_URL", url);

				// Bundling
				SharedMemory.getInstance().put(SharedDataKeys.VOD_POSTER_URL, url);
    		}
        } catch (MalformedURLException e) {
        	e.printStackTrace();
        }
        
        // VDTRMASTER-5605
        MonitorService mService = CommunicationManager.getInstance().getMonitorService();
        String dncsNameStr = null;
        if (mService != null) {
        	try {
				dncsNameStr = mService.getDncsName();
			} catch (RemoteException e) {
				Log.print(e);
			}
        }
        
        Log.printDebug("ConfigurationFileReceiver: updateServerInformation(), dncsNameStr=" + dncsNameStr);
		
        // session
        if (dncsNameStr == null || !dncsNameStr.startsWith("PROD")) {
        	// LAB & QUALIF
        	ip = DataCenter.getInstance().getString("SESSION_IP");
        } else {
        	// PROD_X
        	ip = DataCenter.getInstance().getString("SESSION_IP_" + dncsNameStr);
        }
        port = DataCenter.getInstance().getInt("SESSION_PORT");
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("session ip=" + ip + ", port=" + port);
        }
        
    	SessionProperties.DNCS_IP = ip;
		DataCenter.getInstance().put("DNCS_IP", ip);
		
		SessionProperties.DNCS_PORT = port;
        DataCenter.getInstance().put("DNCS_PORT", String.valueOf(port));
        
        // Gateway
        if (dncsNameStr == null || !dncsNameStr.startsWith("PROD")) {
        	// LAB & QUALIF
        	ip = DataCenter.getInstance().getString("GW_IP");
        } else {
        	// PROD_X
        	ip = DataCenter.getInstance().getString("GW_IP_" + dncsNameStr);
        }
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("Gateway ip=" + ip);
        }
        
        SessionProperties.GS_IP_STR = ip;
        SessionProperties.GS_IP = StringUtil.stringIPtoInt(ip);
        DataCenter.getInstance().put("GW_IP", ip);
        
        // vcds
        ip = DataCenter.getInstance().getString("VCDS_IP");
        port = DataCenter.getInstance().getInt("VCDS_PORT");
        rootContext = DataCenter.getInstance().getString("VCDS_CONTEXT");
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("VCDS ip=" + ip + ", port=" + port + ", context=" + rootContext);
        }
        
        try {
        	if (!rootContext.startsWith("/")) {
        		rootContext = "/" + rootContext;
        	}
        	if (!rootContext.endsWith("/")) {
        		rootContext = rootContext + "/";
        	}
			String url = new URL("HTTP", ip, port, rootContext).toExternalForm();
			if (ip != null && ip.length() > 0) {
				DataCenter.getInstance().put("VCDS_URL", url);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

    public static void main(String[] a) throws Exception {
    	FileInputStream fis = new FileInputStream("d:\\vod_category_config.txt");
    	byte[] buf = new byte[4096];
    	int r = fis.read(buf);
    	String log = new ConfigurationFileReceiver().decodeCategoryConfig(buf);
    	System.out.println(log);
    }
}
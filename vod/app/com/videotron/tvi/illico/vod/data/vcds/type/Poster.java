package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Poster extends Image {
	public boolean portrait = false;
	
	public void setAttribute(Attributes attributes) {
		String orientation = attributes.getValue(AT_ORIENTATION);
		portrait = orientation == null || "portrait".equals(orientation);
		super.setAttribute(attributes);
	}
	
	public String toString() {
		return urlBase + baseName + ", " + portrait; 
	}
}

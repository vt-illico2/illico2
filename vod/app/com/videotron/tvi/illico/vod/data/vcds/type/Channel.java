package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Channel extends BrandingElement implements CacheElement, HasDupNamed {
	public boolean hasAuth;
	public String id; 
	public String channelNumber;
	public String callLetters;
	public LanguageContent[] title = new LanguageContent[NUMBER_OF_LANGUAGE];
	public LanguageContent[] description = new LanguageContent[NUMBER_OF_LANGUAGE];
	public Network network;
	public String networkRef;

	// R7.3
	public MPlexChannels mplexChannels;
	
	public String getId() { return id; }
	public String getDescription() {
		return getLanguageContentData(description);
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return LanguageContent.class;
		} 
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		channelNumber = attributes.getValue(AT_CHANNEL_NUMBER);
		callLetters = attributes.getValue(AT_CALLLETTERS);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_TITLE.equals(qName)){
			setLanguageContent(title, (LanguageContent) element);
		} else if (EL_DESCRIPTION.equals(qName)){
			setLanguageContent(description, (LanguageContent) element);
		} else if (EL_NETWORK.equals(qName)) {
			network = (Network) element;
		} else if (EL_IMAGE_GROUP.equals(qName)) {
			imageGroup = (ImageGroup[]) addArray(imageGroup, ImageGroup.class, element);
		} else if (EL_MPLEX_CHANNELS.equals(qName)) {
			mplexChannels = (MPlexChannels) element;
		}
	}
	
	public void register() {
		// R7.3
		if (description != null && description.length > 0) {
			CatalogueDatabase.getInstance().register(id, this);
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_NETWORK_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				network = (Network) cache;
			} else {
				networkRef = data;
			}
		}
	}
	
	// R5
	public String getTitle() {
		return getLanguageContentData(title);
	}
	
	public String toString(boolean detail) {
		return "Channel(" + id + ") = " + channelNumber + ", call = " + callLetters + ", title = " + getLanguageContentData(title);
	}
	
	public void dispose() {
		id = null;
		channelNumber = null;
		callLetters = null;
		for (int i = 0; title != null && i < title.length; i++) {
			if (title[i] != null) title[i].dispose();
			title[i] = null;
		}
		for (int i = 0; description != null && i < description.length; i++) {
			if (description[i] != null) description[i].dispose();
			description[i] = null;
		}
	}
}

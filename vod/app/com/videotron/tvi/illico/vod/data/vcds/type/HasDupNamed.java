package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public interface HasDupNamed {
	public Class getClass(String qName, Attributes attributes);
}

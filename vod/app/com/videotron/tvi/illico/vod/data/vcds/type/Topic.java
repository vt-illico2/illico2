package com.videotron.tvi.illico.vod.data.vcds.type;

import java.io.File;
import java.util.ArrayList;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Topic extends BrandingElement implements CacheElement, HasDupNamed {
	public boolean needRetrieve = true;
	public String id;
	public String type;
	public String sortOrder;
	public LanguageContent[] title = new LanguageContent[NUMBER_OF_LANGUAGE];
	public LanguageContent[] description = new LanguageContent[NUMBER_OF_LANGUAGE];
	public Channel channel; // 0 .. 1
	public Network network; // 0 .. 1
	//public Tree treeRoot;
	public CategoryContainer treeRoot;
	public String namedTopic;
	public Sample sample;
	
	// R5-TANK
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	public Service service;
	public Service svodService;
	public Service[] services = new Service[0];
	public int leafCount = -1;
	public int containerCount = -1;
	
	public String getId() { return id; }
	public String getTitle() {
		return getLanguageContentData(title);
	}
	public String getDescription() {
		return getLanguageContentData(description);
	}
	public ImageGroup getBanner(String usage) {
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		ArrayList list = new ArrayList();
		for (int i = 0; i < imageGroup.length; i++) {
			if (usage.equals(imageGroup[i].usage) == false) {
				continue;
			}
			if (lang.equals(imageGroup[i].language)) {
				list.add(0, imageGroup[i]);
			} else {
				list.add(imageGroup[i]);
			}
		}
		return (ImageGroup) (list.size() == 0 ? null : list.get(0));
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return LanguageContent.class;
		} 
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		type = attributes.getValue(AT_TYPE);
		sortOrder = attributes.getValue(AT_SORT_ORDER);
		
		try {
			leafCount = Integer.parseInt(attributes.getValue(AT_LEAF_COUNT));
			// VDTRMASTER-5653 & VDTRMASTER-5649
			if (treeRoot != null) {
				treeRoot.totalContents = new MoreDetail[0];
			}
		} catch (NumberFormatException e) {}
	}
	
	public void setChildAttribute(String qName, Attributes attributes) {
		if (EL_NAMED_TOPIC.equals(qName)) {
			namedTopic = attributes.getValue(AT_NAME);
		}
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_TREE_ROOT.equals(qName)) {
			treeRoot = (CategoryContainer) element;
			// R5 - TANK
			treeRoot.topicPointer = new TopicPointer[1];
			treeRoot.topicPointer[0] = new TopicPointer();
			treeRoot.setTopic(this);
			
			if (channel != null || network != null) {
				File categoryList = (File) DataCenter.getInstance().get("meta_tools_category");
				if (categoryList == null) return;
		        String[] categoriesStr = TextReader.read(categoryList);
		        CategoryContainer container = null;
		        String[] parsedStr = null;
		        for (int j = 0; j < categoriesStr.length; j++) {
		            parsedStr = TextUtil.tokenize(categoriesStr[j], "|");
		            if (parsedStr[2].equals("about")) {
		            	container = new CategoryContainer();
		            	container.id = parsedStr[0];
		            	container.type = MenuController.MENU_ABOUT;
		            	container.category = new Category();
			            container.category.description = new Description();
			            container.category.description.title[0] = new LanguageContent(LANGUAGE_EN, parsedStr[4]);
			            container.category.description.title[1] = new LanguageContent(LANGUAGE_FR, parsedStr[5]);
			            treeRoot.categoryContainer = (CategoryContainer[]) addArray(treeRoot.categoryContainer, CategoryContainer.class, container);
		            }
		        }
			}
		} else if (EL_CHANNEL.equals(qName)) {
			channel = (Channel) element;
		} else if (EL_NETWORK.equals(qName)) {
			network = (Network) element;
		} else if (EL_TITLE.equals(qName)){
			setLanguageContent(title, (LanguageContent) element);
		} else if (EL_DESCRIPTION.equals(qName)){
			setLanguageContent(description, (LanguageContent) element);
		} else if (EL_IMAGE_GROUP.equals(qName)) {
			imageGroup = (ImageGroup[]) addArray(imageGroup, ImageGroup.class, element);
		} else if (EL_SAMPLES.equals(qName)) {
			sample = (Sample) element;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) { // R5 - TANK
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) { // R5 - TANK
			platformValidity = (PlatformValidity) element;
		} else if (EL_SERVICE.equals(qName)) { // R5 - TANK
			services = (Service[]) addArray(services, Service.class, element);
			
			if (((Service)element).serviceType == Service.VOD) {
				service = (Service) element;
			} else if (((Service)element).serviceType == Service.SVOD) {
				svodService = (Service) element;
			} 
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_TREE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				treeRoot = (CategoryContainer) cached;
			}
		} else if (EL_CHANNEL_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				channel = (Channel) cached;
			}
		} else if (EL_NETWORK_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				network = (Network) cached;
			}
		}
	}
	
	public void register() {
		// jira 3836
		if (treeRoot != null && treeRoot.categoryContainer.length > 0) {
			int idxOfAbout = -1;
			for (int i = 0; i < treeRoot.categoryContainer.length; i++) {
				if (treeRoot.categoryContainer[i].type == MenuController.MENU_ABOUT) {
					idxOfAbout = i;
					break;
				}
			}
			if (idxOfAbout != -1 && idxOfAbout != treeRoot.categoryContainer.length - 1) {
				CategoryContainer about = treeRoot.categoryContainer[idxOfAbout];
				for (int i = idxOfAbout; i < treeRoot.categoryContainer.length - 1; i++) {
					treeRoot.categoryContainer[i] = treeRoot.categoryContainer[i+1];
				}
				treeRoot.categoryContainer[treeRoot.categoryContainer.length - 1] = about;
			}
		}
		CatalogueDatabase.getInstance().register(id, this);
		if ("Karaoke".equals(type)) {
			CatalogueDatabase.getInstance().setKaraokeTopicId(id);
		}
		
		// R5 - TANK
		CatalogueDatabase.getInstance().setLastTopicId(id);
	}
	
	public String toString(boolean detail) {
		return "Topic(" + id + ") = " + getLanguageContentData(title) + ", " + type;
	}
	
	public void dispose() {
		id = null;
		type = null;
		sortOrder = null;
		for (int i = 0; title != null && i < title.length; i++) {
			if (title[i] != null) title[i].dispose();
			title[i] = null;
		}
		for (int i = 0; description != null && i < description.length; i++) {
			if (description[i] != null) description[i].dispose();
			description[i] = null;
		}
		if (treeRoot != null) {
			treeRoot.dispose();
		}
		treeRoot = null;
		if (sample != null)	sample.dispose();
		sample = null;
	}
	
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class PlatformService extends BaseElement implements CacheElement {
	public String platformName;
	public String serviceName;
	public String id;
	
	public String getId() { return id; }
	public void setAttribute(Attributes attributes) {
		platformName = attributes.getValue(AT_PLATFORM_NAME);
		serviceName = attributes.getValue(AT_SERVICE_NAME);
		id = attributes.getValue(AT_ID);
	}

	public void register() {
		if (id != null) {
			CatalogueDatabase.getInstance().register(id, this);
		}
	}
//	public void regist() {
//		PlatformService old = (PlatformService) CatalogueDatabase.getInstance().getCached(id);
//		if (old == null) {
//			CatalogueDatabase.getInstance().regist(id, this);
//		} else {
//			old.dispose();
//			old.platformName = this.platformName;
//			old.serviceName = this.serviceName;
//			old.id = this.id;
//		}
//	}

	public void dispose() {
		platformName = null;
		serviceName = null;
		id = null;
	}
}

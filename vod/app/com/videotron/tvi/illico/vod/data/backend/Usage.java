package com.videotron.tvi.illico.vod.data.backend;

public class Usage {
    private String pageID;
    private String pageAtttrID;
    private String funcID;
    private String funcAttrID;

    /**
     * @return the funcAttrID
     */
    public String getFuncAttrID() {
        return funcAttrID;
    }

    /**
     * @param funcAttrID
     *            the funcAttrID to set
     */
    public void setFuncAttrID(String funcAttrID) {
        this.funcAttrID = funcAttrID;
    }

    /**
     * @return the funcID
     */
    public String getFuncID() {
        return funcID;
    }

    /**
     * @param funcID
     *            the funcID to set
     */
    public void setFuncID(String funcID) {
        this.funcID = funcID;
    }

    /**
     * @return the pageAtttrID
     */
    public String getPageAtttrID() {
        return pageAtttrID;
    }

    /**
     * @param pageAtttrID
     *            the pageAtttrID to set
     */
    public void setPageAtttrID(String pageAtttrID) {
        this.pageAtttrID = pageAtttrID;
    }

    /**
     * @return the pageID
     */
    public String getPageID() {
        return pageID;
    }

    /**
     * @param pageID
     *            the pageID to set
     */
    public void setPageID(String pageID) {
        this.pageID = pageID;
    }
}

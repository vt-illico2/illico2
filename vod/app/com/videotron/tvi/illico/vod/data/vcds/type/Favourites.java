package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

/**
 * Created by zestyman on 2017-03-20.
 */
public class Favourites extends CategoryContainer {
    public int count;

    public void setAttribute(Attributes attributes) {
        count = Integer.parseInt(attributes.getValue(Definition.AT_COUNT));
        super.setAttribute(attributes);
    }

    public void dispose() {

    }

    public String toString() {
        return "Favourites, count=" + count + ", categoryContainer=" + categoryContainer;
    }
}

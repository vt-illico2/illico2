package com.videotron.tvi.illico.vod.data.vcds.type;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

/**
 * Created by zestyman on 2017-02-23.
 */
public class MPlexChannels extends BaseElement {

    public Channel[] channel = new Channel[0];

    public void endElement(String qName, BaseElement element) {
        if (EL_CHANNEL.equals(qName)) {
            channel = (Channel[]) addArray(channel, Channel.class, element);
        }
    }

    public void setDataAt(String qName, String data) {
        if (EL_CHANNEL_REF.equals(qName)) {
            Object cache = CatalogueDatabase.getInstance().getCached(data);
            if (cache != null) {
                channel = (Channel[]) addArray(channel, Channel.class, cache);
            }
        }
    }

    public void dispose() {
    }
}

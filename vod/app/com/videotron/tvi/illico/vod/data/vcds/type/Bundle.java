package com.videotron.tvi.illico.vod.data.vcds.type;

import java.util.ArrayList;
import java.util.Date;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Bundle extends Orderable implements CacheElement {
	public String episodeNumberString, episodeNumberStringList;
	public BundleDetails bundleDetail;
	
	public boolean collapsed;

	public boolean hasContent(BaseElement element) {
		if (bundleDetail == null || element == null || element instanceof MoreDetail == false) {
			return false;
		}
		MoreDetail md = (MoreDetail) element;
		for (int i = 0; i < bundleDetail.episode.length; i++) {
			if (md.getId().equals(bundleDetail.episode[i].getId())) {
				return true;
			}
		}
		for (int i = 0; i < bundleDetail.video.length; i++) {
			if (md.getId().equals(bundleDetail.video[i].getId())) {
				return true;
			}
		}
		return false;
	}
	
	public MoreDetail[] getTitles() {
		if (bundleDetail == null) {
			return new MoreDetail[0];
		}
		if (bundleDetail.episode.length > 0) {
			return bundleDetail.episode;
		}
		return bundleDetail.video;
	}
	
	public boolean isMovieBundle() {
		return bundleDetail == null || bundleDetail.video.length > 0;
	}
	
	public boolean hasAllData() {
		if (bundleDetail == null) {
			return false;
		}
		if (bundleDetail.videoRef.length > 0) {
			return false;
		}
		if (bundleDetail.episodeRef.length > 0) {
			return false;
		}
		
		MoreDetail[] md = getTitles();
		for (int i = 0; i < md.length; i++) {
			Video v = (Video) md[i];
			if (v.detail.length == 0) return false;
		}
		return true;
	}
	
	public String[] getSspIds() {
		ArrayList list = new ArrayList();
		for (int i = 0; i < detail.length; i++) {
			list.add(detail[i].orderableItem);
		}
		String[] ret = new String[list.size()];
		list.toArray(ret);
		return ret;
	}
	
	public String getSspId() {
		String sspId = getSspId(Resources.HD_FR);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.HD_EN);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.SD_FR);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.SD_EN);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		return "";
	}
	public String getSspId(int select) {
		String lang = "", definition = "";
		
		boolean isUhd = false;
		// VDTRMASTER-5422
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		if (isUhd) {
			definition = DEFNITION_UHD;
			switch (select) {
			case Resources.HD_FR:
				lang = LANGUAGE_FR;
				break;
			case Resources.HD_EN:
				lang = LANGUAGE_EN;
			}
		} else {
			switch (select) {
			case Resources.HD_FR: 
				lang = LANGUAGE_FR;
				definition = "HD";
				break;
	    	case Resources.HD_EN: 
	    		lang = LANGUAGE_EN;
				definition = "HD";
				break;
	    	case Resources.SD_FR: 
	    		lang = LANGUAGE_FR;
				definition = "SD";
				break;
	    	case Resources.SD_EN:
	    		lang = LANGUAGE_EN;
				definition = "SD";
				break;
			}
		}
		for (int i = 0; i < detail.length; i++) {
			if (offer[i].definition.indexOf(definition) != -1
				&& lang.equals(detail[i].language)) {

				return detail[i].orderableItem;
			}
		}
		return "";
	}
	
	public boolean isFree() {
		if (offer.length == 0) {
			return false;
		}
		for (int i = 0; i < offer.length; i++) {
			double amt = Double.parseDouble(offer[i].amount);
			if (amt > 0) return false;
		}
		return true;
	}
	
	public boolean isFree(int select) {
		String lang = "", definition = "";
		
		boolean isUhd = false;
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		if (isUhd) {
			definition = DEFNITION_UHD;
			switch (select) {
			case Resources.HD_FR:
				lang = LANGUAGE_FR;
				break;
			case Resources.HD_EN:
				lang = LANGUAGE_EN;
			}
		} else {
			switch (select) {
			case Resources.HD_FR: 
				lang = LANGUAGE_FR;
				definition = "HD";
				break;
	    	case Resources.HD_EN: 
	    		lang = LANGUAGE_EN;
				definition = "HD";
				break;
	    	case Resources.SD_FR: 
	    		lang = LANGUAGE_FR;
				definition = "SD";
				break;
	    	case Resources.SD_EN:
	    		lang = LANGUAGE_EN;
				definition = "SD";
				break;
			}
		}
		for (int i = 0; i < offer.length; i++) {
			if (offer[i].definition.indexOf(definition) != -1
				&& lang.equals(offer[i].language)) {

				double amt = Double.parseDouble(offer[i].amount);
				if (amt > 0) return false;
			}
		}
		return true;
	}

	public String getEpisodeNumberString(boolean list) {
		if (list) {
			if (episodeNumberStringList != null) {
				return episodeNumberStringList;
			}
		} else {
			if (episodeNumberString != null) {
				return episodeNumberString;
			}
		}
		if (bundleDetail.episode.length == 0) {
			return "";
		}
		if (list == false || bundleDetail.episode.length > 999 || (bundleDetail.episode.length > 5 && bundleDetail.episode[bundleDetail.episode.length-1].episodeNumber == bundleDetail.episode.length)) { // jira 3223
			episodeNumberString = bundleDetail.episode[0].episodeNumber
				+ " "
				+ DataCenter.getInstance().getString(
						Resources.TEXT_BUNDLE_NO)
				+ " "
				+ bundleDetail.episode[bundleDetail.episode.length - 1].episodeNumber;
			return episodeNumberString;
		}
		if (bundleDetail.episode.length == 1) {
			return String.valueOf(bundleDetail.episode[0].episodeNumber);
		}
		StringBuffer buf = new StringBuffer(String.valueOf(bundleDetail.episode[0].episodeNumber));
		for (int i = 1; i < bundleDetail.episode.length; i++) {
			buf.append(", ").append(bundleDetail.episode[i].episodeNumber);
		}
		episodeNumberStringList = buf.toString();
		return episodeNumberStringList;
	}

	public boolean isPortrait() { 
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		if (getTitles().length > 0) {
			return getTitles()[0].isPortrait();
		}
		return true;
	}

	public String getDescription() {
		return description == null ? NA
				: getLanguageContentData(description.description);
	}

	public String getGenre() {
		return bundleDetail == null ? NA : bundleDetail.getGenre();
	}

	public boolean hasClosedCaption() {
		return bundleDetail == null ? false : bundleDetail.hasClosedCaption();
	}
	
	public Date getStart() {
		if (platformService != null) return platformService.validityPeriod.start;
		return super.getStart();
	}
	
	public int getType(String orderableName) {
		if (orderableName == null) {
			return -1;
		}
		for (int i = 0; i < detail.length; i++) {
			if (orderableName.equals(detail[i].orderableItem)) {
				if (LANGUAGE_FR.equals(detail[i].language)) {
					if (detail[i].definition.indexOf("HD") != -1) {
						return Resources.HD_FR;
					} else {
						return Resources.SD_FR;
					}
				} else {
					if (detail[i].definition.indexOf("HD") != -1) {
						return Resources.HD_EN;
					} else {
						return Resources.SD_EN;
					}
				}
			}
		}
		return -1;
	}

	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		version = attributes.getValue(AT_VERSION);
		titleId = attributes.getValue(AT_TITLE_ID);
		isAllScreenType = Boolean.valueOf(attributes.getValue(AT_IS_ALL_SCREEN_TYPE)).booleanValue();
	}

	public void endElement(String qName, BaseElement element) {
		super.endElement(qName, element);
		if (EL_BUNDLE_DETAIL.equals(qName)) {
			bundleDetail = (BundleDetails) element;
		}
	}

	public void setDataAt(String qName, String data) {
		if (EL_IN_BUNDLES.equals(qName)) {
			inBundles = data;
			inBundlesAR = TextUtil.tokenize(data, ' ');
//		} else if (EL_NOT_LISTED.equals(qName)) {
//			notListed = data;
		}
	}

	public void register() {
		if (bundleDetail != null) {
			bundleDetail.sort();
		}
		CatalogueDatabase.getInstance().register(id, this);
	}

	public String toString() {
		return toString(true);
	}

	public String toString(boolean detail) {
		if (description == null) {
			return "Bundle(" + id + ") = NO description";
		}
		return "Bundle(" + id + ") = "
				+ getLanguageContentData(this.description.title);
	}
	
	public void dump() {
		System.out.println("=== dump === " + toString(true));
		MoreDetail[] md = getTitles();
		for (int i = 0; i < md.length; i++) {
			System.out.println("[" + i + "] = " + md[i].toString(true));
		}
		System.out.println("=== dump done. ===");
	}

	public void dispose() {
		if (bundleDetail != null)
			bundleDetail.dispose();
		bundleDetail = null;
		
		episodeNumberString = null;
		episodeNumberStringList = null;
		
		super.dispose();
	}
}

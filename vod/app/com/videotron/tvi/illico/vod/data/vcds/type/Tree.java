package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Tree extends BaseElement implements CacheElement {
	public String id;
	public CategoryContainer[] categoryContainer = new CategoryContainer[0];
	public ShowcaseContainer[] showcaseContainer = new ShowcaseContainer[0];
	//public String categoryContainerRef;
	
	public String getId() { return id; }
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_CATEGORY_CONTAINER.equals(qName)) {
			categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, element);
		} else if (EL_SHOWCASE_CONTAINER.equals(qName)) {
			showcaseContainer = (ShowcaseContainer[]) addArray(showcaseContainer, ShowcaseContainer.class, element);
		} else if (EL_TOPIC_POINTER.equals(qName)) {
			CategoryContainer wrap = new CategoryContainer();
			wrap.type = MenuController.MENU_CATEGORY;
			wrap.topicPointer = new TopicPointer[] {
					(TopicPointer) element
			};
			categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, wrap);
		} else if (EL_CATEGORIZED_BUNDLE_CONTAINER.equals(qName)) {
			categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, element);
			//categoryContainer[categoryContainer.length-1].parent = this;
		}
	}
	public void setDataAt(String qName, String data) {
		if (EL_CATEGORY_CONTAINER_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, cached);
//			} else {
//				categoryContainerRef = data;
			}
		}
	}
	
	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}
//	public void regist() {
//		Tree old = (Tree) CatalogueDatabase.getInstance().getCached(id);
//		if (old == null) {
//			CatalogueDatabase.getInstance().regist(id, this);		
//		} else {
//			old.dispose();
//			old.id = this.id;
//			old.categoryContainer = this.categoryContainer;
//			old.categoryContainerRef = this.categoryContainerRef;
//		}
//	}
	
	public void reset() {
		categoryContainer = new CategoryContainer[0];
	}

	public void dispose() {
		id = null;
		for (int i = 0; categoryContainer != null && i < categoryContainer.length; i++) {
			if (categoryContainer[i] != null) categoryContainer[i].dispose();
			categoryContainer[i] = null;
		}
//		categoryContainer = null;
		for (int i = 0; showcaseContainer != null && i < showcaseContainer.length; i++) {
			if (showcaseContainer[i] != null) showcaseContainer[i].dispose();
			showcaseContainer[i] = null;
		}
	}
}

/**
 * 
 */
package com.videotron.tvi.illico.vod.data.similar.type;

import org.json.simple.JSONObject;

import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class SType extends BaseJson {

	private int code;
	private String description = TextUtil.EMPTY_STRING;
	
	public int getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}
	
	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.vod.data.similar.type.BaseJson#parseJson(java.lang.Object)
	 */
	public void parseJson(Object obj) throws Exception {

		JSONObject jObj = (JSONObject) obj;
		
		code = (int) getLong(jObj, "code");
		description = getString(jObj, "description");
	}

}

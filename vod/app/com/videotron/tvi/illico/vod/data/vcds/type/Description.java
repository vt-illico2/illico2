package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Description extends BrandingElement implements HasDupNamed {
	public LanguageContent[] title;
	public LanguageContent[] description;
	public LanguageContent[] subscriptionInfo;
//	public Poster[] poster = new Poster[0]; // removed R2
	public Image[] background = new Image[0];
//	public ImageGroup[] imageGroup = new ImageGroup[0];
	
	public Description() {
		title = new LanguageContent[NUMBER_OF_LANGUAGE];
		description = new LanguageContent[NUMBER_OF_LANGUAGE];
		subscriptionInfo = new LanguageContent[NUMBER_OF_LANGUAGE];
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return LanguageContent.class;
		}
		return null;
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_TITLE.equals(qName)) {
			setLanguageContent(title, (LanguageContent) element);
		} else if (EL_DESCRIPTION.equals(qName)) {
			setLanguageContent(description, (LanguageContent) element);
		} else if (EL_SUBSCRIPTION_INFO.equals(qName)) {
			setLanguageContent(subscriptionInfo, (LanguageContent) element);
		} else if (EL_BACKGROUND.equals(qName)) {
			background = (Image[]) addArray(background, Image.class, element);
		} else if (EL_IMAGE_GROUP.equals(qName)) {
			imageGroup = (ImageGroup[]) addArray(imageGroup, ImageGroup.class, element);
			((ImageGroup) element).title = getLanguageContentData(title);
		}
	}
	
	public String toString(boolean detail) {
		return "Description of " + getLanguageContentData(title);
	}
	
	public void dispose() {
		for (int i = 0; title != null && i < title.length; i++) {
			if (title[i] != null) title[i].dispose();
			title[i] = null;
		}
//		title = null;
		for (int i = 0; description != null && i < description.length; i++) {
			if (description[i] != null) description[i].dispose();
			description[i] = null;
		}
		for (int i = 0; subscriptionInfo != null && i < subscriptionInfo.length; i++) {
			if (subscriptionInfo[i] != null) subscriptionInfo[i].dispose();
			subscriptionInfo[i] = null;
		}
//		description = null;
//		for (int i = 0; poster != null && i < poster.length; i++) {
//			if (poster[i] != null) poster[i].dispose();
//			poster[i] = null;
//		}
//		poster = null;
		for (int i = 0; background != null && i < background.length; i++) {
			if (background[i] != null) background[i].dispose();
			background[i] = null;
		}
//		background = null;
		for (int i = 0; imageGroup != null && i < imageGroup.length; i++) {
			if (imageGroup[i] != null) imageGroup[i].dispose();
			imageGroup[i] = null;
		}
	}
}

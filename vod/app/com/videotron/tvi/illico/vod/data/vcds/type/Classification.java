package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Classification extends BaseElement {
	public String category;
	public String addInfo;
	
	public void setAttribute(Attributes attributes) {
		category = attributes.getValue(AT_CATEGORY);
		addInfo = attributes.getValue(AT_ADD_INFO);
	}

	public void dispose() {
		category = null;
		addInfo = null;
	}
}

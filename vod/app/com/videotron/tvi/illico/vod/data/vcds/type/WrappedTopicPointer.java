package com.videotron.tvi.illico.vod.data.vcds.type;

/**
 * Created by zestyman on 2017-03-13.
 */
public class WrappedTopicPointer extends BaseElement {

    public Favourites favourites;
    public Subscribed subscribed;
    public Unsubscribed unsubscribed;

    public void endElement(String qName, BaseElement element) {

        if (EL_FAVORITES.equals(qName)) {
            favourites = (Favourites) element;
        } else if (EL_SUBSCRIBED.equals(qName)) {
            subscribed = (Subscribed) element;
        } else if (EL_UNSUBSCRIBED.equals(qName)) {
            unsubscribed = (Unsubscribed) element;
        }
    }

    public void dispose() {

        if (favourites != null) {
            favourites.dispose();
            favourites = null;
        }

        if (subscribed != null) {
            subscribed.dispose();
            subscribed = null;
        }

        if (unsubscribed != null) {
            unsubscribed.dispose();
            unsubscribed = null;
        }
    }

    public String toString() {
        return "favorites=" + favourites + ", subscribed=" + subscribed +", unsubscribed=" + unsubscribed;
    }
}

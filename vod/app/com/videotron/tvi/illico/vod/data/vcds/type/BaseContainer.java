package com.videotron.tvi.illico.vod.data.vcds.type;

import java.lang.reflect.Field;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

//common code for "xxxContainer" class
public class BaseContainer extends BaseElement implements HasDupNamed { 
	public PlatformServiceSpecific platformServiceSpecific;
	public PlatformValidity platformValidity;
	public Service[] service = new Service[0];
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class; 
		}
		return null;
	}
	
	public boolean isValid() { return platformServiceSpecific != null; }
	public int getPosition() { 
		return platformServiceSpecific == null ? 0 : platformServiceSpecific.position;
	}
	
	//R7.4 sykim VDTRMASTER-6175
	public void endElement(String qName, BaseElement element) throws SAXException{
	//public void endElement(String qName, BaseElement element){
		if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformServiceSpecific = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		} else if (EL_SERVICE.equals(qName)) {
			service = (Service[]) addArray(service, Service.class, element);
		} else {
			
			try {
				Field f = getClass().getDeclaredField(qName);
				f.set(this, element);
			} catch (Exception e) {
				//R7.4 sykim VDTRMASTER-6175
				e.printStackTrace();
				throw CatalogueDatabase.UNEXPECTED_ELEMENTS;
			}
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (qName.endsWith("Ref")) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			try {
				if (cached != null) { 
					Field f = getClass().getDeclaredField(qName.substring(0, qName.length()-3));
					f.set(this, cached); // xxx
				} else {
					Field f = getClass().getDeclaredField(qName);
					f.set(this, data); // xxxRef
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void dispose() {
		if (platformServiceSpecific != null) platformServiceSpecific.dispose();
		platformServiceSpecific = null;
	}

}

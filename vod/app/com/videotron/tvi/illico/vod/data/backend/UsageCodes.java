package com.videotron.tvi.illico.vod.data.backend;

public class UsageCodes {
    public final static int SCREEN_TOP_CATEGORY = 1103;
    public final static int SCREEN_SUB_CATEGORY = 1104;
    public final static int SCREEN_TITLE_DETAIL = 1105;
    public final static int SCREEN_CONFIRMATION = 1106;
    public final static int SCREEN_MY_PURCHASES = 1107;
    public final static int SCREEN_ERROR = 1108;

    public final static int FUNC_SELECTED_CATEGORY = 1201;
    public final static int FUNC_SELECTED_TITLE = 1202;
    public final static int FUNC_ACTIVATED_PREVIEW = 1203;
    public final static int FUNC_PURCHASED_TITLE = 1204;
    public final static int FUNC_ACTIVATED_PROMOTION = 1205;
    public final static int FUNC_ACTIVATED_ALTERNATE_LANGUAGE = 1206;
    public final static int FUNC_ADDED_TO_MY_SELECTION = 1207;
    public final static int FUNC_REMOVED_FROM_MY_SELECTION = 1208;
    public final static int FUNC_STARTED_VOD_SESSION = 1209;
    public final static int PAUSE = 1301;
    public final static int FUNC_FAST_FORWARD = 1302;
    public final static int RESUME = 1303;
    public final static int JUMP = 1304;
    public final static int STOP = 1405;
    public final static int FUNC_END_OF_CONTENT = 1306;
    public final static int FUNC_ENDED_VOD_SESSION = 1307;
}

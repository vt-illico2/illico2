/**
 * 
 */
package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 *
 */
public class ExternalRef extends BaseElement {

	public String type;
	public String field;
	public String value;

	public void setAttribute(Attributes attributes) {
		type = attributes.getValue(Definition.AT_TYPE);
		field = attributes.getValue(Definition.AT_FIELD);
		value = attributes.getValue(Definition.AT_VALUE);

		super.setAttribute(attributes);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.videotron.tvi.illico.vod.data.vcds.type.BaseElement#dispose()
	 */
	public void dispose() {
		type = null;
		field = null;
		value = null;
	}

}

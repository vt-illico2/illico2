package com.videotron.tvi.illico.vod.data.vcds.type;


public class SeasonContainer extends BaseContainer {
	public Season season; // or ref
	public String seasonRef;
	
	public String toString(boolean detail) {
		if (season == null) {
			return "season is " + seasonRef;
		}
		return season.toString(detail);
	}
	
	public void dispose() {
		super.dispose();
		if (season != null) season.dispose();
		season = null;
	}
}

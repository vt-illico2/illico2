package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Orderable extends MoreDetail implements HasDupNamed {
	public String id;
	public String version;
	public String titleId;
	public boolean isAllScreenType;
	public ContentDescription description;
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	public Service[] service = new Service[0];
	public VideoDetails[] detail = new VideoDetails[0];
	public String inBundles;
	public String[] inBundlesAR;
	public String inCollectionExtras;
	public String[] inCollectionExtrasAR;
	public Offer[] offer = new Offer[0];

	public Offer[] offerSD = new Offer[0];
	public Offer[] offerHD = new Offer[0];

	private boolean reset = false;

	public void reset() {
		reset = true;
	}

	protected void realReset() {
		reset = false;
		detail = new VideoDetails[0];
		offer = new Offer[0];
		offerSD = new Offer[0];
		offerHD = new Offer[0];
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		String title = description == null ? NA : getLanguageContentData(description.title);
		return title;
	}
	
	// R5
	public String getOriginalTitle() {
		String oTitle = description == null ? null : description.originalTitle;
		return oTitle;
	}

	public java.awt.Image getImage(int width, int height) {
		return getImage(description, width, height);
	}

	public Object chooseImage() {
		if (description == null) {
			return null;
		}
		if (description.imageGroup.length > 0) {
			return chooseImageGroup(description.imageGroup);
		}
		return null;
	}

	public Playout getPreview(String lang) {
		for (int i = 0; i < detail.length; i++) { // check first - language match
			if (detail[i].language.equalsIgnoreCase(lang) && detail[i].previewPlayout.length > 0) {
				return detail[i].previewPlayout[0];
			}
		}
		for (int i = 0; i < detail.length; i++) { // check second - any language
			if (detail[i].previewPlayout.length > 0) {
				return detail[i].previewPlayout[0];
			}
		}
		return null;
	}

	public long getLeaseDuration() {
		return offer.length == 0 ? 0L : offer[0].leaseDuration;
	}

	public String getRating() {
		return description == null ? NA : description.getRating();
	}
	
	public String getRatingInfo() {
		return description == null ? NA : description.getRatingInfo();
	}

	public boolean is3screen() {
		return isAllScreenType;
	}

	public boolean hasEnglish() {
		for (int i = 0; offer != null && i < offer.length; i++) {
			if (LANGUAGE_EN.equals(offer[i].language)) return true;
		}
		return false;
	}
	public boolean hasFrench() {
		for (int i = 0; offer != null && i < offer.length; i++) {
			if (LANGUAGE_FR.equals(offer[i].language)) return true;
		}
		return false;	
	}
	
	public Offer getOfferSD(String lang) {
		if (offerSD.length == 0) {
			return null;
		}
		if (lang == null) {
			return offerSD[0];
		}
		for (int i = 0; i < offerSD.length; i++) {
			if (lang.equalsIgnoreCase(offerSD[i].language)) return offerSD[i];
		}
		return null;
	}

	public Offer getOfferHD(String lang) {
		if (offerHD.length == 0) {
			return null;
		}
		if (lang == null) {
			return offerHD[0];
		}
		for (int i = 0; i < offerHD.length; i++) {
			if (lang.equalsIgnoreCase(offerHD[i].language)) return offerHD[i];
		}
		return null;
	}
	public Offer getOffer(Playout p) {
		String lang = null, def = null;
		
		out:
		for (int i = 0; i < detail.length; i++) {
			for (int j = 0; j < detail[i].videoPlayout.length; j++) {
				if (detail[i].videoPlayout[j].equals(p)) {
					lang = detail[i].language;
					def = detail[i].definition;
					break out;
				}
			}
			for (int j = 0; j < detail[i].previewPlayout.length; j++) {
				if (detail[i].previewPlayout[j].equals(p)) {
					lang = detail[i].language;
					def = detail[i].definition;
					break out;
				}
			}
		}
		
		if (lang == null || def == null) {
			return null;
		}
		
		for (int i = 0; i < offer.length; i++) {
			if (offer[i].language.equals(lang) == false) {
				continue;
			}
			if (offer[i].definition.equals(def) == false) {
				continue;
			}
			return offer[i];
		}
		
		return null;
	}
	
	public String getEndPeriod() { 
		// support 8
		try {
			return df.format(platformValidity.validityPeriod.end);
		} catch (Exception e) {
			try {
				return df.format(platformService.validityPeriod.end);
			} catch (Exception ee) {}
		}
		return "";
	}

	public int getFlag() {
		if (description == null) {
			return -1;
		}
		// tagsInfo added, jira 2794
		if ("LC".equals(description.tagsInfo)) {
			return 2;
		}
		
		if ("EX".equals(description.tagsInfo)) {
			return 3;
		}
		return -1;
	}

	public Class getClass(String qName, Attributes attributes) {
		if (EL_DETAIL.equals(qName)) {
			return VideoDetails.class;
		} else if (EL_DESCRIPTION.equals(qName)) {
			return ContentDescription.class;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class;
		}
		return null;
	}

	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			if (description != null) {
				description.updateWith((ContentDescription) element);
			} else {
				description = (ContentDescription) element;
			}
			description.sort();
		} else if (EL_DETAIL.equals(qName)) {
			if (reset) { realReset(); }
			detail = (VideoDetails[]) addArray(detail, VideoDetails.class, element);
		} else if (EL_OFFER.equals(qName)) {
			if (reset) { realReset(); }
			Offer o = (Offer) element;
			offer = (Offer[]) addArray(offer, Offer.class, o);
			if (DEFNITION_HD.equals(o.definition)) {
				offerHD = (Offer[]) addArray(offerHD, Offer.class, o);
			} else if (DEFNITION_FHD.equals(o.definition)) {
				offerHD = (Offer[]) addArray(offerHD, Offer.class, o);
			} else if (DEFNITION_UHD.equals(o.definition)) { // 4K
				offerHD = (Offer[]) addArray(offerHD, Offer.class, o);
			} else if (DEFNITION_SD.equals(o.definition)) {
				offerSD = (Offer[]) addArray(offerSD, Offer.class, o);
			}
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		} else if (EL_SERVICE.equals(qName)) {
			service = (Service[]) addArray(service, Service.class, element);
		}
	}

	public String getServiceTitle() {
		String title = NA;
		for (int i = 0; i < service.length; i++) {
			if (service[i].serviceType == Service.SVOD) {
				if (service[i].description == null) {
					Service cached = (Service) CatalogueDatabase.getInstance().getCached(service[i].id);
					if (cached != null) {
						service[i] = cached;
					}
				}
				title = service[i].getTitle();
				break;
			}
		}
		return title;
	}

	public boolean hasPacakgeForServiceConfigType() {

		for (int i = 0; i < service.length; i++) {
			if (service[i].serviceType == Service.CONFIG) {
				return CommunicationManager.getInstance().checkPackage(service[i]);
			}
		}

		return true;
	}
	
	public void dispose() {
		super.dispose();
		for (int i = 0; i < service.length; i++) {
			service[i] = null;
		}
		service = new Service[0];
		for (int i = 0; i < detail.length; i++) {
			if (detail[i] != null) {
				detail[i].dispose();
			}
			detail[i] = null;
		}
		detail = new VideoDetails[0];
		for (int i = 0; i < offer.length; i++) {
			if (offer[i] != null) {
				offer[i].dispose();
			}
			offer[i] = null;
		}
		offer = new Offer[0];

		offerSD = new Offer[0];
		offerHD = new Offer[0];
	}

	public String dumpOffer() {
		StringBuffer b = new StringBuffer();
		for (int i = 0; i < offer.length; i++) {
			b.append("[").append(i).append("]=").append(offer[i].toString()).append("\n");
			b.append(offer[i].period.toString(true)).append("\n");
		}
		return b.toString();
	}

	public String dumpPlayout() {
		StringBuffer b = new StringBuffer();
		b.append(toString(true)).append("\n");
		for (int i = 0; i < detail.length; i++) {
			b.append('[').append(i).append(']').append(detail[i].toString()).append("\n");
		}
		return b.toString();
	}

	public String[] dumpImages() {
		if (description == null) {
			return new String[] {"NO description"};
		}
		return description.dumpImages();
	}

	public String dumpService() {
		if (service.length == 0) {
			return "NO service";
		}
		StringBuffer b = new StringBuffer("Service=");
		for (int i = 0; i < service.length; i++) {
			b.append(service[i].id);
			if (CommunicationManager.getInstance().checkPackage(service[i])) {
				b.append("$,");
			} else {
				b.append(",");
			}
		}
		return b.toString();
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import java.awt.Rectangle;

import org.xml.sax.Attributes;

public class ImageGroup extends BaseElement {
	public String usage;
	public boolean portrait;
	public boolean specialSize;
	public String encoding;
	public String language;
	public String ratio;
	public Image[] images = new Image[0];
	
	public String title;
	
//	public String getUrl(boolean large) {
//		// temp logic
//		int width = -1;
//		for (int i = 0; i < images.length; i++) {
//			if (width < images[i].width) {
//				width = images[i].width;
//			}
//		} // find largest
//		for (int i = 0; i < images.length; i++) {
//			if (width == images[i].width) {
//				return "/images/" + images[i].imageName + "." + encoding;
//			}
//		}
//		return "";
//	}
	
	public String toString() {
		return toString(true);
	}
	
	public Image getImage(Rectangle r) {
		for (int i = 0; i < images.length; i++) {
			if (images[i].match(r)) {
				return images[i];
			}
		}
		return null;
	}
	
	public String toString(boolean detail) {
		String names = "";
		for (int i = 0; i < images.length; i++) {
			names += images[i].imageName + "|";
		}
		return "ImageGroup, [" + language + ", " + usage + ", " + encoding + ", " + images.length + " images("+names+"), " + ratio + "]"; 
	}
	
	public void setAttribute(Attributes attributes) {
		usage = attributes.getValue(AT_USAGE);
		ratio = attributes.getValue(AT_RATIO);
		portrait = "2_3".equals(ratio);
		specialSize = !portrait && !"4_3".equals(ratio); // not portrait, not landscape, it's special size
		encoding = attributes.getValue(AT_ENCODING);
		language = attributes.getValue(AT_LANGUAGE);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_IMAGE.equals(qName)) {
			images = (Image[]) addArray(images, Image.class, element);
			((Image) element).parent = this;
		} else if (EL_RESIZABLE_IMAGES.equals(qName)) {
			ResizableImages ri = (ResizableImages) element;
			for (int i = 0; i < ri.images.length; i++) {
				images = (Image[]) addArray(images, Image.class, ri.images[i]);
				ri.images[i].parent = this;
			}
			ri.dispose();
		}
	}

	public void dispose() {
		usage = null;
		encoding = null;
		language = null;
		for (int i = 0; images != null && i < images.length; i++) {
			if (images[i] != null) {
				images[i].parent = null;
				images[i].dispose();
			}
			images[i] = null;
		}
	}
}

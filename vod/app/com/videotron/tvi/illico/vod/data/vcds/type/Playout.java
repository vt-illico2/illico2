package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Playout extends BaseElement {
	public String encoding;
	public boolean isEncrypted;
	public int copyProtectionType;
	public boolean isClosedCaptioned;
	public String name;
	public int duration; // in seconds
	public int type;
	
	public String title; // related title, it may be null
	
	public void setAttribute(Attributes attributes) {
		encoding = attributes.getValue(AT_ENCODING);
		isEncrypted = Boolean.valueOf(attributes.getValue(AT_IS_ENCRYPTED)).booleanValue();
		String cp = attributes.getValue(AT_COPY_PROTECTION_TYPE);
		if ("CO".equals(cp)) { // Copy Once
			copyProtectionType = 1;
		} else if ("CF".equals(cp)) { // Copy Freely
			copyProtectionType = 2;
		} else { // Copy Never
			copyProtectionType = 0;
		}
		isClosedCaptioned = Boolean.valueOf(attributes.getValue(AT_IS_CLOSED_CAPTIONED)).booleanValue();
		try {
			duration = Integer.parseInt(attributes.getValue(AT_DURATION));
		} catch (NumberFormatException nfe) {}
	}
	
	public void setData(String data) {
		name = data;
	}

	public void dispose() {
		encoding = null;
		name = null;
	}
	
	public String toString() {
		return name + "|" + encoding + "|" + isEncrypted + "|" + copyProtectionType + "|" + isClosedCaptioned + "|" + type;
	}
}

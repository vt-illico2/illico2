package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Episode extends Video {
	public int episodeNumber;
	public Season season; // or ref
	public String seasonRef;
	
	public boolean isPortrait() { 
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		return false;
	}
	
	public void setAttribute(Attributes attributes) {
		try {
			episodeNumber = Integer.parseInt(attributes.getValue(AT_EPISODE_NUMBER));
		} catch (NumberFormatException e) {}
		super.setAttribute(attributes);
	}
	
	public void endElement(String qName, BaseElement element) {
		super.endElement(qName, element);
		if (EL_SEASON.equals(qName)) {
			season = (Season) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		super.setDataAt(qName, data);
		if (EL_SEASON_ID.equals(qName) || EL_SEASON_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				season = (Season) cached;
			} else {
				seasonRef = data;
			}
		}
	}
	
	public String toString() {
		return id;
	}
	
	public String toString(boolean detail) {
		if (description == null) {
			return "Episode(" + id + ") = NO description";
		}
		return "Episode(" + id + ") = " + getLanguageContentData(this.description.title);
	}
	
	public void dispose() {
		super.dispose();
		//if (season != null) season.dispose();
		season = null;
	}
}

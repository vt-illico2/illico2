package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class VideoDetails extends BaseElement {
	public String orderableItem;
	public String language;
	public String definition;
	public Playout[] videoPlayout = new Playout[0];
	public String[] videoAudioType = new String[0];
	public Playout[] previewPlayout = new Playout[0];
	public String[] previewAudioType = new String[0];
	
	public void setAttribute(Attributes attributes) {
		orderableItem = attributes.getValue(AT_ORDERABLE_ITEM);
		language = attributes.getValue(AT_LANGUAGE);
		definition = attributes.getValue(AT_DEFINITION);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_VIDEO_PLAYOUT.equals(qName)) {
			videoPlayout = (Playout[]) addArray(videoPlayout, Playout.class, element);
		} else if (EL_PREVIEW_PLAYOUT.equals(qName)) {
			previewPlayout = (Playout[]) addArray(previewPlayout, Playout.class, element);
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_VIDEO_AUDIO_TYPE.equals(qName)) {
			videoAudioType = (String[]) addArray(videoAudioType, String.class, data);
		} else if (EL_PREVIEW_AUDIO_TYPE.equals(qName)) {
			previewAudioType = (String[]) addArray(previewAudioType, String.class, data);
		}
	}

	public void dispose() {
		orderableItem = null;
		language = null;
		definition = null;
		for (int i = 0; videoPlayout != null && i < videoPlayout.length; i++) {
			if (videoPlayout[i] != null) videoPlayout[i].dispose();
			videoPlayout[i] = null;
		}
//		videoPlayout = null;
		for (int i = 0; previewPlayout != null && i < previewPlayout.length; i++) {
			if (previewPlayout[i] != null) previewPlayout[i].dispose();
			previewPlayout[i] = null;
		}
//		previewPlayout = null;
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer("VideDetails|" + orderableItem + "|" + language + "|" + definition + "\n");
		for (int i = 0; i < videoPlayout.length; i++) {
			b.append("VP["+i+"]|" + videoPlayout[i].toString()).append("\n");
		}
		for (int i = 0; i < previewPlayout.length; i++) {
			b.append("PP["+i+"]|" + previewPlayout[i].toString()).append("\n");
		}
		return b.toString();
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import java.util.ArrayList;
import java.util.Date;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Extra extends Orderable implements CacheElement {
	public ExtraDetails extraDetail;
	
	public boolean collapsed;

	public boolean hasContent(BaseElement element) {
		if (extraDetail == null || element == null || element instanceof MoreDetail == false) {
			return false;
		}
		MoreDetail md = (MoreDetail) element;
		for (int i = 0; i < extraDetail.video.length; i++) {
			if (md.getId().equals(extraDetail.video[i].getId())) {
				return true;
			}
		}
		return false;
	}
	
	public MoreDetail[] getTitles() {
		if (extraDetail == null) {
			return new MoreDetail[0];
		}
		return extraDetail.video;
	}
	
	public boolean hasAllData() {
		if (extraDetail == null) {
			return false;
		}
		if (extraDetail.videoRef.length > 0) {
			return false;
		}
		
		MoreDetail[] md = getTitles();
		for (int i = 0; i < md.length; i++) {
			Video v = (Video) md[i];
			if (v.detail.length == 0) return false;
		}
		return true;
	}
	
	public String[] getSspIds() {
		ArrayList list = new ArrayList();
		for (int i = 0; i < detail.length; i++) {
			list.add(detail[i].orderableItem);
		}
		String[] ret = new String[list.size()];
		list.toArray(ret);
		return ret;
	}
	
	public String getSspId() {
		String sspId = getSspId(Resources.HD_FR);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.HD_EN);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.SD_FR);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.SD_EN);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		return "";
	}
	public String getSspId(int select) {
		String lang = "", definition = "";
		
		boolean isUhd = false;
		// VDTRMASTER-5422
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		if (isUhd) {
			definition = DEFNITION_UHD;
			switch (select) {
			case Resources.HD_FR:
				lang = LANGUAGE_FR;
				break;
			case Resources.HD_EN:
				lang = LANGUAGE_EN;
			}
		} else {
			switch (select) {
			case Resources.HD_FR: 
				lang = LANGUAGE_FR;
				definition = "HD";
				break;
	    	case Resources.HD_EN: 
	    		lang = LANGUAGE_EN;
				definition = "HD";
				break;
	    	case Resources.SD_FR: 
	    		lang = LANGUAGE_FR;
				definition = "SD";
				break;
	    	case Resources.SD_EN:
	    		lang = LANGUAGE_EN;
				definition = "SD";
				break;
			}
		}
		for (int i = 0; i < detail.length; i++) {
			if (offer[i].definition.indexOf(definition) != -1
				&& lang.equals(detail[i].language)) {

				return detail[i].orderableItem;
			}
		}
		return "";
	}
	
	public boolean isFree() {
		if (offer.length == 0) {
			return false;
		}
		for (int i = 0; i < offer.length; i++) {
			double amt = Double.parseDouble(offer[i].amount);
			if (amt > 0) return false;
		}
		return true;
	}
	
	public boolean isFree(int select) {
		String lang = "", definition = "";
		boolean isUhd = false;
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		if (isUhd) {
			definition = DEFNITION_UHD;
			switch (select) {
			case Resources.HD_FR:
				lang = LANGUAGE_FR;
				break;
			case Resources.HD_EN:
				lang = LANGUAGE_EN;
			}
		} else {
			switch (select) {
			case Resources.HD_FR: 
				lang = LANGUAGE_FR;
				definition = "HD";
				break;
	    	case Resources.HD_EN: 
	    		lang = LANGUAGE_EN;
				definition = "HD";
				break;
	    	case Resources.SD_FR: 
	    		lang = LANGUAGE_FR;
				definition = "SD";
				break;
	    	case Resources.SD_EN:
	    		lang = LANGUAGE_EN;
				definition = "SD";
				break;
			}
		}
		for (int i = 0; i < offer.length; i++) {
			if (offer[i].definition.indexOf(definition) != -1
				&& lang.equals(offer[i].language)) {

				double amt = Double.parseDouble(offer[i].amount);
				if (amt > 0) return false;
			}
		}
		return true;
	}

	public boolean isPortrait() { 
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		if (getTitles().length > 0) {
			return getTitles()[0].isPortrait();
		}
		return true;
	}

	public String getDescription() {
		return description == null ? NA
				: getLanguageContentData(description.description);
	}

	public String getGenre() {
		return extraDetail == null ? NA : extraDetail.getGenre();
	}

	public boolean hasClosedCaption() {
		return extraDetail == null ? false : extraDetail.hasClosedCaption();
	}
	
	public Date getStart() {
		if (platformService != null) return platformService.validityPeriod.start;
		return super.getStart();
	}
	
	public int getType(String orderableName) {
		if (orderableName == null) {
			return -1;
		}
		for (int i = 0; i < detail.length; i++) {
			if (orderableName.equals(detail[i].orderableItem)) {
				if (LANGUAGE_FR.equals(detail[i].language)) {
					if (detail[i].definition.indexOf("HD") != -1) {
						return Resources.HD_FR;
					} else {
						return Resources.SD_FR;
					}
				} else {
					if (detail[i].definition.indexOf("HD") != -1) {
						return Resources.HD_EN;
					} else {
						return Resources.SD_EN;
					}
				}
			}
		}
		return -1;
	}

	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		version = attributes.getValue(AT_VERSION);
		titleId = attributes.getValue(AT_TITLE_ID);
		isAllScreenType = Boolean.valueOf(attributes.getValue(AT_IS_ALL_SCREEN_TYPE)).booleanValue();
	}

	public void endElement(String qName, BaseElement element) {
		super.endElement(qName, element);
		if (EL_EXTRA_DETAIL.equals(qName)) {
			extraDetail = (ExtraDetails) element;
		}
	}

	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}

	public String toString() {
		return toString(true);
	}

	public String toString(boolean detail) {
		if (description == null) {
			return "Extra(" + id + ") = NO description";
		}
		return "Extra(" + id + ") = "
				+ getLanguageContentData(this.description.title);
	}
	
	public void dump() {
		System.out.println("=== dump === " + toString(true));
		MoreDetail[] md = getTitles();
		for (int i = 0; i < md.length; i++) {
			System.out.println("[" + i + "] = " + md[i].toString(true));
		}
		System.out.println("=== dump done. ===");
	}

	public void dispose() {
		if (extraDetail != null)
			extraDetail.dispose();
		extraDetail = null;
		
		super.dispose();
	}
}

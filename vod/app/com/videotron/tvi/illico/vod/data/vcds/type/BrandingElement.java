package com.videotron.tvi.illico.vod.data.vcds.type;

import java.awt.*;
import java.awt.Image;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

public class BrandingElement extends BaseElement {
	public static final int LOGO_ICON = 0;
	public static final int LOGO_SM = 1;
	public static final int LOGO_LINK_SMALL = 2;
	public static final int LOGO_LINK_LARGE = 3;
	public static final int LOGO_MD = 4;
	public static final int LOGO_LG = 5;
	
	public ImageGroup[] imageGroup = new ImageGroup[0];
	public java.awt.Image[] logos = new java.awt.Image[6];

	// R7.3
    public Image showcaseLogo;
	
	public ImageGroup getBackground(boolean narrow) {
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		ArrayList list = new ArrayList();
		String usage = narrow ? ImageRetriever.CHANNEL_BRAND_DETAIL_BG : ImageRetriever.CHANNEL_BRAND_BG;
		for (int i = 0; i < imageGroup.length; i++) {
			if (usage.equals(imageGroup[i].usage) == false) {
				continue;
			}
			if (lang.equals(imageGroup[i].language)) {
				list.add(0, imageGroup[i]);
			} else {
				list.add(imageGroup[i]);
			}
		}
		return (ImageGroup) (list.size() == 0 ? null : list.get(0));
	}
	public ImageGroup getLogo(int size) {
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		ArrayList list = new ArrayList();
		String usage = "";
		switch (size) {
		case LOGO_ICON: usage = ImageRetriever.CHANNEL_ICON_LOCO; break;
		case LOGO_SM: usage = ImageRetriever.CHANNEL_LISTING_LOGO; break;
		case LOGO_LINK_SMALL: usage = ImageRetriever.LINK_SMALL; break;
		case LOGO_LINK_LARGE: usage = ImageRetriever.LINK_LARGE; break;
		case LOGO_MD: usage = ImageRetriever.CHANNEL_TOP_MENU_LOGO; break;
		case LOGO_LG: usage = ImageRetriever.CHANNEL_OVERLAY_LOGO; break;
		}
		
		if (Log.EXTRA_ON) {
			Log.printDebug("BrandingElement, imageGroup length=" + imageGroup.length);
			Log.printDebug("BrandingElement, usage=" + usage);
		}
		
		for (int i = 0; i < imageGroup.length; i++) {
			Log.printDebug("BrandingElement, imageGroup[" + i +"].usage=" + imageGroup[i].usage);
			if (usage.equals(imageGroup[i].usage) == false) {
				continue;
			}
			if (lang.equals(imageGroup[i].language)) {
				list.add(0, imageGroup[i]);
			} else {
				list.add(imageGroup[i]);
			}
		}
		return (ImageGroup) (list.size() == 0 ? null : list.get(0));
	}
	synchronized public void loadLogo(boolean smallOnly, boolean largeOnly) {
		Log.printDebug("BrandingElement, loadLog(), smallOnly=" + smallOnly + ", largeOnly=" + largeOnly);
		MediaTracker mt = new MediaTracker(CategoryUI.getInstance());
		URL url;
		int cnt = 0;
		int limit = 6;
		if (smallOnly) {
			limit = 4;
		}
		if (largeOnly) {
			limit = 2;
		}
		for (int i = 0; i < limit; i++) {
			int idx = largeOnly ? logos.length - 2 + i : i;
			if (logos[idx] != null) {
				if (Log.EXTRA_ON) {
					Log.printDebug("loadLogo, logos[" + i + "] is not null.");
				}
				continue;
			}
			ImageGroup ig = getLogo(idx);
			if (ig == null) {
				if (Log.EXTRA_ON) {
					Log.printDebug("loadLogo, ImageGroup is null.");
				}
				continue;
			}
			try {
				url = new URL(DataCenter.getInstance().getString("IMAGE_URL") + ImageRetriever.BASE + ig.images[0].imageName + "." + ig.encoding);
				Log.printDebug("loadLogo("+idx+"), " + toString(true) + ", url = " + url.toExternalForm() + ", isEDT = " + EventQueue.isDispatchThread());
//				logos[idx] = Toolkit.getDefaultToolkit().createImage(url);
				logos[idx] = Toolkit.getDefaultToolkit().createImage(URLRequestor.getBytes(url.toExternalForm(), null));
				
				mt.addImage(logos[idx], 99);
				cnt++;
			} catch (Exception e) {}	
		}
		if (cnt > 0) {
			long t = System.currentTimeMillis();
			try {
				mt.waitForID(99, cnt * 5000);
			} catch (InterruptedException e) {}
			t = System.currentTimeMillis() - t;
			Log.printDebug("loadLogo(), " + toString(true) + ", elapsed = " + t + ", image = " + cnt);
		}
		for (int i = 0; i < logos.length; i++) {
			mt.removeImage(logos[i]);
		}
		if (MenuController.getInstance().getCurrentScene() != null) {
			MenuController.getInstance().getCurrentScene().repaint();
		}
	}

	// R7.3
    // To retrieve an image as ratio of showcase
    public ImageGroup getLogo(boolean isPortrait, int size) {
        String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
        ArrayList list = new ArrayList();
        String usage = "";
        switch (size) {
            case LOGO_ICON: usage = ImageRetriever.CHANNEL_ICON_LOCO; break;
            case LOGO_SM: usage = ImageRetriever.CHANNEL_LISTING_LOGO; break;
            case LOGO_LINK_SMALL: usage = ImageRetriever.LINK_SMALL; break;
            case LOGO_LINK_LARGE: usage = ImageRetriever.LINK_LARGE; break;
            case LOGO_MD: usage = ImageRetriever.CHANNEL_TOP_MENU_LOGO; break;
            case LOGO_LG: usage = ImageRetriever.CHANNEL_OVERLAY_LOGO; break;
        }

        if (Log.EXTRA_ON) {
            Log.printDebug("BrandingElement, imageGroup length=" + imageGroup.length);
            Log.printDebug("BrandingElement, usage=" + usage);
        }
        String ratio = isPortrait ? "2_3" : "4_3";

        for (int i = 0; i < imageGroup.length; i++) {
            Log.printDebug("BrandingElement, imageGroup[" + i +"]=" + imageGroup[i]);
            if (usage.equals(imageGroup[i].usage) == false || !ratio.equals(imageGroup[i].ratio)) {
                continue;
            }
            Log.printDebug("BrandingElement, add imageGroup[" + i +"]=" + imageGroup[i]);
            if (lang.equals(imageGroup[i].language)) {
                list.add(0, imageGroup[i]);
            } else {
                list.add(imageGroup[i]);
            }
        }
        return (ImageGroup) (list.size() == 0 ? null : list.get(0));
    }

    // R7.3
    synchronized public void reloadLogo(boolean isPortrait) {
        Log.printDebug("BrandingElement, reloadLogo(), isPortrait=");
        MediaTracker mt = new MediaTracker(CategoryUI.getInstance());
        URL url;

        ImageGroup ig = getLogo(isPortrait, LOGO_SM);

        if (ig == null) {
            ig = getLogo(LOGO_SM);
        }

        Rectangle bounds = null;
        if (isPortrait) {
            bounds = ImageRetriever.PR_SM;
        } else {
            bounds = ImageRetriever.LD_SM;
        }

        int imgIdx = 0;
        for (; imgIdx < ig.images.length; imgIdx++) {
            if (ig.images[imgIdx].width == bounds.width && ig.images[imgIdx].height == bounds.height) {
                break;
            }
        }

        if (imgIdx >= ig.images.length) {
            imgIdx = 0;
        }
        if (Log.EXTRA_ON) {
            Log.printDebug("reloadLogo, imgIdx=" + imgIdx);
        }
        try {
            url = new URL(DataCenter.getInstance().getString("IMAGE_URL") + ImageRetriever.BASE + ig.images[imgIdx].imageName + "." + ig.encoding);
            Log.printDebug("reloadLogo(" + imgIdx + "), " + toString(true) + ", url = " + url.toExternalForm() + ", isEDT = " + EventQueue.isDispatchThread());
            //				logos[idx] = Toolkit.getDefaultToolkit().createImage(url);
            logos[LOGO_SM] = Toolkit.getDefaultToolkit().createImage(URLRequestor.getBytes(url.toExternalForm(), null));

            mt.addImage(logos[LOGO_SM], 99);
        } catch (Exception e) {
        }

        long t = System.currentTimeMillis();
        try {
            mt.waitForID(99, 5000);
        } catch (InterruptedException e) {}
        t = System.currentTimeMillis() - t;
        Log.printDebug("reloadLogo(), " + toString(true) + ", elapsed = " + t);

        mt.removeImage(logos[LOGO_SM]);

        if (MenuController.getInstance().getCurrentScene() != null) {
            MenuController.getInstance().getCurrentScene().repaint();
        }
    }

    // R7.3
    // To retrieve an image as ratio of showcase
    synchronized public void loadLogoForShowcase(boolean isPortrait, boolean isBig) {
        Log.printDebug("BrandingElement, loadLogoForShowcase(), isPortrait=" + isPortrait + ", isBig=" + isBig);
        MediaTracker mt = new MediaTracker(CategoryUI.getInstance());
        URL url;

        ImageGroup ig = getLogo(isPortrait, LOGO_SM);

        if (ig == null) {
            if (Log.EXTRA_ON) {
                Log.printDebug("loadLogoForShowcase, 1st ImageGroup is null.");
            }
            ig = getLogo(LOGO_SM);
        }

        if (ig == null) {
            if (Log.EXTRA_ON) {
                Log.printDebug("loadLogoForShowcase, 2nd ImageGroup is null.");
            }
            return;
        }

        Rectangle bounds = null;

        if (isPortrait) {
            if (isBig) {
                bounds = ImageRetriever.PR_LG;
            } else {
                bounds = ImageRetriever.PR_SM;
            }
        } else {
            if (isBig) {
                bounds = ImageRetriever.LD_LG_MD;
            } else {
                bounds = ImageRetriever.LD_SM;
            }
        }

        int imgIdx = 0;
        for (; imgIdx < ig.images.length; imgIdx++) {
            if (ig.images[imgIdx].width == bounds.width && ig.images[imgIdx].height == bounds.height) {
                break;
            }
        }

        if (imgIdx >= ig.images.length) {
            imgIdx = 0;
        }
        if (Log.EXTRA_ON) {
            Log.printDebug("loadLogoForShowcase, imgIdx=" + imgIdx);
        }
        try {
            url = new URL(DataCenter.getInstance().getString("IMAGE_URL") + ImageRetriever.BASE + ig.images[imgIdx].imageName + "." + ig.encoding);
            Log.printDebug("loadLogoForShowcase(" + imgIdx + "), " + toString(true) + ", url = " + url.toExternalForm() + ", isEDT = " + EventQueue.isDispatchThread());
            //				logos[idx] = Toolkit.getDefaultToolkit().createImage(url);
            showcaseLogo = Toolkit.getDefaultToolkit().createImage(URLRequestor.getBytes(url.toExternalForm(), null));

            mt.addImage(showcaseLogo, 99);
        } catch (Exception e) {
        }

        long t = System.currentTimeMillis();
        try {
            mt.waitForID(99, 5000);
        } catch (InterruptedException e) {}
        t = System.currentTimeMillis() - t;
        Log.printDebug("loadLogoForShowcase(), " + toString(true) + ", elapsed = " + t);

        mt.removeImage(showcaseLogo);
        
        //R7.4 VDTRMASTER-6176 sykim
        try {
        	MenuController.getInstance().debugList[0].add(CatalogueDatabase.SDF.format(new Date()) + ", " + toString(true) + ", "
        			+ ig.images[imgIdx].imageName + ": " + Math.round(t/1000 * 10)/10 + " sec, +0 job(s)");
        } catch (Exception e) {}

        if (MenuController.getInstance().getCurrentScene() != null) {
            MenuController.getInstance().getCurrentScene().repaint();
        }
    }

    // R7.3
    public boolean hasPortraitRatio() {
        Log.printDebug("BrandingElement, hasPortraitRatio");

        if (imageGroup == null || imageGroup.length == 0) {
            return false;
        }

        for (int i = 0; i < imageGroup.length; i++) {
            if ("2_3".equals(imageGroup[i].ratio)) {
                return true;
            }
        }

        return false;
    }
	
	public void dispose() {
		for (int i = 0; i < imageGroup.length; i++) {
			if (imageGroup[i] != null) {
				imageGroup[i].dispose();
			}
			imageGroup[i] = null;
		}
//		imageGroup = null;
		for (int i = 0; i < logos.length; i++) {
			if (logos[i] != null) {
				logos[i].flush();
			}
			logos[i] = null;
		}
//		logos = null;
	}
}

package com.videotron.tvi.illico.vod.data.vcds;

import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.MenuTreeUI;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.vcds.parser.VCDSHandler;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CacheElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Catalog;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.Image;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Service;
import com.videotron.tvi.illico.vod.data.vcds.type.Showcase;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.Vcds;
import com.videotron.tvi.illico.vod.data.vcds.type.Versions;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.data.vcds.type.VideoDetails;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.ListViewUI;

public class CatalogueDatabase implements DataUpdateListener, Runnable, Definition {
	public static final String XML_VERSION = "9";
	public static final String FAIL = "fail_";
	public static final String SORT = "sort";
	public static final String SORT_ALPHA = "alphaAsc";
	public static final String SORT_DATE = "dateDesc";
	public static final String SORT_PROD = "prodYearDesc";
	public static final String SORT_DEFAULT = "default";
	
	public static final Exception MISSING_ELEMENTS = new Exception("missing elements");
	public static final SAXException STOP = new SAXException("stop by user");
	public static final Exception TIMEOUT = new Exception("timeout");
	
	public static final String EXCEPTION_ELEMENT = "unexpected elements";
	public static final SAXException UNEXPECTED_ELEMENTS = new SAXException(EXCEPTION_ELEMENT);
	
	public static final int IMAGE_RETRIEVER_SM = 0;
	public static final int IMAGE_RETRIEVER_MD = 1;
	public static final int IMAGE_RETRIEVER_LG = 2;
	public static final SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss.S");
	
	// R5
	public static final int NB_ELEMENTS = 10;

	private static int MAX_POSTER;
	private static int MAX_BIG_POSTER;
	private static final CatalogueDatabase instance = new CatalogueDatabase();

	synchronized public static CatalogueDatabase getInstance() {
		return instance;
	}

	private CatalogueDatabase() {
		methodToCode.put("retrieveNamedTopic", "01");
		methodToCode.put("retrieveVideos", "02");
		methodToCode.put("retrieveEpisodes", "03");
		methodToCode.put("retrieveImages", "04");
		methodToCode.put("retrieveSeries", "05");
		methodToCode.put("retrieveSeasons", "06");
		methodToCode.put("retrieveBundles", "07");
		methodToCode.put("retrieveBundlesForContent", "08");
		methodToCode.put("retrieveItems", "09");
		methodToCode.put("retrieveProviderTopics", "10");
		methodToCode.put("retrieveShowcases", "11");
		methodToCode.put("retrieveChannel", "12");
		methodToCode.put("retrieveCategory", "13");
		methodToCode.put("retrieveCategorizedBundles", "14");
		methodToCode.put("retrieveVideoForSearch", "15");
		methodToCode.put("retrieveServices", "16");
		methodToCode.put("retrieveCollectionExtras", "17");

//		mandatory.put("retrieveNamedTopic", new String[] {
//				EL_VCDS, EL_TOPIC, EL_CATEGORY, EL_CATEGORY_CONTAINER,
//		});
//		mandatory.put("retrieveVideos", new String[] {
//				EL_VCDS, EL_VIDEO,
//		});
//		mandatory.put("retrieveEpisodes", new String[] {
//				EL_VCDS, EL_EPISODE,
//		});
//		mandatory.put("retrieveSeries", new String[] {
//				EL_VCDS, EL_SERIES,
//		});
//		mandatory.put("retrieveSeasons", new String[] {
//				EL_VCDS, EL_SEASON,
//		});
//		mandatory.put("retrieveBundles", new String[] {
//				EL_VCDS, EL_BUNDLE,
//		});
//		mandatory.put("retrieveItems", new String[] {
//				EL_VCDS, EL_OFFER,
//		});
//		mandatory.put("retrieveShowcases", new String[] {
//				EL_VCDS, EL_SHOWCASE,
//		});
//		mandatory.put("retrieveChannel", new String[] {
//				EL_VCDS, EL_CHANNEL,
//		});
//		mandatory.put("retrieveServices", new String[] {
//				EL_VCDS, EL_SERVICE,
//		});
//		mandatory.put("retrieveCategory", new String[] {
//				EL_VCDS, EL_CATEGORY,
//		});
//		mandatory.put("retrieveCategorizedBundles", new String[] {
//				EL_VCDS, EL_CATEGORIZED_BUNDLE,
//		});

		MAX_POSTER = DataCenter.getInstance().getInt("MAX_POSTER", 50);
		MAX_BIG_POSTER = DataCenter.getInstance().getInt("MAX_BIG_POSTER", 12);
		posterPool = new PosterPool(MAX_POSTER);
		bigPosterPool = new Object[2][MAX_BIG_POSTER];
	}

	private HashMap elementPool = new HashMap(501);
	private HashMap sspIdToElement = new HashMap(91);
	private PosterPool posterPool;// = new PosterPool(MAX_POSTER);
	private Object[][] bigPosterPool;// = new Object[2][MAX_BIG_POSTER];
	private long memoryBigPoster = 0;
	private long cntBigPoster = 0;
	private Vector toParsing = new Vector();
	private Thread survivalThread;
	private Catalog catalog;
	private Vcds root;
	private Versions catalogueVersion;
	private String topicIdForKaraoke;
	private HashSet aliveRetriever = new HashSet(9);
	private com.videotron.tvi.illico.vod.data.vcds.type.Error lastError;
	
	// R5
	private Topic channelTopic;
	// R5 - TANK
	private String lastTopicId;

	private HashMap methodToCode = new HashMap();
//	private HashMap mandatory = new HashMap(); // no-use

	// IMPROVEMENT
	ByteArrayOutputStream rootBaos = new ByteArrayOutputStream();
	int lastCatalogueVersion = 0;
	
	public void monitoringSurvivalKit(boolean start) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CatalogueDatabase monitoringSurvivalKit, start : "
							+ start);
		}
		if (start) {
			if (toParsing == null) {
				toParsing = new Vector();
			}
			survivalThread = new Thread(this, "survival");
			survivalThread.start();
			DataCenter.getInstance().addDataUpdateListener("SURVIVALKIT", this);
			DataCenter.getInstance().addDataUpdateListener("SURVIVALPOSTER",
					this);

			// data is already cached
			if (DataCenter.getInstance().get("SURVIVALKIT") != null) {
				dataUpdated("SURVIVALKIT", null,
						DataCenter.getInstance().get("SURVIVALKIT"));
			}
			if (DataCenter.getInstance().get("SURVIVALPOSTER") != null) {
				dataUpdated("SURVIVALPOSTER", null, DataCenter.getInstance()
						.get("SURVIVALPOSTER"));
			}
		} else {
			DataCenter.getInstance().removeDataUpdateListener("SURVIVALKIT",
					this);
			DataCenter.getInstance().removeDataUpdateListener("SURVIVALPOSTER",
					this);
			if (survivalThread != null) {
				survivalThread.interrupt();
				toParsing.clear();
				toParsing = null;
			}
		}
	}

	public void run() {
		if (Log.DEBUG_ON) {
			Log.printDebug("survivalkit, thread begin");
		}
		try {
			// test
			// Thread.sleep(1000 * 100);
			// File s = new File("resource/retrieveSurvivalKit.xml");
			// toParsing.add(s);
			// end of test
			while (toParsing != null) {
				synchronized (toParsing) {
					if (toParsing.isEmpty()) {
						if (Log.DEBUG_ON) {
							Log.printDebug("survivalkit, wait");
						}
						toParsing.wait();
					}
				}
				File f = (File) toParsing.remove(0);
				if (Log.DEBUG_ON) {
					Log.printDebug("survivalkit, f : " + f);
				}
				if (f.getName().toLowerCase().endsWith("zip")) {
					Hashtable unzip = ZipFileReader.read(f);
					if (Log.DEBUG_ON) {
						Log.printDebug("survivalkit, unzip : "
								+ unzip);
					}
				} else if (f.getName().toLowerCase().endsWith("xml")) {
					try {
						InputStream inStream = new FileInputStream(f);
						InputSource is = new InputSource(new InputStreamReader(
								inStream, "UTF-8"));
						if (f.getName().toLowerCase().indexOf("survivalkit") != -1) {
							VCDSHandler handler = new VCDSHandler();
							SAXParserFactory.newInstance().newSAXParser()
									.parse(is, handler);

							if (handler.getCatalog() != null) {
								catalog = handler.getCatalog();
								if (Log.DEBUG_ON) {
									Log.printDebug("survivalkit, catalog setted "
													+ catalog);
								}
							} else if (handler.getRootVcds() != null) {
								root = handler.getRootVcds();
								if (Log.DEBUG_ON) {
									Log.printDebug("survivalkit, root setted "
													+ root);
								}
							}
						} else {
							CoherenceHandler handler = new CoherenceHandler();
							SAXParserFactory.newInstance().newSAXParser()
									.parse(is, handler);
						}

					} catch (Exception e) {
						Log.print(e);
					}
				}
			}
		} catch (InterruptedException ie) {
		}

	}

	public void dataUpdated(String key, Object old, Object value) {
		if (Log.INFO_ON) {
			Log.printInfo("CatalogueDatabase dataUpdated, key : "
					+ key);
		}
		if ("SURVIVALKIT".equals(key) || "SURVIVALPOSTER".equals(key)) {
			synchronized (toParsing) {
				toParsing.add(value);
				toParsing.notifyAll();
			}
		}
	}

	public void dataRemoved(String key) {

	}

	public void setVersion(Versions ver) {
		catalogueVersion = ver;
		
		// IMPROVEMENT
		lastCatalogueVersion = catalogueVersion.catalogue;
	}

	public Versions getVersion() {
		return catalogueVersion;
	}
	
	// IMPROVEMENT
	public int getLastCatalogueVersion() {
		return lastCatalogueVersion;
	}
	
	// IMPROVEMENT
	public void resetLastCatalogueVersion() {
		lastCatalogueVersion = 0;
	}
	
	public void register(Object key, CacheElement instance) {
		Object old = elementPool.put(key, instance);
	}

	public void registerBySspId(Object key, BaseElement instance) {
		sspIdToElement.put(key, instance);
		if (instance instanceof Orderable) { // put additional key - jira 4760
			Orderable orderable = (Orderable) instance;
			for (int i = 0; i < orderable.detail.length; i++) {
				VideoDetails detail = orderable.detail[i];
				for (int j = 0; j < detail.videoPlayout.length; j++) {
					String name = detail.videoPlayout[j].name;
					if (sspIdToElement.containsKey(name) == false) {
						sspIdToElement.put(name, instance);
					}
				}
			}
		}
	}
	
	public String cacheInfo() {
		return elementPool.size() + " elements";
	}
	
	public String posterInfo() {
		return posterPool.size() + "/" + DataCenter.getInstance().getInt("MAX_POSTER")
			+ ", " + (posterPool.getMemory()/1024) + " KB, "
			+ cntBigPoster + "/" + DataCenter.getInstance().getInt("MAX_BIG_POSTER") 
			+ ", " + (memoryBigPoster/1024) + " KB";
	}

	public CacheElement getCached(Object key) {
		if (key == null) {
			return null;
		}
		return (CacheElement) elementPool.get(key);
	}

	public BaseElement getCachedBySspId(Object key) {
		if (key == null) {
			return null;
		}
		return (BaseElement) sspIdToElement.get(key);
	}

	public java.awt.Image getImage(Image img, int w, int h) {
		Rectangle size = new Rectangle(0, 0, w, h);
		if (img != null) {
			String key = img.getCacheKey();

			if (size.equals(ImageRetriever.PR_LG)
					|| size.equals(ImageRetriever.LD_LG)
					|| size.equals(ImageRetriever.PR_2ND)
					|| size.equals(ImageRetriever.LD_LG_MD)) {
				synchronized (bigPosterPool) {
					for (int i = 0; i < MAX_BIG_POSTER; i++) {
						if (key.equals(bigPosterPool[0][i])) {
							Object found = bigPosterPool[1][i];
							Object[][] tmp = new Object[2][MAX_BIG_POSTER];
							int idx = 0;
							tmp[0][idx] = key;
							tmp[1][idx++] = found; // place first
							for (int j = 0; j < i; j++) {
								tmp[0][idx] = bigPosterPool[0][j];
								tmp[1][idx++] = bigPosterPool[1][j];
							}
							for (int j = i + 1; j < MAX_BIG_POSTER; j++) {
								tmp[0][idx] = bigPosterPool[0][j];
								tmp[1][idx++] = bigPosterPool[1][j];
							}
							for (int j = 0; j < MAX_BIG_POSTER; j++) {
								bigPosterPool[0][j] = null;
								bigPosterPool[1][j] = null;
							}
							bigPosterPool = tmp;
							return (java.awt.Image) found;
						}
					}
				}
			} else {
				if (posterPool.containsKey(key)) {
					return (java.awt.Image) posterPool.get(key);
				}
			}
		}
		return null;
	}

	public boolean containsImage(String key) {
		return posterPool.get(key) != null; // for fresh cache
	}

	public void addImage(String key, java.awt.Image image) {
		if (Resources.appStatus != VODService.APP_STARTED) {
			return;
		}
		//R7.4 catch Exception because default iamge is not displayed when image does not exist.
		try {
			posterPool.put(key, image);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void organizePosterPool() {
		posterPool.cut();
	}

	public boolean containsLargeImage(String key) {
		synchronized (bigPosterPool) {
			for (int i = 0; i < MAX_BIG_POSTER; i++) {
				if (key.equals(bigPosterPool[0][i]))
					return true;
			}
		}
		return false;
	}

	public void addLargeImage(String key, java.awt.Image image) {
		if (Resources.appStatus != VODService.APP_STARTED) {
			return;
		}
		//R7.4 catch Exception because default iamge is not displayed when image does not exist.
		try {
			synchronized (bigPosterPool) {
				if (bigPosterPool[1][MAX_BIG_POSTER - 1] != null) {
					java.awt.Image img = (java.awt.Image) bigPosterPool[1][MAX_BIG_POSTER - 1];
					memoryBigPoster -= img.getWidth(null) * img.getHeight(null) * 4;
					img.flush();
					bigPosterPool[0][MAX_BIG_POSTER - 1] = null;
					bigPosterPool[1][MAX_BIG_POSTER - 1] = null;
				}
				System.arraycopy(bigPosterPool[0], 0, bigPosterPool[0], 1,
						MAX_BIG_POSTER - 1);
				System.arraycopy(bigPosterPool[1], 0, bigPosterPool[1], 1,
						MAX_BIG_POSTER - 1);
				bigPosterPool[0][0] = key;
				bigPosterPool[1][0] = image;
				memoryBigPoster += image.getWidth(null) * image.getHeight(null) * 4;
				
				cntBigPoster = 0;
				for (int i = 0; i < MAX_BIG_POSTER; i++) {
					if (bigPosterPool[0][i] != null) {
						cntBigPoster++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ImageRetriever[] imageRetriever = new ImageRetriever[3];
	int[] lastRetiever = new int[3];

	public void clearDuplicate(int retriever) {
		lastRetiever[retriever] = -1;
	}
	public void retrieveImages(BaseUI listener, BaseElement[] elements) {
		retrieveImages(listener, elements, ImageRetriever.TYPE_MEDIUM | ImageRetriever.TYPE_SMALL, IMAGE_RETRIEVER_SM);
	}
	public void retrieveImages(BaseUI listener, BaseElement[] elements, int type, int retriever) {
		boolean dup = elements.hashCode() == lastRetiever[retriever];
		// prevent to duplicated request
		if (dup) {
			return;
		}
		Log.printDebug("CatalogueDatabase, retrieveImages()"
				+ ", listener = " + listener
				+ ", #element = " + elements.length);
		lastRetiever[retriever] = elements.hashCode();

		if (imageRetriever[retriever] != null) {
			imageRetriever[retriever].interrupt();
		}
		ArrayList list = new ArrayList();
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] instanceof MoreDetail) {
				Object img = null;
				if (elements[i] instanceof Showcase) { // for karaoke
					img = ((Showcase) elements[i]).getBanner(ImageRetriever.LD_KA.width, ImageRetriever.LD_KA.height);
				}
				if (elements[i] instanceof CategorizedBundle) { // for partypak
					img = ((CategorizedBundle) elements[i]).chooseImage(ImageRetriever.AFFICHE);
				}
				if (elements[i] instanceof Extra) {
					try {
						img = ((MoreDetail) CatalogueDatabase.getInstance().getCached(((Extra) elements[i]).extraDetail.mainContentItem)).chooseImage();
					} catch (NullPointerException npe) {
						// NPE may occur in <sample>
					}
				}
				if (img == null && elements[i] instanceof MoreDetail) {
					img = ((MoreDetail) elements[i]).chooseImage();
				}
				MoreDetail md = null;
				if (img == null 
						&& elements[i] instanceof Showcase 
						&& (md = ((Showcase) elements[i]).getContent()) != null) {
					img = md.chooseImage();
				}
				// partypak in showcase
				if (img == null && md != null && md instanceof CategoryContainer && ((CategoryContainer) md).categorizedBundle != null) {
					img = ((CategoryContainer) md).categorizedBundle.chooseImage(ImageRetriever.AFFICHE);
				}
				if (img == null) {
					Log.printDebug("element["+i+"/"+elements.length+"] NO IMG = " + elements[i].toString(true));
					continue;
				}
				if (list.contains(img) == false) {
					Log.printDebug("element["+i+"/"+elements.length+"] ADD LIST = " + elements[i].toString(true));
					list.add(img);
				} else {
					Log.printDebug("element["+i+"/"+elements.length+"] DUP = " + elements[i].toString(true));
				}
			}
		}
		imageRetriever[retriever] = new ImageRetriever("ImageRetriever, len = "
				+ elements.length + ", list.size = " + list.size());// + ", " + list);
		
		for (int i = 0; i < list.size(); i++) {
			Log.printDebug("CatalogueDatabase, retrieveImages(), list[" + i + "]=" + list.get(i));
		}
		
		imageRetriever[retriever].setData(listener, list, type);
		imageRetriever[retriever].start();
	}

	public void retrieveLargeImage(BaseUI listener, BaseElement element, boolean showcase) {
		Log.printDebug("CatalogueDatabase, retrieveLargeImage()"
				+ ", listener = " + listener + ", element = " + element + ", showcase = " + showcase);
		if (element == null) {
			return;
		}
		Object img = null;
		if (element instanceof Showcase) { // for karaoke
			img = ((Showcase) element).getBanner(ImageRetriever.LD_KA.width, ImageRetriever.LD_KA.height);
		}
		if (img == null) {
			img = ((MoreDetail) element).chooseImage();
		}
		if (img == null 
				&& element instanceof Showcase 
				&& ((Showcase) element).getContent() != null) {
			img = ((Showcase) element).getContent().chooseImage();
		}
		if (img == null) {
			return;
		}
		int hash = element.hashCode();
		if (lastRetiever[IMAGE_RETRIEVER_LG] == hash) {
			return;
		}
		lastRetiever[IMAGE_RETRIEVER_LG] = hash;

		if (imageRetriever[IMAGE_RETRIEVER_LG] != null) {
			imageRetriever[IMAGE_RETRIEVER_LG].interrupt();
		}
		imageRetriever[IMAGE_RETRIEVER_LG] = new ImageRetriever("ImageRetriever, Large, "
				+ element.toString(false) + ", img = " + img);
		ArrayList images = new ArrayList();
		images.add(img);
		int type = ImageRetriever.TYPE_LARGE;
		if (showcase) {
			type |= ImageRetriever.TYPE_MEDIUM;
		}
		imageRetriever[IMAGE_RETRIEVER_LG].setData(listener, images, type);
		imageRetriever[IMAGE_RETRIEVER_LG].start();
	}

	public void retrieveLargeImage(BaseUI listener, BaseElement[] elements, boolean showcase) {
		boolean dup = elements.hashCode() == lastRetiever[IMAGE_RETRIEVER_LG];
		// prevent to duplicated request
		if (dup) {
			return;
		}
		Log.printDebug("CatalogueDatabase, retrieveLargeImage()"
				+ ", listener = " + listener + ", element.len = " + elements.length);
		if (imageRetriever[IMAGE_RETRIEVER_LG] != null) {
			imageRetriever[IMAGE_RETRIEVER_LG].interrupt();
		}
		imageRetriever[IMAGE_RETRIEVER_LG] = new ImageRetriever("ImageRetriever, Large " + elements.length);
		ArrayList images = new ArrayList();
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == null) continue;
			Object img = null;
			if (elements[i] instanceof Extra) {
				img = ((MoreDetail) CatalogueDatabase.getInstance().getCached(((Extra) elements[i]).extraDetail.mainContentItem)).chooseImage();
			} else {
				img = ((MoreDetail) elements[i]).chooseImage();
			}
			if (img != null) {
				images.add(img);
			}
		}
		int type = ImageRetriever.TYPE_LARGE;
		if (showcase) {
			type |= ImageRetriever.TYPE_MEDIUM;
		}
		imageRetriever[IMAGE_RETRIEVER_LG].setData(listener, images, type);
		imageRetriever[IMAGE_RETRIEVER_LG].start();
	}

	public void cleanUp() {
		long time = System.currentTimeMillis();
		Log.printInfo("CatalogueDatabase, cleanUp(), alive = " + aliveRetriever.size());

		// make snapshot to prevent ConcurrentModificationException
		for (Iterator i = new ArrayList(aliveRetriever).iterator(); i.hasNext(); ) {
			Retriever r = (Retriever) i.next();
			r.stopParsing();
		}
		
		int cnt = 0;
		while (cnt < 20 && hasAliveRetriever()) {
			try {
				cnt++;
				Thread.sleep(50);
			} catch (InterruptedException e) {}
		}
		Log.printDebug("CatalogueDatabase, cleanUp(), elapsed by step 1 = " + (System.currentTimeMillis() - time) + ", wait " + cnt);
		
		try {
			if (catalog != null) {
				catalog.dispose();
			}
		} catch (Exception e) {
			Log.print(e);
		} finally {
			catalog = null;
		}
		try {
			if (root != null) {
				root.dispose();
			}
		} catch (Exception e) {
			Log.print(e);
		} finally {
			root = null;
		}

		Iterator iter = elementPool.values().iterator();
		while (iter.hasNext()) {
			((BaseElement) iter.next()).dispose();
		}
		elementPool.clear();
		iter = sspIdToElement.values().iterator();
		while (iter.hasNext()) {
			((BaseElement) iter.next()).dispose();
		}
		sspIdToElement.clear();
		Log.printDebug("CatalogueDatabase, cleanUp(), elapsed by step 2 = " + (System.currentTimeMillis() - time));
		//////////////////////////////////////////////////////////

		posterPool.clear();
		Log.printDebug("CatalogueDatabase, cleanUp(), mem(big) = " + memoryBigPoster);
		cnt = 0;
		for (int i = 0; i < MAX_BIG_POSTER; i++) {
			bigPosterPool[0][i] = null;
			if (bigPosterPool[1][i] != null) {
				java.awt.Image img = (java.awt.Image) bigPosterPool[1][i];
				memoryBigPoster -= img.getWidth(null) * img.getHeight(null) * 4;
				img.flush();
				cnt++;
			}
			bigPosterPool[1][i] = null;
		}
		cntBigPoster = 0;
		
		Log.printDebug("CatalogueDatabase, cleanUp(), flush " + cnt
				+ " big image(s), mem = " + memoryBigPoster + ", elapsed total = " + (System.currentTimeMillis() - time));

	}

	public void setLastError(com.videotron.tvi.illico.vod.data.vcds.type.Error err, boolean noShow) {
		Log.printDebug("setLastError, error = " + (err == null ? "NULL" : err.toString(true)) + ", noShow = " + noShow);
		if (err != null) {
			MenuController.getInstance().debugList[1].add(err.toString(true));
			String errorBase = "";
			switch (Integer.parseInt(err.errorCode.substring(4, 5))) {
			case 0:
				errorBase = "VOD105-";
				break;
			case 1:
				errorBase = "VOD106-";
				break;
			case 2:
				errorBase = "VOD107-";
				break;
			}
			boolean ori = false;
			if (noShow) {
				ori = CommunicationManager.getInstance().turnOnErrorMessage(false);
			}
			CommunicationManager.getInstance().errorMessage(errorBase + err.errorCode.substring(4), null);
			if (noShow) {
				CommunicationManager.getInstance().turnOnErrorMessage(ori);
			}
		}
		lastError = err;
	}

	public com.videotron.tvi.illico.vod.data.vcds.type.Error getLastError() {
		return lastError;
	}

	public Catalog getCatalog() {
		return catalog;
	}

	public Vcds getRoot() {
		return root;
	}

	public Topic getKaraokeTopic() {
		return (Topic) getCached(topicIdForKaraoke);
	}

	public void setKaraokeTopicId(String id) {
		topicIdForKaraoke = id;
	}
	
	// R5
	public Topic getChannelTopic() {
		return channelTopic;
	}

	// R5
	public void setChannelTopic(Topic topic) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CatalogueDatabase, setChannelTopic, topic=" + topic);
		}
		this.channelTopic = topic;
	}
	
	// R5 - TANK
	public Topic getLastTopic() {
		return (Topic) getCached(lastTopicId);
	}
	
	// R5 - TANK
	public void setLastTopicId(String id) {
		lastTopicId = id;
	}

	private String lastSortType = "default";
	public String getLastSortType() {
		return lastSortType;
	}
	public int getLastSortTypeIdx() {
		if (SORT_ALPHA.equals(lastSortType)) {
			return 1;
		}
		if (SORT_DATE.equals(lastSortType)) {
			return 2;
		}
		if (SORT_PROD.equals(lastSortType)) {
			return 3;
		}
		return 4;
	}
	public String getSortType(String keyName) {
		if (Resources.OPT_NAME.equals(keyName)) {
			return SORT_ALPHA;
		} 
		if (Resources.OPT_DATE.equals(keyName)) {
			return SORT_DATE;
		} 
		if (Resources.OPT_PROD_YEAR.equals(keyName)) {
			return SORT_PROD;
		}
		return SORT_DEFAULT;
	}
	
	private int lastSortedData;
	private Comparator lastComparator;

	public boolean isSortedWith(BaseElement[] catalogueList, String optName) {
		if (catalogueList == null || lastSortedData != catalogueList.hashCode())
			return false;
		if (Resources.OPT_NAME.equals(optName)) {
			return lastComparator == nameComparator;
		}
		return lastComparator == dateComparatorDesc
				|| lastComparator == wishlistComparator
				|| lastComparator == leaseComparator;
	}
	
	public BaseElement[] sort(BaseElement[] catalogueList, String optName) {
		return sort(catalogueList, optName, false);
	}

	public BaseElement[] sort(BaseElement[] catalogueList, String optName, boolean force) {
		Log.printDebug("CatalogueDatabase, sort, name = " + optName + ", f = " + force);
		if (force == false && DataCenter.getInstance().getInt("PAGE_SIZE") > 0) {
			return catalogueList;
		}
		ArrayList list = new ArrayList(catalogueList.length);
		for (int i = 0; i < catalogueList.length; i++) {
			list.add(catalogueList[i]);
		}
		if (Resources.OPT_NAME.equals(optName)) {
			lastComparator = nameComparator;
		} else {
			UIComponent cur = MenuController.getInstance().getCurrentScene();
			if (cur instanceof ListViewUI) {
				if (((ListViewUI) cur).isWishList()) {
					lastComparator = wishlistComparator;
				} else if (((ListViewUI) cur).isResumeViewing()) {
					lastComparator = leaseComparator;
				} else {
					lastComparator = dateComparatorDesc;
				}
			} else {
				lastComparator = dateComparatorDesc;
			}
		}
		Collections.sort(list, lastComparator);
		list.toArray(catalogueList);
		lastSortedData = catalogueList.hashCode();
		return catalogueList;
	}
	
	public Comparator frComparator = Collator.getInstance(Locale.CANADA_FRENCH); 
	private Comparator nameComparator = new Comparator() {
		public int compare(Object arg0, Object arg1) {
			if (arg0 instanceof Bundle && arg1 instanceof MoreDetail) {
				if (((Bundle) arg0).hasContent((BaseElement) arg1)) {
					return -1;
				}
			} else if (arg0 instanceof MoreDetail && arg1 instanceof Bundle) {
				if (((Bundle) arg1).hasContent((BaseElement) arg0)) {
					return 1;
				}
			}
			return frComparator.compare(((MoreDetail) arg0).getTitle(), ((MoreDetail) arg1).getTitle());
		}
	};
//	private Comparator nameComparatorDesc = new Comparator() {
//		public int compare(Object arg0, Object arg1) {
//			return ((MoreDetail) arg1).getTitle().compareTo(
//					((MoreDetail) arg0).getTitle());
//		}
//	};
//	private Comparator dateComparator = new Comparator() {
//		public int compare(Object arg0, Object arg1) {
//			return ((MoreDetail) arg0).getStart().compareTo(
//					((MoreDetail) arg1).getStart());
//		}
//	};
	private Comparator dateComparatorDesc = new Comparator() {
		public int compare(Object arg0, Object arg1) {
			int ret = ((MoreDetail) arg1).getStart().compareTo(
					((MoreDetail) arg0).getStart());
			if (ret == 0) {
				return ((MoreDetail) arg0).getTitle().compareTo(
						((MoreDetail) arg1).getTitle());
			}
			return ret;
		}
	};
//	private Comparator prodYearComparatorDesc = new Comparator() {
//		public int compare(Object arg0, Object arg1) {
//			int ret = ((MoreDetail) arg1).getProductionYear().compareTo(
//					((MoreDetail) arg0).getProductionYear());
//			if (ret == 0) {
//				return ((MoreDetail) arg0).getTitle().compareTo(
//						((MoreDetail) arg1).getTitle());
//			}
//			return ret;
//		}
//	};
	private Comparator wishlistComparator = new Comparator() {
		public int compare(Object arg0, Object arg1) {
			String bid0 = BookmarkManager.getInstance().isInWishlist(
					(MoreDetail) arg0);
			String bid1 = BookmarkManager.getInstance().isInWishlist(
					(MoreDetail) arg1);
			try {
				int bid00 = Integer.parseInt(bid0);
				int bid11 = Integer.parseInt(bid1);
				return bid11 - bid00;
			} catch (Exception e) {}
			return 0;
//			return bid1 == null ? 0 : bid1.compareTo(bid0);
		}
	};
	private Comparator leaseComparator = new Comparator() {
		public int compare(Object arg0, Object arg1) {
			if (arg0 instanceof Bundle && arg1 instanceof MoreDetail) {
				if (((Bundle) arg0).hasContent((BaseElement) arg1)) {
					return -1;
				}
			} else if (arg0 instanceof MoreDetail && arg1 instanceof Bundle) {
				if (((Bundle) arg1).hasContent((BaseElement) arg0)) {
					return 1;
				}
			}
			Bookmark bm0 = null, bm1 = null;
			long end0 = 0, end1 = 0;
			try {
				if (arg0 instanceof Video) {
					bm0 = BookmarkManager.getInstance().getPurchased((Video) arg0);
					end0 = bm0.getLeaseEnd();
				} else if (arg0 instanceof Bundle) {
					bm0 = BookmarkManager.getInstance().getPurchased((Bundle) arg0);
					end0 = bm0.getLeaseEnd();
				} else if (arg0 instanceof CategorizedBundle) {
					bm0 = BookmarkManager.getInstance().getPurchased((CategorizedBundle) arg0);
					end0 = bm0.getLeaseEnd();
				}
			} catch (NullPointerException npe) {}
			try {
				if (arg1 instanceof Video) {
					bm1 = BookmarkManager.getInstance().getPurchased((Video) arg1);
					end1 = bm1.getLeaseEnd();
				} else if (arg1 instanceof Bundle) {
					bm1 = BookmarkManager.getInstance().getPurchased((Bundle) arg1);
					end1 = bm1.getLeaseEnd();
				} else if (arg1 instanceof CategorizedBundle) {
					bm1 = BookmarkManager.getInstance().getPurchased((CategorizedBundle) arg1);
					end1 = bm1.getLeaseEnd();
				}
			} catch (NullPointerException npe) {}
			if (end0 == end1 && bm0 != null && bm1 != null) {
				// VDTRMASTER-5440
				return frComparator.compare(((MoreDetail) arg0).getTitle(), ((MoreDetail) arg1).getTitle());
			}
			return (int) (end0 - end1);
		}
	};

	public boolean hasAliveRetriever() {
		return aliveRetriever.size() > 0;
	}
	public String elapsed = "";

	private class Retriever extends Thread implements TVTimerWentOffListener {
		public BaseUI listener;
		public String method;
		public String param = "";
		public Object cmd;
		public boolean loading;
		public boolean lowPriority;
		public boolean noShowError;

		private VCDSHandler handler;
		private URL url;
		private URLConnection con;
		private Retriever retriever;
		private InputStream inputStream;
		private boolean timeout = false;
		private boolean stopParsing = false;
		private String ext = null;
		private TVTimerSpec timer = null;

		public Retriever(String name) {
			super(name);
			retriever = this;
		}
		
		public void setParamWithAll(String param) { 
			this.param = param + "&platformId=i2" 
				+ "&language=" + Constants.LANGUAGES[1-Resources.currentLanguage]
				+ "&xmlVersion=" + XML_VERSION
				+ CommunicationManager.getInstance().getAllPackages();
		}
		
		public void setParam(String param) {
			this.param = param + "&platformId=i2" 
				+ "&language=" + Constants.LANGUAGES[1-Resources.currentLanguage]
				+ "&xmlVersion=" + XML_VERSION
				+ (("retrieveServices".equals(method)) ? "" : CommunicationManager.getInstance().getAuthorizedPackage());
		}

		public void stopParsing() {
			stopParsing = true;
			if (handler != null) {
				handler.stopParsing();
			}
		}

		public void run() {
			if (loading) {
				MenuController.getInstance().showLoadingAnimation();
			}
			aliveRetriever.add(retriever);
			setLastError(null, false);
			String errorCode = "";
			CommunicationManager.getInstance().resetLastErrorCode();
			int chance = DataCenter.getInstance().getInt("MESSAGE_RETRY_COUNT", 1);
			
			loop: while (chance > 0) {
				long t = System.currentTimeMillis();
				try {
					// IMPROVEMENT
//					final String DVB_PERSISTENT_ROOT = "dvb.persistent.root";
//					final String REPOSITORY_ROOT = System.getProperty(DVB_PERSISTENT_ROOT);
//					File rootDataFile = new File(REPOSITORY_ROOT + "/vod/VOD_ROOT_MENU_DATA.txt");
					
					// IMPROVEMENT
					Log.printDebug("root size = " + rootBaos.size());
					if (rootBaos.size() > 0 && param.startsWith("?depth=1&allDetails=0&includeSamples=1")) {
//						String currentVersionURL = DataCenter.getInstance().getString("VCDS_URL") + "retrieveCurrentBuild";
//						Log.printDebug("currentVersionURL=" + currentVersionURL);
//						
//						byte[] src = URLRequestor.getBytes(currentVersionURL, null);
//						
//						String buildStr = new String(src);
//						
//						Log.printDebug("buildStr=" + buildStr);
//						
//						String catalogueTagStr = "catalogue=\"";
//						int startIdx = buildStr.indexOf(catalogueTagStr);
//						int endIdx = buildStr.indexOf("\"", startIdx + catalogueTagStr.length());
//						String versionStr = buildStr.substring(startIdx + catalogueTagStr.length(), endIdx).trim();
//						int currentVersion = Integer.parseInt(versionStr);
//						
//						Log.printDebug("currentversion=" + currentVersion);
						
						url = new URL(DataCenter.getInstance().getString("VCDS_URL") + method + param);
						Log.printDebug("VCDS/" + url);
						ext = DataCenter.getInstance().getString("VCDS_URL") + method + param;
						if (BaseRenderer.fmClock.stringWidth(param) > 900) {
							int len = param.length() / 2;
							MenuController.getInstance().debugList[1].add(param.substring(len));
							MenuController.getInstance().debugList[1].add(param.substring(0, len));
						} else {
							MenuController.getInstance().debugList[1].add(param);
						}
						MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", " + ext.substring(0, ext.indexOf('?')));
						ext = ext.substring(ext.indexOf("retrieve") + 8, ext.indexOf('?'));
						
						handler = new VCDSHandler();
						if (param.indexOf("orderableItems") != -1 || param.indexOf("topicType=Karaoke") != -1) {
							handler.setRegisterBySspID();
						}
						if (method.equals("retrieveCatalogueStructure")) {
							handler.setNoReset();
						}
						if (lowPriority) {
							handler.setLowPriority();
						}
						if (noShowError) {
							handler.setNoShowError();
						}
						
//						Log.printDebug("lastCatalogueVersion=" + lastCatalogueVersion + ", currentVersion=" + currentVersion);
						
						inputStream = new BufferedInputStream(new ByteArrayInputStream(rootBaos.toByteArray()), 65536);
						
//						if (lastCatalogueVersion == currentVersion) {
//							inputStream = new BufferedInputStream(new ByteArrayInputStream(rootBaos.toByteArray()), 65536);
//						} else {
//							timeout = false;
//							timer = new TVTimerSpec();
//							timer.addTVTimerWentOffListener(this);
//							timer.setDelayTime(DataCenter.getInstance().getInt("MSG_TIME_OUT", 10000));
//							timer = TVTimer.getTimer().scheduleTimerSpec(timer);
//							
//							con = url.openConnection();
//							Log.printDebug("VCDS/ after openConnection()");
//							if (timeout) {
//								throw TIMEOUT;//("Timeout while openConnection()");
//							}
//							inputStream = new BufferedInputStream(con.getInputStream());
//						}
					} else {
					
						url = new URL(DataCenter.getInstance().getString("VCDS_URL") + method + param);
						Log.printDebug("VCDS/" + url);
						ext = url.toExternalForm();
						if (BaseRenderer.fmClock.stringWidth(param) > 900) {
							int len = param.length() / 2;
							MenuController.getInstance().debugList[1].add(param.substring(len));
							MenuController.getInstance().debugList[1].add(param.substring(0, len));
						} else {
							MenuController.getInstance().debugList[1].add(param);
						}
						MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", " + ext.substring(0, ext.indexOf('?')));
						ext = ext.substring(ext.indexOf("retrieve") + 8, ext.indexOf('?'));
	
						timeout = false;
						timer = new TVTimerSpec();
						timer.addTVTimerWentOffListener(this);
						timer.setDelayTime(DataCenter.getInstance().getInt("MSG_TIME_OUT", 10000));
						timer = TVTimer.getTimer().scheduleTimerSpec(timer);
	
						con = url.openConnection();
						Log.printDebug("VCDS/ after openConnection()");
						if (timeout) {
							throw TIMEOUT;//("Timeout while openConnection()");
						}
	
						handler = new VCDSHandler();
						if (param.indexOf("orderableItems") != -1 || param.indexOf("topicType=Karaoke") != -1) {
							handler.setRegisterBySspID();
						}
						if (method.equals("retrieveCatalogueStructure")) {
							handler.setNoReset();
						}
						if (lowPriority) {
							handler.setLowPriority();
						}
						if (noShowError) {
							handler.setNoShowError();
						}
						
						// IMPROVEMENT
						if (method.equals("retrieveNamedTopic")
								&& param.indexOf("topicType") == -1
								&& handler.getRootVcds() != null) {
							
							rootBaos.flush();
							rootBaos.reset();
							BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
							
							byte[] buf = new byte[4096];
							int readSize = 0;
							while ((readSize = bis.read(buf)) > -1) {
								rootBaos.write(buf, 0, readSize);
							}
							inputStream = new BufferedInputStream(new ByteArrayInputStream(rootBaos.toByteArray()), 65536);
						} else {
							inputStream = new BufferedInputStream(con.getInputStream(), 65536);
						}
					}
					
					
					long t2 = System.currentTimeMillis();
					String elapsed = ", C = " + (t2 - t);
					if (timeout) {
						throw TIMEOUT;//("Timeout while getInputStream()");
					}

					InputSource is = new InputSource(new InputStreamReader(
							inputStream, "UTF-8"));
					SAXParserFactory.newInstance().newSAXParser().parse(is, handler);
					long t3 = System.currentTimeMillis();
					elapsed += ", P = " + (t3 - t2) + ", T = " + (t3 - t);
					if (timeout) {
						throw TIMEOUT;//("Timeout while parsing");
					}

//					String[] elementsToCheck = (String[]) mandatory.get(method);
//					if (elementsToCheck != null) {
//						boolean ret = handler.checkMandatories(elementsToCheck);
//						if (ret == false) {
//							throw MISSING_ELEMENTS;
//						}
//					}

					if (handler.getCatalog() != null) {
						catalog = handler.getCatalog();
					} else if (method.equals("retrieveNamedTopic")
							&& param.indexOf("topicType") == -1
							&& handler.getRootVcds() != null) {
						root = handler.getRootVcds();
						
						// IMPROVEMENT
						Log.printDebug("CatalogueDatabase, root=" + root);
					}
					Log.printDebug("VCDS/" + method + " done"
							+ ", elapsed = " + elapsed);
					MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", " + ext + elapsed);
					break loop;
				} catch (ConnectException e) {
					errorCode = "VOD101";
					MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", " + ext + ", ConnectException " + errorCode);
					Log.printDebug("CatalogueDatabase, errorCode = " + errorCode + ", url = " + url);
					Log.print(e);
				} catch (SAXException e) {
					if (STOP == e) {
						MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", "  + ext + ", STOP by user or timeout");
						Log.printDebug("VCDS/" + method + " stop");
					} else if (UNEXPECTED_ELEMENTS == e) {						
						//R7.4 sykim VDTRMASTER-6175
						errorCode = "VOD110";
						Log.printDebug("CatalogueDatabase, errorCode = " + errorCode + ", url = " + url);
						
					} else {
						errorCode = "VOD103-" + methodToCode.get(method);
						MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", "  + ext + ", SAXException " + errorCode);
						Log.printDebug("CatalogueDatabase, errorCode = " + errorCode + ", url = " + url);
						Log.print(e);
					}
				} catch (IOException e) {
					errorCode = "VOD103-" + methodToCode.get(method);
					Log.printDebug("CatalogueDatabase, errorCode = " + errorCode + ", url = " + url);
					Log.print(e);
					if (timeout == false) {
						MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", "  + ext + ", IOException " + errorCode);
					}
				} catch (Exception e) {
					if (MISSING_ELEMENTS == e) {
						errorCode = "VOD104-" + methodToCode.get(method);
						MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", "  + ext + ", missing mandatory elements " + errorCode);
					} else {
						Log.printDebug("CatalogueDatabase, errorCode = " + errorCode + ", url = " + url);
						Log.print(e);
						//R7.4 sykim VDTRMASTER-6175
						errorCode = "VOD110";
						Log.printDebug("CatalogueDatabase, errorCode = " + errorCode + ", url = " + url);
					}
				} finally {
					
					if (timer != null) {
						timer.removeTVTimerWentOffListener(this);
						TVTimer.getTimer().deschedule(timer);
						timer = null;
					}
					
					try {
						inputStream.close();
					} catch (Exception e) {}
					inputStream = null;

					if (con instanceof HttpURLConnection) {
						((HttpURLConnection) con).disconnect();
					}
					con = null;
					url = null;
				}
				chance--;
				if (chance > 0) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
			} // end of while loop:
			if (stopParsing == false && timeout == false && listener != null) {
				if (chance == 0) { // error case
					cmd = FAIL + errorCode;
				}
				if (Resources.appStatus == VODService.APP_STARTED || Resources.appStatus == VODService.APP_WATCHING) {
					try {
						listener.reflectVCDSResult(cmd);
					} catch (Exception e) {
						Log.print(e);
					}
				} else {
					Log.printDebug("VCDS/will not call back to reflectVCDS(), it's paused or pausing now");
					MenuController.getInstance().hideLoadingAnimation();
				}
			} else {
				//MenuController.getInstance().hideLoadingAnimation();
			}
			aliveRetriever.remove(retriever);
			synchronized (instance) {
				instance.notifyAll();
			}
		} // end of run()

		public void timerWentOff(TVTimerWentOffEvent arg0) {
			if (timer == null) {
				return;
			} // it's done already
			
			if (timer != null) {
				timer.removeTVTimerWentOffListener(this);
				TVTimer.getTimer().deschedule(timer);
				timer = null;
			}
			
			if (handler != null && System.currentTimeMillis() - handler.getLastParsed() < 3000) {
				return; // no problem.. it's on parsing
			}
			timeout = true;
			if (url != null && stopParsing == false) {
				MenuController.getInstance().debugList[1].add(SDF.format(new Date()) + ", "  + ext + ", TimerWentOff VOD102");
				MenuController.getInstance().hideLoadingAnimation();
				stopParsing();
				
				Log.printDebug("Timeout url=" + url);
				CommunicationManager.getInstance().errorMessage("VOD102", null);
			}
			// to handle timeout for Companion
			synchronized (instance) {
				instance.notifyAll();
			}
		}

	}

//	private class ZipImageRetriever extends Thread {
//		public BaseUI listener;
//		public String method;
//		public String param = "";
//		public boolean[] portrait;
//
//		public ZipImageRetriever(String name) {
//			super(name);
//		}
//
//		public void run() {
//			URL url;
//			try {
//				long t = System.currentTimeMillis();
//				url = new URL(DataCenter.getInstance().getString("VCDS_URL")
//						+ method + param);
//				Log.printDebug("VCDS/" + url);
//
//				byte[] retBytes = URLRequestor.getBytes(url.toExternalForm(),
//						null);
//				Hashtable ret = ZipFileReader.read(retBytes);
//				int idx = 0;
//				for (Enumeration e = ret.keys(); e.hasMoreElements();) {
//					String name = (String) e.nextElement();
//					byte[] data = (byte[]) ret.get(name);
//					if (data.length == 0)
//						continue;
//					java.awt.Image tmp = Toolkit.getDefaultToolkit()
//							.createImage(data);
//					Rectangle size = name.indexOf("small") != -1 ? (portrait[idx] ? ImageRetriever.SM_PORTRAIT
//							: ImageRetriever.SM_LAND)
//							: (portrait[idx] ? ImageRetriever.BIG_PORTRAIT
//									: ImageRetriever.BIG_LAND);
//					tmp = tmp.getScaledInstance(size.width, size.height,
//							java.awt.Image.SCALE_DEFAULT);
//					ImageRetriever.tracker.addImage(tmp, 1);
//					ImageRetriever.tracker.waitForID(1);
//					ImageRetriever.tracker.removeImage(tmp, 1);
//					String key = name.substring(0, name.indexOf('_'));
//					CatalogueDatabase.getInstance().addImage(key, tmp);
//					if (listener != null) {
//						listener.reflectImage();
//						Thread.sleep(333);
//					}
//					idx++;
//				}
//				Log.printDebug("VCDS/elapsed = "
//						+ (System.currentTimeMillis() - t));
//			} catch (Exception e) {
//				Log.print(e);
//			} finally {
//				try {
//				} catch (Exception e) {
//				}
//			}
//		}
//	}

	private String listToParam(ArrayList list) {
		if (list.size() == 1) {
			return (String) list.get(0);
		} else if (list.size() == 0) {
			return "";
		}
		StringBuffer buf = new StringBuffer();
		Iterator i = list.iterator();
		buf.append((String) i.next());
		while (i.hasNext()) {
			buf.append(',');
			buf.append((String) i.next());
		}
		return buf.toString();
	}

	public void retrieveShowcases(BaseUI listener, String id, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveShowcases()"
				+ ", listener = " + listener + ", id = " + id + ", cmd = "
				+ cmd);
		Retriever retriever = new Retriever("Showcase=" + id + ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveShowcases";
		retriever.setParam("?showcaseIds=" + URLEncoder.encode(id));
		retriever.cmd = cmd == null ? id : cmd;
		retriever.start();
	}

	public void retrieveEpisodes(BaseUI listener, ArrayList list, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveEpisodes()"
				+ ", listener = " + listener + ", list = " + list + ", cmd = "
				+ cmd);
		Retriever retriever = new Retriever("Episodes=" + list + ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveEpisodes";
//		if (cmd instanceof String && ((String) cmd).startsWith(MenuController.ASSET)) {
//			retriever.setParamWithAll("?episodeIds=" + listToParam(list) + "&writeSeasons=0");
//		} else {
//			retriever.setParam("?episodeIds=" + listToParam(list) + "&writeSeasons=0");
//		}
		
		// VDTRMASTER-5398
		retriever.setParam("?episodeIds=" + listToParam(list) + "&writeSeasons=0");
		retriever.cmd = cmd;
		retriever.start();
	}

	public void retrieveItems(BaseUI listener, ArrayList list, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveItems()"
				+ ", listener = " + listener + ", list = " + list + ", cmd = "
				+ cmd);
		Retriever retriever = new Retriever("Names=" + list + ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveItems";
		retriever.setParam("?orderableItems=" + listToParam(list) + "&writeSeries=0&writeSeasons=0&depth=1");
		retriever.cmd = cmd;
		retriever.noShowError = true;
		retriever.start();
	}

	public void retrieveVideos(BaseUI listener, ArrayList list, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveVideos()"
				+ ", listener = " + listener + ", list = " + list + ", cmd = "
				+ cmd);
		Retriever retriever = new Retriever("Videos=" + list + ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveVideos";
//		if (cmd instanceof String && ((String) cmd).startsWith(MenuController.ASSET)) {
//			retriever.setParamWithAll("?videoIds=" + listToParam(list));
//		} else {
//			retriever.setParam("?videoIds=" + listToParam(list));
//		}
		// VDTRMASTER-5398
		retriever.setParam("?videoIds=" + listToParam(list));
		retriever.cmd = cmd;
		retriever.start();
	}
	
	public void retrieveExtra(BaseUI listener, String extraId, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveExtra()"
				+ ", listener = " + listener + ", extraId = " + extraId
				+ ", cmd = " + cmd);
		Retriever retriever = new Retriever("Extra=" + extraId + ", cmd="
				+ cmd);
		retriever.listener = listener;
		retriever.method = "retrieveCollectionExtras";
		retriever.setParam("?collectionExtraIds=" + extraId);
		retriever.cmd = cmd == null ? extraId : cmd;
		retriever.start();
	}

	public void retrieveBundle(BaseUI listener, String bundleId,
			boolean writeEpisodes, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveBundle()"
				+ ", listener = " + listener + ", bundleId = " + bundleId
				+ ", cmd = " + cmd);
		Retriever retriever = new Retriever("Bundle=" + bundleId + ", cmd="
				+ cmd);
		retriever.listener = listener;
		retriever.method = "retrieveBundles";
		retriever.setParam("?bundleIds=" + bundleId + "&writeSeasons=0&writeEpisodes=" + (writeEpisodes ? 1 : 0));
		retriever.cmd = cmd == null ? bundleId : cmd;
		retriever.start();
	}

	public void retrieveSeason(BaseUI listener, String seasonId, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveSeason()"
				+ ", listener = " + listener + ", seasonId = " + seasonId
				+ ", cmd = " + cmd);
		Retriever retriever = new Retriever("Season=" + seasonId + ", cmd="
				+ cmd);
		retriever.listener = listener;
		retriever.method = "retrieveSeasons";
		retriever.setParam("?seasonIds=" + URLEncoder.encode(seasonId) + "&writeSeries=0");
		retriever.cmd = cmd == null ? seasonId : cmd;
		retriever.start();
	}

	public void retrieveSeries(BaseUI listener, String seriesId, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveSeries()"
				+ ", listener = " + listener + ", seriesId = " + seriesId
				+ ", cmd = " + cmd);
		Retriever retriever = new Retriever("Series=" + seriesId + ", cmd="
				+ cmd);
		retriever.listener = listener;
		retriever.method = "retrieveSeries";
		retriever.setParam("?seriesIds=" + URLEncoder.encode(seriesId) + "&writeEpisodes=0");
		retriever.cmd = cmd == null ? seriesId : cmd;
		retriever.start();
	}

	public void retrieveCategory(BaseUI listener, String categoryId, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveCategory()"
				+ ", listener = " + listener + ", categoryId = " + categoryId
				+ ", cmd = " + cmd);
		Retriever retriever = new Retriever("CategoryContent=" + categoryId
				+ ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveCategory";
		retriever.setParam("?categoryId=" + URLEncoder.encode(categoryId) + "&depth=1&allDetails=0&includeSamples=1");
		retriever.cmd = cmd == null ? categoryId : cmd;
		retriever.start();
	}

	public void retrieveCategoryContents(BaseUI listener, String categoryId,
			Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveCategoryContents()"
						+ ", listener = " + listener + ", categoryId = "
						+ categoryId + ", cmd = " + cmd);
		Retriever retriever = new Retriever("CategoryContent=" + categoryId
				+ ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveCategory";
		retriever.setParam("?categoryId=" + URLEncoder.encode(categoryId) + "&depth=1&writeSeasons=0");
		retriever.cmd = cmd;
		if (cmd instanceof CategoryContainer) {
			if (((CategoryContainer) cmd).categorizedBundle != null) {
				retriever.method = "retrieveCategorizedBundles";
				retriever.setParam("?categorizedBundleIds=" + URLEncoder.encode(categoryId) + "&depth=1&writeSeasons=0");
			}
		}
		retriever.start();
	}
	
	private String lastRequestedCategoryId;
	public CategoryContainer getLastRequestedCategoryContainer() {
		return (CategoryContainer) getCached(lastRequestedCategoryId);
	}

	public void setLastRequestedCategoryContainerId(String id) {
	    lastRequestedCategoryId = id;
    }

	public void retrieveCategoryContents(BaseUI listener, String categoryId,
			Object cmd, int startIdx, int nbElements, String sort) {
		retrieveCategoryContents(listener, categoryId, cmd, startIdx, nbElements, sort, false);
	}
	public void retrieveCategoryContents(BaseUI listener, String categoryId,
			Object cmd, int startIdx, int nbElements, String sort, boolean isPreActive) {
		if (nbElements == 0) {
			retrieveCategoryContents(listener, categoryId, cmd);
			return;
		}
		
		if ("".equals(sort)) {
			sort = lastSortType;
		} else {
			lastSortType = sort;
		}
		
		Log.printDebug("CatalogueDatabase, retrieveCategoryContents()"
						+ ", listener = " + listener + ", categoryId = "
						+ categoryId + ", cmd = " + cmd 
						+ ", start = " + startIdx + ", nb = " + nbElements
						+ ", sort = " + sort);
		if (categoryId == null) {
			categoryId = lastRequestedCategoryId;
		}
		if (SORT.equals(cmd)) {
			CategoryContainer cat = (CategoryContainer) getCached(categoryId);
			if (cat != null) {
				cat.reset();
				cat.madeTotal = false;
			}
		}
		Retriever retriever = new Retriever("CategoryContent=" + categoryId
				+ ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveCategory";
		retriever.setParam("?categoryId=" + URLEncoder.encode(categoryId) 
				+ "&depth=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0&startElementIdx=" 
				+ startIdx + "&nbElements=" + nbElements + "&sortType=" + URLEncoder.encode(sort) 
				+ (isPreActive ? "&_P" + DataCenter.getInstance().getString("PRE_ACTIVE") : ""));
		retriever.cmd = cmd == null ? categoryId : cmd;
		retriever.lowPriority = isPreActive;
		
		CategoryContainer cat = (CategoryContainer) getCached(categoryId);
		if (cat != null) {
			cat.idx = startIdx;
//			}
			if (cmd == null) {
				retriever.cmd = cat;
			}
		}
		lastRequestedCategoryId = categoryId;
		 
		if (cmd instanceof CategoryContainer) {
			if (((CategoryContainer) cmd).categorizedBundle != null) {
				retriever.method = "retrieveCategorizedBundles";
				retriever.setParam("?categorizedBundleIds=" + URLEncoder.encode(categoryId) + "&depth=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
			}
		}
		retriever.start();
	}

	public void retrieveChannel(BaseUI listener, Topic topic, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveChannels(), listener = " + listener + ", topic = " + topic.toString(true));
		Retriever retriever = new Retriever("retrieveChannels");
		retriever.listener = listener;
		retriever.method = "retrieveChannels";
		if (topic.channel != null) {
			retriever.setParam("?callLetters=" + URLEncoder.encode(topic.channel.callLetters) + "&depth=1&includeSamples=1&allDetails=0&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		} else if (topic.network != null) {
			retriever.setParam("?providerIds=" + URLEncoder.encode(topic.id) + "&depth=1&includeSamples=1&allDetails=0&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		}
		retriever.cmd = cmd == null ? MenuTreeUI.MENU_CHANNEL : cmd;
		retriever.start();
	}
	
	// R5
	public void retrieveChannel(BaseUI listener, String callLetter, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveChannels(), listener = " + listener + ", callLetter = " + callLetter);
		Retriever retriever = new Retriever("retrieveChannels");
		retriever.listener = listener;
		retriever.method = "retrieveChannels";
		retriever.setParam("?callLetters=" + URLEncoder.encode(callLetter) + "&depth=1&includeSamples=1&allDetails=0&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		retriever.cmd = cmd == null ? MenuTreeUI.MENU_CHANNEL : cmd;
		retriever.start();
	}
	
	public void retrieveServices(BaseUI listener, String id, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveServices(), listener = " + listener + ", serviceId = " + id);
		Retriever retriever = new Retriever("retrieveServices");
		retriever.listener = listener;
		retriever.method = "retrieveServices";
		retriever.setParam("?serviceIds=" + URLEncoder.encode(id));
		retriever.cmd = cmd;
		retriever.start();
	}
	
	public boolean retrieveServices(BaseUI listener) { // retrieve not retrieved services
		Log.printDebug("CatalogueDatabase, retrieveServices(), listener = " + listener);
		Retriever retriever = new Retriever("retrieveServices");
		retriever.listener = listener;
		retriever.method = "retrieveServices";
		StringBuffer param = new StringBuffer("?serviceIds=");
		for (Iterator i = elementPool.keySet().iterator(); i.hasNext(); ) {
			Object key = i.next();
			Object element = elementPool.get(key);
			if (element instanceof Service) {
				Service svc = (Service) element;
				if (svc.serviceType == Service.SVOD && svc.description == null) {
					param.append(svc.id).append(",");
				}
			}
		}
		String str = param.toString();
		if (str.length() > 12) { // 12 == "?serviceIds=".length()
			retriever.setParam(str);
			retriever.start();
			return true;
		}
		return false;
	}
	
	public void retrieveChannelOnDemand(BaseUI listener, String topicName) {
		Log.printDebug("CatalogueDatabase, retrieveChannelOnDemand(), listener = " + listener + ", name = " + topicName);
		Retriever retriever = new Retriever("retrieveChannelOnDemand=" + topicName);
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		
		String encodeStr = "Environnement des Chaines";
		
		try {
			encodeStr = URLEncoder.encode("Environnement des Chaines", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		retriever.setParam( "?topicType=" + encodeStr + "&topicName=" + (topicName == null ? "Videotron" : URLEncoder.encode(topicName))
				+ "&depth=1&allDetails=0&includeSamples=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		retriever.cmd = MenuTreeUI.MENU_CHANNEL_ON_DEMAND;
		retriever.start();
	}
	
	/**
	 * For R5
	 */
	public void retrieveChannelOnDemand2(BaseUI listener, String topicName, boolean allChannels, int startIdx,
			boolean subscribedChannel) {
		Log.printDebug("CatalogueDatabase, retrieveChannelOnDemand2(), listener = " + listener 
				+ ", name = " + topicName + ", allchannels=" + allChannels + ", startIdx=" + startIdx 
				+ ", subscribedChannel=" + subscribedChannel);
		Retriever retriever = new Retriever("retrieveChannelOnDemand=" + topicName);
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		
		String encodeStr = "Environnement des Chaines";
		
		try {
			encodeStr = URLEncoder.encode("Environnement des Chaines", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		ChannelInfoManager chInfoMgr = ChannelInfoManager.getInstance();
		StringBuffer sf = new StringBuffer("?topicType=" + encodeStr + "&topicName=" + 
				(topicName == null ? "Videotron" : URLEncoder.encode(topicName))
				+ "&depth=1&allDetails=1&includeSamples=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		
		sf.append("&allChannels=" + (allChannels ? "1" : "0" ));
		String favorChannels = chInfoMgr.getParamFavoriteChannels();
		if (favorChannels != null) {
			sf.append("&favChannels=" + chInfoMgr.getParamFavoriteChannels());
		}
		
		if (subscribedChannel) {
			String subscribedChannels = chInfoMgr.getParamSubscribedChannels();
			if (subscribedChannels != null && subscribedChannels.length() > 0) {
				sf.append("&subscribedChannels=" + chInfoMgr.getParamSubscribedChannels());
			}
		}
		
		sf.append("&startElementIdx=" + startIdx);
		sf.append("&nbElements=" + NB_ELEMENTS);
		
		retriever.setParam(sf.toString());
		if (startIdx == 0) {
			retriever.cmd = MenuTreeUI.MENU_CHANNEL_ON_DEMAND;
		} else {
			retriever.cmd = MenuTreeUI.MENU_CHANNEL_NEXT;
		}
		retriever.start();
	}
	
	/**
	 * For R5
	 */
	public void retrieveChannelOnDemand2(BaseUI listener, String topicName, boolean allChannels, int startIdx,
			int length, boolean subscribedChannel) {
		Log.printDebug("CatalogueDatabase, retrieveChannelOnDemand2(), listener = " + listener 
				+ ", name = " + topicName + ", allchannels=" + allChannels + ", startIdx=" + startIdx + ", length=" 
				+ length + ", subscribedChannel=" + subscribedChannel);
		Retriever retriever = new Retriever("retrieveChannelOnDemand=" + topicName);
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		
		String encodeStr = "Environnement des Chaines";
		
		try {
			encodeStr = URLEncoder.encode("Environnement des Chaines", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		ChannelInfoManager chInfoMgr = ChannelInfoManager.getInstance();
		StringBuffer sf = new StringBuffer("?topicType=" + encodeStr + "&topicName=" + 
				(topicName == null ? "Videotron" : URLEncoder.encode(topicName))
				+ "&depth=1&allDetails=1&includeSamples=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		
		sf.append("&allChannels=" + (allChannels ? "1" : "0" ));
		String favorChannels = chInfoMgr.getParamFavoriteChannels();
		if (favorChannels != null) {
			sf.append("&favChannels=" + chInfoMgr.getParamFavoriteChannels());
		}
		
		if (subscribedChannel) {
			String subscribedChannels = chInfoMgr.getParamSubscribedChannels();
			if (subscribedChannels != null && subscribedChannels.length() > 0) {
				sf.append("&subscribedChannels=" + chInfoMgr.getParamSubscribedChannels());
			}
		}
		
		sf.append("&startElementIdx=" + startIdx);
		sf.append("&nbElements=" + length);
		
		retriever.setParam(sf.toString());
		if (startIdx == 0) {
			retriever.cmd = MenuTreeUI.MENU_CHANNEL_ON_DEMAND;
		} else {
			retriever.cmd = MenuTreeUI.MENU_CHANNEL_PRELOAD_NEXT;
		}
		retriever.start();
	}
	
	public void retrieveKaraoke(BaseUI listener, String topicName, int depth) {
		Log.printDebug("CatalogueDatabase, retrieveKaraoke(), listener = " + listener + ", name = " + topicName);
		Retriever retriever = new Retriever("retrieveKaraoke=" + topicName);
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		retriever.setParam("?topicType=Karaoke&topicName=" + (topicName == null ? "Videotron" : URLEncoder.encode(topicName)) + "&depth=2");
		retriever.cmd = MenuTreeUI.MENU_KARAOKE;
		retriever.start();
	}
	
	public void retrieveVSD(BaseUI listener, String topicName, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveVSD(), listener = " + listener + ", name = " + topicName);
		Retriever retriever = new Retriever("retrieveVSD=" + topicName);
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		retriever.setParam("?topicType=VSD&topicName=" + URLEncoder.encode(topicName) + "&depth=1&allDetails=0&includeSamples=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		retriever.cmd = cmd == null ? MenuTreeUI.MENU_VSD : cmd;
		retriever.start();
	}

	public void retrieveCategorizedBundles(BaseUI listener, ArrayList list,
			Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveCategorizedBundles()"
						+ ", listener = " + listener + ", list = " + list
						+ ", cmd = " + cmd);
		Retriever retriever = new Retriever("CategorizedBundles=" + list
				+ ", cmd=" + cmd);
		retriever.listener = listener;
		retriever.method = "retrieveCategorizedBundles";
		retriever.setParam("?categorizedBundleIds=" + listToParam(list) + "&depth=1&allDetails=1&writeActors=1");
		retriever.cmd = cmd;
		retriever.start();
	}

	public void retrieveCatalogueStructure(BaseUI listener, Object cmd) {
		if (true) {
			retrieveNamedTopic(listener, cmd);
			return;
		}
	}

	public void retrieveNamedTopic(BaseUI listener, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveNamedTopic()"
				+ ", listener = " + listener + ", catalog = " + catalog);
		Retriever retriever = new Retriever("NamedTopic");
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		retriever.setParam("?depth=1&allDetails=0&includeSamples=1");
		retriever.cmd = cmd;
		
		// IMPROVEMENT
		if (Resources.appStatus != VODService.APP_PAUSED) {
			retriever.loading = true;
		}
		
		retriever.start();
	}
	
	public void retrieveNamedTopic(BaseUI listener, String topicType, String topicName, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveNamedTopic(), listener = " + listener + ", name = " + topicName);
		Retriever retriever = new Retriever("retrieveNamedTopic=" + topicName);
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		retriever.setParam("?topicType=" + topicType + "&topicName=" + URLEncoder.encode(topicName)
				+ "&depth=1&allDetails=1&includeSamples=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		retriever.cmd = cmd == null ? MenuTreeUI.MENU_VSD : cmd;
		retriever.start();
	}
	
	// R5 - TANK
	// VDTRMASTER-5653 & VDTRMASTER-5649
	public void retrieveNamedTopic(BaseUI listener, String topicType, String topicName, String sort, Object cmd) {
		Log.printDebug("CatalogueDatabase, retrieveNamedTopic(), listener = " + listener + ", name = " + topicName
				+ ", sort=" + sort);
		
		if ("".equals(sort)) {
			sort = lastSortType;
		} else {
			lastSortType = sort;
		}
		
		Retriever retriever = new Retriever("retrieveNamedTopic=" + topicName);
		retriever.listener = listener;
		retriever.method = "retrieveNamedTopic";
		retriever.setParam("?topicType=" + topicType + "&topicName=" + URLEncoder.encode(topicName) + "&sortType="
				+ sort + "&depth=1&allDetails=1&includeSamples=1&writeSeriesSeasons=0&writeSeasonsEpisodes=0");
		retriever.cmd = cmd == null ? MenuTreeUI.MENU_VSD : cmd;
		retriever.start();
	}

	// for search application
	public java.awt.Image getImage(String url) {
		return getImage(null, url);
	}

	// for search application
	public java.awt.Image getImage(String urlBase, String baseName) {
		Log.printInfo("ImageServer, getImage()");
		Log.printDebug("urlBase = " + urlBase + ", baseName = "
				+ baseName);
		
		if (posterPool.containsKey(baseName)) {
			Log.printDebug("Hit on cache");
			return (java.awt.Image) posterPool.get(baseName);
		}

		URL url;
		java.awt.Image ret = null;
		try {
			if (baseName.indexOf(ImageRetriever.BASE) > -1) {
				url = new URL(DataCenter.getInstance().getString("IMAGE_URL") + baseName);
			} else {
				url = new URL(DataCenter.getInstance().getString("IMAGE_URL") + ImageRetriever.BASE + baseName);
			}
//			ret = Toolkit.getDefaultToolkit().createImage(url);
			// R5
			byte[] src = URLRequestor.getBytes(url.toString(), null);
			ret = Toolkit.getDefaultToolkit().createImage(src);
			if (ret.getWidth(null) == -1 && ret.getHeight(null) == -1) {
				Log.printWarning("Failed to retrieve, " + url);
				return null;
			}
			posterPool.put(baseName, ret);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (ret != null) {
			Log.printDebug("Getting success, img = " + ret);
			return ret;
		}
		Log.printDebug("Failed to retrieve");
		return null;
	}

	public void dump() {
		for (int i = 0; i < catalog.contentRoot.length; i++) {
			System.out.println("===== "
					+ catalog.contentRoot[i].toString(false));
			for (int j = 0; j < catalog.contentRoot[i].treeRoot.categoryContainer.length; j++) {
				catalog.contentRoot[i].treeRoot.categoryContainer[j]
						.dumpSub("");
			}
		}
	}

	public void dump2() {
		CategoryContainer[] sub = root.getSubcategories();
		for (int i = 0; i < sub.length; i++) {
			System.out.println("===== " + sub[i].toString(false));
			sub[i].dumpSub("");
		}
	}

	public static void main(String[] args) throws Exception {
		long t = System.currentTimeMillis();
		System.out.println("CatalogueDatabase.main()");

		FileInputStream fis = new FileInputStream("d:/retrieveNamedTopic.xml");
		InputSource is = new InputSource(fis);
//		URL url = new URL("http://10.247.56.146:8080/vcds/retrieveCatalogueStructure");
//		InputSource is = new InputSource(url.openConnection().getInputStream());
		
		VCDSHandler handler = new VCDSHandler();
		SAXParserFactory.newInstance().newSAXParser().parse(is, handler);
		Thread.sleep(3000);
		Catalog catalog = handler.getCatalog();
		Vcds vcds = handler.getRootVcds();
		for (int i = 0; i < vcds.child.length; i++) {
			if (vcds.child[i] instanceof Topic) {
				Topic topic = (Topic) vcds.child[i];
				for (int j = 0; j < topic.treeRoot.categoryContainer.length; j++) {
					topic.treeRoot.categoryContainer[j].dumpSub("");
				}
			}
		}
		// if (vcds.child[1] instanceof CategoryContainer) {
		// CategoryContainer cc = (CategoryContainer) vcds.child[1];
		// System.out.println(cc.categoryContainer[0].categoryContainer[0].categorizedBundleContainer[0].categorizedBundle.dumpContent(""));
		// }
		// if (true) return;
		// CatalogueDatabase db = getInstance();
		// DataCenter.getInstance().put("VCDS_URL",
		// "http://10.247.56.146:8080/vcds/");
		// DataCenter.getInstance().put("IMAGE_URL",
		// "http://10.247.56.146:8080/");

		// ArrayList list = new ArrayList();
		// list.add("dexter_1_08_hd_vf");
		// list.add("district_9_hd_va");
		// db.retrieveByOrderable(null, true, list, null);
		// db.retrieveByOrderable(null, false, list, null);
		//
		// Object o = db.getCachedBySspId("dexter_1_08_hd_vf");
		//
		// db.getImage(null, "images/24_small.jpg");
		// if (true) return;
		// db.retrieveBundle(null, "BU_6778", false, null);
		// Thread.sleep(2000);
		// Bundle b = (Bundle) db.getCached("BU_6778");
		// System.out.println(b);
		// db.retrieveCategoryContent(null, "CA771", null);
		// db.retrieveCatalogueStructure(null, null);
		// ArrayList list = new ArrayList();
		// list.add("EP_12395");
		// db.retrieveEpisodes(null, list, null);
		// Catalog catalog = db.getCatalog();
		// while ((catalog = db.getCatalog()) == null) {
		// Thread.sleep(700);
		// System.out.print(".");
		// }
		System.out.println();
		// for (int i = 0; i < catalog.contentRoot.length; i++) {
		// System.out.println("===== "
		// + catalog.contentRoot[i].toString(false));
		// for (int j = 0; j <
		// catalog.contentRoot[i].treeRoot.categoryContainer.length; j++) {
		// catalog.contentRoot[i].treeRoot.categoryContainer[j]
		// .dumpSub("");
		// }
		// }

		// db.retrieveCategory(null, "N_519", null);
		// Thread.sleep(2000);
		// CategoryContainer cc = (CategoryContainer) db.getCached("N_519");
		// cc.dumpSub("!");
		// ArrayList list = new ArrayList();
		// db.retrieveSeries(null, "SR_25", null);
		// db.retrieveCategoryContent(null, "N_137", null);
		// Thread.sleep(5000);
		// Object c = db.getCached("N_9999");
		// System.out.println(c);
		// for (int i = 0; i < catalog.contentRoot.length; i++) {
		// System.out.println("===== " +
		// catalog.contentRoot[i].toString(false));
		// for (int j = 0; j <
		// catalog.contentRoot[i].treeRoot.categoryContainer.length; j++) {
		// catalog.contentRoot[i].treeRoot.categoryContainer[j].dumpSub("");
		// }
		// }

		// System.out.println("CatalogueDatabase.main(), elapsed : " +
		// (System.currentTimeMillis() - t));
		//
		// System.out.println("before parsing(), " + beforeMem);
		// System.out.println("before GC, " +
		// Runtime.getRuntime().freeMemory());
		// System.gc();
		// System.out.println("after GC, " + Runtime.getRuntime().freeMemory());
		// //catalog.dispose();
		// instance.cleanUp();
		// System.gc();
		// System.out.println("after cleanUp(), GC, " +
		// Runtime.getRuntime().freeMemory());
	}
}

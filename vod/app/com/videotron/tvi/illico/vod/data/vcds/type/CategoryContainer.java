package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;


public class CategoryContainer extends MoreDetail implements HasDupNamed, CacheElement {
	public boolean needRetrieve = true;
	public String id;
	public boolean isAdult;
	public boolean displayAsPoster;
	public String layoutName;
	public String sortOrder;
	public int leafCount = -1;
	public String postersFormat;
	public int idx = Integer.MAX_VALUE;
	public boolean madeTotal = false;
	
	public Category category; // or ref
	//public String categoryRef;
	public CategorizedBundle categorizedBundle;
	
	public CategoryContainer[] categoryContainer = new CategoryContainer[0];
	public ShowcaseContainer[] showcaseContainer = new ShowcaseContainer[0];
	public BundleContainer[] bundleContainer = new BundleContainer[0];
	public ExtraContainer[] extraContainer = new ExtraContainer[0];
	public VideoContainer[] videoContainer = new VideoContainer[0];
	public EpisodeContainer[] episodeContainer = new EpisodeContainer[0];
	public SeasonContainer[] seasonContainer = new SeasonContainer[0];
	public SeriesContainer[] seriesContainer = new SeriesContainer[0];
	public ContainerPointer[] containerPointer = new ContainerPointer[0];
	public TopicPointer[] topicPointer = new TopicPointer[0]; 
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	public Service service;
	public Service[] services = new Service[0];

	// R7.3
    public WrappedTopicPointer wrappedTopicPointer;

	// for convenience
	public CategoryContainer[] subCategories = new CategoryContainer[0];
	public CategoryContainer[] subCategoriesAsPoster = new CategoryContainer[0];
	public int type;
	public CategoryContainer parent;
	public MoreDetail[] totalContents = new MoreDetail[0];
	
	private boolean reset = false;
	
	public Topic getTopic() {
		if (topicPointer.length > 0) {
			return topicPointer[0].topic;
		}
		return null;
	}
	
	public void setTopic(Topic topic) {
		if (topicPointer.length > 0) {
			topicPointer[0].topic = topic;
		}
	}
	
	public int getPosition() {
		if (platformValidity != null) return platformValidity.position;
		return platformService == null ? 0 : platformService.position; 
	}
	public String getId() { return getTopic() == null ? id : getTopic().id; }
	public String getTitle() {
		Topic topic = getTopic();
		if (topic != null) {
			return getLanguageContentData(topic.title);// + "@" + Integer.toHexString(hashCode());
		}
		if (categorizedBundle != null) {
			return categorizedBundle.getTitle();
		}
		return category.getTitle();
	}
	public String getDescription() {
		Topic topic = getTopic();
		if (topic != null) {
			return getLanguageContentData(topic.description);
		}
		if (categorizedBundle != null) {
			return categorizedBundle.getDescription();
		}
		return category.getDescription();
	}
	public boolean hasBranding() {
		if (category == null || category.description == null || category.description.imageGroup.length == 0) {
			return false;
		}
		if (category.description.getBackground(true) == null && category.description.getBackground(false) == null) {
			return false;
		}
		return true;
	}
	
	public java.awt.Image getImage(int width, int height) {
		return getImage(category.description, width, height);
	}
	
	public Object chooseImage() { 
		if (category == null || category.description == null) {
			return null;
		}
		if (category.description.imageGroup.length > 0) {
			return chooseImageGroup(category.description.imageGroup);
		}
		return null;
//		return chooseImage(category.description.poster); 
	}
	public boolean isPortrait() { 
		if (type == MenuController.MENU_CHANNEL) {
			return false;
		}
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		return true;
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class; 
		}
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		isAdult = Boolean.valueOf(attributes.getValue(AT_IS_ADULT)).booleanValue();
		displayAsPoster = Boolean.valueOf(attributes.getValue(AT_DISPLAYASPOSTER)).booleanValue();
		layoutName = attributes.getValue(AT_LAYOUT_NAME);
		sortOrder = attributes.getValue(AT_SORT_ORDER);
		try {
			leafCount = Integer.parseInt(attributes.getValue(AT_LEAF_COUNT));
			if (leafCount > 0 && idx != Integer.MAX_VALUE && madeTotal == false) {
				totalContents = new MoreDetail[leafCount];
				madeTotal = true;
			}
		} catch (NumberFormatException e) {}
		postersFormat = attributes.getValue(AT_POSTERS_FORMAT);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_CATEGORY.equals(qName)) {
			category = (Category) element;
		} else if (EL_CATEGORIZED_BUNDLE.equals(qName)) {
			categorizedBundle = (CategorizedBundle) element;
			type = MenuController.MENU_KARAOKE_PACK;
		} else if (EL_CATEGORY_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, element);
			categoryContainer[categoryContainer.length-1].parent = this;
			if (categorizedBundle != null) {
				categoryContainer[categoryContainer.length-1].type = MenuController.MENU_KARAOKE_CONTENTS;
			}
			if (categoryContainer[categoryContainer.length-1].displayAsPoster) {
				subCategoriesAsPoster = (CategoryContainer[]) addArray(subCategoriesAsPoster, CategoryContainer.class, element);
			} else {
				subCategories = (CategoryContainer[]) addArray(subCategories, CategoryContainer.class, element);
			}
		} else if (EL_CATEGORIZED_BUNDLE_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, element);
			categoryContainer[categoryContainer.length-1].parent = this;
			subCategories = (CategoryContainer[]) addArray(subCategories, CategoryContainer.class, element);
		} else if (EL_TOPIC_POINTER.equals(qName)) {
			if (reset) { realReset(); }
//			topicPointer = (TopicPointer[]) addArray(topicPointer, TopicPointer.class, element);
			Topic topic = ((TopicPointer) element).topic;
			CategoryContainer wrap = new CategoryContainer();
			wrap.isAdult = topic.treeRoot.isAdult; // R5
			wrap.parent = this;
			wrap.type = MenuController.MENU_CATEGORY;
			if ("Karaoke".equals(topic.type)) {
				wrap.type = MenuController.MENU_KARAOKE;
			} else if ("Chaîne".equals(topic.type) || "Chaine".equals(topic.type)) {
				wrap.type = MenuController.MENU_CHANNEL;
			} else if ("Groupe de Chaînes".equals(topic.type) || "Groupe de Chaines".equals(topic.type)) {
				wrap.type = MenuController.MENU_CHANNEL;
			} else if ("Environnement des Chaînes".equals(topic.type) || "Environnement des Chaines".equals(topic.type)) {
				wrap.type = MenuController.MENU_CHANNEL_ENV;
			} else if ("VSD".equals(topic.type)) {
				wrap.type = MenuController.MENU_VSD;
			}
			wrap.topicPointer = new TopicPointer[] {
					(TopicPointer) element
			};
			wrap.category = new Category();
			wrap.category.sample = topic.sample;
			wrap.platformService = wrap.topicPointer[0].platformService;
			wrap.platformValidity = wrap.topicPointer[0].platformValidity;
			
			if (LAYOUT_CONTENU.equals(((TopicPointer) element).layoutName)) {
				if (leafCount > 0 && idx != Integer.MAX_VALUE) {
					if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
					totalContents[idx++] = wrap;
					if (idx >= leafCount) idx -= leafCount;
				} else {
					totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, wrap);
				}
			} else {
				categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, wrap);
				subCategories = (CategoryContainer[]) addArray(subCategories, CategoryContainer.class, wrap);
			}
			
		} else if (EL_CONTAINER_POINTER.equals(qName)) {
			if (reset) { realReset(); }
			
			if (LAYOUT_MENU.equals(((ContainerPointer) element).layoutName)) {
				CategoryContainer cat = ((ContainerPointer) element).categoryContainer;

				// VDTRMASTER-6033
                if (((ContainerPointer)element).platformValidity != null && cat.platformValidity != null) {
                    cat.platformValidity.position = ((ContainerPointer)element).platformValidity.position;
                } else if (((ContainerPointer)element).platformServiceSpecific != null && cat.platformService != null) {
                    cat.platformService.position = ((ContainerPointer)element).platformServiceSpecific.position;
                }

				categoryContainer = (CategoryContainer[]) addArray(categoryContainer, CategoryContainer.class, cat);
				categoryContainer[categoryContainer.length-1].parent = this;
				if (categorizedBundle != null) {
					categoryContainer[categoryContainer.length-1].type = MenuController.MENU_KARAOKE_CONTENTS;
				}
				if (categoryContainer[categoryContainer.length-1].displayAsPoster) {
					subCategoriesAsPoster = (CategoryContainer[]) addArray(subCategoriesAsPoster, CategoryContainer.class, cat);
				} else {
					subCategories = (CategoryContainer[]) addArray(subCategories, CategoryContainer.class, cat);
				}
			} else if (LAYOUT_CONTENU.equals(((ContainerPointer) element).layoutName)) {
				ContainerPointer cp = (ContainerPointer) element;
				if (EL_CATEGORY_CONTAINER.equals(cp.containerElementName)) {
					if (leafCount > 0 && idx != Integer.MAX_VALUE) {
						if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
						totalContents[idx++] = cp.categoryContainer;
						if (idx >= leafCount) idx -= leafCount;
					} else {
						totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, cp.categoryContainer);
					}
					
				} else { // videoContainer, episodeContainer, ...
					endElement(cp.containerElementName, cp.someContainer);	
				}
			} else if (((ContainerPointer) element).pointedContainer != null) {
				String container = ((ContainerPointer) element).pointedContainer;
				CategoryContainer cat = (CategoryContainer) CatalogueDatabase.getInstance().getCached(container);
				if (cat != null) {
					if (((ContainerPointer) element).displayAsPoster) {
						subCategoriesAsPoster = (CategoryContainer[]) addArray(subCategoriesAsPoster, CategoryContainer.class, cat);
					} else {
						subCategories = (CategoryContainer[]) addArray(subCategories, CategoryContainer.class, cat);
					}
				} else {
					containerPointer = (ContainerPointer[]) addArray(containerPointer, ContainerPointer.class, element);
				}
			}
		} else if (EL_SHOWCASE_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			showcaseContainer = (ShowcaseContainer[]) addArray(showcaseContainer, ShowcaseContainer.class, element);
		} else if (EL_BUNDLE_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			bundleContainer = (BundleContainer[]) addArray(bundleContainer, BundleContainer.class, element);
			if (leafCount > 0 && idx != Integer.MAX_VALUE) {
				if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
				totalContents[idx++] = ((BundleContainer) element).bundle;
				if (idx >= leafCount) idx -= leafCount;
			} else {
				totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, ((BundleContainer) element).bundle);
			}
		} else if (EL_EXTRA_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			extraContainer = (ExtraContainer[]) addArray(extraContainer, ExtraContainer.class, element);
			if (leafCount > 0 && idx != Integer.MAX_VALUE) {
				if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
				totalContents[idx++] = ((ExtraContainer) element).extra;
				if (idx >= leafCount) idx -= leafCount;
			} else {
				totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, ((ExtraContainer) element).extra);
			}
		} else if (EL_VIDEO_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			videoContainer = (VideoContainer[]) addArray(videoContainer, VideoContainer.class, element);
			if (((VideoContainer) element).video.service.length == 0) {
				((VideoContainer) element).video.service = ((VideoContainer) element).service;
			}
			if (leafCount > 0 && idx != Integer.MAX_VALUE) {
				if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
				totalContents[idx++] = ((VideoContainer) element).video;
				if (idx >= leafCount) idx -= leafCount;
			} else {
				totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, ((VideoContainer) element).video);
			}
		} else if (EL_EPISODE_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			episodeContainer = (EpisodeContainer[]) addArray(episodeContainer, EpisodeContainer.class, element);
			if (leafCount > 0 && idx != Integer.MAX_VALUE) {
				if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
				totalContents[idx++] = ((EpisodeContainer) element).episode;
				if (idx >= leafCount) idx -= leafCount;
			} else {
				totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, ((EpisodeContainer) element).episode);
			}
		} else if (EL_SEASON_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			seasonContainer = (SeasonContainer[]) addArray(seasonContainer, SeasonContainer.class, element);
			if (leafCount > 0 && idx != Integer.MAX_VALUE) {
				if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
				totalContents[idx++] = ((SeasonContainer) element).season;
				if (idx >= leafCount) idx -= leafCount;
			} else {
				totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, ((SeasonContainer) element).season);
			}
		} else if (EL_SERIES_CONTAINER.equals(qName)) {
			if (reset) { realReset(); }
			seriesContainer = (SeriesContainer[]) addArray(seriesContainer, SeriesContainer.class, element);
			if (leafCount > 0 && idx != Integer.MAX_VALUE) {
				if (idx < 0) idx = -idx > leafCount ? 0 : leafCount + idx;
				totalContents[idx++] = ((SeriesContainer) element).series;
				if (idx >= leafCount) idx -= leafCount;
			} else {
				totalContents = (MoreDetail[]) addArray(totalContents, MoreDetail.class, ((SeriesContainer) element).series);
			}
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		} else if (EL_SERVICE.equals(qName)) {
			services = (Service[]) addArray(services, Service.class, element);
			
			if (service == null || ((Service)element).serviceType == Service.VOD) {
				service = (Service) element;
			}
		} else if (EL_WRAPPED_TOPIC_POINTER.equals(qName)) { // R7.3
            wrappedTopicPointer = (WrappedTopicPointer) element;
        }
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_CATEGORY_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				category = (Category) cached;
//			} else {
//				categoryRef = data;
			}
		} else if (EL_CATEGORIZED_BUNDLE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				categorizedBundle = (CategorizedBundle) cached;
				type = MenuController.MENU_KARAOKE_PACK;
			}
		}
	} 
	
	public void register() {
		// R5 - don't rearrange.
		if ( categoryContainer.length > 0 && categoryContainer[0].type == MenuController.MENU_CHANNEL) {
			CatalogueDatabase.getInstance().register(id, this);
			return;
		}
		
		CategoryContainer tmp;
		for (int i = 0; i < categoryContainer.length; i++) {
			for (int j = i+1; j < categoryContainer.length; j++) {
				int pJ = categoryContainer[j].getPosition();
				int pI = categoryContainer[i].getPosition();
				if (pI == pJ) {
					if (CatalogueDatabase.getInstance().frComparator.compare(categoryContainer[j].getTitle(), categoryContainer[i].getTitle()) < 0) {
						pJ = 0;
						pI = 1;
					}
				}
				if (pJ < pI) {
					tmp = categoryContainer[j];
					categoryContainer[j] = categoryContainer[i];
					categoryContainer[i] = tmp;
				} 
			}
		}
		for (int i = 0; i < subCategories.length; i++) {
			for (int j = i+1; j < subCategories.length; j++) {
				int pJ = subCategories[j].getPosition();
				int pI = subCategories[i].getPosition();
				if (pI == pJ) {
					if (CatalogueDatabase.getInstance().frComparator.compare(subCategories[j].getTitle(), subCategories[i].getTitle()) < 0) {
						pJ = 0;
						pI = 1;
					}
				}
				if (pJ < pI) {
					tmp = subCategories[j];
					subCategories[j] = subCategories[i];
					subCategories[i] = tmp;
				}
			}
		}
		for (int i = 0; i < subCategoriesAsPoster.length; i++) {
			for (int j = i+1; j < subCategoriesAsPoster.length; j++) {
				int pJ = subCategoriesAsPoster[j].getPosition();
				int pI = subCategoriesAsPoster[i].getPosition();
				if (pI == pJ) {
					if (CatalogueDatabase.getInstance().frComparator.compare(subCategoriesAsPoster[j].getTitle(), subCategoriesAsPoster[i].getTitle()) < 0) {
						pJ = 0;
						pI = 1;
					}
				}
				if (pJ < pI) {
					tmp = subCategoriesAsPoster[j];
					subCategoriesAsPoster[j] = subCategoriesAsPoster[i];
					subCategoriesAsPoster[i] = tmp;
				}
			}
		}
		CatalogueDatabase.getInstance().register(id, this);
	}
	
	public void reset() {
		reset = true;
	}
	
	private void realReset() {
		reset = false;
		categoryContainer = new CategoryContainer[0];
		showcaseContainer = new ShowcaseContainer[0];
		bundleContainer = new BundleContainer[0];
		extraContainer = new ExtraContainer[0];
		videoContainer = new VideoContainer[0];
		episodeContainer = new EpisodeContainer[0];
		seasonContainer = new SeasonContainer[0];
		seriesContainer = new SeriesContainer[0];
		containerPointer = new ContainerPointer[0];
		subCategories = new CategoryContainer[0];
		subCategoriesAsPoster = new CategoryContainer[0];
		//totalContents = new MoreDetail[0];
	}
	
	public void dispose() {
		id = null;
		sortOrder = null;
		if (category != null) category.dispose();
		category = null;
		for (int i = 0; subCategories != null && i < subCategories.length; i++) {
			subCategories[i] = null;
		}
//		subCategories = null;
		for (int i = 0; subCategoriesAsPoster != null && i < subCategoriesAsPoster.length; i++) {
			subCategoriesAsPoster[i] = null;
		}
//		subCategoriesAsPoster = null;
		for (int i = 0; categoryContainer != null && i < categoryContainer.length; i++) {
			if (categoryContainer[i] != null) categoryContainer[i].dispose();
			categoryContainer[i] = null;
		}
//		categoryContainer = null;
		for (int i = 0; showcaseContainer != null && i < showcaseContainer.length; i++) {
			if (showcaseContainer[i] != null) showcaseContainer[i].dispose();
			showcaseContainer[i] = null;
		}
//		showcaseContainer = null;
		for (int i = 0; bundleContainer != null && i < bundleContainer.length; i++) {
			if (bundleContainer[i] != null) bundleContainer[i].dispose();
			bundleContainer[i] = null;
		}
//		bundleContainer = null;
		for (int i = 0; videoContainer != null && i < videoContainer.length; i++) {
			if (videoContainer[i] != null) videoContainer[i].dispose();
			videoContainer[i] = null;
		}
//		videoContainer = null;
		for (int i = 0; episodeContainer != null && i < episodeContainer.length; i++) {
			if (episodeContainer[i] != null) episodeContainer[i].dispose();
			episodeContainer[i] = null;
		}
//		episodeContainer = null;
		for (int i = 0; seasonContainer != null && i < seasonContainer.length; i++) {
			if (seasonContainer[i] != null) seasonContainer[i].dispose();
			seasonContainer[i] = null;
		}
//		seasonContainer = null;
		for (int i = 0; seriesContainer != null && i < seriesContainer.length; i++) {
			if (seriesContainer[i] != null) seriesContainer[i].dispose();
			seriesContainer[i] = null;
		}
//		seriesContainer = null;
		for (int i = 0; containerPointer != null && i < containerPointer.length; i++) {
			if (containerPointer[i] != null) containerPointer[i].dispose();
			containerPointer[i] = null;
		}
//		containerPointer = null;
		if (platformService != null) platformService.dispose();
		platformService = null;
		for (int i = 0; totalContents != null && i < totalContents.length; i++) {
			totalContents[i] = null;
		}
//		totalContents = null;
	}
	
	public String toString() {
		return Integer.toHexString(hashCode()) + "@" + toString(false);
	}
	
	public String toString(boolean detail) {
		String title = "NO description";
		if (getTopic() != null) {
			return type+":"+getTopic().toString(detail) + (detail ? ("@"+Integer.toHexString(hashCode())) : "") + ", sub = " + subCategories.length;
		}
		if (categorizedBundle != null) {
			return type+":"+categorizedBundle.toString(detail);
		}
		if (category == null) {
			return super.toString();
		}
		if (category.description != null) {
			title = (isAdult ? "[A]":"")
				+ (displayAsPoster ? "[P]":"")
				+ getLanguageContentData(category.description.title);
		}
		if (detail) {
			return "CategoryContainer(" + id + "@" + getPosition() + ":" + type + ") = " + title 
				+ ", sub = " + categoryContainer.length 
				+ ", sc = " + showcaseContainer.length
				+ ", bu = " + bundleContainer.length
				+ ", v = " + videoContainer.length
				+ ", ep = " + episodeContainer.length
				+ ", ss = " + seasonContainer.length
				+ ", sr = " + seriesContainer.length
				+ ", sam = " + (category.sample == null ? "NULL":""+category.sample.data.length);
		}
		return "CategoryContainer(" + id + ":" + type + ") = " + title;
	}
	
	public void dumpSub(String prefix) {
		System.out.println(prefix + toString(true));
		System.out.print(dumpContent(prefix + "+-"));
		for (int i = 0; i < categoryContainer.length; i++) {
			categoryContainer[i].dumpSub(prefix + "  ");
		}
		for (int i = 0; i < topicPointer.length; i++) {
			topicPointer[i].dumpSub(prefix + "  ");
		}
		
	}
	public String dumpContent(String prefix) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < showcaseContainer.length; i++) {
			buf.append(prefix);
			buf.append(showcaseContainer[i].toString(true));
			buf.append("\n");
		}
		for (int i = 0; i < bundleContainer.length; i++) {
			buf.append(prefix);
			buf.append(bundleContainer[i].toString(true));
			buf.append("\n");
		}
		for (int i = 0; i < videoContainer.length; i++) {
			buf.append(prefix);
			buf.append(videoContainer[i].toString(true));
			buf.append("\n");
		}
		for (int i = 0; i < episodeContainer.length; i++) {
			buf.append(prefix);
			buf.append(episodeContainer[i].toString(true));
			buf.append("\n");
		}
		for (int i = 0; i < seasonContainer.length; i++) {
			buf.append(prefix);
			buf.append(seasonContainer[i].toString(true));
			buf.append("\n");
		}
		for (int i = 0; i < seriesContainer.length; i++) {
			buf.append(prefix);
			buf.append(seriesContainer[i].toString(true));
			buf.append("\n");
			buf.append(seriesContainer[i].dumpContent(prefix+"->"));
			//buf.append("\n");
		}
		return buf.toString();
	}
}

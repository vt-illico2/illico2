package com.videotron.tvi.illico.vod.data.vcds.type;

import java.util.ArrayList;
import java.util.Date;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Video extends Orderable implements CacheElement {
	public Channel channel;
	public Network network;
	public boolean inBundleOnly;
	
	public int selected; // for DDC 76, play all
	
	// R5
	public ExternalRef externalRef;

	public boolean isFreeview;
	public String nextEpisodeId;

	public boolean isPortrait() { 
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		return description != null && description.isSpecialEvent() ? false : true;
	}
	public boolean isSpecialEvent() { return description == null ? false : description.isSpecialEvent(); }
	public String getDescription() { return description == null ? NA : description.getSynopsis(); }
	public String getProductionYear() { return description == null ? NA : (description.showProductionYear() ? description.productionYear : ""); }
	public String getDirector(int max) { return description == null ? NA : getPersonName(description.director, max); }
	public String getActors(int max) { return description == null ? NA : getPersonName(description.actor, max); }
	public long getRuntime() { return description == null ? 0L : description.getRuntime(); }
	public String getGenre() { return description == null ? NA : description.getGenre(); }
	public String getType() { return description == null ? NA : description.getType(); }
	public String getTypeID() { return description == null ? NA : description.getTypeID(); }
	public String getCountry(int max) { return description == null ? NA : (description.isSpecialEvent() ? "" : description.getCountry(max)); }

	public boolean hasClosedCaption() {
		for (int i = 0; detail != null && i < detail.length; i++) {
			for (int j = 0; j < detail[i].videoPlayout.length; j++) {
				if (detail[i].videoPlayout[j].isClosedCaptioned) {
					return true;
				}
			}
		}
		return false;
	}
	
//	private Period getLastPassedPeriod() {
//		if (offer.length == 0) {
//			return null;
//		}
//		Date now = new Date();
//		Date latest = new Date(0);
//		Period ret = null;
//		for (int i = 0; i < offer.length; i++) {
//			if (offer[i].period.start.before(now) && offer[i].period.end.after(now)) {
//				continue; // current offer
//			}
//			if (offer[i].period.start.after(now)) {
//				continue; // net started yet
//			}
//			if (offer[i].period.end.after(latest)) {
//				latest = offer[i].period.end;
//				ret = offer[i].period;
//			}
//		}
//		return ret;
//	}
//	
//	private Period getLastPeriod() {
//		if (offer.length == 0) {
//			return null;
//		}
//		Date latest = new Date(0);
//		Period ret = null;
//		for (int i = 0; i < offer.length; i++) {
//			if (offer[i].period.end.after(latest)) {
//				latest = offer[i].period.end;
//				ret = offer[i].period;
//			}
//		}
//		return ret;
//	}
	
	private Period getClosestPeriod() {
		if (offer.length == 0) {
			return null;
		}
		Date now = new Date();
		Date closest = new Date(Long.MAX_VALUE);
		Period ret = null;
		for (int i = 0; i < offer.length; i++) {
			if (offer[i].period.start.before(now) && offer[i].period.end.after(now)) {
				continue; // current offer
			}
			if (offer[i].period.end.before(now)) {
				continue; // already passed
			}
			if (offer[i].period.start.before(closest)) {
				closest = offer[i].period.start;
				ret = offer[i].period;
			}
		}
		return ret;
	}
	private Period getCurrentPeriod() {
		if (offer.length == 0) {
			return null;
		}		
		Date now = new Date();
		Period ret = null;
		for (int i = 0; i < offer.length; i++) {
			if (offer[i].period.start.after(now)) {
				continue;
			}
			if (offer[i].period.end.before(now)) {
				continue;
			}
			ret = offer[i].period;
		}
		return ret;
	}
	
	public VideoDetails getVideoDetails(String orderableName) {
		for (int i = 0; i < detail.length; i++) {
			for (int j = 0; j < detail[i].videoPlayout.length; i++) {
				if (detail[i].videoPlayout[j].name.equals(orderableName)) {
					return detail[i];
				}
			}
		}
		return null;
	}
	public boolean isHD(Playout playout) {
		for (int i = 0; i < detail.length; i++) {
			for (int j = 0; j < detail[i].videoPlayout.length; j++) {
				if (detail[i].videoPlayout[j].equals(playout)) {
					if ("HD".equals(detail[i].definition)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean isFHD(Playout playout) {
		for (int i = 0; i < detail.length; i++) {
			for (int j = 0; j < detail[i].videoPlayout.length; j++) {
				if (detail[i].videoPlayout[j].equals(playout)) {
					if ("FHD".equals(detail[i].definition)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	// 4K - DDC107
	public boolean isUHD(Playout playout) {
		for (int i = 0; i < detail.length; i++) {
			for (int j = 0; j < detail[i].videoPlayout.length; j++) {
				if (detail[i].videoPlayout[j].equals(playout)) {
					if (Definition.DEFNITION_UHD.equals(detail[i].definition)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public Playout getPlayout() {
		if (detail.length == 0) {
			return null;
		}
		if (detail[0].videoPlayout.length == 0) {
			return null;
		}
		return detail[0].videoPlayout[0];
	}
	public Playout getPlayoutByName(String playoutName) {
		if (playoutName == null) {
			return null;
		}
		for (int i = 0; i < detail.length; i++) {
			for (int j = 0; j < detail[i].videoPlayout.length; j++) {
				if (detail[i].videoPlayout[j].name.equals(playoutName)) {
					return detail[i].videoPlayout[j];
				}
			}
		}
		return null;
	}
	public Playout getPlayout(String orderableName) {
		if (orderableName == null) {
			return null;
		}
		
		boolean isUhd = false;
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		for (int i = 0; i < detail.length; i++) {
			if (orderableName.equals(detail[i].orderableItem)) {
				if (detail[i].videoPlayout.length == 0) {
					return null;
				}
				if (LANGUAGE_FR.equals(detail[i].language)) {
					if (isUhd && DEFNITION_UHD.equals(detail[i].definition)) {
						detail[i].videoPlayout[0].type = Resources.HD_FR;
						// VDTRMASTER-5403
					} else if ("HD".equals(detail[i].definition) || "FHD".equals(detail[i].definition)) {
						detail[i].videoPlayout[0].type = Resources.HD_FR;
					} else {
						detail[i].videoPlayout[0].type = Resources.SD_FR;
					}
				} else {
					if (isUhd && DEFNITION_UHD.equals(detail[i].definition)) {
						detail[i].videoPlayout[0].type = Resources.HD_EN;
						// VDTRMASTER-5403
					} else if ("HD".equals(detail[i].definition) || "FHD".equals(detail[i].definition)) { 
						detail[i].videoPlayout[0].type = Resources.HD_EN;
					} else {
						detail[i].videoPlayout[0].type = Resources.SD_EN;
					}
				}
				return detail[i].videoPlayout[0];
			}
//			for (int j = 0; j < detail[i].videoPlayout.length; j++) {
//				if (detail[i].videoPlayout[j].name.equals(orderableName)) {
//					return detail[i].videoPlayout[j];
//				}
//			}
		}
		return null;
	}
	public Playout getPlayout(int select, boolean preview) {
		String lang = "", definition = "";
		
		boolean isUhd = false;
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		if (isUhd) {
			definition = DEFNITION_UHD;
			switch (select) {
			case Resources.HD_FR:
				lang = LANGUAGE_FR;
				break;
			case Resources.HD_EN:
				lang = LANGUAGE_EN;
			}
		} else {
			switch (select) {
			case Resources.HD_FR: 
				lang = LANGUAGE_FR;
				definition = "FHD";
				break;
	    	case Resources.HD_EN: 
	    		lang = LANGUAGE_EN;
				definition = "FHD";
				break;
	    	case Resources.SD_FR: 
	    		lang = LANGUAGE_FR;
				definition = "SD";
				break;
	    	case Resources.SD_EN:
	    		lang = LANGUAGE_EN;
				definition = "SD";
				break;
			}
		}
		VideoDetails found = null;
		for (int i = 0; i < detail.length; i++) {
			if (definition.indexOf(detail[i].definition) != -1
				&& lang.equals(detail[i].language)) {
				
				found = detail[i];
				break;
			}
		}
		if (found != null) {
			if (preview) {
				return found.previewPlayout.length > 0 ? found.previewPlayout[0] : null;
			} else {
				return found.videoPlayout.length > 0 ? found.videoPlayout[0] : null;
			}
		}
		return null;
	}

	public Date getStart() {
//		if (platformService != null) return platformService.validityPeriod.start;
//		return super.getStart();
		Period p = getCurrentPeriod();
		if (p != null) {
			return p.start;
		}
		p = getClosestPeriod();
		if (p != null) {
			return p.start;
		}
		return super.getStart();
	}
	public boolean inBundle() { return inBundlesAR != null && inBundlesAR.length > 0; }
	public boolean hasTrailer() {
		for (int i = 0; i < detail.length; i++) {
			if (detail[i].previewPlayout.length > 0) return true;
		}
		return false;
	}
	public Playout getTrailer() {
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		for (int i = 0; i < detail.length; i++) { // match language
			if (lang.equals(detail[i].language) && detail[i].previewPlayout.length > 0) {
				return detail[i].previewPlayout[0];
			}
		}
		for (int i = 0; i < detail.length; i++) { // it doesn't care match
			if (detail[i].previewPlayout.length > 0) {
				return detail[i].previewPlayout[0];
			}
		}
		return null;
	}
	
	public String[] getSspIds() {
		ArrayList list = new ArrayList();
		for (int i = 0; i < detail.length; i++) {
			list.add(detail[i].orderableItem);
		}
		String[] ret = new String[list.size()];
		list.toArray(ret);
		return ret;
	}
	
	public String getSspId() {
		String sspId = getSspId(Resources.HD_FR);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.HD_EN);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.SD_FR);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		sspId = getSspId(Resources.SD_EN);
		if ("".equals(sspId) == false) {
			return sspId;
		}
		return "";
	}
	public String getSspId(int select) {
		String lang = "", definition = "";
		
		boolean isUhd = false;
		// VDTRMASTER-5422
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		if (isUhd) {
			definition = DEFNITION_UHD;
			switch (select) {
			case Resources.HD_FR:
				lang = LANGUAGE_FR;
				break;
			case Resources.HD_EN:
				lang = LANGUAGE_EN;
			}
		} else {
			switch (select) {
			case Resources.HD_FR: 
				lang = LANGUAGE_FR;
				definition = "FHD";
				break;
	    	case Resources.HD_EN: 
	    		lang = LANGUAGE_EN;
				definition = "FHD";
				break;
	    	case Resources.SD_FR: 
	    		lang = LANGUAGE_FR;
				definition = "SD";
				break;
	    	case Resources.SD_EN:
	    		lang = LANGUAGE_EN;
				definition = "SD";
				break;
			}
		}
		for (int i = 0; i < detail.length; i++) {
			if (definition.indexOf(detail[i].definition) != -1
				&& lang.equals(detail[i].language)) {

				return detail[i].orderableItem;
			}
		}
		return "";
	}
	public boolean isFree() {
		
		if (Log.EXTRA_ON) {
			Log.printDebug("Video, isFree, offer.length=" + offer.length);
			Log.printDebug("Video, isFree, isFreeview=" + isFreeview);
		}

		if (isFreeview) {
			return true;
		}

		if (offer.length == 0) {
			return false;
		}
		for (int i = 0; i < offer.length; i++) {
			double amt = Double.parseDouble(offer[i].amount);
			if (Log.EXTRA_ON) {
				Log.printDebug("Video, isFree, offer[=" + i +"].amount=" + offer[i].amount);
			}
			if (amt > 0) return false;
		}
		return true;
	}
	
	public boolean isFree(int select) {
		String lang = "", definition = "";
		
		boolean isUhd = false;
		if (getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(getOfferHD(null).definition)) {
			isUhd = true;
		}
		
		if (isUhd) {
			definition = DEFNITION_UHD;
			switch (select) {
			case Resources.HD_FR:
				lang = LANGUAGE_FR;
				break;
			case Resources.HD_EN:
				lang = LANGUAGE_EN;
			}
		} else {
			switch (select) {
			case Resources.HD_FR: 
				lang = LANGUAGE_FR;
				definition = "FHD";
				break;
	    	case Resources.HD_EN: 
	    		lang = LANGUAGE_EN;
				definition = "FHD";
				break;
	    	case Resources.SD_FR: 
	    		lang = LANGUAGE_FR;
				definition = "SD";
				break;
	    	case Resources.SD_EN:
	    		lang = LANGUAGE_EN;
				definition = "SD";
				break;
			}
		}
		for (int i = 0; i < offer.length; i++) {
			// FHD = HD/FHD, SD = SD
			if (definition.indexOf(offer[i].definition) != -1 
				&& lang.equals(offer[i].language)) {

				double amt = Double.parseDouble(offer[i].amount);
				if (amt > 0) return false;
			}
		}
		return true;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		version = attributes.getValue(AT_VERSION);
		titleId = attributes.getValue(AT_TITLE_ID);
		inBundleOnly = Boolean.valueOf(attributes.getValue(AT_IN_BUNDLE_ONLY)).booleanValue();
		isAllScreenType = Boolean.valueOf(attributes.getValue(AT_IS_ALL_SCREEN_TYPE)).booleanValue();

		// Bundling
		isFreeview = Boolean.valueOf(attributes.getValue(AT_FREEVIEW)).booleanValue();

		super.setAttribute(attributes);
	}
	
	public void endElement(String qName, BaseElement element) {
		super.endElement(qName, element);
		if (EL_CHANNEL.equals(qName)) {
			channel = (Channel) element;
		} else if (EL_EXTERNAL_REF.equals(qName)) {
			externalRef = (ExternalRef) element;
		} else if (EL_NETWORK.equals(qName)) {
			network = (Network) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_IN_BUNDLES.equals(qName)) {
			inBundles = data;
			inBundlesAR = TextUtil.tokenize(data, ' ');
		} else if (EL_IN_COLLECTION_EXTRAS.equals(qName)) {
			inCollectionExtras = data;
			inCollectionExtrasAR = TextUtil.tokenize(data, ' ');
		} else if (EL_CHANNEL_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				channel = (Channel) cache;
			}
		} else if (EL_NETWORK_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				network = (Network) cache;
			}
		} else if (EL_NEXT_EPIDOE_ID.equals(qName)) {
			nextEpisodeId = data;
		}
	}
	
	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}

	public String toString() {
		return id;
	}
	
	public String toString(boolean detail) {
		if (description == null) {
			return "Video(" + id + ":" + getType() + ") = NO description";
		}
		return "Video(" + id + ":" + getType() + ") = " + getLanguageContentData(description.title);
	}
	
	public void dispose() {
		super.dispose();
		channel = null;
	}
}

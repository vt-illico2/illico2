package com.videotron.tvi.illico.vod.data.vcds.type;

import java.io.File;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;

public class Vcds extends BaseElement {
	public Versions versions;
	public BaseElement[] child = new BaseElement[0];

	public void endElement(String qName, BaseElement element) {
		if (EL_VERSION_GROUP_ID.equals(qName)) {
			versions = (Versions) element;
		} else {
			child = (BaseElement[]) addArray(child, BaseElement.class, element);
		}
	}

	public CategoryContainer[] getSubcategories() {
		for (int i = 0; i < child.length; i++) {
			if (child[i] instanceof Topic) {
				return ((Topic) child[i]).treeRoot.categoryContainer;
			}
		}
		return new CategoryContainer[0];
	}

	public void addToolsCategory() { // search, resume, wishlist
		for (int i = 0; i < child.length; i++) {
						
			if (child[i] instanceof Topic && "VSD".equals(((Topic) child[i]).type) && "TO_1".equals(((Topic) child[i]).id)) {
				//Tree root = ((Topic) child[i]).treeRoot;
				CategoryContainer root = ((Topic) child[i]).treeRoot;
				if (root.categoryContainer.length > 0 && 
						(root.categoryContainer[root.categoryContainer.length-1].type == MenuController.MENU_SEARCH
								|| root.categoryContainer[root.categoryContainer.length-1].type == MenuController.MENU_RESUME
								|| root.categoryContainer[root.categoryContainer.length-1].type == MenuController.MENU_WISHLIST)) {
					return; // already added
				}
				
				if (Log.DEBUG_ON) {
					Log.printDebug("Vcds, addToolsCategory, root.categoryContainer.length=" + root.categoryContainer.length);
					
					for (int k=0; k<root.categoryContainer.length; k++) {
						//R7.4 sykim test
						Log.printDebug("addToolsCategory " + root.categoryContainer[k]);
					
					}
				}
				
				try {
					File categoryList = (File) DataCenter.getInstance().get("meta_tools_category");
					if (categoryList == null) return;
			        String[] categoriesStr = TextReader.read(categoryList);
			        CategoryContainer container = null;
			        String[] parsedStr = null;
			        for (int j = 0; j < categoriesStr.length; j++) {
			            parsedStr = TextUtil.tokenize(categoriesStr[j], "|");
			            container = new CategoryContainer();
			            container.id = parsedStr[0];

			            // R7.3 removed
//			            if (parsedStr[2].equals("search")) {
//			            	container.type = MenuController.MENU_SEARCH;
//
//			            	// VDTRMASTER-5812
//			            	container.platformValidity = new PlatformValidity();
//				            container.platformValidity.position = 100;
//
//			            } else

                        if (parsedStr[2].equals("resume")) {
			            	container.type = MenuController.MENU_RESUME;
			            	container.isAdult = true;
			            	
			            	// VDTRMASTER-5812
			            	container.platformValidity = new PlatformValidity();
				            container.platformValidity.position = 101;
				            
			            } else if (parsedStr[2].equals("wishlist")) {
			            	container.type = MenuController.MENU_WISHLIST;
			            	container.isAdult = true;
			            	
			            	// VDTRMASTER-5812
			            	container.platformValidity = new PlatformValidity();
				            container.platformValidity.position = 102;
			            } else {
			            	continue;
			            }
			            container.category = new Category();
			            container.category.description = new Description();
			            container.category.description.title[0] = new LanguageContent(LANGUAGE_EN, parsedStr[4]);
			            container.category.description.title[1] = new LanguageContent(LANGUAGE_FR, parsedStr[5]);
			            root.categoryContainer = (CategoryContainer[]) addArray(root.categoryContainer, CategoryContainer.class, container);
			        }
				} catch (Exception e) {
					Log.print(e);
				}
			}
		}
	}

	public void dispose() {
		for (int i = 0; i < child.length; i++) {
			if (child[i] != null) child[i].dispose();
			child[i] = null;
		}
//		child = null;
	}
}

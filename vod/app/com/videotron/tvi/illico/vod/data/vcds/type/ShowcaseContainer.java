package com.videotron.tvi.illico.vod.data.vcds.type;


public class ShowcaseContainer extends BaseContainer {
	public Showcase showcase; // or ref
	public String showcaseRef;
	
	public String toString(boolean detail) {
		if (showcase == null) {
			return "showcase is " + showcaseRef;
		}
		return "(" + getPosition() + ") " + showcase.toString(detail);
	}
	
	public void dispose() {
		super.dispose();
		if (showcase != null) showcase.dispose();
		showcase = null;
	}
}

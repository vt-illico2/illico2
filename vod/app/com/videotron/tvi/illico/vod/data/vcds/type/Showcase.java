package com.videotron.tvi.illico.vod.data.vcds.type;

import java.util.ArrayList;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;

public class Showcase extends MoreDetail implements HasDupNamed, CacheElement {
	public String id;
	public String version;
	public Description description;
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	public ShowcaseDetails detail;

	// R7.3
    public String ratio;
	
	public String getId() { return id; }
	public MoreDetail getContent() { return detail == null ? null : detail.getContent(); }
	public boolean isBigger() { return detail == null ? false : detail.importance > 1; }
	public String getTitle() {
		
		//R7.4 VDTRMASTER-6175
		if (detail != null && detail.isTopicPointer)
			return detail.getContent().getTitle();
		
		return description == null ? NA : getLanguageContentData(description.title); 
	}
//	public java.awt.Image getImage() {
//		if (description == null) {
//			return null;
//		}
//		if (description.imageGroup.length > 0) {
//			return getImage(description.imageGroup);
//		}
//		return null;
////		return getImage(description.poster);
//	}
	public java.awt.Image getImage(int width, int height) {
		if (description != null && description.imageGroup.length > 0) {
			return getImage(description, width, height);
		}
		if (getContent() != null) {
			return getContent().getImage(width, height);
		}
		
		if (detail.getTopic() != null && detail.getTopic().imageGroup.length > 0) {
			return detail.getTopic().getImage(detail.getTopic().imageGroup, width, height);
		} else if (detail.getTopic() != null && detail.getTopic().channel != null && detail.getTopic().channel.imageGroup.length > 0) {
			return detail.getTopic().channel.getImage(detail.getTopic().channel.imageGroup, width, height);
		} else if (detail.getTopic() != null && detail.getTopic().network != null && detail.getTopic().network.imageGroup.length > 0) {
			return detail.getTopic().network.getImage(detail.getTopic().network.imageGroup, width, height);
		}
		return ImageRetriever.getDefault(width, height);
	}
	public Object chooseImage() {
		//R7.4 VDTRMASTER-6175
		if (detail != null && detail.isTopicPointer) {
			;
		} else {		
			if (description == null) {
				return null;
			}
			if (description.imageGroup.length > 0) {
				return chooseImageGroup(description.imageGroup);
			}
		}
		if (getContent() != null) {
			return getContent().chooseImage();
		}
		if (detail.getTopic() != null && detail.getTopic().imageGroup.length > 0) {
			return chooseImageGroup(detail.getTopic().imageGroup);
		} else if (detail.getTopic() != null && detail.getTopic().channel != null && detail.getTopic().channel.imageGroup.length > 0) {
			return chooseImageGroup(detail.getTopic().channel.imageGroup);
		} else if (detail.getTopic() != null && detail.getTopic().network != null && detail.getTopic().network.imageGroup.length > 0) {
			return chooseImageGroup(detail.getTopic().network.imageGroup);
		}
		return null;
//		return chooseImage(description.poster); 
	}
	public Object chooseImage(String usage, boolean withContent) {
		if (description == null) {
			return null;
		}
		if (description.imageGroup.length > 0) {
			return chooseImageGroup(description.imageGroup, usage);
		}
		if (withContent && getContent() != null) {
			return getContent().chooseImage();
		}
		return null;
//		return chooseImage(description.poster); 
	}
	public ImageGroup getBanner(int width, int height) {
		if (description == null) {
			return null;
		}
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		ArrayList list = new ArrayList();
		for (int i = 0; i < description.imageGroup.length; i++) {
			if (description.imageGroup[i] == null) {
				continue;
			}
			boolean different = true;
			for (int j = 0; j < description.imageGroup[i].images.length; j++) {
				if (description.imageGroup[i].images[j].width == width 
						&& description.imageGroup[i].images[j].height == height) {
					different = false;
				}
			}
			if (different) {
				continue;
			}
			if (lang.equals(description.imageGroup[i].language)) {
				list.add(0, description.imageGroup[i]);
			} else {
				list.add(description.imageGroup[i]);
			}
		}
		return (ImageGroup) (list.size() == 0 ? null : list.get(0));
	}
//	public ImageGroup getBanner(String usage) {
//		if (description == null) {
//			return null;
//		}
//		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
//		ArrayList list = new ArrayList();
//		for (int i = 0; description.imageGroup != null && i < description.imageGroup.length; i++) {
//			if (usage.equals(description.imageGroup[i].usage) == false) {
//				continue;
//			}
//			if (lang.equals(description.imageGroup[i].language)) {
//				list.add(0, description.imageGroup[i]);
//			} else {
//				list.add(description.imageGroup[i]);
//			}
//		}
//		return (ImageGroup) (list.size() == 0 ? null : list.get(0));
//	}
	public boolean isPortrait() {
		
		//R7.4 VDTRMASTER-6175
		if (detail != null && detail.isTopicPointer) {
			Object img = chooseImage();
			if (img instanceof Poster) {
				return ((Poster) img).portrait;
			} else if (img instanceof ImageGroup) {
				return ((ImageGroup) img).portrait;
			}
			return true;
		}
		
	    // R7.3
        // check ratio attribute in Showcase for COD
        if (ratio != null) {
            if ("2_3".equals(ratio)) {
                return true;
            }
        }

		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		return true;
	}

	// R7.3
    public boolean hasPortraitRatio() {
        if (description != null && description.imageGroup.length > 0) {
            return description.hasPortraitRatio();
        }

        if (detail.getTopic() != null && detail.getTopic().imageGroup.length > 0) {
            return detail.getTopic().hasPortraitRatio();
        } else if (detail.getTopic() != null && detail.getTopic().channel != null && detail.getTopic().channel.imageGroup.length > 0) {
            return detail.getTopic().channel.hasPortraitRatio();
        } else if (detail.getTopic() != null && detail.getTopic().network != null && detail.getTopic().network.imageGroup.length > 0) {
            return detail.getTopic().network.hasPortraitRatio();
        }

        return false;
    }

	public String getDescription() { return description == null ? NA : getLanguageContentData(description.description); }
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DETAIL.equals(qName)) {
			return ShowcaseDetails.class;
		} else if (EL_DESCRIPTION.equals(qName)) {
			return Description.class;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class;
		}
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		version = attributes.getValue(AT_VERSION);
		id = attributes.getValue(AT_ID);

		// R7.3
        ratio = attributes.getValue(AT_RATIO);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			description = (Description) element;
		} else if (EL_DETAIL.equals(qName)) {
			detail = (ShowcaseDetails) element;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		}
	}
	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}
//	public void regist() {
//		Showcase old = (Showcase) CatalogueDatabase.getInstance().getCached(id);
//		if (old == null) {
//			CatalogueDatabase.getInstance().regist(id, this);
//		} else {
//			old.dispose();
//			old.id = this.id;
//			old.version = this.version;
//			old.description = this.description;
//			old.detail = this.detail;
//		}
//	}
	
	public String toString() {
		return toString(false);
	}
	
	public String toString(boolean detail) {
		if (description == null) {
			return "Showcase(" + id + ") = NO description";
		}
		return "Showcase(" + id + (isBigger()?":BIG":"") + ") = " + 
			getLanguageContentData(this.description.title) + " > " + 
			(getContent()!=null?getContent().toString(false):"NULL");
	}
	
	public void dispose() {
		id = null;
		version = null;
		if (description != null) description.dispose();
		description = null;
		if (platformService != null) platformService.dispose();
		platformService = null;
		if (detail != null) detail.dispose();
		detail = null;
	}
}

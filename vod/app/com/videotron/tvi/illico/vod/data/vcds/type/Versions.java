package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Versions extends BaseElement {
	public int catalogue;
	public String catalogueDate;
	public int survivalKit;
	public String survivalKitDate;
	
	public void setAttribute(Attributes attributes) {
		try {
			catalogue = Integer.parseInt(attributes.getValue(AT_CATALOGUE));
		} catch (NumberFormatException nfe) {}
		catalogueDate = attributes.getValue(AT_CATALOGUE_DATE);
		try {
			survivalKit = Integer.parseInt(attributes.getValue(AT_SURVIVALKIT));
		} catch (NumberFormatException nfe) {}
		survivalKitDate = attributes.getValue(AT_SURVIVALKIT_DATE);
	}
	
	public void dispose() {
		catalogueDate = null;
		survivalKitDate = null;
	}
	
	public String toString() {
		return "Ver " + catalogue;
	}

}

package com.videotron.tvi.illico.vod.data.vcds;

import java.awt.Image;
import java.util.Date;
import java.util.HashMap;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;

/**
 * It caches poster images.
 */
public class PosterPool extends HashMap {
	/**
	 * Most recently used.
	 */
	private Entry head;
	/**
	 * Oldest one, remove this when exceed the limit
	 */
	private Entry foot;
	/**
	 * Maximum number of posters to cache
	 */
	private int limit;
	
	private long memory;

	/**
	 * Double linked list for keep the order used.
	 */
	private class Entry {
		/**
		 * Pointer for previously entry.
		 */
		private Entry prev;
		/**
		 * Pointer for next entry.
		 */
		private Entry next;
		/**
		 * Object for key to hash.
		 */
		private Object key;
		/**
		 * Value object. In this case it is poster Image.
		 */
		private Object value;

		/**
		 * Method for print.
		 */
		public String toString() {
			return value.toString();
		}
	}

	/**
	 * Constructor.
	 *
	 * @param limit
	 *            maximum number of poster
	 */
	public PosterPool(int limit) {
		super(limit * 2);
		this.limit = limit;
	}

	/**
	 * Add new poster.
	 *
	 * @see HashMap.put()
	 */
	synchronized public Object put(Object key, Object value) {
		Entry entry = new Entry();
		entry.key = key;
		entry.value = value;
		if (head != null) {
			head.prev = entry;
		}
		entry.next = head;
		head = entry;
		if (foot == null) {
			foot = entry;
		}
		//Object ret = super.put(key, entry);

		if (entry.value instanceof Image) {
			Image img = (Image) entry.value;
			memory += img.getWidth(null) * img.getHeight(null) * 4;
		}
		//R7.4 catch Exception because default iamge is not diplayed when image does not exist.
		Object ret = super.put(key, entry);
		
		return ret;
	}
	
	synchronized public void cut() {
		int cnt = 0;
		int flushedMem = 0;
		int oneMem;
		while (size() > limit && foot != null) {
			super.remove(foot.key);
			Entry prev = foot.prev;
			foot.prev.next = null;
			foot.prev = null;
			if (foot.value instanceof Image) {
				Image img = (Image) foot.value;
				oneMem = img.getWidth(null) * img.getHeight(null) * 4;
				flushedMem += oneMem;
				memory -= oneMem;
				img.flush();
				cnt++;
			}
			foot = prev;
		}
		if (cnt > 0) {
			MenuController.getInstance().debugList[0].add(CatalogueDatabase.SDF.format(new Date()) + ", image flushed: " + cnt + ", graphics memory restored: " + (flushedMem>>10) + " KB");
		}
	}

	/**
	 * Get poster data.
	 *
	 * @see HashMap.get()
	 */
	synchronized public Object get(Object key) {
		Entry entry = (Entry) super.get(key);
		if (entry == null) { // could not found
			return null;
		}
		if (entry != head) { // move to first
			if (entry.prev != null) {
				entry.prev.next = entry.next;
			}
			if (entry.next != null) {
				entry.next.prev = entry.prev;
			}
			if (foot == entry) {
				foot = entry.prev;
			}
			head.prev = entry;
			entry.next = head;
			entry.prev = null;
			head = entry;
		}
		return entry.value;
	}

	/**
	 * Clean up.
	 */
	public void clear() {
		super.clear();
		int cnt = 0;
		while (head != null) {
			if (head.value instanceof Image) {
				((Image) head.value).flush();
				cnt++;
			}
			Entry next = head.next;
			head.prev = null;
			head.next = null;
			head = next;
		}
		head = null;
		foot = null;
		memory = 0;

		Log.printDebug("PosterPool, clear(), flush " + cnt + " image(s)");
	}
	
	public long getMemory() {
		return memory;
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class ContainerPointer extends BaseContainer implements CacheElement {
	public String id;
	public boolean displayAsPoster;
	public String pointedContainer;
	public String layoutName;
	public CategoryContainer categoryContainer;
	public String containerElementName;
	public BaseContainer someContainer;
	public String someContainerRef;
	public PlatformServiceSpecific platformServiceSpecific;
	
	public String getId() { return id; }
	
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		displayAsPoster = Boolean.valueOf(attributes.getValue(AT_DISPLAYASPOSTER)).booleanValue();
		layoutName = attributes.getValue(AT_LAYOUT_NAME);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_PLATFORM_SERVICE_SPECIFIC.equals(qName)) {
			PlatformServiceSpecific pss = (PlatformServiceSpecific) element;
			if (pss.isValid() && "i2".equals(pss.platformService.platformName)) {
				platformServiceSpecific = pss;
			} else {
				// discard, except "i2" platform and not valid period 
			}
		} else if (EL_CATEGORY_CONTAINER.equals(qName)) {
			containerElementName = qName;
			categoryContainer = (CategoryContainer) element;
		} else if (qName.endsWith(EL_CONTAINER)) {
			containerElementName = qName;
			someContainer = (BaseContainer) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) { // VDTRMASTER-6033
            platformValidity = (PlatformValidity) element;
        }
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_POINTED_CONTAINER.equals(qName)) {
			pointedContainer = data;
		} else if (qName.endsWith(EL_CONTAINER_REF)) {
			containerElementName = qName.substring(0, qName.length() - 3);
			someContainer = (BaseContainer) CatalogueDatabase.getInstance().getCached(data);
			if (someContainer == null) {
				someContainerRef = data;
			}
		}
	}
	
	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}
	
	public void dispose() {
		id = null;
		if (platformServiceSpecific != null) platformServiceSpecific.dispose();
		platformServiceSpecific = null;
		pointedContainer = null;
		someContainer = null;
	}


}

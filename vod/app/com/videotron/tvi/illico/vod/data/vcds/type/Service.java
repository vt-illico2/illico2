package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Service extends BaseElement implements HasDupNamed, CacheElement {
	public static final int VOD = 0;
	public static final int SVOD = 1;
	public static final int CONFIG = 2;

	private static final String SVOD_ATTR = "SVOD";
	private static final String VOD_ATTR = "VOD";
	private static final String CONFIG_ATTR = "CONFIG";

	public String id;
	public int serviceType = VOD;

	public String getId() {
		return id;
	}

	public Description description;

	public String getId2() {
		return id.startsWith("S_") ? id.substring(2) : id;
	}

	public String getTitle() {
		if (description == null) {
			return NA;
		}
		return getLanguageContentData(description.title);
	}

	public String getSubscriptionInfo() {
		if (description == null) {
			return NA;
		}
		return getLanguageContentData(description.subscriptionInfo);
	}

	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);

		if (SVOD_ATTR.equals(attributes.getValue(AT_SERVICE_TYPE))) {
			serviceType = SVOD;
		} else if (CONFIG_ATTR.equals(attributes.getValue(AT_SERVICE_TYPE))) {
			serviceType = CONFIG;
		} else {
			serviceType = VOD;
		}
	}

	public void dispose() {
		id = null;
	}

	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return Description.class;
		}
		return null;
	}

	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			description = (Description) element;
		}
	}

	public String toString() {
		return "Service, " + id + ", " + (serviceType == 0 ? "VOD" : serviceType == 1 ? "SVOD" : "CONFIG");
	}

	public String toString(boolean detail) {
		if (description == null) {
			return "Service(" + id + "), NO description";
		}
		return "Service(" + id + "/" + (serviceType == 0 ? "VOD" : serviceType == 1 ? "SVOD" : "CONFIG") + "), title = "
				+ getLanguageContentData(description.title);
	}

	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}

}

package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Series extends MoreDetail implements HasDupNamed, CacheElement {
	public boolean needRetrieve = true;
	public String id;
	public String version;
	public boolean isAllScreenType;
	public ContentDescription description;
	public PlatformServiceSpecific platformService;
	public PlatformValidity platformValidity;
	public Service service;
	public Season[] season = new Season[0];
	//public StaffRef[] actor = new StaffRef[0];
	//public VideoGenreCode genre; // or ref
	//public String genreRef;
	//public Classification classification;
	public String[] seasonIds = new String[0];
//	public String notListed;
	
	private boolean reset = false;
	
	// R5
	public Channel channel;
	public Network network;
	
	public int seasonSize() { return season.length > 0 ? season.length : seasonIds.length; }
	public String getId() { return id; }
	public String getTitle() { 
		String title = description == null ? NA : getLanguageContentData(description.title); 
//		if (Log.EXTRA_ON) {
//			title = id + "|" + title;
//		}
		return title;
	}
	public java.awt.Image getImage(int width, int height) {
		return getImage(description, width, height);
	}
	public Object chooseImage() { 
		if (description == null) {
			return null;
		}
		if (description.imageGroup.length > 0) {
			return chooseImageGroup(description.imageGroup);
		}
		return null;
//		return chooseImage(description.poster); 
	}
	public boolean isPortrait() { 
		Object img = chooseImage();
		if (img instanceof Poster) {
			return ((Poster) img).portrait;
		} else if (img instanceof ImageGroup) {
			return ((ImageGroup) img).portrait;
		}
		return false;
	}
	public String getDescription() { return description == null ? NA : getLanguageContentData(description.description); }
	public String getProductionYear() { return ""; }//description == null ? NA : (description.isSpecialEvent() ? "" : description.productionYear); }
	public String getGenre() { return description == null ? NA : description.getGenre(); }
	public String getActors(int max) { return description == null ? "" : description.getActors(max); }
	public boolean hasEnglish() { return description.hasEnglish(); }
	public boolean hasFrench() { return description.hasFrench(); }
	public String getRating() { return description == null ? NA : description.getRating(); }
	public boolean hasClosedCaption() { return description.isClosedCaptioned; }
	public boolean is3screen() {
		return isAllScreenType;
//		if (notListed == null) return true;
//		if (notListed.length() == 0) return true;
//		if ("i1".equalsIgnoreCase(notListed)) return true;
//		return false;
	}
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return ContentDescription.class;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformServiceSpecific.class;
		}
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		version = attributes.getValue(AT_VERSION);
		isAllScreenType = Boolean.valueOf(attributes.getValue(AT_IS_ALL_SCREEN_TYPE)).booleanValue();
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			if (description != null) {
				description.updateWith((ContentDescription) element);
			} else {
				description = (ContentDescription) element;
			}
			description.sort();
		} else if (EL_SEASON.equals(qName)) {
			if (reset) { realReset(); }
			season = (Season[]) addArray(season, Season.class, element);
//		} else if (EL_ACTOR.equals(qName)) {
//			actor = (StaffRef[]) addArray(actor, StaffRef.class, element);
//		} else if (EL_CLASSIFICATION.equals(qName)) {
//			classification = (Classification) element;
//		} else if (EL_GENRE.equals(qName)) {
//			genre = (VideoGenreCode) element;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformServiceSpecific) element;
		} else if (EL_PLATFORM_VALIDITY.equals(qName)) {
			platformValidity = (PlatformValidity) element;
		} else if (EL_SERVICE.equals(qName)) {
			//service = (Service[]) addArray(service, Service.class, element);
			service = (Service) element;
		} else if (EL_CHANNEL.equals(qName)) {
			channel = (Channel) element;
		} else if (EL_NETWORK.equals(qName)) {
			network = (Network) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_SEASON_REFS.equals(qName)) {
			if (reset) { realReset(); }
			String[] ref = TextUtil.tokenize(data, ' ');
			for (int i = 0; i < ref.length; i++) {
				Object cached = CatalogueDatabase.getInstance().getCached(data);
				if (cached != null) {
					season = (Season[]) addArray(season, Season.class, cached);
				}
			}
//			Object cached = CatalogueDatabase.getInstance().getCached(data);
//			if (cached != null) {
//				season = (Season[]) addArray(season, Season.class, cached);
//			} else {
//				seasonRefs = data;
//			}
		} else if (EL_GENRE_REF.equals(qName)) {
//			Object cached = CatalogueDatabase.getInstance().getCached(data);
//			if (cached != null) {
//				genre = (VideoGenreCode) cached;
//			} else {
//				genreRef = data;
//			}
		} else if (EL_SEASON_ID.equals(qName)) {
			if (reset) { realReset(); }
			seasonIds = (String[]) addArray(seasonIds, String.class, data);
//		} else if (EL_NOT_LISTED.equals(qName)) {
//			notListed = data;
		} else if (EL_CHANNEL_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				channel = (Channel) cache;
			}
		} else if (EL_NETWORK_REF.equals(qName)) {
			Object cache = CatalogueDatabase.getInstance().getCached(data);
			if (cache != null) {
				network = (Network) cache;
			}
		}
	}
	
	public void register() {
		// sort seasons by season number
		Season temp;
		for (int i = 0; i < season.length; i++) {
			for (int j = i+1; j < season.length; j++) {
				if (season[i].seasonNumber > season[j].seasonNumber) {
					temp = season[i];
					season[i] = season[j];
					season[j] = temp;
				}
			}
		}
		temp = null;
		CatalogueDatabase.getInstance().register(id, this);
	}
//	public void regist() {
//		Series old = (Series) CatalogueDatabase.getInstance().getCached(id);
//		if (old == null) {
//			CatalogueDatabase.getInstance().regist(id, this);
//		} else {
//			old.dispose();
//			old.id = this.id;
//			old.version = this.version;
//			old.description = this.description;
//			old.season = this.season;
//			old.seasonRefs = this.seasonRefs;
//		}
//	}
	
	public void reset() {
		reset = true;
	}
	
	private void realReset() {
		reset = false;
		season = new Season[0];
		seasonIds = new String[0];
	}
	
	public String toString() {
		return Integer.toHexString(hashCode()) + "@" + toString(false);
	}
	
	public String toString(boolean detail) {
		if (detail) {
			if (description == null) {
				return "Series(" + id + ") = NO description, ss = " + season.length;
			}
			return "Series(" + id + ") = " + getLanguageContentData(this.description.title)
				+ ", ss = " + season.length;
		}
		if (description == null) {
			return "Series(" + id + ") = NO description";
		}
		return "Series(" + id + ") = " + getLanguageContentData(this.description.title);
	}
	
	public String dumpContent(String prefix) {
		if (season.length == 0) {
			return prefix + "NO SEASON DATA\n";
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < season.length; i++) {
			buf.append(prefix);
			buf.append(season[i].toString(true));
			buf.append("\n");
			buf.append(season[i].dumpContent(prefix + ">>"));
			//buf.append("\n");
		}
		return buf.toString();
	}
	
	public void dispose() {
		id = null;
		version = null;
		if (description != null) description.dispose();
		description = null;
		if (platformService != null) platformService.dispose();
		platformService = null;
		for (int i = 0; season != null && i < season.length; i++) {
			if (season[i] != null) season[i].dispose();
			season[i] = null;
		}
//		season = null;
	}
}

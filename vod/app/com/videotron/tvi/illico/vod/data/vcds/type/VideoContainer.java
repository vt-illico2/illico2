package com.videotron.tvi.illico.vod.data.vcds.type;


public class VideoContainer extends BaseContainer {
	public Video video; // or ref
	public String videoRef;
	
	public String toString(boolean detail) {
		if (video == null) {
			return "video is " + videoRef;
		}
		return video.toString(detail);
	}
	
	public void dispose() {
		super.dispose();
		if (video != null) video.dispose();
		video = null;
	}
}

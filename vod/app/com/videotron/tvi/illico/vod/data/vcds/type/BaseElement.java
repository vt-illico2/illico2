package com.videotron.tvi.illico.vod.data.vcds.type;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public abstract class BaseElement implements Definition {
	public static final String NA = "N/A";
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat df2 = new SimpleDateFormat("HHmm");
	public static SimpleDateFormat df3 = new SimpleDateFormat("'P'dd'DT'HH'H'");
	
	public void setAttribute(Attributes attributes) {}
	
	public void setChildAttribute(String qName, Attributes attributes) {}
	
	public void setData(String data) {}
	
	public void setDataAt(String qName, String data) {}
	
	//R7.4 sykim VDTRMASTER-6175
	public void endElement(String qName, BaseElement element) throws SAXException {}
	
	public void setLanguageContent(LanguageContent[] languageContents, LanguageContent content) {
		if (LANGUAGE_EN.equals(content.language)) {
			languageContents[0] = content;
		} else {
			languageContents[1] = content;
		}
	}
	
	public void setLanguageContent2(LanguageContent[] languageContents, LanguageContent content) {
		int idx = LANGUAGE_EN.equals(content.language) ? 0 : 1;
		if (languageContents[idx] == null || 
				languageContents[idx].data == null || 
				languageContents[idx].data.length() == 0) {
			languageContents[idx] = content;
			return;
		}
	}
	
	public Object addArray(Object[] array, Class cls, Object toAdd) {
		if (toAdd == null) {
			return array;
		}
		if (toAdd instanceof CacheElement) {
			String id = ((CacheElement) toAdd).getId();
			for (int i = 0; i < array.length; i++) {
				if (array[i] instanceof CacheElement && ((CacheElement) array[i]).getId().equals(id)) {
					array[i] = toAdd;
					return array; // replace element with new element has same id
				}
			}
		} else if (toAdd instanceof String) {
			for (int i = 0; i < array.length; i++) {
				if (array[i].equals(toAdd)) {
					return array; // already has same String
				}
			}
		}
		Object newArray = Array.newInstance(cls, array.length + 1);
		System.arraycopy(array, 0, newArray, 0, array.length);
		Array.set(newArray, array.length, toAdd);
		for (int i = 0; i < array.length; i++) {
			array[i] = null;
		}
		return newArray;
	}
	
	public int getIntValue(String fieldName) {
		try {
			return getClass().getField(fieldName).getInt(this);
		} catch (Exception e) {e.printStackTrace();}
		return 0;
	}
	
	public String toString(boolean detail) {
		return "BaseElement";
	}
	
	public String getLanguageContentData(LanguageContent[] content) {
		if (content == null || content.length == 0) {
			return NA;
		}
		int idx = Resources.currentLanguage;
		if (content[idx] != null) {
			return content[idx].data;
		}
		if (content[1-idx] != null) {
			return content[1-idx].data;
		}
		return NA;
	}
	
	public String getPersonName(StaffRef[] person, int maxPerson) {
		if (person.length == 0) return "";
		StringBuffer buf = new StringBuffer(person[0].getName());
		for (int i = 1; i < Math.min(maxPerson, person.length); i++) {
			buf.append(", ");
			buf.append(person[i].getName());
		}
		return buf.toString();
	}
	
	public Image chooseImage(Image[] images) {
		if (images == null || images.length == 0) return null;
		
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		for (int i = 0; i < images.length; i++) {
			if (lang.equals(images[i].language)) {
				return images[i];
			}
		}
		return images[0];
	}
	
	public ImageGroup chooseImageGroup(ImageGroup[] images) {
		if (images == null || images.length == 0) return null;
		ArrayList list = new ArrayList();
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		for (int i = 0; i < images.length; i++) {
			if (images[i] == null) continue;
			if (lang.equals(images[i].language)) {
				list.add(0, images[i]);
			} else {
				list.add(images[i]);
			}
		}
		return (ImageGroup) (list.size() == 0 ? null : list.get(0));
	}
	
	public ImageGroup chooseImageGroup(ImageGroup[] images, String usage) {
		if (images == null || images.length == 0) return null;
		
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		ArrayList list = new ArrayList();
		for (int i = 0; i < images.length; i++) {
			if (images[i] == null) continue;
			if (images[i].usage.equals(usage) == false) continue;
			if (lang.equals(images[i].language)) {
				list.add(0, images[i]);
			} else {
				list.add(images[i]);
			}
		}
		return (ImageGroup) (list.size() == 0 ? null : list.get(0));
	}
	
	public Image chooseImage(ImageGroup[] images, int width, int height, String usage) {
		if (images == null || images.length == 0) return null;
		
		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
		ArrayList exact = new ArrayList();
		try {
			for (int i = 0; i < images.length; i++) {
				if (usage != null && usage.equals(images[i].usage) == false) {
					continue;
				}
				for (int j = 0; j < images[i].images.length; j++) {
					if (images[i].images[j].width == width && images[i].images[j].height == height) {
						if (lang.equals(images[i].language)) {
							exact.add(0, images[i].images[j]);
						} else {
							exact.add(images[i].images[j]);
						}
					}
				}
			}
		} catch (NullPointerException e) {}
		if (exact.size() == 0) {
			return null;
		}
		return (Image) exact.get(0);
	}
	
//	public java.awt.Image getImage(Image[] images) {
//		return CatalogueDatabase.getInstance().getImage(chooseImage(images));
//	}
	
//	public java.awt.Image getImage(ImageGroup[] images) {
//		return CatalogueDatabase.getInstance().getImage(chooseImageGroup(images));
//	}
	
	public java.awt.Image getImage(ImageGroup[] images, int width, int height) {
		if (images != null && images.length > 0) {
			String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
			ArrayList exact = new ArrayList();
			for (int i = 0; i < images.length; i++) {
				for (int j = 0; j < images[i].images.length; j++) {
					if (images[i].images[j].width == width && images[i].images[j].height == height) {
						if (lang.equals(images[i].language)) {
							exact.add(0, images[i].images[j]);
						} else {
							exact.add(images[i].images[j]);
						}
					}
				}
			}
			if (exact.size() > 0) {
				return CatalogueDatabase.getInstance().getImage((Image) exact.get(0), width, height);
			}
		}
		return CatalogueDatabase.getInstance().getImage(null, width, height);
	}
	
//	public java.awt.Image getImage(ImageGroup[] images, String usage) {
//		if (images == null || images.length == 0) return null;
//		
//		String lang = Resources.currentLanguage == 0 ? LANGUAGE_EN : LANGUAGE_FR;
//		ArrayList exact = new ArrayList();
//		for (int i = 0; i < images.length; i++) {
//			if (images[i].usage.equals(usage)) {
//				if (lang.equals(images[i].language)) {
//					exact.add(0, images[i].images[0]);
//				} else {
//					exact.add(images[i].images[0]);
//				}
//			}
//		}
//		if (exact.size() == 0) {
//			return null;
//		}
//		return CatalogueDatabase.getInstance().getImage((Image) exact.get(0));
//	}
	
	public void reset() {};
	
	abstract public void dispose();
	
	public int ratingToInt(String rating) {
		if (RATING_OVER_8.equals(rating)) return 1;
		if (RATING_OVER_13.equals(rating)) return 2;
		if (RATING_OVER_16.equals(rating)) return 3;
		if (RATING_OVER_18.equals(rating)) return 4;
		return 0;
	}
	
	public String intToRating(int i) {
		switch (i) {
		case 1: return RATING_OVER_8;
		case 2: return RATING_OVER_13;
		case 3: return RATING_OVER_16;
		case 4: return RATING_OVER_18;
		default: return RATING_G;
		}
	}
}

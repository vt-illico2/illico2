package com.videotron.tvi.illico.vod.data.vcds.type;

import java.util.ArrayList;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class ContentDescription extends BaseElement implements HasDupNamed {
	public LanguageContent[] title;
	public LanguageContent[] description;
	public ImageGroup[] imageGroup = new ImageGroup[0];
	public Classification classification;
	public VideoGenreCode genre; // or ref
	public VideoType videoType; // or ref
	public String tagsInfo;
	
	public String productionYear = "";
//	public boolean isSpecialEvent;
	//public String tagsInfo;
	//public LanguageContent[] briefTitle;
	//public LanguageContent[] informativeTitle;
	//public LanguageContent[] originalTitle;
	public LanguageContent[] longSynopsis;
	//public LanguageContent[] mediumSynopsis;
	//public LanguageContent[] shortSynopsis;
//	public Poster[] poster = new Poster[0]; // removed R2
	public Image[] background = new Image[0];
	public LanguageContent[] duration;
	public StaffRef[] actor = new StaffRef[0];
	public StaffRef[] director = new StaffRef[0];
	public StaffRef[] producer = new StaffRef[0];
	public StaffRef[] writer = new StaffRef[0];
	public ProductionCountry[] productionCountry = new ProductionCountry[0]; 
	public boolean isClosedCaptioned;
	public String languages;
	
	// R5
	public String originalTitle;
	public LanguageContent[] longTitle;
	
	private int isSpecialEvent = -1;
	private int showProductionYear = -1;
	
	public void updateWith(ContentDescription obj) {
		if (hasData(obj.title)) title = obj.title;
		if (hasData(obj.description)) description = obj.description;
		if (obj.imageGroup.length > 0) imageGroup = obj.imageGroup;
		if (obj.classification != null) classification = obj.classification;
		if (obj.genre != null) genre = obj.genre;
		if (obj.videoType != null) videoType = obj.videoType;
		if (obj.tagsInfo != null) tagsInfo = obj.tagsInfo;
		if (obj.productionYear != null && obj.productionYear.length() > 0) productionYear = obj.productionYear;
		if (hasData(obj.longSynopsis)) longSynopsis = obj.longSynopsis;
		if (obj.background.length > 0) background = obj.background;
		if (hasData(obj.duration)) duration = obj.duration;
		if (obj.actor.length > 0) actor = obj.actor;
		if (obj.director.length > 0) director = obj.director;
		if (obj.producer.length > 0) producer = obj.producer;
		if (obj.writer.length > 0) writer = obj.writer;
		if (obj.productionCountry.length > 0) productionCountry = obj.productionCountry;
		if (obj.languages != null) languages = obj.languages;
		if (obj.originalTitle != null) originalTitle = obj.originalTitle;
		if (hasData(obj.longTitle)) longTitle = obj.longTitle;
	}
	
	public boolean hasData(LanguageContent[] data) {
		return data != null && data.length == NUMBER_OF_LANGUAGE && (data[0] != null || data[1] != null);
	}
	
	public String getGenre() {
		if (genre != null) {
			return getLanguageContentData(genre.name);
		}
		return NA;
	}
	
	public String getType() {
		if (videoType != null) {
			return getLanguageContentData(videoType.name);
		}
		return NA;
	}
	
	public String getTypeID() {
		if (videoType != null) {
			return videoType.id;
		}
		return NA;
	}
	
	public String getRating() {	return classification == null ? NA : classification.category; } 
	
	public String getRatingInfo() { return classification == null ? NA : classification.addInfo; }
	
	public boolean isSpecialEvent() {
		if (isSpecialEvent < 0) { // not set yet
			isSpecialEvent = "VT_101".equals(getTypeID()) ? 1 : 0;
		}
		return isSpecialEvent > 0;
	}
	
	public boolean showProductionYear() {
		if (showProductionYear < 0) { // not set yet
			showProductionYear = "VT_101".equals(getTypeID()) || "VT_102".equals(getTypeID()) ? 1 : 0;
		}
		return showProductionYear > 0;
	}
	
	public String getActors(int max) {
		return getPersonName(actor, max);
	}
	
	public boolean hasEnglish() {
		return (languages != null && languages.indexOf(LANGUAGE_EN) != -1);
	}
	
	public boolean hasFrench() {
		return (languages != null && languages.indexOf(LANGUAGE_FR) != -1);
	}

	
	public String getCountry(int max) {
		if (productionCountry.length == 0) return "";
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < Math.min(max, productionCountry.length); i++) {
			if (buf.length() > 0) {
				buf.append(", ");
			}
			buf.append(productionCountry[i].country.code != null 
					? productionCountry[i].country.code 
							: getLanguageContentData(productionCountry[i].country.name));
		}
		if (max < productionCountry.length) {
			if (buf.length() > 0) {
				buf.append(", ...");
			}
		}
		return buf.toString();
	}
	
	public ContentDescription() {
		title = new LanguageContent[NUMBER_OF_LANGUAGE];
		description = new LanguageContent[NUMBER_OF_LANGUAGE];
//		briefTitle = new LanguageContent[NUMBER_OF_LANGUAGE];
//		informativeTitle = new LanguageContent[NUMBER_OF_LANGUAGE];
//		originalTitle = new LanguageContent[NUMBER_OF_LANGUAGE];
		longSynopsis = new LanguageContent[NUMBER_OF_LANGUAGE];
//		mediumSynopsis = new LanguageContent[NUMBER_OF_LANGUAGE];
//		shortSynopsis = new LanguageContent[NUMBER_OF_LANGUAGE];
		duration = new LanguageContent[NUMBER_OF_LANGUAGE];
		
		// R5
		longTitle = new LanguageContent[NUMBER_OF_LANGUAGE];
	}
	
	public String toString() {
		return "ContentDescription, title(e):" + (title[0] == null ? "NULL" : title[0].data) 
				+ ", (f):" + (title[1] == null ? "NULL" : title[1].data)  + ", longTitle(e):" 
				+ (longTitle[0] == null ? "NULL" : longTitle[0].data) + ", (f):" 
				+ (longTitle[1] == null ? "NULL" : title[1].data) + ", desc(e):" 
				+ (description[0] == null ? "NULL" : description[0].data) + ", (f):" 
				+ (description[1] == null ? "NULL" : description[1].data);
	}
	
	public String[] dumpImages() {
		if (imageGroup.length == 0) {
			return new String[] {"0-length ImageGroup"};
		}
		ArrayList dump = new ArrayList();
		for (int i = 0; i < imageGroup.length; i++) {
			dump.add("[" + i + "]=" + imageGroup[i].toString(true));
			for (int j = 0; j < imageGroup[i].images.length; j++) {
				dump.add("-[" + j + "]=" + imageGroup[i].images[j].toString(true));
			}
		}
		String[] ret = new String[dump.size()];
		dump.toArray(ret);
		return ret;
	}
	
	public String getSynopsis() {
		int idx = Resources.currentLanguage;
		if (longSynopsis[idx] != null) return longSynopsis[idx].data;
//		if (mediumSynopsis[idx] != null) return mediumSynopsis[idx].data;
//		if (shortSynopsis[idx] != null) return shortSynopsis[idx].data;
		
		idx = 1-idx;
		if (longSynopsis[idx] != null) return longSynopsis[idx].data;
//		if (mediumSynopsis[idx] != null) return mediumSynopsis[idx].data;
//		if (shortSynopsis[idx] != null) return shortSynopsis[idx].data;
		
		return NA;
	}
	
	public long getRuntime() {
		String runtime = getLanguageContentData(duration);
		try {
			long ret = Long.parseLong(runtime);
			return ret * Constants.MS_PER_MINUTE;
		} catch (NumberFormatException nfe) {}
		return 0L;
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return LanguageContent.class;
		}
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		productionYear = attributes.getValue(AT_PRODUCTION_YEAR);
		//isSpecialEvent = Boolean.valueOf(attributes.getValue(AT_IS_SPECIAL_EVENT)).booleanValue();
		tagsInfo = attributes.getValue(AT_TAGS_INFO);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_TITLE.equals(qName)) {
			setLanguageContent(title, (LanguageContent) element);
		} else if (EL_DESCRIPTION.equals(qName)) {
			setLanguageContent(description, (LanguageContent) element);
//		} else if (EL_BRIEF_TITLE.equals(qName)) {
//			setLanguageContent(briefTitle, (LanguageContent) element);
//		} else if (EL_INFORMATIVE_TITLE.equals(qName)) {
//			setLanguageContent(informativeTitle, (LanguageContent) element);
//		} else if (EL_ORIGINAL_TITLE.equals(qName)) {
//			setLanguageContent(originalTitle, (LanguageContent) element);
		} else if (EL_LONG_SYNOPSIS.equals(qName)) {
			setLanguageContent(longSynopsis, (LanguageContent) element);
		} else if (EL_MEDIUM_SYNOPSIS.equals(qName)) {
			setLanguageContent2(longSynopsis, (LanguageContent) element);
		} else if (EL_SHORT_SYNOPSIS.equals(qName)) {
			setLanguageContent2(longSynopsis, (LanguageContent) element);
//		} else if (EL_POSTER.equals(qName)) {
//			poster = (Poster[]) addArray(poster, Poster.class, element);
		} else if (EL_BACKGROUND.equals(qName)) {
			background = (Image[]) addArray(background, Image.class, element);
		} else if (EL_DURATION.equals(qName)) {
			setLanguageContent(duration, (LanguageContent) element);
		} else if (EL_ACTOR.equals(qName)) {
			actor = (StaffRef[]) addArray(actor, StaffRef.class, element);
		} else if (EL_DIRECTOR.equals(qName)) {
			director = (StaffRef[]) addArray(director, StaffRef.class, element);
		} else if (EL_PRODUCER.equals(qName)) {
			producer = (StaffRef[]) addArray(producer, StaffRef.class, element);
		} else if (EL_WRITER.equals(qName)) {
			writer = (StaffRef[]) addArray(writer, StaffRef.class, element);
		} else if (EL_CLASSIFICATION.equals(qName)) {
			classification = (Classification) element;
		} else if (EL_PRODUCTION_COUNTRY.equals(qName)) {
			productionCountry = (ProductionCountry[]) addArray(productionCountry, ProductionCountry.class, element);
		} else if (EL_GENRE.equals(qName)) {
			genre = (VideoGenreCode) element;
		} else if (EL_VIDEO_TYPE.equals(qName)) {
			videoType = (VideoType) element;
		} else if (EL_IMAGE_GROUP.equals(qName)) {
			imageGroup = (ImageGroup[]) addArray(imageGroup, ImageGroup.class, element);
			((ImageGroup) element).title = getLanguageContentData(title);
		} else if (EL_LONG_TITLE.equals(qName)) {
			setLanguageContent(longTitle, (LanguageContent) element);
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_GENRE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				genre = (VideoGenreCode) cached;
			}
		} else if (EL_VIDEO_TYPE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				videoType = (VideoType) cached;
			}
		} else if (EL_IS_CLOSED_CAPTIONED.equals(qName)) {
			isClosedCaptioned = Boolean.valueOf(data).booleanValue();
		} else if (EL_LANGUAGES.equals(qName)) {
			languages = data;
		} else if (EL_ORIGINAL_TITLE.equals(qName)) {
			originalTitle = data;
		}
	}
	
	public void sort() {
		ProductionCountry tmp;
		for (int i = 0; i < productionCountry.length; i++) {
			for (int j = i+1; j < productionCountry.length; j++) {
				if (productionCountry[i].priority > productionCountry[j].priority) {
					tmp = productionCountry[i];
					productionCountry[i] = productionCountry[j];
					productionCountry[j] = tmp;
				}
			}
		}
	}

	public void dispose() {
		productionYear = null;
		for (int i = 0; title != null && i < title.length; i++) {
			if (title[i] != null) title[i].dispose();
			title[i] = null;
		}
//		title = null;
		for (int i = 0; description != null && i < description.length; i++) {
			if (description[i] != null) description[i].dispose();
			description[i] = null;
		}
		for (int i = 0; imageGroup != null && i < imageGroup.length; i++) {
			if (imageGroup[i] != null) imageGroup[i].dispose();
			imageGroup[i] = null;
		}
//		for (int i = 0; briefTitle != null && i < briefTitle.length; i++) {
//			if (briefTitle[i] != null) briefTitle[i].dispose();
//			briefTitle[i] = null;
//		}
//		briefTitle = null;
//		for (int i = 0; informativeTitle != null && i < informativeTitle.length; i++) {
//			if (informativeTitle[i] != null) informativeTitle[i].dispose();
//			informativeTitle[i] = null;
//		}
//		informativeTitle = null;
//		for (int i = 0; originalTitle != null && i < originalTitle.length; i++) {
//			if (originalTitle[i] != null) originalTitle[i].dispose();
//			originalTitle[i] = null;
//		}
//		originalTitle = null;
		for (int i = 0; longSynopsis != null && i < longSynopsis.length; i++) {
			if (longSynopsis[i] != null) longSynopsis[i].dispose();
			longSynopsis[i] = null;
		}
//		longSynopsis = null;
//		for (int i = 0; mediumSynopsis != null && i < mediumSynopsis.length; i++) {
//			if (mediumSynopsis[i] != null) mediumSynopsis[i].dispose();
//			mediumSynopsis[i] = null;
//		}
//		mediumSynopsis = null;
//		for (int i = 0; shortSynopsis != null && i < shortSynopsis.length; i++) {
//			if (shortSynopsis[i] != null) shortSynopsis[i].dispose();
//			shortSynopsis[i] = null;
//		}
//		shortSynopsis = null;
//		for (int i = 0; poster != null && i < poster.length; i++) {
//			if (poster[i] != null) poster[i].dispose();
//			poster[i] = null;
//		}
//		poster = null;
		for (int i = 0; background != null && i < background.length; i++) {
			if (background[i] != null) background[i].dispose();
			background[i] = null;
		}
//		background = null;
		for (int i = 0; duration != null && i < duration.length; i++) {
			duration[i] = null;
		}
//		duration = null;
		for (int i = 0; actor != null && i < actor.length; i++) {
			if (actor[i] != null) actor[i].dispose();
			actor[i] = null;
		}
//		actor = null;
		for (int i = 0; director != null && i < director.length; i++) {
			if (director[i] != null) director[i].dispose();
			director[i] = null;
		}
//		director = null;
		for (int i = 0; producer != null && i < producer.length; i++) {
			if (producer[i] != null) producer[i].dispose();
			producer[i] = null;
		}
//		producer = null;
		for (int i = 0; writer != null && i < writer.length; i++) {
			if (writer[i] != null) writer[i].dispose();
			writer[i] = null;
		}
//		writer = null;
		if (classification != null) classification.dispose();
		classification = null;
		for (int i = 0; productionCountry != null && i < productionCountry.length; i++) {
			if (productionCountry[i] != null) productionCountry[i].dispose();
			productionCountry[i] = null;
		}
//		productionCountry = null;
		if (genre != null) genre.dispose();
		genre = null;
		
		if (videoType != null) videoType.dispose();
		videoType = null;
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import java.lang.reflect.Field;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class ShowcaseDetails extends BaseElement {
	public int importance;
	
	public Video video; // or ref
	public String videoRef;
	
	public Episode episode; // or ref
	public String episodeRef;
	
	public Season season; // or ref
	public String seasonRef;
	
	public Series series; // or ref
	public String seriesRef;
	
	public Bundle bundle; // or ref
	public String bundleRef;
	
	public Category category; // or ref
	public String categoryRef;
	
	public CategoryContainer categoryContainer; // or ref
	public String categoryContainerRef;
	
	public CategoryContainer categorizedBundleContainer;
	
	//R7.4 VDTRMASTER-6175 
	public TopicPointer topicPointer;
	public boolean isTopicPointer;
	
	// R5
	private Topic topic; //R7.4 public --> private 
	public Extra extra;
	public String extraRef;
	
	public MoreDetail getContent() {
		if (video != null) return video;
		if (episode != null) return episode;
		if (season != null) return season;
		if (series != null) return series;
		if (bundle != null) return bundle;
		if (categoryContainer != null) return categoryContainer;
		if (categorizedBundleContainer != null) return categorizedBundleContainer;
		if (extra != null) return extra;	
		
		return null;
	}
	
	public Topic getTopic() {
		return topic;
	}
	
	public void setTopic(Topic tp) {
		topic = tp;
	}
	
	public void setAttribute(Attributes attributes) {
		try {
			importance = Integer.parseInt(attributes.getValue(AT_IMPORTANCE));
		} catch (NumberFormatException nfe) {}
	}
	
	public void endElement(String qName, BaseElement element) throws SAXException{
		try { // video, episode, season, series, bundle, category
			Field f = getClass().getDeclaredField(qName);
			f.set(this, element);
		} catch (Exception e) {
			//R7.4 sykim VDTRMASTER-6175
			e.printStackTrace();
			throw CatalogueDatabase.UNEXPECTED_ELEMENTS;
		}
		
		//R7.4 freelife VDTRMASTER-6175
		if (EL_TOPIC_POINTER.equals(qName)) {			
			topicPointer = (TopicPointer)element;
			if (topicPointer.topic.sample != null &&
					topicPointer.topic.sample.data != null) {
				
				isTopicPointer = true;
				
				Sample sample = topicPointer.topic.sample;
				if (sample.data[0] instanceof Video)
					video = (Video)sample.data[0];
				else if (sample.data[0] instanceof Episode)
					episode = (Episode)sample.data[0];
				else if (sample.data[0] instanceof Season)
					season = (Season)sample.data[0];
				else if (sample.data[0] instanceof Series)
					series = (Series)sample.data[0];
				else if (sample.data[0] instanceof Bundle)
					bundle = (Bundle)sample.data[0];
				else if (sample.data[0] instanceof CategoryContainer)
					categoryContainer = (CategoryContainer)sample.data[0];
				else if (sample.data[0] instanceof Extra)
					extra = (Extra)sample.data[0];
								
				topic = null;
				topicPointer = null;
				
				
			} else if (topicPointer.topic.channel != null) {
				topic = topicPointer.topic;
			} else if (topicPointer.topic.network != null) {
				topic = topicPointer.topic;
			} 
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (qName.endsWith("Ref")) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			try {
				if (cached != null) { 
					Field f = getClass().getDeclaredField(qName.substring(0, qName.length()-3));
					f.set(this, cached); // xxx
				} else {
					Field f = getClass().getDeclaredField(qName);
					f.set(this, data); // xxxRef
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void dispose() {
		if (video != null) video.dispose();
		video = null;
		if (episode != null) episode.dispose();
		episode = null;
		if (season != null) season.dispose();
		season = null;
		if (series != null) series.dispose();
		series = null;
		if (bundle != null) bundle.dispose();
		bundle = null;
		if (category != null) category.dispose();
		category = null;
		if (categoryContainer != null) categoryContainer.dispose();
		categoryContainer = null;
		if (topicPointer != null) topicPointer.dispose();
		topicPointer = null;
		
		// R5
		if (topic != null) topic.dispose();
		topic = null;
		if (extra != null) extra.dispose();
		extra = null;
	}
	
	
}

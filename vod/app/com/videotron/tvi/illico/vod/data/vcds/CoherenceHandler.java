package com.videotron.tvi.illico.vod.data.vcds;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.log.Log;

public class CoherenceHandler extends DefaultHandler {
	private long timeStamp;

	/**
	 * Method for begin parsing.
	 */
	public void startDocument() {
		timeStamp = System.currentTimeMillis();
	}

	/**
	 * Method for finish parsing.
	 */
	public void endDocument() {
		String elapsed = String.valueOf(System.currentTimeMillis() - timeStamp);
		Log.printDebug("CoherenceHandler, parsing done, elapsed : "
				+ elapsed);
	}

	/**
	 * Method for begin an element.
	 */
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if ("coherence".equals(qName)) {
			int lastBuild = Integer.parseInt(attributes.getValue("lastSupportedBuildNumber"));
			System.out.println("Coherence = " + lastBuild);
		} else if ("versions".equals(qName)) {
			int catalogue = Integer.parseInt(attributes.getValue("catalogue"));
			String catalogueDate = attributes.getValue("catalogueDate");
			int survivalKit = Integer.parseInt(attributes.getValue("survivalKit"));
			String survivalKitDate = attributes.getValue("survivalKitDate");
			System.out.println("versions = " + catalogue + ", " + catalogueDate + ", " + survivalKit + ", " + survivalKitDate);
		} else if ("add".equals(qName)) {
			String id = attributes.getValue("id");
		} else if ("upd".equals(qName)) {
			String id = attributes.getValue("id");
		} else if ("del".equals(qName)) {
			String id = attributes.getValue("id");
		} else if ("initem".equals(qName)) {
			String id = attributes.getValue("id");
		}
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class Category extends BaseElement implements HasDupNamed, CacheElement {
	public String id;
	public String version;
	public Description description;
	public Sample sample;
	
	public String getId() { return id; }
	public String getTitle() { return getLanguageContentData(description.title); }
	public String getDescription() { return getLanguageContentData(description.description); }
//	public Image getImage() { return getImage(description.poster); }
	public java.awt.Image getImage(int width, int height) {
		return null;
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_DESCRIPTION.equals(qName)) {
			return Description.class;
		} 
		return null;
	}
	
	public void setAttribute(Attributes attributes) {
		id = attributes.getValue(AT_ID);
		version = attributes.getValue(AT_VERSION);
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_DESCRIPTION.equals(qName)) {
			description = (Description) element;
		} else if (EL_SAMPLES.equals(qName)) {
			sample = (Sample) element;
		}
	}
	
	public void register() {
		CatalogueDatabase.getInstance().register(id, this);
	}
//	public void regist() {
//		Category old = (Category) CatalogueDatabase.getInstance().getCached(id);
//		if (old == null) {
//			CatalogueDatabase.getInstance().regist(id, this);
//		} else {
//			old.dispose();
//			old.id = this.id;
//			old.version = this.version;
//			old.description = this.description;
//		}
//	}
	
	public String toString() {
		return "Category(" + id + ")";
	}
	
	public void dispose() {
		id = null;
		version = null;
		if (description != null) description.dispose();
		description = null;
		if (sample != null)	sample.dispose();
		sample = null;
	}
}

package com.videotron.tvi.illico.vod.data.vcds.parser;

import java.io.File;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Stack;

import com.videotron.tvi.illico.vod.data.vcds.type.*;
import com.videotron.tvi.illico.vod.data.vcds.type.Error;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

/**
 * XML handler for VCDS.
 */
public class VCDSHandler extends DefaultHandler {
	/**
	 * Stack instance for parsing.
	 */
	private Stack stack;
	/**
	 * Name of last element parsed.
	 */
	private String lastElement;
	/**
	 * Root element.
	 */
	private Catalog catalog;
	/**
	 * Root element for R2.
	 */
	private Vcds rootVcds;
	/**
	 * Flag for register by SspID.
	 */
	private boolean registerBySspId = false;
	/**
	 * Flags.
	 */
	private boolean needReset = true;
	private boolean lowPriority = false;
	private boolean noShowError = false;
	/**
	 * Hashset for elements occurred.
	 */
	private HashSet elementNames = new HashSet();
	/**
	 * Flag for stop.
	 */
	private boolean stopParsing;

	private long lastPasred;
	private int elementCnt;

    private static final int ID_AVAILABLE_SOURCE = 1;
    private static final int ID_AVAILABLE_SOURCES = 2;
    private static final int ID_BUNDLE = 3;
    private static final int ID_BUNDLE_CONTAINER = 4;
    private static final int ID_BUNDLE_DETAIL = 5;
    private static final int ID_EXTRA = 6;
    private static final int ID_EXTRA_CONTAINER = 7;
    private static final int ID_EXTRA_DETAIL = 8;
    private static final int ID_CATALOGUE = 9;
    private static final int ID_CATEGORIZED_BUNDLE = 10;
    private static final int ID_CATEGORIZED_BUNDLE_CONTAINER = 11;
    private static final int ID_CATEGORY = 12;
    private static final int ID_CATEGORY_CONTAINER = 13;
    private static final int ID_CHANNEL = 14;
    private static final int ID_NETWORK = 15;
    private static final int ID_CLASSIFICATION = 16;
    private static final int ID_CONTAINER_POINTER = 17;
    private static final int ID_CONTENT_ROOT = 18;
    private static final int ID_CONTENT_DESCRIPTION = 19;
    private static final int ID_COUNTRY = 20;
    private static final int ID_EPISODE = 21;
    private static final int ID_EPISODE_CONTAINER = 22;
    private static final int ID_ERROR = 23;
    private static final int ID_BACKGROUND = 24;
    private static final int ID_TITLE = 25;
    private static final int ID_BRIEF_TITLE = 26;
    private static final int ID_SUBSCRIPTION_INFO = 27;
    private static final int ID_IMAGE = 28;
    private static final int ID_IMAGE_GROUP = 29;
    private static final int ID_RESIZABLE_IMAGES = 30;
    private static final int ID_INFORMATIVE_TITLE = 31;
    private static final int ID_ORIGINAL_TITLE = 32;
    private static final int ID_NAME = 33;
    private static final int ID_LONG_SYNOPSIS = 34;
    private static final int ID_MEDIUM_SYNOPSIS = 35;
    private static final int ID_SHORT_SYNOPSIS = 36;
    private static final int ID_LENGTH = 37;
    private static final int ID_ORIGINAL_LANGUAGE = 38;
    private static final int ID_DURATION = 39;
    private static final int ID_OFFER = 40;
    private static final int ID_PERIOD = 41;
    private static final int ID_VALIDITY_PERIOD = 42;
    private static final int ID_PERSON = 43;
    private static final int ID_PLATFORM_SERVICE = 44;
    private static final int ID_PLATFORM_SERVICE_SPECIFIC = 45;
    private static final int ID_PLATFORM_VALIDITY = 46;
    private static final int ID_VERSIONS = 47;
    private static final int ID_VIDEO_PLAYOUT = 48;
    private static final int ID_PREVIEW_PLAYOUT = 49;
    private static final int ID_POSTER = 50;
    private static final int ID_PRODUCTION_COUNTRY = 51;
    private static final int ID_SEASON = 52;
    private static final int ID_SEASON_CONTAINER = 53;
    private static final int ID_SERIES = 54;
    private static final int ID_SERIES_CONTAINER = 55;
    private static final int ID_SERVICE = 56;
    private static final int ID_SHOWCASE = 57;
    private static final int ID_SHOWCASE_CONTAINER = 58;
    private static final int ID_ACTOR = 59;
    private static final int ID_DIRECTOR = 60;
    private static final int ID_PRODUCER = 61;
    private static final int ID_WRITER = 62;
    private static final int ID_TREE_ROOT = 63;
    private static final int ID_TOPIC = 64;
    private static final int ID_TOPIC_POINTER = 65;
    private static final int ID_VCDS = 66;
    private static final int ID_VIDEO = 67;
    private static final int ID_VIDEO_CONTAINER = 68;
    private static final int ID_GENRE = 69;
    private static final int ID_VIDEO_TYPE = 70;
    private static final int ID_SAMPLES = 71;
    private static final int ID_SECTION = 72;
    
    //R5
    private static final int ID_EXTERNAL_REF = 73;
    private static final int ID_LONG_TITLE = 74;

    private static final int ID_DUPNAMED_DESCRIPTION = 100;
    private static final int ID_DUPNAMED_CONTENT_DESCRIPTION = 101;
    private static final int ID_DUPNAMED_SHOWCASE_DETAILS = 102;
    private static final int ID_DUPNAMED_LANGUAGE_CONTENT = 103;

    // R7.3
	private static final int ID_MPLEX_CHANNELS = 110;
	private static final int ID_WRAPPED_TOPIC_POINTER = 120;
	private static final int ID_FAVORITE = 121;
	private static final int ID_SUBSCRIBED = 122;
	private static final int ID_UNSUBSCRIBED = 123;



	/**
	 * Set of elements have duplicated name.
	 */
	private static HashSet dupElementName = new HashSet();
	private static Hashtable dupNamedElementToID = new Hashtable();
	static {
		// Showcase, Bundle, Video, Episode
		dupElementName.add(Definition.EL_DETAIL);
		// Category, Showcase, Bundle, Video, Episode, Season, Series, Service
		dupElementName.add(Definition.EL_DESCRIPTION);
		// xxxContainer, items, platformService
		dupElementName.add(Definition.EL_PLATFORM_SERVICE);

        dupNamedElementToID.put("com.videotron.tvi.illico.vod.data.vcds.type.Description", new Integer(ID_DUPNAMED_DESCRIPTION) );
        dupNamedElementToID.put("com.videotron.tvi.illico.vod.data.vcds.type.ContentDescription", new Integer(ID_DUPNAMED_CONTENT_DESCRIPTION) );
        dupNamedElementToID.put("com.videotron.tvi.illico.vod.data.vcds.type.ShowcaseDetails", new Integer(ID_DUPNAMED_SHOWCASE_DETAILS) );
        dupNamedElementToID.put("com.videotron.tvi.illico.vod.data.vcds.type.LanguageContent", new Integer(ID_DUPNAMED_LANGUAGE_CONTENT) );
	}


	/**
	 * Class definitions.
	 */
	private static Hashtable elementToClass = new Hashtable();
	private static Hashtable elementToID = new Hashtable();
	static {
		elementToClass.put(Definition.EL_AVAILABLE_SOURCE, AvailableSource.class);
		elementToClass.put(Definition.EL_AVAILABLE_SOURCES, AvailableSources.class);
		elementToClass.put(Definition.EL_BUNDLE, Bundle.class);
		elementToClass.put(Definition.EL_BUNDLE_CONTAINER, BundleContainer.class);
		elementToClass.put(Definition.EL_BUNDLE_DETAIL, BundleDetails.class);
		elementToClass.put(Definition.EL_EXTRA, Extra.class);
		elementToClass.put(Definition.EL_EXTRA_CONTAINER, ExtraContainer.class);
		elementToClass.put(Definition.EL_EXTRA_DETAIL, ExtraDetails.class);
		elementToClass.put(Definition.EL_CATALOGUE, Catalog.class);
		elementToClass.put(Definition.EL_CATEGORIZED_BUNDLE, CategorizedBundle.class);
		elementToClass.put(Definition.EL_CATEGORIZED_BUNDLE_CONTAINER, CategoryContainer.class);
		elementToClass.put(Definition.EL_CATEGORY, Category.class);
		elementToClass.put(Definition.EL_CATEGORY_CONTAINER, CategoryContainer.class);
		elementToClass.put(Definition.EL_CHANNEL, Channel.class);
		elementToClass.put(Definition.EL_NETWORK, Network.class);
		elementToClass.put(Definition.EL_CLASSIFICATION, Classification.class);
		elementToClass.put(Definition.EL_CONTAINER_POINTER, ContainerPointer.class);
		elementToClass.put(Definition.EL_CONTENT_ROOT, ContentRoot.class);
		elementToClass.put(Definition.EL_CONTENT_DESCRIPTION, ContentDescription.class);
		elementToClass.put(Definition.EL_COUNTRY, Country.class);
		elementToClass.put(Definition.EL_EPISODE, Episode.class);
		elementToClass.put(Definition.EL_EPISODE_CONTAINER, EpisodeContainer.class);
		elementToClass.put(Definition.EL_ERROR, Error.class);
		elementToClass.put(Definition.EL_BACKGROUND, Image.class);
		elementToClass.put(Definition.EL_TITLE, LanguageContent.class);
		elementToClass.put(Definition.EL_BRIEF_TITLE, LanguageContent.class);
		elementToClass.put(Definition.EL_SUBSCRIPTION_INFO, LanguageContent.class);
		elementToClass.put(Definition.EL_IMAGE, Image.class);
		elementToClass.put(Definition.EL_IMAGE_GROUP, ImageGroup.class);
		elementToClass.put(Definition.EL_RESIZABLE_IMAGES, ResizableImages.class);
		elementToClass.put(Definition.EL_INFORMATIVE_TITLE, LanguageContent.class);
		elementToClass.put(Definition.EL_ORIGINAL_TITLE, LanguageContent.class);
		elementToClass.put(Definition.EL_NAME, LanguageContent.class);
		elementToClass.put(Definition.EL_LONG_SYNOPSIS, LanguageContent.class);
		elementToClass.put(Definition.EL_MEDIUM_SYNOPSIS, LanguageContent.class);
		elementToClass.put(Definition.EL_SHORT_SYNOPSIS, LanguageContent.class);
		elementToClass.put(Definition.EL_LENGTH, LanguageContent.class);
		elementToClass.put(Definition.EL_ORIGINAL_LANGUAGE, LanguageContent.class);
		elementToClass.put(Definition.EL_DURATION, LanguageContent.class);
		elementToClass.put(Definition.EL_OFFER, Offer.class);
		elementToClass.put(Definition.EL_PERIOD, Period.class);
		elementToClass.put(Definition.EL_VALIDITY_PERIOD, Period.class);
		elementToClass.put(Definition.EL_PERSON, Person.class);
		elementToClass.put(Definition.EL_PLATFORM_SERVICE, PlatformService.class);
		elementToClass.put(Definition.EL_PLATFORM_SERVICE_SPECIFIC, PlatformServiceSpecific.class);
		elementToClass.put(Definition.EL_PLATFORM_VALIDITY, PlatformValidity.class);
		elementToClass.put(Definition.EL_VERSIONS, Versions.class);
		elementToClass.put(Definition.EL_VIDEO_PLAYOUT, Playout.class);
		elementToClass.put(Definition.EL_PREVIEW_PLAYOUT, Playout.class);
		elementToClass.put(Definition.EL_POSTER, Poster.class);
		elementToClass.put(Definition.EL_PRODUCTION_COUNTRY, ProductionCountry.class);
		elementToClass.put(Definition.EL_SEASON, Season.class);
		elementToClass.put(Definition.EL_SEASON_CONTAINER, SeasonContainer.class);
		elementToClass.put(Definition.EL_SERIES, Series.class);
		elementToClass.put(Definition.EL_SERIES_CONTAINER, SeriesContainer.class);
		elementToClass.put(Definition.EL_SERVICE, Service.class);
		elementToClass.put(Definition.EL_SHOWCASE, Showcase.class);
		elementToClass.put(Definition.EL_SHOWCASE_CONTAINER, ShowcaseContainer.class);
		elementToClass.put(Definition.EL_ACTOR, StaffRef.class);
		elementToClass.put(Definition.EL_DIRECTOR, StaffRef.class);
		elementToClass.put(Definition.EL_PRODUCER, StaffRef.class);
		elementToClass.put(Definition.EL_WRITER, StaffRef.class);
		elementToClass.put(Definition.EL_TREE_ROOT, CategoryContainer.class);//Tree.class);
		elementToClass.put(Definition.EL_TOPIC, Topic.class);
		elementToClass.put(Definition.EL_TOPIC_POINTER, TopicPointer.class);
		elementToClass.put(Definition.EL_VCDS, Vcds.class);
		elementToClass.put(Definition.EL_VIDEO, Video.class);
		elementToClass.put(Definition.EL_VIDEO_CONTAINER, VideoContainer.class);
		elementToClass.put(Definition.EL_GENRE, VideoGenreCode.class);
		elementToClass.put(Definition.EL_VIDEO_TYPE, VideoType.class);
		elementToClass.put(Definition.EL_SAMPLES, Sample.class);
		elementToClass.put(Definition.EL_SECTION, Section.class);
		// R5
		elementToClass.put(Definition.EL_EXTERNAL_REF, ExternalRef.class);
		elementToClass.put(Definition.EL_LONG_TITLE, LanguageContent.class);

		// R7.3
		elementToClass.put(Definition.EL_MPLEX_CHANNELS, MPlexChannels.class);
		elementToClass.put(Definition.EL_WRAPPED_TOPIC_POINTER, WrappedTopicPointer.class);
        elementToClass.put(Definition.EL_FAVORITES, Favourites.class);
		elementToClass.put(Definition.EL_SUBSCRIBED, Subscribed.class);
		elementToClass.put(Definition.EL_UNSUBSCRIBED, Unsubscribed.class);

        elementToID.put(Definition.EL_AVAILABLE_SOURCE, new Integer(ID_AVAILABLE_SOURCE));
        elementToID.put(Definition.EL_AVAILABLE_SOURCES, new Integer(ID_AVAILABLE_SOURCES));
        elementToID.put(Definition.EL_BUNDLE, new Integer(ID_BUNDLE));
        elementToID.put(Definition.EL_BUNDLE_CONTAINER, new Integer(ID_BUNDLE_CONTAINER));
        elementToID.put(Definition.EL_BUNDLE_DETAIL, new Integer(ID_BUNDLE_DETAIL));
        elementToID.put(Definition.EL_EXTRA, new Integer(ID_EXTRA));
        elementToID.put(Definition.EL_EXTRA_CONTAINER, new Integer(ID_EXTRA_CONTAINER));
        elementToID.put(Definition.EL_EXTRA_DETAIL, new Integer(ID_EXTRA_DETAIL));
        elementToID.put(Definition.EL_CATALOGUE, new Integer(ID_CATALOGUE));
        elementToID.put(Definition.EL_CATEGORIZED_BUNDLE, new Integer(ID_CATEGORIZED_BUNDLE));
        elementToID.put(Definition.EL_CATEGORIZED_BUNDLE_CONTAINER, new Integer(ID_CATEGORIZED_BUNDLE_CONTAINER));
        elementToID.put(Definition.EL_CATEGORY, new Integer(ID_CATEGORY));
        elementToID.put(Definition.EL_CATEGORY_CONTAINER, new Integer(ID_CATEGORY_CONTAINER));
        elementToID.put(Definition.EL_CHANNEL, new Integer(ID_CHANNEL));
        elementToID.put(Definition.EL_NETWORK, new Integer(ID_NETWORK));
        elementToID.put(Definition.EL_CLASSIFICATION, new Integer(ID_CLASSIFICATION));
        elementToID.put(Definition.EL_CONTAINER_POINTER, new Integer(ID_CONTAINER_POINTER));
        elementToID.put(Definition.EL_CONTENT_ROOT, new Integer(ID_CONTENT_ROOT));
        elementToID.put(Definition.EL_CONTENT_DESCRIPTION, new Integer(ID_CONTENT_DESCRIPTION));
        elementToID.put(Definition.EL_COUNTRY, new Integer(ID_COUNTRY));
        elementToID.put(Definition.EL_EPISODE, new Integer(ID_EPISODE));
        elementToID.put(Definition.EL_EPISODE_CONTAINER, new Integer(ID_EPISODE_CONTAINER));
        elementToID.put(Definition.EL_ERROR, new Integer(ID_ERROR));
        elementToID.put(Definition.EL_BACKGROUND, new Integer(ID_BACKGROUND));
        elementToID.put(Definition.EL_TITLE, new Integer(ID_TITLE));
        elementToID.put(Definition.EL_BRIEF_TITLE, new Integer(ID_BRIEF_TITLE));
        elementToID.put(Definition.EL_SUBSCRIPTION_INFO, new Integer(ID_SUBSCRIPTION_INFO));
        elementToID.put(Definition.EL_IMAGE, new Integer(ID_IMAGE));
        elementToID.put(Definition.EL_IMAGE_GROUP, new Integer(ID_IMAGE_GROUP));
        elementToID.put(Definition.EL_RESIZABLE_IMAGES, new Integer(ID_RESIZABLE_IMAGES));
        elementToID.put(Definition.EL_INFORMATIVE_TITLE, new Integer(ID_INFORMATIVE_TITLE));
        elementToID.put(Definition.EL_ORIGINAL_TITLE, new Integer(ID_ORIGINAL_TITLE));
        elementToID.put(Definition.EL_NAME, new Integer(ID_NAME));
        elementToID.put(Definition.EL_LONG_SYNOPSIS, new Integer(ID_LONG_SYNOPSIS));
        elementToID.put(Definition.EL_MEDIUM_SYNOPSIS, new Integer(ID_MEDIUM_SYNOPSIS));
        elementToID.put(Definition.EL_SHORT_SYNOPSIS, new Integer(ID_SHORT_SYNOPSIS));
        elementToID.put(Definition.EL_LENGTH, new Integer(ID_LENGTH));
        elementToID.put(Definition.EL_ORIGINAL_LANGUAGE, new Integer(ID_ORIGINAL_LANGUAGE));
        elementToID.put(Definition.EL_DURATION, new Integer(ID_DURATION));
        elementToID.put(Definition.EL_OFFER, new Integer(ID_OFFER));
        elementToID.put(Definition.EL_PERIOD, new Integer(ID_PERIOD));
        elementToID.put(Definition.EL_VALIDITY_PERIOD, new Integer(ID_VALIDITY_PERIOD));
        elementToID.put(Definition.EL_PERSON, new Integer(ID_PERSON));
        elementToID.put(Definition.EL_PLATFORM_SERVICE, new Integer(ID_PLATFORM_SERVICE));
        elementToID.put(Definition.EL_PLATFORM_SERVICE_SPECIFIC, new Integer(ID_PLATFORM_SERVICE_SPECIFIC));
        elementToID.put(Definition.EL_PLATFORM_VALIDITY, new Integer(ID_PLATFORM_VALIDITY));
        elementToID.put(Definition.EL_VERSIONS, new Integer(ID_VERSIONS));
        elementToID.put(Definition.EL_VIDEO_PLAYOUT, new Integer(ID_VIDEO_PLAYOUT));
        elementToID.put(Definition.EL_PREVIEW_PLAYOUT, new Integer(ID_PREVIEW_PLAYOUT));
        elementToID.put(Definition.EL_POSTER, new Integer(ID_POSTER));
        elementToID.put(Definition.EL_PRODUCTION_COUNTRY, new Integer(ID_PRODUCTION_COUNTRY));
        elementToID.put(Definition.EL_SEASON, new Integer(ID_SEASON));
        elementToID.put(Definition.EL_SEASON_CONTAINER, new Integer(ID_SEASON_CONTAINER));
        elementToID.put(Definition.EL_SERIES, new Integer(ID_SERIES));
        elementToID.put(Definition.EL_SERIES_CONTAINER, new Integer(ID_SERIES_CONTAINER));
        elementToID.put(Definition.EL_SERVICE, new Integer(ID_SERVICE));
        elementToID.put(Definition.EL_SHOWCASE, new Integer(ID_SHOWCASE));
        elementToID.put(Definition.EL_SHOWCASE_CONTAINER, new Integer(ID_SHOWCASE_CONTAINER));
        elementToID.put(Definition.EL_ACTOR, new Integer(ID_ACTOR));
        elementToID.put(Definition.EL_DIRECTOR, new Integer(ID_DIRECTOR));
        elementToID.put(Definition.EL_PRODUCER, new Integer(ID_PRODUCER));
        elementToID.put(Definition.EL_WRITER, new Integer(ID_WRITER));
        elementToID.put(Definition.EL_TREE_ROOT, new Integer(ID_TREE_ROOT));
        elementToID.put(Definition.EL_TOPIC, new Integer(ID_TOPIC));
        elementToID.put(Definition.EL_TOPIC_POINTER, new Integer(ID_TOPIC_POINTER));
        elementToID.put(Definition.EL_VCDS, new Integer(ID_VCDS));
        elementToID.put(Definition.EL_VIDEO, new Integer(ID_VIDEO));
        elementToID.put(Definition.EL_VIDEO_CONTAINER, new Integer(ID_VIDEO_CONTAINER));
        elementToID.put(Definition.EL_GENRE, new Integer(ID_GENRE));
        elementToID.put(Definition.EL_VIDEO_TYPE, new Integer(ID_VIDEO_TYPE));
        elementToID.put(Definition.EL_SAMPLES, new Integer(ID_SAMPLES));
        elementToID.put(Definition.EL_SECTION, new Integer(ID_SECTION));
        // R5
        elementToID.put(Definition.EL_EXTERNAL_REF, new Integer(ID_EXTERNAL_REF));
        elementToID.put(Definition.EL_LONG_TITLE, new Integer(ID_LONG_TITLE));
        // R7.3
		elementToID.put(Definition.EL_MPLEX_CHANNELS, new Integer(ID_MPLEX_CHANNELS));
		elementToID.put(Definition.EL_WRAPPED_TOPIC_POINTER, new Integer(ID_WRAPPED_TOPIC_POINTER));
        elementToID.put(Definition.EL_FAVORITES, new Integer(ID_FAVORITE));
		elementToID.put(Definition.EL_SUBSCRIBED, new Integer(ID_SUBSCRIBED));
		elementToID.put(Definition.EL_UNSUBSCRIBED, new Integer(ID_UNSUBSCRIBED));
	}

	/**
	 * Constructor.
	 */
	public VCDSHandler() {
		stack = new Stack();
	}

	/**
	 * Field for estimate.
	 */
	private long timeStamp;

	/**
	 * Field for debug info.
	 */
	public String elapsed = "";

	/**
	 * Set flag to true.
	 */
	public void setRegisterBySspID() {
		registerBySspId = true;
	}

	/**
	 * Set flag to false;
	 */
	public void setNoReset() {
		needReset = false;
	}

	public void setLowPriority() {
		lowPriority = true;
	}

	public void setNoShowError() {
		noShowError = true;
	}

	/**
	 * Set flag to true;
	 */
	public void stopParsing() {
		stopParsing = true;
	}

	public long getLastParsed() {
		return lastPasred;
	}

	/**
	 * Method for begin parsing.
	 */
	public void startDocument() {
		timeStamp = System.currentTimeMillis();
	}

	/**
	 * Method for finish parsing.
	 */
	public void endDocument() {
		elapsed = String.valueOf(System.currentTimeMillis() - timeStamp);
		Log.printDebug("VCDSHandler, parsing done, elapsed : "
				+ elapsed);
	}

	/**
	 * Method for begin an element.
	 */
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if (stopParsing) {
			throw CatalogueDatabase.STOP;
		}
		lastPasred = System.currentTimeMillis();

		BaseElement instance = getInstance(qName, attributes);
		if (instance != null) {
			instance.setAttribute(attributes);
			if (instance instanceof CacheElement) {
				((CacheElement) instance).register();
			}
			stack.push(instance);
			lastElement = null;

			if (instance instanceof Catalog) {
				catalog = (Catalog) instance;
			} else if (instance instanceof Vcds) {
				rootVcds = (Vcds) instance;
			}
		} else { // It's primitive or simple type element
			lastElement = qName;
			((BaseElement) stack.peek()).setChildAttribute(qName, attributes);
		}
	}

	/**
	 * Method for finish an element.
	 */
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (stopParsing) {
			throw CatalogueDatabase.STOP;
		}
		
		lastPasred = System.currentTimeMillis();
		elementCnt++;
//		if (elementCnt % 50 == 0) {
//			Log.printDebug("VCDS/ parsed element = " + elementCnt);
//		}
		elementNames.add(qName);
		if (lastElement == null) {
			BaseElement instance = (BaseElement) stack.pop();
			if (instance instanceof CacheElement) {
				((CacheElement) instance).register();
				if (registerBySspId) {
					if (instance instanceof Video) {
						Video v = (Video) instance;
						for (int i = 0; i < v.detail.length; i++) {
							CatalogueDatabase.getInstance().registerBySspId(
									v.detail[i].orderableItem, instance);
						}
					} else if (instance instanceof Bundle) {
						Bundle b = (Bundle) instance;
						for (int i = 0; i < b.detail.length; i++) {
							CatalogueDatabase.getInstance().registerBySspId(
									b.detail[i].orderableItem, instance);
						}
					} else if (instance instanceof CategorizedBundle) {
						CategorizedBundle cb = (CategorizedBundle) instance;
						for (int i = 0; i < cb.detail.length; i++) {
							CatalogueDatabase.getInstance().registerBySspId(
									cb.detail[i].orderableItem, instance);
						}
					}
				}
				if (lowPriority) {
					Thread.yield();
				}
			}
			if (instance instanceof Versions) {
				CatalogueDatabase.getInstance().setVersion((Versions) instance);
			}
			if (instance instanceof Vcds) { // R2
				rootVcds.addToolsCategory();
			} else if (instance instanceof Error) {
				CatalogueDatabase.getInstance().setLastError((Error) instance, noShowError);
			} else if (!stack.empty()) {
				((BaseElement) stack.peek()).endElement(qName, instance);

				if (instance instanceof CategoryContainer) {
					CategoryContainer cat = (CategoryContainer) instance;
					
//					if (Log.DEBUG_ON) {
//						Log.printDebug("VCDSHandler, endElement, ca=" + cat);
//					}
//					// R5
//					if (cat.type == MenuController.MENU_CHANNEL || cat.type == MenuController.MENU_CHANNEL_ENV) {
//						CatalogueDatabase.getInstance().setChannelContainer(cat);
//					} else {
//						CatalogueDatabase.getInstance().setChannelContainer(null);
//					}
					
					if (cat.isAdult // DDC 76.4
							&& cat.categoryContainer.length > 0
							&& cat.categoryContainer[cat.categoryContainer.length - 1].type != MenuController.MENU_RESUME_ADULT) {

						File categoryList = (File) DataCenter.getInstance().get("meta_tools_category");
						String[] categoriesStr = TextReader.read(categoryList);
				        CategoryContainer container = null;
				        String[] parsedStr = null;
				        for (int j = 0; j < categoriesStr.length; j++) {
				            parsedStr = TextUtil.tokenize(categoriesStr[j], "|");
				            if (parsedStr[2].equals("resume18") == false && parsedStr[2].equals("wishlist18") == false) {
				            	continue;
				            }

				            container = new CategoryContainer();
				            container.id = parsedStr[0];
				            if (parsedStr[2].equals("wishlist18")) {
				            	container.type = MenuController.MENU_WISHLIST_ADULT;
				            } else {
				            	container.type = MenuController.MENU_RESUME_ADULT;
				            }
			            	container.isAdult = true; // make sure to show MainToolsUI
				            container.category = new Category();
				            container.category.description = new Description();
				            container.category.description.title[0] = new LanguageContent(Definition.LANGUAGE_EN, parsedStr[4]);
				            container.category.description.title[1] = new LanguageContent(Definition.LANGUAGE_FR, parsedStr[5]);

				            cat.endElement(qName, container);
				        }
					} 
					
//					if (cat.isAdult // R5 - 18 WishList
//							&& cat.categoryContainer.length > 0
//							&& cat.categoryContainer[cat.categoryContainer.length - 1].type != MenuController.MENU_WISHLIST_ADULT) {
//
//						File categoryList = (File) DataCenter.getInstance().get("meta_tools_category");
//						String[] categoriesStr = TextReader.read(categoryList);
//				        CategoryContainer container = null;
//				        String[] parsedStr = null;
//				        for (int j = 0; j < categoriesStr.length; j++) {
//				            parsedStr = TextUtil.tokenize(categoriesStr[j], "|");
//				            if (parsedStr[2].equals("wishlist18") == false) {
//				            	continue;
//				            }
//
//				            container = new CategoryContainer();
//				            container.type = MenuController.MENU_WISHLIST_ADULT;
//			            	container.isAdult = true; // make sure to show MainToolsUI
//				            container.category = new Category();
//				            container.category.description = new Description();
//				            container.category.description.title[0] = new LanguageContent(Definition.LANGUAGE_EN, parsedStr[4]);
//				            container.category.description.title[1] = new LanguageContent(Definition.LANGUAGE_FR, parsedStr[5]);
//
//				            cat.endElement(qName, container);
//				        }
//					}
				} else if (instance instanceof Topic) { // R5
					if (Log.DEBUG_ON) {
						Log.printDebug("VCDSHandler, endElement, Topic=" + instance);
					}
					Topic topic = (Topic) instance;
					
					if ("Chaîne".equals(topic.type) || "Chaine".equals(topic.type)) {
						CatalogueDatabase.getInstance().setChannelTopic(topic);
					} else if ("Groupe de Chaînes".equals(topic.type) || "Groupe de Chaines".equals(topic.type)) {
						CatalogueDatabase.getInstance().setChannelTopic(topic);
					} else {
						CatalogueDatabase.getInstance().setChannelTopic(null);
					}
				}
			}
		} else {
			lastElement = null;
		}
	}

	/**
	 * Method for CDATA.
	 */
	public void characters(char ch[], int start, int length)
			throws SAXException {
		if (stopParsing) {
			throw CatalogueDatabase.STOP;
		}
		if (ch[start] == '\n') {
			return;
		}

		String data = new String(ch, start, length);
		if (lastElement != null) {
			((BaseElement) stack.peek()).setDataAt(lastElement, data);
		} else {
			((BaseElement) stack.peek()).setData(data);
		}
	}

	/**
	 * Returns root element.
	 *
	 * @return root element
	 */
	public Catalog getCatalog() {
		return catalog;
	}

	/**
	 * Returns root element.
	 *
	 * @return root element
	 */
	public Vcds getRootVcds() {
		return rootVcds;
	}

	/**
	 * Checks appearance of elements.
	 * @param mandatory elements names to check
	 * @return true if all of names are appeared
	 */
	public boolean checkMandatories(String[] mandatory) {
		for (int i = 0; i < mandatory.length; i++) {
			if (elementNames.contains(mandatory[i]) == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns appropriate instance for element.
	 *
	 * @param qName
	 *            element's name
	 * @param attributes
	 *            element's attributes
	 * @return instance fresh or cached
	 */
	private BaseElement getInstance(String qName, Attributes attributes) {
		String id = attributes.getValue("id");
		if (id != null) {
			BaseElement e = (BaseElement) CatalogueDatabase.getInstance().getCached(id);
			if (e != null && needReset) {
				e.reset();
				return e;
			}
		}

		Class cls = null;
        int index = 0;
		if (dupElementName.contains(qName)) {
			cls = ((HasDupNamed) stack.peek()).getClass(qName, attributes);
            Integer integer = (Integer) dupNamedElementToID.get(cls.getName());
            if (integer != null) {
                index = integer.intValue();
            }
		} else {
			cls = (Class) elementToClass.get(qName);
            Integer integer = (Integer) elementToID.get(qName);
            if (integer != null) {
                index = integer.intValue();
            }
		}
        if (index != 0) {
            BaseElement be = newInstance(index);
            if (be != null) {
                return be;
            }
        }
		if (cls != null) {
			try {
				return (BaseElement) cls.newInstance();
			} catch (Exception e) {
			}
		}
		return null;
	}

    private BaseElement newInstance(int id) {
        switch (id) {
            case ID_AVAILABLE_SOURCE: return new AvailableSource();
            case ID_AVAILABLE_SOURCES: return new AvailableSources();
            case ID_BUNDLE: return new Bundle();
            case ID_BUNDLE_CONTAINER: return new BundleContainer();
            case ID_BUNDLE_DETAIL: return new BundleDetails();
            case ID_EXTRA: return new Extra();
            case ID_EXTRA_CONTAINER: return new ExtraContainer();
            case ID_EXTRA_DETAIL: return new ExtraDetails();
            case ID_CATALOGUE: return new Catalog();
            case ID_CATEGORIZED_BUNDLE: return new CategorizedBundle();
            case ID_CATEGORIZED_BUNDLE_CONTAINER: return new CategoryContainer();
            case ID_CATEGORY: return new Category();
            case ID_CATEGORY_CONTAINER: return new CategoryContainer();
            case ID_CHANNEL: return new Channel();
            case ID_NETWORK: return new Network();
            case ID_CLASSIFICATION: return new Classification();
            case ID_CONTAINER_POINTER: return new ContainerPointer();
            case ID_CONTENT_ROOT: return new ContentRoot();
            case ID_CONTENT_DESCRIPTION: return new ContentDescription();
            case ID_COUNTRY: return new Country();
            case ID_EPISODE: return new Episode();
            case ID_EPISODE_CONTAINER: return new EpisodeContainer();
            case ID_ERROR: return new Error();
            case ID_BACKGROUND: return new Image();
            case ID_TITLE: return new LanguageContent();
            case ID_BRIEF_TITLE: return new LanguageContent();
            case ID_SUBSCRIPTION_INFO: return new LanguageContent();
            case ID_IMAGE: return new Image();
            case ID_IMAGE_GROUP: return new ImageGroup();
            case ID_RESIZABLE_IMAGES: return new ResizableImages();
            case ID_INFORMATIVE_TITLE: return new LanguageContent();
            case ID_ORIGINAL_TITLE: return new LanguageContent();
            case ID_NAME: return new LanguageContent();
            case ID_LONG_SYNOPSIS: return new LanguageContent();
            case ID_MEDIUM_SYNOPSIS: return new LanguageContent();
            case ID_SHORT_SYNOPSIS: return new LanguageContent();
            case ID_LENGTH: return new LanguageContent();
            case ID_ORIGINAL_LANGUAGE: return new LanguageContent();
            case ID_DURATION: return new LanguageContent();
            case ID_OFFER: return new Offer();
            case ID_PERIOD: return new Period();
            case ID_VALIDITY_PERIOD: return new Period();
            case ID_PERSON: return new Person();
            case ID_PLATFORM_SERVICE: return new PlatformService();
            case ID_PLATFORM_SERVICE_SPECIFIC: return new PlatformServiceSpecific();
            case ID_PLATFORM_VALIDITY: return new PlatformValidity();
            case ID_VERSIONS: return new Versions();
            case ID_VIDEO_PLAYOUT: return new Playout();
            case ID_PREVIEW_PLAYOUT: return new Playout();
            case ID_POSTER: return new Poster();
            case ID_PRODUCTION_COUNTRY: return new ProductionCountry();
            case ID_SEASON: return new Season();
            case ID_SEASON_CONTAINER: return new SeasonContainer();
            case ID_SERIES: return new Series();
            case ID_SERIES_CONTAINER: return new SeriesContainer();
            case ID_SERVICE: return new Service();
            case ID_SHOWCASE: return new Showcase();
            case ID_SHOWCASE_CONTAINER: return new ShowcaseContainer();
            case ID_ACTOR: return new StaffRef();
            case ID_DIRECTOR: return new StaffRef();
            case ID_PRODUCER: return new StaffRef();
            case ID_WRITER: return new StaffRef();
            case ID_TREE_ROOT: return new CategoryContainer();//Tree();
            case ID_TOPIC: return new Topic();
            case ID_TOPIC_POINTER: return new TopicPointer();
            case ID_VCDS: return new Vcds();
            case ID_VIDEO: return new Video();
            case ID_VIDEO_CONTAINER: return new VideoContainer();
            case ID_GENRE: return new VideoGenreCode();
            case ID_VIDEO_TYPE: return new VideoType();
            case ID_SAMPLES: return new Sample();
            case ID_SECTION: return new Section();

            case ID_DUPNAMED_DESCRIPTION: return new Description();
            case ID_DUPNAMED_CONTENT_DESCRIPTION: return new ContentDescription();
            case ID_DUPNAMED_SHOWCASE_DETAILS: return new ShowcaseDetails();
            case ID_DUPNAMED_LANGUAGE_CONTENT: return new LanguageContent();
            default: return null;
        }
    }
}

package com.videotron.tvi.illico.vod.data.vcds.type;


public class BundleContainer extends BaseContainer {
	public Bundle bundle; // or ref
	public String bundleRef;
	
	public String toString(boolean detail) {
		if (bundle == null) {
			return "bundle is " + bundleRef;
		}
		return bundle.toString(detail);
	}
	
	public void dispose() {
		super.dispose();
		if (bundle != null) bundle.dispose();
		bundle = null;
	}
}

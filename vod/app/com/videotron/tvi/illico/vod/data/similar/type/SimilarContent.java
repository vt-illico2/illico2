/**
 *
 */
package com.videotron.tvi.illico.vod.data.similar.type;

import java.awt.Image;
import java.util.Date;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.ui.BaseUI;

/**
 * This class represent Similar content
 *
 * @author zestyman
 */
public class SimilarContent extends BaseJson {

	private String vodId = TextUtil.EMPTY_STRING;
	private String seasonNo = TextUtil.EMPTY_STRING;

	private SType type;
	private SGenre[] genres;

	private String definitions = TextUtil.EMPTY_STRING;
	private String rating = TextUtil.EMPTY_STRING;
	private String title = TextUtil.EMPTY_STRING;
	private String originalTitle = TextUtil.EMPTY_STRING;
	private String contentImage = TextUtil.EMPTY_STRING;
	private String languages = TextUtil.EMPTY_STRING;
	private String channelCallLetter = TextUtil.EMPTY_STRING;
	private String channelName = TextUtil.EMPTY_STRING;
	private boolean channelFreePreviewIndicator;
	private boolean freeContentIndicator;
	private boolean payPerViewContentIndicator;
	private boolean clubContentIndicator;

	private Image poster;

	public String getVodId() {
		return vodId;
	}

	public String getSeasonNo() {
		return seasonNo;
	}

	public SType getType() {
		return type;
	}

	public SGenre[] getGenres() {
		return genres;
	}

	public String getDefinitions() {
		return definitions;
	}

	public String getRating() {
		return rating;
	}

	public String getTitle() {
		return title;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getContentImage() {
		return contentImage;
	}

	public String getLanguages() {
		return languages;
	}

	public String getChannelCallLetter() {
		return channelCallLetter;
	}

	public String getChannelName() {
		return channelName;
	}

	public boolean isChannelFreePreviewIndicator() {
		return channelFreePreviewIndicator;
	}

	public boolean isFreeContentIndicator() {
		return freeContentIndicator;
	}

	public boolean isPayPerViewContentIndicator() {
		return payPerViewContentIndicator;
	}

	public boolean isClubContentIndicator() {
		return clubContentIndicator;
	}

	public Image getPoster() {
		return poster;
	}

	public void retrieveImage(final BaseUI parent) {
		if (contentImage != null && contentImage != TextUtil.EMPTY_STRING) {
			new Thread("LOAD_SIMILAR_IMAGE_" + vodId) {
				public void run() {

					String urlStr = CatalogueDatabase.SDF.format(new Date()) + ", "
							+ DataCenter.getInstance().getString("IMAGE_URL") + ImageRetriever.BASE + contentImage;
					if (BaseRenderer.fmClock.stringWidth(urlStr) > 860) {
						String[] splitStr = TextUtil.split(urlStr, BaseRenderer.fmClock, 860);
						if (splitStr != null) {
							for (int i = splitStr.length - 1; i > -1; i--) {
								MenuController.getInstance().debugList[5].add(splitStr[i]);
							}
						}
					} else {
						MenuController.getInstance().debugList[5].add(urlStr);
					}

					poster = CatalogueDatabase.getInstance().getImage(contentImage);
					parent.repaint();
				}

				;
			}.start();
		}
	}

	public void parseJson(Object obj) throws Exception {
		JSONObject jObj = (JSONObject) obj;

		vodId = getString(jObj, "vodId");
		seasonNo = getString(jObj, "seasonNo");
		type = new SType();
		type.parseJson(jObj.get("type"));

		JSONArray genreArr = (JSONArray) jObj.get("genres");
		genres = new SGenre[genreArr.size()];
		for (int i = 0; i < genreArr.size(); i++) {
			genres[i] = new SGenre();
			genres[i].parseJson(genreArr.get(i));
		}

		definitions = getString(jObj, "definitions");
		rating = getString(jObj, "rating");
		title = getString(jObj, "title");
		originalTitle = getString(jObj, "originalTitle");
		contentImage = getString(jObj, "contentImageName");
		languages = getString(jObj, "languages");
		channelCallLetter = getString(jObj, "channelCallLetter");
		channelName = getString(jObj, "channelName");
		channelFreePreviewIndicator = getBoolean(jObj, "channelFreePreviewIndicator");
		freeContentIndicator = getBoolean(jObj, "freeContentIndicator");
		payPerViewContentIndicator = getBoolean(jObj, "payPreviewContentIndicator");
		clubContentIndicator = getBoolean(jObj, "clubContentIndicator");
	}
}

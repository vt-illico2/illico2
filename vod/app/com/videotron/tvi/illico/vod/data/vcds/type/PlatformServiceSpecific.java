package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;

public class PlatformServiceSpecific extends BaseElement implements HasDupNamed {
	public int position;
	public Period validityPeriod;
	public PlatformService platformService; // or ref
	public String platformServiceRef;
	
	public boolean isValid() {
		return true;
//		if (validityPeriod == null) return true;
//		long now = System.currentTimeMillis();
//		return validityPeriod.start.getTime() < now 
//			&& now < validityPeriod.end.getTime();
	}
	
	public void setAttribute(Attributes attributes) {
		try {
			position = Integer.parseInt(attributes.getValue(AT_POSITION));
		} catch (NumberFormatException nfe) {}
	}
	
	public Class getClass(String qName, Attributes attributes) {
		if (EL_PLATFORM_SERVICE.equals(qName)) {
			return PlatformService.class;
		}
		return null;
	}
	
	public void endElement(String qName, BaseElement element) {
		if (EL_VALIDITY_PERIOD.equals(qName)) {
			validityPeriod = (Period) element;
		} else if (EL_PLATFORM_SERVICE.equals(qName)) {
			platformService = (PlatformService) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_PLATFORM_SERVICE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				platformService = (PlatformService) cached;
			} else {
				platformServiceRef = data;
			}
		}
	}

	public void dispose() {
		if (validityPeriod != null) validityPeriod.dispose();
		validityPeriod = null;
		if (platformService != null) platformService.dispose();
		platformService = null;
	}
}

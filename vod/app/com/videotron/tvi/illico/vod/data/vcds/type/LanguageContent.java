package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class LanguageContent extends BaseElement {
	public String language;
	public String data;
	
	public LanguageContent() {}
	
	public LanguageContent(String language, String data) {
		this.language = language;
		this.data = data;
	}
	
	public void setAttribute(Attributes attributes) {
		language = attributes.getValue(AT_LANGUAGE);
	}
	
	public void setData(String data) {
		this.data = data.trim();
	}

	public void dispose() {
		language = null;
		data = null;
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;

import org.xml.sax.Attributes;

public class Catalog extends BaseElement {
	public String version;
	public ContentRoot[] contentRoot = new ContentRoot[0];

	public void setAttribute(Attributes attributes) {
		version = attributes.getValue(AT_VERSION);
	}

	public void endElement(String qName, BaseElement element) {
		if (EL_CONTENT_ROOT.equals(qName)) {
			contentRoot = (ContentRoot[]) addArray(contentRoot, ContentRoot.class, element);
		}
	}

	public CategoryContainer[] getSubCategories() {
		for (int i = 0; i < contentRoot[0].treeRoot.categoryContainer.length; i++) {
			if (contentRoot[0].treeRoot.categoryContainer[i].getTitle().endsWith("IMOD")) {
				return contentRoot[0].treeRoot.categoryContainer[i].categoryContainer;
			}
		}
		return contentRoot[0].treeRoot.categoryContainer;
	}

	public void dispose() {
		version = null;
		for (int i = 0; contentRoot != null && i < contentRoot.length; i++) {
			if (contentRoot[i] != null) contentRoot[i].dispose();
			contentRoot[i] = null;
		}
	}
}

package com.videotron.tvi.illico.vod.data.vcds.type;



public class SeriesContainer extends BaseContainer {
	public Series series; // or ref
	public String seriesRef;
	
//	public void setDataAt(String qName, String data) {
//		if (EL_SERIES_ID.equals(qName)) {
//			Object cached = CatalogueDatabase.getInstance().getCached(data);
//			if (cached != null) {
//				series = (Series) cached;
//			} else {
//				//seriesRef = data;
//				series = new Series();
//				series.id = data;
//			}
//		}
//	}
	
	public String toString(boolean detail) {
		if (series == null) {
			return "series is " + seriesRef;
		}
		return series.toString(detail);
	}
	
	public String dumpContent(String prefix) {
		return series.dumpContent(prefix);
	}
	
	public void dispose() {
		super.dispose();
		if (series != null) series.dispose();
		series = null;
	}
}

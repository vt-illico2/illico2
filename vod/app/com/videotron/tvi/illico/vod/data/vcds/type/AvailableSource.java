package com.videotron.tvi.illico.vod.data.vcds.type;

import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;


public class AvailableSource extends MoreDetail {
	public String[] episodeIds = new String[0];
	public Channel channel;
	public Network network;
	public Service service;
	
	public Season parent;
	
	public String getTitle() { return parent.getTitle(); }
	public java.awt.Image getImage(int width, int height) { return parent.getImage(width, height); }
	public Object chooseImage() { return parent.chooseImage(); }
	public boolean isPortrait() { return parent.isPortrait(); }
	public String getDescription() { return parent.getDescription(); }
	
	public void endElement(String qName, BaseElement element) {
		if (EL_CHANNEL.equals(qName)) {
			channel = (Channel) element;
		} else if (EL_NETWORK.equals(qName)) {
			network = (Network) element;
		} else if (EL_SERVICE.equals(qName)) {
			service = (Service) element;
		}
	}
	
	public void setDataAt(String qName, String data) {
		if (EL_EPISODE_ID.equals(qName) || EL_EPISODE_REF.equals(qName)) {
			episodeIds = (String[]) addArray(episodeIds, String.class, data);
		} else if (EL_CHANNEL_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				channel = (Channel) cached;
			}
		} else if (EL_NETWORK_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				network = (Network) cached;
			}
		} else if (EL_SERVICE_REF.equals(qName)) {
			Object cached = CatalogueDatabase.getInstance().getCached(data);
			if (cached != null) {
				service = (Service) cached;
			}
		}
	}
	
	public boolean equals(Object o) {
		if (o instanceof AvailableSource == false) {
			return false;
		}
		AvailableSource target = (AvailableSource) o;
		if (episodeIds.length != target.episodeIds.length) {
			return false;
		}
		for (int i = 0; i < episodeIds.length; i++) {
			if (episodeIds[i].equals(target.episodeIds[i]) == false) {
				return false;
			}
		}
		if (channel != null && target.channel != null && channel.id.equals(target.channel.id) == false) {
			return false;
		}
		if (network != null && target.network != null && network.id.equals(target.network.id) == false) {
			return false;
		}
		if (service != null && target.service != null && service.id.equals(target.service.id) == false) {
			return false;
		}
		return true;
	}

	public boolean hasPacakgeForServiceConfigType() {
		// VDTRMASTER-5885
		if (service != null && service.serviceType == Service.CONFIG) {
			return CommunicationManager.getInstance().checkPackage(service);
		}

		return true;
	}
	
	public String toString(boolean detail) {
		return "AvailableSource, parent = " + parent 
				+ ", ch = " + channel + ", nw = " + network 
				+ ", svc = " + (service != null ? service.toString(true) : "null") + ", ep = " + episodeIds.length;
	}
	
	public void dispose() {
		channel = null;
		network = null;
		service = null;
		
	}
}

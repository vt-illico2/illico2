package com.videotron.tvi.illico.vod.ui;

import java.awt.event.KeyEvent;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import com.videotron.tvi.illico.vod.comp.PopChangeView;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ActionBox;
import com.videotron.tvi.illico.vod.comp.ListComp;
import com.videotron.tvi.illico.vod.comp.SimilarContentView;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CacheElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.gui.ListViewRenderer;

public class ListViewUI extends BaseUI {
	public static final int FOCUS_LIST = 0;
	public static final int FOCUS_BUTTON = 1;

	private BaseElement[] catalogueList = new BaseElement[0];
	private BaseElement[] allList = null;
	private BaseElement[] backupList = null;
	private int titleIndex, backupIdx, backupFocus, backupTitleIdx;
	private int mode;
	private boolean adult; // DDC 76.4

	private ListViewRenderer listViewRenderer = new ListViewRenderer();
	private ListComp listComp = new ListComp();

	private ActionBox actionBox;
	private OptionScreen optionBox = new OptionScreen();

	private int[] buttonList = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_VIEW, BaseUI.BTN_PROFILE,
			BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	private int[] buttonListWishlist = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_PROFILE, BaseUI.BTN_OPTION };
	private int[] buttonListResume = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	
	private int[] buttonList2 = new int[] { BaseUI.BTN_VIEW, BaseUI.BTN_PROFILE,BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	private int[] buttonListWishlist2 = new int[] { BaseUI.BTN_PROFILE, BaseUI.BTN_OPTION };
	private int[] buttonListResume2 = new int[] { BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	
	// VDTRMASTER-5690
	private Object param;
	
	public ListViewUI(int mode) {
		//listComp.setBounds(6, 68);
		add(listComp);

		actionBox = ActionBox.newInstance();
		add(actionBox);

		this.mode = mode;

		add(footer);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);
	}
	
	public void setAdult(boolean adult) {
		Log.printDebug("ListViewUI, setAdult = " + adult);
		this.adult = adult;
	}

	public void start(boolean back) {
		if (!back) {
			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];
			
			// VDTRMASTER-5690
			this.param = obj;
			
			BaseElement[] data = null;
			BaseElement focused = null;
			// R5
			fromCategory = false;
			if (obj instanceof String) { // category
				Object cached = CatalogueDatabase.getInstance().getCached(obj);
				Log.printDebug("Carousel, cached = " + cached);
				if (cached instanceof CategoryContainer) {
					CategoryContainer cat = (CategoryContainer) cached;
					menu.setLastCategoryId(cat.id);
					data = cat.totalContents;
					
					String opt = menu.getSortNormal();
					data = CatalogueDatabase.getInstance().sort(data, opt == null ? Resources.OPT_NAME : opt);
					
					listComp.setListType(mode, getHeader());
					setData(data);
					// R5
					fromCategory = true;
				}
//				BaseRenderer.setBrandingBg(1, null);
				setBranding();
			} else {
				if (obj instanceof BaseElement[]) {
					data = (BaseElement[]) obj;
				} else if (obj instanceof Object[]) {
					data = (BaseElement[]) ((Object[]) obj)[0];
					focused = (BaseElement) ((Object[]) obj)[1];
				}
				//data = (BaseElement[]) param[MenuController.PARAM_DATA_IDX];
				String opt = null;
				switch (mode) {
				case ListComp.TYPE_NORMAL:
					listComp.setListType(mode, getHeader());
					setData(data);
//					BaseRenderer.setBrandingBg(1, null);
					setBranding();
					
					// VDTRMASTER-5690
					this.param = DataCenter.getInstance().get("VIEW_PARAM");
					
					break;
				case ListComp.TYPE_RESUME:
					listComp.setListType(mode, dataCenter.getString(menu.openAdultCategory() 
							? Resources.TEXT_T_RESUME_VIEWING_18
							: Resources.TEXT_T_RESUME_VIEWING));
					opt = menu.getSortResumeViewing();
					data = CatalogueDatabase.getInstance().sort(data,
							opt == null ? Resources.OPT_DATE : opt, true);
					for (int i = 0; i < data.length; i++) {
						if (data[i] instanceof Bundle) {
							((Bundle) data[i]).collapsed = true;
						}
					}
					allList = data;
					data = arrangeList(data);
					if (obj instanceof Object[] && ((Object[]) obj).length == 3) {
						setAdult(((Boolean) ((Object[]) obj)[2]).booleanValue());
					}
					setData(data);
					BaseRenderer.setBrandingBg(-1, null);
					break;
				case ListComp.TYPE_WISHLIST:
					adult = false;
					if (menu.isAdult()) {
						adult = true;
						
						// VDTRMASTER-5609
						BaseUI lastScene = menu.getLastScene();
						if (lastScene.getFooter() != null) {
							if (lastScene.getFooter().getParent() instanceof SimilarContentView) {
								BaseElement element = ((SimilarContentView)lastScene.getFooter().getParent()).getFocusedContent();
								if (element != null && element instanceof Orderable) {
									adult = BookmarkManager.getInstance().isAdult((Orderable)element);
								}
								
								// VDTRMASTER-5608
								if (((SimilarContentView)lastScene.getFooter().getParent()).hasChannelName()) {
									adult = false;
								}
							}
						}
					} else {
						BaseUI parent = menu.getParentUI();
						BaseUI lastScene = menu.getLastScene();
						if (menu.isFromSearch() && (parent instanceof TitleDetailUI || lastScene instanceof TitleDetailUI)) {
							Video title = null;
							
							if (parent instanceof TitleDetailUI) {
								title = ((TitleDetailUI) parent).getTitle();
							} else {
								title = ((TitleDetailUI) lastScene).getTitle();
							}
							
							if (CommunicationManager.getInstance().hasAnyPackage() && 
									CommunicationManager.getInstance().checkPackage(title.service)) {
								if (BookmarkManager.getInstance().isAdult(title)) {
									adult = true;
								}
							}
						} else { // VDTRMASTER-5608
							if (lastScene != null && lastScene.getFooter() != null) {
								if (lastScene.getFooter().getParent() instanceof SimilarContentView) {
									BaseElement element = ((SimilarContentView)lastScene.getFooter().getParent()).getFocusedContent();
									if (element != null && element instanceof Orderable) {
										adult = BookmarkManager.getInstance().isAdult((Orderable)element);
									}
									
									// VDTRMASTER-5608
									if (((SimilarContentView)lastScene.getFooter().getParent()).hasChannelName()) {
										adult = false;
									}
								} else if (lastScene instanceof TitleDetailUI) { // VDTRMASTER-5637
									BaseElement element = ((TitleDetailUI) lastScene).getTitle();
									
									if (element == null) {
										element = ((TitleDetailUI) lastScene).getBundle();
									}
									
									if (element != null && element instanceof Orderable) {
										adult = BookmarkManager.getInstance().isAdult((Orderable)element);
									}
								}
							}
						}
					}
					listComp.setListType(mode, dataCenter.getString(adult  
							? Resources.TEXT_T_MY_WISHLIST_18 
							: Resources.TEXT_T_MY_WISHLIST) + "(" + data.length + ")");
					opt = dataCenter.getString(PreferenceNames.WISHLIST_DISPLAY).equals(
							Definitions.WISHLIST_DISPLAY_ALPHABETICAL) ? Resources.OPT_NAME : Resources.OPT_DATE;
					menu.setSortWishlist(opt);
//					data = CatalogueDatabase.getInstance().sort(data, opt, true);
//					for (int i = 0; i < data.length; i++) {
//						if (data[i] instanceof Bundle) {
//							((Bundle) data[i]).collapsed = true;
//						}
//					}
					//data = arrangeList(data);
					setData(data);
					BaseRenderer.setBrandingBg(-1, null);
					break;
				}
			}
			setRenderer(listViewRenderer);
			prepare();
			setCurrentFocusUI(FOCUS_LIST);
			focus = 0;
			setFocus(0);
			listComp.start(back);
			if (mode == ListComp.TYPE_NORMAL) {
				if (param.length > MenuController.PARAM_DATA_IDX + 1) {
					focused = (BaseElement) param[MenuController.PARAM_DATA_IDX + 1];
					listComp.setFocused(focused);
					focus = listComp.getFocus();
					setFocus(listComp.getTitleIndex());
				}
			} else if (mode == ListComp.TYPE_RESUME && focused != null) {
				listComp.setFocused(focused);
				focus = listComp.getFocus();
				setFocus(listComp.getTitleIndex());
			}
		} else {
			if (mode != ListComp.TYPE_NORMAL) {
				if (mode == ListComp.TYPE_WISHLIST) {
					setData(null);
				} else if (mode == ListComp.TYPE_RESUME) {
					// remove expired
					setData(null);
				}
				BaseRenderer.setBrandingBg(-1, null);
			} else {

				// VDTRMASTER-5835
				if (MenuController.getInstance().getChageViewType() != PopChangeView.VIEW_LIST) {
					keyYellow();
					return;
				}

				// VDTRMASTER-5690
				if (Log.DEBUG_ON) {
					Log.printDebug("ListView, param=" + param);
				}
				// VDTRMASTER-5433
				if (param instanceof String) {
					CacheElement cached = CatalogueDatabase.getInstance().getCached(param);
					Log.printDebug("ListView, cached = " + cached);
					if (cached instanceof CategoryContainer) {
						
						boolean isApplied = false;
						CategoryContainer category = (CategoryContainer) cached;
						do {
							isApplied = CategoryUI.setBrandingElementByCategory(category);
							category = category.parent;
						} while (category != null && isApplied == false);
					}
				}
				
//				BaseRenderer.setBrandingBg(1, null);
				setBranding();
			}
			listComp.start(back);
		}
		updateButtons();
		super.start();

		if (ListComp.TYPE_NORMAL == mode && Resources.OPT_NAME.equals(menu.getSortNormal())) {
			menu.setNeedRapid(true);
		} else if (ListComp.TYPE_RESUME == mode && Resources.OPT_NAME.equals(menu.getSortResumeViewing())) {
			menu.setNeedRapid(true);
		} else if (ListComp.TYPE_WISHLIST == mode && Resources.OPT_NAME.equals(menu.getSortWishlist())) {
			menu.setNeedRapid(true);
		}
	}
	
	public boolean focusOnFirst() {
		return titleIndex == 0;
	}
	
	public boolean focusOnLast() {
		return titleIndex == catalogueList.length - 1;
	}
	
	private void setBranding() {
//		Channel channel = null;
//		ImageGroup bg = null;
		
		BaseRenderer.setBrandingBg(1, null);
		ImageGroup bg = null;
		if (CategoryUI.isBranded()) {
			BrandingElement br = CategoryUI.getBrandingElement();
			bg = br.getBackground(false);
			try {
				BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
			} catch (Exception e) {
				BaseRenderer.setBrandingBg(-1, null);
			}
		}
//		try {
//			channel = CategoryUI.getChannel();
//			if (channel.network != null) {
//				bg = channel.network.getBackground(false);
//			} else {
//				bg = channel.getBackground(false);
//			}
//			BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//		} catch (Exception e) {
//			try {
//				BrandingElement br = CategoryUI.getBrandingElement();
//				bg = br.getBackground(false);
//				BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//			} catch (Exception ee) { 
//				BaseRenderer.setBrandingBg(-1, null);
//			}
//		}
	}

	public void searchLetter(Object letter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("searchLetter, letter = " + letter);
		}
		if (catalogueList == null || catalogueList.length == 0) {
			return;
		}
		MoreDetail candidate = (MoreDetail) catalogueList[0];
		Collator c = Collator.getInstance(Locale.CANADA_FRENCH);
		for (int i = 1; i < catalogueList.length; i++) {
			if (c.compare(candidate.getTitle(), letter) > 0) {
				break;
			}
			candidate = (MoreDetail) catalogueList[i];
		}
		scroll = 0;
		listComp.setFocused(candidate);
		focus = listComp.getFocus();
		setFocus(listComp.getTitleIndex());
	}
	
	private String getHeader() {
		String mainTitle = MenuController.getInstance().getLastHistory();
    	BaseUI last = MenuController.getInstance().getLastScene();
		Channel channel = null;
		if (last != null 
				&& last instanceof CategoryUI
				&& CategoryUI.isBranded()
				&& (channel = CategoryUI.getChannel()) != null) {
			
			if (channel.network != null) {
				mainTitle = channel.getLanguageContentData(channel.network.title) + " ≥ " + mainTitle;
			} else {
				mainTitle = channel.getLanguageContentData(channel.title) + " ≥ " + mainTitle;
			}
		} 
		return mainTitle;
	}
	
	public boolean isNormal() {
		return mode == ListComp.TYPE_NORMAL;
	}

	public boolean isWishList() {
		return mode == ListComp.TYPE_WISHLIST;
	}

	public boolean isResumeViewing() {
		return mode == ListComp.TYPE_RESUME;
	}

	private void setActionButtons() {
		String[] actionButtons = getActionButton(getCurrentCatalogue());
		actionBox.setActionBox(actionButtons);
		actionBox.setFocus(focus);
	}
	
	private BaseElement[] arrangeList(BaseElement[] data) {
		// re-arrange bundles
		ArrayList bundle = new ArrayList();
		ArrayList others = new ArrayList();
		
		// VDTRMASTER-5723
		for (int i = 0; i < data.length; i++) {
			if (data[i] instanceof Bundle) {
				bundle.add(data[i]);
			}
			
			others.add(data[i]);
		}
		
		// remove a contents to be included in bundles.
		
		for (int i = 0; i < bundle.size(); i++) {
			Bundle bd = (Bundle) bundle.get(i);
			
			for (int j = 0; j < others.size(); j++) {
				BaseElement be = (BaseElement) others.get(j);
				
				if (be instanceof Bundle == false) {
					if (bd.collapsed && bd.hasContent(be)) {
						others.remove(j);
						j--;
					}
				}
			}
		}
		
		
		if (data.length != others.size()) {
			data = new BaseElement[others.size()];
		}
		others.toArray(data);
		return data;
	}
	
	public BaseElement[] getData() {
		return catalogueList;
	}
	
	public String findCurrentLabel() {
		return findPlayLabel(getCurrentCatalogue());
	}
	
	public String findLabel() {
		return findLabel(catalogueList);
	}
	
	public boolean canPlayAll() {
		// purchased bundle can play all...
		if (getCurrentCatalogue() instanceof Bundle) {
			return BookmarkManager.getInstance().isPurchased((Bundle) getCurrentCatalogue());
		}
		
		if (catalogueList == null || catalogueList.length < 2) {
			return false;
		}
		
		// all free or all purchased
		boolean canPlayAll = true;
		for (int i = 0; catalogueList != null && i < catalogueList.length; i++) {
			if (catalogueList[i] == null) {
				continue;
			}
			if (catalogueList[i] instanceof Video) {
//					if (((Video) catalogueList[i]).isFree() == false && // not a free 
//							BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]) == null) { // not purchased
//						canPlayAll = false;
//						break;
//					}
//					continue;
				
				if (Log.EXTRA_ON) {
					Log.printDebug("catalogueList[i]=" + catalogueList[i].toString(true));
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).getTypeID());
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).isFree());
					Log.printDebug("catalogueList[i] isPurchased=" + BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]));
				}
				
				if (catalogueList[i] instanceof Episode && (((Video) catalogueList[i]).isFree() || BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]) != null)) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_124")) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_104")) {
					continue;
				} else {
					canPlayAll = false;
					break;
				}
			}
//			// mixed category, can't play all
			canPlayAll = false;
			break;
		}
		return canPlayAll;
	}

	public int setData(BaseElement[] data) {
		return setData(data, true);
	}
	
	public int setData(BaseElement[] data, boolean reset) {
		String opt;
		if (data == null) {
			if (mode == ListComp.TYPE_WISHLIST) {
				String[] list = adult ?
						BookmarkManager.getInstance().retrieveWishlistTitlesAdult() :
						BookmarkManager.getInstance().retrieveWishlistTitles(true);
				if (list == null) {
					list = new String[0];
				}
				data = new BaseElement[list.length];
				if (data.length == 0) {
					menu.back();
					return 0;
				}
				for (int i = 0; i < list.length; i++) {
					data[i] = CatalogueDatabase.getInstance().getCachedBySspId(
							list[i]);
				}
				opt = menu.getSortWishlist();
				data = CatalogueDatabase.getInstance().sort(data, opt, true);

				//data = arrangeList(data);
			} else if (mode == ListComp.TYPE_RESUME) {
				BookmarkManager.getInstance().retrieveActivePurchases(false);
				BookmarkManager.getInstance().organize();
				Bookmark[] list = adult ? BookmarkManager.getInstance().retrieveActivePurchasesAdult() 
						: BookmarkManager.getInstance().retrieveActivePurchases(true);
				
				ArrayList array = new ArrayList();
				long cur = System.currentTimeMillis();
				for (int i = 0; list != null && i < list.length; i++) {
					if (list[i].getLeaseEnd() > cur) {
						array.add(CatalogueDatabase.getInstance()
								.getCachedBySspId(list[i].getOrderableName()));
					}
				}
				if (array.size() == 0) {
					menu.back();
					return 0;
				}
				if (allList != null && allList.length == array.size()) { // no change
					return allList.length;
				}
				data = new BaseElement[array.size()];
				array.toArray(data);
				
				for (int i = 0; i < data.length; i++) {
					if (data[i] instanceof Bundle) {
						((Bundle) data[i]).collapsed = true;
					}
				}
				
				opt = menu.getSortResumeViewing();
				data = CatalogueDatabase.getInstance().sort(data, opt, true);
				allList = data;
				data = arrangeList(data);
				backupList = null;
				setFocus(0);
				listComp.reset();
			}
		}
		boolean change = data.length != catalogueList.length;
		if (Log.DEBUG_ON) {
			Log.printDebug("setData, data = " + data.length + ", catalogueList = " + catalogueList.length);
		}
//								int m = (int) (Math.random() * 60) + 10;
//								BaseElement[] t = new BaseElement[m * catalogueList.length];
//								for (int i = 0; i < m; i++) {
//									for (int j = 0; j < catalogueList.length; j++) {
//										t[i*catalogueList.length+j] = catalogueList[j];
//									}
//								}
//								catalogueList = t;
		int t = listComp.getTitleIndex();
		int f = listComp.getFocus();
		listComp.setData(data);
		if (reset) {
			listComp.reset();
		}
		catalogueList = data;
		if (change) {
			if (catalogueList.length == 0) {
				setFocus(0);
			} else {
				setCurrentFocusUI(FOCUS_LIST);
				listComp.setFocusAndIndex(f, t);
				if (t >= data.length) {
					listComp.keyUp();
				} else {
					setActionButtons();
				}
			}
		} else {
			setCurrentFocusUI(FOCUS_LIST);
			setFocus(listComp.getTitleIndex());
			setActionButtons();
		}
		
		repaint();
		return data.length;
	}

	public int getSortStatus() {
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_NAME)) {
//			return 1;
//		}
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_DATE)) {
//			return 2;
//		}
//		return 0;
		if (isNormal()) {
			return CatalogueDatabase.getInstance().getLastSortTypeIdx();
		}
		
		String opt = mode == ListComp.TYPE_RESUME ? menu.getSortResumeViewing() : menu.getSortWishlist();
		return Resources.OPT_NAME.equals(opt) ? 1 : 2;
	}
	
	public void updateButtons() {
		updateButtons(true);
	}

	public void updateButtons(boolean resetWishList) {
		if (mode == ListComp.TYPE_WISHLIST) {
			if (resetWishList) {
				setData(null);
			}
			//setFocus(0);
			setButtons(catalogueList.length > 10 ? buttonListWishlist : buttonListWishlist2, getCurrentCatalogue());
			listComp.setListHeader(dataCenter.getString(adult
					? Resources.TEXT_T_MY_WISHLIST_18 
					: Resources.TEXT_T_MY_WISHLIST) + " (" + catalogueList.length + ")");
			setFocus(listComp.getTitleIndex());
		} else if (mode == ListComp.TYPE_RESUME) {
//			setData(null); // it can be removed by expired
			setButtons(catalogueList.length > 10 ? buttonListResume : buttonListResume2, getCurrentCatalogue());
		} else {
			setButtons(catalogueList.length > 10 ? buttonList : buttonList2, getCurrentCatalogue());
		}
		int f = actionBox.getFocus();
		setActionButtons();
		actionBox.setFocus(f);
		
		// R5
		// VDTRMASTER-5635
		if (actionBox.getActionBox().length > 2) {
			availableView.setBounds(626, 328, 230, 24);
		} else {
			availableView.setBounds(626, 362, 230, 24);
		}
		
		add(availableView);
		availableView.setData((MoreDetail)getCurrentCatalogue(), this, false);
		availableView.startAnimation();
	}

	public boolean handleKey(int key) {
		Log.printDebug("ListViewUI, handleKey()" + ", code = " + key
				+ ", popup = " + getPopUp());
		switch (key) {
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_INFO:
			keyInfo();
			break;
		case HRcEvent.VK_ENTER:
			keyOK();
			break;
		case KeyCodes.COLOR_A:
			if (mode == ListComp.TYPE_NORMAL) {
				keyYellow();
			}
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		case OCRcEvent.VK_PAGE_UP:
		case OCRcEvent.VK_PAGE_DOWN:
			if (catalogueList.length > 10) {
				footer.clickAnimation(Resources.TEXT_CHANGE_PAGE);
			} else {
				return true;
			}
			break;
		}
		listComp.handleKey(key);
		repaint();
		return true;
	}

	public void keyLeft() {
		setCurrentFocusUI(FOCUS_LIST);
	}

	public void keyRight() {
		setCurrentFocusUI(FOCUS_BUTTON);
	}

	public void keyUp() {
		if (getState() == BaseUI.STATE_ACTIVATED) {
			actionBox.moveFocus(true);
		}
	}

	public void keyDown() {
		if (getState() == BaseUI.STATE_ACTIVATED) {
			actionBox.moveFocus(false);
		}
	}

	public void keyGreen() {
		Log.printDebug("ListViewUI, keyGreen()");
		int opt = CatalogueDatabase.getInstance().getLastSortTypeIdx();
		setPopUp(optionBox);
		switch (mode) {
		case ListComp.TYPE_NORMAL:
			break;
		case ListComp.TYPE_WISHLIST:
			opt = Resources.OPT_NAME.equals(menu.getSortWishlist()) ? 1 : 2;
			break;
		case ListComp.TYPE_RESUME:
			opt = Resources.OPT_NAME.equals(menu.getSortResumeViewing()) ? 1 : 2;
			break;
		}
		optionBox.start(menu.getOptionMenu(opt, getCurrentCatalogue()), menu);
		repaint();
	}

	/**
	 * Sets the focus of UI.
	 */
	public void setFocus(int newFocus) {
		titleIndex = newFocus;
		setActionButtons();

		CatalogueDatabase.getInstance().retrieveImages(this, new BaseElement[] {getCurrentCatalogue()}, ImageRetriever.TYPE_SMALL, CatalogueDatabase.IMAGE_RETRIEVER_SM);
	}

	public void keyPageUp() {
		// scrollText.showPreviousPage();
	}

	public void keyPageDown() {
		// scrollText.showNextPage();
	}

	public void keyBack() {
		if (backupList != null) {
			catalogueList = backupList;
			titleIndex = backupIdx;
			backupList = null;
			
			listComp.setData(catalogueList);
			listComp.setFocusAndIndex(backupFocus, backupTitleIdx);
			listComp.setFocused(catalogueList[titleIndex]);
			
			repaint();
			return;
		}
		if (isNormal()) {
			menu.backwardHistory(false);
		}
		menu.back();
		// 20190222 Touche project
		VbmController.getInstance().backwardHistory();		
	}

	public void keyInfo() {
		if (state != BaseUI.STATE_ACTIVATED) { // key on list
			menu.goToDetail(catalogueList[titleIndex], null);
		}
	}
	
	public boolean isExplored() {
		if (mode != ListComp.TYPE_RESUME) {
			return false;
		}
		return backupList != null;
	}
	
	public void exploreBundle() {
		backupList = catalogueList; 
		backupIdx = titleIndex;
		backupTitleIdx = listComp.getTitleIndex();
		backupFocus = listComp.getFocus();
		
		ArrayList subList = new ArrayList();
		Bundle bundle = (Bundle) catalogueList[titleIndex];
		for (int i = 0; i < allList.length; i++) {
			if (bundle.hasContent(allList[i])) {
				subList.add(allList[i]);
			}
		}
		catalogueList = new BaseElement[subList.size()];
		subList.toArray(catalogueList);
		listComp.setData(catalogueList);
		listComp.reset();
		setFocus(0);
		
		setCurrentFocusUI(FOCUS_LIST);
		
		repaint();
	}

	public void keyOK() {
		if (state != BaseUI.STATE_ACTIVATED) { // key on list
			if (mode == ListComp.TYPE_RESUME && catalogueList[titleIndex] instanceof Bundle && backupList == null) {
				exploreBundle();
				return;
			}
			// Touche:  VDTRMASTER-6242
			VbmController.getInstance().writeSelectionDummy();
			menu.goToDetail(catalogueList[titleIndex], null);
			return;
		}
		actionBox.click();
		menu.processActionMenu(actionBox.getMenu(), catalogueList[titleIndex]);
	}

	public void keyYellow() {
        // R7.3
        if (listComp.getLastCategoryContainer() != null) {
            CatalogueDatabase.getInstance().setLastRequestedCategoryContainerId(listComp.getLastCategoryContainer().id);
        }
		// VDTRMASTER-5690
		DataCenter.getInstance().put("VIEW_PARAM", param);
		menu.viewChange(catalogueList, catalogueList[titleIndex]);
		// VDTRMASTER-5690
		DataCenter.getInstance().remove("VIEW_PARAM");
	}

	public BaseElement[] getCurrentCatalogues() {
		return catalogueList;
	}

	public BaseElement getCurrentCatalogue() {
		BaseElement reCatalogue = null;
		if (catalogueList != null && catalogueList.length > titleIndex) {
			reCatalogue = catalogueList[titleIndex];
		}
		return reCatalogue;
	}

	/**
	 * @param currentFocusUI
	 *            the currentFocusUI to set
	 */
	public void setCurrentFocusUI(int currentFocusUI) {
		this.currentFocusUI = currentFocusUI;
		if (currentFocusUI == FOCUS_LIST) {
			setState(BaseUI.STATE_DEACTIVATED);
			listComp.setState(BaseUI.STATE_ACTIVATED);
		} else {
			if (actionBox.getActionBox().length > 0) {
				actionBox.setFocus(0);
				setState(BaseUI.STATE_ACTIVATED);
				listComp.setState(BaseUI.STATE_DEACTIVATED);
			}
		}
	}
	
	public void reflectVCDSResult(Object cmd) {
		menu.hideLoadingAnimation();
		if (CatalogueDatabase.SORT.equals(cmd)) {
			// VDTRMASTER-5653 & VDTRMASTER-5649
			if (menu.getLastCategoryId().startsWith("TO_")) {
				Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(menu.getLastCategoryId());
				setData(topic.treeRoot.totalContents, true);
			} else {
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				setData(cat.totalContents, true);
			}
			listComp.start(false);
			setFocus(0);
			scroll = 0;
		}
	}

	public void selected(MenuItem item) {
		if (Resources.OPT_NAME.equals(item.getKey()) == false
				&& Resources.OPT_DATE.equals(item.getKey()) == false
				&& Resources.OPT_DATE_WISHLIST.equals(item.getKey()) == false
				&& Resources.OPT_DATE_RESUME_VIEWING.equals(item.getKey()) == false) {
			return;
		}
		switch (mode) {
		case ListComp.TYPE_RESUME:
			menu.setSortResumeViewing(item.getKey());
			allList = null;
			break;
		case ListComp.TYPE_WISHLIST:
			String value = Resources.OPT_NAME.equals(item.getKey()) ? Definitions.WISHLIST_DISPLAY_ALPHABETICAL : Definitions.WISHLIST_DISPLAY_NEWEST_SELECTED_ON_TOP;
			dataCenter.put(PreferenceNames.WISHLIST_DISPLAY, value);
			menu.setSortWishlist(item.getKey());
			break;
		}
		if (mode != ListComp.TYPE_NORMAL) {
			setData(null);
		}
//		if (Resources.OPT_NAME.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_NAME);
//			switch (mode) {
//			case ListComp.TYPE_NORMAL:
//				menu.setSortNormal(Resources.OPT_NAME);
//				break;
//			case ListComp.TYPE_RESUME:
//				menu.setSortResumeViewing(Resources.OPT_NAME);
//				break;
//			case ListComp.TYPE_WISHLIST:
//				menu.setSortWishlist(Resources.OPT_NAME);
//				break;
//			}
//			setData(catalogueList);
//		} else if (Resources.OPT_DATE.equals(item.getKey())
//				|| Resources.OPT_DATE_WISHLIST.equals(item.getKey())
//				|| Resources.OPT_DATE_RESUME_VIEWING.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_DATE);
//			switch (mode) {
//			case ListComp.TYPE_NORMAL:
//				menu.setSortNormal(Resources.OPT_DATE);
//				break;
//			case ListComp.TYPE_RESUME:
//				menu.setSortResumeViewing(Resources.OPT_DATE);
//				break;
//			case ListComp.TYPE_WISHLIST:
//				menu.setSortWishlist(Resources.OPT_DATE);
//				break;
//			}
//			setData(catalogueList);
//		}
//		setFocus(listComp.getTitleIndex());
	}

	public void receiveCheckRightFilter(CheckResult checkResult) {
		Log.printDebug("ListViewUI, receiveCheckRightFilter()"
				+ ", filter = " + checkResult.getFilter() + ", response = "
				+ checkResult.getResponse());
		if (checkResult.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
			setActionButtons();
			repaint();
		}
	}
	
public boolean isMixedContents() {
		
		Hashtable countTable = new Hashtable();
		
		if (catalogueList != null) {
			for (int i = 0; i < catalogueList.length; i++) {
				if (catalogueList[i] instanceof Series) {
					countTable.put("Series", catalogueList[i]);
				} else if (catalogueList[i] instanceof Season) {
					countTable.put("season", catalogueList[i]);
				} else if (catalogueList[i] instanceof Bundle) {
					countTable.put("bundle", catalogueList[i]);
				} else if (catalogueList[i] instanceof Video) {
					Video v = (Video) catalogueList[i];
					
					if (v instanceof Episode) {
						countTable.put("episode", catalogueList[i]);
					} else if ("VT_100".equals(v.getTypeID())) {
						countTable.put("VT_100", catalogueList[i]);
					} else if ("VT_102".equals(v.getTypeID())) {
						countTable.put("vt_102", catalogueList[i]);
					} else if ("VT_103".equals(v.getTypeID())) {
						countTable.put("VT_103", catalogueList[i]);
					} else if ("VT_104".equals(v.getTypeID())) {
						countTable.put("VT_104", catalogueList[i]);
					} else if ("VT_124".equals(v.getTypeID())) {
						countTable.put("VT_124", catalogueList[i]);
					}
				}
				
				if (countTable.size() > 1) {
					return true;
				}
			}
		}
		
		return false;
	}
}

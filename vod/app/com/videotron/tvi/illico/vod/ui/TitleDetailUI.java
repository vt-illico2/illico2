package com.videotron.tvi.illico.vod.ui;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import com.videotron.tvi.illico.vod.data.vcds.type.*;
import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ActionBox;
import com.videotron.tvi.illico.vod.comp.SimilarContentView;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.similar.SimilarContentManager;
import com.videotron.tvi.illico.vod.data.similar.type.SHead;
import com.videotron.tvi.illico.vod.data.similar.type.SimilarContent;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.gui.TitleDetailRenderer;

public class TitleDetailUI extends BaseUI {


	private static final long serialVersionUID = 8768044039360734354L;

	private Video title;
	private Bundle bundle;
	private Renderer titleRenderer;

	private ActionBox actionBox;

	private OptionScreen optionBox = new OptionScreen();
	private ScrollTexts scrollText = new ScrollTexts();

//	static private Rectangle recScrollText = new Rectangle(302, 314, 298, 158);
//	static private Rectangle recScrollText2 = new Rectangle(372, 314, 228, 124);
//	static private Rectangle recScrollTextWithSimilar = new Rectangle(302, 314, 298, 158);
//	static private Rectangle recScrollText2WithSimilar = new Rectangle(372, 314, 228, 124);
	static private Rectangle recScrollText = new Rectangle(292, 295, 298, 158);
	static private Rectangle recScrollText2 = new Rectangle(372, 295, 228, 158);
	static private Rectangle recScrollTextWithSimilar = new Rectangle(292, 295, 298, 124);
	static private Rectangle recScrollText2WithSimilar = new Rectangle(372, 295, 228, 124);
	static private Color cScrollTextColor = new DVBColor(255, 255, 255, 230);
	static private int infoPageRowHeight = 17;

	static private int[] buttonList = new int[] { BaseUI.BTN_SCROLL, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST,
			BaseUI.BTN_OPTION };

	public String titleId;
	
	// R5
	private SimilarContentView sView;
	// R7
	boolean writeSimilarPanelOpened = false;

	public TitleDetailUI() {
		titleRenderer = TitleDetailRenderer.getInstance();
		
		if (sView == null) {
			sView = new SimilarContentView();
		}
		
		scrollText.setTopMaskImage(dataCenter.getImage("01_des_sh_dt_t.png"));
		scrollText.setBottomMaskImage(dataCenter.getImage("01_des_sh_dt.png"));
		add(scrollText);

		actionBox = ActionBox.newInstance();
		add(actionBox);

		Image btnPage = (Image) ((Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_PAGE);
		footer.linkWithScrollTexts(btnPage, scrollText);
		add(footer);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);
	}

	public void start(boolean back) {
		Log.printInfo("TitleDetailUI, start(), back = " + back);
		if (!back) {
			setRenderer(titleRenderer);
			prepare();

			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];
			if (obj instanceof Video) {
				title = (Video) obj;
			} else if (obj instanceof String) {
				titleId = (String) obj;
				title = (Video) CatalogueDatabase.getInstance().getCached(obj);
			}
			Log.printDebug("TitleDetailUI, title = " + title);
			if (title == null) {
				updateButtons();
				state = STATE_ACTIVATED;
				super.start();
				CommunicationManager.getInstance().errorMessage("VOD105", null);
				return;
			}

			// VDTRMASTER-6114
            updateContent();
			
			// R5
//			if (MenuController.getInstance().needToBackEPG() || MenuController.getInstance().isFromTitleDetailUI()) {
//				VbmController.getInstance().writeEnterTitleDetailUI();
//			}
			// for ATP, store a log for all case.
			VbmController.getInstance().writeEnterTitleDetailUI();
			
			// R7
			writeSimilarPanelOpened = false;
		} else {
		    // VDTRMASTER-6114
		    BaseElement element = menu.getNext();
		    menu.setNext(element);

		    if (element instanceof Video) {
		        title = (Video) element;
            }

            Log.printDebug("TitleDetailUI, title = " + title);
            if (title == null) {
                updateButtons();
                state = STATE_ACTIVATED;
                super.start();
                CommunicationManager.getInstance().errorMessage("VOD105", null);
                return;
            }

            updateContent();

        }
		//// Jira 3380
		BaseRenderer.setBrandingBg(-1, null);
		//BaseRenderer.setBrandingBg(1, null);
//		BaseRenderer.setBrandingBg(0, null);
//		Channel channel = null;
//		ImageGroup bg = null;
//		
//		try {
//			channel = CategoryUI.getInstance().getChannel();
//			bg = channel.getBackground(true);
//			if (channel.network != null) {
//				bg = channel.network.getBackground(true);
//			} else {
//				bg = channel.getBackground(true);
//			}
//			//brandBG.setImage(dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//			BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//			//setButtons(catalogueList.length > 7 ? buttonListWithoutChangeView : buttonListWithoutChangeView2);
//		} catch (Exception e) {
//			try {
//				BrandingElement br = CategoryUI.getInstance().getBrandingElement();
//				bg = br.getBackground(true);
//				//brandBG.setImage(dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//				BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//				//setButtons(catalogueList.length > 7 ? buttonList : buttonList2);
//			} catch (Exception ee) { 
//				//brandBG.flush();
//				BaseRenderer.setBrandingBg(-1, null);
//			}
//		}
		///////////////////////////
		
		setVisible(true);
		//BaseRenderer.setBrandingBg(-1, null);
		
		synchronized (menu) {
			menu.notifyAll();
		}

		CatalogueDatabase.getInstance().retrieveLargeImage(this, title, false);
	}

	private void updateContent() {
        Rectangle sbBounds = title.isPortrait() ? recScrollText : recScrollText2;
        scrollText.setBounds(sbBounds);
        scrollText.setTopMaskImagePosition(292-sbBounds.x, 315-sbBounds.y);
        scrollText.setBottomMaskImagePosition(292-sbBounds.x, 443-sbBounds.y);
        scrollText.setFont(FontResource.DINMED.getFont(17));
        scrollText.setForeground(cScrollTextColor);
        scrollText.setRowHeight(infoPageRowHeight);
        scrollText.setRows(sbBounds.height / infoPageRowHeight);

        setTitleDetail();
        focus = 0;

        if (title.inBundle()) {
            String bundleId = title.inBundlesAR[0];
            bundle = (Bundle) CatalogueDatabase.getInstance().getCached(
                    bundleId);
            if (bundle == null || bundle.bundleDetail == null) {
                CatalogueDatabase.getInstance().retrieveBundle(this,
                        bundleId, false, "repaint");
            }
        }

        sView.init();
        actionBox.setFocus(focus);
        updateButtons();
        state = STATE_ACTIVATED;
        sView.setActionBox(actionBox);

        boolean needToSimilar = false;
        if (bundle == null && (title.getTypeID().equalsIgnoreCase("VT_100")
                || title.getTypeID().equalsIgnoreCase("VT_102") || title.getTypeID().equalsIgnoreCase("VT_104")
                || title.getTypeID().equalsIgnoreCase("VT_124"))) {
            needToSimilar = true;
        }

        add(sView, 0);

        if (needToSimilar) {
            new Thread("LOAD_SIMILAR") {
                public void run() {

                    //					if (Environment.EMULATOR) {
                    //						return;
                    //					}

                    menu.showLoadingAnimation();
                    SimilarContent[] contents = SimilarContentManager.getInstance().requestSimilarContents(title.id);
                    if (contents != null) {
                        sView.setElements(contents);
                        updateButtons();
                    } else {
                        sView.setElements(null);
                        SHead head = SimilarContentManager.getInstance().getLastHead();
                        if (head != null) {
                            sView.setHead(head);
                            updateButtons();
                        }
                    }

                    menu.hideLoadingAnimation();
                    requestFocus();
                    repaint();

                    if (contents != null) {
                        for (int i = 0; i < contents.length; i++) {
                            contents[i].retrieveImage((TitleDetailUI)sView.getParent());
                        }
                        sView.startAnimation(SimilarContentView.MODE_INTRO);
                    }
                }
            }.start();
        }
    }
	
	public Footer getFooter() {
		if (sView.hasFocus()) {
			return sView.getFooter();
		}
		return super.getFooter();
	}
	
	public void scroll() {
		if (sView.hasFocus()) {
			if (scrollBounds.width == 0) {
				return;
			}
			scroll++;
			sView.repaint();
		} else {
			super.scroll();
		}
	}

	private void setTitleDetail() {
		if (menu == null) {
			menu = MenuController.getInstance();
		}
		if (menu.isAdultBlocked(title)) {
			scrollText.setContents(dataCenter.getString(Resources.TEXT_MSG_BLOCKED));
			scrollText.setButtonIcon(false);
		} else {
			scrollText.setContents(title.getDescription());
			scrollText.setButtonIcon(false);
		}
	}
	private void setActionButtons() {
		String[] actionButtons = getActionButton(title);
		actionBox.setActionBox(actionButtons);
		actionBox.setFocus(focus);
	}

	public void updateButtons() {
		setButtons(buttonList, getTitle());
		int f = actionBox.getFocus();
		setActionButtons();
		actionBox.setFocus(f);
		
		updateScrollText();
		
		if (sView.hasFocus()) {
			actionBox.setDimmedFocus(true);
			actionBox.setDimmedBg(true);
			sView.updateFooter();
		}
		
		// R5
		if (title.isPortrait()) {
//				if (sView.hasContents()) {
//					availableView.setBounds(56, 405, 210, 24);
//				} else {
//					availableView.setBounds(56, 420, 210, 24);
//				}
			availableView.setBounds(46, 400, 230, 24);
		} else {
//				if (sView.hasContents()) {
//					availableView.setBounds(93, 303, 210, 24);
//				} else  {
//					availableView.setBounds(93, 323, 210, 24);
//				}
			availableView.setBounds(46, 237, 230, 24);
		}
		
		add(availableView);
		availableView.setData(title, this, true);
		availableView.startAnimation();
	}
	
	// R5
	public void updateScrollText() {
		Rectangle sbBounds = null;
		
//		if (sView.hasContents()) {
//			sbBounds = title.isPortrait() ? recScrollTextWithSimilar : recScrollText2WithSimilar;
//			scrollText.setBounds(sbBounds);
//			scrollText.setTopMaskImagePosition(292-sbBounds.x, 295-sbBounds.y);
//			scrollText.setBottomMaskImagePosition(292-sbBounds.x, 409-sbBounds.y);
//		} else {
//			sbBounds = title.isPortrait() ? recScrollText : recScrollText2;
//			scrollText.setBounds(sbBounds);
//			scrollText.setTopMaskImagePosition(292-sbBounds.x, 295-sbBounds.y);
//			scrollText.setBottomMaskImagePosition(292-sbBounds.x, 443-sbBounds.y);
//		}
		sbBounds = recScrollTextWithSimilar;
		scrollText.setBounds(recScrollTextWithSimilar);
		scrollText.setTopMaskImagePosition(292-sbBounds.x, 295-sbBounds.y);
		scrollText.setBottomMaskImagePosition(292-sbBounds.x, sbBounds.height - 35);
		
		scrollText.setRows(sbBounds.height / infoPageRowHeight);
	}

	/**
	 * @return the title
	 */
	public Video getTitle() {
		return title;
	}
	
	public String findCurrentLabel() {
		return findPlayLabel(title);
	}

	public Bundle getBundle() {
		return bundle;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(Video title) {
		this.title = title;
	}
	
	// R5
	public boolean needSimilarContentButton() {
		return sView.needButton();
	}
	
	// R5
	public boolean hasSimilarContents() {
		return sView.hasContents();
	}
	
	// R5
	public boolean hasFocusOnSimilarContents() {
		return sView.hasFocus();
	}
	
	// R5
	public boolean needKeyBlockForSimilarContent() {
		
		if (sView.isOpened() && sView.hasContents()) {
			return true;
		}
		
		return false;
	}

	public boolean handleKey(int key) {
		Log.printDebug("TitleDetailUI, handleKey()" + ", code = "
				+ key + ", popup = " + getPopUp() + ", sView.hasFocus=" + sView.hasFocus());
		
		if (sView.hasFocus()) {
			boolean used = sView.handleKey(key);
			
			if (used) {
				return true;
			}
		}
		
		switch (key) {
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_ENTER:
			keyOK();
			break;
		case KeyCodes.COLOR_D:
			if (sView.isOpened())  {
				Log.printDebug("TitleDetailUI, handleKey(int), sView.isOpened=" + sView.isOpened());
				return true;
			}
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
		case HRcEvent.VK_INFO:
			keyBack();
			break;
		case HRcEvent.VK_PAGE_UP:
			if (sView.isOpened())  {
				Log.printDebug("TitleDetailUI, handleKey(int), sView.isOpened=" + sView.isOpened());
				return true;
			}
			keyPageUp();
			break;
		case HRcEvent.VK_PAGE_DOWN:
			if (sView.isOpened())  {
				Log.printDebug("TitleDetailUI, handleKey(int), sView.isOpened=" + sView.isOpened());
				return true;
			}
			keyPageDown();
			break;
		}
		repaint();
		return true;
	}

	public void keyGreen() {
		Log.printDebug("TitleDetailUI, keyGreen()");
		setPopUp(optionBox);
		optionBox.start(menu.getOptionMenu(0, title), menu);
		repaint();
	}

	public void keyOK() {
		actionBox.click();
		if (actionBox.getMenu().equals(dataCenter.getString(Resources.TEXT_SIMILAR_CONTENT)) && sView.hasContents()) {
			sView.setHasFocus(true);
			actionBox.setDimmedFocus(true);
		} else {
			menu.processActionMenu(actionBox.getMenu(), title);
		}
	}
	
	public void keyLeft() {
		String[] actionStrs = actionBox.getActionBox();
		if (actionStrs[actionStrs.length - 1].equals(dataCenter.getString(Resources.TEXT_SIMILAR_CONTENT)) && sView.hasContents()) {
			focus = actionStrs.length - 1;
			if (!sView.isOpened()) {
    			sView.startAnimation(SimilarContentView.MODE_OPEN);
    			
    			// R7
    			if (!writeSimilarPanelOpened) {
    				writeSimilarPanelOpened = true;
    				VbmController.getInstance().writeSimilarPanelOpened(title.id);
    			}
    		}
			actionBox.setFocus(focus);
			sView.setHasFocus(true);
			actionBox.setDimmedFocus(true);
		}
	}

	public void keyUp() {
		actionBox.moveFocus(true);
		if (!actionBox.getMenu().equals(dataCenter.getString(Resources.TEXT_SIMILAR_CONTENT)) && sView.isOpened()) {
			sView.startAnimation(SimilarContentView.MODE_CLOSE);
		}
	}

	public void keyDown() {
		actionBox.moveFocus(false);
		if (actionBox.getMenu().equals(dataCenter.getString(Resources.TEXT_SIMILAR_CONTENT)) && !sView.isOpened()) {
			sView.startAnimation(SimilarContentView.MODE_OPEN);
			
			// R7
			if (!writeSimilarPanelOpened) {
				writeSimilarPanelOpened = true;
				VbmController.getInstance().writeSimilarPanelOpened(title.id);
			}
		}
	}

	public void keyPageUp() {
		footer.clickAnimation(Resources.TEXT_SCROLL_DESCRIPTION);
		scrollText.showPreviousPage();
	}

	public void keyPageDown() {
		footer.clickAnimation(Resources.TEXT_SCROLL_DESCRIPTION);
		scrollText.showNextPage();
	}

	public void keyBack() {
		// 20190222 Touche project, VDTRMASTER-6253		
		if (!menu.isFromSearch())
			VbmController.getInstance().backwardHistory();
		// menu.backwardHistory(false);
		menu.back();		
	}

	public void receiveCheckRightFilter(CheckResult checkResult) {
		Log.printDebug("TitleDetailUI, receiveCheckRightFilter()"
				+ ", filter = " + checkResult.getFilter() + ", response = "
				+ checkResult.getResponse());
		if (checkResult.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
			setActionButtons();
			setTitleDetail();
			repaint();
		}
	}

	public void reflectVCDSResult(Object cmd) { // bundle
		Log.printDebug("TitleDetailUI, reflectVCDSResult()"
				+ ", cur = " + title.toString(false) + ", cmd = " + cmd);
		if ("repaint".equals(cmd)) {
			String id = title.inBundlesAR[0];
			bundle = (Bundle) CatalogueDatabase.getInstance().getCached(id);
			repaint();
		}
	}
}

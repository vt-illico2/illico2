package com.videotron.tvi.illico.vod.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.PromotionBanner;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.vod.app.App;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.KaraokeView;
import com.videotron.tvi.illico.vod.comp.MainToolsUI;
import com.videotron.tvi.illico.vod.comp.MenuTreeUI;
import com.videotron.tvi.illico.vod.comp.ShowcaseView;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.UsageManager;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.util.StringUtils;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Image;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Network;
import com.videotron.tvi.illico.vod.data.vcds.type.Service;
import com.videotron.tvi.illico.vod.data.vcds.type.Showcase;
import com.videotron.tvi.illico.vod.data.vcds.type.ShowcaseContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.gui.CategoryBackgroundRenderer;

public class CategoryUI extends BaseUI {

	private static final long serialVersionUID = -2791732726762645203L;
	private static CategoryUI instance;

	public static final int FOCUS_MENU = 0;
	public static final int FOCUS_RIGHT = 1;
	public static final int FOCUS_OTHER = 2;

	private MenuTreeUI menuTreeUI = new MenuTreeUI();
	private MenuTreeBG menuTreeBG = new MenuTreeBG();

	private ShowcaseView showcaseView = new ShowcaseView();
	private KaraokeView karaokeView = new KaraokeView();
	private MainToolsUI mainToolUI = new MainToolsUI();
	private Renderer bgRenderer = new CategoryBackgroundRenderer();
	static private int[] buttonList = new int[] { BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonList2 = new int[] { BaseUI.BTN_SEARCH, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonList3 = new int[] { BaseUI.BTN_PLAYLIST, BaseUI.BTN_OPTION };
	static private int[] buttonList4_page = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_PROFILE,
			BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonList5_page = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_ALL_CHANNEL,
		BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonList4 = new int[] { BaseUI.BTN_PAGE, BaseUI.BTN_MY_CHANNEL,
		BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
static private int[] buttonList5 = new int[] { BaseUI.BTN_PAGE, BaseUI.BTN_ALL_CHANNEL,
	BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };

	private ArrayList changeCategory = null;
	private Thread own;
	private CategoryContainer lastCategory;
	private AlphaEffect fadeIn;
	private boolean wasShowcaseView;
	private boolean needLoading;

	private Showcase showcasePartyPack;
	private Showcase showcaseTrial;
	private ImageLabel karaokeLeftBanner;
	private ImageLabel karaokeRightBanner;
	private ImageLabel karaokeTopBanner;
	private ImageLabel karaokeBottomBanner;
	private ImageLabel karaokeLabel;
	private static CategoryContainer karaokeContainer;
	private static CategoryContainer partyPack;
	private static CategoryContainer trialPack;
	private static HashSet trialSongs = new HashSet();
	private static Bookmark purchasedPartyPack = null;
	private CategoryContainer paramCategory;

	//R5
	private static CategoryContainer channelContainer;

	private CategoryUI() {
		Log.printDebug("CategoryUI, call Constructor");
		
		add(menuTreeBG);

		add(menuTreeUI, 0);
		menuTreeUI.makeEffect();

		karaokeLeftBanner = new ImageLabel(null);
		karaokeRightBanner = new ImageLabel(null);
		karaokeTopBanner = new ImageLabel(null);
		karaokeBottomBanner = new ImageLabel(null);
		karaokeLabel = new ImageLabel(null);
		karaokeLeftBanner.setVisible(false);
		karaokeRightBanner.setVisible(false);
		karaokeTopBanner.setVisible(false);
		karaokeBottomBanner.setVisible(false);
		karaokeLabel.setVisible(false);

		add(mainToolUI);
		add(showcaseView);
		showcaseView.setVisible(false);
		add(karaokeView);
		add(karaokeLabel);
		karaokeTopBanner.setBounds(373, 111, 513, 107);
		add(karaokeTopBanner);
		karaokeLeftBanner.setBounds(373, 230, 173, 185);
		add(karaokeLeftBanner);
		karaokeRightBanner.setBounds(555, 230, 331, 185);
		add(karaokeRightBanner);
		karaokeBottomBanner.setBounds(373, 230, 513, 185);
		add(karaokeBottomBanner);

		add(footer);

//		fadeIn = new AlphaEffect(this, 5, AlphaEffect.FADE_IN);

		setVisible(false);
	}

	public static CategoryUI getInstance() {
		if (instance == null) {
			instance = new CategoryUI();
		}
		return instance;
	}

	public static CategoryUI newInstance() {
		return new CategoryUI();
	}

	public void start(boolean back) {
		Log.printDebug("CategoryUI, start(), back = " + back);
		setBounds(Resources.fullRec);
		own = new Thread(this, "CategoryUI main thread");
		own.start();

		if (back == false) {
			setRenderer(bgRenderer);
			prepare();

			menuTreeBG.setVisible(false);
			menuTreeUI.setVisible(false);
			menuTreeUI.start(false);

			mainToolUI.start(false);
			mainToolUI.setVisible(false);
			karaokeView.setVisible(false);
			showcaseView.setVisible(false);

			setFocus(CategoryUI.FOCUS_MENU);
			currentFocusUI = CategoryUI.FOCUS_MENU;

			Object[] param = MenuController.peekStack();
			paramCategory = null;
			if (param.length == 2 && param[1] == null) {
				DataCenter.getInstance().getImage("01_default_poster.png");
				// DataCenter.getInstance().getImage("00_menubg.png");
				DataCenter.getInstance().getImage("00_menufocus.png");
				DataCenter.getInstance().getImage("00_icon01.png");
				DataCenter.getInstance().getImage("bg.jpg");
				// FrameworkMain.getInstance().getImagePool().waitForAll();
				menu = MenuController.getInstance();
				setChannel(null);
				setNetwork(null);
				setChannelCategory(null);
				setBrandingElement(null, true);

//				karaokeLeftBanner.setHighlight(dataCenter
//						.getImage("01_karahigh_173.png"));
//				karaokeRightBanner.setHighlight(dataCenter
//						.getImage("01_karahigh_331.png"));
//				karaokeTopBanner.setHighlight(dataCenter
//						.getImage("01_karahigh_513.png"));
//				karaokeBottomBanner.setHighlight(dataCenter
//						.getImage("01_karahigh_511.png"));

				UsageManager.getInstance().addUsageLog("1103", "", "", "");

			} else { // channel menu
				if (param[1] instanceof CategorizedBundle) {
					super.start();
					moveToKaraokePackage(true);
				} else {
					CategoryContainer category = (CategoryContainer) param[1];
					paramCategory = category;
					Log.printDebug("CategoryUI, start(), cat = "
							+ category.toString(true));
					if (category.getTopic() != null) {
						if (category.getTopic().network != null) {
							setNetwork(category.getTopic().network);
						} else if (category.getTopic().channel != null) {
							setChannel(category.getTopic().channel);
						}
					}
					menuTreeBG.start();
					// move up front of if , VDTRMASTER-5306 [CQ][PO1/2/3][R4][VOD] ▪ Posters for the focused item don't show up on the content zone
					super.start();

					if (category.type == MenuController.MENU_CHANNEL_ENV) {
						menuTreeUI.goToChannelOnDemand(category);
					} else {
						if (menuTreeUI.setMenu(category.getTopic().treeRoot.categoryContainer)) {
							categoryChanged(menuTreeUI.getCurrentCategory());
						}
					}
				}
			}
		} else {
			// VDTRMASTER-5684
			menuTreeBG.setVisible(true);
			menuTreeUI.start(true);
			super.start();
			CategoryContainer cat = menuTreeUI.getCurrentCategory();
			showCategory(cat, true);

			// VDTRMASTER-5303
			try {
				int tempFocus = showcaseView.getFocus();
				showcaseView.setData(cat);
				showcaseView.setFocus(tempFocus);
			} catch (Exception e) {
				Log.print(e);
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("CategoryUI, start(), cat=" + cat);
				if (cat.parent != null) {
					Log.printDebug("CategoryUI, start(), cat.parent.getTopic()=" + cat.parent.getTopic());
				}
			}

			CategoryContainer parent = cat.parent;
			while (parent != null && setBrandingElementByCategory(parent) == false) {
				if (Log.DEBUG_ON) {
					Log.printDebug("CategoryUI, start(), parent=" + parent);
					Log.printDebug("CategoryUI, start(), parent.getTopic()=" + parent.getTopic());
				}
				parent = parent.parent;
			} // support 267

			if (Log.DEBUG_ON) {
				Log.printDebug("CategoryUI, start(), last parent=" + parent);
				Log.printDebug("CategoryUI, start(), ParentElement=" + menuTreeUI.getParentElement());
				Log.printDebug("CategoryUI, start(), category=" + menuTreeUI.getParentCategoryHasTopic());
			}

			BrandingElement br = menuTreeUI.getParentElement();
			if (br != null) {
				setBrandingElement(br, true);
			}

			parent = menuTreeUI.getParentCategoryHasTopic();
			if (parent != null) {
				setBrandingElementByCategory(parent);
			}

			if (parent == null && paramCategory != null) {
				setBrandingElementByCategory(paramCategory);
			}

			setBranded(Integer.MAX_VALUE);

			// R5
			if (getCurrentCategory().type == MenuController.MENU_CHANNEL
					&& ChannelInfoManager.getInstance().needToUpdate) {
				ChannelInfoManager.getInstance().needToUpdate = false;
				updateFavoriteChannel();
			}
		}
		updateButtons();

//		own = new Thread(this);
//		own.start();

		BaseRenderer.setBrandingBg(isBranded() || BaseRenderer.hasBrandingBg() ? 1 : -1, null);
		repaint();
		// UsageManager.getInstance().addUsageLog("1103", "", "", "");
	}

	public void fadeIn() {
		if (fadeIn == null) {
			fadeIn = new AlphaEffect(this, 5, AlphaEffect.FADE_IN);
		}
		if (showcaseView != null) {
			showcaseView.setVisible(false);
		}

		// IMPROVEMENT
//		fadeIn.start();
		menuTreeBG.setVisible(true);
		menuTreeUI.setVisible(true);
		showcaseView.setVisible(true);
		super.start();
	}

	public void fadeOutShowcase() {
		if (showcaseView != null) {
			showcaseView.fadeOut();
		}
	}

	public void imageDownloadMore() {
		Log.printDebug("CategoryUI, imageDownloadMore(), showcaseView.visible = " + (showcaseView == null ? "NULL" : String.valueOf(showcaseView.isVisible())));
		if (showcaseView != null && showcaseView.isVisible()) {
			showcaseView.retrieveImages(true);
		}
	}

	public void animationEnded(Effect effect) {
		if (effect == fadeIn) {
			MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF
					.format(new Date()) + ", CategoryUI.fadeIn() done");
			// setVisible(true);
			// reset();
			super.start();
			new Thread("getRoot") {
				public void run() {
					Object root = null;
					needLoading = true;
					while (needLoading) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
						}
						root = CatalogueDatabase.getInstance().getRoot();
						if (isVisible()
								&& Resources.appStatus == VODService.APP_STARTED
								&& root == null
								&& needLoading) {
							menu.showLoadingAnimation(false);
						} else {
							// VDTRMASTER-5640
							if (menu.getParamKey() == null) {
								menu.hideLoadingAnimation();
							}
							break;
						}
					}
				}
			}.start();
		}
	}

	// R7
	// VDTRMASTER-5774
	public void startLoadingChecker() {
		new Thread("LoadingChecker") {
			public void run() {
				while (true) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
					if (!isVisible() && Resources.appStatus == VODService.APP_STARTED) {
						menu.showLoadingAnimation(true);
					} else {
						menu.hideLoadingAnimation();
						break;
					}
				}
			}
		}.start();
	}

	public void stop() {
		super.stop();
		needLoading = false;

		if (own != null) {
//			Thread t = own;
			own = null;
//			t.interrupt();

			synchronized (changeCategory) {
				changeCategory.clear();
				changeCategory.notifyAll();
			}
		}

		lastCategory = null;
		// karaokeLabel.flush();

		showcaseView.setVisible(false);
		mainToolUI.setVisible(false);
		karaokeView.setVisible(false);
		karaokeLabel.setVisible(false);
		karaokeTopBanner.setVisible(false);
		karaokeLeftBanner.setVisible(false);
		karaokeRightBanner.setVisible(false);
		karaokeBottomBanner.setVisible(false);
	}

	public static void setKaraokeContainer(CategoryContainer cat) {
		karaokeContainer = cat;
	}

	public static CategoryContainer getKaraokeContainer() {
		return karaokeContainer;
	}

	public static Bookmark getPurchasedPartyPack() {
		if (purchasedPartyPack == null) {
			Log.printDebug("getPurchasedPartyPack(), purchasedPartyPack is null");
			return null;
		}
		long c = System.currentTimeMillis();
		long e = purchasedPartyPack.getLeaseEnd();
		//Log.printDebug("getPurchasedPartyPack(), c = " + c + ", e = " + e);
		return c < e ? purchasedPartyPack : null;
	}

	public static CategoryContainer getPartyPack() {
		return partyPack;
	}

	public static String getPartyPackString() {
		if (partyPack == null
				|| purchasedPartyPack == null
				|| System.currentTimeMillis() > purchasedPartyPack.getLeaseEnd()) {
			return "";
		}
		long leaseEnd = purchasedPartyPack.getLeaseEnd();
		String str = DataCenter.getInstance().getString(
				Resources.TEXT_MSG_KARAOKE_SUBS_TIME);
		str = StringUtils.replace(str, "[package name]", partyPack.getTitle());
		str = StringUtils.replace(
				str,
				"[countdown timer]",
				Formatter.getCurrent().getDurationText(
						leaseEnd - System.currentTimeMillis()));
		return str;
	}

	public static void setPartyPack(CategoryContainer cat) {
		partyPack = cat;
		if (cat != null && cat.categorizedBundle != null && cat.categorizedBundle.detail.length > 0) {
			String sspId = cat.categorizedBundle.detail[0].orderableItem;
			purchasedPartyPack = BookmarkManager.getInstance().getPurchased(sspId);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("setPartyPack, partyPack = " + cat
					+ ", purchasedPartyPack = " + purchasedPartyPack);
		}
	}
	
	//R7.4 freelife VDTRMASTER-6200
	public static void resetTrialPack() {
		partyPack = null;
		trialPack = null;
		trialSongs.clear();
		purchasedPartyPack = null;
	}

	public static void setTrialPack(CategoryContainer cat) {
		trialPack = cat;
		trialSongs.clear();
		for (int i = 0; i < cat.categoryContainer.length; i++) {
			for (int j = 0; j < cat.categoryContainer[i].videoContainer.length; j++) {
				trialSongs.add(cat.categoryContainer[i].videoContainer[j].video);
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("setTrialPack, songs = " + trialSongs.size());
		}
	}

	public static void addTrialSongs(CategoryContainer cat) {
		for (int i = 0; i < cat.videoContainer.length; i++) {
			trialSongs.add(cat.videoContainer[i].video);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("addTrialSongs, songs = " + trialSongs.size());
		}
	}

	public static boolean isTrialSong(MoreDetail element) {
		Log.printDebug("isTrialSong, element = " + (element == null ? "NULL" : element.toString(true)) + ", contains = " + trialSongs.contains(element));
		return trialSongs.contains(element);
	}

	public void updateKaraokeBanners() {
		// CategoryContainer cur = getCurrentCategory();
		// if (cur == null) {
		// return;
		// }
		// if (cur.getTopic() == null) {
		// cur = karaokeContainer;
		// }
		// if (cur == null || cur.getTopic() == null) {
		// return;
		// }
		Topic karaokeTopic = CatalogueDatabase.getInstance().getKaraokeTopic();
		if (karaokeTopic == null) {
			return;
		}
		ImageGroup ig = karaokeTopic.getBanner(ImageRetriever.AFFICHE);
		if (ig != null) {
			// VDTRMASTER-5445
			karaokeTopBanner.setImage(dataCenter.getString("IMAGE_URL")
					+ ImageRetriever.BASE + ig.images[0].imageName + "." + ig.encoding);
		}
		ShowcaseContainer[] showcase = karaokeTopic.treeRoot.showcaseContainer;
		for (int i = 0; i < showcase.length; i++) {
			if (showcase[i].showcase.detail.importance == 2) {
				showcaseTrial = showcase[i].showcase;
				if (karaokeLeftBanner.isVisible()) {
					ig = (ImageGroup) showcaseTrial.chooseImage(ImageRetriever.KARAOKE_BANNER1, false);
					if (ig != null) {
						// VDTRMASTER-5445
						karaokeLeftBanner.setImage(dataCenter
								.getString("IMAGE_URL")
								+ ImageRetriever.BASE
								+ ig.images[0].imageName + "." + ig.encoding);
					}
				} else if (karaokeBottomBanner.isVisible()) {
					ig = (ImageGroup) showcaseTrial.chooseImage(ImageRetriever.KARAOKE_BANNER2, false);
					if (ig != null) {
						// VDTRMASTER-5445
						karaokeBottomBanner.setImage(dataCenter
								.getString("IMAGE_URL")
								+ ImageRetriever.BASE
								+ ig.images[0].imageName + "." + ig.encoding);
					}
				}
			} else if (showcase[i].showcase.detail.importance == 3) {
				showcasePartyPack = showcase[i].showcase;
				if (karaokeRightBanner.isVisible()) {
					ig = (ImageGroup) showcasePartyPack.chooseImage(ImageRetriever.KARAOKE_BANNER2, false);
					if (ig != null) {
						// VDTRMASTER-5445
						karaokeRightBanner.setImage(dataCenter
								.getString("IMAGE_URL")
								+ ImageRetriever.BASE
								+ ig.images[0].imageName + "." + ig.encoding);
					}
				} else if (karaokeBottomBanner.isVisible()) {
					ig = (ImageGroup) showcasePartyPack.chooseImage(ImageRetriever.KARAOKE_BANNER1, false);
					if (ig != null) {
						// VDTRMASTER-5445
						karaokeBottomBanner.setImage(dataCenter
								.getString("IMAGE_URL")
								+ ImageRetriever.BASE
								+ ig.images[0].imageName + "." + ig.encoding);
					}
				}
			}
		}
		PreferenceProxy.getInstance().loadPlaylist(true);
	}

	public void updateButtons() {
		// R5 remove content for Wishlist
		if (mainToolUI.isVisible() && currentFocusUI == FOCUS_RIGHT) {
			mainToolUI.setData(getCurrentCategory());

			if ((menuTreeUI.getCurrentCategory().type == MenuController.MENU_WISHLIST
					|| menuTreeUI.getCurrentCategory().type == MenuController.MENU_WISHLIST_ADULT)
					&& mainToolUI.getCurrentCatalogue() == null) {
				// data is empty so focus on categoryUI
				setCurrentFocusUI(CategoryUI.FOCUS_MENU);
			}
		}

		if (isTop() && instance == this) {
			setButtons(buttonList2, getCurrentCatalogue());
		} else if (getCurrentCategory() != null
				&& (getCurrentCategory().type == MenuController.MENU_KARAOKE_PACK || getCurrentCategory().type == MenuController.MENU_KARAOKE_CONTENTS)) {
//			if (getPurchasedPartyPack() == null) {
//				setButtons(buttonList4);
//			} else {
				setButtons(buttonList3, getCurrentCatalogue());
//			}
		} else if (getCurrentCategory() != null && getCurrentCategory().type == MenuController.MENU_CHANNEL
				&& getCODCategory() != null && getCurrentCategory().parent.type != MenuController.MENU_CHANNEL) { // VDTRMASTER-5549
			// VDTRMASTER-5798
			Object obj = MenuController.getInstance().getListHistoryObject();
			if (obj instanceof CategoryContainer && ((CategoryContainer) obj).type == MenuController.MENU_CHANNEL_ENV) {
				if (menuTreeUI.getMenu() != null) {
                    setButtons(menuTreeUI.getMenu().length > MenuTreeUI.MENU_SLOT? buttonList4_page : buttonList4, getCurrentCatalogue());
				}
			} else {
				setButtons(buttonList, getCurrentCatalogue());
			}
		} else {
			setButtons(buttonList, getCurrentCatalogue()); // Even if 'back', re-create "wishlist"
									// label
		}
		showcaseView.updateChannelAttributes();
		repaint();
	}

	public boolean isFocusOnResumeViewing() {
		return getCurrentCategory() != null
				&& (getCurrentCategory().type == MenuController.MENU_RESUME
				|| getCurrentCategory().type == MenuController.MENU_RESUME_ADULT);
	}

	public void refresh() {
		lastCategory = null;
		showCategory(menuTreeUI.getCurrentCategory(), false);
	}

	public void showcaseViewFadeout() {
		wasShowcaseView = showcaseView.fadeOut();
	}

	public void flushData() {
		menuTreeUI.reset();
		menuTreeUI.flushData();
		showcaseView.stop();
//		brandBG.flush();

		showcaseView.setVisible(false);
		mainToolUI.setVisible(false);
		karaokeView.setVisible(false);
		karaokeLabel.setVisible(false);
		karaokeTopBanner.setVisible(false);
		karaokeLeftBanner.setVisible(false);
		karaokeRightBanner.setVisible(false);
		karaokeBottomBanner.setVisible(false);

		karaokeLabel.flush();
		karaokeTopBanner.flush();
		karaokeLeftBanner.flush();
		karaokeRightBanner.flush();
		karaokeBottomBanner.flush();

		lastCategory = null;
		karaokeContainer = null;
		//partyPack = null;
		//trialPack = null;
		//trialSongs.clear();
		//purchasedPartyPack = null;
		channelContainer = null;
	}

	public MenuTreeUI getMenuTree() {
		return menuTreeUI;
	}

	public BaseElement getFocusedDataOnShowcaseView() {
		return showcaseView.getCurrentCatalogue();
	}

	/**
	 *
	 * @return if current version is same, return true others return false
	 */
	public boolean reset() {
		Object ready = CatalogueDatabase.getInstance().getRoot();
		Log.printDebug("CategoryUI, reset(), ready = " + ready);
		setBranded(Integer.MIN_VALUE);

		//R7.4 freelife VDTRMASTER-6200
		partyPack = null;
		trialPack = null;
		trialSongs.clear();
		purchasedPartyPack = null;
		
		// IMPROVEMENT
		// To wait a finish to parse a received data
		if (ready == null && CatalogueDatabase.getInstance().getLastCatalogueVersion() == 0) {
			//R7.4 freelife VDTRMASTER-6200
			//수정하기 위해서는 아래 4라인을 주석처리하고 if 바깥으로 옮겨야함. 
//			partyPack = null;
//			trialPack = null;
//			trialSongs.clear();
//			purchasedPartyPack = null;

			// menu.showLoadingAnimation();
			MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF
					.format(new Date())
					+ ", retrieveNamedTopic(), v = "
					+ isVisible());
			CatalogueDatabase.getInstance().retrieveCatalogueStructure(this,
					null);
			if (isVisible() == false) {
				MenuController.getInstance().debugList[3]
						.add(CatalogueDatabase.SDF.format(new Date())
								+ ", CategoryUI.fadeIn()");
				FrameworkMain.getInstance().getImagePool().waitForAll();
				// fadeIn.start();
			}
			return false;
		} else {
			// IMPROVEMENT
			long after, before;
			App.printDebug("CategoryUI, version check");
			String currentVersionURL = DataCenter.getInstance().getString("VCDS_URL") + "retrieveCurrentBuild";
			Log.printDebug("currentVersionURL=" + currentVersionURL);
			before = System.currentTimeMillis();
			App.printDebug("CategoryUI, version check, request start");
			byte[] src = null;
			try {
				src = URLRequestor.getBytes(currentVersionURL, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			after = System.currentTimeMillis();
			App.printDebug("CategoryUI, version check, request end, time=" + (after - before));
			if (src != null) {
				before = after;
				App.printDebug("CategoryUI, version check, parsing start");
				String buildStr = new String(src);

				Log.printDebug("buildStr=" + buildStr);

				// VDTRMASTER-5864
				int currentVersion = -1;
				try {
					String catalogueTagStr = "catalogue=\"";
					int startIdx = buildStr.indexOf(catalogueTagStr);
					int endIdx = buildStr.indexOf("\"", startIdx + catalogueTagStr.length());
					String versionStr = buildStr.substring(startIdx + catalogueTagStr.length(), endIdx).trim();
					currentVersion = Integer.parseInt(versionStr);
				} catch (Exception e) {
					e.printStackTrace();

				}

				Log.printDebug("lastCatalogueVersion=" + CatalogueDatabase.getInstance().getLastCatalogueVersion() + ", currentversion=" + currentVersion);
				after = System.currentTimeMillis();
				App.printDebug("CategoryUI, version check, parsing end, time=" + (after - before));
				before = after;
				App.printDebug("CategoryUI, version check, display start");

				if (ready == null) {
					int count = 20;
					do {
						try {
							Thread.sleep(100L);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						count--;
						ready = CatalogueDatabase.getInstance().getRoot();
					} while (ready == null && count > 0);
					Log.printDebug("CategoryUI, reset, count=" + count);
				}

				if (ready != null && CatalogueDatabase.getInstance().getLastCatalogueVersion() == currentVersion) {
					App.printDebug("CategoryUI, version check, version is same.");
//								setCurrentFocusUI(FOCUS_MENU);
//								menuTreeBG.setVisible(true);
//								menuTreeUI.start(true);
					// VDTRMASTER-5802
					menu.mainScreenReady();

					menuTreeUI.flushData();
					menuTreeUI.reset();
//								menuTreeUI.startMenuAnimation();
					menuTreeUI.setMenu(null);
					menuTreeBG.animationStarted(null);
					menuTreeUI.setVisible(true);

					try {
						showcaseView.setData(menuTreeUI.getCurrentCategory());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					showcaseView.setVisible(true);
					showcaseView.retrieveImages(false);

					// R7.3
                    if (menu.getCurrentScene() != null) {
                        updateButtons();
                    }

					super.start();
					after = System.currentTimeMillis();
					App.printDebug("CategoryUI, version check, display end, time=" + (after - before));

					// VDTRMASTER-6031
					CatalogueDatabase.getInstance().retrieveServices(null);
					return true;
				} else {

					App.printDebug("CategoryUI, version check, version is not same.");
					Log.printDebug("CategoryUI, reset, ready=" + ready);
					CatalogueDatabase.getInstance().cleanUp();
					menuTreeUI.flushData();
					menuTreeUI.setMenu(null);
					try {
						showcaseView.setData(null);
					} catch (Exception e) {
						e.printStackTrace();
					}
					CatalogueDatabase.getInstance().retrieveCatalogueStructure(this,
							null);
					return false;
				}

//				App.printDebug("CategoryUI, version check, ignore to compare the versions.");
//				CatalogueDatabase.getInstance().cleanUp();
//				CatalogueDatabase.getInstance().retrieveCatalogueStructure(this,
//						null);

			} else {
				CatalogueDatabase.getInstance().cleanUp();
				CatalogueDatabase.getInstance().retrieveCatalogueStructure(this,
						null);
				return false;
			}
		}
	}

	public void reflectVCDSResult(Object cmd) {
		Log.printDebug("CategoryUI, reflectVCDSResult(), cmd = " + cmd);

		if (cmd == null) { // first launch
			MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF
					.format(new Date()) + ", retrieveNamedTopic() done");
			// if (Resources.appStatus != VODService.APP_STARTED) {
			// Log.printDebug("VOD will be paused or called pauseXlet()");
			// stop();
			// return;
			// }
			// BookmarkManager.getInstance().organize();
			// updateButtons();

			if (Resources.appStatus == VODService.APP_PAUSED) {
				menu.hideLoadingAnimation();
				return;
			}

			// VDTRMASTER-5640
			if (menu.getParamKey() == null) {
				menu.mainScreenReady();
				MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF
						.format(new Date()) + ", check bookmark done");
			}
			// VDTRMASTER-5640
			menu.showLoadingAnimation(true);

			// VDTRMASTER-5640
//			menu.hideLoadingAnimation();

			// fadeIn.start();
			int retry = 0;
			while ((menu.getCurrentScene() instanceof CategoryUI == false || isVisible() == false) && retry++ < 20) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {}
			}
			Log.printDebug("CategoryUI, reflectVCDSResult(), wait for scene ready, " + retry);
			if (menuTreeUI.setMenu(null)) {

				if (menu.getParamKey() != null) {
					setVisible(false);
					if (menu.getParamKey().equals(MenuController.MENU_PARAM_WISHLIST)) {
						menu.resetParamKey();
						menuTreeUI.moveToWishlist();

						// VDTRMASTER-5574
						if (BookmarkManager.getInstance().retrieveWishlistTitles(true).length == 0) {
							categoryChanged(menuTreeUI.getCurrentCategory());
							setVisible(true);
							menuTreeUI.setVisible(true);
							menuTreeBG.setVisible(true);
						}
					} else if (menu.getParamKey().equals(MenuController.MENU_PARAM_RESUMEVIEWING)) {
						menu.resetParamKey();
						menuTreeUI.moveToResumeViwing();

						// VDTRMASTER-5574
						if (BookmarkManager.getInstance().retrieveActivePurchases(true).length == 0) {
							categoryChanged(menuTreeUI.getCurrentCategory());
							setVisible(true);
							menuTreeUI.setVisible(true);
							menuTreeBG.setVisible(true);
						}
					}

					// R7
					// VDTRMASTER-5774
					menu.mainScreenReady();
					MenuController.getInstance().debugList[3].add(CatalogueDatabase.SDF
							.format(new Date()) + ", check bookmark done");

					// menu.hideLoadingAnimation();
//					menu.hideLoadingAnimation();
				} else if (menu.isGotoCOD() && menuTreeUI.gotoChannelOnDemand()) {
				} else {
					menuTreeBG.start();
					PromotionBanner.getInstance().startMonitoring();
					PromotionBanner.getInstance().setRotationTimer(true);
				}

//				menuTreeBG.start();
//				PromotionBanner.getInstance().startMonitoring();
//				PromotionBanner.getInstance().setRotationTimer(true);
			}

			// VDTRMASTER-5640
			menu.hideLoadingAnimation();
			repaint();

			// VDTRMASTER-6031
			CatalogueDatabase.getInstance().retrieveServices(null);
		} else if (cmd instanceof CategoryContainer[]) {
			menu.hideLoadingAnimation();
			EventQueue.invokeLater(new Runnable() { // animation
						public void run() {
							menuTreeUI.keyOK_afterEffect();
						}
					});
		} else if (MenuTreeUI.MENU_KARAOKE.equals(cmd)) {
			menu.hideLoadingAnimation();
			CategoryContainer container = getKaraokeContainer();
			if (container != null) {
//				container.topicPointer[0].topic.dispose();
				container.topicPointer[0].topic = CatalogueDatabase
						.getInstance().getKaraokeTopic();
			}
			Topic karaokeTopic = CatalogueDatabase.getInstance()
					.getKaraokeTopic();
			karaokeTopic.needRetrieve = false;
			for (int i = 0; i < karaokeTopic.treeRoot.categoryContainer.length; i++) {
				if (karaokeTopic.treeRoot.categoryContainer[i].categorizedBundle
						.isFree()) {
					setTrialPack(karaokeTopic.treeRoot.categoryContainer[i]);
				} else {
					setPartyPack(karaokeTopic.treeRoot.categoryContainer[i]);
				}
			}

			// update karaoke banners
			updateKaraokeBanners();
		} else if (MenuTreeUI.MENU_KARAOKE_CONTENT.equals(cmd)) {
//			showCategory(getCurrentCategory(), false);
			menu.hideLoadingAnimation();
			if (getCurrentCategory() != null && getCurrentCategory().parent == trialPack) {
				addTrialSongs(getCurrentCategory());
			}
			refresh();
		} else if (cmd.toString().startsWith(CatalogueDatabase.FAIL)) {
			needLoading = false;
			CommunicationManager.getInstance().errorMessage(
					cmd.toString().substring(CatalogueDatabase.FAIL.length()),
					errorListner);
			menu.hideLoadingAnimation();
		}
	}

	private ErrorMessageListener errorListner = new ErrorMessageListener() {
		public void actionPerformed(int buttonType) throws RemoteException {
			menu.hideLoadingAnimation();
			// MenuController.getInstance().exit(MenuController.EXIT_BY_ERROR,
			// null);
			if (menu.getLastScene() != null && isTop() && CatalogueDatabase.getInstance().getRoot() == null) {
				MenuController.getInstance().exit(MenuController.EXIT_BY_ERROR, null);
			}
		}
	};

	public boolean handleKey(int key) {
		if (getPopUp() != null) {
			return getPopUp().handleKey(key);
		}
		boolean used = false;
		switch (key) {
		case KeyEvent.VK_BACK_SPACE:
		case KeyCodes.LAST:
			setCurrentFocusUI(FOCUS_MENU);
			if (menuTreeUI.isTop()) {
				if (instance != this) {
					if (menu.getParentUI() != null) {
						setChannel(null);
						setNetwork(null);
						setChannelCategory(null);
						setBrandingElement(null, true);
						BaseRenderer.flushBrandingBG();
						menu.backwardHistory(false);
						menu.back();
						// 20190222 Touche project
						VbmController.getInstance().backwardHistory();		
					} else {
						if (menu.getFrom() != null && menu.getFrom().equals(MenuController.EPG) && menu.needToBackEPG()) {
							VbmController.getInstance().writeExitedWithBack();
							CommunicationManager.getInstance().startUnboundApplication(MenuController.EPG,
									new String[] { MonitorService.REQUEST_APPLICATION_LAST_KEY, Resources.APP_NAME});
							return true;
						}
						menu.goToMainScene(true);
					}
					return true;
					// VDTRMASTER-5796
				} else if (MenuController.getInstance().getHistorySize() > 0
						|| CatalogueDatabase.getInstance().getRoot() == null) {
					menu.goToMainScene(true);
					return true;
				}
			} else {
				showcaseView.fadeOut();
			}
			menuTreeUI.handleKey(key);
			used = true;
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			used = true;
			break;
		case KeyCodes.COLOR_A:
		    // R7.3 - removed
//			if (getCurrentCategory() != null && getCurrentCategory().type == MenuController.MENU_CHANNEL) {
//				menuTreeUI.changeChannelMode();
//				setCurrentFocusUI(FOCUS_MENU);;
//				updateButtons();
//				if (menuTreeUI.getChannelMode() == MenuTreeUI.CH_MODE_ALL) {
//					VbmController.getInstance().writeChannelSortOption(DataCenter.getInstance().getString(Resources.TEXT_VIEW_ALL_CHANNEL));
//				} else {
//					VbmController.getInstance().writeChannelSortOption(DataCenter.getInstance().getString(Resources.TEXT_VIEW_MY_CHANNEL));
//				}
//			}
//			used = true;
			break;
		case KeyCodes.LITTLE_BOY:
		case OCRcEvent.VK_F4:
			Log.printDebug("CategoryUI, handleKey(int), LITTLE_BOY");
			// R5 - channel content
			// VDTRMASTER-5726
			if ((currentFocusUI == FOCUS_MENU && getCurrentCategory().type == MenuController.MENU_CHANNEL)
			|| (currentFocusUI == FOCUS_RIGHT && getCurrentCategory().type == MenuController.MENU_CHANNEL_ENV)) {
				Topic topic = null;
				if (getCurrentCategory().type == MenuController.MENU_CHANNEL
						&& getCurrentCategory().getTopic() != null) {
					topic = getCurrentCategory().getTopic();
				} else if (getCurrentCategory().type == MenuController.MENU_CHANNEL_ENV && currentFocusUI == FOCUS_RIGHT
						&& getFocusedDataOnShowcaseView() instanceof Showcase
						&& ((Showcase)getFocusedDataOnShowcaseView()).detail.getTopic() != null) {
					topic = ((Showcase)getFocusedDataOnShowcaseView()).detail.getTopic();
				}

				Log.printDebug("CategoryUI, handleKey(int), LITTLE_BOY, topic=" + topic);

				if (topic != null) {
					footer.clickAnimation(BaseUI.imgProfile);
					ChannelInfoManager chManager = ChannelInfoManager.getInstance();

					// VDTRMASTER-5661
					if (chManager.isFavoriteChannel(topic)) {
						chManager.removeFavoritehannel(topic);
					} else if (chManager.isSubscribed(topic)){
						chManager.addFavoritehannel(topic, false);
					}
				}
			}

			// R5 - normal content
			if (currentFocusUI == FOCUS_RIGHT && showcaseView.isVisible()) {
				BaseElement element;
				if (getFocusedDataOnShowcaseView() instanceof Showcase) {
					element = ((Showcase)getFocusedDataOnShowcaseView()).getContent();
				} else {
					element = getFocusedDataOnShowcaseView();
				}

				if (element != null) {
					String btnName = getWishButtonName(element);

					if (btnName != null) {
						footer.clickAnimation(BaseUI.imgProfile);
						menu.processActionMenu(btnName, element);
					}
				}
			} else if (mainToolUI.isVisible() && currentFocusUI == FOCUS_RIGHT) {
				BaseElement element =  mainToolUI.getCurrentCatalogue();

				if (element != null) {
					String btnName = getWishButtonName(element);

					if (btnName != null) {
						footer.clickAnimation(BaseUI.imgProfile);
						menu.processActionMenu(btnName, element);
					}
				}
			}
			used = true;
			break;
		default:
			if (currentFocusUI == FOCUS_MENU) {
				// if (key == KeyEvent.VK_ENTER) {
				// showcaseView.fadeOut();
				// }
				used = menuTreeUI.handleKey(key);
			} else {
				scroll = 0;
				if (showcaseView.isVisible()) {
					used = showcaseView.handleKey(key);
					updateButtons();
				} else if (karaokeView.isVisible()) {
					used = karaokeView.handleKey(key);
				} else {
					used = mainToolUI.handleKey(key);
				}
			}
			break;
		}
		return used;
	}

	public void moveToSubCategory(CategoryContainer category) {
		CategoryContainer[] subs = category.subCategories;
		if (category.getTopic() != null) {
			subs = category.getTopic().treeRoot.categoryContainer;
		}
		menuTreeUI.moveToSubMenu(category, subs);
//		if (((CategoryUI) MenuController.getInstance().getCurrentScene()).isBranded()) {
//			((CategoryUI) MenuController.getInstance().getCurrentScene()).setBranded(1);
//		}
		setCurrentFocusUI(CategoryUI.FOCUS_MENU);
	}

//	public void moveToChannel(CategoryContainer category) {
//		menuTreeUI.moveToChannel(category);
//		setCurrentFocusUI(CategoryUI.FOCUS_MENU);
//	}

	private void moveToKaraokePackage(CategoryContainer target) {
		if (target != null) {
			setCurrentFocusUI(CategoryUI.FOCUS_MENU);
			if (target == getCurrentCategory()) {
				keyOkOnMenu(false);
			} else if (getCurrentCategory() != null && getCurrentCategory().type == MenuController.MENU_KARAOKE_CONTENTS) {
				menuTreeUI.moveToUpperMenuForce();
				lastCategory = null;
				menuTreeUI.moveToSubMenu(target, target.subCategories);
			} else {
				menuTreeUI.moveToSubMenu(target, target.subCategories);
			}
		} else {
			Log.printWarning("pack not found");
		}
	}
	public void moveToKaraokePackage(final boolean party) {
		CategoryContainer target = party ? partyPack : trialPack;
		Log.printDebug("moveToKaraokePackage, " + party + ", p = "
				+ partyPack + ", t = " + trialPack);

		if (target == null) {
			menu.showLoadingAnimation();
			final CategoryUI listener = this;
			new Thread("retrieveKaraoke") {
				public void run() {
					synchronized (CatalogueDatabase.getInstance()) {
						CatalogueDatabase.getInstance().retrieveKaraoke(
								listener, null, 2);
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {}
					}
					menu.hideLoadingAnimation();
					moveToKaraokePackage(party ? partyPack : trialPack);
				}
			}.start();
		} else { // already retrieved
			moveToKaraokePackage(target);
		}
	}

	public void keyOkOnMenu(boolean quiet) {
		if (quiet) {
			// menuTreeUI.moveToSubMenuQuiet();
		} else {
			menuTreeUI.keyOK_afterEffect();
		}
		setCurrentFocusUI(CategoryUI.FOCUS_MENU);
	}

	public BaseElement getCurrentCatalogue() {
		if (showcaseView.isVisible() && currentFocusUI == FOCUS_RIGHT) {
			BaseElement be = showcaseView.getCurrentCatalogue();
			if (be instanceof Showcase) {
				return ((Showcase) be).getContent();
			} else {
				return showcaseView.getCurrentCatalogue();
			}
		} else if (mainToolUI.isVisible() && currentFocusUI == FOCUS_RIGHT) {
			return mainToolUI.getCurrentCatalogue();
		} else if (menuTreeUI.isVisible() && currentFocusUI == FOCUS_MENU) {
			return menuTreeUI.getCurrentCategory();
		}
		return null;
	}

	public void keyGreen() {
		Log.printDebug("CategoryUI, keyGreen()");
		setPopUp(optionBox);
		optionBox.start(menu.getOptionMenu(0, getCurrentCatalogue()), menu);
		repaint();
	}

	public boolean isTop() {
		// VDTRMASTER-5796
		return menuTreeUI.isTop() && MenuController.getInstance().getHistorySize() == 0;
	}

    boolean isFocusOnSearch = false;
    public void setFocusOnSearch(boolean focus) {
        isFocusOnSearch = focus;
    }
    public boolean isFocusOnSearch() {
        return isFocusOnSearch;
    }
    // Touche project, VDTRMASTER-6252,VDTRMASTER-6253
    public boolean isFocusOnShowcase() {
    	if (currentFocusUI == FOCUS_MENU) {
			return false;
		} else if (currentFocusUI == FOCUS_RIGHT){
			if (showcaseView.isVisible()) {
				return true;
			}
		}
    	return false;
    }

	public void categoryChanged(CategoryContainer category) {
		if (category == null) {
			Log.printWarning("CategoryUI, categoryChanged(), NULL");
			return;
		}
		Log.printDebug("CategoryUI, categoryChanged()" + ", category = "
				+ category.toString(false) + ", type = " + category.type
				+ ", list = " + changeCategory);
		if (CatalogueDatabase.getInstance().imageRetriever[CatalogueDatabase.IMAGE_RETRIEVER_SM] != null) {
			CatalogueDatabase.getInstance().imageRetriever[CatalogueDatabase.IMAGE_RETRIEVER_SM].interrupt();
			CatalogueDatabase.getInstance().imageRetriever[CatalogueDatabase.IMAGE_RETRIEVER_SM] = null;
		}

		int cnt = 0;
		while (cnt++ < 10 && (own == null || own.isAlive() == false || changeCategory == null)) {
			Log.printDebug("CategoryUI, categoryChanged(), wait for the queue list");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}

		if (changeCategory != null) {
			synchronized (changeCategory) {
				changeCategory.add(category);
				changeCategory.notifyAll();
				Log.printDebug("CategoryUI, categoryChanged(), added the event and notified");
			}
		}
	}

	public void run() {
		Log.printDebug("CategoryUI, thread start");

		int wait = 750;
		CategoryContainer category = null;
		long timeStamp = 0;

		repaint();

		while (own != null) {
            if (changeCategory == null) {
                changeCategory = new ArrayList();
            }
            synchronized (changeCategory) {
                if (changeCategory.isEmpty()) {
                    try {
                        changeCategory.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
            long gap = System.currentTimeMillis() - timeStamp;
            if (gap < 100) {
                gap += (wait - 100);
            }
            if (gap > 0 && gap < wait) {
                boolean waitMore = true;
                while (waitMore) {
                    int size = changeCategory.size();
                    synchronized (changeCategory) {
                        try {
                            changeCategory.wait(gap);
                        } catch (InterruptedException e) {
                        }
                    }
                    waitMore = size != changeCategory.size();
                }
            }
            if (changeCategory.isEmpty() || own == null) {
                break;
            }
            category = (CategoryContainer) changeCategory.get(changeCategory
                    .size() - 1);
            changeCategory.clear();
            showCategory(category, false);
			timeStamp = System.currentTimeMillis();
		} // end of while

		changeCategory.clear();
		changeCategory = null;
		Log.printDebug("CategoryUI, thread done");
	}

	private void showCategory(final CategoryContainer category, boolean back) {
		Log.printDebug("showCategory, cat = " + category + ", back = " + back
				+ ", last = " + lastCategory + ", cur = " + menu.getCurrentScene());
		if (category == null) {
			Log.printDebug("showCategory, return 1");
			return;
		}
		if (back == false && category.equals(lastCategory)) {
			Log.printDebug("showCategory, return 2");
			return;
		}
		if (isTop() && category.isAdult == false) {
			MenuController.getInstance().setOpenAdultCategory(false, false);
		}
		showcaseView.setVisible(false);
		mainToolUI.setVisible(false);
		karaokeView.setVisible(false);
		karaokeLabel.setVisible(false);
		karaokeTopBanner.setVisible(false);
		karaokeLeftBanner.setVisible(false);
		karaokeRightBanner.setVisible(false);
		karaokeBottomBanner.setVisible(false);
		if (category.type == MenuController.MENU_KARAOKE_CONTENTS) {
			if (category.videoContainer.length == 0 && category.needRetrieve) { // not retrieved
				category.needRetrieve = false;
				ArrayList list = new ArrayList();
				if (partyPack != null) {
					list.add(partyPack.id);
				}
				if (trialPack != null) {
					list.add(trialPack.id);
				}
				menu.showLoadingAnimation();
				//CatalogueDatabase.getInstance().retrieveCategorizedBundles(this, list, MenuTreeUI.MENU_KARAOKE_CONTENT);
				CatalogueDatabase.getInstance().retrieveCategoryContents(this, category.id, MenuTreeUI.MENU_KARAOKE_CONTENT);
			}
			int height = karaokeView.setData(category);
			int y = 484 - height;
			karaokeView.setBounds(373, y, 537, height);
			karaokeView.setVisible(true);
			ArrayList banner = new ArrayList();
			Image img = null;
			ImageGroup ig = null;
			String lang = Resources.currentLanguage == 0 ? Definition.LANGUAGE_EN
					: Definition.LANGUAGE_FR;
			for (int i = 0; i < category.category.description.imageGroup.length; i++) {
				ig = category.category.description.imageGroup[i];
				if (ImageRetriever.BANNIERE_1.equals(ig.usage)) {
					if (lang.equals(ig.language)) {
						banner.add(0, ig);
					} else {
						banner.add(ig);
					}
				}
			}
			if (banner.size() > 0) {
				ig = (ImageGroup) banner.get(0);
				img = ig.images[0];
				String url = dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE
						+ img.imageName + "." + ig.encoding;
				karaokeLabel.setImage(url);
				karaokeLabel.setVisible(true);
				karaokeLabel.setBounds(373, y - img.height, img.width,
						img.height);
			}

			if (getPurchasedPartyPack() == null) { // ready for order banner
				banner.clear();
				img = null;
				ig = null;

				for (int i = 0; i < category.parent.categorizedBundle.description.imageGroup.length; i++) {
					ig = category.parent.categorizedBundle.description.imageGroup[i];
					if (ImageRetriever.ORDER_PAK_WIDE.equals(ig.usage)) {
						if (lang.equals(ig.language)) {
							banner.add(0, ig);
						} else {
							banner.add(ig);
						}
					}
				}

				if (banner.size() > 0) {
					ig = (ImageGroup) banner.get(0);
					img = ig.images[0];
					String url = dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE
							+ img.imageName + "." + ig.encoding;
					karaokeView.setOrderBanner(url);
				}
			}

		} else if ((menu.openAdultCategory() == false && category.isAdult)
				|| category.type < MenuController.MENU_CATEGORY) {
			mainToolUI.setData(category);
			if (category.type == MenuController.MENU_KARAOKE) {
				setKaraokeContainer(category);
				if (category.getTopic().needRetrieve) { // need retrieve
					menu.showLoadingAnimation();
					synchronized (CatalogueDatabase.getInstance()) {
						CatalogueDatabase.getInstance().retrieveKaraoke(this,
								category.getTopic().namedTopic, 1);
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {
						}
//						PreferenceProxy.getInstance().loadPlaylist();
					}
					menuTreeUI.repaint();
				}
				karaokeTopBanner.setVisible(true);
				if (getPurchasedPartyPack() != null) { // draw subscribed large
														// banner
					karaokeBottomBanner.setVisible(true);
					mainToolUI.setMaxCnt(1);
				} else {
					karaokeLeftBanner.setVisible(true);
					karaokeRightBanner.setVisible(true);
					mainToolUI.setMaxCnt(3);
				}
				updateKaraokeBanners();
			} else if (category.type == MenuController.MENU_KARAOKE_PACK) {
				karaokeTopBanner.setVisible(true);
				if (getPurchasedPartyPack() != null) { // draw subscribed large
														// banner
					karaokeBottomBanner.setVisible(true);
					mainToolUI.setMaxCnt(1);
				} else {
					karaokeLeftBanner.setVisible(true);
					karaokeRightBanner.setVisible(true);
					mainToolUI.setMaxCnt(3);
				}
				updateKaraokeBanners();
			}
			if (EventQueue.isDispatchThread() == false) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						mainToolUI.setVisible(true);
					}
				});
			} else {
				mainToolUI.setVisible(true);
			}

		} else {
			if (back == false) {
				try {
					showcaseView.setData(category);
				} catch (Exception e) {
					Log.print(e);
				}
			}
			if (EventQueue.isDispatchThread() == false) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						if (isVisible() && menu.getCurrentScene() instanceof CategoryUI) {
							if (menu.isShadowed()) {
								showcaseView.setVisible(true);
							} else {
								showcaseView.fadeIn();
							}
							showcaseView.retrieveImages(false);
						}

					}
				});
			} else {
				if (menu.getCurrentScene() instanceof CategoryUI) {
					if (menu.isShadowed()) {
						showcaseView.setVisible(true);
					} else {
						showcaseView.fadeIn();
					}
					showcaseView.retrieveImages(false);
				}
			}
		}
		lastCategory = category;
	}

	public CategoryContainer getCurrentCategory() {
		return menuTreeUI.getCurrentCategory();
	}

	public CategoryContainer getCODCategory() {
		return menuTreeUI.getCODCategory();
	}

	static Channel lastChannelElement;
	static BrandingElement brandingElement;
	public static BaseElement filterElement; // Themetic Structure
	public static int brandingDepth = 0;

	public static void setBranded(int go) {
		if (Log.DEBUG_ON) {
			Log.printDebug("setBranded(), branded depth = "	+ brandingDepth +
					", go = " + go + ", br = " + (brandingElement == null ? "null" : brandingElement.toString(true)));
		}
		if (go == Integer.MAX_VALUE) { // just check again

		} else if (go > 0) { // move into child
			brandingDepth++;
		} else if (go == 0) {
			brandingDepth = 0;
		} else { // move out to parent
			brandingDepth = Math.max(0, brandingDepth + go);
		}

		if (go < 0 && brandingDepth == 0) {
			lastChannelElement = null;
			brandingElement = null;
			filterElement = null;
			BaseRenderer.setBrandingBg(-1, null);
			BaseRenderer.flushBrandingBG();
			MenuTreeUI.MENU_SLOT = 11;
		} else {
			MenuTreeUI.MENU_SLOT = brandingElement != null && brandingElement.getLogo(BrandingElement.LOGO_MD) != null ? 10 : 11;
		}
	}

	public boolean isSingleton() {
		return instance == this;
	}
	public static boolean isBranded() {
		return brandingElement != null;
	}

	public static void setChannelCategory(CategoryContainer ca) {
		channelContainer = ca;
	}

	public static CategoryContainer getChannelCategory() {
		return channelContainer;
	}

	public static void setNetwork(final Network nw) {
		Log.printDebug("setNetwork, nw = " + (nw == null ? "" : nw.toString(true)));
		lastChannelElement = nw == null || nw.channel.length == 0 ? null : nw.channel[0];

		if (nw == null) {
			return;
		}

		for (int j = 0; j < nw.channel.length; j++) {
			if (nw.channel[j].hasAuth == false) {
				nw.channel[j].hasAuth = CommunicationManager.getInstance()
						.checkAuth(nw.channel[j].callLetters);
			}
			if (nw.channel[j].hasAuth) {
				lastChannelElement = nw.channel[j];
				break;
			}
		}
		Log.printDebug("setNetwork, setuped channel = " + lastChannelElement.toString(true) + ", instance = " + lastChannelElement);

//		setBrandingElement(nw, true);

//		ImageGroup bg = nw.getBackground(false);
//		Log.printDebug("setNetwork, bg = " + bg.toString(true));
////		brandBG.setImage(dataCenter.getString("IMAGE_URL")
////				+ ImageRetriever.BASE + bg.images[0].imageName + "."
////				+ bg.encoding);
////		brandBG.setVisible(true);
//		BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL")
//				+ ImageRetriever.BASE + bg.images[0].imageName + "."
//				+ bg.encoding);
//		new Thread("loadLogo=" + nw.id) {
//			public void run() {
//				nw.loadLogo(false, false);
//			}
//		}.start();
	}

	public static Channel getChannel() {
		return lastChannelElement;
	}

	public static void setChannel(final Channel ch) {
		Log.printDebug("setChannel, ch = " + (ch == null ? "null": ch.toString(true)));
		lastChannelElement = ch;

		if (ch == null) {
			return;
		}

		ch.hasAuth = CommunicationManager.getInstance().checkAuth(ch.callLetters);

//		setBrandingElement(ch, true);
//		ImageGroup bg = ch.getBackground(false);
//		Log.printDebug("setChannel, bg = " + (bg == null ? "NULL" : bg.toString(true)));
////		brandBG.setImage(dataCenter.getString("IMAGE_URL")
////				+ ImageRetriever.BASE + bg.images[0].imageName + "."
////				+ bg.encoding);
////		brandBG.setVisible(true);
//		if (bg == null) {
//			return;
//		}
//		BaseRenderer.setBrandingBg(1, DataCenter.getInstance().getString("IMAGE_URL")
//				+ ImageRetriever.BASE + bg.images[0].imageName + "."
//				+ bg.encoding);
//		new Thread("loadLogo=" + ch.callLetters) {
//			public void run() {
//				ch.loadLogo(false, false);
//			}
//		}.start();
	}

	public static boolean setBrandingElementByCategory(CategoryContainer category) {
		Log.printDebug("setBrandingElementByCategory, category = " + category);
		if (category == null) {
			return false;
		}
		boolean ret = false;
//		filterElement = null;
		if (category.service != null && category.service.serviceType == Service.SVOD) {
			setBrandingElement(category.service.description, true);
			filterElement = category.service;
			ret = true;
		} else if (category.hasBranding()) {
			setBrandingElement(category.category.description, true);
			// VDTRMASTER-5476
			filterElement = null;
			if (category.getTopic() != null) {
				if (category.getTopic().channel != null) {
					setBrandingElement(category.getTopic().channel, true);
					filterElement = category.getTopic().channel;
					ret = true;
				} else if (category.getTopic().network != null) {
					setBrandingElement(category.getTopic().network, true);
					filterElement = category.getTopic().network;
					ret = true;
				}
			}
			ret = true;
		} else if (category.type == MenuController.MENU_VSD) {
			setBrandingElement(category.getTopic(), true);
			if (category.getTopic() != null) {
				if (category.getTopic().channel != null) {
					CategoryUI.filterElement = category.getTopic().channel;
				} else if (category.getTopic().network != null) {
					CategoryUI.filterElement = category.getTopic().network;
				}
			}
			ret = true;
		} else if (category.getTopic() != null) {
			if (category.getTopic().channel != null) {
				setBrandingElement(category.getTopic().channel, true);
				filterElement = category.getTopic().channel;
				ret = true;
			} else if (category.getTopic().network != null) {
				setBrandingElement(category.getTopic().network, true);
				filterElement = category.getTopic().network;
				ret = true;
			}
		}
		Log.printDebug("setBrandingElementByCategory, category = " + category + ", br = "
				+ (brandingElement == null ? "null" : brandingElement.toString(true)) + ", ret = " + ret);
		return ret;
	}

	public static void setBrandingElement(BrandingElement br, boolean update) {
		if (br == null) {
			brandingElement = null;
			filterElement = null;
			Log.printDebug("setBrandingElement, br = NULL");
			return;
		}
		if (brandingElement == br) {
			BaseRenderer.setBrandingBg(update ? 1 : -1, null);
			Log.printDebug("setBrandingElement, SAME br = " + br.toString(true));
			return;
		}

		brandingElement = br;
		Log.printDebug("setBrandingElement, br = " + br.toString(true));
		ImageGroup bg = br.getBackground(false);
		Log.printDebug("setBrandingElement, bg = " + (bg == null ? "NULL" : bg.toString(true)));
		if (bg != null && MenuController.getInstance().getCurrentScene() instanceof TitleDetailUI == false) {
			BaseRenderer.setBrandingBg(1, DataCenter.getInstance().getString("IMAGE_URL")
					+ ImageRetriever.BASE + bg.images[0].imageName + "."
					+ bg.encoding, update);
		}

		final BrandingElement brForLoad = br;
		new Thread("loadLogo=" + br.toString(true)) {
			public void run() {
				brForLoad.loadLogo(false, false);
			}
		}.start();
	}

	public static BrandingElement getBrandingElement() {
		return brandingElement;
	}

	public void receiveCheckRightFilter(CheckResult checkResult) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CategoryUI, receiveCheckRightFilter()"
					+ ", checkResult = " + checkResult + ", filter = "
					+ checkResult.getFilter() + ", response = "
					+ checkResult.getResponse());
		}
		if (PreferenceService.RESPONSE_SUCCESS != checkResult.getResponse()) {
			return;
		}
		if (RightFilter.BLOCK_BY_RATINGS.equals(checkResult.getFilter())) {
			// VDTRMASTER-5692
			showcaseView.updateChannelAttributes();
			handleKey(KeyEvent.VK_ENTER);
		} else {
			showcaseView.updateChannelAttributes();
		}
	}

	public void setCurrentFocusUI(int currentFocusUI) {
		Log.printDebug("CategoryUI, setCurrentFocusUI, currentFocusUI" + currentFocusUI);
		if (this.currentFocusUI == FOCUS_MENU && currentFocusUI == FOCUS_RIGHT) {
			if (showcaseView.isVisible()) {
				showcaseView.setFocus(0);
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						showcaseView.checkFocusArrow();
						showcaseView.repaint();
					}
				});
			} else if (mainToolUI.isVisible()) {
				mainToolUI.setFocus(0);
			}
		}
		
		super.setCurrentFocusUI(currentFocusUI);
		updateButtons();
	}

	public void updateFavoriteChannel() {
		if (menuTreeUI.getCurrentCategory().type == MenuController.MENU_CHANNEL) {
			// VDTRMASTER-5817
			Object obj = MenuController.getInstance().getListHistoryObject();
			if (obj instanceof CategoryContainer && ((CategoryContainer) obj).type == MenuController.MENU_CHANNEL_ENV) {
				menuTreeUI.findNewMenuIdx();
			} else { // R7
				ChannelInfoManager.needToUpdate = true;
			}
		} else if (menuTreeUI.getCurrentCategory().type != MenuController.MENU_CHANNEL_ENV){
			ChannelInfoManager.needToUpdate = true;
		} else {
			ChannelInfoManager.needToUpdate = false;
		}

		showcaseView.updateChannelAttributes();
		showcaseView.repaint();

		if (getCurrentCategory().type == MenuController.MENU_CHANNEL_ENV && currentFocusUI == FOCUS_RIGHT
				&& getFocusedDataOnShowcaseView() instanceof Showcase
				&& ((Showcase)getFocusedDataOnShowcaseView()).detail.getTopic() != null) {
			Topic topic = ((Showcase)getFocusedDataOnShowcaseView()).detail.getTopic();

			ChannelInfoManager.getInstance().updateFavoriteData(topic);
		}

	}

	public void updateBlockChannel() {
		showcaseView.updateChannelAttributes();
		showcaseView.repaint();
	}

	public ShowcaseView getShowcaseView() {
		return showcaseView;
	}

	private class MenuTreeBG extends BaseUI {
		// Effect first;

		public MenuTreeBG() {
			setRenderer(r);
		}

		public void start() {
			// if (first == null) {
			// first = new MovingEffect(this, 12, new Point(0, 50), new Point(
			// 0, 0), MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
			// }
			// setVisible(false);
			// first.start();
			animationEnded(null);
		}

		public void animationEnded(Effect effect) {
			setVisible(true);
			menuTreeUI.startMenuAnimation();
		}

		BaseRenderer r = new BaseRenderer() {
			Rectangle rec = new Rectangle(51, 55, 520, 450);

			public Rectangle getPreferredBounds(UIComponent c) {
				return rec;
			}

			protected void paint(Graphics g, UIComponent c) {
				if (isBranded() || hasBrandingBg()) {
					g.setColor(new Color(40, 40, 40));
					g.fillRect(1, 16, 296, 407);
					g.drawImage(dataCenter.getImage(isBranded() && MenuTreeUI.MENU_SLOT == 10 ? "01_ch_info_high.png" : "00_menubg.png"), 1, 16, c);
				}
			}
		};
	}
}

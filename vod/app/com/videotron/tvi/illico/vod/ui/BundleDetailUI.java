package com.videotron.tvi.illico.vod.ui;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.Vector;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.effect.ArrowEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ActionBox;
import com.videotron.tvi.illico.vod.comp.PosterArray;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.gui.BundleDetailRenderer;

public class BundleDetailUI extends BaseUI {
	private static final long serialVersionUID = 4156056753952944855L;

	public static final int FOCUS_POSTERS = 0;
	public static final int FOCUS_BUNDLE = 1;

	private BundleDetailRenderer bdRenderer = new BundleDetailRenderer();

	private Bundle bundle;
	private BaseElement[] catalogueList;

	private ScrollTexts scrollText = new ScrollTexts();
	private static Rectangle recScrollText = new Rectangle(237, 388, 357, 88);//237, 420, 365, 60);
	private static Rectangle recScrollText2 = new Rectangle(237, 371, 357, 105);
	private static int infoPageRowHeight = 17;
	private Color cScrollTextColor = new DVBColor(255, 255, 255, 230);
	private int[] buttonList = new int[] { BaseUI.BTN_SCROLL, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST,
			BaseUI.BTN_OPTION };

	private PosterArray posterArray;
	private ActionBox actionBox;
	private OptionScreen optionBox = new OptionScreen();

	private ArrowEffect arrowEffect;

	public BundleDetailUI() {
		scrollText.setTopMaskImage(dataCenter.getImage("01_des_sh_t03.png"));
		scrollText.setBottomMaskImage(dataCenter.getImage("01_des_sh_bun.png"));
		add(scrollText);

		actionBox = ActionBox.newInstance();
		add(actionBox);

		posterArray = PosterArray.newInstance();
		posterArray.setBounds(236-63, 100, 960 - (236-63), 230);
		posterArray.setFont(FontResource.BLENDER.getFont(16));
		posterArray.setOverflow(true);
		posterArray.prepare();
		add(posterArray, 0);

		Image btnPage = (Image) ((Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_PAGE);
		footer.linkWithScrollTexts(btnPage, scrollText);
		add(footer);

//		ImageLabel rightShadow = new ImageLabel(dataCenter.getImage("01_poster_sha_r.png"));
//		rightShadow.setLocation(856, 103);
//		add(rightShadow, 0);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);

		arrowEffect = new ArrowEffect(this, 0);
	}

	public void start(boolean back) {
		if (!back) {
			Object[] params = MenuController.peekStack();
			Object param = params[MenuController.PARAM_DATA_IDX];
			Log.printDebug("start(), param = " + param);
			if (param instanceof Bundle) {
				bundle = (Bundle) param;
			} else if (param instanceof String) {
				bundle = (Bundle) CatalogueDatabase.getInstance().getCached(
						param);
			}
			if (bundle != null) {
				catalogueList = bundle.getTitles();
			}

			setCurrentFocusUI(FOCUS_BUNDLE);
			setRenderer(bdRenderer);
			prepare();
			setState(STATE_DEACTIVATED);

			posterArray.setRowCol(1, 5);
			posterArray.setFocusMode(true);
			if (bundle != null && catalogueList != null) {
				posterArray.setData(catalogueList);
			}

			if (bundle != null && catalogueList != null) {
				setTitleDetail();
	
				CatalogueDatabase.getInstance().retrieveLargeImage(this, bundle, false);
				CatalogueDatabase.getInstance().retrieveImages(this, catalogueList);
			}
		} else {
			Object next = menu.getNext();
			Log.printDebug("start(), next = " + next);
			
			if (next != null) {
				if (next instanceof Bundle) {
					bundle = (Bundle) next;
				} else if (next instanceof String) {
					bundle = (Bundle) CatalogueDatabase.getInstance().getCached(
							next);
				}
				if (bundle != null) {
					catalogueList = bundle.getTitles();
				}
	
				setCurrentFocusUI(FOCUS_BUNDLE);
				setRenderer(bdRenderer);
				prepare();
				setState(STATE_DEACTIVATED);
	
				posterArray.setRowCol(1, 5);
				posterArray.setFocusMode(true);
				if (bundle != null && catalogueList != null) {
					posterArray.setData(catalogueList);
				}
				
				if (bundle != null && catalogueList != null) {
					setTitleDetail();
		
					CatalogueDatabase.getInstance().retrieveLargeImage(this, bundle, false);
					CatalogueDatabase.getInstance().retrieveImages(this, catalogueList);
				}
			}
		}
//		Channel channel = null;
//		ImageGroup bg = null;
		
		BaseRenderer.setBrandingBg(0, null);
		ImageGroup bg = null;
		if (CategoryUI.isBranded()) {
			BrandingElement br = CategoryUI.getBrandingElement();
			bg = br.getBackground(true);
			try {
				BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
			} catch (Exception e) {
				BaseRenderer.setBrandingBg(-1, null);
			}
		}
//		try {
//			channel = CategoryUI.getChannel();
//			bg = channel.getBackground(true);
//			if (channel.network != null) {
//				bg = channel.network.getBackground(true);
//			} else {
//				bg = channel.getBackground(true);
//			}
//			//brandBG.setImage(dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//			BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//			//setButtons(catalogueList.length > 7 ? buttonListWithoutChangeView : buttonListWithoutChangeView2);
//		} catch (Exception e) {
//			try {
//				BrandingElement br = CategoryUI.getInstance().getBrandingElement();
//				bg = br.getBackground(true);
//				//brandBG.setImage(dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//				BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//				//setButtons(catalogueList.length > 7 ? buttonList : buttonList2);
//			} catch (Exception ee) { 
//				//brandBG.flush();
//				BaseRenderer.setBrandingBg(-1, null);
//			}
//		}
		updateButtons();
		super.start();
	}
	
	public String findCurrentLabel() {
		return findPlayLabel(getCurrentCatalogue());
	}
	
	public String findLabel() {
		return findLabel(catalogueList);
	}
	
	public boolean canPlayAll() {
		int cnt = 0;
		
		if (BookmarkManager.getInstance().isPurchased(bundle)) {
			return true;
		}
		
		// comment out for VDTRMASTER-5179
//		for (int i = 0; catalogueList != null && i < catalogueList.length; i++) {
//			if (catalogueList[i] instanceof Episode) {
//				cnt++;
//			} else if (catalogueList[i] instanceof Video && (
//						"VT_124".equals(((Video) catalogueList[i]).getTypeID())
//						|| "VT_102".equals(((Video) catalogueList[i]).getTypeID())
//						|| "VT_103".equals(((Video) catalogueList[i]).getTypeID())
//						|| "VT_100".equals(((Video) catalogueList[i]).getTypeID())
//						|| "VT_104".equals(((Video) catalogueList[i]).getTypeID())
//					)
//					) {
//				cnt++;
//			} else if (catalogueList[i] instanceof Bundle && BookmarkManager.getInstance().isPurchased((Bundle) catalogueList[i])) {
//				cnt++;
//			}
//			
//			if (cnt > 1) {
//				return true;
//			}
//		}
		return false;
	}
	
	public boolean focusOnFirst() {
		return posterArray.getIndex() == 0;
	}
	
	public boolean focusOnLast() {
		return posterArray.getIndex() == catalogueList.length - 1;
	}
	
	public BaseElement[] getData() {
		return catalogueList;
	}

	// public boolean isFocusOnPoster() {
	// return state == STATE_DEACTIVATED && focus != 0;
	// }

	private void setActionButtons() {
		String[] actionButtons = getActionButton(currentFocusUI == FOCUS_BUNDLE ? bundle
				: catalogueList[posterArray.getIndex()]);
		actionBox.setActionBox(actionButtons);
		actionBox.setFocus(focus);
	}

	private void setTitleDetail() {
		String titleDetail = currentFocusUI == FOCUS_BUNDLE ? bundle.getDescription() 
				: ((MoreDetail) catalogueList[posterArray.getIndex()]).getDescription();
		
		if ( currentFocusUI == FOCUS_POSTERS) {
			scrollText.setContents("");
			scrollText.setButtonIcon(false);
			setActionButtons();
			setButtons(buttonList, getCurrentCatalogue());
			
			// R5
			availableView.setData((MoreDetail)getCurrentCatalogue(), this, false);
			availableView.startAnimation();
		} else { 
			// in the case of bundle
			if (bundle.isMovieBundle()) {
				scrollText.setBounds(recScrollText);
				scrollText.setTopMaskImagePosition(32-recScrollText.x, 384-recScrollText.y);
				scrollText.setBottomMaskImagePosition(32-recScrollText.x, 454-recScrollText.y);
				scrollText.setFont(FontResource.DINMED.getFont(16));
				scrollText.setForeground(cScrollTextColor);
				scrollText.setRowHeight(infoPageRowHeight);
				scrollText.setRows(recScrollText.height / infoPageRowHeight);
			} else {
				scrollText.setBounds(recScrollText2);
				scrollText.setTopMaskImagePosition(32-recScrollText2.x, 367-recScrollText2.y);
				scrollText.setBottomMaskImagePosition(32-recScrollText2.x, 454-recScrollText2.y);
				scrollText.setFont(FontResource.DINMED.getFont(16));
				scrollText.setForeground(cScrollTextColor);
				scrollText.setRowHeight(infoPageRowHeight);
				scrollText.setRows(recScrollText.height / infoPageRowHeight);
			}
			
			scrollText.setContents(titleDetail);
			setActionButtons();
			setButtons(buttonList, bundle);
			
			// R5
			availableView.setData(bundle, this, false);
			availableView.startAnimation();
		}

		repaint();
	}

	public void updateButtons() {
		int f = actionBox.getFocus();
		setActionButtons();
		actionBox.setFocus(f);
		
		// R5
		if (currentFocusUI == FOCUS_BUNDLE) {
			setButtons(buttonList, bundle);
			if (bundle != null) {
				availableView.setBounds(626, 328, 230, 24);
				
				add(availableView);
				availableView.setData(bundle, this, false);
				availableView.startAnimation();
			}
		} else {
			setButtons(buttonList, getCurrentCatalogue());
			if (getCurrentCatalogue() != null) {
				availableView.setBounds(626, 328, 230, 24);
				
				add(availableView);
				availableView.setData((MoreDetail)getCurrentCatalogue(), this, false);
				availableView.startAnimation();
			}
		}
	}

	public boolean handleKey(int key) {
		Log.printDebug("BundleTitlesUI, handleKey()" + ", code = "
				+ key + ", popup = " + getPopUp() + ", state = " + state
				+ ", focusUI = " + currentFocusUI);
		switch (key) {
		case HRcEvent.VK_LEFT:
			if (state == STATE_ACTIVATED) {
				break;
			}
			if (currentFocusUI == FOCUS_POSTERS) {
				if (!posterArray.keyLeft()) {
					currentFocusUI = FOCUS_BUNDLE;
				}
			}
			setTitleDetail();
			repaint();
			scroll = 0;
			break;
		case HRcEvent.VK_RIGHT:
			if (state == STATE_ACTIVATED) {
				break;
			}
			if (currentFocusUI == FOCUS_BUNDLE) {
				if (bdRenderer.arrowR.x != -1) {
//					Rectangle arrowR = new Rectangle(bdRenderer.arrowR.x, bdRenderer.arrowR.y, 0, 0);
//					arrowEffect.start(arrowR, ArrowEffect.RIGHT);
				}
				currentFocusUI = FOCUS_POSTERS;
			} else if (state == STATE_DEACTIVATED) {
				posterArray.keyRight();
			}
			setTitleDetail();
			repaint();
			scroll = 0;
			break;
		case HRcEvent.VK_UP:
			if (state == STATE_ACTIVATED) {
				if (actionBox.moveFocus(true)) {
					setState(BaseUI.STATE_DEACTIVATED);
				}
			}
			repaint();
			break;
		case HRcEvent.VK_DOWN:
			if (state == STATE_ACTIVATED) {
				actionBox.moveFocus(false);
			} else { // focus on poster
				setState(STATE_ACTIVATED);
			}
			repaint();
			break;
		case HRcEvent.VK_INFO:
//			if (currentFocusUI == FOCUS_POSTERS) {
//				posterArray.keyOK();
//			}
			break;
		case HRcEvent.VK_ENTER:
			if (state == STATE_ACTIVATED) {
				keyOK();
			} else if (currentFocusUI == FOCUS_POSTERS) {
				posterArray.keyOK();
			} else {
				setState(STATE_ACTIVATED);
				repaint();
			}
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			menu.back();
			break;
		case HRcEvent.VK_PAGE_UP:
			scrollText.showPreviousPage();
			break;
		case HRcEvent.VK_PAGE_DOWN:
			scrollText.showNextPage();
			break;
		}
		return true;
	}

	public void keyGreen() {
		Log.printDebug("BundleDetailUI, keyGreen()");
		setPopUp(optionBox);
		BaseElement cur = getCurrentCatalogue();
		if (cur == null && bundle != null) {
			cur = bundle;
		}
		optionBox.start(menu.getOptionMenu(0, cur), menu);
		repaint();
	}

	public void keyOK() {
		if (state != STATE_ACTIVATED) {
			return;
		}
		actionBox.click();
		String actionMenu = actionBox.getMenu();
		BaseElement element = getCurrentCatalogue();
		if (element == null
				|| dataCenter.getString(Resources.TEXT_ORDER_BUNDLE).equals(
						actionMenu)) {
			element = bundle;
		}
		menu.processActionMenu(actionMenu, element);
	}

	public Bundle getBundle() {
		return bundle;
	}

	public BaseElement getCurrentCatalogue() {
		if (currentFocusUI == FOCUS_BUNDLE) {
			return null;
		}
		BaseElement reCatalogue = null;
		if (catalogueList != null) {
			reCatalogue = catalogueList[posterArray.getIndex()];
		}
		return reCatalogue;
	}

	public void receiveCheckRightFilter(CheckResult checkResult) {
		Log.printDebug("BundleDetailUI, receiveCheckRightFilter()"
				+ ", filter = " + checkResult.getFilter() + ", response = "
				+ checkResult.getResponse());
		if (checkResult.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
			setActionButtons();
			setTitleDetail();
			repaint();
		}
	}
	
public boolean isMixedContents() {
		
		Hashtable countTable = new Hashtable();
		
		if (catalogueList != null) {
			for (int i = 0; i < catalogueList.length; i++) {
				if (catalogueList[i] instanceof Series) {
					countTable.put("Series", catalogueList[i]);
				} else if (catalogueList[i] instanceof Season) {
					countTable.put("season", catalogueList[i]);
				} else if (catalogueList[i] instanceof Bundle) {
					countTable.put("bundle", catalogueList[i]);
				} else if (catalogueList[i] instanceof Video) {
					Video v = (Video) catalogueList[i];
					
					if (v instanceof Episode) {
						countTable.put("episode", catalogueList[i]);
					} else if ("VT_100".equals(v.getTypeID())) {
						countTable.put("VT_100", catalogueList[i]);
					} else if ("VT_102".equals(v.getTypeID())) {
						countTable.put("vt_102", catalogueList[i]);
					} else if ("VT_103".equals(v.getTypeID())) {
						countTable.put("VT_103", catalogueList[i]);
					} else if ("VT_104".equals(v.getTypeID())) {
						countTable.put("VT_104", catalogueList[i]);
					} else if ("VT_124".equals(v.getTypeID())) {
						countTable.put("VT_124", catalogueList[i]);
					}
				}
				
				if (countTable.size() > 1) {
					return true;
				}
			}
		}
		
		return false;
	}
}

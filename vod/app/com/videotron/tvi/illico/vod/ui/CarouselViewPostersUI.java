package com.videotron.tvi.illico.vod.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;

import org.dvb.ui.DVBBufferedImage;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ArrowEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.gui.CarouselViewPostersRenderer;

public class CarouselViewPostersUI extends BaseUI {

	private static final long serialVersionUID = 8935191285017566L;
	public static final int MAX_POSTER = 9;

	private BaseElement[] catalogueList;
	private int titleLen;
	Thread imageLoader;

	private int startDisplayIndex;

	private CarouselViewPostersRenderer posterCarouselRenderer = new CarouselViewPostersRenderer();
	private boolean inAnimation;
	private ArrowEffect arrowEffect;
	
	private boolean retrieving;
	private boolean preActiveForward, preActivating;
	private int preActiveCount;
	private int oldFocus, newFocus;
	
	// VDTRMASTER-5757
    private CategoryContainer lastCategoryContainer;

	class CarouselEffect extends Effect {
		boolean right;
		Image[] imgs = new Image[MAX_POSTER];
		Rectangle[] bounds = CarouselViewPostersRenderer.bounds;
		Rectangle[][] dBounds;
		Rectangle leftGone = new Rectangle(0, 214, 60, 90);//new Rectangle(24, 192, 90, 135);
		Rectangle rightGone = new Rectangle(902, 214, 60, 90);//new Rectangle(848, 192, 90, 135);

		static final int FRAME = 5;
		public CarouselEffect(Component c, int n) {
			super(c, FRAME);
			dBounds = new Rectangle[bounds.length][frameCount];
		}
		
		public boolean isRight() {
			return right;
		}

		private Rectangle[] diff(Rectangle from, Rectangle to, int fcs) {
			Rectangle[] ret = new Rectangle[frameCount];
			int diffX = to.x - from.x;
	        int diffY = to.y - from.y;
	        int diffW = to.width - from.width;
	        int diffH = to.height - from.height;

	        for (int i = 0; i < frameCount; i++) {
	        	ret[i] = new Rectangle();
	        	ret[i].x = from.x + Math.round((float) diffX * i * i / fcs);
	        	ret[i].y = from.y + Math.round((float) diffY * i * i / fcs);
	        	ret[i].width = from.width + Math.round((float) diffW * i * i / fcs);
	        	ret[i].height = from.height + Math.round((float) diffH * i * i / fcs);
	        }
			return ret;
		}
		
		private boolean isPortrait(Rectangle r) {
			return r.width / r.height < 1;
		}

		public void readyImages(boolean right) {
			imgs = posterCarouselRenderer.getLastImage();
			int fcs = (frameCount - 1) * (frameCount - 1);
			int C = MAX_POSTER - 1; // center
			Rectangle from = bounds[C]; // center;
			Rectangle to = isPortrait(from) ? CarouselViewPostersRenderer.pBounds[C - (right?2:1)] : CarouselViewPostersRenderer.lBounds[C - (right?2:1)] ;
			dBounds[C] = diff(from, to, fcs);

			if (right) {
//				dBounds[0] = diff(bounds[0], leftGone, fcs);
//				dBounds[1] = diff(bounds[1], bounds[3], fcs);
//				dBounds[2] = diff(bounds[2], bounds[0], fcs);
//				dBounds[3] = diff(bounds[3], bounds[5], fcs);
//				dBounds[4] = diff(bounds[4], bounds[2], fcs);
//				dBounds[5] = diff(bounds[5], bounds[7], fcs);
//				dBounds[6] = diff(bounds[6], bounds[4], fcs);
//				dBounds[7] = diff(bounds[7], bounds[8], fcs);
				
				dBounds[0] = diff(bounds[0], leftGone, fcs);
				dBounds[1] = diff(bounds[1], isPortrait(bounds[1]) ? CarouselViewPostersRenderer.pBounds[3] : CarouselViewPostersRenderer.lBounds[3], fcs);
				dBounds[2] = diff(bounds[2], isPortrait(bounds[2]) ? CarouselViewPostersRenderer.pBounds[0] : CarouselViewPostersRenderer.lBounds[0], fcs);
				dBounds[3] = diff(bounds[3], isPortrait(bounds[3]) ? CarouselViewPostersRenderer.pBounds[5] : CarouselViewPostersRenderer.lBounds[5], fcs);
				dBounds[4] = diff(bounds[4], isPortrait(bounds[4]) ? CarouselViewPostersRenderer.pBounds[2] : CarouselViewPostersRenderer.lBounds[2], fcs);
				dBounds[5] = diff(bounds[5], isPortrait(bounds[5]) ? CarouselViewPostersRenderer.pBounds[7] : CarouselViewPostersRenderer.lBounds[7], fcs);
				dBounds[6] = diff(bounds[6], isPortrait(bounds[6]) ? CarouselViewPostersRenderer.pBounds[4] : CarouselViewPostersRenderer.lBounds[4], fcs);
				dBounds[7] = diff(bounds[7], isPortrait(bounds[7]) ? CarouselViewPostersRenderer.pBounds[8] : CarouselViewPostersRenderer.lBounds[8], fcs);
			} else {
//				dBounds[0] = diff(bounds[0], bounds[2], fcs);
//				dBounds[1] = diff(bounds[1], rightGone, fcs);
//				dBounds[2] = diff(bounds[2], bounds[4], fcs);
//				dBounds[3] = diff(bounds[3], bounds[1], fcs);
//				dBounds[4] = diff(bounds[4], bounds[6], fcs);
//				dBounds[5] = diff(bounds[5], bounds[3], fcs);
//				dBounds[6] = diff(bounds[6], bounds[8], fcs);
//				dBounds[7] = diff(bounds[7], bounds[5], fcs);
				
				dBounds[0] = diff(bounds[0], isPortrait(bounds[0]) ? CarouselViewPostersRenderer.pBounds[2] : CarouselViewPostersRenderer.lBounds[2], fcs);
				dBounds[1] = diff(bounds[1], rightGone, fcs);
				dBounds[2] = diff(bounds[2], isPortrait(bounds[2]) ? CarouselViewPostersRenderer.pBounds[4] : CarouselViewPostersRenderer.lBounds[4], fcs);
				dBounds[3] = diff(bounds[3], isPortrait(bounds[3]) ? CarouselViewPostersRenderer.pBounds[1] : CarouselViewPostersRenderer.lBounds[1], fcs);
				dBounds[4] = diff(bounds[4], isPortrait(bounds[4]) ? CarouselViewPostersRenderer.pBounds[6] : CarouselViewPostersRenderer.lBounds[6], fcs);
				dBounds[5] = diff(bounds[5], isPortrait(bounds[5]) ? CarouselViewPostersRenderer.pBounds[3] : CarouselViewPostersRenderer.lBounds[3], fcs);
				dBounds[6] = diff(bounds[6], isPortrait(bounds[6]) ? CarouselViewPostersRenderer.pBounds[8] : CarouselViewPostersRenderer.lBounds[8], fcs);
				dBounds[7] = diff(bounds[7], isPortrait(bounds[7]) ? CarouselViewPostersRenderer.pBounds[5] : CarouselViewPostersRenderer.lBounds[5], fcs);
			}

			this.right = right;
			frameCount = FRAME - 1;
		}

		protected void animate(Graphics g, DVBBufferedImage image, int frame) {
			for (int i = 0; i < MAX_POSTER - 3; i++) {
				Image poster = imgs[i];
				if (poster == null) continue;
				if (dBounds[i][frame] == null) continue;
				g.drawImage(poster, dBounds[i][frame].x - absoluteClipBounds.x, dBounds[i][frame].y - absoluteClipBounds.y, dBounds[i][frame].width, dBounds[i][frame].height, null);

				if (posterCarouselRenderer.dim[i] != null) {
					g.setColor(posterCarouselRenderer.dim[i]);
					g.fillRect(dBounds[i][frame].x - absoluteClipBounds.x, dBounds[i][frame].y - absoluteClipBounds.y, dBounds[i][frame].width, dBounds[i][frame].height);
				}
			}

			int c = MAX_POSTER - 1;
			int r = MAX_POSTER - 2;
			int l = MAX_POSTER - 3;
			if (right) {
				if (dBounds[r][frame].height > dBounds[c][frame].height) {
					// l - c - r
					if (imgs[l] != null) {
						g.drawImage(imgs[l], dBounds[l][frame].x - absoluteClipBounds.x, dBounds[l][frame].y - absoluteClipBounds.y, dBounds[l][frame].width, dBounds[l][frame].height, null);
					}
					if (imgs[c] != null) {
						g.drawImage(imgs[c], dBounds[c][frame].x - absoluteClipBounds.x, dBounds[c][frame].y - absoluteClipBounds.y, dBounds[c][frame].width, dBounds[c][frame].height, null);
					}
					if (imgs[r] != null) {
						g.drawImage(imgs[r], dBounds[r][frame].x - absoluteClipBounds.x, dBounds[r][frame].y - absoluteClipBounds.y, dBounds[r][frame].width, dBounds[r][frame].height, null);
					}
				} else {
					// l - r - c
					if (imgs[l] != null) {
						g.drawImage(imgs[l], dBounds[l][frame].x - absoluteClipBounds.x, dBounds[l][frame].y - absoluteClipBounds.y, dBounds[l][frame].width, dBounds[l][frame].height, null);
					}
					if (imgs[r] != null) {
						g.drawImage(imgs[r], dBounds[r][frame].x - absoluteClipBounds.x, dBounds[r][frame].y - absoluteClipBounds.y, dBounds[r][frame].width, dBounds[r][frame].height, null);
					}
					if (imgs[c] != null) {
						g.drawImage(imgs[c], dBounds[c][frame].x - absoluteClipBounds.x, dBounds[c][frame].y - absoluteClipBounds.y, dBounds[c][frame].width, dBounds[c][frame].height, null);
					}
				}
			} else {
				if (dBounds[l][frame].height > dBounds[c][frame].height) {
					// r - c - l
					if (imgs[r] != null) {
						g.drawImage(imgs[r], dBounds[r][frame].x - absoluteClipBounds.x, dBounds[r][frame].y - absoluteClipBounds.y, dBounds[r][frame].width, dBounds[r][frame].height, null);
					}
					if (imgs[c] != null) {
						g.drawImage(imgs[c], dBounds[c][frame].x - absoluteClipBounds.x, dBounds[c][frame].y - absoluteClipBounds.y, dBounds[c][frame].width, dBounds[c][frame].height, null);
					}
					if (imgs[l] != null) {
						g.drawImage(imgs[l], dBounds[l][frame].x - absoluteClipBounds.x, dBounds[l][frame].y - absoluteClipBounds.y, dBounds[l][frame].width, dBounds[l][frame].height, null);
					}
				} else {
					// r - l - c
					if (imgs[r] != null) {
						g.drawImage(imgs[r], dBounds[r][frame].x - absoluteClipBounds.x, dBounds[r][frame].y - absoluteClipBounds.y, dBounds[r][frame].width, dBounds[r][frame].height, null);
					}
					if (imgs[l] != null) {
						g.drawImage(imgs[l], dBounds[l][frame].x - absoluteClipBounds.x, dBounds[l][frame].y - absoluteClipBounds.y, dBounds[l][frame].width, dBounds[l][frame].height, null);
					}
					if (imgs[c] != null) {
						g.drawImage(imgs[c], dBounds[c][frame].x - absoluteClipBounds.x, dBounds[c][frame].y - absoluteClipBounds.y, dBounds[c][frame].width, dBounds[c][frame].height, null);
					}
				}
			}
		}
	}
	CarouselEffect carouselEffect;

	public CarouselViewPostersUI() {
		super();
		arrowEffect = new ArrowEffect(this, 0);
	}

	public void start(boolean back) {
		setRenderer(posterCarouselRenderer);
		setFocus(0);
		prepare();
		imageLoader = new Thread(this, "Carousel, imageLoader");
	}

	public void stop() {
		if (imageLoader != null) {
			imageLoader.interrupt();
		}
	}

	public void setData(BaseElement[] catalogues) {
//		int m = (int) (Math.random() * 60) + 10;
//		BaseElement[] t = new BaseElement[m * catalogues.length];
//		for (int i = 0; i < m; i++) {
//			for (int j = 0; j < catalogues.length; j++) {
//				t[i*catalogues.length+j] = catalogues[j];
//			}
//		}
//		catalogueList = t;
		catalogueList = catalogues;
		titleLen = catalogues.length;
//		if (titleLen >= MAX_POSTER) {
//			startDisplayIndex = 0;
//		} else {
//			startDisplayIndex = 4;
//		}
		prepare();

		// VDTRMASTER-5757
        lastCategoryContainer = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
	}
	
	private void retrieveLargeImages(boolean forward) {
		Log.printDebug("CarouselViewPostersUI.retrieveLargeImages(), f = " + forward);
		if (titleLen == 0) return;
		
		ArrayList list = new ArrayList();
		list.add(catalogueList[newFocus]);

		if (forward) {
			if ((newFocus + 1) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus + 1) % titleLen]);
			}
			if ((newFocus - 1 + titleLen) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus - 1 + titleLen) % titleLen]);
			}
			if ((newFocus + 2) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus + 2) % titleLen]);
				list.add(catalogueList[(newFocus + 2) % titleLen]);
				list.add(catalogueList[(newFocus + 2) % titleLen]);
				// consume pattern
			}
			if ((newFocus + 1) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus + 1) % titleLen]);
			}
		} else {
			if ((newFocus - 1 + titleLen) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus - 1 + titleLen) % titleLen]);
			}
			if ((newFocus + 1) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus + 1) % titleLen]);
			}
			if ((newFocus - 2 + titleLen) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus - 2 + titleLen) % titleLen]);
				list.add(catalogueList[(newFocus - 2 + titleLen) % titleLen]);
				list.add(catalogueList[(newFocus - 2 + titleLen) % titleLen]);
				// consume pattern
			}
			if ((newFocus - 1 + titleLen) % titleLen < titleLen) {
				list.add(catalogueList[(newFocus - 1 + titleLen) % titleLen]);
			}
		}
		BaseElement[] data = new BaseElement[list.size()];
		list.toArray(data);
		CatalogueDatabase.getInstance().retrieveLargeImage(this, data, false);

//		strList = new String[data.length];
//		for (int i = 0; i < strList.length; i++) {
//			strList[i] = data[i] == null ? "NULL" : data[i].toString(true);
//		}
	}

	public void setFocused(BaseElement f) {
		for (int i = 0; i < catalogueList.length; i++) {
			if (catalogueList[i] == f) {
				ArrayList list = new ArrayList();
				list.add(catalogueList[focus]);
				if ((focus - 1 + titleLen) % titleLen < titleLen) {
					list.add(catalogueList[(focus - 1 + titleLen) % titleLen]);
				}
				if ((focus + 1) % titleLen < titleLen) {
					list.add(catalogueList[(focus + 1) % titleLen]);
				}
				if ((focus - 2 + titleLen) % titleLen < titleLen) {
					list.add(catalogueList[(focus - 2 + titleLen) % titleLen]);
				}
				if ((focus + 2) % titleLen < titleLen) {
					list.add(catalogueList[(focus + 2) % titleLen]);
				}
				list.add(catalogueList[focus]);
				if ((focus - 1 + titleLen) % titleLen < titleLen) {
					list.add(catalogueList[(focus - 1 + titleLen) % titleLen]);
				}
				if ((focus + 1) % titleLen < titleLen) {
					list.add(catalogueList[(focus + 1) % titleLen]);
				}
				BaseElement[] data = new BaseElement[list.size()];
				list.toArray(data);
				CatalogueDatabase.getInstance().retrieveLargeImage(this, data, false);
				return;
			}
			if (MAX_POSTER >= titleLen) {
				if (focus < titleLen - 1 && startDisplayIndex > 0) {
					startDisplayIndex--;
				}
				focus++;
				if (focus >= titleLen) {
					focus = titleLen - 1;
				}
			} else {
				focus = (focus + 1) % titleLen;
			}
		}
	}

	public void keyLeft(boolean image, boolean ani) {
		if (titleLen == 0) return;
		oldFocus = focus;
		newFocus = focus;
		int oldStart = startDisplayIndex;
		int newStart = startDisplayIndex;
		
		newFocus = (focus - 1 + titleLen) % titleLen;
		if (newFocus == oldFocus && oldStart == newStart) {
			return;
		}
		
		if (/*retrieveNext(false, !ani) && */image) {
			if (ani) {
				if (carouselEffect == null) {
					carouselEffect = new CarouselEffect(this, 0);
				}
				carouselEffect.readyImages(false);
				if (retrieving == false) {
					inAnimation = true;
					carouselEffect.start();
				}
			} else if (retrieving == false) {
				focus = newFocus;
				retrieveLargeImages(false);
			}
		} else {
			focus = newFocus;
		}
		retrieveNext(false, image);
		
		startDisplayIndex = newStart;
		
		// R5
		UIComponent ui = MenuController.getInstance().getCurrentScene();
		if (ui instanceof CarouselViewUI) {
			((CarouselViewUI) ui).updateButtons();
		}
		
		ui.repaint();
	}

	public void keyRight(boolean image, boolean ani) {
		if (titleLen == 0) return;
		oldFocus = focus;
		newFocus = focus;
		int oldStart = startDisplayIndex;
		int newStart = startDisplayIndex;
		
		newFocus = (focus + 1) % titleLen;
		if (newFocus == oldFocus && oldStart == newStart) {
			return;
		}
		
		if (/*retrieveNext(true, !ani) && */image) {
			if (ani) {
				if (carouselEffect == null) {
					carouselEffect = new CarouselEffect(this, 0);
				}
				carouselEffect.readyImages(true);
				if (retrieving == false) {
					inAnimation = true;
					carouselEffect.start();
				}
			} else if (retrieving == false) {
				focus = newFocus;
				retrieveLargeImages(true);
			}
		} else {
			focus = newFocus;
		}
		retrieveNext(true, image);
		
		startDisplayIndex = newStart;
		
		// R5
		UIComponent ui = MenuController.getInstance().getCurrentScene();
		if (ui instanceof CarouselViewUI) {
			((CarouselViewUI) ui).updateButtons();
		}
		
		ui.repaint();
	}
	
	private boolean retrieveNext(final boolean forward, final boolean page) {
		if (retrieving) {
			focus = oldFocus;
			if (page) {
				newFocus = focus;
				retrieveLargeImages(forward);
			}
			return false;
    	}
		boolean hasAll = true;
		for (int i = 0; i < titleLen; i++) {
			if (catalogueList[i] == null) {
				hasAll = false;
				break;
			}
		}
		if (hasAll) {
			repaint();
			return true;
		}
		
		int next = 0;
		if (forward) { // right-way
			next = Math.min(titleLen - 1, focus + MAX_POSTER / 2 + 2);
		} else { // left-way
			next = (focus - MAX_POSTER / 2 - 1 + titleLen) % titleLen;
		}
		
		boolean preActiveMode = false;
		int preActive = DataCenter.getInstance().getInt("PRE_ACTIVE");
		if (preActiveForward == forward) {
			preActiveCount++;
		} else {
			preActiveCount = 1;
			preActiveForward = forward;
		}
		if ((preActive == 0 && catalogueList[next] != null) || (catalogueList[next] != null && preActiveCount < preActive || preActivating)) {
			if (page) {
				newFocus = focus;
				retrieveLargeImages(forward);
			}
			repaint();
			return true;
		}
		
		if (preActive > 0 && preActiveCount >= preActive) {
			preActiveCount = 0;
			preActiveMode = true;
			if (forward) {
				while (next < titleLen && catalogueList[next++] != null);
				next--;
			} else {
				while (next >= 0 && catalogueList[next--] != null);
				if (next < 0) {
					next = titleLen - 1;
					while (next >= 0 && catalogueList[next--] != null);
				}
			}
		}
		
		int size = 0;
		if (forward) {
			while (next >= 0 && catalogueList[next--] == null);
			next += 2;
			for (int i = next; i < catalogueList.length; i++) {
				if (catalogueList[i] == null) {
					size++;
				} else {
					break;
				}
			}
			size = Math.min(DataCenter.getInstance().getInt("PAGE_SIZE"), size);//titleLen - next);
		} else {
			while (next < catalogueList.length && catalogueList[next++] == null);
			next--;
			int max = DataCenter.getInstance().getInt("PAGE_SIZE");
			size = 0;
			while (size < max) {
				next--;
				if (next < 0 || catalogueList[next] != null) {
					next++;
					break;
				}
				size++;
			}
		}
		
		if (size == 0) {
			repaint();
			return true;
		}
		
		final int start = next;
		final int pageSize = size;
		final boolean needLoadingPopup = !preActiveMode;
		if (needLoadingPopup) {
			retrieving = true;
		}
		if (preActiveMode) {
			preActivating = true;
		}
		new Thread("Carousel, retrieveNext") {
			public void run() {
				if (needLoadingPopup) {
					MenuController.getInstance().showLoadingAnimation(true);
				}
				synchronized (CatalogueDatabase.getInstance()) {
					// VDTRMASTER-5757
					if (lastCategoryContainer != null) {
						CatalogueDatabase.getInstance().retrieveCategoryContents(null, lastCategoryContainer.id, null,
								start, pageSize, "", !needLoadingPopup);
					} else {
						CatalogueDatabase.getInstance().retrieveCategoryContents(null, null, null, start, pageSize,
								"", !needLoadingPopup);
					}
					try {
						CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {}
				} 
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				if (cat != null) {
					UIComponent ui = MenuController.getInstance().getCurrentScene();
					if (ui instanceof CarouselViewUI) {
						((CarouselViewUI) ui).setData(cat.totalContents);
					}
				}
				if (needLoadingPopup) {
					retrieving = false;
					MenuController.getInstance().hideLoadingAnimation();
					if (carouselEffect != null) {
						inAnimation = true;
						carouselEffect.start();
					}
				} else {
					preActivating = false;
				}
				if (page) {
					newFocus = focus;
					retrieveLargeImages(forward);
				}
			}
		}.start();
		repaint();
		return true;
	}
	
	public void animationEnded(Effect effect) {
		if (effect == carouselEffect) {
			inAnimation = false;
			focus = newFocus;
			retrieveLargeImages(carouselEffect.isRight());
			carouselEffect = null;
		}
	}

	public BaseElement[] getSide() {
		if (titleLen == 0) {
			return new BaseElement[0];
		}
		ArrayList list = new ArrayList();
		int idx;
		idx = (focus - 2 + titleLen) % titleLen;
		if (idx >= 0 && idx < titleLen) {
			list.add(catalogueList[idx]);
		}
		idx = (focus - 3 + titleLen) % titleLen;
		if (idx >= 0 && idx < titleLen) {
			list.add(catalogueList[idx]);
		}
		idx = (focus - 4 + titleLen) % titleLen;
		if (idx >= 0 && idx < titleLen) {
			list.add(catalogueList[idx]);
		}
		idx = (focus + 2) % titleLen;
		if (idx < titleLen) {
			list.add(catalogueList[idx]);
		}
		idx = (focus + 3) % titleLen;
		if (idx < titleLen) {
			list.add(catalogueList[idx]);
		}
		idx = (focus + 4) % titleLen;
		if (idx < titleLen) {
			list.add(catalogueList[idx]);
		}
		BaseElement[] data = new BaseElement[list.size()];
		list.toArray(data);
		return data;
	}
	
	public BaseElement[] getNextPainted(int max) {
		if (titleLen == 0) {
			return new BaseElement[0];
		}
		ArrayList list = new ArrayList();
		
		int idx;
		for (int offset = 2; list.size() < max; offset++) {
			int size = list.size();
			idx = (focus - offset + titleLen) % titleLen;
			if (idx >= 0 && idx < titleLen) {
				list.add(catalogueList[idx]);
			}
			idx = (focus + offset) % titleLen;
			if (idx < titleLen) {
				list.add(catalogueList[idx]);
			}
			if (size == list.size()) {
				break;
			}
		}
		BaseElement[] ret = new BaseElement[list.size()];
		list.toArray(ret);
		return ret;
	}

	public void keyOK() {
		// Touche:  VDTRMASTER-6242
		VbmController.getInstance().writeSelectionDummy();
		menu.goToDetail(catalogueList[focus], null);
	}

	public boolean inAnimation() {
		return inAnimation;
	}

	public boolean handleKey(int key) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CarouselViewPostersUI handleKey");
		}
		switch (key) {
//		case HRcEvent.VK_UP:
//			retrieveLargeImages(true);
//			break;
//		case HRcEvent.VK_DOWN:
//			retrieveLargeImages(false);
//			break;
			
		case HRcEvent.VK_LEFT:
			if (posterCarouselRenderer.drawnLeft) {
//				arrowEffect.start(new Rectangle(354, 234 - 70, 0, 0), ArrowEffect.LEFT);
			}
			keyLeft(true, true);
			break;
		case HRcEvent.VK_RIGHT:
			if (posterCarouselRenderer.drawnRight) {
//				arrowEffect.start(new Rectangle(589, 234 - 70, 0, 0), ArrowEffect.RIGHT);
			}
			keyRight(true, true);
			break;
		case HRcEvent.VK_INFO:
		case HRcEvent.VK_ENTER:
			keyOK();
			break;
		case OCRcEvent.VK_BACK:
		case OCRcEvent.VK_F11:
			for (int i = 0; i < 6; i++) {
				keyLeft(false, false);
			}
			keyLeft(true, false);
			break;
		case OCRcEvent.VK_FORWARD:
		case OCRcEvent.VK_F12:
			for (int i = 0; i < 6; i++) {
				keyRight(false, false);
			}
			keyRight(true, false);
			break;
		default:
			break;
		}
		return true;
	}

	public String getTotal() {
		return titleLen < 10 ? "/0" + titleLen : "/" + titleLen;
	}

	public String getCur() {
		return focus + 1 < 10 ? "0" + (focus + 1) : "" + (focus + 1);
	}

	/**
	 * @return the startDisplayIndex
	 */
	public int getStartDisplayIndex() {
		return startDisplayIndex;
	}

	/**
	 * @return the titleList
	 */
	public BaseElement[] getCatalogueServiceList() {
		return catalogueList;
	}

	public BaseElement getCurrentCatalogue() {
		if (focus < catalogueList.length) {
			return catalogueList[focus];
		}
		return null;
	}

	// R7.3
    public CategoryContainer getLastCategoryContainer() {
	    return lastCategoryContainer;
    }
}

package com.videotron.tvi.illico.vod.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ActionBox;
import com.videotron.tvi.illico.vod.comp.PopSubscribe;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.Network;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;

public class AboutChannelUI extends BaseUI {

	private static final long serialVersionUID = 8768044039360734354L;

	private Channel channel;
	private BaseRenderer renderer;

	private ActionBox actionBox;

	private OptionScreen optionBox = new OptionScreen();
	private ScrollTexts scrollText = new ScrollTexts();
	private ImageLabel brandLogo;

	static private Rectangle recScrollText = new Rectangle(58, 338, 536, 139);
	static private Color cScrollTextColor = new DVBColor(255, 255, 255, 230);
	static private int infoPageRowHeight = 17;

	static private int[] buttonList = new int[] { BaseUI.BTN_SCROLL, BaseUI.BTN_WISHLIST,BaseUI.BTN_OPTION };

	public String titleId;

	public AboutChannelUI() {
		renderer = new BaseRenderer();
		renderer.setBackGround(dataCenter.getImage("bg.jpg"));

		scrollText.setTopMaskImage(dataCenter.getImage("01_des_sh05.png"));
		scrollText.setBottomMaskImage(dataCenter.getImage("01_des_sh01.png"));
		add(scrollText);

		actionBox = ActionBox.newInstance();
		add(actionBox);

		Image btnPage = (Image) ((Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_PAGE);
		footer.linkWithScrollTexts(btnPage, scrollText);
		add(footer);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);
	}

	public void start(boolean back) {
		Log.printInfo("AboutChannelUI, start(), back = " + back);
		if (!back) {
			setRenderer(renderer);
			prepare();

			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];
			if (Log.DEBUG_ON) {
				Log.printDebug("Param : " + obj);
			}
			if (obj instanceof Channel) {
				channel = (Channel) obj;
				ImageGroup bg = channel.getBackground(true);
				if (channel.network != null) {
					bg = channel.network.getBackground(true);
				}
				if (bg != null) {
					BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//					brandBG.setImage(dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
				}
				Image logo = channel.logos[Channel.LOGO_LG];
				if (channel.network != null) {
					logo = channel.network.logos[Network.LOGO_LG];
				}
				brandLogo = new ImageLabel(logo);
				brandLogo.setBounds(95, 180, 254, 76);
				add(brandLogo, 0);
			}

			scrollText.setBounds(recScrollText);
			scrollText.setTopMaskImagePosition(32-recScrollText.x, 329-recScrollText.y);
			scrollText.setBottomMaskImagePosition(32-recScrollText.x, 454-recScrollText.y);
			scrollText.setFont(FontResource.DINMED.getFont(15));
			scrollText.setForeground(cScrollTextColor);
			scrollText.setRowHeight(infoPageRowHeight);
			scrollText.setRows(recScrollText.height / infoPageRowHeight);
			if (channel != null) {
				scrollText.setContents(channel.getDescription());
				if (channel.network != null) {
					scrollText.setContents(channel.network.getDescription());
				}
			}
			focus = 0;

		}
		updateButtons();
		state = STATE_ACTIVATED;
		BaseRenderer.setBrandingBg(0, null);
		BaseUI.buttonInfo = "";
		
		super.start();

		repaint();
		synchronized (menu) {
			menu.notifyAll();
		}
	}

	private void setActionButtons() {
		String subs = dataCenter.getString(Resources.TEXT_SUBSCRIBE);
		String tune = dataCenter.getString(Resources.TEXT_TUNE_TO);
//		String back = dataCenter.getString(Resources.TEXT_BACK);
		String[] actionButtons = new String[] { channel.hasAuth ? tune : subs };
		actionBox.setActionBox(actionButtons);
		actionBox.setFocus(focus);
	}

	public void updateButtons() {
		setButtons(buttonList, null);
		int f = actionBox.getFocus();
		setActionButtons();
		actionBox.setFocus(f);
	}

	public boolean handleKey(int key) {
		Log.printDebug("AboutChannelUI, handleKey()" + ", code = "
				+ key + ", popup = " + getPopUp());
		switch (key) {
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_ENTER:
			keyOK();
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
		case HRcEvent.VK_INFO:
			keyBack();
			break;
		case HRcEvent.VK_PAGE_UP:
			keyPageUp();
			break;
		case HRcEvent.VK_PAGE_DOWN:
			keyPageDown();
			break;
		}
		repaint();
		return true;
	}

	public void keyGreen() {
		Log.printDebug("AboutChannelUI, keyGreen()");
		setPopUp(optionBox);
		optionBox.start(menu.getOptionMenu(0, null), menu);
		repaint();
	}

	public void keyOK() {
		if (dataCenter.getString(Resources.TEXT_SUBSCRIBE).equals(actionBox.getMenu())) {
			VbmController.getInstance().writeSelectionAddHistory("Subscribe", null);
			actionBox.click();
			setPopUp(PopSubscribe.getInstance());
			PopSubscribe.getInstance().show(this, CategoryUI.getChannel());
		} else { // tune to channel
			VbmController.getInstance().writeSelectionAddHistory("Tune", null);
			if (EventQueue.isDispatchThread()) {
				actionBox.click();
				try {
					TvChannel tvCh = CommunicationManager.getInstance().getChannel(
							channel.callLetters);
					int srcId = tvCh.getSourceId();
					CommunicationManager.getInstance().requestTuner(null, null, srcId);
					return;
				} catch (Exception e) {
					Log.print(e);
				}
			}
			menu.exit(MenuController.EXIT_TO_CHANNEL, channel);
		}
	}

	public void keyUp() {
		actionBox.moveFocus(true);
	}

	public void keyDown() {
		actionBox.moveFocus(false);
	}

	public void keyPageUp() {
		scrollText.showPreviousPage();
	}

	public void keyPageDown() {
		scrollText.showNextPage();
	}

	public void keyBack() {
		menu.back();
	}
}

package com.videotron.tvi.illico.vod.ui;

import java.awt.event.KeyEvent;
import java.text.Collator;
import java.util.Locale;

import javax.tv.util.TVTimerWentOffEvent;

import com.videotron.tvi.illico.vod.comp.PopChangeView;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CacheElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.gui.CarouselViewRenderer;

public class CarouselViewUI extends BaseUI {

	private static final long serialVersionUID = 1653975985320310020L;

	// VDTRMASTER-5809
	private int[] buttonList = new int[] { BaseUI.BTN_VIEW, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST,
			BaseUI.BTN_OPTION };
//	private int[] buttonList2 = new int[] { 
//			BaseUI.BTN_VIEW, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	
	private BaseElement[] catalogueList;

	private Renderer carouselBase = new CarouselViewRenderer();
	private CarouselViewPostersUI posterCarousel = new CarouselViewPostersUI();

	static private OptionScreen optionBox = new OptionScreen();
	
	// VDTRMASTER-5690
	private Object param;

	public CarouselViewUI() {
		add(posterCarousel);

		add(footer);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);
	}

	public void start(boolean back) {
		if (!back) {
			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];
			
			BaseElement[] data = null;
			BaseElement focused = null;
			// R5
			fromCategory = false;
			if (obj instanceof String) { // category
				this.param = obj;
				Object cached = CatalogueDatabase.getInstance().getCached(obj);
				Log.printDebug("Carousel, cached = " + cached);
				if (cached instanceof CategoryContainer) {
					CategoryContainer cat = (CategoryContainer) cached;
					menu.setLastCategoryId(cat.id);
					data = cat.totalContents;
					
					String opt = menu.getSortNormal();
					data = CatalogueDatabase.getInstance().sort(data, opt == null ? Resources.OPT_NAME : opt);
					
					// R5
					fromCategory = true;
				}
				if (data.length > 0) {
					focused = data[0];
				}
			} else {
				data = (BaseElement[]) param[MenuController.PARAM_DATA_IDX];
				
				// VDTRMASTER-5668 & VDTRMASTER-5685
				if (data.length > 0) {
					focused = data[0];
				}
				
				if (param.length > MenuController.PARAM_DATA_IDX + 1) {
					focused = (BaseElement) param[MenuController.PARAM_DATA_IDX + 1];
				}
				
				// VDTRMASTER-5690
				this.param = DataCenter.getInstance().get("VIEW_PARAM");
			}
			setData(data);
			setRenderer(carouselBase);
			prepare();
			posterCarousel.setFocus(0);
			posterCarousel.start(back);
			posterCarousel.setFocused(focused);
			posterCarousel.setVisible(true);
		} else {
			// VDTRMASTER-5835
			if (MenuController.getInstance().getChageViewType() != PopChangeView.VIEW_CARROUSEL) {
				keyYellow();
				return;
			}
			// VDTRMASTER-5690
			if (Log.DEBUG_ON) {
				Log.printDebug("Carousel, param=" + param);
			}
			// VDTRMASTER-5433
			if (param instanceof String) {
				CacheElement cached = CatalogueDatabase.getInstance().getCached(param);
				Log.printDebug("Carousel, cached = " + cached);
				if (cached instanceof CategoryContainer) {
					
					boolean isApplied = false;
					CategoryContainer category = (CategoryContainer) cached;
					do {
						isApplied = CategoryUI.setBrandingElementByCategory(category);
						category = category.parent;
					} while (category != null && isApplied == false);
				}
			}
		}
		BaseRenderer.setBrandingBg(1, null);
//		Channel channel = null;
//		ImageGroup bg = null;
		
		ImageGroup bg = null;
		if (CategoryUI.isBranded()) {
			BrandingElement br = CategoryUI.getBrandingElement();
			bg = br.getBackground(false);
			try {
				BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
			} catch (Exception e) {
				BaseRenderer.setBrandingBg(-1, null);
			}
		}
		
//		try {
//			channel = CategoryUI.getChannel();
//			if (channel.network != null) {
//				bg = channel.network.getBackground(false);
//			} else {
//				bg = channel.getBackground(false);
//			}
//			BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//		} catch (Exception e) {
//			try {
//				BrandingElement br = CategoryUI.getInstance().getBrandingElement();
//				bg = br.getBackground(false);
//				BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//			} catch (Exception ee) { 
//				BaseRenderer.setBrandingBg(-1, null);
//			}
//		}
		
		updateButtons();
		super.start();
		
		setupTimer(true, 0);
		if (Resources.OPT_NAME.equals(menu.getSortNormal())) {
			menu.setNeedRapid(true);
		}
	}


	public void searchLetter(Object letter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("searchLetter, letter = " + letter);
		}
		if (catalogueList == null || catalogueList.length == 0) {
			return;
		}
		MoreDetail candidate = (MoreDetail) catalogueList[0];
		Collator c = Collator.getInstance(Locale.CANADA_FRENCH);
		for (int i = 1; i < catalogueList.length; i++) {
			if (c.compare(candidate.getTitle(), letter) > 0) {
				break;
			}
			candidate = (MoreDetail) catalogueList[i];
		}
		scroll = 0;
		posterCarousel.setFocused(candidate);
		repaint();
	}
	
	public void updateButtons() {
		// VDTRMASTER-5809
		setButtons(buttonList, getCurrentCatalogue());
	}

	public boolean canPlayAll() {
		// purchased bundle can play all...
		if (getCurrentCatalogue() instanceof Bundle) {
			return BookmarkManager.getInstance().isPurchased((Bundle) getCurrentCatalogue());
		}
		
		if (catalogueList == null || catalogueList.length < 2) {
			return false;
		}
		
		// all free or all purchased
		boolean canPlayAll = true;
		for (int i = 0; catalogueList != null && i < catalogueList.length; i++) {
			if (catalogueList[i] == null) {
				continue;
			}
			if (catalogueList[i] instanceof Video) {
				if (Log.EXTRA_ON) {
					Log.printDebug("catalogueList[i]=" + catalogueList[i].toString(true));
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).getTypeID());
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).isFree());
					Log.printDebug("catalogueList[i] isPurchased=" + BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]));
				}
				
				// VDTRMASTER-5432
				if (catalogueList[i] instanceof Episode && (((Video) catalogueList[i]).isFree() || BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]) != null)) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_124")) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_104")) {
					continue;
				} else {
					canPlayAll = false;
					break;
				}
			}
			// mixed category, can't play all
			canPlayAll = false;
			break;
		}
		return canPlayAll;
	}
	
	public BaseElement[] getData() {
		return catalogueList;
	}
	
	public boolean focusOnFirst() {
		return posterCarousel.getCurrentCatalogue() == catalogueList[0];
	}
	
	public boolean focusOnLast() {
		return posterCarousel.getCurrentCatalogue() == catalogueList[catalogueList.length - 1];
	}
	
	public boolean inAnimation() {
		return posterCarousel.inAnimation();
	}

	public void flushData() {
		// for (int i = 0; catalogueList != null && i < catalogueList.length;
		// i++) {
		// catalogueList[i] = null;
		// }
		// catalogueList = null;
	}

	public void setData(BaseElement[] data) {
//		int m = 20;//(int) (Math.random() * 60) + 10;
//		BaseElement[] t = new BaseElement[m * data.length];
//		for (int i = 0; i < m; i++) {
//			for (int j = 0; j < data.length; j++) {
//				t[i*data.length+j] = data[j];
//			}
//		}
//		catalogueList = t;
		catalogueList = data;
//		posterCarousel.setFocus(0);
//		prepare();
		posterCarousel.setData(catalogueList);
		repaint();

//		setupTimer(true, 0);
	}
	int[] lastRequested;
	public void timerWentOff(TVTimerWentOffEvent arg0) {
		BaseElement[] painted = posterCarousel.getSide();
		if (painted.length == 0) {
			return;
		}
		boolean dup = true;
		if (lastRequested == null || lastRequested.length != painted.length) {
			lastRequested = new int[painted.length];
			dup = false;
		} else {
			for (int i = 0; i < lastRequested.length; i++) {
				if (painted[i] != null && lastRequested[i] != painted[i].hashCode()) {
					dup = false;
					break;
				}
			}
		}
		if (dup) {
			return;
		}
		for (int i = 0; i < lastRequested.length; i++) {
			if (painted[i] != null) {
				lastRequested[i] = painted[i].hashCode();
			}
		}
		CatalogueDatabase.getInstance().retrieveImages(this, painted, ImageRetriever.TYPE_DOWNLOAD_MORE | ImageRetriever.TYPE_NO_DELAY | ImageRetriever.TYPE_MEDIUM | ImageRetriever.TYPE_SMALL, CatalogueDatabase.IMAGE_RETRIEVER_SM);
	}
	
	public void imageDownloadMore() {
		BaseElement[] painted = posterCarousel.getNextPainted(20);
		if (painted.length == 0) {
			return;
		}
		CatalogueDatabase.getInstance().retrieveImages(this, painted, ImageRetriever.TYPE_SMALL, CatalogueDatabase.IMAGE_RETRIEVER_SM);
	}

	public int getSortStatus() {
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_NAME)) {
//			return 1;
//		}
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_DATE)) {
//			return 2;
//		}
//		return 0;
		return CatalogueDatabase.getInstance().getLastSortTypeIdx();
	}

	public BaseElement getCurrentCatalogue() {
		return posterCarousel.getCurrentCatalogue();
	}

	/**
	 * Returns the focus position of UI.
	 */
	public int getFocus() {
		return posterCarousel.getFocus();
	}

	public boolean handleKey(int key) {
		Log.printDebug("CarouselViewUI, handleKey()" + ", code = "
				+ key + ", popup = " + getPopUp());
		switch (key) {
		case KeyCodes.COLOR_A:
			keyYellow();
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		case OCRcEvent.VK_BACK:
		case KeyEvent.VK_F11:
		case OCRcEvent.VK_FORWARD:
		case KeyEvent.VK_F12:
			if (isExistPageHorizontal()) {
				footer.clickAnimation(Resources.TEXT_CHANGE_PAGE);
				posterCarousel.handleKey(key);
			}
			break;
		case HRcEvent.VK_UP:
		case HRcEvent.VK_DOWN:
		case HRcEvent.VK_LEFT:
		case HRcEvent.VK_RIGHT:
		case HRcEvent.VK_INFO:
		case HRcEvent.VK_ENTER:
			posterCarousel.handleKey(key);
			break;
		}
		repaint();
		return true;
	}

	public void keyGreen() {
		Log.printDebug("CarouselViewUI, keyGreen()");
		int opt = CatalogueDatabase.getInstance().getLastSortTypeIdx();
//		if (catalogueList == null || catalogueList.length == 0
//				|| catalogueList[0] instanceof Video == false) {
//			opt = 1;
//		} else {
//			opt = CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//					Resources.OPT_NAME) ? 2 : 1;
//		}
		setPopUp(optionBox);
		optionBox.start(menu.getOptionMenu(opt, getCurrentCatalogue()), menu);
		repaint();
	}

	public void keyBack() {
		menu.backwardHistory(false);
		menu.back();
		// 20190222 Touche project
		VbmController.getInstance().backwardHistory();		
	}

	public void keyYellow() {
	    // R7.3
        if (posterCarousel.getLastCategoryContainer() != null) {
            CatalogueDatabase.getInstance().setLastRequestedCategoryContainerId(posterCarousel.getLastCategoryContainer().id);
        }
		// VDTRMASTER-5690
		DataCenter.getInstance().put("VIEW_PARAM", param);
		menu.viewChange(catalogueList, getCurrentCatalogue());
		// VDTRMASTER-5690
		DataCenter.getInstance().remove("VIEW_PARAM");
	}

	public void reflectVCDSResult(Object cmd) {
		menu.hideLoadingAnimation();
		if (CatalogueDatabase.SORT.equals(cmd)) {
			// VDTRMASTER-5653 & VDTRMASTER-5649
			setFocus(0);
			posterCarousel.setFocus(0);
			if (menu.getLastCategoryId().startsWith("TO_")) {
				Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(menu.getLastCategoryId());
				setData(topic.treeRoot.totalContents);
				posterCarousel.setFocused(topic.treeRoot.totalContents[0]);
			} else {
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				setData(cat.totalContents);
				posterCarousel.setFocused(cat.totalContents[0]);
			}
			scroll = 0;
		}
	}
	
	public void selected(MenuItem item) {
//		if (Resources.OPT_NAME.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_NAME);
//			setData(catalogueList);
//			menu.setSortNormal(Resources.OPT_NAME);
//		} else if (Resources.OPT_DATE.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_DATE);
//			setData(catalogueList);
//			menu.setSortNormal(Resources.OPT_DATE);
//		}
//		repaint();
	}
	
	public void receiveCheckRightFilter(CheckResult checkResult) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CategoryUI, receiveCheckRightFilter()"
					+ ", checkResult = " + checkResult + ", filter = "
					+ checkResult.getFilter() + ", response = "
					+ checkResult.getResponse());
		}
		if (PreferenceService.RESPONSE_SUCCESS != checkResult.getResponse()) {
			return;
		}
		if (RightFilter.BLOCK_BY_RATINGS.equals(checkResult.getFilter())) {
			handleKey(KeyEvent.VK_ENTER);
		}
	}
}

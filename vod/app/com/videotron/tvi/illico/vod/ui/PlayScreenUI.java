package com.videotron.tvi.illico.vod.ui;

import java.awt.Dimension;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.vod.comp.PopFreeviewVODStop;
import com.videotron.tvi.illico.vod.comp.PopSubscribe;
import com.videotron.tvi.illico.vod.data.vcds.type.*;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;
import org.ocap.hardware.VideoOutputPort;
import org.ocap.hardware.device.FixedVideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputPortListener;
import org.ocap.hardware.device.VideoOutputSettings;
import org.ocap.hardware.device.VideoResolution;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.flipbar.FlipBar;
import com.videotron.tvi.illico.flipbar.FlipBarAction;
import com.videotron.tvi.illico.flipbar.FlipBarContent;
import com.videotron.tvi.illico.flipbar.FlipBarFactory;
import com.videotron.tvi.illico.flipbar.FlipBarListener;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.vod.VODEvents;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.ixc.vod.VODServiceListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.VideoOutputListener;
import com.videotron.tvi.illico.util.VideoOutputUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.PopSelectOption;
import com.videotron.tvi.illico.vod.comp.PopVODStop;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VODServiceImpl;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.UsageManager;
import com.videotron.tvi.illico.vod.controller.menu.VideoControllerImpl;
import com.videotron.tvi.illico.vod.controller.session.SessionEventType;
import com.videotron.tvi.illico.vod.controller.session.autodiscovery.ServiceGroupController;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionEvent;
import com.videotron.tvi.illico.vod.data.backend.Bookmark;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.gui.PlayScreenRenderer;

public class PlayScreenUI extends BaseUI implements VODServiceListener,
		TVTimerWentOffListener, FlipBarListener, VideoOutputPortListener, VideoOutputListener {

	private static final long serialVersionUID = 7494658549861202863L;
	private Video title;
	private boolean fhdContents;
	private VideoOutputSettings HDMIport;
	private Bundle bundle;
	private ArrayList playlist;
	private Vector playAll;
	private int idxOfBundle;
	private Offer offer;
	private Playout playout;
	private long runtime;
	private Bookmark bookmark;
	private int selectFromPopup;
	private String sspId, orderableItem;
	private boolean needPurchase;
	private boolean haveStream;
	private boolean networkLocked;
	private boolean needLockCheck;
	private boolean error217; // jira 3694

	private int currentMode;
	private int currentNpt, lastNpt;
	private int nextNpt;
	private int cacheNpt;
	private int passed;
	private PopVODStop popVodStop = new PopVODStop();
	// private PopResumeViewingConfirm resumeConfirm = new
	// PopResumeViewingConfirm();
	// private int actionAfterVideoStop;
	private Renderer playScreenRendere = new PlayScreenRenderer();

	private TVTimerSpec timer, cannot, expire;
	private int checkTime = 500;
	private int lastCheckNptTime;
	private FlipBar flipBar, vodBar, karaokeBar;
	private boolean flipBarOn;
	private int flipBarShowTime;
	private final int MAX_FLIP_SHOW_TIME = 10;
	private final int MAX_PAUSE_TIME = 120;
	private final int MAX_STOP_POPUP_SHOW_TIME = 120 * 15;
	private final int CHECK_NPT_TIME = 5;
	private final int FIFTEEN_SECONDS = (int) (15 * Constants.MS_PER_SECOND);
	private final int TWO_SECONDS = (int) (2 * Constants.MS_PER_SECOND);
	private final int THREE_SECONDS = (int) (3 * Constants.MS_PER_SECOND);
	private int limitTime; // ddc 039.1

	public static final int FLIP_SKIP_BACK = 0;
	public static final int FLIP_REW = 1;
	public static final int FLIP_STOP = 2;
	public static final int FLIP_PAUSE = 3;
	public static final int FLIP_PLAY = 4;
	public static final int FLIP_FF = 5;
	public static final int FLIP_SKIP_FORWARD = 6;
	public static final int FLIP_SLOW = 7;
	private int flipStatus;

	private int stopPopUpShowTime;

	private int[] skipSections;
	private boolean sectionSet;
	private long skipTimestamp;
	private long startTimestamp;

	private String repeat;
	private boolean purchasedPartyPack;
	private boolean noLimit;
	
	// DDC-090
	private int lscStatusIntervalLimit = 30;
	private int lscStatusCount = 0;
	private boolean isActiveDDC090;
	private long lastRequestTime;
	private TVTimerSpec firstDelayTime, secondDelayTime;
	private boolean isReachedResponse;

	static public String debug = "", debugHDMI = "";
	
	Bundle nextBundle = null;
	
	boolean preparingNextContent;
	
	long lastSaveTime;
	
	// 4K - DDC1-7
	private boolean isUHDContent;

	// Bundling
	private PopFreeviewVODStop popFreeviewVodStop = new PopFreeviewVODStop();

	public PlayScreenUI() {
		vodBar = FlipBarFactory.getInstance().createVodFlipBar();
		karaokeBar = FlipBarFactory.getInstance().createKaraokeFlipBar();
		// add(flipBar);
		timer = new TVTimerSpec();
		timer.setRepeat(true);
		timer.setAbsolute(false);
		timer.setTime(checkTime);
		timer.setRegular(true);
		timer.addTVTimerWentOffListener(this);
		
		isActiveDDC090 = DataCenter.getInstance().getBoolean("DDC_090");
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("PlayScreenUI, isActiveDDC090=" + isActiveDDC090);
        }
        
        if (isActiveDDC090) {
        	int pLSCInterval = DataCenter.getInstance().getInt("LSC_STATUS_INTERVAL");
        	lscStatusIntervalLimit = pLSCInterval * 2; // because timer's interval set 500ms.
    		
    		if (Log.DEBUG_ON) {
    			Log.printDebug("PlayScreenUI, pLSCInterval=" + pLSCInterval + ", lscStatusIntervalLimit=" + lscStatusIntervalLimit);
    		}
        }
	}

	public void start(boolean back) {
		CommunicationManager.getInstance().debugLock = "";
		if (!back) {
			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];

			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI, start(), param = " + obj);
			}
			noLimit = false;
			selectFromPopup = -1;
			cacheNpt = 0;
			if (playAll != null) {
				playAll.clear();
				playAll = null;
			}
			if (obj instanceof Object[]) { // from purchase option
				selectFromPopup = ((Integer) ((Object[]) obj)[0]).intValue();
				String id = (String) ((Object[]) obj)[1];
				if (id.startsWith("BU")) {
					bundle = (Bundle) CatalogueDatabase.getInstance()
							.getCached(id);
					idxOfBundle = 0;
					try {
						idxOfBundle = ((Integer) ((Object[]) obj)[3]).intValue();
						if (idxOfBundle == -1) { // means play all
							idxOfBundle = 0;
							playAll = new Vector();
							MoreDetail[] titles = bundle.getTitles();
							for (int i = 0; i < titles.length; i++) {
								playAll.addElement(titles[i]);
							}
						}
					} catch (Exception e) {}
					Log.printDebug("PlayScreenUI, start(), Bundle selected idx = " + idxOfBundle);
					title = (Video) bundle.getTitles()[idxOfBundle];
				} else {
					title = (Video) CatalogueDatabase.getInstance().getCached(
							id);
				}
				playout = title.getPlayout(selectFromPopup, false);
				if (playout != null) {
					orderableItem = title.getSspId(selectFromPopup);
					sspId = playout.name;
					runtime = playout.duration * Constants.MS_PER_SECOND;
					offer = (Offer) ((Object[]) obj)[2];
					CatalogueDatabase.getInstance().registerBySspId(orderableItem, title);
					Log.printDebug("PlayScreenUI, start(), playout = " + playout.toString(true) + ", orderableItem = " + orderableItem);
				} else {
					Log.printError("PlayScreenUI, can not found the playout for " + selectFromPopup 
							+ " in " + title.toString(true) + "\n" + title.dumpOffer() + "\n" + title.dumpPlayout());
					error("VOD104", null);
				}
				flipBar = vodBar;
				needPurchase = true;
			} else if (obj instanceof Bookmark) { // from resume viewing
				//noLimit = true;
				bookmark = (Bookmark) obj;
				Object cached = CatalogueDatabase.getInstance()
						.getCachedBySspId(bookmark.getOrderableName());
				if (cached instanceof Video) {
					title = (Video) cached;
					Log.printDebug("PlayScreenUI, cached = " + title.toString(true));
				} else if (cached instanceof Bundle) {
					bundle = (Bundle) cached;
					Log.printDebug("PlayScreenUI, cached = " + bundle.toString(true));
					idxOfBundle = bookmark.idxOfBundle;
					title = (Video) bundle.getTitles()[idxOfBundle];
					bookmark = BookmarkManager.getInstance()
							.getPurchased(title);
				}
				Log.printDebug("PlayScreenUI, bookmark = " + bookmark);
				if (bookmark != null) {
					orderableItem = bookmark.getOrderableName();
				}
				if (title != null) {
					playout = title.getPlayout(orderableItem);
				}
				Log.printDebug("PlayScreenUI, playout = " + playout);
				if (playout != null) {
					sspId = playout.name;
					runtime = playout.duration * Constants.MS_PER_SECOND;
					selectFromPopup = playout.type;
				} else {
					Log.printError("PlayScreenUI, can not found the playout for " + sspId 
							+ " in " + title.toString(true) + "\n" + title.dumpOffer() + "\n" + title.dumpPlayout());
					error("VOD104", null);
				}
				flipBar = vodBar;

				if (expire != null) {
					expire.removeTVTimerWentOffListener(this);
					TVTimer.getTimer().deschedule(expire);
				}
				if (bookmark != null) {
					expire = new TVTimerSpec();
					expire.addTVTimerWentOffListener(this);
					long msExpire = (bookmark.getLeaseEnd() - System.currentTimeMillis());
					expire.setDelayTime(msExpire);
					try {
						expire = TVTimer.getTimer().scheduleTimerSpec(expire);
						Log.printDebug("expire timer setup, " + (msExpire / Constants.MS_PER_SECOND) + " secs");
					} catch (TVTimerScheduleFailedException e) {}
				}
				needPurchase = false;

			} else if (obj instanceof ArrayList) { // from karaoke
				noLimit = true;
				playlist = (ArrayList) obj;
				idxOfBundle = 0;
				title = (Video) playlist.get(idxOfBundle);
				playout = title.getPlayout();
				if (playout != null) {
					sspId = playout.name;
					runtime = playout.duration * Constants.MS_PER_SECOND;
				} else {
					Log.printError("PlayScreenUI, can not found the playout for karaoke" 
							+ " in " + title.toString(true) + "\n" + title.dumpOffer() + "\n" + title.dumpPlayout());

					error("VOD104", null);
				}
				flipBar = karaokeBar;
				if (repeat == null) {
					repeat = FlipBarAction.REPEAT_ONCE.getName();
				}
				purchasedPartyPack = CategoryUI.getPurchasedPartyPack() != null;
				needPurchase = false;
			} else if (obj instanceof Vector) { // from play all 
				playAll = (Vector) obj;
				idxOfBundle = 0;
				Object content = playAll.get(idxOfBundle);
				readyNextContent(content, false);
				flipBar = vodBar;
				
			}
			add(flipBar);
			
			setRenderer(playScreenRendere);
			prepare();
		}
		setBounds(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		super.start();

		PreferenceProxy.getInstance().setAspect(true);
		boolean hdContents = title.isHD(playout) || title.isFHD(playout) || title.isUHD(playout);
		if (hdContents) {
			PreferenceProxy.getInstance().setNormalZoomMode();
		}
		
		fhdContents = title.isFHD(playout);
		if (fhdContents) {
			PreferenceProxy.getInstance().setVideoOutput(true);
			if (checkFHDenv()) {
				PreferenceProxy.getInstance().setFullHdVideoOutput();
			}
		} else {
			debugHDMI = "It's not a FullHD contents";
		}
	
		if (Log.INFO_ON) {
			Log.printInfo("PlayScreen, title = "
					+ title.toString(true) + ", sspId = " + sspId
					+ ", playout = " + playout + ", hd = " + hdContents
					+ ", fhd = " + fhdContents);
		}
		
		startTimestamp = System.currentTimeMillis();
		playFromBegin();
		
		lastRequestTime = 0;
	}
	
	private boolean checkFHDenv() {
		boolean fullHD = false;
		
		String vod1080p = DataCenter.getInstance().getString(
				PreferenceNames.VOD_1080P_SUPPORT);
		Log.printDebug("FULL-HD, vod1080p on Setting = " + vod1080p);
		if (Definitions.OPTION_VALUE_DISABLE.equals(vod1080p)) {
			Log.printDebug(debugHDMI = "FULL-HD, 'Disabled' on Setting");
			return false;
		}
		
		Enumeration e = Host.getInstance().getVideoOutputPorts();
		int idx = 0;
		while (e.hasMoreElements()) {
			VideoOutputPort port = (VideoOutputPort) e.nextElement();
			VideoOutputSettings setting = (VideoOutputSettings) port;
			if (port.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_HDMI) {
				HDMIport = setting;
				HDMIport.addListener(this);
				if (port.status() == false) {
					Log.printDebug(debugHDMI = "FULL-HD, HDMI port is disabled");
				} else if (setting.isDisplayConnected() == false) {
					Log.printDebug(debugHDMI = "FULL-HD, HDMI port is disconnected"); 
				} else {
					VideoOutputConfiguration[] config = setting.getSupportedConfigurations();
					for (int i = 0; i < config.length; i++) {
						FixedVideoOutputConfiguration fixed = (FixedVideoOutputConfiguration) config[i];
						VideoResolution res = fixed.getVideoResolution();
						Dimension dim = res.getPixelResolution();
						Log.printDebug("FULL-HD, config[" + i + "] = " + fixed);
						Log.printDebug("FULL-HD, config[" + i + "], resolution = " + res);
						Log.printDebug("FULL-HD, config[" + i + "], dimension = " + dim);
						Log.printDebug("FULL-HD, config[" + i + "], scanmode = " + res.getScanMode());
						Log.printDebug("FULL-HD, config[" + i + "], rate = " + res.getRate() + ", rounded = " + Math.round(res.getRate()));
						int rate = Math.round(res.getRate());
						if (res.getScanMode() == VideoResolution.SCANMODE_PROGRESSIVE && dim.height == 1080 && (rate == 24 || rate == 30)) {
							Log.printDebug(debugHDMI = ("FULL-HD, OK, found config = " + config[i])); 
							fullHD = true;
							break;
						}
					}
					if (fullHD == false) {
						Log.printDebug(debugHDMI = "FULL-HD, HDMI port is ok, but 1080p(24f/30f) is not supported");
					}
				}
			} else {
				Log.printDebug("FULL-HD, port[" + idx + "] is not HDMI, type = " + port.getType() + ", state = " + port.status() + ", connected = " + setting.isDisplayConnected());
			}
			idx++;
		}
		return fullHD;
	}

	private void playFromBegin() {
		Log.printDebug("PlayScreenUI, playFromBegin(), " + sspId + 
				", cur = " + modeStr());
		if (currentMode == VODEvents.PAUSE) {
			VideoControllerImpl.getInstance().stop(sspId);
		}
		currentMode = VODEvents.STATUS;
		lscStatusCount = 0;
		currentNpt = 0;
		flipBarShowTime = 0;
		flipBarOn = false;
		flipBar.addListener(this);
		flipBar.setMediaTime(0);
		setSkipSections();
		setFlipBar();
		showFlipBar(false);

		nextNpt = -1;
		if (flipBar == vodBar && !title.isFreeview) {
			cacheNpt = 0;
		}

		if (playout == null || sspId == null) {
			return;
		}

		if (flipBar == karaokeBar && purchasedPartyPack && CategoryUI.getPurchasedPartyPack() == null) {
			// karaoke expire
			error("VOD900", null);
			return;
		}
		
		// 4K - DDC107
		if (title != null) {
			isUHDContent = title.isUHD(playout);
			if (Log.INFO_ON) {
				Log.printInfo("PlayScreen, title = "
						+ title.toString(true) + ", sspId = " + sspId
						+ ", playout = " + playout + ", isUHDContent = " + isUHDContent);
			}
			if (isUHDContent) {
				VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(false);
				// HDCP 2.2
				VideoOutputUtil.getInstance().addVideoOutputListener(this);
			} else {
				VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
			}
		}
		
		VideoControllerImpl.getInstance().addVODServiceListener(this);
		Log.printDebug("PlayScreenUI, playFromBegin(), " + sspId + ", cacheNpt = "
				+ cacheNpt + ", noLimit = " + noLimit);
		new Thread(this, "playFromBegin(), " + sspId + ", cacheNpt = "
				+ cacheNpt).start();
	}

	public void stop() {
		CommunicationManager.getInstance().turnOnErrorMessage(true);

		if (currentMode != VODEvents.STOP && currentMode != VODEvents.PAUSE) {
			setNpt(currentNpt, false);
		}
		currentMode = VODEvents.STOP;
		stopTimer();
		stopFirstDelayTimer();
		stopSecondDelayTimer();
		hideFlipBar();
		
//		if (cacheNpt != currentNpt) {
//			setNpt(currentNpt, false);
//		}
		VideoControllerImpl.getInstance().removeVODServiceListener(this);
		VideoControllerImpl.getInstance().stop(sspId);
		if (Resources.appStatus == VODService.APP_WATCHING) {
			Resources.appStatus = VODService.APP_STARTED;
		}
		VODServiceImpl.getInstance().logPlaybackStopped(true);
		
		playout = null;
		sspId = null;
		if (playlist != null) {
			playlist.clear();
		}
		playlist = null;
		debug = "";
		super.stop();

		if (flipBar != null) {
			flipBar.removeListener(this);
			remove(flipBar);
			flipBar = null;
		}

		PreferenceProxy.getInstance().setAspect(false);
		if (fhdContents) {
			PreferenceProxy.getInstance().setVideoOutput(false);
		}
		if (HDMIport != null) {
			HDMIport.removeListener(this);
		}
		
		// 4K - DDC-107
		VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
		// HDCP 2.2
		VideoOutputUtil.getInstance().removeVideoOutputListener(this);
		
		try {
			Log.printDebug("PlayScreenUI, stop(), try to release video context");
			CommunicationManager.getInstance().getMonitorService().releaseVideoContext();
			SharedMemory.getInstance().remove(Resources.APP_NAME+SharedDataKeys.VIDEO_PROGRAM_NAME);
			SharedMemory.getInstance().remove(Resources.APP_NAME+SharedDataKeys.VIDEO_RESOLUTION);
		} catch (RemoteException e1) {
			e1.printStackTrace();
		} catch (AbstractMethodError ame) {} // need latest monitor
	}

	private void setSkipSections() {
		sectionSet = true;

		long minimumLen = 10 * Constants.MS_PER_MINUTE;
		int sectionDenom = 10;
		skipSections = new int[sectionDenom + 1];
		if (runtime > minimumLen) {
			int skipInterval = (int) (runtime / sectionDenom);
			for (int i = 1; i < skipSections.length; i++) {
				skipSections[i] = i * skipInterval;
			}
			skipSections[sectionDenom] = (int) (runtime - FIFTEEN_SECONDS);
			if (Log.DEBUG_ON) {
				for (int i = 0; i < skipSections.length; i++) {
					Log.printDebug("setSkipSections, [" + i + "] = " + skipSections[i]);
				}
			}
		} else {
			sectionSet = false;
		}
	}

	/**
	 * @return the currentMode
	 */
	public int getCurrentMode() {
		return currentMode;
	}
	
	public Video getTitle() {
		return title;
	}

	private void setFlipBar() {
		String name = title.getTitle();
		long startTime = 0;
		long endTime = runtime;

		FlipBarContent content = new FlipBarContent(name, startTime, endTime);
		if (flipBar == vodBar) {
			content.setRating(title.getRating());
			
			// VDTRMASTER-5427
			if (title.getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(title.getOfferHD(null).definition)) {
				content.setDefinition(Constants.DEFINITION_4K);
			} else {
				// VDTRMASTER-5850
				if (Log.DEBUG_ON) {
					Log.printDebug("PlayScreenUI, setFlipBar, selectFromPopup=" + selectFromPopup);
					Log.printDebug("PlayScreenUI, setFlipBar, title.selected=" + title.selected);
				}
				if (selectFromPopup == -1) {
					selectFromPopup = title.selected;
				}

				content.setResolution(selectFromPopup == Resources.HD_EN
						|| selectFromPopup == Resources.HD_FR);
			}
			
			flipBar.setSkippable(sectionSet);
		}
		flipBar.setContent(content);
		flipStatus = FLIP_STOP;
	}

    //->Kenneth[2015.7.4] : Tank 
    public static boolean isPortalVisible = false;
	public void showFlipBar(boolean icon) {
	//private void showFlipBar(boolean icon) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI showFlipBar, flipBar = " + flipBar);
		}
        if (isPortalVisible) {
			if (Log.DEBUG_ON) Log.printDebug("PlayScreenUI.showFlipBar() : Portal is visible, so return");
            return;
        }
		flipBarShowTime = 0;
		if (!flipBarOn) {
			flipBarOn = true;
			// curIcon = feedMode == MODE_SMALL ? SMALL_ICON : LARGE_ICON;
			// startTimer(checkTime);
			currentNpt = VideoControllerImpl.getInstance().getCurrentNPT();
			if (flipBar != null) {
				flipBar.setMediaTime(currentNpt);
				flipBar.start();
			}
		}

		if (icon) {
			curIcon = feedMode == MODE_SMALL ? SMALL_ICON : LARGE_ICON;
		} else {
			curIcon = NO_ICON;
		}
		
		startTimer(checkTime);

		repaint();
	}

    //->Kenneth[2015.7.4] : Tank : private -> public
	public void hideFlipBar() {
	//private void hideFlipBar() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI hideFlipBar ");
		}
		curIcon = NO_ICON;
		flipBarShowTime = 0;
		if (flipBar != null) {
			flipBar.setMediaTime(0);
			flipBar.stop();
		}
		flipBarOn = false;
		repaint();
	}

	private void updateFlipBar(int flipMode) {
		flipStatus = flipMode;
		if (flipBar == karaokeBar) {
			switch (flipMode) {
			case FLIP_SKIP_BACK:
				break; // ok
			case FLIP_REW:
				return; // no use
			case FLIP_STOP:
				flipMode = 1;
				break;
			case FLIP_PAUSE:
				flipMode = 2;
				break;
			case FLIP_PLAY:
				flipMode = 3;
				break;
			case FLIP_FF:
				return; // no use
			case FLIP_SKIP_FORWARD:
				flipMode = 4;
				break;
			}
		} else if (flipMode == FLIP_SLOW){
			flipMode = FLIP_PLAY;
		}
		
		flipBar.setFocus(flipMode);
		showFlipBar(true);
	}

	/**
	 * @return the flipStatus
	 */
	public int getFlipStatus() {
		return flipStatus;
	}

	/**
	 * @return the flipBarOn
	 */
	public boolean isFlipBarOn() {
		return flipBarOn;
	}

	/** Starts the Clock timer. */
	private void startTimer(int time) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI startTimer");
		}
		if (timer != null) {
			TVTimer.getTimer().deschedule(timer);
		}
		try {
			timer = TVTimer.getTimer().scheduleTimerSpec(timer);
		} catch (Exception e) {
			Log.print(e);
		}
	}

	private void stopTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI stopTimer, " + timer);
		}
		flipBarShowTime = 0;
		if (timer != null) {
			timer.removeTVTimerWentOffListener(this);
			TVTimer.getTimer().deschedule(timer);
		}
		if (expire != null) {
			expire.removeTVTimerWentOffListener(this);
			TVTimer.getTimer().deschedule(expire);
		}
	}
	
	// DDC-090
	private void startFirstDelayTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI startFirstDelayTimer 600ms");
		}
		
		if (firstDelayTime == null) {
			firstDelayTime = new TVTimerSpec();
			//R7.4 VDTRMASTER-6174 sykim300 -> 600
            firstDelayTime.setDelayTime(600L);
			firstDelayTime.addTVTimerWentOffListener(this);
		}
		
		TVTimer.getTimer().deschedule(firstDelayTime);

		try {
			firstDelayTime = TVTimer.getTimer().scheduleTimerSpec(firstDelayTime);
		} catch (Exception e) {
			Log.print(e);
		}
	}
	
	// DDC-090
	private void stopFirstDelayTimer() {
		TVTimer.getTimer().deschedule(firstDelayTime);
	}
	
	private void startSecondDelayTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI startSecondDelayTimer 900ms");
		}
		
		if (secondDelayTime == null) {
			secondDelayTime = new TVTimerSpec();
			//R7.4 VDTRMASTER-6174 sykim600 -> 900
            secondDelayTime.setDelayTime(900L);
			secondDelayTime.addTVTimerWentOffListener(this);
		}
		
		TVTimer.getTimer().deschedule(secondDelayTime);
		
		try {
			secondDelayTime = TVTimer.getTimer().scheduleTimerSpec(secondDelayTime);
		} catch (Exception e) {
			Log.print(e);
		}
	}
	
	// DDC-090
	private void stopSecondDelayTimer() {
		TVTimer.getTimer().deschedule(secondDelayTime);
	}

	public void run() {
		CommunicationManager.getInstance().debugLock = "";
		startTimestamp = System.currentTimeMillis();
		menu.showLoadingAnimation();
		Log.printDebug("PlayScreenUI, run()");
		try {
			Thread.sleep(TWO_SECONDS);
		} catch (InterruptedException e1) {}
		Log.printDebug("PlayScreenUI, run(), app = " + Resources.appStatus);
		if (ServiceGroupController.getInstance().getServiceGroupData() == null) {
			if (Log.WARNING_ON) {
				Log.printWarning("PlayScreenUI, Service Group data is missing");
			}
			ServiceGroupController.getInstance().searchServiceGroup();
		}

		if (ServiceGroupController.getInstance().getServiceGroupData() == null) {
			error("VOD502", null);
		} else {
			if (Resources.appStatus == VODService.APP_STARTED) {
				debug = sspId;
				haveStream = false;
				error217 = false;
				limitTime = 0;
				int pubIdx = sspId.indexOf("pub");
				if (pubIdx != -1) {
					try {
						int sec = Integer.parseInt(sspId.substring(pubIdx + 3, sspId.indexOf("_", pubIdx)));
						if (sec > 0) {
							limitTime = (int) (sec * Constants.MS_PER_SECOND);
						}
					} catch (NumberFormatException nfe) {}
				}
				Log.printDebug("PlayScreenUI, limitTime = " + limitTime);
				VideoControllerImpl.getInstance().sessionSetup(sspId);
			} else {
				Log.printDebug("PlayScreenUI, run(), app = " + Resources.appStatus);
			}
			repaint();
		}
	}

	public static final int MODE_NORMAL = 0;
	public static final int MODE_LARGE = 1;
	public static final int MODE_SMALL = 2;
	public static final int MODE_MIXED = 3;
	public static final int LARGE_ICON = 10;
	public static final int SMALL_ICON = 11;
	public static final int NO_ICON = 12;

	public static int timeLarge = 2;
	public static int timeSmall = 5;
	public static int feedMode = MODE_LARGE;
	public static int curIcon;

	public boolean handleKey(int key) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI handleKey : " + key);
		}
		flipBarShowTime = 0;
		switch (key) {
		case HRcEvent.VK_UP:
		case HRcEvent.VK_DOWN:
		case HRcEvent.VK_LEFT:
		case HRcEvent.VK_RIGHT:
		case HRcEvent.VK_INFO:
			if (isFlipBarOn()) {
				return false;
			}
			keyOK();
			break;
		case HRcEvent.VK_ENTER:
			if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
				return true; // ignore for stability
			}
			if (isFlipBarOn()) {
				return false;
			}
			keyOK();
			break;
		case HRcEvent.VK_PLAY:
			return keyPlay();
		case HRcEvent.VK_STOP:
			return keyStop(true, true);
		case HRcEvent.VK_PAUSE:
			return keyPause(false);
		case HRcEvent.VK_FAST_FWD:
			return keyFF();
		case HRcEvent.VK_REWIND:
			return keyRew();
		case OCRcEvent.VK_FORWARD:
			return keySkipFoward();
		case OCRcEvent.VK_BACK:
			return keySkipBackward();
		case OCRcEvent.VK_OPEN_BRACKET:
			return keySkipBackwardCompanion();
		case OCRcEvent.VK_CLOSE_BRACKET:
			return keySkipForwardCompanion();
		case OCRcEvent.VK_LIVE:
		case OCRcEvent.VK_EXIT:
			if (currentMode == VODEvents.STATUS) {
				if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
					break; // ignore for stability
				}
				menu.exit(MenuController.EXIT_NORMAL, null);
				break;
			}
			if (isFlipBarOn()) {
				hideFlipBar();
//			} else if (keyPause(true)) { 
//				showStopPop(true);
			} else { // 4587
				if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
					Log.printDebug("PlayScreenUI keyExit ignored by stable");
					return false; // ignore for stability
				}
				
				if (isActiveDDC090) {
					// during ad time, client can not request to pause,
					// so in case of DDC-090, client work like select Stop.
					return keyStop(true, true);
				} else {
					setNpt(currentNpt, true);
					VideoControllerImpl.getInstance().pause(sspId);
					currentMode = VODEvents.PAUSE;
					updateFlipBar(FLIP_PAUSE);
	
					debug = sspId + " " + modeStr() + " " + title;
					
					showStopPop(true, true);
				}
			}
			break;
		case KeyCodes.VOD:
		case KeyCodes.SETTINGS:
			// case KeyCodes.WIDGET:
			// case KeyCodes.MENU:
			// case OCRcEvent.VK_CHANNEL_DOWN:
			// case OCRcEvent.VK_CHANNEL_UP:
			// case OCRcEvent.VK_GUIDE:
			if (currentMode == VODEvents.STATUS) {
				// ignore while waiting session setup
				break;
			}
//			if (keyPause(true)) {
//				showStopPop(true);
//			}
			keyStop(true, true);
			break;
//		case KeyCodes.SEARCH: // jira 3802
//			CommunicationManager.getInstance().launchSearchApplication(true,
//					false);
//			break;
		case HRcEvent.VK_0:
		case HRcEvent.VK_1:
		case HRcEvent.VK_2:
		case HRcEvent.VK_3:
		case HRcEvent.VK_4:
		case HRcEvent.VK_5:
		case HRcEvent.VK_6:
		case HRcEvent.VK_7:
		case HRcEvent.VK_8:
		case HRcEvent.VK_9:
			if (flipBarOn) {
				keyNumeric(key - 48);
			}
			break;
		// no action but popup
		case KeyCodes.SEARCH: // jira 3802
			showCannot();
			break;
		// no actions
		case OCRcEvent.VK_RECORD:
		case KeyCodes.FAV:
		case OCRcEvent.VK_LAST:
		case OCRcEvent.VK_CHANNEL_DOWN:
		case OCRcEvent.VK_CHANNEL_UP:
			break;
//		case KeyCodes.COLOR_A:
//			if (Log.EXTRA_ON) {
//				feedMode = MODE_NORMAL;
//				repaint();
//			}
//			break;
//		case KeyCodes.COLOR_B:
//			if (Log.EXTRA_ON) {
//				feedMode = MODE_LARGE;
//				timeLarge = 2;
//				repaint();
//			}
//			break;
//		case KeyCodes.COLOR_C:
//			if (Log.EXTRA_ON) {
//				feedMode = MODE_SMALL;
//				timeSmall = 5;
//				repaint();
//			}
//			break;
//		case KeyCodes.COLOR_D:
//			if (Log.EXTRA_ON) {
//				feedMode = MODE_MIXED;
//				timeSmall = 5;
//				timeLarge = 2;
//				repaint();
//			}
//			break;
//		case KeyEvent.VK_PAGE_DOWN:
//			if (Log.EXTRA_ON && feedMode == MODE_MIXED) {
//				timeSmall = Math.max(0, timeSmall - 1);
//				repaint();
//			}
//			break;
//		case KeyEvent.VK_PAGE_UP:
//			if (Log.EXTRA_ON && feedMode == MODE_MIXED) {
//				timeSmall++;
//				repaint();
//			}
//			break;
		// by-pass
		case OCRcEvent.VK_GUIDE:
		case KeyCodes.WIDGET:
		case KeyCodes.MENU:
			hideFlipBar();
			// don't break
		case KeyCodes.LIST:
		case OCRcEvent.VK_VOLUME_DOWN:
		case OCRcEvent.VK_VOLUME_UP:
		case OCRcEvent.VK_MUTE:
		case KeyCodes.ASPECT: // jira 3008
		case KeyCodes.PIP: 
		case KeyCodes.COLOR_D: // by pass to EPG to show D-option
			return false;
		}
		return true;
	}

	boolean cannotIcon;

	public void showCannot() {
		if (cannot == null) {
			cannot = new TVTimerSpec();
			cannot.addTVTimerWentOffListener(this);
		}
		cannot.setDelayTime(3000);
		try {
			cannot = TVTimer.getTimer().scheduleTimerSpec(cannot);
		} catch (TVTimerScheduleFailedException e) {
		}
		cannotIcon = true;
		repaint();
	}

	public boolean cannotIcon() {
		return cannotIcon;
	}
	
//	public void playNextContent() {
//		Log.printDebug("PlayScreenUI, playNextContent, idx = " + idxOfBundle + ", playAll = " + playAll.size() 
//				+ ", title = " + title + ", curMode = " + currentMode);
//		if (playAll == null || playAll.size() - 1 <= idxOfBundle) {
//			Log.printDebug("PlayScreenUI, playNextContent, no more list");
//			return;
//		}
//		if ((currentMode == VODEvents.STATUS || currentMode == VODEvents.STOP) == false) {
//			setNpt(currentNpt, false);
//			hideFlipBar();
//			
//			Resources.appStatus = VODService.APP_STARTED;
//			VideoControllerImpl.getInstance().stop(sspId);
//			currentMode = VODEvents.STOP;
//			updateFlipBar(FLIP_STOP);
//		}
//		title = (Video) playAll.get(++idxOfBundle);
//		Log.printDebug("PlayScreenUI, playNextContent, next idx = " + idxOfBundle + ", next title = " + title);
//		playout = title.getPlayout(title.selected, false);
//		if (playout != null) {
//			sspId = playout.name;
//			runtime = playout.duration * Constants.MS_PER_SECOND;
//			playFromBegin();
//		}
//	}
	
	private void readyNextContent(Object content, boolean force) {
		if (content instanceof Video) {
			title = (Video) content;
		} else if (content instanceof Bundle) {
			bundle = (Bundle) content;
			bookmark = BookmarkManager.getInstance().getPurchased(bundle);
			if (bookmark == null) { // not purchased bundle, need propose
				if (force) { // after PIN
					MoreDetail[] titles = bundle.getTitles();
					int idx = playAll.indexOf(content);
					playAll.remove(idx);
					for (int i = 0; i < titles.length; i++) {
						playAll.add(idx, titles[i]);
						idx++;
					}
				} else {
					Log.printDebug("PlayScreenUI, readyNextContent, not purchased " + bundle.toString(true));
					needPurchase = true;
					return;
				}
			}
			title = (Video) bundle.getTitles()[0];
		}
		
		bookmark = BookmarkManager.getInstance().getPurchased(title);
		Log.printDebug("PlayScreenUI, readyNextContent, bookmark = " + bookmark);
		if (bookmark != null) {
			orderableItem = bookmark.getOrderableName();
			playout = title.getPlayout(orderableItem);
			if (expire != null) {
				expire.removeTVTimerWentOffListener(this);
				TVTimer.getTimer().deschedule(expire);
			}
			expire = new TVTimerSpec();
			expire.addTVTimerWentOffListener(this);
			long msExpire = (bookmark.getLeaseEnd() - System.currentTimeMillis());
			expire.setDelayTime(msExpire);
			try {
				expire = TVTimer.getTimer().scheduleTimerSpec(expire);
				Log.printDebug("PlayScreenUI, readyNextContent, expire timer setup, " + (msExpire / Constants.MS_PER_SECOND) + " secs");
			} catch (TVTimerScheduleFailedException e) {}
			needPurchase = false;
		} else {
			needPurchase = true;
			playout = title.getPlayout(title.selected, false);
			if (playout != null) {
				Log.printDebug("PlayScreenUI, readyNextContent, found playout for same option = " + title.selected);
			} else { // use default select
				Log.printDebug("PlayScreenUI, readyNextContent, no playout for option = " + title.selected);
				String prefLang = DataCenter.getInstance().getString(
						PreferenceNames.PREFERENCES_ON_LANGUAGE);
				String prefDef = DataCenter.getInstance().getString(
						PreferenceNames.PREFERENCES_ON_FORMAT);
				title.selected = Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH.equals(prefLang) 
						? (Definitions.PREFERENCES_ON_FORMAT_HD.equals(prefDef) ? Resources.HD_EN : Resources.SD_EN) 
						: (Definitions.PREFERENCES_ON_FORMAT_HD.equals(prefDef) ? Resources.HD_FR : Resources.SD_FR);
				playout = title.getPlayout(title.selected, false);
				
				if (playout != null) {
					Log.printDebug("PlayScreenUI, readyNextContent, selected playout by preference = " + title.selected);
				} else { // REQ-76.3.01-0140 
					//2.	Same language, smaller definition
					title.selected = title.selected < 2 
							? title.selected + 2 // HD -> SD 
							: title.selected; // SD contents, no change
					playout = title.getPlayout(title.selected, false);
					if (playout != null) {
						Log.printDebug("PlayScreenUI, readyNextContent, selected playout by rule 2. same lang, smaller def = " + title.selected);
					} else {
						//3.	Other language, same definition	
						title.selected = title.selected % 2 == 0 
								? title.selected + 1 // FR -> EN 
								: title.selected - 1; // EN -> FR
						playout = title.getPlayout(title.selected, false);
						if (playout != null) {
							Log.printDebug("PlayScreenUI, readyNextContent, selected playout by rule 3. other lang, same def = " + title.selected);
						} else {
							//4.	Other language, smaller definition.
							title.selected = title.selected + 2;
							playout = title.getPlayout(title.selected, false);
							if (playout != null) {
								Log.printDebug("PlayScreenUI, readyNextContent, selected playout by rule 4. other lang, smaller def = " + title.selected);
							} else {
								if (title.getPlayout() != null) {
									Log.printDebug("PlayScreenUI, readyNextContent, title.detail.length=" + title.detail.length); 
									if (title.detail.length > 1) {
										// show option popup
										PopSelectOption.getInstance().show(new BaseUI() {
											public void popUpClosed(UIComponent popup, Object msg) {
												setPopUp(null);
												remove(popup);
												int select = ((Integer) ((Object[]) msg)[0]).intValue();
												if (select == PopSelectOption.BTN_CANCEL) {
													menu.back();
													return;
												}
												
												Log.printDebug("PlayScreenUI, readyNextContent, from PopSelectOption, select=" + select); 
												
												title.selected = select;

												readyNextContent(title, false);
												
												// TODO
												PreferenceProxy.getInstance().setAspect(true);
                                                boolean hdContents = title.isHD(playout) || title.isFHD(playout) || title.isUHD(playout);
												if (hdContents) {
													PreferenceProxy.getInstance().setNormalZoomMode();
												}
												
												fhdContents = title.isFHD(playout);
												if (fhdContents) {
													PreferenceProxy.getInstance().setVideoOutput(true);
													if (checkFHDenv()) {
														PreferenceProxy.getInstance().setFullHdVideoOutput();
													}
												} else {
													debugHDMI = "It's not a FullHD contents";
												}
											
												if (Log.INFO_ON) {
													Log.printInfo("PlayScreen, title = "
															+ title.toString(true) + ", sspId = " + sspId
															+ ", playout = " + playout + ", hd = " + hdContents
															+ ", fhd = " + fhdContents);
												}
												
												startTimestamp = System.currentTimeMillis();
												playFromBegin();
											}
										}, title);
									} else {
										// if title has a single playout
										playout = title.getPlayout();
										
										if (playout != null) {
											String lan = title.detail[0].language;
											String def = title.detail[0].definition;
											
											if (lan.equals(Definition.LANGUAGE_FR)) {
												//
												if (def.equals("HD") || def.equals("FHD")) {
													title.selected = Resources.HD_FR;
												} else {
													title.selected = Resources.SD_FR;
												}
											} else {
												if (def.equals("HD") || def.equals("FHD")) {
													title.selected = Resources.HD_EN;
												} else {
													title.selected = Resources.SD_EN;
												}
											}
										}
										
									}
								} else {
									Log.printWarning("PlayScreenUI, readyNextContent, NO PLAYOUT");
								}
							}
						}
					}
				}
			}
			orderableItem = title.getSspId(title.selected);
		}
		
		if (playout != null) {
			Log.printInfo("PlayScreenUI, readyNextContent, playout = " + playout);
			sspId = playout.name;
			runtime = playout.duration * Constants.MS_PER_SECOND;
			if (title.selected == Resources.HD_EN || title.selected == Resources.HD_FR) {
				offer = title.getOfferHD(title.selected == Resources.HD_EN ? "en" : "fr");
			} else {
				offer = title.getOfferSD(title.selected == Resources.HD_EN ? "en" : "fr");
			}
		} else {
			Log.printWarning("PlayScreenUI, readyNextContent, playout couldn't found");
		}
	}
	
	public void playNextContent(boolean force) {
		Log.printDebug("PlayScreenUI, playNextContent, idx = " + idxOfBundle + ", playAll = " + playAll.size() 
				+ ", title = " + title + ", curMode = " + currentMode + ", force = " + force);
		if (playAll == null || playAll.size() - 1 <= idxOfBundle) {
			Log.printDebug("PlayScreenUI, playNextContent, no more list");
			return;
		}
		
		preparingNextContent = true;
		if ((currentMode == VODEvents.STATUS || currentMode == VODEvents.STOP) == false) {
			setNpt(currentNpt, false);
			hideFlipBar();
			
			Resources.appStatus = VODService.APP_STARTED;
			VideoControllerImpl.getInstance().stop(sspId);
			currentMode = VODEvents.STOP;
			updateFlipBar(FLIP_STOP);
		}
		
		try {
			Thread.sleep(750);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		idxOfBundle++;
		
		Object next = playAll.get(idxOfBundle);
		readyNextContent(next, force);
		playNextContent(next, force);
		preparingNextContent = false;
	}
	
	private void playNextContent(Object next, boolean force) {
		boolean isFree = false;
		if (next instanceof Video) {
			isFree = ((Video) next).isFree();
		} else if (next instanceof Bundle) {
			isFree = ((Bundle) next).isFree();
		}
		if (needPurchase && isFree == false && force == false) {
			menu.purchaseContent((BaseElement) next, this);
		} else {
			startTimestamp = System.currentTimeMillis();
			noLimit = false;
			playFromBegin();
		}
	}

	public void keyOK() {
		showFlipBar(false);
	}

	public boolean keyPlay() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI keyPlay currentMode: "
					+ modeStr() + ", curNpt = " + currentNpt);
		}
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS) { // no action
			return false;
		}		
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keyPlay ignored by stable");
			return false; // ignore for stability
		}
		if (currentMode != VODEvents.PAUSE && limit()) {
			showCannot();
			Log.printDebug("PlayScreenUI keyPlay ignored by limit");
			return false;
		}
		
		// DDC-090
		if (isActiveDDC090) {
			long curTime = System.currentTimeMillis();
			
			if (curTime - lastRequestTime < 300) {
				showCannot();
				Log.printDebug("PlayScreenUI keyPlay ignored by 1st delay");
				return false;
			}
			
			lastRequestTime = System.currentTimeMillis();
			isReachedResponse = false;
			startFirstDelayTimer();
			startSecondDelayTimer();
			
			if (currentMode == VODEvents.PLAY && flipBar == vodBar) {
				VideoControllerImpl.getInstance().playSlow(sspId);
				return true;
			}
	
			VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY);
			
		} else {
			if (currentMode == VODEvents.PLAY && flipBar == vodBar) {
				VideoControllerImpl.getInstance().playSlow(sspId);
				currentMode = VODEvents.PLAY_SLOW;
				updateFlipBar(FLIP_SLOW);
				return true;
			}
	
			if (currentMode == VODEvents.PAUSE) {
				VODServiceImpl.getInstance().logPlaybackPaused(false);
			}
					
			VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY);
			currentMode = VODEvents.PLAY;
			updateFlipBar(FLIP_PLAY);
	
			debug = sspId + " " + modeStr() + " " + title;
		}
		return true;
	}
	
	public boolean limit() { // DDC 039.1
		if (noLimit) {
			return false;
		}
		return currentNpt < FIFTEEN_SECONDS + limitTime;
	}
	
	public boolean keyStop(boolean resumeView, boolean force) {
		if (currentMode == VODEvents.STATUS) { // before session setup
			Log.printDebug("PlayScreenUI keyStop ignored by status");
			return false;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			return false; // ignore for stability
		}
		//if (fromResumeViewing == false && System.currentTimeMillis() - startTimestamp < FIFTEEN_SECONDS + limitTime) {
		if (limit()) {
			showCannot();
			Log.printDebug("PlayScreenUI keyStop ignored by limit");
			return false;
		}
		
		if (isActiveDDC090) {
			long curTime = System.currentTimeMillis();
			
			if (curTime - lastRequestTime < 300) {
				showCannot();
				Log.printDebug("PlayScreenUI keyStop ignored by 1st delay");
				return false;
			}
			
			// comment out for VDTRMASTER-5320
//			UsageManager.getInstance().addUsageLog("1108", sspId, "1305",
//					String.valueOf(currentNpt));
			setNpt(currentNpt, false);
			
			if (getPopUp() != null) {
				return true;
			}
			
			hideFlipBar();
			
			Resources.appStatus = VODService.APP_STARTED;
			isReachedResponse = false;
			VideoControllerImpl.getInstance().stop(sspId);
			lastRequestTime = System.currentTimeMillis();
			currentMode = VODEvents.STOP;
			updateFlipBar(FLIP_STOP);
			showStopPop(resumeView, force);
			if (resumeView) {
				cacheNpt = currentNpt;
			}
			// VDTRMASTER-5446
			startFirstDelayTimer();
			startSecondDelayTimer();
			
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI keyStop currentMode: "
						+ modeStr());
			}
			// comment out for VDTRMASTER-5320
//			UsageManager.getInstance().addUsageLog("1108", sspId, "1305",
//					String.valueOf(currentNpt));
	//		if (bookmark != null && cacheNpt != currentNpt) {
	//			cacheNpt = currentNpt;
	//			BookmarkManager.getInstance().setNPT(bookmark.getBookmarkID(),
	//					currentNpt);
	//		}
			setNpt(currentNpt, false);
			
			if (getPopUp() != null) {
				return true;
			}
			hideFlipBar();
			
			Resources.appStatus = VODService.APP_STARTED;
			VideoControllerImpl.getInstance().stop(sspId);
			currentMode = VODEvents.STOP;
			updateFlipBar(FLIP_STOP);
			showStopPop(resumeView, force);
			if (resumeView) {
				cacheNpt = currentNpt;
			}
		}
		return true;
	}

	private void showStopPop(boolean resumeView, boolean force) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI showStopPop" +
					", resumeView = " + resumeView + 
					", force = " + force + 
					", playAll = " + playAll + 
					", idx = " + idxOfBundle);
			Thread.dumpStack();
		}
		try {
			Thread.sleep(750);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		menu.hideLoadingAnimation();
		
		if (force == false && playAll != null && idxOfBundle < playAll.size() - 1) {
			Log.printDebug("PlayScreenUI showStopPop, but 'playAll' has more element");
			playNextContent(false);
			return;
		}
		
		
		boolean next = false;
		boolean playFromBegin = true;
		boolean order = false;
		boolean nextIsOrder = false;
		
		nextBundle = null;
		
		if (bundle != null) {
			Log.printDebug("PlayScreenUI showStopPop, for bundle, bundle isMovieBundle = " + bundle.isMovieBundle());
			if (bundle.getTitles().length - 1 > idxOfBundle) {
				next = true;
			} else if (!bundle.isMovieBundle()) {
				// VDTRMASTER-5187 [CQ][PO1/2/3][R4][VOD] The system doesn't propose to purchase the next available bundle
				Object foundBundle = menu.getNextBundle(bundle);
				Log.printDebug("PlayScreenUI showStopPop, for bundle, foundBundle = " + foundBundle);
				if (foundBundle != null) {
					
					if (foundBundle instanceof Bundle) {
						nextBundle = (Bundle) foundBundle;
						Log.printDebug("PlayScreenUI showStopPop, for bundle, nextBundle isPurchased = " + BookmarkManager.getInstance().isPurchased(nextBundle));
						if (!BookmarkManager.getInstance().isPurchased(nextBundle)) {
							next = true;
							nextIsOrder = true;
						}
					}
				}
			}
		} else if (playlist != null) {
			if (playlist.size() - 1 > idxOfBundle) {
				next = true;
			}
			//playFromBegin = false;
			if (CategoryUI.getPurchasedPartyPack() == null) {
				order = true;
			}
		} else {
			Object nextOne = null;
			if (playAll != null && playAll.size() - 1 > idxOfBundle) {
				nextOne = playAll.get(idxOfBundle + 1);
			} else if (playAll != null) {
				
				Object foundBundle = menu.getNextBundle(title);
				Log.printDebug("PlayScreenUI showStopPop, for title of playAll, foundBundle = " + foundBundle);
				if (foundBundle != null) {
					
					if (foundBundle instanceof Bundle) {
						nextBundle = (Bundle) foundBundle;
						Log.printDebug("PlayScreenUI showStopPop, for bundle, nextBundle isMovieBundle = " + nextBundle.isMovieBundle());
						if (!nextBundle.isMovieBundle()) {
							Log.printDebug("PlayScreenUI showStopPop, for bundle, nextBundle isPurchased = " + BookmarkManager.getInstance().isPurchased(nextBundle));
							if (!BookmarkManager.getInstance().isPurchased(nextBundle)) {
								next = true;
								nextIsOrder = true;
							}
						}
					}
				}
			} else {
				
				BaseUI parent = menu.getParentUI();

                // VDTRMASTER-6078
				if ( parent != null && (!(parent instanceof ListViewUI) || ((ListViewUI)parent).isNormal())) {

					nextOne = menu.getNext(title);
					if (playAll != null && nextOne != null) {
						nextOne = playAll.contains(nextOne) ? null : nextOne; // wrap finish
					}
				}
				
				// VDTRMASTER-5210	[CQ][PO1/2/3][R4][VOD] - 'Order next' button is missing on the 'Stop/End' playback popup
				if (nextOne == null && title.inBundlesAR != null && title.inBundlesAR.length > 0) {
					Object foundBundle = menu.getNextBundle(title);
					Log.printDebug("PlayScreenUI showStopPop, for title, foundBundle = " + foundBundle);
					if (foundBundle != null) {
						
						if (foundBundle instanceof Bundle) {
							nextBundle = (Bundle) foundBundle;
							Log.printDebug("PlayScreenUI showStopPop, for bundle, nextBundle isPurchased = " + BookmarkManager.getInstance().isPurchased(nextBundle));
							if (!BookmarkManager.getInstance().isPurchased(nextBundle)) {
								next = true;
								nextIsOrder = true;
							}
						}
					}
				}
			}
			if (nextOne != null) {
				next = true;
                if (Log.DEBUG_ON) {
                    Log.printDebug("PlayScreenUI, showStopPop, nextOne=" + nextOne);
                    Log.printDebug("PlayScreenUI, showStopPop, next=" + next);
                }
				if (nextOne instanceof Video) {
					Video v = (Video) nextOne;
					nextIsOrder = v.isFree() == false && BookmarkManager.getInstance().isPurchased(v) == null;
				} else if (nextOne instanceof Bundle) {
					Bundle b = (Bundle) nextOne;
					nextIsOrder = b.isFree() == false && BookmarkManager.getInstance().isPurchased(b) == false;
					next = b.isFree() == false && nextIsOrder; // no 'view next' button for purchased or free bundle
				} else {
                    // VDTRMASTER-6122
                    if (Log.DEBUG_ON) {
                        Log.printDebug("PlayScreenUI, showStopPop, nextOne is not orderable item");
                    }
                    next = false;
                    nextIsOrder = false;
                }

				// VDTRMASTER-6116
				if (!isSubscribed(getActionButton((BaseElement)nextOne))) {
                    nextIsOrder = true;
                }
			}
		}
		
		try {
			CommunicationManager.getInstance().getMonitorService().stopOverlappingApplication();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AbstractMethodError ame) {} // need latest monitor
		
		// VDTRMASTER-5525
		if (getPopUp() != null) {
			remove(getPopUp());
			setPopUp(null);
		}

		// Bundling
		BaseUI lastScene = menu.getLastScene();
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI, showStopPop, lastScene=" + lastScene);
            Log.printDebug("PlayScreenUI, showStopPop, bundle=" + bundle + ", title=" + title);
		}
		boolean isExistedFreeBtn = false;
		boolean isSubscribed = false;
		boolean hasWatchAll = false;
		if (lastScene != null) {
			// VDTRMASTER-5936
			String[] actions = null;
			if (bundle != null) {
				actions = lastScene.getActionButton(bundle);
			} else {
				actions = lastScene.getActionButton(title);
			}
			if (actions != null && actions.length > 0) {
				if (Log.DEBUG_ON) {
					Log.printDebug("PlayScreenUI, showStopPop, title.actions[0]=" + actions[0]);
				}

				if (actions[0].charAt(0) == '!') {
					isExistedFreeBtn = true;
				}

				// R7.3
				isSubscribed = isSubscribed(actions);

				// R7.3
				hasWatchAll = hasWatchAll(actions);
			}
		} else {
			//R7.4 VDTRMASTER-5850, VDTRMASTER-6198
			//It is considered as purchased content in case of playback from companion. 
			isSubscribed = true;
		}

		// VDTRMASTER-5887
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI, showStopPop, title.isFreeView=" + title.isFreeview);
			Log.printDebug("PlayScreenUI, showStopPop, bookmark=" + bookmark);
			Log.printDebug("PlayScreenUI, showStopPop, title.nextEpisodeId=" + title.nextEpisodeId);
			Log.printDebug("PlayScreenUI, showStopPop, isExistedFreeBtn=" + isExistedFreeBtn);
			Log.printDebug("PlayScreenUI, showStopPop, isSubscribed=" + isSubscribed);
			Log.printDebug("PlayScreenUI, showStopPop, hasWatchAll=" + hasWatchAll);
		}

		// R7.3
		// watch episode
		if (playAll == null && hasWatchAll && title instanceof Episode) {
			BaseElement nextOne = menu.getNext(title);
			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI, showStopPop, watch episode, nextOne=" + nextOne);
			}
			if (nextOne != null) {
				next = true;
				if (nextOne instanceof Video) {
					Video v = (Video) nextOne;
					nextIsOrder = v.isFree() == false && BookmarkManager.getInstance().isPurchased(v) == null;
				}
			}
		}

		// VDTRMASTER-5956 - remove the condition to check the nextEpisode.
//		if (title.isFreeview && isExistedFreeBtn) {
		if (!isSubscribed) {
			CacheElement cached = CatalogueDatabase.getInstance().getCached(title.nextEpisodeId);

			if (cached != null) {
				if (title instanceof Episode) {
					Episode currentEpisode = (Episode) title;
					if (currentEpisode.season == null || currentEpisode.season.episode.length == 0) {
						String seasonId = currentEpisode.season != null ? currentEpisode.season.id : currentEpisode.seasonRef;
						synchronized (CatalogueDatabase.getInstance()) {
							menu.showLoadingAnimation();
							CatalogueDatabase.getInstance().retrieveSeason(null, seasonId, null);
							try {
								CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
							} catch (InterruptedException e) {
							}
							menu.hideLoadingAnimation();
						}
						currentEpisode.season = (Season) CatalogueDatabase.getInstance().getCached(seasonId);
					}

					if (currentEpisode.season.series == null) {
						String seriesId = currentEpisode.season.series != null ? currentEpisode.season.series.id : currentEpisode.season.seriesRef;
						synchronized (CatalogueDatabase.getInstance()) {
							menu.showLoadingAnimation();
							CatalogueDatabase.getInstance().retrieveSeries(null, seriesId, null);
							try {
								CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
							} catch (InterruptedException e) {
							}
							menu.hideLoadingAnimation();
						}
						currentEpisode.season.series = (Series) CatalogueDatabase.getInstance().getCached(seriesId);
					}
				}

				Episode episode = (Episode) cached;
				if (episode.season == null || episode.season.episode.length == 0) {
					String seasonId = episode.season != null ? episode.season.id : episode.seasonRef;
					synchronized (CatalogueDatabase.getInstance()) {
						menu.showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveSeason(
								null, seasonId, null);
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {
						}
						menu.hideLoadingAnimation();
					}
					episode.season = (Season) CatalogueDatabase
							.getInstance().getCached(seasonId);
				}

				if (episode.season.series == null) {
					String seriesId = episode.season.series != null ? episode.season.series.id :episode.season.seriesRef;
					synchronized (CatalogueDatabase.getInstance()) {
						menu.showLoadingAnimation();
						CatalogueDatabase.getInstance().retrieveSeries(
								null, seriesId, null);
						try {
							CatalogueDatabase.getInstance().wait(
									Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {
						}
						menu.hideLoadingAnimation();
					}
					episode.season.series = (Series) CatalogueDatabase
							.getInstance().getCached(seriesId);
				}

				setPopUp(popFreeviewVodStop);
				popFreeviewVodStop.show(this, title, resumeView, playFromBegin, (Episode) cached);
			} else {
				if (title.nextEpisodeId != null) {
					synchronized (CatalogueDatabase.getInstance()) {
						ArrayList list = new ArrayList();
						list.add(title.nextEpisodeId);
						CatalogueDatabase.getInstance().retrieveEpisodes(null, list, null);
						try {
							CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
						} catch (InterruptedException e) {
						}
					}
					cached = CatalogueDatabase.getInstance().getCached(title.nextEpisodeId);

					if (Log.DEBUG_ON) {
						Log.printDebug("PlayScreenUI showStopPop, nextEpisode=" + cached);
					}
				}

				if (cached != null) {

					if (title instanceof Episode) {
						Episode currentEpisode = (Episode) title;
						if (currentEpisode.season == null || currentEpisode.season.episode.length == 0) {
							String seasonId = currentEpisode.season != null ? currentEpisode.season.id : currentEpisode.seasonRef;
							synchronized (CatalogueDatabase.getInstance()) {
								menu.showLoadingAnimation();
								CatalogueDatabase.getInstance().retrieveSeason(null, seasonId, null);
								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
								menu.hideLoadingAnimation();
							}
							currentEpisode.season = (Season) CatalogueDatabase.getInstance().getCached(seasonId);
						}

						if (currentEpisode.season.series == null) {
							String seriesId = currentEpisode.season.series != null ? currentEpisode.season.series.id : currentEpisode.season.seriesRef;
							synchronized (CatalogueDatabase.getInstance()) {
								menu.showLoadingAnimation();
								CatalogueDatabase.getInstance().retrieveSeries(null, seriesId, null);
								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
								menu.hideLoadingAnimation();
							}
							currentEpisode.season.series = (Series) CatalogueDatabase.getInstance().getCached(seriesId);
						}
					}

					Episode episode = (Episode) cached;
					if (episode.season == null || episode.season.episode.length == 0) {
						String seasonId = episode.season != null ? episode.season.id : episode.seasonRef;
						synchronized (CatalogueDatabase.getInstance()) {
							menu.showLoadingAnimation();
							CatalogueDatabase.getInstance().retrieveSeason(
									null, seasonId, null);
							try {
								CatalogueDatabase.getInstance().wait(
										Constants.MS_PER_MINUTE);
							} catch (InterruptedException e) {
							}
							menu.hideLoadingAnimation();
						}
						episode.season = (Season) CatalogueDatabase
								.getInstance().getCached(seasonId);
					}

					if (episode.season.series == null) {
						String seriesId = episode.season.series != null ? episode.season.series.id :episode.season.seriesRef;
						synchronized (CatalogueDatabase.getInstance()) {
							menu.showLoadingAnimation();
							CatalogueDatabase.getInstance().retrieveSeries(
									null, seriesId, null);
							try {
								CatalogueDatabase.getInstance().wait(
										Constants.MS_PER_MINUTE);
							} catch (InterruptedException e) {
							}
							menu.hideLoadingAnimation();
						}
						episode.season.series = (Series) CatalogueDatabase
								.getInstance().getCached(seriesId);
					}

					setPopUp(popFreeviewVodStop);
					popFreeviewVodStop.show(this, title, resumeView, playFromBegin, (Episode) cached);
				} else {
					// VDTRMASTER-5956
					if (Log.DEBUG_ON) {
						Log.printDebug("PlayScreenUI showStopPop, nextEpisode=" + cached);
					}

					if (title instanceof Episode) {
						Episode currentEpisode = (Episode) title;
						if (currentEpisode.season == null || currentEpisode.season.episode.length == 0) {
							String seasonId = currentEpisode.season != null ? currentEpisode.season.id : currentEpisode.seasonRef;
							synchronized (CatalogueDatabase.getInstance()) {
								menu.showLoadingAnimation();
								CatalogueDatabase.getInstance().retrieveSeason(null, seasonId, null);
								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
								menu.hideLoadingAnimation();
							}
							currentEpisode.season = (Season) CatalogueDatabase.getInstance().getCached(seasonId);
						}

						if (currentEpisode.season.series == null) {
							String seriesId = currentEpisode.season.series != null ? currentEpisode.season.series.id : currentEpisode.season.seriesRef;
							synchronized (CatalogueDatabase.getInstance()) {
								menu.showLoadingAnimation();
								CatalogueDatabase.getInstance().retrieveSeries(null, seriesId, null);
								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
								menu.hideLoadingAnimation();
							}
							currentEpisode.season.series = (Series) CatalogueDatabase.getInstance().getCached(seriesId);
						}
					}

					setPopUp(popFreeviewVodStop);
					popFreeviewVodStop.show(this, title, resumeView, playFromBegin, (Episode) cached);
				}
			}
		} else {
			setPopUp(popVodStop);
			popVodStop.show(this, title, resumeView, next, playFromBegin, order, nextIsOrder);

		}
		stopPopUpShowTime = 0;
		repaint();
	}
	
	public Bundle getNextBundle() {
		return nextBundle;
	}

	public BaseElement getNextInPlayAll() {
	    if (playAll != null && playAll.size() - 2 > idxOfBundle) {
            return (BaseElement) playAll.get(idxOfBundle + 1);
        }

        return null;
    }

	public boolean keyPause(boolean force) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI keyPause currentMode: "
					+ modeStr() + ", force = " + force + ", curNpt = " + currentNpt);
		}
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS) {
			Log.printDebug("PlayScreenUI keyPause ignored by status");
			return false; // no action
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keyPause ignored by stable");
			return false; // ignore for stability
		}
		//if (fromResumeViewing == false && System.currentTimeMillis() - startTimestamp < FIFTEEN_SECONDS + limitTime) {
		if (limit()) {
			showCannot();
			Log.printDebug("PlayScreenUI keyPause ignored by limit");
			return false;
		}
		
		// DDC-090
		if (isActiveDDC090) {
			long curTime = System.currentTimeMillis();
			
			if (curTime - lastRequestTime < 300) {
				showCannot();
				Log.printDebug("PlayScreenUI keyPause ignored by 1st delay");
				return false;
			}
			
			if (!force && currentMode == VODEvents.PAUSE) {
				// comment out for VDTRMASTER-5320
//				UsageManager.getInstance().addUsageLog("1108", sspId, "1303",
//						String.valueOf(currentNpt));
				//updateFlipBar(FLIP_PLAY);
				keyPlay();
			} else {
				
				lastRequestTime = System.currentTimeMillis();
				isReachedResponse = false;

				VideoControllerImpl.getInstance().pause(sspId);
                // VTBACKEND-1002
                startFirstDelayTimer();
                startSecondDelayTimer();
			}
			
		} else {

			if (!force && currentMode == VODEvents.PAUSE) {
				// comment out for VDTRMASTER-5320
//				UsageManager.getInstance().addUsageLog("1108", sspId, "1303",
//						String.valueOf(currentNpt));
				//updateFlipBar(FLIP_PLAY);
				keyPlay();
			} else {
				// comment out for VDTRMASTER-5320
//				UsageManager.getInstance().addUsageLog("1108", sspId, "1301",
//						String.valueOf(currentNpt));

				// VDTRMASTER-5666
				Log.printDebug("PlayScreenUI keyPause, currentNpt=" + currentNpt + ", runtime=" + runtime
						+ ", targetTime=" + (runtime - 2 * Constants.MS_PER_MINUTE));
				if (currentNpt > (runtime - 2 * Constants.MS_PER_MINUTE)) {
					setNpt(currentNpt, false);
				} else {
					setNpt(currentNpt, true);
				}
				
				VideoControllerImpl.getInstance().pause(sspId);
				currentMode = VODEvents.PAUSE;
				updateFlipBar(FLIP_PAUSE);
	
				debug = sspId + " " + modeStr() + " " + title;
			}
		}
		return true;
	}

	public boolean keyFF() {
		if (Log.DEBUG_ON) {
			Log.printDebug("keyFF currentMode : " + modeStr()
					+ ", currentNpt : " + currentNpt);
		}
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS || flipBar == karaokeBar) {
			Log.printDebug("PlayScreenUI keyFF ignored by status");
			return false;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keyFF ignored by stable");
			return false; // ignore for stability
		}
//		if (System.currentTimeMillis() - skipTimestamp < TWO_SECONDS) {
//			Log.printDebug("PlayScreenUI keyFF ignored by flipbar stable");
//			return; // ignore for stability
//		}
		//if (fromResumeViewing == false && System.currentTimeMillis() - startTimestamp < FIFTEEN_SECONDS + limitTime) {
		if (limit()) {
			showCannot();
			Log.printDebug("PlayScreenUI keyFF ignored by limit");
			return false;
		}
		
		// DDC-090
		if (isActiveDDC090) {
			long curTime = System.currentTimeMillis();
			
			if (curTime - lastRequestTime < 300 && isReachedResponse == false) {
				showCannot();
				Log.printDebug("PlayScreenUI keyFF ignored by 1st delay");
				return false;
			}
			
			lastRequestTime = System.currentTimeMillis();
			isReachedResponse = false;

			int scale = 0;
			switch (currentMode) {
			case VODEvents.PLAY:
				scale = VODEvents.FF_SCALE_1;
				break;
			case VODEvents.FF_SCALE_1:
				scale = VODEvents.FF_SCALE_2;
				break;
			case VODEvents.FF_SCALE_2:
				scale = VODEvents.FF_SCALE_3;
				break;
			case VODEvents.FF_SCALE_3:
				scale = VODEvents.FF_SCALE_4;
				break;
			case VODEvents.FF_SCALE_4:
				scale = VODEvents.FF_SCALE_1;
				break;
			default:
				scale = VODEvents.FF_SCALE_1;
				break;
			}
			VideoControllerImpl.getInstance().play(sspId, scale);

			// VTBACKEND-1002
            startFirstDelayTimer();
            startSecondDelayTimer();
		} else {
		
			int scale = 0;
			switch (currentMode) {
			case VODEvents.PLAY:
				scale = VODEvents.FF_SCALE_1;
				break;
			case VODEvents.FF_SCALE_1:
				scale = VODEvents.FF_SCALE_2;
				break;
			case VODEvents.FF_SCALE_2:
				scale = VODEvents.FF_SCALE_3;
				break;
			case VODEvents.FF_SCALE_3:
				scale = VODEvents.FF_SCALE_4;
				break;
			case VODEvents.FF_SCALE_4:
				scale = VODEvents.FF_SCALE_1;
				break;
			default:
				scale = VODEvents.FF_SCALE_1;
				break;
			}
			// comment out for VDTRMASTER-5320
//			UsageManager.getInstance().addUsageLog("1108", sspId, "1302",
//					String.valueOf(currentNpt));
			currentMode = scale;
			updateFlipBar(FLIP_FF);
			skipTimestamp = System.currentTimeMillis();
			VideoControllerImpl.getInstance().play(sspId, scale);
	
			if (currentNpt > (runtime - (FIFTEEN_SECONDS + TWO_SECONDS))) { // FF pressed after the 15 sec before the end
				passed = 3;
			}
		}
		debug = sspId + " " + modeStr() + " " + title;
		
		return true;
	}

	public boolean keyRew() {
		if (Log.DEBUG_ON) {
			Log.printDebug("keyRew currentMode :" + modeStr());
		}
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS || flipBar == karaokeBar) {
			Log.printDebug("PlayScreenUI keyRew ignored by status");
			return false;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keyRew ignored by stable");
			return false; // ignore for stability
		}
//		if (System.currentTimeMillis() - skipTimestamp < TWO_SECONDS) {
//			Log.printDebug("PlayScreenUI keyRew ignored by flipbar stable");
//			return; // ignore for stability
//		}
		//if (fromResumeViewing == false && System.currentTimeMillis() - startTimestamp < FIFTEEN_SECONDS + limitTime) {
		if (limit()) {
			showCannot();
			Log.printDebug("PlayScreenUI keyRew ignored by limit");
			return false;
		}
		
		// DDC-090
		if (isActiveDDC090) {
			long curTime = System.currentTimeMillis();

			if (curTime - lastRequestTime < 300 && isReachedResponse == false) {
				showCannot();
				Log.printDebug("PlayScreenUI keyRew ignored by 1st delay");
				return false;
			}

			lastRequestTime = System.currentTimeMillis();
			isReachedResponse = false;

			int scale = 0;
			switch (currentMode) {
			case VODEvents.PLAY:
				scale = VODEvents.REW_SCALE_1;
				break;
			case VODEvents.REW_SCALE_1:
				scale = VODEvents.REW_SCALE_2;
				break;
			case VODEvents.REW_SCALE_2:
				scale = VODEvents.REW_SCALE_3;
				break;
			case VODEvents.REW_SCALE_3:
				scale = VODEvents.REW_SCALE_4;
				break;
			case VODEvents.REW_SCALE_4:
				scale = VODEvents.REW_SCALE_1;
				break;
			default:
				scale = VODEvents.REW_SCALE_1;
				break;
			}
			
			VideoControllerImpl.getInstance().play(sspId, scale);

            // VTBACKEND-1002
            startFirstDelayTimer();
            startSecondDelayTimer();
		} else {
			int scale = 0;
			switch (currentMode) {
			case VODEvents.PLAY:
				scale = VODEvents.REW_SCALE_1;
				break;
			case VODEvents.REW_SCALE_1:
				scale = VODEvents.REW_SCALE_2;
				break;
			case VODEvents.REW_SCALE_2:
				scale = VODEvents.REW_SCALE_3;
				break;
			case VODEvents.REW_SCALE_3:
				scale = VODEvents.REW_SCALE_4;
				break;
			case VODEvents.REW_SCALE_4:
				scale = VODEvents.REW_SCALE_1;
				break;
			default:
				scale = VODEvents.REW_SCALE_1;
				break;
			}
			// comment out for VDTRMASTER-5320
//			UsageManager.getInstance().addUsageLog("1108", sspId, "1307",
//					String.valueOf(currentNpt));
			currentMode = scale;
			updateFlipBar(FLIP_REW);
			skipTimestamp = System.currentTimeMillis();
			VideoControllerImpl.getInstance().play(sspId, scale);

		}
		
		debug = sspId + " " + modeStr() + " " + title;
		
		return true;
	}

	private boolean keySkip(int newNpt) {
//		if (nextNpt >= 0) {
//			Log.printDebug("keySkip(), nextNpt >= 0");
//			return false;
//		}
		if (System.currentTimeMillis() - skipTimestamp < TWO_SECONDS) {
			Log.printDebug("keySkip(), too frequent");
			return false;
		}
		// comment out for VDTRMASTER-5320
//		UsageManager.getInstance().addUsageLog("1108", sspId, "1304",
//				String.valueOf(newNpt));
		//freelife VDTRMASTER-6201
		VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY, newNpt);
		/*
		if (currentMode == VODEvents.PAUSE && newNpt > 0)
			VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY_SLOW, newNpt);
		else
			VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY, newNpt);
		*/
		skipTimestamp = System.currentTimeMillis();
		if (currentMode == VODEvents.PAUSE && newNpt != 0) {
			//freelife VDTRMASTER-6201
			//try {Thread.sleep(500);} catch (Exception e){}
			
			VideoControllerImpl.getInstance().pause(sspId);
			nextNpt = -1;
			currentMode = VODEvents.PAUSE;
		} else {
			//nextNpt = newNpt;
			currentMode = VODEvents.PLAY;
		}
		debug = sspId + " " + modeStr() + " " + title;

		showFlipBar(true);
		return true;
	}
	
	private boolean keySkipForwardCompanion() {
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS) {
			Log.printDebug("PlayScreenUI keySkipForwardCompanion ignored by status");
			return false;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keySkipForwardCompanion ignored by stable");
			return false; // ignore for stability
		}
		if (limit()) {
			showCannot();
			Log.printDebug("PlayScreenUI keySkipForwardCompanion ignored by limit");
			return false;
		}
		long npt = (nextNpt == -1 ? currentNpt : nextNpt) + 30 * Constants.MS_PER_SECOND;
		if (npt > runtime) {
			npt = runtime - FIFTEEN_SECONDS;
		}
		Log.printDebug("keySkipForwardCompanion(), runTime = " + runtime + " npt = " + currentNpt + " -> " + npt);
		if (keySkip((int) npt)) {
			updateFlipBar(FLIP_SKIP_FORWARD);
		}
		
		return true;
	}
	
	private boolean keySkipBackwardCompanion() {
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS) {
			Log.printDebug("PlayScreenUI keySkipBackwardCompanion ignored by status");
			return false;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keySkipBackwardCompanion ignored by stable");
			return false; // ignore for stability
		}
		if (limit()) {
			Log.printDebug("PlayScreenUI keySkipBackwardCompanion ignored by limit");
			showCannot();
			return false;
		}
		long npt = (nextNpt == -1 ? currentNpt : nextNpt) - 8 * Constants.MS_PER_SECOND;
		if (npt < 0) {
			npt = 0;
		}
		Log.printDebug("keySkipBackwardCompanion(), runTime = " + runtime + " npt = " + currentNpt + " -> " + npt);
		if (keySkip((int) npt)) {
			updateFlipBar(FLIP_SKIP_BACK);
		}
		
		return true;
	}

	private boolean keySkipFoward() {
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS) {
			Log.printDebug("PlayScreenUI keySkipForward ignored by status");
			return false;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keySkipForward ignored by stable");
			return false; // ignore for stability
		}
		//if (fromResumeViewing == false && System.currentTimeMillis() - startTimestamp < FIFTEEN_SECONDS + limitTime) {
		if (limit()) {
			showCannot();
			Log.printDebug("PlayScreenUI keySkipForward ignored by limit");
			return false;
		}
		// REW_FF
		//long npt = (nextNpt == -1 ? currentNpt : nextNpt) + 30 * Constants.MS_PER_SECOND;
		long npt = (nextNpt == -1 ? currentNpt : nextNpt) + runtime / 10;;
		if (npt > runtime) {
			npt = runtime - FIFTEEN_SECONDS;
		}
		Log.printDebug("keySkipForward(), runTime = " + runtime + " npt = " + currentNpt + " -> " + npt);
		if (keySkip((int) npt)) {
			updateFlipBar(FLIP_SKIP_FORWARD);
		}
		return true;
	}

	private boolean keySkipBackward() {
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS) {
			Log.printDebug("PlayScreenUI keySkipBackward ignored by status");
			return false;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keySkipBackward ignored by stable");
			return false; // ignore for stability
		}
		//if (fromResumeViewing == false && System.currentTimeMillis() - startTimestamp < FIFTEEN_SECONDS + limitTime) {
		if (limit()) {
			Log.printDebug("PlayScreenUI keySkipBackward ignored by limit");
			showCannot();
			return false;
		}
		// REW_FF
//		long npt = (nextNpt == -1 ? currentNpt : nextNpt) - 8 * Constants.MS_PER_SECOND;
		long npt = (nextNpt == -1 ? currentNpt : nextNpt) - runtime / 10;
		if (npt < 0) {
			npt = 0;
		}
		Log.printDebug("keySkipBackward(), runTime = " + runtime + " npt = " + currentNpt + " -> " + npt);
		if (keySkip((int) npt)) {
			updateFlipBar(FLIP_SKIP_BACK);
		}
		
		return true;
	}

	private void keyNumeric(int section) {
		if (currentMode == VODEvents.STOP || currentMode == VODEvents.STATUS) {
			Log.printDebug("PlayScreenUI keyNumeric ignored by status");
			return;
		}
		if (System.currentTimeMillis() - startTimestamp < THREE_SECONDS) {
			Log.printDebug("PlayScreenUI keyNumeric ignored by stable");
			return; // ignore for stability
		}
		//if (fromResumeViewing == false && System.currentTimeMillis() - startTimestamp < FIFTEEN_SECONDS + limitTime) {
		if (limit()) {
			Log.printDebug("PlayScreenUI keyNumeric ignored by limit");
			showCannot();
			return;
		}
		if (sectionSet == false) {
			return;
		}
		int npt = skipSections[section];
		nextNpt = -1;
		if (keySkip(npt)) {
			updateFlipBar(currentMode == VODEvents.PAUSE ? FLIP_PAUSE
					: FLIP_PLAY);
		}
	}

	private void queryStatus() {
		if (currentMode != VODEvents.STATUS && currentMode != VODEvents.STOP) {
			VideoControllerImpl.getInstance().queryStatus();
		}
	}
	
	// DDC-090
	private void updateCurrentNPT() {
		if (currentMode != VODEvents.STATUS && currentMode != VODEvents.STOP) {
			VideoControllerImpl.getInstance().updateCurrentNPT();
		}
	}
	
	private void setNpt(int npt, boolean errorPopup) {
		Log.printDebug("setNpt(), bm = " + bookmark 
				+ ", newNpt = " + npt 
				+ ", err = " + errorPopup
				+ ", curNpt = " + currentNpt 
				+ ", cached = " + cacheNpt
				+ ", runTime = " + runtime);
		
		if (System.currentTimeMillis() - lastSaveTime < 2000) {
			Log.printDebug("setNpt(), ignore a request in short time");
			return;
		}
		lastSaveTime = System.currentTimeMillis();
		
		cacheNpt = npt;
		if (bookmark == null) {
			return;
		}
		if (npt != 1 && bookmark.getCurrentNPT() == npt / 1000) {
			Log.printDebug("Already stored...");
			return;
		}
		boolean ori = true;
		if (errorPopup == false) {
			ori = CommunicationManager.getInstance().turnOnErrorMessage(false);
		}
		boolean ret = BookmarkManager.getInstance().setNPT(bookmark.getBookmarkID(), npt);
		if (errorPopup == false) {
			CommunicationManager.getInstance().turnOnErrorMessage(ori);
		}
		if (ret) {
			bookmark.setCurrentNPT(npt / 1000);
		}
	}

	public void receiveVODEvent(int event) {
		long time = System.currentTimeMillis();
		
		// DDC-090
		if (event != VODEvents.STATUS) {
			isReachedResponse = true;
			lastRequestTime = 0;
		}
		
		lastNpt = currentNpt;
		currentNpt = VideoControllerImpl.getInstance().getCurrentNPT();
		if (currentNpt > 0) {
			haveStream = true;
		}
		if (currentNpt > FIFTEEN_SECONDS + limitTime) {
			noLimit = true;
		}
		if (currentNpt < TWO_SECONDS) {//(runtime - (FIFTEEN_SECONDS + TWO_SECONDS))) {
			passed = 0;
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("receiveVODEvent, currentMode = " + modeStr() + 
					", event = "+ modeStr(event) + ", cur = " + currentNpt + ", passed = " + passed);
		}
		
		//DDC-090
		// VDTRMASTER-5731
		if (event != VODEvents.STATUS 
				&& VideoControllerImpl.getInstance().getLastStatus() == VideoControllerImpl.LSC_NO_PERMISSION) {
			if (Log.DEBUG_ON) {
				Log.printDebug("receiveVODEvent, received LSC_NO_PERMISSION");
			}
			showCannot();
			return;
		}
		
		switch (event) {
		case SessionEventType.SETUP_SUCCEEDED:
			// comment out for VDTRMASTER-5320
//			UsageManager.getInstance()
//					.addUsageLog("1108", sspId, "1300", sspId);
			int npt = -1;
			needLockCheck = false;
			CommunicationManager.getInstance().debugLock = "";
			if (bookmark != null) { // purchased, don't need check first frame
				npt = BookmarkManager.getInstance().getNPT(orderableItem);
				
				if (Log.DEBUG_ON) {
					Log.printDebug("receiveVODEvent, from bookmark npt = " + npt);
				}
				cacheNpt = npt;
				if (npt == 0) {
					noLimit = false;
				} else if (npt == 1000) {
					npt = 1;
				}
			}
			
			MenuController.getInstance().hideLoadingAnimation();
			if (npt > 0) {
				noLimit = true;
				currentMode = VODEvents.PLAY;
				updateFlipBar(FLIP_PLAY);
				VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY,
						npt);
			} else if (npt < 0 && cacheNpt > 0 && flipBar != karaokeBar) {
				currentMode = VODEvents.PLAY;
				updateFlipBar(FLIP_PLAY);
				VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY,
						cacheNpt);
			} else {
				if (bookmark == null) {
					needLockCheck = true;
					networkLocked = false;
				}
				currentMode = VODEvents.PLAY;
				updateFlipBar(FLIP_PLAY);
				VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY);
				//R7.4 sykim VDTRMASTER-6177 if bookmark is null, set cacheNpt
//				if (bookmark == null) {
//					if (Log.DEBUG_ON) {
//						Log.printDebug("receiveVODEvent, bookmark = null and cacheNpt = " + cacheNpt);
//					}
//					VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY, cacheNpt);
//				} else {
//					VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY);
//				}
				
			}
			debug = sspId + " " + modeStr() + " " + title;
			startTimestamp = System.currentTimeMillis();
			haveStream = false;
			
			Resources.appStatus = VODService.APP_WATCHING;
			VODServiceImpl.getInstance().logPlaybackStopped(false);
			try {
				CommunicationManager.getInstance().getMonitorService().reserveVideoContext(Resources.APP_NAME, null);
				SharedMemory.getInstance().put(Resources.APP_NAME+SharedDataKeys.VIDEO_PROGRAM_NAME, title.getTitle());
				SharedMemory.getInstance().put(Resources.APP_NAME+SharedDataKeys.VIDEO_RESOLUTION, new Boolean(selectFromPopup == Resources.HD_EN || selectFromPopup == Resources.HD_FR));
			} catch (RemoteException e1) {
				e1.printStackTrace();
			} catch (AbstractMethodError ame) {} // need latest monitor
			
			if (isActiveDDC090) {
				new Thread("Initial status") {
					public void run() {
						try {
							Thread.sleep(5000);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						if (Log.DEBUG_ON) {
							Log.printDebug("receiveVODEvent, Initial status run()");
						}
						
						lscStatusCount = 0;
						VideoControllerImpl.getInstance().queryStatus();
					}
				}.start();
			}
			
			break;
		case VODEvents.RESUME:
			keyPlay();
			Resources.appStatus = VODService.APP_WATCHING;
			break;
		case SessionEventType.STREAM_CONTROL_FAILED:
			MenuController.getInstance().hideLoadingAnimation();
			error("VOD603", null);
			break;
		case SessionEventType.SETUP_FAILED:
			MenuController.getInstance().hideLoadingAnimation();
			DSMCCSessionEvent dsmccEvt = VideoControllerImpl.getInstance().getLastEvent();
			if (dsmccEvt != null) {
				error("VOD602-" + dsmccEvt.getResponseCode(), null);
			}
			break;
		case SessionEventType.SETUP_FAILED_IOE:
			MenuController.getInstance().hideLoadingAnimation();
			error("VOD215", null);
			break;
		case SessionEventType.SETUP_TIMEDOUT:
			MenuController.getInstance().hideLoadingAnimation();
			if (Environment.EMULATOR) {
				showStopPop(false, true);
			} else {
				error("VOD215", null);// jira 3664 //"VOD225", null);
			}
			break;
		case SessionEventType.SERVICEGROUP_FAILED:
			MenuController.getInstance().hideLoadingAnimation();
			error("VOD502", null);
			break;
		case VODEvents.NO_CABLE_CARD_MAC:
			MenuController.getInstance().hideLoadingAnimation();
			error("VOD503", null);
			break;
		case SessionEventType.RELEASE_SUCCEEDED:
			currentMode = VODEvents.STOP;
			cacheNpt = currentNpt;
			// comment out for VDTRMASTER-5320
//			UsageManager.getInstance()
//					.addUsageLog("1108", sspId, "1310", sspId);
			break;
		case SessionEventType.RELEASE_FAILED:
			currentMode = VODEvents.STATUS;
			break;
		case SessionEventType.RELEASE_REQUIRED:
			break;
		case VODEvents.PLAY:
			if (currentMode != VODEvents.PLAY) {
				// currentMode = VODEvents.PLAY;
				// updateFlipBar(FLIP_PLAY);
			}
			
			if (isActiveDDC090) {
				if (VideoControllerImpl.getInstance().isPlaySlowMode() && flipBar == vodBar) {
					currentMode = VODEvents.PLAY_SLOW;
					updateFlipBar(FLIP_SLOW);
				} else {
		
					if (currentMode == VODEvents.PAUSE) {
						VODServiceImpl.getInstance().logPlaybackPaused(false);
					}
							
					currentMode = VODEvents.PLAY;
					updateFlipBar(FLIP_PLAY);
				}
				
				// VDTRMASTER-5819
				Resources.appStatus = VODService.APP_WATCHING;
		
				//R7.4 freelife VDTRMASTER-6192 
				
				if (passed == 1) {
					passed = 2;
					Log.printDebug("receiveVODEvent, passed 15 sec, try to pause, cur = " + currentNpt);
					VideoControllerImpl.getInstance().pause(sspId);
					currentMode = VODEvents.PAUSE;
					updateFlipBar(FLIP_PAUSE);
	
					debug = sspId + " " + modeStr() + " " + title;
				}
				
				
				debug = sspId + " " + modeStr() + " " + title;
			} else {
			
				Resources.appStatus = VODService.APP_WATCHING;
				if (cacheNpt == 0) {
					setNpt(1, false);
				}
				if (passed == 1) {
					passed = 2;
					Log.printDebug("receiveVODEvent, passed 15 sec, try to pause, cur = " + currentNpt);
	//				keyPause(true);
					VideoControllerImpl.getInstance().pause(sspId);
					currentMode = VODEvents.PAUSE;
					updateFlipBar(FLIP_PAUSE);
	
					debug = sspId + " " + modeStr() + " " + title;
				}
			}
			break;
		case VODEvents.PAUSE:
//			if (bookmark != null && cacheNpt != currentNpt) {
//				cacheNpt = currentNpt;
//				BookmarkManager.getInstance().setNPT(bookmark.getBookmarkID(),
//						currentNpt);
//			}
			
			// DDC-090
			if (isActiveDDC090) {
				// comment out for VDTRMASTER-5320
//				UsageManager.getInstance().addUsageLog("1108", sspId, "1301",
//						String.valueOf(currentNpt));

				// VDTRMASTER-5666
				Log.printDebug("receiveVODEvent, Pause, currentNpt=" + currentNpt + ", runtime=" + runtime
						+ ", targetTime=" + (runtime - 2 * Constants.MS_PER_MINUTE));
				if (currentNpt > (runtime - 2 * Constants.MS_PER_MINUTE)) {
					setNpt(currentNpt, false);
				} else {
					setNpt(currentNpt, true);
				}
				
				currentMode = VODEvents.PAUSE;
				updateFlipBar(FLIP_PAUSE);
	
				debug = sspId + " " + modeStr() + " " + title;
			} 
			
			Resources.appStatus = VODService.APP_STARTED;
			
			if (Log.DEBUG_ON) {
				Log.printDebug("receiveVODEvent, preparingNextContent=" + preparingNextContent);
			}
			
			if (passed == 2 && !preparingNextContent) {
				// VDTRMASTER-5525
				passed = 0;
				showStopPop(false, true);
				setNpt(currentNpt, false);
			}
			VODServiceImpl.getInstance().logPlaybackPaused(true);
			break;
		case VODEvents.STOP:
//			if (bookmark != null && cacheNpt != currentNpt) {
//				cacheNpt = currentNpt;
//				BookmarkManager.getInstance().setNPT(bookmark.getBookmarkID(),
//						currentNpt);
//			}
			
			if (isActiveDDC090) {
				
				hideFlipBar();
				
				Resources.appStatus = VODService.APP_STARTED;
				currentMode = VODEvents.STOP;
				updateFlipBar(FLIP_STOP);
			}
			
			Resources.appStatus = VODService.APP_STARTED;
			// currentMode = VODEvents.STOP;
			// updateFlipBar(FLIP_STOP);
			// doAction();
			break;
		case VODEvents.REW_SCALE_1:
		case VODEvents.REW_SCALE_2:
		case VODEvents.REW_SCALE_3:
		case VODEvents.REW_SCALE_4:
			// currentMode = event;
			// updateFlipBar(FLIP_REW);
			if (isActiveDDC090) {
				int scale = 0;
				switch (currentMode) {
				case VODEvents.PLAY:
					scale = VODEvents.REW_SCALE_1;
					break;
				case VODEvents.REW_SCALE_1:
					scale = VODEvents.REW_SCALE_2;
					break;
				case VODEvents.REW_SCALE_2:
					scale = VODEvents.REW_SCALE_3;
					break;
				case VODEvents.REW_SCALE_3:
					scale = VODEvents.REW_SCALE_4;
					break;
				case VODEvents.REW_SCALE_4:
					scale = VODEvents.REW_SCALE_1;
					break;
				default:
					scale = VODEvents.REW_SCALE_1;
					break;
				}
				// comment out for VDTRMASTER-5320
//				UsageManager.getInstance().addUsageLog("1108", sspId, "1307",
//						String.valueOf(currentNpt));
				currentMode = scale;
				updateFlipBar(FLIP_REW);
				skipTimestamp = System.currentTimeMillis();
			}
			break;
		case VODEvents.FF_SCALE_1:
		case VODEvents.FF_SCALE_2:
		case VODEvents.FF_SCALE_3:
		case VODEvents.FF_SCALE_4:
			// currentMode = event;
			// updateFlipBar(FLIP_FF);
			if (isActiveDDC090) {
				int scale = 0;
				switch (currentMode) {
				case VODEvents.PLAY:
					scale = VODEvents.FF_SCALE_1;
					break;
				case VODEvents.FF_SCALE_1:
					scale = VODEvents.FF_SCALE_2;
					break;
				case VODEvents.FF_SCALE_2:
					scale = VODEvents.FF_SCALE_3;
					break;
				case VODEvents.FF_SCALE_3:
					scale = VODEvents.FF_SCALE_4;
					break;
				case VODEvents.FF_SCALE_4:
					scale = VODEvents.FF_SCALE_1;
					break;
				default:
					scale = VODEvents.FF_SCALE_1;
					break;
				}
				// comment out for VDTRMASTER-5320
//				UsageManager.getInstance().addUsageLog("1108", sspId, "1302",
//						String.valueOf(currentNpt));
				currentMode = scale;
				updateFlipBar(FLIP_FF);
				skipTimestamp = System.currentTimeMillis();
		
				if (currentNpt > (runtime - (FIFTEEN_SECONDS + TWO_SECONDS))) { // FF pressed after the 15 sec before the end
					passed = 3;
				}
			}
			break;
		case VODEvents.STATUS:
			break;
		case VODEvents.END_OF_STREAM:
			currentNpt = 1; // It will be stored in keyStop()
			haveStream = true;
			if (flipBar == karaokeBar) {
				Resources.appStatus = VODService.APP_STARTED;
				VideoControllerImpl.getInstance().stop(sspId);
				cacheNpt = 0;
				if (CategoryUI.getPurchasedPartyPack() == null) {
					showStopPop(false, false);
					break;
				}
				// or already purchased
				Log.printDebug("Karaoke end, repeat = " + repeat);
				if (FlipBarAction.REPEAT_ONE.getName().equals(repeat)) {
					playFromBegin();
					break;
				} else {
					if (playlist.size() - 1 == idxOfBundle
							&& FlipBarAction.REPEAT_ALL.getName().equals(repeat)) {
						// last song but repeat
						idxOfBundle = -1;
					}
					if (playlist.size() - 1 > idxOfBundle) {
						title = (Video) playlist.get(++idxOfBundle);
						playout = title.getPlayout();
						sspId = playout.name;
						runtime = playout.duration * Constants.MS_PER_SECOND;
						playFromBegin();
						break;
					} else {
						showStopPop(false, true);
					}
				}
			} else {
				if (currentMode == VODEvents.PLAY) {
					startTimestamp = 0; // force key stop
					keyStop(false, false);
				} else if (passed > 0) {
					startTimestamp = 0; // force key stop
					keyStop(false, true);
				} else if (currentMode >= VODEvents.FF_SCALE_1 && currentMode <= VODEvents.FF_SCALE_4 && passed == 0) {
					passed = 1;
										
					if (isActiveDDC090) {
						// during ad time, client can not request to pause,
						// so in case of DDC-090, client work like select Stop.

						//R7.4 freelife VDTRMASTER-6192
						
						Log.printDebug("receiveVODEvent, passed 15 sec, try to play, cur = " + currentNpt);
						VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY, (int) (runtime - FIFTEEN_SECONDS - Constants.MS_PER_SECOND));
						currentMode = VODEvents.PLAY;
						
						//startTimestamp = 0; // force key stop
						//keyStop(false, true);
						//flipBar.setMediaTime(currentNpt);
					} else {
						Log.printDebug("receiveVODEvent, passed 15 sec, try to play, cur = " + currentNpt);
						VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY, (int) (runtime - FIFTEEN_SECONDS - Constants.MS_PER_SECOND));
						currentMode = VODEvents.PLAY;
					}
				}
				// comment out for VDTRMASTER-5320
//				UsageManager.getInstance().addUsageLog("1108", sspId, "1306",
//						sspId);
			}
			break;
		default:
			break;
		}
		debug = sspId + " " + modeStr() + " " + title;
		repaint();
		if (flipBarOn) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI receiveVODEvent setMediaTime cur = "
						+ currentNpt);
			}
			flipBar.setMediaTime(currentNpt);
		}
//		if (currentNpt > 0 || event == VODEvents.END_OF_STREAM) {
//			haveStream = true;
//		}
		if (needLockCheck) {
			CommunicationManager.getInstance().checkLock(this);
			long gap = System.currentTimeMillis() - startTimestamp;
			long timeout = dataCenter.getInt("LOCK_TIMEOUT") * Constants.MS_PER_SECOND;
			Log.printDebug("checkLock, lock = " + networkLocked + ", gap = " + gap + ", timeout = " + timeout);
			if (networkLocked == false && gap > timeout) {
				if (error217 == false) {
					error217 = true;
					error("VOD217", null);
				}
			}
		}
		
		// update a logic for VDTRMASTER-5074
		// for VBM log, write a playback log after purchase in case of Bundle.
		if (haveStream && needPurchase && networkLocked) {
			tryPurchase();
			VbmController.getInstance().writePlayback(true, sspId);
		} else if (!needPurchase && event == SessionEventType.SETUP_SUCCEEDED) {
			VbmController.getInstance().writePlayback(true, sspId);
		}
		
		long time2 = System.currentTimeMillis();
		Log.printDebug("PlayScreenUI receiveVODEvent elap = " + (time2 - time));
	}
	
	public Offer getOffer() {
		return offer;
	}
	
	public Playout getPlayout() {
		return playout;
	}
	
	public void notifyCheckLock(boolean locked) {
		Log.printDebug("notifyCheckLock, locked = " + locked);
		networkLocked = locked;
		if (networkLocked) {
			needLockCheck = false;
		}
	}
	
	private void tryPurchase() {
		Log.printDebug("PlayScreenUI, tryPurchase, offer = " + offer + ", bookmark = " + bookmark);
		needPurchase = false;
		if (offer != null && bookmark == null) {
			if (bundle != null) {
				MoreDetail[] md = bundle.getTitles();
				String[] titles = new String[md.length];
				for (int i = 0; i < titles.length; i++) {
					titles[i] = ((Video) md[i]).getSspId(selectFromPopup);
//					Playout p = ((Video) md[i]).getPlayout(selectFromPopup,
//							false);
//					if (p != null) {
//						titles[i] = p.name;
//					} else {
//						Log.printError("PlayScreenUI, can not found the playout for " + bundle.toString(true) 
//								+ " in " + ((Video) md[i]).toString(true) + "\n" + ((Video) md[i]).dumpOffer() + "\n" + ((Video) md[i]).dumpPlayout());
//
//						error("VOD104", null);
//					}
				}
				if (bundle.isFree()) {
					CommunicationManager.getInstance().turnOnErrorMessage(false);
				}
				boolean ret = BookmarkManager.getInstance().purchaseBundle(
						bundle.getSspId(selectFromPopup), offer.amount,
						offer.leaseDuration, titles);
				if (ret) {
					bookmark = BookmarkManager.getInstance().getPurchased(orderableItem);
//					CatalogueDatabase.getInstance().registerBySspId(sspId,
//							title);
					
					// make a following two line as comment
					// VDTRMASTER-5312 [CQ][PO1/2/3][R4][VOD] ▪ User interactions shall always be blocked during the first 15 seconds + Pub duration 
//					Log.printDebug("PlayScreenUI, purchaseBundle success, try setNpt(1)");
//					setNpt(1, false);
				} else {
					if (bundle.isFree() == false) {
						menu.back();
					}
				}
			} else {
				if (title.isFree()) {
					CommunicationManager.getInstance().turnOnErrorMessage(false);
				}

//				if (Log.DEBUG_ON) {
				//					Log.printDebug("PlayScreenUI, tryPurchase, title.isFreeview=" + title.isFreeview);
				//				}
				//
				//				BaseUI lastScene = menu.getLastScene();
				//				if (Log.DEBUG_ON) {
				//					Log.printDebug("PlayScreenUI, tryPurchase, lastScene=" + lastScene);
				//				}
				//				boolean isExistedFreeBtn = false;
				//				if (lastScene != null) {
				//					String[] actions = lastScene.getActionButton(title);
				//					if (actions != null && actions.length > 0) {
				//						if (Log.DEBUG_ON) {
				//							Log.printDebug("PlayScreenUI, tryPurchase, title.actions[0]=" + actions[0]);
				//						}
				//
				//						if (actions[0].charAt(0) == '!') {
				//							isExistedFreeBtn = true;
				//						}
				//					}
				//				}
				//
				//				if (Log.DEBUG_ON) {
				//					Log.printDebug("PlayScreenUI, showStopPop, bookmark=" + bookmark);
				//					Log.printDebug("PlayScreenUI, showStopPop, isExistedFreeBtn=" + isExistedFreeBtn);
				//				}
				//
				//				if (isExistedFreeBtn) {
				////					bookmark = BookmarkManager.getInstance().purchaseAsset(
				////							orderableItem, "0.00", offer.leaseDuration);
				//					if (Log.DEBUG_ON) {
				//						Log.printDebug("PlayScreenUI, tryPurchase, don't try purchaseAsset for Freeview");
				//					}
				//					return;
				//				} else {
				//					bookmark = BookmarkManager.getInstance().purchaseAsset(orderableItem, offer.amount, offer.leaseDuration);
				//				}

				bookmark = BookmarkManager.getInstance().purchaseAsset(orderableItem, offer.amount, offer.leaseDuration);

				boolean error = bookmark.getErrorCode() != BookmarkManager.NO_ERROR;
				if (error && title.isFree()) {
					error = false;
					// even if error, ignore for free contents
				}
				if (error) {
					menu.back();
				} else {
					if (expire != null) {
						expire.removeTVTimerWentOffListener(this);
						TVTimer.getTimer().deschedule(expire);
					}
					expire = new TVTimerSpec();
					expire.addTVTimerWentOffListener(this);
					bookmark = BookmarkManager.getInstance().getPurchased(orderableItem);
					if (bookmark != null) {
						long msExpire = (bookmark.getLeaseEnd() - System.currentTimeMillis());
						expire.setDelayTime(msExpire);
						try {
							expire = TVTimer.getTimer().scheduleTimerSpec(expire);
							Log.printDebug("PlayScreenUI, expire timer setup, " + (msExpire / Constants.MS_PER_SECOND) + " secs");
						} catch (TVTimerScheduleFailedException e) {}
						// make a following two line as comment
						// VDTRMASTER-5312 [CQ][PO1/2/3][R4][VOD] ▪ User interactions shall always be blocked during the first 15 seconds + Pub duration 
//						Log.printDebug("purchaseAsset success, try setNpt(1)");
//						setNpt(1, false);
					} else {
						Log.printWarning("PlayScreenUI, Couldn't find the bookmark of " + orderableItem);
					}
				}
				CatalogueDatabase.getInstance().registerBySspId(orderableItem, title);
			}
			CommunicationManager.getInstance().turnOnErrorMessage(true);
		}
	}
	
	
	private ErrorMessageListener errorListenr = new ErrorMessageListener() {
		public void actionPerformed(int buttonType) throws RemoteException {
			Log.printDebug("PlayScreenUI, actionPerformed, button = " + buttonType + ", cur = " + menu.getCurrentScene());
			menu.hideLoadingAnimation();
			if (menu.getCurrentScene() instanceof PlayScreenUI) {
				currentMode = VODEvents.STOP;
				menu.back();
			}
		}
	};
	
	public ErrorMessageListener getErrorMessageListener() {
		return errorListenr;
	}

	public void error(String error, Hashtable param) {
		menu.hideLoadingAnimation();
		if (error != null) {
			CommunicationManager.getInstance().errorMessage(error, errorListenr, param, false);
//			EventQueue.invokeLater(new Runnable() {
//				public void run() {
//					if (menu.getCurrentScene() instanceof PlayScreenUI) {
//						menu.back();
//					}
//				}
//			});
		}
//		new Thread("error(), " + error) {
//			public void run() {
//				try {
//					Thread.sleep(2000);
//				} catch (InterruptedException e) {
//				}
//				menu.back();
//				// keyStop(false);
//			}
//		}.start();
	}

	public boolean isKaraoke() {
		return flipBar == karaokeBar;
	}

	public void popUpClosed(UIComponent popup, Object msg) {
		remove(popup);
		setPopUp(null);
		repaint();
		
		if (popup.equals(popVodStop) || popup.equals(popFreeviewVodStop)) {
			int action = ((Integer) msg).intValue();
			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI popUpClosed action : "
						+ action + ", cur = " + modeStr());
			}
			menu.setNext(null);
			switch (action) {
			case PopVODStop.PLAY_FROM_BEGINNING:
				if (currentMode == VODEvents.PAUSE) {
					currentMode = VODEvents.PLAY;
					updateFlipBar(FLIP_PLAY);
					flipBar.setMediaTime(0);
					showFlipBar(true);
					
					VideoControllerImpl.getInstance().play(sspId, VODEvents.PLAY, 0);
					debug = sspId + " " + modeStr() + " " + title;
				} else {
					menu.showLoadingAnimation();
					currentNpt = 0;
					setNpt(1, true);
//					if (bookmark != null) {
//						BookmarkManager.getInstance().setNPT(
//								bookmark.getBookmarkID(), 0);
//					}
//					cacheNpt = 0;
					playFromBegin();
				}
				break;
			case PopVODStop.RESUME_VIEWING:
				if (currentMode == VODEvents.PAUSE) {
					keyPlay();
				} else {
					menu.showLoadingAnimation();
					playFromBegin();
				}
				break;
			case PopVODStop.LIVE_TV:
				try {
					CommunicationManager.getInstance().getMonitorService().releaseVideoContext();
					SharedMemory.getInstance().remove(Resources.APP_NAME+SharedDataKeys.VIDEO_PROGRAM_NAME);
					SharedMemory.getInstance().remove(Resources.APP_NAME+SharedDataKeys.VIDEO_RESOLUTION);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				} catch (AbstractMethodError ame) {} // need latest monitor
				menu.exit(MenuController.EXIT_FROM_WATCH, null);
				break;
			case PopVODStop.BACK_TO_MENU:
				if (currentMode == VODEvents.PAUSE) {
					Resources.appStatus = VODService.APP_STARTED;
					VideoControllerImpl.getInstance().stop(sspId);
					currentMode = VODEvents.STOP;
				}
				menu.back();
				break;
			case PopVODStop.ORDER_NEXT:
				if (bundle != null) {
					// VDTRMASTER-5187 [CQ][PO1/2/3][R4][VOD] The system doesn't propose to purchase the next available bundle
					Object foundBundle = menu.getNextBundle(bundle);
					Log.printDebug("PlayScreenUI showStopPop, for bundle, foundBundle = " + foundBundle);
					if (foundBundle != null) {
						
						if (foundBundle instanceof Bundle) {
							BaseUI parentUI = menu.getParentUI();
							if (parentUI instanceof ListViewUI && ((ListViewUI) parentUI).isWishList()) {
								menu.setMoveToNext(true);
								menu.goToDetail(bundle, null);
								return;
							}
							Bundle nextBundle = (Bundle) foundBundle;
							Log.printDebug("PlayScreenUI showStopPop, for bundle, nextBundle isPurchased = " + BookmarkManager.getInstance().isPurchased(nextBundle));
							if (!BookmarkManager.getInstance().isPurchased(nextBundle)) {
								menu.setNext(nextBundle);
							}
						} else { // VDTRMASTER-5373
							BaseUI parentUI = menu.getParentUI();
							if (parentUI instanceof ListViewUI && ((ListViewUI) parentUI).isWishList()) {
								menu.setMoveToNext(true);
								menu.goToDetail(bundle, null);
								return;
							}
						}
					} else  {
						BaseUI parentUI = menu.getParentUI();
						if (parentUI instanceof ListViewUI && ((ListViewUI) parentUI).isWishList()) {
							menu.setMoveToNext(true);
							menu.goToDetail(bundle, null);
							return;
						}
					}
				} else if (playAll != null) {
					Object foundBundle = menu.getNextBundle(title);
					Log.printDebug("PlayScreenUI showStopPop, for playAll, foundBundle = " + foundBundle);
					if (foundBundle != null) {
						
						if (foundBundle instanceof Bundle) {
							Bundle nextBundle = (Bundle) foundBundle;
							Log.printDebug("PlayScreenUI showStopPop, for playAll, nextBundle isPurchased = " + BookmarkManager.getInstance().isPurchased(nextBundle));
							if (!BookmarkManager.getInstance().isPurchased(nextBundle)) {
								menu.setNext(nextBundle);
							}
						}
					}
				} else {
					// VDTRMASTER-5210
					if (menu.getNext(title) != null) {
						menu.setNext(menu.getNext(title));
					} else if (nextBundle != null) {
						menu.setNext(nextBundle);
					}
				}
				menu.back();
				break;
			case PopVODStop.VIEW_NEXT:
			    // R7.3
                VbmController.getInstance().writeSelection("Play Next");
				if (playAll != null) {
					playNextContent(false);
				} else if (bundle != null) {
				    if (Log.DEBUG_ON) {
                        Log.printDebug("PlayScreenUI, nextbundle = " + nextBundle);
                        Log.printDebug("PlayScreenUI, bundle.getTitles().length = " + bundle.getTitles().length);
                        Log.printDebug("PlayScreenUI, idxOfBundle = " + idxOfBundle);
                    }
                    // VDTRMASTER-6079
                    if (idxOfBundle + 1 < bundle.getTitles().length) {
                        title = (Video) bundle.getTitles()[++idxOfBundle];
                        Log.printDebug("PlayScreenUI, next title = " + title.toString(true));
                        bookmark = BookmarkManager.getInstance()
                                .getPurchased(title);
                        Log.printDebug("PlayScreenUI, bookmark = " + bookmark);
                        if (bookmark != null) {
                            orderableItem = bookmark.getOrderableName();
                            Log.printDebug("PlayScreenUI, orderableItem = " + orderableItem);
                            playout = title.getPlayout(orderableItem);
                            Log.printDebug("PlayScreenUI, playout = " + playout);
                            if (playout != null) {
                                sspId = playout.name;
                                runtime = playout.duration * Constants.MS_PER_SECOND;
                                //						noLimit = false; // noLimit will be 'true' if bookmark has npt > 0
                                playFromBegin();
                            } else {
                                Log.printError("PlayScreenUI, can not found the playout for " + selectFromPopup
                                        + " in " + title.toString(true) + "\n" + title.dumpOffer() + "\n" + title.dumpPlayout());
                                error("VOD104", null);
                            }
                        } else { // bookmark is null
                            Log.printError("PlayScreenUI, bookmark isn't exists for 'view next' title = " + title.toString(true));
                            error("VOD104", null);
                        }
                    } else {
				        // VDTRMASTER-6079
				        // check nextBundle
                        if (nextBundle != null) {
                            idxOfBundle = 0;
                            readyNextContent(nextBundle, false);
                            playNextContent(nextBundle, false);
                        }
                    }
				} else if (playlist != null) {
					title = (Video) playlist.get(++idxOfBundle);
					playout = title.getPlayout();
					sspId = playout.name;
					runtime = playout.duration * Constants.MS_PER_SECOND;
					cacheNpt = 0;
					playFromBegin();
				} else {
					BaseElement next = menu.getNext(title);
					if (menu.isAdultBlocked((MoreDetail) next) || menu.isBlocked((MoreDetail) next)) {
						menu.setNext(menu.getNext(title));
						menu.back();
					} else {
						readyNextContent(next, false);
						playNextContent(next, false);
					}
				}
				break;
			case PopVODStop.ORDER_PARTYPACK:
				menu.back();
				menu.purchaseKaraoke(false);
				break;
			case PopFreeviewVODStop.FREE_WATCH_NOW:
				CacheElement cached = CatalogueDatabase.getInstance().getCached(title.nextEpisodeId);
				readyNextContent(cached, false);
				cacheNpt = 0;
				playFromBegin();
				break;
			case PopFreeviewVODStop.SUBSCRIBE:
				cached = CatalogueDatabase.getInstance().getCached(title.nextEpisodeId);
				// VDTRMASTER-5933
				String buttonName = ((PopFreeviewVODStop)popup).getFocusedButton();

				if (buttonName.equals(dataCenter.getString(Resources.TEXT_WATCH))) {
					if (cached != null) {
						readyNextContent(cached, false);
					}
					cacheNpt = 0;
					playFromBegin();
				} else {
					// VDTRMASTER-5957 & VDTRMASTER-5972
					menu.back();
					if (cached != null) {
						processSubscribe(buttonName, (BaseElement) cached, true);
					} else {
						processSubscribe(buttonName, (BaseElement) title, true);
					}
					currentMode = VODEvents.STOP;
				}
				break;
			case PopFreeviewVODStop.VIEW_ALL_EPISODES:
				if (CategoryUI.getChannel() != null
						|| CategoryUI.getInstance().getCODCategory() != null) {
					// VDTRMASTER-6013
					CategoryUI.setBranded(-1);
					menu.goToAsset(title.nextEpisodeId, false, null);
				} else {
					// VDTRMASTER-5950
					BaseUI lastScene = menu.getLastScene();
					if (lastScene instanceof ListViewUI && ((ListViewUI) lastScene).isResumeViewing()) {
						// VDTRMASTER-6013
						CategoryUI.setBranded(-1);
						cached = CatalogueDatabase.getInstance().getCached(title.nextEpisodeId);
						menu.goToDetail((BaseElement) cached, null);
					} else {
						menu.back();
					}
				}

				break;
			} // end of switch (action)
		} else if (popup.equals(PopSelectOption.getInstance())) {
			selectFromPopup = ((Integer) ((Object[]) msg)[0]).intValue();
			if (selectFromPopup == PopSelectOption.BTN_CANCEL) {
				showStopPop(true, true);
				return;
			}
			String titleId = (String) ((Object[]) msg)[1];
			Object cached = CatalogueDatabase.getInstance().getCached(
					titleId);
			String rating = null;
			rating = ((MoreDetail) cached).getRating();
			offer = (Offer) ((Object[]) msg)[2];
			
			PreferenceProxy.getInstance().checkRightFilter(
					RightFilter.ALLOW_PURCHASE,
					rating,
					this,
					new String[] {
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_PURCHASE_PIN1),
							DataCenter.getInstance().getString(
									Resources.TEXT_MSG_PURCHASE_PIN2) });
		}
	}
	
	public void receiveCheckRightFilter(CheckResult checkResult) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI, receiveCheckRightFilter()"
					+ ", checkResult = " + checkResult + ", filter = "
					+ checkResult.getFilter() + ", response = "
					+ checkResult.getResponse());
		}
		if (PreferenceService.RESPONSE_SUCCESS != checkResult.getResponse()) {
			if (PreferenceService.RESPONSE_CANCEL == checkResult.getResponse()) {
				showStopPop(true, true);
			}
			return;
		}
		if (RightFilter.ALLOW_PURCHASE.equals(checkResult.getFilter())) {
			idxOfBundle--; // try it again
			needPurchase = true;
			playNextContent(true);
		}
	}

	public String findCurrentLabel() {
		return findLabel(title);
	}
	
	/**
	 * TVTimerWentOffListener implementation. Called in every minute.
	 *
	 * @param e
	 *            TVTimerWentOffEvent.
	 */
	public void timerWentOff(TVTimerWentOffEvent e) {
		if (e.getTimerSpec() == cannot) {
			cannot.removeTVTimerWentOffListener(this);
			TVTimer.getTimer().deschedule(cannot);
			cannot = null;
			cannotIcon = false;
			repaint();
		} else if (e.getTimerSpec() == expire) {
			Log.printInfo("current asset is expired");
			if (getPopUp() == popVodStop) {
				popUpClosed(popVodStop, new Integer(PopVODStop.BACK_TO_MENU));
			}
		} else if (e.getTimerSpec() == timer) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI: timerWentOff : currentMode : "
								+ modeStr());
			}
			switch (feedMode) {
			case MODE_LARGE:
				if (flipBarShowTime >= timeLarge * 2) {
					curIcon = NO_ICON;
				}
				break;
			case MODE_SMALL:
				if (flipBarShowTime >= timeSmall * 2) {
					curIcon = NO_ICON;
				}
				break;
			case MODE_MIXED:
				if (curIcon == LARGE_ICON && flipBarShowTime >= timeLarge * 2) {
					curIcon = SMALL_ICON;
					flipBarShowTime = 0;
				} else if (curIcon == SMALL_ICON
						&& flipBarShowTime >= timeSmall * 2) {
					curIcon = NO_ICON;
				}
				break;
			}
			repaint();
			if (flipBarOn) {
				// if (Log.DEBUG_ON) {
				// Log
				// .printDebug("PlayScreenUI: timerWentOff : flipBarShowTime : "
				// + flipBarShowTime);
				// }
				if (flipBarShowTime > MAX_FLIP_SHOW_TIME
						&& (currentMode == VODEvents.PLAY || 
							//R7.4 VDTRMASTER-6178 sykim
							currentMode == VODEvents.PLAY_SLOW)) {
					hideFlipBar();
					return;
				}
				// if ((flipStatus == FILP_SKIP_BACK || flipStatus ==
				// FLIP_SKIP_FORWARD)
				// && flipBarShowTime == 10) {
				// currentMode = VODEvents.PLAY;
				// updateFlipBar(FLIP_PLAY);
				// }
				if (flipBarShowTime > MAX_PAUSE_TIME
						&& currentMode == VODEvents.PAUSE) {
					hideFlipBar();
					showStopPop(true, true);
					return;
				}
				flipBarShowTime++;
				
				if (Log.DEBUG_ON) {
					Log.printDebug("PlayScreenUI, timerWentOff, isActiveDDC090=" + isActiveDDC090 + ", lscStatusCount=" + lscStatusCount);
				}
				
				//DDC-090
				if (isActiveDDC090) {
					if (currentMode != VODEvents.STATUS && currentMode != VODEvents.STOP) {
						updateCurrentNPT();
						// VDTRMASTER-5386
						if (Log.DEBUG_ON) {
							Log.printDebug("PlayScreenUI, timerWentOff, call self-status");
						}
						receiveVODEvent(VODEvents.STATUS);
						if (lscStatusIntervalLimit > 0 && ++lscStatusCount > lscStatusIntervalLimit) {
							lscStatusCount = 0;
							queryStatus();
						} else {
							if (Log.DEBUG_ON) {
								Log.printDebug("PlayScreenUI timerWentOff setMediaTime cur = " + currentNpt);
							}
							flipBar.setMediaTime(currentNpt);
						}
					}
				} else {
					queryStatus();
				}
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("PlayScreenUI, timerWentOff, isActiveDDC090=" + isActiveDDC090 + ", lscStatusCount=" + lscStatusCount);
				}
				
				//DDC-090
				if (isActiveDDC090) {
					if (currentMode != VODEvents.STATUS && currentMode != VODEvents.STOP) {
						updateCurrentNPT();
						// VDTRMASTER-5386
						if (Log.DEBUG_ON) {
							Log.printDebug("PlayScreenUI, timerWentOff, call self-status");
						}
						receiveVODEvent(VODEvents.STATUS);
						if (lscStatusIntervalLimit > 0 && ++lscStatusCount > lscStatusIntervalLimit) {
							lscStatusCount = 0;
							queryStatus();
						}
					}
				} else {
					if (lastCheckNptTime > CHECK_NPT_TIME) {
						lastCheckNptTime = 0;
						queryStatus();
					} else {
						lastCheckNptTime++;
					}
				}
			}
			if (getPopUp() != null && (getPopUp().equals(popVodStop) || getPopUp().equals(popFreeviewVodStop))) {
				if (stopPopUpShowTime > MAX_STOP_POPUP_SHOW_TIME) {
					menu.exit(MenuController.EXIT_NO_ACTION, null);
				}
				stopPopUpShowTime++;
			}
			if (System.currentTimeMillis() - startTimestamp > FIFTEEN_SECONDS && haveStream == false) {
				if (error217 == false) {
					error217 = true;
					error("VOD217", null);
				}
			}
		} else if (e.getTimerSpec() == firstDelayTime) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI, timerWentOff, firstDelayTime=" + firstDelayTime + ", isReachedResponse=" + isReachedResponse);
			}
			// DDC-090
			if (isReachedResponse) {
				stopSecondDelayTimer();
			} else {
				showCannot();
			}
		} else if (e.getTimerSpec() == secondDelayTime) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PlayScreenUI, timerWentOff, secondDelayTime=" + secondDelayTime + ", isReachedResponse=" + isReachedResponse);
			}
			// DDC-090
			if (!isReachedResponse) {
				showCannot();
			}
		}
	}

	public String modeStr() {
		return modeStr(currentMode);
	}
	public String modeStr(int evt) {
		switch (evt) {
		case VODEvents.STOP: return "STOP";
		case VODEvents.PAUSE: return "PAUSE";
		case VODEvents.PLAY: return "PLAY";
		case VODEvents.PLAY_SLOW: return "PLAY_SLOW";
		case VODEvents.RESUME: return "RESUME";
		case VODEvents.STATUS: return "STATUS";
		// REW_FF
//		case VODEvents.REW_SCALE_1: return "RW_4";
//		case VODEvents.REW_SCALE_2: return "RW_16";
//		case VODEvents.REW_SCALE_3: return "RW_64";
//		case VODEvents.REW_SCALE_4: return "RW_128";
//		case VODEvents.FF_SCALE_1: return "FF_4";
//		case VODEvents.FF_SCALE_2: return "FF_16";
//		case VODEvents.FF_SCALE_3: return "FF_64";
//		case VODEvents.FF_SCALE_4: return "FF_128";
		case VODEvents.REW_SCALE_1: return "RW_2";
		case VODEvents.REW_SCALE_2: return "RW_4";
		case VODEvents.REW_SCALE_3: return "RW_8";
		case VODEvents.REW_SCALE_4: return "RW_16";
		case VODEvents.FF_SCALE_1: return "FF_2";
		case VODEvents.FF_SCALE_2: return "FF_4";
		case VODEvents.FF_SCALE_3: return "FF_8";
		case VODEvents.FF_SCALE_4: return "FF_16";
		case VODEvents.END_OF_STREAM: return "EOS";
		
		case SessionEventType.SETUP_SUCCEEDED: return "SETUP_SUCCEEDED";
		case SessionEventType.SETUP_FAILED: return "SETUP_FAILED";
		case SessionEventType.SETUP_TIMEDOUT: return "SETUP_TIMEDOUT";
		case SessionEventType.SETUP_FAILED_IOE: return "SETUP_FAILED_IOE";
		case SessionEventType.RELEASE_SUCCEEDED: return "RELEASE_SUCCEEDED";
		case SessionEventType.RELEASE_FAILED: return "RELEASE_FAILED";
		case SessionEventType.RELEASE_TIMEDOUT: return "RELEASE_TIMEDOUT";
		case SessionEventType.RELEASE_REQUIRED: return "RELEASE_REQUIRED";
		case SessionEventType.SERVICEGROUP_SUCCEEDED: return "SERVICEGROUP_SUCCEEDED";
		case SessionEventType.SERVICEGROUP_FAILED: return "SERVICEGROUP_FAILED";
		case SessionEventType.SERVICEGROUP_CHANGED: return "SERVICEGROUP_CHANGED";
		case SessionEventType.STREAM_CONTROL_FAILED: return "STREAM_CONTROL_FAILED";
		}
		return "";
	}

	/**
	 * FlipBarListener implementation.
	 *
	 * @param action
	 *            FlipBarAction.
	 */
	public void actionPerformed(FlipBarAction action) {
		String actionName = action.getName();
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI FlipBarAction : "
					+ actionName);
		}
		if (actionName.equals(FlipBarAction.PLAY.getName())) {
			keyPlay();
		} else if (actionName.equals(FlipBarAction.PAUSE.getName())) {
			keyPause(false);
		} else if (actionName.equals(FlipBarAction.STOP.getName())) {
			keyStop(true, true);
		} else if (actionName.equals(FlipBarAction.FAST_FORWARD.getName())) {
			keyFF();
		} else if (actionName.equals(FlipBarAction.REWIND.getName())) {
			keyRew();
		} else if (actionName.equals(FlipBarAction.SKIP_BACKWARD.getName())) {
			keySkipBackward();
		} else if (actionName.equals(FlipBarAction.SKIP_FORWARD.getName())) {
			keySkipFoward();
		} else if (actionName.startsWith("REPEAT")) {
			repeat = actionName;
		}
	}

	public void notifyShown(long time) {

	}

	public void notifyHidden(boolean selection) {

	}

	// HDMIport
	public void configurationChanged(VideoOutputPort source, VideoOutputConfiguration oldConfig, VideoOutputConfiguration newConfig) {
		Log.printDebug("HDMI port config changed = " + newConfig);
	}

	public void connectionStatusChanged(VideoOutputPort source, boolean status) {
		Log.printDebug("HDMI port connection status = " + status);
		if (fhdContents == false) {
			return;
		}
		boolean fhdEnable = checkFHDenv();
		
		if (fhdEnable) {
			PreferenceProxy.getInstance().setVideoOutput(true);
			PreferenceProxy.getInstance().setFullHdVideoOutput();
		} else { // disconnected
			PreferenceProxy.getInstance().setVideoOutput(false);
		}
	}

	public void enabledStatusChanged(VideoOutputPort source, boolean status) {
		Log.printDebug("HDMI port enable = " + status);
	}

	public void updateVideoOutputStatus() {
		if (isUHDContent) {
			if (!VideoOutputUtil.getInstance().canPlayUHDContent()) {
				Log.printError("PlayScreenUI, updateVideoOutputStatus(), Error Code : VOD850");
				
				// HDCP 2.2
				Resources.appStatus = VODService.APP_STARTED;
				VideoControllerImpl.getInstance().stop(sspId);
				currentMode = VODEvents.STOP;
				menu.back();
				
				return;
			} else if (!VideoOutputUtil.getInstance().existHdcpApi() 
					|| !VideoOutputUtil.getInstance().isSupportedHdcp()) {
				Log.printError("PlayScreenUI, updateVideoOutputStatus(), Error Code : VOD851");
				
				// HDCP 2.2
				Resources.appStatus = VODService.APP_STARTED;
				VideoControllerImpl.getInstance().stop(sspId);
				currentMode = VODEvents.STOP;
				menu.back();
				return;
			}
		}
	}

	public void updateButtons() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI, updateButtons, currentPopup=" + getPopUp());
		}
		if (getPopUp() != null && getPopUp().equals(popFreeviewVodStop)) {
			menu.goToAsset(title.nextEpisodeId, false, null);
		}
	}

	// VDTRMASTER-5933
	private void processSubscribe(String actionMenu, BaseElement element, boolean fromFreeview) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PlayScreenUI, processSubscribe, actionMenu=" + actionMenu + ", element=" + element);
			Log.printDebug("PlayScreenUI, processSubscribe, " + dataCenter.getString(Resources.TEXT_SUBSCRIBE));
		}
		if (actionMenu.indexOf(dataCenter.getString(Resources.TEXT_SUBSCRIBE)) > -1
				|| dataCenter.getString(Resources.TEXT_CHANGE_MY_PKG).equals(actionMenu)
				|| dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG).equals(actionMenu)) {
			//R7.4 freelife VBM new measurement changed
			//VbmController.getInstance().writeSelection("Subscribe");
			if (fromFreeview == true)
				VbmController.getInstance().writeSelection("Freeview Subscribe");
			
			//currentScene.setPopUp(PopSubscribe.getInstance());

			CacheElement cached = CatalogueDatabase.getInstance().getCached(((Video) element).getId());
			Channel ch = ((Video) cached).channel;

			String entry = element instanceof Episode ? ISAService.ENTRY_POINT_VOD_E03 : ISAService.ENTRY_POINT_VOD_E02;
			CommunicationManager.getInstance().showISA(ch.callLetters, entry);

		} else if (actionMenu.indexOf(dataCenter.getString(Resources.TEXT_SUBSCRIBE_SVOD)) > -1) {
			
			//R7.4 freelife VBM new measurement changed
			//VbmController.getInstance().writeSelection("SubscribeSVOD");
			if (fromFreeview == true)
				VbmController.getInstance().writeSelection("Freeview Subscribe");

			Service service = null;
			Video video = (Video) element;
			service = video.service[0];
			if (service.description == null) {
				service = (Service) CatalogueDatabase.getInstance().getCached(service.id);
				video.service[0] = service;
			}

			if (CommunicationManager.getInstance().isaSupported(service.getId2()) || !CommunicationManager.getInstance()
					.isIncludedInExcludedData(service.getId2())) {
				String entry = element instanceof Episode ? ISAService.ENTRY_POINT_VOD_E05 : ISAService.ENTRY_POINT_VOD_E04;
				CommunicationManager.getInstance().showISA(service.getId2(), entry);
			} else {
				setPopUp(PopSubscribe.getInstance());
				PopSubscribe.getInstance().show(this, service);
			}
		}
	}
}

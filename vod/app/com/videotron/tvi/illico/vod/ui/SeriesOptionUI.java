package com.videotron.tvi.illico.vod.ui;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.Vector;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.effect.ArrowEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.gui.SeriesOptionRenderer;

public class SeriesOptionUI extends BaseUI {

	public static final int OPTION_FULL = 0;
	public static final int OPTION_BUNDLE = 1;
	public static final int OPTION_EPISODE = 2;

	private SeriesOptionRenderer seriesOptionRenderer = new SeriesOptionRenderer();
	private Season season;

	private ScrollTexts scrollText = new ScrollTexts();
	static private Rectangle recScrollText = new Rectangle(287, 410, 445, 68);
	static private int infoPageRowHeight = 17;
	static private Color cScrollTextColor = new DVBColor(255, 255, 255, 230);
	static String[] message = new String[3];

	private int[] buttonList = new int[] { BaseUI.BTN_SCROLL, BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };

	private int[] options;
	private ArrowEffect arrowEffect;

	private OptionScreen optionBox = new OptionScreen();
	public SeriesOptionUI() {
		scrollText.setTopMaskImage(dataCenter.getImage("01_des_sh_t04.png"));
		scrollText.setBottomMaskImage(dataCenter.getImage("01_des_sh_ser.png"));
		add(scrollText);

		Image btnPage = (Image) ((Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_PAGE);
		footer.linkWithScrollTexts(btnPage, scrollText);
		add(footer);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);

		arrowEffect = new ArrowEffect(this, 0);
	}

	public void start(boolean back) {
		if (!back) {
			Object[] param = MenuController.peekStack();
			season = (Season) param[MenuController.PARAM_DATA_IDX];

			setFocus(0);
			setRenderer(seriesOptionRenderer);

			setState(BaseUI.STATE_ACTIVATED);

			scrollText.setBounds(recScrollText);
			scrollText.setTopMaskImagePosition(177-recScrollText.x, 403-recScrollText.y);
			scrollText.setBottomMaskImagePosition(177-recScrollText.x, 452-recScrollText.y);
			scrollText.setFont(FontResource.DINMED.getFont(15));
			scrollText.setForeground(cScrollTextColor);
			scrollText.setRowHeight(infoPageRowHeight);
			scrollText.setRows(recScrollText.height / infoPageRowHeight);

			options = new int[optionLen()];
			int idx = 0;
			if (season.canBuySeason) {
				options[idx++] = OPTION_FULL;
			}
			if (season.canBuyBundle) {
				options[idx++] = OPTION_BUNDLE;
			}
			if (season.canBuyEpisode) {
				options[idx++] = OPTION_EPISODE;
			}
			prepare();
			setDescription();
		}
		setButtons(buttonList, null);
		super.start();
	}

	public Season getCatalogueList() {
		return season;
	}

	public int[] getOptions() {
		return options;
	}

	public int optionLen() {
		if (season == null)
			return 0;
		int cnt = 0;
		if (season.canBuyEpisode) {
			cnt++;
		}
		if (season.canBuyBundle) {
			cnt++;
		}
		if (season.canBuySeason) {
			cnt++;
		}
		return cnt;
	}

	public void prepare() {
		super.prepare();

		StringBuffer buf = new StringBuffer(dataCenter
				.getString(Resources.TEXT_MSG_FULL_SEASON));
		for (int i = 0; i < season.episode.length; i++) {
			buf.append('\n');
			buf.append(season.episode[i].getTitle());
		}
		message[0] = buf.toString();
		message[1] = dataCenter.getString(Resources.TEXT_MSG_ORDER_BUNDLE);
		message[2] = dataCenter.getString(Resources.TEXT_MSG_ORDER_EPISODE);
	}

	private void setDescription() {
		scrollText.setContents(message[options[focus]]);
	}

	public boolean handleKey(int key) {
		switch (key) {
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_ENTER:
			keyOK();
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		case HRcEvent.VK_PAGE_UP:
			keyPageUp();
			break;
		case HRcEvent.VK_PAGE_DOWN:
			keyPageDown();
			break;
		default:
			break;
		}
		return true;
	}

	public void keyLeft() {
		if (focus > 0) {
//			arrowEffect.start(new Rectangle(258 + focus * 141 + (optionLen() == 3 ? 0 : 70), 186, 0, 0), ArrowEffect.LEFT);
			focus--;
			setDescription();
			repaint();
		}
	}

	public void keyRight() {
		if (focus < optionLen() - 1) {
//			arrowEffect.start(new Rectangle(403 + focus * 141 + (optionLen() == 3 ? 0 : 70), 186, 0, 0), ArrowEffect.RIGHT);
			focus++;
			setDescription();
			repaint();
		}
	}

	public void keyPageUp() {
		footer.clickAnimation(Resources.TEXT_SCROLL_DESCRIPTION);
		scrollText.showPreviousPage();
	}

	public void keyPageDown() {
		footer.clickAnimation(Resources.TEXT_SCROLL_DESCRIPTION);
		scrollText.showNextPage();
	}

	public void keyBack() {
		menu.backwardHistory(false);
		menu.back();
		// 20190222 Touche project
		VbmController.getInstance().backwardHistory();
	}

	public void keyGreen() {
		Log.printDebug("SeriesOptionUI, keyGreen()");
		setPopUp(optionBox);
		optionBox.start(menu.getOptionMenu(0, null), menu);
		repaint();
	}

	public void keyOK() {
		int select = options[focus];
		switch (select) {
		case OPTION_FULL:
			Bundle bundle = (Bundle) CatalogueDatabase.getInstance().getCached(
					season.seasonBundle == null ? "" : season.seasonBundle);
			if (bundle != null) {
				if (BookmarkManager.getInstance().isPurchased(bundle)) {
					MoreDetail[] data = bundle.getTitles();
					menu.goToNextScene(UITemplateList.LIST_OF_TITLES, data);
				} else {
					menu.processActionMenu(dataCenter.getString(Resources.TEXT_ORDER_BUNDLE), bundle);
				}
			}
			break;
		case OPTION_BUNDLE:
			Bundle[] data = new Bundle[season.episodeBundles.length];
			for (int i = 0; i < data.length; i++) {
				data[i] = (Bundle) CatalogueDatabase.getInstance().getCached(
						season.episodeBundles[i]);
			}
			menu.forwardHistory(season);
			menu.goToNextScene(UITemplateList.LIST_OF_TITLES, data);
			break;
		case OPTION_EPISODE:
			menu.forwardHistory(season);
			
			Vector episodeVec = new Vector();
			
			for (int i = 0; i < season.episode.length; i++) {
				if (season.episode[i].inBundleOnly == false) {
					episodeVec.add(season.episode[i]);
				}
			}
			
			Episode[] singleEpisodes = new Episode[episodeVec.size()];
			singleEpisodes = (Episode[])episodeVec.toArray(singleEpisodes);
			
			menu.goToNextScene(UITemplateList.LIST_OF_TITLES, singleEpisodes);
			break;
		}
	}

	public Season getCurrentCatalogue() {
		return season;
	}

//	public void reflectVCDSResult(Object cmd) {
//		menu.hideLoadingAnimation();
//		super.prepare();
//		repaint();
//	}
}

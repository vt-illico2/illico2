package com.videotron.tvi.illico.vod.ui;

import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ActionBox;
import com.videotron.tvi.illico.vod.comp.KaraokeView;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Image;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.gui.KaraokeListViewRenderer;

public class KaraokeListViewUI extends BaseUI {
	public static final int FOCUS_LIST = 0;
	public static final int FOCUS_BUTTON = 1;
	public static final int FOCUS_BANNER = 2;
	public static final int CHANGE_ORDER = Integer.MAX_VALUE;

	private BaseElement[] catalogueList = new BaseElement[0];
	private int mode;
	private String title;
	private int[] offsetFocus;

	private KaraokeListViewRenderer listViewRenderer = new KaraokeListViewRenderer();
	private KaraokeView karaokeView = new KaraokeView();
	private ImageLabel karaokeLabel = new ImageLabel(null);
	private ImageLabel orderBanner = new ImageLabel(null);

	private ActionBox actionBox;
	private OptionScreen optionBox = new OptionScreen();

	private int[] buttonList = new int[] { BaseUI.BTN_PLAYLIST, BaseUI.BTN_OPTION };
	private int[] buttonList2 = new int[] { BaseUI.BTN_OPTION };

	public KaraokeListViewUI(int mode) {
		karaokeView.setBounds(54, 138, 537, 347 + 6); // 6 for animation
		add(karaokeView);
		add(orderBanner);
		add(karaokeLabel);

		actionBox = ActionBox.newInstance();
		add(actionBox);

		this.mode = mode;

		add(footer);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);
	}

	public void start(boolean back) {
		if (!back) {
			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];
			Log.printDebug("KaraokeListView, obj = " + obj);

			switch (mode) {
			case UITemplateList.KARAOKE_CONTENTS:
				CategoryContainer category = (CategoryContainer) obj;
				title = category.getTitle() + " (" + category.totalContents.length + ")";;
				setData(category.totalContents);
				ArrayList banner = new ArrayList();
				Image img = null;
				ImageGroup ig = null;
				String lang = Resources.currentLanguage == 0 ? Definition.LANGUAGE_EN : Definition.LANGUAGE_FR;
				for (int i = 0; i < category.category.description.imageGroup.length; i++) {
					ig = category.category.description.imageGroup[i];
					if (ImageRetriever.AFFICHE.equals(ig.usage)) {
						if (lang.equals(ig.language)) {
							banner.add(0, ig);
						} else {
							banner.add(ig);
						}
					}
				}
				if (banner.size() > 0) {
					ig = (ImageGroup) banner.get(0);
					img = ig.images[0];
					String url = dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + img.imageName + "." + ig.encoding;
					karaokeLabel.setImage(url);
					karaokeLabel.setVisible(true);
					karaokeLabel.setBounds(626, 138, img.width, img.height);
				}
				
				if (CategoryUI.getPurchasedPartyPack() == null) { // ready for order banner
					banner.clear();
					img = null;
					ig = null;
					
					for (int i = 0; i < category.parent.categorizedBundle.description.imageGroup.length; i++) {
						ig = category.parent.categorizedBundle.description.imageGroup[i];
						if (ImageRetriever.ORDER_PAK_NARROW.equals(ig.usage)) {
							if (lang.equals(ig.language)) {
								banner.add(0, ig);
							} else {
								banner.add(ig);
							}
						}
					}
					
					if (banner.size() > 0) {
						ig = (ImageGroup) banner.get(0);
						img = ig.images[0];
						String url = dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + img.imageName + "." + ig.encoding;
						orderBanner.setImage(url);
						orderBanner.setVisible(true);
						orderBanner.setBounds(626, 
								actionBox.getY() - img.height, 
								img.width, img.height);
					}
				}
				break;
			case UITemplateList.KARAOKE_PLAYLIST:
				ArrayList list = menu.getPlaylist();
				BaseElement[] data = new BaseElement[list.size()];
				list.toArray(data);
				title = dataCenter.getString(Resources.TEXT_T_PLAYLILST);
				title = TextUtil.replace(title, "0", String.valueOf(data.length));
				setData(data);
				break;
			}
			setRenderer(listViewRenderer);
			prepare();
			setCurrentFocusUI(FOCUS_LIST);
			focus = 0;
			setFocus(0);
		} else {
		}
		updateButtons();

		super.start();

		repaint();
	}

	public String getTitle() {
		return title;
	}

	private void setActionButtons() {
		boolean contain = menu.getPlaylist().contains(karaokeView.getCurrentCatalogue());
		boolean purchased = CategoryUI.getPurchasedPartyPack() != null;
		String[] actionButtons = null;
		if (mode == UITemplateList.KARAOKE_CONTENTS) {
			if (purchased) {
				actionButtons = new String[] {
						DataCenter.getInstance().getString(Resources.TEXT_PLAY_SONG),
						DataCenter.getInstance().getString(contain ? Resources.TEXT_REMOVE_PLAYLIST : Resources.TEXT_ADD_PLAYLIST),
				};
			} else {
				actionButtons = new String[] {
						DataCenter.getInstance().getString(Resources.TEXT_PLAY_SONG),
				};
			}
		} else if (mode == UITemplateList.KARAOKE_PLAYLIST) {
			actionButtons = new String[] {
					DataCenter.getInstance().getString(Resources.TEXT_PLAY_ALL),
					DataCenter.getInstance().getString(Resources.TEXT_PLAY_SELECTED),
					DataCenter.getInstance().getString(Resources.TEXT_CHANGE_ORDER),
					DataCenter.getInstance().getString(Resources.TEXT_REMOVE_PLAYLIST),
			};
		} else {
			actionButtons = new String[] {
					DataCenter.getInstance().getString(Resources.TEXT_CONFIRM_CHANGE),
					DataCenter.getInstance().getString(Resources.TEXT_ADD_TOP),
					DataCenter.getInstance().getString(Resources.TEXT_CANCEL),
			};
		}
		actionBox.setActionBox(actionButtons);
		actionBox.setFocus(focus);
		
		orderBanner.setLocation(626, 
				actionBox.getY() - orderBanner.getHeight());
	}

	public void setData(BaseElement[] data) {
		boolean change = data.length != catalogueList.length;
		catalogueList = data;
		if (Log.DEBUG_ON) {
			Log.printDebug("setData, " + catalogueList.length);
		}
		karaokeView.setData(catalogueList, title);
		if (change) {
			focus = 0;
			//CatalogueDatabase.getInstance().retrieveImages(this, catalogueList);
		}
		repaint();
	}

	public void updateButtons() {
		setButtons(mode == UITemplateList.KARAOKE_CONTENTS /*&& CategoryUI.getInstance().getPartyPack() != null*/ ? buttonList : buttonList2, null);
		setActionButtons();
		int f = actionBox.getFocus();
		setActionButtons();
		actionBox.setFocus(f);
	}

	public int getCount() {
		return karaokeView.count();
	}

	public void flushData() {
		karaokeLabel.flush();
	}

	public boolean handleKey(int key) {
		Log.printDebug("KaraokeListViewUI, handleKey()" + ", code = " + key);
		karaokeView.handleKey(key);
		switch (key) {
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_INFO:
			keyInfo();
			break;
		case HRcEvent.VK_ENTER:
			keyOK();
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		}

		repaint();
		return true;
	}

	public void keyLeft() {
		setCurrentFocusUI(FOCUS_LIST);
	}

	public void keyRight() {
		setCurrentFocusUI(FOCUS_BUTTON);
	}

	public void keyUp() {
		if (getState() == BaseUI.STATE_ACTIVATED) {
			boolean ret = actionBox.moveFocus(true);
			if (ret && CategoryUI.getPurchasedPartyPack() == null) {
				setCurrentFocusUI(FOCUS_BANNER);
			}
		} else {
			setActionButtons();
		}
	}

	public void keyDown() {
		if (currentFocusUI == FOCUS_BANNER) {
			setCurrentFocusUI(FOCUS_BUTTON);
		} else if (getState() == BaseUI.STATE_ACTIVATED) {
			actionBox.moveFocus(false);
		} else {
			setActionButtons();
		}
	}

	public void keyGreen() {
		Log.printDebug("KaraokeListViewUI, keyGreen()");
		int opt = 0;
//		if (catalogueList == null || catalogueList.length == 0
//				|| catalogueList[0] instanceof Video == false) {
//			opt = 1;
//		} else {
//			opt = CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//					Resources.OPT_NAME) ? 2 : 1;
//		}
		setPopUp(optionBox);
		optionBox.start(menu.getOptionMenu(opt, null), menu);
		repaint();
	}

	/**
	 * Sets the focus of UI.
	 */
	public void setFocus(int newFocus) {
		setActionButtons();
	}

	public void keyPageUp() {
		// scrollText.showPreviousPage();
	}

	public void keyPageDown() {
		// scrollText.showNextPage();
	}

	public void keyBack() {
		if (mode == CHANGE_ORDER) {
			cancelChangeOrder();
		} else {
			if (mode != UITemplateList.KARAOKE_PLAYLIST) { // VDTRMASTER-4413
				menu.backwardHistory(false);
			}
			menu.back();
			// 20190222 Touche project
			VbmController.getInstance().backwardHistory();		
		}
	}

	public void keyInfo() {
//		if (state != BaseUI.STATE_ACTIVATED) { // key on list
//			menu.goToDetail(catalogueList[titleIndex], null);
//		}
	}

	public void keyOK() {
		if (currentFocusUI == FOCUS_LIST) {
			focus = 0;
			//keyRight();
			return;
		}
		if (currentFocusUI == FOCUS_BANNER) {
//			if (EventQueue.isDispatchThread()) {
//				actionBox.click();
//				try {
//					CommunicationManager.getInstance().requestTuner(null, null);
//					return;
//				} catch (NullPointerException npe) {}
//			} // 4691
			menu.purchaseKaraoke(true);
			return;
		}
		actionBox.click();
		String actionMenu = actionBox.getMenu();
		if (dataCenter.getString(Resources.TEXT_PLAY_SONG).equals(actionMenu)) {
			if (CategoryUI.getPurchasedPartyPack() != null || CategoryUI.isTrialSong((MoreDetail) karaokeView.getCurrentCatalogue())) {
				if (EventQueue.isDispatchThread()) {
					try {
						CommunicationManager.getInstance().requestTuner(null, karaokeView.getCurrentCatalogue(), 0);
						return;
					} catch (NullPointerException npe) {}
				}
				
				ArrayList list = new ArrayList();
				list.add(karaokeView.getCurrentCatalogue());
				menu.goToNextScene(UITemplateList.PLAY_SCREEN, list);
			} else {
				menu.purchaseKaraoke(true);
			}
		} else if (dataCenter.getString(Resources.TEXT_PLAY_SELECTED).equals(actionMenu)) {
			ArrayList list = new ArrayList();
			list.add(karaokeView.getCurrentCatalogue());
			menu.goToNextScene(UITemplateList.PLAY_SCREEN, list);
		} else if (dataCenter.getString(Resources.TEXT_PLAY_ALL).equals(actionMenu)) {
			VbmController.getInstance().writeSelection("Karaoke Play All");
			ArrayList list = new ArrayList();
			BaseElement[] playlist = karaokeView.getData();
			for (int i = 0; i < playlist.length; i++) {
				list.add(playlist[i]);
			}
			menu.goToNextScene(UITemplateList.PLAY_SCREEN, list);
		} else if (dataCenter.getString(Resources.TEXT_ADD_PLAYLIST).equals(actionMenu)) {
			VbmController.getInstance().writeSelection("Add to PlayList");
			if (CategoryUI.getPurchasedPartyPack() != null) {
				menu.getPlaylist().add(karaokeView.getCurrentCatalogue());
				PreferenceProxy.getInstance().storePlaylist();
				//updateButtons();
				keyLeft();
				menu.refreshEffect();
			} else {
				menu.purchaseKaraoke(true);
			}
		} else if (dataCenter.getString(Resources.TEXT_REMOVE_PLAYLIST).equals(actionMenu)) {
			VbmController.getInstance().writeSelection("Remove from PlayList");
			menu.getPlaylist().remove(karaokeView.getCurrentCatalogue());
			PreferenceProxy.getInstance().storePlaylist();
			if (mode == UITemplateList.KARAOKE_CONTENTS) {
				//updateButtons();
				keyLeft();
				menu.refreshEffect();
			} else {
				catalogueList = karaokeView.removeSelected();
				title = dataCenter.getString(Resources.TEXT_T_PLAYLILST);
				title = TextUtil.replace(title, "0", String.valueOf(catalogueList.length));
				//keyLeft();
				
				if (catalogueList.length == 0) {
					keyBack();
					return;
				} else {
					//setData(karaokeView.getData());
				}
			}
		} else if (dataCenter.getString(Resources.TEXT_CHANGE_ORDER).equals(actionMenu)) {
			offsetFocus = karaokeView.getOffsetFocus();
			karaokeView.resetPicked();
			mode = CHANGE_ORDER;
			updateButtons();
			keyLeft();
		} else if (dataCenter.getString(Resources.TEXT_CONFIRM_CHANGE).equals(actionMenu)) {
			offsetFocus = karaokeView.getOffsetFocus();
			setData(karaokeView.getData());
			menu.getPlaylist().clear();
			for (int i = 0; i < catalogueList.length; i++) {
				menu.getPlaylist().add(catalogueList[i]);
			}
			PreferenceProxy.getInstance().storePlaylist(); // update the list to flash
			karaokeView.setOffsetFocus(offsetFocus);
			mode = UITemplateList.KARAOKE_PLAYLIST;
			updateButtons();
			keyLeft();
		} else if (dataCenter.getString(Resources.TEXT_ADD_TOP).equals(actionMenu)) {
			karaokeView.addTop();
		} else if (dataCenter.getString(Resources.TEXT_CANCEL).equals(actionMenu)) {
			cancelChangeOrder();
		}
	}

	private void cancelChangeOrder() {
		setData(catalogueList);
		karaokeView.setOffsetFocus(offsetFocus);
		mode = UITemplateList.KARAOKE_PLAYLIST;
		updateButtons();
		keyLeft();
	}
	public boolean isChangeOrder() {
		return mode == CHANGE_ORDER;
	}
	public boolean isPlaylist() {
		return mode == UITemplateList.KARAOKE_PLAYLIST;
	}

	public BaseElement[] getCurrentCatalogues() {
		return catalogueList;
	}

	/**
	 * @param currentFocusUI
	 *            the currentFocusUI to set
	 */
	public void setCurrentFocusUI(int currentFocusUI) {
		this.currentFocusUI = currentFocusUI;
		if (currentFocusUI == FOCUS_LIST) {
			setState(BaseUI.STATE_DEACTIVATED);
			karaokeView.setState(BaseUI.STATE_ACTIVATED);
			orderBanner.setHighlight(null);
		} else if (currentFocusUI == FOCUS_BANNER) {
			setState(BaseUI.STATE_DEACTIVATED);
			karaokeView.setState(BaseUI.STATE_DEACTIVATED);
			orderBanner.setHighlight(dataCenter.getImage("01_kara_pack_action_foc.png"));
		} else {
			setState(BaseUI.STATE_ACTIVATED);
			karaokeView.setState(BaseUI.STATE_DEACTIVATED);
			orderBanner.setHighlight(null);
		}
	}

	public void selected(MenuItem item) {
		if (Resources.OPT_NAME.equals(item.getKey())) {
			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
					Resources.OPT_NAME);
			setData(catalogueList);
		} else if (Resources.OPT_DATE.equals(item.getKey())) {
			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
					Resources.OPT_DATE);
			setData(catalogueList);
		}
	}
}

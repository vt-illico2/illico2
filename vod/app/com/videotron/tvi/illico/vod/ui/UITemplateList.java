package com.videotron.tvi.illico.vod.ui;
/**
 * The Class UITemplateList.
 */
public interface UITemplateList {

    /** Category list with preview and vertical Posters. */
    public static final int MAIN = 0x01;

    /** Category list with preview and vertical posters. */
//    public static final int CATEGORY_VERTICAL_B = 0x02;

    /** Category list with preview and horizontal posters. */
//    public static final int CATEGORY_HORIZONTAL = 0x03;

    /** Category list with preview and mixed posters. */
//    public static final int CATEGORY_MIXED = 0x04;

    /** Category list with channels. */
//    public static final int CATEGORY_CHANNELS = 0x05;

    /** Category list with Menu Item Description (Image and text ). */
//    public static final int CATEGORY_DESCRIPTION = 0x0D;

    /** Category list with Menu Item Description (Image and Preview of Content). */
//    public static final int CATEGORY_DESCRIPTION_PREVIEW = 0x0E;

    /** Category list with BARKER. */
//    public static final int CATEGORY_BARKER = 0x12;

    /** List of titles with alternative views (Movies and Series). */
    public static final int LIST_OF_TITLES = 0x06;

    /** List of of Series. */
//    public static final int LIST_OF_SERIES = 0x23;
    
    /** List of of Seasons. */
//    public static final int LIST_OF_SEASONS = 0x07;

    /** Movie details (Or Event Details). */
    public static final int MOVIE_DETAIL = 0x08;

    /** Episodes (series). */
    public static final int LIST_OF_EPISODES = 0x09;

    /** Episode Bundle (Series). */
//    public static final int EPISODE_BUNDLE = 0x0A;

    /** Episode description (Episode bundle). */
//    public static final int EPISODE_DESCRIPTION = 0x0B;

    /** Bundle (Movie / Event). */
//    public static final int BUNDLE_MOVIE_EVENT = 0x0C;

    /** My rentals / My selections (in Options and preferences). */
//    public static final int MY_RENTASL_SELECTIONS = 0X0F;

    /** Music details. */
//    public static final int MUSIC_DETAIL = 0X10;

    /** Our picks. */
//    public static final int OUR_PICKS = 0X11;

    /** Karaoke launch. */
//    public static final int KARAOKE_LAUNCH = 0X13;

    /** Carousel View. */
    public static final int CAROUSEL_VIEW = 0X14;

    /** Wall of Posters view. */
    public static final int WALL_OF_POSTERS = 0X15;

    /** Wish List View. */
    public static final int WISH_LIST = 0X16;

    /** Theme page view. */
//    public static final int THEME_PAGE = 0X17;

    /** Channel page view. */
//    public static final int CHANNEL_PAGE = 0X18;

    /** Resume Viewing. */
    public static final int RESUME_VIEWING = 0X19;

    /** PLAY SCREEN. */
    public static final int PLAY_SCREEN = 0X20;

    /** BUNDLE_TITLE_LIST. */
    public static final int BUNDLE_DETAIL = 0X21;
    
    /** Normal List View. */
    public static final int LIST_VIEW = 0X22;
    
    /** Series Option. */
    public static final int SERIES_OPTIONS = 0X24;

    /** Full screen trailer. */
//    public static final int FULL_TRAILER = 0X25;
    
    /** About this channel. */
    public static final int ABOUT_CHANNEL = 0X30;
    
    /** Karaoke contents. */
    public static final int KARAOKE_CONTENTS = 0X40;
    
    /** Karaoke playlist. */
    public static final int KARAOKE_PLAYLIST = 0X41;
    
    /** Channel list. */
    public static final int CHANNEL_LIST = 0X50;
    
    /** Channel menu. */
    public static final int CHANNEL_MENU = 0X51;
}

package com.videotron.tvi.illico.vod.ui;

import java.awt.event.KeyEvent;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Locale;

import javax.tv.util.TVTimerWentOffEvent;

import com.videotron.tvi.illico.vod.comp.PopChangeView;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.PosterArray;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CacheElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Network;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.gui.WallofPostersFilmInfo;

public class WallofPostersFilmInfoUI extends BaseUI {
	public static final int MODE_NORMAL = 0;
	public static final int MODE_ALL_CHANNEL = 1;
	public static final int MODE_MY_CHANNEL = 2;
	private static final long serialVersionUID = -210110651692771811L;
	private WallofPostersFilmInfo wallofPostersFilmInfo = new WallofPostersFilmInfo();

	private PosterArray posterArray;
	private int mode;

	private BaseElement curCatal;
	private BaseElement[] catalogueList;
	private BaseElement[] allChannel;
	static private int[] buttonList = new int[] { BaseUI.BTN_VIEW, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST,
			BaseUI.BTN_OPTION };
	static private int[] buttonList2 = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_VIEW, BaseUI.BTN_PROFILE,
			BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonList3 = new int[] { BaseUI.BTN_MY_CHANNEL, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST,
				BaseUI.BTN_OPTION };
	static private int[] buttonList3_page = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_MY_CHANNEL, BaseUI.BTN_PROFILE, 
		BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonList4 = new int[] { BaseUI.BTN_ALL_CHANNEL, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST,
				BaseUI.BTN_OPTION };
	static private int[] buttonList4_page = new int[] { BaseUI.BTN_PAGE_V, BaseUI.BTN_ALL_CHANNEL, BaseUI.BTN_PROFILE,
		BaseUI.BTN_WISHLIST,BaseUI.BTN_OPTION };
	private OptionScreen optionBox = new OptionScreen();
	
	// VDTRMASTER-5690
	private Object param;
	
	public WallofPostersFilmInfoUI() {

		posterArray = PosterArray.newInstance();
		posterArray.setBounds(0, 80, 960, 403);
		posterArray.setOverflow(true);
		posterArray.setFont(FontResource.BLENDER.getFont(16));
		posterArray.prepare();
		add(posterArray, 0);

		add(footer);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);
	}

	public void start(boolean back) {
		if (!back) {
			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];
			
			// VDTRMASTER-5690
			this.param = obj;
			
			BaseElement[] data = null;
			BaseElement focused = null;
			// R5
			fromCategory = false;
			if (obj instanceof String) { // category
				Object cached = CatalogueDatabase.getInstance().getCached(obj);
				Log.printDebug("WallOfPosters, cached = " + cached);
				if (cached instanceof CategoryContainer) {
					CategoryContainer cat = (CategoryContainer) cached;
					menu.setLastCategoryId(cat.id);
					data = cat.totalContents;
					
					String opt = menu.getSortNormal();
					data = CatalogueDatabase.getInstance().sort(data, opt == null ? Resources.OPT_NAME : opt);
					// R5
					fromCategory = true;
				}
			} else {
				data = (BaseElement[]) param[MenuController.PARAM_DATA_IDX];

				if (param.length > MenuController.PARAM_DATA_IDX + 1) {
					focused = (BaseElement) param[MenuController.PARAM_DATA_IDX + 1];
				}
				
				// VDTRMASTER-5690
				this.param = DataCenter.getInstance().get("VIEW_PARAM");
			}
			setFocus(0);
			setRenderer(wallofPostersFilmInfo);
			prepare();

			setData(data, focused);
		} else {
			if (mode == MODE_ALL_CHANNEL) {
				MenuController.getInstance().setOpenAdultCategory(false, false);
			}

			// VDTRMASTER-5835
			if (MenuController.getInstance().getChageViewType() != PopChangeView.VIEW_WALL) {
				keyYellow();
				return;
			}
			
			// VDTRMASTER-5690
			if (Log.DEBUG_ON) {
				Log.printDebug("WallOfPosters, param=" + param);
			}
			// VDTRMASTER-5433
			if (param instanceof String) {
				CacheElement cached = CatalogueDatabase.getInstance().getCached(param);
				Log.printDebug("WallOfPosters, cached = " + cached);
				if (cached instanceof CategoryContainer) {
					
					boolean isApplied = false;
					CategoryContainer category = (CategoryContainer) cached;
					do {
						isApplied = CategoryUI.setBrandingElementByCategory(category);
						category = category.parent;
					} while (category != null && isApplied == false);
				}
			}
		}
		BaseRenderer.setBrandingBg(mode == MODE_NORMAL ? 1 : -1, null);

		if (mode == MODE_NORMAL) {
			ImageGroup bg = null;
			if (CategoryUI.isBranded()) {
				BrandingElement br = CategoryUI.getBrandingElement();
				bg = br.getBackground(false);
				try {
					BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
				} catch (Exception e) {
					BaseRenderer.setBrandingBg(-1, null);
				}
			}
			
//			Channel channel = null;
//			ImageGroup bg = null;
//			
//			
//			try {
//				channel = CategoryUI.getChannel();
//				if (channel.network != null) {
//					bg = channel.network.getBackground(false);
//				} else {
//					bg = channel.getBackground(false);
//				}
//				BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//			} catch (Exception e) {
//				try {
//					BrandingElement br = CategoryUI.getBrandingElement();
//					bg = br.getBackground(false);
//					BaseRenderer.setBrandingBg(1, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
//				} catch (Exception ee) { 
//					BaseRenderer.setBrandingBg(-1, null);
//				}
//			}
		}
		
		updateButtons();
		super.start();	
		
		setupTimer(true, 0);
		if (Resources.OPT_NAME.equals(menu.getSortNormal())) {
			menu.setNeedRapid(true);
		}
	}

	public void updateButtons() {
		switch (mode) {
		case MODE_NORMAL:
			// VDTRMASTER-5809
			setButtons(buttonList, getCurrentCatalogue());
			break;
		case MODE_ALL_CHANNEL:
			setButtons(catalogueList.length > posterArray.getCol() * posterArray.getRow() 
					? buttonList3_page : buttonList3, getCurrentCatalogue());
			break;
		case MODE_MY_CHANNEL:
			setButtons(catalogueList.length > posterArray.getCol() * posterArray.getRow()
					? buttonList4_page : buttonList4, getCurrentCatalogue());
			break;
		}
	}

	public void flushData() {
		// for (int i = 0; catalogueList != null && i < catalogueList.length;
		// i++) {
		// catalogueList[i] = null;
		// }
		// catalogueList = null;
	}
	
	public boolean canPlayAll() {
		// purchased bundle can play all...
		if (getCurrentCatalogue() instanceof Bundle) {
			return BookmarkManager.getInstance().isPurchased((Bundle) getCurrentCatalogue());
		}
		
		if (catalogueList == null || catalogueList.length < 2) {
			return false;
		}
		
		// all free or all purchased
		boolean canPlayAll = true;
		for (int i = 0; catalogueList != null && i < catalogueList.length; i++) {
			if (catalogueList[i] == null) {
				continue;
			}
			if (catalogueList[i] instanceof Video) {
				if (Log.EXTRA_ON) {
					Log.printDebug("catalogueList[i]=" + catalogueList[i].toString(true));
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).getTypeID());
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).isFree());
					Log.printDebug("catalogueList[i] isPurchased=" + BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]));
				}
				
				// VDTRMASTER-5432
				if (catalogueList[i] instanceof Episode && (((Video) catalogueList[i]).isFree() || BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]) != null)) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_124")) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_104")) {
					continue;
				} else {
					canPlayAll = false;
					break;
				}
			}
			// mixed category, can't play all
			canPlayAll = false;
			break;
		}
		return canPlayAll;
	}
	
	public BaseElement[] getData() {
		return catalogueList;
	}
	
	public boolean focusOnFirst() {
		return posterArray.getIndex() == 0;
	}
	
	public boolean focusOnLast() {
		return posterArray.getIndex() == catalogueList.length - 1;
	}

	public int getMode() {
		return mode;
	}

	public void run() {
		for (int i = 0; catalogueList != null && i < catalogueList.length; i++) {
			Channel ch = ((CategoryContainer) catalogueList[i]).getTopic().channel;
			if (ch != null) {
				ch.loadLogo(true, false);
				continue;
			}
			Network nw = ((CategoryContainer) catalogueList[i]).getTopic().network;
			if (nw != null) {
				nw.loadLogo(true, false);
			}
		}
	}

	public void setData(BaseElement[] catalogues, BaseElement focused) {
		// int m = (int) (Math.random() * 60) + 10;
		// BaseElement[] t = new BaseElement[m * catalogues.length];
		// for (int i = 0; i < m; i++) {
		// for (int j = 0; j < catalogues.length; j++) {
		// t[i*catalogues.length+j] = catalogues[j];
		// }
		// }
		// catalogueList = t;
		catalogueList = catalogues;

		if (allChannel != null || (catalogueList.length > 0
				&& catalogueList[0] instanceof CategoryContainer
				&& ((CategoryContainer) catalogueList[0]).type == MenuController.MENU_CHANNEL)) {
			posterArray.setBounds(0, 80-10, 960, 413);
			posterArray.setRowCol(3, 5);
			posterArray.setColWidth(165);
			posterArray.setRowHeight(124);
			if (((CategoryContainer) catalogueList[0]).type == MenuController.MENU_CHANNEL) {
				posterArray.setRowHeight(113);
				posterArray.setChannelMode(true);
			}
			posterArray.setOverflow(true);

			new Thread(this, "loadLogo_wall").start();
		} else {
			posterArray.setBounds(0, 80, 960, 403);
			posterArray.setRowCol(2, 5);
			posterArray.setColWidth(165);
//			posterArray.setRowCol(2, 7);
//			posterArray.setColWidth(115);
			posterArray.setRowHeight(173);
			
			mode = MODE_NORMAL;
		}
		posterArray.setData(catalogueList);
		//posterArray.resetIndex();
		posterArray.setFocused(focused);
		if (catalogueList.length > 0) {
			curCatal = catalogueList[posterArray.getIndex()];
		} else {
			curCatal = null;
		}

		repaint();

	}
	
	public void searchLetter(Object letter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("searchLetter, letter = " + letter);
		}
		if (catalogueList == null || catalogueList.length == 0) {
			return;
		}
		MoreDetail candidate = (MoreDetail) catalogueList[0];
		Collator c = Collator.getInstance(Locale.CANADA_FRENCH);
		for (int i = 1; i < catalogueList.length; i++) {
			if (c.compare(candidate.getTitle(), letter) > 0) {
				break;
			}
			candidate = (MoreDetail) catalogueList[i];
		}
		scroll = 0;
		posterArray.setFocused(candidate);
		if (catalogueList.length > 0) {
			curCatal = catalogueList[posterArray.getIndex()];
		} else {
			curCatal = null;
		}
		repaint();
	}

	int[] lastRequested;
	public void timerWentOff(TVTimerWentOffEvent arg0) {
		BaseElement[] painted = posterArray.getPainted();
		if (painted.length == 0) {
			return;
		}
		boolean dup = true;
		if (lastRequested == null || lastRequested.length != painted.length) {
			lastRequested = new int[painted.length];
			dup = false;
		} else {
			for (int i = 0; i < lastRequested.length; i++) {
				if (painted[i] != null && lastRequested[i] != painted[i].hashCode()) {
					dup = false;
					break;
				}
			}
		}
		if (dup) {
			return;
		}
		for (int i = 0; i < lastRequested.length; i++) {
			if (painted[i] != null) {
				lastRequested[i] = painted[i].hashCode();
			}
		}
		CatalogueDatabase.getInstance().retrieveImages(this, painted, ImageRetriever.TYPE_DOWNLOAD_MORE | ImageRetriever.TYPE_NO_DELAY | ImageRetriever.TYPE_SMALL, CatalogueDatabase.IMAGE_RETRIEVER_SM);
	}
	
	public void imageDownloadMore() {
		BaseElement[] painted = posterArray.getNextPainted(30);
		if (painted.length == 0) {
			return;
		}
		CatalogueDatabase.getInstance().retrieveImages(this, painted, ImageRetriever.TYPE_SMALL, CatalogueDatabase.IMAGE_RETRIEVER_SM);
	}

	public boolean handleKey(int key) {
		Log.printDebug("WallofPosters, handleKey()" + ", code = "
				+ key + ", popup = " + getPopUp());
		switch (key) {
		case KeyCodes.COLOR_A:
			keyYellow();
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		case HRcEvent.VK_LEFT:
			posterArray.keyLeft();
			scroll = 0;
			curCatal = catalogueList[posterArray.getIndex()];
			updateButtons();
			break;
		case HRcEvent.VK_RIGHT:
			posterArray.keyRight();
			scroll = 0;
			curCatal = catalogueList[posterArray.getIndex()];
			updateButtons();
			break;
		case HRcEvent.VK_DOWN:
			posterArray.keyDown();
			scroll = 0;
			curCatal = catalogueList[posterArray.getIndex()];
			updateButtons();
			break;
		case HRcEvent.VK_UP:
			posterArray.keyUp();
			scroll = 0;
			curCatal = catalogueList[posterArray.getIndex()];
			updateButtons();
			break;
		case HRcEvent.VK_INFO:
		case HRcEvent.VK_ENTER:
			posterArray.keyOK();
			break;
		case OCRcEvent.VK_PAGE_UP:
			if (isExistPageVertical()) {
				footer.clickAnimation(Resources.TEXT_CHANGE_PAGE);
				posterArray.keyPrev();
				scroll = 0;
				curCatal = catalogueList[posterArray.getIndex()];
				updateButtons();
			}
			break;
		case OCRcEvent.VK_PAGE_DOWN:
			if (isExistPageVertical()) {
				footer.clickAnimation(Resources.TEXT_CHANGE_PAGE);
				posterArray.keyNext();
				scroll = 0;
				curCatal = catalogueList[posterArray.getIndex()];
				updateButtons();
			}
			break;
		}
		repaint();
		return true;
	}

	public void keyGreen() {
		Log.printDebug("WallofPoster, keyGreen()");
		int opt = CatalogueDatabase.getInstance().getLastSortTypeIdx();
//		if (catalogueList == null || catalogueList.length == 0
//				|| catalogueList[0] instanceof Video == false) {
//			opt = 1;
//		} else {
//			opt = CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//					Resources.OPT_NAME) ? 2 : 1;
//		}
		setPopUp(optionBox);
		if (catalogueList.length > 0
				&& catalogueList[0] instanceof CategoryContainer
				&& ((CategoryContainer) catalogueList[0]).type == MenuController.MENU_CHANNEL) {
			optionBox.start(menu.getOptionMenu(0, getCurrentCatalogue()), menu);
		} else {
			optionBox.start(menu.getOptionMenu(opt, getCurrentCatalogue()), menu);
		}

		repaint();
	}

	public void keyBack() {
		menu.backwardHistory(false);
		menu.back();
		// 20190222 Touche project
		VbmController.getInstance().backwardHistory();
	}

	public void keyYellow() {
		switch (mode) {
		case MODE_NORMAL:
            // R7.3
            if (posterArray.getLastCategoryContainer() != null) {
                CatalogueDatabase.getInstance().setLastRequestedCategoryContainerId(posterArray.getLastCategoryContainer().id);
            }
			// VDTRMASTER-5690
			DataCenter.getInstance().put("VIEW_PARAM", param);
			menu.viewChange(catalogueList, posterArray.getFocused());
			// VDTRMASTER-5690
			DataCenter.getInstance().remove("VIEW_PARAM");
			break;
		case MODE_ALL_CHANNEL:
			allChannel = catalogueList;
			ArrayList list = new ArrayList();
			Channel ch;
			Network nw;
			for (int i = 0; i < catalogueList.length; i++) {
				nw = ((CategoryContainer) catalogueList[i]).getTopic().network;
				if (nw != null) {
					for (int j = 0; j < nw.channel.length; j++) {
						if (nw.channel[j].hasAuth == false) {
							nw.channel[j].hasAuth = CommunicationManager.getInstance().checkAuth(nw.channel[j].callLetters);
						}
						if (nw.channel[j].hasAuth) {
							list.add(catalogueList[i]);
							menu.debugList[0].add("Item["+i+"/"+catalogueList.length+"] = "
									+ nw.toString(true) + ", "
									+ nw.channel[j].toString(true)
									+ " is subscribed.");
							break;
						}
					}
				}
				ch = ((CategoryContainer) catalogueList[i]).getTopic().channel;
				if (ch == null) {
					continue;
				}
				if (ch.hasAuth == false) {
					ch.hasAuth = CommunicationManager.getInstance().checkAuth(ch.callLetters);
				}
				if (ch.hasAuth) {
					list.add(catalogueList[i]);
					menu.debugList[0].add("Item["+i+"/"+catalogueList.length+"] = "
							+ ch.toString(true) + " is subscribed.");
				}
			}
			catalogueList = new BaseElement[list.size()];
			list.toArray(catalogueList);
			setFocus(0);
			posterArray.resetIndex();
			setData(catalogueList, null);
			mode = MODE_MY_CHANNEL;
			updateButtons();
			break;
		case MODE_MY_CHANNEL:
			setFocus(0);
			posterArray.resetIndex();
			setData(allChannel, null);
			mode = MODE_ALL_CHANNEL;
			updateButtons();
			break;
		}
	}

	public BaseElement getCurrentCatalogue() {
		return curCatal;
	}

	public int getSortStatus() {
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_NAME)) {
//			return 1;
//		}
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_DATE)) {
//			return 2;
//		}
//		return 0;
		if (catalogueList.length > 0
				&& catalogueList[0] instanceof CategoryContainer
				&& ((CategoryContainer) catalogueList[0]).type == MenuController.MENU_CHANNEL) {
			return 0;
		}
		return CatalogueDatabase.getInstance().getLastSortTypeIdx();
	}

	public void reflectVCDSResult(Object cmd) {
		menu.hideLoadingAnimation();
		if (CatalogueDatabase.SORT.equals(cmd)) {
			// VDTRMASTER-5653 & VDTRMASTER-5649
			if (menu.getLastCategoryId().startsWith("TO_")) {
				Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(menu.getLastCategoryId());
				setData(topic.treeRoot.totalContents, null);
			} else {
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				setData(cat.totalContents, null);
			}
			setFocus(0);
			posterArray.resetIndex();
			scroll = 0;
		}
	}
	
	public void selected(MenuItem item) {
//		BaseElement focused = posterArray.getFocused();
//		if (Resources.OPT_NAME.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_NAME);
//			setData(catalogueList, focused);
//			menu.setSortNormal(Resources.OPT_NAME);
//		} else if (Resources.OPT_DATE.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_DATE);
//			setData(catalogueList, focused);
//			menu.setSortNormal(Resources.OPT_DATE);
//		}
//		repaint();
	}
	
	public void receiveCheckRightFilter(CheckResult checkResult) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CategoryUI, receiveCheckRightFilter()"
					+ ", checkResult = " + checkResult + ", filter = "
					+ checkResult.getFilter() + ", response = "
					+ checkResult.getResponse());
		}
		if (PreferenceService.RESPONSE_SUCCESS != checkResult.getResponse()) {
			return;
		}
		if (RightFilter.BLOCK_BY_RATINGS.equals(checkResult.getFilter())) {
			handleKey(KeyEvent.VK_ENTER);
		}
	}
}

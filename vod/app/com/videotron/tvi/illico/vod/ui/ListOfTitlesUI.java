package com.videotron.tvi.illico.vod.ui;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import javax.tv.util.TVTimerWentOffEvent;

import com.videotron.tvi.illico.vod.comp.PopChangeView;
import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.ActionBox;
import com.videotron.tvi.illico.vod.comp.AvailableDateView;
import com.videotron.tvi.illico.vod.comp.PosterArray;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.AvailableSource;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Section;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.gui.ListOfTitlesRenderer;

public class ListOfTitlesUI extends BaseUI {

	private static final long serialVersionUID = 4156056753952944855L;

	public static final int FOCUS_POSTERS = 0;
	public static final int FOCUS_INFO = 1;

	private ListOfTitlesRenderer titleListRenderer = new ListOfTitlesRenderer();
	private BaseElement[] catalogueList;
	private String subTitle = "";

	private int titleIndex;
	
	// VDTRMASTER-5690
	private Object param;

	private PosterArray posterArray;
	private boolean canSort;
	private int elementLen;

	private ScrollTexts scrollText = new ScrollTexts();
	static private Rectangle recScrollText = new Rectangle(237, 419, 357, 57);
	static private Rectangle recScrollText2 = new Rectangle(60, 388, 532, 88);
	static private Rectangle recScrollText3 = new Rectangle(237, 388, 357, 88);
	static private Rectangle recScrollText4 = new Rectangle(60, 405, 357, 71);
	static private int infoPageRowHeight = 17;
	static private Color cScrollTextColor = new DVBColor(255, 255, 255, 230);
	static private int[] buttonList = new int[] { BaseUI.BTN_SCROLL, BaseUI.BTN_VIEW,
			BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonListWithoutScroll =
		new int[] { BaseUI.BTN_VIEW, BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION  };
	static private int[] buttonListWithoutChangeView = new int[] { BaseUI.BTN_SCROLL, BaseUI.BTN_PROFILE,
		BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };
	static private int[] buttonListWithoutChangeViewNSCROLL =
		new int[] { BaseUI.BTN_PROFILE,BaseUI.BTN_WISHLIST, BaseUI.BTN_OPTION };

	private ActionBox actionBox;
	private OptionScreen optionBox = new OptionScreen();
//	private ImageLabel leftShadow;
//	private ImageLabel rightShadow;
	
	private boolean isFromAvailableSource;
	private BaseElement lastObject;
	private Object cached;
	private boolean useBrandingDeco;
	private boolean clearBrandingBg;
	
	public ListOfTitlesUI() {
		scrollText.setBottomMaskImage(dataCenter.getImage("01_des_sh_bun.png"));
		add(scrollText);

		actionBox = ActionBox.newInstance();
		add(actionBox);

		posterArray = PosterArray.newInstance();
		posterArray.setBounds(0, 100, 960, 280);
		posterArray.setFont(FontResource.BLENDER.getFont(23));
		posterArray.prepare();
		add(posterArray, 0);

		Image btnPage = (Image) ((Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_PAGE);
		footer.linkWithScrollTexts(btnPage, scrollText);
		add(footer);

//		leftShadow = new ImageLabel(
//				dataCenter.getImage("01_poster_sha_l.png"));
//		leftShadow.setLocation(0, 103);
//		add(leftShadow, 0);

//		rightShadow = new ImageLabel(
//				dataCenter.getImage("01_poster_sha_r.png"));
//		rightShadow.setLocation(856, 103);
//		add(rightShadow, 0);

		hotkeyBG = new ImageLabel(dataCenter.getImage("01_hotkeybg.png"));
		hotkeyBG.setLocation(236, 466);
		add(hotkeyBG, 0);
	}

	public void start(boolean back) {
		Log.printInfo("ListOfTitlesUI, start(), back = " + back);
		menu = MenuController.getInstance();
		isFromAvailableSource = false;
		boolean useNewBranding = false;
		
		if (!back) {
			clearBrandingBg = false;
			useBrandingDeco = true;
			Object[] param = MenuController.peekStack();
			Object obj = param[MenuController.PARAM_DATA_IDX];
			this.param = obj;
			Log.printDebug("ListOfTitlesUI, obj = " + obj);

			setFocus(0);
			titleIndex = 0;
			setRenderer(titleListRenderer);
			prepare();
			setState(BaseUI.STATE_DEACTIVATED);

			scrollText.setFont(FontResource.DINMED.getFont(16));
			scrollText.setForeground(cScrollTextColor);
			scrollText.setRowHeight(infoPageRowHeight);

			BaseElement[] data = null;
			canSort = true;
			// R5
			fromCategory = false;
			if (obj instanceof String) {
				cached = CatalogueDatabase.getInstance().getCached(obj);
				Log.printDebug("ListOfTitlesUI, cached = " + cached);
				if (cached instanceof Season) {
					Season season = (Season) cached;
					subTitle = dataCenter.getString(Resources.TEXT_MSG_CHOOSE2);
					data = season.episode;
					
					lastObject = menu.getLastObject();
					
					for (int i = 0; season.availableSources != null && i < season.availableSources.availableSource.length; i++) {
						if (season.availableSources.availableSource[i].equals(lastObject)) {
							isFromAvailableSource = true;
							break;
						}
					}
					
					if (Log.EXTRA_ON) {
						Log.printDebug("Season - filterElement = " + CategoryUI.filterElement);
						Log.printDebug("Season - lastObject = " + menu.getLastObject());
						Log.printDebug("Season - isFromAvailableSource = " + isFromAvailableSource);
					}
					
					if (isFromAvailableSource) {
						AvailableSource availableSource = (AvailableSource) lastObject;
						data = new BaseElement[availableSource.episodeIds.length];
						for (int i = 0; i < availableSource.episodeIds.length; i++) {
							data[i] = (BaseElement) CatalogueDatabase.getInstance().getCached(availableSource.episodeIds[i]);
						}
						
						BaseElement filter = CategoryUI.filterElement;
						
						if (filter != null) {
							if (!filter.equals(availableSource.channel) 
									&& !filter.equals(availableSource.service) 
									&& !filter.equals(availableSource.network) 
									&& !(availableSource.network != null && availableSource.network.has(filter))) {
								clearBrandingBg = true;
							}
						}
					}
					elementLen = data.length;
					setPosterMode(true);
					setButtons(buttonListWithoutChangeView, getCurrentCatalogue());
					canSort = false;
					
					useNewBranding = findBrandingElement(data);
				} else if (cached instanceof Series) {
					subTitle = dataCenter.getString(Resources.TEXT_MSG_CHOOSE1);
					data = ((Series) cached).season;
					elementLen = data.length;
					// may need filtering
					ArrayList filtered = new ArrayList();
					BaseElement filter = CategoryUI.filterElement;
					
					if (Log.EXTRA_ON) {
						Log.printDebug("Series season - filterElement = " + filter);
						Log.printDebug("Series season - lastObject = " + menu.getLastObject());
					}
					
					if (Season.MORE_CONTENT.equals(menu.getLastObject())) {
						filter = null;
						clearBrandingBg = true;
					}
					
					if (Log.EXTRA_ON) {
						Log.printDebug("After check with Season.MORE_CONTENT, Series season - filterElement = " + filter);
					}
					
					boolean haveMoreContent = false;
					for (int i = 0; i < data.length; i++) {
						Season season = (Season) data[i];
						for (int j = 0; season.availableSources != null && j < season.availableSources.availableSource.length; j++) {
							AvailableSource avail = season.availableSources.availableSource[j];
							if (filter == null) { // general seasons
								filtered.add(avail);
							} else if (filter.equals(avail.channel) 
									|| filter.equals(avail.service) 
									|| filter.equals(avail.network) 
									|| (avail.network != null && avail.network.has(filter))) {
								filtered.add(avail);
							} else {
								haveMoreContent = true;
							}
						}
//						for (int j = 0; j < season.sections.length; j++) {
//							filtered.add(season.sections[j]); // section also displayed
//						}
					}
					if (haveMoreContent) {
						filtered.add(Season.MORE_CONTENT);
						useBrandingDeco = false;
					}
					
					if (filter != null && !haveMoreContent) {
						useBrandingDeco = false;
					}
					
					elementLen = filtered.size();
					
					data = new BaseElement[elementLen];
					for (int i = 0; i < elementLen; i++) {
						data[i] = (BaseElement) filtered.get(i);
					}
					setPosterMode(true);
					setButtons(buttonListWithoutChangeView, getCurrentCatalogue());
					canSort = false;
					final BaseElement[] seasonsToLoadLogo = ((Series) cached).season;
					new Thread("Load logo for overlay") {
						public void run() {
							if (Log.DEBUG_ON) {
								Log.printDebug("seasonToLoadLogo length = " + seasonsToLoadLogo.length);
							}
							for (int i = 0; i < seasonsToLoadLogo.length; i++) {
								((Season) seasonsToLoadLogo[i]).loadLogo();
							}
						}
					}.start();
				} else if (cached instanceof CategoryContainer) {
					CategoryContainer cat = (CategoryContainer) cached;
					menu.setLastCategoryId(cat.id);
					data = cat.totalContents;
					elementLen = data.length;
					checkData(data, cat);
					subTitle = "";
					// R5
					fromCategory = true;
				} else if (cached instanceof Extra) {
					data = ((Extra) cached).extraDetail.video;
					elementLen = data.length;
					setPosterMode(true);
					posterArray.setRowCol(1, 7);
					subTitle = "";
				}

			} else {
				data = (BaseElement[]) param[MenuController.PARAM_DATA_IDX];
				elementLen = data.length;
				checkData(data, null);
				useNewBranding = findBrandingElement(data);
				
				 // VDTRMASTER-5573
				BaseElement filter = CategoryUI.filterElement;
				if (filter != null && data[0] instanceof Bundle) {
					Bundle bundle = (Bundle) data[0];
					for (int i = 0;i < bundle.service.length; i++) {
						if (!filter.equals(bundle.service[0])) {
							clearBrandingBg = true;
							break;
						}
					}
				}
				
				// VDTRMASTER-5623
				if (CategoryUI.isBranded() && useNewBranding == false && menu.isUnderWishList()) {
					clearBrandingBg = true;
				}
				
				// VDTRMASTER-5690
				this.param = DataCenter.getInstance().get("VIEW_PARAM");
			}
			for (int i = 0; i < data.length; i++) {
				Log.printDebug("ListOfTitlesUI, data[" + i + "] = " + (data[i] == null ? "NULL" : data[i].toString(true)));
			}
			if (canSort) {
				String opt = menu.getSortNormal();
				data = CatalogueDatabase.getInstance().sort(data, opt == null ? Resources.OPT_NAME : opt);
			}
			posterArray.setEpisodeMode(!canSort);
			setData(data);
			if (param.length > MenuController.PARAM_DATA_IDX+1) {
				posterArray.setFocused((BaseElement) param[MenuController.PARAM_DATA_IDX+1]);
				
				// VDTRMASTER-5373
				if (menu.isMoveToNext()) {
					menu.setMoveToNext(false);
					posterArray.keyRight();
				}
				
				titleIndex = posterArray.getIndex();
				setTitleDetail();
			}
		} else { // back

			// VDTRMASTER-5835
			// VDTRMASTER-5883 - add to check a canSort for season and episode
			if (MenuController.getInstance().getChageViewType() != PopChangeView.VIEW_NORMAL && canSort) {
				keyYellow();
				return;
			}

			BaseElement next = menu.getNext();
			if (next != null) {
				posterArray.setFocused(next);
				titleIndex = posterArray.getIndex();
				setTitleDetail();
			}
			
			if (Log.DEBUG_ON) {
				Log.printDebug("param=" + param);
			}
			// VDTRMASTER-5433
			if (param instanceof String) {
				cached = CatalogueDatabase.getInstance().getCached(param);
				Log.printDebug("ListOfTitlesUI, cached = " + cached);
				if (cached instanceof CategoryContainer) {
					
					boolean isApplied = false;
					CategoryContainer category = (CategoryContainer) cached;
					do {
						isApplied = CategoryUI.setBrandingElementByCategory(category);
						category = category.parent;
					} while (category != null && isApplied == false);
				}
			}
			
			useNewBranding = findBrandingElement(catalogueList);
		}
		
		BaseRenderer.setBrandingBg(0, null);
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ListOfTitlesUI, clearBrandingBg=" + clearBrandingBg + ", useNewBranding=" + useNewBranding
					+ ", CategoryUI.isBranded()=" + CategoryUI.isBranded());
		}
		//R7.4 freelife VDTRMASTER-6173, 6172
		if (useNewBranding)
			clearBrandingBg = false;
		
		ImageGroup bg = null;
		
		if (clearBrandingBg) {
			BaseRenderer.setBrandingBg(-1, null);
		} else {
			if (CategoryUI.isBranded() && !useNewBranding) {
				BrandingElement br = CategoryUI.getBrandingElement();
				bg = br.getBackground(true);
				try {
					BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
					if (menu.isGoToAsset()) { // 4761
						throw new Exception();
					}
//					leftShadow.setVisible(false);
//					rightShadow.setVisible(false);
				} catch (Exception e) {
					Log.printDebug("ListOfTitlesUI setBrandingBg exception ");
					//e.printStackTrace();
					BaseRenderer.setBrandingBg(-1, null);
//					leftShadow.setVisible(true);
//					rightShadow.setVisible(true);
				}
			} else if (isFromAvailableSource) { // VDTRMASTER-5136
				BaseElement lastObject = menu.getLastObject();
				AvailableSource available = (AvailableSource) lastObject;
				
				BrandingElement br = null;
				
				if (available.channel != null) {
					br = available.channel;
				} else if (available.network != null) {
					br = available.network;
				} else if (available.service != null && available.service.description != null) {
					br = available.service.description;
				}
				
				Log.printDebug("isFromAvailableSource = " + isFromAvailableSource + ", br = " + br);
				
				setBrandingElement(br);
			}
		}
		
		updateButtons();
		super.start();
		
		setupTimer(true, 0);
		if (Resources.OPT_NAME.equals(menu.getSortNormal())) {
			menu.setNeedRapid(true);
		}
	}
	
	private boolean findBrandingElement(BaseElement[] data) {
		Log.printDebug("ListOfTitlesUI findBrandingElement " );
		
		BaseUI parent = MenuController.getInstance().getParentUI();
		// VDTRMASTER-5590
		BrandingElement br = null;
		if (data[0] instanceof Video) {
			Video v = (Video) data[0];
			
			if (v.channel != null) {
				if (v.channel.network != null) {
					br = v.channel.network;
				} else {
					br = v.channel;
				}
			} else if (v.service != null && v.service[0] != null) {
				br = v.service[0].description;
			}
			
		} else if (data[0] instanceof Episode) {
			Episode ep = ((Episode)data[0]);
			
			if (ep.channel != null) {
				if (ep.channel.network != null) {
					br = ep.channel.network;
				} else {
					br = ep.channel;
				}
			} else if (ep.service != null && ep.service[0] != null && ep.service[0].description != null) {
				br = ep.service[0].description;
			}
		}
		Log.printDebug("ListOfTitlesUI, came from WishList or ResumeViewing and isFromAvailableSourc=" + isFromAvailableSource);
		Log.printDebug("ListOfTitlesUI, came from WishList or ResumeViewing and br=" + br);
		if (br != null) {
			//R7.4 freelife VDTRMASTER-6173, 6172
			return setBrandingElement(br);			
		}
		return false;
	}
	
	//R7.4 freelife VDTRMASTER-6173, 6172
	private boolean setBrandingElement(BrandingElement br) {
		if (br != null) {
			ImageGroup bg = br.getBackground(true);
			try {
				BaseRenderer.setBrandingBg(0, dataCenter.getString("IMAGE_URL") + ImageRetriever.BASE + bg.images[0].imageName + "." + bg.encoding);
				if (menu.isGoToAsset()) { // 4761
					throw new Exception();
				}
//				leftShadow.setVisible(false);
//				rightShadow.setVisible(false);
			} catch (Exception e) {
				if (Log.DEBUG_ON) {
					Log.printDebug("setBrandingElement error");
					//e.printStackTrace();
				}
				BaseRenderer.setBrandingBg(-1, null);
				return false;
//				leftShadow.setVisible(true);
//				rightShadow.setVisible(true);
			}
			
			return true;
		}
		return false;
	}
	
	public boolean useBradingDeco() {
		return useBrandingDeco;
	}
	
	public boolean clearBrandingBg() {
		return clearBrandingBg;
	}
	
	public boolean isFromAvailableSource() {
		return isFromAvailableSource;
	}
	
	public BaseElement getLastObject() {
		return lastObject;
	}
	
	public boolean focusOnFirst() {
		return titleIndex == 0;
	}
	
	public boolean focusOnLast() {
		return titleIndex == catalogueList.length - 1;
	}
	
	public void searchLetter(Object letter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("searchLetter, letter = " + letter);
		}
		if (catalogueList == null || catalogueList.length == 0) {
			return;
		}
		MoreDetail candidate = (MoreDetail) catalogueList[0];
		Collator c = Collator.getInstance(Locale.CANADA_FRENCH);
		for (int i = 1; i < catalogueList.length; i++) {
			if (c.compare(candidate.getTitle(), letter) > 0) {
				break;
			}
			candidate = (MoreDetail) catalogueList[i];
		}
		scroll = 0;
		posterArray.setFocused(candidate);
		titleIndex = posterArray.getIndex();
		setTitleDetail();
	}

	private void checkData(BaseElement[] data, CategoryContainer cat) {
		boolean allBundle = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] == null) continue;
			if (data[i] instanceof Bundle == false || ((Bundle) data[i]).isMovieBundle()) {
				allBundle = false;
				break;
			}
		}
		boolean allEpisode = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] == null) continue;
			if (data[i] instanceof Episode == false) {
				allEpisode = false;
				break;
			}
		}
		boolean allSeason = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] == null) continue;
			if (data[i] instanceof Season == false) {
				allSeason = false;
				break;
			}
		}
		boolean allSeries = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] == null) continue;
			if (data[i] instanceof Series == false) {
				allSeries = false;
				break;
			}
		}
		boolean allSection = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] == null) continue;
			if (data[i] instanceof Section == false) {
				allSection = false;
				break;
			}
		}
		
		Log.printDebug("checkData, allB = " + allBundle + 
				", allE = " + allEpisode + 
				", allSs = " + allSeason + 
				", allSr = " + allSeries +
				", allSc = " + allSection);
		if (allBundle || allEpisode || allSeason || allSection) {
			setPosterMode(true);
			canSort = false;
			if (allBundle) {
				subTitle = dataCenter.getString(Resources.TEXT_MSG_CHOOSE3);
			} else if (allEpisode) {
				subTitle = dataCenter.getString(Resources.TEXT_MSG_CHOOSE2);
			} else if (allSeason) {
				subTitle = dataCenter.getString(Resources.TEXT_MSG_CHOOSE1);
			} else if (allSection) {
				subTitle = dataCenter.getString(Resources.TEXT_MSG_CHOOSE2);
			}
		} else {
			setPosterMode(false);
			subTitle = "";
		}
	}

	private void setPosterMode(boolean episode) {
		if (episode) {
			posterArray.setFocusMode(true);
			posterArray.setDataLoop(false);
			posterArray.setRowCol(1, 5);
			posterArray.setOverflow(true);
			setButtons(buttonListWithoutChangeView, getCurrentCatalogue());
//			lastButtons = buttonListWithoutChangeView;
		} else {
			posterArray.setFocusMode(false);
			posterArray.setDataLoop(true);
			posterArray.setRowCol(1, 7);
			posterArray.setOverflow(false);
			setButtons(buttonList, getCurrentCatalogue());
//			lastButtons = buttonList;
		}
		
//		setButtons(elementLen > 9 
//				? buttonList : buttonList2);
	}
	
	public int getActionButtonCount() {
		return actionBox.getCount();
	}

	public String getSubTitle() {
		return subTitle;
	}
	
	public void flushData() {
		// for (int i = 0; catalogueList != null && i < catalogueList.length;
		// i++) {
		// catalogueList[i] = null;
		// }
		// catalogueList = null;
//		brandBG.flush();
	}
	
	public BaseElement[] getData() {
		return catalogueList;
	}
	
	public Object getCachedData() {
		return cached;
	}
	
	public boolean isEpisodes() {
		return param instanceof String && ((String) param).startsWith("SS");
	}
	
	public String findCurrentLabel() {
		return findPlayLabel(getCurrentCatalogue());
	}
	
	public String findLabel() {
		if (getCurrentCatalogue() instanceof Bundle) {
			return findLabel(((Bundle) getCurrentCatalogue()).getTitles());
		}
		return findLabel(catalogueList);
	}
	
	public boolean canPlayAll() {
		// purchased bundle can play all...
		if (getCurrentCatalogue() instanceof Bundle) {
			return BookmarkManager.getInstance().isPurchased((Bundle) getCurrentCatalogue());
		}
		
		if (catalogueList == null || catalogueList.length < 2) {
			return false;
		}
		
		// all free or all purchased
		boolean canPlayAll = true;
		for (int i = 0; catalogueList != null && i < catalogueList.length; i++) {
			if (catalogueList[i] == null) {
				continue;
			}
			if (catalogueList[i] instanceof Video) {
//				if (((Video) catalogueList[i]).isFree() == false && // not a free 
//						BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]) == null) { // not purchased
//					canPlayAll = false;
//					break;
//				}
//				continue;
				
				if (Log.EXTRA_ON) {
					Log.printDebug("catalogueList[i]=" + catalogueList[i].toString(true));
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).getTypeID());
					Log.printDebug("catalogueList[i].typeId=" + ((Video) catalogueList[i]).isFree());
					Log.printDebug("catalogueList[i] isPurchased=" + BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]));
				}
				
				if (catalogueList[i] instanceof Episode && (((Video) catalogueList[i]).isFree() || BookmarkManager.getInstance().isPurchased((Video)catalogueList[i]) != null)) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_124")) {
					continue;
				} else if (((Video) catalogueList[i]).getTypeID().equals("VT_104")) {
					continue;
				} else {
					canPlayAll = false;
					break;
				}
			}
//			// mixed category, can't play all
			canPlayAll = false;
			break;
		}
		return canPlayAll;
	}

	public void setData(BaseElement[] catalogues) {
//		int m = (int) (Math.random() * 60) + 10;
//		BaseElement[] t = new BaseElement[m * catalogues.length];
//		for (int i = 0; i < m; i++) {
//			for (int j = 0; j < catalogues.length; j++) {
//				t[i*catalogues.length+j] = catalogues[j];
//			}
//		}
//		catalogueList = t;
		this.catalogueList = catalogues;
		setTitleDetail();
		repaint();

		posterArray.setData(catalogueList);

		//CatalogueDatabase.getInstance().retrieveImages(this, catalogueList, ImageRetriever.TYPE_SMALL);
//		timerWentOff(null);
//		setupTimer(true, 0);
	}

	int[] lastRequested;
	public void timerWentOff(TVTimerWentOffEvent arg0) {
		BaseElement[] painted = posterArray.getPainted();
		if (painted.length == 0) {
			return;
		}
		boolean dup = true;
		if (lastRequested == null || lastRequested.length != painted.length) {
			lastRequested = new int[painted.length];
			dup = false;
		} else {
			for (int i = 0; i < lastRequested.length; i++) {
				if (painted[i] != null && lastRequested[i] != painted[i].hashCode()) {
					dup = false;
					break;
				}
			}
		}
		if (dup) {
			return;
		}
		for (int i = 0; i < lastRequested.length; i++) {
			if (painted[i] != null) {
				lastRequested[i] = painted[i].hashCode();
			}
		}
		CatalogueDatabase.getInstance().retrieveImages(this, painted, ImageRetriever.TYPE_DOWNLOAD_MORE | ImageRetriever.TYPE_NO_DELAY | ImageRetriever.TYPE_SMALL, CatalogueDatabase.IMAGE_RETRIEVER_SM);
	}
	
	public void imageDownloadMore() {
		BaseElement[] painted = posterArray.getNextPainted(20);
		if (painted.length == 0) {
			return;
		}
		CatalogueDatabase.getInstance().retrieveImages(this, painted, ImageRetriever.TYPE_SMALL, CatalogueDatabase.IMAGE_RETRIEVER_SM);
	}

	private void setActionButtons() {
		if (catalogueList.length <= titleIndex) {
			return;
		}
		String[] actionButtons = getActionButton(catalogueList[titleIndex]);
		actionBox.setActionBox(actionButtons);
		actionBox.setFocus(focus);
		actionBox.repaint();
	}

	private void setTitleDetail() {
		if (catalogueList.length <= titleIndex) {
			return;
		}
		scroll = 0;
		MoreDetail title = (MoreDetail) catalogueList[titleIndex];
		if (title == null
				|| (catalogueList[titleIndex] instanceof Video && catalogueList[titleIndex] instanceof Episode == false)
				|| catalogueList[titleIndex] instanceof Extra) {
			scrollText.setContents("");
			scrollText.setButtonIcon(false);
			setActionButtons();
			setButtons(getButtons(), getCurrentCatalogue());
			
			//VDTRMASTER-5564
			// R5
			if (getCurrentCatalogue() instanceof Video || getCurrentCatalogue() instanceof Bundle) {
				if (actionBox.getActionBox().length > 2) {
					availableView.setBounds(626, 328, 230, 24);
				} else {
					availableView.setBounds(626, 362, 230, 24);
				}
				
				add(availableView);
				// VDTRMASTER-5488
				availableView.setData((MoreDetail)getCurrentCatalogue(), this, false, true);
				availableView.startAnimation();
			} else {
				remove(availableView);
			}
			// VDTRMASTER-5693
			repaint();
			// VDTRMASTER-5668
			CatalogueDatabase.getInstance().retrieveImages(this,
					new BaseElement[] {
						catalogueList[titleIndex],
						catalogueList[(titleIndex + 1) % catalogueList.length],
						catalogueList[(titleIndex - 1 + catalogueList.length) % catalogueList.length]},
					ImageRetriever.TYPE_MEDIUM,
					CatalogueDatabase.IMAGE_RETRIEVER_MD);
			return;
		}
		
		String titleDetail = title.getDescription();
		if (catalogueList[titleIndex] instanceof Episode
				|| catalogueList[titleIndex] instanceof Bundle) {
			scrollText.setBounds(recScrollText3);
			scrollText.setTopMaskImage(dataCenter.getImage("01_des_sh_t03.png"));
			scrollText.setTopMaskImagePosition(32-recScrollText3.x, 384-recScrollText3.y);
			scrollText.setBottomMaskImagePosition(32-recScrollText3.x, 454-recScrollText3.y);
			scrollText.setRows(scrollText.getBounds().height
					/ infoPageRowHeight);
		} else if (catalogueList[titleIndex] instanceof Season 
				|| catalogueList[titleIndex] instanceof AvailableSource) {
			scrollText.setBounds(recScrollText4);
			scrollText.setTopMaskImage(dataCenter.getImage("01_des_sh_t03.png"));
			scrollText.setTopMaskImagePosition(32-recScrollText4.x, 399-recScrollText4.y);
			scrollText.setBottomMaskImagePosition(32-recScrollText4.x, 454-recScrollText4.y);
			scrollText.setRows(scrollText.getBounds().height / infoPageRowHeight);
		} else {
			scrollText.setBounds(recScrollText2);
			scrollText.setTopMaskImage(dataCenter.getImage("01_des_sh_t02.png"));
			scrollText.setTopMaskImagePosition(32-recScrollText2.x, 384-recScrollText2.y);
			scrollText.setBottomMaskImagePosition(32-recScrollText2.x, 454-recScrollText2.y);
			scrollText.setRows(scrollText.getBounds().height
					/ infoPageRowHeight);
		}
		if (menu.isAdultBlocked((MoreDetail) catalogueList[titleIndex])) {
			scrollText.setContents(dataCenter
					.getString(Resources.TEXT_MSG_BLOCKED));
			scrollText.setButtonIcon(false);
		} else {
			scrollText.setContents(titleDetail);
			scrollText.setButtonIcon(false);
		}
		
		setActionButtons();
		setButtons(getButtons(), getCurrentCatalogue());
		
		//VDTRMASTER-5564
		// R5
		if (getCurrentCatalogue() instanceof Video || getCurrentCatalogue() instanceof Bundle) {
			if (actionBox.getActionBox().length > 2) {
				availableView.setBounds(626, 328, 230, 24);
			} else {
				availableView.setBounds(626, 362, 230, 24);
			}
			
			add(availableView);
			// VDTRMASTER-5488
			availableView.setData((MoreDetail)getCurrentCatalogue(), this, false, true);
			availableView.startAnimation();
		} else {
			remove(availableView);
		}
		// VDTRMASTER-5693
		repaint();

		CatalogueDatabase.getInstance().retrieveImages(this,
				new BaseElement[] {
					catalogueList[titleIndex],
					catalogueList[(titleIndex + 1) % catalogueList.length],
					catalogueList[(titleIndex - 1 + catalogueList.length) % catalogueList.length]},
				ImageRetriever.TYPE_MEDIUM,
				CatalogueDatabase.IMAGE_RETRIEVER_MD);
	}

	public boolean handleKey(int key) {
		Log.printDebug("ListOfTitlesUI, handleKey()" + ", code = "
				+ key + ", popup = " + getPopUp());
		switch (key) {
		case HRcEvent.VK_LEFT:
			if (currentFocusUI == FOCUS_POSTERS && posterArray.keyLeft()) {
				keyLeft();
			}
			break;
		case HRcEvent.VK_RIGHT:
			if (currentFocusUI == FOCUS_POSTERS && posterArray.keyRight()) {
				keyRight();
			}
			break;
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_INFO:
			if (currentFocusUI == FOCUS_POSTERS) {
				if ((canSort || getCurrentCatalogue() instanceof Season) && getCurrentCatalogue() instanceof Episode == false) {
					posterArray.keyOK(); // 3591
				}
			}
			break;
		case HRcEvent.VK_ENTER:
			if (currentFocusUI == FOCUS_POSTERS) {
				posterArray.keyOK();
			} else {
				keyOK();
			}
			break;
		case KeyCodes.COLOR_A:
			keyYellow();
			break;
		case KeyCodes.COLOR_D:
			keyGreen();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		case HRcEvent.VK_PAGE_UP:
			keyPageUp();
			break;
		case HRcEvent.VK_PAGE_DOWN:
			keyPageDown();
			break;
		case OCRcEvent.VK_BACK:
		case KeyEvent.VK_F11:
			if (elementLen > 7) {
//				footer.clickAnimation(Resources.TEXT_CHANGE_PAGE);
				posterArray.keyPrev();
				titleIndex = posterArray.getIndex();
				setTitleDetail();
			}
			break;
		case OCRcEvent.VK_FORWARD:
		case KeyEvent.VK_F12:
			if (elementLen > 7) {
//				footer.clickAnimation(Resources.TEXT_CHANGE_PAGE);
				posterArray.keyNext();
				titleIndex = posterArray.getIndex();
				setTitleDetail();
			}
			break;
		}
		return true;
	}

	public void keyLeft() {
		titleIndex = posterArray.getIndex();
		setTitleDetail();
	}

	public void keyRight() {
		titleIndex = posterArray.getIndex();
		setTitleDetail();
	}

	public void keyUp() {
		if (state == BaseUI.STATE_ACTIVATED) {
			if (actionBox.moveFocus(true)) {
				setCurrentFocusUI(FOCUS_POSTERS);
				repaint();
			}
		}
	}

	public void keyDown() {
		if (state == BaseUI.STATE_ACTIVATED) {
			actionBox.moveFocus(false);
		} else { // focus on poster
			if (actionBox.getActionBox().length > 0) {
				setCurrentFocusUI(FOCUS_INFO);
			}
			repaint();
		}
	}

	public void keyPageUp() {
		footer.clickAnimation(Resources.TEXT_SCROLL_DESCRIPTION);
		scrollText.showPreviousPage();
	}

	public void keyPageDown() {
		footer.clickAnimation(Resources.TEXT_SCROLL_DESCRIPTION);
		scrollText.showNextPage();
	}

	public void keyBack() {
		// 20190222 Touche project, VDTRMASTER-6253		
		if (!menu.isFromSearch())
			VbmController.getInstance().backwardHistory();
		menu.backwardHistory(false);
		menu.back();
	}

	public void keyGreen() {
		Log.printDebug("ListOfTitlesUI, keyGreen()");
		int opt = 0;
		if (canSort) {
//			if (catalogueList == null || catalogueList.length == 0
//					|| catalogueList[0] instanceof Video == false) {
//				opt = 1;
//			} else {
//				opt = CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//						Resources.OPT_NAME) ? 2 : 1;
//			}
			opt = CatalogueDatabase.getInstance().getLastSortTypeIdx();
		}
		setPopUp(optionBox);
		optionBox.start(menu.getOptionMenu(opt, getCurrentCatalogue()), menu);
		repaint();
	}

	public void keyOK() {
		if (state != BaseUI.STATE_ACTIVATED) {
			return;
		}
		actionBox.click();
		menu.processActionMenu(actionBox.getMenu(), catalogueList[titleIndex]);
	}
	
	public void processFirstActionMenu() {
		String[] actionBtns = getActionButton(catalogueList[titleIndex]);
		if (actionBtns.length > 0) {

			String action = actionBtns[0].charAt(0) == '_' || actionBtns[0].charAt(0) == '+'
					|| actionBtns[0].charAt(0) == '!' ? actionBtns[0].substring(1) : actionBtns[0];

			menu.processActionMenu(action, catalogueList[titleIndex]);
		}
	}
	
	public void keyYellow() {
        // R7.3
        if (posterArray.getLastCategoryContainer() != null) {
            CatalogueDatabase.getInstance().setLastRequestedCategoryContainerId(posterArray.getLastCategoryContainer().id);
        }
		// VDTRMASTER-5772
		// VDTRMASTER-5690
		DataCenter.getInstance().put("VIEW_PARAM", param);
		menu.viewChange(catalogueList, catalogueList[titleIndex]);
		// VDTRMASTER-5690
		DataCenter.getInstance().remove("VIEW_PARAM");
	}

	/**
	 * @param currentFocusUI
	 *            the currentFocusUI to set
	 */
	public void setCurrentFocusUI(int currentFocusUI) {
		this.currentFocusUI = currentFocusUI;
		if (currentFocusUI == FOCUS_POSTERS) {
			setState(BaseUI.STATE_DEACTIVATED);
		} else {
			setState(BaseUI.STATE_ACTIVATED);
		}
	}

	public int getSortStatus() {
		if (canSort == false) {
			return 0;
		}
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_NAME)) {
//			return 1;
//		}
//		if (CatalogueDatabase.getInstance().isSortedWith(catalogueList,
//				Resources.OPT_DATE)) {
//			return 2;
//		}
//		return 0;
		return CatalogueDatabase.getInstance().getLastSortTypeIdx();
	}

	public BaseElement getCurrentCatalogue() {
		BaseElement reCatalogue = null;
		if (catalogueList != null && catalogueList.length > titleIndex) {
			reCatalogue = catalogueList[titleIndex];
		}
		return reCatalogue;
	}

	public void updateButtons() {
		setButtons(getButtons(), getCurrentCatalogue());
//		if (lastButtons != null) {
//			setButtons(lastButtons);
//		}
		int f = actionBox.getFocus();
		setActionButtons();
		actionBox.setFocus(f);
		
		// R5
		if (getCurrentCatalogue() instanceof Video || getCurrentCatalogue() instanceof Bundle) {
			if (actionBox.getActionBox().length > 2) {
				availableView.setBounds(626, 328, 230, 24);
			} else {
				availableView.setBounds(626, 362, 230, 24);
			}
			
			add(availableView);
			// VDTRMASTER-5488
			availableView.setData((MoreDetail)getCurrentCatalogue(), this, false, true);
			availableView.startAnimation();
		} else {
			remove(availableView);
		}
	}
	
	public void reflectVCDSResult(Object cmd) {
		menu.hideLoadingAnimation();
		if (CatalogueDatabase.SORT.equals(cmd)) {
			// VDTRMASTER-5653 & VDTRMASTER-5649
			setFocus(0);
			titleIndex = 0;
			posterArray.resetIndex();
			if (menu.getLastCategoryId().startsWith("TO_")) {
				Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(menu.getLastCategoryId());
				setData(topic.treeRoot.totalContents);
			} else {
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				setData(cat.totalContents);
			}
		}
	}

	public void selected(MenuItem item) {
//		if (Resources.OPT_NAME.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_NAME);
//			setData(catalogueList);
//			menu.setSortNormal(Resources.OPT_NAME);
//		} else if (Resources.OPT_DATE.equals(item.getKey())) {
//			catalogueList = CatalogueDatabase.getInstance().sort(catalogueList,
//					Resources.OPT_DATE);
//			setData(catalogueList);
//			menu.setSortNormal(Resources.OPT_DATE);
//		}
//		repaint();
	}

	public void receiveCheckRightFilter(CheckResult checkResult) {
		Log.printDebug("ListOfTitlesUI, receiveCheckRightFilter()"
				+ ", filter = " + checkResult.getFilter() + ", response = "
				+ checkResult.getResponse());
		if (checkResult.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
			setActionButtons();
			setTitleDetail();
			repaint();
		}
	}
	
	public boolean isMixedContents() {
		
		Hashtable countTable = new Hashtable();
		
		if (catalogueList != null) {
			for (int i = 0; i < catalogueList.length; i++) {
				if (catalogueList[i] instanceof Series) {
					countTable.put("Series", catalogueList[i]);
				} else if (catalogueList[i] instanceof Season) {
					countTable.put("season", catalogueList[i]);
				} else if (catalogueList[i] instanceof Bundle) {
					countTable.put("bundle", catalogueList[i]);
				} else if (catalogueList[i] instanceof Video) {
					Video v = (Video) catalogueList[i];
					
					if (v instanceof Episode) {
						countTable.put("episode", catalogueList[i]);
					} else if ("VT_100".equals(v.getTypeID())) {
						countTable.put("VT_100", catalogueList[i]);
					} else if ("VT_102".equals(v.getTypeID())) {
						countTable.put("vt_102", catalogueList[i]);
					} else if ("VT_103".equals(v.getTypeID())) {
						countTable.put("VT_103", catalogueList[i]);
					} else if ("VT_104".equals(v.getTypeID())) {
						countTable.put("VT_104", catalogueList[i]);
					} else if ("VT_124".equals(v.getTypeID())) {
						countTable.put("VT_124", catalogueList[i]);
					}
				}
				
				if (countTable.size() > 1) {
					return true;
				}
			}
		}
		
		return false;
	}
}

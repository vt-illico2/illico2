package com.videotron.tvi.illico.vod.ui;

import java.awt.*;
import java.awt.Image;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.comp.AvailableDateView;
import com.videotron.tvi.illico.vod.comp.SimilarContentView;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.*;

/**
 * Base class for other screen UI.
 */
public class BaseUI extends UIComponent implements Runnable,
		PinEnablerListener, MenuListener, ErrorMessageListener, TVTimerWentOffListener {

	/**
	 * Instance for DataCenter.
	 */
	protected DataCenter dataCenter = DataCenter.getInstance();
	/**
	 * Instance for MenuController.
	 */
	protected static MenuController menu;
	/**
	 * Which component is focused.
	 */
	protected int currentFocusUI;
	/**
	 * Activate state.
	 */
	public static final int STATE_ACTIVATED = 1;
	/**
	 * Inactive state.
	 */
	public static final int STATE_DEACTIVATED = 0;
	/**
	 * Back/Last in context buttons.
	 */
	public static final int BTN_BACK = 0;
	/**
	 * Search in context buttons.
	 */
	public static final int BTN_SEARCH = 1;
	/**
	 * Scroll in context buttons.
	 */
	public static final int BTN_SCROLL = 2;
	/**
	 * Page in context buttons.
	 */
	public static final int BTN_PAGE = 3;
	/**
	 * Option in context buttons.
	 */
	public static final int BTN_OPTION = 4;
	/**
	 * Change views in context buttons.
	 */
	public static final int BTN_VIEW = 5;
	/**
	 * Wishilist in context buttons.
	 */
	public static final int BTN_WISHLIST = 6;
	/**
	 * Playlist in context buttons.
	 */
	public static final int BTN_PLAYLIST = 7;
	public static final int BTN_ALL_CHANNEL = 8;
	public static final int BTN_MY_CHANNEL = 9;
	public static final int BTN_PAGE_V = 10;
	
	public static final int BTN_PROFILE = 11;

	public static Image imgProfile;
	/**
	 * Values for context buttons.
	 */
	private int[] buttons;
	/**
	 * Button labels for context buttons.
	 */
	private String[] buttonLbls;
	/**
	 * Popup on current screen.
	 */
	private UIComponent popUp;

	public int scroll;
	public Rectangle scrollBounds = new Rectangle();
	public boolean needSearch = false;

	protected ImageLabel hotkeyBG;
	protected Footer footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
	protected OptionScreen optionBox = new OptionScreen();
	protected TVTimerSpec generalTimer;
	
	// R5
	protected AvailableDateView availableView = new AvailableDateView();
	protected boolean fromCategory;

	public static void setMenuController(MenuController instance) {
		menu = instance;
	}
	
	/**
     * R7.4+Touche
     */
	public static MenuController getMenuController() {
		return menu;
	}

	public BaseUI() {
		footer.setFont(FontResource.DINMED.getFont(15));
	}

	/**
	 * Screen start method.
	 *
	 * @param back
	 *            true if this screen is return back
	 */
	public void start(boolean back) {
	}

	/**
	 * Screen start method. It turns on visibility of this screen.
	 */
	public void start() {
		requestFocus();
		setVisible(true);
	}

	/**
	 * Screen stop method. It turns off visibility of this screen. If it has
	 * pop-up, remove it.
	 */
	public void stop() {
		optionBox.stop();
		setupTimer(false, 0);
		setVisible(false);
		if (popUp != null) {
			remove(popUp);
			if (popUp instanceof OptionScreen) {
				((OptionScreen) popUp).stop();
			}
			popUp = null;
		}
		
		availableView.stopAnimation();
		availableView.stop();
	}

	/**
	 * Clean up method.
	 */
	public void flushData() {

	}

	/**
	 * BaseUI implements Runnable.
	 */
	public void run() {
		if (Log.INFO_ON) {
			Log.printDebug("BaseUI run() ");
		}
	}

	public void setupTimer(boolean turnOn, long delay) {
		Log.printDebug("setupTimer(), " + turnOn + ", d = " + delay + ", " + this);
		if (turnOn) {
			setupTimer(false, 0);
			generalTimer = new TVTimerSpec();
			generalTimer.setDelayTime(delay > 0 ? delay : 1000);
			generalTimer.setRepeat(true);
			generalTimer.addTVTimerWentOffListener(this);
			try {
				generalTimer = TVTimer.getTimer().scheduleTimerSpec(generalTimer);
			} catch (TVTimerScheduleFailedException e) {}
		} else {
			if (generalTimer != null) {
				TVTimer.getTimer().deschedule(generalTimer);
				generalTimer.removeTVTimerWentOffListener(this);
				generalTimer = null;
			}
		}
	}
	/**
	 * Handle a key event.
	 *
	 * @param key
	 *            key codes inputed
	 * @return true if consumed
	 */
	public boolean handleKey(int key) {
		return false;
	}

	/**
	 * Returns current state.
	 *
	 * @return current state
	 */
	public int getState() {
		if (getPopUp() != null) {
			return STATE_DEACTIVATED;
		}
		return state;
	}

	/**
	 * @return the currentFocusUI
	 */
	public int getCurrentFocusUI() {
		return currentFocusUI;
	}

	/**
	 * @param newCurrentFocusUI
	 *            the currentFocusUI to set
	 */
	public void setCurrentFocusUI(int newCurrentFocusUI) {
		currentFocusUI = newCurrentFocusUI;
		repaint();
	}

	/**
	 * @return the buttons
	 */
	public int[] getButtons() {
		return buttons;
	}

	/**
	 * Returns Wisilist button is exists or not.
	 *
	 * @return true if exists
	 */
	public boolean isExistWishlistBtn() {
		boolean exist = false;
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				if (buttons[i] == BTN_WISHLIST) {
					exist = true;
				}
			}
		}
		return exist;
	}

	/**
	 * Returns change view button is exists or not.
	 *
	 * @return true if exists
	 */
	public boolean isExistChangeViewBtn() {
		boolean exist = false;
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				if (buttons[i] == BTN_VIEW) {
					exist = true;
				}
			}
		}
		return exist;
	}

	public boolean isExistPlayListBtn() {
		boolean exist = false;
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				if (buttons[i] == BTN_PLAYLIST) {
					exist = true;
				}
			}
		}
		return exist;
	}

	public boolean isExistWishListBtn() {
		boolean exist = false;
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				if (buttons[i] == BTN_WISHLIST) {
					exist = true;
				}
			}
		}
		return exist;
	}
	
	public int getChannelBtn() {
		int exist = 0;
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				if (buttons[i] == BTN_ALL_CHANNEL || buttons[i] == BTN_MY_CHANNEL) {
					exist = buttons[i];
				}
			}
		}
		return exist;
	}
	
	public boolean isExistPageVertical() {
		boolean exist = false;
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				if (buttons[i] == BTN_PAGE_V) {
					exist = true;
				}
			}
		}
		return exist;
	}
	
	public boolean isExistPageHorizontal() {
		boolean exist = false;
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				if (buttons[i] == BTN_PAGE) {
					exist = true;
				}
			}
		}
		return exist;
	}

	/**
	 * UpdateUpdate context buttons.
	 */
	public void updateButtons() {
	}

	/**
	 * @return the buttonLbls
	 */
	public String[] getButtonLbls() {
		return buttonLbls;
	}

	/**
	 * @param btns
	 *            the buttons to set
	 */
//	public void setButtons(int[] btns) {
//		setButtons(btns, null);
//	}
	/**
	 * @param btns
	 *            the buttons to set
	 */
	public void setButtons(int[] btns, BaseElement element) {
		if (Log.DEBUG_ON) {
			for (int i = 0; i < btns.length; i++) {
				Log.printDebug("BaseUI, setButtons, btns[" + i + "]=" + btns[i]);
			}
			Log.printDebug("BaseUI, setButtons, element=" + element);
		}
		// int wishListSize = PreferenceProxy.getInstance().wishListSize();
		this.buttons = btns;

		buttonLbls = new String[buttons.length];
		String tmpLbl = null;
		Image image = null;
		footer.reset();

		Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);

		for (int i = 0; i < buttons.length; i++) {
			tmpLbl = null;
			switch (buttons[i]) {
			case BTN_BACK:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_BACK);
				image = (Image) imgTable.get(PreferenceService.BTN_BACK);
				break;
			case BTN_SEARCH:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_SEARCH);
				image = (Image) imgTable.get(PreferenceService.BTN_SEARCH);
				break;
			case BTN_SCROLL:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_SCROLL_DESCRIPTION);
				image = (Image) imgTable.get(PreferenceService.BTN_PAGE);
				break;
			case BTN_PAGE:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_CHANGE_PAGE);
				image = (Image) imgTable.get(PreferenceService.BTN_SKIP);
				break;
			case BTN_PAGE_V:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_CHANGE_PAGE);
				image = (Image) imgTable.get(PreferenceService.BTN_PAGE);
				break;
			case BTN_OPTION:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_OPTION);
				image = (Image) imgTable.get(PreferenceService.BTN_D);
				break;
			case BTN_VIEW:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_VIEW_CHANGE);
				image = (Image) imgTable.get(PreferenceService.BTN_A);
				break;
			case BTN_WISHLIST:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_MY_WISHLIST);
				
				boolean isAdult = menu.isAdult();
				
				// VDTRMASTER-5532
				if (element != null && element instanceof Orderable) {
					isAdult = BookmarkManager.getInstance().isAdult((Orderable)element);
				}
				
				// VDTRMASTER-5608
				if (getFooter().getParent() instanceof SimilarContentView &&
						((SimilarContentView)getFooter().getParent()).hasChannelName()) {
					isAdult = false;
				}
				
				// showcase content in channel on demand
				// before enter detail screen, there are no channel information.
				if (this instanceof CategoryUI && CategoryUI.getChannelCategory() != null) {
//					CategoryUI cUI = (CategoryUI) this;
//					
//					if (isAdult && currentFocusUI == CategoryUI.FOCUS_RIGHT) {
//						CategoryUI.getChannelCategory();
//					}
					isAdult = false;
				}
				
				if (isAdult) {
					tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_MY_WISHLIST_18);
				}
				
				int wishListSize = 0;
				try {
					wishListSize = isAdult ? BookmarkManager.getInstance().retrieveWishlistTitlesAdult().length : BookmarkManager
							.getInstance().retrieveWishlistTitles(true).length;
				} catch (NullPointerException e) {
					// retrieveWishlistTitlesAdult may return null
				}

				tmpLbl = tmpLbl + " (" + wishListSize + ")";
				image = (Image) imgTable.get(PreferenceService.BTN_B);
				break;
			case BTN_PLAYLIST:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_MY_PLAYLIST);
				int playListSize = MenuController.getInstance().getPlaylist().size();
				tmpLbl = tmpLbl + " (" + playListSize + ")";
				image = (Image) imgTable.get(PreferenceService.BTN_B);
				break;
			case BTN_ALL_CHANNEL:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_VIEW_ALL_CHANNEL);
				image = (Image) imgTable.get(PreferenceService.BTN_A);
				break;
			case BTN_MY_CHANNEL:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_VIEW_MY_CHANNEL);
				image = (Image) imgTable.get(PreferenceService.BTN_A);
				break;
			case BTN_PROFILE:
				if (element != null) {
					tmpLbl = getWishButtonName(element);
					
					if (tmpLbl == null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("BaseUI, setButtons, button name is null");
						}
						continue;
					}
					image = (Image) imgTable.get(PreferenceService.BTN_PRO);
					imgProfile = image;
				} else {
					continue;
				}
				break;
			}
			buttonLbls[i] = tmpLbl;

			footer.addButtonWithLabel(image, tmpLbl);
		}
	}
	
	public String getWishButtonName(BaseElement element) {
		if (Log.DEBUG_ON) {
			Log.printDebug("BaseUI, getWishButtonName(), element=" + element);
		}
		
		// VDTRMASTER-5608
		if (getFooter() != null && getFooter().getParent() instanceof SimilarContentView) {
			boolean isAdult = false;
			BaseElement fContent = ((SimilarContentView)getFooter().getParent()).getFocusedContent();
			if (fContent != null && fContent instanceof Orderable) {
				isAdult = BookmarkManager.getInstance().isAdult((Orderable)fContent);
				
				if (((SimilarContentView)getFooter().getParent()).hasChannelName()) {
					isAdult = false;
				}
				
				if (BookmarkManager.getInstance().isInWishlist((MoreDetail) fContent) != null) {
					return dataCenter.getString(isAdult ? Resources.TEXT_REMOVE_FROM_WISHLIST_18 : Resources.TEXT_REMOVE_FROM_WISHLIST);
				} else {
					return dataCenter.getString(isAdult ? Resources.TEXT_ADDTO_WISHLIST_18 : Resources.TEXT_ADDTO_WISHLIST);
				}
			}
			
		} else {
			
			if (element instanceof Video) {
				Video title = (Video) element;
				if (CommunicationManager.getInstance().hasAnyPackage() && 
						CommunicationManager.getInstance().checkPackageForWishList(title.service)) {
					boolean isAdult = BookmarkManager.getInstance().isAdult(title);
					if (this instanceof CategoryUI && CategoryUI.getChannelCategory() != null) {
						isAdult = false;
					}
					if (BookmarkManager.getInstance().isInWishlist(title) != null) {
						return dataCenter.getString(isAdult ? Resources.TEXT_REMOVE_FROM_WISHLIST_18 : Resources.TEXT_REMOVE_FROM_WISHLIST);
					} else {
						return dataCenter.getString(isAdult ? Resources.TEXT_ADDTO_WISHLIST_18 : Resources.TEXT_ADDTO_WISHLIST);
					}
				}
			} else if (element instanceof Bundle) {
				Bundle bundle = (Bundle) element;
				if (CommunicationManager.getInstance().hasAnyPackage()
						&& CommunicationManager.getInstance().checkPackageForWishList(bundle.service)) {
					boolean isAdult = BookmarkManager.getInstance().isAdult(bundle);
					if (this instanceof CategoryUI && CategoryUI.getChannelCategory() != null) {
						isAdult = false;
					}
					if (BookmarkManager.getInstance().isInWishlist(bundle) != null) {
						return dataCenter.getString(isAdult ? Resources.TEXT_REMOVE_FROM_WISHLIST_18 : Resources.TEXT_REMOVE_FROM_WISHLIST);
					} else {
						return dataCenter.getString(isAdult ? Resources.TEXT_ADDTO_WISHLIST_18 : Resources.TEXT_ADDTO_WISHLIST);
					}
				}
			} else if (element instanceof CategoryContainer && ((CategoryContainer) element).type == MenuController.MENU_CHANNEL) {
			    CategoryContainer categoryContainer = (CategoryContainer) element;

			    if (ChannelInfoManager.getInstance().isSubscribed(categoryContainer.getTopic())) {
			        if (ChannelInfoManager.getInstance().isFavoriteChannel(categoryContainer.getTopic())) {
                        // remove from favorite
                        return dataCenter.getString(Resources.OPT_REMOVE_FAVORITE_CHANNEL);
                    } else {
			            // add from favorite
                        return dataCenter.getString(Resources.OPT_ADD_FAVORITE_CHANNEL);
                    }
                }
            }
		}
		
		return null;
	}

	/**
	 * @return the popUp
	 */
	public UIComponent getPopUp() {
		return popUp;
	}

	public Footer getFooter() {
		return footer;
	}

	/**
	 * @param newPopUp
	 *            the popUp to set
	 */
	public void setPopUp(UIComponent newPopUp) {
	    // R7.3
        if (newPopUp instanceof OptionScreen) {
            return;
        }

		popUp = newPopUp;
//		if (newPopUp != null) {
//			add(newPopUp, 0);
//		}
        // VDTRMASTER-6042
        menu.addPopup(newPopUp);
		repaint();
	}

	// R7.3
    public void remove(UIComponent comp) {
	    super.remove(comp);

	    if (comp != null) {
            menu.removePopup(comp);
        }
    }

	/**
	 * It will be called when pop-up is closed.
	 *
	 * @param popup
	 *            pop-up instance
	 * @param msg
	 *            data from pop-up
	 */
	public void popUpClosed(UIComponent popup, Object msg) {
	}

	/**
	 * It will be called when VCDS connection is done.
	 *
	 * @param cmd
	 *            data from caller
	 */
	public void reflectVCDSResult(Object cmd) {
	}

	/**
	 * It will be called when getting image is done.
	 */
	public void reflectImage() {
		repaint();
	}
	
	public void imageDownloadMore() { // DDC 37.1
		
	}

	/**
	 * Receive a result and detail from PinEnabler. In case of the only error,
	 * It receive a detail.
	 *
	 * @param result
	 *            The one of {@link PreferenceService#RESPONSE_SUCCESS}, {@link PreferenceService#RESPONSE_FAIL},
	 *            {@link PreferenceService#RESPONSE_ERROR}, {@link PreferenceService#RESPONSE_CANCEL}
	 * @param detail
	 *            The detail message of error.
	 * @throws RemoteException
	 *             The remote exception
	 */
	public void receivePinEnablerResult(int result, String detail)
			throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceProxy receivePinEnablerResult result : "
							+ result);
			Log.printDebug("PreferenceProxy receivePinEnablerResult detail : "
							+ detail);
		}
	}

	// public void showWishlistDock(WishlistDockUI wishlist) {
	// Log.printDebug("showWishlistDock(), showing now = "
	// + (getPopUp() == wishlist));
	// if (getPopUp() == wishlist) {
	// wishlist.keyBlue();
	// return;
	// }
	// ArrayList list = PreferenceProxy.getInstance().getWishList();
	// BaseElement[] data = new BaseElement[list.size()];
	// for (int i = 0; i < data.length; i++) {
	// data[i] = (BaseElement) CatalogueDatabase.getInstance().getCached(
	// list.get(i));
	// if (data[i] == null) {
	// // need retrieve
	// }
	// }
	// setPopUp(wishlist);
	// wishlist.show(this, data);
	// }

	/**
	 * It will be called when option popup closed with user selection.
	 *
	 * @param item
	 *            user selected
	 */
	public void selected(MenuItem item) {
		setPopUp(null);
	}

	/**
	 * It will be called when option popup closed without user selection.
	 */
	public void canceled() {
		setPopUp(null);
	}

	/**
	 * Returns action buttons.
	 *
	 * @param element
	 *            buttons will made by element's type
	 * @return action buttons
	 */
	public static String buttonInfo;
	public String[] getActionButton(BaseElement element) {
		buttonInfo = "";
		ArrayList list = new ArrayList();
		boolean isAvailableService = false;

		// Bundling
		// VDTRMASTER-5940
		boolean addedFreeviewButton = false;

		if (element != null) {
			if (element instanceof Video) {
				Video title = (Video) element;
				boolean orderBundle = false;
				if (this instanceof BundleDetailUI) {
					Bundle bundle = (Bundle) CatalogueDatabase.getInstance().getCached(title.inBundlesAR[0]);
					if (MenuController.getInstance().isBlocked(bundle) == false
							&& BookmarkManager.getInstance().isPurchased(bundle) == false
							&& bundle.getStart().before(new Date())) {
						list.add(dataCenter.getString(Resources.TEXT_ORDER_BUNDLE));
						orderBundle = true;
					}
				} else if (this instanceof ListViewUI && ((ListViewUI) this).isResumeViewing()) {
					buttonInfo = "config = " + DataCenter.getInstance().getString(RightFilter.BLOCK_BY_RATINGS) + ", title = "
							+ title.getRating();
					// Bundling
					// VDTRMASTER-5934
					if (MenuController.getInstance().isBlocked(title)) {
						return new String[] {
								dataCenter.getString(Resources.TEXT_UNBLOCK)
						};
					} else {
						if (((ListViewUI) this).isExplored()) {
							String label = ((ListViewUI) this).findLabel();
							return new String[] {
									dataCenter.getString(Resources.TEXT_WATCH),
									dataCenter.getString(Resources.TEXT_WATCH_ALL) + label,
							};
						} else {
//							return new String[] { dataCenter.getString(Resources.TEXT_WATCH) };

							if (title.channel != null) {
								String watch = null;
								if (title.channel.network == null) { // 1 channel
									if (title.channel.hasAuth == false) {
										title.channel.hasAuth = CommunicationManager.getInstance().checkAuth(title.channel.callLetters);
									}
									if (title.channel.hasAuth) {
										watch = title.channel.callLetters;
									}
								} else {
									for (int i = 0; i < title.channel.network.channel.length; i++) {
										if (title.channel.network.channel[i].hasAuth == false) {
											title.channel.network.channel[i].hasAuth = CommunicationManager.getInstance().checkAuth(title.channel.network.channel[i].callLetters);
										}
										if (title.channel.network.channel[i].hasAuth) {
											watch = title.channel.network.channel[i].callLetters;
											break;
										}
									}
								}
								if (watch == null || !title.hasPacakgeForServiceConfigType()) {
									buttonInfo = "STB has no channel rights";
									// Bundling
									if (title.isFreeview) {
										return new String[] {
												"!" + dataCenter.get(Resources.BTN_FREE_WATCH_NOW)
										};
									}
								} else {
									buttonInfo = "Normal bookmark";
									if (title.isFreeview) {
										return new String[] {
												"!" + dataCenter.get(Resources.BTN_FREE_WATCH_NOW)
										};
									} else {
										return new String[] { dataCenter.getString(Resources.TEXT_WATCH) };
									}
								}
							} else if (CommunicationManager.getInstance().checkPackage(title.service) == false
									|| !title.hasPacakgeForServiceConfigType()) {
								if (CommunicationManager.getInstance().hasAnyPackage()) {

									// Bundling
									if (title.isFreeview) {
										return new String[] {
												"!" + dataCenter.get(Resources.BTN_FREE_WATCH_NOW)
										};
									} else {
										buttonInfo = "Normal bookmark";
										if (title.isFreeview) {
											return new String[] {
													"!" + dataCenter.get(Resources.BTN_FREE_WATCH_NOW)
											};
										} else {
											return new String[] { dataCenter.getString(Resources.TEXT_WATCH) };
										}
									}
								} else {
									buttonInfo = "Err or No vod_category_config";
								}
							} else {
								if (title.isFreeview) {
									return new String[] {
											"!" + dataCenter.get(Resources.BTN_FREE_WATCH_NOW)
									};
								} else {
									return new String[] { dataCenter.getString(Resources.TEXT_WATCH) };
								}
							}
						}
					}
				}
				if (Log.DEBUG_ON) {
				    Log.printDebug("BaseUI, getActionButton, title.inBundleOnly=" + title.inBundleOnly);
                }
				if (title.inBundleOnly) {
					if (MenuController.getInstance().isBlocked(title)) {
						list.add(dataCenter.getString(Resources.TEXT_UNBLOCK));
						buttonInfo = "config = " + DataCenter.getInstance().getString(RightFilter.BLOCK_BY_RATINGS) + ", title = "
								+ title.getRating() + ", bndl only";
					} else {
						if (this instanceof BundleDetailUI && !orderBundle) {
							list.add(dataCenter.getString(Resources.TEXT_WATCH));
							buttonInfo = "bundle is subscribed";
						} else {
							buttonInfo = "in bundle only";

							// VDTRMASTER-5991
//							if (!orderBundle) {
//								list.add((dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE)));
//							}
                            // VDTRMASTER-6084
                            if (title.inBundlesAR.length > 0) {
                                Bundle bundle = (Bundle) CatalogueDatabase.getInstance().getCached(title.inBundlesAR[0]);
                                if (bundle != null) {
                                    orderBundle = BookmarkManager.getInstance().isPurchased(bundle);
                                }

                                if (orderBundle) {
                                    // VDTRMASTER-6106
                                    list.add(dataCenter.getString(Resources.TEXT_WATCH));
                                }
                            } else {
                                if (this instanceof BundleDetailUI == false) {
                                    list.add((dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE)));
                                }
                            }
						}
					}
				} else {
					// TODO
					if (this instanceof ListOfTitlesUI && ((ListOfTitlesUI)this).isFromAvailableSource()) {
						Log.printDebug("getActionButton, isFromAvailableSource");
						BaseElement lastObject = ((ListOfTitlesUI)this).getLastObject();
						AvailableSource available = (AvailableSource) lastObject;
						
						if (MenuController.getInstance().isBlocked(title)) {
							list.add(dataCenter.getString(Resources.TEXT_UNBLOCK));
							buttonInfo = "config = " + DataCenter.getInstance().getString(RightFilter.BLOCK_BY_RATINGS) + ", title = "
									+ title.getRating();
						} else if (available.channel != null) {
							String watch = null;
							if (available.channel.hasAuth == false) {
								available.channel.hasAuth = CommunicationManager.getInstance().checkAuth(available.channel.callLetters);
							}
							if (available.channel.hasAuth) {
								watch = available.channel.callLetters;
							}

							if (watch == null || !available.hasPacakgeForServiceConfigType()) {
								// Bundling
								// VDTRMASTER-5940
								addedFreeviewButton = processFreeview(list, title);

								if (CommunicationManager.getInstance().isaSupported(available.channel.callLetters)
										|| !CommunicationManager.getInstance().isIncludedInExcludedData(
										available.channel.callLetters)) {

									list.add("+" + dataCenter.getString(Resources.TEXT_SUBSCRIBE));
								} else {
									list.add(dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG));
								}
								
								buttonInfo = "STB has no channel rights";
							} else if (title.isFree()) {
								// VDTRMASTER-5940
								if (!processFreeview(list, title)) {
									list.add(dataCenter.getString(Resources.TEXT_WATCH));
								}
								buttonInfo = watch + " is subscribed";
							} else if (title.getStart().before(new Date())) {
								list.add(dataCenter.getString(orderBundle ? Resources.TEXT_ORDER_TITLE : Resources.TEXT_ORDER));
								buttonInfo = "Pay+" + watch + "=orderable";
							} else {
								buttonInfo = "Pay+" + watch + ", " + title.getStart();
							}
						} else if (available.network != null) {
							String watch = null;
							for (int i = 0; i < available.network.channel.length; i++) {
								if (available.network.channel[i].hasAuth == false) {
									available.network.channel[i].hasAuth = CommunicationManager.getInstance().checkAuth(available.network.channel[i].callLetters);
								}
								if (available.network.channel[i].hasAuth) {
									watch = available.network.channel[i].callLetters;
									break;
								}
							}

							if (watch == null || !available.hasPacakgeForServiceConfigType()) {
								// Bundling
								// VDTRMASTER-5940
								addedFreeviewButton = processFreeview(list, title);
								if (CommunicationManager.getInstance().isaSupported(available.network.getId())
										|| !CommunicationManager.getInstance().isIncludedInExcludedData(
										available.network.getId())) { // VDTRMASTER-5885

									list.add("+" + dataCenter.getString(Resources.TEXT_SUBSCRIBE));
								} else {
									list.add(dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG));
								}
								
								buttonInfo = "STB has no channel rights";
							} else if (title.isFree()) {
								// VDTRMASTER-5940
								if (!processFreeview(list, title)) {
									list.add(dataCenter.getString(Resources.TEXT_WATCH));
								}
								buttonInfo = watch + " is subscribed";
							} else if (title.getStart().before(new Date())) {
								list.add(dataCenter.getString(orderBundle ? Resources.TEXT_ORDER_TITLE : Resources.TEXT_ORDER));
								buttonInfo = "Pay+" + watch + "=orderable";
							} else {
								buttonInfo = "Pay+" + watch + ", " + title.getStart();
							}
						} else if (available.service != null && available.service.description != null && CommunicationManager.getInstance().checkPackage(available.service) == false) {
							// Bundling
							// VDTRMASTER-5940
							addedFreeviewButton = processFreeview(list, title);
							if (CommunicationManager.getInstance().hasAnyPackage()) {
								isAvailableService = true;
								list.add((CommunicationManager.getInstance().isaSupported(available.service.getId2())
										|| !CommunicationManager.getInstance().isIncludedInExcludedData(
										available.service.getId2()) ? "+" : "")
										+ dataCenter.getString(Resources.TEXT_SUBSCRIBE_SVOD) + available.service.getLanguageContentData(available.service.description.title));
								buttonInfo = "No " + title.dumpService();
							} else {
								buttonInfo = "Err or No vod_category_config";
							}
						} else {
							if (title.isFree()) {
								// VDTRMASTER-5940
								if (!processFreeview(list, title)) {
									list.add(dataCenter.getString(Resources.TEXT_WATCH));
								}
								buttonInfo = "It's free and no channel info";
							} else if (title.getStart().before(new Date())) {
								list.add(dataCenter.getString(orderBundle ? Resources.TEXT_ORDER_TITLE : Resources.TEXT_ORDER));
								buttonInfo = "Pay and orderable";
							} else {
								buttonInfo = "Pay, " + title.getStart();
							}
						}
					} else {
						if (MenuController.getInstance().isBlocked(title)) {
							list.add(dataCenter.getString(Resources.TEXT_UNBLOCK));
							buttonInfo = "config = " + DataCenter.getInstance().getString(RightFilter.BLOCK_BY_RATINGS) + ", title = "
									+ title.getRating();
						} else if ((buttonInfo = BookmarkManager.getInstance().isPurchased(title)) != null) {
//							list.add(dataCenter.getString(Resources.TEXT_WATCH));
//							buttonInfo += " is purchased";

							// Bundling
							// VDTRMASTER-5934
							if (title.channel != null) {
								String watch = null;
								if (title.channel.network == null) { // 1 channel
									if (title.channel.hasAuth == false) {
										title.channel.hasAuth = CommunicationManager.getInstance().checkAuth(title.channel.callLetters);
									}
									if (title.channel.hasAuth) {
										watch = title.channel.callLetters;
									}
								} else {
									for (int i = 0; i < title.channel.network.channel.length; i++) {
										if (title.channel.network.channel[i].hasAuth == false) {
											title.channel.network.channel[i].hasAuth = CommunicationManager.getInstance().checkAuth(title.channel.network.channel[i].callLetters);
										}
										if (title.channel.network.channel[i].hasAuth) {
											watch = title.channel.network.channel[i].callLetters;
											break;
										}
									}
								}

								if (watch == null || !title.hasPacakgeForServiceConfigType()) {

									// Bundling
									// VDTRMASTER-5940
									addedFreeviewButton = processFreeview(list, title);

									buttonInfo = "STB has no channel rights";
									// Bundling
									if (!(this instanceof ListViewUI && ((ListViewUI) this).isResumeViewing())) {
										if (CommunicationManager.getInstance().isaSupported(title.channel.callLetters)
												|| !CommunicationManager.getInstance().isIncludedInExcludedData(
												title.channel.callLetters)) {
											list.add("+" + dataCenter.getString(Resources.TEXT_SUBSCRIBE));
										} else {
											list.add(dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG));
										}
									}
								} else {
									buttonInfo += " is purchased";
									// VDTRMASTER-5940
									if (!processFreeview(list, title)) {
										list.add(dataCenter.getString(Resources.TEXT_WATCH));
									}
								}

							} else if (CommunicationManager.getInstance().checkPackage(title.service) == false
									|| !title.hasPacakgeForServiceConfigType()) {

								// VDTRMASTER-5940
								addedFreeviewButton = processFreeview(list, title);

								if (CommunicationManager.getInstance().hasAnyPackage()) {

									// Bundling
									if (!(this instanceof ListViewUI && ((ListViewUI) this).isResumeViewing())) {
										list.add((CommunicationManager.getInstance().isaSupported(title.service[0].getId2())
												|| !CommunicationManager.getInstance().isIncludedInExcludedData(
												title.service[0].getId2()) ? "+" : "")
												+ dataCenter.getString(Resources.TEXT_SUBSCRIBE_SVOD) + title.getServiceTitle());
										buttonInfo = "No " + title.dumpService();
									}
								} else {
									buttonInfo = "Err or No vod_category_config";
								}
							} else {
								buttonInfo += " is purchased";
								// VDTRMASTER-5940
								if (!processFreeview(list, title)) {
									list.add(dataCenter.getString(Resources.TEXT_WATCH));
								}
							}
						} else if (title.channel != null) {
							String watch = null;
							if (title.channel.network == null) { // 1 channel
								if (title.channel.hasAuth == false) {
									title.channel.hasAuth = CommunicationManager.getInstance().checkAuth(title.channel.callLetters);
								}
								if (title.channel.hasAuth) {
									watch = title.channel.callLetters;
								}
							} else {
								for (int i = 0; i < title.channel.network.channel.length; i++) {
									if (title.channel.network.channel[i].hasAuth == false) {
										title.channel.network.channel[i].hasAuth = CommunicationManager.getInstance().checkAuth(title.channel.network.channel[i].callLetters);
									}
									if (title.channel.network.channel[i].hasAuth) {
										watch = title.channel.network.channel[i].callLetters;
										break;
									}
								}
							}

							if (watch == null || !title.hasPacakgeForServiceConfigType()) {
								// Bundling
								// VDTRMASTER-5940
								addedFreeviewButton = processFreeview(list, title);
								if (CommunicationManager.getInstance().isaSupported(title.channel.callLetters)
										|| !CommunicationManager.getInstance().isIncludedInExcludedData(
										title.channel.callLetters)) {
									list.add("+" + dataCenter.getString(Resources.TEXT_SUBSCRIBE));
								} else {
									list.add(dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG));
								}
								
								buttonInfo = "STB has no channel rights";
							} else if (title.isFree()) {
								// VDTRMASTER-5940
								if (!processFreeview(list, title)) {
									list.add(dataCenter.getString(Resources.TEXT_WATCH));
								}
								buttonInfo = watch + " is subscribed";
							} else if (title.getStart().before(new Date())) {
								list.add(dataCenter.getString(orderBundle ? Resources.TEXT_ORDER_TITLE : Resources.TEXT_ORDER));
								buttonInfo = "Pay+" + watch + "=orderable";
							} else {
								buttonInfo = "Pay+" + watch + ", " + title.getStart();
							}
						} else if (CommunicationManager.getInstance().checkPackage(title.service) == false
								|| !title.hasPacakgeForServiceConfigType()) {

							// Bundling
							// VDTRMASTER-5940
							addedFreeviewButton = processFreeview(list, title);

							if (CommunicationManager.getInstance().hasAnyPackage()) {

								list.add((CommunicationManager.getInstance().isaSupported(title.service[0].getId2())
										|| !CommunicationManager.getInstance().isIncludedInExcludedData(
										title.service[0].getId2()) ? "+" : "")
										+ dataCenter.getString(Resources.TEXT_SUBSCRIBE_SVOD) + title.getServiceTitle());
								buttonInfo = "No " + title.dumpService();
							} else {
								buttonInfo = "Err or No vod_category_config";
							}
						} else {
							if (title.isFree()) {
								// VDTRMASTER-5940
								if (!processFreeview(list, title)) {
									list.add(dataCenter.getString(Resources.TEXT_WATCH));
								}
								buttonInfo = "It's free and no channel info";
							} else if (title.getStart().before(new Date())) {
								list.add(dataCenter.getString(orderBundle ? Resources.TEXT_ORDER_TITLE : Resources.TEXT_ORDER));
								buttonInfo = "Pay and orderable";
							} else {
								buttonInfo = "Pay, " + title.getStart();
							}
						}
					}
					Log.printDebug("getActionButton, e = " + title.toString(true) + ", " + buttonInfo);
				}
				if (title instanceof Episode == false && title.inBundle()
						&& !(this instanceof BundleDetailUI)) {
					list.add(dataCenter.getString(Resources.TEXT_SEE_BUNDLE));
				}
				if (title.hasTrailer()) {
					if (!MenuController.getInstance().isAdultBlocked(title)) {
						list.add(dataCenter.getString(Resources.TEXT_SEE_TRAILER));
					}
				}
				
				// R5 add
				if (this instanceof TitleDetailUI && ((TitleDetailUI) this).needSimilarContentButton()) {
					if (title.getTypeID().equalsIgnoreCase("VT_100") || title.getTypeID().equalsIgnoreCase("VT_102")
							|| title.getTypeID().equalsIgnoreCase("VT_104") || title.getTypeID().equalsIgnoreCase("VT_124")) {
						list.add(dataCenter.getString(Resources.TEXT_SIMILAR_CONTENT));
					}
				}
				// R5 comment out - 2015.04.15 by zestyman
//				if (CommunicationManager.getInstance().hasAnyPackage() && 
//						CommunicationManager.getInstance().checkPackage(title.service) && !isAvailableService) {
//					if (BookmarkManager.getInstance().isInWishlist(title) != null) {
//						list.add(dataCenter
//								.getString(BookmarkManager.getInstance().isAdult(title) ? Resources.TEXT_REMOVE_FROM_WISHLIST_18 : Resources.TEXT_REMOVE_FROM_WISHLIST));
//					} else {
//						list.add(dataCenter
//								.getString(BookmarkManager.getInstance().isAdult(title) ? Resources.TEXT_ADDTO_WISHLIST_18 : Resources.TEXT_ADDTO_WISHLIST));
//					}
//				}
			} else if (element instanceof Series) {
				if (MenuController.getInstance().isBlocked((MoreDetail) element)) {
					list.add(dataCenter.getString(Resources.TEXT_UNBLOCK));
				} else {
					list.add(dataCenter.getString(Resources.TEXT_SELECT_SERIES));
				}
			} else if (element.equals(Season.MORE_CONTENT)) {
				list.add(dataCenter.getString(Resources.TEXT_SEE_MORE_CONTENT));
			} else if (element instanceof AvailableSource) {
				AvailableSource available = (AvailableSource) element;
				Season season = available.parent;
				if (season != null) {
					if (MenuController.getInstance().isBlocked(season)) {
						list.add(dataCenter.getString(Resources.TEXT_UNBLOCK));
					} else {
						boolean useBradingDeco = menu.useBradingDeco();
						
						Log.printDebug("current availableSource=" + available.toString(true));
						Log.printDebug("useBradingDeco=" + useBradingDeco);
						
						if (useBradingDeco && available.service != null && available.service.description != null) {
							
							if (CommunicationManager.getInstance().checkPackage(available.service)) {
								list.add(dataCenter.getString(Resources.TEXT_WATCH_IN_CLUB));
							} else {
								list.add(dataCenter.getString(Resources.TEXT_GOTO_CLUB));
							}
						} else if (useBradingDeco && (available.channel != null || available.network != null)) {
							list.add(dataCenter.getString(Resources.TEXT_GOTO_CHANNEL));
						} else {
							list.add(dataCenter.getString(Resources.TEXT_SELECT_SEASON) + " " + (season).seasonNumber);
						}
					}
				}
			} else if (element instanceof Season) {
				if (MenuController.getInstance().isBlocked((MoreDetail) element)) {
					list.add(dataCenter.getString(Resources.TEXT_UNBLOCK));
				} else {
					list.add(dataCenter.getString(Resources.TEXT_SELECT_SEASON) + " " + ((Season) element).seasonNumber);
				}
			} else if (element instanceof CategorizedBundle) {
				list.add(dataCenter.getString(Resources.TEXT_SELECT));
			} else if (element instanceof Extra) {
				list.add(dataCenter.getString(Resources.TEXT_SELECT_THIS_CONTENT));
			} else if (element instanceof Section) {
				list.add(dataCenter.getString(Resources.TEXT_SELECT_THIS_CONTENT));
			} else if (element instanceof CategoryContainer) {
				list.add(dataCenter.getString(Resources.TEXT_SELECT_THIS_CONTENT));
			} else if (element instanceof Bundle) {
				Bundle bundle = (Bundle) element;
				if (this instanceof ListViewUI && ((ListViewUI) this).isResumeViewing()) {
					if (bundle.getTitles().length > 0 && bundle.getTitles()[0] instanceof Episode) {
						list.add(dataCenter.getString(Resources.TEXT_VIEW_EPISODES));
					} else {
						list.add(dataCenter.getString(Resources.TEXT_VIEW_BUNDLE_ITEMS));
					}
				} else if (MenuController.getInstance().isBlocked(bundle)) {
					list.add(dataCenter.getString(Resources.TEXT_UNBLOCK));
					buttonInfo = "config = " + DataCenter.getInstance().getString(RightFilter.BLOCK_BY_RATINGS) + ", title = "
							+ bundle.getRating();
				} else if (!(this instanceof BundleDetailUI) && bundle.isFree() && bundle.hasPacakgeForServiceConfigType()) {
					//list.add(dataCenter.getString(Resources.TEXT_WATCH));
					// VDTRMASTER-5508
					if (bundle.isMovieBundle()) {
						list.add(dataCenter.getString(Resources.TEXT_SELECT_A_MOVIE));
					} else {
						list.add(dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE));
					}
					buttonInfo = "It's free";
				} else if (!(this instanceof BundleDetailUI) && BookmarkManager.getInstance().isPurchased(bundle)) {
					//list.add(dataCenter.getString(Resources.TEXT_WATCH));
					// VDTRMASTER-5508
					if (bundle.isMovieBundle()) {
						list.add(dataCenter.getString(Resources.TEXT_SELECT_A_MOVIE));
					} else {
						list.add(dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE));
					}
					buttonInfo = "Already purchased";
				} else if (CommunicationManager.getInstance().checkPackage(bundle.service) == false
						|| !bundle.hasPacakgeForServiceConfigType()) {
					if (CommunicationManager.getInstance().hasAnyPackage()) {
						list.add(dataCenter.getString(Resources.TEXT_SUBSCRIBE_SVOD) + bundle.getServiceTitle());
						buttonInfo = "No " + bundle.dumpService();
					} else {
						buttonInfo = "Err or No vod_category_config";
					}
				} else {
					if (this instanceof BundleDetailUI) {
						// movie bundle
						if (!BookmarkManager.getInstance().isPurchased(bundle)) {
							list.add(dataCenter.getString(Resources.TEXT_ORDER_BUNDLE));
							buttonInfo = "Order in Bundle detail screen";
						}
					} else if (bundle.bundleDetail.episode.length > 0) {
						// episode bundle
						list.add(DataCenter.getInstance().getString(Resources.TEXT_ORDER_EPISODES) + " "
								+ bundle.getEpisodeNumberString(false));
						buttonInfo = "Order by bundle of episodes";
					}
				}
				Log.printDebug("getActionButton, e = " + bundle.toString(true) + ", " + buttonInfo);
				if (bundle.isMovieBundle() && this instanceof BundleDetailUI == false) {
					list.add(dataCenter.getString(Resources.TEXT_SEE_BUNDLE));
				}
				// R5 comment out - 2015.04.15 by zestyman
//				if (this instanceof ListViewUI == false || ((ListViewUI) this).isResumeViewing() == false) {
//					if (CommunicationManager.getInstance().hasAnyPackage()
//							&& CommunicationManager.getInstance().checkPackage(bundle.service)) {
//						if (BookmarkManager.getInstance().isInWishlist(bundle) != null) {
//							list.add(dataCenter
//									.getString(BookmarkManager.getInstance().isAdult(bundle) ? Resources.TEXT_REMOVE_FROM_WISHLIST_18 : Resources.TEXT_REMOVE_FROM_WISHLIST));
//						} else {
//							list.add(dataCenter
//									.getString(BookmarkManager.getInstance().isAdult(bundle) ? Resources.TEXT_ADDTO_WISHLIST_18 : Resources.TEXT_ADDTO_WISHLIST));
//						}
//					}
//				}
			}

            Log.printDebug("getActionButton, buttonInfo " + buttonInfo);
	
			boolean canPlayAll = false;
			String label = "";
			if (this instanceof ListOfTitlesUI && ((ListOfTitlesUI) this).canPlayAll()) {
				// VDTRMASTER-5940
				canPlayAll = (!addedFreeviewButton && list.get(0).toString()
						.indexOf(dataCenter.getString(Resources.BTN_FREE_WATCH_NOW)) > -1)
						|| dataCenter.getString(Resources.TEXT_WATCH).equals(list.get(0))
						|| dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE).equals(list.get(0))
						|| dataCenter.getString(Resources.TEXT_SELECT_A_MOVIE).equals(list.get(0)); // VDTRMASTER-5540
				label = ((ListOfTitlesUI) this).findLabel();
	//			if (canPlayAll && element instanceof Bundle && BookmarkManager.getInstance().isPurchased(((Bundle) element)) == false) {
	//				canPlayAll = false;
	//			}
			} else if (this instanceof ListViewUI && ((ListViewUI) this).canPlayAll() && ((ListViewUI) this).isNormal()) {
				// VDTRMASTER-5940
				canPlayAll = (!addedFreeviewButton && list.get(0).toString()
						.indexOf(dataCenter.getString(Resources.BTN_FREE_WATCH_NOW)) > -1)
						|| dataCenter.getString(Resources.TEXT_WATCH).equals(list.get(0))
						|| dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE).equals(list.get(0))
						|| dataCenter.getString(Resources.TEXT_SELECT_A_MOVIE).equals(list.get(0)); // VDTRMASTER-5540
				label = ((ListViewUI) this).findLabel();
			} else if (this instanceof BundleDetailUI) {
				canPlayAll = ((BundleDetailUI) this).canPlayAll();
				if (canPlayAll) {
					label = ((BundleDetailUI) this).findLabel();
				}
			} else if (this instanceof TitleDetailUI && menu.isGoToAsset() == false) {
				BaseUI last = menu.getLastScene();
				BaseElement[] data = null;
				if (last != null) {
					if (last instanceof ListOfTitlesUI || last instanceof ListViewUI) {
						String[] actions = last.getActionButton(element);
						for (int i = 0; i < actions.length; i++) {
							if (actions[i].startsWith(dataCenter.getString(Resources.TEXT_WATCH_ALL))) {
								canPlayAll = true;
								data = last instanceof ListOfTitlesUI 
										? ((ListOfTitlesUI) last).getData() 
												: ((ListViewUI) last).getData();
								break;
							}
						}
					} else if (last instanceof WallofPostersFilmInfoUI) {
						canPlayAll = ((WallofPostersFilmInfoUI) last).canPlayAll();
						data = ((WallofPostersFilmInfoUI) last).getData();
					} else if (last instanceof CarouselViewUI) {
						canPlayAll = ((CarouselViewUI) last).canPlayAll();
						data = ((CarouselViewUI) last).getData();
					}
					if (canPlayAll && label.length() == 0 && data != null) {
						String[] actions = last.getActionButton(element);
						// VDTRMASTER-5940
						canPlayAll = (!addedFreeviewButton && list.get(0).toString()
								.indexOf(dataCenter.getString(Resources.BTN_FREE_WATCH_NOW)) > -1)
								|| dataCenter.getString(Resources.TEXT_WATCH).equals(list.get(0))
								|| dataCenter.getString(Resources.TEXT_SELECT_AN_EPISODE).equals(list.get(0))
								|| dataCenter.getString(Resources.TEXT_SELECT_A_MOVIE).equals(list.get(0)); // VDTRMASTER-5540
						label = findLabel(data);
					}
				}
			}
			if (canPlayAll) {
				// R5 comment out - 2015.04.15 by zestyman
//				if (list.size() == 1 && (list.get(0).equals(dataCenter.getString(Resources.TEXT_REMOVE_FROM_WISHLIST))
//						|| list.get(0).equals(dataCenter.getString(Resources.TEXT_REMOVE_FROM_WISHLIST_18)) 
//						|| list.get(0).equals(dataCenter.getString(Resources.TEXT_ADDTO_WISHLIST_18)) 
//						|| list.get(0).equals(dataCenter.getString(Resources.TEXT_ADDTO_WISHLIST))
//						)) {
//					list.add(0, dataCenter.getString(Resources.TEXT_WATCH_ALL) + label);
//				} else {
//					list.add(1, dataCenter.getString(Resources.TEXT_WATCH_ALL) + label);
//				}
				if (list.size() > 0) {
					list.add(1, dataCenter.getString(Resources.TEXT_WATCH_ALL) + label);
				} else {
					list.add(0, dataCenter.getString(Resources.TEXT_WATCH_ALL) + label);
				}

			}
		}
		String[] ret = new String[list.size()];
		list.toArray(ret);
		return ret;
	}
	
	public String findLabel(BaseElement data) {
		if (Log.DEBUG_ON) {
			Log.printDebug("BaseUI, findLabel(), data=" + data);
		}
		if (data == null) {
			return dataCenter.getString(Resources.TEXT_LBL_CONTENT);
		}
		
		if (data instanceof Bundle) {
			return dataCenter.getString(Resources.TEXT_LBL_ORDER_EPISODE_PACAKGE);
		}
		
		if (data instanceof Episode) {
			return dataCenter.getString(Resources.TEXT_LBL_EPISODE2);
		}
		if (data instanceof Video) {
			
			if (Log.DEBUG_ON) {
				Log.printDebug("BaseUI, findLabel(), video id=" + ((Video) data).getTypeID());
			}
			if ("VT_124".equals(((Video) data).getTypeID())) {
				return dataCenter.getString(Resources.TEXT_LBL_TRAILER);
			}
			if ("VT_104".equals(((Video) data).getTypeID())) {
			    // VDTRMASTER-6136
				return dataCenter.getString(Resources.TEXT_LBL_EXTRAS);
			}
			if ("VT_100".equals(((Video) data).getTypeID())) {
				return dataCenter.getString(Resources.TEXT_LBL_EPISODE2);
			}
			if ("VT_102".equals(((Video) data).getTypeID())) {
				return dataCenter.getString(Resources.TEXT_LBL_MOVIE2);
			}
		}
		return dataCenter.getString(Resources.TEXT_LBL_CONTENT);
	}
	
	// // VDTRMASTER-5214
	public String findPlayLabel(BaseElement data) {
		if (data == null) {
			return dataCenter.getString(Resources.TEXT_LBL_CONTENT3);
		}
		
		boolean isMixed = false;
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		if (cur instanceof ListOfTitlesUI) {
			isMixed = ((ListOfTitlesUI) cur).isMixedContents();
		} else if (cur instanceof ListViewUI) {
			isMixed = ((ListViewUI) cur).isMixedContents();
		} else if (cur instanceof BundleDetailUI) {
			isMixed = ((BundleDetailUI) cur).isMixedContents();
		}
		
		if (isMixed) {
			return dataCenter.getString(Resources.TEXT_LBL_CONTENT3);
		}
		
		if (data instanceof Bundle) {
			return dataCenter.getString(Resources.TEXT_LBL_ORDER_EPISODE_PACAKGE);
		}
		
		if (data instanceof Episode) {
			return dataCenter.getString(Resources.TEXT_LBL_EPISODE3);
		}
		if (data instanceof Video) {
			if ("VT_124".equals(((Video) data).getTypeID())) {
				return dataCenter.getString(Resources.TEXT_LBL_TRAILER);
			}
			if ("VT_104".equals(((Video) data).getTypeID())) {
				return dataCenter.getString(Resources.TEXT_LBL_EXTRA3);
			}
			if ("VT_100".equals(((Video) data).getTypeID())) {
				return dataCenter.getString(Resources.TEXT_LBL_EPISODE3);
			}
			if ("VT_102".equals(((Video) data).getTypeID())) {
				return dataCenter.getString(Resources.TEXT_LBL_MOVIE3);
			}
		}
		return dataCenter.getString(Resources.TEXT_LBL_CONTENT3);
	}
	
	public String findLabel(BaseElement[] data) {
		boolean sameType = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null && data[i] instanceof Episode == false) {
				sameType = false;
				break;
			}
		}
		if (sameType) {
			return dataCenter.getString(Resources.TEXT_LBL_EPISODES2);
		}
		
		sameType = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null && (data[i] instanceof Video == false || "VT_124".equals(((Video) data[i]).getTypeID()) == false)) {
				sameType = false;
				break;
			}
		}
		if (sameType) {
			return dataCenter.getString(Resources.TEXT_LBL_TRAILERS);
		}
		
		sameType = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null && (data[i] instanceof Video == false || "VT_104".equals(((Video) data[i]).getTypeID()) == false)) {
				sameType = false;
				break;
			}
		}
		if (sameType) {
			return dataCenter.getString(Resources.TEXT_LBL_EXTRAS);
		}
		
		sameType = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null && (data[i] instanceof Video == false || data[i] instanceof Episode == false || "VT_100".equals(((Video) data[i]).getTypeID()) == false)) {
				sameType = false;
				break;
			}
		}
		if (sameType) {
			return dataCenter.getString(Resources.TEXT_LBL_EPISODES2);
		}
		
		sameType = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null && (data[i] instanceof Video == false || "VT_102".equals(((Video) data[i]).getTypeID()) == false)) {
				sameType = false;
				break;
			}
		}
		if (sameType) {
			return dataCenter.getString(Resources.TEXT_LBL_MOVIES2);
		}
		
		return dataCenter.getString(Resources.TEXT_LBL_CONTENTS);
	}

	/**
	 * It will be called when PIN enabler is closed.
	 *
	 * @param result
	 */
	public void receiveCheckRightFilter(CheckResult result) {

	}

	/**
	 * It will be called when ErrorMessage popup is closed.
	 *
	 * @param buttonType
	 */
	public void actionPerformed(int buttonType) throws RemoteException {

	}

	public void timerWentOff(TVTimerWentOffEvent arg0) {
	}

	public void scroll() {
		if (scrollBounds.width == 0) {
			return;
		}
		scroll++;
		repaint(scrollBounds.x, scrollBounds.y, scrollBounds.width, scrollBounds.height);
	}
	
	// R5
	public boolean fromCategory() {
		return fromCategory;
	}

	// Bundling
	public boolean processFreeview(ArrayList list, Video video) {
		if (video.isFreeview) {
			list.add("!" + dataCenter.get(Resources.BTN_FREE_WATCH_NOW));
			return true;
		}

		return false;
	}

	// R7.3
	public boolean isSubscribed(String[] actions) {
		if (Log.DEBUG_ON) {
			Log.printDebug("BaseUI, isSubscribed, actions=" + actions);
		}

		if (actions == null || actions.length == 0) {
			if (Log.DEBUG_ON) {
				if (actions == null) {
					Log.printDebug("BaseUI, isSubscribed, return true");
				} else {
					Log.printDebug("BaseUI, isSubscribed, actions.length=" + actions.length + ", return true");
				}

			}
			return true;
		}

		for (int i = 0; i < actions.length; i++) {
			String actionMenu = actions[i];
			if (Log.DEBUG_ON) {
				Log.printDebug("BaseUI, isSubscribed, actionMenu=" + actionMenu);
			}
			if (actionMenu.indexOf(dataCenter.getString(Resources.TEXT_SUBSCRIBE)) > -1
					|| actionMenu.indexOf(dataCenter.getString(Resources.TEXT_SUBSCRIBE_SVOD)) > -1
					|| dataCenter.getString(Resources.TEXT_CHANGE_MY_PKG).equals(actionMenu)
					|| dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG).equals(actionMenu)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("BaseUI, isSubscribed, return false");
				}
				return false;
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("BaseUI, isSubscribed, after check actions, return true");
		}

		return true;
	}

	// R7.3
	public boolean hasWatchAll(String[] actions) {
		if (Log.DEBUG_ON) {
			Log.printDebug("BaseUI, hasWatchAll, actions=" + actions);
		}

		if (actions == null || actions.length == 0) {
			if (Log.DEBUG_ON) {
				if (actions == null) {
					Log.printDebug("BaseUI, hasWatchAll, return false");
				} else {
					Log.printDebug("BaseUI, hasWatchAll, actions.length=" + actions.length + ", return false");
				}

			}
			return false;
		}

		for (int i = 0; i < actions.length; i++) {
			String actionMenu = actions[i];
			if (Log.DEBUG_ON) {
				Log.printDebug("BaseUI, hasWatchAll, actionMenu=" + actionMenu);
			}
			if (actionMenu.startsWith(dataCenter.getString(Resources.TEXT_WATCH_ALL))) {
				if (Log.DEBUG_ON) {
					Log.printDebug("BaseUI, hasWatchAll, return true");
				}
				return true;
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("BaseUI, hasWatchAll, after check actions, return false");
		}

		return false;
	}
}

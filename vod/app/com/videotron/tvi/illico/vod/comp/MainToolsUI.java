package com.videotron.tvi.illico.vod.comp;

import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.effect.ArrowEffect;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.gui.MainToolsRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

public class MainToolsUI extends BaseUI {
	private static final long serialVersionUID = 6204306236361648185L;
	private int type;
	private int maxCnt;
	private boolean hasSeeAll;
	private MainToolsRenderer toolsRenderer = new MainToolsRenderer();
	private ArrowEffect arrowEffect;

	public MainToolsUI() {
		super();
		arrowEffect = new ArrowEffect(this, 0);
	}

	public void start(boolean back) {
		setRenderer(toolsRenderer);
		prepare();
	}

	public void setData(CategoryContainer category) {
		type = category.type;
		toolsRenderer.setData(category);
		if (type == MenuController.MENU_ABOUT) {
			maxCnt = 1;
		}
		// repaint();
		
		// R5
		focus = 0;
	}

	public int getType() {
		return type;
	}

	public void setMaxCnt(int maxCnt) {
		this.maxCnt = maxCnt;
	}

	public void setSeeAll(boolean seeAll) {
		this.hasSeeAll = seeAll;
	}

	public boolean handleKey(int key) {
		Log.printDebug("MainToolUI, handleKey(), code = " + key);
		boolean ret = true;
		switch (key) {
		case KeyEvent.VK_LEFT:
			Rectangle arrowL = new Rectangle();
			arrowL.x = toolsRenderer.arrowL.x - getX();
			arrowL.y = toolsRenderer.arrowL.y - getY();
//			arrowEffect.start(arrowL, ArrowEffect.LEFT);
			if (focus == 0) {
				((CategoryUI) getParent())
						.setCurrentFocusUI(CategoryUI.FOCUS_MENU);
				// 20190222 Touche project
				VbmController.getInstance().backwardHistory();
			} else {
				focus--;
				repaint();
			}
			break;
		case KeyEvent.VK_RIGHT:
			if (focus < maxCnt - 1) {
				Rectangle arrowR = new Rectangle();
				arrowR.x = toolsRenderer.arrowR.x - getX();
				arrowR.y = toolsRenderer.arrowR.y - getY();
//				arrowEffect.start(arrowR, ArrowEffect.RIGHT);
				focus++;
				repaint();
			}
			break;
		case HRcEvent.VK_ENTER:
		case HRcEvent.VK_INFO:
			if (type == MenuController.MENU_KARAOKE || type == MenuController.MENU_KARAOKE_PACK) {
				if (CategoryUI.getPurchasedPartyPack() != null) { // select
					CategoryUI.getInstance().moveToKaraokePackage(true);
				} else {
					switch (focus) {
					case 0: // try
						CategoryUI.getInstance().moveToKaraokePackage(false);
						break;
					case 1: // order
//						if (EventQueue.isDispatchThread()) {
//							try {
//								CommunicationManager.getInstance().requestTuner(null, null);
//								return true;
//							} catch (NullPointerException npe) {}
//						} // 4691
						menu.purchaseKaraoke(false);
						break;
					case 2: // preview
						CategoryUI.getInstance().moveToKaraokePackage(true);
						break;
					}
				}
			} else if (type == MenuController.MENU_ABOUT) {
				Channel ch = CategoryUI.getChannel();
				if (ch != null) {
					if (ch.hasAuth) { // tune
						Log.printDebug("MainToolsUI, ch.hasAuth, " + ch.toString(true));
						VbmController.getInstance().writeSelectionAddHistory("Tune in to channel", null);
						MenuController.getInstance().exit(MenuController.EXIT_TO_CHANNEL, ch);
					} else if (CommunicationManager.getInstance().isaSupported(ch.callLetters)
							|| !CommunicationManager.getInstance().isIncludedInExcludedData(ch.callLetters)) { // isa
						Log.printDebug("MainToolsUI, ISA, " + ch.toString(true));
						//R7.4 VBM measurement changed
						// Touche : VDTRMASTER-6251
						VbmController.getInstance().writeSelection("Channel Subscribe");						
						//VbmController.getInstance().writeSelection("Subscribe to this channel");
						CommunicationManager.getInstance().showISA(ch.callLetters, ISAService.ENTRY_POINT_VOD_E01);
					} else { // cyo
						Log.printDebug("MainToolsUI, not ISA, " + ch.toString(true));
						//R7.4 VBM measurement changed
						// Touche : VDTRMASTER-6251
						VbmController.getInstance().writeSelection("Channel Subscribe");											
						//VbmController.getInstance().writeSelection("Add This channel(Change my package)");
						CommunicationManager.getInstance().showISA(ch.callLetters, ISAService.ENTRY_POINT_VOD_E01);
						//CommunicationManager.getInstance().launchCYO();
					}
				}
			} else {
				MoreDetail[] data = ((MainToolsRenderer) renderer).getData();
				if (data != null ) {
					if (focus == maxCnt - 1 && hasSeeAll) { // See all
						((CategoryUI) getParent()).keyOkOnMenu(false);
					} else if (data[focus] instanceof CategorizedBundle) { // karaoke on resume viewing
						CategoryUI.getInstance().moveToKaraokePackage(true);
					} else if (data[focus] instanceof Bundle 
							&& (type == MenuController.MENU_RESUME || type == MenuController.MENU_RESUME_ADULT)) {
						MenuController.getInstance().goToResumeViewing(data[focus], 
								type == MenuController.MENU_RESUME_ADULT);
					} else {
						MenuController.getInstance().goToDetail(data[focus], null);
					}
				}
			}
			break;
		default:
			ret = false;
		}
		return ret;
	}

	public CategoryContainer getCurrentCategory() {
		return ((CategoryUI) getParent()).getCurrentCategory();
	}
	
	public BaseElement getCurrentCatalogue() {
		try {
			if (type == MenuController.MENU_RESUME || 
					type == MenuController.MENU_RESUME_ADULT
					|| type == MenuController.MENU_WISHLIST
					|| type == MenuController.MENU_WISHLIST_ADULT) {
				
				if (focus == maxCnt - 1 && hasSeeAll) { // see all
					return null;
				}
				return ((MainToolsRenderer) renderer).getData()[focus];
			}
		} catch (Exception e) {}
		
		return null;
	}

	public void stop() {
		if (Log.DEBUG_ON) {
			Log.printDebug("MainToolsUI : stop");
		}
	}
}

/**
 * 
 */
package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Hashtable;

import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBBufferedImage;
import org.dvb.ui.DVBColor;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;
import org.havi.ui.event.HRcEvent;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.similar.SimilarContentManager;
import com.videotron.tvi.illico.vod.data.similar.type.SError;
import com.videotron.tvi.illico.vod.data.similar.type.SHead;
import com.videotron.tvi.illico.vod.data.similar.type.SimilarContent;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CacheElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Service;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.UITemplateList;

/**
 * @author zestyman
 *
 */
public class SimilarContentView extends UIComponent implements Runnable {

	public static final int MODE_INTRO = 0;
	public static final int MODE_OPEN = 1;
	public static final int MODE_CLOSE = 2;
	
	private final int NORMAL_FRAMES = 12;
	private final int INTRO_FRAMES = 8;
	
	private final long STEP_DELAY = 40;
	private ActionBox actionBox;
	private boolean isAnimating;
	private int mode;
	private boolean hasFocus;

	private SimilarContent[] contents;
	private boolean[] isFavorites = new boolean[5];
	private boolean[] useTitles = new boolean[5];
	private String[] shortenTitles = new String[5];
	private String[] shortenChannelNames = new String[5];
	private SHead head;

	private DVBBufferedImage background;
	private DVBBufferedImage contentDVBImage;
	private Toolkit toolKit = Toolkit.getDefaultToolkit();

	private Rectangle clipBounds = new Rectangle(46, 291, 558, 192);

	private DVBAlphaComposite[] openAlpha;
	private DVBAlphaComposite[] closeAlpha;

	private int[] openPosY;
	private int[] closePosY;
	private int[] introPosY;
	private int[] landOpenPosY;
	private int[] landClosePosY;
	

	private int frames;
	private int frameCount;

	private Footer footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
	private int[] buttonList = new int[] { BaseUI.BTN_PROFILE, BaseUI.BTN_WISHLIST };
	private boolean hasProfileButton;
	private String profileBtnStr;

	public SimilarContentView() {

		renderer = new SimilarContentRenderer();

		setRenderer(renderer);

		if (background == null) {
			SharedMemory sm = SharedMemory.getInstance();
			synchronized (sm) {
				background = getFullScreenImage(sm, "DVBBufferedImage.1");
			}
		}
		
		if (contentDVBImage == null) {
			SharedMemory sm = SharedMemory.getInstance();
			synchronized (sm) {
				contentDVBImage = getFullScreenImage(sm, "DVBBufferedImage.SIMILAR");
			}
		}

		openAlpha = new DVBAlphaComposite[NORMAL_FRAMES];
		for (int i = 0; i < NORMAL_FRAMES; i++) {
			if (i == 0) {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.25f);
			} else if (i == 1) {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.5f);
			} else if (i == 2) {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.82f);
			} else {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 1f);
			}

		}

		closeAlpha = new DVBAlphaComposite[NORMAL_FRAMES];
		for (int i = 0; i < NORMAL_FRAMES; i++) {
			if (i == 10) {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.5f);
			} else if (i == 11) {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.25f);
			} else if (i == 9) {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.82f);
			} else {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 1f);
			}

		}

		int fcs = (NORMAL_FRAMES - 1) * (NORMAL_FRAMES - 1);
		int openDiffY = 291 - 437;
		int closeDiffY = 437 - 291;

		openPosY = new int[NORMAL_FRAMES];
		closePosY = new int[NORMAL_FRAMES];
		landOpenPosY = new int[NORMAL_FRAMES]; 
		landClosePosY = new int[NORMAL_FRAMES]; 
		introPosY = new int[INTRO_FRAMES];

		for (int i = 0; i < NORMAL_FRAMES; i++) {
			openPosY[i] = 437 + Math.round((float) openDiffY * i * i / fcs);
			closePosY[i] = 291 + Math.round((float) closeDiffY * i * i / fcs);
			landOpenPosY[i] = Math.round((float) 33 * i * i / fcs);
			landClosePosY[i] = landOpenPosY[i] - 33;
		}
		
		fcs = (INTRO_FRAMES - 1) * (INTRO_FRAMES - 1);
		int introDiffY = 437 - 482;
		for (int i = 0; i < INTRO_FRAMES; i++) {
			introPosY[i] = 482 + Math.round((float) introDiffY * i * i / fcs);
		}
		
		footer.setBounds(10, 488, 810, 24);
		footer.setFont(FontResource.DINMED.getFont(15));
		add(footer);
		setVisible(true);
	}

	public void init() {
		mode = MODE_INTRO;
		hasFocus = false;
		contents = null;
		head = null;
		// VDTRMASTER-5499
		footer.reset();
		prepare();
	}

	public void startAnimation(int mode) {
		this.mode = mode;
		
		if (actionBox != null) {
			if (mode == MODE_OPEN && contents != null) {
				actionBox.setDimmedBg(true);
			}
		}
		if (!hasContents()) {
			animationEnded();
			return;
		}

		if (EventQueue.isDispatchThread()) {
			run();
		} else {
			EventQueue.invokeLater(this);
		}
	}

	public void setHasFocus(boolean focus) {
		hasFocus = focus;
		if (contents != null) {
			this.focus = contents.length - 1;

			if (hasFocus) {
				updateFooter();
			} else {
				footer.reset();
			}
		}
	}

	public boolean hasFocus() {
		return hasFocus;
	}

	public void animationStarted() {
		setVisible(false);
		
		if (mode == MODE_INTRO) {
			frames = INTRO_FRAMES;
		} else {
			frames = NORMAL_FRAMES;
		}
		frameCount = frames - 1;
	}

	public void animationEnded() {
		if (actionBox != null) {
			if (mode != MODE_OPEN || !hasContents()) {
				actionBox.setDimmedBg(false);
			}
		}
		setVisible(true);
	}

	public boolean isAnimating() {
		return isAnimating;
	}

	public boolean isOpened() {
		return mode == MODE_OPEN;
	}

	public void resetScroll() {
		BaseUI parent = (BaseUI) getParent();
		parent.scroll = 0;
	}

	public void updateFooter() {
		String tmpLbl = null;
		Image image = null;
		footer.reset();
		hasProfileButton = false;
		BaseElement element = (BaseElement) CatalogueDatabase.getInstance().getCached(contents[focus].getVodId());
		Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
		for (int i = 0; i < buttonList.length; i++) {
			tmpLbl = null;
			switch (buttonList[i]) {
			case BaseUI.BTN_WISHLIST:
				tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_MY_WISHLIST);
				
				// VDTRMASTER-5498
				boolean isAdult = false;
				if (element != null && element instanceof Orderable) {
					isAdult = BookmarkManager.getInstance().isAdult((Orderable)element);
				}
				
				if (hasChannelName()) {
					isAdult = false;
				}
				
				if (isAdult) {
					tmpLbl = DataCenter.getInstance().getString(Resources.TEXT_MY_WISHLIST_18);
				}

				int wishListSize = 0;
				try {
					wishListSize = isAdult ? BookmarkManager.getInstance().retrieveWishlistTitlesAdult().length
							: BookmarkManager.getInstance().retrieveWishlistTitles(true).length;
				} catch (NullPointerException e) {
					// retrieveWishlistTitlesAdult may return null
				}

				tmpLbl = tmpLbl + " (" + wishListSize + ")";
				image = (Image) imgTable.get(PreferenceService.BTN_B);
				break;
			case BaseUI.BTN_PROFILE:
				tmpLbl = ((BaseUI) getParent()).getWishButtonName(element);
				if (tmpLbl == null) {
					continue;
				}
				profileBtnStr = tmpLbl;
				image = (Image) imgTable.get(PreferenceService.BTN_PRO);
				hasProfileButton = true;
				break;
			}
			footer.addButtonWithLabel(image, tmpLbl);
		}
		
		updateFavoriteData();
	}

	public void setElements(SimilarContent[] contents) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContent, setElements, contents=" + contents);
		}
		this.contents = contents;
		// VDTRMASTER-5499
		footer.reset();
		if (contents == null || contents.length == 0) {
			return;
		}

		ArrayList videoList = new ArrayList();
		for (int i = 0; i < contents.length; i++) {
			if (contents[i].getVodId().startsWith("V_")) {
				CacheElement cache = CatalogueDatabase.getInstance().getCached(contents[i].getVodId());
				if (cache == null) {
					videoList.add(contents[i].getVodId());
				}
			}
		}

		if (videoList.size() > 0) {
			synchronized (CatalogueDatabase.getInstance()) {
				CatalogueDatabase.getInstance().retrieveVideos(null, videoList, null);

				try {
					CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
				} catch (InterruptedException e) {
				}
			}
		}
		updateFavoriteData();
	}
	
	public boolean hasContents() {
		if (contents != null && contents.length > 0) {
			return true;
		}
		return false;
	}
	
	public boolean needButton() {
		if (contents != null || head != null) {
			return true;
		}
		return false;
	}

	public void updateFavoriteData() {
		FontMetrics fm16 = FontResource.getFontMetrics(FontResource.BLENDER.getFont(16));
		if (contents != null) {
			for (int i = 0; i < contents.length; i++) {
				isFavorites[i] = false;
				shortenChannelNames[i] = TextUtil.shorten(contents[i].getChannelName(), fm16, 87);
				if (contents[i].getVodId().startsWith("V_")) {
					CacheElement cache = CatalogueDatabase.getInstance().getCached(contents[i].getVodId());
					if (cache != null) {
						String result = BookmarkManager.getInstance().isInWishlist((Video) cache);
						if (result != null) {
							isFavorites[i] = true;
						}
					}
				}

				if (isFavorites[i]) {
					shortenTitles[i] = TextUtil.shorten(contents[i].getTitle(), fm16, 77);
				} else {
					shortenTitles[i] = TextUtil.shorten(contents[i].getTitle(), fm16, 87);
				}

				if (shortenTitles[i].length() > shortenChannelNames[i].length()) {
					useTitles[i] = true;
				} else {
					useTitles[i] = false;
				}

				if (Log.EXTRA_ON) {
					Log.printDebug("SimilarContentView, updateFavoriteData, isFavorites[" + i + "]=" + isFavorites[i]);
					Log.printDebug("SimilarContentView, updateFavoriteData, shortenChannelNames[" + i + "]="
							+ shortenChannelNames[i]);
					Log.printDebug("SimilarContentView, updateFavoriteData, shortenTitles[" + i + "]="
							+ shortenTitles[i]);
					Log.printDebug("SimilarContentView, updateFavoriteData, useTitles[" + i + "]=" + useTitles[i]);
				}
			}
		}
	}

	public void setHead(SHead h) {
		head = h;
	}

	public void setActionBox(ActionBox actionBox) {
		this.actionBox = actionBox;
	}

	public Footer getFooter() {
		return footer;
	}
	
	// R5
	// VDTRMASTER-5499
	public BaseElement getFocusedContent() {
		BaseElement el = (BaseElement) CatalogueDatabase.getInstance().getCached(contents[focus].getVodId());
		
		return el;
	}
	
	// R5
	// VDTRMASTER-5608
	public boolean hasChannelName() {
		if (contents[focus].getChannelName().length() > 0) {
			return true;
		}
		
		return false;
	}

	public boolean handleKey(int key) {

		switch (key) {
		case HRcEvent.VK_UP:
		case HRcEvent.VK_DOWN:
			return true;
		case HRcEvent.VK_LEFT:
			if (contents != null) {
				if (focus > 0) {
					focus--;
					resetScroll();
					updateFooter();
					repaint();
				}
			}
			return true;
		case HRcEvent.VK_RIGHT:
			if (contents != null) {
				if (focus < contents.length - 1) {
					focus++;
					updateFooter();
				} else {
					setHasFocus(false);
					actionBox.setDimmedFocus(false);
				}
				resetScroll();
				repaint();
			}
			return true;
		case HRcEvent.VK_ENTER:
			// VDTRMASTER-5579
			VbmController.getInstance().writeSimilarContent(contents[focus].getVodId());
			if (contents[focus].getVodId().startsWith("V_")) {
				BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(contents[focus].getVodId());
				if (cache != null) {
					synchronized (CatalogueDatabase.getInstance()) {
						if (((Video)cache).service.length > 0 && ((Video)cache).service[0].description == null) {
							CatalogueDatabase.getInstance().retrieveServices(null, ((Video)cache).service[0].id, ((Video)cache).service[0]);
							
							try {
								CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
							} catch (InterruptedException e) {
							}
							
							Service service = (Service) CatalogueDatabase.getInstance().getCached(((Video)cache).service[0].id);
							((Video)cache).service[0] = service;
						}
					}
					MenuController.getInstance().goToDetail(cache, null);
				} else {
					new Thread("LOAD_ViDEO_ON_SIMILAR") {
						public void run() {
							MenuController.getInstance().showLoadingAnimation();
							synchronized (CatalogueDatabase.getInstance()) {
								ArrayList videoList = new ArrayList();
								videoList.add(contents[focus].getVodId());
								CatalogueDatabase.getInstance().retrieveVideos(null, videoList, null);

								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
								
								BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(
										contents[focus].getVodId());
								
								if (((Video)cache).service.length > 0 && ((Video)cache).service[0].description == null) {
									CatalogueDatabase.getInstance().retrieveServices(null, ((Video)cache).service[0].id, ((Video)cache).service[0]);
									
									try {
										CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
									} catch (InterruptedException e) {
									}
									
									Service service = (Service) CatalogueDatabase.getInstance().getCached(((Video)cache).service[0].id);
									((Video)cache).service[0] = service;
								}
								
								MenuController.getInstance().hideLoadingAnimation();

								if (cache != null) {
									MenuController.getInstance().goToDetail(cache, null);
								}
							}
						};
					}.start();
				}
			} else if (contents[focus].getVodId().startsWith("SR_")) {
				BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(contents[focus].getVodId());

				if (cache != null) {
					MenuController.getInstance().goToDetail(cache, null);
				} else {
					new Thread("LOAD_SERIES_ON_SIMILAR") {
						public void run() {
							MenuController.getInstance().showLoadingAnimation();
							synchronized (CatalogueDatabase.getInstance()) {
								CatalogueDatabase.getInstance().retrieveSeries(null, contents[focus].getVodId(),
										contents[focus].getVodId());

								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
							}

							BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(
									contents[focus].getVodId());
							MenuController.getInstance().hideLoadingAnimation();
							if (cache != null) {
								MenuController.getInstance().goToDetail(cache, null);
							}
						};
					}.start();
				}
			} else if (contents[focus].getVodId().startsWith("SS_")) {
				BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(contents[focus].getVodId());

				if (cache != null) {
					MenuController.getInstance().goToDetail(cache, null);
				} else {
					new Thread("LOAD_SEASON_ON_SIMILAR") {
						public void run() {
							MenuController.getInstance().showLoadingAnimation();
							synchronized (CatalogueDatabase.getInstance()) {
								CatalogueDatabase.getInstance().retrieveSeries(null, contents[focus].getVodId(),
										contents[focus].getVodId());

								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
							}

							BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(
									contents[focus].getVodId());
							MenuController.getInstance().hideLoadingAnimation();
							if (cache != null) {
								MenuController.getInstance().goToDetail(cache, null);
							}
						};
					}.start();
				}
			} else if (contents[focus].getVodId().startsWith("CE_")) {
				BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(contents[focus].getVodId());
				
				if (cache != null && ((Extra) cache).extraDetail != null) {
					MenuController.getInstance().forwardHistory(cache);
					MenuController.getInstance().goToNextScene(UITemplateList.LIST_OF_TITLES, ((Extra) cache).id);
				} else {
					new Thread("LOAD_EXTRA_ON_SIMILAR") {
						public void run() {
							MenuController.getInstance().showLoadingAnimation();
							synchronized (CatalogueDatabase.getInstance()) {
								CatalogueDatabase.getInstance().retrieveExtra(null, contents[focus].getVodId(),
										contents[focus].getVodId());

								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
								} catch (InterruptedException e) {
								}
							}

							BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(
									contents[focus].getVodId());
							MenuController.getInstance().hideLoadingAnimation();
							if (cache != null) {
								MenuController.getInstance().forwardHistory(cache);
								MenuController.getInstance().goToNextScene(UITemplateList.LIST_OF_TITLES, ((Extra) cache).id);
							}
						};
					}.start();
				}
			}
			return true;
		case KeyCodes.LITTLE_BOY:
			if (hasProfileButton) {
				footer.clickAnimation(0);
				BaseElement cache = (BaseElement) CatalogueDatabase.getInstance().getCached(contents[focus].getVodId());
				MenuController.getInstance().processActionMenu(profileBtnStr, cache);
			}
			return true;
		case KeyCodes.COLOR_B:
			MenuController.getInstance().goToWishlist(true);
			return true;
		}

		return false;
	}

	private static DVBBufferedImage getFullScreenImage(SharedMemory sm, String key) {
		DVBBufferedImage buf = (DVBBufferedImage) sm.get(key);
		if (buf == null) {
			buf = new DVBBufferedImage(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			sm.put(key, buf);
		}
		return buf;
	}

	public void run() {
		animationStarted();

		frameCount = 0;
		// make parent buffer.
		DVBGraphics bGraphics = (DVBGraphics) background.createGraphics();
		getParent().paint(bGraphics);

		isAnimating = true;
		DVBGraphics pGraphics = (DVBGraphics) getParent().getGraphics();

		do {
			long start = System.currentTimeMillis();
			pGraphics.drawImage(background, 0, 0, getParent());

			paint(pGraphics);

			toolKit.sync();

			frameCount++;

			long end = System.currentTimeMillis();
			long diff = end - start;
			if (diff < STEP_DELAY) {
				try {
					Thread.sleep(STEP_DELAY - diff);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		} while (frameCount < frames);
		frameCount--;

		clear(bGraphics);
		clear(pGraphics);
		pGraphics.dispose();
		bGraphics.dispose();
		isAnimating = false;
		animationEnded();
	}

	private void clear(DVBGraphics g) {
		g.setClip(null);
		try {
			g.setDVBComposite(DVBAlphaComposite.Src);
		} catch (UnsupportedDrawingOperationException ex) {
			Log.print(ex);
		}
		g.clearRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		try {
			g.setDVBComposite(DVBAlphaComposite.SrcOver);
		} catch (UnsupportedDrawingOperationException ex) {
			Log.print(ex);
		}
	}

	class SimilarContentRenderer extends Renderer {

		private DVBColor bgColor = new DVBColor(17, 17, 17, 230);
		private Color focuseColor = new Color(249, 194, 0);
		private Color titleColor = new Color(224, 224, 224);
		private Color channelColor = new Color(174, 174, 174);
		private Color cDimmed102 = new DVBColor(0, 0, 0, 102);
		private Color similarBgColor = new Color (25, 25, 25);
		private Color lineColor = new Color(48, 48, 48);
		private Image wishIconImg, threeDIconImg, UHDIcon;
		private Image arrowLeftImg, arrowRightImg;
		private Image defaultImg;
		private Image i02ArsL, i02ArsLDim;

		private String errorCodeHeaderStr;
		private String noSimilarContent;

		public Rectangle getPreferredBounds(UIComponent c) {
			return Constants.SCREEN_BOUNDS;
		}

		public void prepare(UIComponent c) {
			wishIconImg = DataCenter.getInstance().getImage("icon_wish_s.png");
			threeDIconImg = DataCenter.getInstance().getImage("icon_3d.png");
			UHDIcon = DataCenter.getInstance().getImage("icon_uhd.png");
			arrowLeftImg = DataCenter.getInstance().getImage("02_ars_l_s.png");
			arrowRightImg = DataCenter.getInstance().getImage("02_ars_r_s.png");
			defaultImg = DataCenter.getInstance().getImage("VOD_default_S_Portrait.png");
			i02ArsL = DataCenter.getInstance().getImage("02_ars_l.png");
			i02ArsLDim = DataCenter.getInstance().getImage("02_ars_l_dim.png");

			errorCodeHeaderStr = DataCenter.getInstance().getString(Resources.TEXT_LBL_ERROR_CODE);
			noSimilarContent = DataCenter.getInstance().getString(Resources.TEXT_MSG_NO_SIMILAR_CONTENT);

		}

		protected void paint(Graphics g, UIComponent c) {
			if (isAnimating && mode != MODE_INTRO) {
				DVBGraphics gg = (DVBGraphics) g;

				DVBAlphaComposite origin = gg.getDVBComposite();
				try {
					if (mode == MODE_OPEN) {
						gg.setDVBComposite(openAlpha[frameCount]);
					} else {
						gg.setDVBComposite(closeAlpha[frameCount]);
					}
				} catch (UnsupportedDrawingOperationException e) {
					e.printStackTrace();
				}

				g.setColor(bgColor);
				g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

				try {
					gg.setDVBComposite(origin);
				} catch (UnsupportedDrawingOperationException e) {
					e.printStackTrace();
				}
			} else if (mode == MODE_OPEN && !isAnimating && hasContents()) {
				g.setColor(bgColor);
				g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
				
				if (!hasFocus && hasContents()) {
					g.drawImage(i02ArsL, 606, 442, c);
				}
			}
			
			if (hasContents() && !hasFocus && (mode != MODE_OPEN || isAnimating)) {
				g.drawImage(i02ArsLDim, 606, 442, c);
			}

			if (actionBox != null) {
				int moveX = actionBox.getBounds().x;
				int moveY = actionBox.getBounds().y;
				g.translate(moveX, moveY);
				actionBox.paint(g);
				g.translate(-moveX, -moveY);
			}


			// Rectangle originClip = g.getClipBounds();
			// g.setClip(clipBounds);

			if (contents != null && contents.length > 0) {

				paintContents(contentDVBImage, c);
				
				g.drawImage(contentDVBImage, 0, 0, c);
				
			} else if (mode == MODE_OPEN && head != null && head.getError() != null) {
				g.setColor(similarBgColor);
				g.fillRect(55, 388, 537, 87);
				g.setColor(lineColor);
				g.fillRect(55, 388, 537, 1);
				SError error = head.getError();
				g.setFont(FontResource.BLENDER.getFont(21));
				g.setColor(titleColor);

//					String message = TextUtil.shorten(error.getMessage(), g.getFontMetrics(), 435);
				String[] message = TextUtil.split(error.getMessage(), g.getFontMetrics(), 400, 2);
				
				int posY = 431 - (message.length - 1) * 22;
				if (message.length == 2) {
					posY += 8;
				}
				
				for(int i = 0; i < message.length; i++) {
					g.drawString(message[i], 125, posY + i * 22);
				}

				String errorCodeStr = errorCodeHeaderStr + " [" + head.getError().getCode() + "]";
				g.setFont(FontResource.BLENDER.getFont(16));
				g.drawString(errorCodeStr, 125, posY + message.length * 22);
			} else if (mode == MODE_OPEN) {
				g.setColor(similarBgColor);
				g.fillRect(55, 388, 537, 87);
				g.setColor(lineColor);
				g.fillRect(55, 388, 537, 1);
				// no similar content found.
				g.setFont(FontResource.BLENDER.getFont(21));
				g.setColor(titleColor);
				GraphicUtil.drawStringCenter(g, noSimilarContent, 338, 443);
			}
			
			// for debug
			if (Log.EXTRA_ON) {
				g.setColor(Color.green);
				g.setFont(FontResource.DINMED.getFont(14));
				g.drawString(SimilarContentManager.debugStr, 280, 498);
			}
		}
		
		private void paintContents(DVBBufferedImage context, UIComponent c) {
			DVBGraphics gg = (DVBGraphics) context.getGraphics();
			
			clear(gg);
			
			Rectangle originClip = gg.getClipBounds();
			if ((originClip != null && originClip.height > 50) || isAnimating || mode != MODE_OPEN) {
				gg.setClip(clipBounds);
			} else {
				originClip = null;
			}
			
			int posY = 0;
			if (mode == MODE_INTRO) {
				posY = introPosY[frameCount];
			} else if (mode == MODE_OPEN) {
				posY = openPosY[frameCount];
			} else {
				posY = closePosY[frameCount];
			}
			
			gg.setColor(similarBgColor);
			gg.fillRect(clipBounds.x, posY, clipBounds.width, clipBounds.height - 1);
			gg.setColor(lineColor);
			gg.fillRect(clipBounds.x, posY, clipBounds.width, 1);

			posY += 12;
			BaseUI parent = (BaseUI) c.getParent();
			for (int i = 0; i < contents.length; i++) {
				// wish list
				int gapX = i * 110;
				if (i == focus && hasFocus) {
					gg.setColor(focuseColor);
					
					if (contents[i].getPoster() != null) {
						int width = contents[i].getPoster().getWidth(c) + 4;
						int height = contents[i].getPoster().getHeight(c) + 4;
						if (contents[i].getPoster().getHeight(c) > 100) {
							gg.fillRect(58 + gapX, posY - 2, width, height);
						} else {
							gg.fillRect(57 + gapX, posY - 2 + 33, width, height);
						}
					} else {
						gg.fillRect(58 + gapX, posY - 2, 94, 139);
					}
					
					// arrow
					if (focus > 0) {
						gg.drawImage(arrowLeftImg, 46 + gapX, 370, c);
						gg.drawImage(arrowRightImg, 157 + gapX, 370, c);
					} else {
						gg.drawImage(arrowRightImg, 157 + gapX, 370, c);
					}
				}

				if (contents[i].getPoster() != null) {
					if (contents[i].getPoster().getHeight(c) > 100) {
						gg.drawImage(contents[i].getPoster(), 60 + gapX, posY, c);

						if ((mode != MODE_OPEN && !isAnimating) || mode == MODE_INTRO) {
							gg.setColor(cDimmed102);
							gg.fillRect(60 + gapX, posY, contents[i].getPoster().getWidth(c), contents[i]
									.getPoster().getHeight(c));
						}

						if (contents[i].getDefinitions().indexOf("UHD") > -1) {
							gg.drawImage(UHDIcon, 60 + gapX, posY + 118, c);
						} else if (contents[i].getDefinitions().indexOf("3D") > -1) {
							gg.drawImage(threeDIconImg, 60 + gapX, posY + 118, c);
						}
					} else {
						int landPosY = posY;
						if (mode == MODE_OPEN) {
							landPosY += landOpenPosY[frameCount];
						} else if (mode == MODE_CLOSE){
							landPosY -= landClosePosY[frameCount];
						}
						gg.drawImage(contents[i].getPoster(), 59 + gapX, landPosY, c);

						if ((mode != MODE_OPEN && !isAnimating) || mode == MODE_INTRO) {
							gg.setColor(cDimmed102);
							gg.fillRect(59 + gapX, landPosY, contents[i].getPoster().getWidth(c), contents[i]
									.getPoster().getHeight(c));
						}

						if (contents[i].getDefinitions().indexOf("UHD") > -1) {
							gg.drawImage(UHDIcon, 59 + gapX, landPosY + 20, c);
						} else if (contents[i].getDefinitions().indexOf("3D") > -1) {
							gg.drawImage(threeDIconImg, 59 + gapX, landPosY + 20, c);
						}
					}
				} else { // default bg
					gg.drawImage(defaultImg, 60 + gapX, posY, 90, 135, c);
				}

				int posX = 61 + gapX;

				if (i == focus && hasFocus) {
					gg.setColor(focuseColor);
				} else {
					gg.setColor(titleColor);
				}
				if (isFavorites[i]) {
					posX -= 5;
					gg.drawImage(wishIconImg, posX - 5, posY + 142, c);
					posX += 10;
				}
				gg.setFont(FontResource.BLENDER.getFont(16));
				if (i == focus && hasFocus && !isAnimating) {

					Rectangle ori = gg.getClipBounds();

					int cnt = 0;
					cnt = Math.max(parent.scroll - 10, 0);

					if (contents[i].getTitle() != null) {
						if (shortenTitles[i].equals(contents[i].getTitle())) {
							gg.drawString(shortenTitles[i], posX, posY + 153);
						} else {
							parent.scrollBounds.x = posX;
							parent.scrollBounds.y = posY + 153 - 12;
							parent.scrollBounds.width = 88;
							parent.scrollBounds.height = 30;

							gg.clipRect(parent.scrollBounds.x, parent.scrollBounds.y, parent.scrollBounds.width,
									parent.scrollBounds.height);
							try {
								gg.drawString(contents[i].getTitle().substring(cnt), posX, posY + 153);
							} catch (IndexOutOfBoundsException ex) {
								if (useTitles[i]) {
									parent.scroll = 0;
								}
							}
						}
					}

					gg.setFont(FontResource.BLENDER.getFont(15));
					if (contents[i].getChannelName() != null) {
						if (shortenChannelNames[i].equals(contents[i].getChannelName())) {
							gg.drawString(shortenChannelNames[i], posX, posY + 167);
						} else {
							parent.scrollBounds.x = posX;
							parent.scrollBounds.y = posY + 153 - 12;
							parent.scrollBounds.width = 88;
							parent.scrollBounds.height = 30;

							gg.clipRect(parent.scrollBounds.x, parent.scrollBounds.y, parent.scrollBounds.width,
									parent.scrollBounds.height);
							try {
								gg.drawString(contents[i].getChannelName().substring(cnt), posX, posY + 167);
							} catch (IndexOutOfBoundsException ex) {
								if (!useTitles[i]) {
									parent.scroll = 0;
								}
							}
						}
					}

					gg.setClip(ori);
				} else {
					if (!hasFocus) {
						parent.scrollBounds.width = 0;
					}
					if (shortenTitles[i] != null) {
						gg.drawString(shortenTitles[i], posX, posY + 153);
					}
					gg.setFont(FontResource.BLENDER.getFont(15));
					gg.setColor(channelColor);
					if (shortenChannelNames[i] != null) {
						gg.drawString(shortenChannelNames[i], posX, posY + 167);
					}
				}
			}

			gg.setClip(originClip);
		}
	}
}

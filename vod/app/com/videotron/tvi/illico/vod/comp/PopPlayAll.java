package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.BundleDetailUI;
import com.videotron.tvi.illico.vod.ui.ListOfTitlesUI;
import com.videotron.tvi.illico.vod.ui.ListViewUI;
import com.videotron.tvi.illico.vod.ui.TitleDetailUI;

public class PopPlayAll extends BaseUI {
	public static final int CANCEL = 0;
	public static final int PLAY_1_TO_LAST = 1;
	public static final int PLAY_SEL_TO_LAST = 2;
	public static final int PLAY_SELECTED_ONLY = 3;
	
	private static PopPlayAll instance;
	
	private BaseUI parent;
	private int[] buttons;
	private ClickingEffect clickEffect;
	private Rectangle clickBounds = new Rectangle();
	private int btnX, btnY, btnW, btnH;
	
	private String title;
	private BaseElement element;

	synchronized public static PopPlayAll getInstance() {
		if (instance == null) {
			instance = new PopPlayAll();
		}
		return instance;
	}
	
	public PopPlayAll() {
		setRenderer(new PopPlayAllRenderer());
		prepare();
		setVisible(false);

		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, Object data, boolean isLast) {
		parent = comp;
		int btnCnt = 3;
		buttons = new int[btnCnt];
		buttons[0] = isLast ? PLAY_SELECTED_ONLY : PLAY_SEL_TO_LAST;
		buttons[1] = PLAY_1_TO_LAST;
		buttons[2] = CANCEL;

		element = (BaseElement) data;
		title = ((MoreDetail) element).getTitle();
		Season season = null;
		if (element instanceof Episode) {
			
			Episode episode = (Episode) element;
			if (episode.season != null) {
				season = episode.season;
			} else if (episode.seasonRef != null) {
				season = (Season) CatalogueDatabase.getInstance().getCached(episode.seasonRef);
			}
		}
		if (season != null) {
			title = season.getTitle() + ", " + title;
			Series series = null;
			if (season.series != null) {
				series = season.series;
			} else if (season.seriesRef != null) {
				series = (Series) CatalogueDatabase.getInstance().getCached(season.seriesRef);
			}
			if (series != null) {
				title = series.getTitle() + ", " + title;
			}
		}
		
		prepare();
		focus = 0;
		if (Log.DEBUG_ON) {
			Log.printDebug("PopPlayAll.show(), title = " + title);
		}
		setVisible(true);
	}

	public void keyOK() {
		parent.popUpClosed(this, new Object[] {
				element, new Integer(buttons[focus])
		});
	}

	public void keyLeft() {
	}

	public void keyRight() {
	}

	public void keyUp() {
		if (focus > 0) {
			focus--;
			repaint();
		}
	}

	public void keyDown() {
		if (focus < buttons.length - 1) {
			focus++;
			repaint();
		}
	}

	public void keyBack() {
		parent.popUpClosed(this, new Integer(CANCEL));
	}

	public boolean handleKey(int key) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PopVODStop handleKey " + key);
		}
		switch (key) {
		case HRcEvent.VK_ENTER:
			clickEffect.start(btnX, btnY, btnW, btnH);
			keyOK();
			break;
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case KeyCodes.LAST:
		case OCRcEvent.VK_EXIT:
			keyBack();
			break;
		}
		return true;
	}

	private class PopPlayAllRenderer extends Renderer {
//		private Image i_05_bgglow_t;// = dataCenter.getImage("05_bgglow_t.png");
//		private Image i_05_bgglow_b;// = dataCenter.getImage("05_bgglow_b.png");
//		private Image i_05_bgglow_m;// = dataCenter.getImage("05_bgglow_m.png");
//		private Image i_05_high;// = dataCenter.getImage("05_high.png");
		private Image i_05_sep;// = dataCenter.getImage("05_sep.png");
//		private Image i_05_popup_sha;// = dataCenter.getImage("05_popup_sha.png");
		private Image i_btn_293;// = dataCenter.getImage("btn_293.png");
		private Image i_btn_293_foc;// = dataCenter.getImage("btn_293_foc.png");
		private Image i_btn_exit;// = dataCenter.getImage("btn_exit.png");
		//private Image i_btn_menu;// = dataCenter.getImage("btn_menu.png");
		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);
		private Color c3 = new Color(3, 3, 3);
		private Color c35 = new Color(35, 35, 35);
		private Color c219 = new Color(219, 219, 219);
		private Color cTitle = new Color(252, 202, 4);

		public void prepare(UIComponent c) {
			i_btn_exit = (Image) ((Hashtable) SharedMemory.getInstance().get(
					PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_EXIT);

//			i_05_bgglow_t = dataCenter.getImage("05_bgglow_t.png");
//			i_05_bgglow_b = dataCenter.getImage("05_bgglow_b.png");
//			i_05_bgglow_m = dataCenter.getImage("05_bgglow_m.png");
//			i_05_high = dataCenter.getImage("05_high.png");
			i_05_sep = dataCenter.getImage("05_sep.png");
//			i_05_popup_sha = dataCenter.getImage("05_popup_sha.png");
			i_btn_293 = dataCenter.getImage("btn_293.png");
			i_btn_293_foc = dataCenter.getImage("btn_293_foc.png");
		}

		public void paint(Graphics g, UIComponent c) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			// background
//			g.drawImage(i_05_bgglow_t, 276, 113, c);
//			g.drawImage(i_05_bgglow_m, 276, 168, 410, 172, c);
//			g.drawImage(i_05_bgglow_b, 276, 340, c);

			g.setColor(c35);
			g.fillRect(306, 143, 350, 224);
//			g.drawImage(i_05_high, 306, 143, c);
			g.drawImage(i_05_sep, 285, 181, c);
//			g.drawImage(i_05_popup_sha, 304, 364, c);

			// popup title
			g.setFont(FontResource.BLENDER.getFont(24));
			g.setColor(cTitle);
			if (title != null) {
				GraphicUtil.drawStringCenter(g, TextUtil.shorten(
						title, g.getFontMetrics(), 320), 480, 170);
			}

			int y = 238;
			switch (buttons.length) {
			case 3:
				y = 223;
				break;
			case 4:
				y = 201;
				break;
			}
			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(c3);
			for (int i = 0; i < buttons.length; i++) {
				if (focus == i) {
					g.drawImage(i_btn_293_foc, btnX = 333, btnY = y + 37 * i, c);
					btnW = 295;
					btnH = 32;
				} else {
					g.drawImage(i_btn_293, 334, y + 37 * i, c);
				}
				String lbl = "";
				switch (buttons[i]) {
				case PLAY_1_TO_LAST:
					lbl = Resources.TEXT_PLAY_1_TO_LAST;
					break;
				case PLAY_SEL_TO_LAST:
					lbl = Resources.TEXT_PLAY_SEL_TO_LAST;
					break;
				case PLAY_SELECTED_ONLY:
					lbl = Resources.TEXT_PLAY_SEL_ONLY;
					break;
				case CANCEL:
					lbl = Resources.TEXT_CANCEL;
					break;
				}
				lbl = dataCenter.getString(lbl);
				if (buttons[i] == PLAY_SELECTED_ONLY) {
					UIComponent cur = MenuController.getInstance().getCurrentScene();
					if (cur instanceof ListOfTitlesUI) {
						lbl += ((ListOfTitlesUI) cur).findCurrentLabel();
					} else if (cur instanceof ListViewUI) {
						lbl += ((ListViewUI) cur).findCurrentLabel();
					} else if (cur instanceof BundleDetailUI) {
						lbl += ((BundleDetailUI) cur).findCurrentLabel();
					} else if (cur instanceof TitleDetailUI) {
						lbl += ((TitleDetailUI) cur).findCurrentLabel();
					}
				}
				GraphicUtil.drawStringCenter(g, lbl, 481, y + 21 + 37 * i);
			}
			// bottom
//			g.setFont(FontResource.BLENDER.getFont(17));
//			g.setColor(c219);
//			int x = GraphicUtil.drawStringRight(g,
//					dataCenter.getString(Resources.TEXT_BACK_TO_TV), 650, 393);
//			clickBounds.width = i_btn_exit.getWidth(c);
//			clickBounds.height = i_btn_exit.getHeight(c);
//			clickBounds.x = x - clickBounds.width - 5;
//			g.drawImage(i_btn_exit, clickBounds.x, clickBounds.y = 378, c);
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Resources.fullRec;
		}
	}
}

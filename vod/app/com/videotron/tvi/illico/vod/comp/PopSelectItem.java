package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;

public class PopSelectItem extends UIComponent {
	private BaseUI parent;

	private static PopSelectItem instance;

	private ScrollTexts scrollText = new ScrollTexts();

	private int offset;
	private MoreDetail[] titles;
	private ClickingEffect clickEffect;
	private Rectangle clickBounds = new Rectangle();
	private int btnX, btnY, btnW, btnH;

	synchronized public static PopSelectItem getInstance() {
		if (instance == null) {
			instance = new PopSelectItem();
		}
		return instance;
	}

	private PopSelectItem() {
		setRenderer(new PopSelectRenderer());
		prepare();

		Rectangle sbBounds = new Rectangle(520, 236, 234, 140);
		scrollText.setBounds(sbBounds);
		scrollText.setTopMaskImagePosition(516-sbBounds.x, 230-sbBounds.y);
		scrollText.setBottomMaskImagePosition(516-sbBounds.x, 351-sbBounds.y);
		scrollText.setFont(FontResource.DINMED.getFont(15));
		scrollText.setForeground(new Color(200, 200, 200));
		scrollText.setRowHeight(17);
		scrollText.setRows(sbBounds.height / 17);
		add(scrollText);

		setVisible(false);

		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, Object data) {
//		scrollText.setTopMaskImage(DataCenter.getInstance().getImage("07_iss_txt_sha_t.png"));
//		scrollText.setBottomMaskImage(DataCenter.getInstance().getImage("07_iss_txt_sha.png"));

		focus = 0;
		offset = 0;

		if (data instanceof Object[]) {
			Object[] param = (Object[]) data;
			String id = (String) param[1];
			Bundle bundle = (Bundle) CatalogueDatabase.getInstance().getCached(id);
			Log.printDebug("show, bundle = " + bundle.toString(true));
			titles = bundle.getTitles();
			if (Log.DEBUG_ON) {
				for (int i = 0; i < titles.length; i++) {
					Log.printDebug("show, titles[" + i + "/" + titles.length + "] = " + titles[i].toString(true));
				}
			}
			updateContent();
		} else {
			titles = new MoreDetail[0];
		}

		parent = comp;
		prepare();
		setVisible(true);
	}

	public void keyOK() {
		if (Log.DEBUG_ON) {
			Log.printDebug("keyOK, offset = " + offset + ", focus = " + focus);
		}
		if (focus < 0) {
			keyBack();
			return;
		}

		parent.popUpClosed(this, new Integer(offset + focus));
	}

	public void keyBack() {
		parent.popUpClosed(this, new Integer(-1));
	}

	public boolean handleKey(int key) {
		int newFocus;
		switch (key) {
		case HRcEvent.VK_ENTER:
			clickEffect.start(btnX, btnY, btnW, btnH);
			keyOK();
			break;
		case HRcEvent.VK_RIGHT:
			if (focus >= 0) {
				focus = -(focus + 1);
				repaint();
			}
			break;
		case HRcEvent.VK_LEFT:
			if (focus < 0) {
				focus = -focus - 1;
				repaint();
			}
			break;
		case HRcEvent.VK_UP:
			if (focus >= 0) {
				if (focus == 3 && offset > 0) {
					offset--;
					updateContent();
				} else {
					newFocus = Math.max(0, focus - 1);
					if (newFocus != focus) {
						focus = newFocus;
						updateContent();
					}
				}
			}
			break;
		case HRcEvent.VK_DOWN:
			if (focus >= 0) {
				if (focus == 3 && offset < len() - 7) {
					offset++;
					updateContent();
				} else {
					newFocus = Math.min(Math.min(6, len() - 1), focus + 1);
					if (newFocus != focus) {
						focus = newFocus;
						updateContent();
					}
				}
			}
			break;
		case HRcEvent.VK_PAGE_DOWN:
			clickEffect.start(clickBounds.x, clickBounds.y, clickBounds.width, clickBounds.height);
			scrollText.showNextPage();
			break;
		case HRcEvent.VK_PAGE_UP:
			clickEffect.start(clickBounds.x, clickBounds.y, clickBounds.width, clickBounds.height);
			scrollText.showPreviousPage();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
		case OCRcEvent.VK_EXIT:
			keyBack();
			break;
		}
		return true;
	}

	private int len() {
		return titles.length;
	}

	private void updateContent() {
		if (focus < 0) {
			return;
		}
		scrollText.setContents(titles[offset + focus].getDescription());
		repaint();
	}

	private class PopSelectRenderer extends BaseRenderer {

		Image i_pop_631_bg;
//		Image i_pop_sha;
		Image i_07_iss_t;
		Image i_07_iss_m;
		Image i_07_iss_b;
		Image i_07_iss_foc_dim;
		Image i_07_iss_foc;
		Image i_btn_210_foc;
		Image i_btn_210;
		Image i_btn_scroll;
		Image i_02_ars_t;
		Image i_02_ars_b;

		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);
		private Color c3 = new Color(3, 3, 3);
		private Color c35 = new Color(35, 35, 35);
		private Color c160 = new Color(160, 160, 160);
		private Color c229 = new Color(229, 229, 229);
		private Color cTitle = new Color(252, 202, 4);

		public void prepare(UIComponent c) {
			i_pop_631_bg = dataCenter.getImage("pop_631_bg.png");
//			i_pop_sha = dataCenter.getImage("pop_sha.png");
			i_07_iss_t = dataCenter.getImage("07_iss_t.png");
			i_07_iss_m = dataCenter.getImage("07_iss_m.png");
			i_07_iss_b = dataCenter.getImage("07_iss_b.png");
			i_btn_210_foc = dataCenter.getImage("btn_210_foc.png");
			i_btn_210 = dataCenter.getImage("btn_210.png");
			i_07_iss_foc_dim = dataCenter.getImage("07_iss_foc_dim.png");
			i_07_iss_foc = dataCenter.getImage("07_iss_foc.png");
			i_02_ars_t = dataCenter.getImage("02_ars_t.png");
			i_02_ars_b = dataCenter.getImage("02_ars_b.png");
			i_btn_scroll = (Image) ((Hashtable) SharedMemory.getInstance().get(
					PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_PAGE);
			super.prepare(c);
		}

		public void paint(Graphics g, UIComponent c) {
			String title;
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			g.drawImage(i_pop_631_bg, 166, 66, c);
//			g.drawImage(i_pop_sha, 196, 434, 571, 79, c);

			g.setFont(FontResource.BLENDER.getFont(24));
			g.setColor(cTitle);
			GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_MSG_CHOOSE_TITLE), 480, 123);
			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(Color.white);
			GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_MSG_CHOOSE_TITLE2), 480, 171);

			g.drawImage(i_07_iss_t, 209, 201, c);
			g.drawImage(i_07_iss_m, 209, 233, c);
			g.drawImage(i_07_iss_m, 209, 264, c);
			g.drawImage(i_07_iss_m, 209, 295, c);
			g.drawImage(i_07_iss_m, 209, 326, c);
			g.drawImage(i_07_iss_m, 209, 357, c);
			g.drawImage(i_07_iss_b, 209, 388, c);

			g.setFont(FontResource.BLENDER.getFont(17));
			g.setColor(Color.white);
			for (int i = 0; i < 7 && i < len(); i++) {
				title = titles[offset + i].getTitle();
				g.drawString(TextUtil.shorten(title, g.getFontMetrics(), 275), 220, 224 + 31 * i);
			}

			int idx;
			if (focus < 0) {
				g.drawImage(i_btn_210_foc, btnX = 530, btnY = 388, c);
				btnW = 212;
				btnH = 32;
				g.drawImage(i_07_iss_foc_dim, 209, 202 + 31 * (-focus - 1), c);
				idx = (-focus - 1);
			} else {
				g.drawImage(i_btn_210, 531, 388, c);
				g.drawImage(i_07_iss_foc, btnX = 209, btnY = 202 + 31 * focus, c);
				btnW = 298;
				btnH = 32;
				idx = focus;
			}
			g.setColor(c3);
			title = titles[offset + idx].getTitle();
			g.drawString(TextUtil.shorten(title, g.getFontMetrics(), 275), 220, 224 + 31 * idx);
			g.setFont(FontResource.BLENDER.getFont(18));
			GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_CANCEL), 635, 410);
			g.setColor(Color.white);
			g.drawString(TextUtil.shorten(title, g.getFontMetrics(), 230), 521, 216); // right

			if (offset > 0) {
				g.drawImage(i_02_ars_t, 354, 192, c);
			}
			if (offset < len() - 7) {
				g.drawImage(i_02_ars_b, 353, 412, c);
			}

			g.setFont(FontResource.BLENDER.getFont(17));
			g.setColor(c229);
			int x = GraphicUtil.drawStringRight(g, dataCenter.getString(Resources.TEXT_SCROLL_DESCRIPTION), 758, 457);
			clickBounds.width = i_btn_scroll.getWidth(c);
			clickBounds.height = i_btn_scroll.getHeight(c);
			clickBounds.x = x - clickBounds.width - 5;
			g.drawImage(i_btn_scroll, clickBounds.x, clickBounds.y = 442, c);

		}

	}
}

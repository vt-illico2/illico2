package com.videotron.tvi.illico.vod.comp;

import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Vector;

import com.videotron.tvi.illico.vod.data.vcds.type.*;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CheckResult;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.gui.MenuTreeRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.UITemplateList;

public class MenuTreeUI extends BaseUI {
	public static int MENU_SLOT = 11;
	public static final String MENU_KARAOKE = "MENU_KARAOKE";
	public static final String MENU_KARAOKE_CONTENT = "MENU_KARAOKE_CONTENT";
	public static final String MENU_VSD = "MENU_VSD";
	public static final String MENU_CHANNEL = "MENU_CHANNEL";
	public static final String MENU_CHANNEL_ON_DEMAND = "MENU_CHANNEL_ON_DEMAND";
	// R5
	public static final String MENU_CHANNEL_NEXT = "MENU_CHANNEL_NEXT";
	public static final String MENU_CHANNEL_PRELOAD_NEXT = "MENU_CHANNEL_PRELOAD_NEXT";
	
	public static final int CH_MODE_ALL = 0;
	public static final int CH_MODE_SUBSCRIBED = 1;

	private Renderer menuRenderer = new MenuTreeRenderer();
	private CategoryContainer[] menus, nextMenus;
	private int nextFocus, nextIndex;
	private int menuLenth;
	private int menuIndex;

	private Stack menuHistory = new Stack();

	private CategoryContainer toChannel;
	private BrandingElement parentBranding;

	// animation
	private Effect showingEffect;
	private Effect hidingEffect;

	private Effect fadeInEffect;
	private Effect leftOutEffect;
	private Effect rightInEffect;
	private Effect rightOutEffect;
	private Effect leftInEffect;
	private ClickingEffect clickEffect;

	private int step;
	// R5
	private int nextStartIdx;
	private int preMenuIndex;
	private boolean isPreloading;
	private ArrayList bufferedList = new ArrayList();
	private Thread loadNextThread;
	private CategoryContainer[] allChannel;
	private CategoryContainer codCategory;
	private Vector loadTopicVec = new Vector();
	
	public static boolean isAnimating = false;

	// R7.3
    CategoryContainer searchContainer = new CategoryContainer();

	public void makeEffect() {
		setRenderer(menuRenderer);
		int x = getX();
		int y = getY();
//		Point from = new Point(x, y + getHeight());
//		Point to = new Point(x, y);
//		showingEffect = new MovingEffect(this, 15, from, to,
//				MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
//		hidingEffect = new MovingEffect(this, 15, to, from,
//				MovingEffect.FADE_OUT, MovingEffect.GRAVITY);

		fadeInEffect = new AlphaEffect(this, 10, AlphaEffect.FADE_IN);
		int move = 12;
		leftOutEffect = new MenuTreeEffect(this, 10, new Point(0, 0), new Point(
				-move, 0), MovingEffect.FADE_OUT, MovingEffect.GRAVITY);
		rightInEffect = new MenuTreeEffect(this, 10, new Point(move, 0), new Point(
				0, 0), MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
		rightOutEffect = new MenuTreeEffect(this, 10, new Point(0, 0), new Point(
				move, 0), MovingEffect.FADE_OUT, MovingEffect.GRAVITY);
		leftInEffect = new MenuTreeEffect(this, 10, new Point(-move, 0), new Point(
				0, 0), MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
		clickEffect = new ClickingEffect(this, 5);
	}

	public void start(boolean back) {
		prepare();
		if (!back) {
			setFocus(0);
			setMenuIndex(0);
		} else {
			setVisible(true);
		}
	}

	public int getStep() {
		return step;
	}

	public void startMenuAnimation() {
		// showingEffect.start();
		
		if (MenuController.getInstance().isShadowed()) {
			UIComponent current = MenuController.getInstance().getCurrentScene();
			if (current == null || current instanceof CategoryUI == false) {
				return;
			}
			CategoryUI curScene = (CategoryUI) current;
			step = 0;
			setVisible(true);
			curScene.categoryChanged(getCurrentCategory());
		} else {
			//step = 1;
			//fadeInEffect.start();
			animationEnded(fadeInEffect);
		}
	}


	public boolean setMenu(CategoryContainer[] sub) {
//		Thread.dumpStack();
		Log.printDebug("MenuTreeUI.setMenu, " + (sub == null ? "NULL" : "len=" + sub.length));
//		for (int i = 0; sub != null && i < sub.length; i++) {
//			Log.printDebug(i + "> " + sub[i]);
//		}
		
		// R5
		isPreloading = false;
		
		if (sub == null) {
			// R5
			codCategory = null;
			if (CatalogueDatabase.getInstance().getRoot() != null) {
				
				// IMPROVEMENT
				if (Log.DEBUG_ON) {
					Log.printDebug("MenuTreeUI.setMenu, root=" + CatalogueDatabase.getInstance().getRoot());
					Log.printDebug("MenuTreeUI.setMenu, root.subCategoryeis=" + CatalogueDatabase.getInstance().getRoot().getSubcategories());
					Log.printDebug("MenuTreeUI.setMenu, root.subCategoryeis.length=" + CatalogueDatabase.getInstance().getRoot().getSubcategories().length);
				}
				
				menus = CatalogueDatabase.getInstance().getRoot().getSubcategories();
				menuLenth = menus.length;
				 // R5 - TANK
				menuIndex = 0;
				focus = 0;
				return menuLenth > 0;
			}
		} else {
			menus = new CategoryContainer[sub.length];
			for (int i = 0; i < sub.length; i++) {
				menus[i] = sub[i];
			}
			menuLenth = menus.length;
			return menuLenth > 0;
		}
		
		// R5
		if (getCurrentCategory() != null && getCurrentCategory().type == MenuController.MENU_CHANNEL) {
			CategoryUI.setChannelCategory(getCurrentCategory());
		}
		
		return false;
	}

	public void flushData() {
		menus = null;
		menuHistory.clear();
		loadNextThread = null;
		bufferedList.clear();
		try {
			bufferedList.notifyAll();
		} catch (Exception e){};
		
		// R5
		codCategory = null;
		loadTopicVec.clear();
	}

	public CategoryContainer[] getMenu() {
		return menus;
	}

	public CategoryContainer getCurrentCategory() {
		if (menus == null || menus.length <= menuIndex) {
			return null;
		}

		// R7.3
        if (CategoryUI.getInstance().isFocusOnSearch()) {
            searchContainer.type = MenuController.MENU_SEARCH;

            if (searchContainer.category == null) {
                searchContainer.category = new Category();
                searchContainer.category.description = new Description();
                searchContainer.category.description.title[0] = new LanguageContent(Definition.LANGUAGE_EN, dataCenter.getString("menu.search_en"));
                searchContainer.category.description.title[1] = new LanguageContent(Definition.LANGUAGE_FR, dataCenter.getString("menu.search_fr"));
            }

            return searchContainer;
        }

		return menus[menuIndex];
	}
	
	public CategoryContainer getCODCategory() {
		return codCategory;
	}

	public boolean handleKey(int key) {
		Log.printDebug("MenuTreeUI, handleKey(), code = " + key + ", step = " + step);
		if (step > 0) {
			return true;
		}
		boolean ret = true;
		switch (key) {
		case HRcEvent.VK_UP:
			keyUp(true);
			break;
		case HRcEvent.VK_DOWN:
			keyDown(true);
			break;
		case HRcEvent.VK_LEFT:
			ret = keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			ret = keyRight();
			break;
		case HRcEvent.VK_ENTER:
			ret = keyOK();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		case HRcEvent.VK_PAGE_UP:
			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
				((BaseUI)MenuController.getInstance().getCurrentScene()).getFooter().clickAnimation(
						Resources.TEXT_CHANGE_PAGE);
				for (int i = 0; i < MENU_SLOT; i++) {
					keyUp(false);
				}
                // VDTRMASTER-6103
                CategoryUI.getInstance().updateButtons();
				repaint();
				loadNextCategory();
			}
			break;
		case HRcEvent.VK_PAGE_DOWN:
			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
				((BaseUI)MenuController.getInstance().getCurrentScene()).getFooter().clickAnimation(
						Resources.TEXT_CHANGE_PAGE);
				for (int i = 0; i < MENU_SLOT; i++) {
					keyDown(false);
				}
				// VDTRMASTER-6103
                CategoryUI.getInstance().updateButtons();
				repaint();
				loadNextCategory();
			}
			break;
		default:
			ret = false;
			break;
		}
		return ret;
	}

	public void keyUp(boolean paint) {
		Log.printDebug("keyUp, menuIndex=" + menuIndex + ", focus=" + focus);
		boolean moved = false;
		if (focus > 0) {
			menuIndex--;
			if (focus != (MENU_SLOT-1)/2 || menuIndex - focus < 0) {
				focus--;
			}
			if (menuIndex >= 0) {
				while (menuIndex < menus.length 
						&& (menus[menuIndex].type == MenuController.MENU_UNSUBSCRIBED_CHANNELS
                        || menus[menuIndex].type == MenuController.MENU_SUBSCRIBED_CHANNELS)) {
					keyUp(false);
				}

				if (menuIndex < 0) {
					keyDown(false);
				}
				
			} else {
				keyDown(false);
			}
			moved = true;
		} else if (!paint) {
			keyDown(false);
		} else { // R7.3
			//R7.4 VDTRMASTER-6161
            if (isRoot() && isTop() && menuIndex == 0 && focus == 0) {
                CategoryUI.getInstance().setFocusOnSearch(true);
                ((CategoryUI) MenuController.getInstance().getCurrentScene())
                        .categoryChanged(getCurrentCategory());
            }
            moved = true;
        }
		
		if (moved && paint) {
			repaint();
            // R7.3
            if (menus[menuIndex].type == MenuController.MENU_CHANNEL) {
                CategoryUI.getInstance().updateButtons();
            }
			// R5
			loadNextCategory();
		}
	}

	public void keyDown(boolean paint) {
		Log.printDebug("keyDown, menuIndex=" + menuIndex + ", menuLenth=" + menuLenth + ", focus=" + focus + ", paint="
				+ paint);
		boolean moved = false;
		if (menuIndex < menuLenth - 1) {

		    // R7.3
            if (CategoryUI.getInstance().isFocusOnSearch()) {
                CategoryUI.getInstance().setFocusOnSearch(false);
                ((CategoryUI) MenuController.getInstance().getCurrentScene())
                        .categoryChanged(getCurrentCategory());
            } else {
                menuIndex++;
                if (focus != (MENU_SLOT - 1) / 2 || menuLenth - menuIndex <= focus + (MENU_SLOT % 2 == 0 ? 1 : 0)) {
                    focus++;
                }
                //R7.3 sykim VDTRMASTER-6160
                if (menuIndex == menuLenth - 1 && menus[menuIndex].type == MenuController.MENU_UNSUBSCRIBED_CHANNELS) {
                	keyUp(false);
                } else {
	                while (menuIndex < menuLenth
	                        && (menus[menuIndex].type == MenuController.MENU_UNSUBSCRIBED_CHANNELS
	                        || menus[menuIndex].type == MenuController.MENU_SUBSCRIBED_CHANNELS)) {
	                    keyDown(false);
	                }
                }
            }
			
			moved = true;
		} 
		
		if (!paint && 
				(menus[menuIndex].type == MenuController.MENU_SUBSCRIBED_CHANNELS
				&& ChannelInfoManager.getInstance().getSubscribedChannelsCount() == 0)) {
			keyUp(false);
		}
		
		if (moved && paint) {
            // R7.3
            if (menus[menuIndex].type == MenuController.MENU_CHANNEL) {
                CategoryUI.getInstance().updateButtons();
            }
			repaint();
			// R5
			loadNextCategory();
		}
	}

	public boolean keyLeft() {
		CategoryContainer cat = getCurrentCategory();
		if (cat == null) return false;
		return true;
	}

	public boolean keyRight() {
		CategoryContainer cat = getCurrentCategory();
		if (cat == null) return false;
		Log.printDebug("MenuTreeUI currentFocusUI=" + ((CategoryUI) MenuController.getInstance().getCurrentScene()).getCurrentFocusUI());
		if (((CategoryUI) MenuController.getInstance().getCurrentScene()).getCurrentFocusUI() == CategoryUI.FOCUS_MENU) {
			if ((menu.openAdultCategory() == false && cat.isAdult && cat.type != MenuController.MENU_RESUME
					&& cat.type != MenuController.MENU_WISHLIST)
					|| cat.type == MenuController.MENU_SEARCH
					//|| cat.type == MenuController.MENU_ABOUT
					|| (cat.type == MenuController.MENU_RESUME_ADULT && 
						BookmarkManager.getInstance().numberActivePurchase(true) == 0)
					|| (cat.type == MenuController.MENU_RESUME && 
						BookmarkManager.getInstance().numberActivePurchase(false) == 0)
					|| (cat.type == MenuController.MENU_WISHLIST &&
						BookmarkManager.getInstance().retrieveWishlistTitles(false).length == 0)
					|| (cat.type == MenuController.MENU_WISHLIST_ADULT &&
						BookmarkManager.getInstance().retrieveWishlistTitlesAdult().length == 0)
					) {
				return true;
			} else {
				//R7.4 freelife Add right key vbm
				try {
					switch (cat.type) {
					case MenuController.MENU_CATEGORY:
						VbmController.getInstance().writeSelectionAddHistory(cat.getId(), cat.parent);
						break;
					case MenuController.MENU_KARAOKE:
						VbmController.getInstance().writeSelectionAddHistory(cat.getId(), cat.parent);
						break;
					case MenuController.MENU_KARAOKE_PACK: // party pack, trial pack
						VbmController.getInstance().writeSelectionAddHistory(cat.getId(), cat.parent);
						break;
					case MenuController.MENU_KARAOKE_CONTENTS: // standard, rock, kids, ...
						VbmController.getInstance().writeSelectionAddHistory(cat.getId(), cat.parent);
						break;
					case MenuController.MENU_CHANNEL_ENV:
						VbmController.getInstance().writeSelectionAddHistory(cat.getId(), cat.parent);
						break;
					case MenuController.MENU_CHANNEL:
						VbmController.getInstance().writeSelectionAddHistory(cat.getId(), cat.parent);
						break;
					case MenuController.MENU_VSD:
						VbmController.getInstance().writeSelectionAddHistory(cat.getId(), cat.parent);
						break;
					case MenuController.MENU_RESUME:
						// Touche: VDTRMASTER-6246, VDTRMASTER-6247						
						VbmController.getInstance().writeSelectionAddHistory("N_Resume Viewing");
						break;
					case MenuController.MENU_RESUME_ADULT:
						// Touche: VDTRMASTER-6246, VDTRMASTER-6247
						VbmController.getInstance().writeSelectionAddHistory("N_Resume Viewing");
						break;
					case MenuController.MENU_SEARCH:
						break;
					case MenuController.MENU_WISHLIST:
					case MenuController.MENU_WISHLIST_ADULT:
						// Touche: VDTRMASTER-6245, VDTRMASTER-6246 
						VbmController.getInstance().writeSelectionAddHistory("wishlist");
						break;
					case MenuController.MENU_ABOUT:		
						// Touche:  20190222 
						VbmController.getInstance().writeSelectionAddHistory("About Channel", cat.parent);
						break;
					}
				} catch (Exception e) {
					
				}
				((CategoryUI) MenuController.getInstance().getCurrentScene()).setCurrentFocusUI(
						CategoryUI.FOCUS_RIGHT);
			}
		}
		return true;
		// repaint();
	}

	public void keyBack() {
		moveToUpperMenu();
		Log.printInfo("MenuTreeUI keyBack currentCategory.type " + getCurrentCategory().type);
		// Touche Project
		// VDTRMASTER-6254
		if (getCurrentCategory().type == MenuController.MENU_ABOUT) {
			Log.printInfo("MenuTreeUI keyBack in AboutChannel");
			VbmController.getInstance().backwardHistory();
		}
	
		// R5
		// VDTRMASTER-5817
		if (codCategory != null && getCurrentCategory().type == MenuController.MENU_CHANNEL && isInCOD()) {
			MenuController.getInstance().backwardHistory(false);
			Log.printInfo("MenuTreeUI keyBack inCOD");
			// 20190222 Touche project
			VbmController.getInstance().backwardHistory();		
		}
	}

	public boolean keyOK() {
		CategoryContainer cat = getCurrentCategory();
		if (cat == null) return false;
//		keyOK_afterEffect();
//     	Point p = getLocation();
//     	Rectangle target = new Rectangle(49 - p.x, 102 + focus * 34 - p.y, 302, 50);
		boolean isBranded = CategoryUI.isBranded() && MenuTreeUI.MENU_SLOT == 10;
		// R7.3
        if (CategoryUI.getInstance().isFocusOnSearch()) {
            clickEffect.start(2, 13, 293, 40);
        } else {
            clickEffect.start(2, 47 + focus * 34 + (isBranded ? 34 : 0), 293, 40);
        }
		return true;
	}

	public void keyOK_afterEffect() {
		CategoryContainer category = getCurrentCategory();
		Log.printInfo("MenuTreeUI, keyOK_afterEffect(), menu = " + category + ", type = " + category.type + ", visible = " + isVisible());
		if (isVisible() == false) {
			return;
		}
		if (category.service != null && category.service.serviceType == Service.SVOD) {
			Service service = (Service) CatalogueDatabase.getInstance().getCached(category.service.id);
			if (service.description == null) {
				Log.printDebug("MenuTreeUI, keyOK_afterEffect(), there is no Service description");
				menu.showLoadingAnimation();
				CatalogueDatabase.getInstance().retrieveServices(this, category.service.id, category.service);
				return;
			}
			category.service = service;
		}
		
		boolean ret = false;
		switch (category.type) {
		case MenuController.MENU_CATEGORY:
			ret = menu.goToCategory(category, this);
			Log.printDebug("MENU_CATEGORY" + ", ret = " + ret
					+ ", #sub = " + category.subCategories.length);
			VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
			if (ret) {
				moveToSubMenu(category, category.subCategories);
//				if (((CategoryUI) MenuController.getInstance().getCurrentScene()).isBranded()) {
//					((CategoryUI) MenuController.getInstance().getCurrentScene()).setBranded(1);
//				}
			}
			break;
		case MenuController.MENU_KARAOKE:
			Log.printDebug("MENU_KARAOKE"// + ", ret = " + ret
					+ ", #sub = " + category.getTopic().treeRoot.categoryContainer.length);
			VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
			CategoryUI.setKaraokeContainer(category);
//			if (category.getTopic().treeRoot.categoryContainer.length == 0 || category.getTopic().needRetrieve) { // need retrieve
//				menu.showLoadingAnimation();
//				CatalogueDatabase.getInstance().retrieveKaraoke(this, category.getTopic().namedTopic);
//			} else {
			if (EventQueue.isDispatchThread()) {
				new Thread("keyOK, MENU_KARAOKE") {
					public void run() {
						if (getCurrentCategory().getTopic().treeRoot.categoryContainer.length == 0) {
							synchronized (CatalogueDatabase.getInstance()) {
								try {
									CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE / 12);
								} catch (InterruptedException e) {
								}
							}
						}
						moveToSubMenu(getCurrentCategory(), getCurrentCategory().getTopic().treeRoot.categoryContainer);
						
					}
				}.start();
			} else {
				moveToSubMenu(getCurrentCategory(), getCurrentCategory().getTopic().treeRoot.categoryContainer);
			}
//			}
			break;
		case MenuController.MENU_KARAOKE_PACK: // party pack, trial pack
			VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
			moveToSubMenu(category, category.categoryContainer);
			break;
		case MenuController.MENU_KARAOKE_CONTENTS: // standard, rock, kids, ...
			VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
			menu.goToKaraoke(category);
			break;
		case MenuController.MENU_CHANNEL_ENV:
			Log.printDebug("MENU_CHANNEL_ENV"// + ", ret = " + ret
					+ ", topic = " + category.getTopic() + ", tree = " + category.getTopic().treeRoot.toString(true));
			Log.printDebug("MENU_CHANNEL_ENV category.getId " + category.getId());
			VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
			
			codCategory = category;

			// R7.3
			if (category.getTopic().treeRoot.wrappedTopicPointer == null) {
				// VDTRMASTER-5817
				ChannelInfoManager.needToUpdate = false;
				menu.showLoadingAnimation();
				// VDTRMASTER-5418
//				CatalogueDatabase.getInstance().retrieveChannelOnDemand(this, category.getTopic().namedTopic);
                // R7.3
                ChannelInfoManager.getInstance().updateSubscribedList();
				CatalogueDatabase.getInstance().retrieveChannelOnDemand2(this, category.getTopic().namedTopic, true, 0, true);
			} else {
			    // R7.3
                allChannel = ChannelInfoManager.getInstance().getChannelCategoryContainers();
                moveToSubMenu(getCurrentCategory(), allChannel);
//				// R5
//				if (chMode == CH_MODE_ALL) {
//					allChannel = ChannelInfoManager.getInstance().getDataForAllChannelMode();
////					menu.forwardHistory(getCurrentCategory());
//					moveToSubMenu(getCurrentCategory(), allChannel);
//				} else {
//					CategoryContainer[] newData = ChannelInfoManager.getInstance().getDataForSubscribedMode();
////					menu.forwardHistory(getCurrentCategory());
//					moveToSubMenu(getCurrentCategory(), newData);
//				}
			}
			break;
		case MenuController.MENU_CHANNEL:
			Log.printDebug("MENU_CHANNEL"
					+ ", topic = " + category.getTopic() + ", #sub = " + category.getTopic().treeRoot.categoryContainer.length);
			
			// R5
			if (menu.openAdultCategory() == false && category.isAdult) {
				// "A popup asks PIN code even if parental controls are off", refer
				// UI parental controls
				PreferenceProxy.getInstance().checkRightFilter(
						RightFilter.ALLOW_ADULT_CATEGORY,
						null,
						this,
						new String[] {
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN1),
								DataCenter.getInstance().getString(
										Resources.TEXT_MSG_UNBLOCK_PIN2) });
				break;
			} else {
			
				VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
				if (category.getTopic().treeRoot.categoryContainer.length == 1) { // need retrieve, 1 is "about channel"
					menu.showLoadingAnimation();
					toChannel = category;
					CatalogueDatabase.getInstance().retrieveChannel(this, category.getTopic(), null);
				} else {
					moveToSubMenu(category, category.getTopic().treeRoot.categoryContainer);
	//				if (((CategoryUI) MenuController.getInstance().getCurrentScene()).isBranded()) {
	//					((CategoryUI) MenuController.getInstance().getCurrentScene()).setBranded(1);
	//				}
				}
			}
			break;
		case MenuController.MENU_VSD:
			Log.printDebug("MENU_VSD"
					+ ", topic = " + category.getTopic() + ", #sub = " + category.getTopic().treeRoot.categoryContainer.length);
			VbmController.getInstance().writeSelectionAddHistory(category.getId(), category.parent);
			// R5 - TANK
			if (category.getTopic().leafCount == 0 && category.getTopic().treeRoot.categoryContainer.length == 0) {
				menu.showLoadingAnimation();
//				CatalogueDatabase.getInstance().retrieveVSD(this, category.getTopic().namedTopic, null);
				CatalogueDatabase.getInstance().retrieveNamedTopic(this, category.getTopic().type, 
						category.getTopic().namedTopic, null);
			} else {
				// R5 - TANK
				if (Log.DEBUG_ON) {
					Log.printDebug("MenuTreeUI, keyOK_afterEffect(), topic.leafCount=" + category.getTopic().leafCount);
				}
				if (category.getTopic().leafCount > 0) {
					menu.goToCategory(category.getTopic().treeRoot, this);
				} else {
					moveToSubMenu(category, category.getTopic().treeRoot.categoryContainer);
				}
//				if (((CategoryUI) MenuController.getInstance().getCurrentScene()).isBranded()) {
//					((CategoryUI) MenuController.getInstance().getCurrentScene()).setBranded(1);
//				}
			}
			break;
		case MenuController.MENU_RESUME:
			// Touche: VDTRMASTER-6246
			VbmController.getInstance().writeSelectionAddHistory("Resume Viewing");
			menu.goToResumeViewing(null, false);
			break;
		case MenuController.MENU_RESUME_ADULT:
			// Touche: VDTRMASTER-6246
			VbmController.getInstance().writeSelectionAddHistory("Resume Viewing");
			menu.goToResumeViewing(null, true);
			break;
		case MenuController.MENU_SEARCH:
			VbmController.getInstance().writeSelectionNoHistory("Search");
			CommunicationManager.getInstance().launchSearchApplication(true, false);
			break;
		case MenuController.MENU_WISHLIST:
		case MenuController.MENU_WISHLIST_ADULT:
			// Touche: VDTRMASTER-6245, VDTRMASTER-6246, VDTRMASTER-6249 
			// VbmController.getInstance().writeSelectionAddHistory("wishlist");
			menu.goToWishlist(false);
			break;
		case MenuController.MENU_ABOUT:
			VbmController.getInstance().writeSelectionAddHistory("About Channel", category.parent);
			//menu.goToNextScene(UITemplateList.ABOUT_CHANNEL, CategoryUI.getChannel());
			keyRight();
			getParent().repaint();
			break;
		}
	}
	
	public boolean gotoChannelOnDemand() {
		CategoryContainer category = null;
		
		int codIndex = -1;
		menuIndex = -1;
		for (int i = 0; i < menus.length; i++) {
			keyDown(false);
			category = menus[menuIndex];
			if (category.getTopic() != null) {
				Topic topic = category.getTopic();
				if ("Environnement des Chaînes".equals(topic.type) || "Environnement des Chaines".equals(topic.type)) {
					codIndex = i;
					break;
				}
			}
		}
		
		if (codIndex > -1) {
			if (category.getTopic().treeRoot.categoryContainer.length == 0) {
				menu.showLoadingAnimation();
				ChannelInfoManager.getInstance().updateSubscribedList();
				CatalogueDatabase.getInstance().retrieveChannelOnDemand2(this, category.getTopic().namedTopic, true, 0, true);
			}
			return true;
		}
		
		return false;
	}

	public boolean isTop() {
		return menuHistory.empty();
	}
	
	//R7.4 VDTRMASTER-6161
	private boolean isRoot() {
		boolean ret = (CatalogueDatabase.getInstance().getRoot() == null) ||
				(MenuController.getInstance().getHistorySize() == 0);
		
		ret = ret && CategoryUI.getInstance().isTop();
		ret = ret && ((CategoryUI) MenuController.getInstance().getCurrentScene()).isSingleton();
		
		Log.printInfo("MenuTree UI isRoot " + ret);
		return ret;
	}
		
	public void reset() {
	    // R7.3
        CategoryUI.getInstance().setFocusOnSearch(false);
        step = 0;

		if (menuHistory.empty()) {
			if (menus == null) {
				setMenu(null);
			}
			setFocus(0);
			setMenuIndex(0);
			repaint();
			UIComponent cur = MenuController.getInstance().getCurrentScene();
			if (cur instanceof CategoryUI && cur.isVisible()) {
				if (getCurrentCategory() != null && getCurrentCategory().type == MenuController.MENU_CHANNEL) {
					MenuController.getInstance().setOpenAdultCategory(false, false);
				}
				((CategoryUI) cur).categoryChanged(getCurrentCategory());
			}
			return;
		}
		Object[] upperInfo = (Object[]) menuHistory.get(0);
		menuHistory.clear();
		if (upperInfo != null) {
			if (isVisible() && getParent().isVisible()) {
				//hidingEffect.start();
				step = 2;
				rightOutEffect.start();
			}
			nextMenus = (CategoryContainer[]) upperInfo[2];
			parentBranding = (BrandingElement) upperInfo[3];
			nextFocus = 0;
			nextIndex = 0;
		}
	}

//	public void moveToChannel(CategoryContainer category) {
//		if (category.getTopic().treeRoot.categoryContainer.length == 1) { // need retrieve, 1 is "about channel"
//			menu.showLoadingAnimation();
//			toChannel = category;
//			CatalogueDatabase.getInstance().retrieveChannel(this, category.getTopic(), null);
//		} else {
//			moveToSubMenu(category, category.getTopic().treeRoot.categoryContainer);
//			//CategoryUI.setChannel(category.getTopic().channel);
//		}
//	}

	public void moveToSubMenu(CategoryContainer category, CategoryContainer[] subMenu) {
		//hidingEffect.start();
		if (menus != null && menus.length > 0) {
			step = 1;
			
			if (!menu.isGotoCOD()) {
				leftOutEffect.start();
			}
		}
		nextMenus = subMenu;
		nextFocus = 0;
		nextIndex = 0;
		addHistory();
		menu.forwardHistory(category);
		if (menus == null || menus.length == 0) {
			animationEnded(leftOutEffect);
		}
		
		if (category.type == MenuController.MENU_CHANNEL) {
			if (category.getTopic().channel != null) {
				CategoryUI.setChannel(category.getTopic().channel);
			} else if (category.getTopic().network != null) {
				CategoryUI.setNetwork(category.getTopic().network);
			}
			
			CategoryUI.setChannelCategory(category);
		}
		CategoryUI.setBrandingElementByCategory(category);
		if (CategoryUI.isBranded()) {
			CategoryUI.setBranded(1);
		}
		
		// R5
		if (menu.isGotoCOD()) {
			animationEnded(leftOutEffect);
		} else {
			menu.setNeedToBackEPG(false);
		}
	}

	public void moveToUpperMenuForce() {
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		if (cur instanceof CategoryUI == false) {
			return;
		}
		CategoryUI categoryUI = (CategoryUI) cur;
		Object[] upperInfo = (Object[]) menuHistory.peek();
		nextFocus = ((Integer) upperInfo[0]).intValue();
		nextIndex = ((Integer) upperInfo[1]).intValue();
		nextMenus = (CategoryContainer[]) upperInfo[2];
		parentBranding = (BrandingElement) upperInfo[3];
		menu.backwardHistory(false);
		// 20190222 Touche project
		VbmController.getInstance().backwardHistory();		
		CategoryUI.setBranded(-1);

		// R5
//		menus = nextMenus;
		setMenu(nextMenus);
		nextMenus = null;
		menuLenth = menus.length;
		setFocus(nextFocus);
		setMenuIndex(nextIndex);
		menuHistory.pop();
		
		// R5
		if (getCurrentCategory().type == MenuController.MENU_CHANNEL 
				&& ChannelInfoManager.needToUpdate) {
			ChannelInfoManager.needToUpdate = false;
			categoryUI.updateFavoriteChannel();
		}

		categoryUI.categoryChanged(getCurrentCategory());
		categoryUI.updateButtons();
	}

	private boolean moveToUpperMenu() {
		if (menuHistory.empty()) {
			return false;
		}
		UIComponent cur = MenuController.getInstance().getCurrentScene();
		if (cur instanceof CategoryUI == false) {
			return false;
		}
		
		// R5
		// came from EPG to COD
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuTreeUI, moveToUpperMenu(), from=" + menu.getFrom() + ", needToBackEPG="
					+ menu.needToBackEPG() + ", menuHistroty.size=" + menuHistory.size());
		}
		if (menu.getFrom() != null && menu.getFrom().equals(MenuController.EPG) && menu.needToBackEPG()) {
			if (menuHistory.size() == 1) {
				VbmController.getInstance().writeExitedWithBack();
				CommunicationManager.getInstance().startUnboundApplication(MenuController.EPG, 
						new String[] { MonitorService.REQUEST_APPLICATION_LAST_KEY, Resources.APP_NAME});
				return true;
			}
		}
		
		boolean move = false;
		Object[] upperInfo = (Object[]) menuHistory.peek();
		if (upperInfo != null) {
			//hidingEffect.start();
			step = 1;
			rightOutEffect.start();
			nextFocus = ((Integer) upperInfo[0]).intValue();
			nextIndex = ((Integer) upperInfo[1]).intValue();
			nextMenus = (CategoryContainer[]) upperInfo[2];
			parentBranding = (BrandingElement) upperInfo[3];
			menu.backwardHistory(false);
			// 20190222 Touche project
			Log.printInfo("moveToUpperMenu upperInfo");
			VbmController.getInstance().backwardHistory();		
			move = true;
		}
		CategoryUI.setBranded(-1);
		return move;
	}

	public boolean isFirstLevel() {
		boolean firstLevel = true;
		if (menuHistory != null && menuHistory.size() > 0) {
			firstLevel = false;
		}
		return firstLevel;
	}

	public CategoryContainer getParentCategory() {
		if (getCurrentCategory() != null) {
			return getCurrentCategory().parent;
		}
		return null;
	}

	private void addHistory() {
		if (menus == null || menus.length == 0) {
			return;
		}
		Object[] currentInfo = new Object[4];
		currentInfo[0] = new Integer(getFocus());
		currentInfo[1] = new Integer(menuIndex);
		currentInfo[2] = menus;
		currentInfo[3] = CategoryUI.getBrandingElement();
		menuHistory.add(currentInfo);
	}

	/**
	 * @return the menuIndex
	 */
	public int getMenuIndex() {
		return menuIndex;
	}

	/**
	 * @param menuIndex
	 *            the menuIndex to set
	 */
	public void setMenuIndex(int menuIndex) {
		// R5
		Log.printDebug("MenuTreeUI, setMenuIndex(), menuIndex=" + menuIndex + ", menuLenth=" + menuLenth 
				+ ", menus=" + menus);
		this.menuIndex = menuIndex;
		// VDTRMASTER-5585
		if (menuIndex == 0 && menus != null && menus.length > 0 
				&& menus[0] != null && menus[0].type == MenuController.MENU_SUBSCRIBED_CHANNELS) {
			Log.printDebug("MenuTreeUI, menus[0].type=" + menus[0].type);
//			int next = 1;
//			while (next < menus.length 
//					&& (menus[next].type == MenuController.MENU_FAVORITE_CHANNELS 
//						|| menus[next].type == MenuController.MENU_SUBSCRIBED_CHANNELS
//						|| menus[next].type == MenuController.MENU_ALL_CHANNELS)) {
//				next++;
//			}
//			
//			if (next == menus.length) {
//				this.menuIndex = 0;
//			} else {
//				this.menuIndex = next;
//				
//				if (focus != (MENU_SLOT-1)/2 || menuLenth - menuIndex <= focus + (MENU_SLOT % 2 == 0 ? 1 : 0)) {
//	            	focus++;
//	            }
//			}
			keyDown(false);
		}
		
		Log.printDebug("MenuTreeUI, setMenuIndex(), menuIndex=" + menuIndex);
	}

	/**
	 * @return the menuLenth
	 */
	public int getMenuLenth() {
		return menuLenth;
	}
	
	public BrandingElement getParentElement() {
		BrandingElement br = null;
		
		int size = menuHistory.size();
		
		for (int i = size - 1; i > 0; i--) {
			Object[] upperInfo = (Object[]) menuHistory.get(i);
			if (upperInfo != null) {
				
				br = (BrandingElement) upperInfo[3];
				
				if (br != null) {
					break;
				}
			}
		}
		
		return br;
	}
	
	public CategoryContainer getParentCategoryHasTopic() {
		int size = menuHistory.size();
		
		Log.printDebug("getParentCategoryHasTopic(), menuHistory size = " + menuHistory.size());
		
		for (int i = size - 1; i > -1; i--) {
			Object[] upperInfo = (Object[]) menuHistory.get(i);
			if (upperInfo != null) {
				int lastIndex = ((Integer) upperInfo[1]).intValue();
				CategoryContainer[] lastMenus = (CategoryContainer[]) upperInfo[2];
				
				
				if (lastMenus[lastIndex].getTopic() != null) {
					return lastMenus[lastIndex];
				}
			}
		}
		
		return null;
	}

	public void animationStarted(Effect effect) {
		isAnimating = true;
	}

	public void animationEnded(final Effect effect) {
		UIComponent current = MenuController.getInstance().getCurrentScene();
		if (current == null || current instanceof CategoryUI == false) {
			// VDTRMASTER-5429
			isAnimating = false;
			return;
		}
		final CategoryUI curScene = (CategoryUI) current;
		if (effect == fadeInEffect) {
			step = 0;
			setVisible(true);
			if (getCurrentCategory() != null && getCurrentCategory().type == MenuController.MENU_CHANNEL) {
				MenuController.getInstance().setOpenAdultCategory(false, false);
			}
			curScene.categoryChanged(getCurrentCategory());
			// VDTRMASTER-5429
			isAnimating = false;
		} else if (effect == showingEffect || effect == rightInEffect || effect == leftInEffect) {
			Log.printDebug("MenuTreeUI, animationEnded(SHOWING)");
			step = 0;
			setVisible(true);
			// R5
			if (getCurrentCategory().type == MenuController.MENU_CHANNEL_ENV) {
				codCategory = null;
			}
			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
				MenuController.getInstance().setOpenAdultCategory(false, false);
			}
			curScene.categoryChanged(getCurrentCategory());
			curScene.updateButtons();
			// VDTRMASTER-5429
			isAnimating = false;
		} else if (effect == hidingEffect || effect == leftOutEffect || effect == rightOutEffect) {
			Log.printDebug("MenuTreeUI, animationEnded(HIDING)");
			setVisible(false);
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					// R5
//					menus = nextMenus;
					setMenu(nextMenus);
					nextMenus = null;
					menuLenth = menus == null ? 0 : menus.length;
					setFocus(nextFocus);
					setMenuIndex(nextIndex);
					//showingEffect.start();
					
					if (effect == leftOutEffect) {
						rightInEffect.start();
					} else if (effect == rightOutEffect) {
						if (step == 2) {
							menuHistory.clear();
						} else if (menuHistory.empty() == false) {
							menuHistory.pop();
						}
						if (parentBranding != null) {
							if (parentBranding instanceof Channel) {
								CategoryUI.setChannel((Channel) parentBranding);
							} else if (parentBranding instanceof Network) {
								CategoryUI.setNetwork((Network) parentBranding);
							}
							CategoryUI.setBrandingElement(parentBranding, true);
							CategoryUI.filterElement = parentBranding;
						}
						leftInEffect.start();
					} else {
						// VDTRMASTER-5429
						isAnimating = false;
					}
					
//					Log.printDebug("MenuTreeUI, getCurrentCategory().title=" + getCurrentCategory().getTitle());
//					Log.printDebug("MenuTreeUI, getCurrentCategory().parent.title=" + getCurrentCategory().parent.getTitle());
//					Log.printDebug("MenuTreeUI, getCurrentCategory().parent.type=" + getCurrentCategory().parent.type);
//					Log.printDebug("MenuTreeUI, menus[0]=" + menus[0].getTitle());
//					Log.printDebug("MenuTreeUI, menus[0].parent=" + menus[0].parent.getTitle());
//					Log.printDebug("MenuTreeUI, menus[0].parent.type=" + menus[0].parent.type);
					
					// R5
					if (getCurrentCategory() != null && getCurrentCategory().parent != null 
							&& getCurrentCategory().parent.type == MenuController.MENU_CHANNEL) {
						CategoryUI.setChannelCategory(getCurrentCategory().parent);
					} else if (menuIndex != 0 && menus != null && menus[0] != null && menus[0].parent != null &&
							menus[0].parent.getTopic() != null 
							&& menus[0].parent.getTopic().type.equals("Chaine")) { // VDTRMASTER-5810
						menus[0].parent.type = MenuController.MENU_CHANNEL; // VDTRMASTER-5815
						CategoryUI.setChannelCategory(menus[0].parent);
					}
					
					if (getCurrentCategory().type == MenuController.MENU_CHANNEL 
							&& ChannelInfoManager.needToUpdate) {
						ChannelInfoManager.needToUpdate = false;
						curScene.updateFavoriteChannel();
					}
					
					if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
						MenuController.getInstance().setOpenAdultCategory(false, false);
					}
				}
			});
		} else if (effect == clickEffect) {
			curScene.showcaseViewFadeout();
			keyOK_afterEffect();
			// VDTRMASTER-5429
			isAnimating = false;
		}
	}

	public void receiveCheckRightFilter(CheckResult checkResult) {
		Log.printDebug("MenuTreeUI, receiveCheckRightFilter()"
				+ ", response = " + checkResult.getResponse() + ", filter = "
				+ checkResult.getFilter());
		if (RightFilter.ALLOW_ADULT_CATEGORY.equals(checkResult.getFilter())) {
			if (checkResult.getResponse() == PreferenceService.RESPONSE_SUCCESS) {
				menu.setOpenAdultCategory(true, false);
				repaint();
				UIComponent cur = MenuController.getInstance().getCurrentScene();
				if (cur instanceof CategoryUI) {
					((CategoryUI) cur).refresh();	
				}
				//((CategoryUI) MenuController.getInstance().getCurrentScene()).categoryChanged(getCurrentCategory());
				// CategoryContainer category = menus[menuIndex];
				// menu.forwardHistory(category);
				// moveToSubMenu(category.subCategories);
			} else if (checkResult.getResponse() == PreferenceService.RESPONSE_FAIL) {
			}
		}
	}

	public void reflectVCDSResult(Object cmd) {
		Log.printDebug("MenuTreeUI, reflectVCDSResult(), id = "
				+ getCurrentCategory().id + ", cmd = " + cmd);
		menu.hideLoadingAnimation();
		CategoryContainer category = null;
		if (cmd instanceof CategoryContainer) {
			category = (CategoryContainer) cmd;
			if (category.type == MenuController.MENU_KARAOKE) {
				moveToSubMenu(category, category.getTopic().treeRoot.categoryContainer);
				return;
			}
		} else if (MENU_VSD.equals(cmd)) {
			category = getCurrentCategory();
			
			// VDTRMASTER-5671
			if (category.getTopic().svodService != null && category.getTopic().svodService.description == null) {
				Service service = (Service) CatalogueDatabase.getInstance().getCached(category.getTopic().svodService.id);
				if (service.description == null) {
					Log.printDebug("MenuTreeUI, reflectVCDSResult(MENU_VSD), there is no Service description");
					menu.showLoadingAnimation();
					CatalogueDatabase.getInstance().retrieveServices(this, category.getTopic().svodService.id,
							category.getTopic().svodService);
					return;
				}
			}
			
			// R5 - TANK
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuTreeUI, keyOK_afterEffect(), topic.leafCount=" + category.getTopic().leafCount);
			}
			if (category.getTopic().leafCount > 0) {
				menu.goToCategory(category.getTopic().treeRoot, this);
			} else {
				moveToSubMenu(category, category.getTopic().treeRoot.categoryContainer);
			}
		} else if (MENU_KARAOKE.equals(cmd)) {
			// replace with retrieved one
			category = getCurrentCategory();
			String topicId = category.topicPointer[0].topic.id;
			category.topicPointer[0].topic.dispose();
			category.topicPointer[0].topic = (Topic) CatalogueDatabase.getInstance().getCached(topicId);
			moveToSubMenu(category, category.getTopic().treeRoot.categoryContainer);
		} else if (MENU_CHANNEL_ON_DEMAND.equals(cmd)) {
			Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(getCurrentCategory().getTopic().id);
			getCurrentCategory().setTopic(topic);
//			menu.goToChannelList(getCurrentCategory().getTopic().treeRoot.categoryContainer);
			// R7.3
			if (Log.DEBUG_ON) {
			    CategoryContainer treeRoot = getCurrentCategory().getTopic().treeRoot;
			    Log.printDebug("MenuTreeUI, getCurrentCategory().getTopic().treeRoot=" + treeRoot.categoryContainer);
                Log.printDebug("MenuTreeUI, getCurrentCategory().getTopic().treeRoot.categoryContainer="
                        + treeRoot.categoryContainer.length);

                if (treeRoot.categoryContainer.length > 0) {
                    Log.printDebug("MenuTreeUI, treeRoot.categoryContainer.categoryContainer.length="
                            + treeRoot.categoryContainer[0].categoryContainer.length);
                }

                Log.printDebug("MenuTreeUI, treeRoot.wrappedTopicPointer=" + treeRoot.wrappedTopicPointer);

                if (treeRoot.wrappedTopicPointer != null) {
                    Favourites favorites = treeRoot.wrappedTopicPointer.favourites;
                    if (favorites != null) {
                        Log.printDebug("MenuTreeUI, favorites.count=" + favorites.count);
                        Log.printDebug("MenuTreeUI, favorites.=" + favorites.categoryContainer.length);
                    }

                    Subscribed subscribed = treeRoot.wrappedTopicPointer.subscribed;
                    if (subscribed != null) {
                        Log.printDebug("MenuTreeUI, subscribed.count=" + subscribed.count);
                        Log.printDebug("MenuTreeUI, subscribed.=" + subscribed.categoryContainer.length);
                    }

                    Unsubscribed unsubscribed = treeRoot.wrappedTopicPointer.unsubscribed;
                    if (unsubscribed != null) {
                        Log.printDebug("MenuTreeUI, unsubscribed.count=" + unsubscribed.count);
                        Log.printDebug("MenuTreeUI, unsubscribed.=" + unsubscribed.categoryContainer.length);
                    }
                }
            }

//			ChannelInfoManager.getInstance().setAllChannels(getCurrentCategory().getTopic().treeRoot.categoryContainer);

            allChannel = ChannelInfoManager.getInstance().getChannelCategoryContainers(getCurrentCategory().getTopic().treeRoot);
            moveToSubMenu(getCurrentCategory(), allChannel);
            // VDTRMASTER-5504
            addLoadTopic(0, allChannel);
			
			new Thread("LOAD_CHANNLE_LOG") {
				public void run() {
					for (int i = 0; allChannel != null && i < allChannel.length; i++) {
						if (allChannel[i].getTopic() != null) {
							Channel ch = ((CategoryContainer) allChannel[i]).getTopic().channel;
							if (ch != null) {
								ch.loadLogo(true, false);
								continue;
							}
							Network nw = ((CategoryContainer) allChannel[i]).getTopic().network;
							if (nw != null) {
								nw.loadLogo(true, false);
							}
						}
					}
				}
			}.start();
				
		} else if (MENU_CHANNEL.equals(cmd)) {
			// replace with retrieved one
			String id = toChannel.getTopic().treeRoot.id;
			Object cached = CatalogueDatabase.getInstance().getCached(id);
			if (cached != null) {
				toChannel.getTopic().treeRoot = (CategoryContainer) cached;
			}
			moveToSubMenu(toChannel, toChannel.getTopic().treeRoot.categoryContainer);
		} else if (MENU_CHANNEL_NEXT.equals(cmd)) {
			for (int i = 0; menus != null && i < menus.length; i++) {
				if (menus[i].getTopic() != null) {
					Object cached = CatalogueDatabase.getInstance().getCached(menus[i].getTopic().id);
					if (cached != null) {
						Topic topic = (Topic) cached;
						menus[i].category.sample = topic.sample;
					}
				}
			}
			
			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
				MenuController.getInstance().setOpenAdultCategory(false, false);
			}
			((CategoryUI) MenuController.getInstance().getCurrentScene()).categoryChanged(getCurrentCategory());
			
			// VDTRMASTER-5504
			addLoadTopic(menuIndex, menus);
		} else if (MENU_CHANNEL_PRELOAD_NEXT.equals(cmd)) { // R5
			for (int i = 0; menus != null && i < menus.length; i++) {
				if (menus[i].getTopic() != null) {
					Object cached = CatalogueDatabase.getInstance().getCached(menus[i].getTopic().id);
					if (cached != null) {
						Topic topic = (Topic) cached;
						menus[i].category.sample = topic.sample;
					}
				}
			}
			
			addLoadTopic(preMenuIndex, 10, menus);
			preMenuIndex += 10;
			
			if ((menus.length - 10) > preMenuIndex) {
				addLoadTopic(preMenuIndex, 10, menus);
				CatalogueDatabase.getInstance().retrieveChannelOnDemand2(this, menus[preMenuIndex].getTopic().namedTopic, false,
						preMenuIndex, 10, true);
			} else if ((menus.length - preMenuIndex) > preMenuIndex) {
				CatalogueDatabase.getInstance().retrieveChannelOnDemand2(this, menus[preMenuIndex].getTopic().namedTopic, false,
						preMenuIndex, menus.length - preMenuIndex, true);
			}
			
		} else if (cmd instanceof Service) {
			keyOK_afterEffect();
		} else {
			menu.goToNextScene(UITemplateList.LIST_OF_TITLES, getCurrentCategory().id);
		}
	}
	
//	public void changeChannelMode() {
//		if (chMode == CH_MODE_ALL) {
//			chMode = CH_MODE_SUBSCRIBED;
//			CategoryContainer[] newData = ChannelInfoManager.getInstance().getDataForSubscribedMode();
//			setMenu(newData);
//			focus = 0;
//			setMenuIndex(0);
//			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
//				MenuController.getInstance().setOpenAdultCategory(false, false);
//			}
//			((CategoryUI) MenuController.getInstance().getCurrentScene()).categoryChanged(getCurrentCategory());
//			((CategoryUI) MenuController.getInstance().getCurrentScene()).updateButtons();
//			loadNextCategory(this);
//		} else {
//			chMode = CH_MODE_ALL;
//			// VDTRMASTER-5606
//			CategoryContainer[] newMenus = ChannelInfoManager.getInstance().getDataForAllChannelMode();
//			allChannel = newMenus;
//			setMenu(allChannel);
//			focus = 0;
//			setMenuIndex(0);
//			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
//				MenuController.getInstance().setOpenAdultCategory(false, false);
//			}
//			((CategoryUI) MenuController.getInstance().getCurrentScene()).categoryChanged(getCurrentCategory());
//			((CategoryUI) MenuController.getInstance().getCurrentScene()).updateButtons();
//		}
//		repaint();
//	}
	
//	public int getChannelMode() {
//		return chMode;
//	}

	private void loadNextCategory() {
		if ((getCurrentCategory().type == MenuController.MENU_CHANNEL 
				&& getCurrentCategory().getTopic().sample != null) 
				|| getCurrentCategory().type != MenuController.MENU_CHANNEL) {
			
//			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
//				CategoryUI.setChannelCategory(getCurrentCategory());
//			}
			if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
				MenuController.getInstance().setOpenAdultCategory(false, false);
			}
			((CategoryUI) MenuController.getInstance().getCurrentScene()).categoryChanged(getCurrentCategory());

			if (!isPreloading && getCurrentCategory().type == MenuController.MENU_CHANNEL && menus.length > 12 
					&& menus[12].getTopic().sample == null) {
				isPreloading = true;
				preMenuIndex = 10;
				if ((menus.length - 10) > preMenuIndex) {
					CatalogueDatabase.getInstance().retrieveChannelOnDemand2(this, menus[preMenuIndex].getTopic().namedTopic, false,
							preMenuIndex, 10, true);
				} else {
					CatalogueDatabase.getInstance().retrieveChannelOnDemand2(this, menus[preMenuIndex].getTopic().namedTopic, false,
							preMenuIndex, menus.length - preMenuIndex, true);
				}
			}
		} else {
			loadNextCategory(this);
		}
	}
	
	private void loadNextCategory(final BaseUI ui) {
		Log.printDebug("MenuTreeUI, loadNextCategory");
		CategoryContainer category = getCurrentCategory();
		if (bufferedList == null) {
			return;
		}
		
		synchronized (bufferedList) {
			bufferedList.add(0, category);
			bufferedList.notifyAll();
		}
		
		if (loadNextThread == null) {
			loadNextThread = new Thread("CHANNEL_LOAD_SAMPLES") {
				public void run() {
					
					while (loadNextThread != null) {
						
						synchronized (bufferedList) {
							if (bufferedList.size() > 0) {
								
								try {
									bufferedList.wait(600L);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								
								if (bufferedList.size() == 1) {
									CategoryContainer c = (CategoryContainer) bufferedList.get(0);
									Log.printDebug("MenuTreeUI, c.title=" + c.getTitle() + ", c.id=" + c.getTopic().getId());
									// VDTRMASTER-5504
									if (c.getTopic().sample == null && !loadTopicVec.contains(c.getTopic().getId())) {
										// VDTRMASTER-5504
										((CategoryUI) MenuController.getInstance().getCurrentScene()).getShowcaseView()
											.setVisible(false);
										
										nextStartIdx = getRealIndexInChannelList();
										menu.showLoadingAnimation();
										
										CatalogueDatabase.getInstance().retrieveChannelOnDemand2(
												ui, c.getTopic().namedTopic, false, nextStartIdx,true);
									} else {
										if (getCurrentCategory().type == MenuController.MENU_CHANNEL) {
											MenuController.getInstance().setOpenAdultCategory(false, false);
										}
										((CategoryUI) MenuController.getInstance().getCurrentScene())
											.categoryChanged(getCurrentCategory());
									}
									bufferedList.remove(0);
								} else {
									Object o = bufferedList.get(bufferedList.size() - 1);
									bufferedList.clear();
									bufferedList.add(o);
									
//									try {
//										bufferedList.wait(400L);
//									} catch (InterruptedException e) {
//										e.printStackTrace();
//									}
								}
							} else {
								
								try {
									bufferedList.wait();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			};
			loadNextThread.start();
		}
	}
	
	int tempMenuIndex, tempMenuLength, tempMenuFocus;
	public void findNewMenuIdx() {
		Log.printDebug("MenuTreeUI, findNewMenuIdx, menuIndex=" + menuIndex);
		CategoryContainer focusedContrainer = menus[menuIndex];

		CategoryContainer[] newMenus = ChannelInfoManager.getInstance().getChannelCategoryContainers();
        allChannel = newMenus;
        tempMenuIndex = 0;
        tempMenuLength = newMenus.length;
        tempMenuFocus = 0;
        for (int i = 0; i < newMenus.length; i++) {
            if (newMenus[tempMenuIndex].getTopic() != null && newMenus[tempMenuIndex].getTopic().id.equals(focusedContrainer.getTopic().id)) {
                break;
            } else {
                moveNextIndex(newMenus);
            }
        }
        menus = newMenus;
        menuIndex = tempMenuIndex;
        menuLenth = tempMenuLength;
        focus = tempMenuFocus;
        CategoryUI.getInstance().updateButtons();
        repaint();
		
//		if (chMode == CH_MODE_ALL) {
//			CategoryContainer[] newMenus = ChannelInfoManager.getInstance().getDataForAllChannelMode(focusedContrainer, menuIndex);
//			// VDTRMASTER-5539
//			allChannel = newMenus;
//			tempMenuIndex = 0;
//			tempMenuLength = newMenus.length;
//			tempMenuFocus = 0;
//			for (int i = 0; i < newMenus.length; i++) {
//				if (newMenus[tempMenuIndex].getTopic() != null && newMenus[tempMenuIndex].getTopic().id.equals(focusedContrainer.getTopic().id)) {
//					break;
//				} else {
//					moveNextIndex(newMenus);
//				}
//			}
//			menus = newMenus;
//			menuIndex = tempMenuIndex;
//			menuLenth = tempMenuLength;
//			focus = tempMenuFocus;
//			repaint();
//		} else {
//			CategoryContainer[] newMenus = ChannelInfoManager.getInstance().getDataForSubscribedMode(focusedContrainer, menuIndex);
//			tempMenuIndex = 0;
//			tempMenuLength = newMenus.length;
//			tempMenuFocus = 0;
//			for (int i = 0; i < newMenus.length; i++) {
//				if (newMenus[tempMenuIndex].getTopic() != null && newMenus[tempMenuIndex].getTopic().id.equals(focusedContrainer.getTopic().id)) {
//					break;
//				} else {
//					moveNextIndex(newMenus);
//				}
//			}
//			menus = newMenus;
//			menuIndex = tempMenuIndex;
//			menuLenth = tempMenuLength;
//			focus = tempMenuFocus;
//
//			// if unsubscribed channel is removed, the focus have to move to first one.
//			if (newMenus[tempMenuIndex].getTopic() != null && newMenus[tempMenuIndex].getTopic().id.equals(focusedContrainer.getTopic().id) == false) {
//				menuIndex = 0;
//				focus = 0;
//				keyDown(false);
//			}
//
//			repaint();
//		}
	}
	
	private void moveNextIndex(CategoryContainer[] newMenus) {
		if (tempMenuIndex < tempMenuLength - 1) {
			tempMenuIndex++;
			if (tempMenuFocus != (MENU_SLOT-1)/2 || tempMenuLength - tempMenuIndex <= tempMenuFocus + (MENU_SLOT % 2 == 0 ? 1 : 0)) {
				tempMenuFocus++;
            }
//			while (tempMenuIndex < tempMenuLength 
//					&& (menus[tempMenuIndex].type == MenuController.MENU_FAVORITE_CHANNELS 
//						|| menus[tempMenuIndex].type == MenuController.MENU_SUBSCRIBED_CHANNELS
//						|| menus[tempMenuIndex].type == MenuController.MENU_ALL_CHANNELS)) {
//				moveNextIndex(newMenus);
//			}
		}
	}
	
	// R5 - TANK
	public void goToChannelOnDemand(CategoryContainer ca) {
		Log.printDebug("MenuTreeUI, goToChannelOnDemand");
		Topic topic = (Topic) CatalogueDatabase.getInstance().getCached(ca.getTopic().id);
//		menu.goToChannelList(getCurrentCategory().getTopic().treeRoot.categoryContainer);
		// R7.3
        allChannel = ChannelInfoManager.getInstance().getChannelCategoryContainers(ca.getTopic().treeRoot);
        // VDTRMASTER-5504
        addLoadTopic(0, allChannel);
//			menu.forwardHistory(getCurrentCategory());
        setMenu(allChannel);
        menuLenth = allChannel == null ? 0 : allChannel.length;
        setFocus(0);
        setMenuIndex(0);

        CategoryUI.setChannelCategory(ca);
        codCategory = ca;

        UIComponent current = MenuController.getInstance().getCurrentScene();
        final CategoryUI curScene = (CategoryUI) current;

        curScene.categoryChanged(getCurrentCategory());
        curScene.updateButtons();

		new Thread("LOAD_CHANNLE_LOG") {
			public void run() {
				for (int i = 0; allChannel != null && i < allChannel.length; i++) {
					if (allChannel[i].getTopic() != null) {
						Channel ch = ((CategoryContainer) allChannel[i]).getTopic().channel;
						if (ch != null) {
							ch.loadLogo(true, false);
							continue;
						}
						Network nw = ((CategoryContainer) allChannel[i]).getTopic().network;
						if (nw != null) {
							nw.loadLogo(true, false);
						}
					}
				}
			}
		}.start();
	}
	
	// R5 - TANK
	public void moveToWishlist() {
		Log.printDebug("MenuTreeUI, moveToWishlist");
		CategoryContainer category = null;
		
		menuIndex = -1;
		for (int i = 0; i < menus.length; i++) {
			keyDown(false);
			category = menus[menuIndex];
			Log.printDebug("MenuTreeUI, moveToWishlist, category.type=" + category.type);
			if (category.type == MenuController.MENU_WISHLIST) {
				break;
			}
		}
	}
	
	// R5 - TANK
	public void moveToResumeViwing() {
		Log.printDebug("MenuTreeUI, moveToResumeViwing");
		CategoryContainer category = null;
		
		menuIndex = -1;
		for (int i = 0; i < menus.length; i++) {
			keyDown(false);
			category = menus[menuIndex];
			Log.printDebug("MenuTreeUI, moveToResumeViwing, category.type=" + category.type);
			if (category.type == MenuController.MENU_RESUME) {
				break;
			}
		}
	}
	
	// R5
	public int getRealIndexInChannelList() {
		Log.printDebug("MenuTreeUI, getRealIndexInChannelList");
		int tabCount = 0;
		
		for (int i = 0; i < menuIndex; i++) {
			if (menus[i].type <= MenuController.MENU_SUBSCRIBED_CHANNELS) {
				tabCount++;
			}
		}
		
		Log.printDebug("MenuTreeUI, getRealIndexInChannelList, return " + (menuIndex - tabCount));
		return menuIndex - tabCount;
	}
	
	// R5
	private void addLoadTopic(int startIdx, CategoryContainer[] data) {
		addLoadTopic(startIdx, 10, data);
	}
	
	private void addLoadTopic(int startIdx, int length, CategoryContainer[] data) {
		// VDTRMASTER-5504
		int count = 0;
		int idx = startIdx;
		Log.printDebug("MenuTreeUI, addLoadTopic, idx=" + idx + ", allChannel.length=" + allChannel.length);
		while (count < length && idx < data.length) {
			if (((CategoryContainer) data[idx]).type > MenuController.MENU_SUBSCRIBED_CHANNELS) {
				loadTopicVec.add(((CategoryContainer) data[idx]).getTopic().getId());
				Log.printDebug("MenuTreeUI, addLoadTopic, added topic.id=" + ((CategoryContainer) data[idx]).getTopic().getId());
				count++;
			}
			idx++;
			Log.printDebug("MenuTreeUI, addLoadTopic, idx=" + idx + ", count=" + count);
		}
		
		Log.printDebug("MenuTreeUI, addLoadTopic, loadTopicVec.size=" + loadTopicVec.size());
	}
	
	// R7
	// VDTRMASTER-5817
	/**
	 * If current categories is for COD, return true, if not return false
	 * e.g. The Movie network and HBO have a recursive category for each others. when come back from MENU_CHANNEL type
	 * it confuse whether current data is for COD or normail channel category.
	 * @return
	 */
	public boolean isInCOD() {
		if (menus == null) {
			Log.printDebug("MenuTreeUI isInCOD return false");
			return false;
		}
		
		if (menus != null) {
			for (int i = 0; i < menus.length; i++) {
				//R7.4 freelife VDTRMASTER-6200
				if (menus[i] != null && menus[i].type != MenuController.MENU_UNSUBSCRIBED_CHANNELS
                        && menus[i].type != MenuController.MENU_SUBSCRIBED_CHANNELS
                        && menus[i].type != MenuController.MENU_CHANNEL) {
					Log.printDebug("MenuTreeUI isInCOD return false");
					return false;
				}
			}
		}
		return true;
	}
}

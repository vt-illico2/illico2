package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;

public class PopOrderResult extends BaseUI {
	private static final long serialVersionUID = 8767647554647304319L;

	public static final int WATCH_NOW = 0;
	public static final int PLAY_ALL = 1;
	public static final int SELECT_ANOTHER = 2;
	public static final int VOD_MENU = 3;
	public static final int CLOSE = 4;

	private Object object;
	private BaseUI parent;
	private BaseElement first;

	private static PopOrderResult instance;
	private ClickingEffect clickEffect;
	private int btnX, btnY, btnW, btnH;

	synchronized public static PopOrderResult getInstance() {
		if (instance == null) {
			instance = new PopOrderResult();
		}
		return instance;
	}

	private PopOrderResult() {
		setRenderer(new PopSelectOptionRenderer());
		prepare();
		setVisible(false);
		
		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, Object data) {
		parent = comp;
		object = data;
		if (data != null) {
			Object cached = CatalogueDatabase.getInstance().getCached(((Object[]) data)[1]);
			if (cached != null) {
				object = cached;
				first = ((Bundle) cached).getTitles()[0];
			}
		}
		focus = 0;
		prepare();
		setVisible(true);
	}

	public void stop() {
		setVisible(false);
		parent.remove(this);
		parent.setPopUp(null);
	}

	public boolean handleKey(int key) {
		switch (key) {
		case OCRcEvent.VK_UP:
			if (object != null) {
				focus = Math.max(focus - 1, 0);
				repaint();
			}
			break;
		case OCRcEvent.VK_DOWN:
			if (object != null) {
				focus = Math.min(focus + 1, 2);
				repaint();
			}
			break;
		case OCRcEvent.VK_ENTER:
			clickEffect.start(btnX, btnY, btnW, btnH);
			keyOK();
			break;
		case OCRcEvent.VK_EXIT:
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		}
		return true;
	}

	public void keyOK() {
		if (object == null) {
			parent.popUpClosed(this, new Integer(CLOSE));
		} else {
			parent.popUpClosed(this, new Integer(focus));
		}
	}

	public void keyBack() {
		if (object == null) {
			parent.popUpClosed(this, new Integer(CLOSE));
		}
	}

	private class PopSelectOptionRenderer extends Renderer {
//		private Image i_05_bgglow_t;// = dataCenter.getImage("05_bgglow_t.png");
//		private Image i_05_bgglow_b;// = dataCenter.getImage("05_bgglow_b.png");
//		private Image i_05_bgglow_m;// = dataCenter.getImage("05_bgglow_m.png");
//		private Image i_05_high;// = dataCenter.getImage("05_high.png");
		private Image i_05_sep;// = dataCenter.getImage("05_sep.png");
//		private Image i_05_popup_sha;// = dataCenter.getImage("05_popup_sha.png");
		private Image i_btn_293;// = dataCenter.getImage("btn_293.png");
		private Image i_btn_293_foc;// = dataCenter.getImage("btn_293_foc.png");
		private Image i_05_focus;
		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);
		private Color c3 = new Color(3, 3, 3);
		private Color c35 = new Color(35, 35, 35);
		private Color c229 = new Color(229, 229, 229);
		private Color cTitle = new Color(252, 202, 4);

		public void prepare(UIComponent c) {
//			i_05_bgglow_t = dataCenter.getImage("05_bgglow_t.png");
//			i_05_bgglow_b = dataCenter.getImage("05_bgglow_b.png");
//			i_05_bgglow_m = dataCenter.getImage("05_bgglow_m.png");
//			i_05_high = dataCenter.getImage("05_high.png");
			i_05_sep = dataCenter.getImage("05_sep.png");
//			i_05_popup_sha = dataCenter.getImage("05_popup_sha.png");
			i_btn_293 = dataCenter.getImage("btn_293.png");
			i_btn_293_foc = dataCenter.getImage("btn_293_foc.png");
			i_05_focus = dataCenter.getImage("05_focus.png");
		}

		public void paint(Graphics g, UIComponent c) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			// background
//			g.drawImage(i_05_bgglow_t, 276, 113, c);
//			g.drawImage(i_05_bgglow_m, 276, 168, 410, 172, c);
//			g.drawImage(i_05_bgglow_b, 276, 340, c);
			
			g.setColor(c35);
			g.fillRect(306, 143, 350, 224);
//			g.drawImage(i_05_high, 306, 143, c);
			g.drawImage(i_05_sep, 285, 181, c);
//			g.drawImage(i_05_popup_sha, 304, 364, c);

			// popup title
			g.setFont(FontResource.BLENDER.getFont(24));
			g.setColor(cTitle);
			GraphicUtil.drawStringCenter(g, dataCenter
					.getString(Resources.TEXT_T_ORDER_COMPLETED), 480, 170);

			// msg
			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(c229);
			GraphicUtil.drawStringCenter(g, dataCenter
					.getString(Resources.TEXT_MSG_THANK), 480, object == null ? 221 : 208);
			GraphicUtil.drawStringCenter(g, dataCenter
					.getString(Resources.TEXT_MSG_ORDER_COMPLETE), 480, object == null ? 251 : 228);

			// buttons
			g.setColor(c3);
			if (object == null) { // for karaoke
				g.drawImage(i_05_focus, btnX = 403, btnY = 318, c);
				btnW = 156;
				btnH = 32;
				GraphicUtil.drawStringCenter(g, dataCenter
						.getString(Resources.TEXT_OK), 480, 339);
			} else { // bundle
				if (focus == WATCH_NOW) {
					g.drawImage(i_btn_293_foc, btnX = 333, btnY = 242, c);
					btnW = 295;
					btnH = 32;
				} else {
					g.drawImage(i_btn_293, 334, 242, c);
				}
				String btn = dataCenter.getString(Resources.TEXT_WATCH_NOW);
				if (first instanceof Episode) {
					btn = TextUtil.replace(dataCenter.getString(Resources.TEXT_WATCH_EPISODE_NOW), 
							"%%%", String.valueOf(((Episode) first).episodeNumber));
				} else {
					btn = TextUtil.shorten(dataCenter.getString(Resources.TEXT_WATCH_ITEM_NOW1) + ((Video) first).getTitle(), g.getFontMetrics(), 200);
					btn += dataCenter.getString(Resources.TEXT_WATCH_ITEM_NOW2);
				}
				GraphicUtil.drawStringCenter(g, btn, 480, 264);
				
				if (focus == PLAY_ALL) {
					g.drawImage(i_btn_293_foc, btnX = 333, btnY = 279, c);
					btnW = 295;
					btnH = 32;
				} else {
					g.drawImage(i_btn_293, 334, 279, c);
				}
				String lbl = findLabel(((Bundle) object).getTitles());
				GraphicUtil.drawStringCenter(g, dataCenter
						.getString(Resources.TEXT_WATCH_ALL) + lbl, 480, 301);
				
				if (focus == SELECT_ANOTHER) {
					g.drawImage(i_btn_293_foc, btnX = 333, btnY = 316, c);
					btnW = 295;
					btnH = 32;
				} else {
					g.drawImage(i_btn_293, 334, 316, c);
				}
				
				// VDTRMASTER-5503
				if (((Bundle)object).isMovieBundle()) {
					GraphicUtil.drawStringCenter(g, dataCenter
							.getString(Resources.TEXT_SELECT_ANOTHER_MOVIE), 480, 338);
				} else {
					GraphicUtil.drawStringCenter(g, dataCenter
							.getString(Resources.TEXT_SELECT_ANOTHER_EPISODE), 480, 338);
				}
			}
			
//			// buttons
//			g.setColor(c3);
//			if (isSetPurchase) {
//				if (focus == 0) {
//					g.drawImage(i_btn_293_foc, 333, 244, c);
//				} else {
//					g.drawImage(i_btn_293, 334, 244, c);
//				}
//				GraphicUtil.drawStringCenter(g, dataCenter
//						.getString(Resources.TEXT_WATCH_EPISODE_NOW), 480,
//						isSetPurchase ? 229 : 245);
//			}
//			if (focus == 1) {
//				g.drawImage(i_btn_293_foc, 333, 281, c);
//			} else {
//				g.drawImage(i_btn_293, 334, 281, c);
//			}
//			GraphicUtil.drawStringCenter(g, dataCenter
//					.getString(isSetPurchase ? Resources.TEXT_SELECT_ANOTHER
//							: Resources.TEXT_WATCH_NOW), 480,
//					isSetPurchase ? 229 : 245);
//			if (focus == 2) {
//				g.drawImage(i_btn_293_foc, 333, 318, c);
//			} else {
//				g.drawImage(i_btn_293, 334, 318, c);
//			}
//			GraphicUtil.drawStringCenter(g, dataCenter
//					.getString(Resources.TEXT_VOD_MENU), 480,
//					isSetPurchase ? 229 : 245);
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Resources.fullRec;
		}
	}
}

package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Description;
import com.videotron.tvi.illico.vod.data.vcds.type.Network;
import com.videotron.tvi.illico.vod.data.vcds.type.Service;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;

// Show information about subscribe a channel
public class PopSubscribe extends UIComponent {
	private BaseUI parent;

	private static PopSubscribe instance;
	private ClickingEffect clickEffect;
	private Channel ch;
	private Service service;

	synchronized public static PopSubscribe getInstance() {
		if (instance == null) {
			instance = new PopSubscribe();
		}
		return instance;
	}

	private PopSubscribe() {
		setRenderer(new PopSubscribeRenderer());
		prepare();
		setVisible(false);

		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, Object data) {
		parent = comp;
		prepare();
		
		ch = null;
		service = null;
		
		if (data instanceof Channel) {
			ch = (Channel) data;
		} else if (data instanceof Service) {
			service = (Service) data;
		}
		
		setVisible(true);
	}

	public void keyOK() {
		if (Log.DEBUG_ON) {
			Log.printDebug("keyOK, focus = " + focus);
		}
		stop();
	}

	public void stop() {
		parent.remove(this);
		parent.setPopUp(null);
		parent.repaint();
	}

	public void keyBack() {
		stop();
	}

	public boolean handleKey(int key) {
		switch (key) {
		case HRcEvent.VK_ENTER:
			if (ch != null) {
				clickEffect.start(403, 367, 156, 32);
			} else if (service != null) {
				clickEffect.start(403, 367 + 50, 156, 32);
			}
			keyOK();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
		case OCRcEvent.VK_EXIT:
			keyBack();
			break;
		}
		return true;
	}

	private class PopSubscribeRenderer extends BaseRenderer {

//		private Image i_05_high_mid;// = dataCenter.getImage("05_high_mid.png");
		private Image i_05_focus;// = dataCenter.getImage("05_focus.png");
		private Image i_05_sep;// = dataCenter.getImage("05_sep.png");
//		private Image i_05_popup_sha;// = dataCenter.getImage("05_popup_sha.png");
		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);
		private Color c3 = new Color(3, 3, 3);
		private Color c35 = new Color(35, 35, 35);
		private Color c160 = new Color(160, 160, 160);
		private Color c229 = new Color(229, 229, 229);
		private Color cTitle = new Color(252, 202, 4);

		public void prepare(UIComponent c) {
//			i_05_high_mid = dataCenter.getImage("05_high_mid.png");
			i_05_focus = dataCenter.getImage("05_focus.png");
			i_05_sep = dataCenter.getImage("05_sep.png");
//			i_05_popup_sha = dataCenter.getImage("05_popup_sha.png");

			super.prepare(c);
		}

		public void paint(Graphics g, UIComponent c) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			Channel ch = ((PopSubscribe) c).ch;
			Description description = null;
			if (((PopSubscribe) c).service != null) {
				description = ((PopSubscribe) c).service.description;
			}
			
			if (ch != null) { /////////////////////////////////////////////
				g.setColor(c35);
				g.fillRect(217, 123, 529, 293);

//				g.drawImage(i_05_popup_sha, 214, 413, 535, 79, c);
				g.drawImage(i_05_sep, 285, 161, c);
				
				// popup title
				g.setFont(FontResource.BLENDER.getFont(24));
				g.setColor(cTitle);
				GraphicUtil.drawStringCenter(g,
								dataCenter.getString(Resources.TEXT_MSG_NOT_SUBS),
								480, 149);
	
				if (ch != null && ch.logos[Channel.LOGO_LG] != null) {
					g.drawImage(ch.logos[Channel.LOGO_LG], 354, 182, c);
				} else if (ch != null && ch.network != null && ch.network.logos[Network.LOGO_LG] != null) {
					g.drawImage(ch.network.logos[Network.LOGO_LG], 354, 182, c);
				}
	
				g.setFont(FontResource.BLENDER.getFont(21));
				g.setColor(c229);
				GraphicUtil.drawStringCenter(g,
						dataCenter.getString(Resources.TEXT_MSG_TO_SUBS), 480, 287);
	
				g.setFont(FontResource.BLENDER.getFont(18));
				String web = "www.videotron.com/espaceclient";
				GraphicUtil.drawHighlightedTextCenter(g,
						dataCenter.getString(Resources.TEXT_MSG_GOTO_WEB), 480,
						308, c160, new Color[] { cTitle }, new String[] { web });
	
				g.setColor(c160);
				GraphicUtil.drawStringCenter(g,
						dataCenter.getString(Resources.TEXT_MSG_OR), 480, 308 + 18);
	
				String call = "1-88-VIDEOTRON";
				GraphicUtil.drawHighlightedTextCenter(g,
						dataCenter.getString(Resources.TEXT_MSG_CALL), 480,
						308 + 18 * 2, c160, new Color[] { cTitle },
						new String[] { call });
				
				g.drawImage(i_05_focus, 403, 367, c);
				g.setColor(c3);
				GraphicUtil.drawStringCenter(g,
						dataCenter.getString(Resources.TEXT_OK), 480, 388);

//				g.drawImage(i_05_high_mid, 217, 123, c);
				
			} else if (service != null) { ///////////////////////////////////////////////////
				int deltaY = 50; // if it needs change, don't forget about button animation on OK
				int deltaH = deltaY * 2;
				g.setColor(c35);
				g.fillRect(217, 123 - deltaY, 529, 293 + deltaH);

//				g.drawImage(i_05_popup_sha, 214, 413 + deltaY, 535, 79, c);
				g.drawImage(i_05_sep, 285, 161 - deltaY, c);
				
				// popup title
				g.setFont(FontResource.BLENDER.getFont(24));
				g.setColor(cTitle);
				GraphicUtil.drawStringCenter(g,
						dataCenter.getString(Resources.TEXT_MSG_SUBS_REQUIRED),
						480, 149 - deltaY);
				
				if (service.description.logos[BrandingElement.LOGO_LG] != null) {
					g.drawImage(service.description.logos[BrandingElement.LOGO_LG], 354, 182 - deltaY, c);
				}
				
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(c229);
				String[] desc = TextUtil.split(service.getSubscriptionInfo(), g.getFontMetrics(), 500, '|');
				for (int i = 0; i < desc.length; i++) {
					GraphicUtil.drawStringCenter(g, desc[i], 480, 240 + i * 20);
				}
				
				g.drawImage(i_05_focus, 403, 367 + deltaY, c);
				g.setColor(c3);
				GraphicUtil.drawStringCenter(g,
						dataCenter.getString(Resources.TEXT_OK), 480, 388 + deltaY);

//				g.drawImage(i_05_high_mid, 217, 123 - deltaY, c);
			}///////////////////////////////////////////////////////////////////

		}

	}
}

package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Hashtable;

import javax.media.Player;
import javax.tv.media.AWTVideoSize;
import javax.tv.media.AWTVideoSizeControl;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.davic.net.tuning.NetworkInterfaceEvent;
import org.davic.net.tuning.NetworkInterfaceListener;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterfaceTuningOverEvent;
import org.dvb.event.UserEvent;
import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBColor;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vod.VODEvents;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.ixc.vod.VODServiceListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VODServiceImpl;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.controller.menu.VideoControllerImpl;
import com.videotron.tvi.illico.vod.controller.session.SessionEventType;
import com.videotron.tvi.illico.vod.controller.session.dsmcc.DSMCCSessionEvent;
import com.videotron.tvi.illico.vod.data.PlayData;
import com.videotron.tvi.illico.vod.ui.BaseUI;

// Show the result of add/remove to wish list
public class PopTrailer extends UIComponent implements VODServiceListener,
		LayeredKeyHandler, TVTimerWentOffListener {
	private static PopTrailer instance;
	private BaseUI popTrailerAdaptor;
	private PlayData playData;
	private Rectangle av = new Rectangle(265, 89, 430, 242);
	private boolean fullSize;
	private TVTimerSpec timer;
	private boolean hide;
	private ClickingEffect clickEffect;
	private Rectangle clickBounds = new Rectangle();
	private int btnX, btnY;
	private int appStatus; // trailer may called from Help
	
	synchronized public static PopTrailer getInstance() {
		if (instance == null) {
			instance = new PopTrailer();
		}
		return instance;
	}

	private PopTrailer() {
		setRenderer(new PopTrailerRenderer());
		prepare();

		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, PlayData playData) {
		Log.printDebug("PopTrailer, show()" + ", comp = " + comp
				+ ", playData = " + playData);
		
		appStatus = Resources.appStatus; // keep original status - may be PAUSED in case of HELP
		
		popTrailerAdaptor = comp;
		this.playData = playData;
		fullSize = playData.titleName == null;

		focus = 1;

		prepare();
		setVisible(true);

		VideoControllerImpl.getInstance().addVODServiceListener(this);

		setAVSize(fullSize);
		//VideoControllerImpl.getInstance().queryStatus();

		timerOn();
		VbmController.getInstance().writePlayback(true, playData.sspId);
	}

	private boolean setAVSize(boolean fullSize) {
		AWTVideoSizeControl vsc = getControl();
		if (vsc == null) {
			return false;
		}
		Rectangle src = new Rectangle(vsc.getSourceVideoSize());
		Rectangle dst = fullSize ? Resources.fullRec : av;
		AWTVideoSize size = new AWTVideoSize(src, dst);
		if (dst.width > 0 && dst.height > 0) {
			size = vsc.checkSize(size);
		}
		return vsc.setSize(size);
	}

	private AWTVideoSizeControl getControl() {
		ServiceContentHandler[] handlers = Environment.getServiceContext(0)
				.getServiceContentHandlers();
		Player player = null;
		if (handlers != null && handlers.length > 0) {
			player = (Player) handlers[0];
		}
		return (AWTVideoSizeControl) player
				.getControl("javax.tv.media.AWTVideoSizeControl");
	}

	private void timerOn() {
		if (fullSize == false) {
			return;
		}
		if (timer != null) {
			timer.removeTVTimerWentOffListener(this);
    		TVTimer.getTimer().deschedule(timer);
    	}
    	timer = new TVTimerSpec();
    	timer.setDelayTime(5000);
    	timer.addTVTimerWentOffListener(this);
    	try {
			timer = TVTimer.getTimer().scheduleTimerSpec(timer);
		} catch (TVTimerScheduleFailedException e) {}
	}
	
	public void setAppStatus(int appStatus) {
		this.appStatus = appStatus; 
	}

	public void stop() {
		Log.printDebug("PopTrailer stop(), popTrailerAdaptor = " + popTrailerAdaptor);
		if (popTrailerAdaptor == null) {
			return;
		}
		
		VideoControllerImpl.getInstance().removeVODServiceListener(null);
		Host.getInstance().removePowerModeChangeListener(powerListener);
		
		setVisible(false);
		if (getParent() != null) {
			getParent().remove(this);
		}
		setAVSize(true);
		popTrailerAdaptor.setPopUp(null);
		popTrailerAdaptor = null;
		VbmController.getInstance().writePlayback(false, playData.sspId);
		Log.printDebug("PopTrailer stop() END");
		
//		try {
//			if (parent == null) {
//				parent = (BaseUI) MenuController.getInstance().getCurrentScene();
//			}
//			parent.setPopUp(null);
//			parent.remove(this);
//			parent.repaint();
//			parent = null;
//		} catch (NullPointerException e) {}
//		Thread.yield();
//		Log.printDebug("PopTrailer stop() END");
	}

	public boolean handleKeyEvent(UserEvent evt) {
		if (evt.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}
		return handleKey(evt.getCode());
	}

	public boolean handleKey(int key) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PopTrailer handleKey : " + key + ", "
					+ playData);
		}
		boolean ret = false;
		switch (key) {
		// no actions
		case OCRcEvent.VK_LAST:
		case OCRcEvent.VK_CHANNEL_DOWN:
		case OCRcEvent.VK_CHANNEL_UP:
		case OCRcEvent.VK_0:
		case OCRcEvent.VK_1:
		case OCRcEvent.VK_2:
		case OCRcEvent.VK_3:
		case OCRcEvent.VK_4:
		case OCRcEvent.VK_5:
		case OCRcEvent.VK_6:
		case OCRcEvent.VK_7:
		case OCRcEvent.VK_8:
		case OCRcEvent.VK_9:
		case KeyCodes.PIP:
		case KeyCodes.PIP_MOVE:
		case KeyCodes.ASPECT:
		case KeyCodes.COLOR_A:
		case KeyCodes.COLOR_B:
		case KeyCodes.COLOR_C:
		case KeyCodes.COLOR_D:
			
		// block special keys
		case KeyCodes.MENU:
		case KeyCodes.FAV:
		case KeyCodes.LIST:
		case KeyCodes.WIDGET:
		case OCRcEvent.VK_GUIDE:
		case OCRcEvent.VK_INFO:
		case OCRcEvent.VK_RECORD:
			return true;
		case OCRcEvent.VK_VOLUME_DOWN:
		case OCRcEvent.VK_VOLUME_UP:
		case OCRcEvent.VK_MUTE:
			return false;
		case HRcEvent.VK_PLAY:
			keyPlay();
			focus = 1;
			repaint();
			ret = true;
			break;
		case HRcEvent.VK_PAUSE:
			keyPause();
			focus = 1;
			repaint();
			ret = true;
			break;
		case HRcEvent.VK_STOP:
			keyStop();
			ret = true;
			break;
		case HRcEvent.VK_REWIND:
			keyRew();
			focus = 0;
			repaint();
			ret = true;
			break;
		case HRcEvent.VK_FAST_FWD:
			keyFF();
			focus = 2;
			repaint();
			ret = true;
			break;
		// case OCRcEvent.VK_LAST:
		case OCRcEvent.VK_EXIT:
			clickEffect.start(clickBounds.x, clickBounds.y, clickBounds.width, clickBounds.height);
			if (playData != null && playData.currentMode == SessionEventType.SETUP_FAILED) {
				stop();
			} else {
				keyStop();
				stop();
			}
			ret = true;
			break;
		}
		if (hide) {
			hide = false;
			timerOn();
			return true;
		}
		switch (key) {
		case HRcEvent.VK_ENTER:
			clickEffect.start(btnX, btnY, 35, 35);
			keyOK();
			ret = true;
			break;
		case HRcEvent.VK_LEFT:
			if (focus > 0) {
				focus--;
				repaint();
			}
			ret = true;
			break;
		case HRcEvent.VK_RIGHT:
			if (focus < 3) {
				focus++;
				repaint();
			}
			ret = true;
			break;
		}
		timerOn();
		return ret;
	}

	private void keyOK() {
		switch (focus) {
		case 0: // rew
			keyRew();
			break;
		case 1: // play or pause
			if (playData.currentMode != VODEvents.PLAY) {
				keyPlay();
			} else {
				keyPause();
			}
			break;
		case 2: // ff
			keyFF();
			break;
		case 3: // full size
			if (fullSize) {
				fullSize = false;
				setAVSize(fullSize);
				TVTimer.getTimer().deschedule(timer);
			} else {
				fullSize = true;
				setAVSize(fullSize);
			}
			repaint();
			break;
		}
	}

	private void keyRew() {
		VideoControllerImpl.getInstance().play(playData.sspId,
				playData.currentMode = VODEvents.REW_SCALE_1);
	}

	private void keyFF() {
		VideoControllerImpl.getInstance().play(playData.sspId,
				playData.currentMode = VODEvents.FF_SCALE_1);
	}

	private void keyPause() {
		VideoControllerImpl.getInstance().pause(playData.sspId);
	}

	private void keyPlay() {
		if (playData.currentMode == VODEvents.PLAY) {
			return;
		}
		if (playData.currentMode == VODEvents.PAUSE) {
			VODServiceImpl.getInstance().logPlaybackPaused(false);
		}
		VideoControllerImpl.getInstance().play(playData.sspId,
				playData.currentMode = VODEvents.PLAY);
	}

	private void keyStop() {
		if (playData != null) {
			Resources.appStatus = appStatus;
			playData.currentMode = VODEvents.STOP;
			VideoControllerImpl.getInstance().stop(playData.sspId);
		} else {
			Log.printDebug("PopTrailer, keyStop(), playData is null");
		}
	}
	
	ErrorMessageListener errorListener = new ErrorMessageListener() {
		public void actionPerformed(int buttonType) throws RemoteException {
			MenuController.getInstance().hideLoadingAnimation();
			VideoControllerImpl.getInstance().stop(playData.sspId);
			instance.stop();
		}
	};
	
	PowerModeChangeListener powerListener = new PowerModeChangeListener() {
		public void powerModeChanged(int powerMode) {
			Log.printDebug("PopTrailer, powerModeChanged(), power = " + (powerMode == Host.LOW_POWER ? "LOW" : "FULL") + ", status = " + Resources.appStatus);
			if (Host.LOW_POWER == powerMode && Resources.appStatus == VODService.APP_WATCHING) {
				if (playData != null && playData.currentMode == SessionEventType.SETUP_FAILED) {
					stop();
				} else {
					stop();
					keyStop();
				}
			}
		}
	};
	

	
	public void addPowerModeChangeListener() {
		Log.printDebug("PopTrailer, addPowerModeChangeListener()");
		Host.getInstance().addPowerModeChangeListener(powerListener);
	};

	String lastError;
	public void error(String error, Hashtable param) {
		if (isVisible() == false) {
			return;
		}
		if (error != null) {
			lastError = error;
			CommunicationManager.getInstance().errorMessage(error, errorListener, param, true);
			stop();
		}
	}

	public void receiveVODEvent(int event) throws RemoteException {
		if (playData != null) {
			playData.npt = VideoControllerImpl.getInstance().getCurrentNPT();
			if (event != VODEvents.STATUS) {
				playData.currentMode = event;
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("receiveVODEvent, p = " + playData + ", e = " + event);
		}
		boolean needCheck = false;
		switch (event) {
		case SessionEventType.SETUP_SUCCEEDED:
			keyPlay();
			Resources.appStatus = VODService.APP_WATCHING;
			Log.printDebug("receiveVODEvent, appStatus = " + Resources.appStatus);
			try {
				CommunicationManager.getInstance().getMonitorService().reserveVideoContext(Resources.APP_NAME, null);
			} catch (RemoteException e1) {
				Log.print(e1);
			}
			VODServiceImpl.getInstance().logPlaybackStopped(false);
			break;
		case VODEvents.RESUME:
			keyPlay();
			Resources.appStatus = VODService.APP_WATCHING;
			focus = 1;
			repaint();
			break;
		case VODEvents.PLAY:
		case VODEvents.FF_SCALE_1:
		case VODEvents.REW_SCALE_1:
			Resources.appStatus = VODService.APP_WATCHING;
		case VODEvents.STATUS:
			needCheck = true;
			break;
		case VODEvents.PAUSE:
			Resources.appStatus = appStatus;
			Log.printDebug("receiveVODEvent, appStatus = " + Resources.appStatus);
			playData.currentMode = VODEvents.PAUSE;
			VODServiceImpl.getInstance().logPlaybackPaused(true);
			break;
		case VODEvents.END_OF_STREAM:
			keyStop();
			stop();
			break;
		case VODEvents.STOP:
		case SessionEventType.RELEASE_SUCCEEDED:
		case SessionEventType.RELEASE_FAILED:
			Resources.appStatus = appStatus;
			stop();
			break;
		case SessionEventType.STREAM_CONTROL_FAILED:
			error("VOD603", null);
			break;
		case SessionEventType.SETUP_FAILED:
			DSMCCSessionEvent dsmccEvt = VideoControllerImpl.getInstance().getLastEvent();
			if (dsmccEvt != null) {
				error("VOD602-" + dsmccEvt.getResponseCode(), null);
			}
			break;
		case SessionEventType.SETUP_FAILED_IOE:
			error("VOD215", null);
			break;
		case SessionEventType.SETUP_TIMEDOUT:
			error("VOD225", null);
			break;
		case SessionEventType.SERVICEGROUP_FAILED:
			error("VOD502", null);
			break;
		case VODEvents.NO_CABLE_CARD_MAC:
			error("VOD503", null);
			break;
		}
		repaint();

		if (needCheck) {
			new Thread("PopTrailer, needCheck") {
				public void run() {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
					if (playData != null
							&& playData.currentMode != VODEvents.STOP
							&& playData.currentMode != SessionEventType.RELEASE_SUCCEEDED) {
						VideoControllerImpl.getInstance().queryStatus();
					}
				}
			}.start();
		}
		
		synchronized (instance) {
			instance.notifyAll();
		}
	}

	public void timerWentOff(TVTimerWentOffEvent arg0) {
		if (timer != null) {
			timer.removeTVTimerWentOffListener(this);
    		TVTimer.getTimer().deschedule(timer);
    	}
		hide = true;
		repaint();
	}

	private class PopTrailerRenderer extends Renderer {
		DataCenter dataCenter = DataCenter.getInstance();
		Image i_04_miniflipbg;// = dataCenter.getImage("04_miniflipbg.png");
		Image i_01_flip_bg;// = dataCenter.getImage("01_flip_bg.png");
		Image i_04_flip_st_foc_01;// = dataCenter.getImage("04_flip_st_foc_01.png");
		Image i_04_flip_st_02_foc;// = dataCenter.getImage("04_flip_st_02_foc.png");
		Image i_04_flip_st_04;// = dataCenter.getImage("04_flip_st_04.png");
		Image i_04_flip_st_04_foc;// = dataCenter.getImage("04_flip_st_04_foc.png");
		Image i_04_flip_st_05;// = dataCenter.getImage("04_flip_st_05.png");
		Image i_04_flip_st_05_foc;// = dataCenter.getImage("04_flip_st_05_foc.png");
		Image i_04_flip_st_06_foc;// = dataCenter.getImage("04_flip_st_06_foc.png");
		Image i_04_flip_st_09;// = dataCenter.getImage("04_flip_st_09.png");
		Image i_04_flip_st_09_foc;// = dataCenter.getImage("04_flip_st_09_foc.png");
		Image i_04_flip_st_10;// = dataCenter.getImage("04_flip_st_10.png");
		Image i_04_flip_st_10_foc;// = dataCenter.getImage("04_flip_st_10_foc.png");
		Image i_04_flip_now01;// = dataCenter.getImage("04_flip_now01.png");
		Image i_04_flip_now02;// = dataCenter.getImage("04_flip_now02.png");
		Image i_btn_exit;// = dataCenter.getImage("btn_exit.png");
		Color c241 = new Color(241, 241, 241);
		Color c175 = new Color(175, 175, 175);
		Color c38 = new Color(38, 38, 38);
		Color cBar = new Color(255, 203, 0);

		public void prepare(UIComponent c) {
			i_btn_exit = (Image) ((Hashtable) SharedMemory.getInstance().get(
					PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_EXIT);
			i_04_miniflipbg = dataCenter.getImage("04_miniflipbg.png");
			i_01_flip_bg = dataCenter.getImage("01_flip_bg.png");
			i_04_flip_st_foc_01 = dataCenter.getImage("04_flip_st_foc_01.png");
			i_04_flip_st_02_foc = dataCenter.getImage("04_flip_st_02_foc.png");
			i_04_flip_st_04 = dataCenter.getImage("04_flip_st_04.png");
			i_04_flip_st_04_foc = dataCenter.getImage("04_flip_st_04_foc.png");
			i_04_flip_st_05 = dataCenter.getImage("04_flip_st_05.png");
			i_04_flip_st_05_foc = dataCenter.getImage("04_flip_st_05_foc.png");
			i_04_flip_st_06_foc = dataCenter.getImage("04_flip_st_06_foc.png");
			i_04_flip_st_09 = dataCenter.getImage("04_flip_st_09.png");
			i_04_flip_st_09_foc = dataCenter.getImage("04_flip_st_09_foc.png");
			i_04_flip_st_10 = dataCenter.getImage("04_flip_st_10.png");
			i_04_flip_st_10_foc = dataCenter.getImage("04_flip_st_10_foc.png");
			i_04_flip_now01 = dataCenter.getImage("04_flip_now01.png");
			i_04_flip_now02 = dataCenter.getImage("04_flip_now02.png");
		}

		public void paint(Graphics g, UIComponent c) {
			if (fullSize) {
				try {
					DVBAlphaComposite ori = ((DVBGraphics) g).getDVBComposite();
					((DVBGraphics) g).setDVBComposite(DVBAlphaComposite.Src);
					g.clearRect(Resources.fullRec.x, Resources.fullRec.y,
							Resources.fullRec.width, Resources.fullRec.height);
					((DVBGraphics) g).setDVBComposite(ori);
				} catch (UnsupportedDrawingOperationException e) {
					e.printStackTrace();
				}
				if (hide || playData == null) {
					return;
				}
				g.drawImage(i_01_flip_bg, 0, 411, c);
				btnY = 447;
				switch (c.getFocus()) {
				case 0: // rew
					g.drawImage(i_04_flip_st_foc_01, btnX = 266, 447, c);
					g.drawImage(i_04_flip_st_02_foc, 268, 451, c);
					break;
				case 1: // pause or play
					g.drawImage(i_04_flip_st_foc_01, btnX = 300, 447, c);
					if (playData.currentMode != VODEvents.PLAY) {
						g.drawImage(i_04_flip_st_05_foc, 303, 451, c);
					} else {
						g.drawImage(i_04_flip_st_04_foc, 303, 451, c);
					}
					break;
				case 2: // ff
					g.drawImage(i_04_flip_st_foc_01, btnX = 334, 447, c);
					g.drawImage(i_04_flip_st_06_foc, 337, 451, c);
					break;
				case 3: // mini size
					g.drawImage(i_04_flip_st_foc_01, btnX = 658, 447, c);
					g.drawImage(i_04_flip_st_10_foc, 661, 451, c);
					break;
				}
				if (c.getFocus() != 1) {
					if (playData.currentMode != VODEvents.PLAY) {
						g.drawImage(i_04_flip_st_05, 303, 451, c);
					} else {
						g.drawImage(i_04_flip_st_04, 303, 451, c);
					}
				}
				if (c.getFocus() != 3) {
					g.drawImage(i_04_flip_st_10, 661, 451, c);
				}

				int width = 0;
				long duration = playData.duration * 1000;
				String str = Formatter.getCurrent().getDuration(duration);
				g.setColor(Color.black);
				g.setFont(FontResource.BLENDER.getFont(16));
				if (str.indexOf(':') == str.lastIndexOf(':')) { // mm:ss
					g.fillRect(381, 460, 227, 8);
					g.setColor(c175);
					g.drawString(str, 614, 469);
					width = Math.min(225,
							(int) (225 * ((double) playData.npt / duration)));
				} else { // hh:mm:ss
					g.fillRect(381, 460, 204, 8);
					g.setColor(c175);
					g.drawString(str, 595, 469);
					width = Math.min(202,
							(int) (202 * ((double) playData.npt / duration)));
				}
				g.setColor(cBar);
				g.fillRect(382, 461, width, 6);
				int x = 382 + width;

				g.setColor(c38);
				str = Formatter.getCurrent().getDuration(playData.npt);
				if (str.indexOf(':') == str.lastIndexOf(':')) { // mm:ss
					g.drawImage(i_04_flip_now02, x - 24, 435, c);
					GraphicUtil.drawStringCenter(g, str, x, 449);
				} else { // hh:mm:ss
					g.drawImage(i_04_flip_now01, x - 33, 435, c);
					GraphicUtil.drawStringCenter(g, str, x, 449);
				}

				g.setFont(FontResource.BLENDER.getFont(17));
				g.setColor(c241);
				x = GraphicUtil.drawStringRight(g,
						dataCenter.getString(Resources.TEXT_LBL_CLOSE_PREVIEW),
						686, 503);
				clickBounds.width = i_btn_exit.getWidth(c);
				clickBounds.height = i_btn_exit.getHeight(c);
				clickBounds.x = x - clickBounds.width - 5;
				g.drawImage(i_btn_exit, clickBounds.x, clickBounds.y = 488, c);
				return;
			} // end of full size

			g.setColor(new DVBColor(12, 12, 12, 204));
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			g.drawImage(i_04_miniflipbg, 262, 86, c);
			btnY = 338;
			switch (c.getFocus()) {
			case 0: // rew
				g.drawImage(i_04_flip_st_foc_01, btnX = 266, 338, c);
				g.drawImage(i_04_flip_st_02_foc, 268, 342, c);
				break;
			case 1: // pause or play
				g.drawImage(i_04_flip_st_foc_01, btnX = 300, 338, c);
				if (playData.currentMode != VODEvents.PLAY) {
					g.drawImage(i_04_flip_st_05_foc, 303, 342, c);
				} else {
					g.drawImage(i_04_flip_st_04_foc, 303, 342, c);
				}
				break;
			case 2: // ff
				g.drawImage(i_04_flip_st_foc_01, btnX = 334, 338, c);
				g.drawImage(i_04_flip_st_06_foc, 337, 342, c);
				break;
			case 3: // mini size
				g.drawImage(i_04_flip_st_foc_01, btnX = 658, 338, c);
				g.drawImage(i_04_flip_st_09_foc, 661, 342, c);
				break;
			}
			if (c.getFocus() != 1) {
				if (playData.currentMode != VODEvents.PLAY) {
					g.drawImage(i_04_flip_st_05, 303, 342, c);
				} else {
					g.drawImage(i_04_flip_st_04, 303, 342, c);
				}
			}
			if (c.getFocus() != 3) {
				g.drawImage(i_04_flip_st_09, 661, 342, c);
			}

			try {
				DVBAlphaComposite ori = ((DVBGraphics) g).getDVBComposite();
				((DVBGraphics) g).setDVBComposite(DVBAlphaComposite.Src);
				g.clearRect(av.x, av.y, av.width, av.height);
				((DVBGraphics) g).setDVBComposite(ori);
			} catch (UnsupportedDrawingOperationException e) {
				e.printStackTrace();
			}

			int width = 0;
			long duration = playData.duration * 1000;
			String str = Formatter.getCurrent().getDuration(duration);
			g.setColor(Color.black);
			g.setFont(FontResource.BLENDER.getFont(16));
			if (str.indexOf(':') == str.lastIndexOf(':')) { // mm:ss
				g.fillRect(381, 352, 227, 8);
				g.setColor(c175);
				g.drawString(str, 614, 360);
				width = Math.min(224,
						(int) (225 * ((double) playData.npt / duration)));
			} else { // hh:mm:ss
				g.fillRect(381, 352, 204, 8);
				g.setColor(c175);
				g.drawString(str, 595, 360);
				width = Math.min(202,
						(int) (202 * ((double) playData.npt / duration)));
			}
			g.setColor(cBar);
			g.fillRect(382, 353, width, 6);
			int x = 382 + width;

			g.setColor(c38);
			str = Formatter.getCurrent().getDuration(playData.npt);
			if (str.indexOf(':') == str.lastIndexOf(':')) { // mm:ss
				g.drawImage(i_04_flip_now02, x - 24, 326, c);
				GraphicUtil.drawStringCenter(g, str, x, 340);
			} else { // hh:mm:ss
				g.drawImage(i_04_flip_now01, x - 33, 326, c);
				GraphicUtil.drawStringCenter(g, str, x, 340);
			}

			g.setFont(FontResource.BLENDER.getFont(17));
			g.setColor(c241);
			x = GraphicUtil.drawStringRight(g,
					dataCenter.getString(Resources.TEXT_LBL_CLOSE_PREVIEW),
					692, 401);
			clickBounds.width = i_btn_exit.getWidth(c);
			clickBounds.height = i_btn_exit.getHeight(c);
			clickBounds.x = x - clickBounds.width - 5;
			g.drawImage(i_btn_exit, clickBounds.x, clickBounds.y = 386, c);

			if (playData.titleName != null) {
				g.drawString(playData.titleName, 268, 401);
			}
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Resources.fullRec;
		}
	}
}

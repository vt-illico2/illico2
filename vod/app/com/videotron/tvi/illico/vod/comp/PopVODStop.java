package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;

public class PopVODStop extends BaseUI {
	public static final int PLAY_FROM_BEGINNING = 0;
	public static final int BACK_TO_MENU = 1;
	public static final int VIEW_NEXT = 2;
	public static final int LIVE_TV = 3;
	public static final int CANCEL = 4;
	public static final int ORDER_PARTYPACK = 5;
	public static final int ORDER_NEXT = 6;
	public static final int RESUME_VIEWING = 99;

	private static final long serialVersionUID = -5635279541670147559L;
	private BaseElement title;
	private BaseUI parent;
	private int[] buttons;
	private ClickingEffect clickEffect;
	private Rectangle clickBounds = new Rectangle();
	private int btnX, btnY, btnW, btnH;

	public PopVODStop() {
		setRenderer(new PopVODStopRenderer());
		prepare();
		setVisible(false);

		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, Object data, boolean resume, boolean next, boolean playFromBegin, boolean order, boolean orderNext) {
		parent = comp;
		int btnCnt = 1;
		if (resume) {
			btnCnt++;
		}
		if (next) {
			btnCnt++;
		}
		if (playFromBegin) {
			btnCnt++;
		}
		if (order) {
			btnCnt++;
		}
		buttons = new int[btnCnt];
		int btnIdx = 0;
		if (resume) {
			buttons[btnIdx++] = RESUME_VIEWING;
		}
		if (playFromBegin) {
			buttons[btnIdx++] = PLAY_FROM_BEGINNING;
		}
		if (next) {
			buttons[btnIdx++] = orderNext ? ORDER_NEXT : VIEW_NEXT;
		}
		if (order) {
			buttons[btnIdx++] = ORDER_PARTYPACK;
		}
		buttons[btnIdx++] = BACK_TO_MENU;

		title = (BaseElement) data;
		prepare();
		focus = 0;
		if (Log.DEBUG_ON) {
			Log.printDebug("PopVODStop title :"
					+ (title == null ? "NULL" : title.toString(true)) + ", button = " + buttons.length);
		}
		setVisible(true);
	}

	public void keyOK() {
		parent.popUpClosed(this, new Integer(buttons[focus]));
	}

	public void keyLeft() {
	}

	public void keyRight() {
	}

	public void keyUp() {
		if (focus > 0) {
			focus--;
			repaint();
		}
	}

	public void keyDown() {
		if (focus < buttons.length - 1) {
			focus++;
			repaint();
		}
	}

	public void keyBack() {
		parent.popUpClosed(this, new Integer(CANCEL));
	}

	public boolean handleKey(int key) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PopVODStop handleKey " + key);
		}
		switch (key) {
		case HRcEvent.VK_ENTER:
			clickEffect.start(btnX, btnY, btnW, btnH);
			keyOK();
			break;
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case KeyCodes.LAST:
			// keyBack();
			break;
		case OCRcEvent.VK_EXIT:
		case OCRcEvent.VK_LIVE:
			clickEffect.start(clickBounds.x, clickBounds.y, clickBounds.width, clickBounds.height);
			parent.popUpClosed(this, new Integer(LIVE_TV));
			break;

		case OCRcEvent.VK_PLAY:
		case OCRcEvent.VK_PAUSE:
		case OCRcEvent.VK_STOP:
		case OCRcEvent.VK_FAST_FWD:
		case OCRcEvent.VK_REWIND:
		case OCRcEvent.VK_RECORD:
		case OCRcEvent.VK_FORWARD:
		case OCRcEvent.VK_BACK:
		// DDC-113 Visually Impaired : “STAR” key is blocked.
        case KeyCodes.STAR:
			break;
		}
		return true;
	}

	private class PopVODStopRenderer extends Renderer {
//		private Image i_05_bgglow_t;// = dataCenter.getImage("05_bgglow_t.png");
//		private Image i_05_bgglow_b;// = dataCenter.getImage("05_bgglow_b.png");
//		private Image i_05_bgglow_m;// = dataCenter.getImage("05_bgglow_m.png");
//		private Image i_05_high;// = dataCenter.getImage("05_high.png");
		private Image i_05_sep;// = dataCenter.getImage("05_sep.png");
//		private Image i_05_popup_sha;// = dataCenter.getImage("05_popup_sha.png");
		private Image i_btn_293;// = dataCenter.getImage("btn_293.png");
		private Image i_btn_293_foc;// = dataCenter.getImage("btn_293_foc.png");
		private Image i_btn_exit;// = dataCenter.getImage("btn_exit.png");
		//private Image i_btn_menu;// = dataCenter.getImage("btn_menu.png");
		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);
		private Color c3 = new Color(3, 3, 3);
		private Color c35 = new Color(35, 35, 35);
		private Color c219 = new Color(219, 219, 219);
		private Color cTitle = new Color(252, 202, 4);

		public void prepare(UIComponent c) {
			i_btn_exit = (Image) ((Hashtable) SharedMemory.getInstance().get(
					PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_EXIT);

//			i_05_bgglow_t = dataCenter.getImage("05_bgglow_t.png");
//			i_05_bgglow_b = dataCenter.getImage("05_bgglow_b.png");
//			i_05_bgglow_m = dataCenter.getImage("05_bgglow_m.png");
//			i_05_high = dataCenter.getImage("05_high.png");
			i_05_sep = dataCenter.getImage("05_sep.png");
//			i_05_popup_sha = dataCenter.getImage("05_popup_sha.png");
			i_btn_293 = dataCenter.getImage("btn_293.png");
			i_btn_293_foc = dataCenter.getImage("btn_293_foc.png");
		}

		public void paint(Graphics g, UIComponent c) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			// background
//			g.drawImage(i_05_bgglow_t, 276, 113, c);
//			g.drawImage(i_05_bgglow_m, 276, 168, 410, 172, c);
//			g.drawImage(i_05_bgglow_b, 276, 340, c);

			g.setColor(c35);
			g.fillRect(306, 143, 350, 224);
//			g.drawImage(i_05_high, 306, 143, c);
			g.drawImage(i_05_sep, 285, 181, c);
//			g.drawImage(i_05_popup_sha, 304, 364, c);

			// popup title
			g.setFont(FontResource.BLENDER.getFont(24));
			g.setColor(cTitle);
			if (title != null) {
				GraphicUtil.drawStringCenter(g, TextUtil.shorten(
						((Orderable)title).getTitle(), g.getFontMetrics(), 320), 480, 170);
			}

			int y = 238;
			switch (buttons.length) {
			case 3:
				y = 223;
				break;
			case 4:
				y = 201;
				break;
			}
			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(c3);
			for (int i = 0; i < buttons.length; i++) {
				if (focus == i) {
					g.drawImage(i_btn_293_foc, btnX = 333, btnY = y + 37 * i, c);
					btnW = 295;
					btnH = 32;
				} else {
					g.drawImage(i_btn_293, 334, y + 37 * i, c);
				}
				String lbl = "";
				switch (buttons[i]) {
				case PLAY_FROM_BEGINNING:
					lbl = Resources.TEXT_PLAY_FROM_BEGIN;
					break;
				case RESUME_VIEWING:
					lbl = Resources.TEXT_RESUME_VIEW;
					break;
				case VIEW_NEXT:
					lbl = Resources.TEXT_VIEW_NEXT;
					if (MenuController.getInstance().getCurrentScene() instanceof PlayScreenUI && ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).isKaraoke()) {
						lbl = Resources.TEXT_NEXT_SONG;
					}
					break;
				case ORDER_NEXT:
					lbl = Resources.TEXT_ORDER_NEXT;
					break;
				case ORDER_PARTYPACK:
					lbl = Resources.TEXT_ORDER;
					break;
				case BACK_TO_MENU:
					lbl = Resources.TEXT_BACK_TO_MENU;
					if (MenuController.getInstance().getCurrentScene() instanceof PlayScreenUI && ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).isKaraoke()) {
						lbl = Resources.TEXT_BACK_TO_PLAYLIST;
					}
					break;
				}
				lbl = dataCenter.getString(lbl);
				if (MenuController.getInstance().getCurrentScene() instanceof PlayScreenUI) {
					switch (buttons[i]) {
					case ORDER_PARTYPACK:
						lbl += (" " + CategoryUI.getPartyPack().getTitle());
						break;
					case ORDER_NEXT:
					case VIEW_NEXT:
						
						if (((PlayScreenUI) MenuController.getInstance().getCurrentScene()).isKaraoke() == false) {
							Bundle nextBundle = ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).getNextBundle();
							
							if (nextBundle != null) {
								lbl += ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).findLabel(nextBundle);
							} else {

							    // check first in the case of Play All
                                /*
								BaseElement next = ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).getNextInPlayAll();

                                if (next == null) {
                                    next = menu.getNext(title);
                                }
                                lbl += ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).findLabel(next);
                                */
								
								BaseElement next = menu.getNext(title);
								lbl += ((PlayScreenUI) MenuController.getInstance().getCurrentScene()).findLabel(next);
							}
						}
						break;
					}
				}
				GraphicUtil.drawStringCenter(g, lbl, 481, y + 21 + 37 * i);
			}
			// bottom
			g.setFont(FontResource.BLENDER.getFont(17));
			g.setColor(c219);
			int x = GraphicUtil.drawStringRight(g,
					dataCenter.getString(Resources.TEXT_BACK_TO_TV), 650, 393);
			clickBounds.width = i_btn_exit.getWidth(c);
			clickBounds.height = i_btn_exit.getHeight(c);
			clickBounds.x = x - clickBounds.width - 5;
			g.drawImage(i_btn_exit, clickBounds.x, clickBounds.y = 378, c);
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Resources.fullRec;
		}
	}
}

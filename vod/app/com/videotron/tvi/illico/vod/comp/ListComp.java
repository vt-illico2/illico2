package com.videotron.tvi.illico.vod.comp;

import com.videotron.tvi.illico.log.Log;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.vod.controller.ComponentPool;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.gui.ListComponentRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.ListViewUI;

public class ListComp extends UIComponent {
    private static final long serialVersionUID = -4294364984270031173L;
    public static final int TYPE_RESUME = 0;
    public static final int TYPE_NORMAL = 1;
    public static final int TYPE_WISHLIST = 2;
    
    public static final int MAX_SIZE = 10;
    
    private int titleLen;
    private int titleIndex;
    private int oldTitleIndex, oldFocus;
    private ListComponentRenderer listComponent = ListComponentRenderer.getInstance();

    private int listType;
    private String listHeader;
    
    private boolean retrieving;
	private boolean preActiveForward, preActivating;
	private int preActiveCount;
    
    public static int instanceCounter = 0;
    
    // VDTRMASTER-5756
    private CategoryContainer lastCategoryContainer;
    
    public static ListComp newInstance() {
    	instanceCounter++;
    	ListComp newInstance = new ListComp();
    	ComponentPool.addComponent(newInstance);
    	return newInstance;
    }
    
    public void stop() {
    	renderer = null;
    	instanceCounter--;
    }
    
    /**
     * @return the listType
     */
    public int getListType() {
        return listType;
    }
    
//    public void setBounds(int x, int y){
//        this.setBounds(x, y, 533, 451);
//    }

    /**
     * @param listType
     *            the listType to set
     */
    public void setListType(int listType, String title) {
        this.listType = listType;
        listHeader = title;        
    }

    public void start(boolean back) {
        setRenderer(listComponent);
        if (!back) {
            focus = 0;
            titleIndex = 0;
        }
        prepare();
    }

    /**
     * @return the listHeader
     */
    public String getListHeader() {
        return listHeader;
    }

    /**
     * @param listTitle the listHeader to set
     */
    public void setListHeader(String listTitle) {
        this.listHeader = listTitle;
    }

    public void setData(BaseElement[] catalogueList) {
        // VDTRMASTER-5756
        Log.printDebug("ListComp, setData, lastCategoryContainer=" + lastCategoryContainer);
        if (lastCategoryContainer == null) {
            lastCategoryContainer = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
        }
        titleLen = catalogueList.length;
    }
    
    public void reset() {
    	 titleIndex = 0;
         focus = 0;
    }
    
    public void setFocused(BaseElement f) {
    	BaseElement[] elements = getData();
    	
    	for (int i = 0; i < titleLen; i++) {
    		if (f == elements[titleIndex]) {
    			UIComponent comp = (UIComponent) this.getParent();
    	        comp.setFocus(titleIndex);
    			return;
    		}
    		if (titleIndex < titleLen - 1) {
                titleIndex++;
                if (focus != (MAX_SIZE-1)/2 || titleLen - titleIndex <= focus + (MAX_SIZE % 2 == 0 ? 1 : 0)) {
                	focus++;
                }
            }
    	}
    	
    	// VDTRMASTER-5756
        Log.printDebug("ListComp, setFocused, lastCategoryContainer=" + lastCategoryContainer);
        if (lastCategoryContainer == null) {
            lastCategoryContainer = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
        }
    }
    
    public BaseElement[] getData() {
    	Object parent = getParent();
    	if (parent instanceof ListViewUI) {
    		return ((ListViewUI) parent).getCurrentCatalogues();
    	}
    	return null;
    }

    public boolean handleKey(int key) {
    	if (state != BaseUI.STATE_ACTIVATED) {
            return false;
        }
        switch (key) {
        case HRcEvent.VK_UP:
            keyUp();
            break;
        case HRcEvent.VK_DOWN:
            keyDown();
            break;
        case HRcEvent.VK_ENTER:
            keyOk();
            break;
//        case OCRcEvent.VK_BACK:
//        case OCRcEvent.VK_F11: // test
        case HRcEvent.VK_PAGE_UP:
        	keyPageBack();
        	break;
//        case OCRcEvent.VK_FORWARD:
//        case OCRcEvent.VK_F12: // test
        case HRcEvent.VK_PAGE_DOWN:
        	keyPageForward();
        	break;
        }
        return false;
    }
    
    public void keyPageBack() {
    	for (int i = 0; i < MAX_SIZE && focus > 0; i++) {
    		keyUp();
    	}
//    	oldTitleIndex = titleIndex;
//    	oldFocus = focus;
//    	
//    	for (int i = 0; i < MAX_SIZE && focus > 0; i++) {
//    		titleIndex--;
//    		if (focus != (MAX_SIZE-1)/2 || titleIndex - focus < 0) {
//            	focus--;
//            }
//    	}
//    	retrieveNext(false);
//    	UIComponent comp = (UIComponent) this.getParent();
//        comp.setFocus(titleIndex);
    }

    public void keyPageForward() {
    	for (int i = 0; i < MAX_SIZE && titleIndex < titleLen - 1; i++) {
    		keyDown();
    	}
//    	oldTitleIndex = titleIndex;
//    	oldFocus = focus;
//
//    	for (int i = 0; i < MAX_SIZE && titleIndex < titleLen - 1; i++) {
//    		titleIndex++;
//    		if (focus != (MAX_SIZE-1)/2 || titleLen - titleIndex <= focus + (MAX_SIZE % 2 == 0 ? 1 : 0)) {
//            	focus++;
//            }
//    	}
//    	retrieveNext(true);
//    	UIComponent comp = (UIComponent) this.getParent();
//        comp.setFocus(titleIndex);
    }
    
    public void keyUp() {
    	oldTitleIndex = titleIndex;
    	oldFocus = focus;
    	
    	if (focus > 0) {
            titleIndex--;
            if (focus != (MAX_SIZE-1)/2 || titleIndex - focus < 0) {
            	focus--;
            }
        }
    	retrieveNext(false);
        UIComponent comp = (UIComponent) this.getParent();
        comp.setFocus(titleIndex);
        
        // R5
        if (comp instanceof ListViewUI) {
        	((ListViewUI)comp).updateButtons(false);
        }
    }

    public void keyDown() {
    	oldTitleIndex = titleIndex;
    	oldFocus = focus;
    	
        if (titleIndex < titleLen - 1) {
            titleIndex++;
            if (focus != (MAX_SIZE-1)/2 || titleLen - titleIndex <= focus + (MAX_SIZE % 2 == 0 ? 1 : 0)) {
            	focus++;
            }
        }
        retrieveNext(true);
        UIComponent comp = (UIComponent) this.getParent();
        comp.setFocus(titleIndex);
        
        // R5
        if (comp instanceof ListViewUI) {
        	((ListViewUI)comp).updateButtons(false);
        }
    }
    
    public void moveFocus() {
    	while (focus-1 > (MAX_SIZE-1)/2 && titleLen - titleIndex > (MAX_SIZE-1)/2) {
    		focus--;
    	}
    }
    
    private void retrieveNext(boolean forward) {
    	if (retrieving) {
    		titleIndex = oldTitleIndex;
    		focus = oldFocus;
    		return;
    	}
    	
    	retrieving = true;
    	
    	BaseElement[] elements = getData();
    	boolean hasAll = true;
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == null) {
				hasAll = false;
				break;
			}
		}
		if (hasAll) {
			repaint();
			retrieving = false;
			return;
		}
    	
		int next = 0;
		if (forward) { // down-way
			next = Math.min(titleLen - 1, titleIndex - focus + MAX_SIZE);
		} else { // up-way
			next = (titleIndex - focus - MAX_SIZE + elements.length) % elements.length;
		}
        boolean preActiveMode = false;
		int preActive = DataCenter.getInstance().getInt("PRE_ACTIVE");
		if (preActiveForward == forward) {
			preActiveCount++;
		} else {
			preActiveCount = 1;
			preActiveForward = forward;
		}
		
		if ((preActive == 0 && elements[next] != null) || (elements[next] != null && preActiveCount < preActive || preActivating)) {
			repaint();
			retrieving = false;
			return;
		}
		
		if (preActive > 0 && preActiveCount >= preActive) {
			preActiveCount = 0;
			preActiveMode = true;
			if (forward) {
				while (next < elements.length && elements[next++] != null);
				next--;
			} else {
				while (next >= 0 && elements[next--] != null);
				if (next < 0) {
					next = elements.length - 1;
					while (next >= 0 && elements[next--] != null);
				}
			}
		}
		
		// R5 - move from lower line.
		final boolean needLoadingPopup = !preActiveMode;
			
					
		int size = 0;
		if (forward) {
			while (next >= 0 && elements[next--] == null);
			next += 2;
			for (int i = next; i < elements.length; i++) {
				if (elements[i] == null) {
					size++;
				} else {
					break;
				}
			}
			size = Math.min(DataCenter.getInstance().getInt("PAGE_SIZE"), size);//elements.length - next);
		} else {
			while (next < elements.length && elements[next++] == null);
			next--;
			int max = DataCenter.getInstance().getInt("PAGE_SIZE");
			size = 0;
			while (size < max) {
				next--;
				if (next < 0 || elements[next] != null) {
					next++;
					break;
				}
				size++;
			}
		}
		
		if (size == 0) {
			repaint();
			retrieving = false;
			return;
		}
		
		final int start = next;
		final int pageSize = size;
		
		// R5 - move to upper line for 4K because in 4K 
//		final boolean needLoadingPopup = !preActiveMode;
//		if (needLoadingPopup) {
//			retrieving = true;
//		}
		if (preActiveMode) {
			preActivating = true;
		}
		new Thread("List, retrieveNext") {
			public void run() {
				if (needLoadingPopup) {
					MenuController.getInstance().showLoadingAnimation(true);
				}
				synchronized (CatalogueDatabase.getInstance()) {
                    Log.printDebug("ListComp, retrieveNext, Thread, lastCategoryContainer=" + lastCategoryContainer);
                    //VDTRMASTER-5756
					if (lastCategoryContainer != null) {
						CatalogueDatabase.getInstance().retrieveCategoryContents(null, lastCategoryContainer.id, null,
								start, pageSize, "", !needLoadingPopup);
					} else {
						CatalogueDatabase.getInstance().retrieveCategoryContents(null, null, null, start, pageSize,
								"", !needLoadingPopup);
					}
					try {
						CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {}
				} 
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				if (cat != null) {
					UIComponent ui = MenuController.getInstance().getCurrentScene();
					if (ui instanceof ListViewUI) {
						((ListViewUI) ui).setData(cat.totalContents, false);
					}
				}
				if (needLoadingPopup) {
					MenuController.getInstance().hideLoadingAnimation();
				} else {
					preActivating = false;
				}
				retrieving = false;
			}
		}.start();
    }

    public void keyOk() {
//    	menu.goToDetail(catalogueList[titleIndex], this);
    }

    /**
     * @return the titleIndex
     */
    public int getTitleIndex() {
        return titleIndex;
    }
    public void setTitleIndex(int idx) {
    	titleIndex = idx;
    }
    
    public void setFocusAndIndex(int focus, int index) {
    	this.focus = focus;
    	this.titleIndex = index;
    }

    // R7.3
    public CategoryContainer getLastCategoryContainer() {
        return lastCategoryContainer;
    }
}

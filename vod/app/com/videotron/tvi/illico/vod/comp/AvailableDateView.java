package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.ui.BaseUI;

public class AvailableDateView extends UIComponent {

	private DataCenter dataCenter = DataCenter.getInstance();
	private String availableStr;
	private String availableStr2;
	private String dateStr;
	private String wishStr;
	
	private BaseUI parent;
	
	private boolean needAnimation;
	private boolean isAnimating;
	private boolean hasEndPeriod;
	
	private Color availableDateColor = new Color(225, 195, 80);
	private Color addWishColor = new Color(241, 241, 241);
	
	private Image profileImg;
	
	private int posY = 0;
	
	private DVBBufferedImage bufferedImage = new DVBBufferedImage(210, 72);
	
	private Thread aniThread;
	private boolean running;
	
	public AvailableDateView() {
		setRenderer(new AvailableDateRenderer());
		
		availableStr = dataCenter.getString(Resources.TEXT_LBL_AVAILABLE);
		availableStr2 = dataCenter.getString(Resources.TEXT_LBL_AVAILABLE2);
	}
	
	public void setBounds(int x, int y, int width, int height) {
		bufferedImage = new DVBBufferedImage(width, 72);
		super.setBounds(x, y, width, height);
	}
	
	public void prepare() {
		Hashtable footerTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
		profileImg = (Image) footerTable.get(PreferenceService.BTN_PRO);
		super.prepare();
	}
	
	// VDTRMASTER-5488
	public void setData(MoreDetail md, BaseUI parent, boolean centerAlign) {
		setData(md, parent, centerAlign, false);
	}
	
	public void setData(MoreDetail md, BaseUI parent, boolean centerAlign, boolean listTitleView) {
		if (Log.DEBUG_ON) {
			Log.printDebug("AvailableDateView, setData, md=" + md + ", parent=" + parent + ", centerAlign="
					+ centerAlign);
		}
		prepare();
		stopAnimation();
		needAnimation = false;
		wishStr = null;
		dateStr = "";
		if (md != null && md.getEndPeriod().length() > 0) {
		    // comment out because three screen icon was removed. (enough space)
//			if (listTitleView) {
//				dateStr = availableStr2 + " " + md.getEndPeriod();
//			} else {
//				dateStr = availableStr + " " + md.getEndPeriod();
//			}
            dateStr = availableStr + " " + md.getEndPeriod();
			hasEndPeriod = true;
		} else {
			hasEndPeriod = false;
		}
		
		this.parent = parent;
		
		if (BookmarkManager.getInstance().isInWishlist(md) == null) {
			needAnimation = true;
			wishStr = parent.getWishButtonName(md);
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("AvailableDateView, setData, needAnimation=" + needAnimation + ", wishStr=" + wishStr);
		}
		
		Graphics gg = bufferedImage.getGraphics();
		gg.clearRect(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());

		gg.setColor(availableDateColor);
		if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_FRENCH)) {
			gg.setFont(FontResource.BLENDER.getBoldFont(16));
		} else {
			gg.setFont(FontResource.BLENDER.getBoldFont(17));
		}
		if (centerAlign) {
			GraphicUtil.drawStringCenter(gg, dateStr, bufferedImage.getWidth()/2, 17);
			GraphicUtil.drawStringCenter(gg, dateStr, bufferedImage.getWidth()/2, 65);
		} else {
			gg.drawString(dateStr, 0, 17);
			gg.drawString(dateStr, 0, 65);
		}
		
		if (wishStr != null) {
			gg.setColor(addWishColor);
			if (centerAlign) {
				int x = GraphicUtil.drawStringCenter(gg, wishStr, bufferedImage.getWidth()/2 + 10, 41);
				gg.drawImage(profileImg, x - 30, 26, this);
	
			} else {
				gg.drawImage(profileImg, 0, 26, this);
				gg.drawString (wishStr, 26, 41);
			}
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("AvailableDateView, setData, hasEndPeriod=" + hasEndPeriod);
		}
		
		if (hasEndPeriod) {
			posY = 0;
		} else {
			posY = 24;
			needAnimation = false;
		}
	}
	
	public void startAnimation() {
		start();
		if (needAnimation) {
			aniThread = new Thread("ANIMATION_THREAD") {
				public void run() {
					running = true;
					isAnimating = false;
					while (running) {
					
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							e.printStackTrace();
							return;
						}
						
						if (posY >= 48) {
							posY = 0;
						}
						
						isAnimating = true;
						for (int i = 0; i  < 12; i++) {
							posY += 2;
							repaint();
							try {
								Thread.sleep(80);
							} catch (InterruptedException e) {
								e.printStackTrace();
								return;
							}
						}
						
						isAnimating = false;
					}
				}
			};
			
			aniThread.start();
		}
	}
	
	public void stopAnimation() {
		running = false;
		if (aniThread != null) {
			aniThread.interrupt();
			aniThread = null;
		}
	}
	
	
	class AvailableDateRenderer extends Renderer {
		
		public Rectangle getPreferredBounds(UIComponent c) {
			return c.getBounds();
		}

		public void prepare(UIComponent c) {
			
		}

		protected void paint(Graphics g, UIComponent c) {
			
			g.drawImage(bufferedImage, 0, 0, bufferedImage.getWidth(), 24, 0, posY, bufferedImage.getWidth(), posY + 24, c);
		}
		
	}
}

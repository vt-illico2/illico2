package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Orderable;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.TitleDetailUI;

// Show the result of add/remove to wish list
// or another message (temporary)
public class PopAddWishlist extends UIComponent implements Runnable {
	public final static int WISHLIST_ADDED = 0;
	public final static int WISHLIST_REMOVED = 1;
	public final static int WISHLIST_FAIL = 2;

	public final static int NO_WISHLIST = 3;
	public final static int NO_RESUMEVIEWING = 4;
	public final static int CANT_JUMP = 5;
	public final static int NO_PLAYLIST = 6;

	private static final long serialVersionUID = -7283752523819655114L;

	private static PopAddWishlist instance;

	private String title;
	private BaseUI parent;
	private int type;
	private ClickingEffect clickEffect;

	synchronized public static PopAddWishlist getInstance() {
		if (instance == null) {
			instance = new PopAddWishlist();
		}
		return instance;
	}

	private PopAddWishlist() {
		setRenderer(new PopAddWishlistRenderer());
		setVisible(false);

		clickEffect = new ClickingEffect(this, 5);
	}

	Thread closer;

	public void show(BaseUI comp, Object data, int msgType) {
		Log.printDebug("PopAddWishlist, show()" + ", comp = " + comp
				+ ", data = " + data + ", type = " + msgType);
		parent = comp;
		if (data != null) {
			title = ((MoreDetail) data).getTitle();
		} else {
			title = "";
		}
		type = msgType;
		prepare();
		setVisible(true);

		closer = new Thread(instance, "PopupAutoClose");
		closer.start();
	}

	public void run() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
		close();
	}

	synchronized void close() {
		setVisible(false);
		if (parent != null) {
			parent.popUpClosed(this, new Boolean(type == WISHLIST_ADDED || type == WISHLIST_REMOVED));
			parent = null;
		}
		if (closer != null) {
			closer.interrupt();
			closer = null;
		}
	}

	public boolean handleKey(int key) {
		switch (key) {
		case OCRcEvent.VK_ENTER:
			clickEffect.start(403, 336, 156, 32);
			close();
			break;
		case OCRcEvent.VK_EXIT:
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			close();
			break;
		default:
			break;
		}
		return true;
	}

	private class PopAddWishlistRenderer extends Renderer {
		DataCenter dataCenter = DataCenter.getInstance();
//		private Image i05PopshMid;// = dataCenter.getImage("05_popsh_mid.png");
//		private Image i05PopSha;// = dataCenter.getImage("05_pop_sha.png");
		private Image i05SepMid;// = dataCenter.getImage("05_sep_mid.png");
//		private Image i05HighMid;// = dataCenter.getImage("05_high_mid.png");
		private Image i_05_focus;// = dataCenter.getImage("05_focus.png");
//		private Image i05Focus;// = dataCenter.getImage("05_focus.png");
		private Color popBg = new Color(30, 30, 30);
		private Color c3 = new Color(3, 3, 3);
		private Color cMessage = new Color(229, 229, 229);
		private Color cButton = new Color(3, 3, 3);
		private Color cTitle = new Color(252, 202, 4);
		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);

		private String titleStr;
		private String[] message;
		private String msg;
		
		// R5
		private Image iLittleBoy;
		private FontMetrics fmMsg;

		public void prepare(UIComponent c) {
			titleStr = null;
			message = null;
			if (type == PopAddWishlist.WISHLIST_ADDED) {
				titleStr = dataCenter.getString(Resources.TEXT_T_ADDWISHLIST_SUCCESS);
				msg = title
						+ " "
						+ dataCenter
								.getString(Resources.TEXT_MSG_ADDWISHLIST_SUCESS);
			} else if (type == PopAddWishlist.WISHLIST_REMOVED) {
				titleStr = dataCenter
						.getString(Resources.TEXT_T_DELWISHLIST_SUCCESS);
				msg = title
						+ " "
						+ dataCenter
								.getString(Resources.TEXT_MSG_DELWISHLIST_SUCESS);
			} else if (type == NO_WISHLIST) {
				// R5
				// VDTRMASTER-5500
				BaseUI currentScene = (BaseUI) MenuController.getInstance().getCurrentScene(); 
				if (currentScene.getFooter() != null) {
					boolean isAdult = false;
					if (currentScene.getFooter().getParent() instanceof SimilarContentView) {
						BaseElement element = ((SimilarContentView)currentScene.getFooter().getParent()).getFocusedContent();
						if (element != null && element instanceof Orderable) {
							isAdult = BookmarkManager.getInstance().isAdult((Orderable)element);
						}
						// R5 - support Similar content
						if (((SimilarContentView)currentScene.getFooter().getParent()).hasChannelName()) {
							isAdult = false;
						}
						
					} else {
						isAdult = MenuController.getInstance().isAdult();
					}
					
					if (isAdult) {
						titleStr = dataCenter.getString(Resources.TEXT_MSG_NO_WISH_TITLE_18);
						msg = dataCenter.getString(Resources.TEXT_MSG_NO_WISH_18);
					} else {
						titleStr = dataCenter.getString(Resources.TEXT_MSG_NO_WISH_TITLE);
						msg = dataCenter.getString(Resources.TEXT_MSG_NO_WISH);
					}
				}
				
			} else if (type == NO_PLAYLIST) {
				titleStr = dataCenter.getString(Resources.TEXT_MSG_NO_PLAY_TITLE);
				msg = dataCenter.getString(Resources.TEXT_MSG_NO_PLAY);
			} else if (type == NO_RESUMEVIEWING) {
				titleStr = ("ResumeViewing");
				msg = "Resume viewing is empty.\nPlease watch a content first.\n(This is temporary GUI)";
			} else if (type == CANT_JUMP) {
				titleStr = ("We are SORRY");
				msg = "Your request is rejected.\nYou have to watch the classification infomation for a while.\n(This is temporary GUI)";
			} else {
				titleStr = ("Fail: "
						+ BookmarkManager.getInstance().getLastEcode());
				msg = "We are sorry.";
			}
//			i05PopshMid = dataCenter.getImage("05_popsh_mid.png");
//			i05PopSha = dataCenter.getImage("05_pop_sha.png");
			i05SepMid = dataCenter.getImage("05_sep_mid.png");
//			i05HighMid = dataCenter.getImage("05_high_mid.png");
			i_05_focus = dataCenter.getImage("05_focus.png");
//			i05Focus = dataCenter.getImage("05_focus.png");
			
			Hashtable footerTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
			iLittleBoy = (Image) footerTable.get(PreferenceService.BTN_PRO);
			
			fmMsg = FontResource.BLENDER.getFontMetrics(FontResource.BLENDER.getFont(18));
		}

		public void paint(Graphics g, UIComponent c) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
//			g.drawImage(i05PopshMid, 229, 113, c);
//			g.drawImage(i05PopSha, 259, 382, 442, 79, c);

			g.setColor(popBg);
			g.fillRect(259, 143, 442, 242);
			g.drawImage(i05SepMid, 227, 181, 505, 16, c);
//			g.drawImage(i05HighMid, 259, 143, 442, 200, c);
			//g.drawImage(i05LineMid, 250, 307, this);

			g.setFont(FontResource.BLENDER.getFont(24));
			g.setColor(cTitle);
//			if (titleStr == null) {
//				titleStr = TextUtil.split(title, g.getFontMetrics(), 220);
//			}
//			for (int i = 0; i < titleStr.length && i < 2; i++) {
//				GraphicUtil.drawStringCenter(g, titleStr[i], 480,
//						169 + i * 20);
//			}
			GraphicUtil.drawStringCenter(g, titleStr, 481, 169);

			g.setFont(FontResource.BLENDER.getFont(18));
			if (message == null) {
				message = TextUtil.split(msg, fmMsg, 330, "|");
			}
			int y = 248 - message.length / 2 * 10;
//			if (type >= NO_WISHLIST) {
//				y -= 50;
//			}
			
			g.setColor(cMessage);
			int pkPos = 0;
			for (int i = 0; i < message.length; i++) {
				int yPos = y + i * 20;
				if (( pkPos = message[i].indexOf("[PK]")) > -1) {
					String pre = message[i].substring(0, pkPos);
					String sub = message[i].substring(pkPos + 4);
					int tWidth = fmMsg.stringWidth(message[i]);
					int preWidth = fmMsg.stringWidth(pre);
					int subWidth = fmMsg.stringWidth(sub);
					g.drawString(pre, 480 - tWidth/2, yPos);
					g.drawString(sub, 480 + tWidth/2 - subWidth, yPos);
					
					g.drawImage(iLittleBoy, 480 - tWidth/2 + preWidth, yPos - 15, c);
				} else {
					GraphicUtil.drawStringCenter(g, message[i], 480, yPos);
				}
				
//				GraphicUtil.drawStringCenter(g, message[i], 480, y + i * 20);
			}
			g.drawImage(i_05_focus, 403, 336, c);
			g.setColor(c3);
			g.setFont(FontResource.BLENDER.getFont(18));
			GraphicUtil.drawStringCenter(g, dataCenter
					.getString(Resources.TEXT_OK), 481, 357);
//			super.paint(g, c);
//			g.drawImage(i05Focus, 403, 318, c);
//
//			g.setFont(FontResource.BLENDER.getFont(20));
//			g.setColor(Color.white);
//			if (titleStr == null) {
//				titleStr = TextUtil.split(title, g.getFontMetrics(), 220);
//			}
//			if (titleStr.length < 2) {
//				GraphicUtil.drawStringCenter(g, title, 480, 235);
//			} else {
//				for (int i = 0; i < titleStr.length && i < 2; i++) { // max 2
//																		// lines
//					GraphicUtil.drawStringCenter(g, titleStr[i], 480,
//							227 + i * 20);
//				}
//			}
//
//			g.setFont(FontResource.BLENDER.getFont(16));
//			g.setColor(cMessage);
//			if (message == null) {
//				message = TextUtil.split(msg, g.getFontMetrics(), 285);
//			}
//			if (message.length < 2) {
//				GraphicUtil.drawStringCenter(g, msg, 480, 287);
//			} else {
//				int y = 280;
//				if (type >= NO_WISHLIST) {
//					y -= 50;
//				}
//				for (int i = 0; i < message.length; i++) {
//					GraphicUtil.drawStringCenter(g, message[i], 480,
//							y + i * 20);
//				}
//			}
//			g.setFont(FontResource.BLENDER.getFont(18));
//			g.setColor(cButton);
//			GraphicUtil.drawStringCenter(g, dataCenter
//					.getString(Resources.TEXT_OK), 481, 339);
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Resources.fullRec;
		}
	}
}

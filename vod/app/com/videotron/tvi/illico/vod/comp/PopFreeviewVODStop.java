package com.videotron.tvi.illico.vod.comp;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.*;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.PlayScreenUI;
import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import java.awt.*;
import java.awt.Image;
import java.util.Date;
import java.util.Hashtable;

public class PopFreeviewVODStop extends BaseUI {
	public static final int PLAY_FROM_BEGINNING = PopVODStop.PLAY_FROM_BEGINNING;
	public static final int BACK_TO_MENU = PopVODStop.BACK_TO_MENU;
	public static final int VIEW_NEXT = PopVODStop.VIEW_NEXT;
	public static final int LIVE_TV = PopVODStop.LIVE_TV;
	public static final int CANCEL = PopVODStop.CANCEL;
	public static final int ORDER_PARTYPACK = PopVODStop.ORDER_PARTYPACK;
	public static final int ORDER_NEXT = PopVODStop.ORDER_NEXT;
	public static final int FREE_WATCH_NOW = 7;
	public static final int SUBSCRIBE = 8;
	public static final int VIEW_ALL_EPISODES = 9;
	public static final int RESUME_VIEWING = PopVODStop.RESUME_VIEWING;

	private static final long serialVersionUID = -5635279541670147559L;
	private Video title;
	private Episode nextEpisode;
	private BaseUI parent;
	private int[] buttons;
	private ClickingEffect clickEffect;

	public PopFreeviewVODStop() {
		setRenderer(new PopFreeviewVODStopRenderer());
		prepare();
		setVisible(false);
		footer.addButton((Image) ((Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG))
				.get(PreferenceService.BTN_EXIT), Resources.TEXT_BACK_TO_TV);
		footer.setBounds(632, 463, 223, 28);
		add(footer);
		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, Video title, boolean resume, boolean playFromBegin, Episode nextEpisode) {
		if (Log.DEBUG_ON) {
			Log.printDebug(
					"PopFreeviewVODStop, comp=" + comp + ", resume=" + resume + ", playFromBegin=" + playFromBegin);
		}
		parent = comp;
		int btnCnt = 3;
		if (resume) {
			btnCnt++;
		}

		if (playFromBegin) {
			btnCnt++;
		}
		footer.setBounds(632, 469, 223, 28);
		// VDTRMASTER-5956
		if (nextEpisode == null) {
			btnCnt -= 1;
			footer.setBounds(632, 389, 223, 28);
		}

		buttons = new int[btnCnt];
		int btnIdx = 0;
		if (resume) {
			buttons[btnIdx++] = RESUME_VIEWING;
		}
		if (playFromBegin) {
			buttons[btnIdx++] = PLAY_FROM_BEGINNING;
		}

		buttons[btnIdx++] = BACK_TO_MENU;

		// VDTRMASTER-5956
		if (nextEpisode != null) {
			if (nextEpisode.isFreeview) {
				buttons[btnIdx++] = FREE_WATCH_NOW;
			} else {
				buttons[btnIdx++] = SUBSCRIBE;
			}

			buttons[btnIdx] = VIEW_ALL_EPISODES;
		} else {
			buttons[btnIdx++] = SUBSCRIBE;
		}

		this.title = title;
		this.nextEpisode = nextEpisode;
		prepare();
		// VDTRMASTER-5954 && VDTRMASTER-5956
		if (resume || nextEpisode == null) {
			focus = 0;
		} else {
			focus = 2;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug(
					"PopFreeviewVODStop title :" + (title == null ? "NULL" : title.toString(true)) + ", nextEpisode : "
							+ (nextEpisode == null ? "NULL" : nextEpisode.toString(true)) + ", button = "
							+ buttons.length);
		}
		setVisible(true);

		// R7.3
		Video targetTitle = this.title;
		if (nextEpisode != null) {
			targetTitle = nextEpisode;
		}
		if (targetTitle != null) {
			final Video targetTitleFin = targetTitle;
			new Thread("LOAD_CHANNLE_LOG") {
				public void run() {
					if (targetTitleFin.channel != null) {
						targetTitleFin.channel.loadLogo(false, true);
					} else if (targetTitleFin.network != null) {
						targetTitleFin.network.loadLogo(false, true);
					} else if (targetTitleFin.service != null && targetTitleFin.service[0].description != null) {
						targetTitleFin.service[0].description.loadLogo(false, true);
					}
					repaint();
				}
			}.start();
		}
	}

	public void keyOK() {
		parent.popUpClosed(this, new Integer(buttons[focus]));
	}

	public void keyLeft() {
	}

	public void keyRight() {
	}

	public void keyUp() {
		if (focus > 0) {
			focus--;
			repaint();
		}
	}

	public void keyDown() {
		if (focus < buttons.length - 1) {
			focus++;
			repaint();
		}
	}

	public void keyBack() {
		parent.popUpClosed(this, new Integer(BACK_TO_MENU));
	}

	public boolean handleKey(int key) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PopVODStop handleKey " + key);
		}
		switch (key) {
		case HRcEvent.VK_ENTER:
			int focusY = 0;
			final int STEP = 39;

			// VDTRMASTER-5956
			if (buttons.length == 2) {
				focusY = 222 + focus * STEP;
			} else if (buttons.length == 3) {
				focusY = 202 + focus * STEP;
			} else if (buttons.length == 4) {
				if (focus < 2) {
					focusY = 102 + focus * STEP;
				} else {
					focusY = 361 + (focus - 2) * STEP;
				}
			} else {
				// buttons.length == 5
				if (focus < 3) {
					focusY = 82 + focus * STEP;
				} else {
					focusY = 361 + (focus - 3) * STEP;
				}
			}

			if (nextEpisode != null) {
				if (buttons.length == 4) {
					if (focus < 2) {
						focusY = 102 + focus * STEP;
					} else {
						focusY = 361 + (focus - 2) * STEP;
					}
				} else {
					// buttons.length == 5
					if (focus < 3) {
						focusY = 82 + focus * STEP;
					} else {
						focusY = 361 + (focus - 3) * STEP;
					}
				}
			} else {
				if (buttons.length == 3) {
					if (focus < 2) {
						focusY = 172 + focus * STEP;
					} else {
						focusY = 314 + (focus - 2) * STEP;
					}
				} else if (buttons.length == 4) {
					if (focus < 3) {
						focusY = 152 + focus * STEP;
					} else {
						focusY = 314 + (focus - 3) * STEP;
					}
				}
			}

			clickEffect.start(533, focusY, 292, 50);
			keyOK();
			break;
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case KeyCodes.LAST:
			// keyBack();
			break;
		case OCRcEvent.VK_EXIT:
		case OCRcEvent.VK_LIVE:
			footer.clickAnimation(0);
			parent.popUpClosed(this, new Integer(LIVE_TV));
			break;

		case OCRcEvent.VK_PLAY:
		case OCRcEvent.VK_PAUSE:
		case OCRcEvent.VK_STOP:
		case OCRcEvent.VK_FAST_FWD:
		case OCRcEvent.VK_REWIND:
		case OCRcEvent.VK_RECORD:
		case OCRcEvent.VK_FORWARD:
		case OCRcEvent.VK_BACK:
			// DDC-113 Visually Impaired : “STAR” key is blocked.
		case KeyCodes.STAR:
			break;
		}
		return true;
	}

	private String getButtonName(int button) {
		switch (button) {
		case PLAY_FROM_BEGINNING:
			return dataCenter.getString(Resources.TEXT_PLAY_FROM_BEGIN);
		case RESUME_VIEWING:
			return dataCenter.getString(Resources.TEXT_RESUME_VIEW);
		case BACK_TO_MENU:
			return dataCenter.getString(Resources.TEXT_BACK_TO_MENU);
		case SUBSCRIBE:
		case FREE_WATCH_NOW:
			String labelStr = "";
			String buttonInfo = "";

			// R7.3
			if (Log.DEBUG_ON) {
				Log.printDebug("PopFreeviewVODStop, getButtonName, nextEpisode=" + nextEpisode);
			}

			Video targetVideo = title;
			if (nextEpisode != null) {
				targetVideo = nextEpisode;
			}

			if (targetVideo != null) {
				if (targetVideo.channel != null) {
					String watch = null;

					if (targetVideo.channel.network == null) { // 1 channel
						if (targetVideo.channel.hasAuth == false) {
							targetVideo.channel.hasAuth = CommunicationManager.getInstance()
									.checkAuth(targetVideo.channel.callLetters);
						}
						if (targetVideo.channel.hasAuth) {
							watch = targetVideo.channel.callLetters;
						}
					} else {
						for (int i = 0; i < targetVideo.channel.network.channel.length; i++) {
							if (targetVideo.channel.network.channel[i].hasAuth == false) {
								targetVideo.channel.network.channel[i].hasAuth = CommunicationManager.getInstance()
										.checkAuth(targetVideo.channel.network.channel[i].callLetters);
							}
							if (targetVideo.channel.network.channel[i].hasAuth) {
								watch = targetVideo.channel.network.channel[i].callLetters;
								break;
							}
						}
					}
					if (watch == null || !targetVideo.hasPacakgeForServiceConfigType()) {
						if (targetVideo.isFreeview && nextEpisode != null) {
							labelStr = "!" + dataCenter.getString(Resources.BTN_FREE_WATCH_NOW);
							buttonInfo = "nextEpisode is freeeview";
						} else {
							if (CommunicationManager.getInstance().isaSupported(targetVideo.channel.callLetters)
									|| !CommunicationManager.getInstance()
									.isIncludedInExcludedData(targetVideo.channel.callLetters)) {
								labelStr = "+" + dataCenter.getString(Resources.TEXT_SUBSCRIBE);
							} else {
								labelStr = dataCenter.getString(Resources.TEXT_CHANGE_MY_TV_PKG);
							}
							buttonInfo = "STB has no channel rights";
						}

					} else {
						if (targetVideo.isFreeview) {
							labelStr = "!" + dataCenter.getString(Resources.BTN_FREE_WATCH_NOW);
							buttonInfo = "nextEpisode is freeeview";
						} else {
							labelStr = dataCenter.getString(Resources.TEXT_WATCH);
							buttonInfo = watch + " is subscribed";
						}
					}
				} else if (CommunicationManager.getInstance().checkPackage(targetVideo.service) == false || !nextEpisode
						.hasPacakgeForServiceConfigType()) {
					if (CommunicationManager.getInstance().hasAnyPackage()) {
						if (targetVideo.isFreeview && nextEpisode != null) {
							labelStr = "!" + dataCenter.getString(Resources.BTN_FREE_WATCH_NOW);
							buttonInfo = "nextEpisode is freeeview";
						} else {
							labelStr = (CommunicationManager.getInstance().isaSupported(targetVideo.service[0].getId2())
									|| !CommunicationManager.getInstance()
									.isIncludedInExcludedData(targetVideo.channel.callLetters) ? "+" : "") + dataCenter
									.getString(Resources.TEXT_SUBSCRIBE_SVOD) + targetVideo.getServiceTitle();
							buttonInfo = "No " + targetVideo.dumpService();
						}
					} else {
						buttonInfo = "Err or No vod_category_config";
					}
				} else {
					if (targetVideo.isFreeview) {
						labelStr = "!" + dataCenter.getString(Resources.BTN_FREE_WATCH_NOW);
						buttonInfo = "It's freeview and no channel info";
					} else {
						labelStr = dataCenter.getString(Resources.TEXT_WATCH);
						buttonInfo = "It's normal and no channel info";
					}
				}
			}
			Log.printDebug("PopFreeviewVODStop, getButtonName, buttonInfo=" + buttonInfo);
			return labelStr;
		case VIEW_ALL_EPISODES:
			return dataCenter.getString(Resources.LBL_VIEW_ALL_EPISODES);
		default:
			return " ";
		}
	}

	public String getFocusedButton() {
		return getButtonName(buttons[focus]);
	}

	private class PopFreeviewVODStopRenderer extends Renderer {

		private Image i_01_acbg_1st, i_01_acbg_2nd, i_01_acbg_m, i_01_acline;
		private Image i_pop_sh_13, i_02_detail_ar, i_02_icon_isa_ac, i_02_icon_isa_bl;
		private Image i_02_detail_bt_foc, i_03_srcicon_ppv_foc, i_03_srcicon_ppv_foc2;
		private Image i_slash_clar, iconG, icon8, icon13, icon16, icon18, iconFr, iconEn, iconCc;
		private Image i_icon_event;

		private Color cDimmedBG = new DVBColor(0, 0, 0, 204);
		private Color C058058058 = new Color(58, 58, 58);
		private Color C044044044 = new Color(44, 44, 44);
		private Color C124124124 = new Color(124, 124, 124);
        private Color C172172172 = new Color(172,172, 172);

		public void prepare(UIComponent c) {
			i_01_acbg_1st = dataCenter.getImage("01_acbg_1st.png");
			i_01_acbg_2nd = dataCenter.getImage("01_acbg_2nd.png");
			i_01_acbg_m = dataCenter.getImage("01_acbg_m.png");
			i_01_acline = dataCenter.getImage("01_acline.png");
			i_pop_sh_13 = dataCenter.getImage("pop_sh_13.png");
			i_02_detail_ar = dataCenter.getImage(("02_detail_ar.png"));
			i_02_icon_isa_ac = dataCenter.getImage("02_icon_isa_ac.png");
			i_02_icon_isa_bl = dataCenter.getImage("02_icon_isa_bl.png");
			i_02_detail_bt_foc = dataCenter.getImage("02_detail_bt_foc.png");
			i_03_srcicon_ppv_foc = dataCenter.getImage("03_srcicon_ppv_foc.png");
			i_03_srcicon_ppv_foc2 = dataCenter.getImage("03_srcicon_ppv_foc2.png");
			i_slash_clar = dataCenter.getImage("slash_clar.png");

			iconG = dataCenter.getImage("icon_g.png");
			icon8 = dataCenter.getImage("icon_8.png");
			icon13 = dataCenter.getImage("icon_13.png");
			icon16 = dataCenter.getImage("icon_16.png");
			icon18 = dataCenter.getImage("icon_18.png");
			iconFr = dataCenter.getImage("icon_fr.png");
			iconEn = dataCenter.getImage("icon_en.png");
			iconCc = dataCenter.getImage("icon_cc.png");

			i_icon_event = dataCenter.getImage("icon_event.png");
		}

		public void paint(Graphics g, UIComponent c) {
			final int STEP = 39;

			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			if (nextEpisode != null) {
				// background
				g.setColor(C044044044);
				g.fillRect(110, 60, 745, 162);
				g.setColor(C058058058);
				g.fillRect(110, 222, 745, 240);
				g.drawImage(i_pop_sh_13, 110, 209, 745, 13, c);

				// Current Episdoe
				g.setColor(C124124124);
				g.setFont(FontResource.BLENDER.getFont(17));
				g.drawString(dataCenter.getString(Resources.LBL_NOW_PALING), 154, 120);

				// series name + season number
				g.setColor(ColorUtil.getColor(255, 205, 12));
				g.setFont(FontResource.BLENDER.getFont(18));

				if (title instanceof Episode) {
					Episode currentEpisode = (Episode) title;
					try {
						String seasonStr = currentEpisode.season.series.getTitle() + " - " + dataCenter
								.getString(Resources.TEXT_LBL_SEASON) + " " + currentEpisode.season.seasonNumber;
						g.drawString(seasonStr, 154, 139);
					} catch (Exception e) {
						Log.printDebug("PopFreeviewVODStop, currentEpisode=" + currentEpisode);
						if (currentEpisode.season != null) {
							Log.printDebug("PopFreeviewVODStop, currentEpisode.season=" + currentEpisode.season);

							if (currentEpisode.season.series != null) {
								Log.printDebug("PopFreeviewVODStop, currentEpisode.season.series="
										+ currentEpisode.season.series);
							}
						}
						e.printStackTrace();
					}
					// episode number
					g.setFont(FontResource.BLENDER.getFont(27));
					String episodeStr =
							dataCenter.getString(Resources.TEXT_LBL_EPISODE) + " " + currentEpisode.episodeNumber;
					g.drawString(episodeStr, 154, 168);

					// VDTRMASTER-5990
					// Freeview icon + free episode
					g.drawImage(i_03_srcicon_ppv_foc2, 280, 153, c);
					g.setFont(FontResource.BLENDER.getFont(18));
					g.drawString(dataCenter.getString(Resources.LBL_FREE_EPISODE), 307, 166);
				} else { // VDTRMASTER-5990
					g.setFont(FontResource.BLENDER.getFont(27));
					String titleStr = TextUtil
							.shorten(title.getTitle(), FontResource.getFontMetrics(FontResource.BLENDER.getFont(27)),
									345);
					g.drawString(titleStr, 154, 158);

					g.drawImage(i_03_srcicon_ppv_foc2, 154, 173, c);
					g.setFont(FontResource.BLENDER.getFont(18));
					g.drawString(dataCenter.getString(Resources.TEXT_LBL_FREE), 181, 186);
				}

				// button bg
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(ColorUtil.getColor(230, 230, 230));
				if (buttons.length == 4) {
					g.drawImage(i_01_acbg_1st, 535, 102, c);
					g.drawImage(i_01_acbg_2nd, 535, 144, c);
					g.drawImage(i_01_acbg_m, 535, 180, c);
					g.drawImage(i_01_acline, 550, 141, c);

					for (int i = 0; i < 2; i++) {
						g.drawImage(i_02_detail_ar, 558, 120 + i * STEP, c);
						g.drawString(getButtonName(buttons[i]), 580, 128 + i * STEP);
					}

				} else {
					// buttons.length == 5
					g.drawImage(i_01_acbg_1st, 535, 82, c);
					g.drawImage(i_01_acbg_2nd, 535, 124, c);
					g.drawImage(i_01_acbg_m, 535, 160, 284, 40, c);
					g.drawImage(i_01_acline, 550, 121, c);
					g.drawImage(i_01_acline, 550, 160, c);

					for (int i = 0; i < 3; i++) {
						g.drawImage(i_02_detail_ar, 558, 100 + i * STEP, c);
						g.drawString(getButtonName(buttons[i]), 580, 108 + i * STEP);
					}
				}

				// Next Episode
				// Button bg
				g.drawImage(i_01_acbg_1st, 535, 361, c);
				g.drawImage(i_01_acbg_2nd, 535, 403, c);
				g.drawImage(i_01_acbg_m, 535, 439, c);
				g.drawImage(i_01_acline, 550, 400, c);

				// next episode
				g.setColor(C124124124);
				g.setFont(FontResource.BLENDER.getFont(17));
				g.drawString(dataCenter.getString(Resources.LBL_NEXT_EPISODE), 154, 261);

				// next episode series name + seaosn number
				g.setColor(ColorUtil.getColor(255, 255, 255));
				g.setFont(FontResource.BLENDER.getFont(18));
				try {
					String seasonStr = nextEpisode.season.series.getTitle() + " - " + dataCenter
							.getString(Resources.TEXT_LBL_SEASON) + " " + nextEpisode.season.seasonNumber;
					g.drawString(seasonStr, 154, 280);
				} catch (Exception e) {
					Log.printDebug("PopFreeviewVODStop, nextEpisode=" + nextEpisode);
					if (nextEpisode.season != null) {
						Log.printDebug("PopFreeviewVODStop, nextEpisode.season=" + nextEpisode.season);

						if (nextEpisode.season.series != null) {
							Log.printDebug(
									"PopFreeviewVODStop, nextEpisode.season.series=" + nextEpisode.season.series);
						}
					}
					e.printStackTrace();
				}

				// next episode number
				g.setFont(FontResource.BLENDER.getFont(27));
				String episodeStr = dataCenter.getString(Resources.TEXT_LBL_EPISODE) + " " + nextEpisode.episodeNumber;
				g.drawString(episodeStr, 154, 309);

				// Freeview icon + free episode
				if (nextEpisode.isFreeview) {
					g.drawImage(i_icon_event, 275, 288, c);
					g.setFont(FontResource.BLENDER.getFont(18));
					g.drawString(dataCenter.getString(Resources.LBL_FREE_EPISODE), 307, 307);
				}

				// next episode info
				g.setColor(ColorUtil.getColor(200, 200, 200));
				g.setFont(FontResource.BLENDER.getFont(18));

				int infoX = 154;
				String runTimeStr = Formatter.getCurrent().getDurationText(nextEpisode.getRuntime());
				g.drawString(runTimeStr, infoX, 336);
				infoX += (g.getFontMetrics().stringWidth(runTimeStr) + 10);

				String genreStr = nextEpisode.getGenre();
				if (genreStr != null) {
					g.drawImage(i_slash_clar, infoX, 325, c);
					infoX += 12;
					g.drawString(genreStr, infoX, 336);
					infoX += (g.getFontMetrics().stringWidth(genreStr) + 10);
				}

				g.drawImage(i_slash_clar, infoX, 325, c);
				infoX += 12;

				// icons - lang, rating, cc
				if (nextEpisode.hasEnglish()) {
					g.drawImage(iconEn, infoX, 325, c);
					infoX = infoX + iconEn.getWidth(c) + 3;
				}
				if (nextEpisode.hasFrench()) {
					g.drawImage(iconFr, infoX, 325, c);
					infoX = infoX + iconFr.getWidth(c) + 3;
				}
				Image rating = getRatingImage(nextEpisode.getRating());
				if (rating != null) {
					g.drawImage(rating, infoX, 325, c);
					infoX = infoX + rating.getWidth(c) + 3;
				}
				if (nextEpisode.hasClosedCaption()) {
					g.drawImage(iconCc, infoX, 325, c);
				}

				// explain
				g.setColor(ColorUtil.getColor(172, 172, 172, 230));
				g.setFont(FontResource.BLENDER.getFont(16));

				String[] explainStr = TextUtil.split(nextEpisode.getDescription(), g.getFontMetrics(), 345, 5);
				for (int i = 0; i < explainStr.length; i++) {
					g.drawString(explainStr[i], 153, 369 + i * 16);
				}

				// logo
				Image logo = null;
				if (nextEpisode.channel != null) {
					logo = nextEpisode.channel.logos[BrandingElement.LOGO_LG];
				} else if (nextEpisode.network != null) {
					logo = nextEpisode.network.logos[BrandingElement.LOGO_LG];
				} else if (nextEpisode.service != null && nextEpisode.service[0].description != null) {
					logo = nextEpisode.service[0].description.logos[BrandingElement.LOGO_LG];
				}

				if (logo != null) {
					// 678, 270;
					g.drawImage(logo, 678 - logo.getWidth(c) / 2, 270 - logo.getHeight(c) / 2, c);
				}

				// subscribe explain
				if (getButtonName(buttons[buttons.length - 2]).charAt(0) == '+') {
					g.setColor(ColorUtil.getColor(255, 205, 12));
					g.setFont(FontResource.BLENDER.getFont(17));
					String[] subscribeExplainArr = TextUtil
							.split(dataCenter.getString(Resources.LBL_SUBSCRIBE_EXPLAIN), g.getFontMetrics(), 250);

					for (int i = 0; i < subscribeExplainArr.length; i++) {
						g.drawString(subscribeExplainArr[i], 538, 332 + i * 17);
					}
				}

				// button
				g.drawImage(i_01_acbg_1st, 535, 361, c);
				g.drawImage(i_01_acbg_2nd, 535, 403, c);
				g.drawImage(i_01_acbg_m, 535, 439, c);
				g.drawImage(i_01_acline, 550, 400, c);
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(ColorUtil.getColor(230, 230, 230));

				for (int i = 0; i < 2; i++) {
					String buttonStr = getButtonName(buttons[buttons.length - 2 + i]);
					// VDTRMASTER-5919
					if (buttonStr.charAt(0) == '!') {
						buttonStr = buttonStr.substring(1);
						g.drawImage(i_03_srcicon_ppv_foc2, 551, 374, c);
					} else if (buttonStr.charAt(0) == '+') {
						buttonStr = buttonStr.substring(1);
						g.drawImage(i_02_icon_isa_ac, 551, 372, c);
					} else {
						g.drawImage(i_02_detail_ar, 558, 379 + i * STEP, c);
					}
					g.drawString(buttonStr, 580, 387 + i * STEP);
				}

				// focus button
				g.setFont(FontResource.BLENDER.getFont(21));
				g.setColor(ColorUtil.getColor(3, 3, 3));

				int focusY = 0;
				if (buttons.length == 4) {
					if (focus < 2) {
						focusY = 128 + focus * STEP;
					} else {
						focusY = 387 + (focus - 2) * STEP;
					}
				} else {
					// buttons.length == 5
					if (focus < 3) {
						focusY = 108 + focus * STEP;
					} else {
						focusY = 387 + (focus - 3) * STEP;
					}
				}
				g.drawImage(i_02_detail_bt_foc, 533, focusY - 26, c);

				String buttonStr = getButtonName(buttons[focus]);

				// VDTRMASTER-5919
				if (buttonStr.charAt(0) == '!') {
					buttonStr = buttonStr.substring(1);
					g.drawImage(i_03_srcicon_ppv_foc, 551, 374, c);
				} else if (buttonStr.charAt(0) == '+') {
					buttonStr = buttonStr.substring(1);
					g.drawImage(i_02_icon_isa_bl, 551, 372, c);
				}
				g.drawString(buttonStr, 580, focusY);
			} else {
				// VDTRMASTER-5956
				// in the case of nextEpisode is null.
				g.setColor(C044044044);
				g.fillRect(110, 130, 745, 162);
				g.setColor(C058058058);
				g.fillRect(110, 292, 745, 87);
				g.drawImage(i_pop_sh_13, 110, 279, 745, 13, c);

				// Current Episdoe
				g.setColor(C124124124);
				g.setFont(FontResource.BLENDER.getFont(17));
				g.drawString(dataCenter.getString(Resources.LBL_NOW_PALING), 154, 190);

				if (title instanceof Episode) {
					// series name + season number
					g.setColor(ColorUtil.getColor(255, 205, 12));
					g.setFont(FontResource.BLENDER.getFont(18));

					Episode currentEpisode = (Episode) title;
					try {
						String seasonStr = currentEpisode.season.series.getTitle() + " - " + dataCenter
								.getString(Resources.TEXT_LBL_SEASON) + " " + currentEpisode.season.seasonNumber;
						g.drawString(seasonStr, 154, 209);
					} catch (Exception e) {
						Log.printDebug("PopFreeviewVODStop, currentEpisode=" + currentEpisode);
						if (currentEpisode.season != null) {
							Log.printDebug("PopFreeviewVODStop, currentEpisode.season=" + currentEpisode.season);

							if (currentEpisode.season.series != null) {
								Log.printDebug("PopFreeviewVODStop, currentEpisode.season.series="
										+ currentEpisode.season.series);
							}
						}
						e.printStackTrace();
					}
					// episode number
					g.setFont(FontResource.BLENDER.getFont(27));
					String episodeStr =
							dataCenter.getString(Resources.TEXT_LBL_EPISODE) + " " + currentEpisode.episodeNumber;
					g.drawString(episodeStr, 154, 238);

					// VDTRMASTER-5990
					// Freeview icon + free episode
					g.drawImage(i_03_srcicon_ppv_foc2, 280, 223, c);
					g.setFont(FontResource.BLENDER.getFont(18));
					g.drawString(dataCenter.getString(Resources.LBL_FREE_EPISODE), 307, 236);
				} else { // VDTRMASTER-5990
					g.setFont(FontResource.BLENDER.getFont(27));
					String titleStr = TextUtil
							.shorten(title.getTitle(), FontResource.getFontMetrics(FontResource.BLENDER.getFont(27)),
									345);
					g.drawString(titleStr, 154, 215);

					g.drawImage(i_03_srcicon_ppv_foc2, 150, 223, c);
					g.setFont(FontResource.BLENDER.getFont(18));
					g.drawString(dataCenter.getString(Resources.TEXT_LBL_FREE), 177, 236);
				}

                // logo
                Image logo = null;
                if (title.channel != null) {
                    logo = title.channel.logos[BrandingElement.LOGO_LG];
                } else if (title.network != null) {
                    logo = title.network.logos[BrandingElement.LOGO_LG];
                } else if (title.service != null && title.service[0].description != null) {
                    logo = title.service[0].description.logos[BrandingElement.LOGO_LG];
                }

                if (logo != null) {
                    g.drawImage(logo, 151, 321, 100, 30, c);
                }

                // explain for subscription
                g.setColor(C172172172);
                g.setFont(FontResource.BLENDER.getFont(17));
                String msgStr = dataCenter.getString(Resources.LBL_WITHOUT_NEXT_EPISODE_SUBSCRIBE_EXPLAIN);
                // VDTRMASTER-6087
                // To display a TOC under 2 lines. adjust the width.
                String[] msgArr = TextUtil.split(msgStr, g.getFontMetrics(), 235);
                for (int i = 0 ; i < msgArr.length; i++) {
                    g.drawString(msgArr[i], 281, 333 + i * 16);
                }

				// button bg
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(ColorUtil.getColor(230, 230, 230));
				if (buttons.length == 3) {
					g.drawImage(i_01_acbg_1st, 535, 172, c);
					g.drawImage(i_01_acbg_2nd, 535, 214, c);
					g.drawImage(i_01_acbg_m, 535, 250, c);
					g.drawImage(i_01_acline, 550, 211, c);

					for (int i = 0; i < 2; i++) {
						g.drawImage(i_02_detail_ar, 558, 190 + i * STEP, c);
						g.drawString(getButtonName(buttons[i]), 580, 198 + i * STEP);
					}
				} else {
					// buttons.length == 4
					g.drawImage(i_01_acbg_1st, 535, 152, c);
					g.drawImage(i_01_acbg_2nd, 535, 194, c);
					g.drawImage(i_01_acbg_m, 535, 230, 284, 40, c);
					g.drawImage(i_01_acline, 550, 191, c);
					g.drawImage(i_01_acline, 550, 230, c);

					for (int i = 0; i < 3; i++) {
						g.drawImage(i_02_detail_ar, 558, 170 + i * STEP, c);
						g.drawString(getButtonName(buttons[i]), 580, 178 + i * STEP);
					}
				}

				// subscribe button
				g.drawImage(i_01_acbg_1st, 535, 314, c);
				String buttonStr = getButtonName(buttons[buttons.length - 1]);
				if (buttonStr.charAt(0) == '!') {
					buttonStr = buttonStr.substring(1);
					g.drawImage(i_03_srcicon_ppv_foc2, 551, 330, c);
				} else if (buttonStr.charAt(0) == '+') {
					buttonStr = buttonStr.substring(1);
					g.drawImage(i_02_icon_isa_ac, 551, 328, c);
				} else {
					g.drawImage(i_02_detail_ar, 558, 335, c);
				}
				g.drawString(buttonStr, 580, 341);

				// focus button
				g.setFont(FontResource.BLENDER.getFont(21));
				g.setColor(ColorUtil.getColor(3, 3, 3));

				int focusY = 0;
				if (buttons.length == 3) {
					if (focus < 2) {
						focusY = 172 + focus * STEP;
					} else {
						focusY = 314 + (focus - 2) * STEP;
					}
				} else {
					// buttons.length == 4
					if (focus < 3) {
						focusY = 152 + focus * STEP;
					} else {
						focusY = 314 + (focus - 3) * STEP;
					}
				}
				g.drawImage(i_02_detail_bt_foc, 533, focusY, c);

				buttonStr = getButtonName(buttons[focus]);
				if (buttonStr.charAt(0) == '!') {
					buttonStr = buttonStr.substring(1);
					g.drawImage(i_03_srcicon_ppv_foc, 551, focusY + 16, c);
				} else if (buttonStr.charAt(0) == '+') {
					buttonStr = buttonStr.substring(1);
					g.drawImage(i_02_icon_isa_bl, 551, focusY + 14, c);
				}
				g.drawString(buttonStr, 580, focusY + 26);
			}
		}

		public Image getRatingImage(String rating) {
			Image ratingImg = null;
			if (rating != null) {
				if (rating.equalsIgnoreCase(Definition.RATING_G)) {
					ratingImg = iconG;
				} else if (rating.equals(Definition.RATING_OVER_8)) {
					ratingImg = icon8;
				} else if (rating.equals(Definition.RATING_OVER_13)) {
					ratingImg = icon13;
				} else if (rating.equals(Definition.RATING_OVER_16)) {
					ratingImg = icon16;
				} else if (rating.equals(Definition.RATING_OVER_18)) {
					ratingImg = icon18;
				}
			}

			return ratingImg;
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Resources.fullRec;
		}
	}
}

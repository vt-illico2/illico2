package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ArrowEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.ChannelInfoManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Description;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Network;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.data.vcds.type.Series;
import com.videotron.tvi.illico.vod.data.vcds.type.Showcase;
import com.videotron.tvi.illico.vod.data.vcds.type.ShowcaseDetails;
import com.videotron.tvi.illico.vod.data.vcds.type.Topic;
import com.videotron.tvi.illico.vod.data.vcds.type.TopicPointer;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;

public class ShowcaseView extends UIComponent {
	private static DataCenter dataCenter = DataCenter.getInstance();

	CategoryContainer category;
	MoreDetail[] data = new MoreDetail[0];
	int dataMax;

	HashMap dataForRenderer;
	Rectangle[] area;
	boolean freshArea;
	boolean arrowUp, arrowDn, arrowLeft, arrowRight;

	private Effect showingEffect;
	private Effect hidingEffect;
	private ArrowEffect arrowEffect;
	private boolean isAllLandscape;

	// R5
	private boolean[] isInWishListArray = new boolean[15];
	private boolean[] isBlockContentArray = new boolean[15];
	private boolean[] isPreviewArray = new boolean[15];

	public ShowcaseView() {
		setRenderer(new ShowcaseViewRenderer());
		prepare();
		setBounds(355, 110, 540, 370);

		dataForRenderer = new HashMap();

		showingEffect = new AlphaEffect(this, 2, AlphaEffect.FADE_IN);
		hidingEffect = new AlphaEffect(this, 2, AlphaEffect.FADE_OUT);
		arrowEffect = new ArrowEffect(this, 0);
	}

	public boolean handleKey(int key) {
		Log.printDebug("ShowcaseView, handleKey(), code = " + key);
		boolean ret = true;
		switch (key) {
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_ENTER:
		case HRcEvent.VK_INFO:
			keyOK();
			break;
		// case KeyCodes.LAST:
		// keyBack();
		// break;
		default:
			ret = false;
			break;
		}
		return ret;
	}

	public boolean keyLeft() {
		if (area[focus].x == LEFT_X) {
			((CategoryUI) getParent()).setCurrentFocusUI(CategoryUI.FOCUS_MENU);

			// 20190222 Touche project 
			VbmController.getInstance().backwardHistory();
			return true;
		}

		// find nearest one from left side
		int candidate = focus;
		int gap = Integer.MAX_VALUE;
		for (int i = 0; i < area.length; i++) {
			if (area[focus].y > (area[i].y + area[i].height) // check vertical
					// range
					|| (area[focus].y + area[focus].height) < area[i].y)
				continue;
			if (area[i].x >= area[focus].x)
				continue; // it's locate on right side or same position
			if (gap > area[focus].x - area[i].x) {
				gap = area[focus].x - area[i].x;
				candidate = i;
			}
		}
		if (focus != candidate) {
			Rectangle arrowBounds = new Rectangle(area[focus]);
			arrowBounds.x -= getX();
			arrowBounds.y -= getY();
			arrowBounds.x -= 1 + 17; // 17 == width of arrow image
			arrowBounds.y += area[focus].height / 2 - 25 / 2; // 25 == height of
																// arrow image
			// arrowEffect.start(arrowBounds, ArrowEffect.LEFT);
			focus = candidate;
			checkFocusArrow();
			repaint();
		}
		return false;
	}

	public boolean keyRight() {
		// find nearest one from right side
		int candidate = focus;
		int gap = Integer.MAX_VALUE;
		for (int i = 0; i < area.length; i++) {
			// check vertical range
			if (area[focus].y > (area[i].y + area[i].height) || (area[focus].y + area[focus].height) < area[i].y)
				continue;
			if (area[i].x <= area[focus].x)
				continue; // it's locate on left side or same position
			if (gap > area[i].x - area[focus].x) {
				gap = area[i].x - area[focus].x;
				candidate = i;
			}
		}
		if (focus == candidate) {
			gap = Integer.MAX_VALUE;
			for (int i = 0; i < area.length; i++) {
				if (area[i].x <= area[focus].x)
					continue; // it's locate on left side or same position
				if (gap > area[i].x - area[focus].x) {
					gap = area[i].x - area[focus].x;
					candidate = i;
				}
			}
		}
		if (focus != candidate) {
			Rectangle arrowBounds = new Rectangle(area[focus]);
			arrowBounds.x -= getX();
			arrowBounds.y -= getY();
			arrowBounds.x += area[focus].width + 1;
			arrowBounds.y += area[focus].height / 2 - 25 / 2; // 25 == height of
																// arrow image
			// arrowEffect.start(arrowBounds, ArrowEffect.RIGHT);
			focus = candidate;
			checkFocusArrow();
			repaint();
		}
		return false;
	}

	public boolean keyUp() {
		if (area[focus].y == TOP_Y)
			return true;

		// find nearest one from top side
		int candidate = focus;
		int gap = Integer.MAX_VALUE;
		for (int i = 0; i < area.length; i++) {
			if (area[focus].x > (area[i].x + area[i].width) // check horizontal
					// range
					|| (area[focus].x + area[focus].width) < area[i].x)
				continue;
			if (area[i].y >= area[focus].y)
				continue; // it's locate on bottom side or same position
			int delta = calcDelta(area[i], area[focus]);
			if (gap > delta) {
				gap = delta;
				candidate = i;
			}
		}
		if (focus != candidate) {
			Rectangle arrowBounds = new Rectangle(area[focus]);
			arrowBounds.x -= getX();
			arrowBounds.y -= getY();
			arrowBounds.x += area[focus].width / 2 - 25 / 2; // 25 == width of
																// arrow image
			arrowBounds.y -= 17; // 17 == height of arrow image
			// arrowEffect.start(arrowBounds, ArrowEffect.UP);
			focus = candidate;
			checkFocusArrow();
			repaint();
		}
		return false;
	}

	public boolean keyDown() {
		// find nearest one from bottom side
		int candidate = focus;
		int gap = Integer.MAX_VALUE;
		for (int i = 0; i < area.length; i++) {
			// check horizontal range
			if (area[focus].x > (area[i].x + area[i].width) || (area[focus].x + area[focus].width) < area[i].x)
				continue;
			if (area[i].y <= area[focus].y)
				continue; // it's locate on top side or same position
			int delta = calcDelta(area[i], area[focus]);
			if (gap > delta) {
				gap = delta;
				candidate = i;
			}
		}
		if (focus == candidate) {
			gap = Integer.MAX_VALUE;
			for (int i = 0; i < area.length; i++) {
				if (area[i].y <= area[focus].y)
					continue; // it's locate on top side or same position
				int delta = calcDelta(area[i], area[focus]);
				if (gap > delta) {
					gap = delta;
					candidate = i;
				}
			}
		}
		if (focus != candidate) {
			Rectangle arrowBounds = new Rectangle(area[focus]);
			arrowBounds.x -= getX();
			arrowBounds.y -= getY();
			arrowBounds.x += area[focus].width / 2 - 25 / 2; // 25 == width of
																// arrow image
			arrowBounds.y += area[focus].height + 15;
			if (area[focus].width == ImageRetriever.LD_KA.width && area[focus].height == ImageRetriever.LD_KA.height) {
				arrowBounds.y = area[focus].y + area[focus].height + 2 - getY();
			}
			// arrowEffect.start(arrowBounds, ArrowEffect.DOWN);
			focus = candidate;
			checkFocusArrow();
			repaint();
		}
		return false;
	}

	private int calcDelta(Rectangle a, Rectangle b) {
		int height = Math.abs((a.y + a.height / 2) - (b.y + b.height / 2));
		int width = Math.abs((a.x + a.width / 2) - (b.x + b.width / 2));
		int delta = (int) Math.sqrt(Math.pow(height, 2) + Math.pow(width, 2));
		return delta;
	}

	public void checkFocusArrow() {
		arrowUp = (area[focus].y > TOP_Y);
		arrowLeft = (area[focus].x >= LEFT_X);

		arrowDn = false;
		arrowRight = false;
		int x = area[focus].x + area[focus].width;
		int y = area[focus].y + area[focus].height;
		for (int i = 0; i < area.length; i++) {
			if (area[i].x > x) {
				arrowRight = true;
			}
			if (area[i].y > y) {
				arrowDn = true;
			}
		}
	}

	public BaseElement getCurrentCatalogue() {
		if (data.length == focus) {
			return null;
		}
		return data[focus];
	}

	public boolean keyOK() {
						
		if (data.length == focus) { // See all
			((CategoryUI) getParent()).keyOkOnMenu(false);
		} else if (data[focus] instanceof CategoryContainer) {
			// display as poster
			MenuController.getInstance().goToCategory((CategoryContainer) data[focus], null);
		} else if (data[focus] instanceof Showcase && MenuTreeUI.MENU_KARAOKE.equals(((Showcase) data[focus]).id)) {
			CategoryContainer container = CategoryUI.getKaraokeContainer();
			if (container == null) { // not yet into Music category
				CategoryContainer[] sub = CategoryUI.getInstance().getCurrentCategory().subCategories;
				for (int i = 0; i < sub.length; i++) {
					if (sub[i].type == MenuController.MENU_KARAOKE) {
						CategoryUI.setKaraokeContainer(sub[i]);
						container = sub[i];
					}
				}
			}
			Topic karaokeTopic = CatalogueDatabase.getInstance().getKaraokeTopic();
			if (karaokeTopic.needRetrieve == false && container.getTopic().needRetrieve) {
				container.topicPointer[0].topic = karaokeTopic;
			}
			if (container.getTopic().needRetrieve) {
				MenuController.getInstance().showLoadingAnimation();
				synchronized (CatalogueDatabase.getInstance()) {
					CatalogueDatabase.getInstance().retrieveKaraoke(CategoryUI.getInstance(),
							container.getTopic().namedTopic, 3);
					try {
						CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {
					}
				}
			}
			CategoryUI.getInstance().moveToSubCategory(container);

		} else if (data[focus] instanceof Showcase && ((Showcase) data[focus]).detail.getTopic() != null) {
			// CategoryContainer category = (CategoryContainer) ((Showcase)
			// data[focus]).getContent();
			// MenuController.getInstance().goToChannelMenu(category);

			CategoryContainer wrap = new CategoryContainer();
			wrap.isAdult = ((Showcase) data[focus]).detail.getTopic().treeRoot.isAdult;
			wrap.type = MenuController.MENU_CHANNEL;
			wrap.topicPointer = new TopicPointer[] { new TopicPointer() };
			wrap.topicPointer[0].topic = ((Showcase) data[focus]).detail.getTopic();
			
			//R7.4 freelife VDTRMASTER-6185
			wrap.id = data[focus].getId();
			
			// R5
			CategoryUI.setChannelCategory(wrap);
			MenuController.getInstance().forwardHistory(wrap);
			
			MenuController.getInstance().goToDetail(wrap, null);
		} else {
			// R5
			BaseElement current = data[focus];
			if (data[focus] instanceof Showcase) {
				Showcase sc = (Showcase) data[focus];
				MoreDetail content = sc.getContent();
				current = content;
				Log.printDebug("MenuController, cur = " + current.toString(false));
				
				if (current instanceof CategoryContainer) {
					((CategoryContainer) current).needRetrieve = true; 
				}
			}
			String action = null;
			try {
				action = ((BaseUI) getParent()).getActionButton(current)[0];
			} catch (ArrayIndexOutOfBoundsException e) {}
			if (DataCenter.getInstance().getString(Resources.TEXT_UNBLOCK).equals(action)) { // for Block contents
				MenuController.getInstance().goToDetail(data[focus], null, true);		
				//R7.4 freelife VDTRMASTER-6227, not included in R7.4
				//VbmController.getInstance().writeSelection(data[focus].getId());
				//R7.4+Touche 
				VbmController.getInstance().writeSelectionAddHistory(data[focus].getId(), category);
			} else {
				CategoryUI.setBrandingElementByCategory(category);
				// Touche: VDTRMASTER-6244
				MenuController.getInstance().goToDetail(data[focus], null, true);				
				VbmController.getInstance().writeSelectionAddHistory(data[focus].getId(), category);
			}
			
		}
		return true;
	}

	public boolean fadeOut() {
		if (isVisible()) {
			hidingEffect.start();
			return true;
		}
		return false;
	}

	public void fadeIn() {
		// if (!isVisible()) {
		showingEffect.start();
		// }
	}

	public void animationEnded(Effect effect) {
		if (effect == showingEffect) {
			setVisible(true);
		} else if (effect == hidingEffect) {
			setVisible(false);
		}
	}

	public void setData(CategoryContainer container) throws Exception {
		data = new MoreDetail[0];
		dataForRenderer.clear();
		dataMax = 11;
		focus = 0;

		Container p = getParent();
		if (p instanceof CategoryUI) {
			category = container;
			if (category == null) {
				category = ((CategoryUI) p).getCurrentCategory();
				
				// IMPROVEMENT
				if (category == null) {
					Log.printDebug("showcaseView, setData, category is null");
					return;
				}
			}

			boolean channelLogo = false;

			ArrayList dataArray = new ArrayList();
			ArrayList portrait = new ArrayList();
			ArrayList landscape = new ArrayList();

			// 0.0 topic for karaoke

			// 0.1 topic for channels
			if (category.getTopic() != null && category.type == MenuController.MENU_CHANNEL_ENV) {
				Log.printDebug("setData, current category is Channel Env, " + category);

//				for (int i = 0; i < category.getTopic().sample.data.length; i++) {
//					landscape.add(category.getTopic().sample.data[i]);
//				}

				channelLogo = true;
				// for (int i = 0; i < category.getTopic().sample.topics.length;
				// i++) {
				// Showcase channel = new Showcase();
				// Description description = new Description();
				// Topic child = category.getTopic().sample.topics[i];
				// description.title = child.title;
				// description.description = child.description;
				// ImageGroup[] imageGroup = null;
				// if (child.network != null) {
				// imageGroup = child.network.imageGroup;
				// } else if (child.channel != null) {
				// imageGroup = child.channel.imageGroup;
				// } else {
				// imageGroup = child.imageGroup;
				// }
				//
				// if (imageGroup != null) {
				// description.imageGroup = new ImageGroup[imageGroup.length];
				// System.arraycopy(imageGroup, 0, description.imageGroup, 0,
				// imageGroup.length);
				// }
				// ShowcaseDetails detail = new ShowcaseDetails();
				// CategoryContainer wrap = new CategoryContainer();
				// wrap.type = MenuController.MENU_CHANNEL;
				// wrap.topicPointer = new TopicPointer[] {
				// new TopicPointer()
				// };
				// wrap.topicPointer[0].topic = child;
				// detail.categoryContainer = wrap;
				// channel.description = description;
				// channel.detail = detail;
				// channel.id = MenuTreeUI.MENU_CHANNEL;
				//
				// landscape.add(channel);
				// }
			}

			// showcase moved into samples
			for (int i = 0; category.category != null && category.category.sample != null
					&& i < category.category.sample.data.length; i++) {

				if (category.category.sample.data[i] instanceof Showcase == false) {
					continue;
				}
				if (((Showcase) category.category.sample.data[i]).isBigger()) {
					if (dataArray.size() > 0 && category.category.sample.data[i].isPortrait()) {
						continue;
					}
					dataArray.add(category.category.sample.data[i]);
					if (category.category.sample.data[i].isPortrait()) {
						break; // max to 1 portrait bigger poster
					}
					if (dataArray.size() == 2) {
						break; // max to 2 landscape bigger posters
					}
				}
			}

			// 1. put the category that will be displayed as poster
			for (int i = 0; i < category.subCategoriesAsPoster.length; i++) {
				if (category.subCategoriesAsPoster[i].isPortrait()) {
					portrait.add(category.subCategoriesAsPoster[i]);
				} else {
					landscape.add(category.subCategoriesAsPoster[i]);
				}
			}

			// 2. put the showcases except bigger
			for (int i = 0; category.category != null && category.category.sample != null
					&& i < category.category.sample.data.length; i++) {

				if (category.category.sample.data[i] instanceof Showcase == false) {
					continue;
				}
				if (dataArray.contains(category.category.sample.data[i])) {
					continue;
				}
				if (category.category.sample.data[i].isPortrait()) {
					portrait.add(category.category.sample.data[i]);
				} else {
					landscape.add(category.category.sample.data[i]);
				}
			}

			// 4. put the sample
			outer: for (int i = 0; category.category != null && category.category.sample != null
					&& i < category.category.sample.data.length; i++) {
				if (dataArray.contains(category.category.sample.data[i])) {
					continue;
				}
				for (int j = 0; j < portrait.size(); j++) {
					if (((MoreDetail) portrait.get(j)).getId().equals(category.category.sample.data[i].getId())) {
						continue outer;
					}
				}
				for (int j = 0; j < landscape.size(); j++) {
					if (((MoreDetail) landscape.get(j)).getId().equals(category.category.sample.data[i].getId())) {
						continue outer;
					}
				}
				if (category.category.sample.data[i].isPortrait()) {
					portrait.add(category.category.sample.data[i]);
				} else {
					landscape.add(category.category.sample.data[i]);
				}
			}

			// 3. put the other contents

			// 5. move to data field
			if (dataArray.size() == 0) { // no bigger, 9p/11l/5p3l/4l4p
				if (portrait.size() >= 9 || landscape.size() == 0) {
					for (int i = 0; i < 9 && i < portrait.size(); i++) {
						dataArray.add(portrait.get(i));
					}
				} else if (landscape.size() >= 11 || portrait.size() == 0) {
					for (int i = 0; i < 11 && i < landscape.size(); i++) {
						dataArray.add(landscape.get(i));
					}
				} else if (portrait.size() > landscape.size()) {
					for (int i = 0; i < 5 && i < portrait.size(); i++) {
						dataArray.add(portrait.get(i));
					}
					if (landscape.size() > 0) {
						dataArray.add(null);
					}
					for (int i = 0; i < 3 && i < landscape.size(); i++) {
						dataArray.add(landscape.get(i));
					}
				} else {
					for (int i = 0; i < 4 && i < landscape.size(); i++) {
						dataArray.add(landscape.get(i));
					}
					if (portrait.size() > 0) {
						dataArray.add(null);
					}
					for (int i = 0; i < 4 && i < portrait.size(); i++) {
						dataArray.add(portrait.get(i));
					}
				}
			} else if (dataArray.size() == 1) {
				if (((Showcase) dataArray.get(0)).isPortrait()) {
					// 1 portrait bigger, 5p/3l/3p1l/2l2p
					if (portrait.size() >= 5 || landscape.size() == 0) {
						for (int i = 0; i < 5 && i < portrait.size(); i++) {
							dataArray.add(portrait.get(i));
						}
					} else if (landscape.size() >= 3 || portrait.size() == 0) {
						for (int i = 0; i < 3 && i < landscape.size(); i++) {
							dataArray.add(landscape.get(i));
						}
					} else if (portrait.size() >= 3) {
						for (int i = 0; i < 3 && i < portrait.size(); i++) {
							dataArray.add(portrait.get(i));
						}
						for (int i = 0; i < 1 && i < landscape.size(); i++) {
							dataArray.add(landscape.get(i));
						}
					} else {
						for (int i = 0; i < 2 && i < landscape.size(); i++) {
							dataArray.add(landscape.get(i));
						}
						for (int i = 0; i < 2 && i < portrait.size(); i++) {
							dataArray.add(portrait.get(i));
						}
					}
				} else { // 1 landscape bigger, 3l/2p3l
					if (landscape.size() >= 3 || portrait.size() == 0) {
						for (int i = 0; i < 3 && i < landscape.size(); i++) {
							dataArray.add(landscape.get(i));
						}
					} else {
						for (int i = 0; i < 2 && i < portrait.size(); i++) {
							dataArray.add(portrait.get(i));
						}
						for (int i = 0; i < 3 && i < landscape.size(); i++) {
							dataArray.add(landscape.get(i));
						}
					}
				}
			} else if (dataArray.size() == 2) { // 2 landscape bigger, 3l
				for (int i = 0; i < 3 && i < landscape.size(); i++) {
					dataArray.add(landscape.get(i));
				}
			}
			data = new MoreDetail[dataArray.size()];
			dataArray.toArray(data);

			dataArray.clear();
			portrait.clear();
			landscape.clear();

			isAllLandscape = true;
			for (int i = 0; i < data.length && i < 6; i++) {
				// VDTRMASTER-5448
				if (data[i] != null && data[i].isPortrait()) {
					isAllLandscape = false;
				}
			}

			updateChannelAttributes();
			
			// Log.printDebug("setData, channelLogo = " + channelLogo);
			if (channelLogo) {
				new Thread("loadLogo_sc") {
					public void run() {
						for (int i = 0; data != null && i < data.length; i++) {
							if (data[i] instanceof Showcase) {
								Object obj = ((Showcase) data[i]).detail.getTopic();
								if (obj != null) {
									Topic topic = (Topic) obj;
									// Log.printDebug("setData, " + i + "/" +
									// data.length + ", cat = " + cat + ", t = " +
									// topic + ", c = " + topic.channel + ", n = " +
									// topic.network);
									Channel ch = topic.channel;
									if (ch != null) {
//										ch.loadLogo(data[i].isPortrait(), true, false);
                                        ch.loadLogoForShowcase(data[i].isPortrait(), ((Showcase) data[i]).isBigger());
										continue;
									}
									Network nw = topic.network;
									if (nw != null) {
//										nw.loadLogo(data[i].isPortrait(), true, false);
                                        nw.loadLogoForShowcase(data[i].isPortrait(), ((Showcase) data[i]).isBigger());
									}
								} else {
									MoreDetail detail = ((Showcase) data[i]).getContent();
	
									if (detail instanceof Video) {
										Video v = (Video) detail;
										if (v.channel != null) {
											v.channel.loadLogo(true, false);
										}
									} else if (detail instanceof Series) {
										Series s = (Series) detail;
										if (s.channel != null) {
											s.channel.loadLogo(true, false);
										} else if (s.network != null) {
											s.network.loadLogo(true, false);
										}
									} else if (detail instanceof Season) {
										Season s = (Season) detail;
										if (s.channel != null) {
											s.channel.loadLogo(true, false);
										} else if (s.network != null) {
											s.network.loadLogo(true, false);
										}
									}
								}
							} else {
								MoreDetail detail = (MoreDetail) data[i];
								if (detail instanceof Video) {
									Video v = (Video) detail;
									if (v.channel != null) {
										v.channel.loadLogo(true, false);
									}
								} else if (detail instanceof Series) {
									Series s = (Series) detail;
									if (s.channel != null) {
										s.channel.loadLogo(true, false);
									} else if (s.network != null) {
										s.network.loadLogo(true, false);
									}
								} else if (detail instanceof Season) {
									Season s = (Season) detail;
									if (s.channel != null) {
										s.channel.loadLogo(true, false);
									} else if (s.network != null) {
										s.network.loadLogo(true, false);
									}
								}
							}
						}
					}
				}.start();
			} else {
				// retrieveImages(false);
			}
		}

		area = new Rectangle[data.length + 1];
		for (int i = 0; i < area.length; i++) {
			area[i] = new Rectangle();
		}
		freshArea = true;
	}
	
	public void updateChannelAttributes() {
		Log.printDebug("ShowcaseView, updateChannelAttributes");
		
		if (data == null) {
			Log.printDebug("ShowcaseView, updateChannelAttributes, data=" + data);
			return;
		}
		
		for (int i = 0; i < data.length; i++) {
			isInWishListArray[i] = false;
			isBlockContentArray[i] = false;
			isPreviewArray[i] = false;

			// check wishlist
			if (data[i] instanceof Showcase) {
				Showcase sc = (Showcase) data[i];
				if (BookmarkManager.getInstance().isInWishlist(sc.getContent()) != null) {
					isInWishListArray[i] = true;
				} else if (((Showcase) data[i]).detail.getTopic() != null) {
					isInWishListArray[i] = ChannelInfoManager.getInstance()
							.isFavoriteChannel(((Showcase) data[i]).detail.getTopic());
				}
			} else {
				if (BookmarkManager.getInstance().isInWishlist(data[i]) != null) {
					isInWishListArray[i] = true;
				}
			}

			// check block content
			if (!isBlockContentArray[i]) {
				if (data[i] instanceof Showcase) {
					Showcase sc = (Showcase) data[i];
					MoreDetail md = sc.getContent();

					if (md != null) {
						isBlockContentArray[i] = MenuController.getInstance().isBlocked(sc.getContent());
						
					} else {
						Topic topic = sc.detail.getTopic();
						if (topic != null) {
							isBlockContentArray[i] = ChannelInfoManager.getInstance().isBlockedChannel(topic);
						}
					}
				} else {
					if (MenuController.getInstance().isBlocked(data[i])) {
						isBlockContentArray[i] = true;
					}
				}
			}
			// check isPreview
			if (!isInWishListArray[i] && !isBlockContentArray[i]) {
				if (data[i] instanceof Showcase) {
					Showcase sc = (Showcase) data[i];
					MoreDetail md = sc.getContent();

					if (md != null) {
						if (md instanceof Video) {
							Video v = (Video) md;
							if (v.channel != null) {
								isPreviewArray[i] = CommunicationManager.getInstance().isPreviewChannel(v.channel.callLetters);
							}
						} else if (md instanceof Series) {
							Series s = (Series) md;
							if (s.channel != null) {
								isPreviewArray[i] = CommunicationManager.getInstance().isPreviewChannel(s.channel.callLetters);
							} else if (s.network != null) {
								for (int nIdx = 0; nIdx < s.network.channel.length; nIdx++) {
									if (CommunicationManager.getInstance().isPreviewChannel(s.network.channel[nIdx].callLetters)) {
										isPreviewArray[i] = true;
										break;
									}
								}
							}
						}
					} else {
						Topic topic = sc.detail.getTopic();
						if (topic.channel != null) {
							if (CommunicationManager.getInstance().isPreviewChannel(topic.channel.callLetters)) {
								isPreviewArray[i] = true;
							}
						} else if (topic.network != null) {
							for (int nIdx = 0; nIdx < topic.network.channel.length; nIdx++) {
								if (CommunicationManager.getInstance().isPreviewChannel(
										topic.network.channel[nIdx].callLetters)) {
									isPreviewArray[i] = true;
									break;
								}
							}
						}
					}
				} else {
					if (data[i] instanceof Video) {
						Video v = (Video) data[i];
						if (v.channel != null 
								&& CommunicationManager.getInstance().isPreviewChannel(v.channel.callLetters)) {
							isPreviewArray[i] = true;
						}
					} else if (data[i] instanceof Series) {
						Series s = (Series) data[i];
						if (s.channel != null 
								&& CommunicationManager.getInstance().isPreviewChannel(s.channel.callLetters)) {
							isPreviewArray[i] = true;
						}
					}
				}
			}
		}
	}

	public void retrieveImages(boolean again) {
		if (again) {
			Log.printDebug("retrieveImages, try again");
			CatalogueDatabase.getInstance().clearDuplicate(CatalogueDatabase.IMAGE_RETRIEVER_SM);
		}
		CatalogueDatabase.getInstance().retrieveImages((BaseUI) getParent(), data, ImageRetriever.TYPE_SMALL,
				CatalogueDatabase.IMAGE_RETRIEVER_SM);

		// VDTRMASTER-6096
		// R5
		// this case don't has a large images.
//		if (category.getTopic() != null && category.type == MenuController.MENU_CHANNEL_ENV) {
//			return;
//		}

		ArrayList large = new ArrayList();
		for (int i = 0; i < data.length; i++) {
			if (data[i] instanceof Showcase && ((Showcase) data[i]).isBigger()) {
				large.add(data[i]);
				if (large.size() > 1) {
					break;
				}
			}
		}
		if (large.size() > 0) {
			BaseElement[] elements = new BaseElement[large.size()];
			large.toArray(elements);
			CatalogueDatabase.getInstance().retrieveLargeImage((BaseUI) getParent(), elements, true);
		}
	}

	public void stop() {
		setVisible(false);
		data = null;
		category = null;
		dataForRenderer.clear();
	}

	static final int LEFT_X = 373;
	static final int TOP_Y = 128;
	static final int GAP_X_P = 16;
	static final int GAP_X_PR_LG = 12;
	static final int GAP_X_L = 11;
	static final int GAP_X_LD_LG_MD = 35;
	static final int GAP_Y = 31;
	static final int GAP_Y_PL = 60;

	class ShowcaseViewRenderer extends Renderer {

		public Rectangle getPreferredBounds(UIComponent c) {
			return new Rectangle(350, 120, 545, 360);
		}

		Rectangle focusedRect = new Rectangle();
		Color cTitle = new Color(255, 205, 12);
		Color c65 = new Color(65, 65, 65);
		Image arrUp;// = dataCenter.getImage("02_ars_t.png");
		Image arrDn;// = dataCenter.getImage("02_ars_b.png");
		Image arrLeft;// = dataCenter.getImage("02_ars_l.png");
		Image arrRight;// = dataCenter.getImage("02_ars_r.png");
		Image blockedPortrait;// = dataCenter.getImage("01_poster_block.png");
		Image blockedLandscape;// =
								// dataCenter.getImage("01_poster_block_h.png");

		// Image i_01_possh_s_l;// = dataCenter.getImage("01_possh_s_l.png");
		// Image i_01_poshigh_s_l;// =
		// dataCenter.getImage("01_poshigh_s_l.png");
		// Image i_01_possh_s;// = dataCenter.getImage("01_possh_s.png");
		// Image i_01_poshigh_s;// = dataCenter.getImage("01_poshigh_s.png");
		// Image i_01_kara_mainbn_high;
		// Image i_01_kara_mainbn_sh;
		
		// R5
		Image wishIconImg, blockIconImg, previewIconImg, iconUhd;
		

		protected void paint(Graphics g, UIComponent c) {
			// g.setColor(Color.red);
			// g.drawRect(0, 0, c.getWidth()-1, c.getHeight()-1);

			CategoryUI ui = (CategoryUI) c.getParent();
			boolean isFocus = ui.getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT;
			// g.drawString("L="+data.length+",F="+isFocus, 5, 20);

			g.translate(-c.getX(), -c.getY());
			String[] splitedTitle;
			boolean focused;
			// if (freshArea) {
			int x = LEFT_X, y = TOP_Y, w = ImageRetriever.PR_SM.width, h = ImageRetriever.PR_SM.height;
			// bigger poster can be displayed by max to 1 or 2
			int bigCnt = 0;
			for (int i = 0; data != null && i < data.length; i++) {
				if (data[i] == null) {
					// it means next poster should be drawn in another row
					// because this poster type is different
					if (x > LEFT_X) {
						x = LEFT_X;
						y += h + GAP_Y_PL;
					}
					continue;
				}
				boolean portrait = data[i].isPortrait();
				boolean showTitle = true;
				int bigMax = portrait ? 1 : 2;

				if (data[i] instanceof Showcase
						&& (MenuTreeUI.MENU_KARAOKE.equals(((Showcase) data[i]).id) || ((Showcase) data[i]).getBanner(
								ImageRetriever.LD_KA.width, ImageRetriever.LD_KA.height) != null)) {
					w = ImageRetriever.LD_KA.width;
					h = ImageRetriever.LD_KA.height;
					showTitle = false;
					bigCnt++;
				} else if (data[i] instanceof Showcase && ((Showcase) data[i]).isBigger() && bigCnt < bigMax) {
					w = portrait ? ImageRetriever.PR_LG.width : ImageRetriever.LD_LG_MD.width;
					h = portrait ? ImageRetriever.PR_LG.height : ImageRetriever.LD_LG_MD.height;
					bigCnt++;
				} else {
					w = portrait ? ImageRetriever.PR_SM.width : ImageRetriever.LD_SM.width;
					h = portrait ? ImageRetriever.PR_SM.height : ImageRetriever.LD_SM.height;
				}
				// if (data[i] instanceof Showcase && ((Showcase) data[i]).id ==
				// MenuTreeUI.MENU_CHANNEL) {
				// w = ImageRetriever.LD_SM.width;
				// h = ImageRetriever.LD_SM.height;
				// g.setColor(c65);
				// g.fillRect(x, y, w, h);
				// // g.drawImage(i_01_poshigh_s_l, x, y, ui);
				// // g.drawImage(i_01_possh_s_l, x, y + h, ui);
				// Channel ch = ((CategoryContainer) ((Showcase)
				// data[i]).getContent()).getTopic().channel;
				// if (ch != null && ch.logos[Channel.LOGO_SM] != null) {
				// g.drawImage(ch.logos[Channel.LOGO_SM],
				// x + w/2 - 50/*ch.logos[Channel.LOGO_SM].getWidth(null)/2*/,
				// y + h/2 - 35/*ch.logos[Channel.LOGO_SM].getHeight(null)/2*/,
				// ui);
				// } else {
				// Network nw = ((CategoryContainer) ((Showcase)
				// data[i]).getContent()).getTopic().network;
				// if (nw != null && nw.logos[Network.LOGO_SM] != null) {
				// g.drawImage(nw.logos[Network.LOGO_SM],
				// x + w/2 - 50/*nw.logos[Network.LOGO_SM].getWidth(null)/2*/,
				// y + h/2 - 35/*nw.logos[Network.LOGO_SM].getHeight(null)/2*/,
				// ui);
				// }
				// }
				//
				// } else {
				Image poster = null;
				boolean isAdultBlocked = false;
				if (data[i] instanceof Showcase) {
					isAdultBlocked = MenuController.getInstance().isAdultBlocked(((Showcase) data[i]).getContent());
				} else {
					isAdultBlocked = MenuController.getInstance().isAdultBlocked(data[i]);
				}
				if (isAdultBlocked) {
					poster = data[i].isPortrait() ? blockedPortrait : blockedLandscape;
				} else if (data[i] instanceof Showcase && !((Showcase) data[i]).isBigger() && ((Showcase) data[i]).detail.getTopic() != null) {
//					w = ImageRetriever.LD_SM.width;
//					h = ImageRetriever.LD_SM.height;
					g.setColor(c65);
					g.fillRect(x, y, w, h);
					// g.drawImage(i_01_poshigh_s_l, x, y, ui);
					// g.drawImage(i_01_possh_s_l, x, y + h, ui);
                    int forceW = 84;
                    int forceH = 63;
					Channel ch = ((Showcase) data[i]).detail.getTopic().channel;
					if (ch != null && ch.showcaseLogo != null) {
                        // VDTRMASTER-6096
					    if (portrait) {
//                            g.drawImage(ch.logos[Channel.LOGO_SM], x + w / 2 - 45, y + h / 2 - 68, 70, 115, ui);
                            if (ch.showcaseLogo.getWidth(c) < ch.showcaseLogo.getHeight(c)) {
                                g.drawImage(ch.showcaseLogo, x, y, 90, 135, ui);
                            } else {
                                g.drawImage(ch.showcaseLogo, x + 3, y + h / 2 - forceH/2, forceW, forceH, c);
                            }

                        } else {
					        if (ch.showcaseLogo.getWidth(c) == 100) {
                                g.drawImage(ch.showcaseLogo, x + w / 2 - 50, y + h / 2 - 35, 100, 70, ui);
                            } else {
                                g.drawImage(ch.showcaseLogo, x,  y, 120, 90, ui);
                            }
                        }
					} else {
						Network nw = ((Showcase) data[i]).detail.getTopic().network;
						if (nw != null && nw.showcaseLogo != null) {
                            // VDTRMASTER-6096
						    if (portrait) {
//                                g.drawImage(nw.logos[Network.LOGO_SM], x + w / 2 - 45, y + h / 2 - 68, 70, 115, ui);
                                if (nw.showcaseLogo.getWidth(c) < nw.showcaseLogo.getHeight(c)) {
                                    g.drawImage(nw.showcaseLogo, x, y, 90, 135, ui);
                                } else {
                                    g.drawImage(nw.showcaseLogo, x + 3, y + h / 2 - forceH/2, forceW, forceH, c);
                                }
                            } else {
                                if (nw.showcaseLogo.getWidth(c) == 100) {
                                    g.drawImage(nw.showcaseLogo, x + w / 2 - 50, y + h / 2 - 35, 100, 70, ui);
                                } else {
                                    g.drawImage(nw.showcaseLogo, x,  y, 120, 90, ui);
                                }
                            }
						}
					}
				} else {
                    if (data[i] instanceof Showcase && ((Showcase) data[i]).isBigger() && ((Showcase) data[i]).detail.getTopic() != null) {
                        g.setColor(c65);
                        g.fillRect(x, y, w, h);
                        Channel ch = ((Showcase) data[i]).detail.getTopic().channel;
                        if (ch != null && ch.showcaseLogo != null) {
                            poster = ch.showcaseLogo;
                        } else {
                            Network nw = ((Showcase) data[i]).detail.getTopic().network;
                            if (nw != null && nw.showcaseLogo != null) {
                                poster = nw.showcaseLogo;
                            }
                        }
                    } else {
                        poster = data[i].getImage(w, h);
                    }
				}
				g.setFont(FontResource.DINMED.getFont(15));
				if (poster != null) {
					g.drawImage(poster, x, y, w, h, ui);
				}
				if (isAdultBlocked == false) {
					Image flag = null;
					int flagValue = -1;
					if (data[i] instanceof Showcase && ((Showcase) data[i]).getContent() != null) {
						flagValue = ((Showcase) data[i]).getContent().getFlag();
					} else {
						flagValue = data[i].getFlag();
					}
					switch (flagValue) {
					case 1: // new
						flag = dataCenter.getImageByLanguage("icon_new.png");
						break;
					case 2: // last chance
						flag = dataCenter.getImageByLanguage("icon_last.png");
						break;
					case 3: // extras
						flag = dataCenter.getImageByLanguage("icon_extras.png");
						break;
					}
					
//					if (flag == null && data[i] instanceof Showcase
//							&& ((Showcase)data[i]).getContent() instanceof Extra 
//							&& ((Extra)((Showcase)data[i]).getContent()).getFlag() == 3) {
//						flag = dataCenter.getImageByLanguage("icon_extras.png");
//					}

					// R5
					if (category.getTopic() != null && category.type == MenuController.MENU_CHANNEL_ENV) {
						if (data[i] instanceof Showcase && ((Showcase) data[i]).getContent() != null) {
							MoreDetail detail = ((Showcase) data[i]).getContent();
							if (Log.ALL_ON) {
								Log.printDebug("ShowcaseView, data index=" + i);
							}
							flag = getLogoIcon(detail);
						} else {
							if (Log.ALL_ON) {
								Log.printDebug("ShowcaseView, not show case, data index=" + i);
							}
							if (data[i] != null) {
								flag = getLogoIcon(data[i]);
							}
						}
					}

					if (flag != null && flag.getWidth(c) > 0) {
						g.drawImage(flag, x + w - flag.getWidth(c), y, c);
					}
					// R5 - UHD icon
					Offer hdOffer = data[i].getOfferHD(null);
					if (data[i] instanceof Showcase && ((Showcase)data[i]).getContent() != null) {
						hdOffer = ((Showcase)data[i]).getContent().getOfferHD(null);
					}
					if (hdOffer != null) {
						boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
						if (uhd) {
							g.drawImage(iconUhd, w, y + h - iconUhd.getHeight(ui), ui);
						}
					}
				}
				if ((w == ImageRetriever.PR_SM.width && h == ImageRetriever.PR_SM.height)
						|| (w == ImageRetriever.PR_LG.width && h == ImageRetriever.PR_LG.height)) {
					// g.drawImage(i_01_poshigh_s, x, y, w, h, ui);
					// g.drawImage(i_01_possh_s, x, y + h, w,
					// i_01_possh_s.getHeight(null), ui);
				} else if ((w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height)
						|| (w == ImageRetriever.LD_LG_MD.width && h == ImageRetriever.LD_LG_MD.height)) {
					// g.drawImage(i_01_poshigh_s_l, x, y, w, h, ui);
					// g.drawImage(i_01_possh_s_l, x, y + h, w,
					// i_01_possh_s_l.getHeight(null), ui);
				} else if (w == ImageRetriever.LD_KA.width && h == ImageRetriever.LD_KA.height) {
					// g.drawImage(i_01_kara_mainbn_high, x, y, ui);
					// g.drawImage(i_01_kara_mainbn_sh, x, y + h, ui);
				}
				// }
				area[i].setBounds(x, y, w, h);

				focused = focus == i;
				g.setFont(FontResource.BLENDER.getFont(16));
				if (isFocus && focused) {
					g.setColor(cTitle);
					focusedRect.setBounds(x, y, w, h);
				} else {
					g.setColor(Color.white);
				}

				if (showTitle) {
					
					int posX = x;
					int newWidth = w;
					int iconPosX = x;
					
					String title = data[i].getTitle();
					
					// VDTRMASTER-5307
					if (data[i] instanceof Season && !((Season) data[i]).getLongTitle().equals("N/A")) {
						title = ((Season) data[i]).getLongTitle();
					}
					
					// R5
					if (category.getTopic() != null && category.type == MenuController.MENU_CHANNEL_ENV) {
						if (data[i] instanceof Showcase && ((Showcase) data[i]).getContent() != null) {
							MoreDetail detail = ((Showcase) data[i]).getContent();
							if (Log.DEBUG_ON) {
								Log.printDebug("ShowcaseView, title, data index=" + i);
							}
							if (detail != null) {
								if (detail instanceof Video) {
									Video video = (Video) detail;
									if (Log.ALL_ON) {
										Log.printDebug("ShowcaseView, title, video.channel=" + video.channel);
									}
									if (video.channel != null) {
										title = video.channel.getTitle() + " - " + title;
									} else if (video.network != null) {
										title = video.network.getTitle() + " - " + title;
									}
								} else if (detail instanceof Series) {
									Series series = (Series) detail;
									if (Log.ALL_ON) {
										Log.printDebug("ShowcaseView, title, series.channel=" + series.channel);
										Log.printDebug("ShowcaseView, title, series.network=" + series.network);
									}
									if (series.channel != null) {
										title = series.channel.getTitle() + " - " + title;
									} else if (series.network != null) {
										title = series.network.getTitle() + " - " + title;
									}
								}
							}
						} else {
							if (Log.ALL_ON) {
								Log.printDebug("ShowcaseView, title, no showcase data index=" + i);
							}
							MoreDetail detail = (MoreDetail) data[i];
							if (detail instanceof Video) {
								Video video = (Video) detail;
								if (Log.ALL_ON) {
									Log.printDebug("ShowcaseView, title, video.channel=" + video.channel);
								}
								if (video.channel != null) {
									title = video.channel.getTitle() + " - " + title;
								} else if (video.network != null) {
									title = video.network.getTitle() + " - " + title;
								}
							} else if (detail instanceof Series) {
								Series series = (Series) detail;
								if (Log.ALL_ON) {
									Log.printDebug("ShowcaseView, title, series.channel=" + series.channel);
									Log.printDebug("ShowcaseView, title, series.network=" + series.network);
								}
								if (series.channel != null) {
									title = series.channel.getTitle() + " - " + title;
								} else if (series.network != null) {
									title = series.network.getTitle() + " - " + title;
								}
							} else if (detail instanceof Season) {
								Season season = (Season) detail;
								if (Log.ALL_ON) {
									Log.printDebug("ShowcaseView, title, season.channel=" + season.channel);
									Log.printDebug("ShowcaseView, title, season.network=" + season.network);
								}
								if (season.channel != null) {
									title = season.channel.getTitle() + " - " + title;
								} else if (season.network != null) {
									title = season.network.getTitle() + " - " + title;
								}
							}
						}
					}
					
					splitedTitle = (String[]) dataForRenderer.get(data[i].getId() + i);
					
					// VDTRMASTER-5700
					if (isAdultBlocked) {
						title = DataCenter.getInstance().getString(Resources.TEXT_LBL_BLOCKED_TITLE);
					}
					
					if (isFocus && focused && g.getFontMetrics().stringWidth(title) <= w) {
						iconPosX = x + w / 2 - g.getFontMetrics().stringWidth(title) / 2 - 5;
						posX -= 5;
					}
					
					if (isFocus && i == focus) {
						if (isBlockContentArray[i] && !MenuController.getInstance().needToDisplayFavorite()) {
							g.drawImage(blockIconImg, iconPosX, y + h + 5, c);
							posX += 15;
							newWidth -= 15;
						} else if (isInWishListArray[i]) {
							g.drawImage(wishIconImg, iconPosX, y + h + 5, c);
							posX += 15;
							newWidth -= 15;
						} else if (isPreviewArray[i]) {
							g.drawImage(previewIconImg, iconPosX, y + h + 5, c);
							posX += 15;
							newWidth -= 15;
						} else if (isFocus && focused && g.getFontMetrics().stringWidth(title) <= w) {
							posX += 7;
						}
					} else {
						if (isBlockContentArray[i]) {
							g.drawImage(blockIconImg, iconPosX, y + h + 5, c);
							posX += 15;
							newWidth -= 15;
						} else if (isInWishListArray[i]) {
							g.drawImage(wishIconImg, iconPosX, y + h + 5, c);
							posX += 15;
							newWidth -= 15;
						} else if (isPreviewArray[i]) {
							g.drawImage(previewIconImg, iconPosX, y + h + 5, c);
							posX += 15;
							newWidth -= 15;
						} else if (isFocus && focused && g.getFontMetrics().stringWidth(title) <= w) {
							posX += 7;
						}
					}
					
					if (splitedTitle == null) {
						// splitedTitle = TextUtil.split(title,
						// g.getFontMetrics(), w - 4, 2);
						// if (splitedTitle.length > 1) {
						// splitedTitle[1] = TextUtil.shorten(splitedTitle[1],
						// g.getFontMetrics(), w);
						// }
						
						// VDTRMASTER-5700
						if (isAdultBlocked) {
							splitedTitle = new String[] { TextUtil.shorten(title, g.getFontMetrics(), newWidth) };
						} else {
							splitedTitle = new String[] { TextUtil.shorten(title, g.getFontMetrics(), newWidth) };
							dataForRenderer.put(data[i].getId() + i, splitedTitle);
						}
					}
					
					if (isFocus && focused) {
						// GraphicUtil.drawStringCenter(g, splitedTitle[0], x +
						// w
						// / 2, y + h + 16);
						// if (splitedTitle.length > 1) {
						// GraphicUtil.drawStringCenter(g, splitedTitle[1], x
						// + w / 2, y + h + 16 + 12);
						// }
						if (g.getFontMetrics().stringWidth(title) > newWidth) {
							int cnt = Math.max(ui.scroll - 10, 0);
							Rectangle ori = g.getClipBounds();
							ui.scrollBounds.x = posX;
							ui.scrollBounds.y = y + h;
							ui.scrollBounds.width = newWidth;
							ui.scrollBounds.height = 20;
							g.clipRect(posX, y + h, newWidth, 20);
							try {
								g.drawString(title.substring(cnt), posX + 2, y + h + 16);
							} catch (IndexOutOfBoundsException e) {
								ui.scroll = 0;
							}
							g.setClip(ori);
						} else {
							ui.scrollBounds.width = 0;
							GraphicUtil.drawStringCenter(g, title, posX + w / 2, y + h + 16);
						}
					} else {
						g.drawString(splitedTitle[0], posX + 2, y + h + 16);
						if (splitedTitle.length > 1) {
							g.drawString(splitedTitle[1], posX + 2, y + h + 16 + 12);
						}
					}
				}

				// calculate next position
				int tmpXgap = GAP_X_P;
				if (w == ImageRetriever.LD_LG_MD.width && bigCnt > 0) {
					tmpXgap = GAP_X_LD_LG_MD;
				} else if (w == ImageRetriever.PR_LG.width || bigCnt > 0) {
					tmpXgap = GAP_X_PR_LG;
				} else if (w == ImageRetriever.LD_SM.width) {
					tmpXgap = GAP_X_L;
				}
				x += w + tmpXgap;
				int need = (w == ImageRetriever.PR_LG.width || w == ImageRetriever.PR_SM.width) ? ImageRetriever.PR_SM.width
						: ImageRetriever.LD_SM.width;
				if (x + need >= 895) {
					// draw another row because there is no space
					y += h + GAP_Y;
					if (bigCnt > 0) {
						y += 15;// 20;
					} else if (isAllLandscape) {
						y -= 3;
					}
					x = LEFT_X;
					for (int j = 0; j < i; j++) {
						if (area[j].y < y && y < area[j].y + area[j].height) {
							if (x < area[j].x + area[j].width) {
								x = area[j].x + area[j].width + tmpXgap;
							}
						}
					}
				}
			} // end of for

			// R5
			if ((data == null || data.length == 0) && category != null && category.type == MenuController.MENU_CHANNEL) {
				x = LEFT_X;
				y = TOP_Y;
				w = ImageRetriever.LD_SM.width;
				h = ImageRetriever.LD_SM.height;
			}
			
			// draw "See All"
			w = (w == ImageRetriever.PR_LG.width || w == ImageRetriever.PR_SM.width) ? ImageRetriever.PR_SM.width
					: ImageRetriever.LD_SM.width;
			h = (h == ImageRetriever.PR_LG.height || h == ImageRetriever.PR_SM.height) ? ImageRetriever.PR_SM.height
					: ImageRetriever.LD_SM.height;
			
			paintSeeAll(g, x, y, w, h, c);
			if (area != null && area.length > data.length) {
				area[data.length].setBounds(x, y, w, h);
			}
			if (isFocus && focus == data.length) {
				focusedRect.setBounds(x, y, w, h);
			}
			// } // end of if (freshArea)

			if (isFocus) { // draw focus rim
				g.setColor(cTitle);
				g.drawRect(focusedRect.x - 1, focusedRect.y - 1, focusedRect.width + 1, focusedRect.height + 1);
				g.drawRect(focusedRect.x - 2, focusedRect.y - 2, focusedRect.width + 3, focusedRect.height + 3);
				g.drawRect(focusedRect.x - 3, focusedRect.y - 3, focusedRect.width + 5, focusedRect.height + 5);

				if (arrowUp) {
					g.drawImage(arrUp, focusedRect.x + focusedRect.width / 2 - arrUp.getWidth(c) / 2, focusedRect.y
							- arrUp.getHeight(c), c);
				}
				if (arrowDn) {
					int yOffset = 15;
					if (focusedRect.width == ImageRetriever.LD_KA.width
							&& focusedRect.height == ImageRetriever.LD_KA.height) {
						yOffset = 2;
					}
					g.drawImage(arrDn, focusedRect.x + focusedRect.width / 2 - arrDn.getWidth(c) / 2, focusedRect.y
							+ focusedRect.height + yOffset, c);
				}
				if (arrowLeft) {
					g.drawImage(arrLeft, focusedRect.x - arrLeft.getWidth(c) - 1, focusedRect.y + focusedRect.height
							/ 2 - arrLeft.getHeight(c) / 2, c);
				}
				if (arrowRight) {
					g.drawImage(arrRight, focusedRect.x + focusedRect.width + 1, focusedRect.y + focusedRect.height / 2
							- arrRight.getHeight(c) / 2, c);
				}

			}

			g.translate(c.getX(), c.getY());
		}

		String[] debugTime = new String[] { "", "", "", "", "" };

		private void paintSeeAll(Graphics g, int x, int y, int w, int h, UIComponent c) {
			int seeAllX, seeAllY;
			g.setColor(c65);
			g.fillRect(x, y, w, h);
			if (w == ImageRetriever.PR_SM.width) {
				seeAllX = 38;
				seeAllY = 39;
			} else {
				seeAllX = 55;
				seeAllY = 11;
			}
			
			g.setFont(FontResource.BLENDER.getFont(17));
			if (category != null && category.type == MenuController.MENU_CHANNEL) {
                int forceW = 84;
                int forceH = 63;
				// channel logo
				if (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) {
					Image logo = null;
					Channel ch = category.getTopic().channel;
					if (ch != null && ch.logos[Channel.LOGO_SM] != null) {
						logo = ch.logos[Channel.LOGO_SM];
					} else {
						Network nw = category.getTopic().network;
						if (nw != null && nw.logos[Network.LOGO_SM] != null) {
							logo = nw.logos[Network.LOGO_SM];
						}
					}
					if (logo != null) {
					    // VDTRMASTER-6096
                        if (logo.getWidth(c) == 100 && logo.getHeight(c) == 70) {
                            g.drawImage(logo, x + w / 2 - 50, y + h / 2 - 35, 100, 70, c);
                        } else if (logo.getWidth(c) == ImageRetriever.LD_SM.width) {
                            g.drawImage(logo, x, y, 120, 90, c);
                        } else {
//                            if (ch != null && ch.logos[Channel.LOGO_SM] != null) {
//                                ch.reloadLogo(false);
//                            } else {
//                                Network nw = category.getTopic().network;
//                                if (nw != null && nw.logos[Network.LOGO_SM] != null) {
//                                    nw.reloadLogo(false);
//                                }
//                            }

                            if (logo.getWidth(c) < logo.getHeight(c)) {
                                g.drawImage(logo, x + 3, y + h / 2 - forceH/2, forceW, forceH, c);
                            } else {
                                g.drawImage(logo, x + w / 2 - 50, y + h / 2 - 35, 100, 70, c);
                            }
                        }

					}
				} else {
//					int forceW = 84;
//					int forceH = 63;
//					Image logo = null;
//					Channel ch = category.getTopic().channel;
//					if (ch != null && ch.logos[Channel.LOGO_SM] != null) {
//						logo = ch.logos[Channel.LOGO_SM];
//					} else {
//						Network nw = category.getTopic().network;
//						if (nw != null && nw.logos[Network.LOGO_SM] != null) {
//							logo = nw.logos[Network.LOGO_SM];
//						}
//					}
//					if (logo != null) {
//						g.drawImage(logo, x + 3, y + h / 2 - forceH/2, forceW, forceH, c);
//					}

                    Image logo = null;
                    Channel ch = category.getTopic().channel;
					if (ch != null && ch.logos[Channel.LOGO_SM] != null) {
						logo = ch.logos[Channel.LOGO_SM];
					} else {
						Network nw = category.getTopic().network;
						if (nw != null && nw.logos[Network.LOGO_SM] != null) {
							logo = nw.logos[Network.LOGO_SM];
						}
					}

					if (logo != null) {
                        if (logo.getWidth(c) == ImageRetriever.PR_SM.width) {
                            g.drawImage(logo, x, y, 90, 135, c);
                        } else {
                            if (logo.getWidth(c) > logo.getHeight(c)) {
                                g.drawImage(logo, x + 3, y + h / 2 - forceH/2, forceW, forceH, c);
                            } else {
                                g.drawImage(logo, x, y, 90, 135, c);
                            }
                        }
					}
				}
				
				String titlesStr = DataCenter.getInstance().getString(Resources.TEXT_MSG_ALL_CONTENT);
				g.setColor(Color.white);
				CategoryUI ui = (CategoryUI) c.getParent();
				boolean isFocus = ui.getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT;
				if (isFocus && focus == data.length) {
					GraphicUtil.drawStringCenter(g, titlesStr, x + 2 + w/2, y + h + 16);
				} else {
					g.drawString(titlesStr, x + 2, y + h + 16);
				}
			} else {
				g.drawImage(arrRight, x + seeAllX, y + seeAllY, c);
				boolean isChEnv = false;
				try {
					if (category.getTopic() != null && category.type == MenuController.MENU_CHANNEL_ENV) {
						isChEnv = true;
					}
				} catch (Exception e) {
				}
				
				String seeAllStr = DataCenter.getInstance().getString(
						isChEnv ? Resources.TEXT_MSG_SEE_ALL_CH1 : Resources.TEXT_MSG_SEE)
						+ " "
						+ DataCenter.getInstance()
								.getString(isChEnv ? Resources.TEXT_MSG_SEE_ALL_CH2 : Resources.TEXT_MSG_ALL);
				int seeAllW = g.getFontMetrics().stringWidth(seeAllStr);
				String seeStr = DataCenter.getInstance().getString(
						isChEnv ? Resources.TEXT_MSG_SEE_ALL_CH1 : Resources.TEXT_MSG_SEE)
						+ " ";
				int seeW = g.getFontMetrics().stringWidth(seeStr);
				String allStr = DataCenter.getInstance().getString(
						isChEnv ? Resources.TEXT_MSG_SEE_ALL_CH2 : Resources.TEXT_MSG_ALL);
				String titlesStr = DataCenter.getInstance().getString(
						isChEnv ? Resources.TEXT_MSG_SEE_ALL_CH3 : Resources.TEXT_MSG_TITLES);
	
				g.setColor(cTitle);
				g.drawString(seeStr, x + w / 2 - seeAllW / 2, y + seeAllY + 25 + 15);
				g.setColor(Color.white);
				GraphicUtil.drawStringCenter(g, titlesStr, x + w / 2, y + seeAllY + 25 + 30);
				g.drawString(allStr, x + w / 2 - seeAllW / 2 + seeW, y + seeAllY + 25 + 15);
				if (w == ImageRetriever.PR_SM.width && h == ImageRetriever.PR_SM.height) {
					// g.drawImage(i_01_poshigh_s, x, y, c);
					// g.drawImage(i_01_possh_s, x, y + h, c);
				} else if (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) {
					// g.drawImage(i_01_poshigh_s_l, x, y, c);
					// g.drawImage(i_01_possh_s_l, x, y + h, c);
				}
			}
		}

		public void prepare(UIComponent c) {
			arrUp = dataCenter.getImage("02_ars_t.png");
			arrDn = dataCenter.getImage("02_ars_b.png");
			arrLeft = dataCenter.getImage("02_ars_l.png");
			arrRight = dataCenter.getImage("02_ars_r.png");
			blockedPortrait = dataCenter.getImage("01_poster_block.jpg");
			blockedLandscape = dataCenter.getImage("01_poster_block_h.jpg");
			// i_01_possh_s_l = dataCenter.getImage("01_possh_s_l.png");
			// i_01_poshigh_s_l = dataCenter.getImage("01_poshigh_s_l.png");
			// i_01_possh_s = dataCenter.getImage("01_possh_s.png");
			// i_01_poshigh_s = dataCenter.getImage("01_poshigh_s.png");
			// i_01_kara_mainbn_high =
			// dataCenter.getImage("01_kara_mainbn_high.png");
			// i_01_kara_mainbn_sh =
			// dataCenter.getImage("01_kara_mainbn_sh.png");
			
			wishIconImg = dataCenter.getImage("icon_wish_s.png");
			blockIconImg = dataCenter.getImage("icon_lock_s.png");
			previewIconImg = dataCenter.getImage("icon_event_s.png");
			iconUhd = dataCenter.getImage("icon_uhd.png");
		}
		
		// R5
		private Image getLogoIcon(MoreDetail detail) {
			Image flag = null;
			if (detail instanceof Video) {
				Video video = (Video) detail;
				if (Log.ALL_ON) {
					Log.printDebug("ShowcaseView, video.channel=" + video.channel);
				}
				if (video.channel != null) {
					flag = video.channel.logos[BrandingElement.LOGO_ICON];
				} else if (video.network != null) {
					flag = video.network.logos[BrandingElement.LOGO_ICON];
				}
			} else if (detail instanceof Series) {
				Series series = (Series) detail;
				if (Log.ALL_ON) {
					Log.printDebug("ShowcaseView, series.channel=" + series.channel);
					Log.printDebug("ShowcaseView, series.network=" + series.network);
				}
				if (series.channel != null) {
					flag = series.channel.logos[BrandingElement.LOGO_ICON];
				} else if (series.network != null) {
					flag = series.network.logos[BrandingElement.LOGO_ICON];
				}
			} else if (detail instanceof Season) {
				Season season = (Season) detail;
				if (Log.ALL_ON) {
					Log.printDebug("ShowcaseView, season.channel=" + season.channel);
					Log.printDebug("ShowcaseView, season.network=" + season.network);
				}
				if (season.channel != null) {
					flag = season.channel.logos[BrandingElement.LOGO_ICON];
				} else if (season.network != null) {
					flag = season.network.logos[BrandingElement.LOGO_ICON];
				}
			}
			return flag;
		}
	}
}

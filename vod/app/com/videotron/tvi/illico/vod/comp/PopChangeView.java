package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ArrowEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CarouselViewUI;
import com.videotron.tvi.illico.vod.ui.ListOfTitlesUI;
import com.videotron.tvi.illico.vod.ui.WallofPostersFilmInfoUI;

public class PopChangeView extends BaseUI implements LayeredKeyHandler {
	public static final int VIEW_NORMAL = 0;
	public static final int VIEW_WALL = 1;
	public static final int VIEW_CARROUSEL = 2;
	public static final int VIEW_LIST = 3;
	private LayeredUI ui;
	int focus;
	int initialFocus;
	boolean changed;
	private static PopChangeView instance;
	private PopChangeViewRenderer renderer = new PopChangeViewRenderer();
	private ArrowEffect arrowEffect;
	private ClickingEffect clickEffect;

	public static PopChangeView getInstance() {
		if (instance == null) {
			instance = new PopChangeView();
		}
		return instance;
	}

	private PopChangeView() {
		ui = WindowProperty.VOD_CHANGE_VIEW.createLayeredDialog(renderer,
				Constants.SCREEN_BOUNDS, this);
		// renderer.prepare();
		arrowEffect = new ArrowEffect(renderer, 0);
	}

	public void notifyShown(long time) {
	}

	public void notifyHidden(boolean selection) {
	}

	public void show(BaseUI currentScene) {
		Log.printDebug("PopChangeView show");
		renderer.prepare();
		if (currentScene instanceof ListOfTitlesUI) {
			initialFocus = VIEW_NORMAL;
		} else if (currentScene instanceof WallofPostersFilmInfoUI) {
			initialFocus = VIEW_WALL;
		} else if (currentScene instanceof CarouselViewUI) {
			initialFocus = VIEW_CARROUSEL;
		} else {
			initialFocus = VIEW_LIST;
		}
		focus = initialFocus;
		changed = false;
		ui.activate();
		clickEffect = new ClickingEffect(renderer, 5);
	}

	public void stop() {
		ui.deactivate();
	}

	public boolean handleKeyEvent(UserEvent e) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PopChangeView handleKeyEvent "
					+ e.getType() + "  code : " + e.getCode());
		}
		if (e.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}
		int key = e.getCode();
		switch (key) {
		case HRcEvent.VK_LEFT:
			keyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_UP:
			if (focus >= 10) {
				focus -= 10; // this value means focus on buttons
				renderer.repaint();
			}
			break;
		case HRcEvent.VK_DOWN:
			if (focus < 4) {
				focus += 10; // this value means focus on cancel button
				renderer.repaint();
			}
			break;
		case HRcEvent.VK_ENTER:
			keyOK();
			break;
		case KeyCodes.COLOR_A:
			focus = (focus + 1) % 4;
			focusChanged();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
		case OCRcEvent.VK_EXIT:
			keyBack();
			break;
		default:
			break;
		}
		return true;
	}

	public void keyOK() {
		if (focus >= 10) {
			clickEffect.start(403, 336, 156, 32);
			keyBack();
		} else {
			VbmController.getInstance().writeLayoutChange(MenuController.getInstance().getLastView());
			stop();
		}
	}

	public void keyLeft() {
		if (0 < focus && focus < 4) {
			Rectangle arrowBounds = new Rectangle();
			arrowBounds.x = 281 + focus * 101;
			arrowBounds.y = 250;
//			arrowEffect.start(arrowBounds, ArrowEffect.LEFT);
			focus--;
			focusChanged();
		}
	}

	public void keyRight() {
		if (0 <= focus && focus < 3) {
			Rectangle arrowBounds = new Rectangle();
			arrowBounds.x = 358 + focus * 101;
			arrowBounds.y = 250;
//			arrowEffect.start(arrowBounds, ArrowEffect.RIGHT);
			focus++;
			focusChanged();
		}
	}

	public void keyBack() {
		if (changed) {
			menu.setChageViewType(initialFocus);
			menu.getCurrentScene().handleKey(KeyCodes.COLOR_A);
		}
		stop();
	}

	private void focusChanged() {
		menu.setChageViewType(focus);
		if (menu.getCurrentScene() != null) {
			menu.getCurrentScene().handleKey(KeyCodes.COLOR_A);
		}
		renderer.repaint();
		changed = true;
	}

	private class PopChangeViewRenderer extends LayeredWindow {
		private DataCenter dataCenter = DataCenter.getInstance();
//		private Image i05PopshMid = dataCenter.getImage("05_popsh_mid.png");
//		private Image i05PopSha = dataCenter.getImage("05_pop_sha.png");
//		private Image i05SepMid = dataCenter.getImage("05_sep_mid.png");
//		private Image i05HighMid = dataCenter.getImage("05_high_mid.png");
//		private Image i05LineMid = dataCenter.getImage("05_line_mid.png");
//		private Image i05Viewicon01 = dataCenter.getImage("05_viewicon01.png");
//		private Image i05Viewicon02 = dataCenter.getImage("05_viewicon02.png");
//		private Image i05Viewicon03 = dataCenter.getImage("05_viewicon03.png");
//		private Image i05Viewicon04 = dataCenter.getImage("05_viewicon04.png");
//		private Image i05ViewiconFoc = dataCenter.getImage("05_viewicon_foc.png");
//		private Image i_05_focus_dim = dataCenter.getImage("05_focus_dim.png");
//		private Image i_05_focus = dataCenter.getImage("05_focus.png");
//		private Image i_01_arrow_l = dataCenter.getImage("01_arrow_l.png");
//		private Image i_01_arrow_r = dataCenter.getImage("01_arrow_r.png");

		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);
		private Color popBg = new Color(30, 30, 30);
		private Color cTitle = new Color(252, 202, 4);
		private Color c182 = new Color(182, 182, 182);
		private Color c3 = new Color(3, 3, 3);

		private String[] lables;
		private String[] msg;

		public void prepare() {
			setBounds(Constants.SCREEN_BOUNDS);
			lables = new String[4];
			lables[0] = dataCenter.getString(Resources.TEXT_LBL_NORMAL);
			lables[1] = dataCenter.getString(Resources.TEXT_LBL_WALL_POSTER);
			lables[2] = dataCenter.getString(Resources.TEXT_LBL_CARROUSEL);
			lables[3] = dataCenter.getString(Resources.TEXT_LBL_LIST);
			msg = new String[1];
			msg[0] = dataCenter.getString(Resources.TEXT_MSG_CHANGEVIEW1);
			// msg[1] = dataCenter.getString(Resources.TEXT_MSG_CHANGEVIEW2);
		}

		public void paint(Graphics g) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
//			g.drawImage(dataCenter.getImage("05_popsh_mid.png"), 229, 113, this);
//			g.drawImage(dataCenter.getImage("05_pop_sha.png"), 259, 382, 442, 79, this);

			g.setColor(popBg);
			g.fillRect(259, 143, 442, 242);
			g.drawImage(dataCenter.getImage("05_sep_mid.png"), 227, 181, 505, 16, this);
//			g.drawImage(dataCenter.getImage("05_high_mid.png"), 259, 143, 442, 200, this);
			//g.drawImage(i05LineMid, 250, 307, this);

			g.setFont(FontResource.BLENDER.getFont(24));
			g.setColor(cTitle);
			GraphicUtil.drawStringCenter(g, dataCenter
					.getString(Resources.TEXT_VIEW_CHANGE), 481, 169);

			g.setFont(FontResource.BLENDER.getFont(18));
			g.setColor(Color.white);
			GraphicUtil.drawStringCenter(g, msg[0], 481, 211);

			g.drawImage(dataCenter.getImage("05_viewicon01.png"), 300, 234, this);
			g.drawImage(dataCenter.getImage("05_viewicon02.png"), 401, 234, this);
			g.drawImage(dataCenter.getImage("05_viewicon03.png"), 502, 234, this);
			g.drawImage(dataCenter.getImage("05_viewicon04.png"), 603, 234, this);
			if (focus < 4) {
				g.drawImage(dataCenter.getImage("05_viewicon_foc.png"), 299 + focus * 101, 233, this);
				if (focus != 0) {
					g.drawImage(dataCenter.getImage("02_ars_l.png"), 281 + focus * 101, 250, this);
				}
				if (focus != 3) {
					g.drawImage(dataCenter.getImage("02_ars_r.png"), 358 + focus * 101, 250, this);
				}
				g.drawImage(dataCenter.getImage("05_focus_dim.png"), 403, 336, this);
			} else {
				g.drawImage(dataCenter.getImage("05_focus.png"), 403, 336, this);
			}
			g.setColor(c3);
			g.setFont(FontResource.BLENDER.getFont(18));
			GraphicUtil.drawStringCenter(g, dataCenter
					.getString(Resources.TEXT_CANCEL), 481, 357);

			for (int i = 0; i < lables.length; i++) {
				if (i == focus) {
					g.setFont(FontResource.BLENDER.getFont(20));
					g.setColor(cTitle);
				} else {
					g.setFont(FontResource.BLENDER.getFont(18));
					g.setColor(c182);
				}
				GraphicUtil.drawStringCenter(g, lables[i], 329 + i * 101, 313);
			}
		}
	}

}

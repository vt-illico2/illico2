package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;

import org.dvb.ui.DVBBufferedImage;
import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ArrowEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.ComponentPool;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.BookmarkManager;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.CatalogueDatabase;
import com.videotron.tvi.illico.vod.data.vcds.ImageRetriever;
import com.videotron.tvi.illico.vod.data.vcds.type.AvailableSource;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.BrandingElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Bundle;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.Channel;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Extra;
import com.videotron.tvi.illico.vod.data.vcds.type.ImageGroup;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Network;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.data.vcds.type.Season;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.BundleDetailUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.ListOfTitlesUI;
import com.videotron.tvi.illico.vod.ui.WallofPostersFilmInfoUI;

public class PosterArray extends UIComponent {
	// focused portrait
	private static Rectangle FPS = new Rectangle(25, 7, ImageRetriever.PR_MD.width, ImageRetriever.PR_MD.height);
	// normal portrait
	private static Rectangle NPS = new Rectangle(0, 41, ImageRetriever.PR_SM.width, ImageRetriever.PR_SM.height);
	// focused landscape
	private static Rectangle FLS = new Rectangle(25, 32, ImageRetriever.LD_MD.width, ImageRetriever.LD_MD.height);
	// normal landscape
	private static Rectangle NLS = new Rectangle(0, 67, ImageRetriever.LD_SM.width, ImageRetriever.LD_SM.height);
	// focused portrait for grid
	private static Rectangle FPS_W = new Rectangle(-15, 43, ImageRetriever.PR_SM.width, ImageRetriever.PR_SM.height);
	// normal portrait for grid
	private static Rectangle NPS_W = new Rectangle(FPS_W);
	// focused landscape for grid
	private static Rectangle FLS_W = new Rectangle(0, 43, ImageRetriever.LD_SM.width, ImageRetriever.LD_SM.height);
	// normal landscape for grid
	private static Rectangle NLS_W = new Rectangle(FLS_W);
	// focused portrait for bundle
	private static Rectangle FPS_B = new Rectangle(0, 21, ImageRetriever.PR_SM.width, ImageRetriever.PR_SM.height);
	// normal portrait for bundle
	private static Rectangle NPS_B = new Rectangle(FPS_B);

	private static int GAP_LG = 28;
	private static int GAP_SM = 10;
	private static int GAP_FO1 = 29;
	private static int GAP_FO2 = 20;
	private static int GAP_BUN = 27;
	
	private static DataCenter dataCenter = DataCenter.getInstance();

	private Renderer renderer = PosterArrayRenderer.getInstance();
	private MoreDetail[] elements;
	private int row, col, width, height;
	private int offset, line;
	private int oldOffset, oldFocus, newFocus;
	private int startIdx, endIdx;
	private boolean hybridFocus; // or fixed focus
	private boolean dataLoop; // display by looping or not
	private boolean overflow;
	private boolean episodeMode;
	private boolean channelMode;
	private HashMap dataForRenderer;
	
	private FlowEffect flowEffect;
	private Rectangle focusR = new Rectangle();
	private Rectangle rightR = new Rectangle();
	private Rectangle leftR = new Rectangle();
	private boolean notAnimation = true;
	
	private ArrowEffect arrowEffect;
	private Point[] arrows = new Point[] {
			new Point(), new Point(), new Point(), new Point(),
	};
	
	private boolean retrieving;
	private boolean preActiveForward, preActivating;
	private int preActiveCount;
	
	// VDTRMASTER-5757
    private CategoryContainer lastCategoryContainer;

	private PosterArray() {
		setRenderer(renderer);
		prepare();
		dataForRenderer = new HashMap();
		
		arrowEffect = new ArrowEffect(this, 0);
	}
	
	class FlowEffect extends Effect {
		boolean rightToLeft;
		
		int[] enlargeX;
		int[] enlargeY;
		int[] enlargeW;
		int[] enlargeH;
		int[] shrinkX;
		int[] shrinkY;
		int[] shrinkW;
		int[] shrinkH;
		int[] deltaPull;
		int[] deltaPush;
		int pullX;
		int pushX;
		Image leftSh;
		Image rightSh;
		int shY;
		Color cTitle = new Color(255, 205, 12);
		
		static final int FRAME = 5;
		public FlowEffect(Component c, int n) {
			super(c, FRAME);
			enlargeX = new int[frameCount];
			enlargeY = new int[frameCount];
			enlargeW = new int[frameCount];
			enlargeH = new int[frameCount];
			shrinkX = new int[frameCount];
			shrinkY = new int[frameCount];
			shrinkW = new int[frameCount];
			shrinkH = new int[frameCount];
			deltaPull = new int[frameCount];
			deltaPush = new int[frameCount];
		}

		public void ready(boolean rightToLeft) {
			this.rightToLeft = rightToLeft;
			Rectangle nextFocus = new Rectangle();
			if ((rightToLeft && rightR.width == NPS.width && rightR.height == NPS.height) 
				|| (rightToLeft == false && leftR.width == NPS.width && leftR.height == NPS.height)) {
				nextFocus.x = getWidth() / 2 - FPS.width / 2;
				nextFocus.y = FPS.y;
				nextFocus.width = FPS.width;
				nextFocus.height = FPS.height;
			} else {
				nextFocus.x = getWidth() / 2 - FLS.width / 2;
				nextFocus.y = FLS.y;
				nextFocus.width = FLS.width;
				nextFocus.height = FLS.height;
			}
		
			int fcs = (frameCount - 1) * (frameCount - 1);
			
			Rectangle delta = new Rectangle();
			delta.x = nextFocus.x - (rightToLeft ? rightR.x : leftR.x);
			delta.y = nextFocus.y - (rightToLeft ? rightR.y : leftR.y);
			delta.width = nextFocus.width - (rightToLeft ? rightR.width : leftR.width);
			delta.height = nextFocus.height - (rightToLeft ? rightR.height : leftR.height);
			
			for (int i = 0; i < frameCount; i++) {
				enlargeX[i] = (rightToLeft ? rightR.x : leftR.x) + Math.round((float) delta.x * i * i / fcs);
				enlargeY[i] = (rightToLeft ? rightR.y : leftR.y) + Math.round((float) delta.y * i * i / fcs);
				enlargeW[i] = (rightToLeft ? rightR.width : leftR.width) + Math.round((float) delta.width * i * i / fcs);
				enlargeH[i] = (rightToLeft ? rightR.height : leftR.height) + Math.round((float) delta.height * i * i / fcs);
			}
			
			int gap = hybridFocus ? GAP_FO2 : GAP_LG;
			Rectangle nextShrink = new Rectangle();
			if (focusR.width == FPS.width && focusR.height == FPS.height) {
				nextShrink.x = rightToLeft ? nextFocus.x - gap - NPS.width : nextFocus.x + nextFocus.width + gap;
				nextShrink.y = NPS.y;
				nextShrink.width = NPS.width;
				nextShrink.height = NPS.height;
			} else {
				nextShrink.x = rightToLeft ? nextFocus.x - gap - NLS.width : nextFocus.x + nextFocus.width + gap;
				nextShrink.y = NLS.y;
				nextShrink.width = NLS.width;
				nextShrink.height = NLS.height;
			}
			
			delta.x = nextShrink.x - focusR.x;
			delta.y = nextShrink.y - focusR.y;
			delta.width = nextShrink.width - focusR.width;
			delta.height = nextShrink.height - focusR.height;
			
			for (int i = 0; i < frameCount; i++) {
				shrinkX[i] = focusR.x + Math.round((float) delta.x * i * i / fcs);
				shrinkY[i] = focusR.y + Math.round((float) delta.y * i * i / fcs);
				shrinkW[i] = focusR.width + Math.round((float) delta.width * i * i / fcs);
				shrinkH[i] = focusR.height + Math.round((float) delta.height * i * i / fcs);
			}
			
			int rightRight = rightR.x + rightR.width + GAP_SM;
			int nextRight = nextFocus.x + nextFocus.width + gap;
			int leftLeft = leftR.x - GAP_SM;
			int nextLeft = nextFocus.x - gap;
			delta.x = rightToLeft ? nextRight - rightRight : nextLeft - leftLeft;
			pullX = rightToLeft ? rightRight : (leftLeft - 480);
			for (int i = 0; i < frameCount; i++) {
				deltaPull[i] = pullX + Math.round((float) delta.x * i * i / fcs);
			}
			
			nextRight += GAP_SM + (focusR.width == FPS.width && focusR.height == FPS.height ? NPS.width : NLS.width);
			nextLeft -= GAP_SM + (focusR.width == FPS.width && focusR.height == FPS.height ? NPS.width : NLS.width) + 480;
			pushX = rightToLeft ? focusR.x - gap - 480 : focusR.x + focusR.width + gap;
			delta.x = rightToLeft ? nextLeft - pushX : nextRight - pushX;
			for (int i = 0; i < frameCount; i++) {
				deltaPush[i] = pushX + Math.round((float) delta.x * i * i / fcs);
			}
			
			shY = 103 - getY();
			
			BaseUI last = MenuController.getInstance().getLastScene();
			Channel channel = null;
			ImageGroup bg = null;
//			if (last != null 
//					&& last instanceof CategoryUI
//					&& ((CategoryUI) last).isBranded()
//					&& (channel = ((CategoryUI) last).getChannel()) != null) {
			//if (CategoryUI.getInstance().getBrandingElement() != null || (last instanceof CategoryUI && ((CategoryUI) last).isBranded())) {
			if (BaseRenderer.hasBrandingBg()) {
				leftSh = null;
				rightSh = null;
			} else {
//				leftSh = dataCenter.getImage("01_poster_sha_l.png");
//				rightSh = dataCenter.getImage("01_poster_sha_r.png");
			}
			
			frameCount = FRAME - 1;
		}
		
		protected void animate(Graphics g, DVBBufferedImage image, int frame) {
			Rectangle src = rightToLeft ? rightR : leftR;
			g.drawImage(image, enlargeX[frame], enlargeY[frame], enlargeX[frame] + enlargeW[frame], enlargeY[frame] + enlargeH[frame], 
					src.x, src.y, src.x + src.width, src.y + src.height, null);
			
			g.drawImage(image, shrinkX[frame], shrinkY[frame], shrinkX[frame] + shrinkW[frame], shrinkY[frame] + shrinkH[frame], 
					focusR.x, focusR.y, focusR.x + focusR.width, focusR.y + focusR.height, null);
			
			int h = 200;
			g.drawImage(image, deltaPull[frame], 40, deltaPull[frame] + 480, h,
					pullX, 40, pullX + 480, h, null);
			
			g.drawImage(image, deltaPush[frame], 40, deltaPush[frame] + 480, h,
					pushX, 40, pushX + 480, h, null);
			
			if (leftSh != null) {
				g.drawImage(leftSh, 0, shY, null);
			}
			if (rightSh != null) {
				g.drawImage(rightSh, 856, shY, null);
			}
		}
		
	}

	public static int instanceCounter = 0;

	public static PosterArray newInstance() {
		instanceCounter++;
		PosterArray newInstance = new PosterArray();
		ComponentPool.addComponent(newInstance);
		return newInstance;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public void setRowCol(int row, int col) {
		setRow(row);
		setCol(col);
	}
	
	public int getRow() {
		return row;
	}
	
	public int getCol() {
		return col;
	}

	public void setRowHeight(int height) {
		this.height = height;
	}

	public void setColWidth(int width) {
		this.width = width;
	}

	public void setOverflow(boolean overflow) {
		this.overflow = overflow;
	}
	
	public void setEpisodeMode(boolean episodeMode) {
		this.episodeMode = episodeMode;
	}
	
	public void setChannelMode(boolean channelMode) {
		this.channelMode = channelMode;
	}

	public BaseElement getFocused() {
		return elements.length > offset + focus ? elements[offset + focus] : null;
	}
	
	public BaseElement[] getPainted() {
		try {
			if (elements == null || elements.length == 0) { 
				//Log.printDebug("getPainted(), null elements");
				return new BaseElement[0];
			}
			//Log.printDebug("getPainted(), s = " + startIdx + ", e = " + endIdx);
			if (endIdx - startIdx < 0) { // it can occur on fixed focus with loop
				int cnt = endIdx + 1 + (elements.length - startIdx);
				BaseElement[] ret = new BaseElement[cnt];
				System.arraycopy(elements, 0, ret, 0, endIdx + 1);
				System.arraycopy(elements, startIdx, ret, endIdx + 1, cnt - endIdx - 1);
				return ret;
			}
			BaseElement[] ret = new BaseElement[endIdx - startIdx + 1];
			System.arraycopy(elements, startIdx, ret, 0, ret.length);
			return ret;
		} catch (ArrayIndexOutOfBoundsException e) {
			Log.printDebug("getPainted(), exception");
			return new BaseElement[0];
		}
	}
	
	public BaseElement[] getNextPainted(int max) {
		if (elements == null || elements.length == 0) { 
			return new BaseElement[0];
		}
		ArrayList list = new ArrayList();
		
		int idx;
		for (int offset = 1; list.size() < max; offset++) {
			int size = list.size();
			idx = endIdx + offset;
			if (idx < elements.length && elements[idx] != null) {
				list.add(elements[idx]);
			}
			idx = startIdx - offset;
			if (idx >= 0 && elements[idx] != null) {
				list.add(elements[idx]);
			}
			if (size == list.size()) {
				break;
			}
		}
		BaseElement[] ret = new BaseElement[list.size()];
		list.toArray(ret);
		return ret;
	}

	public int getIndex() {
		return offset + focus;
	}
	
	public void resetIndex() {
		offset = 0;
		focus = 0;
	}
	
	public void setData(BaseElement[] elements) {
		this.elements = new MoreDetail[elements.length];
		for (int i = 0; i < elements.length; i++) {
			this.elements[i] = (MoreDetail) elements[i];
		}
		line = elements.length / col;
		if (elements.length % col != 0) {
			line++;
		}
		dataForRenderer.clear();
		
		// VDTRMASTER-5757
        lastCategoryContainer = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
	}

	public void setFocused(BaseElement f) {
		if (f == null) {
			return;
		}
		int idx = 0;
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == f) {
				idx = i;
				break;
			}
		}
		if (row > 1) { // grid
			offset = idx / col * col;
			if (offset > col * 2) {
				offset -= col;
			}
			focus = idx - offset;
		} else if (hybridFocus) { // hybrid
			for (int i = 0; i < elements.length; i++) {
				if (f == elements[offset + focus]) {
					return;
				}
				if (focus == col / 2 && offset < elements.length - col) {
					offset++;
				} else {
					if (focus == col - 1 || focus == elements.length - 1) {
						continue;
					}
					focus++;
				}
			}

		} else { // fixed
			focus = idx;
		}
	}

	public void setFocusMode(boolean hybrid) {
		hybridFocus = hybrid;
	}

	public void setDataLoop(boolean loop) {
		dataLoop = loop;
	}
	
	public void animationEnded(Effect effect) {
		if (effect == flowEffect) {
			notAnimation = true;
			focus = newFocus;
			flowEffect = null;
		}
	}

	public boolean keyRight() {
		if (elements == null || elements.length < 2)
			return false;
		if (arrows[ArrowEffect.RIGHT].x != -1) {
			Rectangle arrowBounds = new Rectangle();
			arrowBounds.x = arrows[ArrowEffect.RIGHT].x;
			arrowBounds.y = arrows[ArrowEffect.RIGHT].y;
//			arrowEffect.start(arrowBounds, ArrowEffect.RIGHT);
		}
		if (row > 1) {
			if (focus % col == col - 1) // most right
				return false;
			if ((offset + focus) / col + 1 == line
					&& offset + focus == elements.length - 1) { // last element
				if (focus >= col) { // and not first row
					focus = focus + 1 - col;
				}
			} else {
				focus++;
			}
		} else if (hybridFocus) {
			if (focus == col / 2 && offset < elements.length - col) {
				if (getParent() instanceof BundleDetailUI == false) {
					if (flowEffect == null) {
						flowEffect = new FlowEffect(this, 0);
					}
					newFocus = focus;
					if (MenuController.getInstance().isContinousEvent() == false) {
						notAnimation = false;
						flowEffect.ready(true);
						flowEffect.start();
						notAnimation = true;
					}
				}
				offset++;
			} else {
				if (focus == col - 1 || focus == elements.length - 1) {
					return false;
				}
				focus++;
			}
		} else { // fixed focus
			oldOffset = offset;
			oldFocus = focus;
			newFocus = (focus + 1) % elements.length;
			
			if (retrieveNext(true)) {
				if (flowEffect == null) {
					flowEffect = new FlowEffect(this, 0);
				}
				flowEffect.ready(true);
				if (retrieving == false) {
					if (MenuController.getInstance().isContinousEvent()) {
						focus = newFocus;
					} else {
						notAnimation = false;
						flowEffect.start();
					}
				}
			}
		}
		repaint();
		return true;
	}

	public boolean keyLeft() {
		if (elements == null || elements.length < 2)
			return false;
		if (arrows[ArrowEffect.LEFT].x != -1) {
			Rectangle arrowBounds = new Rectangle();
			arrowBounds.x = arrows[ArrowEffect.LEFT].x;
			arrowBounds.y = arrows[ArrowEffect.LEFT].y;
//			arrowEffect.start(arrowBounds, ArrowEffect.LEFT);
		}
		if (row > 1) {
			if (focus % col == 0)
				return false;
			focus--;
		} else if (hybridFocus) {
			if (focus == 0)
				return false;
			if (focus == col / 2 && offset > 0) {
				if (getParent() instanceof BundleDetailUI == false) {
					if (flowEffect == null) {
						flowEffect = new FlowEffect(this, 0);
					}
					newFocus = focus;
					if (MenuController.getInstance().isContinousEvent() == false) {
						notAnimation = false;
						flowEffect.ready(false);
						flowEffect.start();
						notAnimation = true;
					}
				}
				offset--;
			} else {
				focus--;
			}
		} else { // fixed focus
			oldOffset = offset;
			oldFocus = focus;
			newFocus = (focus - 1 + elements.length) % elements.length;
			
			if (retrieveNext(false)) {
				if (flowEffect == null) {
					flowEffect = new FlowEffect(this, 0);
				}
				flowEffect.ready(false);
				if (retrieving == false) {
					if (MenuController.getInstance().isContinousEvent()) {
						focus = newFocus;
					} else {
						notAnimation = false;
						flowEffect.start();
					}
				}
			}
		}
		repaint();
		return true;
	}

	public boolean keyDown() {
		if (row == 1) {
			return false;
		}
		if (arrows[ArrowEffect.DOWN].x != -1) {
			Rectangle arrowBounds = new Rectangle();
			arrowBounds.x = arrows[ArrowEffect.DOWN].x;
			arrowBounds.y = arrows[ArrowEffect.DOWN].y;
//			arrowEffect.start(arrowBounds, ArrowEffect.DOWN);
		}
		int curLine = (focus) / col + 1;
		if (curLine < row && curLine < line) {
			focus += col;
			while (offset + focus >= elements.length) {
				focus--;
			}
			retrieveNext(true);
		} else if ((offset + focus) / col + 1 < line) {
			oldOffset = offset;
			oldFocus = focus;
			
			offset += col;
			while (offset + focus >= elements.length) {
				focus--;
			}
			return retrieveNext(true);
		} else {
			return false;
		}
		repaint();
		return true;
	}

	public boolean keyUp() {
		if (row == 1) {
			return false;
		}
		if (arrows[ArrowEffect.UP].x != -1) {
			Rectangle arrowBounds = new Rectangle();
			arrowBounds.x = arrows[ArrowEffect.UP].x;
			arrowBounds.y = arrows[ArrowEffect.UP].y;
//			arrowEffect.start(arrowBounds, ArrowEffect.UP);
		}
		int curLine = (focus) / col + 1;
		if (curLine > 1) {
			focus -= col;
			retrieveNext(false);
		} else if (offset > 0) {
			oldOffset = offset;
			oldFocus = focus;
			
			offset -= col;
			retrieveNext(false);
		} else {
			return false;
		}
		repaint();
		return true;
	}

	public boolean keyNext() {
		if (row > 1) { // grid
//			int curLine = (offset + focus) / col + 1;
//			if (line == curLine) {
//				return false;
//			} else if (offset >= elements.length / (col * 2) * (col * 2)) {
//				return false;
//			}
			if (elements.length - (offset + col * 2) <= 0) {
				return false;
			}
			
			oldOffset = offset;
			oldFocus = focus;
			
			offset += col;// * 2;
			while (offset + focus >= elements.length) {
				focus--;
			}
			/*return */retrieveNext(true);
			
			offset += col;// * 2;
			while (offset + focus >= elements.length) {
				focus--;
			}
			oldOffset = offset;
			oldFocus = focus;
			return retrieveNext(true);
			
		} else {
			oldOffset = offset;
			oldFocus = focus;
			for (int i = 0; i < 7 && i < elements.length; i++) {
				if (hybridFocus) {
					if (focus == col / 2 && offset < elements.length - col) {
						offset++;
					} else {
						if (focus == col - 1 || focus == elements.length - 1) {
							break;
						}
						focus++;
					}
				} else {
					focus = (focus + 1) % elements.length;
				}
				retrieveNext(true);
			}
			
			repaint();
		}
		return true;
	}

	public boolean keyPrev() {
		if (row > 1) { // grid
			oldOffset = offset;
			oldFocus = focus;
			
			offset = Math.max(0, offset - col/* * 2*/);
			//repaint();
			/*return */retrieveNext(false);
			
			offset = Math.max(0, offset - col/* * 2*/);
			oldOffset = offset;
			oldFocus = focus;
			return retrieveNext(false);
		} else {
			oldOffset = offset;
			oldFocus = focus;
			for (int i = 0; i < 7 && i < elements.length; i++) {
				if (hybridFocus) {
					if (focus == 0)
						break;
					if (focus == col / 2 && offset > 0) {
						offset--;
					} else {
						focus--;
					}
				} else {
					focus = (focus - 1 + elements.length) % elements.length;
				}
				retrieveNext(false);
			}
			
			repaint();
		}
		return true;
	}

	public boolean keyOK() {
		if (elements != null && elements.length > offset + focus) {
			if (getParent() instanceof ListOfTitlesUI && ((ListOfTitlesUI) getParent()).getSubTitle().length() > 0 || episodeMode) {
				((ListOfTitlesUI) getParent()).processFirstActionMenu();
			} else {
				// 20190222 Touche
				VbmController.getInstance().writeSelectionDummy();
				MenuController.getInstance().goToDetail(elements[offset + focus], null);
			}
		}
		return true;
	}
	
	private boolean retrieveNext(boolean forward) {
		if (retrieving) {
			offset = oldOffset;
			focus = oldFocus;
			return false;
    	}
		boolean hasAll = true;
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == null) {
				hasAll = false;
				break;
			}
		}
		if (hasAll) {
			repaint();
			return true;
		}
		
		int next = 0;
		if (row > 1) {
			if (forward) { // down-way
				next = Math.min(elements.length - 1, focus + offset + col);
			} else { // up-way
				next = (focus + offset - col + elements.length) % elements.length;
			}
			int lineStart = next / col * col;
			int lineEnd = Math.min(elements.length, lineStart + col);
			for (int i = lineStart; i < lineEnd; i++) {
				if (elements[i] == null) {
					next = i;
					break;
				}
			}
		} else {
			if (forward) { // right-way
				next = Math.min(elements.length - 1, focus + offset + col / 2 + 2);
			} else { // left-way
				next = (focus + offset - col / 2 - 2 + elements.length) % elements.length;
			}
		}
		
		boolean preActiveMode = false;
		int preActive = DataCenter.getInstance().getInt("PRE_ACTIVE");
		if (preActiveForward == forward) {
			preActiveCount++;
		} else {
			preActiveCount = 1;
			preActiveForward = forward;
		}
		
		if ((preActive == 0 && elements[next] != null) || (elements[next] != null && preActiveCount < preActive || preActivating)) {
			repaint();
			return true;
		}
		
		if (preActive > 0 && preActiveCount >= preActive) {
			preActiveCount = 0;
			preActiveMode = true;
			if (forward) {
				while (next < elements.length && elements[next++] != null);
				next--;
			} else {
				while (next >= 0 && elements[next--] != null);
				if (next < 0) {
					next = elements.length - 1;
					while (next >= 0 && elements[next--] != null);
				}
			}
		}
		
		int size = 0;
		if (forward) {
			while (next >= 0 && elements[next--] == null);
			next += 2;
			for (int i = next; i < elements.length; i++) {
				if (elements[i] == null) {
					size++;
				} else {
					break;
				}
			}
			size = Math.min(DataCenter.getInstance().getInt("PAGE_SIZE"), size);//elements.length - next);
		} else {
			while (next < elements.length && elements[next++] == null);
			next--;
			int max = DataCenter.getInstance().getInt("PAGE_SIZE");
			size = 0;
			while (size < max) {
				next--;
				if (next < 0 || elements[next] != null) {
					next++;
					break;
				}
				size++;
			}
		}
		
		if (size == 0) {
			repaint();
			return true;
		}
		
		final int start = next;
		final int pageSize = size;
		final boolean needLoadingPopup = !preActiveMode;
		if (needLoadingPopup) {
			retrieving = true;
		}
		if (preActiveMode) {
			preActivating = true;
		}
		new Thread("PosterArray, retrieveNext") {
			public void run() {
				if (needLoadingPopup) {
					MenuController.getInstance().showLoadingAnimation(true);
				}
				synchronized (CatalogueDatabase.getInstance()) {
					//VDTRMASTER-5756
					if (lastCategoryContainer != null) {
						CatalogueDatabase.getInstance().retrieveCategoryContents(null, lastCategoryContainer.id, null,
								start, pageSize, "", !needLoadingPopup);
					} else {
						CatalogueDatabase.getInstance().retrieveCategoryContents(null, null, null, start, pageSize,
								"", !needLoadingPopup);
					}
					try {
						CatalogueDatabase.getInstance().wait(Constants.MS_PER_MINUTE);
					} catch (InterruptedException e) {}
				} 
				CategoryContainer cat = CatalogueDatabase.getInstance().getLastRequestedCategoryContainer();
				if (cat != null) {
					UIComponent ui = MenuController.getInstance().getCurrentScene();
					if (ui instanceof WallofPostersFilmInfoUI) {
						((WallofPostersFilmInfoUI) ui).setData(cat.totalContents, null);
					} else if (ui instanceof ListOfTitlesUI) {
						((ListOfTitlesUI) ui).setData(cat.totalContents);
					}
				}
				if (needLoadingPopup) {
					retrieving = false;
					MenuController.getInstance().hideLoadingAnimation();
					if (flowEffect != null) {
						notAnimation = false;
						flowEffect.start();
					}
				} else {
					preActivating = false;
				}
			}
		}.start();
		repaint();
		return true;
	}

	public void stop() {
		renderer = null;
		// for (int i = 0; elements != null && i < elements.length; i++) {
		// elements[i] = null;
		// }
		elements = null;
		instanceCounter--;
	}

	private BaseUI getContainer() {
		if (getParent() instanceof BaseUI) {
			return (BaseUI) getParent();
		}
		return null;
	}

	// R7.3
	public CategoryContainer getLastCategoryContainer() {
	    return lastCategoryContainer;
    }

	static class PosterArrayRenderer extends Renderer {
		private static PosterArrayRenderer instance;

		synchronized public static PosterArrayRenderer getInstance() {
			if (instance == null) {
				instance = new PosterArrayRenderer();
			}
			return instance;
		}

		private Color cTitle = new Color(255, 205, 12);
		Color c75 = new Color(75, 75, 75);
		// 4K
		DVBColor posterDimmedColor = new DVBColor(0, 0, 0, 200);
		DVBColor chDimmedColor = new DVBColor(0, 0, 0, 104);
		
		private Image arrUp;// = dataCenter.getImage("02_ars_t.png");
		private Image arrDn;// = dataCenter.getImage("02_ars_b.png");
		private Image arrLeft;// = dataCenter.getImage("02_ars_l.png");
		private Image arrRight;// = dataCenter.getImage("02_ars_r.png");
		private Image i02_icon_con;// = dataCenter.getImage("02_icon_con.png");
		private Image blockedPortrait;// = dataCenter.getImage("01_poster_block.png");
		private Image blockedLandscape;// = dataCenter.getImage("01_poster_block_h.png");
//		private Image i_01_possh_m_l;// = dataCenter.getImage("01_possh_b_m.png");
//		private Image i_01_poshigh_m_l;// = dataCenter.getImage("01_poshigh_b_m.png");
//		private Image i_01_possh_m;// = dataCenter.getImage("01_possh_m.png");
//		private Image i_01_poshigh_m;// = dataCenter.getImage("01_poshigh_m.png");
//		private Image i_01_possh_s_l;// = dataCenter.getImage("01_possh_s_l.png");
//		private Image i_01_poshigh_s_l;// = dataCenter.getImage("01_poshigh_s_l.png");
//		private Image i_01_possh_s;// = dataCenter.getImage("01_possh_s.png");
//		private Image i_01_poshigh_s;// = dataCenter.getImage("01_poshigh_s.png");
		private Image i_01_wall_btsh;
		// R5
		private Image iconUhd;
		private Image i02_icon_fav;

		public Rectangle getPreferredBounds(UIComponent c) {
			return null;
		}

		public void prepare(UIComponent c) {
			arrUp = dataCenter.getImage("02_ars_t.png");
			arrDn = dataCenter.getImage("02_ars_b.png");
			arrLeft = dataCenter.getImage("02_ars_l.png");
			arrRight = dataCenter.getImage("02_ars_r.png");
			i02_icon_con = dataCenter.getImage("02_icon_con.png");
			blockedPortrait = dataCenter.getImage("01_poster_block.jpg");
			blockedLandscape = dataCenter.getImage("01_poster_block_h.jpg");
//			i_01_possh_m_l = dataCenter.getImage("01_possh_m_l.png");
//			i_01_poshigh_m_l = dataCenter.getImage("01_poshigh_m_l.png");
//			i_01_possh_m = dataCenter.getImage("01_possh_m.png");
//			i_01_poshigh_m = dataCenter.getImage("01_poshigh_m.png");
//			i_01_possh_s_l = dataCenter.getImage("01_possh_s_l.png");
//			i_01_poshigh_s_l = dataCenter.getImage("01_poshigh_s_l.png");
//			i_01_possh_s = dataCenter.getImage("01_possh_s.png");
//			i_01_poshigh_s = dataCenter.getImage("01_poshigh_s.png");
			i_01_wall_btsh = dataCenter.getImage("01_wall_btsh.png");
			// R5
			iconUhd = dataCenter.getImage("icon_uhd.png");
			i02_icon_fav = dataCenter.getImage("02_icon_fav.png");
		}

		protected void paint(Graphics g, UIComponent c) {
			PosterArray ui = (PosterArray) c;
			// int center = ui.getWidth() / 2;

			// g.setColor(Color.darkGray);
			// g.fillRect(0, 0, ui.getWidth(), ui.getHeight());
//			 g.setColor(Color.red);
//			 g.drawRect(0, 0, ui.getWidth()-1, ui.getHeight()-1);
//			 g.drawString("r="+ui.row+",c="+ui.col, 10, 40);
			// g.drawLine(center, 0, center, ui.getHeight());

			if (ui.elements == null || ui.elements.length == 0) {
				// GraphicUtil.drawStringCenter(g, "NO DATA", center, 100);
				return;
			}

			if (ui.row > 1) {
				paintMultiRow(g, ui);
			} else if (ui.hybridFocus) {
				if (ui.getParent() instanceof BundleDetailUI) {
					paintHybridFocusOnBundle(g, ui);
				} else {
					paintHybridFocus(g, ui);
				}
			} else {
				paintFixedFocus(g, ui);
			}
		}

		private void paintHybridFocusOnBundle(Graphics g, PosterArray ui) {
			Rectangle r = null;
			boolean isCursor = ui.getContainer().getCurrentFocusUI() == BundleDetailUI.FOCUS_POSTERS;
			boolean isFocused = ui.getContainer().getState() == BaseUI.STATE_DEACTIVATED
					&& isCursor;
			int x = 63;
			int idx = 0;
			ui.startIdx = ui.offset;
			for (int i = 0; i < ui.col; i++) {
				idx = i + ui.offset;
				if (idx >= ui.elements.length)
					break;
				if (ui.elements[idx] == null)
					break;
				
				if (isCursor && idx - ui.offset == ui.focus) {
					r = ui.elements[idx].isPortrait() ? FPS_B : NLS;
					x -= r.x;
				} else {
					r = ui.elements[idx].isPortrait() ? NPS_B : NLS;
				}
				paintPoster(g, idx, x, r.y, r.width, r.height, isCursor && idx - ui.offset == ui.focus, true, ui);
				if (isCursor && idx - ui.offset == ui.focus) {
					paintFocus(g, r, isFocused, x, idx, ui, true,
							idx < ui.elements.length - 1);
				}
				x += r.width;
				x += GAP_BUN;
				if (i == ui.focus) {
					x -= r.x;
				}
			}
			ui.endIdx = idx;
			if (r != null) {
				r = r.width == FPS_B.width || r.width == NPS_B.width ? NPS_B : NLS;
				if (ui.overflow) {
					if (idx + 1 < ui.elements.length && ui.elements.length > ui.col) {
						// paint overflow poster on right
						paintPoster(g, idx + 1, x, r.y, r.width, r.height, false, true, ui);
						ui.endIdx = idx + 1;
					}
				}
			}
		}

		private void paintHybridFocus(Graphics g, PosterArray ui) {
			Rectangle r = null;
			boolean isFocused = ui.getContainer().getState() == BaseUI.STATE_DEACTIVATED;
			int start = 109;
			int x = start;
			int idx = 0;
			ui.startIdx = ui.offset;
			int step = 0;
			Rectangle tempR;
			for (int i = 0; i < ui.col; i++) {
				idx = i + ui.offset;
				if (idx >= ui.elements.length)
					break;
				
				if (ui.elements[idx] == null)
					break;
				
				if (idx - ui.offset == ui.focus) {
					r = ui.elements[idx].isPortrait() ? FPS : FLS;
				} else {
					r = ui.elements[idx].isPortrait() ? NPS : NLS;
				}
				if (i == ui.focus && i != 0) {
					x += (GAP_FO2 - GAP_SM - 1);
				}
				paintPoster(g, idx, x, r.y, r.width, r.height, idx - ui.offset == ui.focus, true, ui);
				tempR = new Rectangle();
				tempR.x = x;
				tempR.y = r.y;
				tempR.width = r.width;
				tempR.height = r.height;
				if (idx - ui.offset == ui.focus) {
					paintFocus(g, r, isFocused, x, idx, ui, idx > 0,
							idx < ui.elements.length - 1);
					ui.focusR = tempR;
					step = 1;
				} else {
					if (step == 0) {
						ui.leftR = tempR;
					} else if (step == 1) {
						ui.rightR = tempR;
						step = 2;
					}
				}
				x += r.width;
				if (i == ui.focus) {
					if (i == 0) {
						x += GAP_FO1;
					} else {
						x += GAP_FO2;
					}
				} else {
					x += GAP_SM;
				}
			}
			ui.endIdx = Math.min(ui.elements.length-1, idx);
			
			if (r != null) {
				r = r.width == FPS_B.width || r.width == NPS_B.width ? NPS_B : NLS;
				if (ui.overflow) {
					if (idx + 1 < ui.elements.length && ui.elements.length > ui.col) {
						// paint overflow poster on right
						paintPoster(g, idx + 1, x, r.y, r.width, r.height, false, true, ui);
						ui.endIdx = idx + 1;
					}
					if (ui.offset > 0) {
						// paint overflow poster on left
						idx = ui.offset - 1;
						x = start - r.width - GAP_SM;
						paintPoster(g, idx, x, r.y, r.width, r.height, false, true, ui);
						ui.startIdx = idx;
					}
	
				}
			}
		}

		private void paintFixedFocus(Graphics g, PosterArray ui) {
			Rectangle r;
			int center = ui.getWidth() / 2;
			boolean isFocused = ui.getContainer().getCurrentFocusUI() == ListOfTitlesUI.FOCUS_POSTERS;

			int idx = ui.focus + ui.offset;
			if (ui.elements[idx] == null) {
				//return;
				r = FPS;
			} else {
				r = ui.elements[idx].isPortrait() ? FPS : FLS;
			}
			int x = center - r.width / 2;
			int left = x - GAP_LG;

			// draw center focused
			paintPoster(g, idx, x, r.y, r.width, r.height, true, ui.notAnimation, ui);
			if (ui.notAnimation) {
				paintFocus(g, r, isFocused, x, idx, ui, idx > 0 || (ui.dataLoop && ui.elements.length > 1),
					idx < ui.elements.length - 1 || (ui.dataLoop && ui.elements.length > 1));
				ui.startIdx = idx;
				ui.endIdx = idx;
				ui.focusR.x = x;
				ui.focusR.y = r.y;
				ui.focusR.width = r.width;
				ui.focusR.height = r.height;
			}
			x += r.width + GAP_LG;
			// draw right side
			boolean first = true;
			int sideCnt = 0;
			while (/*ui.elements.length > idx + 1 && */sideCnt < ui.elements.length / 2) {
				idx++;
				if (/*ui.dataLoop && */idx >= ui.elements.length) {
					idx = idx % ui.elements.length;
				}
				if (ui.elements[idx] == null) {
					//break;
					r = NPS;
				} else {
					r = ui.elements[idx].isPortrait() ? NPS : NLS;
				}

				paintPoster(g, idx, x, r.y, r.width, r.height, false, false, ui);
				ui.endIdx = idx;
				sideCnt++;
				if (first && ui.notAnimation) {
					ui.rightR.x = x;
					ui.rightR.y = r.y;
					ui.rightR.width = r.width;
					ui.rightR.height = r.height;
					first = false;
				}
				x += r.width + GAP_SM;
				if (x >= ui.getWidth())
					break; // out of bounds
			}

			// draw left side
			idx = ui.dataLoop ? ui.focus : ui.focus + ui.offset;
			x = left;
			first = true;
			sideCnt = 0;
			while (/*ui.elements.length >= idx && */sideCnt < (ui.elements.length - 1) / 2) {
				idx--;
				if (/*ui.dataLoop && */idx < 0) {
					idx = (idx + ui.elements.length) % ui.elements.length;
				}
				if (ui.elements[idx] == null) {
					//break;
					r = NPS;
				} else {
					r = ui.elements[idx].isPortrait() ? NPS : NLS;
				}

				paintPoster(g, idx, x - r.width, r.y, r.width, r.height, false, false,
						ui);
//				ui.startIdx = idx;
				sideCnt++;
				if (first && ui.notAnimation) {
					ui.leftR.x = x - r.width;
					ui.leftR.y = r.y;
					ui.leftR.width = r.width;
					ui.leftR.height = r.height;
					first = false;
				}
				x -= r.width + GAP_SM;
				if (x < 0 - r.width)
					break; // out of bounds
				ui.startIdx = idx;
			}
		}

		private void paintFocus(Graphics g, Rectangle r, boolean isFocused,
				int x, int idx, PosterArray ui, boolean left, boolean right) {
			g.setColor(isFocused ? cTitle : Color.lightGray);
			g.drawRect(x - 1, r.y - 1, r.width + 1, r.height + 1);
			g.drawRect(x - 2, r.y - 2, r.width + 3, r.height + 3);
			g.drawRect(x - 3, r.y - 3, r.width + 5, r.height + 5);
			if (ui.notAnimation) {
				g.setFont(FontResource.BLENDER.getFont(16));
				String n = String.valueOf(ui.elements.length);
				if (n.length() == 1) {
					n = "/0" + n;
				} else {
					n = "/" + n;
				}
				String c = String.valueOf(idx + 1);
				if (c.length() == 1) {
					c = "0" + c;
				}
				int nW = g.getFontMetrics().stringWidth(c + n) + 10;
				g.setColor(isFocused ? cTitle : Color.lightGray);
				g.fillRect(x + r.width - nW, r.y + r.height - 17, nW, 17);
				g.setColor(new Color(74, 72, 64));
				int pX = GraphicUtil.drawStringRight(g, n, x + r.width - 3, r.y
						+ r.height - 3);
				g.setColor(Color.black);
				GraphicUtil.drawStringRight(g, c, pX, r.y + r.height - 3);
			}
			if (isFocused) {
				if (left) {
					ui.arrows[ArrowEffect.LEFT].x = x - arrLeft.getWidth(ui) - 4;
					ui.arrows[ArrowEffect.LEFT].y = r.y + r.height / 2 - arrLeft.getHeight(ui) / 2;
					g.drawImage(arrLeft, 
							ui.arrows[ArrowEffect.LEFT].x,
							ui.arrows[ArrowEffect.LEFT].y,
//							x - arrLeft.getWidth(ui) - 5, 
//							94, 
							ui);
				} else {
					ui.arrows[ArrowEffect.LEFT].x = -1;
				}
				if (right) {
					ui.arrows[ArrowEffect.RIGHT].x = x + r.width + 5;
					ui.arrows[ArrowEffect.RIGHT].y = r.y + r.height / 2 - arrLeft.getHeight(ui) / 2;
					g.drawImage(arrRight, 
							ui.arrows[ArrowEffect.RIGHT].x,
							ui.arrows[ArrowEffect.RIGHT].y,
//							x + r.width + 5, 
//							94, 
							ui);
				} else {
					ui.arrows[ArrowEffect.RIGHT].x = -1;
				}
			}
		}

		private void paintMultiRow(Graphics g, PosterArray ui) {
			Rectangle r;

			int y = 0;
			int idx = 0;
			ui.startIdx = ui.offset;
			boolean drawOverflow = false;
			outside: for (int row = 0; row < ui.row
					|| (ui.overflow && row <= ui.row); row++) {
				if (row == ui.row) {
					drawOverflow = true;
				}
				int x = 90; // most left
				for (int col = 0; col < ui.col; col++) {
					idx = ui.offset + row * ui.col + col;
					if (idx >= ui.elements.length)
						break outside;
//					if (ui.elements[idx] == null) 
//						continue;
					boolean focus = idx - ui.offset == ui.focus;
					if (ui.elements[idx] == null) {
						r = focus ? FPS_W : NPS_W;
					} else {
						if (focus) {
							r = ui.elements[idx].isPortrait() ? FPS_W : FLS_W;
						} else {
							r = ui.elements[idx].isPortrait() ? NPS_W : NLS_W;
						}
					}
					
					//if (drawOverflow == false)
					
					// 4K
					if (ui.channelMode) {
						g.setColor(c75);
						g.fillRect(x - r.x, r.y + y, r.width, r.height);
					}
					
					paintPoster(g, idx, x - r.x, r.y + y, r.width, r.height,
							focus, true, ui);

					if (focus) {
						g.setColor(cTitle);
						g.drawRect(x - r.x - 1, r.y + y - 1, r.width + 1,
								r.height + 1);
						g.drawRect(x - r.x - 2, r.y + y - 2, r.width + 3,
								r.height + 3);
						g.drawRect(x - r.x - 3, r.y + y - 3, r.width + 5,
								r.height + 5);
						if (row > 0 || ui.offset > 0) {
							ui.arrows[ArrowEffect.UP].x = x - r.x + r.width / 2 - arrUp.getWidth(ui) / 2;
							ui.arrows[ArrowEffect.UP].y = r.y + y - arrUp.getHeight(ui);
							g.drawImage(arrUp,
									ui.arrows[ArrowEffect.UP].x,
									ui.arrows[ArrowEffect.UP].y,
//									x - r.x + r.width / 2 - arrUp.getWidth(ui)
//											/ 2, r.y + y - arrUp.getHeight(ui),
									ui);
						} else {
							ui.arrows[ArrowEffect.UP].x = -1;
						}
						if ((ui.offset + ui.focus) / ui.col + 1 < ui.line) {
							ui.arrows[ArrowEffect.DOWN].x = x - r.x + r.width / 2 - arrDn.getWidth(ui) / 2;
							ui.arrows[ArrowEffect.DOWN].y = r.y + y + r.height + 18;
							if (ui.channelMode) {
								ui.arrows[ArrowEffect.DOWN].y -= 18;
							}
							g.drawImage(arrDn,
									ui.arrows[ArrowEffect.DOWN].x,
									ui.arrows[ArrowEffect.DOWN].y,
//									x - r.x + r.width / 2 - arrDn.getWidth(ui)
//											/ 2, r.y + y + r.height + 18, 
									ui);
						} else {
							ui.arrows[ArrowEffect.DOWN].x = -1;
						}
						if (col > 0) {
							ui.arrows[ArrowEffect.LEFT].x = x - r.x - arrLeft.getWidth(ui) - 5;
							ui.arrows[ArrowEffect.LEFT].y = r.y + y + r.height / 2 - arrLeft.getHeight(ui) / 2;
							g.drawImage(
									arrLeft,
									ui.arrows[ArrowEffect.LEFT].x,
									ui.arrows[ArrowEffect.LEFT].y,
//									x - r.x - arrLeft.getWidth(ui) - 5,
//									r.y + y + r.height / 2
//											- arrLeft.getHeight(ui) / 2, 
									ui);
						} else {
							ui.arrows[ArrowEffect.LEFT].x = -1;
						}
						if (col < ui.col - 1
								&& ((ui.offset + ui.focus + 1) <= ui.elements.length - 1)) {
							ui.arrows[ArrowEffect.RIGHT].x =  x - r.x + r.width + 5;
							ui.arrows[ArrowEffect.RIGHT].y = r.y + y + r.height / 2 - arrRight.getHeight(ui) / 2;
							g.drawImage(arrRight,
									ui.arrows[ArrowEffect.RIGHT].x,
									ui.arrows[ArrowEffect.RIGHT].y,
//									x - r.x + r.width + 5, r.y
//									+ y + r.height / 2 - arrRight.getHeight(ui)
//									/ 2, 
									ui);
						} else {
							ui.arrows[ArrowEffect.RIGHT].x = -1;
						}
					}
					x += ui.width;
				}
				y += ui.height;
			}
//			if (drawOverflow) {
//				idx -= ui.col;
//			}
			ui.endIdx = Math.min(idx, ui.elements.length - 1);
			
			// 4k - don't use this in the case of displaying theme background (ex. club illico)
			if (CategoryUI.getBrandingElement() == null) {
				if (ui.overflow && ui.elements.length - ui.offset > ui.row * ui.col) {
					g.drawImage(i_01_wall_btsh, 0, 365 - ui.getY(), ui);
				}
			}
		}

		private void paintHighlight(Graphics g, int x, int y, int w, int h, PosterArray ui) {
			if (w == FPS.width && h == FPS.height) { // medium-portrait
//				g.drawImage(i_01_poshigh_m, x, y, ui);
//				g.drawImage(i_01_possh_m, x - 3, y + h, ui);
			} else if (w == NPS.width && h == NPS.height) { // small-portrait
//				g.drawImage(i_01_poshigh_s, x, y, ui);
//				g.drawImage(i_01_possh_s, x, y + h, ui);
			} else if (w == FLS.width && h == FLS.height) { // medium-landscape
//				g.drawImage(i_01_poshigh_m_l, x, y, ui);
//				g.drawImage(i_01_possh_m_l, x, y + h, ui);
			} else if (w == NLS.width && h == NLS.height) { // small-landscape
//				g.drawImage(i_01_poshigh_s_l, x, y, ui);
//				g.drawImage(i_01_possh_s_l, x, y + h, ui);
			}
		}
		
		private void paintPoster(Graphics g, int idx, int x, int y, int w,
				int h, boolean focus, boolean showTitle, PosterArray ui) {
			if (ui.elements[idx] == null) {
				g.drawImage(ImageRetriever.getDefault(w, h), x, y, w, h, ui);
				paintHighlight(g, x, y, w, h, ui);
				
				// 4K
				if (ui.row > 1 && y + h > 400) {
					g.setColor(posterDimmedColor);
					g.fillRect(x, y, w, h);
				}
				return;
			}
			if (ui.elements[idx] instanceof CategoryContainer 
					&& ((CategoryContainer) ui.elements[idx]).type == MenuController.MENU_CHANNEL) {
//				g.drawImage(i_01_poshigh_s_l, x, y, ui);
//				g.drawImage(i_01_possh_s_l, x, y + h, 
//						i_01_possh_s_l.getWidth(ui), 12, ui);
				Channel ch = ((CategoryContainer) ui.elements[idx]).getTopic().channel;
				if (ch != null && ch.logos[Channel.LOGO_SM] != null) {
					g.drawImage(ch.logos[Channel.LOGO_SM], 
							x + w/2 - 50/*ch.logos[Channel.LOGO_SM].getWidth(null)/2*/, 
							y + h/2 - 35/*ch.logos[Channel.LOGO_SM].getHeight(null)/2*/, 
							ui);
				}
				Network nw = ((CategoryContainer) ui.elements[idx]).getTopic().network;
				if (nw != null && nw.logos[Network.LOGO_SM] != null) {
					g.drawImage(nw.logos[Network.LOGO_SM], 
							x + w/2 - 50/*nw.logos[Network.LOGO_SM].getWidth(null)/2*/, 
							y + h/2 - 35/*nw.logos[Network.LOGO_SM].getHeight(null)/2*/, 
							ui);
				}
				
				// 4K
				if (ui.row > 1 && y + h > 400) {
					g.setColor(chDimmedColor);
					g.fillRect(x, y, w, h);
				}
				
				return; // support 247
			} else {
				Image poster = null;
				boolean isAdultBlocked = MenuController.getInstance().isAdultBlocked(ui.elements[idx]); 
				if (isAdultBlocked) {
					poster = ui.elements[idx].isPortrait() ? blockedPortrait
							: blockedLandscape;
				} else {
					if (ui.elements[idx] instanceof Extra) {
						MoreDetail titleInExtra = (MoreDetail) CatalogueDatabase.getInstance().getCached(((Extra) ui.elements[idx]).extraDetail.mainContentItem);
						poster = titleInExtra.getImage(w, h);
					} else if (ui.elements[idx].equals(Season.MORE_CONTENT)) {
						if (w == 120 && h == 90) {
							poster = dataCenter.getImageByLanguage("More_Seasons_120x90.jpg");
						} else if (w == 200 && h == 150) {
							poster = dataCenter.getImageByLanguage("More_Seasons_200x150.jpg");
						} else if (w == 240 && h == 180) {
							poster = dataCenter.getImageByLanguage("More_Seasons_240x180.jpg");
						} else { //if (w == 280 && h == 210) {
							poster = dataCenter.getImageByLanguage("More_Seasons_280x210.jpg");
						}
					} else {
						poster = ui.elements[idx].getImage(w, h);
					}
				}
				g.setFont(FontResource.DINMED.getFont(15));
				if (poster != null) {
					g.drawImage(poster, x, y, w, h, ui);
				} else {
					g.drawImage(ImageRetriever.getDefault(w, h), x, y, w, h, ui);
				}
				
				if (isAdultBlocked == false && ui.notAnimation) {
					Image flag = null;
					switch (ui.elements[idx].getFlag()) {
					case 1: // new
						flag = dataCenter.getImageByLanguage("icon_new.png");
						break;
					case 2: // last chance
						flag = dataCenter.getImageByLanguage("icon_last.png");
						break;
					case 3: // extras
						flag = dataCenter.getImageByLanguage("icon_extras.png");
						break;
					}
					
					Image bradingFlag = null;
					if (ui.elements[idx] instanceof AvailableSource) {
						AvailableSource availableSource = (AvailableSource) ui.elements[idx];
						
						if (availableSource.service != null && availableSource.service.description != null) {
							bradingFlag = availableSource.service.description.logos[BrandingElement.LOGO_ICON];
							if (bradingFlag == null) {
								bradingFlag = availableSource.service.description.logos[BrandingElement.LOGO_LINK_SMALL];
							}
							if (bradingFlag == null) {
								bradingFlag = availableSource.service.description.logos[BrandingElement.LOGO_LINK_LARGE];
							}
						} else if (availableSource.channel != null) {
							bradingFlag = availableSource.channel.logos[BrandingElement.LOGO_ICON];
							if (bradingFlag == null) {
								bradingFlag = availableSource.channel.logos[BrandingElement.LOGO_LINK_SMALL];
							}
							if (bradingFlag == null) {
								bradingFlag = availableSource.channel.logos[BrandingElement.LOGO_LINK_LARGE];
							}
						} else if (availableSource.network != null) {
							bradingFlag = availableSource.network.logos[BrandingElement.LOGO_ICON];
							if (bradingFlag == null) {
								bradingFlag = availableSource.network.logos[BrandingElement.LOGO_LINK_SMALL];
							}
							if (bradingFlag == null) {
								bradingFlag = availableSource.network.logos[BrandingElement.LOGO_LINK_LARGE];
							}
						}
					}
					// add hasMoreContent() for VDTRMASTER-5218 [CQ][PO1/2/3][R4][VOD] - Club illico overlay icon should not be displayed
					if (bradingFlag != null && MenuController.getInstance().useBradingDeco()) {
						g.drawImage(bradingFlag, x + w - bradingFlag.getWidth(ui), y, ui);
					} else if (flag != null) {
						// VDTRMASTER-5363	[CQ][R4.1][VOD] 'Last chance' tag is not displayed in Grid view
						// VDTRMASTER-5364	[CQ][R4.1][VOD] 'Movie + Extra' tag is not visible in Grid view
						g.drawImage(flag, x + w - flag.getWidth(ui), y, ui);
					}
					
					// R5 - UHD icon
					Offer hdOffer = ui.elements[idx].getOfferHD(null);
					if (hdOffer != null) {
						boolean uhd = Definition.DEFNITION_UHD.equals(hdOffer.definition);
						if (uhd) {
							// VDTRMASTER-5555
							g.drawImage(iconUhd, x, y + h - iconUhd.getHeight(ui), ui);
						}
					}
				}
				paintHighlight(g, x, y, w, h, ui);
				// 4K
				if (ui.row > 1 && y + h > 400) {
					g.setColor(posterDimmedColor);
					g.fillRect(x, y, w, h);
				}
			}
			
			if (ui.notAnimation == false) {
				return;
			}
			//g.drawImage(i01PosterSha, x, y + h, w, 18, ui);

//			boolean focus = (w == FLS.width && h == FLS.height)
//					|| (w == FPS.width && h == FPS.height)
//					|| (w == FPS_W.width && h == FPS_W.height)
//					|| (w == FLS_W.width && h == FLS_W.height)
//					|| (w == FPS_B.width && h == FPS_B.height);

			if (focus) {
				g.setFont(ui.getFont());
				g.setColor(cTitle);
			} else {
				g.setFont(FontResource.BLENDER.getFont(16));
				g.setColor(Color.white);
			}

			boolean adultBlocked = MenuController.getInstance().isAdultBlocked(
					ui.elements[idx]);
			boolean ageBlocked = adultBlocked
					|| MenuController.getInstance().isBlocked(ui.elements[idx]);
			String title = "";
			if (ui.elements[idx] instanceof Season && ui.episodeMode) {
				Season season = (Season) ui.elements[idx];
				//title = dataCenter.getString(Resources.TEXT_LBL_SEASON) + " " + season.seasonNumber;
				title = season.getTitle();
				
				// VDTRMASTER-5307
				if (Log.DEBUG_ON) {
					Log.printDebug("PosterArrayRenderer, fromCategory=" + ((BaseUI) ui.getParent()).fromCategory() + 
							", season.getLongTitle()=" + season.getLongTitle());
				}
				if (((BaseUI) ui.getParent()).fromCategory() && !season.getLongTitle().equals("N/A")) {
					title = TextUtil.shorten(season.getLongTitle(), g.getFontMetrics(), 750);
				}
			} else if (ui.elements[idx] instanceof Episode && ui.episodeMode) {
				Episode episode = (Episode) ui.elements[idx];
				//title = dataCenter.getString(Resources.TEXT_LBL_EPISODE) + " " + episode.episodeNumber;
				title = episode.getTitle();
			} else if (ui.elements[idx] instanceof Bundle) {
				Bundle bundle = (Bundle) ui.elements[idx];
				// VDTRMASTER-5489
				if (ui.episodeMode == false) {
					title = ui.elements[idx].getTitle();
				} else {
					title = dataCenter.getString(Resources.TEXT_LBL_EPISODES) + " " + bundle.getEpisodeNumberString(true);
				}
			} else {
				title = ui.elements[idx].getTitle();
				
				// VDTRMASTER-5307
				if (ui.elements[idx] instanceof Season && ((BaseUI) ui.getParent()).fromCategory()) {
					title = TextUtil.shorten(((Season) ui.elements[idx]).getLongTitle(), g.getFontMetrics(), 750);
				}
			}
			if (adultBlocked) {
				title = dataCenter.getString(Resources.TEXT_LBL_BLOCKED_TITLE);
			}
			if (focus) {
				int height = 25;
				if (FontResource.BLENDER.getFont(16).equals(g.getFont())) {
					height = 18;
				} else if (FontResource.BLENDER.getFont(18).equals(g.getFont())) {
					height = 20;
				}
				// R5 - remove year
//				if (adultBlocked == false && ui.row == 1
//						&& ui.hybridFocus == false) {
//					String year = ui.elements[idx].getProductionYear();
//					if (year != null && year.length() > 0) {
//						title += " (" + year + ")";
//					}
//				}
				boolean needScroll = false;
				if (ui.getParent() instanceof BundleDetailUI && focus) { // bundle detail
					//title = TextUtil.shorten(title, g.getFontMetrics(), w - 5);
					needScroll = g.getFontMetrics().stringWidth(title) > w;
				} else if (ui.getParent() instanceof WallofPostersFilmInfoUI) { // grid
					//title = TextUtil.shorten(title, g.getFontMetrics(), w);
					needScroll = g.getFontMetrics().stringWidth(title) > w;
				} else if (ui.getParent() instanceof ListOfTitlesUI && w == FPS.width && h == FPS.height) { // normal
					// VDTRMASTER-5415
					needScroll = x + w / 2 - g.getFontMetrics().stringWidth(title) / 2 < 60;
					if (needScroll) {
						x += w/2 - 250/2;
						w = 250;
					}
					
					// VDTRMASTER-5738
					if (!needScroll) {
						// VDTRMASTER-5564
						needScroll = x + w / 2 + g.getFontMetrics().stringWidth(title) / 2 > 620;
						if (needScroll) {
							x += w/2 - 250/2;
							w = 250;
							height = 24;
						}
					}
				} else if (ui.hybridFocus && ui.getParent() instanceof ListOfTitlesUI && title.length() > 40) {
//					if (idx == 0 || (ui.elements.length > 4 && ui.focus == 4)) {
//						needScroll = true;
//						x += w/2 - 300/2;
//						w = 300;
//						height = 24;
//					}
					
					needScroll = true;
					x += w/2 - 300/2;
					w = 300;
					height = 24;
				} else if (!ui.hybridFocus && ui.getParent() instanceof ListOfTitlesUI) {	// VDTRMASTER-5564
					needScroll = x + w / 2 + g.getFontMetrics().stringWidth(title) / 2 > 620;
					if (needScroll) {
						x += w/2 - 250/2;
						w = 250;
						height = 24;
					}
				}
				
				int icon = x;
				if (needScroll) {
					BaseUI base = ((BaseUI) ui.getParent());
					int cnt = Math.max(base.scroll - 10, 0);
					Rectangle ori = g.getClipBounds();
					base.scrollBounds.x = x + ui.getX();
					base.scrollBounds.y = y + h + ui.getY();
					base.scrollBounds.width = w;
					base.scrollBounds.height = (w == 250 || w == 300 ? height + 4 : 20);
					g.clipRect(x, y + h, w, base.scrollBounds.height);
					try {
						g.drawString(title.substring(cnt), x + 2, y + h + height/*(w == 250 ? height : 16)*/);
					} catch (IndexOutOfBoundsException e) {
						base.scroll = 0;
					} 
					g.setClip(ori);
				} else {
					((BaseUI) ui.getParent()).scrollBounds.width = 0;
					icon = GraphicUtil.drawStringCenter(g, title, x + w / 2, y + h + height/*(w == 250 ? height : 16)*/);
				}
				int offset = (w == ImageRetriever.PR_SM.width && h == ImageRetriever.PR_SM.height) 
						|| (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) ? 16 : 20;
				if (ageBlocked && !MenuController.getInstance().needToDisplayFavorite()) {
//					if (ui.getParent() instanceof WallofPostersFilmInfoUI) { // small
//						g.drawImage(i02_icon_con, icon - 18, y + h + height
//								- 14, 17, 26, ui);
//					} else {
//						int offset = ui.getParent() instanceof WallofPostersFilmInfoUI ? 16 : 20;
						
						g.drawImage(i02_icon_con,
								icon - i02_icon_con.getWidth(ui) - 3, y + h
										+ height - offset, ui);
//					}
				} else if (BookmarkManager.getInstance().isInWishlist(ui.elements[idx]) != null) {
						g.drawImage(i02_icon_fav,
								icon - i02_icon_fav.getWidth(ui) - 3, y + h
										+ height - offset + 2, ui);
				}
			} else if (showTitle) { // not focused
				int offset = (w == ImageRetriever.PR_SM.width && h == ImageRetriever.PR_SM.height) 
						|| (w == ImageRetriever.LD_SM.width && h == ImageRetriever.LD_SM.height) ? 16 : 20;
				if (adultBlocked && !MenuController.getInstance().needToDisplayFavorite()) {
					g.drawImage(i02_icon_con, x + 2, y + h + 15 - offset, ui);
					g.drawString(TextUtil.shorten(dataCenter
							.getString(Resources.TEXT_LBL_BLOCKED_TITLE), g
							.getFontMetrics(), w - 25), x + 2 + 25, y + h + 15);
				} else {
					if (BookmarkManager.getInstance().isInWishlist(ui.elements[idx]) != null) {
						g.drawImage(i02_icon_fav, x + 2, y + h + 15 - offset, ui);
						g.drawString(TextUtil.shorten(title, g.getFontMetrics(), w - 25), x + 2 + 25, y + h + 15);
					} else {
						g.drawString(TextUtil.shorten(title, g.getFontMetrics(), w), x + 2, y + h + 15);
					}
//					String[] split = TextUtil.split(title, g.getFontMetrics(),
//							w - 4, 2);
//					String[] split = new String[] {
//							TextUtil.shorten(title, g.getFontMetrics(), w)
//					};
//					for (int i = 0; i < split.length; i++) {
//						g.drawString(split[i], x + 2, y + h + 13 + i * 12);
//					}
				}

			}

			// g.setFont(FontResource.BLENDER.getFont(18));
			// g.setColor(Color.red);
			// g.drawString("i="+idx, x+5, y + 25);
			// g.drawString("o="+ui.offset, x+5, y + 45);
			// g.drawString("f="+ui.focus, x+5, y + 65);
		}
	}
}

package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.VideoOutputPort;
import org.ocap.hardware.device.VideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputPortListener;
import org.ocap.hardware.device.VideoOutputSettings;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.VODServiceImpl;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.CategorizedBundle;
import com.videotron.tvi.illico.vod.data.vcds.type.Definition;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.data.vcds.type.Offer;
import com.videotron.tvi.illico.vod.gui.BaseRenderer;
import com.videotron.tvi.illico.vod.ui.BaseUI;

// Show options(sd/hd and fr/en) about purchase and select one
public class PopSelectOption extends UIComponent implements VideoOutputPortListener {
	private static final long serialVersionUID = 992049683746474284L;
	private BaseUI parent;
	private Offer[] offer = new Offer[4];
	private String title;
	private String titleId;
	private CategorizedBundle karaoke;
	private Offer karaokeOffer;
	private ClickingEffect clickEffect;
	private int btnX, btnY, btnW, btnH;

	// 4K
	private Offer[] uhdOffer = new Offer[2];
	private boolean isUhd;
	private VideoOutputSettings hdmiPort;
	
	public static final int BTN_CANCEL = 5;

	private static PopSelectOption instance;
	
	private int step = 0;
	private static final int STEP_FORMAT = 0;
	private static final int STEP_LANG = 1;
	private static final int STEP_CONFIRM = 2;
	
	private boolean is3D;
	private boolean isFree;
	private boolean hasMultiFormat;
	private boolean hasMultiLang;
	
	private String prefLang;
	private String prefDef;
	
	private String selectedFormat;
	private String selectedLang;
	private Offer selectedOffer;

	synchronized public static PopSelectOption getInstance() {
		if (instance == null) {
			instance = new PopSelectOption();
		}
		return instance;
	}

	private PopSelectOption() {
		setRenderer(new PopSelectOptionRenderer());
		prepare();
		setVisible(false);
		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI comp, Object data) {
		parent = comp;
		MoreDetail element = (MoreDetail) data; // Bundle or Video

		// 4K
		isUhd = false;
		// VDTRMASTER-5416
		if (element.getOfferHD(null) != null && Definition.DEFNITION_UHD.equals(element.getOfferHD(null).definition)) {
			isUhd = true;

			uhdOffer[Resources.HD_FR] = element.getOfferHD(Definition.LANGUAGE_FR);
			uhdOffer[Resources.HD_EN] = element.getOfferHD(Definition.LANGUAGE_EN);

			// 4K - DDC-107
			// UHD content will be handled before this popup.
			//isConnectedUHDTV = checkUHD();			
		} else {
			offer[Resources.HD_FR] = element.getOfferHD(Definition.LANGUAGE_FR);
			offer[Resources.HD_EN] = element.getOfferHD(Definition.LANGUAGE_EN);
			offer[Resources.SD_FR] = element.getOfferSD(Definition.LANGUAGE_FR);
			offer[Resources.SD_EN] = element.getOfferSD(Definition.LANGUAGE_EN);
		}

		title = element.getTitle();
		titleId = element.getId();
		
		prefLang = DataCenter.getInstance().getString(VODServiceImpl.AGENT_PREF_LANG);
		prefDef = DataCenter.getInstance().getString(VODServiceImpl.AGENT_PREF_DEF);
		if (prefLang == null || prefLang.length() == 0) {
			prefLang = DataCenter.getInstance().getString(PreferenceNames.PREFERENCES_ON_LANGUAGE);
		} else {
			if ("EN".equalsIgnoreCase(prefLang)) {
				prefLang = Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH;
			} else {
				prefLang = Definitions.PREFERENCES_ON_LANGUAGE_FRENCH;
			}
			DataCenter.getInstance().remove(VODServiceImpl.AGENT_PREF_LANG);
		}
		if (prefDef == null || prefDef.length() == 0) {
			prefDef = DataCenter.getInstance().getString(PreferenceNames.PREFERENCES_ON_FORMAT);
		} else {
			if ("HD".equalsIgnoreCase(prefDef)) {
				prefDef = Definitions.PREFERENCES_ON_FORMAT_HD;
			} else {
				prefDef = Definitions.PREFERENCES_ON_FORMAT_STANDARD;
			}
			DataCenter.getInstance().remove(VODServiceImpl.AGENT_PREF_DEF);
		}

		if (isUhd) {
			int[] priority = new int[2];

			if (prefLang.equalsIgnoreCase(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				priority[0] = Resources.HD_EN;
				priority[1] = Resources.HD_FR;
			} else { // prefer french
				priority[0] = Resources.HD_FR;
				priority[1] = Resources.HD_EN;
			}

			if (data instanceof CategorizedBundle) {
				karaoke = (CategorizedBundle) data;
				
				for (int i = 0; i < priority.length; i++) {
					if (uhdOffer[priority[i]] != null) {
						karaokeOffer = offer[priority[i]];
						break;
					}
				}
			} else {
				karaoke = null;
				for (int i = 0; i < priority.length; i++) {
					if (uhdOffer[priority[i]] != null) {
						break;
					}
				}
			}
		} else {
			int[] priority = new int[4];
			if (prefLang.equalsIgnoreCase(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				if (prefDef.equalsIgnoreCase(Definitions.PREFERENCES_ON_FORMAT_HD)) {
					priority[0] = Resources.HD_EN;
					priority[1] = Resources.SD_EN;
					priority[2] = Resources.HD_FR;
					priority[3] = Resources.SD_FR;
				} else { // prefer sd
					priority[0] = Resources.SD_EN;
					priority[1] = Resources.HD_EN;
					priority[2] = Resources.SD_FR;
					priority[3] = Resources.HD_FR;
				}
			} else { // prefer french
				if (prefDef.equalsIgnoreCase(Definitions.PREFERENCES_ON_FORMAT_HD)) {
					priority[0] = Resources.HD_FR;
					priority[1] = Resources.SD_FR;
					priority[2] = Resources.HD_EN;
					priority[3] = Resources.SD_EN;
				} else { // prefer sd
					priority[0] = Resources.SD_FR;
					priority[1] = Resources.HD_FR;
					priority[2] = Resources.SD_EN;
					priority[3] = Resources.HD_EN;
				}
			}

			if (data instanceof CategorizedBundle) {
				karaoke = (CategorizedBundle) data;
				
				for (int i = 0; i < priority.length; i++) {
					if (offer[priority[i]] != null) {
						karaokeOffer = offer[priority[i]];
						break;
					}
				}
			} else {
				karaoke = null;
				for (int i = 0; i < priority.length; i++) {
					if (offer[priority[i]] != null) {
						break;
					}
				}
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("show" + ", focus = " + focus + ", prefLang = " + prefLang + ", prefDef = " + prefDef
					+ ", offer[Resources.HD_FR] = " + offer[Resources.HD_FR] + ", offer[Resources.HD_EN] = "
					+ offer[Resources.HD_EN] + ", offer[Resources.SD_FR] = " + offer[Resources.SD_FR]
					+ ", offer[Resources.SD_EN] = " + offer[Resources.SD_EN]);
			Log.printDebug("show, isUhd=" + isUhd + ", uhdOffer[Resources.HD_FR] = " + uhdOffer[Resources.HD_FR]
					+ ", uhdOffer[Resources.HD_EN] = " + uhdOffer[Resources.HD_EN]);
		}
				
		//xFocus = focus;
		selectedOffer = null;		
		selectedFormat = null;
		selectedLang = null;
		checkFormat();
		
		prepare();
		setVisible(true);
	}
	
	private void checkFormat() {
		is3D = false;
		if (title != null) {
			if (title.indexOf("- 3D") > -1) {
				is3D = true;
				selectedFormat = Definitions.PREFERENCES_ON_FORMAT_HD;
			}
		}
		if (!isUhd) {
			//check whether it's multiple format.
			if ( (offer[Resources.HD_FR] != null || offer[Resources.HD_EN] != null) &&
					(offer[Resources.SD_FR] != null || offer[Resources.SD_EN] != null)) {
				hasMultiFormat = true;
			} else {
				hasMultiFormat = false;
				
				if (offer[Resources.HD_FR] != null || offer[Resources.HD_EN] != null) {
					selectedFormat = Definitions.PREFERENCES_ON_FORMAT_HD;
					checkLanguage(Definitions.PREFERENCES_ON_FORMAT_HD);
				}else {
					selectedFormat = "SD";
					checkLanguage(null);
				}
				
			}
		} else {
			selectedFormat = Definitions.PREFERENCES_ON_FORMAT_HD;
			hasMultiFormat = false;
			checkLanguage(null);
		}
		
		if (hasMultiFormat == true) {
			focus = 0;
			if (prefDef != null && 
					prefDef.equals(Definitions.PREFERENCES_ON_FORMAT_STANDARD)) {				
				focus = 1;
			}
				
			step = STEP_FORMAT;		
		}
	}
	
	private void checkLanguage(String format) {
		if (!isUhd) {
			if (format != null && format.equalsIgnoreCase(Definitions.PREFERENCES_ON_FORMAT_HD)) {
				if (offer[Resources.HD_FR] != null && offer[Resources.HD_EN] != null)
					hasMultiLang = true;
				else {
					hasMultiLang = false;
					if (offer[Resources.HD_FR] != null)
						selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_FRENCH;
					else
						selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH;
				}
			} else { //sd
				if (offer[Resources.SD_FR] != null && offer[Resources.SD_EN] != null)
					hasMultiLang = true;
				else {
					hasMultiLang = false;
					if (offer[Resources.SD_FR] != null)
						selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_FRENCH;
					else
						selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH;
				}
			}			
		} else {
			if (uhdOffer[Resources.HD_FR] != null && uhdOffer[Resources.HD_EN] != null)
				hasMultiLang = true;
			else {
				hasMultiLang = false;
				if (uhdOffer[Resources.HD_FR] != null)
					selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_FRENCH;
				else
					selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH;
			}
		}
		
		if (hasMultiLang == true) {
			focus = 0;
			step = STEP_LANG;
		} else {			
			setSelectedOffer();
			step = STEP_CONFIRM;
		}
	}
	
	private void setSelectedOffer() {
		Log.printDebug("setSelectedOffer, selectedFormat " + selectedFormat + ", selectedLang " + selectedLang);		
		focus = 0;
		
		if (isUhd) {
			if (selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				selectedOffer = uhdOffer[Resources.HD_EN];											
			} else {
				selectedOffer = uhdOffer[Resources.HD_FR];
			}
			
		} else {	
			if (selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH)) {
				selectedOffer = offer[Resources.HD_FR];			
			} else if (selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				selectedOffer = offer[Resources.HD_EN];			
			} else if (!selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				selectedOffer = offer[Resources.SD_EN];		
			} else if (!selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH)) {
				selectedOffer = offer[Resources.SD_FR];			
			}
		}
		
		
		if (hasMultiFormat == false && 
				hasMultiLang == false &&
				selectedOffer != null) {
			try {
				double p = Double.parseDouble(selectedOffer.amount);
				if (p == 0) {
					submitOffer();
					return;
				}
			} catch (NumberFormatException nfe) {}
		}
		
		//if (selectedOffer == null)
		//	return;
				
	}
	
	private void submitOffer() {
		Log.printDebug("submitOffer");
		
		if (isUhd) {
			if (selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH)) {
				focus = 0;
			} else if (selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				focus = 1;
			}
		} else {		
			if (selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH)) {
				focus = 0;
			} else if (selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				focus = 1;
			} else if (!selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH)) {
				focus = 2;
			} else if (!selectedFormat.equals(Definitions.PREFERENCES_ON_FORMAT_HD) && selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)) {
				focus = 3;
			}
		}
		Log.printDebug("submitOffer  focus " + focus);
		parent.popUpClosed(this, new Object[] { new Integer(focus), titleId, selectedOffer });
	}

	public void keyOK() {
		if (Log.DEBUG_ON) {
			Log.printDebug("keyOK, focus = " + focus);
		}
				
		if (focus == BTN_CANCEL) {
			Log.printDebug("keyOK, BTN_CANCEL");
			parent.popUpClosed(this, new Object[] { new Integer(BTN_CANCEL), titleId });
			return;
		}
		
		if (karaoke != null) {
			Log.printDebug("keyOK, karaoke");
			parent.popUpClosed(this, karaoke);
			return;
		}

		if (step == STEP_FORMAT) {
			Log.printDebug("keyOK, STEP_FORMAT");
			if (focus == 0)
				selectedFormat = Definitions.PREFERENCES_ON_FORMAT_HD;
			else 
				selectedFormat = "SD";
			
			checkLanguage(selectedFormat);
			focus = 0; 
			repaint();
			
		} else if (step == STEP_LANG) {
			Log.printDebug("keyOK, STEP_LANG");
			
			if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH) && focus == 0)
				selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH;
			else if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH) && focus == 1)
				selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_FRENCH;
			else if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH) && focus == 0)
				selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_FRENCH;
			else if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH) && focus == 1)
				selectedLang = Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH;
							
			focus = 0;
			step = STEP_CONFIRM;
			setSelectedOffer();
			repaint();
			
		} else if (step == STEP_CONFIRM) {
			Log.printDebug("keyOK, STEP_CONFIRM focus " + focus);
			
			if (focus == 0) {
				submitOffer();
			} else if (focus == 1) {
				if (hasMultiFormat) {
					step = STEP_FORMAT;
					focus = 0;
					if (prefDef != null && 
							prefDef.equals(Definitions.PREFERENCES_ON_FORMAT_STANDARD)) {				
						focus = 1;
					}
					repaint();
				} else if (hasMultiLang) {
					step = STEP_LANG;
					focus = 0;
					repaint();
				} else {
					Log.printDebug("keyOK, BTN_CANCEL");
					parent.popUpClosed(this, new Object[] { new Integer(BTN_CANCEL), titleId });
				}
			}
			
		}		
	}

	public void stop() {
		parent.remove(this);
		parent.setPopUp(null);
		parent.repaint();

		if (hdmiPort != null) {
			hdmiPort.removeListener(this);
		}
	}

	public void keyUp() {
		if (karaoke != null) {
			switch (focus) {
			case BTN_CANCEL:
				focus = 0;
			break;
			}
			repaint();
			return;
		}
		if (step < STEP_CONFIRM) {
			switch (focus) {
			case 0:
				break;
			case 1:
				focus = 0;
				break;
			case BTN_CANCEL:			
				focus = 1;
				break;
			}
				
		}
		
		repaint();
	}

	public void keyDown() {
		if (karaoke != null) {
			switch (focus) {
			case 0:
				focus = BTN_CANCEL;
			break;
			}
			repaint();
			return;
		}
		
		if (step < STEP_CONFIRM) {
			switch (focus) {
			case 0:
				focus = 1;
				break;
			case 1:
				focus = BTN_CANCEL;
				break;			
			}
				
		}
		repaint();
	}

	public void KeyLeft() {
		if (step == STEP_CONFIRM) {
			switch (focus) {
			case 0:
				focus = 1;
				break;
			case 1:
				focus = 0;
				break;			
			}
				
		}
		repaint();
	}

	public void keyRight() {
		if (step == STEP_CONFIRM) {
			switch (focus) {
			case 0:
				focus = 1;
				break;
			case 1:
				focus = 0;
				break;			
			}		
		}
		repaint();
	}

	public void keyBack() {
		parent.popUpClosed(this, new Object[] { new Integer(BTN_CANCEL) });
	}

	public boolean handleKey(int key) {
		switch (key) {
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_LEFT:
			KeyLeft();
			break;
		case HRcEvent.VK_RIGHT:
			keyRight();
			break;
		case HRcEvent.VK_ENTER:
			clickEffect.start(btnX, btnY, btnW, btnH);
			keyOK();
			break;
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
		case OCRcEvent.VK_EXIT:
			keyBack();
			break;
		}
		return true;
	}

	private class PopSelectOptionRenderer extends BaseRenderer {

		private Image i_05_sep;// = dataCenter.getImage("05_sep.png");
		private Image i_btn_154;// = dataCenter.getImage("btn_154.png");
		private Image i_btn_154_foc;// = dataCenter.getImage("btn_154_foc.png");
		private Image i_btn_164;
		private Image i_btn_164_foc;
		private Image i_btn_202;// = dataCenter.getImage("btn_202.png");
		private Image i_btn_202_foc;// = dataCenter.getImage("btn_202_foc.png");
		private Image i_icon_bundle;// = dataCenter.getImage("icon_bundle.png");
		private Image i_pop_list292x220;// =
										// dataCenter.getImage("pop_list272x220.png");
		private Image i05Focus;// = dataCenter.getImage("05_focus.png");
		private Image i05FocusDim;// = dataCenter.getImage("05_focus_dim.png");
		private Image iconHd;// = dataCenter.getImage("icon_hd_long.png");
		private Image iconHdF;// = dataCenter.getImage("icon_hd_f_long.png");
		private Image iconFHd;// = dataCenter.getImage("icon_hd_long.png");
		private Image iconFHdF;// = dataCenter.getImage("icon_hd_f_long.png");
		private Image iconSd;// = dataCenter.getImage("icon_sd.png");
		private Image iconSdF;// = dataCenter.getImage("icon_sd_f.png");
		private Image errorTextBg;
		private String[] titleStr;
		private Color cDimmedBG = new DVBColor(12, 12, 12, 230);
		private Color c3 = new Color(3, 3, 3);
		private Color c28 = new Color(28, 28, 28);
		private Color c35 = new Color(35, 35, 35);
		private Color c61 = new Color(61, 61, 61);
		private Color c182 = new Color(182, 182, 182);
		private Color c229 = new Color(229, 229, 229);
		private Color c240 = new Color(240, 240, 240);
		private Color cTitle = new Color(252, 202, 4);
		
		
		private Color cBtnFocus = new Color(249, 194, 0);
		private Color cBtnUnFocus = new Color(150, 150, 150);
		
		private Color cBtnDimed = new Color(92, 92, 92);

		// 4K
		private Color c44 = new Color(44, 44, 44);
		private Color warningColor = new Color(255, 100, 100);
		
		private Color cImportant = new Color(236, 211, 143);

		// 4K
		private Image iconUhd;
		private Image iconUhdF;

		public void prepare(UIComponent c) {
			titleStr = null;

			i_btn_154 = dataCenter.getImage("btn_154.png");
			i_btn_154_foc = dataCenter.getImage("btn_154_foc.png");
			i_btn_164 = dataCenter.getImage("btn_164.png");
			i_btn_164_foc = dataCenter.getImage("btn_164_foc.png");
			i_btn_202 = dataCenter.getImage("btn_202.png");
			i_btn_202_foc = dataCenter.getImage("btn_202_foc.png");
			i_icon_bundle = dataCenter.getImage("icon_bundle.png");
			i_pop_list292x220 = dataCenter.getImage("pop_list292x220.png");
			i05Focus = dataCenter.getImage("05_focus.png");
			i05FocusDim = dataCenter.getImage("05_focus_dim.png");
			iconHd = dataCenter.getImage("icon_hd_long.png");
			iconHdF = dataCenter.getImage("icon_hd_f_long.png");
			iconFHd = dataCenter.getImage("icon_fhd_long.png");
			iconFHdF = dataCenter.getImage("icon_fhd_f_long.png");
			iconSd = dataCenter.getImage("icon_sd.png");
			iconSdF = dataCenter.getImage("icon_sd_f.png");

			// 4K
			iconUhd = dataCenter.getImage("icon_uhd_long.png");
			iconUhdF = dataCenter.getImage("icon_uhd_f_long.png");
			
			//3D
			errorTextBg = dataCenter.getImage("error_txt_bg_h60.png");
			
			super.prepare(c);
		}

		public void paint(Graphics g, UIComponent c) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			int gapY = 0;
			String str;
			
			
			if (step != STEP_CONFIRM) {
				
				//background 
				g.setColor(c35);
				g.fillRect(305, 157, 350, 235);
				g.setColor(c61);
				g.fillRect(318, 195, 324, 1);				
									
				g.setFont(FontResource.BLENDER.getFont(18));
				
				
				g.setColor(focus == BTN_CANCEL ? cBtnFocus : cBtnUnFocus);
				g.fillRect(403, 343, 154, 32);
				if (focus == BTN_CANCEL) {
					btnX = 403;
					btnY = 343;
					btnW = 154;
					btnH = 32;
				}
				g.setColor(c3);
				GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_CANCEL), 480, 364);

				gapY = 27;				
	
				// popup title
				g.setFont(FontResource.BLENDER.getFont(24));
				g.setColor(cTitle);
				
				if (karaoke != null) {
					GraphicUtil.drawStringCenter(g, title, 480, 183);
					
					g.setFont(FontResource.BLENDER.getFont(18));
					g.setColor(c229);				
					GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_MSG_KARAOKE_SUBS_WANT) + " "
							+ title + "?", 480, 232);
					GraphicUtil.drawStringCenter(g, "(" + getPrice(karaokeOffer.amount)
							+ (Resources.currentLanguage == 0 ? " for " : " pour ")
							+ Formatter.getCurrent().getDurationText2(karaokeOffer.leaseDuration) + ")", 480, 252);
					int x = 404;
					int y = 258 + gapY;
					if (focus != BTN_CANCEL) { // 486, 240
						btnX = x -1;
						btnY = y;
						g.setColor(cBtnFocus);
						g.fillRect(btnX, btnY, 154, 32);
						//g.drawImage(i_btn_154_foc, btnX = x - 1, btnY = y, c);
						g.setColor(c3);
					} else {
						g.setColor(cBtnUnFocus);
						g.fillRect(x, y, 154, 32);
						//g.drawImage(i_btn_154, x, y, c);
						g.setColor(c240);
					}
					g.setFont(FontResource.BLENDER.getFont(18));
					GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_ORDER), 480, y + 21);
					return;
				}
				
				GraphicUtil.drawStringCenter(g, TextUtil.shorten(title, g.getFontMetrics(), 310), 480, 183);
				
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(c229);
				if (step == STEP_FORMAT)
					GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_MSG_SELECT_FORMAT), 480, 225);
				else
					GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_MSG_SELECT_LANGUAGE), 480, 225);				
			}
			
			//draw format
			if (step == STEP_FORMAT) {
								
				drawButtonFormat(g, 0, true, 326, 247, c);
				drawButtonFormat(g, 1, false, 326, 283, c);
				
			} else if (step == STEP_LANG) {
								
				drawButtonLang(g, 0, 326, 247, c);
				drawButtonLang(g, 1, 326, 283, c);								
				
			} else { //step == STEP_CONFIRM		
			
				g.setColor(c35);
				g.fillRect(305, is3D?137:157, 350, is3D?275:225);
				g.setColor(c61);
				g.fillRect(318, is3D?175:195, 324, 1);
				g.setFont(FontResource.BLENDER.getFont(24));
				g.setColor(cTitle);
				GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_T_CONFIRM), 480,is3D?163:183);
				
				if (is3D)
					g.drawImage(errorTextBg, 322, 286, c);
				
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(c229);
				GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_MSG_ORDER_CONFIRM), 480, is3D?205:236);
				GraphicUtil.drawStringCenter(g, TextUtil.shorten(title, g.getFontMetrics(), 340), 480, is3D?205+22:236+22);
				
				
				String selected = "";
				
				if (Resources.currentLanguage == 0) {
					selected += selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)?
							dataCenter.getString(Resources.TEXT_LANGUAGE_EN_CONFIRM):dataCenter.getString(Resources.TEXT_LANGUAGE_FR_CONFIRM);
					selected += " "+ dataCenter.getString(Resources.TEXT_MSG_VERSION);
					selected += " in ";																											
				} else {
					selected += dataCenter.getString(Resources.TEXT_MSG_VERSION) + " " ;
					
					selected += selectedLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH)?
							dataCenter.getString(Resources.TEXT_LANGUAGE_EN_CONFIRM):dataCenter.getString(Resources.TEXT_LANGUAGE_FR_CONFIRM);					
					selected += " en ";																	
				}
				if (isUhd) {
					selected += dataCenter.getString(Resources.TEXT_FORMAT_UHD);
				} else {
					if (Definition.DEFNITION_FHD.equals(selectedOffer.definition))
						selected += dataCenter.getString(Resources.TEXT_FORMAT_FHD);
					else if (Definition.DEFNITION_HD.equals(selectedOffer.definition))
						selected += dataCenter.getString(Resources.TEXT_FORMAT_HD);
					else
						selected += dataCenter.getString(Resources.TEXT_FORMAT_SD);
				}
				GraphicUtil.drawStringCenter(g, selected, 480, is3D?205+44:236+44);
				
				
				
				selected = dataCenter.getString(Resources.TEXT_MSG_FOR) + " ";
				selected += getPrice(selectedOffer.amount);
				GraphicUtil.drawStringCenter(g, selected, 480, is3D?205+66:236+66);
								
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(focus == 0 ? cBtnFocus : cBtnUnFocus);
				g.fillRect(322, is3D?363:333, 154, 32);				
				if (focus == 0) {
					btnX = 322;
					btnY = is3D?363:333;
					btnW = 154;
					btnH = 32;
				}
				g.setColor(c3);
				GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_OK), 400, is3D?384:354);

				g.setColor(focus == 1 ? cBtnFocus : cBtnUnFocus);
				g.fillRect(484, is3D?363:333, 154, 32);
				if (focus == 1) {
					btnX = 484;
					btnY = is3D?363:333;
					btnW = 154;
					btnH = 32;
				}
				g.setColor(c3);
				if (hasMultiFormat || hasMultiLang)
					GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_MODIFY), 562, is3D?384:354);
				else
					GraphicUtil.drawStringCenter(g, dataCenter.getString(Resources.TEXT_CANCEL), 562, is3D?384:354);
				
				if (is3D) {
					g.setFont(FontResource.BLENDER.getFont(18));
					g.setColor(cImportant);
					g.drawString(dataCenter.getString(Resources.TEXT_MSG_IMPORTANT), 343, 312);
					g.setColor(c182);
					g.drawString(dataCenter.getString(Resources.TEXT_MSG_IMPORTANT_3D1), 417, 312);
					g.drawString(dataCenter.getString(Resources.TEXT_MSG_IMPORTANT_3D2), 355, 312+18);
				}
			}
					
		}
		
		private void drawButtonFormat(Graphics g, int idx, boolean hd, int x, int y, UIComponent c) {
			
			//uhd and 3d contents don't allow this button.
			if (isUhd || is3D) {				
				return;
			}
			
			Offer idxOffer = null;			
			
			if (hd) {
				if (offer[Resources.HD_EN] != null)
					idxOffer = offer[Resources.HD_EN];
				else if (offer[Resources.HD_FR] != null)
					idxOffer = offer[Resources.HD_FR];
			} else {
				if (offer[Resources.SD_EN] != null)
					idxOffer = offer[Resources.SD_EN];
				else if (offer[Resources.SD_FR] != null)
					idxOffer = offer[Resources.SD_FR];
			}

			if (idxOffer == null) {
				Log.printDebug("drawButtonFormat idxOffer is null !!!");
				return;
			}
			
			if (focus == idx) { // focused
				g.setColor(cBtnFocus);
				g.fillRect(btnX = x, btnY = y, 311, 32);
				btnW = 311;
				btnH = 32;				
				g.setColor(c3);
			} else {
				g.setColor(cBtnUnFocus);
				g.fillRect(x, y, 311, 32);				
				g.setColor(c240);
			}
			
			String format = "";
			if (isUhd) {
				//format = "";
				//g.drawImage(iconUhdF, x + 7, y + 9, c);
			} else {
				if (hd) {
					if (Definition.DEFNITION_FHD.equals(idxOffer.definition))
						format = dataCenter.getString(Resources.TEXT_FORMAT_FHD);
					else
						format = dataCenter.getString(Resources.TEXT_FORMAT_HD);
				} else
					format = dataCenter.getString(Resources.TEXT_FORMAT_SD);
				
				format += " " + dataCenter.getString(Resources.TEXT_MSG_FOR);
				format += " " + getPrice(idxOffer.amount);					
			}	
			format = format.substring(0, 1).toUpperCase() + format.substring(1);
			g.setFont(FontResource.BLENDER.getFont(18));
			GraphicUtil.drawStringCenter(g, format, 480, y + 21);			
		}
		
		private void drawButtonLang(Graphics g, int idx, int x, int y, UIComponent c) {
			
			String lang = "";			
						
			if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH) && idx == 0) {
				lang = dataCenter.getString(Resources.TEXT_LANGUAGE_EN);
			} else if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_ENGLISH) && idx == 1) {
				lang = dataCenter.getString(Resources.TEXT_LANGUAGE_FR);
			} else if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH) && idx == 0) {
				lang = dataCenter.getString(Resources.TEXT_LANGUAGE_FR);
			} else if (prefLang.equals(Definitions.PREFERENCES_ON_LANGUAGE_FRENCH) && idx == 1) {
				lang = dataCenter.getString(Resources.TEXT_LANGUAGE_EN);
			}
			
			if (focus == idx) { // focused
				g.setColor(cBtnFocus);
				g.fillRect(btnX = x, btnY = y, 311, 32);
				btnW = 311;
				btnH = 32;				
				g.setColor(c3);
			} else {
				g.setColor(cBtnUnFocus);
				g.fillRect(x, y, 311, 32);				
				g.setColor(c240);
			}
			
			g.setFont(FontResource.BLENDER.getFont(18));
			GraphicUtil.drawStringCenter(g, lang, x+150, y + 21);			
		}

	}

	public void configurationChanged(VideoOutputPort source, VideoOutputConfiguration oldConfig,
			VideoOutputConfiguration newConfig) {

	}

	public void connectionStatusChanged(VideoOutputPort source, boolean status) {

	}

	public void enabledStatusChanged(VideoOutputPort source, boolean status) {

	}
}

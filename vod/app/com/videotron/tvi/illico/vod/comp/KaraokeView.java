package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;

import org.dvb.ui.DVBBufferedImage;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.vod.controller.communication.VbmController;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.CategoryContainer;
import com.videotron.tvi.illico.vod.data.vcds.type.MoreDetail;
import com.videotron.tvi.illico.vod.ui.BaseUI;
import com.videotron.tvi.illico.vod.ui.CategoryUI;
import com.videotron.tvi.illico.vod.ui.KaraokeListViewUI;
import com.videotron.tvi.illico.vod.ui.UITemplateList;

public class KaraokeView extends UIComponent {
	private static DataCenter dataCenter = DataCenter.getInstance();
	private static final int FOCUS_SEE_ALL = -1;
	private static final int FOCUS_ORDER = -2;

	CategoryContainer category;
	MoreDetail[] data = new MoreDetail[0];
	int row, offset;
	String title;
	String subTitle;
	boolean seeAll;
	boolean order;
	boolean picked;
	boolean needDim;
	boolean animation;
	boolean drawUpperShadow;
	boolean drawLowerShadow;

	ChangeOrderEffect changeOrder;
	RemoveEffect removeEffect;

	Image i_01_kara_pack_list;
	String bannerUrl;
	
	public KaraokeView() {
		setRenderer(new KaraokeViewRenderer());
		prepare();
	}

	public boolean handleKey(int key) {
		Log.printDebug("KaraokeView, handleKey(), code = " + key);
		if (getParent() instanceof KaraokeListViewUI && state != BaseUI.STATE_ACTIVATED) {
            return false;
        }
		boolean ret = true;
		switch (key) {
		case HRcEvent.VK_UP:
			keyUp();
			break;
		case HRcEvent.VK_DOWN:
			keyDown();
			break;
		case HRcEvent.VK_LEFT:
			if (getParent() instanceof CategoryUI) {
				((CategoryUI) getParent()).setCurrentFocusUI(CategoryUI.FOCUS_MENU);
				// 20190222 Touche project
				VbmController.getInstance().backwardHistory();				
			}
			break;
		case HRcEvent.VK_RIGHT:
			break;
		case HRcEvent.VK_ENTER:
		case HRcEvent.VK_INFO:
			keyOK();
			break;
		// case KeyCodes.LAST:
		// keyBack();
		// break;
		default:
			ret = false;
			break;
		}
		return ret;
	}

	public boolean keyUp() {
		if (getParent() instanceof CategoryUI) {
			switch (focus) {
			case FOCUS_ORDER:
				if (seeAll) {
					focus = FOCUS_SEE_ALL;
					break;
				}
				// else, don't stop, go through
			case FOCUS_SEE_ALL:
				focus = Math.min(row, data.length) - 1;
				break;
			default:
				focus = Math.max(focus - 1, 0);
				break;
			}
		} else { // list view
			int oldIdx = offset + focus;
			int newFocus = focus;
			int newOffset = offset;
			boolean focusChanged = true;
			if (data.length <= row || offset == 0 || focus != row / 2) {
				newFocus = Math.max(0, focus - 1);
			} else {
				newOffset--;
				focusChanged = false;
			}
			int newIdx = newOffset + newFocus;
			if (((KaraokeListViewUI) getParent()).isChangeOrder() && picked && oldIdx != newIdx) {
				if (changeOrder == null) {
					changeOrder = new ChangeOrderEffect(this, 6);
				}
				if (focusChanged) {
					changeOrder.setBounds(new Rectangle(0, 38 + (focus) * 32, getWidth(), 36), new Rectangle(0, 38 + 4 + (focus-1) * 32, getWidth(), 32 - 4));
				} else {
					changeOrder.setBounds(new Rectangle(0, 38 + (focus) * 32, getWidth(), 36), true);
				}
				animation = true;
				changeOrder.start();
				animation = false;
				MoreDetail tmp = data[oldIdx];
				data[oldIdx] = data[newIdx];
				data[newIdx] = tmp;
			}
			offset = newOffset;
			focus = newFocus;
		}

		repaint();
		return false;
	}

	public boolean keyDown() {
		if (getParent() instanceof CategoryUI) {
			switch (focus) {
			case FOCUS_SEE_ALL:
				if (order) {
					focus = FOCUS_ORDER;
				}
				break;
			default:
				if (focus == Math.min(row, data.length) - 1) {
					if (seeAll) {
						focus = FOCUS_SEE_ALL;
					} else if (order) {
						focus = FOCUS_ORDER;
					}
				} else if (focus >= 0) {
					focus++;
				}
				break;
			}
		} else { // list view
			int oldIdx = offset + focus;
			int newFocus = focus;
			int newOffset = offset;
			boolean focusChanged = true;
			if (data.length <= row || data.length - offset == row || focus != row / 2) {
				newFocus = Math.min(data.length - 1, Math.min(row - 1, focus + 1));
			} else {
				newOffset++;
				focusChanged = false;
			}
			int newIdx = newOffset + newFocus;
			if (((KaraokeListViewUI) getParent()).isChangeOrder() && picked && oldIdx != newIdx) {
				if (changeOrder == null) {
					changeOrder = new ChangeOrderEffect(this, 6);
				}
				if (focusChanged) {
					changeOrder.setBounds(new Rectangle(0, 38 + (focus) * 32, getWidth(), 36), new Rectangle(0, 38 + 4 + (focus+1) * 32, getWidth(), 32 - 4));
				} else {
					changeOrder.setBounds(new Rectangle(0, 38 + (focus) * 32, getWidth(), 36), false);
				}
				animation = true;
				changeOrder.start();
				animation = false;
				MoreDetail tmp = data[oldIdx];
				data[oldIdx] = data[newIdx];
				data[newIdx] = tmp;
			}
			offset = newOffset;
			focus = newFocus;
		}
		repaint();
		return false;
	}

	public void resetPicked() {
		picked = false;
	}

	public boolean keyOK() {
		if (focus >= 0) {
			if (getParent() instanceof KaraokeListViewUI && ((KaraokeListViewUI) getParent()).isChangeOrder()) {
				picked = !picked;
			} else {
//				if (EventQueue.isDispatchThread()) {
//					try {
//						CommunicationManager.getInstance().requestTuner(null, getCurrentCatalogue());
//						return true;
//					} catch (NullPointerException npe) {}
//				} // 4691
				if (CategoryUI.getPurchasedPartyPack() != null || CategoryUI.isTrialSong((MoreDetail) getCurrentCatalogue())) {
					if (EventQueue.isDispatchThread()) {
						try {
							CommunicationManager.getInstance().requestTuner(null, getCurrentCatalogue(), 0);
							return true;
						} catch (NullPointerException npe) {}
					} // 4691
					ArrayList list = new ArrayList();
					list.add(getCurrentCatalogue());
					MenuController.getInstance().goToNextScene(UITemplateList.PLAY_SCREEN, list);
				} else {
					MenuController.getInstance().purchaseKaraoke(true);
				}
			}
		} else if (focus == FOCUS_SEE_ALL) {
			if (getParent() instanceof CategoryUI) {
				((CategoryUI) getParent()).keyOkOnMenu(false);
			}
		} else if (focus == FOCUS_ORDER) {
//			if (EventQueue.isDispatchThread()) {
//				try {
//					CommunicationManager.getInstance().requestTuner(null, null);
//					return true;
//				} catch (NullPointerException npe) {}
//			} // 4691
			MenuController.getInstance().purchaseKaraoke(false);
		}
		return true;
	}

	public MoreDetail[] removeSelected() {
		if (data.length == 1) {
			data = new MoreDetail[0];
			return data;
		}
		if (removeEffect == null) {
			removeEffect = new RemoveEffect(this, 6);
		}
		removeEffect.setBounds(new Rectangle(0, 38 + (focus) * 32, getWidth(), 36));
		animation = true;
		removeEffect.start();
		animation = false;
		String oldStr = String.valueOf(data.length);
		ArrayList list = new ArrayList();
		for (int i = 0; i < data.length; i++) {
			if (i == offset + focus) {
				continue;
			}
			list.add(data[i]);
		}
		data = new MoreDetail[list.size()];
		list.toArray(data);
		if (offset + focus == data.length) {
			if (offset > 0) {
				offset--;
			} else {
				keyUp();
			}
		}
		String newStr = String.valueOf(data.length);
		title = TextUtil.replace(title, oldStr, newStr);
		return data;
	}

	public void addTop() {
		int oldIdx = offset + focus;
		if (oldIdx == 0) {
			return;
		}
		MoreDetail tmp = data[oldIdx];
		System.arraycopy(data, 0, data, 1, oldIdx);
		data[0] = tmp;
		offset = 0;
		focus = 0;
		repaint();
	}

	public BaseElement getCurrentCatalogue() {
		if (data.length <= offset + focus) {
			return null;
		}
		return data[offset + focus];
	}

	public BaseElement[] getData() {
		return data;
	}

	public void setOffsetFocus(int[] offsetFocus) {
		offset = offsetFocus[0];
		focus = offsetFocus[1];
	}

	public int[] getOffsetFocus() {
		return new int[] {offset, focus};
	}

	public void setData(BaseElement[] data, String title) {
		if (Log.DEBUG_ON) {
			Log.printDebug("called setData(MD), data = "
					+ data.length);
		}
		this.title = title + " (" + data.length + ")";
		subTitle = dataCenter.getString(Resources.TEXT_LBL_STYLE);
		this.data = new MoreDetail[data.length];
		for (int i = 0; i < data.length; i++) {
			this.data[i] = (MoreDetail) data[i];
		}
		offset = 0;
		focus = 0;
		row = 9;

		seeAll = false;
		needDim = true;
		order = false;
	}

	public int setData(CategoryContainer container) {
		if (Log.DEBUG_ON) {
			Log.printDebug("called setData(Container), data = "
					+ container.totalContents.length);
		}
		subTitle = dataCenter.getString(Resources.TEXT_LBL_STYLE);
		data = container.totalContents;
		title = container.getTitle() + " (" + data.length + ")";
		offset = 0;
		focus = 0;
		row = 5;

		seeAll = row < data.length;
		order = CategoryUI.getPurchasedPartyPack() == null;

		return 202 + (seeAll ? 33 : 0) + (order ? 69 : 0) + 22;
	}
	
	public void setOrderBanner(String url) {
		if (url != null && url.equals(bannerUrl) == false) {
			i_01_kara_pack_list = null;
			bannerUrl = url;
			final ImageLabel tmpLbl = new ImageLabel(null);
			tmpLbl.setImage(url);
			new Thread("setOrderBanner") {
				public void run() {
					while ((i_01_kara_pack_list = tmpLbl.getImage()) == null) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {}
					}
					repaint();
				}
			}.start();
			
		}
	}

	public void stop() {
		setVisible(false);
		data = null;
		category = null;
	}

	public int count() {
		return Math.min(row, data.length);
	}

	class RemoveEffect extends Effect {
		int[] delta;
		Rectangle focus;

		Image i_01_kara_list_sh_b;
		Image i_01_kara_list_sh_t;

		public RemoveEffect(Component c, int frameCount) {
			super(c, frameCount);
			delta = new int[frameCount];
			int diff = 32;
			int fcs = (frameCount - 1) * (frameCount - 1);
			for (int i = 0; i < frameCount; i++) {
				delta[i] = Math.round((float) diff * i * i / fcs);
			}
		}

		public void setBounds(Rectangle focus) {
			this.focus = focus;
//			i_01_kara_list_sh_b = dataCenter.getImage("01_kara_list_sh_b.png");
//			i_01_kara_list_sh_t = dataCenter.getImage("01_kara_list_sh_t.png");
		}

		protected void animate(Graphics g, DVBBufferedImage image, int frame) {
			g.drawImage(image, 0, 0, focus.width, focus.y,
					0, 0, focus.width, focus.y, null);

			g.drawImage(image, 0, focus.y - delta[frame] + 32, focus.width, focus.y - delta[frame] + 32 + component.getHeight(),
					0, focus.y + 32, focus.width, focus.y + 32 + component.getHeight(), null);

			if (drawUpperShadow) {
				g.drawImage(i_01_kara_list_sh_t, 1, 37, null);
			}
			if (drawLowerShadow) {
				g.drawImage(i_01_kara_list_sh_b, 1, 265, null);
			}
		}
	}

	class ChangeOrderEffect extends Effect {
		int[] delta;
		Rectangle focus, next;
		boolean up;

		Image i_01_kara_list_sh_b;
		Image i_01_kara_list_sh_t;

		public ChangeOrderEffect(Component c, int frameCount) {
			super(c, frameCount);
			delta = new int[frameCount];
			int diff = 32;
			int fcs = (frameCount - 1) * (frameCount - 1);
			for (int i = 0; i < frameCount; i++) {
				delta[i] = Math.round((float) diff * i * i / fcs);
			}
		}

		public void setBounds(Rectangle focus, Rectangle next) {
			this.focus = focus;
			this.next = next;
//			i_01_kara_list_sh_b = dataCenter.getImage("01_kara_list_sh_b.png");
//			i_01_kara_list_sh_t = dataCenter.getImage("01_kara_list_sh_t.png");
		}

		public void setBounds(Rectangle focus, boolean up) {
			this.focus = focus;
			this.next = null;
			this.up = up;
//			i_01_kara_list_sh_b = dataCenter.getImage("01_kara_list_sh_b.png");
//			i_01_kara_list_sh_t = dataCenter.getImage("01_kara_list_sh_t.png");
		}

		protected void animate(Graphics g, DVBBufferedImage image, int frame) {
			Rectangle ori = g.getClipBounds();
			g.clipRect(0, 38, component.getWidth(), component.getHeight() - 38 - 32);
			if (next != null) { // focus move
				if (next.y < focus.y) { // up
					g.drawImage(image, 0, 0, focus.width, next.y, 0, 0, focus.width, next.y, null);
					g.drawImage(image, 0, focus.y + focus.height, focus.width, component.getHeight(),  0, focus.y + focus.height, focus.width, component.getHeight(), null);
					g.drawImage(image, 0, next.y + delta[frame], next.width, next.y + delta[frame] + next.height, 0, next.y, next.width, next.y + next.height, null);
					g.drawImage(image, 0, focus.y - delta[frame], focus.width, focus.y - delta[frame] + focus.height, 0, focus.y, focus.width, focus.y + focus.height, null);
				} else { // down
					g.drawImage(image, 0, 0, focus.width, focus.y, 0, 0, focus.width, focus.y, null);
					g.drawImage(image, 0, next.y + next.height, focus.width, component.getHeight(),  0, next.y + next.height, focus.width, component.getHeight(), null);
					g.drawImage(image, 0, next.y - delta[frame], next.width, next.y - delta[frame] + next.height, 0, next.y, next.width, next.y + next.height, null);
					g.drawImage(image, 0, focus.y + delta[frame], focus.width, focus.y + delta[frame] + focus.height, 0, focus.y, focus.width, focus.y + focus.height, null);
				}
			} else { // offset move
				if (up) {
					//g.drawImage(image, 0, delta[frame], null);
					g.drawImage(image, 0, delta[frame], focus.width, delta[frame] + focus.y - 32, 0, 0, focus.width, focus.y - 32, null);
					g.drawImage(image, 0, delta[frame] + focus.y + focus.height, focus.width, delta[frame] + component.getHeight(), 0, focus.y + focus.height, focus.width, component.getHeight(), null);
					g.drawImage(image, 0, delta[frame] * 2 + focus.y - 32, focus.width, delta[frame] * 2 + focus.y, 0, focus.y - 32, focus.width, focus.y, null);
					g.drawImage(image, 0, focus.y, focus.width, focus.y + focus.height, 0, focus.y, focus.width, focus.y + focus.height, null);
				} else {
					//g.drawImage(image, 0, -delta[frame], null);
					g.drawImage(image, 0, -delta[frame], focus.width, -delta[frame] + focus.y, 0, 0, focus.width, focus.y, null);
					g.drawImage(image, 0, -delta[frame] + focus.y + focus.height + 32, focus.width, -delta[frame] + component.getHeight(), 0, focus.y + focus.height + 32, focus.width, component.getHeight(), null);
					g.drawImage(image, 0, -delta[frame] * 2 + focus.y + focus.height, focus.width, -delta[frame] * 2 + focus.y + focus.height + 32, 0, focus.y + focus.height, focus.width, focus.y + focus.height + 32, null);
					g.drawImage(image, 0, focus.y, focus.width, focus.y + focus.height, 0, focus.y, focus.width, focus.y + focus.height, null);
				}
			}
			g.setClip(ori);
			if (drawUpperShadow) {
				g.drawImage(i_01_kara_list_sh_t, 1, 37, null);
			}
			if (drawLowerShadow) {
				g.drawImage(i_01_kara_list_sh_b, 1, 265, null);
			}
		}

	}

	class KaraokeViewRenderer extends Renderer {

		public Rectangle getPreferredBounds(UIComponent c) {
			return null;
		}

		Image arrUp;// = dataCenter.getImage("02_ars_t.png");
		Image arrDn;// = dataCenter.getImage("02_ars_b.png");
		Image i_01_kara_list_bg;// = dataCenter.getImage("01_kara_list_bg.png");
		Image i_01_kara_list_bg_2;// = dataCenter.getImage("01_kara_list_bg_2.png");
//		Image i_01_kara_pack_list;// = dataCenter.getImage("01_kara_pack_list.png");
		Image i_01_kara_pack_list_foc;// = dataCenter.getImage("01_kara_pack_list_foc.png");
		Image i_01_kara_list_line;// = dataCenter.getImage("01_kara_list_line.png");
//		Image i_01_kara_list_sh;// = dataCenter.getImage("01_kara_list_sh.png");
//		Image i_01_kara_list_gap;// = dataCenter.getImage("01_kara_list_gap.png");
//		Image i_01_kara_list_sh_b;// = dataCenter.getImage("01_kara_list_sh_b.png");
//		Image i_01_kara_list_sh_t;// = dataCenter.getImage("01_kara_list_sh_t.png");
		Image i_01_kara_list_foc;// = dataCenter.getImage("01_kara_list_foc.png");
		Image i_01_kara_list_dim;// = dataCenter.getImage("01_kara_list_dim.png");
		Image i_09_icon_song;
		Image i_09_icon_song_foc;
		Color cBase = new Color(46, 46, 45);
		Color cTitle = new Color(214, 182, 61);
		Color cSubTitle = new Color(196, 196, 196);
		Color cList = new Color(230, 230, 230);
		Color cListDim = new Color(117, 117, 117);
		Color cHigh = new Color(249, 194, 0);
		Color c3 = new Color(3, 3, 3);
		Color cLine = new Color(120, 120, 120, 100);
		Color cFocus = new Color(255, 205, 12);

		protected void paint(Graphics g, UIComponent c) {
			boolean isFocus = false;
			boolean isDetail = false;
			boolean isChangeOrder = false;
			boolean isPlaylist = false;
			if (c.getParent() instanceof CategoryUI) {
				CategoryUI ui = (CategoryUI) c.getParent();
				isFocus = ui.getCurrentFocusUI() == CategoryUI.FOCUS_RIGHT;
			} else { // detail..
				//g.translate(0, 14);
				isDetail = true;
				KaraokeListViewUI ui = (KaraokeListViewUI) c.getParent();
				isFocus = ui.getCurrentFocusUI() == KaraokeListViewUI.FOCUS_LIST;
				isChangeOrder = ui.isChangeOrder();
				isPlaylist = ui.isPlaylist();
			}

			// bg
			if (isDetail == false) {
				g.drawImage(i_01_kara_list_bg, 0, 0, c);

				// title
				if (title != null) {
					g.setFont(FontResource.BLENDER.getFont(20));
					g.setColor(cBase);
					g.drawString(title, 19, 26);
					g.setColor(cTitle);
					g.drawString(title, 18, 25);
				}

				// subtitle
//				if (subTitle != null) {
//					g.setFont(FontResource.BLENDER.getFont(18));
//					g.setColor(cSubTitle);
//					GraphicUtil.drawStringRight(g, subTitle, 521, 24);
//				} else {
////					g.setFont(FontResource.BLENDER.getFont(18));
////					g.setColor(cSubTitle);
////					GraphicUtil.drawStringRight(g, "o = " + offset + ", f = " + focus + ", l = " + data.length, 521, 24);
//				}
			}
			if (subTitle != null) {
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(cSubTitle);
				GraphicUtil.drawStringRight(g, subTitle, 521, 24);
			}

			// list
			g.setFont(FontResource.BLENDER.getFont(18));
			FontMetrics fm = g.getFontMetrics();
			int limit = animation && isChangeOrder ? row + 1 : row;
			int start = animation && isChangeOrder && offset > 0 ? -1 : 0;
			ArrayList playlist = MenuController.getInstance().getPlaylist();
			int x;
			boolean purchased = CategoryUI.getPurchasedPartyPack() != null;
			for (int i = start; offset + i < data.length && i < limit; i++) {
				g.setColor(purchased || CategoryUI.isTrialSong(data[offset + i]) ? cList : cListDim);
				x = 19;
				if (isPlaylist == false && playlist.contains(data[offset + i]) && isChangeOrder == false) {
					g.drawImage(i_09_icon_song, 16, 45 + i * 32, c);
					x += 20;
				}
				if (isDetail) {
					String songTitle = data[offset + i].getTitle();
					if (isPlaylist || isChangeOrder) {
						int number = offset+i+1;
						songTitle = String.valueOf(number) + ". " + songTitle;
						if (number < 10) {
							songTitle = "0" + songTitle;
						}
					}
					g.drawString(TextUtil.shorten(songTitle, fm, 275), x, 61 + i * 32);
					GraphicUtil.drawStringRight(g, TextUtil.shorten(data[offset + i].getActors(1), fm, 150),
							464, 61 + i * 32);
					GraphicUtil.drawStringRight(g, Formatter.getCurrent().getDuration(data[offset + i].getRuntime()),
							521, 61 + i * 32);
				} else {
					g.drawString(TextUtil.shorten(data[offset + i].getTitle(), fm, 335), x, 61 + i * 32);
					GraphicUtil.drawStringRight(g, TextUtil.shorten(data[offset + i].getActors(1), fm, 150),
							521, 61 + i * 32);
				}

				if (isDetail == false && (i < row - 1 || seeAll)) {
					//g.drawImage(i_01_kara_list_line, 19, 72 + i * 32, c);
					g.setColor(cLine);
					g.drawLine(19, 72 + i * 32, 19 + 502, 72 + i * 32);
				}
			}
			int h = i_01_kara_list_bg.getHeight(c);
			if (seeAll) {
				g.drawImage(i_01_kara_list_bg_2, 0, 202, c);
				String str = "> "
						+ dataCenter.getString(Resources.TEXT_MSG_SEE) + " %% "
						+ dataCenter.getString(Resources.TEXT_MSG_TITLES);
				String all = dataCenter.getString(Resources.TEXT_MSG_ALL);
				g.setFont(FontResource.BLENDER.getFont(20));
				GraphicUtil.drawHighlightedTextCenter(g, str, 269, 223, cList,
						new Color[] { cHigh }, new String[] { all });
				h += i_01_kara_list_bg_2.getHeight(c);
			}
			if (order && i_01_kara_pack_list != null) {
//				g.drawImage(i_01_kara_list_gap, 0, h, c);
				h += 4;//i_01_kara_list_gap.getHeight(c);
				g.drawImage(i_01_kara_pack_list, 0, h, c);
				if (isFocus && focus == FOCUS_ORDER) {
					g.drawImage(i_01_kara_pack_list_foc, 0, h, c);
				}
				h += i_01_kara_pack_list.getHeight(c);
			}
			if (isDetail) {
				h += 123;
			} else {
//				g.drawImage(i_01_kara_list_sh, 0, h, c);
			}
			//g.drawImage(i_01_kara_list_sh, 0, h, c);

			// arrow & shadow
			drawUpperShadow = offset > 0;
			if (animation == false && drawUpperShadow) {
//				g.drawImage(i_01_kara_list_sh_t, 1, 37, c);
				g.drawImage(arrUp, 254, 31, c);
			}

			drawLowerShadow = isDetail && data.length - row > offset;
			if (animation == false && drawLowerShadow) {
//				g.drawImage(i_01_kara_list_sh_b, 1, 265, c);
				g.drawImage(arrDn, 254, 317, c);
			}

			// focus
			g.setFont(FontResource.BLENDER.getFont(19));
			fm = g.getFontMetrics();
			boolean text = false;
			if (isFocus) {
				if (focus >= 0) {
					if (isChangeOrder && picked == false) {
						text = false;
						g.setColor(cFocus);
						int w = getWidth() - 3;
						g.drawRect(1, 38 + focus * 32, w, 36);
						g.drawRect(2, 39 + focus * 32, w - 2, 34);
						g.drawRect(3, 40 + focus * 32, w - 4, 32);
					} else {
						g.drawImage(i_01_kara_list_foc, -1, 38 + focus * 32, c);
						text = true;
						g.setColor(c3);
					}
					if (isChangeOrder && picked && animation == false) {
						if (focus > 0) {
							g.drawImage(arrUp, 254, 38 + focus * 32 - 17, c);
						}
						if (focus < row - 1 && focus < data.length - 1)
						g.drawImage(arrDn, 254, 38 + focus * 32 + 36, c);
					}
				} else if (focus == FOCUS_SEE_ALL) {
					g.setFont(FontResource.BLENDER.getFont(20));
					g.setColor(c3);
					g.drawImage(i_01_kara_list_foc, -1,
							i_01_kara_list_bg.getHeight(c) - 2, c);
					String str = "> " + dataCenter.getString(Resources.TEXT_MSG_SEE)
							+ " "
							+ dataCenter.getString(Resources.TEXT_MSG_ALL)
							+ " "
							+ dataCenter.getString(Resources.TEXT_MSG_TITLES);
					GraphicUtil.drawStringCenter(g, str, 269, 223);
				}
			} else if (needDim && isChangeOrder == false && animation == false) {
				g.drawImage(i_01_kara_list_dim, -1, 38 + focus * 32, c);
				g.setColor(c3);
				text = true;
			}
			if (text) {
				x = 19;
				if (isPlaylist == false && playlist.contains(data[offset + focus]) && isChangeOrder == false) {
					g.drawImage(i_09_icon_song_foc, 16, 45 + focus * 32, c);
					x +=20;
				}
				if (isDetail) {
					String songTitle = data[offset + focus].getTitle();
					if (isPlaylist || isChangeOrder) {
						int number = offset+focus+1;
						songTitle = String.valueOf(number) + ". " + songTitle;
						if (number < 10) {
							songTitle = "0" + songTitle;
						}
					}
					g.drawString(TextUtil.shorten(songTitle, fm, 275), x, 61 + focus * 32);
					GraphicUtil.drawStringRight(g, TextUtil.shorten(data[offset + focus].getActors(1), fm, 150),
							464, 61 + focus * 32);
					GraphicUtil.drawStringRight(g, Formatter.getCurrent().getDuration(data[offset + focus].getRuntime()),
							521, 61 + focus * 32);
				} else {
					g.drawString(TextUtil.shorten(data[offset + focus].getTitle(), fm, 335), x, 61 + focus * 32);
					GraphicUtil.drawStringRight(g, TextUtil.shorten(data[offset + focus].getActors(1), fm, 150),
							521, 61 + focus * 32);
				}
			}
//			if (detail) {
//				g.translate(0, -14);
//			}
		}

		public void prepare(UIComponent c) {
			arrUp = dataCenter.getImage("02_ars_t.png");
			arrDn = dataCenter.getImage("02_ars_b.png");
			i_01_kara_list_bg = dataCenter.getImage("01_kara_list_bg.png");
			i_01_kara_list_bg_2 = dataCenter.getImage("01_kara_list_bg_2.png");
//			i_01_kara_pack_list = dataCenter.getImage("01_kara_pack_list.png");
			i_01_kara_pack_list_foc = dataCenter.getImage("01_kara_pack_list_foc.png");
			i_01_kara_list_line = dataCenter.getImage("01_kara_list_line.png");
//			i_01_kara_list_sh = dataCenter.getImage("01_kara_list_sh.png");
//			i_01_kara_list_gap = dataCenter.getImage("01_kara_list_gap.png");
//			i_01_kara_list_sh_b = dataCenter.getImage("01_kara_list_sh_b.png");
//			i_01_kara_list_sh_t = dataCenter.getImage("01_kara_list_sh_t.png");
			i_01_kara_list_foc = dataCenter.getImage("01_kara_list_foc.png");
			i_01_kara_list_dim = dataCenter.getImage("01_kara_list_dim.png");
			i_09_icon_song = dataCenter.getImage("09_icon_song.png");
			i_09_icon_song_foc = dataCenter.getImage("09_icon_song_foc.png");
		}
	}
}

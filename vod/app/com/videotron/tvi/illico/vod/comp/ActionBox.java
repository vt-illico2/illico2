package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.controller.ComponentPool;
import com.videotron.tvi.illico.vod.controller.menu.MenuController;
import com.videotron.tvi.illico.vod.data.vcds.type.BaseElement;
import com.videotron.tvi.illico.vod.data.vcds.type.Episode;
import com.videotron.tvi.illico.vod.data.vcds.type.Video;
import com.videotron.tvi.illico.vod.ui.BaseUI;

/**
 * Menu list on lower-right.
 */
public final class ActionBox extends UIComponent {
	/** Instance of DataCenter. */
	private static DataCenter dataCenter = DataCenter.getInstance();
	/** Labels of buttons. */
	private String[] buttons;
	/** Renderer instance. */
	private Renderer renderer = ActionBoxRenderer.getInstance();

	private ClickingEffect clickEffect;
	
	// R5
	private boolean useDimmedFocus;
	private boolean hasDimmedBg;
	
	/** Contructor. */
	private ActionBox() {
		setRenderer(renderer);
		prepare();
	}

	/** Counter for this component. */
	public static int instanceCounter = 0;

	/** Factory pattern, instance getter. */
	public static ActionBox newInstance() {
		instanceCounter++;
		ActionBox newInstance = new ActionBox();
		ComponentPool.addComponent(newInstance);
		return newInstance;
	}

	/** Stop. */
	public void stop() {
		renderer = null;
		buttons = null;
		instanceCounter--;
	}

	/**
	 * Set data for this component.
	 * 
	 * @param label
	 *            labels for buttons
	 * @param x
	 *            position of this
	 * @param y
	 *            position of this
	 */
	public void setActionBox(String[] label, int x, int y) {
		buttons = label;
		setBtnBounds(x, y);
		useDimmedFocus = false;
		hasDimmedBg = false;
	}

	/**
	 * Set data for this component.
	 * 
	 * @param label
	 *            labels for buttons
	 */
	public void setActionBox(String[] label) {
		buttons = label;
		int height = buttons.length * ActionBoxRenderer.GAP + 22;
		if (buttons.length == 1) {
			height += 3;
		}
		setBtnBounds(579, 497 - height);
		focus = 0;
		useDimmedFocus = false;
		hasDimmedBg = false;
	}
	
	public String[] getActionBox() {
		return buttons;
	}

	/**
	 * Move focus.
	 * 
	 * @param up
	 *            move up or down
	 * @return true if reaches top or bottom
	 */
	public boolean moveFocus(boolean up) {
		if (up) {
			if (focus == 0) {
				return true;
			}
			focus--;
		} else {
			if (focus == buttons.length - 1) {
				return true;
			}
			focus++;
		}
		repaint();
		return false;
	}

	/**
	 * Getter for buttons.
	 * 
	 * @return button labels
	 */
	public String getMenu() {
		return buttons[focus].charAt(0) == '_' || buttons[focus].charAt(0) == '+' || buttons[focus].charAt(0) == '!' ?
				buttons[focus].substring(1) : buttons[focus];
//		return buttons[focus];
	}
	
	public void click() {
		clickEffect.start(45, -1 + getFocus() * ActionBoxRenderer.GAP, 292 - 8, 50 - 9);
	}
	
	/**
	 * Set focus index.
	 * 
	 * @param newFocus
	 *            focus
	 */
	public void setFocus(int newFocus) {
		if (buttons == null) {
			return;
		}
		focus = Math.min(newFocus, buttons.length - 1);
	}
	
	public int getCount() {
		return buttons == null ? 0 : buttons.length;
	}

	/**
	 * Set position.
	 * 
	 * @param x
	 *            position of boundary
	 * @param y
	 *            position of boundary
	 */
	private void setBtnBounds(int x, int y) {
		int height = buttons.length * ActionBoxRenderer.GAP + 22;
		this.setBounds(x, y, 372, height);
		clickEffect = new ClickingEffect(this, 5);
		// renderer.prepare(this);
		// isFocused = true;
	}

	/**
	 * Returns has focus or not.
	 * 
	 * @return true if it has focus
	 */
	public boolean hasFocus() {
		BaseUI parent = (BaseUI) getParent();
		return parent != null && parent.getState() == BaseUI.STATE_ACTIVATED;
	}
	
	public void setDimmedFocus(boolean dimmed) {
		useDimmedFocus = dimmed;
	}
	
	public boolean useDimmedFocus() {
		return useDimmedFocus;
	}
	
	public void setDimmedBg(boolean dimmed) {
		hasDimmedBg = dimmed;
	}
	
	public boolean hasDimmedBg() {
		return hasDimmedBg;
	}

	// R7.3
	public String getWatchNowLabel() {

		BaseElement element = MenuController.getInstance().getCurrentElement();

		if (element instanceof Episode) {
			return dataCenter.getString(Resources.LBL_EPISDOE_FOR_TEXT_WATCH);
		} else if (element instanceof Video) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ActionBox, getWatchNowLabel(), video id=" + ((Video) element).getTypeID());
			}
			if ("VT_124".equals(((Video) element).getTypeID())) {
				return " " + dataCenter.getString(Resources.TEXT_LBL_TRAILER);
			}
			if ("VT_104".equals(((Video) element).getTypeID())) {
				return dataCenter.getString(Resources.LBL_EXTRA_FOR_TEXT_WATCH);
			}
			if ("VT_100".equals(((Video) element).getTypeID())) {
				return dataCenter.getString(Resources.LBL_EPISDOE_FOR_TEXT_WATCH);
			}
			if ("VT_102".equals(((Video) element).getTypeID())) {
				return " " + dataCenter.getString(Resources.TEXT_LBL_MOVIE2);
			}
		}
		return " " + dataCenter.getString(Resources.TEXT_LBL_CONTENT);
	}


	/** Renderer class. */
	static class ActionBoxRenderer extends Renderer {
		/** Singleton pattern. */
		private static ActionBoxRenderer instance;
		/** Size between buttons. */
		public static final int GAP = 39;
		/** Image. */
		private Image i01Acbg1st;// = dataCenter.getImage("01_acbg_1st.png");
		/** Image. */
		private Image i01Acbg2nd;// = dataCenter.getImage("01_acbg_2nd.png");
		/** Image. */
		private Image i01AcbgM;// = dataCenter.getImage("01_acbg_m.png");
		/** Image. */
//		private Image i01AcShadow;// = dataCenter.getImage("01_ac_shadow.png");
		/** Image. */
//		private Image i01Acglow;// = dataCenter.getImage("01_acglow.png");
		/** Image. */
		private Image i01Vodfocus;// = dataCenter.getImage("02_detail_bt_foc.png");
		/** Image. */
		private Image i01Acline;// = dataCenter.getImage("01_acline.png");
		/** Image. */
		private Image blit01;// = dataCenter.getImage("02_detail_ar.png");
		private Image i_02_icon_isa_ac;
		private Image i_02_icon_isa_bl;
		// R5
		private Image i02VodDim;
		private Image iHeartFocus;
		private Image iHeartDim;
		private Image i01Acbg1st_1;
		private Image i01Acbg2nd_1;
		private Image i01AcbgM_1;
		private Image i01Acline_1;
		private String similarStr;

		// Bundling
		private Image i_03_srcicon_ppv_foc;
		private Image i_03_srcicon_ppv_foc2;
		
		/** Color. */
		private Color cBtnDim = new Color(230, 230, 230);
		/** Color. */
		private Color cBtn = new Color(3, 3, 3);
		/** Color. */
		private Color cDisable = new Color(113, 113, 113);
		
		/** Instance getter. */
		public static synchronized ActionBoxRenderer getInstance() {
			if (instance == null) {
				instance = new ActionBoxRenderer();
			}
			return instance;
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return null;
		}

		public void prepare(UIComponent c) {
			i01Acbg1st = dataCenter.getImage("01_acbg_1st.png");
			i01Acbg2nd = dataCenter.getImage("01_acbg_2nd.png");
			i01AcbgM = dataCenter.getImage("01_acbg_m.png");
//			i01AcShadow = dataCenter.getImage("01_ac_shadow.png");
//			i01Acglow = dataCenter.getImage("01_acglow.png");
			i01Vodfocus = dataCenter.getImage("02_detail_bt_foc.png");
			i01Acline = dataCenter.getImage("01_acline.png");
			blit01 = dataCenter.getImage("02_detail_ar.png");
			i_02_icon_isa_ac = dataCenter.getImage("02_icon_isa_ac.png");
			i_02_icon_isa_bl = dataCenter.getImage("02_icon_isa_bl.png");
			
			// R5
			i02VodDim = dataCenter.getImage("02_detail_bt_foc-dim.png");
			iHeartFocus = dataCenter.getImage("icon_heart_f.png");
			iHeartDim = dataCenter.getImage("icon_heart_n.png");
			i01Acbg1st_1 = dataCenter.getImage("01_acbg_1st_1.png");
			i01Acbg2nd_1 = dataCenter.getImage("01_acbg_2nd_1.png");
			i01AcbgM_1 = dataCenter.getImage("01_acbg_m_1.png");
			i01Acline_1 = dataCenter.getImage("01_acline_1.png");
			
			similarStr = dataCenter.getString(Resources.TEXT_SIMILAR_CONTENT);

			// Bundling
			i_03_srcicon_ppv_foc = dataCenter.getImage("03_srcicon_ppv_foc.png");
			i_03_srcicon_ppv_foc2 = dataCenter.getImage("03_srcicon_ppv_foc2.png");
		}

		public void paint(Graphics g, UIComponent c) {
			ActionBox ui = (ActionBox) c;
			if (ui.buttons == null || ui.buttons.length == 0) {
				return;
			}
			// draw background
			int btnBgHeight = ui.buttons.length * GAP;
			if (ui.buttons.length == 1) {
				btnBgHeight -= 3;
			}
//			g.drawImage(i01AcShadow, 0, btnBgHeight - 10, c);
			if (ui.hasDimmedBg) {
				if (ui.buttons.length > 2) {
					g.drawImage(i01AcbgM_1, 47, 79, 284, btnBgHeight - 79, c);
				}
				if (ui.buttons.length == 1) {
					g.drawImage(i01Acbg1st_1, 47, 0, 284, 39, c);
				} else {
					g.drawImage(i01Acbg1st_1, 47, 0, c);
				}
				if (ui.buttons.length > 1) {
					g.drawImage(i01Acbg2nd_1, 47, 43, c);
				}
			} else {
				if (ui.buttons.length > 2) {
					g.drawImage(i01AcbgM, 47, 79, 284, btnBgHeight - 79, c);
				}
				if (ui.buttons.length == 1) {
					g.drawImage(i01Acbg1st, 47, 0, 284, 39, c);
				} else {
					g.drawImage(i01Acbg1st, 47, 0, c);
				}
				if (ui.buttons.length > 1) {
					g.drawImage(i01Acbg2nd, 47, 43, c);
				}
			}
//			g.drawImage(i01Acglow, 46, btnBgHeight - 20, c);

			// draw buttons
			g.setFont(FontResource.BLENDER.getFont(18));
			for (int i = 0; i < ui.buttons.length; i++) {
				if (ui.buttons[i].charAt(0) == '!') {
					g.drawImage(i_03_srcicon_ppv_foc2, 59, 12 + i * GAP, c);
				} else if (ui.buttons[i].charAt(0) == '+') {
					g.drawImage(i_02_icon_isa_ac, 59, 10 + i * GAP, c);
				} else {
					if (ui.buttons[i].equals(similarStr)) {
						g.drawImage(iHeartDim, 55, 7 + i * GAP, c);
					} else {
						g.drawImage(blit01, 70, 17 + i * GAP, c);
					}
				}
				g.setColor(ui.buttons[i].charAt(0) == '_' ? cDisable : cBtnDim);

				if (ui.buttons[i].equals(dataCenter.getString(Resources.TEXT_WATCH))) {
					g.drawString(TextUtil.shorten(ui.buttons[i] + ui.getWatchNowLabel(),
							g.getFontMetrics(), 230),92, 25 + i * GAP);
				} else {
					g.drawString(TextUtil.shorten(ui.buttons[i].charAt(0) == '_' || ui.buttons[i].charAt(0) == '+'
							|| ui.buttons[i].charAt(0) == '!' ?
							ui.buttons[i].substring(1) : ui.buttons[i], g.getFontMetrics(), 230), 92,
							25 + i * GAP);
				}
				if (i < ui.buttons.length - 1) {
					if (ui.hasDimmedBg) {
						g.drawImage(i01Acline_1, 62, 40 + i * GAP, c);
					} else {
						g.drawImage(i01Acline, 62, 40 + i * GAP, c);
					}
				}
			}
			if (ui.hasFocus()) {
				if (ui.useDimmedFocus()) {
					g.drawImage(i02VodDim, 45, -1 + c.getFocus() * GAP, c);
				} else {
					g.drawImage(i01Vodfocus, 45, -1 + c.getFocus() * GAP, c);
				}
				
				if (ui.buttons[c.getFocus()].equals(similarStr)) {
					g.drawImage(iHeartFocus, 55, 7 + c.getFocus() * GAP, c);
				}

				g.setColor(ui.buttons[c.getFocus()].charAt(0) == '_' ? cDisable : cBtn);
				g.setFont(FontResource.BLENDER.getFont(21));
				if (ui.buttons[c.getFocus()].equals(dataCenter.getString(Resources.TEXT_WATCH))) {
					g.drawString(TextUtil.shorten(ui.buttons[c.getFocus()] + ui.getWatchNowLabel(),
							g.getFontMetrics(), 230),92, 25 + c.getFocus() * GAP);
				} else {

					if (ui.buttons[c.getFocus()].charAt(0) == '!') {
						g.drawImage(i_03_srcicon_ppv_foc, 59, 12 + c.getFocus() * GAP, c);
					} else if (ui.buttons[c.getFocus()].charAt(0) == '+') {
						g.drawImage(i_02_icon_isa_bl, 59, 10 + c.getFocus() * GAP, c);
					}
					g.drawString(TextUtil.shorten(ui.buttons[c.getFocus()].charAt(0) == '_'
									|| ui.buttons[c.getFocus()].charAt(0) == '+' || ui.buttons[c.getFocus()].charAt(0) == '!' ?
									ui.buttons[c.getFocus()].substring(1) : ui.buttons[c.getFocus()], g.getFontMetrics(), 230),
							92, 25 + c.getFocus() * GAP);
				}
			}
			if (Log.EXTRA_ON || Environment.EMULATOR) {
				g.setFont(FontResource.BLENDER.getFont(19));
				g.setColor(Color.green);
				GraphicUtil.drawStringCenter(g, BaseUI.buttonInfo, c.getWidth()/2, c.getHeight()-8);
			}
		}
	}
}

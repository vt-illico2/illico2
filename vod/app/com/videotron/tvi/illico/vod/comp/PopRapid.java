package com.videotron.tvi.illico.vod.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.vod.app.Resources;
import com.videotron.tvi.illico.vod.ui.BaseUI;

public class PopRapid extends BaseUI implements Runnable {
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;

	private BaseUI parent;
	private int mode;
	private Thread closer;
	private static PopRapid instance;

	synchronized public static PopRapid getInstance() {
		if (instance == null) {
			instance = new PopRapid();
		}
		return instance;
	}

	private PopRapid() {
		setRenderer(new PopSelectOptionRenderer());
		prepare();
		setVisible(false);
	}
	
	public void run() {
		while (true) {
			try {
				Thread.sleep(2000);
				break;
			} catch (InterruptedException e1) {
				continue;
			}
		}
		if (closer == Thread.currentThread()) {
			keyBack();
		}
	}

	public void show(BaseUI comp, int mode) {
		parent = comp;
		this.mode = mode;

		focus = 0;
		prepare();
		setVisible(true);
		
		closer = new Thread(this);
		closer.start();
	}

	public void stop() {
		setVisible(false);
		parent.remove(this);
		parent.setPopUp(null);
	}
	
	public String getCurrent() {
		return "";
	}

	public boolean handleKey(int key) {
		switch (key) {
		case OCRcEvent.VK_UP:
			if (mode == VERTICAL) {
				focus--;
				if (focus < 0) {
					focus = 26;
				}
				repaint(364, 153, 232, 289);
			}
			break;
		case OCRcEvent.VK_DOWN:
			if (mode == VERTICAL) {
				focus++;
				if (focus > 26) {
					focus = 0;
				}
				repaint(364, 153, 232, 289);
			}
			break;
		case OCRcEvent.VK_LEFT:
			if (mode == HORIZONTAL) {
				focus--;
				if (focus < 0) {
					focus = 26;
				}
				repaint(364, 153, 232, 289);
			}
			break;
		case OCRcEvent.VK_RIGHT:
			if (mode == HORIZONTAL) {
				focus++;
				if (focus > 26) {
					focus = 0;
				}
				repaint(364, 153, 232, 289);
			}
			break;
		case OCRcEvent.VK_ENTER:
			keyOK();
			break;
		case OCRcEvent.VK_EXIT:
		case KeyCodes.LAST:
		case KeyEvent.VK_BACK_SPACE:
			keyBack();
			break;
		}
		closer.interrupt();
		return true;
	}

	public void keyOK() {
		parent.popUpClosed(this, focus == 0 ? "0" : String.valueOf((char) ('A'+(focus-1))));
	}

	public void keyBack() {
		parent.popUpClosed(this, null);
	}

	private class PopSelectOptionRenderer extends Renderer {
		private Image i_01_rapid_bg;
		private Image arrUp;// = dataCenter.getImage("02_ars_t.png");
		private Image arrDn;// = dataCenter.getImage("02_ars_b.png");
		private Image arrLeft;// = dataCenter.getImage("02_ars_l.png");
		private Image arrRight;// = dataCenter.getImage("02_ars_r.png");

		private Color cDimmedBG = new DVBColor(12, 12, 12, 204);
		private Color cText = new Color(255, 205, 12);
		public void prepare(UIComponent c) {
			i_01_rapid_bg = dataCenter.getImage("01_rapid_bg.png");
			arrUp = dataCenter.getImage("02_ars_t.png");
			arrDn = dataCenter.getImage("02_ars_b.png");
			arrLeft = dataCenter.getImage("02_ars_l.png");
			arrRight = dataCenter.getImage("02_ars_r.png");
		}

		public void paint(Graphics g, UIComponent c) {
			g.setColor(cDimmedBG);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			// background
			g.drawImage(i_01_rapid_bg, 364, 153, c);

			if (mode == HORIZONTAL) {
				g.drawImage(arrLeft, 369, 248, c);
				g.drawImage(arrRight, 570, 248, c);
			} else if (mode == VERTICAL) {
				g.drawImage(arrUp, 464, 154, c);
				g.drawImage(arrDn, 464, 356, c);
			}
			String text = focus == 0 ? "0" : String.valueOf((char) ('A'+(focus-1)));
			g.setColor(cText);
			g.setFont(FontResource.BLENDER.getFont(100));
			GraphicUtil.drawStringCenter(g, text, 481, 294);
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Resources.fullRec;
		}
	}
}

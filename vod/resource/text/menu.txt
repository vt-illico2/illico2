sorting	Tri	Sorting
filter	Filtrer les chaînes	Filter Channels
view_this_program	Visionner l'émission	View this program
record_this_program	Enregistrer l'émission	Record this program
save_as_a_favorite	Ajouter chaîne aux favoris	Add to Favorite Channels
block_this_channel	Bloquer cette chaîne	Block this Channel
sort_by_number	Trier par numéro	Sort by number
sort_by_name	Trier par nom	Sort by name
channel_attributes	Attributs du canal	Channel attributes
channel_type	Type de canal	Channel type
genre	Genre	Genre
channel_call_letter	Lettre d'appel du canal	Channel call letter
title	Titre	Title
personalized_filters	Filtres personnalisés	Personalized filters
technician	Technicien	Technician
search	Recherche	Search

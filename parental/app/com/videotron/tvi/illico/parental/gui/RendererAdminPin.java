package com.videotron.tvi.illico.parental.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.ui.SceneAdminPin;
import com.videotron.tvi.illico.util.TextUtil;

public class RendererAdminPin extends Renderer {
    private DataCenter dataCenter = DataCenter.getInstance();
    
    private Image imgShadow = dataCenter.getImage("12_ppop_sha.png");
    private Image imgPop = dataCenter.getImage("12_pop_high_625.png");
    private Image imgTitleLine = dataCenter.getImage("07_respop_title.png");
    private Image imgStepLine = dataCenter.getImage("12_ppop_step.png");
    private Image imgButtonB = dataCenter.getImage("05_focus_dim.png");
    private Image imgButtonF = dataCenter.getImage("05_focus.png");
    private Image imgPinBoxB = dataCenter.getImage("05_pin_box.png");
    private Image imgPinBoxF = dataCenter.getImage("05_pin_focus.png");  
    
    private Image imgIconCheck = dataCenter.getImage("icon_g_check.png");
    private Image imgIconX = dataCenter.getImage("icon_r_x.png");
    
    private final int descWth = 576;
    
    private final String txtStar = "*";
    private final int areaGap = 38;
    private final int pinGap = 38;
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paint(Graphics g, UIComponent c) {
        //Background
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
        if (imgShadow != null) {
            g.drawImage(imgShadow, 170, 483, 625, 57, c);
        }
        g.setColor(Rs.C033033033);
        g.fillRect(170, 51, 625, 435);
        if (imgPop != null) {
            g.drawImage(imgPop, 170, 51, c);
        }
        if (imgTitleLine != null) {
            g.drawImage(imgTitleLine, 197, 89, c);
        }
        if (imgStepLine != null) {
            g.drawImage(imgStepLine, 176, 108, c);
        }
        g.setColor(Rs.DVB255255255038);
        g.fillRect(191, 293, 585, 1);
        
        //Description
        g.setFont(Rs.F18);
        g.setColor(Rs.C229229229);
        String desc0 = (String) dataCenter.get("TxtParental.AdminPinDesc0");
        if (desc0 != null) {
            String[] desc0s = TextUtil.split(desc0, Rs.FM18, descWth);
            for (int i=0; i<desc0s.length; i++) {
                g.drawString(desc0s[i], 193, 164+(i*20));
            }
        }
        String desc1 = (String) dataCenter.get("TxtParental.AdminPinDesc1");
        if (desc1 != null) {
            String[] desc1s = TextUtil.split(desc1, Rs.FM18, descWth);
            for (int i=0; i<desc1s.length; i++) {
                g.drawString(desc1s[i], 193, 256+(i*20));
            }
        }
        
        SceneAdminPin scene = (SceneAdminPin)c;
        boolean isButtonArea = scene.isButtonArea();
        int curState = scene.getCurrentState();
        int curArea = scene.getCurrentArea();
        int curIndex = scene.getCurrentIndex();
        String[] pinCodes = scene.getPINCodes();
        String[] rPinCodes = scene.getRepeatPINCodes();
        
        //Desc
        String desc2 = null;
        switch(curState) {
            case SceneAdminPin.STATE_CANCEL:
            case SceneAdminPin.STATE_NORMAL:
                desc2 = (String) dataCenter.get("TxtParental.AdminPinDesc2");                
                break;
            case SceneAdminPin.STATE_VALID:
                desc2 = (String) dataCenter.get("TxtParental.AdminPinDesc2");
                g.drawImage(imgIconCheck, 630, 344, c) ;
                g.drawImage(imgIconCheck, 630, 344+areaGap, c) ;
                break;
            case SceneAdminPin.STATE_INVALID:
                desc2 = (String) dataCenter.get("TxtParental.AdminPinDesc3");
                g.drawImage(imgIconCheck, 630, 344, c) ;
                g.drawImage(imgIconX, 630, 344+areaGap, c) ;
                break;
        }
        if (desc2 != null) {
            g.drawString(desc2, 480-(Rs.FM18.stringWidth(desc2)/2), 322);
        }
        
        //Button
        Image imgButton = null;
        if (isButtonArea) {
            imgButton = imgButtonF;
        } else {
            imgButton = imgButtonB;
        }
        if (imgButton != null) {
            g.drawImage(imgButton, 400, 437, c);
        }
        String txtButton = null;
        switch(curState) {
            case SceneAdminPin.STATE_CANCEL:
                txtButton = (String) dataCenter.get("TxtParental.LabelCancel");
                break;
            case SceneAdminPin.STATE_NORMAL:
            case SceneAdminPin.STATE_INVALID:
                txtButton = (String) dataCenter.get("TxtParental.LabelDelete");
                break;
            case SceneAdminPin.STATE_VALID:
                txtButton = (String) dataCenter.get("TxtParental.LabelNext");
                break;
        }
        if (txtButton != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 478-(Rs.FM18.stringWidth(txtButton)/2), 458);
        }
        
        //Label
        g.setFont(Rs.F17);
        g.setColor(Rs.DVB255255255128);
        for (int i=0; i<SceneAdminPin.AREA_COUNT; i++) {
            String txtLable = null;
            if (i == SceneAdminPin.AREA_PIN_CODE) {
                txtLable = (String) dataCenter.get("TxtParental.LabelPinCode");
            } else if (i == SceneAdminPin.AREA_REPEAT_PIN_CODE) {
                txtLable = (String) dataCenter.get("TxtParental.LabelRepeatPinCode");
            }
            if (txtLable != null) {
                g.drawString(txtLable, 458-Rs.FM17.stringWidth(txtLable), 361+(i*areaGap)) ;
            }
        }
        
        //PIN Code
        g.setFont(Rs.F24);
        g.setColor(Rs.C255255255);
        for (int i=0; i<SceneAdminPin.AREA_COUNT; i++) {
            for (int j=0; j<SceneAdminPin.PIN_MAX_LENGTH; j++) {
                Image imgPinBox = null;
                if (i == curArea && j == curIndex && !isButtonArea) {
                    imgPinBox = imgPinBoxF;
                } else {
                    imgPinBox = imgPinBoxB;
                }
                if (imgPinBox != null) {
                    g.drawImage(imgPinBox, 474+(j*pinGap), 341+(i*areaGap), c);
                }
                
                String pinCode = null;
                switch(i) {
                    case SceneAdminPin.AREA_PIN_CODE:
                        pinCode = pinCodes[j];
                        break;
                    case SceneAdminPin.AREA_REPEAT_PIN_CODE:
                        pinCode = rPinCodes[j];
                        break;
                }
                if (pinCode != null) {
                    g.drawString(txtStar, 491+(j*pinGap)-(Rs.FM24.stringWidth(txtStar)/2), 368+(i*areaGap));  
                }
            }
        }
    }
}

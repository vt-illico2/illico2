package com.videotron.tvi.illico.parental.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.ui.SceneIntro;


public class RendererIntro extends Renderer {
    private DataCenter dataCenter = DataCenter.getInstance();
 
    private Image imgShadow = dataCenter.getImage("12_ppop_sha.png");
    private Image imgPop = dataCenter.getImage("12_pop_high_625.png");
    private Image imgTitleLine = dataCenter.getImage("07_respop_title.png");
    private Image imgStepLine = dataCenter.getImage("12_ppop_step.png");
    private Image imgButtonSB = dataCenter.getImage("05_focus_dim.png");
    private Image imgButtonSF = dataCenter.getImage("05_focus.png");
    
    private final int buttonGap = 158;   
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }
    
    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paint(Graphics g, UIComponent c) {
        //Background
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
        if (imgShadow != null) {
            g.drawImage(imgShadow, 170, 483, 625, 57, c);
        }
        g.setColor(Rs.C033033033);
        g.fillRect(170, 51, 625, 435);
        if (imgPop != null) {
            g.drawImage(imgPop, 170, 51, c);
        }
        if (imgTitleLine != null) {
            g.drawImage(imgTitleLine, 197, 89, c);
        }
        if (imgStepLine != null) {
            g.drawImage(imgStepLine, 176, 108, c);
        }
        
        //Button
        SceneIntro scene = (SceneIntro)c;
        int curButton = scene.getCurrentButton();
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        for (int i=0; i<SceneIntro.BUTTON_COUNT; i++) {
            Image imgButtonS = null;
            if (i == curButton) {
                imgButtonS = imgButtonSF; 
            } else {
                imgButtonS = imgButtonSB;
            }
            if (imgButtonS != null) {
                g.drawImage(imgButtonS, 321 + (i*buttonGap), 437, c);
            }
            String txtButtonS = null;
            if (i == SceneIntro.BUTTON_NEXT) {
                txtButtonS = (String) dataCenter.get("TxtParental.LabelNext");
            } else if (i == SceneIntro.BUTTON_CANCEL) {
                txtButtonS = (String) dataCenter.get("TxtParental.LabelCancel");
            }
            if (txtButtonS != null) {
                int txtButtonSWth = Rs.FM18.stringWidth(txtButtonS);
                g.drawString(txtButtonS, 399 + (i*buttonGap) - (txtButtonSWth/2), 458);
            }
        }
    }
}

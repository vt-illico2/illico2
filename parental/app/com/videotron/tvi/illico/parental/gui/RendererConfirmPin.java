package com.videotron.tvi.illico.parental.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.ui.SceneConfirmPin;

public class RendererConfirmPin extends Renderer {
    private DataCenter dataCenter = DataCenter.getInstance();
    
    private Image imgShadow = dataCenter.getImage("12_ppop_sha.png");
    private Image imgPop = dataCenter.getImage("12_pop_high_625.png");
    private Image imgTitleLine = dataCenter.getImage("07_respop_title.png");
    private Image imgStepLine = dataCenter.getImage("12_ppop_step.png");
    private Image imgButtonB = dataCenter.getImage("05_focus_dim.png");
    private Image imgButtonF = dataCenter.getImage("05_focus.png");
    private Image imgPinBoxB = dataCenter.getImage("05_pin_box.png");
    private Image imgPinBoxF = dataCenter.getImage("05_pin_focus.png"); 
    
    private final String txtStar = "*";
    private final int pinGap = 38;
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    
    protected void paint(Graphics g, UIComponent c) {
        //Background
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
        if (imgShadow != null) {
            g.drawImage(imgShadow, 170, 483, 625, 57, c);
        }
        g.setColor(Rs.C033033033);
        g.fillRect(170, 51, 625, 435);
        if (imgPop != null) {
            g.drawImage(imgPop, 170, 51, c);
        }
        if (imgTitleLine != null) {
            g.drawImage(imgTitleLine, 197, 89, c);
        }
        if (imgStepLine != null) {
            g.drawImage(imgStepLine, 176, 108, c);
        }
        
        //Description
        g.setFont(Rs.F18);
        g.setColor(Rs.C229229229);
        String desc0 = (String) dataCenter.get("TxtParental.ConfirmPinDesc0");
        if (desc0 != null) {
            g.drawString(desc0, 480-(Rs.FM18.stringWidth(desc0)/2), 264);
        }
        
        SceneConfirmPin scene = (SceneConfirmPin)c;
        boolean isButtonArea = scene.isButtonArea();
        int curState = scene.getCurrentState();
        int curIndex = scene.getCurrentIndex();
        String[] pinCodes = scene.getPINCodes();
        //Button
        Image imgButton = null;
        if (isButtonArea) {
            imgButton = imgButtonF;
        } else {
            imgButton = imgButtonB;
        }
        if (imgButton != null) {
            g.drawImage(imgButton, 400, 437, c);
        }
        String txtButton = null;
        switch(curState) {
            case SceneConfirmPin.STATE_CANCEL:
                txtButton = (String) dataCenter.get("TxtParental.LabelCancel");
                break;
            case SceneConfirmPin.STATE_NORMAL:
                txtButton = (String) dataCenter.get("TxtParental.LabelDelete");
                break;
            case SceneConfirmPin.STATE_VALID:
                txtButton = (String) dataCenter.get("TxtParental.LabelNext");
                break;
        }
        if (txtButton != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 478-(Rs.FM18.stringWidth(txtButton)/2), 458);
        }
        
        //Label
        String txtLable = (String) dataCenter.get("TxtParental.LabelPinCode");
        if (txtLable != null) {
            g.setFont(Rs.F17);
            g.setColor(Rs.DVB255255255128);
            g.drawString(txtLable, 342, 313) ;
        }
        
        //PIN Code
        g.setFont(Rs.F24);
        g.setColor(Rs.C255255255);
        for (int j=0; j<SceneConfirmPin.PIN_MAX_LENGTH; j++) {
            Image imgPinBox = null;
            if (j == curIndex && !isButtonArea) {
                imgPinBox = imgPinBoxF;
            } else {
                imgPinBox = imgPinBoxB;
            }
            if (imgPinBox != null) {
                g.drawImage(imgPinBox, 474+(j*pinGap), 293, c);
            }
            
            String pinCode = pinCodes[j];
            if (pinCode != null) {
                g.drawString(txtStar, 491+(j*pinGap)-(Rs.FM24.stringWidth(txtStar)/2), 320);  
            }
        }
    }
}

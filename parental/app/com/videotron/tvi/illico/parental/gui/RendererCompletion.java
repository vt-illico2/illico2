package com.videotron.tvi.illico.parental.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.util.TextUtil;

public class RendererCompletion extends Renderer {
    private DataCenter dataCenter = DataCenter.getInstance();

    private Image imgShadow = dataCenter.getImage("12_ppop_sha.png");
    private Image imgPop = dataCenter.getImage("12_pop_high_625.png");
    private Image imgTitleLine = dataCenter.getImage("07_respop_title.png");
    private Image imgStepLine = dataCenter.getImage("12_ppop_step.png");
    private Image imgButtonSF = dataCenter.getImage("05_focus.png");
    
    private final int descWth = 576;
   
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paint(Graphics g, UIComponent c) {
        //Background
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
        if (imgShadow != null) {
            g.drawImage(imgShadow, 170, 483, 625, 57, c);
        }
        g.setColor(Rs.C033033033);
        g.fillRect(170, 51, 625, 435);
        if (imgPop != null) {
            g.drawImage(imgPop, 170, 51, c);
        }
        if (imgTitleLine != null) {
            g.drawImage(imgTitleLine, 197, 89, c);
        }
        if (imgStepLine != null) {
            g.drawImage(imgStepLine, 176, 108, c);
        }
        
        //Description
        g.setFont(Rs.F18);
        g.setColor(Rs.C229229229);
        String desc0 = (String) dataCenter.get("TxtParental.CompletionDesc0");
        if (desc0 != null) {
            String[] desc0s = TextUtil.split(desc0, Rs.FM18, descWth, 2);
            for (int i=0; i<desc0s.length; i++) {
                g.drawString(desc0s[i], 193, 164+(i*20));
            }
        }
        String desc1 = (String) dataCenter.get("TxtParental.CompletionDesc1");
        if (desc1 != null) {
            String[] desc1s = TextUtil.split(desc1, Rs.FM18, descWth, 2);
            for (int i=0; i<desc1s.length; i++) {
                g.drawString(desc1s[i], 193, 204+(i*20));
            }
        }
        String desc2 = (String) dataCenter.get("TxtParental.CompletionDesc2");
        if (desc2 != null) {
            String[] desc2s = TextUtil.split(desc2, Rs.FM18, descWth, 2);
            for (int i=0; i<desc2s.length; i++) {
                g.drawString(desc2s[i], 193, 264+(i*20));
            }
        }
        //Button
        if (imgButtonSF != null) {
            g.drawImage(imgButtonSF, 400, 437, c);
        }
        String txtButton = (String) dataCenter.get("TxtParental.LabelFinish");
        if (txtButton != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 478-(Rs.FM18.stringWidth(txtButton)/2), 458);
        }
    }
}

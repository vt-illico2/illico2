package com.videotron.tvi.illico.parental.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.ui.SceneParentalControl;
import com.videotron.tvi.illico.util.TextUtil;

public class RendererParentalControl extends Renderer {
    private DataCenter dataCenter = DataCenter.getInstance();
    private Image imgShadow = dataCenter.getImage("12_ppop_sha.png");
    private Image imgPop = dataCenter.getImage("12_pop_high_625.png");
    private Image imgTitleLine = dataCenter.getImage("07_respop_title.png");
    private Image imgStepLine = dataCenter.getImage("12_ppop_step.png");
    private Image imgButtonB = dataCenter.getImage("05_focus_dim.png");
    private Image imgButtonF = dataCenter.getImage("05_focus.png");
    private Image imgSelectBoxB = dataCenter.getImage("input_210.png");
    private Image imgSelectBoxF = dataCenter.getImage("input_210_foc2.png");
    
    private final int descWth = 576;
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paint(Graphics g, UIComponent c) {
        //Background
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
        if (imgShadow != null) {
            g.drawImage(imgShadow, 170, 483, 625, 57, c);
        }
        g.setColor(Rs.C033033033);
        g.fillRect(170, 51, 625, 435);
        if (imgPop != null) {
            g.drawImage(imgPop, 170, 51, c);
        }
        if (imgTitleLine != null) {
            g.drawImage(imgTitleLine, 197, 89, c);
        }
        if (imgStepLine != null) {
            g.drawImage(imgStepLine, 176, 108, c);
        }
        g.setColor(Rs.DVB255255255038);
        g.fillRect(191, 234, 585, 1);
        
        //Description
        g.setFont(Rs.F18);
        g.setColor(Rs.C229229229);
        String desc0 = (String) dataCenter.get("TxtParental.ParentalControlDesc0");
        if (desc0 != null) {
            String[] desc0s = TextUtil.split(desc0, Rs.FM18, descWth, 2);
            for (int i=0; i<desc0s.length; i++) {
                g.drawString(desc0s[i], 193, 164+(i*20));
            }
        }
        String desc1 = (String) dataCenter.get("TxtParental.ParentalControlDesc1");
        if (desc1 != null) {
            desc1 = TextUtil.shorten(desc1, Rs.FM18, descWth);
            g.drawString(desc1, 193, 220);
        }
        String label = (String) dataCenter.get("TxtParental.LabelMyChildAge");
        if (label != null) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C255255255);
            g.drawString(label, 355-(Rs.FM24.stringWidth(label)/2), 340);
        }

        SceneParentalControl scene = (SceneParentalControl)c;
        int curArea = scene.getCurrentArea();
        int curState = scene.getCurrentState();
        String txtSelectedValue = (String) dataCenter.get(scene.getSelectedValue());
        //Button
        Image imgButton = null;
        if (curArea == SceneParentalControl.AREA_BUTTON) {
            imgButton = imgButtonF;
        } else {
            imgButton = imgButtonB;
        }
        if (imgButton != null) {
            g.drawImage(imgButton, 400, 437, c);
        }
        String txtButton = null;
        switch(curState) {
            case SceneParentalControl.STATE_VALID:
                txtButton = (String) dataCenter.get("TxtParental.LabelNext");
                break;
            case SceneParentalControl.STATE_INVALID:
                txtButton = (String) dataCenter.get("TxtParental.LabelCancel");
                break;
        }
        if (txtButton != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 478-(Rs.FM18.stringWidth(txtButton)/2), 458);
        }
        //SelectBox
        Image imgSelectBox = null;
        if (curArea == SceneParentalControl.AREA_PARENTAL) {
            imgSelectBox = imgSelectBoxF;
        } else {
            imgSelectBox = imgSelectBoxB;
        }
        if (imgSelectBox != null) {
            g.drawImage(imgSelectBox, 469, 319, c);
        }
        if (txtSelectedValue != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C255255255);
            g.drawString(txtSelectedValue, 483, 340);
        }
    }
}

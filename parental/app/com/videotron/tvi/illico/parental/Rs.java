package com.videotron.tvi.illico.parental;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of main menu.
 * 
 * @version $Revision: 1.2 $ $Date: 2010/09/13 18:37:13 $
 * @author Sangjoon Kwon
 */
public class Rs {
    /*********************************************************************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */
    public static final String APP_VERSION = "0.2.04";
    /** Application Name. */
    public static final String APP_NAME = "Parental";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;
    
    /*********************************************************************************
     * Font-related
     **********************************************************************************/
    public static final Font F17 = FontResource.BLENDER.getFont(17);
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    public static final Font F19 = FontResource.BLENDER.getFont(19);
    public static final Font F20 = FontResource.BLENDER.getFont(20);
    public static final Font F21 = FontResource.BLENDER.getFont(21);
    public static final Font F22 = FontResource.BLENDER.getFont(22);
    public static final Font F24 = FontResource.BLENDER.getFont(24);
    public static final Font F25 = FontResource.BLENDER.getFont(25);
    public static final Font F30 = FontResource.BLENDER.getFont(30);
    public static final Font F32 = FontResource.BLENDER.getFont(32);
    public static final Font F34 = FontResource.BLENDER.getFont(34);
    public static final Font F48 = FontResource.BLENDER.getFont(48);
    
    public static final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    public static final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    public static final FontMetrics FM19 = FontResource.getFontMetrics(F19);
    public static final FontMetrics FM20 = FontResource.getFontMetrics(F20);
    public static final FontMetrics FM21 = FontResource.getFontMetrics(F21);
    public static final FontMetrics FM22 = FontResource.getFontMetrics(F22);
    public static final FontMetrics FM24 = FontResource.getFontMetrics(F24);
    public static final FontMetrics FM25 = FontResource.getFontMetrics(F25);
    public static final FontMetrics FM30 = FontResource.getFontMetrics(F30);
    public static final FontMetrics FM32 = FontResource.getFontMetrics(F32);
    public static final FontMetrics FM34 = FontResource.getFontMetrics(F34);
    public static final FontMetrics FM48 = FontResource.getFontMetrics(F48);
    
    /*********************************************************************************
     * Color-related
     *********************************************************************************/
    public static final Color C003003003 = new Color(3, 3, 3);
    public static final Color C033033033 = new Color(33, 33, 33);
    public static final Color C047047047 = new Color(47, 47, 47);
    public static final Color C078078078 = new Color(78, 78, 78);
    public static final Color C133133133 = new Color(133, 133, 133);
    public static final Color C164164164 = new Color(164, 164, 164);
    public static final Color C171171171 = new Color(171, 171, 171);
    public static final Color C193191191 = new Color(193, 191, 191);
    public static final Color C198198198 = new Color(198, 198, 198);
    public static final Color C214182055 = new Color(214, 182, 55);
    public static final Color C229229229 = new Color(229, 229, 229);
    public static final Color C236236237 = new Color(236, 236, 237);
    public static final Color C250202000 = new Color(250, 202, 0);
    public static final Color C255234160 = new Color(255, 234, 160);
    public static final Color C255255255 = new Color(255, 255, 255);
    
    public static final  Color DVB000000000153 = new DVBColor(0, 0, 0, 153);
    public static final  Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    public static final  Color DVB255255255038 = new DVBColor(255, 255, 255, 38);
    public static final  Color DVB255255255128 = new DVBColor(255, 255, 255, 128);    
    /*********************************************************************************
     * Key Code-related
     *********************************************************************************/
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    public static final int KEY_PAGEUP = OCRcEvent.VK_PAGE_UP;
    public static final int KEY_PAGEDOWN = OCRcEvent.VK_PAGE_DOWN;
    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;
    
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, SCREEN_SIZE_WIDTH, SCREEN_SIZE_HEIGHT);
}

package com.videotron.tvi.illico.parental.controller;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.parental.App;
import com.videotron.tvi.illico.parental.Rs;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.<p>
 * Try to look up UPP Service at the time of initializing this class.
 * If successful, upp data get from the PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @author Sangjoon Kwon
 */
public class PreferenceProxy implements PreferenceListener {
    private static PreferenceProxy instance;

    private static final int UPP_INDEX_COUNT = 2;
    private static final int UPP_INDEX_LANGUAGE = 0;
    private static final int UPP_INDEX_RATING= 1;
    private String[] prefNames;
    private String[] prefValues;
    private PreferenceService prefSvc;

    private String language = Definitions.LANGUAGE_FRENCH;
    private String rating = "";

    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceProxy();
        }
        return instance;
    }

    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() {
        prefNames = new String[UPP_INDEX_COUNT];
        prefNames[UPP_INDEX_LANGUAGE] = PreferenceNames.LANGUAGE;
        prefNames[UPP_INDEX_RATING] = RightFilter.BLOCK_BY_RATINGS;

        prefValues = new String[UPP_INDEX_COUNT];
        prefValues[UPP_INDEX_LANGUAGE] = language;
        prefValues[UPP_INDEX_RATING] = rating;
    }

    /**
     * Dispose PreferenceProxy.
     */
    public void dispose() {
        if (prefSvc != null) {
            try{
                prefSvc.removePreferenceListener(this);
            }catch(Exception ignore) {
            }
            prefSvc = null;
        }
        prefNames = null;
        prefValues = null;
        instance = null;
    }

    /**
     * Start PreferenceProxy.
     */
    public void start() {
    }

    /**
     * Stop PreferenceProxy.
     */
    public void stop() {
    }

    /**
     * Look up User Profile and Preference.
     */
    private PreferenceService getPreferenceService() {
        if (prefSvc == null) {
            try {
                String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
                prefSvc = (PreferenceService) IxcRegistry.lookup(App.xletContext, lookupName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (prefSvc != null) {
                String[] uppData = null;
                try {
                    uppData = prefSvc.addPreferenceListener(this, Rs.APP_NAME, prefNames, prefValues) ;
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
                try {
                    receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[UPP_INDEX_LANGUAGE]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    receiveUpdatedPreference(RightFilter.BLOCK_BY_RATINGS, uppData[UPP_INDEX_RATING]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return prefSvc;
    }

    /**
     * invoked when the value of the preference has changed.
     * @param name Updated Preference Name
     * @param value Updated value of Preference
     * @throws RemoteException the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PreferenceProxy.receiveUpdatedPreference]name:[" + name + "]-value:[" + value
                    + "]");
        }
        if (name == null || value == null) {
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            language = value;
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE , value);
        } else if (name.equals(RightFilter.BLOCK_BY_RATINGS)) {
            rating = value;
        }
    }

    /*********************************************************************************
     * Admin PIN-related
     **********************************************************************************/
    public void setAdminPIN(String rPIN) {
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                pSvc.setAdminPIN(rPIN);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    public boolean existAdminPIN() {
        PreferenceService pSvc = getPreferenceService();
        boolean existAdminPIN = false;
        if (pSvc != null) {
            try{
                existAdminPIN = pSvc.isExistedAdminPIN();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.existAdminPIN]Result - existAdminPIN : "+existAdminPIN);
        }
        return existAdminPIN;
    }
    public boolean checkAdminPIN(String rPIN) {
        PreferenceService pSvc = getPreferenceService();
        boolean checkAdminPIN = false;
        if (pSvc != null) {
            try{
                checkAdminPIN = pSvc.equalsWithAdminPIN(rPIN);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.checkAdminPIN]Result - checkAdminPIN : "+checkAdminPIN);
        }
        return checkAdminPIN;
    }
    /*********************************************************************************
     * Rating-related
     **********************************************************************************/
    public boolean setRatingValue(String rValue) {
        PreferenceService pSvc = getPreferenceService();
        boolean isSuccess = false;
        if (pSvc != null) {
            try{
                isSuccess = pSvc.setPreferenceValue(RightFilter.BLOCK_BY_RATINGS, rValue);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }
    public String getRatingValue() {
        return rating;
    }
}

package com.videotron.tvi.illico.parental.controller;

public class SceneTemplate {
    public static final int SCENE_ID_INVALID = -1;
    public static final int SCENE_ID_INTRO = 0;
    public static final int SCENE_ID_ADMIN_PIN = 1;
    public static final int SCENE_ID_CONFIRM_PIN = 2;
    public static final int SCENE_ID_DEFAULT_PARENTAL_CONTROL = 3;
    public static final int SCENE_ID_COMPLETE = 4;
}

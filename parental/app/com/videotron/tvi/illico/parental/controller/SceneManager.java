package com.videotron.tvi.illico.parental.controller;

import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.ui.Scene;
import com.videotron.tvi.illico.parental.ui.SceneAdminPin;
import com.videotron.tvi.illico.parental.ui.SceneCompletion;
import com.videotron.tvi.illico.parental.ui.SceneConfirmPin;
import com.videotron.tvi.illico.parental.ui.SceneIntro;
import com.videotron.tvi.illico.parental.ui.SceneParentalControl;
import com.videotron.tvi.illico.util.WindowProperty;

public class SceneManager {
    private static SceneManager instance;
    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    private LayeredUI lUIWizard;
    private LayeredWindow lWinWizard;
    private LayeredKeyWizard lKeyWizard;

//    private MonitorService monitorSvc;

    private Hashtable scenes;
    private Scene curScene;
    private int curSceneId;

    public static synchronized SceneManager getInstance() {
        if (instance == null) {
            instance = new SceneManager();
        }
        return instance;
    }

    private SceneManager(){
    }

    public void init() {
        if (lWinWizard == null) {
            lWinWizard = new LayeredWindowWizard();
            lWinWizard.setVisible(true);
        }
        if (lKeyWizard == null) {
            lKeyWizard = new LayeredKeyWizard();
        }
        if (lUIWizard == null) {
            lUIWizard = WindowProperty.PARENTAL_WIZARD.createLayeredDialog(lWinWizard, lWinWizard.getBounds(), lKeyWizard);
            lUIWizard.deactivate();
        }
        if (scenes == null) {
            scenes = new Hashtable();
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
    }

    public void dispose() {
        if (scenes != null) {
            Enumeration enu = scenes.keys();
            while (enu.hasMoreElements()) {
                Integer key = (Integer)enu.nextElement();
                if (key == null) {
                    continue;
                }
                Scene scene = (Scene)scenes.get(key);
                if (scene == null) {
                    continue;
                }
                scene.dispose();
                scene = null;
            }
            scenes = null;
        }
        curScene = null;

        if (lUIWizard != null) {
            lUIWizard.deactivate();
            lUIWizard = null;
        }
        lKeyWizard = null;
        lWinWizard = null;
    }

    public void start() {
//        showParentalWizard();
    }

    public void stop() {
    }

    public void goToNextScene(int reqSceneId, boolean isReset) {
        if (curSceneId == reqSceneId) {
            return;
        }
        //Stop before scene
        if (curScene != null) {
            curScene.stop();
            lWinWizard.remove(curScene);
        }

        //Start current scene
        Scene scene = getScene(reqSceneId);
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.handleKeyEvent]scene : "+scene);
        }
        if (scene != null) {
            scene.start(isReset);
            lWinWizard.add(scene);
            curSceneId = reqSceneId;
            curScene = scene;
        }

        //Repaint();
        lWinWizard.repaint();
    }

    private Scene getScene(int reqSceneId) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.getScene]reqSceneId : "+reqSceneId);
        }
        Scene res = (Scene)scenes.get(new Integer(reqSceneId));
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.getScene]res : "+res);
        }
        //Check Scene
        if (res == null) {
            res = createScene(reqSceneId);
            if (res != null) {
                scenes.put(new Integer(reqSceneId), res);
            }
        }
        return res;
    }

    private Scene createScene(int reqSceneId) {
        Scene scene = null;
        switch(reqSceneId) {
            case SceneTemplate.SCENE_ID_INTRO:
                scene = new SceneIntro();
                break;
            case SceneTemplate.SCENE_ID_ADMIN_PIN:
                scene = new SceneAdminPin();
                break;
            case SceneTemplate.SCENE_ID_CONFIRM_PIN:
                scene = new SceneConfirmPin();
                break;
            case SceneTemplate.SCENE_ID_DEFAULT_PARENTAL_CONTROL:
                scene = new SceneParentalControl();
                break;
            case SceneTemplate.SCENE_ID_COMPLETE:
                scene = new SceneCompletion();
                break;
        }
        if (scene != null) {
            scene.init();
        }
        return scene;
    }

    /*****************************************************************
     * methods - ParentalWizardService-related
     *****************************************************************/
    public void showParentalWizard() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneManager.showParentalWizard]lUIWizard : "+lUIWizard);
        }
        init();
        if (lUIWizard != null) {
            lUIWizard.activate();
        }
        goToNextScene(SceneTemplate.SCENE_ID_INTRO, true);
    }
    public void hideParentalWizard() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneManager.showParentalWizard]lUIWizard : "+lUIWizard);
        }
        dispose();
    }
    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowWizard extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        public LayeredWindowWizard() {
            setBounds(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
        }
        public void notifyShadowed() {
        }
    }
    class LayeredKeyWizard implements LayeredKeyHandler {
        public boolean handleKeyEvent(UserEvent userEvent) {
            if (userEvent == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredKeyWizard.handleKeyEvent]User Event is null.");
                }
                return false;
            }
            if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
                return false;
            }
            if (curScene == null) {
                return false;
            }
            return curScene.handleKey(userEvent.getCode());
        }
    }
}

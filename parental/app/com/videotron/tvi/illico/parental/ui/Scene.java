package com.videotron.tvi.illico.parental.ui;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.communication.ParentalWizardServiceImpl;
import com.videotron.tvi.illico.parental.controller.PreferenceProxy;
import com.videotron.tvi.illico.parental.popup.Popup;
import com.videotron.tvi.illico.parental.popup.PopupAdapter;
import com.videotron.tvi.illico.parental.popup.PopupController;
import com.videotron.tvi.illico.parental.popup.PopupEvent;
import com.videotron.tvi.illico.parental.popup.PopupListener;
import com.videotron.tvi.illico.util.KeyCodes;

abstract public class Scene extends UIComponent {
    private static final long serialVersionUID = 1L;

    public static final int[] STEP_ID_NO_PIN = {
        ComponentStep.STEP_ID_INTRO,
        ComponentStep.STEP_ID_ADMIN_PIN,
        ComponentStep.STEP_ID_DEFAULT_PARENTAL_CONTROL,
        ComponentStep.STEP_ID_COMPLETE,
    };
    public static final int[] STEP_ID_EXIST_PIN = {
        ComponentStep.STEP_ID_INTRO,
        ComponentStep.STEP_ID_DEFAULT_PARENTAL_CONTROL,
        ComponentStep.STEP_ID_COMPLETE,
    };

    private static int[] stepIds;
    private static boolean existAdminPIN;

    /*****************************************************************
     * variables - Popup-related
     *****************************************************************/
    private PopupNotification popNotification;
    private PopupAdapter popAdaptDefault;

    /*****************************************************************
     * variables - TVTimerSpec
     *****************************************************************/
    protected static StringBuffer dateBuffer;

    /*****************************************************************
     * methods - Life Cycle
     *****************************************************************/
    public void init() {
        if (dateBuffer == null) {
            dateBuffer = new StringBuffer();
        }
        if (popAdaptDefault == null) {
            popAdaptDefault = new PopupAdapter() {
                public void popupOK(PopupEvent pe) {
                    PopupController.getInstance().closePopup(popNotification, Scene.this);
                }
            };
        }
        initScene();
    }

    public void dispose() {
        closePopup();
        stopScene();
        disposeScene();
        if (popNotification != null) {
            popNotification.dispose();
            popNotification = null;
        }
        popAdaptDefault=null;
        dateBuffer = null;
    }

    public void start(boolean reset) {
        startScene(reset);
    }

    public void stop() {
        closePopup();
        stopScene();
        if (popNotification != null) {
            popNotification.removePopupListener();
            PopupController.getInstance().closePopup(popNotification, this);
        }
    }

    /*****************************************************************
     * methods - Popup-related
     *****************************************************************/
    private void createPopupInfo(PopupListener l) {
        if (popNotification == null) {
            popNotification = new PopupNotification();
            popNotification.setBounds(0, 0, 960, 540);
        }
        popNotification.setPopupListener(l);
    }

    public void openPopupInfo(String cont) {
        openPopupInfo(popAdaptDefault, null, cont, PopupNotification.POPUP_SIZE_SMALL);
    }

    public void openPopupInfo(String sTitle, String cont) {
        openPopupInfo(popAdaptDefault, sTitle, cont, PopupNotification.POPUP_SIZE_SMALL);
    }

    protected void openPopupInfo(PopupListener l, String cont) {
        openPopupInfo(l, null, cont, PopupNotification.POPUP_SIZE_SMALL);
    }

    protected void openPopupInfo(PopupListener l,String sTitle, String cont, int size) {
        openPopup(l, sTitle, cont, size, PopupNotification.POPUP_TYPE_INFORMATION);
    }

    protected void openPopupQuestion(PopupListener l, String sTitle, String cont) {
        openPopupQuestion(l, sTitle, cont, PopupNotification.POPUP_SIZE_SMALL);
    }

    protected void openPopupQuestion(PopupListener l, String cont) {
        openPopupQuestion(l, null, cont, PopupNotification.POPUP_SIZE_SMALL);
    }

    protected void openPopupQuestion(PopupListener l, String sTitle, String cont, int size) {
        openPopup(l, sTitle, cont, size, PopupNotification.POPUP_TYPE_QUESTION);
    }

    private void openPopup(PopupListener l, String sTitle, String cont, int size, int type) {
        createPopupInfo(l);
        popNotification.setPopupSize(size);
        popNotification.setPopupType(type);
        if (sTitle != null) {
            popNotification.setSubTitle(sTitle);
        }
        popNotification.setContent(cont);
        PopupController.getInstance().openPopup(popNotification, this);
    }

    protected void closePopup() {
        if (popNotification != null) {
            popNotification.removePopupListener();
            PopupController.getInstance().closePopup(popNotification, this);
        }
    }

    /*****************************************************************
     * implement KeyListener
     *****************************************************************/
    public boolean handleKey(int code) {
        switch(code) {
            case KeyCodes.COLOR_A:
            case KeyCodes.COLOR_B:
            case KeyCodes.COLOR_C:
            case KeyCodes.COLOR_D:
            case KeyCodes.LAST:
            case KeyCodes.REPLAY:
            case KeyCodes.VOD:
            case KeyCodes.MENU:
            case KeyCodes.FAV:
            case KeyCodes.REWIND:
            case KeyCodes.FAST_FWD:
            case KeyCodes.PLAY:
            case KeyCodes.PAUSE:
            case KeyCodes.RECORD:
            case KeyCodes.LIVE:
            case KeyCodes.LIST:
            case KeyCodes.PIP:
            case KeyCodes.PIP_MOVE:
            case KeyCodes.WIDGET:
            case KeyCodes.SEARCH:
            case KeyCodes.SETTINGS:
            case KeyCodes.NEXT_DAY:
                return true;
            case Rs.KEY_EXIT:
                ParentalWizardServiceImpl.getInstance().responseCanceled();
//                SceneManager.getInstance().hideParentalWizard();
                return true;
            default :
                boolean isActivePopup = PopupController.getInstance().isActivePopup();
                if (isActivePopup) {
                    Popup activePop = PopupController.getInstance().getLastPopup();
                    return activePop.keyAction(code);
                }
                return keyAction(code);
        }
    }

    public static int[] getStepIds() {
        if (stepIds == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[Scene.getStepIds]start.");
            }
            existAdminPIN = PreferenceProxy.getInstance().existAdminPIN();
            if (existAdminPIN) {
                stepIds = STEP_ID_EXIST_PIN;
            } else {
                stepIds = STEP_ID_NO_PIN;
            }
        }
        return stepIds;
    }

    public static boolean existAdminPIN() {
        return existAdminPIN;
    }
    /*****************************************************************
     * methods - abstract
     *****************************************************************/
    abstract protected void initScene();

    abstract protected void disposeScene();

    abstract protected void startScene(boolean resetScene);

    abstract protected void stopScene();

    abstract protected boolean keyAction(int keyCode);
}

package com.videotron.tvi.illico.parental.ui;

import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.communication.ParentalWizardServiceImpl;
import com.videotron.tvi.illico.parental.gui.RendererCompletion;

public class SceneCompletion extends Scene {
    private static final long serialVersionUID = 1719247830341282139L;
    private ComponentStep compStep;
    private ComponentTitle compTitle;
    
    protected void initScene() {
        if (renderer == null) {
            renderer = new RendererCompletion();
            setRenderer(renderer);
        }
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(240, 50, 480, 40);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setStepIds(getStepIds());
            compStep.setCurrentStep(ComponentStep.STEP_ID_COMPLETE);
            compStep.setBounds(170, 100, 625, 29);
            add(compStep);
        } 
    }
    
    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle = null;
        }
        renderer = null; 
    }

    protected void startScene(boolean resetScene) {
        if (resetScene) {
        }
        prepare();   
    }

    protected void stopScene() {
    }
    
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_LEFT:
            case Rs.KEY_RIGHT:
                return true;
            case Rs.KEY_OK:
//                SceneManager.getInstance().hideParentalWizard();
                ParentalWizardServiceImpl.getInstance().responseCompleted();
                return true;
        }   
        return false;
    }
}

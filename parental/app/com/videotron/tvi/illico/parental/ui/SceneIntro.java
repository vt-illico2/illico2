package com.videotron.tvi.illico.parental.ui;

import java.awt.Color;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.communication.ParentalWizardServiceImpl;
import com.videotron.tvi.illico.parental.controller.SceneManager;
import com.videotron.tvi.illico.parental.controller.SceneTemplate;
import com.videotron.tvi.illico.parental.gui.RendererIntro;

public class SceneIntro extends Scene {
    private static final long serialVersionUID = -8319423869576097504L;
    private DataCenter dataCenter = DataCenter.getInstance();
    
    private ComponentStep compStep;
    private ComponentTitle compTitle;
    private ComponentScrollText compSTexts;
    
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_NEXT = 0;
    public static final int BUTTON_CANCEL = 1;
    private int curButton;
    
    protected void initScene() {
        if (renderer == null) {
            renderer = new RendererIntro();
            setRenderer(renderer);
        }
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(240, 50, 480, 40);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setStepIds(getStepIds());
            compStep.setCurrentStep(ComponentStep.STEP_ID_INTRO);
            compStep.setBounds(170, 100, 625, 29);
            add(compStep);
        }
        if (compSTexts == null) {
            compSTexts = new ComponentScrollText();
            compSTexts.setBounds(192, 162, 556, 266);
            compSTexts.setFont(Rs.F18);
            compSTexts.setForeground(Color.white);
            compSTexts.setRows(13);
            compSTexts.setRowHeight(20);
            String desc1 = (String) dataCenter.get("TxtParental.ParentalControlDesc1");
            desc1 = desc1 + desc1+ desc1+ desc1+ desc1+desc1+desc1+desc1 + desc1+ desc1+ desc1+ desc1+desc1+desc1+desc1 + desc1+ desc1+ desc1+ desc1+desc1+desc1;
            compSTexts.setContents(desc1);
            add(compSTexts);
        }
    }
    
    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle = null;
        }
        if (compSTexts!= null) {
            remove(compSTexts);
            compSTexts = null;
        }
        renderer = null;
    }

    protected void startScene(boolean isReset) {
        if (isReset) {
            curButton = BUTTON_NEXT;
        }
        prepare();
    }

    protected void stopScene() {
    }
    
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_PAGEUP:
            case Rs.KEY_UP:
                if (compSTexts != null) {
                    compSTexts.showPreviousPage();
                    repaint();
                }
                break;
            case Rs.KEY_PAGEDOWN:
            case Rs.KEY_DOWN:
                if (compSTexts != null) {
                    compSTexts.showNextPage();
                    repaint();
                }
                break;
            case Rs.KEY_LEFT:
                if (curButton == BUTTON_NEXT) {
                    return true;
                }
                curButton = BUTTON_NEXT;
                repaint();
                return true;
            case Rs.KEY_RIGHT:
                if (curButton == BUTTON_CANCEL) {
                    return true;
                }
                curButton = BUTTON_CANCEL;
                repaint();
                return true;
            case Rs.KEY_OK:
                switch(curButton) {
                    case BUTTON_NEXT:
                        if (existAdminPIN()) {
                            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_CONFIRM_PIN, true);
                        } else {
                            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_ADMIN_PIN, true);
                        }
                        return true;
                    case BUTTON_CANCEL:
//                        SceneManager.getInstance().hideParentalWizard();
                        ParentalWizardServiceImpl.getInstance().responseCanceled();
                        return true;
                }
                return true;
        }
        return false;
    }
    
    public int getCurrentButton() {
        return curButton;
    }
}

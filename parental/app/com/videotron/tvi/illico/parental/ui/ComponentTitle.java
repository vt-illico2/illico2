package com.videotron.tvi.illico.parental.ui;

import java.awt.Component;
import java.awt.Graphics;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.parental.Rs;

public class ComponentTitle extends Component {
    private static final long serialVersionUID = 1L;
    
    public void paint(Graphics g) {
        String txtParental = (String) DataCenter.getInstance().get("TxtParental.ParentalContWiz");
        if (txtParental != null) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C250202000);
            g.drawString(txtParental, (getWidth()/2)-(Rs.FM24.stringWidth(txtParental)/2), 28);
        }
        
        //Version
        g.setFont(Rs.F17);
        g.setColor(Rs.C255255255);
        String data = "Ver v"+Rs.APP_VERSION;
        g.drawString(data, getWidth()-Rs.FM17.stringWidth(data), 28);
    }
}

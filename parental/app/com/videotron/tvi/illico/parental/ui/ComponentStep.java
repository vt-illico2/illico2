package com.videotron.tvi.illico.parental.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.parental.Rs;

public class ComponentStep extends Component {
    private static final long serialVersionUID = 1L;
    private DataCenter dataCenter = DataCenter.getInstance();

    private static final int STEP_ID_COUNT = 4;
    public static final int STEP_ID_INTRO = 0;
    public static final int STEP_ID_ADMIN_PIN = 1;
    public static final int STEP_ID_DEFAULT_PARENTAL_CONTROL = 2;
    public static final int STEP_ID_COMPLETE = 3;
    
    private int curStep;
    
    public static final int STEP_GAP = 4;

    private Image imgStepSeparator;
    private Image[] imgStepNumsF;
    private Image[] imgStepNumsB;
    private String[] txtKeySteps;
    private int[] stepIds;

    public ComponentStep() {
        if (imgStepSeparator == null) {
            imgStepSeparator = dataCenter.getImage("12_sep_s.png");
        }
        if (imgStepNumsF == null) {
            imgStepNumsF = new Image[STEP_ID_COUNT];
            imgStepNumsF[STEP_ID_INTRO] = dataCenter.getImage("12_1_foc.png");
            imgStepNumsF[STEP_ID_ADMIN_PIN] = dataCenter.getImage("12_2_foc.png");
            imgStepNumsF[STEP_ID_DEFAULT_PARENTAL_CONTROL] = dataCenter.getImage("12_3_foc.png");
            imgStepNumsF[STEP_ID_COMPLETE] = dataCenter.getImage("12_4_foc.png");
        }
        if (imgStepNumsB == null) {
            imgStepNumsB = new Image[STEP_ID_COUNT];
            imgStepNumsB[STEP_ID_INTRO] = dataCenter.getImage("12_1_s.png");
            imgStepNumsB[STEP_ID_ADMIN_PIN] = dataCenter.getImage("12_2_s.png");
            imgStepNumsB[STEP_ID_DEFAULT_PARENTAL_CONTROL] = dataCenter.getImage("12_3_s.png");
            imgStepNumsB[STEP_ID_COMPLETE] = dataCenter.getImage("12_4_s.png");
        }
        if (txtKeySteps == null) {
            txtKeySteps = new String[STEP_ID_COUNT];
            txtKeySteps[STEP_ID_INTRO] = "TxtParental.Step0";
            txtKeySteps[STEP_ID_ADMIN_PIN] = "TxtParental.Step1";
            txtKeySteps[STEP_ID_DEFAULT_PARENTAL_CONTROL] = "TxtParental.Step2";
            txtKeySteps[STEP_ID_COMPLETE] = "TxtParental.Step3";
        }
    }

    public void paint(Graphics g) {
        if (stepIds == null) {
            return;
        }
        //calculate Step Width
        int stepW = 0;
        for (int i = 0; i < stepIds.length; i++) {
            String txtStep = (String) dataCenter.get(txtKeySteps[stepIds[i]]);
            if (txtStep == null) {
                txtStep = "";
            }
            FontMetrics fmStep = null;
            if (stepIds[i] == curStep) {
                if (imgStepNumsF[i] != null) {
                    stepW += imgStepNumsF[i].getWidth(this);
                    stepW += STEP_GAP;
                }
                fmStep = Rs.FM25;
            } else {
                if (imgStepNumsB[i] != null) {
                    stepW += imgStepNumsB[i].getWidth(this);
                    stepW += STEP_GAP;
                }
                fmStep = Rs.FM20;
            }
            stepW += fmStep.stringWidth(txtStep);
            if (i == stepIds.length-1) {
                continue;
            }
            stepW += STEP_GAP;
            if (imgStepSeparator != null) {
                stepW += imgStepSeparator.getWidth(this);
            }
            stepW += STEP_GAP;
        }
        //draw Step
        int xPos = (getWidth()/2) - (stepW/2);
        for (int i = 0; i < stepIds.length; i++) {
            String txtStep = (String) dataCenter.get(txtKeySteps[stepIds[i]]);
            if (txtStep == null) {
                txtStep = "";
            }
            Color cStep = null;
            Font fStep = null;
            FontMetrics fmStep = null;
            if (stepIds[i] == curStep) {
                if (imgStepNumsF[i] != null) {
                    g.drawImage(imgStepNumsF[i], xPos, 0, this);
                    xPos += imgStepNumsF[i].getWidth(this);
                    xPos += STEP_GAP;
                }
                cStep = Rs.C250202000;
                fStep = Rs.F25;
                fmStep = Rs.FM25;
            } else {
                if (imgStepNumsB[i] != null) {
                    g.drawImage(imgStepNumsB[i], xPos, 4, this);
                    xPos += imgStepNumsB[i].getWidth(this);
                    xPos += STEP_GAP;
                }
                cStep = Rs.C133133133;
                fStep = Rs.F20;
                fmStep = Rs.FM20;
            }
            g.setFont(fStep);
            g.setColor(cStep);
            g.drawString(txtStep, xPos, 22);
            if (i == stepIds.length-1) {
                continue;
            }
            xPos += fmStep.stringWidth(txtStep);
            xPos += STEP_GAP;
            if (imgStepSeparator != null) {
                g.drawImage(imgStepSeparator, xPos, 6, this);
                xPos += imgStepSeparator.getWidth(this);
            }
            xPos += STEP_GAP;
        }
    }
    
    public void setCurrentStep(int currentStep) {
        curStep = currentStep;
    }
    
    public void setStepIds(int[] stepIds) {
        this.stepIds = stepIds;
    }
}

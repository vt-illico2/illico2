package com.videotron.tvi.illico.parental.ui;

import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.communication.ParentalWizardServiceImpl;
import com.videotron.tvi.illico.parental.controller.PreferenceProxy;
import com.videotron.tvi.illico.parental.controller.SceneManager;
import com.videotron.tvi.illico.parental.controller.SceneTemplate;
import com.videotron.tvi.illico.parental.gui.RendererParentalControl;
import com.videotron.tvi.illico.parental.popup.PopupAdapter;
import com.videotron.tvi.illico.parental.popup.PopupController;
import com.videotron.tvi.illico.parental.popup.PopupEvent;

public class SceneParentalControl extends Scene  {
    private static final long serialVersionUID = 6088398007392057171L;
    private ComponentStep compStep;
    private ComponentTitle compTitle;
    private PopupParentalControl popParentalCont;
    private PopupAdapter popAdaptParentalCont;
    
    public static final int STATE_INVALID = 0;
    public static final int STATE_VALID = 1;
    private int curState;
    
    public static final int AREA_PARENTAL = 0;
    public static final int AREA_BUTTON = 1;
    private int curArea;
    
    private static final String defaultSelectedValue = "TxtParental.LabelSelect";
    private int selectedIndex;
    private String selectedValue;
    
    protected void initScene() {
        if (renderer == null) {
            renderer = new RendererParentalControl();
            setRenderer(renderer);
        }
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(240, 50, 480, 40);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setStepIds(getStepIds());
            compStep.setCurrentStep(ComponentStep.STEP_ID_DEFAULT_PARENTAL_CONTROL);
            compStep.setBounds(170, 100, 625, 29);
            add(compStep);
        }  
    }
    
    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle = null;
        }
        renderer = null;
    }

    protected void startScene(boolean resetScene) {
        if (resetScene) {
            selectedValue = null;
            curState = STATE_INVALID;
            selectedIndex = PopupParentalControl.getRatingIndex(PreferenceProxy.getInstance().getRatingValue());
            if (selectedIndex != -1) {
                try{
                    selectedValue = PopupParentalControl.getRatingName(selectedIndex);
                    curState = STATE_VALID;
                }catch(Exception e) {
                    selectedValue = null;
                    curState = STATE_INVALID;
                }
            } else {
                selectedIndex = 0;
                selectedValue = null;
                curState = STATE_INVALID;
            }
        }
        prepare();
    }

    protected void stopScene() {
    }
    
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_UP:
                if (curArea == AREA_PARENTAL) {
                    return true;
                }
                curArea = AREA_PARENTAL;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (curArea == AREA_BUTTON) {
                    return true;
                }
                curArea = AREA_BUTTON;
                repaint();       
                return true;
//            case KeyCodes.LAST:
//                if (existAdminPIN()) {
//                    SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_INTRO, false);
//                } else {
//                    SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_ADMIN_PIN, false);
//                }
//                return true;
            case Rs.KEY_OK:
                switch(curArea) {
                    case AREA_PARENTAL:
                        if (popParentalCont == null) {
                            popParentalCont = new PopupParentalControl();
                            popParentalCont.setBounds(0, 0, 960, 540);
                        }
                        popParentalCont.setPopupListener(getPopupAdapterParentalControl());
                        popParentalCont.setCurrentIndex(selectedIndex);
                        PopupController.getInstance().openPopup(popParentalCont, this);
                        break;
                    case AREA_BUTTON:
                        switch(curState) {
                            case STATE_INVALID:
//                                SceneManager.getInstance().hideParentalWizard();
                                ParentalWizardServiceImpl.getInstance().responseCanceled();
                                break;
                            case STATE_VALID:
                                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_COMPLETE, true);
                                break;
                        }
                        break;
                }
                return true;
        }   
        return false;
    }
    
    public PopupAdapter getPopupAdapterParentalControl() {
        if (popAdaptParentalCont == null) {
            popAdaptParentalCont = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupParentalControl pop = (PopupParentalControl)popupEvent.getPopup();
                    selectedIndex = pop.getCurrentIndex();
                    try{
                        selectedValue = PopupParentalControl.getRatingName(selectedIndex);
                    }catch(Exception e) {
                        selectedValue = null;
                    }
                    if (selectedValue != null) {
                        curState = STATE_VALID;
                    }
                    boolean isSuccess = false;
                    try{
                        String ratingValue = PopupParentalControl.getRatingValue(selectedIndex);
                        isSuccess = PreferenceProxy.getInstance().setRatingValue(ratingValue);
                    }catch(Exception e){
                        isSuccess = false;
                    }
                    if (!isSuccess) {
                        openPopupInfo("Invalid Parent Control Value", "aflkdsjalfjkasdlfj");
                    }
                    curArea = AREA_BUTTON;
                    popParentalCont.removePopupListener();
                    PopupController.getInstance().closePopup(popParentalCont, SceneParentalControl.this);
                }
                public void popupCancel(PopupEvent popupEvent) {
                    popParentalCont.removePopupListener();
                    PopupController.getInstance().closePopup(popParentalCont, SceneParentalControl.this);
                }
            };
        }
        return popAdaptParentalCont;
    }
    
    public int getCurrentState() {
        return curState;
    }
    
    public int getCurrentArea() {
        return curArea;
    }
    
    public String getSelectedValue() {
        if (selectedValue == null) {
            return defaultSelectedValue;
        }
        return selectedValue;
    }
}

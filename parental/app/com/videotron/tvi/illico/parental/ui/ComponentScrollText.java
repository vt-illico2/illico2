package com.videotron.tvi.illico.parental.ui;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class ComponentScrollText extends UIComponent {
    private static final long serialVersionUID = 8197519571951L;

    private DataCenter dataCenter = DataCenter.getInstance();

    private FontMetrics fontMetrics;
    private int rowCount = 3;
    private int rowHeight = 20;
    private int currentPage = 0;
    private int totalPage = 1;
    private String[] texts;

    public ComponentScrollText() {
        setRenderer(new ScrollTextsRenderer());
    }

    public void setFont(Font f) {
        super.setFont(f);
        fontMetrics = FontResource.getFontMetrics(f);
    }

    /** Sets the number of rows for this ScrollTexts. */
    public void setRows(int rows) {
        rowCount = rows;
    }

    public void setRowHeight(int height) {
        rowHeight = height;
    }
    
    public void showFirstPage() {
        if (currentPage > 0) {
                currentPage = 0;
                repaint();
        }
    }

    public void showNextPage() {
        if (currentPage < totalPage - 1) {
            currentPage++;
            repaint();
        }
    }

    public void showPreviousPage() {
        if (currentPage > 0) {
            currentPage--;
            repaint();
        }
    }

    public void setContents(String str) {
        texts = TextUtil.split(str, fontMetrics, getWidth() - 15);
        totalPage = ((texts.length - 1) / rowCount) + 1;
        currentPage = 0;
    }

    private class ScrollTextsRenderer extends Renderer {
        private Image[] iBackground = {
            dataCenter.getImage("scrbg_up.png"),
            dataCenter.getImage("scrbg_m.png"),
            dataCenter.getImage("scrbg_dn.png")
        };

        private Image[] iScroll = {
            dataCenter.getImage("scr_up.png"),
            dataCenter.getImage("scr_m.png"),
            dataCenter.getImage("scr_dn.png")
        };

        private Image[] iDimmed = {
            dataCenter.getImage("scr_up_dim.png"),
            dataCenter.getImage("scr_m_dim.png"),
            dataCenter.getImage("scr_dn_dim.png")
        };
        
        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        public void prepare(UIComponent c) {
        }

        public void paint(Graphics g, UIComponent c) {
            if (texts == null) {
                return;
            }
            g.setColor(getForeground());
            Font font = g.getFont();
            g.setFont(font);
            int index = currentPage * rowCount;
            int y = font.getSize();
            for (int i = 0; i < rowCount && index < texts.length; i++) {
                g.drawString(texts[index], 1, y);
                index++;
                y = y + rowHeight;
            }

            if (totalPage > 1) {
                
                int width = c.getWidth();
                int height = c.getHeight();
                
                int x = width - 7;
                g.drawImage(iBackground[0], x, 0, c);
                g.drawImage(iBackground[1], x, 10, 5, height - 20, c);
                g.drawImage(iBackground[2], x, height - 10, c);

                Image[] images;
                if (isEnabled()) {
                    images = iScroll;
                } else {
                    images = iDimmed;
                }
                x = width - 9;
                int barHeight = Math.max(30, height / totalPage);
                y = Math.min(currentPage * barHeight, height - barHeight);
                g.drawImage(images[0], x, y, c);
                g.drawImage(images[1], x, y + 20, 9, barHeight - 30, c);
                g.drawImage(images[2], x, y + barHeight - 10, c);
            }
        }
    }
}


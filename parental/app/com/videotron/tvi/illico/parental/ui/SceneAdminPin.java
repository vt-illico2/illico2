package com.videotron.tvi.illico.parental.ui;

import java.awt.event.KeyEvent;

import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.communication.ParentalWizardServiceImpl;
import com.videotron.tvi.illico.parental.controller.PreferenceProxy;
import com.videotron.tvi.illico.parental.controller.SceneManager;
import com.videotron.tvi.illico.parental.controller.SceneTemplate;
import com.videotron.tvi.illico.parental.gui.RendererAdminPin;

public class SceneAdminPin extends Scene {
    private static final long serialVersionUID = -8481153590286618987L;
    private ComponentStep compStep;
    private ComponentTitle compTitle;
    
    public static final int STATE_CANCEL = 0;
    public static final int STATE_NORMAL = 1;
    public static final int STATE_VALID = 2;
    public static final int STATE_INVALID = 3;
    private int curState;
    
    public static final int AREA_COUNT = 2;
    public static final int AREA_PIN_CODE = 0;
    public static final int AREA_REPEAT_PIN_CODE = 1;
    private int curArea;
    private int curIndex;
    public static final int PIN_MAX_LENGTH = 4;
    private String[] pinCodes;
    private String[] rPinCodes;
    private StringBuffer buffer = new StringBuffer();
    private boolean isButtonArea;
    
    protected void initScene() {
        if (renderer == null) {
            renderer = new RendererAdminPin();
            setRenderer(renderer);
        }
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(240, 50, 480, 40);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setStepIds(getStepIds());
            compStep.setCurrentStep(ComponentStep.STEP_ID_ADMIN_PIN);
            compStep.setBounds(170, 100, 625, 29);
            add(compStep);
        } 
    }
    
    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle = null;
        }
        renderer = null;  
    }

    protected void startScene(boolean resetScene) {
        if (resetScene) {
            curArea = AREA_PIN_CODE;
            curIndex = 0;
            curState = STATE_CANCEL;
            pinCodes = new String[PIN_MAX_LENGTH];
            rPinCodes = new String[PIN_MAX_LENGTH];
            isButtonArea = false;
        }
        prepare();  
    }

    protected void stopScene() {
        
    }

    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
                if (isButtonArea) {
                    return true;
                }
                String inputData = String.valueOf((keyCode - KeyEvent.VK_0));
                if (curArea == AREA_PIN_CODE) {
                    pinCodes[curIndex] = inputData;
                } else if (curArea == AREA_REPEAT_PIN_CODE) {
                    rPinCodes[curIndex] = inputData;
                }
                if (curArea == AREA_REPEAT_PIN_CODE && curIndex == PIN_MAX_LENGTH-1) {
                    String pinCode = getPINCode(AREA_PIN_CODE);
                    String rPinCode = getPINCode(AREA_REPEAT_PIN_CODE);
                    if (pinCode.equals(rPinCode)) {
                        curState = STATE_VALID;
                    } else {
                        curState = STATE_INVALID;
                    }
                    isButtonArea = true;
                } else if (curArea == AREA_PIN_CODE && curIndex == PIN_MAX_LENGTH-1){
                    curArea = AREA_REPEAT_PIN_CODE;
                    curIndex = 0;
                } else {
                    curIndex ++;
                    curState = STATE_NORMAL;
                }
                repaint();
                return true;
            case Rs.KEY_UP:
                if (!isButtonArea) {
                    return true;
                }
                isButtonArea = false;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (isButtonArea) {
                    return true;  
                }
                isButtonArea = true;  
                repaint();
                return true;
//            case KeyCodes.LAST:
//                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_INTRO, false);
//                return true;
            case Rs.KEY_OK:
                if (!isButtonArea) {
                    return true;
                }
                switch(curState) {
                    case STATE_CANCEL:
//                        SceneManager.getInstance().hideParentalWizard();
                        ParentalWizardServiceImpl.getInstance().responseCanceled();
                        break;
                    case STATE_VALID:
                        String pinCode = getPINCode(AREA_PIN_CODE);
                        PreferenceProxy.getInstance().setAdminPIN(pinCode);
                        SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_DEFAULT_PARENTAL_CONTROL, true);
                        break;
                    case STATE_INVALID:
                        curState = STATE_NORMAL;
                        rPinCodes[curIndex] = null;
                        repaint();
                        break;
                    case STATE_NORMAL:
                        if (curArea == AREA_REPEAT_PIN_CODE && curIndex == 0) {
                            curArea = AREA_PIN_CODE;
                            curIndex = PIN_MAX_LENGTH-1;
                        } else if (curArea == AREA_PIN_CODE && curIndex == 0){
                            curArea = AREA_PIN_CODE;
                            curIndex = 0;
                        } else {
                            curIndex --;
                        }
                        if (curArea == AREA_PIN_CODE) {
                            pinCodes[curIndex] = null;
                        } else if (curArea == AREA_REPEAT_PIN_CODE) {
                            rPinCodes[curIndex] = null;
                        }
                        if (curArea == AREA_PIN_CODE && curIndex == 0) {
                            curState = STATE_CANCEL;
                        } else {
                            curState = STATE_NORMAL;
                        }
                        repaint();
                        break;
                }
                return true;
        }
        return false;
    }
    
    private String getPINCode(int reqArea) {
        String[] target = null;
        switch(reqArea) {
            case AREA_PIN_CODE:
                target = pinCodes;
                break;
            case AREA_REPEAT_PIN_CODE:
                target = rPinCodes;
                break;
        }
        buffer.setLength(0);
        for (int i=0; i<target.length; i++) {
            if (target[i] != null) {
                buffer.append(target[i]);    
            }
        }
        return buffer.toString();
    }
    
    public boolean isButtonArea() {
        return isButtonArea;
    }
    
    public int getCurrentState() {
        return curState;
    }
    
    public int getCurrentArea() {
        return curArea;
    }
    
    public int getCurrentIndex() {
        return curIndex;
    }
    
    public String[] getPINCodes() {
        return pinCodes;
    }
    
    public String[] getRepeatPINCodes() {
        return rPinCodes;
    }
}

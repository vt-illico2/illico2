package com.videotron.tvi.illico.parental.ui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.popup.Popup;
import com.videotron.tvi.illico.parental.popup.PopupEvent;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class PopupNotification extends Popup {
    private static final long serialVersionUID = 5911355373218833154L;
    private DataCenter dataCenter = DataCenter.getInstance();

    public static final int POPUP_SIZE_BIG = 0;
    public static final int POPUP_SIZE_SMALL = 1;
    private static final int[] POPUP_SIZE_VALID_W = {370, 318};
    private static final int[] POPUP_SIZE_MAX_LINE = {6, 4};
    private static final int[][] POPUP_SIZE_Y_POS = {{288, 280, 272, 264, 256, 248}, {266, 258, 250, 242}};
    private static final int CONTENT_GAP = 19;
    private int curPopSize = POPUP_SIZE_BIG;


    public static final int POPUP_TYPE_QUESTION = 0;
    public static final int POPUP_TYPE_INFORMATION = 1;
    private int curPopType = POPUP_TYPE_INFORMATION;

    private final int buttonGap = 157;
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_OK = 0;
    public static final int BUTTON_CANCEL = 1;
    private int curButton;

    private String subTitle;
    private String content;

    private Image imgPopRedHaloBig;
    private Image imgPopRedHaloSmall;
    private Image imgPopBGBig;
    private Image imgPopBGSmall;
    private Image imgPopGap;
    private Image imgPopShadow;
    private Image imgPopIcon;
    private Image imgButtonB;
    private Image imgButtonF;

    public PopupNotification() {
        imgPopRedHaloBig = dataCenter.getImage("pop_red_538.png");
        imgPopRedHaloSmall = dataCenter.getImage("pop_red_478.png");
        imgPopBGBig = dataCenter.getImage("pop_high_402.png");
        imgPopBGSmall = dataCenter.getImage("pop_high_350.png");
        imgPopGap = dataCenter.getImage("pop_gap_379.png");
        imgPopShadow = dataCenter.getImage("pop_sha.png");
        imgPopIcon = dataCenter.getImage("icon_noti_red.png");
        imgButtonB = dataCenter.getImage("05_focus_dim.png");
        imgButtonF = dataCenter.getImage("05_focus.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void disposePopup() {
        if (imgPopRedHaloBig != null) {
            dataCenter.removeImage("pop_red_538.png");
            imgPopRedHaloBig = null;
        }
        if (imgPopRedHaloSmall != null) {
            dataCenter.removeImage("pop_red_478.png");
            imgPopRedHaloSmall = null;
        }
        if (imgPopBGBig != null) {
            dataCenter.removeImage("pop_high_402.png");
            imgPopBGBig = null;
        }
        if (imgPopBGSmall != null) {
            dataCenter.removeImage("pop_high_350.png");
            imgPopBGSmall = null;
        }
        if (imgPopGap != null) {
            dataCenter.removeImage("pop_gap_379.png");
            imgPopGap = null;
        }
        if (imgPopShadow != null) {
            dataCenter.removeImage("pop_sha.png");
            imgPopShadow = null;
        }
        if (imgPopIcon != null) {
            dataCenter.removeImage("icon_noti_red.png");
            imgPopIcon = null;
        }
        if (imgButtonB != null) {
            dataCenter.removeImage("05_focus_dim.png");
            imgButtonB = null;
        }
        if (imgButtonF != null) {
            dataCenter.removeImage("05_focus.png");
            imgButtonF = null;
        }
    }

    protected void startPopup() {
        curButton = BUTTON_OK;
    }

    protected void stopPopup() {
        curPopSize = POPUP_SIZE_BIG;
        curPopType = POPUP_TYPE_INFORMATION;
    }

    public void paint(Graphics g) {
        // Backgound
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
        switch(curPopSize) {
            case POPUP_SIZE_BIG:
                if (imgPopRedHaloBig != null) {
                    g.drawImage(imgPopRedHaloBig, 216, 64, this);
                }
                if (imgPopShadow != null) {
                    g.drawImage(imgPopShadow, 280, 415, 404, 79, this);
                }
                g.setColor(Rs.C033033033);
                g.fillRect(280, 143, 402, 276);
                if (imgPopBGBig != null) {
                    g.drawImage(imgPopBGBig, 280, 143, this);
                }
                if (imgPopGap != null) {
                    g.drawImage(imgPopGap, 292, 181, this);
                }
                break;
            case POPUP_SIZE_SMALL:
                if (imgPopRedHaloSmall != null) {
                    g.drawImage(imgPopRedHaloSmall, 242, 79, this);
                }
                if (imgPopShadow != null) {
                    g.drawImage(imgPopShadow, 306, 364, 350, 79, this);
                }
                g.setColor(Rs.C033033033);
                g.fillRect(306, 143, 350, 224);
                if (imgPopBGSmall != null) {
                    g.drawImage(imgPopBGSmall, 306, 143, this);
                }
                if (imgPopGap != null) {
                    g.drawImage(imgPopGap, 285, 181, this);
                }
                break;
        }
        //Icon
        final int titleGap = 6;
        int titleWth = 0;
        int imgPopIconWth = 0;
        if (imgPopIcon !=null) {
            imgPopIconWth = imgPopIcon.getWidth(this);
            titleWth += imgPopIconWth;
            titleWth += titleGap;
        }
        int txtTitleWth = 0;
        String txtTitle = (String) dataCenter.get("TxtParental.LabelNotification");
        if (txtTitle != null) {
            txtTitleWth = Rs.FM24.stringWidth(txtTitle);
            titleWth += txtTitleWth;
        }
        int xPos = 480 - (titleWth/2);
        if (imgPopIcon !=null) {
            g.drawImage(imgPopIcon, xPos, 153, this) ;
            xPos += imgPopIconWth;
            xPos += titleGap;
        }
        if (txtTitleWth > 0) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C250202000);
            g.drawString(txtTitle, xPos, 169);
        }

        //Button
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        int yPos = 318;
        if (curPopSize == POPUP_SIZE_BIG) {
            yPos = 368;
        }
        switch(curPopType) {
            case POPUP_TYPE_QUESTION:
                for (int i=0; i<BUTTON_COUNT; i++) {
                    Image imgButton = null;
                    if (i == curButton) {
                        imgButton = imgButtonF;
                    } else {
                        imgButton = imgButtonB;
                    }
                    if (imgButton != null) {
                        g.drawImage(imgButton, 325+(i*buttonGap), yPos, this);
                    }
                    String txtButton = null;
                    if (i == BUTTON_OK) {
                        txtButton = (String) dataCenter.get("TxtWizard.Confirm");
                    } else if (i == BUTTON_CANCEL){
                        txtButton = (String) dataCenter.get("TxtWizard.Cancel");
                    }
                    if (txtButton != null) {
                        int txtButtonWth = Rs.FM18.stringWidth(txtButton);
                        g.drawString(txtButton, 403+(i*buttonGap)-(txtButtonWth/2), yPos +22);
                    }
                }
                break;
            case POPUP_TYPE_INFORMATION:
                g.drawImage(imgButtonF, 403, yPos, this);
                String txtOK = "OK";
                int txtOkWth = Rs.FM18.stringWidth(txtOK);
                g.drawString(txtOK, 480 -(txtOkWth/2), yPos+22);
                break;
        }

        //SubTitle
        if (subTitle != null) {
            int subTitleWth = Rs.FM19.stringWidth(subTitle);
            if (subTitleWth > POPUP_SIZE_VALID_W[curPopSize]) {
                subTitle = TextUtil.shorten(subTitle, Rs.FM19, POPUP_SIZE_VALID_W[curPopSize]);
                subTitleWth = POPUP_SIZE_VALID_W[curPopSize];
            }
            g.setFont(Rs.F19);
            g.setColor(Rs.C255234160);
            g.drawString(subTitle, 480 - (subTitleWth/2), 214);
        }

        //Content
        if (content != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C229229229);
            String[] conts = TextUtil.split(content, Rs.FM18, POPUP_SIZE_VALID_W[curPopSize], POPUP_SIZE_MAX_LINE[curPopSize]);
            int contsCnt = conts.length;
            if (Log.DEBUG_ON) {
                Log.printDebug("[Popup.paint]curPopSize : "+curPopSize);
                Log.printDebug("[Popup.paint]contsCnt : "+contsCnt);
            }
            int yCont = POPUP_SIZE_Y_POS[curPopSize][contsCnt-1];
            if (subTitle == null) {
                yCont -= 16;
            }
            for (int i=0; i<contsCnt; i++){
                String cont = conts[i];
                int contWth = Rs.FM18.stringWidth(cont);
                g.drawString(cont, 480 - (contWth/2), yCont+(i*CONTENT_GAP));
            }
        }
    }

    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_LEFT:
                if (curPopType == POPUP_TYPE_INFORMATION) {
                    return true;
                }
                if (curButton == BUTTON_OK) {
                    return true;
                }
                curButton = BUTTON_OK;
                repaint();
                return true;
            case Rs.KEY_RIGHT:
                if (curPopType == POPUP_TYPE_INFORMATION) {
                    return true;
                }
                if (curButton == BUTTON_CANCEL) {
                    return true;
                }
                curButton = BUTTON_CANCEL;
                repaint();
                return true;
            case KeyCodes.LAST:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
            case Rs.KEY_OK:
                if (curButton == BUTTON_OK) {
                    pListener.popupOK(new PopupEvent(this));
                } else {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }

    public int getCurrentButton() {
        return curButton;
    }

    public void setPopupSize(int popupSize) {
        curPopSize = popupSize;
    }

    public void setPopupType (int popupType) {
        curPopType = popupType;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

package com.videotron.tvi.illico.parental.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.parental.Rs;
import com.videotron.tvi.illico.parental.popup.Popup;
import com.videotron.tvi.illico.parental.popup.PopupEvent;
import com.videotron.tvi.illico.util.KeyCodes;

public class PopupParentalControl extends Popup {
    private static final long serialVersionUID = -1430573623555773316L;
    private DataCenter dataCenter = DataCenter.getInstance();

    private Image imgOptionB;
    private Image imgOptionM;
    private Image imgOptionT;
    private Image imgOptionHigh;
    private Image imgOptionMask;
    private Image imgOptionShadowB;
    private Image imgOptionShadowT;
    private Image imgOptionF;
    private Image imgArrowT;
    private Image imgArrowB;

    public static final int RATING_G = 0;
    public static final int RATING_8 = 1;
    public static final int RATING_13 = 2;
    public static final int RATING_16 = 3;
    public static final int RATING_18 = 4;
    public static final String[] RATING_VALUES = {
        Definitions.RATING_G,
        Definitions.RATING_8,
        Definitions.RATING_13,
        Definitions.RATING_16,
        Definitions.RATING_18
    };
    public static final String[] RATING_NAME_KEY = {
        "TxtParental.RatingNameG",
        "TxtParental.RatingName8",
        "TxtParental.RatingName13",
        "TxtParental.RatingName16",
        "TxtParental.RatingName18"
    };
    public static final String[] RATING_NAME_YEAR_KEY = {
        "TxtParental.RatingNameYearG",
        "TxtParental.RatingNameYear8",
        "TxtParental.RatingNameYear13",
        "TxtParental.RatingNameYear16",
        "TxtParental.RatingNameYear18"
    };
    private int curIdx;
    private ParentalControlText pcText;

    public PopupParentalControl() {
        imgOptionB = dataCenter.getImage("12_op_bg_b.png");
        imgOptionM = dataCenter.getImage("12_op_bg_m.png");
        imgOptionT = dataCenter.getImage("12_op_bg_t.png");
        imgOptionHigh = dataCenter.getImage("11_op_high.png");
        imgOptionMask = dataCenter.getImage("option_mask.png");
        imgOptionShadowB = dataCenter.getImage("12_op_sh_b.png");
        imgOptionShadowT = dataCenter.getImage("12_op_sh_t.png");
        imgOptionF = dataCenter.getImage("12_op_foc.png");
        imgArrowT = dataCenter.getImage("02_ars_t.png");
        imgArrowB = dataCenter.getImage("02_ars_b.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
        if (pcText == null) {
            pcText = new ParentalControlText();
            pcText.setBounds(598, 306, 286, 60);
            add(pcText);
        }
    }
    protected void disposePopup() {
        if (pcText != null) {
            remove(pcText);
            pcText = null;
        }
    }
    protected void startPopup() {
    }
    protected void stopPopup() {
    }
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_UP:
                curIdx = (RATING_VALUES.length + curIdx - 1) % RATING_VALUES.length;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                curIdx = (curIdx + 1) % RATING_VALUES.length;
                repaint();
                return true;
            case Rs.KEY_OK:
                if (pListener != null) {
                    pListener.popupOK(new PopupEvent(this));
                }
                return true;
            case KeyCodes.LAST:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }

    public void setCurrentIndex(int idx) {
        curIdx = idx;
    }

    public int getCurrentIndex() {
        return curIdx;
    }

    public static String getRatingValue(int idx) throws Exception {
        return RATING_VALUES[idx];
    }

    public static String getRatingName(int idx) throws Exception {
        return RATING_NAME_KEY[idx];
    }

    public static int getRatingIndex(String rValue) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupParentalControl.getRatingIndex]rValue : "+rValue);
        }
        int result = -1;
        if (rValue != null) {
            for (int i=0; i<RATING_VALUES.length; i++) {
                if (RATING_VALUES[i].equals(rValue)) {
                    result = i;
                    break;
                }
            }
        }
        return result;
    }

    public void paint(Graphics g) {
        // Backgound
        g.setColor(Rs.DVB000000000153);
        g.fillRect(0, 0, 960, 540);
        if (imgOptionB != null) {
            g.drawImage(imgOptionB, 597, 350, this);
        }
        if (imgOptionM != null) {
            g.drawImage(imgOptionM, 597, 323, 290, 27, this);
        }
        if (imgOptionT != null) {
            g.drawImage(imgOptionT, 597, 304, this);
        }
        g.setColor(Rs.C078078078);
        g.fillRect(599, 277, 286, 29);
        if (imgOptionHigh != null) {
            g.drawImage(imgOptionHigh, 632, 292, this);
        }
        String label = (String) dataCenter.get("TxtParental.LabelMyChildAge");
        if (label != null) {
            g.setFont(Rs.F19);
            g.setColor(Rs.C047047047);
            g.drawString(label, 611, 297);
            g.setColor(Rs.C214182055);
            g.drawString(label, 610, 296);
        }

        if (imgOptionMask != null) {
            g.drawImage(imgOptionMask, 601, 306, this);
        }
        if (imgOptionShadowB != null) {
            g.drawImage(imgOptionShadowB, 601, 339, this);
        }
        if (imgOptionShadowT != null) {
            g.drawImage(imgOptionShadowT, 601, 306, this);
        }

        //Focus
        if (imgOptionF != null) {
            g.drawImage(imgOptionF, 599, 319, this);
        }
        //Arrow
        if (imgArrowT != null) {
            g.drawImage(imgArrowT, 731, 262, this);
        }
        if (imgArrowB != null) {
            g.drawImage(imgArrowB, 731, 366, this);
        }
        super.paint(g);
    }

    private class ParentalControlText extends Component{
        private static final long serialVersionUID = 6295610833217421422L;
        public void paint(Graphics g) {
            g.setFont(Rs.F19);
            for (int i=0; i<3; i++) {
                String txtRatingName = null;
                String txtRatingNameYear = null;
                switch(i) {
                    case 0:
                        int prevIdx = (RATING_VALUES.length+curIdx-1)%RATING_VALUES.length;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[PopupParentalControl.getRatingIndex]prevIdx : "+prevIdx);
                        }
                        txtRatingName = (String) dataCenter.get(RATING_NAME_KEY[prevIdx]);
                        txtRatingNameYear = (String) dataCenter.get(RATING_NAME_YEAR_KEY[prevIdx]);
                        g.setColor(Rs.C193191191);
                        break;
                    case 1:
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[PopupParentalControl.getRatingIndex]curIdx : "+curIdx);
                        }
                        txtRatingName = (String) dataCenter.get(RATING_NAME_KEY[curIdx]);
                        txtRatingNameYear = (String) dataCenter.get(RATING_NAME_YEAR_KEY[curIdx]);
                        g.setColor(Rs.C003003003);
                        break;
                    case 2:
                        int nextIdx = (curIdx+1)%RATING_VALUES.length;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[PopupParentalControl.getRatingIndex]nextIdx : "+nextIdx);
                        }
                        txtRatingName = (String) dataCenter.get(RATING_NAME_KEY[nextIdx]);
                        txtRatingNameYear = (String) dataCenter.get(RATING_NAME_YEAR_KEY[nextIdx]);
                        g.setColor(Rs.C193191191);
                        break;
                }
                final int x = 598;
                final int y = 306;
                g.drawString(txtRatingName, 610-x, 313-y+(i*28));
                g.drawString(txtRatingNameYear, 872-x-Rs.FM19.stringWidth(txtRatingNameYear), 313-y+(i*28));
            }
        }
    }
}

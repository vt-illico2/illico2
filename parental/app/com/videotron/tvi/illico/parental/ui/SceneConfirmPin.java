package com.videotron.tvi.illico.parental.ui;

import java.awt.event.KeyEvent;

import com.videotron.tvi.illico.parental.communication.ParentalWizardServiceImpl;
import com.videotron.tvi.illico.parental.controller.PreferenceProxy;
import com.videotron.tvi.illico.parental.controller.SceneManager;
import com.videotron.tvi.illico.parental.controller.SceneTemplate;
import com.videotron.tvi.illico.parental.gui.RendererConfirmPin;

public class SceneConfirmPin extends Scene {
    private static final long serialVersionUID = -51202866469930921L;
    private ComponentStep compStep;
    private ComponentTitle compTitle;
    
    public static final int STATE_CANCEL = 0;
    public static final int STATE_NORMAL = 1;
    public static final int STATE_VALID = 2;
    private int curState;
    
    private int curIndex;
    public static final int PIN_MAX_LENGTH = 4;
    private String[] pinCodes;
    private StringBuffer buffer = new StringBuffer();
    private boolean isButtonArea;
    
    protected void initScene() {
        if (renderer == null) {
            renderer = new RendererConfirmPin();
            setRenderer(renderer);
        }
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(240, 50, 480, 40);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setStepIds(getStepIds());
            compStep.setCurrentStep(ComponentStep.STEP_ID_DEFAULT_PARENTAL_CONTROL);
            compStep.setBounds(170, 100, 625, 29);
            add(compStep);
        }  
    }
    
    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle = null;
        }
        renderer = null;
    }

    protected void startScene(boolean resetScene) {
        if (resetScene) {
            isButtonArea = false;
            curIndex = 0;
            curState = STATE_CANCEL;
            pinCodes = new String[PIN_MAX_LENGTH];
        }
        prepare(); 
    }

    protected void stopScene() {
        
    }
    
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
                if (isButtonArea) {
                    return true;
                }
                String inputData = String.valueOf((keyCode - KeyEvent.VK_0));
                pinCodes[curIndex] = inputData;
                if (curIndex == PIN_MAX_LENGTH-1) {
                    curState = STATE_VALID;
                    isButtonArea = true;
                } else {
                    curState = STATE_NORMAL;
                    curIndex ++;
                }
                repaint();
                return true;
            case KeyEvent.VK_UP:
                if (isButtonArea) {
                    isButtonArea = false;
                }
                repaint();
                return true;
            case KeyEvent.VK_DOWN:
                if (isButtonArea) {
                    return true;  
                }
                isButtonArea = true;
                repaint();
                return true;
//            case KeyEvent.VK_LEFT:
//                if (isButtonArea) {
//                    return true;  
//                }
//                if (curIndex == 0) {
//                    return true;
//                }
//                curIndex --;
//                repaint();
//                return true;
//            case KeyEvent.VK_RIGHT:
//                if (isButtonArea) {
//                    return true;  
//                }
//                if (curIndex == PIN_MAX_LENGTH-1) {
//                    return true;
//                }
//                curIndex ++;
//                repaint();
//                return true;
            case KeyEvent.VK_ENTER:
                if (!isButtonArea) {
                    return true;
                }
                switch(curState) {
                    case STATE_CANCEL:
//                        SceneManager.getInstance().hideParentalWizard();
                        ParentalWizardServiceImpl.getInstance().responseCanceled();
                        break;
                    case STATE_NORMAL:
                        if (curIndex > 0) {
                            curIndex --;
                        }
                        pinCodes[curIndex] = null;
                        if (curIndex == 0) {
                            curState = STATE_CANCEL;
                        }
                        repaint();
                        break;
                    case STATE_VALID:
                        String inputPINCode = getPINCode();
                        boolean isSuccess = PreferenceProxy.getInstance().checkAdminPIN(inputPINCode);
                        if (isSuccess) {
                            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_DEFAULT_PARENTAL_CONTROL, true);
                        } else {
                            isButtonArea = false;
                            curIndex = 0;
                            curState = STATE_CANCEL;
                            pinCodes = new String[PIN_MAX_LENGTH];
                            openPopupInfo("Invalid PIN Code", "aflkdsjalfjkasdlfj");
                        }
                        break;
                }
                return true;
        }
        return false;
    }
    
    private String getPINCode() {
        buffer.setLength(0);
        for (int i=0; i<pinCodes.length; i++) {
            if (pinCodes[i] != null) {
                buffer.append(pinCodes[i]);    
            }
        }
        return buffer.toString();
    }
    
    public boolean isButtonArea() {
        return isButtonArea;
    }
    
    public int getCurrentState() {
        return curState;
    }
    
    public int getCurrentIndex() {
        return curIndex;
    }
    
    public String[] getPINCodes() {
        return pinCodes;
    }
}

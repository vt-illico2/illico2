package com.videotron.tvi.illico.parental.communication;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.ixc.parental.ParentalWizardListener;
import com.videotron.tvi.illico.ixc.parental.ParentalWizardService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.parental.App;
import com.videotron.tvi.illico.parental.controller.SceneManager;

public class ParentalWizardServiceImpl implements ParentalWizardService {
    private static ParentalWizardServiceImpl instance;
    private ParentalWizardListener listener;

    public static synchronized ParentalWizardServiceImpl getInstance() {
        if (instance == null) {
            instance = new ParentalWizardServiceImpl();
        }
        return instance;
    }

    private ParentalWizardServiceImpl() {
    }

    public void init() {
        bindToIxc();
    }

    public void dispose() {
        try {
            IxcRegistry.unbind(App.xletContext, ParentalWizardService.IXC_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showParentalWizard(ParentalWizardListener listener) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ParentalWizardServiceImpl.showParentalWizard]Start");
        }
        this.listener = listener;
        SceneManager.getInstance().showParentalWizard();
    }

    public void hideParentalWizard() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ParentalWizardServiceImpl.hideParentalWizard]Start");
        }
        SceneManager.getInstance().hideParentalWizard();
    }

    public void responseCompleted() {
        if (listener != null) {
            try {
                listener.requestCompleted();
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    public void responseCanceled() {
        if (listener != null) {
            try {
                listener.requestCanceled();
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }


    /**
     * Bind to IxcRegistry.
     */
    private void bindToIxc() {
        try {
            if (Log.INFO_ON) {
                Log.printInfo("[ParentalWizardServiceImpl.bindToIxc]bind to IxcRegistry");
            }
            IxcRegistry.bind(App.xletContext, ParentalWizardService.IXC_NAME, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.videotron.tvi.illico.parental.popup;

import java.awt.Container;
import java.util.ArrayList;
import java.util.Iterator;

public class PopupController {
    private static PopupController instance;
    /*****************************************************************
     * variables - General
     *****************************************************************/
    private ArrayList popupList;
    private int popupListSize;

    /*****************************************************************
     * Constructor and Disposer
     *****************************************************************/
    public static synchronized PopupController getInstance() {
        if (instance == null) {
            instance = new PopupController();
        }
        return instance;
    }
    private PopupController() {
        if (popupList == null) {
            popupList = new ArrayList();
        }
    }

    public void init() {
    }

    public void dispose() {
        removeAllPopup();
    }

    /*****************************************************************
     * methods - Functional
     *****************************************************************/
    /** open pop-up **/
    public void openPopup(Popup openPopup, Container target) {
        if (popupList == null || openPopup == null || target == null) {
            return;
        }
        openPopup.start();
        popupList.add(openPopup);
        popupListSize = popupList.size();
        target.add(openPopup, 0);
        target.repaint();
    }

    /** close the last of pop-up **/
    public void closePopup(Popup closePopup, Container target) {
        if (popupList == null || closePopup == null || target == null) {
            return;
        }
        closePopup.stop();
        int popupIndex = popupList.indexOf(closePopup);
        if (popupIndex != -1) {
            popupList.remove(popupIndex);
            popupListSize = popupList.size();
        }
        target.remove(closePopup);
        target.repaint();
    }

    public void closeAllPopup(Container target) {
        if (popupList == null || popupListSize <= 0 || target ==null) {
            return;
        }
        Iterator iterator = popupList.iterator();
        while (iterator.hasNext()) {
            Popup closePopup = (Popup) iterator.next();
            if (closePopup == null) {
                continue;
            }
            target.remove(closePopup);
            closePopup.stop();
        }
        popupList.clear();
        popupListSize = popupList.size();
        target.repaint();
    }

    /** get the length of a pop-up **/
    public int getPopupLength() {
        return popupListSize;
    }

    public Popup getPopup(int popupIdx) {
        if (popupList == null)
            return null;
        return (Popup) popupList.get(popupIdx);
    }

    public Popup getLastPopup() {
        int popupListSize = -1;
        if (popupList == null || (popupListSize = popupList.size()) <= 0)
            return null;
        return (Popup) popupList.get(popupListSize - 1);
    }

    public boolean isActivePopup() {
        if (popupListSize > 0)
            return true;
        return false;
    }

    public void removeAllPopup() {
        if (popupList == null || popupListSize <= 0)
            return;
        Iterator iterator = popupList.iterator();
        while (iterator.hasNext()) {
            Popup popup = (Popup) iterator.next();
            if (popup == null)
                continue;
            popup.dispose();
        }
        popupList.clear();
        popupList = null;
    }

//    public void addPopupInRootContainer(Popup popup) {
//        if (popup == null) {
//            return;
//        }
//        App.sceneContainer.add(popup, 0);
//    }
//
//    public void removePopupInRootContainer(Popup popup) {
//        if (popup == null) {
//            return;
//        }
//        App.sceneContainer.remove(popup);
//    }
//
//    public void repaintRootContainer() {
//        App.sceneContainer.repaint();
//    }
}

package com.videotron.tvi.illico.parental.popup;

import java.awt.Container;

abstract public class Popup extends Container {
    private static final long serialVersionUID = 1L;
    /*****************************************************************
     * variables - PopupListener
     *****************************************************************/
    protected PopupListener pListener;

    /*****************************************************************
     * methods - Life Cycle
     *****************************************************************/
    public void dispose() {
        stopPopup();
        disposePopup();
        pListener = null;
    }

    public void start() {
        startPopup();
    }

    public void stop() {
        stopPopup();
    }

    /*****************************************************************
     * methods - PopupListener
     *****************************************************************/
    public void setPopupListener(PopupListener pListener) {
        this.pListener = pListener;
    }

    public void removePopupListener() {
        this.pListener = null;
    }

    public PopupListener getPopupListener() {
        return pListener;
    }

    /*****************************************************************
     * methods - abstract
     *****************************************************************/
    abstract protected void disposePopup();

    abstract protected void startPopup();

    abstract protected void stopPopup();

    abstract public boolean keyAction(int keyCode);
}

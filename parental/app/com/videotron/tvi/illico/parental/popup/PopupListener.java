package com.videotron.tvi.illico.parental.popup;

public interface PopupListener {
    public Object request(PopupEvent pe) throws Exception;

    public void requestCompleted(PopupEvent pe, Object resultObj);

    public void requestFailed(PopupEvent pe, String errorMessage);

    public void popupOK(PopupEvent pe);

    public void popupCancel(PopupEvent pe);
}

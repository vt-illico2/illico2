package com.videotron.tvi.illico.parental;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.parental.communication.ParentalWizardServiceImpl;
import com.videotron.tvi.illico.parental.controller.SceneManager;

/**
 * The initial class of Main Menu.
 * @author Sangjoon Kwon
 */
public class App implements Xlet, ApplicationConfig {
    /** XletContext instance. */
    public static XletContext xletContext;
    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx
     *            The XletContext of this Xlet.
     * @throws XletStateChangeException
     *             If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        System.out.println("[Parental][App.initXlet]start.");
        xletContext = ctx;
        Log.setApplication(this);
        FrameworkMain.getInstance().init(this);
        SceneManager.getInstance().init();
        ParentalWizardServiceImpl.getInstance().init();
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException
     *             is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        System.out.println("[Parental][App.startXlet]start.");
        FrameworkMain.getInstance().start();
        SceneManager.getInstance().start();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        System.out.println("[Parental][App.pauseXlet]start.");
        FrameworkMain.getInstance().pause();
        SceneManager.getInstance().stop();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional
     *            If unconditional is true when this method is called, requests by the Xlet to not enter the destroyed
     *            state will be ignored.
     * @throws XletStateChangeException
     *             is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed state). This exception
     *             is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        System.out.println("[Parental][App.destroyXlet]start.");
        FrameworkMain.getInstance().destroy();
        SceneManager.getInstance().dispose();
        ParentalWizardServiceImpl.getInstance().dispose();
    }

    /**
     * Gets the version of this application.
     * @return the version of this application.
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }

    /**
     * Gets the name of this application.
     * @return the name of this application.
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }

    /**
     * Gets the XletContext instance of this application.
     * @return XletContext instance of this application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

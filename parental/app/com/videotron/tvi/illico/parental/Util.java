package com.videotron.tvi.illico.parental;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import com.videotron.tvi.illico.log.Log;

public class Util {
    /** The Constant ZIP_FULE_BYTE_BUFFER.*/
    public static final int ZIP_FILE_BYTE_BUFFER = 1024;

    public static int twoBytesToInt(byte[] data, int offset, boolean byNegative){
        return(((((int)data[offset])&(byNegative ? 0x7F : 0xFF))<<8)+(((int)data[offset+1])&0xFF));
    }

    public static byte[] intTo2Byte(int data) {
        byte[] byteData = new byte[2];
        byteData[0] = (byte) ((data >> 8) & 0xff);
        byteData[1] = (byte) (data & 0xff);
        return byteData;
    }

    public static byte[] stringTo2Byte(String data) {
        if (data == null) {
            return null;
        }
        data += " ";
        byte[] dataBytes = data.getBytes();
        byte[] byteData = new byte[2];
        System.arraycopy(dataBytes, 0, byteData, 0, 2);
        return byteData;
    }

    public static void flushZipFile(ZipFile target) {
        if (target != null) {
            try{
                target.close();
            }catch(Exception e) {
                e.printStackTrace();
            }finally {
                target = null;
            }
        }
    }

    public static ZipFile createZipFile(File source) {
        if (source == null) {
            return null;
        }
        ZipFile result = null;
        try{
            result = new ZipFile(source);
        }catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static byte[] getBytesFromZip(ZipFile zipFile, String entryName) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Util.getBytesFromZip]zipFile : " + zipFile);
            Log.printDebug("[Util.getBytesFromZip]entryName : " + entryName);
        }
        if (zipFile == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Zip File is null.");
            }
            return null;
        }

        if (entryName == null) {
            entryName = "";
        } else {
            entryName = entryName.trim();
        }
        if (entryName.length()<=0) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Entry Name is invalid.");
            }
            return null;
        }
        return getBytesFromZip(zipFile, zipFile.getEntry(entryName));
    }

    public static byte[] getBytesFromZip(ZipFile zipFile, ZipEntry zipEntry) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Util.getBytesFromZip]zipFile : " + zipFile);
            Log.printDebug("[Util.getBytesFromZip]zipEntry : " + zipEntry);
        }
        if (zipFile == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Zip File is null.");
            }
            return null;
        }

        if (zipEntry == null) {
            return null;
        }

        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(zipFile.getInputStream(zipEntry));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[ZIP_FILE_BYTE_BUFFER];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (bout != null) {
                    bout.reset();
                    bout.close();
                    bout = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    /**
     * Gets the bytes from zip.
     * @param zipFile
     *            the zip file
     * @param entryName
     *            the entry name
     * @return the bytes from zip
     * @throws ZipException
     *             the zip exception
     */

    public static byte[] getByteArrayFromFile(File file) {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getByteFromFile]file : " + file);
//        }
        if (file == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[ZIP_FILE_BYTE_BUFFER];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (bout != null) {
                    bout.reset();
                    bout.close();
                    bout = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static byte[] getURLConnect(String data) {
        BufferedInputStream bis = null;
        byte[] b = null;
        try {
            URL url = new URL(data);
            ((HttpURLConnection) url.openConnection()).getResponseCode();
            bis = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int c;
            while ((c = bis.read(buf)) != -1) {
                baos.write(buf, 0, c);
            }
            b = baos.toByteArray();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return b;
    }
}

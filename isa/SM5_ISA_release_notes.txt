This is ISA App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version (R7.2+CYO).5.2
    - Release App file Name
      : ISA.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2016.11.01

***
version (R7.2+CYO).5.2 - 2016.11.01
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5978 [CQ][Bundling][R7.2][Functional BE Bundling with R7.2][ISA] Optimisation pop-up is displayed in wrong position when subscribing a Premium ISA channel
        + VDTRMASTER-5979 VDTRMASTER-5976[CQ][Bundling][R7.2][Functional BE Bundling with R7.2][ISA]: There is not TOC displayed for Redirection information screen
***
version (R7.2+CYO).4.1 - 2016.10.28
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5944 [CQ][Bundling][R7.2][Functional BE Bundling with R7.2][ISA]: Optimisation pop-up is not displayed for Combinable channels in Skinny + New packages
***
version (R7.2+CYO).3.2 - 2016.10.18
    - New Feature
        + update TOCs.
    - Resolved Issues
        + VDTRMASTER-5910 [CQ][Bundling][Functional BE Bundling with R7.2][ISA]: On the regular ISA info screen, several UI elements are not consistent with the TOC
        + VDTRMASTER-5935 [CQ][Bundling][Functional BE Bundling with R7.2][ISA]: On the Excluded ISA info screen, the subscription option text are not consistent with the TOC
***
version (R7.2+CYO).2.1 - 2016.10.04
    - New Feature
        + update TOCs.
    - Resolved Issues
        + VDTRMASTER-5889 [CQ][Bundling][Functional BE Bundling with R7.2][ISA]: Regular ISA Info screen is displayed when subscribing to club illico.
***
version (R7.2+CYO).1.0 - 2016.09.19
    - New Feature
        + implements Bundling requirements
    - Resolved Issues
        N/A
***
version (R5+SM5)_2.1 - 2016.02.26
    - New Feature   
        N/A
    - Resolved Issues
        + VDTRMASTER-5722 [CQ][SM5][R5][ISA] Club illico should replace Club unlimited in subscription confirmation popup
***
version (R5+SM5)_1.0 - 2015.09.23
    - New Feature   
        + handle ISA22 according to DDC SM5.
    - Resolved Issues
        N/A

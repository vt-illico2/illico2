/**
 * 
 */
package com.videotron.tvi.illico.isa.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.ui.CompleteOrderTrackingUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author zestyman
 *
 */
public class CompleteOrderTrackingRenderer extends BaseRenderer {

	private Image gapImg, iconCallImg, focusButtonImg, iconGCheckImg, iconNotiImg;
	private String okStr;
	
	/**
	 * 
	 */
	public CompleteOrderTrackingRenderer() {
	}

	public void prepare(UIComponent c) {
		DataCenter dc = DataCenter.getInstance();
		
//		bgMiddleImg = dc.getImage("05_pop_glow_m.png");
//		bgTopImg = dc.getImage("05_pop_glow_t.png");
//		bgBottomImg = dc.getImage("05_pop_glow_b.png");
				
//		shadowImg = dc.getImage("pop_sha.png");
//		highImg = dc.getImage("pop_high_350.png");
		gapImg = dc.getImage("pop_gap_379.png");
		focusButtonImg = dc.getImage("05_focus.png");
		iconCallImg = dc.getImage("icon_call.png");
		iconGCheckImg = dc.getImage("icon_g_check.png");
		iconNotiImg = dc.getImage("icon_noti_red.png");
		
		FrameworkMain.getInstance().getImagePool().waitForAll();
		
		okStr = getString(Def.KEY_OK);
		super.prepare(c);
	}
	
	protected void paint(Graphics g, UIComponent c) {
		CompleteOrderTrackingUI ui = (CompleteOrderTrackingUI) c;
		// background
		g.setColor(Rs.C000_000_000_204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		
//		g.drawImage(bgMiddleImg, 276, 193, 410, 124, c);
//		g.drawImage(bgTopImg, 276, 113, 410, 80, c);
//		g.drawImage(bgBottomImg, 276, 317, 410, 80, c);
//		g.drawImage(shadowImg, 306, 364, 350, 79, c);
		g.setColor(Rs.C35);
		g.fillRect(306, 143, 350, 224);
//		g.drawImage(highImg, 306, 143, 350, 200, c);
		g.drawImage(gapImg, 285, 181, 379, 16, c);
		
		// title
		g.setColor(Rs.C250_202_0);
		g.setFont(Rs.F24);
		if (ui.getIconType().equals(PopupTitle.ICON_TYPE_2)) {
			int x = GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 170);
			g.drawImage(iconNotiImg, x - 35, 152, c);
		} else if (ui.getIconType().equals(PopupTitle.ICON_TYPE_1)) {
			int x = GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 170);
			g.drawImage(iconGCheckImg, x - 34, 151, c);
		} else {
			GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 170);
		}
		
		// explain
		g.setFont(Rs.F18);
		g.setColor(Rs.C229);
		String[] explain = ui.getExplain();
		if (explain != null) {
			if (explain.length == 1) {
				int x = GraphicUtil.drawStringCenter(g, explain[0], 482, 239);
				g.drawImage(iconCallImg, x - 25, 225, c);
			} else if (explain.length == 2) {
				int x = GraphicUtil.drawStringCenter(g, explain[0], 492, 247);
				GraphicUtil.drawStringCenter(g, explain[1], 482, 267);
				
				g.drawImage(iconCallImg, x - 25, 233, c);
			} else if (explain.length == 3) {
				int x = GraphicUtil.drawStringCenter(g, explain[0], 492, 237);
				GraphicUtil.drawStringCenter(g, explain[1], 482, 257);
				GraphicUtil.drawStringCenter(g, explain[2], 482, 277);
				
				g.drawImage(iconCallImg, x - 25, 223, c);
			}
		}
		
		g.drawImage(focusButtonImg, 403, 318, 162, 40, c);
		g.setFont(Rs.F18);
		g.setColor(Rs.C3);
		GraphicUtil.drawStringCenter(g, okStr, 481, 339);
		
		super.paint(g, c);
	}
}

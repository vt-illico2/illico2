/**
 * 
 */
package com.videotron.tvi.illico.isa.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.ui.OrderTrackingUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author zestyman
 *
 */
public class OrderTrackingRenderer extends BaseRenderer {

	private Image bgImg, focusButtonImg, dimButtonImg, offButtonImg, iconCallImg, iconGCheckImg, iconXImg, iconNotiImg, inputImg, inputFocusImg;
	
	private String callMeStr, clearStr;
	
	/**
	 * 
	 */
	public OrderTrackingRenderer() {
	}
	
	public void prepare(UIComponent c) {
		DataCenter dc = DataCenter.getInstance();
		
		bgImg = dc.getImage("pop_506_bg_2.png");
		focusButtonImg = dc.getImage("05_focus.png");
		dimButtonImg = dc.getImage("05_focus_dim.png");
		offButtonImg = dc.getImage("05_focus_off.png");
		iconCallImg = dc.getImage("icon_call.png");
		iconGCheckImg = dc.getImage("icon_g_check.png");
		iconXImg = dc.getImage("icon_r_x__small.png");
		iconNotiImg = dc.getImage("icon_noti_red.png");
		inputImg = dc.getImage("input_210.png");
		inputFocusImg = dc.getImage("input_210_foc2.png");
		
		FrameworkMain.getInstance().getImagePool().waitForAll();
		
		callMeStr = getString(Def.KEY_CALL_ME);
		clearStr = getString(Def.KEY_CLEAR);
		
		super.prepare(c);
	}

	protected void paint(Graphics g, UIComponent c) {
		OrderTrackingUI ui = (OrderTrackingUI) c;
		// background
		g.setColor(Rs.C000_000_000_204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		
		g.drawImage(bgImg, 197, 85, 566, 394, c);
		
		
		// title
		g.setFont(Rs.F24);
		g.setColor(Rs.C250_202_0);
		
		if (ui.getIconType().equals(PopupTitle.ICON_TYPE_2)) {
			int x = GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 142);
			g.drawImage(iconNotiImg, x - 35, 124, c);
		} else if (ui.getIconType().equals(PopupTitle.ICON_TYPE_1)) {
			int x = GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 142);
			g.drawImage(iconGCheckImg, x - 34, 123, c);
		} else {
			GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 142);
		}
		
		// explain with red color
		g.setFont(Rs.F18);
		g.setColor(Rs.C241_35_35);
		
		int x = 0;
		String [] explain = ui.getTopExplain();
		x = GraphicUtil.drawStringCenter(g, explain[0], 494, 200);
		if (explain.length > 1) {
			GraphicUtil.drawStringCenter(g, explain[1], 484, 224);
		}
		g.drawImage(iconXImg, x - 28, 183, c);
		
		// explain to input phone number
		g.setColor(Rs.C229);
		explain = ui.getBottomExplain();
		x = GraphicUtil.drawStringCenter(g, explain[0], 492, 264);
		if (explain.length > 1) {
			GraphicUtil.drawStringCenter(g, explain[1], 482, 284);
		}
		g.drawImage(iconCallImg, x - 25, 250, c);
		
		// input of phone number
		if (ui.getFocus() == 0) {
			g.drawImage(inputFocusImg, 376, 304, c);
		} else {
			g.drawImage(inputImg, 376, 304, c);
		}
		
		GraphicUtil.drawStringCenter(g, ui.getInputPhoneNumber(), 482, 325);
		
		// button
		if (ui.getFocus() == 1) {
			g.drawImage(focusButtonImg, 321, 374, 162, 40, c);
		} else {
			g.drawImage(dimButtonImg, 321, 374, 162, 40, c);
		}
		g.setColor(Rs.C3);
		GraphicUtil.drawStringCenter(g, callMeStr, 400, 396);
		
		if (ui.getFocus() == 2) {
			g.drawImage(focusButtonImg, 485, 374, 162, 40, c);
			g.setColor(Rs.C3);
			GraphicUtil.drawStringCenter(g, clearStr, 564, 396);
		} else {
			
			if (ui.getInputPhoneNumberSize() > 0) {
				g.drawImage(dimButtonImg, 485, 374, 162, 40, c);
				g.setColor(Rs.C3);
				GraphicUtil.drawStringCenter(g, clearStr, 564, 396);
			} else {
				g.drawImage(offButtonImg, 485, 374, 162, 40, c);
				g.setColor(Rs.C110);
				GraphicUtil.drawStringCenter(g, clearStr, 565, 396);
				g.setColor(Rs.C50);
				GraphicUtil.drawStringCenter(g, clearStr, 564, 395);
			}
		}
		
		super.paint(g, c);
	}
}

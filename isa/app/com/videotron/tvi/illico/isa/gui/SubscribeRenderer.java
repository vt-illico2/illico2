/**
 * 
 */
package com.videotron.tvi.illico.isa.gui;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.data.obj.ShortDescription;
import com.videotron.tvi.illico.isa.ui.SubscribeUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author zestyman
 *
 */
public class SubscribeRenderer extends BaseRenderer {

	private Image bgTitleImg, bgSeperatorImg, focusButtonImg, dimButtonImg, iconGrImg, iconBlImg;
	private Image channelImg;
	
	private String orStr, subscribeNowStr, closeStr;
	
	/**
	 * 
	 */
	public SubscribeRenderer() {
	}

	public Rectangle getPreferredBounds(UIComponent c) {
		return c.getBounds();
	}

	public void prepare(UIComponent c) {
		DataCenter dc = DataCenter.getInstance();
//		bgShadowImg = dc.getImage("12_ppop_sha.png");
//		bgHighImg = dc.getImage("12_pop_high_625.png");
		bgTitleImg = dc.getImage("07_respop_title.png");
		bgSeperatorImg = dc.getImage("pop_sep_589.png");
		focusButtonImg = dc.getImage("05_focus.png");
		dimButtonImg = dc.getImage("05_focus_dim.png");
		iconGrImg = dc.getImage("02_icon_isa_gr.png");
		iconBlImg = dc.getImage("02_icon_isa_bl.png");
		
		FrameworkMain.getInstance().getImagePool().waitForAll();
		
		orStr = getString(Def.KEY_OR);
		subscribeNowStr = getString(Def.KEY_SUBSCRIBE_NOW);
		closeStr = getString(Def.KEY_CLOSE);
	}

	protected void paint(Graphics g, UIComponent c) {
		SubscribeUI ui = (SubscribeUI) c;
		
		// dimmed background
		g.setColor(Rs.C000_000_000_204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		
		// popup background
		g.setColor(Rs.C33);
		g.fillRect(170, 51, 625, 435);
//		g.drawImage(bgShadowImg, 170, 483, 625, 57, c);
//		g.drawImage(bgHighImg, 170, 51, 625, 378, c);
		g.drawImage(bgTitleImg, 197, 89, 533, 16, c);
		g.drawImage(bgSeperatorImg, 196, 436, 589, 17, c);
		
		if (c.getFocus() == 0) {
			g.drawImage(focusButtonImg, 233, 357, 258, 40, c);
			g.drawImage(dimButtonImg, 487, 357, 254, 40, c);
		} else {
			g.drawImage(dimButtonImg, 233, 357, 254, 40, c);
			g.drawImage(focusButtonImg, 487, 357, 258, 40, c);
		}
		
		// title
		g.setColor(Rs.C250_202_0);
		g.setFont(Rs.F24);
		GraphicUtil.drawStringCenter(g, ui.getPopupTitle(), 481, 78);
		
		// channel image
		if (channelImg != null) {
			g.drawImage(channelImg, 189, 108, 588, 98, c);
		}
		
		// callToAction
		g.setColor(Rs.C254_234_160);
		g.setFont(Rs.F20);
		GraphicUtil.drawStringCenter(g,  ui.getCallToActionText(), 479, 238);
		
		// short Description
		g.setColor(Rs.C229);
		g.setFont(Rs.F18);
		
		String[] shorrDescription = ui.getShortDescription();
		
		if (shorrDescription != null && shorrDescription[0] != null && shorrDescription[1] != null) {
			int width = Rs.FM18.stringWidth(shorrDescription[0]);
			int width2 = Rs.FM18.stringWidth(shorrDescription[1]);
			
			int x = 483 - (width2 / 2) - 9;
					
			int targetX = GraphicUtil.drawStringCenter(g, shorrDescription[0], x, 332);
			g.setColor(Rs.C250_209_0);
			g.drawString(shorrDescription[1], targetX + width + 9, 332);
		}
		
		// or & or
		g.setFont(Rs.F16);
		g.setColor(Rs.C172);
		GraphicUtil.drawStringCenter(g, orStr, 481, 413);
		
		// Fine Print 
		String[] finePrint = ui.getFinePrint();
		
		if (finePrint != null && finePrint.length > 0) {
			g.setColor(Rs.C139);
			g.setFont(Rs.F13);
			
			int y = 470;
			
			if (finePrint.length > 1) {
				y -= finePrint.length/2 * 13;
				
				if (finePrint.length %2 == 1) {
					y -= 6;
				}
			}
			
			if ( y < 453) {
				y = 453;
			}
			
			for (int i = 0; i < finePrint.length; i++) {
				GraphicUtil.drawStringCenter(g, finePrint[i], 483, y + i * 14);
			}
		}
		
		// button text
		g.setFont(Rs.F18);
		g.setColor(Rs.C3);
		int subscribeNowPosX = GraphicUtil.drawStringCenter(g, subscribeNowStr, 370, 378);
		GraphicUtil.drawStringCenter(g, closeStr, 612, 378);
		
		if (ui.getFocus() == 0) {	
			g.drawImage(iconBlImg, subscribeNowPosX - 30, 363, c);
		} else {
			g.drawImage(iconGrImg, subscribeNowPosX - 28, 362, c);
		}
		
		super.paint(g, c);
	}
	
	public void setChannelImage(Image chImg) {
		channelImg = chImg;
	}

}

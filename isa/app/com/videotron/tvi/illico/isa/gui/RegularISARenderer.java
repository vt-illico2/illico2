/**
 *
 */
package com.videotron.tvi.illico.isa.gui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.ui.CYOUI;
import com.videotron.tvi.illico.isa.ui.RegularISAUI;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;

import java.awt.*;

/**
 * @author zestyman
 */
public class RegularISARenderer extends BaseRenderer {

	private Image channelBgImg, channelImg;

	private String closeStr, visitURLStr, dialNumberStr;

	private Image exitImg;
	private String modifyChannelsStr, aLaCarteChannelStr;

	/**
	 *
	 */
	public RegularISARenderer() {
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.UIComponent)
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return c.getBounds();
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
	 */
	public void prepare(UIComponent c) {
		RegularISAUI ui = (RegularISAUI) c;

		DataCenter dc = DataCenter.getInstance();

		channelBgImg = dc.getImage("chlogobg_2.png");

		closeStr = getString(Def.KEY_CLOSE);
		visitURLStr = getString(Def.KEY_VISIT_URL);
		dialNumberStr = getString(Def.KEY_DIAL_NUMBER);

		modifyChannelsStr = getString(Def.KEY_MODIFY_CHANNEL);
		aLaCarteChannelStr = getString(Def.KEY_A_LA_CARTE_CHANNEL);



		channelImg = ui.getChannelLogoImage();
		exitImg = BaseRenderer.getFooterImage(PreferenceService.BTN_EXIT);

		FrameworkMain.getInstance().getImagePool().waitForAll();
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics, com.videotron.tvi.illico.framework.UIComponent)
	 */
	protected void paint(Graphics g, UIComponent c) {
		RegularISAUI ui = (RegularISAUI) c;
		// background
		g.setColor(Rs.C000_000_000_204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

		g.setColor(Rs.C44);
		g.fillRect(200, 93, 565, 339);
		g.setColor(Rs.C66);
		g.fillRect(220, 132, 525, 1);

		if (c.getFocus() == 0) {
			g.setColor(Rs.C255_191_0);
			g.fillRect(233, 311, 245, 32);
			g.setColor(Rs.C157);
			g.fillRect(486, 311, 245, 32);
		} else {
			g.setColor(Rs.C157);
			g.fillRect(233, 311, 245, 32);
			g.setColor(Rs.C255_191_0);
			g.fillRect(486, 311, 245, 32);
		}

		// title
		g.setColor(Rs.C250_202_0);
		g.setFont(Rs.F24);
		GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 120);

		// channel logo
		// 12, 24 - gap of channel logo from background
		g.drawImage(channelBgImg, 232, 180, c);
		g.drawImage(channelImg, 244, 204, 100, 30, c);

		// explain
		g.setFont(Rs.F18);
		g.setColor(Rs.C229);
		String[] explain = ui.getExplain();
		if (explain != null) {
			int posY = 224;
			if (explain.length % 2 == 0) {
				posY = posY - (explain.length / 2 * 20) + 10;
			} else {
				posY = posY - (explain.length / 2 * 20);
			}

			for (int i = 0; i < explain.length; i++) {
				g.drawString(explain[i], 396, posY + i * 20);
			}
		}

		// button
		g.setFont(Rs.F18);
		g.setColor(Rs.C3);
		GraphicUtil.drawStringCenter(g, modifyChannelsStr, 357, 333);
		GraphicUtil.drawStringCenter(g, aLaCarteChannelStr, 609, 333);

		// or & or
		g.setFont(Rs.F16);
		g.setColor(Rs.C172);
		GraphicUtil.drawStringCenter(g, visitURLStr, 481, 370);
		GraphicUtil.drawStringCenter(g, dialNumberStr, 481, 390);

		// exit button
		g.setColor(Rs.C241);
		int x = GraphicUtil.drawStringRight(g, closeStr, 755, 455);
		g.drawImage(exitImg, x - 5 - exitImg.getWidth(c), 439, c);

		super.paint(g, c);
	}
}

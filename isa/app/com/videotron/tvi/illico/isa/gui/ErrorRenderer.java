/**
 * 
 */
package com.videotron.tvi.illico.isa.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.ui.ErrorUI;
import com.videotron.tvi.illico.isa.ui.OrderConfirmationUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author zestyman
 *
 */
public class ErrorRenderer extends BaseRenderer {

	private Image gapImg, focusButtonImg, iconGCheckImg, iconNotiImg;
	
	/**
	 * 
	 */
	public ErrorRenderer() {
	}
	
	public void prepare(UIComponent c) {
		DataCenter dc = DataCenter.getInstance();
		
//		bgImg = dc.getImage("pop_d_glow_461.png");
//		shadowImg = dc.getImage("pop_sha.png");
//		highImg = dc.getImage("pop_high_402.png");
		gapImg = dc.getImage("pop_gap_379.png");
		focusButtonImg = dc.getImage("05_focus.png");
		iconGCheckImg = dc.getImage("icon_g_check.png");
		iconNotiImg = dc.getImage("icon_noti_red.png");
		
		FrameworkMain.getInstance().getImagePool().waitForAll();
		
		super.prepare(c);
	}

	protected void paint(Graphics g, UIComponent c) {
		ErrorUI ui = (ErrorUI) c;
		
		g.setColor(Rs.C000_000_000_204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		
//		g.drawImage(bgImg, 250, 113, 461, 319, c);
//		g.drawImage(shadowImg, 280, 399, 402, 79, c);
		g.setColor(Rs.C35);
		g.fillRect(280, 143, 402, 259);
//		g.drawImage(highImg, 280, 143, 402, 200, c);
		g.drawImage(gapImg, 292, 181, 379, 16, c);
		
		// title
		g.setColor(Rs.C250_202_0);
		g.setFont(Rs.F24);
		if (ui.getIconType().equals(PopupTitle.ICON_TYPE_2)) {
			int x = GraphicUtil.drawStringCenter(g, ui.getTitle(), 490, 170);
			g.drawImage(iconNotiImg, x - 35, 152, c);
		} else if (ui.getIconType().equals(PopupTitle.ICON_TYPE_1)) {
			int x = GraphicUtil.drawStringCenter(g, ui.getTitle(), 490, 170);
			g.drawImage(iconGCheckImg, x - 34, 151, c);
		} else {
			GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 170);
		}
		
		// Explain
		g.setFont(Rs.F19);
		g.setColor(Rs.C241);
		String[] explain = ui.getExplain();
		
		if (explain != null) {
			
			int posY = 264 - explain.length * 24 / 2 + 24;
			
			for (int i = 0; i < explain.length; i++) {
				GraphicUtil.drawStringCenter(g, explain[i], 479, posY + i * 24);
			}
		}
		
		g.drawImage(focusButtonImg, 403, 353, 162, 40, c);
		g.setFont(Rs.F18);
		g.setColor(Rs.C3);
		GraphicUtil.drawStringCenter(g, ui.getButtonName(), 481, 374);
		
		super.paint(g, c);
	}
}

/**
 * 
 */
package com.videotron.tvi.illico.isa.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.ui.CYOUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author zestyman
 *
 */
public class CYORenderer extends BaseRenderer {
	
	private Image respopTitleImg, focusButtonImg, dimButtonImg, channelBgImg, channelImg;
	
	private String addThisChannelStr, closeStr, visitURLStr, dialNumberStr;
	/**
	 * 
	 */
	public CYORenderer() {
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.UIComponent)
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		return c.getBounds();
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
	 */
	public void prepare(UIComponent c) {
		CYOUI ui = (CYOUI) c;
		
		DataCenter dc = DataCenter.getInstance();
		
//		shadowImg = dc.getImage("12_ppop_sha.png");
//		highImg = dc.g`etImage("12_pop_high_545.png");
		respopTitleImg = dc.getImage("07_respop_title.png");
		focusButtonImg = dc.getImage("05_focus.png");
		dimButtonImg = dc.getImage("05_focus_dim.png");
		channelBgImg = dc.getImage("chlogobg_2.png");
		
		addThisChannelStr = getString(Def.KEY_ADD_THIS_CHANNEL);
		closeStr = getString(Def.KEY_CLOSE);
		visitURLStr = getString(Def.KEY_VISIT_URL);
		dialNumberStr = getString(Def.KEY_DIAL_NUMBER);
		
		FrameworkMain.getInstance().getImagePool().waitForAll();
		
		channelImg = ui.getChannelLogoImage();
		
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics, com.videotron.tvi.illico.framework.UIComponent)
	 */
	protected void paint(Graphics g, UIComponent c) {
		CYOUI ui = (CYOUI) c;
		// background
		g.setColor(Rs.C000_000_000_204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		
//		g.drawImage(shadowImg, 210, 419, 545, 57, c);
		g.setColor(Rs.C35);
		g.fillRect(210, 123, 545, 299);
//		g.drawImage(highImg, 210, 123, 545, 378, c);
		g.drawImage(respopTitleImg, 197, 161, 533, 16, c);
		
		if (c.getFocus() == 0) {
			g.drawImage(focusButtonImg, 231, 301, 258, 40, c);
			g.drawImage(dimButtonImg, 485, 301, 254, 40, c);
		} else {
			g.drawImage(dimButtonImg, 231, 301, 254, 40, c);
			g.drawImage(focusButtonImg, 485, 301, 258, 40, c);
		}
		
		// title
		g.setColor(Rs.C250_202_0);
		g.setFont(Rs.F24);
		GraphicUtil.drawStringCenter(g, ui.getTitle(), 480, 150);
		
		// channel logo
		g.drawImage(channelBgImg, 276, 190, c);
		g.drawImage(channelImg, 288, 214, 100, 30, c);
		
		// explain
		g.setFont(Rs.F18);
		g.setColor(Rs.C229);
		String[] explain = ui.getExplain();
		if (explain != null) {
			if (explain.length == 1) {
				g.drawString(explain[0], 421, 226);
			} else if (explain.length == 2) {
				g.drawString(explain[0], 421, 226);
				g.drawString(explain[1], 421, 250);
			}
		}
		
		// or & or
		g.setFont(Rs.F16);
		g.setColor(Rs.C255);
		GraphicUtil.drawStringCenter(g, visitURLStr, 481, 370);
		GraphicUtil.drawStringCenter(g, dialNumberStr, 481, 390);
		
		// button
		g.setFont(Rs.F18);
		g.setColor(Rs.C3);
		GraphicUtil.drawStringCenter(g, addThisChannelStr, 357, 323);
		GraphicUtil.drawStringCenter(g, closeStr, 609, 323);
		
		super.paint(g, c);
	}

}

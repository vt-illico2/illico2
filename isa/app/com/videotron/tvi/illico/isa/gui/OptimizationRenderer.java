package com.videotron.tvi.illico.isa.gui;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.ui.OptimizationUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

import java.awt.*;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-07-08.
 */
public class OptimizationRenderer extends BaseRenderer {

	private String titleStr, explainStr, modifyStr, continueStr;

	public Rectangle getPreferredBounds(UIComponent c) {
		return c.getBounds();
	}

	public void prepare(UIComponent c) {

		titleStr = BaseRenderer.getString(Def.KEY_OPTIMIZATION_TITLE);
		explainStr = BaseRenderer.getString(Def.KEY_OPTIMIZATION_EXPLAIN);
		modifyStr = BaseRenderer.getString(Def.KEY_OPTIMIZATION_BUTTON1);
		continueStr = BaseRenderer.getString(Def.KEY_OPTIMIZATION_BUTTON2);

		super.prepare(c);
	}

	protected void paint(Graphics g, UIComponent c) {
		OptimizationUI ui = (OptimizationUI) c;
		// background
		g.setColor(Rs.C000_000_000_204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		g.setColor(Rs.C44);
		g.fillRect(200, 93, 565, 339);
		g.setColor(Rs.C66);
		g.fillRect(220, 132, 525, 1);

		// title
		g.setFont(Rs.F24);
		g.setColor(Rs.C250_202_0);
		GraphicUtil.drawStringCenter(g, titleStr, 480, 120);

		// explain
		g.setFont(Rs.F19);
		g.setColor(Rs.C229);
		String[] explain = ui.getExplain();

		if (explain != null) {
			int posY = 242;
			if (explain.length % 2 == 0) {
				posY = posY - (explain.length / 2 * 20) + 10;
			} else {
				posY = posY - (explain.length / 2 * 20);
			}

			for (int i = 0; i < explain.length; i++) {
				GraphicUtil.drawStringCenter(g, explain[i], 480, posY + i * 20);
			}
		}

		// button
		if (c.getFocus() == 0) {
			g.setColor(Rs.C255_191_0);
			g.fillRect(233, 355, 245, 52);
			g.setColor(Rs.C157);
			g.fillRect(486, 355, 245, 52);
		} else {
			g.setColor(Rs.C157);
			g.fillRect(233, 355, 245, 52);
			g.setColor(Rs.C255_191_0);
			g.fillRect(486, 355, 245, 52);
		}

		g.setFont(Rs.F18);
		g.setColor(Rs.C3);
		String[] button1Str = TextUtil.split(modifyStr, Rs.FM18, 225, "|");
		if (button1Str.length == 1) {
			GraphicUtil.drawStringCenter(g, button1Str[0], 357, 387);
		} else {
			for (int i = 0; i < button1Str.length; i++) {
				GraphicUtil.drawStringCenter(g, button1Str[i], 357, 378 + i * 18);
			}
		}
		String[] button2Str = TextUtil.split(continueStr, Rs.FM18, 225, "|");
		if (button2Str.length == 1) {
			GraphicUtil.drawStringCenter(g, button2Str[0], 609, 387);
		} else {
			for (int i = 0; i < button2Str.length; i++) {
				GraphicUtil.drawStringCenter(g, button2Str[i], 609, 378 + i * 18);
			}
		}

		super.paint(g, c);
	}
}

/**
 * 
 */
package com.videotron.tvi.illico.isa.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class BaseRenderer extends Renderer {

	private static Hashtable footerImgTable;
	
	/**
	 * 
	 */
	public BaseRenderer() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.UIComponent)
	 */
	public Rectangle getPreferredBounds(UIComponent c) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
	 */
	public void prepare(UIComponent c) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics, com.videotron.tvi.illico.framework.UIComponent)
	 */
	protected void paint(Graphics g, UIComponent c) {

		if (Log.EXTRA_ON || Log.ALL_ON) {
			g.setFont(Rs.F16);
			g.setColor(Color.cyan);

			if (ISADataManager.getInstance().getLastURL() != null) {
				String[] urlArr = TextUtil.split(ISADataManager.getInstance().getLastURL(), Rs.FM16, 750);
				for (int i = 0; i < urlArr.length; i++) {
					if (i == 0) g.drawString("url : " + urlArr[i], 100, 100);
					else g.drawString(urlArr[i], 100, 100 + i * 20);
				}
			} else {
				g.drawString("url : " + ISADataManager.getInstance().getLastURL(), 100, 100);
			}

			if (ISADataManager.getInstance().getLastResponse() != null) {
				String[] responseArr = TextUtil.split(ISADataManager.getInstance().getLastResponse(), Rs.FM16, 750);
				for (int i = 0; i < responseArr.length; i++) {
					if (i == 0) g.drawString("response : " + responseArr[i], 100, 200);
					else g.drawString(responseArr[i], 100, 200 + i * 20);
				}
			} else {
				g.drawString("response : " + ISADataManager.getInstance().getLastResponse(), 100, 200);
			}

			
			g.drawString("errorCode : " + ISADataManager.getInstance().getErrorCode(), 100, 450);
			g.drawString("errorType : " + ISADataManager.getInstance().getErrorType(), 100, 470);
			
			if (ISADataManager.getInstance().getErrorMessage() != null) {
				String[] messages = TextUtil.split(ISADataManager.getInstance().getErrorMessage(), Rs.FM16, 750);
				
				for (int i = 0; i < messages.length; i++) {
					if (i == 0) g.drawString("errorMessage : " + messages[i], 100, 490);
					else g.drawString(messages[i], 100, 490 + i * 20);
				}
			} else {
				g.drawString("errorMessage : " + ISADataManager.getInstance().getErrorMessage(), 100, 490);
			}
			
		}
	}
	
	public static String getString(String key) {
		DataCenter dc = DataCenter.getInstance();
		
		String src = dc.getString("Label." + key);
		
		return src;
	}
	
	public static final Image getFooterImage(String key) {
        Image img;

        if (footerImgTable == null) {
            footerImgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        }

        img = (Image) footerImgTable.get(key);

        return img;
    }

}

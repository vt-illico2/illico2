/**
 * 
 */
package com.videotron.tvi.illico.isa;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.util.FontResource;

/**
 * @author zestyman
 *
 */
public class Rs {

	/** The Constant F4. */
    public static final Font F13 = FontResource.BLENDER.getFont(13);
	/** The Constant F14. */
    public static final Font F14 = FontResource.BLENDER.getFont(14);
    /** The Constant F16. */
    public static final Font F16 = FontResource.BLENDER.getFont(16);
    /** The Constant F17. */
    public static final Font F17 = FontResource.BLENDER.getFont(17);
	/** The Constant F18. */
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    /** The Constant F19. */
    public static final Font F19 = FontResource.BLENDER.getFont(19);
	/** The Constant F20. */
    public static final Font F20 = FontResource.BLENDER.getFont(20);
    /** The Constant F24. */
    public static final Font F24 = FontResource.BLENDER.getFont(24);
    
    public static final FontMetrics FM13 = FontResource.BLENDER.getFontMetrics(F13);
    public static final FontMetrics FM14 = FontResource.BLENDER.getFontMetrics(F14);
    public static final FontMetrics FM16 = FontResource.BLENDER.getFontMetrics(F16);
    public static final FontMetrics FM18 = FontResource.BLENDER.getFontMetrics(F18);
    public static final FontMetrics FM19 = FontResource.BLENDER.getFontMetrics(F19);
    
	
    
    
    
	public static final DVBColor C000_000_000_204 = new DVBColor(000, 000, 000, 204);
	
	public static final Color C250_202_0 = new Color(250, 202, 0);
	public static final Color C250_209_0 = new Color(250, 209, 0);
	public static final Color C254_234_160 = new Color(254, 234, 160);
	public static final Color C241_35_35 = new Color(241, 35, 35);
	public static final Color C236_211_143 = new Color(236, 211, 143);
	
	public static final Color C229 = new Color(229, 229, 229);
	public static final Color C190 = new Color(190, 190, 190);
	public static final Color C172 = new Color(172, 172, 172);
	public static final Color C139 = new Color(139, 139, 139);
	
	public static final Color C255 = Color.WHITE;
	public static final Color C3 = new Color(3, 3, 3);
	public static final Color C33 = new Color(33, 33, 33);
	public static final Color C35 = new Color(35, 35, 35);
	public static final Color C50 = new Color(50, 50, 50);
	public static final Color C110 = new Color(110, 110, 110);
	public static final Color C241 = new Color(241, 241, 241);
	public static final Color C44 = new Color(44, 44, 44);
	public static final Color C66 = new Color(66, 66, 66);
	public static final Color C157 = new Color(157, 157, 157);
	public static final Color C255_191_0 = new Color(255, 191, 0);

}

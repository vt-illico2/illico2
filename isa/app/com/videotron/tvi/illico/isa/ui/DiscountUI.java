package com.videotron.tvi.illico.isa.ui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.isa.data.obj.MessageContent;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.data.obj.RootMessage;
import com.videotron.tvi.illico.isa.gui.BaseRenderer;
import com.videotron.tvi.illico.isa.gui.DiscountRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

public class DiscountUI extends BaseUI {

	private DiscountRenderer renderer;
	private RootMessage replyMessage;
	private MessageContent mc;

	public DiscountUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		renderer = new DiscountRenderer();
		setRenderer(renderer);
	}

	public void start() {
		replyMessage = (RootMessage) DataCenter.getInstance().get(Def.KEY_REPLY_MESSAGE);
		mc = replyMessage.getMessageContent();
		focus = 0;
		super.start();
	}

	public void stop() {
		super.stop();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case Def.VK_LEFT:
			if (focus != 0) {
				focus = 0;
				repaint();
			}
			break;
		case Def.VK_RIGHT:
			if (focus != 1) {
				focus = 1;
				repaint();
			}
			break;
		case Def.VK_OK:
			if (focus == 0) {
				ISADataManager.getInstance()
						.requestSubscribeToService(true, ISADataManager.FORCE_SUBSCRIPTION_MODE_NONE);
			} else {
				ISAController.getInstance().hideAll();
			}
			break;
		case Def.VK_EXIT:
			ISAController.getInstance().hideAll();
			break;
		}

		return true;
	}

	public String getIconType() {

		String iconType = PopupTitle.ICON_TYPE_0;

		if (replyMessage != null && replyMessage.getMessageContent() != null
				&& replyMessage.getMessageContent().getPopupTitle() != null) {
			iconType = replyMessage.getMessageContent().getPopupTitle().getIconType();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("CompleteOrderTrackingUI: getIconType(): can't find iconType.");
			}
		}

		return iconType;
	}

	public String getTitle() {

		String title = "";
		if (mc != null && mc.getPopupTitle() != null) {
			title = mc.getPopupTitle().getValue();
		} else {
			title = BaseRenderer.getString(Def.KEY_COMPLETE_TRACKING_TITLE);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("DiscountUI: getTitle(): title=" + title);
		}
		return title;
	}

	public String[] getTopExplain() {
		String explain = "";

		if (mc != null && mc.getTopMessageText() != null) {
			explain = mc.getTopMessageText().getValue();
		}

		String[] retVal = TextUtil.split(explain, Rs.FM18, 300, '\n');

		if (Log.DEBUG_ON) {
			Log.printDebug("DiscountUI: getTopExplain(): explain=" + explain);

			for (int i = 0; i < retVal.length; i++) {
				Log.printDebug("DiscountUI: getExplain(): retVal=" + retVal[i]);
			}
		}

		return retVal;
	}
}

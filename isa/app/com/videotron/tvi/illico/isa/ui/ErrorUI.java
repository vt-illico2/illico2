package com.videotron.tvi.illico.isa.ui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.obj.MessageContent;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.data.obj.RootMessage;
import com.videotron.tvi.illico.isa.gui.BaseRenderer;
import com.videotron.tvi.illico.isa.gui.ErrorRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

public class ErrorUI extends BaseUI {

	private ErrorRenderer renderer;
	private RootMessage replyMessage;
	private MessageContent mc;

	public ErrorUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		renderer = new ErrorRenderer();
		setRenderer(renderer);
	}

	public void start() {
		replyMessage = (RootMessage) DataCenter.getInstance().get(Def.KEY_REPLY_MESSAGE);
		if (replyMessage != null) {
			mc = replyMessage.getMessageContent();
		}
		super.start();
	}

	public void stop() {
		DataCenter.getInstance().remove(Def.KEY_ERROR_POPUP_TITLE);
		DataCenter.getInstance().remove(Def.KEY_ERROR_POPUP_EXPLAIN);
		super.stop();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case Def.VK_OK:
		case Def.VK_EXIT:
			ISAController.getInstance().hideAll();
			break;
		}

		return true;
	}

	public String getIconType() {

		String iconType = PopupTitle.ICON_TYPE_2;
		if (mc == null || (mc != null && mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA500))) {
			iconType = PopupTitle.ICON_TYPE_2;
		} else if (replyMessage != null && replyMessage.getMessageContent() != null
				&& replyMessage.getMessageContent().getPopupTitle() != null) {
			iconType = replyMessage.getMessageContent().getPopupTitle().getIconType();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("ErrorUI: getIconType(): can't find iconType.");
			}
		}

		return iconType;
	}

	public String getTitle() {

		String title = "";

		if (mc == null || (mc != null && mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA500))) {
			title = BaseRenderer.getString(Def.KEY_TITLE_GENERIC_ERROR);
		} else if (mc != null && mc.getPopupTitle() != null && mc.getPopupTitle().getValue() != null
				&& mc.getPopupTitle().getValue().length() > 0) {
			title = mc.getPopupTitle().getValue();
		} else {
			title = BaseRenderer.getString(Def.KEY_TITLE_GENERIC_ERROR);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ErrorUI: getTitle(): title=" + title);
		}

		return title;
	}

	public String[] getExplain() {
		String explain = "";

		if (mc == null || (mc != null && mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA500))) {
			explain = BaseRenderer.getString(Def.KEY_MESSAGE_GENERIC_ERROR);
		} else if (mc != null && mc.getTopMessageText() != null) {
			explain = mc.getTopMessageText().getValue();
		} else {
			explain = BaseRenderer.getString(Def.KEY_MESSAGE_GENERIC_ERROR);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ErrorUI: getExplain(): explain=" + explain);
		}

		String[] retVal = TextUtil.split(explain, Rs.FM19, 360, '|');

		return retVal;
	}
	
	public String getButtonName() {
		String name = "";

		if (mc == null || (mc != null && mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA500))) {
			name = BaseRenderer.getString(Def.KEY_CLOSE);
		} else {
			name = BaseRenderer.getString(Def.KEY_OK);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ErrorUI: getButtonName(): name=" + name);
		}

		return name;
	}
}

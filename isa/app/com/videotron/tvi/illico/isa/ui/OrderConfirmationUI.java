package com.videotron.tvi.illico.isa.ui;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.data.obj.RootMessage;
import com.videotron.tvi.illico.isa.gui.OrderConfirmationRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

public class OrderConfirmationUI extends BaseUI {

	private OrderConfirmationRenderer renderer;
	private RootMessage replyMessage;

	public OrderConfirmationUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		renderer = new OrderConfirmationRenderer();
		setRenderer(renderer);
	}

	public void start() {
		replyMessage = (RootMessage) DataCenter.getInstance().get(Def.KEY_REPLY_MESSAGE);
		super.start();
	}

	public void stop() {
		super.stop();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case Def.VK_OK:
		case Def.VK_EXIT:
			ISAController.getInstance().hideAll();
			break;
		}

		return true;
	}

	public String getIconType() {
		
		String iconType = PopupTitle.ICON_TYPE_0;
		
		if (replyMessage != null && replyMessage.getMessageContent() != null && replyMessage.getMessageContent().getPopupTitle() != null) {
			iconType = replyMessage.getMessageContent().getPopupTitle().getIconType();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("OrderTrackingUI: getIconType(): can't find iconType.");
			}
		}
		
		return iconType;
	}
	
	public String getTitle() {
		String retVal = "";
		
		if (replyMessage != null && replyMessage.getMessageContent() != null && replyMessage.getMessageContent().getPopupTitle() != null) {
			retVal = replyMessage.getMessageContent().getPopupTitle().getValue();
		}
		return retVal;
	}

	public String[] getTopExplain() {

		String message = "";
		
		if (replyMessage != null && replyMessage.getMessageContent() != null && replyMessage.getMessageContent().getTopMessageText() != null) {
			message = replyMessage.getMessageContent().getTopMessageText().getValue();
		}

		String[] retVal = TextUtil.split(message, Rs.FM19, 360, '\n');
		return retVal;
	}

	public String[] getBottomExplain() {

		String message = "";

		if (replyMessage != null && replyMessage.getMessageContent() != null && replyMessage.getMessageContent().getBottomMessageText() != null) {
			message = replyMessage.getMessageContent().getBottomMessageText().getValue();
		}

		String[] retVal = TextUtil.split(message, Rs.FM18, 360, '\n');
		return retVal;
	}
}

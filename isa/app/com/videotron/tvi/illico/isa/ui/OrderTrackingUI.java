/**
 * 
 */
package com.videotron.tvi.illico.isa.ui;

import java.awt.Image;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.isa.data.obj.MessageContent;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.data.obj.RootMessage;
import com.videotron.tvi.illico.isa.data.obj.ServiceDescriptionResponse;
import com.videotron.tvi.illico.isa.gui.BaseRenderer;
import com.videotron.tvi.illico.isa.gui.OrderTrackingRenderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class OrderTrackingUI extends BaseUI {

	private OrderTrackingRenderer renderer;
	
	private Footer footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
	private StringBuffer phoneNumber = new StringBuffer();
	private StringBuffer displayPhoneNumber = new StringBuffer();
	
	private ServiceDescriptionResponse sdResponse;
	private RootMessage	replyMessage;
	
	private String cancelStr;
	
	private boolean isInvaild;
	
	/**
	 * x
	 */
	public OrderTrackingUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		renderer = new OrderTrackingRenderer();
		setRenderer(renderer);
		Image exitImg = BaseRenderer.getFooterImage(PreferenceService.BTN_EXIT);
		cancelStr = DataCenter.getInstance().getString("Label." + Def.KEY_CANCEL);
		footer.addButtonWithLabel(exitImg, cancelStr);
		footer.setBounds(228, 430, 505, 24);
		add(footer);
	}

	public void start() {
		clearInputPhoneNumber();
		focus = 0;
		isInvaild = false;
		sdResponse = (ServiceDescriptionResponse) DataCenter.getInstance().get(Def.KEY_SERVICE_DESCRIPTION_RESPONSE);
		replyMessage = (RootMessage) DataCenter.getInstance().get(Def.KEY_REPLY_MESSAGE);
		
		String pNum = "";
		MessageContent mc = null;
		
		if (replyMessage != null) {
			mc = replyMessage.getMessageContent();
			pNum = replyMessage.getPhoneNumber();
		} else if (sdResponse != null) {
			mc = sdResponse.getMessageContent();
			pNum = sdResponse.getPhoneNumber();
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("OrderTrackingUI: start(): mc=" + mc + ", pNum=" + pNum);
		}
			
		if (pNum != null && pNum.length() > 0) {
			StringBuffer sf = new StringBuffer();
			
			char[] chaNum = pNum.toCharArray();
			
			for (int i = 0; i < chaNum.length; i++) {
				if (chaNum[i] >= 48 && chaNum[i] <= 57) {
					sf.append(chaNum[i]);
				}
			}
			
			for (int i = 0; i < sf.toString().length(); i++) {
				addInputNumber(sf.toString().substring(i, i + 1));
			}
			
			if (phoneNumber.length() == 0) {
				focus = 1;
			}
		}
		super.start();
	}
	
	public void stop() {
		super.stop();
	}
	
	public boolean handleKey(int code) {
		
		switch (code) {
		case Def.VK_UP:
			if (focus != 0) {
				focus = 0;
				repaint();
			}
			break;
		case Def.VK_DOWN:
			if (focus == 0) {
				focus = 1;
				repaint();
			}
			break;
		case Def.VK_LEFT:
			if (focus == 2) {
				focus = 1;
				repaint();
			}
			break;
		case Def.VK_RIGHT:
			if (focus == 1 && getInputPhoneNumberSize() > 0) {
				focus = 2;
				repaint();
			}
			break;
		case OCRcEvent.VK_0:
		case OCRcEvent.VK_1:
		case OCRcEvent.VK_2:
		case OCRcEvent.VK_3:
		case OCRcEvent.VK_4:
		case OCRcEvent.VK_5:
		case OCRcEvent.VK_6:
		case OCRcEvent.VK_7:
		case OCRcEvent.VK_8:
		case OCRcEvent.VK_9:
			if (getInputPhoneNumberSize() < 10) {
				int inputedNumber = code - OCRcEvent.VK_0;
				
				addInputNumber(String.valueOf(inputedNumber));
				
				if (getInputPhoneNumberSize() == 10) {
					focus = 1;
				}
				
				repaint();
			}
			
			break;
		case Def.VK_OK:
			if (focus == 1) {
				// call me
				if (phoneNumber.length() == 10) {
					ISADataManager.getInstance().requestRegisterTracking(phoneNumber.toString());
				} else {
					isInvaild = true;
					focus = 0;
					repaint();
				}
			} else if (focus == 2) {
				// clear
				clearInputPhoneNumber();
				focus = 0;
				repaint();
				
			}
			break;
		case Def.VK_EXIT:
			footer.clickAnimation(0);
			ISAController.getInstance().hideAll();
			break;
		}
		return true;
	}
	
	public String getIconType() {
		MessageContent mc = null;
		if (replyMessage != null) {
			mc = replyMessage.getMessageContent();
		} else if (sdResponse != null) {
			mc = sdResponse.getMessageContent();
		}
		
		String iconType = PopupTitle.ICON_TYPE_0;
		
		if (mc != null && mc.getPopupTitle() != null) {
			iconType = mc.getPopupTitle().getIconType();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("OrderTrackingUI: getIconType(): can't find iconType.");
			}
		}
		
		return iconType;
	}
	
	public String getTitle() {
		
		MessageContent mc = null;
		if (replyMessage != null) {
			mc = replyMessage.getMessageContent();
		} else if (sdResponse != null) {
			mc = sdResponse.getMessageContent();
		}
		
		String title = "";
		if (mc != null) {
			if (mc.getPopupTitle() != null) {
				title = mc.getPopupTitle().getValue();
			} else {
				title = BaseRenderer.getString(Def.KEY_TITLE_ERROR);
			}
		} else {
			title = BaseRenderer.getString(Def.KEY_TITLE_ERROR);
		}
	
		return title;
	}
	
	public String[] getTopExplain() {
		
		MessageContent mc = null;
		if (replyMessage != null) {
			mc = replyMessage.getMessageContent();
		} else if (sdResponse != null) {
			mc = sdResponse.getMessageContent();
		}
		
		
		String message = "";
		
		if (isInvaild) {
			message = BaseRenderer.getString(Def.KEY_MESSAGE_INVAILD_PHONE);
		} else {
			if (mc != null) {
				
				if (mc.getTopMessageText() != null) {
					message = mc.getTopMessageText().getValue();
				}
				
				if (Log.DEBUG_ON) {
					Log.printDebug("OrderTrackingUI: getTopExplain(): message=" + message);
				}
				
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("OrderTrackingUI: getTopExplain(): message=" + message);
				}
				message = BaseRenderer.getString(Def.KEY_MESSAGE_PROBLEM_SUBMTTING_ORDER);
			}
		}
		
		String[] retVal = TextUtil.split(message, Rs.FM18, 460, '|');
		return retVal;
	}
	
	public String[] getBottomExplain() {
		MessageContent mc = null;
		if (replyMessage != null) {
			mc = replyMessage.getMessageContent();
		} else if (sdResponse != null) {
			mc = sdResponse.getMessageContent();
		}
		
		
		String message = "";
		
		if (isInvaild) {
			message = BaseRenderer.getString(Def.KEY_MESSAGE_INVAILD_PHONE);
		} else {
			if (mc != null) {
				if (mc.getBottomMessageText() != null) {
					message = mc.getBottomMessageText().getValue();
				}
				
				if (Log.DEBUG_ON) {
					Log.printDebug("OrderTrackingUI: getBottomExplain(): message=" + message);
				}
				
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("OrderTrackingUI: getBottomExplain(): message=" + message);
				}
				message = BaseRenderer.getString(Def.KEY_ENTER_YOUR_PHONE_NUMBER);
			}
		}
		
		String[] retVal = TextUtil.split(message, Rs.FM18, 460, '|');
		return retVal;
	}
	
	public String getInputPhoneNumber() {
		
		return displayPhoneNumber.toString();
	}
	
	public void addInputNumber(String number) {
		phoneNumber.append(number);
		
		if (phoneNumber.length() == 4 || phoneNumber.length() == 7) {
			displayPhoneNumber.append(" - ");
		}
		displayPhoneNumber.append(number);
		
	}
	
	public void clearInputPhoneNumber() {
		phoneNumber.setLength(0);
		displayPhoneNumber.setLength(0);
	}
	
	public int getInputPhoneNumberSize() {
		return phoneNumber.length();
	}
}

/**
 * 
 */
package com.videotron.tvi.illico.isa.ui;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.isa.data.URLCreator;
import com.videotron.tvi.illico.isa.data.obj.ServiceDescriptionResponse;
import com.videotron.tvi.illico.isa.gui.BaseRenderer;
import com.videotron.tvi.illico.isa.gui.SubscribeRenderer;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class SubscribeUI extends BaseUI implements DataUpdateListener, PinEnablerListener {

	private SubscribeRenderer renderer;
	
	private ServiceDescriptionResponse sdResponse;
	
	private ScrollTexts scrollTexts = new ScrollTexts();
	private Footer footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
	
	private String monthStr;
	/**
	 * 
	 */
	public SubscribeUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		renderer = new SubscribeRenderer();
		setRenderer(renderer);
		
		monthStr = BaseRenderer.getString(Def.KEY_MONTH);
		
		scrollTexts.setForeground(Rs.C190);
        scrollTexts.setRowHeight(18);
        scrollTexts.setFont(Rs.F16);
        scrollTexts.setInsets(0, 0, 0, 0);
        scrollTexts.setBounds(189, 251, 588, 54);
        scrollTexts.setRows(3);
//        scrollTexts.setTopMaskImage(DataCenter.getInstance().getImage("isa_pop_sh_t.png"));
        scrollTexts.setBottomMaskImagePosition(0, 0);
//        scrollTexts.setBottomMaskImage(DataCenter.getInstance().getImage("isa_pop_sh_b.png"));
        scrollTexts.setBottomMaskImagePosition(0, 36);
        
        scrollTexts.setCenterAlign(true);
        scrollTexts.setVisible(true);
        
        footer.setBounds(558, 490, 220, 21);
        footer.reset();
        footer.addButtonWithLabel(BaseRenderer.getFooterImage(PreferenceService.BTN_PAGE), BaseRenderer.getString(Def.KEY_BTN_SCROLL));
        this.add(footer);
        this.add(scrollTexts);
	}
	
	public void start() {
		//Image channelImg = DataCenter.getInstance().getImage("01_chbg_TMN_S.png");
		
		//renderer.setChannelImage(channelImg);
		focus = 0;
		sdResponse = (ServiceDescriptionResponse) DataCenter.getInstance().get(Def.KEY_SERVICE_DESCRIPTION_RESPONSE);
		
		String description = sdResponse.getServiceDescription().getDescription(ISADataManager.getInstance().getCurrentLocale()).getFullDescription().getValue();
		
//		String description = "lkasjdflkjalfjkslakwe kas;ldfkj ;qwlkjf ealdjks flkajsdf;lajks fd;wioj flajksfdlkjsjf;l ajksf;lajks f;l asdf;jk s;aldfks ;alfjks dfj ;alwjkf;aljksf;lafjks;afjks;lafjks ;alfjkslkadsjflkajsf;lajksdf;ljkasfd;ljkas ;dfl;l sadjkf;lajsdf;ljas;dfj;aldfjks;aljksf;fl ;dksj asdfl jasdfa;ldjksf;dlajksdf;lajksdflsajflkjslfjkslajksfdfjs ;a as; fd;as";
//		description = "lkasjdflajsdf ladfj lafjsdla dfjs";
//		description = "lkasjdflajsdf ladfj lafjsdla dfjs asdfkj aldfjsla jsdflajk sfdl kjasdfl jasdfkl a laksdf las dfl asfdl ajksfl jasjsfjldfjs lsd flkajs fdlkasjfdlas jflajs fl ajsfjsflkjasfjalfjkaldfjslkafjslajsfldajksf lkajs dfla fdsaldfksj aldjksfjks";
		scrollTexts.setContents(description);
		
		if (scrollTexts.getLineCount() > 3) {
			footer.setVisible(true);
		} else {
			footer.setVisible(false);
		}
		
		DataCenter.getInstance().addDataUpdateListener(Def.KEY_REMOTE_IMAGE, this);
		Thread t = new Thread(new Runnable() {
			public void run() {
				String imgName = sdResponse.getServiceDescription().getLogoFileName();
				String imgUrl = URLCreator.getImageURL(imgName);
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ImageRequestThread: url=" + imgUrl);
				}
				byte[] src;
				try {
					
					boolean useProxy = DataCenter.getInstance().getBoolean("USE_PROXY");
					String proxyIP = DataCenter.getInstance().getString("PROXY_IP");
					int port = DataCenter.getInstance().getInt("PORT");
					
					if (Log.DEBUG_ON) {
						Log.printDebug("SubscribeUI: request image: useProxy=" + useProxy);
						Log.printDebug("SubscribeUI: request image: proxyIP=" + proxyIP + ", port=" + port);
					}
					if (useProxy && proxyIP != null) {
						src = URLRequestor.getBytes(new String[] {proxyIP}, new int[] {port}, imgUrl, null);
					} else {
						src = URLRequestor.getBytes(imgUrl, null);
					}
					
					Image remoteImg = FrameworkMain.getInstance().getImagePool().createImage(src, Def.KEY_REMOTE_IMAGE);
					DataCenter.getInstance().put(Def.KEY_REMOTE_IMAGE, remoteImg);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		
		t.start();
		
		super.start();
	}
	
	public void stop() {
		super.stop();
	}

	
	public boolean handleKey(int keyCode) {
		
		switch (keyCode) {
		case Def.VK_LEFT:
			if (focus == 1) {
				focus = 0;
				repaint();
			}
			break;
		case Def.VK_RIGHT:
			if (focus == 0) {
				focus = 1;
				repaint();
			}
			break;
		case Def.VK_PAGE_UP:
			footer.clickAnimation(0);
			scrollTexts.showPreviousPage();
			break;
		case Def.VK_PAGE_DOWN:
			footer.clickAnimation(0);
			scrollTexts.showNextPage();
			break;
		case Def.VK_OK:
			if (focus == 0) {
				// try to subscribe
				
				PreferenceService pService = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
				
				if (pService != null) {
					try {
						setVisible(false);
						String explain = BaseRenderer.getString(Def.KEY_PIN_EXPLAIN);
						if (ISADataManager.getInstance().isIllicoService()) {
							String serviceName = BaseRenderer.getString(Def.KEY_CLUB_UNLIMITED);
							explain = TextUtil.replace(explain, Def.CH_NAME, serviceName);
						} else {
							String serviceId = ISADataManager.getInstance().getCurrentServiceId();
							explain = TextUtil.replace(explain, Def.CH_NAME, serviceId);
						}
						
						String[] message = TextUtil.split(explain, Rs.FM18, 310, '|');
						
						pService.showPinEnabler(this, message);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
				
			} else if (focus == 1) {
				// close
				//ISAController.getInstance().showUIComponent(Def.SCENE_ID_COMPLETE_ORDER_TRACKING);
				//ISAController.getInstance().showUIComponent(Def.SCENE_ID_ORDER_CONFIRMATION);
				ISAController.getInstance().hideAll();
			}
			break;
		case Def.VK_EXIT:
			ISAController.getInstance().hideAll();
			break;
		}
		
		return true;
	}
	
	public String getPopupTitle() {
		
		String title = "";
		try {
			title = sdResponse.getServiceDescription().getDescription(ISADataManager.getInstance().getCurrentLocale()).getPopupTitle().getValue();
		} catch (Exception e) {
			Log.printDebug("getPopupTitle, occur a exception");
			Log.printWarning(e);
		}
		
		return title;
	}
	
	public String getCallToActionText() {
		
		String description = "";
		try {
			description = sdResponse.getServiceDescription().getDescription(ISADataManager.getInstance().getCurrentLocale()).getCallToActionText().getValue();;
		} catch (Exception e){
			Log.printDebug("getCallToActionText, occur a exception");
			Log.printWarning(e);
		}
		return description; 
	}
	
	public String[] getFullDescription() {
		String description = "";
		
		try {
			description = sdResponse.getServiceDescription().getDescription(ISADataManager.getInstance().getCurrentLocale()).getFullDescription().getValue();
		} catch (Exception e) {
			Log.printDebug("getFullDescription, occur a exception");
			Log.printWarning(e);
		}
		String[] retStr = TextUtil.split(description, Rs.FM16, 588);
		return retStr;
	}
	
	public String[] getShortDescription() {
		String description = "";
		
		try {
			description = sdResponse.getServiceDescription().getDescription(ISADataManager.getInstance().getCurrentLocale()).getShortDescription().getValue();
		} catch (Exception e) {
			Log.printDebug("getShortDescription, occur a exception");
			Log.printWarning(e);
		}
		String price = sdResponse.getServiceDescription().getPrice();
		
		String[] retStr = new String[2];
		retStr[0] = description;
		retStr[1] = "$" + price + " / " + monthStr + "*";
		return retStr;
	}
	
	public String[] getFinePrint() {
		String finePrint = TextUtil.EMPTY_STRING;
		
		if (sdResponse.getGeneralInformation().getFapl() != null) {
			finePrint = sdResponse.getGeneralInformation().getFapl().getFinePrint(ISADataManager.getInstance().getCurrentLocale()).getValue();
		}
		String legalText = sdResponse.getGeneralInformation().getLegalText().getDescription(ISADataManager.getInstance().getCurrentLocale()).getValue();
		
		if (finePrint != null && finePrint.length() > 0) {
			String[] retStr = TextUtil.split("* " + legalText + " " + finePrint, Rs.FM13, 588);
			return retStr;
		} else {
			String[] retStr = TextUtil.split("* " + legalText, Rs.FM13, 588);
			return retStr;
		}
		
//		String[] retStr = TextUtil.split("* " + "Cette offre d'une duree limitee s'adresse aux clients qul s'abonnent au Clum illico et qul n'ont pas profite de l'offre du premler mois gratuit au cours des 12 dernlers mois. Le client doit souscrire a un abonnement mensuel de 9,99 $ pour avoir acces au club illico via illico teie nouvelle generation ou illico.tv sur le web et sur tablette. La disponbilite des films et des series differe selon les platef dkdjdkasl sldkfjsadkjflaksdjfkdldk temps. L'acces internet. y compris la brands passante. est a la charge du client. L;abonnement confere le droit", Rs.FM13, 588);
//		retStr = TextUtil.split("* " + "Cette offre d'une duree limitee s'adresse aux clients qul s'abonnent au Clum illico et qul n'ont pas profite de l'offre du premler", Rs.FM13, 588);
//		return retStr;
	}

	public void dataRemoved(String key) {
		
	}

	public void dataUpdated(String key, Object old, Object value) {
		
		if (key.equalsIgnoreCase(Def.KEY_REMOTE_IMAGE)) {
			Image channelBannerImg = (Image)value;
			renderer.setChannelImage(channelBannerImg);
			repaint();
		}
	}

	public void receivePinEnablerResult(int response, String detail) throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("CYOPoupUI: receivePinEnablerResult: response=" + response + ", detail="+ detail);
		}
		
		if (response == PreferenceService.RESPONSE_SUCCESS && detail.equals("admin")) {

			// When the ISA channel is combinable and DNCS new subscription Package is present
			// , the optimisation screen shall be displayed.
			if (ISADataManager.getInstance().isCombinable() && ISADataManager.getInstance().hasDNCSPackage()) {
				ISAController.getInstance().showUIComponent(Def.SCENE_ID_OPTIMIZATION);
			} else {
				ISADataManager.getInstance().requestSubscribeToService(false,
						ISADataManager.FORCE_SUBSCRIPTION_MODE_NONE);
			}
			
		} else if (response == PreferenceService.RESPONSE_CANCEL) {
			setVisible(true);
		}
	}
}

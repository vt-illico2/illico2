package com.videotron.tvi.illico.isa.ui;

import java.awt.FontMetrics;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.obj.MessageContent;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.data.obj.RootMessage;
import com.videotron.tvi.illico.isa.gui.BaseRenderer;
import com.videotron.tvi.illico.isa.gui.CompleteOrderTrackingRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

public class CompleteOrderTrackingUI extends BaseUI {

	private CompleteOrderTrackingRenderer renderer;
	private RootMessage replyMessage;
	private MessageContent mc;

	public CompleteOrderTrackingUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		renderer = new CompleteOrderTrackingRenderer();
		setRenderer(renderer);
	}

	public void start() {
		replyMessage = (RootMessage) DataCenter.getInstance().get(Def.KEY_REPLY_MESSAGE);
		mc = replyMessage.getMessageContent();
		super.start();
	}

	public void stop() {
		super.stop();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case Def.VK_OK:
		case Def.VK_EXIT:
			ISAController.getInstance().hideAll();
			break;
		}

		return true;
	}

	public String getIconType() {

		String iconType = PopupTitle.ICON_TYPE_0;

		if (replyMessage != null && replyMessage.getMessageContent() != null
				&& replyMessage.getMessageContent().getPopupTitle() != null) {
			iconType = replyMessage.getMessageContent().getPopupTitle().getIconType();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("CompleteOrderTrackingUI: getIconType(): can't find iconType.");
			}
		}

		return iconType;
	}

	public String getTitle() {

		String title = "";
		if (mc != null && mc.getPopupTitle() != null) {
			title = mc.getPopupTitle().getValue();
		} else {
			title = BaseRenderer.getString(Def.KEY_COMPLETE_TRACKING_TITLE);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("CompleteOrderTrackingUI: getTitle(): title=" + title);
		}
		return title;
	}

	public String[] getExplain() {
		String explain = "";
		if (mc != null && mc.getTopMessageText() != null && mc.getTopMessageText().getValue().length() > 0) {
			explain = mc.getTopMessageText().getValue();
		} else if (mc != null && mc.getBottomMessageText() != null && mc.getBottomMessageText().getValue().length() > 0) {
			explain = mc.getBottomMessageText().getValue();
		} else {
			explain = BaseRenderer.getString(Def.KEY_COMPLETE_TRACKING_EXPLAIN);
		}

		String[] retVal = TextUtil.split(explain, Rs.FM18, 300, '\n');

		if (Log.DEBUG_ON) {
			Log.printDebug("CompleteOrderTrackingUI: getExplain(): explain=" + explain);

			for (int i = 0; i < retVal.length; i++) {
				Log.printDebug("CompleteOrderTrackingUI: getExplain(): retVal=" + retVal[i]);
			}
		}

		return retVal;
	}
}

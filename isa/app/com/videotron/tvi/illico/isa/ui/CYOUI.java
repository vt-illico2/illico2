/**
 *
 */
package com.videotron.tvi.illico.isa.ui;

import java.awt.Image;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.isa.gui.BaseRenderer;
import com.videotron.tvi.illico.isa.gui.CYORenderer;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 */
public class CYOUI extends BaseUI implements PinEnablerListener {

	private CYORenderer renderer;

	/**
	 *
	 */
	public CYOUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		renderer = new CYORenderer();
		setRenderer(renderer);
	}

	public void start() {
		focus = 0;
		super.start();
	}

	public void stop() {
		super.stop();
	}

	public void dispose() {

		super.dispose();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case Def.VK_LEFT:
			if (focus == 1) {
				focus = 0;
				repaint();
			}
			break;
		case Def.VK_RIGHT:
			if (focus == 0) {
				focus = 1;
				repaint();
			}
			break;
		case Def.VK_OK:
			if (focus == 0) {
				// launch CYO application.
				PreferenceService pService = (PreferenceService) DataCenter.getInstance()
						.get(PreferenceService.IXC_NAME);

				if (pService != null) {
					try {
						setVisible(false);
						String explain = BaseRenderer.getString(Def.KEY_PIN_EXPLAIN);
						String serviceId = ISADataManager.getInstance().getCurrentServiceId();
						explain = TextUtil.replace(explain, Def.CH_NAME, serviceId);
						String[] message = TextUtil.split(explain, Rs.FM18, 310, '|');

						pService.showPinEnabler(this, message);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			} else if (focus == 1) {
				// close
				ISAController.getInstance().hideAll();
			}

			break;
		case Def.VK_EXIT:
		case Def.VK_BACK:
			ISAController.getInstance().hideAll();
			break;
		}
		return true;
	}

	public String getTitle() {
		String prefix = BaseRenderer.getString(Def.KEY_CYO_TITLE_PREFIX);
		String subfix = BaseRenderer.getString(Def.KEY_CYO_TITLE_SUBFIX);
		String serviceId = ISADataManager.getInstance().getCurrentServiceId();

		if (ISADataManager.getInstance().getCurrentEntryPoint().equals(ISAService.ENTRY_POINT_VOD_E04) || ISADataManager
				.getInstance().getCurrentEntryPoint().equals(ISAService.ENTRY_POINT_VOD_E05)) {

			serviceId = BaseRenderer.getString(Def.KEY_CLUB_UNLIMITED);
		}

		String retVal = prefix + serviceId + subfix;
		return retVal;
	}

	public String[] getExplain() {
		String[] retVal;

		retVal = TextUtil.split(BaseRenderer.getString(Def.KEY_CYO_EXPLAIN), Rs.FM18, 300);

		return retVal;
	}

	public Image getChannelLogoImage() {
		Image logo;
		logo = (Image) SharedMemory.getInstance().get("logo." + ISADataManager.getInstance().getCurrentServiceId());

		if (logo == null) {
			return (Image) SharedMemory.getInstance().get("logo.default");
		} else {
			return logo;
		}
	}

	public void receivePinEnablerResult(int response, String detail) throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("CYOPoupUI: receivePinEnablerResult: response=" + response + ", detail=" + detail);
		}

		if (response == PreferenceService.RESPONSE_SUCCESS && detail.equals("admin")) {
			ISAController.getInstance().hideAll();
			MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

			if (mService != null) {
				mService.startUnboundApplication("Options",
						new String[] { "ISA", ISADataManager.getInstance().getParentApplicationName() });
			}
		} else if (response == PreferenceService.RESPONSE_CANCEL) {
			setVisible(true);
		}
	}
}

package com.videotron.tvi.illico.isa.ui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.Rs;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.isa.gui.BaseRenderer;
import com.videotron.tvi.illico.isa.gui.OptimizationRenderer;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

import java.rmi.RemoteException;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-07-08.
 */
public class OptimizationUI extends BaseUI implements PinEnablerListener {

	public OptimizationUI() {
		setBounds(Constants.SCREEN_BOUNDS);
		setRenderer(new OptimizationRenderer());
	}

	public void prepare() {
		super.prepare();
	}

	public void start() {
		super.start();
	}

	public void stop() {
		super.stop();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case Def.VK_LEFT:
			focus = 0;
			repaint();
			return true;
		case Def.VK_RIGHT:
			focus = 1;
			repaint();
			return true;
		case Def.VK_OK:
			// launch CYO application.
			if (focus == 0) {
				// VDTRMASTER-5978
				ISAController.getInstance().hideAll();
				MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
				try {
					mService.startUnboundApplication("Options", new String[] {"ISA",
							ISADataManager.getInstance().getParentApplicationName(), "package"});
				} catch (RemoteException e) {
					e.printStackTrace();
					Log.print(e);
				}
			} else {
				ISADataManager.getInstance().requestSubscribeToService(false,
						ISADataManager.FORCE_SUBSCRIPTION_MODE_TRUE);
			}
			return true;
		case Def.VK_EXIT:
			ISAController.getInstance().hideAll();
			return true;
		}

		return super.handleKey(code);
	}

	public String[] getExplain() {
		String[] retVal;

		String explainStr = BaseRenderer.getString(Def.KEY_OPTIMIZATION_EXPLAIN);

		String serviceName = ISADataManager.getInstance().getCurrentServiceId();
		if (ISADataManager.getInstance().getCurrentEntryPoint().equals(ISAService.ENTRY_POINT_VOD_E04) || ISADataManager
				.getInstance().getCurrentEntryPoint().equals(ISAService.ENTRY_POINT_VOD_E05)) {

			serviceName = BaseRenderer.getString(Def.KEY_CLUB_UNLIMITED);
		} else {
			EpgService eService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);

			if (eService != null) {
				try {
					TvChannel ch = eService.getChannel(ISADataManager.getInstance().getCurrentServiceId());

					if (ch != null) {
						serviceName = ch.getFullName();
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("OptimizationUI, getExplain, serviceName=" + serviceName);
		}

		if (serviceName != null && serviceName != TextUtil.EMPTY_STRING) {
			explainStr = TextUtil.replace(explainStr, Def.CH_NAME, serviceName);
		} else {
			explainStr = TextUtil.replace(explainStr, Def.CH_NAME, ISADataManager.getInstance().getCurrentServiceId());
		}

		retVal = TextUtil.split(explainStr, Rs.FM19, 435, "|");

		return retVal;
	}

	public void receivePinEnablerResult(int response, String detail) throws RemoteException {

		if (Log.DEBUG_ON) {
			Log.printDebug("OptimizationUI: receivePinEnablerResult: response=" + response + ", detail="+ detail);
		}

		if (response == PreferenceService.RESPONSE_SUCCESS && detail.equals("admin")) {
			ISAController.getInstance().hideAll();
			MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

			if (mService != null) {

				if (focus == 0) {
					mService.startUnboundApplication("Options", new String[] {"ISA",
							ISADataManager.getInstance().getParentApplicationName(), "package"});
				} else {
					ISADataManager.getInstance().requestSubscribeToService(false,
							ISADataManager.FORCE_SUBSCRIPTION_MODE_TRUE);
				}
			}
		} else if (response == PreferenceService.RESPONSE_CANCEL) {
			setVisible(true);
		}
	}
}

/**
 * @(#)App.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.isa;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.log.Log;

/**
 * implements Xlet of ISA Application.
 * @author zestyman
 * @version 1.1
 */
public class App implements Xlet, ApplicationConfig {
    
    public static String NAME = "ISA";
    /**
     * The instance of XletContext.
     */
    private XletContext xletContext;

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional If unconditional is true when this method is called, requests by the Xlet to not enter the
     *            destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *             state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("ISA: destroyXlet");
        }
        FrameworkMain.getInstance().destroy();
        ISAController.getInstance().dispose();
    }

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param xContext The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext xContext) throws XletStateChangeException {
        this.xletContext = xContext;
        FrameworkMain.getInstance().init(this);
        if (Log.INFO_ON) {
            Log.printInfo("ISA: initXlet");
        }
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        if (Log.INFO_ON) {
            Log.printInfo("ISA: pauseXlet");
        }
        FrameworkMain.getInstance().pause();
        ISAController.getInstance().stop();
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("ISA: startXlet");
        }
        FrameworkMain.getInstance().start();
        ISAController.getInstance().start();
    }

    /**
     * Return a name of ISA
     * @return The name of application
     */
    public String getApplicationName() {
        return NAME;
    }

    /**
     * Return a version of ISA.
     * @return The version of application
     */
    public String getVersion() {
        return "(R7.5).0.1";
    }

    /**
     * Return a XletContext of ISA application.
     * @return The XletContext for Application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

	public void init() {
		ISAController.getInstance().init(xletContext);
	}
}

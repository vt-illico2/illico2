/**
 *
 */
package com.videotron.tvi.illico.isa.controller;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.isa.ui.*;
import org.dvb.event.UserEvent;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.communication.CommunicationManager;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.isa.data.OOBDataManager;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * @author zestyman
 */
public class ISAController extends LayeredWindow implements DataUpdateListener, PreferenceListener, LayeredKeyHandler {

	private static ISAController instance;

	/**
	 * The implement class for LogLevelListener.
	 */
	private LogLevelAdapter logListener;

	/**
	 * The LayeredUI to use PinEnablerUI.
	 */
	private LayeredUI layeredUI;

	/**
	 * if exist, a current UIcomponet.
	 */
	private UIComponent currentUI;

	private Hashtable uiTable;

	private LoadingAnimationService loadingService;

	private boolean isLoading;

	/**
	 *
	 */
	private ISAController() {
	}

	public static synchronized ISAController getInstance() {

		if (instance == null) {
			instance = new ISAController();
		}

		return instance;
	}

	public void init(XletContext xContext) {

		setBounds(Constants.SCREEN_BOUNDS);

		uiTable = new Hashtable();

		layeredUI = WindowProperty.ISA.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);

		logListener = new LogLevelAdapter();

		DataCenter.getInstance().addDataUpdateListener(PreferenceService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(MonitorService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(StcService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(VbmService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(LoadingAnimationService.IXC_NAME, this);

		CommunicationManager.getInstance().start(xContext);

		OOBDataManager.getInstance().init();
	}

	public void start() {

	}

	public void stop() {

	}

	public void dispose() {
		ISADataManager.getInstance().dispose();
	}

	public void showSubscriptionPopup(String serviceId, String entryPoint) {
		if (Log.DEBUG_ON) {
			Log.printDebug(
					"ISAController: showSubscriptionPopup(): serviceId=" + serviceId + ", entryPoint=" + entryPoint);
		}

		if (ISADataManager.getInstance().checkISAService(serviceId, entryPoint)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ISAController: showSubscriptionPopup(): serviceId=" + serviceId
						+ "is included in ISA service list");
			}
			ISADataManager.getInstance().requestServiceInformation(serviceId, entryPoint);
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("ISAController: showSubscriptionPopup(): serviceId=" + serviceId
						+ " is NOT included in ISA service list");
			}
			ISADataManager.getInstance().setCurrentServiceId(serviceId);
			ISADataManager.getInstance().setCurrentEntryPoint(entryPoint);

			// Bundling
			// check whether channel is included in excluded channels and excluded types
			if (ISADataManager.getInstance().isIncludedInExcludedChannels(serviceId) || ISADataManager.getInstance()
					.isIncludedInExcludedChannelTypes(serviceId)) {
				showUIComponent(Def.SCENE_ID_CYO);
			} else {
				showUIComponent(Def.SCENE_ID_REGULAR_ISA);
			}
		}
	}

	public void showUIComponent(int sceneId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISAController: showUIComponent(): sceneId=" + sceneId);
		}
		UIComponent component;

		if (uiTable.containsKey(String.valueOf(sceneId))) {
			component = (UIComponent) uiTable.get(String.valueOf(sceneId));
		} else {
			component = createUIComponent(sceneId);
		}

		if (currentUI != null) {
			remove(currentUI);

			currentUI.stop();
		}

		currentUI = component;

		currentUI.start();
		add(currentUI);
		currentUI.repaint();

		if (!layeredUI.isActive()) {
			layeredUI.activate();
		}

	}

	public UIComponent createUIComponent(int sceneId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISAController: createUIComponent(): sceneId=" + sceneId);
		}
		UIComponent newUIComponent = null;

		if (sceneId == Def.SCENE_ID_SUBSCRIBE) {
			newUIComponent = new SubscribeUI();
		} else if (sceneId == Def.SCENE_ID_CYO) {
			newUIComponent = new CYOUI();
		} else if (sceneId == Def.SCENE_ID_ORDER_CONFIRMATION) {
			newUIComponent = new OrderConfirmationUI();
		} else if (sceneId == Def.SCENE_ID_COMPLETE_ORDER_TRACKING) {
			newUIComponent = new CompleteOrderTrackingUI();
		} else if (sceneId == Def.SCENE_ID_ORDER_TRACKING) {
			newUIComponent = new OrderTrackingUI();
		} else if (sceneId == Def.SCENE_ID_ERROR) {
			newUIComponent = new ErrorUI();
		} else if (sceneId == Def.SCENE_ID_DISCOUNT) {
			newUIComponent = new DiscountUI();
		} else if (sceneId == Def.SCENE_ID_OPTIMIZATION) {
			newUIComponent = new OptimizationUI();
		} else if (sceneId == Def.SCENE_ID_REGULAR_ISA) {
			newUIComponent = new RegularISAUI();
		}

		uiTable.put(String.valueOf(sceneId), newUIComponent);

		return newUIComponent;
	}

	public void hideAll() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISAController: hideAll()");
		}
		layeredUI.deactivate();

		if (currentUI != null) {
			FrameworkMain.getInstance().removeComponent(currentUI);
			currentUI.stop();
		}

		Enumeration enums = uiTable.elements();
		Log.printInfo("uiTable size " + uiTable.size());

		while (enums.hasMoreElements()) {
			BaseUI ui = (BaseUI) enums.nextElement();
			if (ui != null) {
				ui.stop();
				ui.dispose();
				ui = null;
			}
		}

		currentUI = null;
		uiTable.clear();

		FrameworkMain.getInstance().getImagePool().clear();

		DataCenter.getInstance().remove(Def.KEY_SERVICE_DESCRIPTION_RESPONSE);
		DataCenter.getInstance().remove(Def.KEY_REPLY_MESSAGE);
	}

	public void showLoading() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISAController: showLoading()");
		}

		if (loadingService != null) {
			try {
				loadingService.showLoadingAnimation(new Point(480, 270));
				isLoading = true;
			} catch (RemoteException e) {
				Log.print(e);
			}
		}
	}

	public void hideLoading() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISAController: hideLoading(");
		}

		if (loadingService != null) {
			try {
				loadingService.hideLoadingAnimation();
				isLoading = false;
			} catch (RemoteException e) {
				Log.print(e);
			}
		}
	}

	/**
	 * Process a key event base on LayeredUI.
	 *
	 * @param event The key event to input from RCU.
	 * @return true if a key is used by PinEnablerController, false otherwise.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (event.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}
		int keyCode = event.getCode();

		if (Log.DEBUG_ON) {
			Log.printDebug("ISAController: handleKeyEvent: keyCode=" + keyCode);
		}

		switch (keyCode) {
		case OCRcEvent.VK_UP:
		case OCRcEvent.VK_DOWN:
		case OCRcEvent.VK_LEFT:
		case OCRcEvent.VK_RIGHT:
		case OCRcEvent.VK_ENTER:
		case OCRcEvent.VK_EXIT:
		case OCRcEvent.VK_CHANNEL_UP:
		case OCRcEvent.VK_CHANNEL_DOWN:
		case OCRcEvent.VK_PAGE_UP:
		case OCRcEvent.VK_PAGE_DOWN:
		case KeyCodes.LAST:
		case KeyCodes.MENU:
		case KeyCodes.PIP:
		case KeyCodes.VOD:
		case KeyCodes.SEARCH:
		case KeyCodes.SETTINGS:
		case KeyCodes.WIDGET:
		case KeyCodes.RECORD:
		case KeyCodes.LIST:
		case KeyCodes.FAV:
		case HRcEvent.VK_GUIDE:
		case KeyCodes.STOP:
		case KeyCodes.COLOR_A:
		case KeyCodes.COLOR_B:
		case KeyCodes.COLOR_C:
		case KeyCodes.COLOR_D:
		case OCRcEvent.VK_0:
		case OCRcEvent.VK_1:
		case OCRcEvent.VK_2:
		case OCRcEvent.VK_3:
		case OCRcEvent.VK_4:
		case OCRcEvent.VK_5:
		case OCRcEvent.VK_6:
		case OCRcEvent.VK_7:
		case OCRcEvent.VK_8:
		case OCRcEvent.VK_9:
			if (!isLoading && currentUI != null) {
				return currentUI.handleKey(keyCode);
			}
			return true;
		default:
			return false;
		}
	}

	/**
	 * Handle if data is removed in DataCenter.
	 *
	 * @param key a key of data in DataCenter.
	 */
	public void dataRemoved(String key) {

	}

	/**
	 * Handle if data is updated in DataCenter.
	 *
	 * @param key   a key of updated data in DataCenter.
	 * @param old   last data.
	 * @param value new data.
	 */
	public void dataUpdated(String key, Object old, Object value) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISAController: dataUpdated(): key = " + key + "\t old = " + old + "\t value = " + value);
		}

		if (key.equals(MonitorService.IXC_NAME)) {
			MonitorService monitorService = (MonitorService) value;
			try {

				monitorService.addInbandDataListener(OOBDataManager.getInstance(),
						FrameworkMain.getInstance().getApplicationName());

				ISADataManager.getInstance().setMacAddress();
				if (Log.DEBUG_ON) {
					Log.printInfo("ISAController: MonitorService Lookup completed");
				}
			} catch (RemoteException e) {
				Log.print(e);
			}

		} else if (key.equals(PreferenceService.IXC_NAME)) {
			PreferenceService pService = (PreferenceService) value;
			try {
				String[] values = pService.addPreferenceListener(this, FrameworkMain.getInstance().getApplicationName(),
						new String[] { PreferenceNames.LANGUAGE }, new String[] { Definitions.LANGUAGE_FRENCH });

				if (Log.DEBUG_ON) {
					for (int i = 0; values != null && i < values.length; i++) {
						Log.printDebug("ISAController: dataUpdated(): values = " + values[i]);
					}
				}

				DataCenter.getInstance().put(PreferenceNames.LANGUAGE, values[0]);
			} catch (RemoteException e) {
				Log.print(e);
			}
		} else if (key.equals(StcService.IXC_NAME)) {
			StcService stcService = (StcService) value;
			String appName = FrameworkMain.getInstance().getApplicationName();
			try {
				int currentLevel = stcService.registerApp(appName);
				Log.printDebug("ISAController: stc log level = " + currentLevel);
				Log.setStcLevel(currentLevel);
			} catch (RemoteException ex) {
				Log.print(ex);
			}
			Object o = SharedMemory.getInstance().get("stc-" + appName);
			if (o != null) {
				DataCenter.getInstance().put("LogCache", o);
			}
			try {
				stcService.addLogLevelChangeListener(appName, logListener);
			} catch (RemoteException ex) {
				Log.print(ex);
			}

		} else if (key.equals(LoadingAnimationService.IXC_NAME)) {
			this.loadingService = (LoadingAnimationService) value;
		}
	}

	/**
	 * Receive a updated preference from Profile application.
	 *
	 * @param preferenceName a name of updated preference.
	 * @param value          a updated value.
	 * @throws RemoteException remote exception.
	 */
	public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("CyoController: receiveUpdatedPreference()");
		}
		if (preferenceName.equals(PreferenceNames.LANGUAGE)) {
			DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
		}
	}

	/**
	 * Receives a log level from STC application and apply.
	 *
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class LogLevelAdapter implements LogLevelChangeListener {

		/**
		 * Receives a log level from STC application and apply.
		 *
		 * @param logLevel logLevel to be updated.
		 * @throws RemoteException throws exception
		 */
		public void logLevelChanged(int logLevel) throws RemoteException {
			Log.setStcLevel(logLevel);
		}
	}
}

/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 *
 */
public class ServiceDescription extends BaseElement {

	private String serviceNumber;
	private String SGAServiceNumber;
	private String logoFileName;
	private String price;
	
	private Description[] description;
	
	/**
	 * 
	 */
	public ServiceDescription() {
		description = new Description[2];
	}
	
	

	public String getServiceNumber() {
		return serviceNumber;
	}



	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}



	public String getSGAServiceNumber() {
		return SGAServiceNumber;
	}



	public void setSGAServiceNumber(String sGAServiceNumber) {
		SGAServiceNumber = sGAServiceNumber;
	}



	public String getLogoFileName() {
		return logoFileName;
	}



	public void setLogoFileName(String logoFileName) {
		this.logoFileName = logoFileName;
	}



	public String getPrice() {
		return price;
	}



	public void setPrice(String price) {
		this.price = price;
	}



	public Description[] getDescription() {
		return description;
	}



	public void setDescription(Description[] description) {
		this.description = description;
	}
	
	public void setDescription(int idx, Description d) {
		
		if (idx < description.length) {
			this.description[idx] = d;
		}
	}
	
	public Description getDescription(String lang) {
		
		for (int i = 0; i < description.length; i++) {
			if (description[i] != null && description[i].getLanguage() != null && description[i].getLanguage().equalsIgnoreCase(lang)) {
				return description[i];
			}
		}
		
		return null;
	}
	
	public void proccessAttributes(Attributes attributes) {
		
		for (int i = 0; i < attributes.getLength(); i++) {
			String localName = attributes.getLocalName(i);
			String value = attributes.getValue(i);
			
			if (localName.equalsIgnoreCase("ServiceNumber")) {
				setServiceNumber(value);
			} else if (localName.equalsIgnoreCase("SGAServiceNumber")) {
				setSGAServiceNumber(value);
			} else if (localName.equalsIgnoreCase("logoFileName")) {
				setLogoFileName(value);
			} else if (localName.equalsIgnoreCase("price")) {
				setPrice(value);
			}
		}
	}
}

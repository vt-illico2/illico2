
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

public class LegalText extends BaseElement {

	private Description[] description;
	
	public LegalText() {
		description = new Description[2];
	}

	public Description[] getDescription() {
		return description;
	}

	public void setDescription(Description[] description) {
		this.description = description;
	}
	
	public void setDescription(int idx, Description d) {
		
		if (idx < description.length) {
			this.description[idx] = d;
		}
	}
	
	public Description getDescription(String lang) {
		
		for (int i = 0; i < description.length; i++) {
			if (description[i] != null && description[i].getLanguage().equalsIgnoreCase(lang)) {
				return description[i];
			}
		}
		
		return null;
	}

	public void proccessAttributes(Attributes attributes) {
	}
}

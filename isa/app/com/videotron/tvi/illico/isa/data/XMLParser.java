/**
 * @(#)XMLParser.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.isa.data;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.data.obj.BaseElement;
import com.videotron.tvi.illico.isa.data.obj.CallToActionText;
import com.videotron.tvi.illico.isa.data.obj.Description;
import com.videotron.tvi.illico.isa.data.obj.FAPL;
import com.videotron.tvi.illico.isa.data.obj.FinePrint;
import com.videotron.tvi.illico.isa.data.obj.FullDescription;
import com.videotron.tvi.illico.isa.data.obj.GeneralInformation;
import com.videotron.tvi.illico.isa.data.obj.LegalText;
import com.videotron.tvi.illico.isa.data.obj.MessageContent;
import com.videotron.tvi.illico.isa.data.obj.PopupTitle;
import com.videotron.tvi.illico.isa.data.obj.RootMessage;
import com.videotron.tvi.illico.isa.data.obj.ServiceDescription;
import com.videotron.tvi.illico.isa.data.obj.ServiceDescriptionResponse;
import com.videotron.tvi.illico.isa.data.obj.ShortDescription;
import com.videotron.tvi.illico.log.Log;

/**
 * This class makes ISA Data from XML Data.
 * 
 * @author zestyman
 */
public class XMLParser extends DefaultHandler {

	public static final int DATA_TYPE_SERVICE_DESCRIPTION = 0;
	public static final int DATA_TYPE_RESPONSE = 1;

	private Stack stack = new Stack();

	private int dataType = DATA_TYPE_SERVICE_DESCRIPTION;

	private String lastQName = "";
	private String lastLang = "";

	public XMLParser(int dataType) {
		this.dataType = dataType;
	}

	/**
	 * doesn't used.
	 * 
	 * @throws SAXException
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 */
	public void startDocument() throws SAXException {
	}

	/**
	 * doesn't used.
	 * 
	 * @throws SAXException
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 */
	public void endDocument() throws SAXException {
	}

	/**
	 * doesn't used.
	 * 
	 * @param name
	 *            The name of the skipped entity.
	 * @throws SAXException
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 */
	public void skippedEntity(String name) throws SAXException {
	}

	/**
	 * Receive notification of the start of an element. Is received values of
	 * xml. This method makes ISA data.
	 * 
	 * @param uri
	 *            The Namespace URI, or the empty string if the element has no
	 *            Namespace URI or if Namespace processing is not being
	 *            performed.
	 * @param localName
	 *            The local name (without prefix), or the empty string if
	 *            Namespace processing is not being performed.
	 * @param qName
	 *            The qualified name (with prefix), or the empty string if
	 *            qualified names are not available.
	 * @param attributes
	 *            The attributes attached to the element. If there are no
	 *            attributes, it shall be an empty Attributes object.
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 * @see org.xml.sax.ContentHandler#startElement
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (Log.DEBUG_ON) {
			Log.printDebug("XMLParser: startElement(): qName " + qName + " attributes = " + attributes);
		}
		lastQName = qName;

		if (dataType == DATA_TYPE_SERVICE_DESCRIPTION) {

			if (qName.equalsIgnoreCase("serviceDescriptionResponse")) {
				ServiceDescriptionResponse serviceDetail = new ServiceDescriptionResponse();
				serviceDetail.proccessAttributes(attributes);
				stack.push(serviceDetail);
			} else if (qName.equalsIgnoreCase("messageContent")) {
				MessageContent mc = new MessageContent();
				mc.proccessAttributes(attributes);
				stack.push(mc);
			} else if (qName.equalsIgnoreCase("ServiceDescription")) {
				ServiceDescription sd = new ServiceDescription();
				sd.proccessAttributes(attributes);
				stack.push(sd);
			} else if (qName.equalsIgnoreCase("description")) {
				Description d = new Description();
				d.proccessAttributes(attributes);
				stack.push(d);
			} else if (qName.equalsIgnoreCase("popupTitle")) {
				PopupTitle pt = new PopupTitle();
				pt.proccessAttributes(attributes);
				stack.push(pt);
			} else if (qName.equalsIgnoreCase("calltoactionText")) {
				CallToActionText ctat = new CallToActionText();
				ctat.proccessAttributes(attributes);
				stack.push(ctat);
			} else if (qName.equalsIgnoreCase("fullDescription")) {
				FullDescription fd = new FullDescription();
				fd.proccessAttributes(attributes);
				stack.push(fd);
			} else if (qName.equalsIgnoreCase("shortDescription")) {
				ShortDescription sd = new ShortDescription();
				sd.proccessAttributes(attributes);
				stack.push(sd);
			} else if (qName.equalsIgnoreCase("generalInformation")) {
				GeneralInformation gi = new GeneralInformation();
				gi.proccessAttributes(attributes);
				stack.push(gi);
			} else if (qName.equalsIgnoreCase("legalText")) {
				LegalText lt = new LegalText();
				lt.proccessAttributes(attributes);
				stack.push(lt);
			} else if (qName.equalsIgnoreCase("FAPL")) {
				FAPL f = new FAPL();
				f.proccessAttributes(attributes);
				stack.push(f);
			} else if (qName.equalsIgnoreCase("finePrint")) {
				FinePrint fp = new FinePrint();
				fp.proccessAttributes(attributes);
				stack.push(fp);
			} else if (qName.equalsIgnoreCase("topMessageText") || qName.equalsIgnoreCase("bottomMessageText")) {
				ShortDescription tmt = new ShortDescription();
				tmt.proccessAttributes(attributes);
				stack.push(tmt);
			} else if (qName.equalsIgnoreCase("internalInfoText")) {
				ShortDescription tmt = new ShortDescription();
				tmt.proccessAttributes(attributes);
				stack.push(tmt);
			}
		} else {
			if (qName.equalsIgnoreCase("replyMessage")) {
				RootMessage rm = new RootMessage();
				rm.proccessAttributes(attributes);
				stack.push(rm);
			} else if (qName.equalsIgnoreCase("messageContent")) {
				MessageContent mc = new MessageContent();
				mc.proccessAttributes(attributes);
				stack.push(mc);
			} else if (qName.equalsIgnoreCase("popupTitle")) {
				PopupTitle pt = new PopupTitle();
				pt.proccessAttributes(attributes);
				stack.push(pt);
			} else if (qName.equalsIgnoreCase("topMessageText") || qName.equalsIgnoreCase("bottomMessageText")) {
				ShortDescription tmt = new ShortDescription();
				tmt.proccessAttributes(attributes);
				stack.push(tmt);
			} else if (qName.equalsIgnoreCase("internalInfoText")) {
				ShortDescription tmt = new ShortDescription();
				tmt.proccessAttributes(attributes);
				stack.push(tmt);
			} else {
				stack.push(new ShortDescription());
			}
		}
	}

	/**
	 * Receive notification of the end of an element. Saves ISA data.
	 * <p>
	 * By default, do nothing. Application writers may override this method in a
	 * subclass to take specific actions at the end of each element (such as
	 * finalising a tree node or writing output to a file).
	 * </p>
	 * 
	 * @param uri
	 *            The Namespace URI, or the empty string if the element has no
	 *            Namespace URI or if Namespace processing is not being
	 *            performed.
	 * @param localName
	 *            The local name (without prefix), or the empty string if
	 *            Namespace processing is not being performed.
	 * @param qName
	 *            The qualified name (with prefix), or the empty string if
	 *            qualified names are not available.
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 * @see org.xml.sax.ContentHandler#endElement
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (Log.DEBUG_ON) {
			Log.printDebug("XMLParser: endElement(): qName " + qName);
		}

		BaseElement be = (BaseElement) stack.pop();

		if (dataType == DATA_TYPE_SERVICE_DESCRIPTION) {

			if (qName.equalsIgnoreCase("serviceDescriptionResponse")) {
				ServiceDescriptionResponse sdResponse = (ServiceDescriptionResponse) be;
				DataCenter.getInstance().put(Def.KEY_SERVICE_DESCRIPTION_RESPONSE, sdResponse);
			} else if (qName.equalsIgnoreCase("messageContent")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof ServiceDescriptionResponse) {
					((ServiceDescriptionResponse) parentElement).setMessageContent((MessageContent) be);
				} else if (parentElement instanceof RootMessage) {
					((RootMessage) parentElement).setMessageContent((MessageContent) be);
				}

			} else if (qName.equalsIgnoreCase("ServiceDescription")) {
				BaseElement parentElement = (BaseElement) stack.peek();
				if (parentElement instanceof ServiceDescriptionResponse) {
					((ServiceDescriptionResponse) parentElement).setServiceDescription((ServiceDescription) be);
				}
			} else if (qName.equalsIgnoreCase("description")) {
				BaseElement parentElement = (BaseElement) stack.peek();
				if (parentElement instanceof ServiceDescription) {
					lastLang = ((Description) be).getLanguage();

					if (lastLang.equalsIgnoreCase("FR_CA")) {
						((ServiceDescription) parentElement).setDescription(0, (Description) be);
					} else {
						((ServiceDescription) parentElement).setDescription(1, (Description) be);
					}
				} else if (parentElement instanceof LegalText) {
					lastLang = ((Description) be).getLanguage();
					if (lastLang.equalsIgnoreCase("FR_CA")) {
						((LegalText) parentElement).setDescription(0, (Description) be);
					} else {
						((LegalText) parentElement).setDescription(1, (Description) be);
					}
				}
			} else if (qName.equalsIgnoreCase("popupTitle")) {
				BaseElement parentElement = (BaseElement) stack.peek();
				if (parentElement instanceof Description) {
					((Description) parentElement).setPopupTitle((PopupTitle) be);
				}

			} else if (qName.equalsIgnoreCase("calltoactionText")) {
				BaseElement parentElement = (BaseElement) stack.peek();
				if (parentElement instanceof Description) {
					((Description) parentElement).setCallToActionText((CallToActionText) be);
				}
			} else if (qName.equalsIgnoreCase("fullDescription")) {
				BaseElement parentElement = (BaseElement) stack.peek();
				if (parentElement instanceof Description) {
					((Description) parentElement).setFullDescription((FullDescription) be);
				}
			} else if (qName.equalsIgnoreCase("shortDescription")) {
				BaseElement parentElement = (BaseElement) stack.peek();
				if (parentElement instanceof Description) {
					((Description) parentElement).setShortDescription((ShortDescription) be);
				}
			} else if (qName.equalsIgnoreCase("generalInformation")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof ServiceDescriptionResponse) {
					((ServiceDescriptionResponse) parentElement).setGeneralInformation((GeneralInformation) be);
				}
			} else if (qName.equalsIgnoreCase("legalText")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof GeneralInformation) {
					((GeneralInformation) parentElement).setLegalText((LegalText) be);
				}
			} else if (qName.equalsIgnoreCase("FAPL")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof GeneralInformation) {
					((GeneralInformation) parentElement).setFapl((FAPL) be);
				}
			} else if (qName.equalsIgnoreCase("finePrint")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof FAPL) {
					lastLang = ((FinePrint) be).getLanguage();
					if (lastLang.equalsIgnoreCase("FR_CA")) {
						((FAPL) parentElement).setFinePrint(0, (FinePrint) be);
					} else {
						((FAPL) parentElement).setFinePrint(1, (FinePrint) be);
					}
				}
			} else if (qName.equalsIgnoreCase("topMessageText")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof MessageContent) {
					((MessageContent) parentElement).setTopMessageText((ShortDescription) be);
				}
			} else if (qName.equalsIgnoreCase("bottomMessageText")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof MessageContent) {
					((MessageContent) parentElement).setBottomMessageText((ShortDescription) be);
				}
			} else if (qName.equalsIgnoreCase("internalInfoText")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof MessageContent) {
					((MessageContent) parentElement).setValue(((ShortDescription) be).getValue());
				}
			}
		} else {
			if (qName.equalsIgnoreCase("replyMessage")) {
				RootMessage rm = (RootMessage) be;
				DataCenter.getInstance().put(Def.KEY_REPLY_MESSAGE, rm);
			} else if (qName.equalsIgnoreCase("messageContent")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof RootMessage) {
					((RootMessage) parentElement).setMessageContent((MessageContent) be);
				}
			} else if (qName.equalsIgnoreCase("popupTitle")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof MessageContent) {
					((MessageContent) parentElement).setPopupTitle((PopupTitle) be);
				}
			} else if (qName.equalsIgnoreCase("topMessageText")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof MessageContent) {
					((MessageContent) parentElement).setTopMessageText((ShortDescription) be);
				}
			} else if (qName.equalsIgnoreCase("bottomMessageText")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof MessageContent) {
					((MessageContent) parentElement).setBottomMessageText((ShortDescription) be);
				}
			} else if (qName.equalsIgnoreCase("internalInfoText")) {
				BaseElement parentElement = (BaseElement) stack.peek();

				if (parentElement instanceof MessageContent) {
					((MessageContent) parentElement).setValue(((ShortDescription) be).getValue());
				}
			}
		}
	}

	/**
	 * doesn't used.
	 * 
	 * @param s
	 *            The Namespace prefix being declared.
	 * @param s1
	 *            The Namespace URI mapped to the prefix.
	 * @throws SAXException
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 */
	public void startPrefixMapping(String s, String s1) throws SAXException {
	}

	/**
	 * doesn't used.
	 * 
	 * @param prefix
	 *            The Namespace prefix being declared.
	 * @throws SAXException
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 */
	public void endPrefixMapping(String prefix) throws SAXException {
	}

	/**
	 * Receive notification of character data inside an element.
	 * <p>
	 * By default, do nothing. Application writers may override this method to
	 * take specific actions for each chunk of character data (such as adding
	 * the data to a node or buffer, or printing it to a file).
	 * </p>
	 * 
	 * @param ch
	 *            The characters.
	 * @param start
	 *            The start position in the character array.
	 * @param length
	 *            The number of characters to use from the character array.
	 * @exception org.xml.sax.SAXException
	 *                Any SAX exception, possibly wrapping another exception.
	 * @see org.xml.sax.ContentHandler#characters
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		String value = (new String(ch, start, length)).trim();
		if (value.length() > 0) {
			System.out.println("value = " + value);

			((BaseElement) stack.peek()).setValue(value);

		}
	}

	public static void main(String[] args) {
		System.out.println("start");

		try {
			BufferedInputStream bis = null;

			// File file = new File("src/test/service_response.txt");
			// bis = new BufferedInputStream(new FileInputStream(file2));
			//
			// XMLParser parser = new
			// XMLParser(XMLParser.DATA_TYPE_SERVICE_DESCRIPTION);

			File file = new File("src/test/request_response.txt");
			bis = new BufferedInputStream(new FileInputStream(file));

			XMLParser parser = new XMLParser(XMLParser.DATA_TYPE_RESPONSE);

			SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();

			saxParser.parse(bis, parser);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}

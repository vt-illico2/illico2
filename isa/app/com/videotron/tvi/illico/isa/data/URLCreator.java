/**
 *
 */
package com.videotron.tvi.illico.isa.data;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;

/**
 * @author zestyman
 */
public class URLCreator {

	/**
	 *
	 */
	public URLCreator() {
		// TODO Auto-generated constructor stub
	}

	public static String getServiceResponseURL(String serviceId, String entryPoint) {

		StringBuffer sf = new StringBuffer();

		String serverIP = ISADataManager.getInstance().getServerIP();
		int port = ISADataManager.getInstance().getServerPort();
		String rootContext = ISADataManager.getInstance().getContextRoot();
		String mac = ISADataManager.getInstance().getMacAddress();
		String language = DataCenter.getInstance().getString(PreferenceNames.LANGUAGE);

		sf.append("http://");
		sf.append(serverIP);
		sf.append(":");
		sf.append(port);
		sf.append(rootContext);
		sf.append("/getServiceDescription?macAddress=");
		sf.append(mac);
		sf.append("&serviceId=");
		sf.append(serviceId);
		sf.append("&requestLocale=");

		if (language.equals(Definitions.LANGUAGE_FRENCH)) {
			sf.append(ISAService.LOCALE_FR_CA);
		} else {
			sf.append(ISAService.LOCALE_EN_CA);
		}

		sf.append("&entryPoint=");
		sf.append(entryPoint);

		return sf.toString();
	}

	public static String getSubscribeToServiceURL(String serviceId, String entryPoint, boolean needDiscount,
			int forceSubscriptionMode) {

		StringBuffer sf = new StringBuffer();

		String serverIP = ISADataManager.getInstance().getServerIP();
		int port = ISADataManager.getInstance().getServerPort();
		String rootContext = ISADataManager.getInstance().getContextRoot();
		String mac = ISADataManager.getInstance().getMacAddress();
		String language = DataCenter.getInstance().getString(PreferenceNames.LANGUAGE);

		sf.append("http://");
		sf.append(serverIP);
		sf.append(":");
		sf.append(port);
		sf.append(rootContext);
		sf.append("/subscribeToService?macAddress=");
		sf.append(mac);
		sf.append("&serviceId=");
		sf.append(serviceId);
		sf.append("&requestLocale=");

		if (language.equals(Definitions.LANGUAGE_FRENCH)) {
			sf.append(ISAService.LOCALE_FR_CA);
		} else {
			sf.append(ISAService.LOCALE_EN_CA);
		}

		sf.append("&entryPoint=");
		sf.append(entryPoint);

		if (needDiscount) {
			sf.append("&discount=false");
		}

		switch (forceSubscriptionMode) {
		case ISADataManager.FORCE_SUBSCRIPTION_MODE_NONE:
			break;
		case ISADataManager.FORCE_SUBSCRIPTION_MODE_TRUE:
			sf.append("&forceSubscription=true");
			break;
		case ISADataManager.FORCE_SUBSCRIPTION_MODE_FALSE:
			sf.append("&forceSubscription=false");
			break;
		}

		return sf.toString();
	}

	public static String getRegisterToOrderTrackingURL(String phoneNumber, String serviceId, String entryPoint) {
		StringBuffer sf = new StringBuffer();

		String serverIP = ISADataManager.getInstance().getServerIP();
		int port = ISADataManager.getInstance().getServerPort();
		String rootContext = ISADataManager.getInstance().getContextRoot();
		String mac = ISADataManager.getInstance().getMacAddress();
		String language = DataCenter.getInstance().getString(PreferenceNames.LANGUAGE);

		sf.append("http://");
		sf.append(serverIP);
		sf.append(":");
		sf.append(port);
		sf.append(rootContext);
		sf.append("/registerToOrderTracking?macAddress=");
		sf.append(mac);
		sf.append("&phoneNumber=");
		sf.append(phoneNumber);
		sf.append("&serviceId=");
		sf.append(serviceId);
		sf.append("&requestLocale=");

		if (language.equals(Definitions.LANGUAGE_FRENCH)) {
			sf.append(ISAService.LOCALE_FR_CA);
		} else {
			sf.append(ISAService.LOCALE_EN_CA);
		}

		sf.append("&entryPoint=");
		sf.append(entryPoint);

		return sf.toString();
	}

	public static String getImageURL(String ImageName) {
		StringBuffer sf = new StringBuffer();

		String serverIP = ISADataManager.getInstance().getServerIP();
		int port = ISADataManager.getInstance().getServerPort();
		String imagePath = ISADataManager.getInstance().getImagePath();

		sf.append("http://");
		sf.append(serverIP);
		sf.append(":");
		sf.append(port);
		sf.append(imagePath);
		sf.append("/");
		sf.append(ImageName);

		return sf.toString();
	}
}

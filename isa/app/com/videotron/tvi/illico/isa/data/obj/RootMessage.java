/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 *
 */
public class RootMessage extends BaseElement {
	
	public static final String SUCCESS = "1";
	public static final String UNSUCCESS = "0";

	/**
		Flag indicating the result status of the command.
		A value of '0' is returned for unsuccessful status.
		A value of '1' is returned for successful status.

	 */
	private String success;
	/**
	 * 					User's main phone number if available.
					To be used in Call-me screen.

	 */
	private String phoneNumber;
	
	/**
	 * Message returned by the Instant Subscription Service.
	 */
	private MessageContent messageContent;
	
	/**
	 * 
	 */
	public RootMessage() {
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public MessageContent getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(MessageContent messageContent) {
		this.messageContent = messageContent;
	}

	public void proccessAttributes(Attributes attributes) {
		for (int i = 0; i < attributes.getLength(); i++) {
			String localName = attributes.getLocalName(i);
			String value = attributes.getValue(i);

			if (localName.equalsIgnoreCase("SUCCESS")) {
				setSuccess(value);
			} else if (localName.equalsIgnoreCase("PHONENUMBER")) {
				setPhoneNumber(value);
			}
		}
	}
}

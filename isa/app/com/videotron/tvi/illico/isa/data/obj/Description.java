/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 *
 */
public class Description extends BaseElement{

	private String language;
	
	private PopupTitle popupTitle;
	private CallToActionText callToActionText;
	private FullDescription fullDescription;
	private ShortDescription shortDescription;
	
	/**
	 * 
	 */
	public Description() {
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public PopupTitle getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(PopupTitle popupTitle) {
		this.popupTitle = popupTitle;
	}

	public CallToActionText getCallToActionText() {
		return callToActionText;
	}

	public void setCallToActionText(CallToActionText callToActionText) {
		this.callToActionText = callToActionText;
	}

	public FullDescription getFullDescription() {
		return fullDescription;
	}

	public void setFullDescription(FullDescription fullDescription) {
		this.fullDescription = fullDescription;
	}

	public ShortDescription getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(ShortDescription shortDescription) {
		this.shortDescription = shortDescription;
	}

	public void proccessAttributes(Attributes attributes) {
		
		for (int i = 0; i < attributes.getLength(); i++) {
			String localName = attributes.getLocalName(i);
			String value = attributes.getValue(i);
			
			if (localName.equalsIgnoreCase("language")) {
				setLanguage(value);
			}
		}
		
	}
}

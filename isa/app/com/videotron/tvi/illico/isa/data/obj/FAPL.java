/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 *
 */
public class FAPL extends BaseElement {

	private String activationStatus;
	
	private FinePrint[] finePrint;
	
	/**
	 * 
	 */
	public FAPL() {
		finePrint = new FinePrint[2];
	}

	public String getActivationStatus() {
		return activationStatus;
	}

	public void setActivationStatus(String activationStatus) {
		this.activationStatus = activationStatus;
	}

	public FinePrint[] getFinePrint() {
		return finePrint;
	}

	public void setFinePrint(FinePrint[] finePrint) {
		this.finePrint = finePrint;
	}
	
	public void setFinePrint(int idx, FinePrint fp) {
		
		if (idx < finePrint.length) {
			this.finePrint[idx] = fp;
		}
	}
	
	public FinePrint getFinePrint(String lang) {
		
		for (int i = 0; i < finePrint.length; i++) {
			if (finePrint[i] != null && finePrint[i].getLanguage().equalsIgnoreCase(lang)) {
				return finePrint[i];
			}
		}
		
		return null;
	}

	public void proccessAttributes(Attributes attributes) {
		
	}

	
}

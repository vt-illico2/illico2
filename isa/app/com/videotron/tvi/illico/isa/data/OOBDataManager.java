package com.videotron.tvi.illico.isa.data;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.isa.communication.CommunicationManager;
import com.videotron.tvi.illico.isa.util.ConvertUtil;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;

public class OOBDataManager implements DataUpdateListener, InbandDataListener {

	private static OOBDataManager instance;

	private static final String ISA_DATA = "ISA_DATA";

	private int currentVersion = -1;

	private OOBDataManager() {
	}

	public static synchronized OOBDataManager getInstance() {

		if (instance == null) {
			instance = new OOBDataManager();
		}

		return instance;
	}

	public void init() {
		if (Log.DEBUG_ON) {
			Log.printDebug("OOBDataManager: init()");
		}

		DataCenter.getInstance().addDataUpdateListener(ISA_DATA, this);

		if (Environment.EMULATOR) {
			File file = new File("resource/isa.txt");

			parseData(file);

			ISADataManager.getInstance().completeISAServiceData();

			if (!CommunicationManager.getInstance().isBinded()) {
				CommunicationManager.getInstance().bindISAService();
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("OOBDataManager: init() end");
		}
	}

	public void receiveInbandData(String locator) {
		if (Log.DEBUG_ON) {
			Log.printDebug("OOBDataManager: receiveInbandData = " + locator);
		}
		DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
		MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

		if (mService != null) {
			try {
				mService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
			} catch (RemoteException e) {
				Log.print(e);
			}
		}
	}

	public void dataUpdated(String key, Object old, Object value) {
		Log.printInfo("key = " + key + " value = " + value);

		if (key != null) {
			if (key.equals(ISA_DATA)) {
				try {
					parseData((File) value);
				} catch (Exception e) {
					Log.printWarning("OOBDataManager: for parsing a isa data from oob, it has a exception");
					ISADataManager.getInstance().clearData();
				}

				ISADataManager.getInstance().completeISAServiceData();

				if (!CommunicationManager.getInstance().isBinded()) {
					CommunicationManager.getInstance().bindISAService();
				}
			}
		}
	}

	public void dataRemoved(String key) {
	}

	private void parseData(File file) {

		byte[] data = BinaryReader.read(file);

		int offset = 0;

		// Version
		int version = ConvertUtil.oneByteToInt(data, offset++);
		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: version=" + version);
		}

		if (currentVersion == version) {
			if (Log.DEBUG_ON) {
				Log.printDebug("parseData: version is same, so return.");
			}

			return;
		}

		currentVersion = version;

		ISADataManager.getInstance().clearData();

		// server ip
		int serverIPLength = ConvertUtil.oneByteToInt(data, offset++);
		String serverIP = ConvertUtil.byteArrayToString(data, offset, serverIPLength);
		ISADataManager.getInstance().setServerIP(serverIP);
		offset += serverIPLength;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: serverIP=" + serverIP);
		}

		// server port
		int serverPort = ConvertUtil.twoBytesToInt(data, offset);
		ISADataManager.getInstance().setServerPort(serverPort);
		offset += 2;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: serverPort=" + serverPort);
		}

		// contextRoot
		int contextRootLength = ConvertUtil.oneByteToInt(data, offset++);
		String contextRoot = ConvertUtil.byteArrayToString(data, offset, contextRootLength);
		ISADataManager.getInstance().setContextRoot(contextRoot);
		offset += contextRootLength;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: contextRoot=" + contextRoot);
		}

		// image path
		int imagePathLength = ConvertUtil.oneByteToInt(data, offset++);
		String imagePath = ConvertUtil.byteArrayToString(data, offset, imagePathLength);
		ISADataManager.getInstance().setImagePath(imagePath);
		offset += imagePathLength;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: imagePath=" + imagePath);
		}

		// service list
		int serviceSize = ConvertUtil.twoBytesToInt(data, offset);
		offset += 2;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: serviceSize=" + serviceSize);
		}

		for (int i = 0; i < serviceSize; i++) {
			String serviceType = ConvertUtil.byteArrayToString(data, offset, 1);
			offset++;

			int serviceNumber = ConvertUtil.fourBytesToInt(data, offset);
			offset += 4;

			int serviceIdLength = ConvertUtil.oneByteToInt(data, offset++);
			String serviceId = ConvertUtil.byteArrayToString(data, offset, serviceIdLength);
			offset += serviceIdLength;

			if (Log.DEBUG_ON) {
				Log.printDebug(
						"parseData: index=" + i + ", serviceType=" + serviceType + ", serviceNumber=" + serviceNumber
								+ ", serviceId=" + serviceId);
			}

			ISADataManager.getInstance().addISAServiceData(serviceType, serviceNumber, serviceId);
		}

		// Bundling
		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: Bundling data parsing start");
		}

		for (int i = 0; i < serviceSize; i++) {
			String serviceType = ConvertUtil.byteArrayToString(data, offset, 1);
			offset++;

			int serviceNumber = ConvertUtil.fourBytesToInt(data, offset);
			offset += 4;

			String combinable = ConvertUtil.byteArrayToString(data, offset, 1);
			offset++;

			ISADataManager.getInstance().setCombinableState(i, serviceType, serviceNumber, combinable);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: To parse the data for combinable done");
		}

		int dncsPackageSize = ConvertUtil.oneByteToInt(data, offset);
		offset++;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: dncsPacakgeSize=" + dncsPackageSize);
		}

		Vector dncsPackageVec = new Vector();
		for (int i = 0; i < dncsPackageSize; i++) {
			int packageNameLength = ConvertUtil.oneByteToInt(data, offset);
			offset++;

			String packageName = ConvertUtil.byteArrayToString(data, offset, packageNameLength);
			offset += packageNameLength;

			if (Log.DEBUG_ON) {
				Log.printDebug("parseData: packageName=" + packageName);
			}

			dncsPackageVec.add(packageName);
		}

		String[] dncsPackageNames = new String[dncsPackageVec.size()];
		dncsPackageVec.copyInto(dncsPackageNames);
		ISADataManager.getInstance().setDncsPackages(dncsPackageNames);
		dncsPackageVec.clear();

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: To parse the dncs pacakge done");
		}

		// excluded channels
		int excludedChannelSize = ConvertUtil.twoBytesToInt(data, offset);
		offset += 2;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: excludedChannelSize=" + excludedChannelSize);
		}

		String[] excludedChannels = new String[excludedChannelSize];
		for (int i = 0; i < excludedChannelSize; i++) {
			int callLetterLength = ConvertUtil.oneByteToInt(data, offset);
			offset++;

			String callLetter = ConvertUtil.byteArrayToString(data, offset, callLetterLength);
			offset += callLetterLength;

			if (Log.DEBUG_ON) {
				Log.printDebug("parseData: excluded channels, callLetter=" + callLetter);
			}

			excludedChannels[i] = callLetter;
		}

		ISADataManager.getInstance().setExcludedChannels(excludedChannels);

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: To parse the excluded channels done");
		}

		// excluded channels types
		int excludedChannelTypeSize = ConvertUtil.oneByteToInt(data, offset);
		offset++;

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: excludedChannelTypeSize=" + excludedChannelTypeSize);
		}

		int[] excludedChannelTypes = new int[excludedChannelTypeSize];
		for (int i = 0; i < excludedChannelTypeSize; i++) {
			int channelType = ConvertUtil.oneByteToInt(data, offset);
			offset++;

			if (Log.DEBUG_ON) {
				Log.printDebug("parseData: excluded channel types, channelType=" + channelType);
			}

			excludedChannelTypes[i] = channelType;
		}

		ISADataManager.getInstance().setExcludedChannelTypes(excludedChannelTypes);

		if (Log.DEBUG_ON) {
			Log.printDebug("parseData: To parse the excluded channel types done");
		}
	}
}

/**
 *
 */
package com.videotron.tvi.illico.isa.data;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.videotron.tvi.illico.isa.data.obj.OOBData;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import org.ocap.hardware.Host;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.isa.Def;
import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.obj.MessageContent;
import com.videotron.tvi.illico.isa.data.obj.RootMessage;
import com.videotron.tvi.illico.isa.data.obj.ServiceDescriptionResponse;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.isa.ISAServiceDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * Manage a data for ISA.
 *
 * @author zestyman
 */
public class ISADataManager {

	private final String MS_ISA_SERVICE_TYPE_CHANNEL = "C";
	private final String MS_ISA_SERVICE_TYPE_FREE_PREVIEW = "F";
	private final String MS_ISA_SERVICE_TYPE_ILLICO_SERVICE = "I";

	private final String MS_ISA_COMBINABLE_TRUE = "T";
	private final String MS_ISA_COMBINABLE_FALSE = "F";

	public static final int FORCE_SUBSCRIPTION_MODE_NONE = 0x0;
	public static final int FORCE_SUBSCRIPTION_MODE_TRUE = 0x1;
	public static final int FORCE_SUBSCRIPTION_MODE_FALSE = 0x2;

	private static ISADataManager instance;

	private Hashtable listeners;

	private Vector channelList;
	private Vector freePreviewList;
	private Vector illicoServiceList;

	private String serverIP;
	private int serverPort;
	private String contextRoot;
	private String imagePath;

	private String macAddress;

	private String serviceId;
	private String entryPoint;

	private String errorCode = "";
	private String errorType = "";
	private String errorMessage = "";

	private boolean isIllicoService;

	// Bundling
	private Vector oobDataVector;
	private String[] excludedChannels;
	private int[] excludedChannelTypes;
	private String[] dncsPackages;
	private String lastURL = "";
	private String lastResponse = "";

	/**
	 *
	 */
	private ISADataManager() {

		listeners = new Hashtable();

		channelList = new Vector();
		freePreviewList = new Vector();
		illicoServiceList = new Vector();

		oobDataVector = new Vector();
	}

	public static synchronized ISADataManager getInstance() {

		if (instance == null) {
			instance = new ISADataManager();
		}

		return instance;
	}

	public void dispose() {
		clearData();

		listeners.clear();
	}

	public void clearData() {
		channelList.clear();
		freePreviewList.clear();
		illicoServiceList.clear();

		oobDataVector.clear();
		excludedChannels = null;
		excludedChannelTypes = null;
		dncsPackages = null;
	}

	public String getServerIP() {

		if (Environment.EMULATOR) {
			return "dev01il2a.int.videotron.com";
		}

		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public int getServerPort() {

		if (Environment.EMULATOR) {
			return 80;
		}

		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public String getContextRoot() {
		if (Environment.EMULATOR) {
			return "/subscription/services/isa";
		}

		return contextRoot;
	}

	public void setContextRoot(String contextRoot) {
		this.contextRoot = contextRoot;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress() {
		if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO) {
			macAddress = Host.getInstance().getReverseChannelMAC();
			String[] token = TextUtil.tokenize(macAddress, ":");
			StringBuffer address = new StringBuffer();
			for (int i = 0; i < token.length; i++) {
				address.append(token[i]);
			}
			macAddress = address.toString();
		} else {
			try {
				MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
				if (monitor != null) {
					byte[] addr = monitor.getCableCardMacAddress();

					macAddress = getStringBYTEArray(addr);
				}
			} catch (Exception e) {
				if (Log.EXTRA_ON) {
					Log.print(e);
				}
				e.printStackTrace();
			}
		}

		if (macAddress != null) {
			macAddress = macAddress.toUpperCase();
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager, macAddress : " + macAddress);
		}
	}

	public String getCurrentServiceId() {
		return serviceId;
	}

	public void setCurrentServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCurrentEntryPoint() {
		return entryPoint;
	}

	public void setCurrentEntryPoint(String entryPoint) {
		this.entryPoint = entryPoint;
	}

	// Bundling
	public String getParentApplicationName() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager, getParentApplicationName, entryPoint=" + entryPoint);
		}

		String parentAppName = "";

		if (entryPoint != null) {
			if (entryPoint.startsWith("vod_")) {
				parentAppName = "VOD";
			} else if (entryPoint.startsWith("fpr_")) {
				parentAppName = "Preview";
			} else if (entryPoint.startsWith("epg_")) {
				parentAppName = "EPG";
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager, getParentApplicationName, parentAppName=" + parentAppName);
		}

		return parentAppName;
	}

	public String getCurrentLocale() {
		String lang = (String) DataCenter.getInstance().get(PreferenceNames.LANGUAGE);

		if (lang.equals(Definitions.LANGUAGE_FRENCH)) {
			return ISAService.LOCALE_FR_CA;
		} else {
			return ISAService.LOCALE_EN_CA;
		}

	}

	public void requestServiceInformation(final String serviceId, final String entryPoint) {
		ISAController.getInstance().showLoading();

		this.serviceId = serviceId;
		this.entryPoint = entryPoint;

		new Thread(new Runnable() {
			public void run() {
				BufferedInputStream bis = null;
				HttpURLConnection connect = null;
				try {
					long startTime = System.currentTimeMillis();

					String urlStr;

					if (Environment.EMULATOR) {
						urlStr =
								"http://dev01il2a.int.videotron.com/subscription/services/isa/getServiceDescription?macAddress=48448719B23E&serviceId=M&requestLocale="
										+ getCurrentLocale() + "&entryPoint=testStub";
					} else {
						String EncodeServiceIdStr = TextUtil.urlEncode(serviceId);
						urlStr = URLCreator.getServiceResponseURL(EncodeServiceIdStr, entryPoint);
					}

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestServiceInformation(): urlStr=" + urlStr);
					}

					URL url = null;

					boolean useProxy = DataCenter.getInstance().getBoolean("USE_PROXY");
					String proxyIP = DataCenter.getInstance().getString("PROXY_IP");
					int port = DataCenter.getInstance().getInt("PORT");

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestServiceInformation(): useProxy=" + useProxy);
						Log.printDebug(
								"ISADataManager: requestServiceInformation(): proxyIP=" + proxyIP + ", port=" + port);
					}
					if (useProxy && proxyIP != null) {
						url = new URL("HTTP", proxyIP, port, urlStr);
					} else {
						url = new URL(urlStr);
					}

					// Debug
					lastURL = url.toString();

					connect = (HttpURLConnection) url.openConnection();

					int code = connect.getResponseCode();

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestServiceInformation(): response code : " + code);
					}

					if (code == HttpURLConnection.HTTP_NOT_FOUND) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ISADataManager: requestServiceInformation(): HTTP_NOT_FOUND!");
						}
						// return;
						throw new Exception("HTTP_NOT_FOUND");
					} else if (code == HttpURLConnection.HTTP_INTERNAL_ERROR) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ISADataManager: requestServiceInformation(): HTTP_INTERNAL_ERROR!");
						}
						// return;
						throw new Exception("HTTP_INTERNAL_ERROR");
					}

					bis = new BufferedInputStream(connect.getInputStream());

					if (Log.DEBUG_ON) {
						Log.printDebug(
								"ISADataManager: requestServiceInformation(): request page download!! pass parser~!" + (
										System.currentTimeMillis() - startTime));
					}

					int size = 0;
					byte[] buf = new byte[1024];

					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					while ((size = bis.read(buf)) > -1) {
						baos.write(buf, 0, size);
					}

					String src = baos.toString("UTF-8");

					// Debug
					lastResponse = src;

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestServiceInformation(): received data");
						Log.printDebug(src);
					}
					baos.flush();
					baos.close();
					ByteArrayInputStream bais = new ByteArrayInputStream(src.getBytes());

					SAXParserFactory factory = SAXParserFactory.newInstance();
					SAXParser parser = factory.newSAXParser();
					XMLParser handler = new XMLParser(XMLParser.DATA_TYPE_SERVICE_DESCRIPTION);
					parser.parse(bais, handler);

					ServiceDescriptionResponse sdResponse = (ServiceDescriptionResponse) DataCenter.getInstance()
							.get(Def.KEY_SERVICE_DESCRIPTION_RESPONSE);

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestServiceInformation(): sdResponse=" + sdResponse);
					}

					if (sdResponse.getSuccess().equalsIgnoreCase(RootMessage.SUCCESS)) {

						if (Log.EXTRA_ON || Log.ALL_ON) {
							errorCode = "";
							errorType = "";
							errorMessage = "";
						}

						ISAController.getInstance().showUIComponent(Def.SCENE_ID_SUBSCRIBE);
					} else {
						MessageContent mc = sdResponse.getMessageContent();

						if (Log.DEBUG_ON) {
							if (mc != null) {
								Log.printDebug(
										"ISADataManager: requestServiceInformation(): code=" + mc.getCode() + ", type="
												+ mc.getType());
								Log.printDebug("ISADataManager: requestServiceInformation(): message=" + mc.getValue());
							} else {
								Log.printDebug("ISADataManager: requestServiceInformation(): MessageContent is null");
							}
						}

						if (mc != null) {
							errorCode = mc.getCode();
							errorType = mc.getType();
							errorMessage = mc.getValue();

							Log.printError("ISA500: errorCode=" + errorCode + ", errorType=" + errorType);
							Log.printError("ISA500: errorMessage=" + errorMessage);

						}

						if (mc != null && (mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA10) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA11) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA12))) {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ORDER_TRACKING);
						} else if (mc != null && (mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA20) || mc
								.getCode().equalsIgnoreCase(MessageContent.CODE_ISA21) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA22) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA500) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA23))) {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
						} else {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ORDER_TRACKING);
						}
					}
				} catch (UnknownHostException e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} catch (IOException e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} catch (Exception e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} finally {
					ISAController.getInstance().hideLoading();
					if (bis != null) {
						try {
							bis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						bis = null;
					}

					if (connect != null) {
						connect.disconnect();
						connect = null;
					}
				}
			}
		}, "requestServiceInformation").start();

	}

	public void requestSubscribeToService(final boolean needDisCount, final int forceSubscriptionMode) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: requestSubscribeToService(): needDisCount=" + needDisCount
					+ ", forceSubscriptionMode=" + forceSubscriptionMode);
		}
		ISAController.getInstance().showLoading();
		new Thread(new Runnable() {
			public void run() {
				BufferedInputStream bis = null;
				HttpURLConnection connect = null;
				try {
					long startTime = System.currentTimeMillis();

					String urlStr;

					if (Environment.EMULATOR) {
						// urlStr =
						// "http://dev01il2a.int.videotron.com/subscription/services/isa/subscribeToService?macAddress=4844871A43F2&serviceId=M&requestLocale="
						// + getCurrentLocale() + "&entryPoint=testStub";
						// urlStr =
						// "http://dev01il2a.int.videotron.com/subscription/services/isa/subscribeToService?macAddress=48448719B23E&serviceId=M&requestLocale="
						// + getCurrentLocale() + "&entryPoint=testStub";
						if (needDisCount) {
							urlStr =
									"http://dev01il2a.int.videotron.com:80/subscription/services/isa/subscribeToService?macAddress=0021BED5777D&serviceId=HBOCH&requestLocale="
											+ getCurrentLocale() + "&entryPoint=testStub&discount=true";
						} else {
							urlStr =
									"http://dev01il2a.int.videotron.com:80/subscription/services/isa/subscribeToService?macAddress=0021BED5777D&serviceId=HBOCH&requestLocale="
											+ getCurrentLocale() + "&entryPoint=testStub";
						}

					} else {
						String EncodeServiceIdStr = TextUtil.urlEncode(serviceId);
						urlStr = URLCreator.getSubscribeToServiceURL(EncodeServiceIdStr,
								ISADataManager.getInstance().getCurrentEntryPoint(), needDisCount,
								forceSubscriptionMode);
					}

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestSubscribeToService(): urlStr=" + urlStr);
					}

					URL url = null;

					boolean useProxy = DataCenter.getInstance().getBoolean("USE_PROXY");
					String proxyIP = DataCenter.getInstance().getString("PROXY_IP");
					int port = DataCenter.getInstance().getInt("PORT");

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestSubscribeToService(): useProxy=" + useProxy);
						Log.printDebug(
								"ISADataManager: requestSubscribeToService(): proxyIP=" + proxyIP + ", port=" + port);
					}
					if (useProxy && proxyIP != null) {
						url = new URL("HTTP", proxyIP, port, urlStr);
					} else {
						url = new URL(urlStr);
					}

					// Debug
					lastURL = url.toString();

					connect = (HttpURLConnection) url.openConnection();

					int code = connect.getResponseCode();

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestSubscribeToService(): response code : " + code);
					}

					if (code == HttpURLConnection.HTTP_NOT_FOUND) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ISADataManager: requestSubscribeToService(): HTTP_NOT_FOUND!");
						}
						// return;
						throw new Exception("HTTP_NOT_FOUND");
					} else if (code == HttpURLConnection.HTTP_INTERNAL_ERROR) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ISADataManager: requestSubscribeToService(): HTTP_INTERNAL_ERROR!");
						}
						// return;
						throw new Exception("HTTP_INTERNAL_ERROR");
					}

					bis = new BufferedInputStream(connect.getInputStream());

					if (Log.DEBUG_ON) {
						Log.printDebug(
								"ISADataManager: requestSubscribeToService(): request page download!! pass parser~!" + (
										System.currentTimeMillis() - startTime));
					}

					int size = 0;
					byte[] buf = new byte[1024];

					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					while ((size = bis.read(buf)) > -1) {
						baos.write(buf, 0, size);
					}

					String src = baos.toString("UTF-8");

					// Debug
					lastResponse = src;

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestSubscribeToService(): received data");
						Log.printDebug(src);
					}
					baos.flush();
					baos.close();
					ByteArrayInputStream bais = new ByteArrayInputStream(src.getBytes());

					SAXParserFactory factory = SAXParserFactory.newInstance();
					SAXParser parser = factory.newSAXParser();
					XMLParser handler = new XMLParser(XMLParser.DATA_TYPE_RESPONSE);
					parser.parse(bais, handler);

					RootMessage replyMessage = (RootMessage) DataCenter.getInstance().get(Def.KEY_REPLY_MESSAGE);

					if (replyMessage.getSuccess().equalsIgnoreCase(RootMessage.SUCCESS)) {

						if (Log.EXTRA_ON || Log.ALL_ON) {
							errorCode = "";
							errorType = "";
							errorMessage = "";
						}

						if (replyMessage.getMessageContent() != null && replyMessage.getMessageContent().getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA20)) {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
						} else {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ORDER_CONFIRMATION);
						}
					} else {
						MessageContent mc = replyMessage.getMessageContent();

						if (Log.DEBUG_ON) {
							if (mc != null) {
								Log.printDebug(
										"ISADataManager: requestSubscribeToService(): code=" + mc.getCode() + ", type="
												+ mc.getType());
								Log.printDebug("ISADataManager: requestSubscribeToService(): message=" + mc.getValue());
							} else {
								Log.printDebug("ISADataManager: requestSubscribeToService(): MessageContent is null");
							}
						}

						if (mc != null) {
							errorCode = mc.getCode();
							errorType = mc.getType();
							errorMessage = mc.getValue();

							Log.printError("ISA500: errorCode=" + errorCode + ", errorType=" + errorType);
							Log.printError("ISA500: errorMessage=" + errorMessage);

						}

						if (mc != null && (mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA10) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA11) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA12))) {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ORDER_TRACKING);
						} else if (mc != null && (mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA20) || mc
								.getCode().equalsIgnoreCase(MessageContent.CODE_ISA21) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA22) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA500) || mc.getCode()
								.equalsIgnoreCase(MessageContent.CODE_ISA23))) {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
						} else if (mc != null && mc.getCode().equalsIgnoreCase(MessageContent.CODE_ISA30)) {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_DISCOUNT);
						} else {
							ISAController.getInstance().showUIComponent(Def.SCENE_ID_ORDER_TRACKING);
						}
					}
				} catch (UnknownHostException e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} catch (IOException e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} catch (Exception e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} finally {
					ISAController.getInstance().hideLoading();
					if (bis != null) {
						try {
							bis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						bis = null;
					}

					if (connect != null) {
						connect.disconnect();
						connect = null;
					}
				}
			}
		}, "requestSubscribeToService").start();

	}

	public void requestRegisterTracking(final String phoneNumber) {
		ISAController.getInstance().showLoading();
		new Thread(new Runnable() {
			public void run() {
				BufferedInputStream bis = null;
				HttpURLConnection connect = null;
				try {
					long startTime = System.currentTimeMillis();

					String urlStr;

					if (Environment.EMULATOR) {
						// urlStr =
						// "http://dev01il2a.int.videotron.com/subscription/services/isa/subscribeToService?macAddress=4844871A43F2&serviceId=M&requestLocale="
						// + getCurrentLocale() + "&entryPoint=testStub";
						urlStr =
								"http://dev01il2a.int.videotron.com/subscription/services/isa/registerToOrderTracking?macAddress=48448719B23E&serviceId=HBOC&requestLocale="
										+ getCurrentLocale() + "&entryPoint=testStub";
					} else {
						String EncodeServiceIdStr = TextUtil.urlEncode(serviceId);
						urlStr = URLCreator.getRegisterToOrderTrackingURL(phoneNumber, EncodeServiceIdStr,
								ISADataManager.getInstance().getCurrentEntryPoint());
					}
					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestRegisterTracking(): urlStr=" + urlStr);
					}

					URL url = null;

					boolean useProxy = DataCenter.getInstance().getBoolean("USE_PROXY");
					String proxyIP = DataCenter.getInstance().getString("PROXY_IP");
					int port = DataCenter.getInstance().getInt("PORT");

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestRegisterTracking(): useProxy=" + useProxy);
						Log.printDebug(
								"ISADataManager: requestRegisterTracking(): proxyIP=" + proxyIP + ", port=" + port);
					}
					if (useProxy && proxyIP != null) {
						url = new URL("HTTP", proxyIP, port, urlStr);
					} else {
						url = new URL(urlStr);
					}

					// Debug
					lastURL = url.toString();

					connect = (HttpURLConnection) url.openConnection();

					int code = connect.getResponseCode();

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestRegisterTracking(): response code : " + code);
					}

					if (code == HttpURLConnection.HTTP_NOT_FOUND) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ISADataManager: requestRegisterTracking(): HTTP_NOT_FOUND!");
						}
						// return;
						throw new Exception("HTTP_NOT_FOUND");
					} else if (code == HttpURLConnection.HTTP_INTERNAL_ERROR) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ISADataManager: requestRegisterTracking(): HTTP_INTERNAL_ERROR!");
						}
						// return;
						throw new Exception("HTTP_INTERNAL_ERROR");
					}

					bis = new BufferedInputStream(connect.getInputStream());

					if (Log.DEBUG_ON) {
						Log.printDebug(
								"ISADataManager: requestRegisterTracking(): request page download!! pass parser~!" + (
										System.currentTimeMillis() - startTime));
					}

					int size = 0;
					byte[] buf = new byte[1024];

					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					while ((size = bis.read(buf)) > -1) {
						baos.write(buf, 0, size);
					}

					String src = baos.toString("UTF-8");

					// Debug
					lastResponse = src;

					if (Log.DEBUG_ON) {
						Log.printDebug("ISADataManager: requestRegisterTracking(): received data");
						Log.printDebug(src);
					}
					baos.flush();
					baos.close();
					ByteArrayInputStream bais = new ByteArrayInputStream(src.getBytes());

					SAXParserFactory factory = SAXParserFactory.newInstance();
					SAXParser parser = factory.newSAXParser();
					XMLParser handler = new XMLParser(XMLParser.DATA_TYPE_RESPONSE);
					parser.parse(bais, handler);

					RootMessage replyMessage = (RootMessage) DataCenter.getInstance().get(Def.KEY_REPLY_MESSAGE);

					MessageContent mc = replyMessage.getMessageContent();

					if (Log.DEBUG_ON) {
						Log.printDebug(
								"ISADataManager: requestRegisterTracking(): code=" + mc.getCode() + ", type=" + mc
										.getType());
						Log.printDebug("ISADataManager: requestRegisterTracking(): message=" + mc.getValue());
					}

					if (replyMessage.getSuccess().equalsIgnoreCase(RootMessage.SUCCESS)) {

						if (Log.EXTRA_ON || Log.ALL_ON) {
							errorCode = "";
							errorType = "";
							errorMessage = "";
						}

						ISAController.getInstance().showUIComponent(Def.SCENE_ID_COMPLETE_ORDER_TRACKING);
					} else {

						if (mc != null) {
							errorCode = mc.getCode();
							errorType = mc.getType();
							errorMessage = mc.getValue();

							Log.printError("ISA500: errorCode=" + errorCode + ", errorType=" + errorType);
							Log.printError("ISA500: errorMessage=" + errorMessage);

						}

						ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
					}
				} catch (UnknownHostException e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} catch (IOException e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} catch (Exception e) {
					Log.printError("ISA500:" + e);
					ISAController.getInstance().showUIComponent(Def.SCENE_ID_ERROR);
				} finally {
					ISAController.getInstance().hideLoading();
					if (bis != null) {
						try {
							bis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						bis = null;
					}

					if (connect != null) {
						connect.disconnect();
						connect = null;
					}
				}
			}
		}, "requestRegisterTracking").start();
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorType() {
		return errorType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getLastURL() {
		return lastURL;
	}

	public String getLastResponse() {
		return lastResponse;
	}

	public void addISAServiceData(String serviceType, int serviceNumber, String serviceId) {
		if (Log.DEBUG_ON) {
			Log.printDebug(
					"ISADataManager: addISAServiceData: serviceType=" + serviceType + ", serviceNumber=" + serviceNumber
							+ ", serviceId=" + serviceId);
		}

		// Bundling
		OOBData oobData = new OOBData(serviceType, serviceNumber, serviceId);
		oobDataVector.add(oobData);

		if (serviceType.equals(MS_ISA_SERVICE_TYPE_CHANNEL)) {
			channelList.add(serviceId);
		} else if (serviceType.equals(MS_ISA_SERVICE_TYPE_FREE_PREVIEW)) {
			freePreviewList.add(serviceId);
		} else if (serviceType.equals(MS_ISA_SERVICE_TYPE_ILLICO_SERVICE)) {
			illicoServiceList.add(serviceId);
		}
	}

	// Bundling

	/**
	 * @param serviceType
	 * @param serviceNumber
	 * @param combinable
	 */
	public void setCombinableState(int idx, String serviceType, int serviceNumber, String combinable) {
		if (Log.DEBUG_ON) {
			Log.printDebug(
					"ISADataManager: setCombinableState: idx=" + idx + "serviceType=" + serviceType + ", serviceNumber="
							+ serviceNumber + ", combinable=" + combinable);
		}
		if (oobDataVector != null) {
			OOBData oobData = (OOBData) oobDataVector.get(idx);

			if (MS_ISA_COMBINABLE_TRUE.equals(combinable)) {
				oobData.setCombinable(true);
			}
		}
	}

	// Bundling
	public boolean isCombinable() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: isCombinable");
		}
		return isCombinable(serviceId, entryPoint);
	}

	/**
	 * @param entryPoint
	 * @param serviceId
	 * @return
	 */
	public boolean isCombinable(String serviceId, String entryPoint) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: isCombinable: entryPoint=" + entryPoint + ", serviceId=" + serviceId);
		}

		String serviceType = MS_ISA_SERVICE_TYPE_CHANNEL;

		if (entryPoint.startsWith("fpr_E")) {
			serviceType = MS_ISA_SERVICE_TYPE_FREE_PREVIEW;
		} else if (entryPoint.startsWith("vod_E") && !entryPoint.equals(ISAService.ENTRY_POINT_VOD_E01) && !entryPoint
				.equals(ISAService.ENTRY_POINT_VOD_E02) && !entryPoint.equals(ISAService.ENTRY_POINT_VOD_E03)) {
			serviceType = MS_ISA_SERVICE_TYPE_ILLICO_SERVICE;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: isCombanable: serviceType=" + serviceType);
		}

		if (oobDataVector != null) {
			Enumeration enu = oobDataVector.elements();

			while (enu.hasMoreElements()) {
				OOBData oobData = (OOBData) enu.nextElement();

				if (Log.DEBUG_ON) {
					Log.printDebug("ISADataManager: isCombanable: oobData=" + oobData);
				}

				if (oobData.getServiceType().equals(serviceType) && oobData.getServiceId().equals(serviceId)) {
					return oobData.isCombinable();
				}
			}
		}

		return false;
	}

	// Bunding
	public void setDncsPackages(String[] dncsPackages) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: setDncsPacakges: dncsPackages=" + dncsPackages);
		}

		this.dncsPackages = dncsPackages;
	}

	// Bundling
	public boolean hasDNCSPackage() {

		MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: hasDNCSPackage: dncsPackages=" + dncsPackages + ", mService=" + mService);
		}

		if (dncsPackages != null && mService != null) {

			for (int i = 0; i < dncsPackages.length; i++) {
				try {
					int ret = mService.checkResourceAuthorization(dncsPackages[i]);

					if (Log.DEBUG_ON) {
						Log.printDebug(
								"ISADataManager: hasDNCSPackage: mService.checkResourceAuthorization ret=" + ret);
					}

					switch (ret) {
					case MonitorService.NOT_AUTHORIZED:
						Log.printDebug("CommunicationManager checkPackage return = NOT_AUTHORIZED");
						break;
					case MonitorService.AUTHORIZED:
						Log.printDebug("CommunicationManager checkPackage return = AUTHORIZED");
						break;
					case MonitorService.NOT_FOUND_ENTITLEMENT_ID:
						Log.printDebug("CommunicationManager checkPackage return = NOT_FOUND_ENTITLEMENT_ID");
						break;
					case MonitorService.CHECK_ERROR:
						Log.printDebug("CommunicationManager checkPackage return = CHECK_ERROR");
						break;
					}

					return ret == MonitorService.AUTHORIZED;

				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

		}

		return false;
	}

	// Bundling
	public void setExcludedChannels(String[] excludedChannels) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: setExcludedChannels: excludedChannels=" + excludedChannels);
		}
		this.excludedChannels = excludedChannels;
	}

	// Bundling
	public String[] getExcludedChannels() {
		return excludedChannels;
	}

	// Bundling
	public void setExcludedChannelTypes(int[] excludedChannelTypes) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: setExcludedChannelTypes: excludedChannelTypes=" + excludedChannelTypes);
		}
		this.excludedChannelTypes = excludedChannelTypes;
	}

	// Bundling
	public int[] getExcludedChannelTypes() {
		return excludedChannelTypes;
	}

	// Bundling
	public boolean isIncludedInExcludedChannels(String serviceId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: isIncludedInExcludedChannels: serviceId=" + serviceId);
		}
		if (excludedChannels != null) {
			for (int i = 0; i < excludedChannels.length; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug(
							"ISADataManager: setExcludedChannels: excludedChannels[" + i + "]=" + excludedChannels[i]);
				}
				if (excludedChannels[i].equals(serviceId)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isIncludedInExcludedChannelTypes(String serviceId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: isIncludedInExcludedChannelTypes: serviceId=" + serviceId);
		}

		EpgService eService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);

		if (excludedChannelTypes != null) {
			TvChannel ch = null;
			try {
				ch = eService.getChannel(serviceId);

				if (ch != null) {
					for (int i = 0; i < excludedChannelTypes.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug(
									"ISADataManager: isIncludedInExcludedChannelTypes: ch.getType=" + ch.getType());
						}
						if (excludedChannelTypes[i] == ch.getType()) {
							return true;
						}
					}

				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	public String[] getISAServiceList(String isaServiceType) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: getISAServiceList: isaServiceType=" + isaServiceType);
		}

		String[] result = null;

		if (isaServiceType.equals(ISAService.ISA_SERVICE_TYPE_CHANNEL)) {
			result = new String[channelList.size()];
			channelList.copyInto(result);
		} else if (isaServiceType.equals(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW)) {
			result = new String[freePreviewList.size()];
			freePreviewList.copyInto(result);
		} else if (isaServiceType.equals(ISAService.ISA_SERVICE_TYPE_ILLICO_SERVICE)) {
			result = new String[illicoServiceList.size()];
			illicoServiceList.copyInto(result);
		}

		if (result == null) {
			result = new String[0];
		}

		if (Log.DEBUG_ON) {

			if (result.length > 0) {
				for (int i = 0; i < result.length; i++) {
					Log.printDebug("ISADataManager: getISAServiceList(): result[" + i + "]=" + result[i]);
				}
			} else {
				Log.printDebug("ISADataManager: getISAServiceList(): result's size is zero");
			}
		}

		return result;
	}

	public boolean checkISAService(String isaServiceId, String entryPoint) {
		if (Log.DEBUG_ON) {
			Log.printDebug(
					"ISADataManager: checkISAService: isaServiceId=" + isaServiceId + ", entryPoint=" + entryPoint);
		}
		isIllicoService = false;

		String serviceType = ISAService.ISA_SERVICE_TYPE_CHANNEL;

		if (entryPoint.startsWith("fpr_E")) {
			serviceType = ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW;
		} else if (entryPoint.startsWith("vod_E") && !entryPoint.equals(ISAService.ENTRY_POINT_VOD_E01) && !entryPoint
				.equals(ISAService.ENTRY_POINT_VOD_E02) && !entryPoint.equals(ISAService.ENTRY_POINT_VOD_E03)) {
			serviceType = ISAService.ISA_SERVICE_TYPE_ILLICO_SERVICE;
			isIllicoService = true;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: checkISAService: serviceType=" + serviceType);
		}

		String[] list = getISAServiceList(serviceType);

		for (int j = 0; j < list.length; j++) {
			if (list[j].equals(isaServiceId)) {
				return true;
			}
		}

		if (serviceType == ISAService.ISA_SERVICE_TYPE_CHANNEL) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ISADataManager: checkISAService: try to find on ISA_SERVICE_TYPE_FREE_PREVIEW");
			}
			list = getISAServiceList(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW);

			for (int j = 0; j < list.length; j++) {
				if (list[j].equals(isaServiceId)) {
					return true;
				}
			}
		}

		return false;
	}

	public boolean isIllicoService() {
		return isIllicoService;
	}

	public void addISAServiceDataListener(ISAServiceDataListener l, String appName) {
		if (Log.DEBUG_ON) {
			Log.printDebug(
					"ISADataManager: addISAServiceDataListener: ISAServiceDataListener=" + l + ", appName=" + appName);
		}

		synchronized (listeners) {
			if (listeners.containsKey(appName)) {
				listeners.remove(appName);
				listeners.put(appName, l);
			} else {
				listeners.put(appName, l);
			}
		}
	}

	public void removeISAServiceDataListener(String appName) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: removeISAServiceDataListener: appName=" + appName);
		}

		synchronized (listeners) {
			listeners.remove(appName);
		}
	}

	public void completeISAServiceData() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ISADataManager: completeISAServiceData");
		}

		String[] channelList = getISAServiceList(ISAService.ISA_SERVICE_TYPE_CHANNEL);
		String[] freePreviewList = getISAServiceList(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW);
		String[] illicoServiceList = getISAServiceList(ISAService.ISA_SERVICE_TYPE_ILLICO_SERVICE);

		synchronized (listeners) {
			Enumeration keys = listeners.keys();

			while (keys.hasMoreElements()) {
				String appName = (String) keys.nextElement();

				ISAServiceDataListener l = (ISAServiceDataListener) listeners.get(appName);

				if (Log.DEBUG_ON) {
					Log.printDebug("ISADataManager: completeISAServiceData: appName=" + appName);
				}

				try {
					l.updateISAServiceList(ISAService.ISA_SERVICE_TYPE_CHANNEL, channelList);
					l.updateISAServiceList(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW, freePreviewList);
					l.updateISAServiceList(ISAService.ISA_SERVICE_TYPE_ILLICO_SERVICE, illicoServiceList);
					l.updateISAExcludedChannelsAndTypes(excludedChannels, excludedChannelTypes);
				} catch (Exception e) {
					Log.print(e);
				}
			}
		}
	}

	/**
	 * Returns a String from the byte array.
	 *
	 * @param data byte array
	 * @return String
	 */
	public String getStringBYTEArray(byte[] data) {
		if (data == null) {
			return "";
		}
		return getStringBYTEArrayOffset(data, 0);
	}

	/**
	 * Returns a String from the byte array.
	 *
	 * @param data   byte array
	 * @param offset offset
	 * @return String
	 */
	public String getStringBYTEArrayOffset(byte[] data, int offset) {
		if (data == null) {
			return "";
		}

		StringBuffer buff = new StringBuffer(4096);
		for (int i = offset; i < offset + data.length; i++) {
			buff.append(getHexStringBYTE(data[i]));
			if (i != data.length - 1) {
				buff.append("");
			}
		}
		return buff.toString();
	}

	/**
	 * Returns a String from the byte array.
	 *
	 * @param b byte
	 * @return String
	 */
	public String getHexStringBYTE(byte b) {
		String str = "";

		// handle the case of -1
		if (b == -1) {
			str = "FF";
			return str;
		}

		int value = (((int) b) & 0xFF);

		if (value < 16) {
			// pad out string to make it look nice
			str = "0";
		}
		str += (Integer.toHexString(value)).toUpperCase();
		return str;
	}
}

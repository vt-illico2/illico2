/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * 
 * Code	Type	Description <br>
	ISA00	0	Order Confirmation (club) <br>
	ISA01	0	Order Confirmation (new channel)<br>
	ISA10	1	SGA Down. <br>
	ISA11	1	Order failure (1).<br>
	ISA12	1	Technical problem. <br>
	ISA20	2	Order Confirmation (Free preview)<br>
	ISA21	2	Phone number registering failure.<br>
	ISA23	2	Pending order<br>
	ISA30	3	Discount refused for Club Unlimited.<br>
	ISA40	4	Phone number registering confirmation<br>
	ISA500	2	Generic System error <br>
 * 
 * @author zestyman
 * 
 */
public class MessageContent extends Description {

	/**
	 * Value of '0' is for Information screen. Typically, Information Screen is
	 * used for confirmation message.
	 */
	public static final String TYPE_0 = "0";

	/**
	 * Value of '1' is for Error Screen. Error screen with phone number field
	 * and Call me button to be used as needed by user to receive a call back
	 * from a customer support agent.
	 */
	public static final String TYPE_1 = "1";

	/**
	 * Value of '2' is for System error. System errors occur at the backend side
	 * because of some system failure or network issue.
	 */
	public static final String TYPE_2 = "2";
	
	public static final String TYPE_3 = "3";
	public static final String TYPE_4 = "4";
	
	/**
	 * ISA00	0	Order Confirmation (club)
	 */
	public static final String CODE_ISA00 = "ISA00";
	
	/**
	 * ISA01	0	Order Confirmation (new channel)
	 */
	public static final String CODE_ISA01 = "ISA01";
	
	/**
	 * ISA10	1	SGA Down.
	 */
	public static final String CODE_ISA10 = "ISA10";
	
	/**
	 * ISA11	1	Order failure (1).
	 */
	public static final String CODE_ISA11 = "ISA11";
	
	/**
	 * ISA12	1	Technical problem. 
	 */
	public static final String CODE_ISA12 = "ISA12";
	
	/**
	 * ISA20	2	Order Confirmation (Free preview)
	 */
	public static final String CODE_ISA20 = "ISA20";
	
	/**
	 * ISA21	2	Phone number registering failure.
	 */
	public static final String CODE_ISA21 = "ISA21";
	
	/**
	 * ISA22	2	the specific situation of customer with balance due or with « rating at risk»
	 */
	public static final String CODE_ISA22 = "ISA22";
	
	/**
	 * ISA23	2	Pending order
	 */
	public static final String CODE_ISA23 = "ISA23";
	
	/**
	 * ISA30	3	Discount refused for Club Unlimited.
	 */
	public static final String CODE_ISA30 = "ISA30";
	
	/**
	 * ISA40	4	Phone number registering confirmation
	 */
	public static final String CODE_ISA40 = "ISA40";
	
	/**
	 * ISA500	5	Generic System error 
	 */
	public static final String CODE_ISA500 = "ISA500";

	/**
	 * Unique message identifier
	 */
	private String code;

	/**
	 * Used to specify the type of message
	 * 
	 * Value of '0' is for Information screen. Typically, Information Screen is
	 * used for confirmation message.
	 * 
	 * Value of '1' is for Error Screen. Error screen with phone number field
	 * and Call me button to be used as needed by user to receive a call back
	 * from a customer support agent.
	 * 
	 * Value of '2' is for System error. System errors occur at the backend side
	 * because of some system failure or network issue.
	 */
	private String type;

	/**
	 * popupTitle tag: specify the text to be displayed in the popup title zone
	 */
	private PopupTitle popupTitle;
	
	/**
	 * topMessageText tag: specify the message to be display on top of the screen.
	 */
	private ShortDescription topMessageText;
	
	/**
	 * bottomMessageText tag (optional): specify the message to be displayed on the bottom of the screen.
	 */
	private ShortDescription bottomMessageText;
	
	private ShortDescription internalInfo;
	
	/**
	 * 
	 */
	public MessageContent() {
		code = "";
		type = "";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public PopupTitle getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(PopupTitle popupTitle) {
		this.popupTitle = popupTitle;
	}

	public ShortDescription getTopMessageText() {
		return topMessageText;
	}

	public void setTopMessageText(ShortDescription topMessageText) {
		this.topMessageText = topMessageText;
	}

	public ShortDescription getBottomMessageText() {
		return bottomMessageText;
	}

	public void setBottomMessageText(ShortDescription bottomMessageText) {
		this.bottomMessageText = bottomMessageText;
	}
	
	public ShortDescription getInternalInfo() {
		return internalInfo;
	}

	public void setInternalInfo(ShortDescription internalInfo) {
		this.internalInfo = internalInfo;
	}

	public void proccessAttributes(Attributes attributes) {

		for (int i = 0; i < attributes.getLength(); i++) {
			String localName = attributes.getLocalName(i);
			String value = attributes.getValue(i);

			if (localName.equalsIgnoreCase("CODE")) {
				setCode(value);
			} else if (localName.equalsIgnoreCase("TYPE")) {
				setType(value);
			}
		}

	}

}

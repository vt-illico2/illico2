package com.videotron.tvi.illico.isa.data.obj;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-08-19.
 */
public class OOBData {

	String serviceType;
	int serviceNumber;
	String serviceId;
	boolean combinable;

	public OOBData(String serviceType, int serviceNumber, String serviceId) {
		this.serviceType = serviceType;
		this.serviceNumber = serviceNumber;
		this.serviceId = serviceId;

	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public int getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(int serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public boolean isCombinable() {
		return combinable;
	}

	public void setCombinable(boolean combinable) {
		this.combinable = combinable;
	}

	public String toString() {
		return super.toString() + ", serviceType=" + serviceType + ", serviceNumber=" + serviceNumber + ", serviceId="
				+ serviceId + ", combinable=" + combinable;
	}
}

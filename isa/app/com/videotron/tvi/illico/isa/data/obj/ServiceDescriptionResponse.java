/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 * 
 */
public class ServiceDescriptionResponse extends RootMessage {

	private ServiceDescription serviceDescription;
	private GeneralInformation generalInformation;

	/**
	 * 
	 */
	public ServiceDescriptionResponse() {
	}

	public ServiceDescription getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(ServiceDescription serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public GeneralInformation getGeneralInformation() {
		return generalInformation;
	}

	public void setGeneralInformation(GeneralInformation generalInformation) {
		this.generalInformation = generalInformation;
	}

	public void proccessAttributes(Attributes attributes) {

		super.proccessAttributes(attributes);

	}
}

/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 *
 */
public class GeneralInformation extends BaseElement {

	private LegalText legalText;
	private FAPL fapl;
	
	/**
	 * 
	 */
	public GeneralInformation() {
	}

	public LegalText getLegalText() {
		return legalText;
	}

	public void setLegalText(LegalText legalText) {
		this.legalText = legalText;
	}

	public FAPL getFapl() {
		return fapl;
	}

	public void setFapl(FAPL fapl) {
		this.fapl = fapl;
	}

	public void proccessAttributes(Attributes attributes) {
		
	}

	
}

/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.log.Log;

/**
 * @author zestyman
 *
 */
public class PopupTitle extends BaseElement {

	/**
	 *  0: no icon,
	 */
	public static final String ICON_TYPE_0 = "0";
	/** 
	 * 1: confirmation icon
	 */
	public static final String ICON_TYPE_1 = "1";
	/**
	 * 2: warning icon
	 */
	public static final String ICON_TYPE_2 = "2";
	
	
	private String iconType;
	/**
	 * 
	 */
	public PopupTitle() {
		iconType = ICON_TYPE_0;
	}
	
	public String getIconType() {
		return iconType;
	}

	public void setIconType(String iconType) {
		this.iconType = iconType;
	}

	public void proccessAttributes(Attributes attributes) {
		for (int i = 0; i < attributes.getLength(); i++) {
			String localName = attributes.getLocalName(i);
			String value = attributes.getValue(i);
			
			if (localName.equalsIgnoreCase("iconType")) {
				setIconType(value);
			}
		}

	}

	

}

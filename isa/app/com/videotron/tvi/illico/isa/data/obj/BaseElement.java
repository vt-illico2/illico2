package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

public abstract class BaseElement {

	private String value;
	
	public BaseElement() {
	}
	
	

	public String getValue() {
		return value;
	}



	public void setValue(String value) {
		this.value = value;
	}



	public abstract void proccessAttributes(Attributes attributes);
}

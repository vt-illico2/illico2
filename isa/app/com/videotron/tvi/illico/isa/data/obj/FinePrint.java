/**
 * 
 */
package com.videotron.tvi.illico.isa.data.obj;

import org.xml.sax.Attributes;

/**
 * @author zestyman
 *
 */
public class FinePrint extends BaseElement {

	private String language;
	
	/**
	 * 
	 */
	public FinePrint() {
	}
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void proccessAttributes(Attributes attributes) {
		for (int i = 0; i < attributes.getLength(); i++) {
			String localName = attributes.getLocalName(i);
			String value = attributes.getValue(i);
			
			if (localName.equalsIgnoreCase("language")) {
				setLanguage(value);
			}
		}
	}

}

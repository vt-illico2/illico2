/**_SUM
 * 
 */
package com.videotron.tvi.illico.isa;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author zestyman
 *
 */
public class Def {

	public static final int SCENE_ID_SUBSCRIBE = 0;
	public static final int SCENE_ID_ORDER_CONFIRMATION = 1;
	public static final int SCENE_ID_CYO = 2;
	public static final int SCENE_ID_COMPLETE_ORDER_TRACKING = 3;
	public static final int SCENE_ID_ORDER_TRACKING = 4;
	public static final int SCENE_ID_ERROR = 5;
	public static final int SCENE_ID_DISCOUNT = 6;
	// Bundling
	public static final int SCENE_ID_OPTIMIZATION = 7;
	public static final int SCENE_ID_REGULAR_ISA = 8;

	
	public static final int VK_LEFT = OCRcEvent.VK_LEFT;
	public static final int VK_RIGHT = OCRcEvent.VK_RIGHT;
	public static final int VK_UP = OCRcEvent.VK_UP;
	public static final int VK_DOWN = OCRcEvent.VK_DOWN;
	public static final int VK_BACK = KeyCodes.LAST;
	public static final int VK_EXIT = OCRcEvent.VK_EXIT;
	public static final int VK_OK = OCRcEvent.VK_ENTER;
	public static final int VK_PAGE_UP = OCRcEvent.VK_PAGE_UP;
	public static final int VK_PAGE_DOWN = OCRcEvent.VK_PAGE_DOWN;
	
	public static final String CH_NAME = "CH_NAME";
	
	public static final String KEY_SERVICE_DESCRIPTION_RESPONSE = "SERVICE_DESCRIPTION_RESPONSE";
	public static final String KEY_REPLY_MESSAGE = "REPLY_MESSAGE";
	
	public static final String KEY_REMOTE_IMAGE = "REMOTE_IMAGE";
	
	public static final String KEY_ERROR_POPUP_TITLE = "ERROR_POPUP_TITLE";
	public static final String KEY_ERROR_POPUP_EXPLAIN = "ERROR_POPUP_EXPLAIN";
	
	//// Label Text
	public static final String KEY_OR = "or";
	public static final String KEY_SUBSCRIBE_NOW = "subscribe.now";
	public static final String KEY_CLOSE = "close";
	public static final String KEY_ADD_THIS_CHANNEL = "add.this.channel";
	
	
	public static final String KEY_VISIT_URL = "visit.url";	//"www.videotron.com/changechannels";
	public static final String KEY_DIAL_NUMBER = "dial.number"; //"1-88-VIDEOTRON";
	
	public static final String KEY_OK = "ok";
	
	public static final String KEY_CALL_ME = "call.me";
	public static final String KEY_CLEAR = "clear";
	public static final String KEY_CANCEL = "cancel";
	
	public static final String KEY_MONTH = "month";
	public static final String KEY_BTN_SCROLL = "btn.scroll";
	
	public static final String KEY_TITLE_ERROR = "title.error";
	public static final String KEY_ENTER_YOUR_PHONE_NUMBER = "enter.your.phone.number";
	public static final String KEY_CYO_TITLE_PREFIX = "cyo.title.prefix";
	public static final String KEY_CYO_TITLE_SUBFIX = "cyo.title.subfix";
	public static final String KEY_CYO_EXPLAIN = "cyo.explain";
	public static final String KEY_PIN_EXPLAIN = "pin.explain";
	public static final String KEY_MESSAGE_INVAILD_PHONE = "message.invalid.phone";
	public static final String KEY_MESSAGE_PROBLEM_SUBMTTING_ORDER = "message.problem.submitting.order";
	public static final String KEY_COMPLETE_TRACKING_TITLE = "complete.tracking.title";
	public static final String KEY_COMPLETE_TRACKING_EXPLAIN = "complete.tracking.explain";
	
	public static final String KEY_TITLE_GENERIC_ERROR = "title.generic.error";
	public static final String KEY_MESSAGE_GENERIC_ERROR = "message.generic.error";
	
	public static final String KEY_CLUB_UNLIMITED = "club.unlimited";
	public static final String KEY_MODIFY_CHANNEL = "modify.channel";
	public static final String KEY_A_LA_CARTE_CHANNEL = "a.la.carte.channel";
	public static final String KEY_OPTIMIZATION_TITLE = "optimization.title";
	public static final String KEY_OPTIMIZATION_EXPLAIN = "optimization.explain";
	public static final String KEY_OPTIMIZATION_BUTTON1 = "optimization.button1";
	public static final String KEY_OPTIMIZATION_BUTTON2 = "optimization.button2";
	public static final String KEY_REGULAR_EXPLAIN = "regular.explain";
}

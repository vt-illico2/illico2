/**
 * @(#)CommunicationManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.isa.communication;

import java.rmi.AlreadyBoundException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * The CommunicationManager class lookup a remote Service of Other Applcation.
 * @author Woojung Kim
 * @version 1.1
 */
public class CommunicationManager {

    /** The XletContext of ISA Application. */
    private XletContext xletContext;
    /** The instance of CommunicationManager. */
    private static CommunicationManager instance = new CommunicationManager();
    
    private ISAService isaServiceImpl;
    
    private boolean isBinded;

    IxcLookupWorker lookupWorker;
    
    /**
     * Instantiates a new CommunicationManager.
     * @return Singleton instance of this class.
     */
    public static synchronized CommunicationManager getInstance() {
        return instance;
    }

    /**
     * Start a lookups.
     * @param xContext XletContext for CYO application.
     */
    public void start(XletContext xContext) {
        if (Log.INFO_ON) {
            Log.printInfo("CommunicationManager: start()");
        }
        this.xletContext = xContext;

        lookupWorker = new IxcLookupWorker(xletContext);
        
        lookupWorker.lookup("/1/1/", MonitorService.IXC_NAME);
        lookupWorker.lookup("/1/2026/", EpgService.IXC_NAME);
        lookupWorker.lookup("/1/2030/", PreferenceService.IXC_NAME);
        lookupWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME);
        lookupWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME);
        lookupWorker.lookup("/1/2100/", StcService.IXC_NAME);
        lookupWorker.lookup("/1/2085/", VbmService.IXC_NAME);
        
        bindISAService();
        
    }

    /**
     * Dispose a resources.
     */
    public void dispose() {
    }
    
    public void bindISAService() {
    	if (Log.INFO_ON) {
            Log.printInfo("CommunicationManager: bindISAService()");
        }
    	
    	
    	if (isaServiceImpl == null) {
    		isaServiceImpl = new ISAServiceImpl();
    	}
    	
    	if (Log.DEBUG_ON) {
    		Log.printDebug("CommunicationManager: isaServiceImpl=" + isaServiceImpl);
    	}
    	
    	try {
			IxcRegistry.bind(xletContext, ISAService.IXC_NAME, isaServiceImpl);
			isBinded = true;
		} catch (AlreadyBoundException e) {
			Log.print(e);
		}
    	
    	if (Log.INFO_ON) {
            Log.printInfo("CommunicationManager: bindISAService() complete.");
        }
    }
    
    public boolean isBinded() {
    	return isBinded;
    }
}

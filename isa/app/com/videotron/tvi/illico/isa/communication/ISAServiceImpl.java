/**
 * 
 */
package com.videotron.tvi.illico.isa.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.isa.controller.ISAController;
import com.videotron.tvi.illico.isa.data.ISADataManager;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.isa.ISAServiceDataListener;

/**
 * @author zestyman
 *
 */
public class ISAServiceImpl implements ISAService {

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.ixc.isa.ISAService#getISAServiceList(java.lang.String)
	 */
	public String[] getISAServiceList(String isaServiceType) throws RemoteException {
		String[] serviceList = ISADataManager.getInstance().getISAServiceList(isaServiceType);
		return serviceList;
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.ixc.isa.ISAService#showSubscriptionPopup(java.lang.String, java.lang.String)
	 */
	public void showSubscriptionPopup(String serviceId, String entryPoint) throws RemoteException {
		ISAController.getInstance().showSubscriptionPopup(serviceId, entryPoint);
	}
	
	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.ixc.isa.ISAService#addISAServiceDataListener(com.videotron.tvi.illico.ixc.isa.ISAServiceDataListener, java.lang.String)
	 */
	public void addISAServiceDataListener(ISAServiceDataListener l, String appName) throws RemoteException {
		ISADataManager.getInstance().addISAServiceDataListener(l, appName);;
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.ixc.isa.ISAService#removeISAServiceDataListener(java.lang.String)
	 */
	public void removeISAServiceDataListener(String appName) throws RemoteException {
		ISADataManager.getInstance().removeISAServiceDataListener(appName);

	}

	public String[] getExcludedChannels() throws RemoteException {
		return ISADataManager.getInstance().getExcludedChannels();
	}

	public int[] getExcludedChannelTypes() throws RemoteException {
		return ISADataManager.getInstance().getExcludedChannelTypes();
	}

	public void hideSubscriptionPopup() throws RemoteException {
		ISAController.getInstance().hideAll();
	}

}

package com.videotron.tvi.illico.help.comp;

import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.controller.HelpController;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.data.DataManager;
import com.videotron.tvi.illico.help.data.Topic;
import com.videotron.tvi.illico.help.gui.MenuTreeRenderer;
import com.videotron.tvi.illico.help.ui.BaseUI;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
/**
 * This class represents data structure.
 * @author Jinjoo Ok
 *
 */
public class MenuTree extends UIComponent implements TVTimerWentOffListener {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = -7935609147178752540L;
    /** Maximum number of list. */
    public static final int MAX_LIST_NUM = 11;
    /** Index of beginning of hybrid scroll. */
    public static final int HYBRID_FOCUS = MAX_LIST_NUM / 2;
    /** Indicates animation is started. */
    public static final int ANIMATION_STARTED = 1;
    /** Indicates animation is ended. */
    public static final int ANIMATION_ENDED = 0;
    /** TVTimerSpec. */
    private TVTimerSpec timerSpec = new TVTimerSpec();
    /** Tv timer. */
    private TVTimer timer = TVTimer.getTimer();
    /** Effect. */
    protected MovingEffect leftOut;
    /** Effect. */
    protected MovingEffect rightIn;
    /** Effect. */
    protected MovingEffect rightOut;
    /** Effect. */
    protected MovingEffect leftIn;
    /** Effect. */
    protected ClickingEffect clickEffect;
    /** Effect. */
    protected AlphaEffect fadeInEffect;
    /** Category detail image. */
    protected Image detailImg;
    /** Indicates try count of getting image from server. */
    protected int getImgTryCnt;
    /** Count for showing default no image. */
    private int noImg;
    /** Top category.*/
    protected static Category root;
    /** Current category. */
    protected static Category current;
    /** true if it is started by Back key. */
    private boolean back;
    /** {@link BastUI}.*/
    protected BaseUI categoryUI;
    /** for animation effect. */
    protected int step;
    /** DataCenter Key - detail image. */
    private static final String DETAIL_IMG = "DETAIL_IMG";
    /** The Parameter. Because of animation effect, it has to be reserved.*/
    private Object param;
    /** Default constructor. */
    public MenuTree() {

    }
    /**
     * Constructor.
     * @param ui BastUI
     **/
    public MenuTree(final BaseUI ui) {
        setRenderer(new MenuTreeRenderer());
        categoryUI = ui;
        clickEffect = new ClickingEffect(this, 5);
        fadeInEffect = new AlphaEffect(this, 15, AlphaEffect.FADE_IN);
        leftOut = new MenuTreeEffect(this, 10, new Point(0, 0), new Point(-12, 0), MovingEffect.FADE_OUT,
                MovingEffect.GRAVITY);
        rightIn = new MenuTreeEffect(this, 10, new Point(12, 0), new Point(0, 0), MovingEffect.FADE_IN,
                MovingEffect.ANTI_GRAVITY);
        rightOut = new MenuTreeEffect(this, 10, new Point(0, 0), new Point(12, 0), MovingEffect.FADE_OUT,
                MovingEffect.GRAVITY);
        leftIn = new MenuTreeEffect(this, 10, new Point(-12, 0), new Point(0, 0), MovingEffect.FADE_IN,
                MovingEffect.ANTI_GRAVITY);
     }
    /**
     * Start menu tree.
     * @param bck whether if starts with 'Back' key.
     * @param obj parameter.
     **/
    public void start(boolean bck, Object obj) {
        setVisible(false);
        prepare();
        init();
        this.back = bck;
        param = obj;
        if (!HelpController.firstAniStart) {
            HelpController.firstAniStart = true;
        } else {
            focus = current.getParent().indexOf(current);
            current = current.getParent();
            step = ANIMATION_STARTED;
            leftIn.start();
        }
        setVisible(true);
    }
    /**
     * Start the animation.
     */
    public void startAnimation() {
        if (root == null) {
            root = DataManager.getInstance().getRoot();
        }
        if (!back) {
            if (param != null) {
                current = (Category) ((Object[]) param)[0];
                Category cat = (Category) ((Object[]) param)[1];
                if (cat != null) {
                    focus = current.indexOf(cat);
                }
            } else {
                if (current == null || current.getParent() == root) {
                    current = root;
                }
            }
        } else {
            focus = current.getParent().indexOf(current);
            current = current.getParent();
        }
        setImage();
        step = ANIMATION_STARTED;
        fadeInEffect.start();
    }

    /**
     * Initialize.
     */
    protected void init() {
        focus = 0;
        getImgTryCnt = 0;
        noImg = -1;
        step = ANIMATION_ENDED;
    }
    /**
     * 1111.
     * @return step
     */
    public int getStep() {
        return step;
    }
    /**
     * Called when the animation effect is ended.
     * @param effect {@link Effect}
     */
    public void animationEnded(Effect effect) {
        if (root == null) {
            root = DataManager.getInstance().getRoot();
        }
        if (effect == fadeInEffect) {
            step = ANIMATION_ENDED;
            if (!back) {
                if (current == null || current.getParent() == root) {
                    current = root;
                }
                if (root == null || current == null) {
                    HelpController.getInstance().showErrorPopup(Rs.ERROR_CODE_HLP503);
                }
            } else {
                focus = current.getParent().indexOf(current);
                current = current.getParent();
            }
            setImage();
        } else if (effect.equals(leftOut)) {
            Category nextCat = current.getItemAt(focus);
            if (checkFinalDepth(nextCat) == 0) {
                current = nextCat;
                init();
                setVisible(false);
                HelpController.getInstance().goToUI(HelpController.SCENE_ITEM, false, null);
            } else {
                current = current.getItemAt(focus);
                init();
                rightIn.start();
            }
        } else if (effect.equals(rightOut)) {
            if (current != root) {
                focus = current.getParent().indexOf(current);
                current = current.getParent();
                leftIn.start();
            }
        } else if (effect.equals(leftIn) || effect.equals(rightIn)) {
            step = ANIMATION_ENDED;
            categoryUI.init();
            if (current == root && !HelpController.getInstance().isContextualHelp()) {
                categoryUI.getFooter().setVisible(false);
            } else {
                categoryUI.getFooter().setVisible(true);
            }
            setImage();
        }
    }
    /**
     * Gets category scene.
     * @return {@link CategoryUI}
     */
    public BaseUI getCategoryUI() {
        return categoryUI;
    }

    public void prepare() {
        renderer.prepare(this);
    }
    /**
     * Starts the timer.
     */
    protected void startTimer() {
       synchronized (timerSpec) {
            stopTimer();
            try {
                timerSpec.setRegular(false);
                timerSpec.setAbsolute(false);
                timerSpec.setTime(1000L);
                timerSpec.addTVTimerWentOffListener(this);
                timer.scheduleTimerSpec(timerSpec);
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }
    }

    /** Stop timer. */
    protected void stopTimer() {
        if (Log.DEBUG_ON) {
            Log.printDebug("stopTimer");
        }
        synchronized (timerSpec) {
            timerSpec.removeTVTimerWentOffListener(this);
            timer.deschedule(timerSpec);
        }
    }

    /**
     * Gets detail image.
     * @return image
     */
    public Image getDetailImg() {
        return detailImg;
    }
    /**
     * Stop.
     */
    public void stop() {
        setVisible(false);
        stopTimer();
        if (detailImg != null) {
                FrameworkMain.getInstance().getImagePool().remove(DETAIL_IMG);
        }
        detailImg = null;
        if (Log.DEBUG_ON) {
            Log.printDebug(" Tree stop " + isVisible());
        }
    }
    /**
     * Release all images.
     */
    public void releaseImage() {
        DataCenter.getInstance().removeImage("05_focus.png");
        DataCenter.getInstance().removeImage("05_focus_dim.png");
        DataCenter.getInstance().removeImage("08_main_icon.png");
        DataCenter.getInstance().removeImage("02_ars_t.png");
        DataCenter.getInstance().removeImage("02_ars_b.png");
        DataCenter.getInstance().removeImage("00_bullet01.png");
        DataCenter.getInstance().removeImage("00_menufocus_dim.png");
//        DataCenter.getInstance().removeImage("00_bottom_shadow.png");
//        DataCenter.getInstance().removeImage("00_top_shadow.png");
    }
    /**
     * Destroys all resource.
     */
    public void destroy() {
        current = null;
        root = null;
        releaseImage();
    }
    /**
     * Checks if the category is final depth or not.
     * @param cat category
     * @return return true if current category is final depth.
     */
    public static int checkFinalDepth(Category cat) {
        if (cat == null || cat.size() == 0) {
            return -1;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("checkFinalDepth " + cat.getName() + " " + cat.size());
        }
        for (int i = 0, len = cat.size(); i < len; i++) {
            if (cat.getItemAt(i).hasChild()) {
                return 1;
            }
        }
        return 0;
    }
    /**
     * Handles when 'OK' key is pressed.
     */
    private boolean keyOk() {
        if (current != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug(current.hasChild() + " checkFinalDepth " + current.getName() + " "
                        + current.size());
            }
            if (current.hasChild()) {
                int startIdx = 0;
                int size = current.size();
                if (focus >= HYBRID_FOCUS && size > MAX_LIST_NUM) {
                    startIdx = focus - HYBRID_FOCUS;
                    if (focus + HYBRID_FOCUS >= size - 1) {
                        startIdx = size - MAX_LIST_NUM;
                    }
                }
                Rectangle rec = renderer.getPreferredBounds(this);
                clickEffect.start(49 - rec.x, 102 + (focus - startIdx) * 34 - rec.y, 305, 43);
                if (!current.getItemAt(focus).hasChild()) {
                    return false;
                }
                stopTimer();
                step = ANIMATION_STARTED;
                leftOut.start();
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug(current.hasChild() + "has no child , return");
                }
                return false;
            }
        }
        return true;
    }
    /**
     * Gets the currently selected item's topic.
     * @return {@link Topic}
     */
    public Topic getTopic() {
        if (current == null) {
            return null;
        }
        return current.getItemAt(focus).getTopic();
    }
    /**
     * Handles key event.
     * @param code key code
     * @return  if true is returned then key event is not passed to other application.
     */
    public boolean handleKey(int code) {
        switch (code) {
        case Rs.KEY_OK:
            return keyOk();
        case KeyCodes.LAST:
            stopTimer();
            if (HelpController.getInstance().getContextualCategory() == current
                    && HelpController.getInstance().isContextualHelp()) {
               boolean result = HelpController.getInstance().startUnboundApplication();
               if (result) {
                   return true;
               }
            }

            if (current == null || current == root) {
//                HelpController.getInstance().exit();
            } else {
                step = ANIMATION_STARTED;
                rightOut.start();
            }
            break;
        case Rs.KEY_DOWN:
            if (current != null && focus < current.size() - 1) {
                focus++;
                flushImage();
                startTimer();
                repaint();
            }
            break;
        case Rs.KEY_UP:
            if (focus > 0) {
                focus--;
                flushImage();
                startTimer();
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            break;
        case Rs.KEY_LEFT:
            break;
        case OCRcEvent.VK_EXIT:
            HelpController.getInstance().exit();
            break;
        default:
        }
        return true;
    }
    /**
     * Gets current category.
     * @return Category.
     */
    public Category getCurrent() {
        return current;
    }

    /**
     * Gets root category.
     * @return root
     */
    public Category getRoot() {
        return root;
    }

    private void flushImage() {
        noImg = -1;
        if (detailImg != null) {
            FrameworkMain.getInstance().getImagePool().remove(DETAIL_IMG);
            detailImg = null;
        }
    }
    /**
     * Sets teaser image from Web.
     */
    protected void setImage() {
        if (Log.DEBUG_ON) {
            Log.printDebug("setImage start!!");
        }
        if (current == null || current.size() == 0) {
            return;
        }
        new Thread() {
            public void run() {
                StringBuffer imgName = new StringBuffer(DataCenter.getInstance().getString(Rs.DCKEY_IMG_PATH));
                imgName.append(current.getItemAt(focus).getTopic().getImageName());
                HelpController.getInstance().showLoadingAnimation();
                try {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("setImage img url: " + imgName.toString());
                    }
                    byte[] imgByte = URLRequestor.getBytes(imgName.toString(), null);
                    if (imgByte == null || imgByte.length == 0) {
                        noImg = 0;
                        HelpController.getInstance().showErrorPopup(Rs.ERROR_CODE_HLP501);
                    } else {
                        if (isVisible()) {
                            detailImg = FrameworkMain.getInstance().getImagePool().createImage(imgByte,
                                        DETAIL_IMG);
                        }
                        noImg = 1;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("detailImg end!!");
                        }
                         repaint();
                    }
                } catch (Exception e) {
                    Log.print(e);
                    Log.printDebug("timerWentOff IOException " + getImgTryCnt);
                    if (getImgTryCnt > 0) {
                        noImg = 0;
                        HelpController.getInstance().showErrorPopup(Rs.ERROR_CODE_HLP502);
                    } else {
                        getImgTryCnt++;
                        setImage();
                        return;
                    }
                }
                HelpController.getInstance().hideLoadingAnimation();
                getImgTryCnt = 0;
            }
        } .start();
    }
    /**
     * If {@link} noImg is 0, displays default image.
     * @return  If noImg is 0, return true
     */
    public boolean isNoImage() {
        if (noImg == 0) {
            return true;
        }
        return false;
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
         setImage();
    }

}

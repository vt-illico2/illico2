package com.videotron.tvi.illico.help;

import org.havi.ui.event.HRcEvent;

/**
 * Resource class.
 * @author Jinjoo Ok
 *
 */
public final class Rs {
    /**
     * Constructor.
     */
    private Rs() {

    }
    /** Data Center Key - inband data path. */
    public static final String DCKEY_INBAND = "INBAND";
    /** Application name. */
    public static final String APP_NAME = "Help";
    /** Application version. */
    public static final String APP_VERSION = "(R7.4).1.1";//"1.0.3.0";
    /** Image download url. */
    public static final String DCKEY_IMG_PATH = "IMG_PATH";
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** error code - Cannot retrieve image from the MS HTTP Server.*/
    public static final String ERROR_CODE_HLP501 = "HLP501";
    /** error code - Cannot contact MS HTTP Server.*/
    public static final String ERROR_CODE_HLP502 = "HLP502";
    /** error code - Cannot get the list of topics from the TS Broadcaster.*/
    public static final String ERROR_CODE_HLP503 = "HLP503";
    /** error code - Cannot parse the list of topics received from the TS Broadcaster.*/
    public static final String ERROR_CODE_HLP504 = "HLP504";
    /** error code - Cannot get the Loading Animation.*/
    public static final String ERROR_CODE_HLP505 = "HLP505";
}

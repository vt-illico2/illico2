package com.videotron.tvi.illico.help.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.comp.MenuTree;
import com.videotron.tvi.illico.help.controller.HelpController;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.gui.MenuTreeRenderer;
import com.videotron.tvi.illico.util.TextUtil;
/**
 * This class represents a final depth having no children category.
 * @author Jinjoo Ok
 *
 */
public class ItemTree extends MenuTree {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = 8472552073043397376L;
    /** Paramenter. */
    private Object param;
    /**
     * Constructor.
     * @param ui {@link BaseUI}
     */
    public ItemTree(final BaseUI ui) {
        setRenderer(new ItemRenderer());
        categoryUI = ui;
        fadeInEffect = new AlphaEffect(this, 15, AlphaEffect.FADE_IN);
    }

    public void startAnimation() {
        if (!HelpController.firstAniStart) {
            HelpController.firstAniStart = true;
            if (param != null) {
                current = (Category) ((Object[]) param)[0];
                Category cat = (Category) ((Object[]) param)[1];
                if (cat != null) {
                    focus = current.indexOf(cat);
                }
            }
            step = ANIMATION_STARTED;
            fadeInEffect.start();
        } else {
            setVisible(true);
            setImage();
            step = ANIMATION_ENDED;
            repaint();
        }
    }
    /**
     * Called when the animation is ended.
     * @param effect {@link Effect}
     */
    public void animationEnded(Effect effect) {
        if (effect == fadeInEffect) {
            step = ANIMATION_ENDED;
            if (current == null) {
                HelpController.getInstance().showErrorPopup(Rs.ERROR_CODE_HLP503);
            }
            setImage();
        }
    }

    public void start(boolean back, Object obj) {
        setVisible(false);
        prepare();
        init();
        param = obj;
        setVisible(true);
    }

    /**
     * Releases images.
     */
    public void releaseImage() {
        super.releaseImage();
//        DataCenter.getInstance().removeImage("10_list_sh_t.png");
//        DataCenter.getInstance().removeImage("10_list_sh_b.png");
        DataCenter.getInstance().removeImage("10_focus_dim.png");
        DataCenter.getInstance().removeImage("10_focus.png");
    }
    /**
     * Renderer for ItemTree.
     */
    class ItemRenderer extends MenuTreeRenderer {
        private Rectangle rec = new Rectangle(60, 135, 960 - 60, 295);
        /** list shadow image. */
//        private Image iListShaT;
        /** list shadow image. */
//        private Image iListShaB;

        public Rectangle getPreferredBounds(UIComponent c) {
            return rec;
        }

        public void prepare(UIComponent c) {
            super.prepare(c);
//            iListShaT = DataCenter.getInstance().getImage("10_list_sh_t.png");
//            iListShaB = DataCenter.getInstance().getImage("10_list_sh_b.png");
            iListFocDim = DataCenter.getInstance().getImage("10_focus_dim.png");
            iListFoc = DataCenter.getInstance().getImage("10_focus.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        protected void paint(Graphics g, UIComponent c) {
            g.translate(-rec.x, -rec.y);
            Category category = categoryUI.getCurrent();
            if (category != null) {
                g.drawString(category.getName(), 61, 98);
            }
            if (category != null && category.size() > 0) {
                int startIdx = 0;
                // int focus = ((CategoryUI) c).getFocus();
                int size = category.size();
                if (focus >= HYBRID_FOCUS && size > MAX_LIST_NUM) {
                    startIdx = focus - HYBRID_FOCUS;
                    if (focus + HYBRID_FOCUS >= size - 1) {
                        startIdx = size - MAX_LIST_NUM;
                    }
                }
                g.setFont(f18);
                g.setColor(c230);
                Category cat = null;
                for (int i = 0; i < size; i++) {
                    if (i > MAX_LIST_NUM - 1) {
                        break;
                    }
                    cat = category.getItemAt(startIdx + i);
                    if (startIdx + i != focus) {
                        g.drawString(cat.getName(), 78, 156 + i * 33);
                    }
                    if (i < MAX_LIST_NUM - 1) {
                        g.drawImage(iLine, 66, 167 + i * 33, c);
                    }
                }

                cat = category.getItemAt(focus);
                if (size > MAX_LIST_NUM) {
                    if (focus > HYBRID_FOCUS) {
//                        g.drawImage(iListShaT, 60, 135, c);
                        g.drawImage(iArrowTop, 212, 129, c);
                    }
                    if (focus + HYBRID_FOCUS < size - 1) {
//                        g.drawImage(iListShaB, 60, 362, c);
                        g.drawImage(iArrowBottom, 212, 421, c);
                    }
                }
                if (step == ANIMATION_ENDED) {
                    if (categoryUI.isFocusOnList()) {
                        g.drawImage(iListFoc, 59, 134 + (focus - startIdx) * 34, c);
                    } else {
                        g.drawImage(iListFocDim, 59, 134 + (focus - startIdx) * 34, c);
                    }
                }
                if (step == ANIMATION_ENDED) {
                    g.setColor(c3);
                } else {
                    g.setColor(c230);
                }
                g.drawString(cat.getName(), 77, 156 + (focus - startIdx) * 34);
            }
            g.translate(rec.x, rec.y);
        }
    }
}

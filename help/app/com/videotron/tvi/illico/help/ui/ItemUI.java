package com.videotron.tvi.illico.help.ui;

import java.awt.Image;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.comp.MenuTree;
import com.videotron.tvi.illico.help.controller.HelpController;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.data.Topic;
import com.videotron.tvi.illico.help.gui.ItemUIRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * ItemUI scene is displays menu that has no more sub menu.
 * @author Jinjoo Ok
 */
public class ItemUI extends BaseUI {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = 2904379546488341953L;
    /** Effect. */
    private AlphaEffect fadeIn;
    /** {@link ItemTree}. */
    private ItemTree itemTree;

    /**
     * Constructor.
     */
    public ItemUI() {
        setRenderer(new ItemUIRenderer());
        itemTree = new ItemTree(this);
        fadeIn = new AlphaEffect(this, 8, AlphaEffect.FADE_IN);
        add(itemTree, 0);
    }

    /**
     * Starts this scene.
     * @param back whether if started by last key.
     * @param obj parameter
     */
    public void start(boolean back, Object obj) {
        addClockListener();
        setVisible(false);
        prepare();
        init();
        itemTree.start(back, obj);
        if (!HelpController.firstUIStart) {
            HelpController.firstUIStart = true;
            fadeIn.start();
            footer.setVisible(true);
        } else {
            footer.setVisible(true);
            itemTree.startAnimation();
            setVisible(true);
        }
    }
    /**
     * Called when the animation effect is ended.
     * @param effect {@link Effect}
     */
    public void animationEnded(Effect effect) {
        if (effect == fadeIn) {
            setVisible(true);
            itemTree.startAnimation();
        }
        requestFocus();
    }

    public void destory() {
        ((ItemUIRenderer) renderer).releaseImage();
        itemTree.destroy();
        itemTree = null;
    }
    /**
     * Gets current category.
     * @return {@link Category}.
     */
    public Category getCurrent() {
        return itemTree.getCurrent();
    }
    /**
     * Gets current topic.
     * @return {@link Topic}.
     */
    public Topic getTopic() {
        return itemTree.getTopic();
    }

    /**
     * Gets detail image.
     * @return image
     */
    public Image getDetailImg() {
        return itemTree.getDetailImg();
    }

    /**
     * Returns {@linkplain MenuTree#isNoImage()}.
     * @return Returns {@linkplain MenuTree#isNoImage()}.
     */
    public boolean isNoImage() {
        return itemTree.isNoImage();
    }

    public boolean handleKey(int code) {
        if (topicPop != null) {
            topicPop.handleKey(code);
            return true;
        }
        switch (code) {
        case KeyCodes.LAST:
            footer.clickAnimation(-1);
            if (HelpController.getInstance().getContextualCategory() == itemTree.getCurrent()
                    && HelpController.getInstance().isContextualHelp()) {
               boolean result = HelpController.getInstance().startUnboundApplication();
               if (result) {
                   return true;
               }
            }
            HelpController.getInstance().goToUI(HelpController.SCENE_CATEGORY, true,
                    new Object[] {itemTree.getCurrent(), null});
            break;
        case Rs.KEY_OK:
            Topic topic = itemTree.getTopic();
            if (!focusOnList) {
                int type = topic.getType();
                if (type != Topic.IMAGE_ONLY) {
                    clickEffect.start(563, 370, 155, 33);
                }
                if (type == Topic.IMAGE_TEXT) {
                    topicPop = new TopicPop(topic);
                    topicPop.start(this);
                } else if (type == Topic.IMAGE_VIDEO) {
                    HelpController.getInstance().showVideo(topic.getSspId(), topic.getDuration());
                }
            } else {
                if (topic != null && topic.getType() != Topic.IMAGE_ONLY) {
                    focusOnList = false;
                    repaint();
                }
            }
            break;
        case Rs.KEY_DOWN:
            if (!focusOnList) {
                return true;
            }
            itemTree.handleKey(code);
            break;
        case Rs.KEY_UP:
            if (!focusOnList) {
                return true;
            }
            itemTree.handleKey(code);
            break;
        case Rs.KEY_RIGHT:
            if (focusOnList) {
                Topic toc = itemTree.getTopic();
                if (toc != null && toc.getType() != Topic.IMAGE_ONLY) {
                    focusOnList = false;
                    repaint();
                }
            }
            break;
        case Rs.KEY_LEFT:
            if (!focusOnList) {
                focusOnList = true;
                repaint();
            }
            break;
        case OCRcEvent.VK_EXIT:
            HelpController.getInstance().exit();
            break;
        default:
        }

        return true;
    }

    /**
     * Stops the scene.
     */
    public void stop() {
        removeClockListener();
        setVisible(false);
        itemTree.stop();
        if (Log.DEBUG_ON) {
            Log.printDebug(" Item stop ");
        }
    }
}

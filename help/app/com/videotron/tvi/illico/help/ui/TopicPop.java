package com.videotron.tvi.illico.help.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.controller.HelpController;
import com.videotron.tvi.illico.help.data.Topic;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * class for Image-Text topic.
 * @author jinjoo ok
 */
public class TopicPop extends UIComponent {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = -1565938749937034674L;
    /** Topic class. */
    private Topic topic;
    /** PopupListener. */
    private PopupListener listener;
    /** List of topic images. */
    private Image[] topicImg;
    /** Count for displaying of default image.
     * -1: displayed nothing, 0: displayed default image, 1:displayed topic image.
     */
    private int noImg;
    /** Effect. */
    private ClickingEffect clickEffect;

    /**
     * Constructor.
     * @param tpc Topic
     */
    public TopicPop(final Topic tpc) {
        setRenderer(new TopicPopRenderer());
        clickEffect = new ClickingEffect(this, 5);
        this.topic = tpc;
        topicImg = new Image[topic.getText().length];
        noImg = -1;
        focus = 0;
    }

    /**
     * Starts scene.
     * @param c UIComponent
     */
    public void start(UIComponent c) {
        setVisible(false);
        noImg = -1;
        listener = (BaseUI) c;
        c.add(this, 0);
        prepare();
        setVisible(true);
        setImage();
        c.repaint();
    }

    /**
     * Gets topic images from HTTP connection.
     */
    public void setImage() {
        final StringBuffer imgName = new StringBuffer();
        final String[] textImg = topic.getText();
        new Thread("GetTopicImg") {
            public void run() {
                try {
                    imgName.append(DataCenter.getInstance().getString(Rs.DCKEY_IMG_PATH));
                    imgName.append(textImg[focus]);
                    if (Log.INFO_ON) {
                        Log.printInfo("set topic img path:" + imgName.toString());
                    }
                    HelpController.getInstance().showLoadingAnimation();

                    byte[] imgByte = URLRequestor.getBytes(imgName.toString(), null);
                    if (imgByte == null || imgByte.length == 0) {
                        noImg = 0;
                        HelpController.getInstance().showErrorPopup(Rs.ERROR_CODE_HLP501);
                    } else {
                        if (isVisible()) {
                            topicImg[focus] = FrameworkMain.getInstance().getImagePool()
                                    .createImage(imgByte, imgName.toString());
                            noImg = 1;
                            repaint();
                        }
                    }
                } catch (Exception e) {
                    noImg = 0;
                    HelpController.getInstance().showErrorPopup(Rs.ERROR_CODE_HLP501);
                    Log.print(e);
                }
                HelpController.getInstance().hideLoadingAnimation();
            }
        } .start();
    }

    public boolean handleKey(int code) {
        Log.printDebug("Topic popup handleKey");
        switch (code) {
        case Rs.KEY_LEFT:
            if (focus > 0) {
                focus--;
                if (topicImg[focus] == null) {
                    setImage();
                }
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            if (topicImg != null && focus < topicImg.length - 1) {
                focus++;
                if (topicImg[focus] == null) {
                    setImage();
                }
                repaint();
            }
            break;
        case Rs.KEY_OK:
            if (topicImg != null && focus == topicImg.length - 1) {
                clickEffect.start(403, 398, 155, 33);
                stop();
            }
            break;
        case OCRcEvent.VK_EXIT:
            clickEffect.start(729, 465, 31, 17);
            stop();
            break;
        default:
        }
        return false;
    }

    /**
     * Stops this scene.
     */
    public void stop() {
        listener.popClosed(this, null);
        HelpController.getInstance().hideLoadingAnimation();
        this.setVisible(false);
        if (topicImg != null) {
            StringBuffer imgName = new StringBuffer();
            String[] textImg = topic.getText();
            for (int i = 0; i < topicImg.length; i++) {
                if (topicImg[i] != null) {
                    imgName.setLength(0);
                    imgName.append(DataCenter.getInstance().getString(Rs.DCKEY_IMG_PATH));
                    imgName.append(textImg[i]);
                    FrameworkMain.getInstance().getImagePool().remove(imgName.toString());
                    topicImg[i] = null;
                }
            }
        }
        topicImg = null;
        topic = null;
        ((TopicPopRenderer) renderer).releaseImage();
        Container con = getParent();
        if (con != null && con instanceof BaseUI) {
            ((BaseUI) con).remove(this);
            con.repaint();
        }
    }

    /**
     * Renderer.
     * @author Jinjoo Ok
     */
    class TopicPopRenderer extends Renderer {
        /** Color. */
        private Color cBg = new Color(15, 15, 15);
        /** Color. */
        private Color cGray = new Color(182, 182, 182);
        /** Color. */
        private Color cBox = new Color(249, 195, 0);
        /** Background image. */
        private Image iBg;
        /** Arrow Image. */
        private Image iArrR;
        /** Arrow Image. */
        private Image iArrL;
        /** Button Image. */
        private Image iButton;
        /** Exit icon Image. */
        private Image iExit;
        /** Default image. */
        private Image iDefault;
        /** Font of size 18. */
        private Font f18 = FontResource.BLENDER.getFont(18);
        /** Font of size 22. */
        private Font f22 = FontResource.BLENDER.getFont(22);

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        /**
         * Release images.
         */
        public void releaseImage() {
            DataCenter.getInstance().removeImage("10_dt_bg.png");
            DataCenter.getInstance().removeImage("02_ars_l.png");
            DataCenter.getInstance().removeImage("02_ars_r.png");
            DataCenter.getInstance().removeImage("05_focus.png");
            DataCenter.getInstance().removeImage("10_img_default_b.png");
        }

        public void prepare(UIComponent c) {
            iBg = DataCenter.getInstance().getImage("10_dt_bg.png");
            iDefault = DataCenter.getInstance().getImage("10_img_default_b.png");
            iArrL = DataCenter.getInstance().getImage("02_ars_l.png");
            iArrR = DataCenter.getInstance().getImage("02_ars_r.png");
            iButton = DataCenter.getInstance().getImage("05_focus.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
            Hashtable imgButtonTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
            iExit = (Image) imgButtonTable.get(PreferenceService.BTN_EXIT);
        }

        protected void paint(Graphics g, UIComponent c) {
            g.setColor(cBg);
            g.fillRect(0, 0, 960, 540);
            g.drawImage(iBg, 156, 70, c);
            // g.setColor(c35);
            // g.fillRect(160, 74, 641, 380);
            g.setFont(f18);
            if (topicImg != null) {
                if (topicImg.length > 0 && topicImg[focus] != null) {
                    g.drawImage(topicImg[focus], 160, 74, 641, 380, c);
                } else {
                    if (noImg == 0) {
                        g.drawImage(iDefault, 160, 74, c);
                        g.setFont(f22);
                        g.setColor(Color.black);
                        String str = DataCenter.getInstance().getString("Label.lblUnavailable");
                        GraphicUtil.drawStringCenter(g, str, 482, 367);
                        g.setColor(cGray);
                        GraphicUtil.drawStringCenter(g, str, 481, 366);
                    }
                }
                if (topicImg.length > 1) {
                    g.setColor(cBox);
                    g.fillRect(755, 437, 48, 17);
                }
                if (topicImg.length > 1) {
                    g.setFont(f18);
                    g.setColor(Color.black);
                    StringBuffer sb = new StringBuffer();
                    sb.append(focus + 1);
                    sb.append("/");
                    sb.append(topicImg.length);
                    GraphicUtil.drawStringCenter(g, sb.toString(), 780, 452);
                    if (focus != topicImg.length - 1) {
                        g.drawImage(iArrR, 804, 251, c);
                    }
                    if (focus != 0) {
                        g.drawImage(iArrL, 140, 251, c);
                    }
                }
                if (focus == topicImg.length - 1) {
                    g.drawImage(iButton, 403, 398, c);
                    g.setColor(Color.black);
                    GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblFinish"), 482, 419);
                }

                if (iExit != null) {
                    String close = DataCenter.getInstance().getString("Label.lblClose");
                    g.drawImage(iExit, 805 - g.getFontMetrics().stringWidth(close) - 33, 463, c);
                    g.setColor(Color.white);
                    g.setFont(f18);
                    GraphicUtil.drawStringRight(g, close, 805, 478);
                }
            }

        }
    }
}

package com.videotron.tvi.illico.help.ui;

import java.awt.Image;
import org.ocap.ui.event.OCRcEvent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.comp.MenuTree;
import com.videotron.tvi.illico.help.controller.HelpController;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.data.Topic;
import com.videotron.tvi.illico.help.gui.CategoryUIRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * MenuUI scene is displays categories have sub category.
 * @author Jinjoo Ok
 */
public class CategoryUI extends BaseUI {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = 680900204397731148L;
    /** Effect. */
    private AlphaEffect fadeIn;
    // /** Footer. */
    // protected Footer footer = new Footer();
    // protected AlphaEffect fadeIn;
    /** {@link MenuTree}.*/
    private MenuTree menuTree;

    /** Constructor. */
    public CategoryUI() {
        setRenderer(new CategoryUIRenderer());
        fadeIn = new AlphaEffect(this, 8, AlphaEffect.FADE_IN);
        menuTree = new MenuTree(this);
        add(menuTree, 0);
    }
    /**
     * Called when the Animation is ended.
     * @param effect {@link Effect}
     */
    public void animationEnded(Effect effect) {
        if (effect == fadeIn) {
            setVisible(true);
            menuTree.startAnimation();
        }
        requestFocus();
    }
    /**
     * Returns {@linkplain MenuTree#isNoImage()}.
     * @return Returns {@linkplain MenuTree#isNoImage()}.
     */
    public boolean isNoImage() {
        return menuTree.isNoImage();
    }

    /**
     * Starts the scene.
     * @param back whether if started by last key.
     * @param obj parameter.
     */
    public void start(boolean back, Object obj) {
        addClockListener();
        setVisible(false);
        prepare();
        init();
        if (back) {
            setVisible(true);
        }
        menuTree.start(back, obj);
        if (!HelpController.firstUIStart) {
            HelpController.firstUIStart = true;
            if (!HelpController.getInstance().isContextualHelp()) {
                footer.setVisible(false);
            } else {
                footer.setVisible(true);
            }
            fadeIn.start();
        }
    }

    /**
     * Gets root category.
     * @return root
     */
    public Category getRoot() {
        return menuTree.getRoot();
    }

    // /**
    // * Initialize.
    // */
    // protected void init() {
    // focusInList = true;
    // }

    /**
     * Stops the scene.
     */
    public void stop() {
        removeClockListener();
        setVisible(false);
        menuTree.stop();
        if (Log.DEBUG_ON) {
            Log.printDebug(" Category stop " + isVisible());
        }
    }

    public void destory() {
        if (Log.DEBUG_ON) {
            Log.printDebug(" Category destory ");
        }
        menuTree.destroy();
        ((CategoryUIRenderer) renderer).releaseImage();
        menuTree = null;
    }

    // private void keyOk() {
    // if (current != null) {
    // if (focusInList) {
    // if (Log.DEBUG_ON) {
    // Log.printDebug(current.hasChild() + " checkFinalDepth " + current.getName() + " "
    // + current.size());
    // }
    // if (current.hasChild()) {
    // if (!current.getItemAt(focus).hasChild()) {
    // return;
    // }
    // int startIdx = 0;
    // int size = current.size();
    // if (focus >= MenuUIRenderer.HYBRID_FOCUS && size > MenuUIRenderer.MAX_LIST_NUM) {
    // startIdx = focus - MenuUIRenderer.HYBRID_FOCUS;
    // if (focus + MenuUIRenderer.HYBRID_FOCUS >= size - 1) {
    // startIdx = size - MenuUIRenderer.MAX_LIST_NUM;
    // }
    // }
    // clickEffect.start(49, 102 + (focus - startIdx) * 34, 305, 50);
    // leftOut.start();
    // // current = current.getItemAt(focus);
    // // init();
    // // if (checkFinalDepth()) {
    // // HelpController.getInstance().goToUI(HelpController.SCENE_ITEM, false, null);
    // // } else {
    // // setImage();
    // // this.requestFocus();
    // // }
    // } else {
    // if (Log.DEBUG_ON) {
    // Log.printDebug(current.hasChild() + "has no child , return");
    // }
    // return;
    // }
    //
    // } else {
    // Topic topic = current.getItemAt(focus).getTopic();
    // if (topic.getType() == Topic.IMAGE_TEXT) {
    // topicPop = new TopicPop(topic);
    // topicPop.start(this);
    // } else if (topic.getType() == Topic.IMAGE_VIDEO) {
    // HelpController.getInstance().showVideo(topic.getSspId(), topic.getDuration());
    // }
    // }
    // repaint();
    // }
    // }

    public boolean handleKey(int code) {
        if (topicPop != null) {
            topicPop.handleKey(code);
            return true;
        }
        switch (code) {
        case Rs.KEY_OK:
            if (focusOnList) {
                boolean used = menuTree.handleKey(code);
                if (!used) {
                    handleKey(Rs.KEY_RIGHT);
                }
            } else {
                Topic topic = menuTree.getTopic();
                int type = topic.getType();
                if (type != Topic.IMAGE_ONLY) {
                    clickEffect.start(563, 427, 155, 33);
                }
                if (type == Topic.IMAGE_TEXT) {
                    topicPop = new TopicPop(topic);
                    topicPop.start(this);
                } else if (type == Topic.IMAGE_VIDEO) {
                    HelpController.getInstance().showVideo(topic.getSspId(), topic.getDuration());
                }
            }
            break;
        case KeyCodes.LAST:
            if (footer.isVisible()) {
                footer.clickAnimation(-1);
            }
            menuTree.handleKey(code);
            break;
        case Rs.KEY_DOWN:
            if (!focusOnList) {
                return true;
            }
            menuTree.handleKey(code);
            break;
        case Rs.KEY_UP:
            if (!focusOnList) {
                return true;
            }
            menuTree.handleKey(code);
            break;
        case Rs.KEY_RIGHT:
            if (focusOnList) {
                Topic toc = menuTree.getTopic();
                if (toc != null && toc.getType() != Topic.IMAGE_ONLY) {
                    focusOnList = false;
                    repaint();
                }
            }
            break;
        case Rs.KEY_LEFT:
            if (!focusOnList) {
                focusOnList = true;
                repaint();
            }
            break;
        case OCRcEvent.VK_EXIT:
            clickEffect.start(185, 490, 31, 17);
            HelpController.getInstance().exit();
            break;
        default:
        }
        return true;
    }


    /**
     * Gets current category.
     * @return {@link Category}.
     */
    public Category getCurrent() {
        return menuTree.getCurrent();
    }
    /**
     * Gets current Topic.
     * @return {@link Topic}.
     */
    public Topic getTopic() {
        return menuTree.getTopic();
    }

    /**
     * Gets detail image.
     * @return image
     */
    public Image getDetailImg() {
        return menuTree.getDetailImg();
    }

}

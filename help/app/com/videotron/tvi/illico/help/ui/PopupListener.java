package com.videotron.tvi.illico.help.ui;

import com.videotron.tvi.illico.framework.UIComponent;
/**
 * PopupListener for popup's action.
 * @author Jinjoo Ok
 *
 */
public interface PopupListener {
    /**
     * Informs popup is closed.
     * @param src UIComponent
     * @param param parameters
     */
    void popClosed(UIComponent src, Object param);
}

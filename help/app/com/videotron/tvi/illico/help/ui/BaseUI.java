package com.videotron.tvi.illico.help.ui;

import java.awt.Image;
import java.util.Date;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;
/**
 * BaseUI.
 */
public abstract class BaseUI extends UIComponent implements PopupListener, ClockListener {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = -715534649164717432L;
    /** Footer. */
    protected Footer footer = new Footer();
    /** true, if focus on the list. */
    protected boolean focusOnList;
    /** Topic popup. */
    protected TopicPop topicPop;
    /** Whether if listener is added to Clock.*/
    private static boolean isAddedClockListener;
    /** Effect. */
    protected ClickingEffect clickEffect;

    /**Constructor.*/
    public BaseUI() {
        setBounds(Constants.SCREEN_BOUNDS);
        footer.setBounds(0, 488, 906, 25);
        add(footer);
        clickEffect = new ClickingEffect(this, 5);
    }
    /**
     * Prepares.
     */
    public void prepare() {
        footer.reset();
        Hashtable imgButtonTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        footer.addButton((Image) imgButtonTable.get(PreferenceService.BTN_BACK), "Label.lblBack");
        super.prepare();
    }
    /**
     * Gets Footer.
     * @return {@link Footer}
     */
    public Footer getFooter() {
        return footer;
    }

    /**
     * Starts the scene.
     * @param back whether if started by last key.
     * @param obj parameter.
     */
    public abstract void start(boolean back, Object obj);

    public abstract void destory();
    /**
     * Gets detail image of currently selected item's.
     * @return Image
     **/
    public abstract Image getDetailImg();

    /**
     * Gets current category.
     * @return Category.
     */
    public abstract Category getCurrent();

    /**
     * Initialize.
     */
    public void init() {
        focusOnList = true;
    }
    /**
     * if the focus is on the list, return true.
     * @return   true, if focus on the list.
     */
    public boolean isFocusOnList() {
        return focusOnList;
    }
    /**
     * Called when the Popup is closed.
     * @param src {@link UIComponent}
     * @param param parameter
     */
    public void popClosed(UIComponent src, Object param) {
        topicPop = null;
        this.requestFocus();
        repaint();
    }
    /**
     * Called when clock is updated per 1 sec.
     * @param date current date
     */
    public void clockUpdated(Date date) {
        repaint();
    }
    /**
     * Adds listener to the Clock.
     */
    public void addClockListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("addClockListener");
        }
        if (!isAddedClockListener) {
            isAddedClockListener = true;
            Clock.getInstance().addClockListener(this);
        }
    }
    /**
     * Removes listener from the Clock.
     */
    public void removeClockListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("removeClockListener");
        }
        if (isAddedClockListener) {
            isAddedClockListener = false;
            Clock.getInstance().removeClockListener(this);
        }
    }
}

package com.videotron.tvi.illico.help;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.help.communication.CommunicationController;
import com.videotron.tvi.illico.help.communication.PreferenceProxy;
import com.videotron.tvi.illico.help.controller.HelpController;

/**
 * Help main class.
 * @author Jinjoo Ok
 * @version $Revision: 1.2
 */
public class App implements Xlet, ApplicationConfig {
    /** XletContext.*/
    private XletContext context;
    /**
     * called when application destroyed.
     * @param ctx XletContext
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *                 state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean ctx) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();
    }
    /**
     * called by Monitor when STB booted.
     * @param ctx XletContext
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
    	context = ctx;
        FrameworkMain.getInstance().init(this);
    }
    /**
     * called when application paused.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
        HelpController.getInstance().stop();
        FrameworkMain.getInstance().getImagePool().clear();
    }
    /**
     * called by Monitor when STB booted.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        FrameworkMain.getInstance().start();
        HelpController.getInstance().start();
    }
    /**
     * Returns name of application. The name can be used as a unique key to find <code>AppProxy</code> from
     * <code>AppsDatabase</code>.
     *
     * @return name of application
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }
    /**
     * Returns version of application.
     *
     * @return version of application
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }
    /**
     * Returns the XletContext of application.
     *
     * @return XletContext
     */
    public XletContext getXletContext() {
        return context;
    }

    public void init() {
        CommunicationController.getInstance().init(context);
        PreferenceProxy.getInstance().init(context);
    }
}

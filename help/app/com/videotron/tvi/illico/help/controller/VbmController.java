package com.videotron.tvi.illico.help.controller;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Vector;

import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;

public class VbmController implements LogCommandTriggerListener {

    protected static VbmController instance = new VbmController();
    /** The Constant MID_APP_START. */
    private static final long MID_START_APPLICATION = 1016000001L;
    /** The Constant MID_APP_EXIT. */
    private static final long MID_END_APPLICATION = 1016000002L;
    /** The Constant MID_PARENT_APP. */
    private static final long MID_PARENT_APPLICATION = 1016000003L;
    /** The session id. */
    private String sessionId;

    public static VbmController getInstance() {
        return instance;
    }

    public static boolean ENABLED = false;

    protected VbmService vbmService;
    protected HashSet idSet = new HashSet();
    protected Vector vbmLogBuffer;

    private VbmController() {
    }

    public void init(VbmService service) {
        if (Log.DEBUG_ON) {
            Log.printInfo("VbmController.init");
        }
        vbmService = service;
        long[] ids = null;
        try {
            ids = vbmService.checkLogCommand(Rs.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        try {
            vbmService.addLogCommandChangeListener(Rs.APP_NAME, this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }

        if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmController: ids[" + i + "] = " + ids[i]);
                }
                idSet.add(new Long(ids[i]));
            }
        }

        vbmLogBuffer = (Vector) SharedMemory.getInstance().get(VbmService.VBM_SM_INTF);
        checkEmpty();
        if (vbmLogBuffer == null) {
            Log.printDebug("VbmController: found vbmLogBuffer");
        } else {
            Log.printDebug("VbmController: not found vbmLogBuffer.");
        }
    }

    public void dispose() {
        try {
            vbmService.removeLogCommandChangeListener(Rs.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

    protected void checkEmpty() {
        ENABLED = vbmLogBuffer != null && !idSet.isEmpty();
    }

    public void writeStartApplication() {
        write(MID_START_APPLICATION, sessionId);
    }

    public void writeEndApplication() {
        write(MID_END_APPLICATION, sessionId);
        sessionId = null;
    }

    public void writeParentApplication(String app) {
        sessionId = Long.toString(System.currentTimeMillis());
        write(MID_PARENT_APPLICATION, app);
    }

    private void write(long id, String value) {
        write(id, new String[]{value});
    }

    public boolean write(long id, String[] value) {
        if (!ENABLED) {
            return false;
        }
        if (!idSet.contains(new Long(id))) {
            return false;
        }
        String str = Long.toString(id);
        if (value != null) {
            StringBuffer buffer = new StringBuffer(100);
            buffer.append(str);
            buffer.append(VbmService.SEPARATOR);
            buffer.append(-1);
            for (int i = 0; i < value.length; i++) {
                buffer.append(VbmService.SEPARATOR);
                buffer.append(value[i]);
            }
            str = buffer.toString();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("VbmController.write: " + str);
        }
        vbmLogBuffer.addElement(str);
        return true;
    }

    /**
     * event invoked when logLevel data updated. it is only applied to action measurements
     * @param Measurement to notify measurement an frequencies
     * @param command - true to start logging, false to stop logging
     * @throws RemoteException - occur during the execution of a remote method call
     */
    public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
        Log.printInfo("VbmController.logCommandChanged: " + measurementId + ", " + command);
        if (command) {
            idSet.add(new Long(measurementId));
        } else {
            idSet.remove(new Long(measurementId));
        }
        checkEmpty();
    }

    /**
     * event invoked when log of trigger measurement should be collected
     * @param measurementId trigger measurement
     * @throws RemoteException - occur during the execution of a remote method call
     */
    public void triggerLogGathering(long measurementId) {
        Log.printInfo("VbmController.triggerLogGathering: " + measurementId);
    }

}

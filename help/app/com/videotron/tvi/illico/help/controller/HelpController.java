package com.videotron.tvi.illico.help.controller;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.comp.MenuTree;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.data.DataManager;
import com.videotron.tvi.illico.help.ui.BaseUI;
import com.videotron.tvi.illico.help.ui.ItemUI;
import com.videotron.tvi.illico.help.ui.CategoryUI;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * HelpController controls main feature of Help Application. It controls transition among screens
 * @author Jinjoo Ok
 */
public final class HelpController implements KeyListener, ScreenSaverConfirmationListener {
    /** Class instance. */
    private static HelpController instance = new HelpController();
    /** Scenes. */
    private BaseUI[] menu = new BaseUI[2];
    /** Menu category. */
    public static final int SCENE_CATEGORY = 0;
    /** Menu category that does not have sub depth. */
    public static final int SCENE_ITEM = 1;
    /** Current scene code. */
    private int sceneCode = -1;
    /** point of Loading animation. */
    private static Point lodingPoint = new Point(480, 240);
    /** Whether if Help is started by D key, sets true. */
    private boolean isContextualHelp;
    /** the Category that linked by D key. */
    private Category contextualCategory;
    /** Application name that starts Help by D key. */
    private String contextualAppName;
    public static boolean firstUIStart;
    public static boolean firstAniStart;

    /** Constructor. */
    private HelpController() {
    }

    /**
     * Gets HelpController instance.
     * @return HelpController
     */
    public static HelpController getInstance() {
        return instance;
    }

    /**
     * Creates the scene.
     * @param uiCode scene code
     * @return MenuUI
     */
    private BaseUI createUI(int uiCode) {
        BaseUI ui = null;
        if (uiCode == SCENE_CATEGORY) {
            ui = new CategoryUI();
        } else if (uiCode == SCENE_ITEM) {
            ui = new ItemUI();
        }
        return ui;
    }

    /**
     * Starts application.
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("start()");
        }
        addScreenSaverListener();
        MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        String[] param = null;
        if (monitor != null) {
            try {
                param = monitor.getParameter(Rs.APP_NAME);
                if (Log.DEBUG_ON) {
                    Log.printDebug("getParameter() " + param.length);
                    if (param != null) {
                        for (int i = 0; i < param.length; i++) {
                            Log.printDebug("param[" + i + "] : " + param[i]);
                        }
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }

        firstUIStart = false;
        firstAniStart = false;
        if (param != null && param.length > 1) {
            isContextualHelp = true;
            contextualAppName = param[1];
            
            //R7.4 freelife VDTRMASTER-6169
            if (contextualAppName.equalsIgnoreCase("tv") || 
            		contextualAppName.equalsIgnoreCase("menu")) {
            	isContextualHelp = false;
            }
            
            
        } else {
            isContextualHelp = false;
            contextualAppName = null;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("contextualAppName= " + contextualAppName);
        }
        if (param != null && param.length > 3) {
            Category cat = DataManager.getInstance().findAppLink(param[2], param[3]);
            int finDepth = MenuTree.checkFinalDepth(cat);
            if (Log.DEBUG_ON) {
                Log.printDebug("finDepth " + finDepth);
            }
            if (cat != null && cat.getParent() != DataManager.getInstance().getRoot()) {
                if (finDepth == 0) {
                    contextualCategory = cat;
                    goToUI(SCENE_ITEM, false, new Object[]{cat, null});
                } else {
                    contextualCategory = cat.getParent();
                    goToUI(SCENE_ITEM, false, new Object[]{cat.getParent(), cat});
                }
            } else {
                if (Log.DEBUG_ON && cat != null) {
                    Log.printDebug("cat " + cat.getName());
                }
                if (finDepth == -1) {
                    contextualCategory = DataManager.getInstance().getRoot();
                    if (cat != null) {
                        goToUI(SCENE_CATEGORY, false, new Object[]{contextualCategory, cat});
                    } else {
                        goToUI(SCENE_CATEGORY, false, null);
                    }
                } else if (finDepth == 0) {
                    contextualCategory = cat;
                    goToUI(SCENE_CATEGORY, false, new Object[]{cat, null});
                } else {
                    contextualCategory = cat.getParent();
                    goToUI(SCENE_CATEGORY, false, new Object[]{cat.getParent(), cat});
                }
            }
        } else {
            contextualCategory = DataManager.getInstance().getRoot();
            goToUI(SCENE_CATEGORY, false, null);
        }
        if (contextualAppName != null) {
            VbmController.getInstance().writeParentApplication(contextualAppName);
        } else {
            VbmController.getInstance().writeParentApplication(TextUtil.EMPTY_STRING);
        }
        VbmController.getInstance().writeStartApplication();
    }

    /**
     * Moves scene.
     * @param code scene code
     * @param back whether if started by 'back' key
     * @param obj parameter
     */
    public void goToUI(int code, boolean back, Object obj) {
        if (code == -1) {
            return;
        }
        hideUI();
        sceneCode = code;
        loadingUI();
        showUI(back, obj);
    }

    /**
     * Shows the scene.
     * @param back whether if started by 'back' key
     * @param obj parameter
     */
    private void showUI(boolean back, Object obj) {
        if (sceneCode == -1) {
            return;
        }
        FrameworkMain.getInstance().addComponent(menu[sceneCode]);
        menu[sceneCode].start(back, obj);
        menu[sceneCode].addKeyListener(this);
        menu[sceneCode].requestFocus();
    }

    /**
     * Hides current scene.
     */
    private void hideUI() {
        if (sceneCode == -1) {
            return;
        }
        menu[sceneCode].stop();
        FrameworkMain.getInstance().removeComponent(menu[sceneCode]);
        menu[sceneCode].removeKeyListener(this);
    }

    /**
     * Loads scene.
     */
    private void loadingUI() {
        if (menu[sceneCode] == null) {
            menu[sceneCode] = createUI(sceneCode);
        }
    }

    /**
     * Stops scene.
     */
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("stop called");
        }
        hideLoadingAnimation();
        removeScreenSaverListener();
        if (sceneCode == -1) {
            return;
        }
        hideUI();
        menu[sceneCode].stop();
        for (int i = 0; i < menu.length; i++) {
            if (menu[i] != null) {
                menu[i].destory();
            }
            menu[i] = null;
        }
        sceneCode = -1;
        stopVideo();
        VbmController.getInstance().writeEndApplication();
    }

    /**
     * Adds ScreenSaverListener.
     */
    public void addScreenSaverListener() {
        MonitorService ms = ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME));
        try {
            if (ms != null) {
                ms.addScreenSaverConfirmListener(this, Rs.APP_NAME);
            } else {
                if (Log.WARNING_ON) {
                    Log.printWarning("Help can't get MonitorService. so Help can't add listener to Monitor");
                }
            }
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * Remove ScreenSaverConfirmListener.
     */
    private void removeScreenSaverListener() {
        MonitorService ms = ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME));
        if (ms != null) {
            try {
                ms.removeScreenSaverConfirmListener(this, Rs.APP_NAME);
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    /**
     * shows error Pop-up through ErrorMessageService.
     * @param code error code.
     */
    public void showErrorPopup(String code) {
        if (sceneCode == -1) {
            return;
        }
        ErrorMessageService errorService = (ErrorMessageService) DataCenter.getInstance().get(
                ErrorMessageService.IXC_NAME);
        if (errorService != null) {
            try {
                errorService.showErrorMessage(code, null);
            } catch (Exception e) {
                Log.print(e);
            }
        } else {
            if (Log.ERROR_ON) {
                Log.printError("can't get ErrorMessageService");
            }
        }
    }

    /**
     * Shows loading animation.
     */
    public void showLoadingAnimation() {
        if (Log.DEBUG_ON) {
            Log.printDebug("showLoadingAnimation");
        }
        try {

            LoadingAnimationService loading = (LoadingAnimationService) DataCenter.getInstance().get(
                    LoadingAnimationService.IXC_NAME);
            if (loading != null) {
                loading.showLoadingAnimation(lodingPoint);
            } else {
                showErrorPopup(Rs.ERROR_CODE_HLP505);
            }

        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("after call showLoadingAnimation");
        }
    }

    /**
     * Hides loading animation.
     */
    public void hideLoadingAnimation() {
        try {
            LoadingAnimationService loading = (LoadingAnimationService) DataCenter.getInstance().get(
                    LoadingAnimationService.IXC_NAME);
            if (loading != null) {
                loading.hideLoadingAnimation();
            } else {
                showErrorPopup(Rs.ERROR_CODE_HLP505);
            }
        } catch (Exception e) {
            showErrorPopup(Rs.ERROR_CODE_HLP505);
            Log.print(e);
        }
    }

    public boolean isContextualHelp() {
        return isContextualHelp;
    }

    public Category getContextualCategory() {
        return contextualCategory;
    }

    /**
     * Response Monitor's confirm.
     * @return if it returns true, ScreenSaver will be started.
     * @throws RemoteException remote exception
     */
    public boolean confirmScreenSaver() throws RemoteException {
        VODService vod = (VODService) DataCenter.getInstance().get(VODService.IXC_NAME);
        if (vod != null) {
            Log.printDebug("confirmScreenSaver VOD state " + vod.getVODState());
            if (vod.getVODState() == VODService.APP_WATCHING) {
                return false;
            }
        }
        Log.printDebug("confirmScreenSaver return true");
        return true;
    }

    /**
     * Exit this application.
     */
    public void exit() {
        MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (monitor != null) {
            try {
            	if ("Wizard".equals(contextualAppName) 
            			&& DataCenter.getInstance().getString(PreferenceNames.DISPLAY_MENU_AT_POWER_ON)
            				.equals(Definitions.OPTION_VALUE_YES)) {
            		monitor.startUnboundApplication("Menu", new String[] {MonitorService.REQUEST_APPLICATION_MENU});
            	} else {
            		monitor.exitToChannel();
            	}
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * @see KeyListener.keyPressed.
     * @param e KeyEvent.
     */
    public void keyPressed(KeyEvent e) {
        if (FrameworkMain.getInstance().getHScene().isShowing()) {
            int keyCode = e.getKeyCode();
            handleKeyEvent(keyCode);
        }
    }

    /**
     * Handles key event.
     * @param code key code
     */
    private void handleKeyEvent(int code) {
        if (sceneCode == -1) {
            return;
        }

        if (menu[sceneCode] != null) {
            menu[sceneCode].handleKey(code);
        }
    }

    /**
     * Requests video to VOD application.
     * @param sspId ssp ID
     * @param duration duration
     */
    public void showVideo(String sspId, int duration) {
        if (Log.DEBUG_ON) {
            Log.printDebug("showVideo called " + sspId);
        }
        VODService vod = (VODService) DataCenter.getInstance().get(VODService.IXC_NAME);
        if (vod != null) {
            try {
                vod.showPopupVideo(sspId, duration, "");
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Requests stop to VOD application.
     */
    public void stopVideo() {
        if (Log.DEBUG_ON) {
            Log.printDebug("stopVideo() called ");
        }
        VODService vod = (VODService) DataCenter.getInstance().get(VODService.IXC_NAME);
        if (vod != null) {
            try {
                vod.getVideoController().stop(null);
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    /**
     * Starts unbound application.
     * @return if it success return true.
     */
    public boolean startUnboundApplication() {
        if (Log.DEBUG_ON) {
            Log.printDebug("startUnboundApplication " + contextualAppName);
        }
        if (contextualAppName != null
                && (contextualAppName.equals("TV") || contextualAppName.equals("Wizard"))) {
            exit();
            return true;
        }
        MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (monitor != null) {
            try {
                if (contextualAppName == null) {
                    return false;
                }
                monitor.startUnboundApplication(contextualAppName,
                        new String[]{Rs.APP_NAME});
            } catch (Exception e) {
                Log.print(e);
            }
        }
        return true;
    }
}

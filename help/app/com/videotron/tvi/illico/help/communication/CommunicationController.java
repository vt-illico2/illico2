package com.videotron.tvi.illico.help.communication;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.controller.VbmController;
import com.videotron.tvi.illico.help.data.DataManager;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.IxcLookupWorker;
import com.videotron.tvi.illico.util.SharedMemory;
/**
 * CommunicationManager looks up communication interface provided by other applications and handles calling.
 * @author Jinjoo Ok
 */
public final class CommunicationController implements DataUpdateListener {
    /** XletContext. */
    private XletContext xletCtx;
    /** Instance of this class. */
    private static CommunicationController instance = new CommunicationController();
    /** Listener for Receiving Inband data. */
    private InbandListener inbandListener = new InbandListener();
    /** Listener for log level change. */
    private LogLevelAdapter logListener = new LogLevelAdapter();
    
    IxcLookupWorker ixcWorker;
    
    /**
     * Get Communication instance.
     * @return CommunicationController
     */
    public static CommunicationController getInstance() {
        return instance;
    }
    /**
     * Constructor.
     */
    private CommunicationController() {
    }
    /**
     * Starts lookup.
     * @param xCtx XletContext
     */
    public void init(XletContext xCtx) {
        xletCtx = xCtx;
        this.ixcWorker = new IxcLookupWorker(xletCtx);
        
        DataManager.getInstance();
        DataCenter.getInstance().addDataUpdateListener(Rs.DCKEY_INBAND, this);
        DataCenter.getInstance().addDataUpdateListener(MonitorService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(StcService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(VbmService.IXC_NAME, this);
        
        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME);
        ixcWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME);
        ixcWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME);
        if (!Environment.EMULATOR) {
        	ixcWorker.lookup("/1/2100/", StcService.IXC_NAME);
        }
        ixcWorker.lookup("/1/1020/", VODService.IXC_NAME);
        ixcWorker.lookup("/1/2085/", VbmService.IXC_NAME);
    }

    public void dataRemoved(String key) {
    }
    /**
     * Called by DataCenter whenever data is updated.
     * @param key DataCenter key
     * @param old old value
     * @param value new value
     */
    public void dataUpdated(String key, Object old, Object value) {
        if (value instanceof File) {
            if (key.equals(Rs.DCKEY_INBAND) && value != null) {
                DataManager.getInstance().parseData((File) value);
            }
        } else if (key.equals(MonitorService.IXC_NAME)) {
            MonitorService monitor = (MonitorService) value;
            try {
                monitor.addInbandDataListener(inbandListener, Rs.APP_NAME);
            } catch (Exception e) {
                Log.print(e);
            }
        } else if (key.equals(StcService.IXC_NAME)) {
            try {
                int currentLevel = ((StcService) value).registerApp(Rs.APP_NAME);
                if (Log.DEBUG_ON) {
                    Log.printDebug("stc log level : " + currentLevel);
                }
                Log.setStcLevel(currentLevel);
                Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                if (o != null) {
                    DataCenter.getInstance().put("LogCache", o);
                }
                try {
                    ((StcService) value).addLogLevelChangeListener(Rs.APP_NAME, logListener);
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
             } catch (Exception e) {
               Log.print(e);
             }
        } else if (key.equals(VbmService.IXC_NAME)) {
            VbmController.getInstance().init((VbmService) value);
        }
    }

    /**
     * Log level change listener.
     * @author Jinjoo Ok
     *
     */
    class LogLevelAdapter implements LogLevelChangeListener {
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    /**
     * Listener for receiving Inband data.
     */
    class InbandListener implements InbandDataListener {
        /**
         * called when Monitor tuned Inband channel.
         * @param locator string Locator
         */
        public void receiveInbandData(String locator) {

            if (Log.DEBUG_ON) {
                Log.printDebug("receiveInbandData " + locator);
            }
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            int monitorFileVersion = -1;
            try {
                 monitorFileVersion = monitor.getInbandDataVersion(MonitorService.help_topic);
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                    Log.printWarning(e);
                }
            }
            int versionHelpConfig = DataManager.getInstance().getVersionHelpConfig();
            if (monitorFileVersion != versionHelpConfig) {
                DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
            }
            try {
                if (monitor != null) {
                    monitor.completeReceivingData(Rs.APP_NAME);
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

}

package com.videotron.tvi.illico.help.communication;

import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * PreferenceProxy integrated with User Profile Application to apply the setting values.
 * @author Jinjoo Ok
 */
public class PreferenceProxy implements PreferenceListener {
    /** This class instance. */
    private static PreferenceProxy instance = new PreferenceProxy();
    /** XletContext. */
    private XletContext xletContext;
    /**
     * Get instance.
     * @return PreferenceProxy
     */
    public static PreferenceProxy getInstance() {
        return instance;
    }
    /**
     * Initialize.
     * @param xlet XletContext
     */
    public void init(XletContext xlet) {
        this.xletContext = xlet;
        lookupUPP();
    }

    /**
     * Lookup UPP.
     */
    private void lookupUPP() {
        try {
            new Thread() {
                public void run() {
                    String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
                    PreferenceService preferenceService = null;
                    while (true) {
                        if (preferenceService == null) {
                            try {
                                preferenceService = (PreferenceService) IxcRegistry.lookup(xletContext, lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("Not bound - " + lookupName);
                                }
                            }
                        }
                        if (preferenceService != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("PreferenceProxy.lookUpService: preferenceService ");
                            }
                            DataCenter.getInstance().put(PreferenceService.IXC_NAME, preferenceService);
                            addPreferenceListener();
                            break;
                        }
                        try {
                            Thread.sleep(Constants.MS_PER_SECOND * 2);
                        } catch (Exception e) {
                            Log.print(e);
                        }
                    }
                }
            } .start();
        } catch (Exception e) {
            Log.print(e);
        }
    }
    /**
     * register listener to UPP.
     */
    private void addPreferenceListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, addPreferenceListener");
        }
        try {
            PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                    PreferenceService.IXC_NAME);
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            String[] keys = null;
            String[] values = null;
            if (Log.DEBUG_ON) {
                Log.printDebug("PreferenceProxy, monitor exist ? " + (monitor != null));
            }
            keys = new String[] {PreferenceNames.LANGUAGE, PreferenceNames.DISPLAY_MENU_AT_POWER_ON};
            values = preferenceService.addPreferenceListener(this, Rs.APP_NAME, keys, new String[]{
                    Definitions.LANGUAGE_ENGLISH, Definitions.OPTION_VALUE_YES});
            for (int i = 0; i < values.length; i++) {
                DataCenter.getInstance().put(keys[i], values[i]);
            }
        } catch (RemoteException e) {
            Log.print(e);
        }
    }
    /**
     * Receive preferences from Profile.
     * @param name preference name
     * @param value value of preference
     * @exception RemoteException RemoteException
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("receiveUpdatedPreference, name : " + name + " value: " + value);
        }
        DataCenter.getInstance().put(name, value);
    }
    /**
     * Destroy.
     */
    public void destroy() {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.removePreferenceListener(Rs.APP_NAME, this);
            } catch (Exception e) {
                Log.print(e);
            }
        }
        DataCenter.getInstance().remove(PreferenceService.IXC_NAME);
    }

}

package com.videotron.tvi.illico.help.data;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Constants;
/**
 * An object class that configures topic data.
 * @author Jinjoo Ok
 *
 */
public class Topic {
    /** Topic type - image. */
    public static final int IMAGE_ONLY = 1;
    /** Topic type - text image. */
    public static final int IMAGE_TEXT = 2;
    /** Topic type - video. */
    public static final int IMAGE_VIDEO = 3;
    /** Topic type.*/
    private int type;
    /** French image name. */
    private String imageNameFr;
    /** English image name. */
    private String imageNameEn;
    /** SSP ID of French. */
    private String sspIdFr;
    /** SSP ID of English. */
    private String sspIdEn;
    /** Text name of French. */
    private String[] textImgFr;
    /** Text name of English. */
    private String[] textImgEn;
    /** Play duration when type is video.*/
    private int duration;
    /**
     * Gets the duration.
     * @return duration
     */
    public int getDuration() {
        return duration;
    }
    /**
     * Sets the duration.
     * @param dr duration
     */
    public void setDuration(int dr) {
        this.duration = dr;
    }

    /**
     * Constructor.
     */
    public Topic() {
        type = IMAGE_ONLY;
    }
    /**
     * Gets this topic's type.
     * @return type
     */
    public int getType() {
        return type;
    }
    /**
     * Sets type.
     * @param t type
     */
    public void setType(int t) {
        type = t;
    }
    /**
     * Sets image name in English.
     * @param name image name
     */
    public void setImageNameEn(String name) {
        imageNameEn = name;
    }
    /**
     * Sets image name in French.
     * @param name image name
     */
    public void setImageNameFr(String name) {
        imageNameFr = name;
    }
    /**
     * Gets image name according to language.
     * @return image name
     */
    public String getImageName() {
        if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_FRENCH)) {
            return imageNameFr;
        } else {
            return imageNameEn;
        }
    }
    /**
     * Initializes array.
     * @param size array length.
     */
    public void setTextImgLength(int size) {
        textImgFr = new String[size];
        textImgEn = new String[size];
    }
    /**
     * Sets image name in French.
     * @param i array index
     * @param str url
     **/
    public void setTextImgFr(int i, String str) {
        textImgFr[i] = str;
    }
    /**
     * Sets image name in English.
     * @param i array index
     * @param str url
     **/
    public void setTextImgEn(int i, String str) {
        textImgEn[i] = str;
    }
    /**
     * Gets text image URL according to the setting of language.
     * @return text image URL
     */
    public String[] getText() {
        if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_FRENCH)) {
            return textImgFr;
        } else {
            return textImgEn;
        }
    }
    /**
     * Sets SSP id in French.
     * @param id SSP id
     */
    public void setSspIdFr(String id) {
        sspIdFr = id;
    }
    /**
     * Sets SSP id in English.
     * @param id SSP id
     */
    public void setSspIdEn(String id) {
        sspIdEn = id;
    }
    /**
     * Gets SSP id.
     * @return SSP id
     */
    public String getSspId() {
        if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_FRENCH)) {
            return sspIdFr;
        } else {
            return sspIdEn;
        }
    }
}

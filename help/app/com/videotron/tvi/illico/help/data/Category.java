package com.videotron.tvi.illico.help.data;

import java.util.ArrayList;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Constants;

public class Category implements Cloneable {
    /** parent menu item. */
    private Category parent;
    /** menu id. */
    private String key;
    /** list of item. */
    private ArrayList items = new ArrayList();
    /** The category name in French. */
    private String nameFr;
    /** The category name in English. */
    private String nameEn;
    /** Topic. */
    private Topic topic;
    /** list of application name that related with the category. */
    private String[] appList;
    /**
     * Constructor.
     * @param id category id
     * */
    public Category(final String id) {
       this.key = id;
    }
    /**
     * Returns the key of this menu item.
     * @return key
     *  */
    public String getKey() {
        return key;
    }
    /**
     * Sets the key of this menu imte.
     * @param id key
     */
    public void setKey(String id) {
        key = id;
    }
    /**
     * Returns topic of this category.
     * @return topic
     */
    public Topic getTopic() {
        return topic;
    }
    /**
     * Sets topic.
     * @param tpc topic
     */
    public void setTopic(Topic tpc) {
        this.topic = tpc;
    }

    /**
     * Adds the child menu item.
     * @param item category
     * */
    public void add(Category item) {
        if (item != null) {
            item.parent = this;
            items.add(item);
        }
    }
    /**
     * Returns the index of child menu item.
     * @param item category
     * @return Index of this category
     **/
    public int indexOf(Category item) {
        return items.indexOf(item);
    }

    /**
     * Returns parent menu item.
     * @return parent category
     **/
    public Category getParent() {
        return parent;
    }

    /**
     * Returns the size of child menu item.
     * @return the size of child menu item.
     **/
    public int size() {
        return items.size();
    }

    /**
     * Checks this item has child or not.
     * @return true if this item have children.
     */
    public boolean hasChild() {
        return items.size() > 0;
    }

    /** Adds the child menu item. */
    public void setItemAt(Category item, int index) {
        item.parent = this;
        items.add(index, item);
    }

    /**
     * Returns the child menu item.
     * @param index
     * @return child menu
     **/
    public Category getItemAt(int index) {
        return (Category) items.get(index);
    }

    public Object clone() throws CloneNotSupportedException {
        items = new ArrayList();
        return super.clone();
    }
    /**
     * Returns category name depending current language setting.
     * @return category name
     */
    public String getName() {
        if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_FRENCH)) {
            return nameFr;
        } else {
            return nameEn;
        }
    }

    /**
     * Sets French name.
     * @param name French name
     **/
    public void setNameFr(String name) {
        nameFr = name;
    }

    /**
     * Sets English name.
     * @param name English name.
     **/
    public void setNameEn(String name) {
        nameEn = name;
    }

    /**
     * initialize application link.
     * @param size array size
     */
    public void initAppList(int size) {
        appList = null;
        appList = new String[size];
    }

    public void setAppList(int i, String name) {
        appList[i] = name;
    }
    /**
     * Whether if the application has a link to this Category.
     * @param name App name
     * @return if App has the link to this Category, return true.
     */
    public boolean hasAppLink(String name) {
        for (int i = 0; i < appList.length; i++) {
            if (name.equals(appList[i])) {
                return true;
            }
        }
        return false;
    }
    /**
     * Clears data.
     */
    public void clear() {
        topic = null;
        appList = null;
        items.clear();
    }

}

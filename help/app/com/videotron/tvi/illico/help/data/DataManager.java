package com.videotron.tvi.illico.help.data;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.help.Rs;
import com.videotron.tvi.illico.help.controller.HelpController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * This class integrated with Management System and Application Property to apply the setting values of Help
 * Application.
 * @author Jinjoo Ok
 */
public final class DataManager {
    /** Class instance. */
    private static DataManager instance = new DataManager();
    /** The version of configuration file. */
    private int versionHelpConfig;
    /** Array of categories. */
    private Category[] totalCategory;
    /** Root category. */
    private Category root;

    /**
     * Gets version of configuration file version.
     * @return version number
     */
    public int getVersionHelpConfig() {
        return versionHelpConfig;
    }

    /**
     * Constructor.
     */
    private DataManager() {
    }

    /**
     * Gets instance of this class.
     * @return DataManager
     */
    public static DataManager getInstance() {
        return instance;
    }

    /**
     * Parse data.
     * @param file inband data
     */
    public void parseData(File file) {
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            // File
            fis = new FileInputStream(file);
            baos = new ByteArrayOutputStream();
            int len = 0;
            while ((len = fis.read()) != -1) {
                baos.write(len);
            }
            byte[] arr = baos.toByteArray();
            len = arr.length;

            int index = 0;
            int version = oneByteToInt(arr, index++);
            if (Log.INFO_ON) {
                Log.printInfo("Help parseData() version = " + version);
            }
            if (version != versionHelpConfig) {
                versionHelpConfig = version;
                init();
                // language
                int lang = oneByteToInt(arr, index++);
                // category size
                len = oneByteToInt(arr, index++);
                if (len > 0) {
                    totalCategory = new Category[len];
                    String language, name;
                    Topic topic = null;
                    for (int i = 0, tempSize, type; i < len; i++) {
                        totalCategory[i] = new Category(String.valueOf(twoBytesToInt(arr, index)));
                        if (Log.INFO_ON) {
                            Log.printInfo("help parseData category id: " + totalCategory[i].getKey());
                        }
                        index += 2;
                        topic = new Topic();
                        for (int j = 0, length; j < lang; j++) {
                            language = new String(arr, index, 2).toLowerCase();// en,fr etc..
                            index += 2;
                            length = oneByteToInt(arr, index++);
                            name = new String(arr, index, length, "ISO8859_1");
                            if (language.equals(Constants.LANGUAGE_FRENCH)) {
                                totalCategory[i].setNameFr(name);
                            } else {
                                totalCategory[i].setNameEn(name);
                            }
                            if (Log.INFO_ON) {
                                Log.printInfo(" help parseData category name " + name);
                            }
                            index += length;
                            length = oneByteToInt(arr, index++);
                            name = new String(arr, index, length);
                            if (language.equals(Constants.LANGUAGE_FRENCH)) {
                                topic.setImageNameFr(name);
                            } else {
                                topic.setImageNameEn(name);
                            }
                            if (Log.INFO_ON) {
                                Log.printInfo("help parseData category image name: " + name);
                            }
                            index += length;
                        }
                        tempSize = oneByteToInt(arr, index++);
                        totalCategory[i].initAppList(tempSize);
                        String app;
                        for (int j = 0, length; j < tempSize; j++) {
                            length = oneByteToInt(arr, index++);
                            app = new String(arr, index, length);
                            totalCategory[i].setAppList(j, app);
                            if (Log.INFO_ON) {
                                Log.printInfo("help parseData category app link : " + app);
                            }
                            index += length;
                        }
                        type = oneByteToInt(arr, index++);
                        topic.setType(type);
                        if (Log.INFO_ON) {
                            Log.printInfo("help parseData topic type : " + topic.getType());
                        }
                        if (type == 2) {
                            tempSize = oneByteToInt(arr, index++);// page list size
                            if (Log.INFO_ON) {
                                Log.printInfo("help parseData topic page size : " + tempSize);
                            }
                            topic.setTextImgLength(tempSize);
                            for (int j = 0; j < tempSize; j++) {
                                for (int k = 0, length; k < lang; k++) {
                                    language = new String(arr, index, 2).toLowerCase();// fr, en
                                    index += 2;
                                    length = oneByteToInt(arr, index++);
                                    name = new String(arr, index, length);
                                    if (Log.INFO_ON) {
                                        Log.printInfo("help parseData topic name : " + name);
                                    }
                                    if (language.equals(Constants.LANGUAGE_FRENCH)) {
                                        topic.setTextImgFr(j, name);
                                    } else {
                                        topic.setTextImgEn(j, name);
                                    }
                                    index += length;
                                }
                            }
                        } else if (type == 3) {
                            topic.setDuration(twoBytesToInt(arr, index));
                            index += 2;
                            for (int j = 0, length; j < lang; j++) {
                                language = new String(arr, index, 2).toLowerCase();// fr, en
                                index += 2;
                                length = oneByteToInt(arr, index++);
                                name = new String(arr, index, length);
                                if (language.equals(Constants.LANGUAGE_FRENCH)) {
                                    topic.setSspIdFr(name);
                                } else {
                                    topic.setSspIdEn(name);
                                }
                                if (Log.INFO_ON) {
                                    Log.printInfo("help parseData topic name : " + name + " " + topic.getDuration());
                                }
                                index += length;
                            }
                        }
                        totalCategory[i].setTopic(topic);
                    }
                }
                len = oneByteToInt(arr, index++);// 1depth category size
                if (Log.INFO_ON) {
                    Log.printInfo("help parseData 1depth category size : " + len);
                }
                if (len > 0) {
                    String id;
                    root = new Category("Help");
                    root.setNameEn("Help");
                    root.setNameFr("Aide");
                    Category depth1 = null, depth2 = null, depth3 = null;
                    for (int i = 0, dep2, length; i < len; i++) {
                        length = oneByteToInt(arr, index++);
                        id = new String(arr, index, length);
                        index += length;
                        if (Log.INFO_ON) {
                            Log.printInfo("help parseData 1depth id : " + id);
                        }
                        dep2 = oneByteToInt(arr, index++);
                        depth1 = findCategory(id);
                        root.add(depth1);
                        if (Log.INFO_ON) {
                            Log.printInfo(" help parseData 2depth size : " + dep2);
                            Log.printInfo(" help parseData depth1  : " + depth1.getName() + " parent ; "
                                    + depth1.getParent().getName());
                        }
                        for (int j = 0, dep3; j < dep2; j++) {
                            length = oneByteToInt(arr, index++);
                            id = new String(arr, index, length);
                            index += length;
                            if (Log.INFO_ON) {
                                Log.printInfo("    help parseData 2depth id : " + id);
                            }
                            dep3 = oneByteToInt(arr, index++);
                            depth2 = findCategory(id);
                            depth1.add(depth2);
                            if (Log.INFO_ON) {
                                Log.printInfo(" help parseData 3depth size : " + dep3);
                            }
                            // if (Log.INFO_ON) {
                            // Log.printInfo("       depth1.add(depth2) : " + depth1.hashCode()
                            // +" " + depth2.hashCode());
                            // }
                            for (int k = 0; k < dep3; k++) {
                                length = oneByteToInt(arr, index++);
                                id = new String(arr, index, length);
                                index += length;
                                if (Log.INFO_ON) {
                                    Log.printInfo("       help parseData 3depth id : " + id);
                                }
                                depth3 = findCategory(id);
                                depth2.add(depth3);
                                if (Log.INFO_ON) {
                                    Log.printInfo("       depth3.parent : " + depth3.getParent().getName());
                                }
                            }
                        }
                    }
                }
                len = oneByteToInt(arr, index++);
                DataCenter.getInstance().put(Rs.DCKEY_IMG_PATH, new String(arr, index, len));
                if (Log.INFO_ON) {
                    Log.printInfo("Server url : " + DataCenter.getInstance().getString(Rs.DCKEY_IMG_PATH));
                }
            }
        } catch (Exception ex) {
            init();
            HelpController.getInstance().showErrorPopup(Rs.ERROR_CODE_HLP504);
            Log.print(ex);
        }
    }

    /**
     * Initializes data.
     */
    private void init() {
        if (totalCategory == null) {
            return;
        }
        for (int i = 0; i < totalCategory.length; i++) {
            totalCategory[i].clear();
        }
        totalCategory = null;
        if (root != null) {
            root.clear();
        }
        root = null;
    }

    /**
     * Finds category.
     * @param id id of category
     * @return Category {@link Category}
     */
    private Category findCategory(String id) {
        if (totalCategory == null) {
            return null;
        }
        String oriId = id;
        Category c = null;
        StringBuffer sb = new StringBuffer(oriId);
        int index = id.indexOf("_");
        if (index != -1) {
            id = sb.substring(index + 1);
        }
        Log.printDebug("findCategory original :" + oriId + " new :" + id);
        try {
            for (int i = 0; i < totalCategory.length; i++) {
                if (id.equals(totalCategory[i].getKey())) {
                    c = (Category) totalCategory[i].clone();
                    c.setKey(oriId);
                    return c;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return null;
    }

    /**
     * Finds application link.
     * @param name link name
     * @param id category key
     * @return category having the link {@link Category}
     */
    public Category findAppLink(String name, String id) {
        if (root == null) {
            return null;
        }

        Category cat = null;
        for (int i = 0, size = root.size(); i < size; i++) {
            cat = root.getItemAt(i);
            if (cat.hasChild()) {
                cat = findCategory(cat, id);
                if (cat != null) {
                    return cat;
                }
            } else if (cat.getKey().equals(id)) {
                return cat;
            }
        }
        return null;
    }

    /**
     * Finds specific category.
     * @param category Category
     * @param id category id
     * @return category {@link Category}
     */
    private Category findCategory(Category category, String id) {
        Category cat = null;
        for (int i = 0, size = category.size(); i < size; i++) {
            cat = category.getItemAt(i);
            if (cat.getKey().equals(id)) {
                return cat;
            }
            cat = findCategory(cat, id);
        }
        return cat;
    }

    /**
     * Gets root category.
     * @return root {@link Category}
     */
    public Category getRoot() {
        return root;
    }

    /**
     * Changes one byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]) & 0xFF);
    }

    /**
     * Changes two byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int twoBytesToInt(byte[] data, int offset) {
        return (((((int) data[offset]) & 0xFF) << 8) + (((int) data[offset + 1]) & 0xFF));
    }
}

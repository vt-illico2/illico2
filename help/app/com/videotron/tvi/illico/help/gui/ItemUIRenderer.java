package com.videotron.tvi.illico.help.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.data.Topic;
import com.videotron.tvi.illico.help.ui.BaseUI;
import com.videotron.tvi.illico.help.ui.ItemUI;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * Renderer for ItemUI. Display 3 depth.
 * @author Jinjoo Ok
 */
public class ItemUIRenderer extends CategoryUIRenderer {
    /** list shadow image. */
//    private Image iShadow;
    /** Background image. */
    private Image bg;
    /** Background image of list. */
//    private Image iListBg;
    /** Color. */
    private Color c12 = new Color(12, 12, 12);
    /** Color. */
    private Color c0Alpha = new Color(0, 0, 0, 77);
    /** Font of size 26. */
    private Font f26 = FontResource.BLENDER.getFont(26);

    protected void paint(Graphics g, UIComponent c) {
        paintBg(g, c);
        g.setFont(f26);
        g.setColor(Color.white);
        Category category = ((BaseUI) c).getCurrent();
        if (category != null) {
            g.drawString(category.getName(), 61, 98);
        }
        if (category != null && category.size() > 0) {
            // cat = category.getItemAt(focus);
            Image detail = ((BaseUI) c).getDetailImg();

            g.setColor(c0Alpha);
            g.fillRect(386, 139, 510, 287);
            if (detail != null) {
                g.drawImage(detail, 386, 139, 510, 287, c);
            } else {
                if (((ItemUI) c).isNoImage()) {
                    g.drawImage(defaultDetailImage, 386, 139, 510, 287, c);
                    g.setFont(f22);
                    g.setColor(Color.black);
                    String str = DataCenter.getInstance().getString("Label.lblUnavailable");
                    GraphicUtil.drawStringCenter(g, str, 641, 343);
                    g.setColor(cGray);
                    GraphicUtil.drawStringCenter(g, str, 640, 342);
                }
            }
            Topic topic = ((ItemUI) c).getTopic();
            if (((ItemUI) c).isFocusOnList()) {
                if (topic.getType() != Topic.IMAGE_ONLY) {
                    g.drawImage(iButtonDim, 563, 370, c);
                }
            } else {
                g.drawImage(iDetailFoc, 382, 135, c);
                if (topic.getType() != Topic.IMAGE_ONLY) {
                    g.drawImage(iButtonFoc, 563, 370, c);
                }
            }
            g.setFont(f18);
            g.setColor(c3);
            if (topic.getType() == Topic.IMAGE_TEXT) {
                GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblView"), 642, 391);
            } else if (topic.getType() == Topic.IMAGE_VIDEO) {
                GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblPlay"), 642, 391);
            }
        }
    }
    /**
     * Paints background.
     * @param g Graphics
     * @param c UIComponent
     */
    private void paintBg(Graphics g, UIComponent c) {
        g.drawImage(bg, 0, 0, c);
        paintDateTime(g, c);
        Image imgLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        int start = 53;
        if (imgLogo != null) {
            g.drawImage(imgLogo, start, 21, c);
            start += imgLogo.getWidth(c) + 14;
            g.drawImage(iHistory, start, 45, c);
            start += 11;
        }
        g.setColor(c239);
        g.setFont(f18);
        Category category = ((ItemUI) c).getCurrent();
        if (category != null) {
            g.drawString(category.getParent().getName(), start, 57);
        }
        g.drawImage(iBottom, 367, 466, c);
        g.setColor(c12);
        g.fillRect(60, 135, 322, 295);
//        g.drawImage(iListBg, 60, 135, c);
        g.drawImage(iOutline, 382, 135, c);
//        g.drawImage(iShadow, 60, 426, c);

    }

    public void prepare(UIComponent c) {
        super.prepare(c);
//        iShadow = DataCenter.getInstance().getImage("10_listbg_sh.png");
        bg = DataCenter.getInstance().getImage("bg.jpg");
//        iListBg = DataCenter.getInstance().getImage("10_bg_high.png");
        iDetailFoc = DataCenter.getInstance().getImage("10_img_foc_l.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    /**
     * Releases images.
     */
    public void releaseImage() {
        super.releaseImage();
//        DataCenter.getInstance().removeImage("10_bg_high.png");
        DataCenter.getInstance().removeImage("10_img_foc.png");
        DataCenter.getInstance().removeImage("10_focus.png");
//        DataCenter.getInstance().removeImage("10_listbg_sh.png");
        DataCenter.getInstance().removeImage("10_img_foc_l.png");
        DataCenter.getInstance().removeImage("bg.jpg");
    }
}

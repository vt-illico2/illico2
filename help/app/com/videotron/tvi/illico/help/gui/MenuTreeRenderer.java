package com.videotron.tvi.illico.help.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.help.comp.MenuTree;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.ui.BaseUI;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class MenuTreeRenderer extends Renderer {
    /** list bottom shadow image. */
//    private Image iBottomShadow;
    /** list top shadow image. */
//    private Image iTopShadow;
    /** Focus image of list. */
    protected Image iListFoc;
    /** Dimmed Focus image of list. */
    protected Image iListFocDim;
    /** bullet image. */
    protected Image iBullet;
    /** list line image. */
    protected Image iLine;
    /** Top of pane arrow image. */
    protected Image iArrowTop;
    /** Bottom of pane arrow image. */
    protected Image iArrowBottom;
    /** icon image. */
    protected Image icon;
    /** Font of size 18. */
    protected Font f18 = FontResource.BLENDER.getFont(18);
    /** Font of size 20. */
    private Font f20 = FontResource.BLENDER.getFont(20);
    /** Font of size 21. */
    private Font f21 = FontResource.BLENDER.getFont(21);
    /** Font of size 28. */
    private Font f28 = FontResource.BLENDER.getFont(28);
    /** Color. */
    private Color cTitle = new Color(214, 182, 55);
    /** Color. */
    private Color cGray = new Color(187, 189, 192);
    /** Color. */
    protected Color c230 = new Color(230, 230, 230);
    /** Color. */
    protected Color c3 = new Color(3, 3, 3);
    private Rectangle rec = new Rectangle(52, 71, 960 - 52, 415);

    public Rectangle getPreferredBounds(UIComponent c) {
        return rec;
    }

    protected void paint(Graphics g, UIComponent c) {
        g.translate(-rec.x, -rec.y);
        MenuTree m = (MenuTree) c;
        Category category = m.getCurrent();
        if (category == null) {
            return;
        }
        BaseUI ui = m.getCategoryUI();
        // VDTRMASTER-5550
        g.drawImage(icon, 58 + 5, 76 + 3, c);
        g.setFont(f20);
        g.setColor(cTitle);
        g.drawString(category.getName(), 91, 94);
        int focus = m.getFocus();
        if (category.size() > 0) {
            int startIdx = 0;
            int size = category.size();
            if (focus >= MenuTree.HYBRID_FOCUS && size > MenuTree.MAX_LIST_NUM) {
                startIdx = focus - MenuTree.HYBRID_FOCUS;
                if (focus + MenuTree.HYBRID_FOCUS >= size - 1) {
                    startIdx = size - MenuTree.MAX_LIST_NUM;
                }
            }
            g.setFont(f18);
            g.setColor(c230);
            Category cat = null;
            int step = m.getStep();

            for (int i = 0; i < size; i++) {
                if (i > MenuTree.MAX_LIST_NUM - 1) {
                    break;
                }
                cat = category.getItemAt(startIdx + i);
                if (startIdx + i != focus) {
                    g.drawString(TextUtil.shorten(cat.getName(), g.getFontMetrics(), 230), 97, 128 + i * 34);
                    g.drawImage(iBullet, 72, 119 + i * 34, c);
                }
                if (i < MenuTree.MAX_LIST_NUM - 1) {
                    g.drawImage(iLine, 66, 139 + i * 34, 268, 1, c);
                }
            }

            cat = category.getItemAt(focus);
            g.setFont(f28);
            g.setColor(cGray);
            g.drawString(cat.getName(), 375, 97);
            if (size > MenuTree.MAX_LIST_NUM) {
                if (focus > MenuTree.HYBRID_FOCUS) {
//                    g.drawImage(iTopShadow, 53, 104, c);
                    g.drawImage(iArrowTop, 189, 95, c);
                }
                if (focus + MenuTree.HYBRID_FOCUS < size - 1) {
//                    g.drawImage(iBottomShadow, 52, 449, c);
                    g.drawImage(iArrowBottom, 189, 470, c);
                }
            }

            if (step == MenuTree.ANIMATION_ENDED) {
                if (ui.isFocusOnList()) {
                    g.drawImage(iListFoc, 49, 102 + (focus - startIdx) * 34, c);
                } else {
                    g.drawImage(iListFocDim, 49, 102 + (focus - startIdx) * 34, c);
                }
            }
            g.setFont(f21);
            if (step == MenuTree.ANIMATION_ENDED) {
                g.setColor(c3);
            } else {
                g.setColor(c230);
            }
            g.drawString(TextUtil.shorten(cat.getName(), g.getFontMetrics(), 230), 97, 128 + (focus - startIdx) * 34);
        }
        g.translate(rec.x, rec.y);
    }

    public void prepare(UIComponent c) {
        iBullet = DataCenter.getInstance().getImage("01_bullet01.png");
        iLine = DataCenter.getInstance().getImage("00_mini_line.png");
        icon = DataCenter.getInstance().getImage("08_main_icon.png");
//        iBottomShadow = DataCenter.getInstance().getImage("00_bottom_shadow.png");
//        iTopShadow = DataCenter.getInstance().getImage("00_top_shadow.png");
        iListFoc = DataCenter.getInstance().getImage("00_menufocus.png");
        iListFocDim = DataCenter.getInstance().getImage("00_menufocus_dim.png");
        iArrowTop = DataCenter.getInstance().getImage("02_ars_t.png");
        iArrowBottom = DataCenter.getInstance().getImage("02_ars_b.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

}

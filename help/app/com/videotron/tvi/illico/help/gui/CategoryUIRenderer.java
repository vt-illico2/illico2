package com.videotron.tvi.illico.help.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.help.data.Category;
import com.videotron.tvi.illico.help.data.Topic;
import com.videotron.tvi.illico.help.ui.CategoryUI;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * Renderer for MenuUI. Menu have children menu.
 * @author Jinjoo Ok
 *
 */
public class CategoryUIRenderer extends Renderer {
    /** Background image.*/
    protected Image bg;
    /** image. */
    protected Image iHistory;
    /** focus image of right part. */
    protected Image iDetailFoc;
    /** button focus image. */
    protected Image iButtonFoc;
    /** dimmed button image. */
    protected Image iButtonDim;
    /** bullet image.*/
    protected Image iBullet;
    /** bottom image of place hotkey located. */
    protected Image iBottom;
    /** the Clock icon image. */
    private Image iClock;
     /** shadow image on menu list. */
//    private Image iMenuShadow;
    /** Background image.*/
    private Image iMenuBg;
    /** outline image. */
    protected Image iOutline;
    /** small outline image. */
    protected Image iSmallOutline;
    /** default detail image. */
    protected Image defaultDetailImage;
    /** default small detail image. */
    protected Image smallDefaultDetailImage;
    /** exit icon image. It is loaded in shared memory. */
    private Image iExit;
    /** Color. */
    protected Color c239 = new Color(239, 239, 239);
    /** Color. */
    protected Color c230 = new Color(230, 230, 230);
    /** Color. */
    protected Color c3 = new Color(3, 3, 3);
    /** Color. */
    private Color cExit = new Color(236, 211, 143);
    /** Color. */
    protected Color cGray = new Color(182, 182, 182);
    /** Font of size 17. */
    private Font f17 = FontResource.BLENDER.getFont(17);
    /** Font of size 18. */
    protected Font f18 = FontResource.BLENDER.getFont(18);
    /** Font of size 22. */
    protected Font f22 = FontResource.BLENDER.getFont(22);
    /** Maximum number of list. */
    public static final int MAX_LIST_NUM = 11;
    /** The index of starting hybrid focusing.*/
    public static final int HYBRID_FOCUS = MAX_LIST_NUM / 2;

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    protected void paint(Graphics g, UIComponent c) {
        CategoryUI ui = (CategoryUI) c;
        Category category = ui.getCurrent();

        paintBg(g, ui);
        Category root = ((CategoryUI) c).getRoot();
        if (root == ((CategoryUI) c).getCurrent()) {
            g.setFont(f18);
            g.setColor(cExit);
            String exit = DataCenter.getInstance().getString("Label.lblExit");
            g.drawString(exit, 64, 503);
            g.drawImage(iExit, 73 + g.getFontMetrics().stringWidth(exit), 489, c);
        }

        if (category == null) {
            return;
        }
        if (category.size() > 0) {
            Topic topic = ui.getTopic();
            int type = topic.getType();
            Image detail = ui.getDetailImg();
            int y = 355;
            if (type == Topic.IMAGE_ONLY) {
                g.drawImage(iOutline, 382, 119, c);
            } else {
                g.drawImage(iSmallOutline, 430, 149, c);
                y = 342;
            }
            if (detail != null) {
                if (type == Topic.IMAGE_ONLY) {
                    g.drawImage(detail, 386, 123, 510, 287, c);
                } else {
                    g.drawImage(detail, 434, 153, 415, 233, c);
                }
            } else {
                if (((CategoryUI) c).isNoImage()) {
                    if (type == Topic.IMAGE_ONLY) {
                        g.drawImage(defaultDetailImage, 386, 123, c);
                    } else {
                        g.drawImage(smallDefaultDetailImage, 434, 153, c);
                    }
                    g.setFont(f22);
                    g.setColor(Color.black);
                    String str = DataCenter.getInstance().getString("Label.lblUnavailable");
                    GraphicUtil.drawStringCenter(g, str, 641, y + 1);
                    g.setColor(cGray);
                    GraphicUtil.drawStringCenter(g, str, 640, y);
                }
            }
            if (ui.isFocusOnList()) {
                if (type != Topic.IMAGE_ONLY) {
                    g.drawImage(iButtonDim, 563, 427, c);
                }
            } else {
                if (type != Topic.IMAGE_ONLY) {
                    g.drawImage(iDetailFoc, 430, 149, c);
                    g.drawImage(iButtonFoc, 563, 427, c);
                }
            }

            g.setFont(f18);
            g.setColor(c3);
            if (topic.getType() == Topic.IMAGE_TEXT) {
                GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblView"), 642, 448);
            } else if (topic.getType() == Topic.IMAGE_VIDEO) {
                GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblPlay"), 642, 448);
            }
        }
    }
    /**
     * Paints date and time.
     * @param g Graphics
     * @param c UIComponent
     */
    protected void paintDateTime(Graphics g, UIComponent c) {
        Formatter formatter = Formatter.getCurrent();
        int longDateWth = 0;
        if (formatter != null) {
            String longDate = formatter.getLongDate();
            if (longDate != null) {
                g.setFont(f17);
                g.setColor(Color.white);
                longDateWth = FontResource.getFontMetrics(f17).stringWidth(longDate);
                g.drawString(longDate, 910 - longDateWth, 57);
            }
        }
        if (iClock != null) {
            int imgClockWth = iClock.getWidth(c);
            g.drawImage(iClock, 910 - longDateWth - 8 - imgClockWth, 43, c);
        }
    }
    /**
     * Draws background.
     * @param g Graphics
     * @param c UIComponent
     */
    private void paintBg(Graphics g, UIComponent c) {
        g.drawImage(bg, 0, 0, c);
        paintDateTime(g, c);
        Image imgLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        int start = 53;
        if (imgLogo != null) {
            g.drawImage(imgLogo, start, 21, c);
            start += imgLogo.getWidth(c) + 14;
            g.drawImage(iHistory, start, 45, c);
            start += 11;
        }
        g.setColor(c239);
        g.setFont(f18);
        g.drawString(DataCenter.getInstance().getString("Label.lblHelp"), start, 57);

//        g.drawImage(iMenuShadow, 0, 477, c);
        g.drawImage(iMenuBg, 52, 71, c);
        g.drawImage(iBottom, 367, 466, c);
    }

    public void prepare(UIComponent c) {
        Hashtable imgButtonTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        iExit = (Image) imgButtonTable.get(PreferenceService.BTN_EXIT);
        bg = DataCenter.getInstance().getImage("07_bg_main.jpg");
        iHistory = DataCenter.getInstance().getImage("his_over.png");
        iDetailFoc = DataCenter.getInstance().getImage("10_img_foc.png");
        iButtonFoc = DataCenter.getInstance().getImage("05_focus.png");
        iButtonDim = DataCenter.getInstance().getImage("05_focus_dim.png");
        iBullet = DataCenter.getInstance().getImage("01_bullet01.png");
        iBottom = DataCenter.getInstance().getImage("01_hotkeybg_setting.png");
        iClock = DataCenter.getInstance().getImage("clock.png");
//        iMenuShadow = DataCenter.getInstance().getImage("00_menushadow.png");
        iMenuBg = DataCenter.getInstance().getImage("00_menubg.png");
        smallDefaultDetailImage = DataCenter.getInstance().getImage("10_img_default_s.png");
        iSmallOutline = DataCenter.getInstance().getImage("10_img_outline_s.png");
        iOutline = DataCenter.getInstance().getImage("10_img_outline.png");
        defaultDetailImage = DataCenter.getInstance().getImage("10_img_default_m.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    /**
     * Releases images.
     */
    public void releaseImage() {
        DataCenter.getInstance().removeImage("10_img_outline_s.png");
        DataCenter.getInstance().removeImage("10_img_default_s.png");
        DataCenter.getInstance().removeImage("10_img_default_m.png");
        DataCenter.getInstance().removeImage("07_bg_main.jpg");
        DataCenter.getInstance().removeImage("his_over.png");
        DataCenter.getInstance().removeImage("10_img_foc.png");
        DataCenter.getInstance().removeImage("00_mini_line.png");
        DataCenter.getInstance().removeImage("01_bullet01.png");
        DataCenter.getInstance().removeImage("01_hotkeybg_setting.png");
        DataCenter.getInstance().removeImage("clock.png");
//        DataCenter.getInstance().removeImage("00_menushadow.png");
        DataCenter.getInstance().removeImage("00_menubg.png");
//        DataCenter.getInstance().removeImage("00_bottom_shadow.png");
//        DataCenter.getInstance().removeImage("00_top_shadow.png");
        DataCenter.getInstance().removeImage("00_menufocus.png");
        DataCenter.getInstance().removeImage("00_menufocus_dim.png");
        DataCenter.getInstance().removeImage("10_img_outline.png");
    }

}

This is Keyboard App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version 4K.1.0
    - Release App file Name
      : Keyboard.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2015.4.10

***
version 4K.1.0 - 2015.4.10
    - New Feature   
        + [4K] Initial draft for periscope features.
        + [4K] Flat design applied.
    - Resolved Issues
        N/A
      
***
version 1.0.1.1 - 2013.02.22
     - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4467   [R3] [MR] [INTGR] Selection feedback is offset in manual recording

***
version 1.0.0.0
     - New Feature
        + new version R3 1.0.0.0 
    - Resolved Issues
        + N/A
***
      version  0.3.21.0
    - New Feature
       + N/A
    - Resolved Issues
       + fixed that doesn't display keyboard in Setting app.
***
      version  0.3.20.1
    - New Feature
       + N/A
    - Resolved Issues
       + VDTRMASTER-3492  [ATP ENABLERS] [ALT-Enablers-1105] - Yellow cursor in Input Field doesn't move from right to left after a certain number of inputed characters
***
      version  0.3.19.2
    - New Feature
       + N/A
    - Resolved Issues
       + VDTRMASTER-3083  [UI/GUI] [Enabler-virtual keyboard] Virtual Keyboard API should allow to activated/deactivated the "A" function
       + VDTRMASTER-3082  [UI/GUI] [Enabler- virtual keyboard] Error in special characters keyboard
***
      version  0.3.18.0
    - New Feature
       + N/A
    - Resolved Issues
       + fixed the load image.
***
      version  0.3.17.0
    - New Feature
       + N/A
    - Resolved Issues
       + fixed the load image.
***
      version  0.3.16.0
    - New Feature
       + make a function to erase a character even if the keyboard component has not the focus(it's for Embedded.)
    - Resolved Issues
       + N/A
***
      version  0.3.15.1
    - New Feature
       + N/A
    - Resolved Issues
       + fixed the Log class.
***
      version  0.3.14.0
    - New Feature
       + N/A
    - Resolved Issues
       + fixed the Log class.
***
      version  0.3.13.1
    - New Feature
       + N/A
    - Resolved Issues
       + VDTRMASTER-2529 : [TOC] PVR We should see ... when recording name is too long
***
      version  0.3.12.1
    - New Feature
       + Adjust the bounds of Keyboard when the mode is embedded
    - Resolved Issues
       + VDTRMASTER-2137 : [UI/GUI] [ENA] keyboard's buttons all blink on select
***
      version  0.3.11.0
    - New Feature
       + Adjust the bounds of Keyboard when the mode is embedded
    - Resolved Issues
       + N/A
***
      version  0.3.10.0
    - New Feature
       + Clicking animation
    - Resolved Issues
       + VDTRMASTER-2097 -[UI/GUI] [Animation / General] - all selectable items should show a "bump" effect.
***
      version  0.3.9.1
    - New Feature
       + Applied modified Framework(OC unload)
    - Resolved Issues
       + VDTRMASTER-2094 : [UI/GUI] [Search] Add 'C' Erase key
***
      version  0.3.8.2
    - New Feature
        + N/A
    - Resolved Issues
       + VDTRMASTER-1730 [UI/GUI][enablers] - Virtual keyboard - Cursor movement is slow
***
      version  0.3.8.1
    - New Feature
        + N/A
    - Resolved Issues
       + VDTRMASTER-1859 [UI/GUI] [Search] - Keyboard behaviour
***
      version  0.3.7.1
    - New Feature
        + N/A
    - Resolved Issues
       + VDTRMASTER-1859 [UI/GUI] [Search] - Keyboard behaviour
***
version  0.3.6
    - New Feature
        + N/A
    - Resolved Issues
       + VDTRMASTER-1730 [UI/GUI][enablers] - Virtual keyboard - Cursor movement is slow 
***
version  0.3.5
    - New Feature
        + N/A
    - Resolved Issues
       + Do not use Log.developer.print(e) when it catches NotBoundException
       + changed text of change button "Changer de clavier" to "Chang. clavier"
***
version  0.3.4
    - New Feature
        + N/A
    - Resolved Issues
        + Changed text of Clear button "Suppr" to "Suppr."
***
version  0.3.3
    - New Feature
        + Pass event regarding text cleared when the type is embedded.
    - Resolved Issues
    	N/A
***
- Release Details
        New Features -NA
        Jira Fixed - VDTRMASTER-1179 New clear button is incorrect in French

        - Release name
          : version 0.3.2
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2011.03.30
***
- Release Details
        New Features -
                                       able to numeric key on RCU

        - Release name
          : version 0.3.1
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2011.03.18
***
- Release Details
		New Features -
		                              add the alphanumericKeyboards Type.
		                              able to numeric key on RCU
                                       display  "Clear"  according to input text

        - Release name
          : version 0.3.0
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2011.03.17
***------------------

- Release Details
		fixed the jira VDTRMASTER-909

        - Release name
          : version 0.2.14
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2011.02.18
***------------------
- Release Details
        new features - modify keyboard event for input field not to have a focus in embedded mode.

        - Release name
          : version 0.2.13
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2011.01.18
***------------------
- Release Details
        bug fixed - If new character is inputed, default character disappear.

        - Release name
          : version 0.2.12
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2011.01.04

- Release Details
        bug fixed - can not input when keyboard launched again after maximum input

        - Release name
          : version 0.2.11
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2010.12.23

- Release Details
        new feature - Remove contextual A key when mode is embedded.
                                - Always the first letter is upper case.
                                 - Modify keyboard phrase
        - Release name
          : version 0.2.10
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2010.12.13

- Release Details
        new feature - Registered API share a version with the Keyboard's version
        bug fixed -  do not passes event to listener when KEY_UP or KEY_DOWN is pressed, and mode is  embedded.

        - Release name
          : version 0.2.09
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2010.12.07

- Release Details
        bug fixed -  A key  passes to other application.
        - Release name
          : version 0.2.08
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2010.12.06

- Release Details
        embedded mode is added Alphanumeric Keyboard
        - Release name
          : version 0.2.07
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2010.12.01

- Release Details
        Use registered API (embedded keyboard)
        - Release name
          : version 0.2.06
        - Release type (official, debug, test,...)
          : official
        - Release date
          : 2010.11.30

- Resolved Issues

version 0.2.06
    + Add a Alphanumeric Keyboard.
version 0.2.04
    + fixed a bug, MULTI_TAB keyboard don't apply a max length of character.
    + fixed a bug to occur when move between Alphanumeric and MULTI-TAB with max char.

version 0.2.3
    + Add setting config
    + Add setting configuration data
    + Bug fixed

version 0.2.2
    + Bug fixed

version 0.2.1
    + update a version 0.2.1
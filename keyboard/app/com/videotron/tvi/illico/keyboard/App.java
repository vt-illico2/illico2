/**
 * @(#)App.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.io.File;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import org.ocap.system.RegisteredApiManager;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

/**
 * Implements Xlet for Virtual Keyboard Application.
 * @author Woojung Kim
 * @version 1.1
 */
public class App implements Xlet, ApplicationConfig {

    /**
     * The instance of XletContext. An interface that provides methods allowing an Xlet to discover information about
     * its environment.
     */
    private XletContext xletContext;
    /** The name of registered API. */
    private static final String REGISTERED_API_NAME = "reg_api_embedded_keyboard";

    /** The registered api storage prior. */
    public static final short REGISTERED_API_STORAGE_PRIOR = 123;
    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional If unconditional is true when this method is called, requests by the Xlet to not enter the
     *                destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *                 state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("Keyboard: destroyXlet");
        }
        FrameworkMain.getInstance().destroy();
        Controller.getInstance().dispose();
    }

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param xc the xlet context
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext xc) throws XletStateChangeException {
        xletContext = xc;
        registerAPI();
        FrameworkMain.getInstance().init(this);
        if (Log.DEBUG_ON) {
            Log.printInfo("Keyboard: initXlet");
        }
    }

    public void init() {
        Controller.getInstance().init(xletContext);
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        if (Log.INFO_ON) {
            Log.printInfo("Keyboard: pauseXlet");
        }
        FrameworkMain.getInstance().pause();
        Controller.getInstance().stop();
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("Keyboard: startXlet");
        }
        FrameworkMain.getInstance().start();
        Controller.getInstance().start();
    }

    /**
     * Returns the name of Keyboard application.
     * @return the name of keyboard application
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getApplicationName()
     */
    public String getApplicationName() {
        return "Keyboard";
    }

    /**
     * returns the version of Keyboard application.
     * @return the version of Keyboard application
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getVersion()
     */
    public String getVersion() {
        return "4K.1.0";//"1.0.1.1";
    }

    /**
     * return the XletContext of Keyboard application.
     * @return the XletContext of Keyboard application
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getXletContext()
     */
    public XletContext getXletContext() {
        return xletContext;
    }

    /**
     * Register api.
     */
    private void registerAPI() {
      if (Log.DEBUG_ON) {
          Log.printDebug("[Keyboard-App.registerAPI]start");
      }
      try {
          RegisteredApiManager rm = RegisteredApiManager.getInstance();
          rm.register(REGISTERED_API_NAME, getVersion(), new File("./scdf.xml").getAbsoluteFile(),
                  REGISTERED_API_STORAGE_PRIOR);
          String[] rAPINames = rm.getNames();
          if (rAPINames != null) {
              if (Log.DEBUG_ON) {
                  Log.printDebug("[Keyboard-App.registerAPI]Registered API Name Count : " + rAPINames.length);
              }
              for (int i = 0; i < rAPINames.length; i++) {
                  String rAPIName = rAPINames[i];
                  if (rAPIName == null) {
                      continue;
                  }
                  String rAPIVersion = rm.getVersion(rAPIName);
                  if (Log.DEBUG_ON) {
                      Log.printDebug("[Keyboard-App.registerAPI]" + i + "-Name[" + rAPIName + "], Versiont["
                          + rAPIVersion + "]");
                  }
              }
          }
      } catch (Exception ex) {
          Log.print(ex);
          if (Log.DEBUG_ON) {
              Log.printDebug("[Keyboard-App.registerAPI]Exception occurred");
          }
          return;
      }
      if (Log.DEBUG_ON) {
          Log.printDebug("[Keyboard-App.registerAPI]end");
      }
  }
}

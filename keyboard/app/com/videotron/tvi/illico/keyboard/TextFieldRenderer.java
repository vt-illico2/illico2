/**
 * @(#)TextFieldRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * The TextFieldRenderer class show a text area for TextField. this draw a background and text and cursor.
 * @author Woojung Kim
 * @version 1.1
 */
public class TextFieldRenderer extends Renderer {

    /** The max width of text area for VirtualKeyboard & VirtualExtendedKeyboard. */
    private final int MAX_TEXT_WIDTH = 175;

    /** The max width of text area for Multi-Tab. */
    private final int MAX_TEXT_WIDTH_FOR_MULTI_TAB = 190;

    /** The bounds of TextField area. */
    private Rectangle bounds = new Rectangle(8, 33, 239, 37);

    /** The text bounds of TextField area for VirtualKeyoard & VirtualExtendedKeyboard. */
    private Rectangle textBounds = new Rectangle(8, 7, 175, 25);

    /** The text bounds of TextField area for Multi-Tab. */
    private Rectangle text2Bounds = new Rectangle(8, 7, 205, 25);

    /** The background image of TextField area. */
    private Image bgImage;

    private Image bgMultiTabImage;

    /** The cursor image to notify a current position to input text. */
    private Image cursorImage;

    /** The Background coordinate for TextField area. */
    private Point bgPoint = new Point(0, 0);

    /** The initialized cursor point for TextField area. */
    private Point cursorPoint = new Point(8, 27);

    /** The initialized text point for TextField area. */
    private Point textPoint = new Point(8, 24);

    /** The font20 of Font. */
    private Font font20 = FontResource.BLENDER.getFont(20);

    /** The fm20 of FontMetrics. */
    private FontMetrics fm20 = FontResource.getFontMetrics(font20);

    /** The text which is shown in TextField area. */
    private String text;
    /** The position of cursor. */
    private int cursorPosition;
    /** The position of cursor. */
    private int textFieldCursorPosition;
    /** The length of current text. */
    private int textLength;

    /** The max width for current keyboard type. */
    private int currentMaxWidth;

    /** The width for temporary char in case of over MAX_TEXT_WIDTH_FOR_MULTI_TAB. */
    private int temporaryWidth;

    /** if TextField's text is over width of rendering text, it is true. */
    private boolean isOver;

    /** The current keyboard to use TextField is Multi-Tab or not */
    private boolean isMultiTab;

    private int maxChar;

    private String originalText = "";

    /**
     * Instantiates a new text field renderer.
     */
    public TextFieldRenderer() {
    }

    /**
     * Returns the preferred bounds of textFieldRenderer.
     * @param c a {@link UIComponent} to implement {@link TextField}
     * @return bounds a bounds of TextRenderer.
     * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.UIComponent)
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Paint Background and text and cursor.
     * @param g a Graphics of {@link UIComponent}.
     * @param c a {@link UIComponent} of {@link TextField}.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics,
     *      com.videotron.tvi.illico.framework.UIComponent)
     */
    protected void paint(Graphics g, UIComponent c) {

        g.setFont(font20);
        g.setColor(Color.white);
        Rectangle backBounds = g.getClipBounds();

        if (isMultiTab) {
            g.drawImage(bgMultiTabImage, bgPoint.x, bgPoint.y, c);
        } else {
            g.drawImage(bgImage, bgPoint.x, bgPoint.y, c);
        }

        int maxTextGap = 0;
        Log.printInfo("originalText.length() = " + originalText.length() + " cursorPosition =" + cursorPosition);
         if (text != null && originalText != null && originalText.length() >= maxChar && cursorPosition >= maxChar) {
             maxTextGap = 33;
         }
        Log.printInfo("isOver = " + isOver);
        // if (isOver) {
        // g.drawImage(cursorImage, cursorPoint.x + maxTextGap + currentMaxWidth, cursorPoint.y, c);
        // } else {

        if(textFieldCursorPosition <= 0) {
            g.drawImage(cursorImage, cursorPoint.x + maxTextGap, cursorPoint.y, c);
        } else {
            Log.printInfo("X = " +(cursorPoint.x + maxTextGap
                    + fm20.stringWidth(text.substring(0, textFieldCursorPosition))));
            g.drawImage(cursorImage, cursorPoint.x + maxTextGap
                    + fm20.stringWidth(text.substring(0, textFieldCursorPosition)), cursorPoint.y, c);
        }
        // }

        int width = 0;
        if (isMultiTab) {
            // g.clipRect(8, 7, 205, 25);
            g.clipRect(text2Bounds.x, text2Bounds.y, text2Bounds.width, text2Bounds.height);
            // g.setClip(text2Bounds);
            width = text2Bounds.width;
        } else {
            // g.clipRect(8, 7, 175, 25);
            g.clipRect(textBounds.x, textBounds.y, textBounds.width + 5, textBounds.height);
            // g.setClip(textBounds);
            width = textBounds.width;
        }

//        if (isOver) {
//            GraphicUtil.drawStringRight(g, text, textPoint.x + currentMaxWidth + temporaryWidth, textPoint.y, fm20);
//        } else {
            g.drawString(text, textPoint.x, textPoint.y);
//        }
        // g.setClip(backBounds);
        g.clipRect(backBounds.x, backBounds.y, backBounds.width, backBounds.height);
    }

    /**
     * Prepare a text from TextField and calculate a length of the text.
     * @param c a UIComponent to implement {@link TextField}.
     * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
     */
    public void prepare(UIComponent c) {
        if (Log.INFO_ON) {
            Log.printInfo("TextFieldRenderer: called prepare()");
        }
        bgImage = DataCenter.getInstance().getImage("04_key_search.png");
        bgMultiTabImage = DataCenter.getInstance().getImage("04_t9_search.png");
        cursorImage = DataCenter.getInstance().getImage("04_key_cursor.png");

        TextField textField = ((TextField) c);
        maxChar = textField.getMaxChar();
        Log.printInfo("MaxChar = " + maxChar);
        isMultiTab = textField.isMultiTab();
        cursorPosition = textField.getCursorPosition();

        if (isMultiTab) {
            currentMaxWidth = MAX_TEXT_WIDTH_FOR_MULTI_TAB;
        } else {
            currentMaxWidth = MAX_TEXT_WIDTH;
        }

        String tempText = textField.getText();
        originalText = tempText;
        int tempWidth = fm20.stringWidth(tempText);
        isOver = false;
        
        cursorPosition = textField.getCursorPosition();
        
        if(textFieldCursorPosition < 0 ) {
            textFieldCursorPosition  = 0;
        }

        if(text != null && originalText != null 
                && cursorPosition < (originalText.length() - text.length())) {
            text = tempText.substring(cursorPosition, cursorPosition + text.length());            
            isOver = true;
            Log.printInfo("text1 = " + text);
        } else if (tempWidth > currentMaxWidth) {
            int i = tempText.length();
            while (true) {
                String subText = tempText.substring(i - 1);
                if (fm20.stringWidth(subText) > currentMaxWidth) {
                    break;
                }
                i--;
            }
            // for margin
            if (i > 0) {
                i--;
            }
            text = tempText.substring(i);            
            isOver = true;
        } else {
            text = tempText;
        }        
        
        textFieldCursorPosition = cursorPosition - (originalText.length() - text.length());
        if(textFieldCursorPosition < 0) {
            textFieldCursorPosition = 0;
        }
        
        Log.printInfo("cursorPosition = " + cursorPosition);
        Log.printInfo("originalText.length = " + originalText.length());
        Log.printInfo("text.length() = " + text.length());
        Log.printInfo("textFieldCursorPosition = " + textFieldCursorPosition);
        
        textLength = fm20.stringWidth(text);
        if (isMultiTab && textField.getTemporaryChar() != 0) {
            StringBuffer sf = new StringBuffer(text);
            sf.append(textField.getTemporaryChar());
            text = sf.toString();
            temporaryWidth = fm20.charWidth(textField.getTemporaryChar());
        } else {
            temporaryWidth = 0;
        }        
    }
}

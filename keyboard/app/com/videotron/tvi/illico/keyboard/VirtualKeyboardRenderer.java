/**
 * @(#)VirtaulKeyboardRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * The VirtualKeyboardRenderer class paint a background, focus and key names.
 *
 * @author Woojung Kim
 * @version 1.1
 */
public class VirtualKeyboardRenderer extends Renderer {

    /** The y coordinate' offset for focused key has one space. */
    private static final int OFFSET_Y_FOCUSED_SMALL_KEYS = 4;

    /** The y coordinate's offset for focused key has double space. */
    private static final int OFFSET_Y_FOCUSED_BIG_KEYS = 3;

    /** The X coordinate's offset for key has double space. */
    private static final int OFFSET_X_DOUBLE_KEYS = 20;

    /** The X coordinate's offset for focused key has double space. */
    private static final int OFFSET_X_FOCUSED_DOUBLE_KEYS = 19;

    /** The width of one key. */
    private final int KEY_WIDTH = 40;

    /** The height of one key. */
    private final int KEY_HEIGHT = 31;

    /** The bounds of VirtualKeyboard. */
    private Rectangle bounds = new Rectangle(354, 174, 255, 350);

    /** The background image. */
    private Image bgImage = DataCenter.getInstance().getImage("04_keyboard01.png");

    /** The shadow image. */
//    private Image shadowImage = DataCenter.getInstance().getImage("04_keyboard_sh.png");

    /** The background image for Key line type 1. */
    private Image keyBg1Image = DataCenter.getInstance().getImage("04_keybg_01.png");

    /** The background image for Key line type 2. */
    private Image keyBg2Image = DataCenter.getInstance().getImage("04_keybg_02.png");

    /** The background image for Key line type 3. */
    private Image keyBg3Image = DataCenter.getInstance().getImage("04_keybg_03.png");

    /** The background iamge for Key line type 4. */
    private Image keyBg4Image = DataCenter.getInstance().getImage("04_keyBg_04.png");

    /** The focus image for small key. */
    private Image keyFocus1Image = DataCenter.getInstance().getImage("04_keyfoc_01.png");

    /** The focus image for big key. */
    private Image keyFocus2Image = DataCenter.getInstance().getImage("04_keyfoc_02.png");

    /** The coordinate of background image. */
    private Point bgPoint = new Point(0, 0);

    /** The coordinate of {@link #shadowImage}. */
    private Point shadowPoint = new Point(0, 307);

    /** The coordinate of {@link #keyBg1Image}. */
    private Point keyBg1Point = new Point(5, 50);

    /** The coordinate of {@link #keyBg2Image}. */
    private Point[] keyBg2Point = { new Point(5, 83), new Point(5, 114), new Point(5, 145), new Point(5, 207),
            new Point(5, 238) };

    /** The coordinate of {@link #keyBg3Image}. */
    private Point keyBg3Point = new Point(8, 176);

    /** The coordinate of {@link #keyBg4Image}. */
    private Point keyBg4Point = new Point(8, 269);

    /** The start coordinate of key string. */
    private Point keyAPoint = new Point(29, 73);

    /** The start coordinate of key focus. */
    private Point keyFocusPoint = new Point(7, 51);

    /** The font of size 19. */
    private Font font19 = FontResource.BLENDER.getFont(19);

    /** The font of size 22. */
    private Font font22 = FontResource.BLENDER.getFont(22);

    /** The FontMetics for font19. */
    private FontMetrics fm19 = FontResource.getFontMetrics(font19);

    /** The FontMetics for font22. */
    private FontMetrics fm22 = FontResource.getFontMetrics(font22);

    /** The String array of key strings. */
    private String[][] keyboardStrings;

    /**
     * Instantiates a new virtual keyboard renderer.
     */
    public VirtualKeyboardRenderer() {
    }

    /**
     * Returns a bounds of VirtualKeyboard.
     *
     * @param c a UIComponent of VirtualKeyboard.
     * @return a bounds of VirtualKeyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.
     *      UIComponent)
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Paint a background and key for VirtualKeyboard.
     *
     * @param g the Graphics to paint VirtualKeyboard.
     * @param c the UIComponent to implement VirtualKeyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics,
     *      com.videotron.tvi.illico.framework.UIComponent)
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImage, bgPoint.x, bgPoint.y, c);
//        g.drawImage(shadowImage, shadowPoint.x, shadowPoint.y, c);

        // TODO the version number for QA
        g.drawString(FrameworkMain.getInstance().getApplicationVersion(), shadowPoint.x + 10, shadowPoint.y + 30);

        // textbox
        g.drawImage(keyBg1Image, keyBg1Point.x, keyBg1Point.y, c);
        for (int i = 0; i < keyBg2Point.length; i++) {
            g.drawImage(keyBg2Image, keyBg2Point[i].x, keyBg2Point[i].y, c);
        }
        g.drawImage(keyBg3Image, keyBg3Point.x, keyBg3Point.y, c);
        g.drawImage(keyBg4Image, keyBg4Point.x, keyBg4Point.y, c);

        for (int i = 0; i < keyboardStrings.length; i++) {
            for (int j = 0; j < keyboardStrings[i].length; j++) {
                if (keyboardStrings[i][j] != null) {
                    int currentIndex = i * VirtualKeyboard.HORIZONTAL_WIDTH + j;
                    int stepX = j * KEY_WIDTH;
                    int stepY = i * KEY_HEIGHT;
                    if (c.getFocus() == currentIndex) {
                        g.setColor(Color.black);
                        g.setFont(font22);
                        if (keyboardStrings[i][j].length() > 1) { // case of space, erase, Done, Cancel, Muti-Tap
                            g.drawImage(keyFocus2Image, keyFocusPoint.x + stepX, keyFocusPoint.y + stepY, c);
                            GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], keyAPoint.x + stepX
                                    + OFFSET_X_FOCUSED_DOUBLE_KEYS, keyAPoint.y + stepY + OFFSET_Y_FOCUSED_BIG_KEYS,
                                    fm22);
                        } else {
                            g.drawImage(keyFocus1Image, keyFocusPoint.x + stepX, keyFocusPoint.y + stepY, 44, 37, c);
                            GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], keyAPoint.x + stepX - 1, keyAPoint.y
                                    + stepY + OFFSET_Y_FOCUSED_SMALL_KEYS, fm22);
                        }
                    } else {
                        g.setColor(Color.white);
                        g.setFont(font19);
                        if (keyboardStrings[i][j].length() > 1) { // case of space, erase, Done, Cancel, Muti-Tap
                            GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], keyAPoint.x + stepX
                                    + OFFSET_X_DOUBLE_KEYS, keyAPoint.y + stepY, fm19);
                        } else {
                            GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], keyAPoint.x + stepX, keyAPoint.y
                                    + stepY, fm19);
                        }
                    }
                }
            }
        }
    }

    /**
     * Prepare before rendering. currently just wait for Images.
     *
     * @param c the UIComponent to implement {@link VirtualKeyboard}.
     * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
     */
    public void prepare(UIComponent c) {
        if (Log.INFO_ON) {
            Log.printInfo("VirtualKeyboardRenderer: called prepare().");
        }
        keyboardStrings = ((VirtualKeyboard) c).getKeyboardStrings();
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
}

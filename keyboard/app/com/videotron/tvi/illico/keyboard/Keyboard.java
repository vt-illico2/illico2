/**
 * @(#)Keyboard.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * The Base class of Keyboard UI that provide various API as an abstract class. Can obtain different Keyboard UI class
 * using Create() method. Calling Application must set the location to use in embedded mode. Because it is sub-class of
 * Java.awt.Component, use setLocation(int, int) method.
 * @author Woojung Kim
 * @version 1.1
 */
public class Keyboard extends UIComponent {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1177727607960401259L;

    /** The event which finish to input text. */
    protected static final int EVENT_INPUT_ENDED = 0;

    /** The event which cancel to input text. */
    protected static final int EVENT_INPUT_CANCELED = 1;

    /** The event which move cursor in text. */
    protected static final int EVENT_CURSOR_MOVED = 2;

    /** The event that text changed. */
    protected static final int EVENT_TEXT_CHANGED = 3;
    /** The event that focus in out of keyboard. */
    protected static final int EVENT_FOCUS_OUT = 4;
    /** The event that mode changed. */
    protected static final int EVENT_MODE_CHANGED = 5;

    /** The event that input cleared. */
    protected static final int EVENT_INPUT_CLEARED = 6;

    /** The text field for Keyboard. */
    protected static TextField textField;

    /** The keyboard listeners to notify Keyboard events. */
    protected KeyboardListener keyboardListener;

    /** The input pattern. */
    protected String inputPattern;

    /** The max char length of text which is inputed by User. */
    protected int maxCharslen;

    /** The echo char to hide a text which is inputed by User. */
    protected char echoChar;

    /** close or not if reach a max of char length. */
    protected boolean closeWithMaxInput;

    /** The type of Keyboard. */
    protected int keyboardType;

    /** The mode of Keyboard. */
    protected short keyboardMode;

    /** The LayeredUI. */
    private static LayeredUI layeredUI;
    /** The LayeredUI. */
    private static LayeredWindowImpl layeredWindowImpl;
    /** The component is the current Component. */
    private static UIComponent currentUIComponet;

    /** The Keyboard Language is setting to read from UPP. */
    protected static String settingLanguage;
    /** The Keyboard type is setting to read from UPP. */
    protected static String settingKeyboardType = "";

    /** The string for title. */
    protected String titleString;
    /** Effect. */
    protected ClickingEffect clickEffect;
    
    protected boolean aKeyDeactive = false;

    /**
     * Instantiates a new Keyboard.
     */
    public Keyboard() {

    }

    /**
     * Called when start a keyboard. Adds a keyboard in layeredUI. make layeredUI to activate.
     * @param c The current UIComponent of current keyboard.
     */
    public void start(UIComponent c) {
        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            if (textField == null) {
                textField = new TextField();
            }
            if (layeredWindowImpl == null) {
                layeredWindowImpl = new LayeredWindowImpl();
            }

            if (layeredUI == null) {
                layeredUI = WindowProperty.KEYBOARD.createLayeredDialog(layeredWindowImpl, Constants.SCREEN_BOUNDS,
                        layeredWindowImpl);
            }
        }
        currentUIComponet = c;
        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            layeredWindowImpl.add(c);
        }
        currentUIComponet.setVisible(true);
        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            layeredUI.activate();
        }

    }

    /**
     * Called when stop a keyboard. removes a keybaord in layeredUI. make layeredUI to deactivate.
     */
    public void stop() {
        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            layeredWindowImpl.remove(currentUIComponet);
            currentUIComponet.setVisible(false);
        }
        if (Controller.getInstance().isShow()) {
            Controller.getInstance().setShow(false);
        }

        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            layeredUI.deactivate();
        }
    }

    /**
     * Calling Application must set the location to use in embedded mode.
     * @param x x coordination to display keyboard
     * @param y y coordination to display keyboard.
     * @see java.awt.Component#setLocation(int, int)
     */
    public void setLocation(int x, int y) {
        setBounds(x, y, getWidth(), getHeight());
        repaint();
    }

    /**
     * Creates a Keyboard to use type and mode.
     * @param type the type of keyboard
     * @param mode the mode of keyboard
     * @return the keyboard which created. Default value of return is the VirtualKeyboard.
     */
    public static Keyboard create(int type, short mode) {

        Keyboard keyboard = null;

        if (type == KeyboardOption.TYPE_VIRTUAL) {
            keyboard = new VirtualKeyboard();
        } else if (type == KeyboardOption.TYPE_MULTI_TAP) {
            keyboard = new MultiTabKeyboard();
        } else if (type == KeyboardOption.TYPE_ALPHANUMERIC) {
            keyboard = new AlphanumericKeyboard();
        } else {
            keyboard = new AlphanumericKeyboard();
        }
//        else if (type == KeyboardOption.TYPE_VIRTUAL_EXTENDED) {
//            keyboard = new VirtualExtendedKeyboard();
//        }

        keyboard.setType(type);
        keyboard.setMode(mode);

        return keyboard;
    }

    /**
     * Creates the Keyboard by using KeyboardOption.
     * @param o the option to create Keyboard
     * @return the keyboard the object of Keyboard.
     */
    public static Keyboard create(KeyboardOption o) {

        try {
            Keyboard keyboard = null;

            if (o.getType() == KeyboardOption.TYPE_VIRTUAL) {
                keyboard = new VirtualKeyboard();
            } else if (o.getType() == KeyboardOption.TYPE_MULTI_TAP) {
                keyboard = new MultiTabKeyboard();
            } else if (o.getType() == KeyboardOption.TYPE_ALPHANUMERIC) {
                keyboard = new AlphanumericKeyboard();
            } else {
                keyboard = new AlphanumericKeyboard();
            }

            if (keyboard != null) {
                keyboard.setAlphanumericType(o.getAlphanumericType());
                keyboard.setText(o.getDefaultText());
                keyboard.setTempViewText(o.getTempDefaultText());
                keyboard.setInputPattern(o.getInputPattern());
                keyboard.setMaxChars(o.getMaxChars());
                keyboard.setEchoChar(o.getEchoChars());
                keyboard.setCloseWithMaxInput(o.isClosedWithMaxInput());
                keyboard.setType(o.getType());
                keyboard.setMode(o.getMode());
                keyboard.setTitleString(o.getTitle());
                keyboard.setAkeyActivate(o.isKeyboardChangedDeactivate());
            }

            return keyboard;
        } catch (RemoteException e) {
            Log.print(e);
        }

        return null;
    }

    /**
     * Sets the text into text field.
     * @param text the new text which want to set.
     */
    public void setText(String text) {
        if (textField == null) {
            textField = new TextField();
        }
        textField.setText(text);
    }

    /**
     * Set the temporary text.
     * @param view is
     */
    public void setTempViewText(boolean view) {
        if (textField == null) {
            textField = new TextField();
        }
        textField.setTempViewText(view);
    }

    /**
     * is the temporary text view.
     * @return false
     */
    public boolean getTempViewText() {
        return false;
    }

    /**
     * Sets the input pattern.
     * @param pattern the new input pattern
     */
    public void setInputPattern(String pattern) {
        inputPattern = pattern;
    }

    /**
     * Sets the max chars.
     * @param len the new max chars
     */
    public void setMaxChars(int len) {
        if (textField == null) {
            textField = new TextField();
        }
        maxCharslen = len;
        textField.setMaxChar(maxCharslen);
    }

    /**
     * Sets the echo char.
     * @param echo the new echo char
     */
    public void setEchoChar(char echo) {
        echoChar = echo;
    }

    /**
     * Sets a boolean of closeWithMaxInput.
     * @see {@link #closeWithMaxInput}
     * @param close the new close with max input
     */
    public void setCloseWithMaxInput(boolean close) {
        closeWithMaxInput = close;
    }

    /**
     * Sets the type of Keyboard to create.
     * @param type the new type
     */
    public void setType(int type) {
        keyboardType = type;
    }

    /**
     * Sets the mode of Keyboard to create.
     * @param mode the new mode
     */
    public void setMode(short mode) {
        keyboardMode = mode;
    }

    /**
     * Set Title text.
     * @param title is title.
     */
    public void setTitleString(String title) {
        this.titleString = title;
    }
    
    public void setAkeyActivate(boolean b) {
        aKeyDeactive = b;
    }

    /**
     * Gets the type of Keyboard.
     * @return the type
     */
    public int getType() {
        return keyboardType;
    }

    /**
     * Gets the mode of Keyboard.
     * @return the mode
     */
    public short getMode() {
        return keyboardMode;
    }

    /**
     * Returns the string of TextField.
     * @return the text in TextField.
     */
    public String getText() {
        return textField.getText();
    }

    /**
     * Returns the InputPattern.
     * @return the pattern string.
     */
    public String getInputPattern() {
        return inputPattern;
    }

    /**
     * Returns the max char length for TextField.
     * @return a value of MaxCharslen
     */
    public int getMaxCharsLen() {
        return maxCharslen;
    }

    /**
     * Returns the echo char for TextField.
     * @return the echo char.
     */
    public char getEchoChar() {
        return echoChar;
    }

    /**
     * Returns a boolean of closeWithMaxInput for TextField.
     * @see #setCloseWithMaxInput(boolean)
     * @return the value of closeWithMaxInput
     */
    public boolean getCloseWithMaxInput() {
        return closeWithMaxInput;
    }
    
    public boolean isAkeyDeactive () {
        return aKeyDeactive;
    }

    /**
     * Get the title.
     * @return titleString
     */
    public String getTitleStrings() {
        return titleString;
    }

    /**
     * Notify keyboard event to caller application via KeyboardListener.
     * @param event the event type (EVENT_INPUT_ENDED, EVENT_INPUT_CANCELED, EVENT_CURSOR_MOVED, EVENT_TEXT_CHANGED)
     * @param position In the case of EVENT_CURSOR_MOVED,
     */
    protected void notifyKeyboardEvent(int event, int position) {
        try {
            if (keyboardListener == null) {
                return;
            }
            String text = textField.getText();
            if (event == EVENT_INPUT_ENDED) {
                keyboardListener.inputEnded(text);
                Controller.getInstance().stop();
            } else if (event == EVENT_INPUT_CANCELED) {
                keyboardListener.inputCanceled(text);
                Controller.getInstance().stop();
            } else if (event == EVENT_CURSOR_MOVED) {
                keyboardListener.cursorMoved(position);
            } else if (event == EVENT_TEXT_CHANGED) {
                keyboardListener.textChanged(text);
            } else if (event == EVENT_FOCUS_OUT) {
                keyboardListener.focusOut(position);
            } else if (event == EVENT_MODE_CHANGED) {
                keyboardListener.modeChanged(position);
            }
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * Register the listener to receive Keyboard event.
     * @param l the listener to receive Keyboard event.
     */
    public void registerKeyboardListener(KeyboardListener l) {
        keyboardListener = l;
    }

    /**
     * Removes the listener to receive Keyboard event.
     * @param l the listener to implement KeyboardListener.
     */
    public void removeKeyboardListener(KeyboardListener l) {
        if (l == null) {
            return;
        }
        if (keyboardListener != null && keyboardListener.equals(l)) {
            keyboardListener = null;
        }
    }

    /**
     * Returns a registered KeyboardListener.
     * @return a registered listener.
     */
    public KeyboardListener getKeyboardListener() {
        return keyboardListener;
    }

    /**
     * The LayeredWindowImpl implements LayeredKeyHandler.
     * @author PellOs
     * @version 1.1
     */
    class LayeredWindowImpl extends LayeredWindow implements LayeredKeyHandler {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1177727607960401260L;

        /**
         * Constructor.
         */
        public LayeredWindowImpl() {
            setBounds(Constants.SCREEN_BOUNDS);
        }

        /**
         * The handleKeyEvent.
         * @param event is UserEvent.
         * @return used
         */
        public boolean handleKeyEvent(UserEvent event) {
            if (event.getType() != KeyEvent.KEY_PRESSED) {
                return false;
            }

            int keyCode = event.getCode();

            switch (keyCode) {
            case KeyCodes.COLOR_C:
            case OCRcEvent.VK_EXIT:
            case KeyCodes.LAST:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_ENTER:
            case OCRcEvent.VK_COLORED_KEY_3:
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
                boolean used = true;
                if (currentUIComponet != null) {
                    used = currentUIComponet.handleKey(keyCode);
                }
                return used;
            default:
                return false;
            }
        }
    }

    /**
     * Keyboard language is set from UPP data of Setting application.
     * @param language is "English" or "Français".
     */
    public static void setLanguage(String language) {

        if (language.equals("English")) {
            settingLanguage = "_en";
        } else {
            settingLanguage = "_fr";
        }
    }

    /**
     * Keyboard type is set from UPP data of Setting application.
     * @param type is "Alphanumeric" or "Multi-tap".
     */
    public static void setSettingKeyboardType(String type) {
        settingKeyboardType = type;
    }

    /**
     * Set the alphanumeric type.
     * @param t is alphanumeric type.
     */
    public void setAlphanumericType(int t) {
    }
}

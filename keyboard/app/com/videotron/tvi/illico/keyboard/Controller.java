/**
 * @(#)Controller.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.awt.Point;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;
import com.videotron.tvi.illico.log.Log;

/**
 * The Main class of Keyboard Enabler. To communicate with other application bind a KeyboardService for IXC. Manages to
 * show and hide Keyboard UI and move between Keyboards.
 * @author Woojung Kim
 * @version 1.1
 */
public final class Controller implements KeyboardService {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** The instance of Controller. */
    private static Controller instance = new Controller();
    /** The xlet context. */
    private XletContext xletContext;
    /** The uiTable has a UIComponet to implement each keyboard. */
    private Hashtable uiTable;
    /** The current keyboard. */
    private Keyboard currentKeyboard;

    /** This value is double check keyboard. */
    private boolean isShow;

    /**
     * Instantiates a Controller for singleton.
     */
    private Controller() {
    }

    /**
     * Gets the singleton instance of Controller.
     * @return single instance of Controller
     */
    public static Controller getInstance() {
        return instance;
    }

    /**
     * Inits the Controller. creates {@link #uiTable}.
     * @param xc the xc received from {@link App} is used to bind and lookup IXC
     */
    public void init(XletContext xc) {
        uiTable = new Hashtable();
        xletContext = xc;
        PreferenceProxy.getInstance().init(xletContext);
    }

    /**
     * Called in {@link App#startXlet()} when Keyboard Application start.
     */
    public void start() {
        bind();

//        if(Environment.EMULATOR){
////            KeyboardOption
//            KeyboardOptionImpl o = new KeyboardOptionImpl();
//
//            Keyboard keyboard = Keyboard.create(o);
//            currentKeyboard = keyboard;
//            Point p =  null;
//            try {
//                p = o.getPosition();
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//            if (p != null) {
//                keyboard.setLocation(p.x, p.y);
//            }
//
//            keyboard.setLanguage("English");
//            keyboard.prepare();
//            keyboard.start();
//        }
    }

    /**
     * Called by {@link App#pauseXlet()} when Keybaord Application start.
     */
    public void stop() {
        Enumeration enums = uiTable.elements();

        while (enums.hasMoreElements()) {
            Keyboard keyboard = (Keyboard) enums.nextElement();
            keyboard.stop();
        }

        if (currentKeyboard != null) {
            currentKeyboard.setVisible(false);
            currentKeyboard.stop();
            currentKeyboard = null;
        }
    }

    /**
     * Called by {@link App#destroyXlet(boolean)} when Keyboard Application destroy.
     */
    public void dispose() {
        stop();
    }

    /**
     * Creates the keyboard.
     * @return the keyboard
     */
    protected Keyboard createKeyboard() {
        return new Keyboard();
    }

    /**
     * Binds a remote Object of KeyboardService to communicate with application which want to use Keyboard.
     */
    private void bind() {
        if (Log.INFO_ON) {
            Log.printInfo("Controller: called bind()");
        }
        try {
            IxcRegistry.bind(xletContext, KeyboardService.IXC_NAME, this);
        } catch (AlreadyBoundException e) {
            Log.print(e);
        }
    }

    /**
     * Implements {@link KeyboardService#showPopupKeyboard(KeyboardOption, KeyboardListener)}. Creates Popup Virtual
     * Keyboard to use KeyboardOption and adds listener to listener lists to notify Virtual Keyboard event.
     * @param o the keyboard option includes informations to create keyboard.
     * @param listener the listener to notify Virtual Keyboard event.
     * @throws RemoteException If a remote stub class cannot be called.
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardService#showPopupKeyboard(com.videotron.tvi.illico.ixc.
     *      keyboard.KeyboardOption, com.videotron.tvi.illico.ixc.keyboard.KeyboardListener)
     */
    public void showPopupKeyboard(KeyboardOption o, KeyboardListener listener) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("Controller: showPoupKeyboard");
        }
        if (isShow) {
            if (Log.INFO_ON) {
                Log.printInfo("Controller: showPoupKeyboard already showing, so ignored");
            }
            return;
        }
        isShow = true;

        Keyboard keyboard = Keyboard.create(o);
        keyboard.registerKeyboardListener(listener);
        uiTable.put(String.valueOf(o.getType()), keyboard);
        currentKeyboard = keyboard;
        Point p = o.getPosition();
        if (p != null) {
            keyboard.setLocation(p.x, p.y);
        }
        keyboard.prepare();
        keyboard.start();

    }

    /**
     * Hides Popup Virtual Keyboard. {@link uiTable} is clear to remove a added keyboards. removes KeyboardListener and
     * KeyListener. hides UICompoenent to show Virtual Keyboard.
     * @param listener the listener to remove a registered listener.
     * @throws RemoteException the remote exception if a remote stub class cannot be called.
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardService#hidePopupKeyboard(com.videotron.tvi.illico.ixc.
     *      keyboard.KeyboardListener)
     */
    public void hidePopupKeyboard(KeyboardListener listener) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("Controller: hidePopupKeyboard");
        }
        if (currentKeyboard != null) {
            if (!isShow) {
                if (Log.INFO_ON) {
                    Log.printInfo("Controller: hidePopupKeyboard already closed, so ignored");
                }
                return;
            }
            isShow = false;
            uiTable.clear();

            Enumeration enums = uiTable.elements();

            while (enums.hasMoreElements()) {
                Keyboard keyboard = (Keyboard) enums.nextElement();

                KeyboardListener l = keyboard.getKeyboardListener();
                if (listener.equals(l)) {
                    keyboard.removeKeyboardListener(l);
                    keyboard.stop();
                }
            }

            if (currentKeyboard != null) {
                currentKeyboard.removeKeyboardListener(listener);
                currentKeyboard.stop();
                currentKeyboard = null;
            }
        }
    }

    /**
     * Move to next keyboard when User select a other keyboard by pressing button in UI. ui type means a one of
     * {@link KeyboardOption#TYPE_VIRTUAL}, {@link KeyboardOption#TYPE_VIRTUAL_EXTENDED},
     * {@link KeyboardOption#TYPE_MULTI_TAP}.
     * @param type the type of keyboard
     */
    public void moveToNextKeyboard(int type) {
        Keyboard requestKeyboard;
        if (uiTable.contains(String.valueOf(type))) {
            requestKeyboard = (Keyboard) uiTable.get(String.valueOf(type));
        } else {
            requestKeyboard = Keyboard.create(type, currentKeyboard.getMode());
        }

        String text = currentKeyboard.getText();
        if (text == null) {
            text = "";
        }
        requestKeyboard.setText(text);
        requestKeyboard.setInputPattern(currentKeyboard.getInputPattern());
        requestKeyboard.setCloseWithMaxInput(currentKeyboard.getCloseWithMaxInput());
        requestKeyboard.setMaxChars(currentKeyboard.maxCharslen);
        requestKeyboard.setEchoChar(currentKeyboard.getEchoChar());
        requestKeyboard.setMode(currentKeyboard.getMode());
        requestKeyboard.registerKeyboardListener(currentKeyboard.getKeyboardListener());
        requestKeyboard.setLocation(currentKeyboard.getLocation().x, currentKeyboard.getLocation().y);
        if (currentKeyboard != null) {
            currentKeyboard.setVisible(false);
            currentKeyboard.stop();
        }
        currentKeyboard = requestKeyboard;
        currentKeyboard.prepare();
        currentKeyboard.start();
    }

    /**
     *  Is double check value.
     * @return isShow
     */
    public boolean isShow() {
        return isShow;
    }

    /**
     * Set double check value.
     * @param show is boolean value.
     */
    public void setShow(boolean show) {
        this.isShow = show;
    }
}

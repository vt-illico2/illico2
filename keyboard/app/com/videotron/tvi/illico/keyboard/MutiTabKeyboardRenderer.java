/**
 * @(#)MutiTabKeyboardRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class MutiTabKeyboardRenderer extends Renderer {

    /** The width of keys by one. */
    private final int KEY_WIDTH = 74;

    /** The height of keys by one. */
    private final int KEY_HEIGHT = 38;

    /** The bounds of MultiTabKeyboard. */
    private Rectangle bounds = new Rectangle(354, 231, 238, 293);

    /** The background Image of MultiTabKeyboard. */
    private Image bgImage = DataCenter.getInstance().getImage("04_t9_bg.png");

    /** The image of shadow. */
//    private Image shadowImage = DataCenter.getInstance().getImage("04_t9_sh.png");

    /** The image of focus. */
    private Image focusImage = DataCenter.getInstance().getImage("04_t9_foc.png");

    /** The coordinate of {@link #bgImage}. */
    private Point bgPoint = new Point(0, 0);

    /** The coordinate of {@link #shadowImage}. */
    private Point shadowPoint = new Point(354, 250);

    /** The coordinate of {@link #focusImage}. */
    private Point focusStartPoint = new Point(7, 52);

    /**
     * The start coordinate of number strings.
     * 
     * @see MultiTabKeyboard#getKeyboardNumberStrings()
     */
    private Point numberStartPoint = new Point(46, 71);

    /**
     * The start coordinate of sub strings.
     * 
     * @see MultiTabKeyboard#getKeyboardSubStrings()
     */
    private Point subStartPoint = new Point(46, 84);

    /** The color for sub strings. (ABC, DEF) */
    private Color c150 = new Color(150, 150, 150);

    /** The color for focused sub strings. (ABC, DEF) */
    private Color c76 = new Color(76, 76, 76);

    /** The font of size 15. */
    private Font font15 = FontResource.BLENDER.getFont(15);

    /** The font of size 17. */
    private Font font17 = FontResource.BLENDER.getFont(17);

    /** The font of size 19. */
    private Font font19 = FontResource.BLENDER.getFont(19);

    /** The fontMetrics for (@link font15}. */
    private FontMetrics fm15 = FontResource.BLENDER.getFontMetrics(font15);

    /** The FontMetrics for {@link font17}. */
    private FontMetrics fm17 = FontResource.BLENDER.getFontMetrics(font17);

    /** The FontMetrics for {@link font 19}. */
    private FontMetrics fm19 = FontResource.BLENDER.getFontMetrics(font19);

    /** The string array of each keys. */
    private String[][] keyboardNumberStrings;

    /** The string array of each keys's sub. */
    private String[][] keyboardSubStrings;

    /**
     * Instantiates a new Multi-Tab keyboard renderer.
     */
    public MutiTabKeyboardRenderer() {
    }

    /**
     * Returns a bounds of Multi-Tab keyboard renderer.
     * 
     * @param c
     *            The UIComponent of Multi-Tab keyboard.
     * @return a bound of Multi-Tab keyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.
     *      UIComponent)
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Paint a background and keys and focus for Multi-Tab keyboard.
     * 
     * @param g
     *            The graphics to paint.
     * @param c
     *            The UIComponent to implement MultiTabKeyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics, com.videotron.tvi.illico.framework.
     *      UIComponent)
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImage, bgPoint.x, bgPoint.y, c);
//        g.drawImage(shadowImage, shadowPoint.x, shadowPoint.y, c);

        for (int i = 0; i < keyboardNumberStrings.length; i++) {
            for (int j = 0; j < keyboardNumberStrings[i].length; j++) {
                int index = i * MultiTabKeyboard.AMOUNT_KEYS_BY_LINE + j;
                int stepX = j * KEY_WIDTH;
                int stepY = i * KEY_HEIGHT;

                if (index == c.getFocus()) {
                    g.drawImage(focusImage, focusStartPoint.x + stepX, focusStartPoint.y + stepY, c);
                    g.setColor(Color.BLACK);
                } else {
                    g.setColor(Color.WHITE);
                }
                if (index == MultiTabKeyboard.INDEX_KEYBOARD) {
                    g.setFont(font17);
                } else {
                    g.setFont(font19);
                }

                if (keyboardSubStrings[i][j] != null) {
                    GraphicUtil.drawStringCenter(g, keyboardNumberStrings[i][j], numberStartPoint.x + stepX,
                            numberStartPoint.y + stepY);
                    if (index == c.getFocus()) {
                        g.setColor(c76);
                    } else {
                        g.setColor(c150);
                    }
                    g.setFont(font15);
                    GraphicUtil.drawStringCenter(g, keyboardSubStrings[i][j], subStartPoint.x + stepX, subStartPoint.y
                            + stepY, fm15);
                } else {
                    GraphicUtil.drawStringCenter(g, keyboardNumberStrings[i][j], numberStartPoint.x + stepX,
                            numberStartPoint.y + stepY + 5);
                }
            }
        }
    }

    /**
     * Prepare a key strings to paint for Multi-Tab keyboard.
     * 
     * @param c
     *            The UIComponent to implement MultiTabKeyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
     */
    public void prepare(UIComponent c) {
        keyboardNumberStrings = ((MultiTabKeyboard) c).getKeyboardNumberStrings();
        keyboardSubStrings = ((MultiTabKeyboard) c).getKeyboardSubStrings();

        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
}

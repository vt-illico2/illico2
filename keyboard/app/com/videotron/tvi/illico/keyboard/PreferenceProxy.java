package com.videotron.tvi.illico.keyboard;

import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

/**
 * PreferenceProxy integrated with User Profile Application to apply the setting values of Search Application.
 * @author
 */
public class PreferenceProxy implements PreferenceListener {
    /** This class instance. */
    private static PreferenceProxy instance = new PreferenceProxy();
    /** XletContext. */
    private XletContext xletContext;
    /** PreferenceService instance. */
    private PreferenceService preferenceService;
    /** Key set. */
    public static String[] preferenceKeys;

    /** The thread sleep delay. */
    private static final int THREAD_DALAY = 1000;

    /**
     * Get instance.
     * @return PreferenceProxy
     */
    public static PreferenceProxy getInstance() {
        return instance;
    }

    /**
     * Initialize.
     * @param xlet XletContext
     */
    public void init(XletContext xlet) {
        this.xletContext = xlet;
        preferenceKeys = new String[]{PreferenceNames.LANGUAGE};
//        preferenceKeys = new String[]{PreferenceNames.VIRTUAL_KEYBOARD,
//                        PreferenceNames.KEYBOARD_LANGUAGE};
        lookupUPP();
    }

    /**
     * Lookup UPP.
     */
    private void lookupUPP() {
        try {
            new Thread() {
                public void run() {
                    String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
                    while (true) {
                        if (preferenceService == null) {
                            try {
                                preferenceService = (PreferenceService) IxcRegistry.lookup(xletContext, lookupName);
                            } catch (Exception e) {
                                Log.printInfo("NotBoundException - PreferenceService");
                            }
                        }
                        if (preferenceService != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("PreferenceProxy.lookUpService: preferenceService ");
                            }
                            addPreferenceListener();
                            break;
                        }
                        try {
                            Thread.sleep(THREAD_DALAY);
                        } catch (Exception e) {
                            Log.print(e);
                        }
                    }
                }
            } .start();
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * register listener to UPP.
     */

    private void addPreferenceListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, addPreferenceListener");
        }
        try {
            String[] values = preferenceService.addPreferenceListener(this, "Keyboard",
                    preferenceKeys, new String[]{""});

            if(values != null){
                for(int a = 0; a < values.length; a++){
                    Log.printInfo("values[" + a + "]" +values[a]);
//                    if(a == 0){
//                        Keyboard.setSettingKeyboardType(values[0]);
//                    } else if(a == 1){
                    DataCenter.getInstance().put(PreferenceNames.LANGUAGE, values[0]);
                    Keyboard.setLanguage(values[0]);
//                    }
                }
            }
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * values are received update Listener from UPP.
     */
    public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
        Log.printInfo("PreferenceProxy, addPreferenceListener");
        Log.printInfo("preferenceName = " + preferenceName + " value = " + value);

        if (PreferenceNames.LANGUAGE.equals(preferenceName)) {
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
        }
//        if(PreferenceNames.VIRTUAL_KEYBOARD.equals(preferenceName)){
//            Keyboard.setSettingKeyboardType(value);
//        } else if(PreferenceNames.KEYBOARD_LANGUAGE.equals(preferenceName)){
//            Keyboard.setLanguage(value);
//        }
    }
}

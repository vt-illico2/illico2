/**
 * @(#)VirtualKeyboard.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * The VirtualKeyboard implements a functions for a standard type keyboard.
 *
 * @author Woojung Kim
 * @version 1.1
 */
public class VirtualKeyboard extends Keyboard {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2585421177946119014L;

    /** The key width of VirtualKeyboard. */
    public static final int HORIZONTAL_WIDTH = 6;

    /** The total count of Keys to show. */
    private final int KEY_SPACE_TOTAL = 48;

    /** the initial focus when Keyboard start. */
    private final int INDEX_INITIAL = 20;

    /** The index of Space Key. */
    private final int INDEX_SPACE = 26;

    /** The index of Erase Key. */
    private final int INDEX_ERASE = 28;

    /** The index of Done Key. */
    private final int INDEX_DONE = 42;

    /** The index of Cancel Key. */
    private final int INDEX_CANCEL = 44;

    /** The index of Multi-Tap Key. */
    private final int INDEX_MULTI_TAP = 46;

    /** The char value for SPACE Key. */
    private final char CHAR_SPACE = ' ';

    /** The index of Last line on VirtualKeyboard. */
    private final int LAST_INDEX_VERTINAL = 7;

    /** The amount to move up a focus with correction. */
    private final int AMOUNT_MOVE_UP_WITH_CORRECTION = 5;

    /** The amount to move up a focus. */
    private final int AMOUNT_MOVE_UP = 6;

    /** The amount to move down focus with correction on last line. */
    private final int AMOUNT_MOVE_DOWN_CORRECTION_LAST_LINE = 8;

    /** The amount to move dowm focus on last line. */
    private final int AMOUNT_MOVE_DOWN_LAST_LINE = 7;

    /** The amount to move down focus with correction. */
    private final int AMOUNT_MOVE_DOWN_CORRECTION = 7;

    /** The amount to move down focus. */
    private final int AMOUNT_MOVE_DOWN = 6;

    /** The renderer to draw VirtualKeyboard. */
    private Renderer renderer = new VirtualKeyboardRenderer();

    /** The boolean value to need or not a correction for Key Navigation. */
    private boolean needCorrection;

    /** The string for double space keys (ex> Space, Cancel). */
    private String spaceString, eraseString, doneString, cancelString;

    /** The strings for keyboard. */
    private String[][] keyboardStrings;

    /**
     * Instantiates a new virtual keyboard.
     */
    public VirtualKeyboard() {
        Log.printInfo("VirtualKEyboard constructor");
        setRenderer(renderer);
    }

    /**
     * Call before showing VirutalKeyboard. prepare to the textField and initialize focus and needCorrection.
     *
     * @see com.videotron.tvi.illico.framework.UIComponent#start()
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("VirtualKeyboard: start()");
        }

        textField.setMutiTab(false);
        textField.start(true);
        add(textField);

        focus = INDEX_INITIAL;
        needCorrection = false;
        super.start(this);
    }

    /**
     * Call when Keyboard be closed. It make to stop the textField and remove the keyboardListeners.
     *
     * @see com.videotron.tvi.illico.framework.UIComponent#stop()
     */
    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("VirtualKeyboard: stop()");
        }
        textField.stop();
        remove(textField);
        keyboardListener = null;
        super.stop();
    }

    /**
     * Receive a key code and process a action for key code.
     *
     * @param code the key code from RCU.
     * @see com.videotron.tvi.illico.framework.UIComponent#handleKey(int)
     * @return whether key used in other application or not. True means the key is used only this application.
     */
    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) {
            Log.printDebug("VirtualKeyboard: handleKey(): key = " + code);
        }
        int focusHorizontal = 0;
        int focusVertical = 0;
        boolean isUsed = false;
        switch (code) {
        case OCRcEvent.VK_EXIT:
        case KeyCodes.LAST:
            notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
            isUsed = true;
            break;
        case KeyEvent.VK_LEFT:
            needCorrection = false;
            focus--;
            if (focus < 0) {
                focus = INDEX_MULTI_TAP;
            } else {
                focusHorizontal = focus % HORIZONTAL_WIDTH;
                focusVertical = focus / HORIZONTAL_WIDTH;
                if (keyboardStrings[focusVertical][focusHorizontal] == null) {
                    focus--;
                }
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_RIGHT:
            needCorrection = false;
            focus++;
            focusHorizontal = focus % HORIZONTAL_WIDTH;
            focusVertical = focus / HORIZONTAL_WIDTH;
            if (keyboardStrings[focusVertical][focusHorizontal] == null) {
                focus++;
            }
            Log.printDebug("focus : " + focus);
            if (focus >= KEY_SPACE_TOTAL) {
                focus = 0;
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_UP:
            focusVertical = focus / HORIZONTAL_WIDTH;
            if (focus == 0) {
                focus = INDEX_MULTI_TAP;
                needCorrection = true;
            } else if (focusVertical == 0) {
                if (focus % 2 == 0) {
                    needCorrection = true;
                }
                int partition = (focus - 1) / 2;
                if (partition == 0) {
                    focus = INDEX_DONE;
                } else if (partition == 1) {
                    focus = INDEX_CANCEL;
                } else if (partition == 2) {
                    focus = INDEX_MULTI_TAP;
                }

            } else {
                if (needCorrection) {
                    focus -= AMOUNT_MOVE_UP_WITH_CORRECTION;
                    needCorrection = false;
                } else {
                    focus -= AMOUNT_MOVE_UP;
                }
                focusHorizontal = focus % HORIZONTAL_WIDTH;
                focusVertical = focus / HORIZONTAL_WIDTH;
                if (keyboardStrings[focusVertical][focusHorizontal] == null) {
                    focus--;
                    needCorrection = true;
                }
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_DOWN:
            focusVertical = focus / HORIZONTAL_WIDTH;
            if (focusVertical == LAST_INDEX_VERTINAL) {
                if (needCorrection && focus == INDEX_MULTI_TAP) {
                    focus = 0;
                    needCorrection = false;
                } else if (needCorrection) {
                    focus += AMOUNT_MOVE_DOWN_CORRECTION_LAST_LINE;
                    needCorrection = false;
                } else {
                    focus += AMOUNT_MOVE_DOWN_LAST_LINE;
                }
            } else {
                if (needCorrection) {
                    focus += AMOUNT_MOVE_DOWN_CORRECTION;
                    needCorrection = false;
                } else {
                    focus += AMOUNT_MOVE_DOWN;
                    focusHorizontal = focus % HORIZONTAL_WIDTH;
                    focusVertical = focus / HORIZONTAL_WIDTH;
                    if (keyboardStrings[focusVertical][focusHorizontal] == null) {
                        focus--;
                        needCorrection = true;
                    }
                }
            }
            if (focus >= KEY_SPACE_TOTAL) {
                focus -= KEY_SPACE_TOTAL;
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_ENTER:
            isUsed = true;
            focusHorizontal = focus % HORIZONTAL_WIDTH;
            focusVertical = focus / HORIZONTAL_WIDTH;
            if (keyboardStrings[focusVertical][focusHorizontal].length() > 1) {
                if (focus == INDEX_SPACE) {
                    if (maxCharslen < textField.getText().length()) {
                        break;
                    }
                    textField.keyTyped(CHAR_SPACE);
                    repaint();
                } else if (focus == INDEX_ERASE) {
                    String text = textField.getText();
                    int pos = textField.getCursorPosition();
                    Log.printInfo("pos = " + pos);
                    if (pos > 0) {
                        StringBuffer sb = new StringBuffer(text);
                        text = sb.deleteCharAt(pos - 1).toString();
                        Log.printInfo("text = " + text);
                    }
                    textField.moveCursor(pos - 1);
                    if (text != null && text.length() >= 0) {
                        textField.setText(text);
                    }
                    repaint();
                } else if (focus == INDEX_DONE) {
                    notifyKeyboardEvent(EVENT_INPUT_ENDED, 0);
                } else if (focus == INDEX_CANCEL) {
                    notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
                } else if (focus == INDEX_MULTI_TAP) {
                    Controller.getInstance().moveToNextKeyboard(KeyboardOption.TYPE_MULTI_TAP);
                }
            } else {
                if (maxCharslen <= textField.getText().length()) {
                    break;
                }
                char c = keyboardStrings[focusVertical][focusHorizontal].toCharArray()[0];
                textField.keyTyped(c);
                repaint();
            }
            break;
        default:
            isUsed = false;
            break;
        }

        super.handleKey(code);
        return isUsed;
    }

    /**
     * Prepare a key names to need a translate and a key strings for VirualKeyboard. Wait for a images to finish
     * loading.
     */
    public void prepare() {
        spaceString = (String) DataCenter.getInstance().get("buttonNames.Space" + settingLanguage);
        eraseString = (String) DataCenter.getInstance().get("buttonNames.Erase" + settingLanguage);
        doneString = (String) DataCenter.getInstance().get("buttonNames.Done" + settingLanguage);
        cancelString = (String) DataCenter.getInstance().get("buttonNames.Cancel" + settingLanguage);
        keyboardStrings = new String[][] { { "A", "B", "C", "D", "E", "F" }, { "G", "H", "I", "J", "K", "L" },
                { "M", "N", "O", "P", "Q", "R" }, { "S", "T", "U", "V", "W", "X" },
                { "Y", "Z", spaceString, null, eraseString, null }, { "0", "1", "2", "3", "4", "5" },
                { "6", "7", "8", "9", "'", "." }, { doneString, null, cancelString, null, "Multi-Tap", null } };
        FrameworkMain.getInstance().getImagePool().waitForAll();
        super.prepare();
    }

    /**
     * Gets the keyboard strings.
     *
     * @return the keyboard strings of Virtual Keyboard.
     */
    public String[][] getKeyboardStrings() {
        return keyboardStrings;
    }
}

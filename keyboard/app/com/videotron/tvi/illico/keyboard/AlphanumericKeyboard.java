package com.videotron.tvi.illico.keyboard;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The AlphanumericKeyboard implements a functions for a standard type keyboard.
 * @author PellOs
 * @version 1.1
 */
public class AlphanumericKeyboard extends Keyboard {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7747765472989520829L;

    /** The mode of upper case. */
    // public static final int MODE_UPPERCASE = 0x0;

    /** The mode of lower case. */
    public static final int MODE_LOWERCASE = 0x0;

    /** The mode of accents case. */
    public static final int MODE_ACCENTS = 0x1;

    /** The width of keyboard by key. */
    public static final int KEY_WIDTH = 6;

    /** The height of keyboard line by key. */
    public static final int LAST_LINE_NUMBER = 7;

    /** The index of Space key. */
    public static final int INDEX_SPACE = 26;

    /** The index of Erase key. */
    public static final int INDEX_ERASE = 28;

    /** the initial focus when Keyboard start. */
    public static final int INDEX_INITIAL = 20;

    /** The index of UP key. */
    public static final int INDEX_UP = 38;

    /** The index of DOWN key. */
    public static final int INDEX_DOWN = 39;

    /** The index of LEFT key. */
    public static final int INDEX_LEFT = 40;

    /** The index of Right key. */
    public static final int INDEX_RIGHT = 41;

    /** The index of Done key. */
    public static final int INDEX_DONE = 44;

    /** The index of Cancel key. */
    public static final int INDEX_CANCEL = 46;

    /** The index of Done key. */
    public static final int ACCENTS_INDEX_DONE = 42;

    /** The index of Cancel key. */
    public static final int ACCENTS_INDEX_CANCEL = 45;

    /** The index of last horizontal first vertical. */
    public static final int INDEX_LAST_HORIZONTAL_FIRST_VERTICAL = 42;

    /** The index of .com key. */
    public static final int INDEX_DOT_COM = 20;

    /** The index of .net key. */
    public static final int INDEX_DOT_NET = 22;

    /** The index of .tv key. */
    public static final int INDEX_DOT_TV = 24;

    /** The last index of sixth line. */
    private static final int LAST_INDEX_EIGHTH_LINE = 47;

    /** The last index of keyboard. */
    private static final int LAST_INDEX = 47;

    /** The index of third key space. */
    // private static final int INDEX_THIRD_SPACE = 3;

    /** The index of U key. */
    private static final int INDEX_KEY_U = 20;

    /** The index of question mark key. */
    // private static final int INDEX_KEY_QUESTION = 14;
    /** Indicate focus out of keyboard. */
    public static final int INDEX_FOCUS_LOST = 48;

    /** The char value for SPACE Key. */
    private static final char CHAR_SPACE = ' ';

    /** The string array for {@link #MODE_UPPERCASE}. */
    private String[][] upperCaseKeyStrings;

    /** The string array for {@link #MODE_LOWERCASE}. */
    private String[][] lowerCaseKeyStrings;

    /** The string array for {@link #MODE_ACCENTS}. */
    private String[][] accentKeyStrings;

    /** The string for double space keys (ex> Space, Cancel). */
    private String spaceString;
    /** The string for double space keys (ex> Space, Cancel). */
    private String eraseString;
    /** The string for double space keys (ex> Space, Cancel). */
    private String doneString;
    /** The string for double space keys (ex> Space, Cancel). */
    private String cancelString;
    /** The string for double space keys (ex> Space, Cancel). */
    private String clearString;

    /** The string for change mode. */
    private String changeModeStrings;

    /** The renderer to paint VirtualKeyboard. */
    private Renderer renderer = new AlphanumericKeyboardRenderer();

    /** The current mode, a initial value is {@link #MODE_UPPERCASE}. */
    private int mode = MODE_LOWERCASE;

    /** The reminder index of last focus, the focus is single space. */
    private int reminderFocus;

    /** The focus index is history Focus. */
    private int historyFocus;

    /** store a last key code to clear reminderFocus. */
    private int beforeKeyEvent = 0;

    /** the keyboard strings for current mode. */
    private String[][] keyboardStrings;

    /** The cursor position in TextField for MODE_EMBEDDED. */
    private int cursorPosition;

    /** The value is a temporary text. */
    private boolean tempViewed;

    /** The text of showing in TextFiled for MODE_EMBEDDED. */
    private String text;

    /** The type of alphanumeric. */
    private int alphanumericType = 0;

    /**
     * Instantiates a new virtual Extended keyboard and set renderer to paint.
     */
    public AlphanumericKeyboard() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Call before showing VirutalKeyboard. prepare to the textField and initialize focus and needCorrection.
     * @see com.videotron.tvi.illico.framework.UIComponent#start()
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("VirtualExtendedKeyboard: start()");
        }

        super.start(this);

        if (alphanumericType == KeyboardOption.ALPHANUMERIC_ALL) {
            mode = MODE_LOWERCASE;
            keyboardStrings = lowerCaseKeyStrings;
        } else if (alphanumericType == KeyboardOption.ALPHANUMERIC_LARGE) {
            mode = MODE_LOWERCASE;
        } else if (alphanumericType == KeyboardOption.ALPHANUMERIC_SAMLL) {
            mode = MODE_LOWERCASE;
            keyboardStrings = lowerCaseKeyStrings;
        } else if (alphanumericType == KeyboardOption.ALPHANUMERIC_ACCENT) {
            mode = MODE_ACCENTS;
            keyboardStrings = accentKeyStrings;
        }

        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            textField.start(true);
            add(textField);
        } else {
            text = TextUtil.EMPTY_STRING;
        }
    }

    public void setMode(short mode) {
        super.setMode(mode);
        if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
            setSize(248, 335);
            clickEffect = null;
            clickEffect = new ClickingEffect(this, 5);
        }
    }

    /**
     * Call when Keyboard be closed. It make to stop the textField and remove the keyboardListener.
     * @see com.videotron.tvi.illico.framework.UIComponent#stop()
     */
    public void stop() {
        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            textField.stop();
            remove(textField);
        }
        keyboardListener = null;
        super.stop();
    }

    /**
     * Receive a key code and process a action for key code.
     * @param code the key code to select by user.
     * @see com.videotron.tvi.illico.framework.UIComponent#handleKey(int)
     * @return whether key used in other application or not. True means the key is used only this application.
     */
    public boolean handleKey(int code) {
        if (focus == INDEX_FOCUS_LOST && code != KeyCodes.COLOR_A) {
            if (Log.DEBUG_ON) {
                Log.printDebug("EmbeddedKeyboard: handleKey(): keyboard has not focus.");
            }
            return false;
        }
        int focusHorizontal = 0;
        int focusVertical = 0;
        boolean isUsed = false;
        switch (code) {
        case OCRcEvent.VK_EXIT:
            notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
            isUsed = true;
            break;
        case KeyEvent.VK_LEFT:
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                focus--;
                if (focus < 0 || focus % KEY_WIDTH == (KEY_WIDTH - 1)) {
                    focus = focus + KEY_WIDTH;
                }
                historyFocus = 0;
                focusHorizontal = focus % KEY_WIDTH;
                focusVertical = focus / KEY_WIDTH;

                while (keyboardStrings[focusVertical][focusHorizontal] == null) {
                    focus--;
                    if (focus % KEY_WIDTH == (KEY_WIDTH - 1)) {
                        focus = focus + KEY_WIDTH;
                    }
                    focusHorizontal = focus % KEY_WIDTH;
                    focusVertical = focus / KEY_WIDTH;
                }
                repaint();
                notifyKeyboardEvent(EVENT_CURSOR_MOVED, -1);
            } else {
                focus--;
                if (focus < 0 || focus % KEY_WIDTH == (KEY_WIDTH - 1)) {
                    focus = focus + KEY_WIDTH;
                }
                focusHorizontal = focus % KEY_WIDTH;
                focusVertical = focus / KEY_WIDTH;

                while (keyboardStrings[focusVertical][focusHorizontal] == null) {
                    focus--;
                    if (focus % KEY_WIDTH == (KEY_WIDTH - 1)) {
                        focus = focus + KEY_WIDTH;
                    }

                    focusHorizontal = focus % KEY_WIDTH;
                    focusVertical = focus / KEY_WIDTH;

                    Log.printDebug("focus1 = " + focus);
                    Log.printDebug("focusHorizontal1 = " + focusHorizontal);
                    Log.printDebug("focusVertical1 = " + focusVertical);
                }

                Log.printDebug("focus = " + focus);
                Log.printDebug("focusHorizontal = " + focusHorizontal);
                Log.printDebug("focusVertical = " + focusVertical);
            }
            reminderFocus = focus;
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_RIGHT:
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                focus++;
                historyFocus = 0;
                if (keyboardMode == KeyboardOption.MODE_EMBEDDED
                        && ((focus % KEY_WIDTH) == 0 || focus - 1 == INDEX_CANCEL || focus - 1 == INDEX_ERASE)) {
                    reminderFocus = focus - 1;
                    focus = INDEX_FOCUS_LOST;
                    repaint();
                    notifyKeyboardEvent(EVENT_FOCUS_OUT, KeyEvent.VK_RIGHT);
                    return true;
                }
                if (keyboardMode == KeyboardOption.MODE_POPUP && mode != MODE_ACCENTS && focus > INDEX_CANCEL) {
                    focus = 0;
                } else if (keyboardMode == KeyboardOption.MODE_POPUP && mode == MODE_ACCENTS
                        && focus > ACCENTS_INDEX_CANCEL) {
                    focus = 0;
                } else {
                    focusHorizontal = focus % KEY_WIDTH;
                    focusVertical = focus / KEY_WIDTH;

                    while (keyboardStrings[focusVertical][focusHorizontal] == null) {
                        focus++;
                        focusHorizontal = focus % KEY_WIDTH;
                        focusVertical = focus / KEY_WIDTH;
                    }
                    repaint();
                    if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                        repaint();
                        notifyKeyboardEvent(EVENT_CURSOR_MOVED, -1);
                    }
                }
            } else {
                focus++;
                if (focus % KEY_WIDTH == 0) {
                    focus = focus - KEY_WIDTH;
                }
                focusHorizontal = focus % KEY_WIDTH;
                focusVertical = focus / KEY_WIDTH;

                while (keyboardStrings[focusVertical][focusHorizontal] == null) {
                    focus++;
                    if (focus % KEY_WIDTH == 0) {
                        focus = focus - KEY_WIDTH;
                    }
                    focusHorizontal = focus % KEY_WIDTH;
                    focusVertical = focus / KEY_WIDTH;

                    Log.printDebug("focus1 = " + focus);
                    Log.printDebug("focusHorizontal1 = " + focusHorizontal);
                    Log.printDebug("focusVertical1 = " + focusVertical);
                }

                Log.printDebug("focus = " + focus);
                Log.printDebug("focusHorizontal = " + focusHorizontal);
                Log.printDebug("focusVertical = " + focusVertical);

            }
            reminderFocus = focus;
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_UP:
            keyUp();
            isUsed = true;
            break;
        case KeyEvent.VK_DOWN:
            keyDown();
            isUsed = true;
            break;
        case KeyEvent.VK_ENTER:
            keyOk();
            isUsed = true;
            repaint();
            break;
        case KeyCodes.COLOR_A:
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED || aKeyDeactive) {
                return true;
            }
            isUsed = true;

            if (alphanumericType == KeyboardOption.ALPHANUMERIC_ALL) {
                if (++mode > MODE_ACCENTS) {
                    mode = MODE_LOWERCASE;
                }

                if (mode == MODE_LOWERCASE) {
                    keyboardStrings = lowerCaseKeyStrings;
                    if (focus == ACCENTS_INDEX_CANCEL) {
                        focus = INDEX_CANCEL;
                    } else if (focus == ACCENTS_INDEX_DONE) {
                        focus = INDEX_DONE;
                    }

                } else if (mode == MODE_ACCENTS) {
                    keyboardStrings = accentKeyStrings;
                    if (focus == INDEX_CANCEL) {
                        focus = ACCENTS_INDEX_CANCEL;
                    } else if (focus == INDEX_DONE) {
                        focus = ACCENTS_INDEX_DONE;
                    } else if (focus == 42 || focus == 43) {
                        focus = INDEX_KEY_U;
                    }
                }

                if (focus != INDEX_FOCUS_LOST) {
                    reminderFocus = focus;
                }
                renderer.prepare(this);
            }
            repaint();
            notifyKeyboardEvent(EVENT_MODE_CHANGED, mode);
            break;
        case KeyCodes.COLOR_C:
            if (keyboardMode != KeyboardOption.MODE_EMBEDDED) {
                return true;
            }
            isUsed = true;
            erase(KeyCodes.COLOR_C);
            break;
        case KeyEvent.VK_0:
        case KeyEvent.VK_1:
        case KeyEvent.VK_2:
        case KeyEvent.VK_3:
        case KeyEvent.VK_4:
        case KeyEvent.VK_5:
        case KeyEvent.VK_6:
        case KeyEvent.VK_7:
        case KeyEvent.VK_8:
        case KeyEvent.VK_9:
            isUsed = true;
            int k = code - KeyEvent.VK_0;
            keyNumber(k);
            break;
        default:
            isUsed = false;
            break;
        }
        beforeKeyEvent = code;

        super.handleKey(code);
        return isUsed;
    }

    public void erase(int keyCode) {
        if (cursorPosition > 0) {
            StringBuffer sb = new StringBuffer(text);
            text = sb.deleteCharAt(cursorPosition - 1).toString();
        }
        if (text != null && text.length() == 0) {
            lowerCaseKeyStrings[7][4] = cancelString;
        }
        if (keyCode == KeyEvent.VK_ENTER) {
            clickEffect.start(7 + 4 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                    76 + 4 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
        }
        moveCursor(cursorPosition - 1);
        repaint();
        notifyKeyboardEvent(EVENT_TEXT_CHANGED, 0);
        notifyKeyboardEvent(EVENT_CURSOR_MOVED, cursorPosition);
    }
    /**
     * set key Number.
     * @param k is number.
     */
    private void keyNumber(int k) {

        if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
            if (!tempViewed && maxCharslen <= text.length()) {
                return;
            }
            char[] keyChars = String.valueOf(k).toCharArray();
            for (int i = 0; i < keyChars.length; i++) {
                keyTyped(keyChars[i]);
            }
            if (text != null && text.length() > 0) {
                lowerCaseKeyStrings[7][4] = clearString;
            } else {
                lowerCaseKeyStrings[7][4] = cancelString;
            }
            repaint();
            notifyKeyboardEvent(EVENT_TEXT_CHANGED, 0);
            notifyKeyboardEvent(EVENT_CURSOR_MOVED, cursorPosition);
        } else {
            if (!tempViewed && maxCharslen <= textField.getText().length()) {
                return;
            }
            tempViewed = false;
            char[] keyChars = String.valueOf(k).toCharArray();
            for (int i = 0; i < keyChars.length; i++) {
                textField.keyTyped(keyChars[i]);
            }
            if (textField.getText().length() > 0) {
                lowerCaseKeyStrings[7][4] = clearString;
            } else {
                lowerCaseKeyStrings[7][4] = cancelString;
            }
            repaint();
            notifyKeyboardEvent(EVENT_TEXT_CHANGED, 0);
            notifyKeyboardEvent(EVENT_CURSOR_MOVED, textField.getCursorPosition());
        }
        repaint();
    }

    /**
     * Process a index of focus to move a next focus when VK_UP pressed.
     */
    private void keyUp() {

        if (beforeKeyEvent != KeyEvent.VK_UP) {
            reminderFocus = focus;
        }

        int focusHorizontal = 0;
        int focusVertical = 0;

        focusVertical = focus / KEY_WIDTH;
        focusHorizontal = focus % KEY_WIDTH;

        if (focus >= 0 && focus < 6) {
            historyFocus = focus;
            if (focus == 0) {
                focus = 42;
            } else if (focus == 1) {
                focus = mode != MODE_ACCENTS ? 43 : 42;
            } else if (focus == 2) {
                focus = mode != MODE_ACCENTS ? 44 : 42;
            } else if (focus == 3) {
                focus = mode != MODE_ACCENTS ? 44 : 45;
            } else if (focus == 4) {
                focus = mode != MODE_ACCENTS ? 46 : 45;
            } else if (focus == 5) {
                focus = mode != MODE_ACCENTS ? 46 : 45;
            }
            reminderFocus = focus;
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                focus = INDEX_FOCUS_LOST;
                historyFocus = 0;
                repaint();
                notifyKeyboardEvent(EVENT_FOCUS_OUT, KeyEvent.VK_UP);
                return;
            }
            focusVertical = focus / KEY_WIDTH;
            focusHorizontal = focus % KEY_WIDTH;
            Log.printDebug("focus = " + focus);
            Log.printDebug("focusHorizontal = " + focusHorizontal);
            Log.printDebug("focusVertical = " + focusVertical);
        } else {
            if (focusVertical == 0) {
                if (mode != MODE_ACCENTS) {

                    int partition = (focus - 1) / 2;
                    if (partition == 1) {
                        if (reminderFocus == 4) {
                            historyFocus = INDEX_DONE + 1;
                        }
                        focus = INDEX_DONE;
                    } else if (partition == 2) {
                        focus = INDEX_CANCEL;
                    } else {
                        if (focus == 0) {
                            focus = INDEX_CANCEL;
                            historyFocus = INDEX_CANCEL + 1;
                        } else if (focus == 1) {
                            focus = INDEX_LAST_HORIZONTAL_FIRST_VERTICAL;
                            historyFocus = 0;
                        } else if (focus == 2) {
                            focus = INDEX_LAST_HORIZONTAL_FIRST_VERTICAL + 1;
                            historyFocus = 0;
                        }
                    }
                } else {
                    if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                        reminderFocus = focus;
                        focus = INDEX_FOCUS_LOST;
                        historyFocus = 0;
                        repaint();
                        notifyKeyboardEvent(EVENT_FOCUS_OUT, KeyEvent.VK_UP);
                        return;
                    }
                    if (focus == 0 || focus == 5 || focus == 4) {
                        if (focus == 5) {
                            historyFocus = ACCENTS_INDEX_CANCEL + 1;
                        } else if (focus == 4) {
                            historyFocus = ACCENTS_INDEX_CANCEL;
                        } else if (focus == 0) {
                            historyFocus = ACCENTS_INDEX_CANCEL + 2;
                        }

                        focus = ACCENTS_INDEX_CANCEL;
                    } else {
                        if (focus == 3) {
                            historyFocus = ACCENTS_INDEX_DONE + 2;
                        } else if (focus == 2) {
                            historyFocus = ACCENTS_INDEX_DONE + 1;
                        }

                        focus = ACCENTS_INDEX_DONE;
                    }
                }
                reminderFocus = focus;
            } else {
                if (reminderFocus < KEY_WIDTH) {
                    int beforeHorizontal = reminderFocus % KEY_WIDTH;
                    focus = LAST_INDEX - (KEY_WIDTH - beforeHorizontal);
                } else {
                    if (keyboardMode == KeyboardOption.MODE_POPUP && mode != MODE_ACCENTS
                            && (historyFocus == 3 || historyFocus == 5)) {
                        focus = focus - KEY_WIDTH + 1;
                    } else if (keyboardMode == KeyboardOption.MODE_POPUP && mode == MODE_ACCENTS
                            && (historyFocus == 1 || historyFocus == 2 || historyFocus == 4 || historyFocus == 5)) {
                        if (historyFocus == 1 || historyFocus == 4) {
                            focus = focus - KEY_WIDTH + 1;
                        } else {
                            focus = focus - KEY_WIDTH + 2;
                        }
                    } else {
                        focus = focus - KEY_WIDTH;
                    }
                }

                focusVertical = focus / KEY_WIDTH;
                focusHorizontal = focus % KEY_WIDTH;

                if (mode != MODE_ACCENTS
                        && (historyFocus == (INDEX_SPACE + 1) || historyFocus == (INDEX_ERASE + 1)
                                || historyFocus == (LAST_INDEX_EIGHTH_LINE - KEY_WIDTH)
                                || historyFocus == (INDEX_DONE + 1) || historyFocus == (INDEX_CANCEL + 1))) {
                    focus = historyFocus - KEY_WIDTH;
                    Log.printDebug("focus = " + focus);
                    Log.printDebug("historyFocus = " + historyFocus);
                    historyFocus = 0;
                } else if (mode == MODE_ACCENTS
                        && (historyFocus == (INDEX_SPACE + 1) || historyFocus == (INDEX_ERASE + 1)
                                || historyFocus == (LAST_INDEX_EIGHTH_LINE - KEY_WIDTH)
                                || historyFocus == (ACCENTS_INDEX_DONE + 1) || historyFocus == (ACCENTS_INDEX_DONE + 2)
                                || historyFocus == (ACCENTS_INDEX_CANCEL + 1) || historyFocus == (ACCENTS_INDEX_CANCEL + 2))) {
                    focus = historyFocus - KEY_WIDTH;
                    historyFocus = 0;
                } else if (keyboardStrings[focusVertical][focusHorizontal] == null) {
                    historyFocus = focus;
                    focus = focus - 1;
                } else {
                    historyFocus = 0;
                }
                reminderFocus = focus;
                if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                    repaint();
                    notifyKeyboardEvent(EVENT_CURSOR_MOVED, -1);
                }
            }
        }
        repaint();
    }

    /**
     * Process a index of focus to move a next focus when VK_DOWN pressed.
     */
    public void keyDown() {
        if (beforeKeyEvent != KeyEvent.VK_DOWN) {
            reminderFocus = focus;
        }

        int focusHorizontal = 0;
        int focusVertical = 0;

        focusVertical = focus / KEY_WIDTH;
        focusHorizontal = focus % KEY_WIDTH;

        if (focusVertical == LAST_LINE_NUMBER) {
            int beforeHorizontal = reminderFocus % KEY_WIDTH;
            focus = beforeHorizontal;
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                reminderFocus = focus;
                historyFocus = 0;
                focus = INDEX_FOCUS_LOST;
                repaint();
                notifyKeyboardEvent(EVENT_FOCUS_OUT, KeyEvent.VK_DOWN);
                return;
            }
        } else {
            if (mode != MODE_ACCENTS) {
                if (focus == (INDEX_SPACE + 1 - KEY_WIDTH)) {
                    historyFocus = INDEX_SPACE + 1;
                } else if (focus == (INDEX_ERASE + 1 - KEY_WIDTH)) {
                    historyFocus = INDEX_ERASE + 1;
                } else if (focus == (INDEX_CANCEL + 1 - KEY_WIDTH)) {
                    historyFocus = INDEX_CANCEL + 1;
                } else if (focus == (INDEX_DONE + 1 - KEY_WIDTH)) {
                    historyFocus = INDEX_DONE + 1;
                } else {
                    historyFocus = 0;
                }
            } else if (mode == MODE_ACCENTS) {
                if (focus == (INDEX_SPACE + 1 - KEY_WIDTH)) {
                    historyFocus = INDEX_SPACE + 1;
                } else if (focus == (INDEX_ERASE + 1 - KEY_WIDTH)) {
                    historyFocus = INDEX_ERASE + 1;
                } else if (focus == (ACCENTS_INDEX_CANCEL + 2 - KEY_WIDTH)) {
                    historyFocus = ACCENTS_INDEX_CANCEL + 2;
                } else if (focus == (ACCENTS_INDEX_CANCEL + 1 - KEY_WIDTH)) {
                    historyFocus = ACCENTS_INDEX_CANCEL + 1;
                } else if (focus == (ACCENTS_INDEX_DONE + 2 - KEY_WIDTH)) {
                    historyFocus = ACCENTS_INDEX_DONE + 2;
                } else if (focus == (ACCENTS_INDEX_DONE + 1 - KEY_WIDTH)) {
                    historyFocus = ACCENTS_INDEX_DONE + 1;
                } else {
                    historyFocus = 0;
                }
            }

            int tempFocus = 0;
            int beforeHorizontal = reminderFocus % KEY_WIDTH;
            if (focusVertical > 0 && beforeHorizontal > 0 && beforeHorizontal != focusHorizontal) {
                tempFocus = focus + Math.abs(focusHorizontal - beforeHorizontal);
            } else {
                tempFocus = focus;
            }
            tempFocus += KEY_WIDTH;
            focusVertical = tempFocus / KEY_WIDTH;
            focusHorizontal = tempFocus % KEY_WIDTH;

            if (focusHorizontal > 1 && keyboardStrings[focusVertical][focusHorizontal] == null
                    && keyboardStrings[focusVertical][focusHorizontal - 1] == null
                    && keyboardStrings[focusVertical][focusHorizontal - 2] == null) {

                focus = findNextKey(tempFocus);
            } else if (keyboardStrings[focusVertical][focusHorizontal] != null) {
                focus = tempFocus;
                reminderFocus = focus;
            } else if (((focusVertical > 2 && focusHorizontal > 1) || (focusVertical > 3 && focusHorizontal > 0))
                    && keyboardStrings[focusVertical][focusHorizontal - 1] != null) {
                focus = tempFocus - 1;
            } else if (focusVertical > 4 && focusHorizontal > 1
                    && keyboardStrings[focusVertical][focusHorizontal - 2] != null) {
                focus = tempFocus - 2;
            } else if (focusHorizontal > 0 && keyboardStrings[focusVertical][focusHorizontal] == null
                    && keyboardStrings[focusVertical][focusHorizontal - 1] == null) {
                focus = findNextKey(tempFocus);
            } else if (focusVertical > 2 && focusHorizontal == 0
                    && keyboardStrings[focusVertical][focusHorizontal] == null) {
                focus = findNextKey(tempFocus);
            } else if (keyboardStrings[focusVertical][focusHorizontal] == null) {
                while (keyboardStrings[focusVertical][focusHorizontal] == null) {
                    tempFocus += KEY_WIDTH;
                    focusHorizontal = tempFocus % KEY_WIDTH;
                    focusVertical = tempFocus / KEY_WIDTH;
                }
                focus = tempFocus;
            } else {
                focus = tempFocus;
                reminderFocus = focus;
            }
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                repaint();
                notifyKeyboardEvent(EVENT_CURSOR_MOVED, -1);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("focus : " + focus);
                Log.printDebug("reminderFocus : " + reminderFocus);
                Log.printDebug("historyFocus : " + historyFocus);
            }
        }
        repaint();
    }

    /**
     * clear inputText Field.
     */
    private void clear() {
        text = TextUtil.EMPTY_STRING;
        moveCursor(0);
        repaint();
        notifyKeyboardEvent(EVENT_TEXT_CHANGED, 0);
        notifyKeyboardEvent(EVENT_CURSOR_MOVED, cursorPosition);
    }

    /**
     * Implements a functions to run when VK_ENTER pressed.
     */
    private void keyOk() {
        Log.printInfo("june - focus = " + focus);
        Log.printInfo("june - keyboardMode = " + keyboardMode);
        Log.printInfo("june - mode = " + mode);

        if (focus == INDEX_ERASE) {
            if (keyboardMode == KeyboardOption.MODE_POPUP) {
                String txt = textField.getText();
                int pos = textField.getCursorPosition();
                if (pos > 0) {
                    StringBuffer sb = new StringBuffer(txt);
                    txt = sb.deleteCharAt(pos - 1).toString();
                }
                textField.moveCursor(pos - 1);
                if (txt != null && txt.length() >= 0) {
                    textField.setText(txt);
                }
                if (textField.getText().length() == 0) {
                    lowerCaseKeyStrings[7][4] = cancelString;
                }
                clickEffect.start(7 + 4 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                        76 + 4 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
                repaint();
                notifyKeyboardEvent(EVENT_TEXT_CHANGED, 0);
                notifyKeyboardEvent(EVENT_CURSOR_MOVED, textField.getCursorPosition());
            } else {
                erase(KeyEvent.VK_ENTER);
            }
        } else if (mode != MODE_ACCENTS && focus == INDEX_DONE) {
            clickEffect.start(7 + 2 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                    76 + 7 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
            repaint();
            notifyKeyboardEvent(EVENT_INPUT_ENDED, 0);
        } else if (mode != MODE_ACCENTS && focus == INDEX_CANCEL) {
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                if (text != null && text.length() > 0) {
                    clear();
                    clickEffect.start(7 + 4 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                            76 + 7 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
                    lowerCaseKeyStrings[7][4] = cancelString;
                    repaint();
                    notifyKeyboardEvent(EVENT_INPUT_CLEARED, 0);
                } else {
                    clickEffect.start(7 + 4 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                            76 + 7 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
                    repaint();
                    notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
                }
            } else { // POPUP type
                clickEffect.start(7 + 4 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                        76 + 7 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
                if (textField.getText().length() > 0) {
                    textField.moveCursor(0);
                    textField.setText(TextUtil.EMPTY_STRING);
                    lowerCaseKeyStrings[7][4] = cancelString;
                } else {
                    repaint();
                    notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
                }
            }
        } else if (mode == MODE_ACCENTS && focus == ACCENTS_INDEX_DONE) {
            clickEffect.start(7 + 0 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                    76 + 7 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 123, 32);
            repaint();
            notifyKeyboardEvent(EVENT_INPUT_ENDED, 0);
        } else if (mode == MODE_ACCENTS && focus == ACCENTS_INDEX_CANCEL) {
            clickEffect.start(7 + 3 * AlphanumericKeyboardRenderer.HORIZONTAL_STEP,
                    76 + 7 * AlphanumericKeyboardRenderer.VERTICAL_STEP, 123, 32);
            repaint();
            notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
        } else if (mode == MODE_ACCENTS && focus == INDEX_UP) {

        } else if (mode == MODE_ACCENTS && focus == INDEX_DOWN) {

        } else if (mode == MODE_ACCENTS && focus == INDEX_LEFT) {
            Log.printInfo("pellos Left focus");
            if (keyboardMode == KeyboardOption.MODE_POPUP) {
                int focusHorizontal = focus % KEY_WIDTH;
                int focusVertical = focus / KEY_WIDTH;
                clickEffect.start(7 + focusHorizontal * AlphanumericKeyboardRenderer.HORIZONTAL_STEP, 76
                        + focusVertical * AlphanumericKeyboardRenderer.VERTICAL_STEP, 41, 32);
                textField.moveCursor(textField.getCursorPosition() - 1);
                repaint();
            } else {
                moveCursor(cursorPosition - 1);
                repaint();
                notifyKeyboardEvent(EVENT_CURSOR_MOVED, cursorPosition);
            }
            // notifyKeyboardEvent(EVENT_CURSOR_MOVED, textField.getCursorPosition());
        } else if (mode == MODE_ACCENTS && focus == INDEX_RIGHT) {
            if (keyboardMode == KeyboardOption.MODE_POPUP) {
                int focusHorizontal = focus % KEY_WIDTH;
                int focusVertical = focus / KEY_WIDTH;
                clickEffect.start(7 + focusHorizontal * AlphanumericKeyboardRenderer.HORIZONTAL_STEP, 76
                        + focusVertical * AlphanumericKeyboardRenderer.VERTICAL_STEP, 41, 32);
                textField.moveCursor(textField.getCursorPosition() + 1);
            } else {
                moveCursor(cursorPosition + 1);
                repaint();
                notifyKeyboardEvent(EVENT_CURSOR_MOVED, cursorPosition);
            }
            // notifyKeyboardEvent(EVENT_CURSOR_MOVED, textField.getCursorPosition());
        } else {
            int focusHorizontal = focus % KEY_WIDTH;
            int focusVertical = focus / KEY_WIDTH;
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {
                if (!tempViewed && maxCharslen <= text.length()) {
                    return;
                }
                if (focus == INDEX_SPACE) {
                    keyTyped(CHAR_SPACE);
                    clickEffect.start(7 + focusHorizontal * AlphanumericKeyboardRenderer.HORIZONTAL_STEP, 76
                            + focusVertical * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
                } else {
                    char[] keyChars = keyboardStrings[focusVertical][focusHorizontal].toCharArray();
                    for (int i = 0; i < keyChars.length; i++) {
                        keyTyped(keyChars[i]);
                    }
                    clickEffect.start(7 + focusHorizontal * AlphanumericKeyboardRenderer.HORIZONTAL_STEP, 76
                            + focusVertical * AlphanumericKeyboardRenderer.VERTICAL_STEP, 41, 32);
                }
                if (text != null && text.length() > 0) {
                    lowerCaseKeyStrings[7][4] = clearString;
                } else {
                    lowerCaseKeyStrings[7][4] = cancelString;
                }
                repaint();
                notifyKeyboardEvent(EVENT_TEXT_CHANGED, 0);
                notifyKeyboardEvent(EVENT_CURSOR_MOVED, cursorPosition);
            } else {
                if (!tempViewed && maxCharslen <= textField.getText().length()) {
                    return;
                }
                tempViewed = false;
                if (focus == INDEX_SPACE) {
                    textField.keyTyped(CHAR_SPACE);
                    clickEffect.start(7 + focusHorizontal * AlphanumericKeyboardRenderer.HORIZONTAL_STEP, 76
                            + focusVertical * AlphanumericKeyboardRenderer.VERTICAL_STEP, 81, 32);
                } else {
                    char[] keyChars = keyboardStrings[focusVertical][focusHorizontal].toCharArray();
                    for (int i = 0; i < keyChars.length; i++) {
                        textField.keyTyped(keyChars[i]);
                    }
                    clickEffect.start(7 + focusHorizontal * AlphanumericKeyboardRenderer.HORIZONTAL_STEP, 76
                            + focusVertical * AlphanumericKeyboardRenderer.VERTICAL_STEP, 41, 32);
                }
                if (textField.getText().length() > 0) {
                    lowerCaseKeyStrings[7][4] = clearString;
                } else {
                    lowerCaseKeyStrings[7][4] = cancelString;
                }
                repaint();
                notifyKeyboardEvent(EVENT_TEXT_CHANGED, 0);
                notifyKeyboardEvent(EVENT_CURSOR_MOVED, textField.getCursorPosition());
            }
        }
        repaint();
    }

    /**
     * Receive a char from Keyboard and append a char to {@link #text} call
     * {@link TextFieldRenderer#prepare(UIComponent)} to prepare.
     * @param c the char to pass via Keyboard.
     */
    public void keyTyped(char c) {
        if (tempViewed) {
            tempViewed = false;
            text = "";
            cursorPosition = 0;
        }
        StringBuffer sf = new StringBuffer(text);
        sf.insert(cursorPosition, c);
        text = sf.toString();
        cursorPosition++;
    }

    /**
     * Sets the text into text field.
     * @param s the new text which want to set.
     */
    public void setText(String s) {
        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            super.setText(s);
        } else {
            text = s;
        }
    }

    /**
     * Notify keyboard event to caller application via KeyboardListener.
     * @param event the event type (EVENT_INPUT_ENDED, EVENT_INPUT_CANCELED, EVENT_CURSOR_MOVED, EVENT_TEXT_CHANGED)
     * @param position In the case of EVENT_CURSOR_MOVED,
     */
    protected void notifyKeyboardEvent(int event, int position) {
        try {
            if (keyboardMode == KeyboardOption.MODE_EMBEDDED) {

                if (event == EVENT_TEXT_CHANGED) {
                    keyboardListener.textChanged(text);
                } else if (event == EVENT_INPUT_ENDED) {
                    focus = INDEX_FOCUS_LOST;
                    keyboardListener.inputEnded(text);
                } else if (event == EVENT_INPUT_CANCELED) {
                    keyboardListener.inputCanceled(text);
                } else if (event == EVENT_INPUT_CLEARED) {
                    keyboardListener.inputCleared();
                } else {
                    super.notifyKeyboardEvent(event, position);
                }
            } else {
                super.notifyKeyboardEvent(event, position);
            }

        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * Finds a next index from current focus.
     * @param focus current focus.
     * @return the focus index to find.
     */
    private int findNextKey(int focus) {
        int focusHorizontal = focus % KEY_WIDTH;
        int focusVertical = focus / KEY_WIDTH;
        while (keyboardStrings[focusVertical][focusHorizontal] == null) {
            focus += KEY_WIDTH;
            focusHorizontal = focus % KEY_WIDTH;
            focusVertical = focus / KEY_WIDTH;
            if (focusHorizontal > 0 && keyboardStrings[focusVertical][focusHorizontal - 1] != null) {
                focus--;
                break;
            }
        }
        return focus;
    }

    /**
     * Prepare a key names to need a translate and a key strings for VirtualExtendedKeyboard. Wait for a images to
     * finish loading.
     * @see com.videotron.tvi.illico.framework.UIComponent#prepare()
     */
    public void prepare() {
        if (Log.DEBUG_ON) {
            Log.printDebug("VirtualRxtendedKeyboard: prepare()");
        }
        changeModeStrings = DataCenter.getInstance().getString("buttonNames.Change");
        spaceString = DataCenter.getInstance().getString("buttonNames.Space");
        eraseString = DataCenter.getInstance().getString("buttonNames.Erase");
        doneString = DataCenter.getInstance().getString("buttonNames.Done");
        cancelString = DataCenter.getInstance().getString("buttonNames.Cancel");
        clearString = DataCenter.getInstance().getString("buttonNames.Clear");

        // String temp = (String) DataCenter.getInstance().get("buttonNames.ChangeMode" + settingLanguage);
        // changeModeStrings = TextUtil.tokenize(temp, '^');
        upperCaseKeyStrings = null;
        upperCaseKeyStrings = new String[][]{{"A", "B", "C", "D", "E", "F"}, {"G", "H", "I", "J", "K", "L"},
                {"M", "N", "O", "P", "Q", "R"}, {"S", "T", "U", "V", "W", "X"},
                {"Y", "Z", spaceString, null, eraseString, null}, {".", "1", "2", "3", "4", "5"},
                {"'", "6", "7", "8", "9", "0"}, {"-", "@", doneString, null, cancelString, null}

        };
        lowerCaseKeyStrings = null;
        lowerCaseKeyStrings = new String[][]{{"a", "b", "c", "d", "e", "f"}, {"g", "h", "i", "j", "k", "l"},
                {"m", "n", "o", "p", "q", "r"}, {"s", "t", "u", "v", "w", "x"},
                {"y", "z", spaceString, null, eraseString, null}, {".", "1", "2", "3", "4", "5"},
                {"'", "6", "7", "8", "9", "0"}, {"-", "@", doneString, null, cancelString, null}};
        accentKeyStrings = null;
        accentKeyStrings = new String[][]{{"à", "ä", "é", "è", "ê", "ë"}, {"ô", "î", "ï", "ù", "ç", "."},
                {"'", "?", "!", ":", ";", "\""}, {"@", "(", ")", "/", "-", "_"},
                {".com", ".ca", spaceString, null, eraseString, null}, {".net", ".tv", "&", "+", "=", "$"},
                {"%", "*", "UP", "DOWN", "LEFT", "RIGHT"}, {doneString, null, null, cancelString, null, null}};

        keyboardStrings = lowerCaseKeyStrings;

        if (alphanumericType == KeyboardOption.ALPHANUMERIC_ALL) {
            mode = MODE_LOWERCASE;
            keyboardStrings = lowerCaseKeyStrings;
        } else if (alphanumericType == KeyboardOption.ALPHANUMERIC_LARGE) {
            mode = MODE_LOWERCASE;
        } else if (alphanumericType == KeyboardOption.ALPHANUMERIC_SAMLL) {
            mode = MODE_LOWERCASE;
            keyboardStrings = lowerCaseKeyStrings;
        } else if (alphanumericType == KeyboardOption.ALPHANUMERIC_ACCENT) {
            mode = MODE_ACCENTS;
            keyboardStrings = accentKeyStrings;
        }

        if (keyboardMode == KeyboardOption.MODE_POPUP) {
            if (textField != null && textField.getText().length() > 0) {
                lowerCaseKeyStrings[7][4] = clearString;
            } else {
                lowerCaseKeyStrings[7][4] = cancelString;
            }
        } else {
            if (text != null && text.length() > 0) {
                lowerCaseKeyStrings[7][4] = clearString;
            } else {
                lowerCaseKeyStrings[7][4] = cancelString;
            }
        }

        // focus = INDEX_KEY_U;
        focusInit();
        FrameworkMain.getInstance().getImagePool().waitForAll();
        super.prepare();
    }

    /**
     * Returns a current key strings.
     * @return the string array of current keyboard.
     */
    public String[][] getCurrentKeyStrings() {
        return keyboardStrings;
    }

    /**
     * Return a change mode string.
     * @return the string array of change mode.
     */
    public String getChangeModeStrings() {
        return changeModeStrings;
    }

    /**
     * Return a current mode.
     * @see {@link #MODE_UPPERCASE}, {@link #MODE_LOWERCASE}, {@link #MODE_ACCENTS}, {@link #MODE_NUMBERS}
     * @return the current mode.
     */
    public int getCurrentMode() {
        return mode;
    }

    /**
     * Sets temp viewed.
     * @param view is temp text.
     */
    public void setTempViewText(boolean view) {
        tempViewed = view;
        super.setTempViewText(tempViewed);
    }

    /**
     * Set focus to previous focus.
     */
    public void setPreviousFocus() {
        focus = reminderFocus;
        repaint();
    }

    public void setPreviousFocus(int direction) {
        if (direction == KeyEvent.VK_DOWN) {
            int hor = reminderFocus % KEY_WIDTH;
            reminderFocus = focus;
            focus = hor;
        } else {
            focus = reminderFocus;
            if (focus >= 0 && focus < 6) {
                historyFocus = focus;
                if (focus == 0) {
                    focus = 42;
                } else if (focus == 1) {
                    focus = mode != MODE_ACCENTS ? 43 : 42;
                } else if (focus == 2) {
                    focus = mode != MODE_ACCENTS ? 44 : 42;
                } else if (focus == 3) {
                    focus = mode != MODE_ACCENTS ? 44 : 45;
                } else if (focus == 4) {
                    focus = mode != MODE_ACCENTS ? 46 : 45;
                } else if (focus == 5) {
                    focus = mode != MODE_ACCENTS ? 46 : 45;
                }
                reminderFocus = focus;
            }
        }
    }

    /**
     * Set alphanumeric Type.
     * @param t is type.
     */
    public void setAlphanumericType(int t) {
        alphanumericType = t;
        Log.printDebug("alphanumericType = " + alphanumericType);
    }

    /**
     * Get alphanumeric Type.
     * @return alphanumericType
     */
    public int getAlphanumericType() {
        return alphanumericType;
    }

    /**
     * set reset focus.
     */
    public void focusInit() {
        focus = INDEX_INITIAL;
        reminderFocus = focus;
        historyFocus = 0;
    }

    /**
     * Move the cursor position when mode is embedded.
     * @param pos the position of cursor
     */
    public void moveCursor(int pos) {
        if (pos > -1 && text.length() >= pos) {
            cursorPosition = pos;
            // renderer.prepare(this);
        }
    }

    /**
     * Returns temp viewed.
     * @return When keyboard launched at first time, default text is viewed or not.
     */
    public boolean getTempViewText() {
        return tempViewed;
    }
}

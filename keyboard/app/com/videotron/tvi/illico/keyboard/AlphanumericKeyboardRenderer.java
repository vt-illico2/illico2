package com.videotron.tvi.illico.keyboard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
/**
 * The AlphanumericKeyboardRenderer implements a functions for a standard type keyboard.
 * @author PellOs
 * @version 1.1
 */
public class AlphanumericKeyboardRenderer extends Renderer {

    /** The y coordinate offset for first change mode string. */
//    private static final int OFFSET_Y_FIRST_CHANGE_MODE_KEY = 5;

    /** The y coordinate offset for second change mode string. */
//    private static final int OFFSET_Y_SECOND_CHANGE_MODE_KEY = 8;

    /** The x coordinate offset for key has double space. (Space, Erase, etc) */
    private static final int OFFSET_X_DOUBLE_KEYS = 20;

    /** The x coordination offset for key has triple space. (Done, Cancel) */
    private static final int OFFSET_X_TRIPLE_KEYS = 40;

    /** the bounds of VirtualExtenedKeyboard. */
    private Rectangle bounds = new Rectangle(354, 151, 257, 377);

    /** The background Image. */
    private Image bgImage;

    /** The shadow images. */
//    private Image shadowImage;

    /** The background image for key line type 1. */
    private Image keyBg1Image;

    /** The background image for key line type 2. */
    private Image keyBg2Image;

    /** The background image for key line type 3. */
    private Image keyBg3Image;

    /** The background image for key line type 4. */
    private Image keyBg4Image;

    /** The background image for key line type 5. */
    private Image keyBg5Image;

    /** The focused image of arrow up. */
//    private Image arrowUpFocusedImage = DataCenter.getInstance().getImage("04_key_t_foc.png");

    /** The focused image of arrow down. */
//    private Image arrowDownFocusedImage = DataCenter.getInstance().getImage("04_key_b_foc.png");

    /** The focused image of arrow right. */
    private Image arrowRightFocusedImage;

    /** The focused Image of arrow left. */
    private Image arrowLeftFocusedImage;

    /** The image of arrow up. */
    private Image arrowUpImageDim;

    /** The image of arrow down. */
    private Image arrowDownImageDim;

    /** The image of arrow right. */
    private Image arrowRightImage;

    /** The image of arrow left. */
    private Image arrowLeftImage;

    /** The focus image of one space. */
    private Image keyFocus1Image;
    /** The focus Image of double space. */
    private Image keyFocus2Image;
    /** The focus Image of double space. */
    private Image keyFocus3Image;
    /** The image of button A. */
    private Image btnA;

    /** The color for key Strings. */
    private Color c255 = new Color(255, 255, 255);

    /** The color for change Strings. */
    private Color c241 = new Color(241, 241, 241);

    /** The color for title strings. */
    private Color c46 = new Color(46, 46, 46);

    /** The color for title strings. */
    private Color c202 = new Color(202, 174, 97);

    /** The color for focused key strings. */
    private Color c3 = new Color(3, 3, 3);

    /** The width of one key. */
    public static final int HORIZONTAL_STEP = 40;
    /** The height of one key. */
    public static final int VERTICAL_STEP = 31;

    /** The font of size 17. */
    private Font font17 = FontResource.BLENDER.getFont(17);

    /** The font of size 19. */
    private Font font19 = FontResource.BLENDER.getFont(19);

    /** The font of size 21. */
    private Font font21 = FontResource.BLENDER.getFont(21);

    /** The FontMetrics for {@link #font19}. */
    private FontMetrics fm19 = FontResource.getFontMetrics(font19);

    /** The string array of key strings. */
    private String[][] keyboardStrings;

    /** changed message text. */
    private String changeModeStrings;

    /** Title text. */
    private String title;

    /**
     * Instantiates a new virtual extended keyboard renderer.
     */
    public AlphanumericKeyboardRenderer() {
    }

    /**
     * Returns a bounds of AlphanumericKeyboard.
     * @param c The UIComponent of AlphanumericKeyboard.
     * @return a bounds of AlphanumericKeyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.
     *      UIComponent)
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Paint a background and keys for AlphanumericKeyboard.
     * @param g the Graphics to paint AlphanumericKeyboard.
     * @param c the UIComponent to implement VirtualKeyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics, com.videotron.tvi.illico.framework.
     *      UIComponent)
     */
    protected void paint(Graphics g, UIComponent c) {
         g.translate(-354, -151);
        // key background
        AlphanumericKeyboard ui = ((AlphanumericKeyboard) c);
        int currentMode = ui.getCurrentMode();
        g.setFont(font21);
        if (ui.getMode() == KeyboardOption.MODE_POPUP) {
           g.setColor(c255);
           g.fillRect(355, 152, 255, 29);
           g.drawImage(bgImage, 354, 151, c);
//           g.drawImage(shadowImage, 354, 485, c);
        } else {
            g.setColor(Color.black);
            g.fillRect(363, 77 + 151, 237, 247);
        }
        if (title != null) {
            g.setColor(c46);
            g.drawString(title, 366, 174);
            g.setColor(c202);
            g.drawString(title, 365, 173);
        }
        g.drawImage(keyBg1Image, 359, 226, c);
        g.drawImage(keyBg2Image, 359, 259, c);
        g.drawImage(keyBg2Image, 359, 290, c);
        g.drawImage(keyBg2Image, 359, 321, c);
        g.drawImage(keyBg3Image, 362, 352, c);
        g.drawImage(keyBg2Image, 359, 383, c);
        g.drawImage(keyBg2Image, 359, 414, c);

        if (currentMode == AlphanumericKeyboard.MODE_ACCENTS) {
            g.drawImage(keyBg5Image, 362, 445, c);
        } else {
            g.drawImage(keyBg4Image, 359, 445, c);
        }

        int index = 0;
        for (int i = 0, focus = c.getFocus(); i < keyboardStrings.length; i++) {
            for (int j = 0; j < keyboardStrings[i].length; j++) {
                if (keyboardStrings[i][j] == null) {
                    continue;
                }

                index = i * AlphanumericKeyboard.KEY_WIDTH + j;
                g.setFont(font21);
                if (index == focus) {
                    g.setColor(c3);

                    if (index == AlphanumericKeyboard.INDEX_SPACE || index == AlphanumericKeyboard.INDEX_ERASE) {
                        g.drawImage(keyFocus2Image, 361 + j * HORIZONTAL_STEP, 227 + i * VERTICAL_STEP, c);
                    } else if (currentMode != AlphanumericKeyboard.MODE_ACCENTS
                            && (index == AlphanumericKeyboard.INDEX_DONE || index == AlphanumericKeyboard.INDEX_CANCEL)) {

                        g.drawImage(keyFocus2Image, 361 + j * HORIZONTAL_STEP, 227 + i * VERTICAL_STEP, c);

                    } else if (currentMode == AlphanumericKeyboard.MODE_ACCENTS
                            && (index == AlphanumericKeyboard.ACCENTS_INDEX_DONE || index == AlphanumericKeyboard.ACCENTS_INDEX_CANCEL)) {

                        g.drawImage(keyFocus3Image, 361 + j * HORIZONTAL_STEP, 227 + i * VERTICAL_STEP, c);

                    } else {
                        g.drawImage(keyFocus1Image, 361 + j * HORIZONTAL_STEP, 227 + i * VERTICAL_STEP, 44, 37, c);
                    }
                } else {
//                    g.setFont(font22);
                    g.setColor(c255);
                }

                if (currentMode == AlphanumericKeyboard.MODE_ACCENTS && index == AlphanumericKeyboard.INDEX_LEFT) {
                    if (index == focus) {
                        g.drawImage(arrowLeftFocusedImage, 534, 422, c);
                    } else {
                        g.drawImage(arrowLeftImage, 534, 422, c);
                    }
                } else if (currentMode == AlphanumericKeyboard.MODE_ACCENTS
                        && index == AlphanumericKeyboard.INDEX_RIGHT) {
                    if (index == focus) {
                        g.drawImage(arrowRightFocusedImage, 576, 422, c);
                    } else {
                        g.drawImage(arrowRightImage, 576, 422, c);
                    }
                } else if (currentMode == AlphanumericKeyboard.MODE_ACCENTS && index == AlphanumericKeyboard.INDEX_UP) {
                    if (index == focus) {
                        g.drawImage(arrowUpImageDim, 456, 422, c);
                    } else {
                        g.drawImage(arrowUpImageDim, 456, 422, c);
                    }
                } else if (currentMode == AlphanumericKeyboard.MODE_ACCENTS && index == AlphanumericKeyboard.INDEX_DOWN) {
                    if (index == focus) {
                        g.drawImage(arrowDownImageDim, 496, 422, c);
                    } else {
                        g.drawImage(arrowDownImageDim, 496, 422, c);
                    }
                } else if (currentMode != AlphanumericKeyboard.MODE_ACCENTS
                        && (index == AlphanumericKeyboard.INDEX_DONE || index == AlphanumericKeyboard.INDEX_CANCEL)) {
                    g.setFont(font19);
                    GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], 383 + j * HORIZONTAL_STEP
                            + OFFSET_X_DOUBLE_KEYS, 249 + i * VERTICAL_STEP, fm19);
                } else if (currentMode == AlphanumericKeyboard.MODE_ACCENTS
                        && (index == AlphanumericKeyboard.ACCENTS_INDEX_DONE || index == AlphanumericKeyboard.ACCENTS_INDEX_CANCEL)) {
                    g.setFont(font19);
                    GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], 383 + j * HORIZONTAL_STEP
                            + OFFSET_X_TRIPLE_KEYS, 249 + i * VERTICAL_STEP, fm19);
                } else if (index == AlphanumericKeyboard.INDEX_SPACE || index == AlphanumericKeyboard.INDEX_ERASE) {
                    g.setFont(font19);
                    GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], 383 + j * HORIZONTAL_STEP
                            + OFFSET_X_DOUBLE_KEYS, 249 + i * VERTICAL_STEP, fm19);
                } else {
                    GraphicUtil.drawStringCenter(g, keyboardStrings[i][j], 383 + j * HORIZONTAL_STEP, 249 + i
                            * VERTICAL_STEP, g.getFontMetrics());
                }
            }
        }
        if (ui.keyboardMode == KeyboardOption.MODE_POPUP
                && ui.getAlphanumericType() == KeyboardOption.ALPHANUMERIC_ALL && !ui.isAkeyDeactive()) {
            g.drawImage(btnA, 363, 488, c);
            g.setFont(font17);
            g.setColor(c241);
            g.drawString(changeModeStrings, 389, 503);
        }
        g.translate(354, 151);
    }

    /**
     * Prepare a key strings to paint for AlphanumericKeyboard.
     * @param c The UIComponent to implement AlphanumericKeyboard.
     * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
     */
    public void prepare(UIComponent c) {
        
        bgImage = DataCenter.getInstance().getImage("04_keyboard01.png");
//        shadowImage = DataCenter.getInstance().getImage("04_keyboard_sh.png");
        keyBg1Image = DataCenter.getInstance().getImage("04_keybg_01.png");
        keyBg2Image = DataCenter.getInstance().getImage("04_keybg_02.png");

        keyBg3Image = DataCenter.getInstance().getImage("04_keybg_03.png");
        keyBg4Image = DataCenter.getInstance().getImage("04_keybg_04.png");

        keyBg5Image = DataCenter.getInstance().getImage("04_keybg_05.png");

        arrowRightFocusedImage = DataCenter.getInstance().getImage("04_key_r_foc.png");

         arrowLeftFocusedImage = DataCenter.getInstance().getImage("04_key_l_foc.png");

         arrowUpImageDim = DataCenter.getInstance().getImage("04_key_t_dim.png");

         arrowDownImageDim = DataCenter.getInstance().getImage("04_key_b_dim.png");

        arrowRightImage = DataCenter.getInstance().getImage("04_key_r.png");

        arrowLeftImage = DataCenter.getInstance().getImage("04_key_l.png");
        keyFocus1Image = DataCenter.getInstance().getImage("04_keyfoc_01.png");
        keyFocus2Image = DataCenter.getInstance().getImage("04_keyfoc_02.png");
        keyFocus3Image = DataCenter.getInstance().getImage("04_keyfoc_03.png");
        btnA = DataCenter.getInstance().getImage("btn_a.png");

        
        keyboardStrings = ((AlphanumericKeyboard) c).getCurrentKeyStrings();
        changeModeStrings = ((AlphanumericKeyboard) c).getChangeModeStrings();
        title = ((AlphanumericKeyboard) c).getTitleStrings();
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
}

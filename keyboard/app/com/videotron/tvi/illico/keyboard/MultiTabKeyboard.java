/**
 * @(#)MultiTabKeyboard.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import java.awt.event.KeyEvent;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class MultiTabKeyboard extends Keyboard implements TVTimerWentOffListener {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2877880237990770079L;

    /** The index of number 8 key. */
    public static final int INDEX_NUM_8 = 7;

    /** The index of Space key. */
    public static final int INDEX_SPACE = 9;

    /** The index of number 0 key. */
    public static final int INDEX_NUM_0 = 10;

    /** The index of Erase key. */
    public static final int INDEX_ERASE = 11;

    /** The index of Done key. */
    public static final int INDEX_DONE = 12;

    /** The index of Cancel key. */
    public static final int INDEX_CANCEL = 13;

    /** The index of Keyboard key to move {@link VirtualKeyboard}. */
    public static final int INDEX_KEYBOARD = 14;

    /** The amount value of keys by line. */
    public static final int AMOUNT_KEYS_BY_LINE = 3;

    /** The max value of key line. */
    public static final int MAX_LINE = 5;

    /** The char value for SPACE Key. */
    private static final char CHAR_SPACE = ' ';

    /** The renderer to paint Multi-Tab keyboard. */
    private Renderer renderer = new MutiTabKeyboardRenderer();

    /** The keyboard string array to display for each keys. */
    private String[][] keyboardNumberStrings = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" },
            { "Space", "0", "Erase"}, { null, null, "Keyboard"} };

    /** The keyboard string array to display sub key that display under {@link #keyboardNumberStrings}. */
    private String[][] keyboardSubStrings = { { null, "ABC", "DEF" }, { "GHI", "JKL", "MNO" },
            { "PQRS", "TUV", "WXYZ" }, { null, ".,+?!", null }, { null, null, null } };

    /** The keyboard string array to insert into TextField for each keys. */
    private String[][] keyboardInputStrings = { { "1", "ABC2abcÀÂÄàâäÇç", "DEF3defÈÉÊËèéêë" }, { "GHI4ghiÎÏîï", "JKL5jkl", "MNO6mnoÔŒôœ" },
            { "PQRS7pqrs", "TUV8tuvÙÛÜùûü", "WXYZ9wxyzŸÿ" }, { null, ".,+?!0", null }, { null, null, null } };

    /** The string for double space keys (ex> Space, Cancel). */
    private String spaceString, eraseString, doneString, cancelString;

    /** The tvSpec to wait a next user input. */
    private TVTimerSpec tvSpec;

    /** The temporarily selected index of character array on keys. */
    private int temporaryIndex;

    /** The last key to select. */
    private int lastKeyEvent;

    /**
     * Instantiates a new multi-tab keyboard and set renderer to paint. Loads a property and create a TVTimerSpec for
     * delay time to wait a continuously user input.
     */
    public MultiTabKeyboard() {
        setRenderer(renderer);
        long delay = Long.valueOf((String) DataCenter.getInstance().get("delayTime")).longValue();
        tvSpec = new TVTimerSpec();
        tvSpec.setDelayTime(delay);
        tvSpec.addTVTimerWentOffListener(this);
    }

    /**
     * Called before showing MultiTabKeyboard. prepare to the textField and initialize some values.
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printInfo("MultiTabKeyboard: start()");
        }

        lastKeyEvent = 0;
        temporaryIndex = 0;
        focus = INDEX_NUM_8;

        textField.setMutiTab(true);
        textField.start(true);
        add(textField);
        super.start(this);
    }

    /**
     * Called When keyboard be closed. It make to stop the textField and remove the keyboardListerner.
     */
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printInfo("MultiTabKeyboard: stop()");
        }
        textField.stop();
        remove(textField);
        keyboardListener = null;
        super.stop();
    }

    /**
     * Receive a key code and process a action for key code.
     *
     * @param code
     *            the key code to select by user.
     * @see com.videotron.tvi.illico.framework.UIComponent#handleKey(int)
     * @return true or false if return true, then other application can't receive this key event.
     */
    public boolean handleKey(int code) {
        boolean isUsed = false;
        switch (code) {
        case OCRcEvent.VK_EXIT:
        case KeyCodes.LAST:
            notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
            isUsed = true;
            break;
        case KeyEvent.VK_LEFT:
            updateTemporaryChar();
            if (--focus < 0) {
                focus = INDEX_KEYBOARD;
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_RIGHT:
            updateTemporaryChar();
            if (++focus > INDEX_KEYBOARD) {
                focus = 0;
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_UP:
            updateTemporaryChar();
            if (focus == 0) {
                focus = INDEX_KEYBOARD;
            } else if (focus == 1) {
                focus = INDEX_DONE;
            } else if (focus == 2) {
                focus = INDEX_CANCEL;
            } else {
                focus -= AMOUNT_KEYS_BY_LINE;
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_DOWN:
            updateTemporaryChar();
            if (focus == INDEX_DONE) {
                focus = 1;
            } else if (focus == INDEX_CANCEL) {
                focus = 2;
            } else if (focus == INDEX_KEYBOARD) {
                focus = 0;
            } else {
                focus += AMOUNT_KEYS_BY_LINE;
            }
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_0:
        case KeyEvent.VK_1:
        case KeyEvent.VK_2:
        case KeyEvent.VK_3:
        case KeyEvent.VK_4:
        case KeyEvent.VK_5:
        case KeyEvent.VK_6:
        case KeyEvent.VK_7:
        case KeyEvent.VK_8:
        case KeyEvent.VK_9:
            numericKeys(code);
            repaint();
            isUsed = true;
            break;
        case KeyEvent.VK_ENTER:
            keyOK();
            repaint();
            isUsed = true;
            break;
        default:
            isUsed = false;
            break;
        }

        super.handleKey(code);
        return isUsed;
    }

    /**
     * If last key event not equals a current key and has a last temporarily char, update textField. calculate a current
     * focus and process a code by calling keyOK().
     *
     * @param code
     *            the key code of KeyEvent.VK_0 ~ KeyEvent.VK_9
     */
    private void numericKeys(int code) {

        if (lastKeyEvent != code) {
            updateTemporaryChar();
            lastKeyEvent = code;
        }

        if (code >= KeyEvent.VK_1) {
            focus = code - KeyEvent.VK_1;
        } else {
            focus = INDEX_NUM_0;
        }
        keyOK();
    };

    /**
     * Process a key for user input. Update a TextField or do a action according to user input.
     */
    private void keyOK() {
        startWaiting();

        if (focus == INDEX_SPACE) {
            if (maxCharslen < textField.getText().length()) {
                return;
            }
            textField.keyTyped(CHAR_SPACE);
        } else if (focus == INDEX_ERASE) {
            String text = textField.getText();
            int pos = textField.getCursorPosition();
            if (pos > 0) {
                StringBuffer sb = new StringBuffer(text);
                text = sb.deleteCharAt(pos - 1).toString();
                Log.printInfo("text = " + text);
            }
            textField.moveCursor(pos - 1);
            if (text != null && text.length() >= 0) {
                textField.setText(text);
            }
            repaint();
        } else if (focus == INDEX_DONE) {
            notifyKeyboardEvent(EVENT_INPUT_ENDED, 0);
        } else if (focus == INDEX_CANCEL) {
            notifyKeyboardEvent(EVENT_INPUT_CANCELED, 0);
        } else if (focus == INDEX_KEYBOARD) {
            Controller.getInstance().moveToNextKeyboard(KeyboardOption.TYPE_VIRTUAL);
        } else {
            if (maxCharslen < textField.getText().length()) {
                return;
            }
            int horizontal = focus % AMOUNT_KEYS_BY_LINE;
            int vertical = focus / AMOUNT_KEYS_BY_LINE;
            if (focus == 0) { // case 1 key
                textField.keyTyped(keyboardInputStrings[vertical][horizontal].charAt(0));
            } else { // others (2~0 key)

                textField.setTemporaryChar(keyboardInputStrings[vertical][horizontal].charAt(temporaryIndex));

                int maxLength = keyboardInputStrings[vertical][horizontal].toCharArray().length;
                if (++temporaryIndex == maxLength) {
                    temporaryIndex = 0;
                }
            }
        }
    }

    /**
     * Prepare a key names to need a translate and a key strings for MultiTabKeyboard. Wait for a images to finish
     * loading.
     */
    public void prepare() {
        spaceString = (String) DataCenter.getInstance().get("buttonNames.Space" + settingLanguage );
        eraseString = (String) DataCenter.getInstance().get("buttonNames.Erase" + settingLanguage);
        doneString = (String) DataCenter.getInstance().get("buttonNames.Done" + settingLanguage);
        cancelString = (String) DataCenter.getInstance().get("buttonNames.Cancel" + settingLanguage);

        keyboardNumberStrings = new String[][] { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" },
                { spaceString, "0", eraseString }, { doneString, cancelString, "Keyboard" } };

        FrameworkMain.getInstance().getImagePool().waitForAll();
        super.prepare();
    }

    /**
     * Returns a key string for display (ex. 1, 2, 3).
     *
     * @return The string array for keys.
     */
    public String[][] getKeyboardNumberStrings() {
        return keyboardNumberStrings;
    }

    /**
     * Returns a sub key string for display (ex. ABC, DEF).
     *
     * @return The string array for keys.
     */
    public String[][] getKeyboardSubStrings() {
        return keyboardSubStrings;
    }

    /**
     * set a timer to wait a user input for selecting next character.
     */
    public void startWaiting() {
        stopWaiting();

        try {
            TVTimer.getTimer().scheduleTimerSpec(tvSpec);
        } catch (TVTimerScheduleFailedException e) {
            Log.printWarning(e);
        }
    }

    /**
     * stop a time to wait a user input for selecting next character.
     *
     * @see #startWaiting()
     */
    public void stopWaiting() {
        TVTimer.getTimer().deschedule(tvSpec);
    }

    /**
     * Updates a temporary char if exist.
     */
    public void updateTemporaryChar() {
        temporaryIndex = 0;
        stopWaiting();
        textField.finishToInputTemporaryChar();
        textField.prepare();
        repaint();
    }

    /**
     * If timer went off, Update a temporaryChar.
     *
     * @param event
     *            the event of delayTime to select a next character.
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("MultiTabKeyboard: timerWentOff()");
        }
        updateTemporaryChar();
    }
}

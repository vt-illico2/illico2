/**
 * @(#)TextField.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.keyboard;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * UI class that represents the current inputed text and cursor.
 * @author Woojung Kim
 * @version 1.1
 */
public class TextField extends UIComponent {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3011862684505982481L;

    /** The text of showing in TextFiled. */
    private String text;

    private boolean tempViewed;

    /** The text of temporary char for Multi-Tab. */
    private char temporaryChar;

    /** The cursor position in TextField. */
    private int cursorPosition;

    /** The boolean indicate that current keyboard type is Multi-Tab . */
    private boolean isMultiTab;

    /** The renderer to draw TextField. */
    private TextFieldRenderer renderer = new TextFieldRenderer();
    
    private int maxChar = 0;
    
    /**
     * Instantiates a new text field. initialize {@link #text}.
     */
    public TextField() {
        text = TextUtil.EMPTY_STRING;

    }

    /**
     * Call by Keyboard when Keyboard start. It set the {@link #renderer} and prepare the renderer.
     * @param visible whether text box show or not
     * @see com.videotron.tvi.illico.framework.UIComponent#start()
     */
    public void start(boolean visible) {
        if (Log.INFO_ON) {
            Log.printInfo("TextField: start()" + text);
        }
        temporaryChar = 0;
        cursorPosition = 0;
        if (text != null) {
            cursorPosition = text.length();
        }
        if (visible) {
            setRenderer(renderer);
            setVisible(true);
            renderer.prepare(this);
        }
    }

    /**
     * Call by Keyboard when Keyboard stop. It set a false to Visible
     * @see com.videotron.tvi.illico.framework.UIComponent#stop()
     */
    public void stop() {
        setVisible(false);
    }

    /**
     * Move the cursor position.
     * @param pos the position of cursor
     */
    public void moveCursor(int pos) {
        Log.printInfo("pos = " + pos + " text.length() " + text.length());
        if (pos > -1 && text.length() >= pos) {
            cursorPosition = pos;
            renderer.prepare(this);
            repaint();
        }
    }

    public int getCursorPosition() {
        return cursorPosition;
    }

    /**
     * Receive a char from Keyboard and append a char to {@link #text} call
     * {@link TextFieldRenderer#prepare(UIComponent)} to prepare.
     * @param c the char to pass via Keyboard.
     */
    public void keyTyped(char c) {
        if(tempViewed) {
            tempViewed = false;
            text = "";
            cursorPosition = 0;
        }

        StringBuffer sf = new StringBuffer(text);
        sf.insert(cursorPosition, c);
        text = firstStirngUpper(sf.toString());

        cursorPosition++;// = text.length();
        renderer.prepare(this);
    }

    public String firstStirngUpper(String txt){
        String str = txt;
        if(str != null && str.length() > 1){
            String upper = str.substring(0, 1);
            String lower = str.substring(1, str.length());
            str = upper.toUpperCase() + lower.toLowerCase();
        } else if(str != null && str.length() == 1){
            str = str.toUpperCase();
        }

        return str;
    }

    /**
     * Sets the text which be shown in Keyboard.
     * @param txt the new text
     */
    public void setText(String txt) {
        this.text = firstStirngUpper(txt);
        renderer.prepare(this);
        repaint();
    }

    public void setMaxChar(int num) {
        if(num == 0) {
            maxChar = 30;
        }
        maxChar = num;
    }
    
    public int getMaxChar() {
        return maxChar;
    }

    /**
     * Sets temp viewed
     * @param view
     */
    public void setTempViewText(boolean view) {
        tempViewed = view;
        Log.printDebug("tempViewed = " + tempViewed);
        renderer.prepare(this);
        repaint();
    }

    /**
     * Gets the text which was shown in Keyboard.
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * In case of Multi-Tab keyboard, TextField has a temporary char to input by user.
     * @param c the temporary char to input by user.
     */
    public void setTemporaryChar(char c) {
        if(tempViewed){
            tempViewed = false;
            text = "";
            cursorPosition = 0;
        }

        temporaryChar = c;
        renderer.prepare(this);
    }

    /**
     * Returns a current temporarily char.
     * @see #setTemporaryChar(char)
     * @return the temporary char
     */
    public char getTemporaryChar() {
        return temporaryChar;
    }

    /**
     * the temporary char is added in text and cleared.
     */
    public void finishToInputTemporaryChar() {
        if (temporaryChar != 0) {
            StringBuffer sf = new StringBuffer(text);
            sf.append(temporaryChar);
            text = sf.toString();
            cursorPosition = text.length();
            temporaryChar = 0;
        }
    }

    /**
     * Returns a current keyboard type.
     * @return true if current keyboard is Multi-Tab keyboard, false otherwise.
     */
    public boolean isMultiTab() {
        return isMultiTab;
    }

    /**
     * Set a current keyboard type to Multi-Tab keyboard.
     * @param multiTab true if current keyboard is Multi-Tab keyboard, false otherwise.
     */
    public void setMutiTab(boolean multiTab) {
        this.isMultiTab = multiTab;
    }
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.controller.SearchController;
import com.videotron.tvi.illico.search.data.SearchDataManager;
import com.videotron.tvi.illico.util.Constants;

/**
 * <code>RendererKeypad</code>
 * 
 * @version $Id: RendererSearchKeypad.java,v 1.4 2017/01/09 20:46:12 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 25.
 * @description
 */
public class RendererSearchKeypad extends Renderer {
	public Rectangle getPreferredBounds(UIComponent c) {
		return Constants.SCREEN_BOUNDS;
	}

	public void prepare(UIComponent c) {
		
	}

	protected void paint(Graphics g, UIComponent c) {
		if (Log.EXTRA_ON) {
			printExtra(g, c);
		}
    	g.setColor(Rs.C30);
    	g.fillRect(0, 480, Constants.SCREEN_WIDTH, 60);
	}
	
	private void printExtra(Graphics g, UIComponent c) {
		g.setColor(Color.green);
		g.setFont(Rs.F18);
		g.drawString("Start From-init: " + SearchController.getInstance().getLaunchPlatform() + " - " + SearchController.getInstance().getInitValue(), 253, 30);
		g.drawString("Server IP: " + SearchDataManager.getInstance().getUrlPath(), 253, 45);
		String[] proxyInfos = SearchDataManager.getInstance().getProxyInfo();
		if (proxyInfos != null) {
			for (int i=0; i<proxyInfos.length; i++) {
				g.drawString("Proxy["+i+"]: " + proxyInfos[i], 253, 60 + (i*15));
			}
		}
	}
}

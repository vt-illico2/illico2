/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.SearchKeypadDef;
import com.videotron.tvi.illico.search.ui.component.Keypad;

/**
 * <code>RendererSearchKeypad</code>
 * 
 * @version $Id: RendererKeypad.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 31.
 * @description
 */
public class RendererKeypad extends Renderer {
	private Image imgIconB, imgIconC;
	
	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(53, 172, 340, 244);
	}

	public void prepare(UIComponent c) {
		imgIconB = DataCenter.getInstance().getImage("b_btn_b.png");
		imgIconC = DataCenter.getInstance().getImage("b_btn_c.png");
	}

	public void paint(Graphics g, UIComponent c) {
		Keypad keypad = (Keypad)c;
		drawBackground(g, keypad);
		drawItem(g, keypad);
	}
	
	private void drawBackground(Graphics g, Keypad keypad) {
		g.setColor(Rs.DC29_29_29_128);
		g.fillRect(0, 0, keypad.getWidth(), keypad.getHeight());
	}
	
	private void drawItem(Graphics g, Keypad keypad) {
		for (int i=0; i<Keypad.KEYPAD_DATA.length; i++) {
			for (int ii=0; ii<Keypad.KEYPAD_DATA[i].length; ii++) {
				Color itemBgC = Rs.C56;
				Color itemFontC = Rs.C241;
				int posX =3 + (ii * Keypad.H_GAP);
				int posY = 3 + (i * Keypad.V_GAP);
				if (i == keypad.curPoint.x && ii == keypad.curPoint.y && keypad.isFocused()) {
					itemBgC = Rs.C246_199_0;
					itemFontC = Rs.C0;
				} else {
					switch(Keypad.KEYPAD_DATA[i][ii]) {
						case SearchKeypadDef.ID_0:
						case SearchKeypadDef.ID_1:
						case SearchKeypadDef.ID_2:
						case SearchKeypadDef.ID_3:
						case SearchKeypadDef.ID_4:
						case SearchKeypadDef.ID_5:
						case SearchKeypadDef.ID_6:
						case SearchKeypadDef.ID_7:
						case SearchKeypadDef.ID_8:
						case SearchKeypadDef.ID_9:
							itemBgC = Rs.C78;
							break;
						default:
							itemBgC = Rs.C56;
							break;
					}
				}
				int keypadAreaW = -1;
				int keypadAreaH = 38;
				Image imgIcon = null;
				int keypadDataTxtX = -1;
				int keypadDataTxtY = -1;
				Font fontKeypadData = Rs.F22;
				if (Keypad.KEYPAD_DATA[i][ii] == SearchKeypadDef.ID_SPACE) {
					keypadAreaW = 94;
					imgIcon = imgIconB;
					keypadDataTxtX = posX + 37;
					keypadDataTxtY = posY + 24;
					fontKeypadData = Rs.F17;
				} else if (Keypad.KEYPAD_DATA[i][ii] == SearchKeypadDef.ID_DELETE) {
					keypadAreaW = 94;
					imgIcon = imgIconC;
					keypadDataTxtX = posX + 37;
					keypadDataTxtY = posY + 24;
					fontKeypadData = Rs.F17;
				} else {
					keypadAreaW = 46;
					keypadDataTxtX = posX + 23 - (keypad.keypadDataW[i][ii] / 2);
					keypadDataTxtY = posY + 25;
					fontKeypadData = Rs.F22;
				}
				g.setColor(itemBgC);
				g.fillRect(posX, posY, keypadAreaW, keypadAreaH);
				g.setFont(fontKeypadData);
				g.setColor(itemFontC);
				if (imgIcon != null) {
					g.drawImage(imgIcon, posX + 12, posY + 8, keypad);
				}
				g.drawString(keypad.keypadDataTxts[i][ii], keypadDataTxtX, keypadDataTxtY);	
			}
		}
	}

}

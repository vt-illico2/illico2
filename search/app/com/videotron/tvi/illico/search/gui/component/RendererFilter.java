/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.ui.component.Filter;

/**
 * <code>RendererSearchFilter</code>
 * 
 * @version $Id: RendererFilter.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 31.
 * @description
 */
public class RendererFilter extends Renderer {
	private Image imgIconA;
	private Image imgArrowL, imgArrowR;
	private String txtFilter;
	
	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(0, 94, 960, 44);
	}

	public void prepare(UIComponent c) {
		imgIconA = DataCenter.getInstance().getImage("b_btn_a.png");
		imgArrowL = DataCenter.getInstance().getImage("02_ars_l_s.png");
		imgArrowR = DataCenter.getInstance().getImage("02_ars_r_s.png");
		txtFilter = DataCenter.getInstance().getString("Label.Filter");
	}

	protected void paint(Graphics g, UIComponent c) {
		Filter sf = (Filter)c;
		drawBackground(g, sf);
		drawItem(g, sf);
	}
	
	private void drawBackground(Graphics g, Filter sf) {
		final int BASE_Y = sf.getY();
		g.setColor(Rs.C55);
		g.fillRect(0, 0, sf.getWidth(), sf.getHeight());
		g.setColor(Rs.C40);
		if (sf.isLangEnglish) {
			g.fillRect(766, 0, 8, sf.getHeight());
		} else {
			g.fillRect(780, 0, 8, sf.getHeight());
		}
		if (imgIconA != null) {
			g.drawImage(imgIconA, 56, 106 - BASE_Y, sf);
		}
		if (txtFilter != null) {
			g.setFont(Rs.F19);
			g.setColor(Rs.C222);
			g.drawString(txtFilter, 84, 122 - BASE_Y);
		}
	}
	
	private void drawItem(Graphics g, Filter sf) {
		final int BASE_Y = sf.getY();
		g.setFont(Rs.F19);
		if (sf.filterStr != null) {
			int filterStrX =  (Filter.GENERAL_FILTER_VALID_W / 2) - (sf.generalFilterTotalW / 2);
			if (sf.isLangEnglish) {
				filterStrX += Filter.ITEM_BASE_X_EN;
			} else {
				filterStrX += Filter.ITEM_BASE_X_FR;
			}
			for (int i = 0; i < sf.filterStr.length; i++) {
				// filter String width + side margine = filter String Area width
				int filterStrAreaW = sf.filterStrW[i] + (sf.margin * 2);
				// adult filter area is different from other filters.
				if (i == sf.filterStr.length - 1) {
					if (sf.isLangEnglish) {
						filterStrX = Filter.ITEM_ADULT_BASE_X_EN;
					} else {
						filterStrX = Filter.ITEM_ADULT_BASE_X_FR;
					}
				}
				if (i == sf.curIdx && sf.isFocused()) {
					g.setColor(Rs.C246_199_0);
					g.fillRect(filterStrX, 0, filterStrAreaW, sf.getHeight());
					g.drawImage(imgArrowL, filterStrX - 12, 110 - BASE_Y, sf);
					g.drawImage(imgArrowR, filterStrX + filterStrAreaW + 5, 110 - BASE_Y, sf);
					g.setColor(Rs.C0);
				} else {
					if (i == sf.selectedIdx) {
						g.setColor(Rs.C246_199_0);
					} else {
						if (Filter.FILTER_IDS[i] == SearchFilterDef.ID_ADULT && !DataCenter.getInstance().getBoolean(Rs.DCKEY_IS_AUTHORIZED)) {
							g.setColor(Rs.C149);
						} else {
							g.setColor(Rs.C222);
						}
					}
				}
				g.drawString(sf.filterStr[i], filterStrX + sf.margin, 122 - BASE_Y);
				filterStrX += filterStrAreaW;
			}
		}
	}
}

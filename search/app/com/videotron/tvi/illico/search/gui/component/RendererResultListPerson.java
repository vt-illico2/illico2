/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.resource.SearchedEpg;
import com.videotron.tvi.illico.search.data.resource.SearchedPerson;
import com.videotron.tvi.illico.search.ui.component.ResultList;

/**
 * <code>RendererResultListLive</code>
 * 
 * @version $Id: RendererResultListPerson.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 1.
 * @description
 */
public class RendererResultListPerson extends RendererResultList{
	public static final int BASE_X = 54;
	public static final int BASE_Y = 158;
	protected static final int VALID_TITLE_W = 280;
	
	private Image imgArrowU, imgArrowD;
	private Image imgIconPerson;
	
	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(54, 158, 855, 319);
	}
	
	protected void prepareSub(UIComponent c) {
		imgArrowU = DataCenter.getInstance().getImage("02_ars_t.png");
		imgArrowD = DataCenter.getInstance().getImage("02_ars_b.png");
		imgIconPerson = DataCenter.getInstance().getImage("03_icon_person.png");
	}

	public void paint(Graphics g, UIComponent c) {
		ResultList resultList = (ResultList) c;
		drawBackground(g, resultList);
		for (int i = 0; i<resultList.curPageCount; i++) {
			if (i != resultList.curPageCount - 1) {
				drawItemLine(g, i);
			}
		}
		for (int i = 0; i < resultList.curPageCount; i++) {
			if (resultList.adapter != null && i < resultList.adapter.getTotalCount()) {
				int index = resultList.curPageStartIdx + i;
				Resource resource = resultList.adapter.getResource(index);
				drawItem(g, resource, i, resultList);
			}
		}
	}
	
	private void drawBackground(Graphics g, ResultList resultList) {
		g.setColor(Rs.C56);
		g.fillRect(54 - BASE_X, 167 - BASE_Y, 855, 301);
		if (resultList.totalCount > ResultList.ROW && resultList.curPageStartIdx > 0) {
			g.drawImage(imgArrowU, 480 - BASE_X, 158 - BASE_Y, resultList);
		}
		if (resultList.totalCount > ResultList.ROW && resultList.curPageStartIdx + resultList.curPageCount < resultList.totalCount) {
			g.drawImage(imgArrowD, 480 - BASE_X, 460 - BASE_Y, resultList);
		}
	}
	
	private void drawItemLine(Graphics g, int i) {
		g.setColor(Rs.C39);
		g.fillRect(69 - BASE_X, 217 - BASE_Y + (i * ResultList.ROW_GAP), 824, 1);
	}

	protected void drawItem(Graphics g, Resource resource, int i, ResultList resultList) {
		boolean isFocused = resultList.isFocused() && resultList.curIdx == resultList.curPageStartIdx + i;
		if (isFocused) {
			g.setColor(Rs.C246_199_0);
			g.fillRect(54 - BASE_X, 166 - BASE_Y + (i * ResultList.ROW_GAP), 855, 52);
		}
		SearchedPerson searchedPerson = (SearchedPerson) resource;
		if (searchedPerson == null) {
			drawInvalidTitle(g, i, resultList, isFocused);
			return;
		}
		
		// Draws icon person
		g.drawImage(imgIconPerson, 70 - BASE_X, 174 - BASE_Y + (i * ResultList.ROW_GAP), resultList);
		
		drawTitle(g, searchedPerson, i, resultList, isFocused);
	}
	
//	private void drawTitle(Graphics g, SearchedPerson searchedPerson, int index, ResultList resultList, boolean isFocused) {
//		boolean isSearchedLetters = resultList.searchParameter.isSearchedLetters();
//		g.setFont(Rs.F20);
//		int posX = 136 - BASE_X;
//		int posY = 197 - BASE_Y + (index * ResultList.ROW_GAP);
//		
//		String remain = searchedPerson.getTitle();
//		if (isSearchedLetters) {
//			final int START_EM_TAG_LTH = Rs.TAG_EM_START.length();
//			final int END_EM_TAG_LTH = Rs.TAG_EM_END.length();
//			
//			int emStartIdx = remain.indexOf(Rs.TAG_EM_START);
//			int emEndIdx = remain.indexOf(Rs.TAG_EM_END);
//			while(emStartIdx != -1 && emEndIdx != -1) {
//				String drawTxt = null;
//				int drawTxtW = 0;
//				if (emStartIdx > 0) {
//					drawTxt = remain.substring(0, emStartIdx);
//					drawTxtW = Rs.FM20.stringWidth(drawTxt);
//					if (isFocused) {
//						g.setColor(Rs.C0);
//					} else {
//						g.setColor(Rs.C255);
//					}
//					g.drawString(drawTxt, posX, posY);
//					posX += drawTxtW;
//				}
//				drawTxt = remain.substring(emStartIdx + START_EM_TAG_LTH, emEndIdx);
//				if (Log.DEBUG_ON) {
//					Log.printDebug("[RendererResultListPerson.drawTitle] drawTxt: " + drawTxt);
//				}
//				drawTxtW = Rs.FM20.stringWidth(drawTxt);
//				if (isFocused) {
//					g.setColor(Rs.C0);
//				} else {
//					g.setColor(Rs.C246_199_0);
//				}
//				g.drawString(drawTxt, posX, posY);
//				posX += drawTxtW;
//				
//				remain = remain.substring(emEndIdx + END_EM_TAG_LTH);
//				emStartIdx = remain.indexOf(Rs.TAG_EM_START);
//				emEndIdx = remain.indexOf(Rs.TAG_EM_END);
//			}
//		} else {
//			remain = Util2.getStringExceptString(remain, Rs.TAG_EM_START);
//			remain = Util2.getStringExceptString(remain, Rs.TAG_EM_END);
//		}
//		
//		if (remain.length() > 0) {
//			if (isFocused) {
//				g.setColor(Rs.C0);
//			} else {
//				g.setColor(Rs.C255);
//			}
//			g.drawString(remain, posX, posY);
//		}
//	}
}

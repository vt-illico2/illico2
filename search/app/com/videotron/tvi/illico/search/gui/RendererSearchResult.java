/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.controller.SearchController;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.ui.SceneSearchResult;
import com.videotron.tvi.illico.util.Constants;

/**
 * <code>RendererSearchResult</code>
 * 
 * @version $Id: RendererSearchResult.java,v 1.4 2017/01/09 20:46:12 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 28.
 * @description
 */
public class RendererSearchResult extends Renderer {
	private Image imgIconBack;
	private String txtBack, txtResults;
	private int txtResultsW;
	
	private String txtNoResult;
	
	public Rectangle getPreferredBounds(UIComponent c) {
		return Constants.SCREEN_BOUNDS;
	}

	public void prepare(UIComponent c) {
		imgIconBack = DataCenter.getInstance().getImage("b_btn_back.png");
		txtBack = DataCenter.getInstance().getString("Label.result_back_key");
		txtResults = DataCenter.getInstance().getString("Label.Results");
		txtResultsW = Rs.FM21.stringWidth(txtResults);
	}

	protected void paint(Graphics g, UIComponent c) {
		if (Log.EXTRA_ON) {
			printExtra(g, c);
		}
		SceneSearchResult scene = (SceneSearchResult)c;
		//draw search keyword and search filter
		int posX = 56;
		g.setFont(Rs.F21);
		g.setColor(Rs.C241_182_61);
		g.drawString(txtResults, posX, 105);
		posX += txtResultsW + 12;
		if (scene.pSearchKeyword != null) {
			g.setColor(Rs.C222);
			String displaySearchKeyword = "\"" + scene.displaySearchKeyword + "\"";
			g.drawString(displaySearchKeyword, posX,105);
			posX += scene.displaySearchKeywordW;
			
			String txtSearchFilter = null;
			if (scene.pSearchFilterId != SearchFilterDef.ID_ALL) {
				txtSearchFilter = SearchFilterDef.getSectionFilterText(scene.pSearchFilterId);
			}
			if (txtSearchFilter != null) {
				posX += 23;
				String txtFilter = DataCenter.getInstance().getString("Label.Filter");
				String displaySearchFilter = "(" + txtFilter + "   " + txtSearchFilter +")";
				g.drawString(displaySearchFilter, posX,105);
			}
		}
		
		// draw "back to search"
		g.drawImage(imgIconBack, 54, 488, c);
		g.setFont(Rs.F17);
		g.setColor(Rs.C241);
		g.drawString(txtBack, 91, 503);
		
		if (scene.isNoResult) {
			drawNoResultPage(g, c);
		}
	}
	
	private void drawNoResultPage(Graphics g, UIComponent c) {
		g.setColor(Rs.C23);
		g.fillRect(54, 129, 855, 38);
		
		g.setColor(Rs.C56);
		g.fillRect(54, 167, 576, 301);
		
		g.setColor(Rs.C39);
		for (int i = 0; i < 4; i++) {
			g.fillRect(69, 267 + (50 * i), 546, 1);
		}
		g.setColor(Rs.C246_199_0);
		g.fillRect(54, 167, 576, 52);
		
		g.setColor(Rs.C77);
		g.fillRect(630, 167, 279, 301);
		
		g.setColor(Rs.C56);
		g.fillRect(652, 417, 236, 1);
	}
	
	private void printExtra(Graphics g, UIComponent c) {
		SceneSearchResult ssr = (SceneSearchResult)c;
		g.setColor(Color.green);
		g.setFont(Rs.F18);
		g.drawString("Start From-init: " + SearchController.getInstance().getLaunchPlatform() + " - " + SearchController.getInstance().getInitValue(), 253, 30);
		g.drawString("Search Keyword: \"" + Util.getExtraString(ssr.pSearchKeyword) +"\"", 253, 45);
		g.drawString("Filter ID: " + ssr.pSearchFilterId +" ("+SearchFilterDef.getSectionFilterText(ssr.pSearchFilterId)+")", 253, 60);
		g.drawString("Letter Search?(false is keyword): " + ssr.isSearchedLetters, 253, 75);
		g.drawString("SortValue: " + ssr.curSortValue, 253, 90);
	}
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.DateUtil;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.resource.SearchedEpg;
import com.videotron.tvi.illico.search.ui.component.ResultList;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * <code>RendererResultListLive</code>
 * 
 * @version $Id: RendererResultListLive.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 1.
 * @description
 */
public class RendererResultListLive extends RendererResultList{
	private Image imgIconPpvF, imgIconPpvB, imgIconFreePreviewF, imgIconFreePreviewB;
	private String txtSeason, txtEpisode;
	
	protected void prepareSub(UIComponent c) {
		imgIconPpvF = DataCenter.getInstance().getImage("icon_ppv_f.png");
		imgIconPpvB = DataCenter.getInstance().getImage("icon_ppv.png");
		imgIconFreePreviewF = DataCenter.getInstance().getImage("03_srcicon_ppv_foc.png");
		imgIconFreePreviewB = DataCenter.getInstance().getImage("03_srcicon_ppv.png");
		
		txtSeason = DataCenter.getInstance().getString("Label.Season");
		txtEpisode = DataCenter.getInstance().getString("Label.Episodes");
	}

	protected void drawItem(Graphics g, Resource resource, int i, ResultList resultList) {
		boolean isFocused = resultList.isFocused() && resultList.curIdx == resultList.curPageStartIdx + i;
		Color cDisplayInfo = null;
		Color cDate = null;
		
		if (isFocused) {
			g.setColor(Rs.C246_199_0);
			g.fillRect(54 - ResultList.BASE_X, 166 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), 576, 52);
			g.setColor(Rs.C56);
			g.fillRect(526 - ResultList.BASE_X, 167 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), 103, 50);
			
			cDisplayInfo = Rs.C0;
			cDate = Rs.C0;
		} else {
			cDisplayInfo = Rs.C139;
			cDate = Rs.C214;
		}

		SearchedEpg searchedLinear = (SearchedEpg)resource;
		if (searchedLinear == null) {
			drawInvalidTitle(g, i, resultList, isFocused);
			return;
		}
		
		Image imgIconFreePreview = null;
		Image imgIconPpv = null;
		if (searchedLinear.isPayPerView()) {
			if (isFocused) {
				imgIconPpv = imgIconPpvF;
			} else {
				imgIconPpv = imgIconPpvB;
			}
		} else {
			if (searchedLinear.isFreePreview()) {
				if (isFocused) {
					imgIconFreePreview = imgIconFreePreviewF;
				} else {
					imgIconFreePreview = imgIconFreePreviewB;
				}
			}
		}
		
		int posAlignLeftX = 520 - ResultList.BASE_X;
		if (imgIconPpv != null) {
			int imgIconPpvW = imgIconPpv.getWidth(resultList);
			g.drawImage(imgIconPpv, posAlignLeftX - imgIconPpvW, 185 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), resultList);
			posAlignLeftX -= imgIconPpvW - 3;
		}
		if (imgIconFreePreview != null) {
			int imgIconFreePreviewW = imgIconFreePreview.getWidth(resultList);
			g.drawImage(imgIconFreePreview, posAlignLeftX - imgIconFreePreviewW, 185 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), resultList);
		}
		
		drawTitle(g, searchedLinear, i, resultList, isFocused);
		
		SearchedEpg.HashData type = searchedLinear.getType();
		int typeCode = type.getCode();
		String displayInfo = null;
		if (typeCode == Definition.PROGRAM_TYPE_SERIES) {
			String seasonNo = searchedLinear.getSeasonNo();
			String episodeNo = searchedLinear.getEpisodeNo();
			if (seasonNo != null && episodeNo != null) {
				displayInfo = txtSeason + " " + seasonNo + ", " + txtEpisode + " " + episodeNo;
			}
		}
		
		if (displayInfo == null) {
			displayInfo = searchedLinear.getType().getTitle();
		}
		g.setColor(cDisplayInfo);
		g.setFont(Rs.F17);
		if (displayInfo != null) {
			g.drawString(displayInfo, 70 - ResultList.BASE_X, 207 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		}
		
		g.setColor(cDate);
		long startMillis = searchedLinear.getStartTimeMillis();
		String dateStr = DateUtil.getDateString(startMillis);
		g.drawString (dateStr, 388 - ResultList.BASE_X, 189 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		String timeStr = DateUtil.getTimeString(startMillis);
		g.drawString (timeStr, 388 - ResultList.BASE_X, 205 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		
		Image imgChLogo = (Image) SharedMemory.getInstance().get("logo." + searchedLinear.getChannelCallLetter());
		if (imgChLogo == null) {
			imgChLogo = (Image) SharedMemory.getInstance().get("logo.default");
		}
		int imgChLogoW = imgChLogo.getWidth(resultList);
		g.drawImage(imgChLogo, 580 - ResultList.BASE_X - (imgChLogoW / 2), 180 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), resultList);
		
		if (Log.EXTRA_ON) {
			g.setColor(Color.green);
			g.setFont(Rs.F18);
			g.drawString("ChLogoW: " + imgChLogoW , 580 - ResultList.BASE_X - (imgChLogoW / 2), 180 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		}
	}
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.SearchDataManager;
import com.videotron.tvi.illico.search.ui.component.InputBox;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>RendererInputBox</code>
 * 
 * @version $Id: RendererInputBox.java,v 1.4.2.1 2017/03/27 15:28:48 freelife Exp $
 * @author ZioN
 * @since 2015. 3. 31.
 * @description
 */
public class RendererInputBox extends Renderer {
	private static final int SUGGESTED_KEYWORD_LIST_GAP = 40;
	
	private Image imgInputboxN, imgInputboxF, imgInputboxInactive, imgArrowU, imgArrowD;
	private String txtSearch;
	private String txtSuggestedKeywords;
	private String txtSearchKeyword;
	private int txtSearchW;
	private int txtSearchKeywordW;
	
	private Image imgIconAlert;
	private String txtErrorTitle;
	private String txtErrorCode;
	private int txtErrorCodeW;
	
	private String[] instructionTxts;
	private String instuctionTxtEmphasis;
	private int lastInstTxtW;
	
	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(444, 175, 480, 285);
	}

	public void prepare(UIComponent c) {
		
		imgArrowU = DataCenter.getInstance().getImage("02_ars_t.png");
		imgArrowD = DataCenter.getInstance().getImage("02_ars_b.png");
		
		imgInputboxN = DataCenter.getInstance().getImage("03_src_foc_dim.png");
		imgInputboxF = DataCenter.getInstance().getImage("03_src_foc.png");
		imgInputboxInactive = DataCenter.getInstance().getImage("03_src_foc_inactive.png");
		
		txtSearch = DataCenter.getInstance().getString("Label.Search_Result");
		txtSearchW = Rs.FM19.stringWidth(txtSearch);
		
		txtSuggestedKeywords = DataCenter.getInstance().getString("Label.Suggested_Keywords");
		txtSearchKeyword = DataCenter.getInstance().getString("Label.Search_Keyword");
		txtSearchKeywordW = Rs.FM19.stringWidth(txtSearchKeyword);
		
		String txtInstructions = DataCenter.getInstance().getString("Descriptions.Keyword_Instruction");
		instructionTxts = TextUtil.split(txtInstructions, Rs.FM16, 430, '|');
		instuctionTxtEmphasis = DataCenter.getInstance().getString("Descriptions.Keyword_Instruction_Emphasis");
		
		if (instructionTxts != null && instructionTxts.length > 0) {
			lastInstTxtW = Rs.FM19.stringWidth(instructionTxts[instructionTxts.length-1]);
		}
		txtErrorTitle = DataCenter.getInstance().getString("Label.Technical_Problem");
		imgIconAlert = DataCenter.getInstance().getImage("icon_noti_red.png");
		txtErrorCode = DataCenter.getInstance().getString("Label.Error_Code");
		txtErrorCodeW = Rs.FM16.stringWidth(txtErrorCode);
	}
	
	public void paint(Graphics g, UIComponent c) {
		if (Log.EXTRA_ON) {
			printExtra(g, c);
		}		
		InputBox inputBox = (InputBox)c;
		
		drawInputbox(g, inputBox);
		if (Log.DEBUG_ON) {
			Log.printDebug("[RendererInputBox.paint] inputBox.searchText: " + inputBox.searchText);
		}
		if (inputBox.searchText == null || inputBox.searchText.trim().length() < 2) {
			drawInstructions(g, inputBox);
		} else {
			if (inputBox.exception != null) {
				drawException(g, inputBox);
			} else {
				drawSuggestedKeywordList(g, inputBox);
			}
		}
	}
	
	private void drawInputbox(Graphics g, InputBox inputBox) {
		boolean isSelectable = inputBox.isSelectable();
		boolean isFocused = inputBox.isFocused() && inputBox.isInputboxFocused;
		boolean isDimed = inputBox.isReadyToClear();
		boolean isFocusedSuggest = inputBox.isFocused() && !inputBox.isInputboxFocused;
		
		Image imgInputbox = null;
		Color cTxtSearch = null;
		Color cCorsur = null;
		if (isDimed) {
			cCorsur = Rs.C170;
		} else {
			cCorsur = Rs.C246_199_0;
		}
		if (isSelectable) {
			if (isFocused) {
				imgInputbox = imgInputboxF;
				cTxtSearch = Rs.C0;
//				cCorsur = Rs.C246_199_0;
			} else {
				imgInputbox = imgInputboxN;
				cTxtSearch = Rs.C255;
//				cCorsur = Rs.C255;
			}
		} else {
			imgInputbox = imgInputboxInactive;
			cTxtSearch = Rs.C118;
//			cCorsur = Rs.C170;
		}
		if (imgInputbox != null) {
			g.drawImage(imgInputbox, 0, 0, 465, 38, inputBox);
		}
		
		g.setFont(Rs.F19);
		g.setColor(cTxtSearch);
		Log.printDebug("[RendererInputBox.drawInputbox] fromSearchKeyword " + inputBox.fromSearchKeyword);
		if (inputBox.fromSearchKeyword)
			g.drawString(txtSearchKeyword, 366-(txtSearchKeywordW/2), 25);
		else
			g.drawString(txtSearch, 366 - (txtSearchW / 2), 25);
		
		if (isSelectable && isFocused && inputBox.suggestedKeywords != null) {
			if (inputBox.isInputboxFocused && inputBox.suggestedKeywords.length > 0)
				g.drawImage(imgArrowD, 222, 37, inputBox);			
		}
		
		if (inputBox.searchDisplayText != null) {
			g.setFont(Rs.F22);
			if (isDimed) {
				g.setColor(Rs.C77);
				if (inputBox.isOverInputboxWidth) {
					g.fillRect(250 - inputBox.searchDisplayTextW - 3 - 3, 6, inputBox.searchDisplayTextW + 6, 26);
				} else {
					g.fillRect(13 - 3, 6, inputBox.searchDisplayTextW + 6, 26);
				}
				g.setColor(Rs.C241);
			} else {
				g.setColor(Rs.C241);
			}
			if (inputBox.isOverInputboxWidth) {
				g.drawString(inputBox.searchDisplayText, 250 - inputBox.searchDisplayTextW - 3, 25);
			} else {
				g.drawString(inputBox.searchDisplayText, 13, 25);
			}
		}
		if (!isFocusedSuggest) {
			g.setColor(cCorsur);
			if (inputBox.isOverInputboxWidth) {			
				g.fillRect(250, 25, 8, 3);
			} else {
				int cursorX = 13 + inputBox.searchDisplayTextW;
				if (inputBox.searchDisplayTextW > 0) {
					cursorX+=3;
				}
				g.fillRect(cursorX, 25, 8, 3);
			}
		}
	}
	
	private void drawInstructions(Graphics g, InputBox inputBox) {
		if (instructionTxts != null) {
			g.setFont(Rs.F19);
			g.setColor(Rs.C192);
			for (int i=0; i<instructionTxts.length; i++) {
				
				if (i==instructionTxts.length -1){ 
					g.drawString(instructionTxts[i], 2, 66 + (24*i));
					g.setFont(Rs.F19_B);
					g.drawString(instuctionTxtEmphasis, lastInstTxtW+2+6, 66 + (24*i));
				} else
					g.drawString(instructionTxts[i], 2, 66 + (24*i));
			}
		}
	}
	
	private void drawException(Graphics g, InputBox inputBox) {
		g.drawImage(imgIconAlert, 446 - 444, 240 - 175, inputBox);
		g.setFont(Rs.F22);
		g.setColor(Rs.C224);
		g.drawString(txtErrorTitle, 476 - 444, 257 - 175);
		
		g.setFont(Rs.F16);
		g.setColor(Rs.C224);
		int posX = 446 - 444;
		g.drawString(txtErrorCode, 445 - 444, 400 - 175);
		posX = txtErrorCodeW + 4;
		String errorCode = inputBox.exception.getCode();
		g.drawString(errorCode, posX, 400 - 175);
		
		// Draws error message
		String[] errMsg = null;
		if (errorCode != null) {
			errMsg = TextUtil.split(SearchDataManager.getInstance().getErrorMsg(errorCode), Rs.FM16, 436, 5);
		}
		if (errMsg != null && errMsg.length > 0) {
			g.setColor(Rs.C224);
			g.setFont(Rs.F16);
			int posY = 330 - 175 - (12 * (errMsg.length - 1));
			for (int i=0; i<errMsg.length; i++) {
				g.drawString(errMsg[i], 445 - 444, posY + (i * 24));
			}
		}
	}
	
	private void drawSuggestedKeywordList(Graphics g, InputBox inputBox) {
		if (inputBox.suggestedKeywords != null && inputBox.suggestedKeywords.length > 0) {
			
			//R7.3 prototype 1
			g.setFont(Rs.F19);
			g.setColor(Rs.C139);
			g.drawString(txtSuggestedKeywords, 0, 62);
						
			for (int i=0; i<inputBox.suggestedKeywords.length && i<5; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[RendererInputBox.drawSuggestedKeywordList]suggestedKeywords["+i+"]: " + inputBox.suggestedKeywords[i]);
				}
				int posY = 39 + (SUGGESTED_KEYWORD_LIST_GAP * i) + 39; 
				drawSuggestedKeywordListItem(g, inputBox.suggestedKeywords[i], inputBox.suggestedShortenKeywords[i], i, posY, inputBox);
				if (i != inputBox.suggestedKeywords.length - 1 && i<4) {
					drawItemLine(g, posY);
				}
			}
		}
	}
	
	private void drawSuggestedKeywordListItem(Graphics g, String item, String shortenItem, int index, int posY, InputBox inputBox) {
		String remain = item;
		final int START_EM_TAG_LTH = Rs.TAG_EM_START.length();
		final int END_EM_TAG_LTH = Rs.TAG_EM_END.length();
		
		int emStartIdx = remain.indexOf(Rs.TAG_EM_START);
		int emEndIdx = remain.indexOf(Rs.TAG_EM_END);
		
		boolean isFocusedItem = (inputBox.isFocused() && !inputBox.isInputboxFocused && index == inputBox.suggestedKeywordIdx);
		if (isFocusedItem) {
			g.drawImage(imgArrowU, 222, posY-14, inputBox);
			if (index < 4 && index != (inputBox.suggestedKeywords.length-1)) {
				g.drawImage(imgArrowD, 222, posY+38, inputBox);
			}
			
			g.drawImage(imgInputboxF, 0, posY, 465, 41, inputBox);
			
			g.setColor(Rs.C0);
			g.drawString(txtSearchKeyword, 366-(txtSearchKeywordW/2), posY+26);
		}
		
		int posX = 13;
		g.setFont(Rs.F19);
		
		if (isFocusedItem) {
			g.setColor(Rs.C255);
			g.drawString(shortenItem, posX, posY + 26);
		} else {
			g.setColor(Rs.C246_199_0);
			while(emStartIdx != -1 && emEndIdx != -1) {
				String drawTxt = null;
				int drawTxtW = 0;
				if (emStartIdx > 0) {
					drawTxt = remain.substring(0, emStartIdx);
					drawTxtW = Rs.FM19.stringWidth(drawTxt);
					
					g.setColor(Rs.C255);
					
					g.drawString(drawTxt, posX, posY + 26);
					posX += drawTxtW;
				}
				drawTxt = remain.substring(emStartIdx + START_EM_TAG_LTH, emEndIdx);
				drawTxtW = Rs.FM19.stringWidth(drawTxt);
				if (isFocusedItem) {
					g.setColor(Rs.C255);
				} else {
					g.setColor(Rs.C246_199_0);
				}
				g.drawString(drawTxt, posX, posY + 26);
				posX += drawTxtW;
				
				remain = remain.substring(emEndIdx + END_EM_TAG_LTH);
				emStartIdx = remain.indexOf(Rs.TAG_EM_START);
				emEndIdx = remain.indexOf(Rs.TAG_EM_END);
			}
			if (remain.length() > 0) {			
				g.setColor(Rs.C255);
				g.drawString(remain, posX, posY + 26);
			}
		}
	}
	
	private void drawItemLine(Graphics g, int posY) {
		g.setColor(Rs.C60);
		g.fillRect(0, posY + 40, 465, 1);
	}

	private void printExtra(Graphics g, UIComponent c) {
		g.setColor(Color.green);
		g.setFont(Rs.F18);
		InputBox inputBox = (InputBox)c;
		String[] requestState = TextUtil.split(inputBox.requestState, Rs.FM18, 230);
		
		if (requestState != null) {
			for (int i = 0 ; i<requestState.length; i++) {
				g.drawString(requestState[i], 672-444, 234-175 + (i*15));
			}
		}
	}
}

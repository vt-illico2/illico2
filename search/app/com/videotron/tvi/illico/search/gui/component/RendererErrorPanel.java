/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.ui.component.ErrorPanel;

/**
 * <code>RendererErrorPanel</code>
 * 
 * @version $Id: RendererErrorPanel.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 14.
 * @description
 */
public class RendererErrorPanel extends Renderer{
	private static final int BASE_X = 54;
	private static final int BASE_Y = 167;
	private Image imgIconOk, imgIconAlert;
	private String txtBack;
	private String txtErrorCode;
	private int txtErrorCodeW;
	
	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(54, 167, 855, 301);
	}

	public void prepare(UIComponent c) {
		imgIconOk = DataCenter.getInstance().getImage("b_btn_ok.png");
		imgIconAlert = DataCenter.getInstance().getImage("icon_noti_or.png");
		txtBack = DataCenter.getInstance().getString("Label.result_back_key");
		txtErrorCode = DataCenter.getInstance().getString("Label.Error_Code");
		txtErrorCodeW = Rs.FM15.stringWidth(txtErrorCode);
	}

	protected void paint(Graphics g, UIComponent c) {
		drawBackground(g, c);
		drawMessage(g, c);
	}
	
	private void drawBackground(Graphics g, UIComponent c) {
		g.setColor(Rs.C56);
		g.fillRect(54 - BASE_X, 167 - BASE_Y, 576, 301);
		g.setColor(Rs.C39);
		for (int i = 0; i< 5; i++) {
			g.fillRect(69 - BASE_X, 217 - BASE_Y + (i * 50), 546, 1);
		}
		g.setColor(Rs.C77);
		g.fillRect(630 - BASE_X, 167 - BASE_Y, 279, 301);
		g.setColor(Rs.C56);
		g.fillRect(652 - BASE_X, 417 - BASE_Y, 236, 1);
		
		g.drawImage(imgIconOk, 652 - BASE_X, 428 - BASE_Y, c);
		g.setFont(Rs.F18);
		g.setColor(Rs.C255);
		g.drawString(txtBack, 676 - BASE_X, 442 - BASE_Y);
		
		g.setColor(Rs.C246_199_0);
		g.fillRect(54 - BASE_X, 167 - BASE_Y, 576, 52);
	}
	
	private void drawMessage(Graphics g, UIComponent c) {
		ErrorPanel errorPanel = (ErrorPanel)c;
		if (errorPanel.itemMessage != null) {
			int posX = 69 - BASE_X;
			if (errorPanel.errorCode != null) {
				g.drawImage(imgIconAlert, posX, 180 - BASE_Y, c);
				posX += imgIconAlert.getWidth(c) + 8;
			}
			g.setFont(Rs.F20);
			g.setColor(Rs.C0);
			g.drawString(errorPanel.itemMessage, posX, 197 - BASE_Y);
		}
		
		if (errorPanel.detailTitle != null) {
			g.setFont(Rs.F22);
			g.setColor(Rs.C255);
			g.drawString(errorPanel.detailTitle, 652 - BASE_X, 202 - BASE_Y);
		}
		
		if (errorPanel.detailDescs != null) {
			g.setFont(Rs.F16);
			g.setColor(Rs.C214);
			for (int i = 0 ; i<errorPanel.detailDescs.length; i++) {
				g.drawString(errorPanel.detailDescs[i], 652 - BASE_X, 246 - BASE_Y + (i * 16));
			}
		}
		
		if (errorPanel.errorCode != null) {
			g.setFont(Rs.F15);
			g.setColor(Rs.C214);
			int posX = 652 - BASE_X;
			g.drawString(txtErrorCode, posX, 396 - BASE_Y);
			posX += txtErrorCodeW + 4;
			g.drawString(errorPanel.errorCode, posX, 396 - BASE_Y);
		}
	}
}

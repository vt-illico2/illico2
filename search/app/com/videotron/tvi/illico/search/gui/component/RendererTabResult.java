/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.ui.component.ResultList;
import com.videotron.tvi.illico.search.ui.component.TabResult;
import com.videotron.tvi.illico.search.ui.component.TabResult.TabResultInfo;

/**
 * <code>RendererTabResult</code>
 * 
 * @version $Id: RendererTabResult.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 1.
 * @description
 */
public class RendererTabResult extends Renderer {
	private static final int TAB_RESULT_GAP = 45;
	private Image imgArrowR, imgArrowL;

	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(54, 129, 855, 38);
	}

	public void prepare(UIComponent c) {
		imgArrowL = DataCenter.getInstance().getImage("02_ars_l_s.png");
		imgArrowR = DataCenter.getInstance().getImage("02_ars_r_s.png");
	}

	public void paint(Graphics g, UIComponent c) {
		TabResult tabResult = (TabResult) c;
		
		int tabResultVecSize = tabResult.tabResultVec.size();
		if (tabResultVecSize > 0) {
			// drawBackground
			drawBackground(g);
			drawItems(g, tabResult);
		}
	}
	
	private void drawBackground(Graphics g) {
//		g.setColor(Rs.C23);
//		g.fillRect(0, 0, 855, 38);
	}
	
	private void drawItems(Graphics g, TabResult tabResult) {
		final int BASE_X = 45;
		final int BASE_Y = 129;
		int tabResultVecSize = tabResult.tabResultVec.size();
		int posX = 26;
		g.setFont(Rs.F19);
		for (int i=0; i<tabResultVecSize; i++) {
			TabResultInfo tabResInfo = (TabResultInfo) tabResult.tabResultVec.get(i);
			if (i == tabResult.curTabResultIdx) {
				g.setColor(Rs.C56);
				g.fillRect(posX - 8, 0, tabResInfo.getTabResultDisplayTextWidth() + 16, 38);
				
				g.setColor(Rs.C246_199_0);
//				g.drawImage(imgArrowL, posX - 13, 142 - BASE_Y, tabResult);
//				g.drawImage(imgArrowR, posX + tabResInfo.getTabResultDisplayTextWidth() + 6, 142 - BASE_Y, tabResult);
				g.drawImage(imgArrowL, posX - 26, 142 - BASE_Y, tabResult);
				g.drawImage(imgArrowR, posX + tabResInfo.getTabResultDisplayTextWidth() + 19, 142 - BASE_Y, tabResult);
			} else {
				g.setColor(Rs.C227);
			}
			g.drawString(tabResInfo.getTabResultDisplayText(), posX, 154 - BASE_Y);
			posX += tabResInfo.getTabResultDisplayTextWidth() + TAB_RESULT_GAP;
		}
	}
}

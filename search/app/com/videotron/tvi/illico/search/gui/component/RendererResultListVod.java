/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.resource.SearchedVod;
import com.videotron.tvi.illico.search.ui.component.ResultList;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * <code>RendererResultListVod</code>
 * 
 * @version $Id: RendererResultListVod.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 1.
 * @description
 */
public class RendererResultListVod extends RendererResultList{
	private Image imgIconFreePreviewF, imgIconFreePreviewB;
	private Image imgLogoPay, imgLogoClub, imgLogoFree;
	
	private String txtSeason;
	
	protected void prepareSub(UIComponent c) {
		imgIconFreePreviewF = DataCenter.getInstance().getImage("03_srcicon_ppv_foc.png");
		imgIconFreePreviewB = DataCenter.getInstance().getImage("03_srcicon_ppv.png");
		
		imgLogoPay = DataCenter.getInstance().getImage("icon_pay.png");
		imgLogoClub = DataCenter.getInstance().getImage("icon_club.png");
		imgLogoFree = DataCenter.getInstance().getImageByLanguage("icon_free.png");
		
		txtSeason = DataCenter.getInstance().getString("Label.Season");
	}

	protected void drawItem(Graphics g, Resource resource, int i, ResultList resultList) {
		boolean isFocused = resultList.isFocused() && resultList.curIdx == resultList.curPageStartIdx + i;
		// draw focus
		Color cDisplayInfo = null;
		if (isFocused) {
			g.setColor(Rs.C246_199_0);
			g.fillRect(54 - ResultList.BASE_X, 166 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), 576, 52);
			g.setColor(Rs.C56);
			g.fillRect(526 - ResultList.BASE_X, 167 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), 103, 50);
			
			cDisplayInfo = Rs.C0;
		} else {
			cDisplayInfo = Rs.C139;
		}
		
		SearchedVod searchedVod = (SearchedVod)resource;
		if (searchedVod == null) {
			drawInvalidTitle(g, i, resultList, isFocused);
			return;
		}
		
		// draw Title
		drawTitle(g, searchedVod, i, resultList, isFocused);
		
		// draw Genre and Type
		g.setFont(Rs.F17);
		g.setColor(cDisplayInfo);
		SearchedVod.HashData type = searchedVod.getType();
		int typeCode = type.getCode();
		String displayInfo = type.getTitle();
		if (typeCode == Definition.VOD_TYPE_SERIES) {
			String seasonNo = searchedVod.getSeasonNo();
			if (seasonNo != null) {
				displayInfo += " | " + txtSeason + " " + seasonNo;
			}
		} else {
			SearchedVod.HashData[] genres = searchedVod.getGenres();
			if (genres != null && genres.length > 0) {
				String displayGenres = "";
				for (int ii = 0; ii < genres.length; ii++) {
					displayGenres += genres[ii].getTitle();
					if (ii != genres.length - 1) {
						displayGenres += ", ";
					}
				}
				displayInfo += " | " + displayGenres;
			}
		}
		g.drawString(displayInfo, 70 - ResultList.BASE_X, 207 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		
		// draw Icon and Logo
		Image imgIconFreePreview = null;
		if (searchedVod.isChannelFreePreview()) {
			if (isFocused) {
				imgIconFreePreview = imgIconFreePreviewF;
			} else {
				imgIconFreePreview = imgIconFreePreviewB;
			}
		}
		if (imgIconFreePreview != null) {
			g.drawImage(imgIconFreePreview, 502 - ResultList.BASE_X, 185 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), resultList);
		}
		Image imgItemLogo = getItemLogo(searchedVod);
		int imgItemLogoW = -1;
		if (imgItemLogo != null) {
			imgItemLogoW = imgItemLogo.getWidth(resultList);
			g.drawImage(imgItemLogo, 580 - ResultList.BASE_X - (imgItemLogoW / 2), 180 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), resultList);
		}
		if (Log.EXTRA_ON) {
			g.setColor(Color.green);
			g.setFont(Rs.F18);
			g.drawString("ChLogoW: " + imgItemLogoW, 580 - ResultList.BASE_X - (imgItemLogoW / 2), 180 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		}
	}
	
	private Image getItemLogo(SearchedVod searchedVod) {
		Image imgItemLogo = null;
		if (searchedVod.isChannelOnDemandContent()) {
			String callLetter = searchedVod.getChannelCallLetter();
			imgItemLogo = (Image) SharedMemory.getInstance().get("logo." + callLetter);
			if (imgItemLogo == null) {
				imgItemLogo = (Image) SharedMemory.getInstance().get("logo.default");
			}
		} else if (searchedVod.isClubContent()) {
			imgItemLogo = imgLogoClub;
		} else { // This is regular item, Pay or Free
			if (searchedVod.isFreeContent()) {
				imgItemLogo = imgLogoFree;
			} else {
				imgItemLogo = imgLogoPay;
			}
		}
		return imgItemLogo;
	}
}

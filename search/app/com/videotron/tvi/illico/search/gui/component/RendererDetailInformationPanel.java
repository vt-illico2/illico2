/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.resource.SearchedEpg;
import com.videotron.tvi.illico.search.data.resource.SearchedPvr;
import com.videotron.tvi.illico.search.data.resource.SearchedPvrGroup;
import com.videotron.tvi.illico.search.data.resource.SearchedVod;
import com.videotron.tvi.illico.search.ui.component.DetailInformationPanel;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>DetailInformationPanel</code>
 * 
 * @version $Id: RendererDetailInformationPanel.java,v 1.4.2.1 2017/03/30 11:54:43 freelife Exp $
 * @author Administrator
 * @since 2015. 4. 2.
 * @description
 */
public class RendererDetailInformationPanel extends Renderer {
	protected static final int BASE_X = 630;
	protected static final int BASE_Y = 167;
	
	private Image imgIconOK;
	private String txtColor;
	
	private Image imgIconRatingG, imgIconRating08, imgIconRating13, imgIconRating16, imgIconRating18;
	private Image imgIconBigStandard, imgIconBigHD1080, imgIconBigHD, imgIconBig3D, imgIconBigUHD;
	private Image imgIconStandard, imgIconHD1080, imgIconHD, imgIcon3D, imgIconUHD;
	private Image imgIconEn, imgIconFr;
	
	private Image imgPosterDefaultPortrait, imgPosterDefaultLandscape;
	
	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(630, 167, 279, 301);
	}

	public void prepare(UIComponent c) {
		imgIconOK = DataCenter.getInstance().getImage("b_btn_ok.png");
		txtColor = DataCenter.getInstance().getString("Descriptions.Result_Ok");
		
		imgIconRatingG = DataCenter.getInstance().getImage("icon_g.png");
		imgIconRating08 = DataCenter.getInstance().getImage("icon_8.png");
		imgIconRating13 = DataCenter.getInstance().getImage("icon_13.png");
		imgIconRating16 = DataCenter.getInstance().getImage("icon_16.png");
		imgIconRating18 = DataCenter.getInstance().getImage("icon_18.png");
		
		
		imgIconBigStandard = DataCenter.getInstance().getImage("01_icon_sd_s.png");
		imgIconBigHD1080 = DataCenter.getInstance().getImage("01_icon_fhd_s.png");
		imgIconBigHD = DataCenter.getInstance().getImage("01_icon_hd_s.png");
		imgIconBig3D = DataCenter.getInstance().getImage("icon_3d.png");
		imgIconBigUHD = DataCenter.getInstance().getImage("01_icon_uhd_s.png");
		
		imgIconStandard = DataCenter.getInstance().getImage("01_icon_sd_s.png");
		imgIconHD = DataCenter.getInstance().getImage("icon_hd.png");
		imgIconHD1080 = DataCenter.getInstance().getImage("01_icon_fhd_s.png");
		imgIcon3D = DataCenter.getInstance().getImage("icon_3d.png");
		imgIconUHD = DataCenter.getInstance().getImage("icon_uhd.png");
		
		imgIconFr = DataCenter.getInstance().getImage("icon_fr.png");
		imgIconEn = DataCenter.getInstance().getImage("icon_en.png");
		
		imgPosterDefaultPortrait = DataCenter.getInstance().getImage("VOD_default_S_Portrait.png");
		imgPosterDefaultLandscape = DataCenter.getInstance().getImage("VOD_default_S_Landscape.png");
	}

	protected void paint(Graphics g, UIComponent c) {
		drawBackground(g);
		
		Resource resource = ((DetailInformationPanel)c).rsc;
		if (resource != null) {
			if (resource instanceof SearchedEpg) {
				drawDetailInformationEpg(g, c, (SearchedEpg)resource);
			} else if (resource instanceof SearchedVod) {
				drawDetailInformationVod(g, c, (SearchedVod)resource);
			}  else if (resource instanceof SearchedPvrGroup) {
				drawDetailInformationPvr(g, c, (SearchedPvrGroup)resource);
			} 
		}
		drawColorKey(g, c);
	}
	
	private void drawBackground(Graphics g) {
		g.setColor(Rs.C77);
		g.fillRect(630 - BASE_X, 167 - BASE_Y, 279, 301);
	}
	
	private void drawColorKey(Graphics g, UIComponent c) {
		g.setColor(Rs.C56);
		g.fillRect(652 - BASE_X, 417 - BASE_Y, 236, 1);
		
		g.drawImage(imgIconOK, 652 - BASE_X, 428 - BASE_Y, c);
		
		g.setFont(Rs.F18);
		g.setColor(Rs.C255);
		g.drawString(txtColor, 676 - BASE_X, 442 - BASE_Y);
	}
	
	private void drawDetailInformationEpg(Graphics g, UIComponent c, SearchedEpg searchedLinear) {
		int posX = 653 - BASE_X;
		
		String chGridPos = searchedLinear.getChannelGridPositions();
		if (chGridPos != null) {
			g.setFont(Rs.F18);
			g.setColor(Rs.C200);
			g.drawString(chGridPos, posX, 202 - BASE_Y);
			posX += Rs.FM18.stringWidth(chGridPos) + 8;
		}
		
		Image imgIconDefinition = getChannelDefinitionIcon(searchedLinear);
//		String definition = searchedLinear.getDefinition();
//		if (definition != null) {
//			if (definition.toUpperCase().equals(Definition.FORM_HD)) {
//				imgIconDefinition = imgIconHD;
//			} else if (definition.toUpperCase().equals(Definition.FORM_UHD)) {
//				imgIconDefinition = imgIconUHD;
//			}
//		}
		if (imgIconDefinition != null) {
			g.drawImage(imgIconDefinition, posX, 190 - BASE_Y, c);
		}
		
		String chName = searchedLinear.getChannelName();
		if (chName != null) {
			g.setFont(Rs.F20);
			g.setColor(Rs.C254);
			chName = TextUtil.shorten(chName, Rs.FM20, 236);
			g.drawString(chName, 653 - BASE_X, 222 - BASE_Y);
		}
		
		g.setColor(Rs.C56);
		g.fillRect(652 - BASE_X, 239 - BASE_Y, 236, 1);
		
		posX = 653 - BASE_X;
		SearchedEpg.HashData[] genres = searchedLinear.getGenres();
		if (genres != null && genres.length > 0) {
			g.setColor(Rs.C235);
			g.setFont(Rs.F18);
			String displayGenres = "";
			for (int i=0; i<genres.length; i++) {
				displayGenres += genres[i].getTitle();
				if (i != genres.length - 1) {
					displayGenres +=", ";
				}
			}
			g.drawString(displayGenres, posX, 268 - BASE_Y);
			posX += Rs.FM18.stringWidth(displayGenres) + 6;
		}
		
		String rating = searchedLinear.getRating();
		if (rating != null) {
			Image imgIconRating = null;
			if (rating.equals(Definition.RATING_G)) {
				imgIconRating = imgIconRatingG;
			} else if (rating.equals(Definition.RATING_8)) {
				imgIconRating = imgIconRating08;
			} else if (rating.equals(Definition.RATING_13)) {
				imgIconRating = imgIconRating13;
			} else if (rating.equals(Definition.RATING_16)) {
				imgIconRating = imgIconRating16;
			} else if (rating.equals(Definition.RATING_18)) {
				imgIconRating = imgIconRating18;
			}
			if (imgIconRating != null) {
				g.drawImage(imgIconRating, posX, 257 - BASE_Y, c);
			}
		}
		String description = searchedLinear.getDescription();
		int descriptionW = 0;
		if (description != null) {
			descriptionW = Rs.FM16.stringWidth(description);
		}
		if (descriptionW > 0) {
			String[] desciptions = TextUtil.split(description, Rs.FM16, 230, 7);
			if (desciptions != null && desciptions.length > 0) {
				g.setFont(Rs.F16);
				g.setColor(Rs.C215);
				for (int i = 0; i < desciptions.length; i++) {
					g.drawString(desciptions[i], 653 - BASE_X, 295 - BASE_Y + (i * 18));
				}
			}
		}
		if (Log.EXTRA_ON) {
			g.setColor(Color.green);
			g.setFont(Rs.F18);
			g.drawString("CallLetter: \"" + Util.getExtraString(searchedLinear.getChannelCallLetter()) +"\"", 752 - BASE_X, 232 - BASE_Y);
			g.drawString("StartTimeMillis: \"" + searchedLinear.getStartTimeMillis() +"\"", 752 - BASE_X, 247 - BASE_Y);
		}
	}
	
	private Image getChannelDefinitionIcon(SearchedEpg searchedLinear) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[RendererDetailInformationPanel.getChannelDefinitionIcon] Called.");
		}
		Image imgIconDefinition = null;
		String callLetter = searchedLinear.getChannelCallLetter();
		if (Log.DEBUG_ON) {
			Log.printDebug("[RendererDetailInformationPanel.getChannelDefinitionIcon] callLetter: " + callLetter);
		}
		int definition = -1;
		if (callLetter != null) {
			EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
			TvChannel tvChannel = null;
			try {
				tvChannel = epgService.getChannel(callLetter);
				if (Log.DEBUG_ON) {
					Log.printDebug("[RendererDetailInformationPanel.getChannelDefinitionIcon] tvChannel: " + tvChannel);
				}
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
			if (tvChannel != null) {
				try {
					definition = tvChannel.getDefinition();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("[RendererDetailInformationPanel.getChannelDefinitionIcon] definition: " + definition);
			}
			
			TvChannel tvSisterChannel = null;
			if (definition <= 0 && tvChannel != null) {
				try {
					tvSisterChannel = tvChannel.getSisterChannel();
					if (Log.DEBUG_ON) {
						Log.printDebug("[RendererDetailInformationPanel.getChannelDefinitionIcon] tvSisterChannel: " + tvSisterChannel);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				if (tvSisterChannel != null) {
					try {
						definition = tvSisterChannel.getDefinition();
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		switch(definition) {
			case TvChannel.DEFINITION_HD:
				imgIconDefinition = imgIconHD;
				break;
				
			case TvChannel.DEFINITION_4K:
				imgIconDefinition = imgIconUHD;
				break;
		}
		return imgIconDefinition;
	}
	
	private void drawDetailInformationVod(Graphics g, UIComponent c, SearchedVod searchedVod) {
		DetailInformationPanel resDetInfo = (DetailInformationPanel) c;
		
		boolean isPortrait = (searchedVod.getImageType() == SearchedVod.IMAGE_TYPE_PORTRAIT);
		if (Log.DEBUG_ON) {
			Log.printDebug("[RendererDetailInformationPanel.drawDetailInformationVod] isPortrait - info: " + isPortrait);
		}
		// draws Poster
		if (Log.DEBUG_ON) {
			Log.printDebug("[RendererDetailInformationPanel.drawDetailInformationVod] resDetInfo.requestor: " + resDetInfo.requestor);
		}
		Image imgPoster = null;
		if (resDetInfo.requestor != null) {
			imgPoster = resDetInfo.requestor.getPosterImage();
			if (Log.DEBUG_ON) {
				Log.printDebug("[RendererDetailInformationPanel.drawDetailInformationVod] imgPoster: " + imgPoster);
			}
		}
		if (imgPoster != null) {
			int posterW = imgPoster.getWidth(c);
			int posetH = imgPoster.getHeight(c);
			isPortrait = posetH > posterW;
			if (Log.DEBUG_ON) {
				Log.printDebug("[RendererDetailInformationPanel.drawDetailInformationVod] isPortrait- real: " + isPortrait);
			}
		} else {
			if (isPortrait) {
				imgPoster = imgPosterDefaultPortrait;
			} else {
				imgPoster = imgPosterDefaultLandscape;
			}
		}
		if (isPortrait) {
			g.drawImage(imgPoster, 652 - BASE_X, 189 - BASE_Y, 90, 135, resDetInfo);
		} else {
			g.drawImage(imgPoster, 652 - BASE_X, 189 - BASE_Y, 120, 90, resDetInfo);
		}
		// draws Icon - Definitions
		int posY = 189 - BASE_Y;
		boolean isSD = searchedVod.isAdaptedDefinition(Definition.FORM_SD);
		if (isSD) {
			g.drawImage(imgIconBigStandard, 890 - BASE_X  - imgIconBigStandard.getWidth(c), posY, c);
			posY += imgIconBigStandard.getHeight(c) + 3;
		}
		boolean isHD = searchedVod.isAdaptedDefinition(Definition.FORM_HD);
		if (isHD) {
			g.drawImage(imgIconBigHD, 890 - BASE_X - imgIconBigHD.getWidth(c), posY, c);
			posY += imgIconBigStandard.getHeight(c) + 3;
		}
		
		//R7.3 VDTRMASTER-6049
		boolean is1080P = searchedVod.isAdaptedDefinition(Definition.FORM_FHD);
		if (is1080P) {
			g.drawImage(imgIconBigHD1080, 890 - BASE_X  - imgIconBigHD1080.getWidth(c), posY, c);
			posY += imgIconBigStandard.getHeight(c) + 3;
		}
		
		boolean isUHD = searchedVod.isAdaptedDefinition(Definition.FORM_UHD);
		if (isUHD) {
			g.drawImage(imgIconBigUHD, 890 - BASE_X - imgIconBigUHD.getWidth(c), posY, c);
			posY += imgIconBigStandard.getHeight(c) + 3;
		}
		boolean is3D = searchedVod.isAdaptedDefinition(Definition.FORM_3D);
		if (is3D) {
			g.drawImage(imgIcon3D, 890 - BASE_X - imgIcon3D.getWidth(c), posY, c);
			posY += imgIconBigStandard.getHeight(c) + 3;
		}
		
		// draws Language
		int posX = 890 - BASE_X;
		if (isPortrait) {
			posY = 291 - BASE_Y;
		} else {
			posY = 246 - BASE_Y;
		}
		boolean isLanguageEn = searchedVod.isAdaptedLanguage(Definition.LANG_EN); 
		if (Log.DEBUG_ON) {
			Log.printDebug("[RendererDetailInformationPanel.drawDetailInformationVod]  isLanguageEn: " + isLanguageEn);
		}
		if (isLanguageEn) {
			g.drawImage(imgIconEn, posX - imgIconEn.getWidth(c), posY, c);
			posX = posX - imgIconEn.getWidth(c) - 1;
		}
		boolean isLanguageFr = searchedVod.isAdaptedLanguage(Definition.LANG_FR); 
		if (Log.DEBUG_ON) {
			Log.printDebug("[RendererDetailInformationPanel.drawDetailInformationVod]  isLanguageFr: " + isLanguageFr);
		}
		if (isLanguageFr) {
			g.drawImage(imgIconFr, posX - imgIconFr.getWidth(c), posY, c);
		}
		
		// draws Ratings
		if (isPortrait) {
			posY = 309 - BASE_Y;
		} else {
			posY = 264 - BASE_Y;
		}
		String rating = searchedVod.getRating();
		if (rating != null) {
			Image imgIconRating = null;
			if (rating.equals(Definition.RATING_G)) {
				imgIconRating = imgIconRatingG;
			} else if (rating.equals(Definition.RATING_8)) {
				imgIconRating = imgIconRating08;
			} else if (rating.equals(Definition.RATING_13)) {
				imgIconRating = imgIconRating13;
			} else if (rating.equals(Definition.RATING_16)) {
				imgIconRating = imgIconRating16;
			} else if (rating.equals(Definition.RATING_18)) {
				imgIconRating = imgIconRating18;
			}
			if (imgIconRating != null) {
				g.drawImage(imgIconRating, 864 - BASE_X, posY, c);
			}
		}
		
		// draws Synopsis
		String description = searchedVod.getDescription();
		
		SearchedVod.HashData type = searchedVod.getType();
		int typeCode = type.getCode();
		if (typeCode == Definition.VOD_TYPE_SERIES || typeCode == Definition.VOD_TYPE_MOVIE) {
			String originalTitle = searchedVod.getRealOriginalTitle();
			
			// VDTRMASTER-5675
			if (description == null || description.equals("null")) {
				description = originalTitle;
			} else {
				description = originalTitle + "\n" + description;
			}
		}
		int descriptionW = 0;
		if (description != null) {
			descriptionW = Rs.FM16.stringWidth(description);
		}
		if (descriptionW > 0) {
			int synopsisLineCount = 0;
			if (isPortrait) {
				synopsisLineCount = 4;
				posY = 346 - BASE_Y;
			} else {
				synopsisLineCount = 7;
				posY = 301 - BASE_Y;
			}
			String[] desciptions = TextUtil.split(description, Rs.FM16, 230, synopsisLineCount);
			if (desciptions != null && desciptions.length > 0) {
				g.setFont(Rs.F16);
				g.setColor(Rs.C215);
				for (int i = 0; i < desciptions.length; i++) {
					g.drawString(desciptions[i], 653 - BASE_X, posY + (i * 18));
				}
			}
		}
		
		if (Log.EXTRA_ON) {
			g.setColor(Color.green);
			g.setFont(Rs.F18);
			g.drawString("VOD ID: \"" + Util.getExtraString(searchedVod.getVodId()) +"\"", 752 - BASE_X, 232 - BASE_Y);
			g.drawString("Image: \"" + Util.getExtraString(searchedVod.getContentImage()) +"\"", 752 - BASE_X, 247 - BASE_Y);
			g.drawString("CallLetter: \"" + searchedVod.getChannelCallLetter() +"\"", 752 - BASE_X, 262 - BASE_Y);
		}
	}
	
	private void drawDetailInformationPvr(Graphics g, UIComponent c, SearchedPvrGroup searchedPvrGroup) {
		DetailInformationPanel resDetInfo = (DetailInformationPanel) c;
		
		SearchedPvr searchedPvr = searchedPvrGroup.getItem(0);
		int itemCount = searchedPvrGroup.getItemCount();
		boolean isGroup = itemCount > 1;
		boolean isMultipleChannelSource = searchedPvrGroup.isMultipleChannelSource();
		
		int posX = 653 - BASE_X;
		if (!isMultipleChannelSource) {
			g.setColor(Rs.C56);
			g.fillRect(652 - BASE_X, 239 - BASE_Y, 236, 1);

			// Channel Number
			String chNum = String.valueOf(searchedPvr.getChannelNumber());
			if (chNum != null) {
				g.setFont(Rs.F18);
				g.setColor(Rs.C200);
				g.drawString(chNum, posX, 202-BASE_Y);
				posX += Rs.FM18.stringWidth(chNum) + 6;
			}
			
			int definition = searchedPvr.getDefinition();
			Image imgIconDefinition = null;
			switch(definition) {
				case TvProgram.DEFINITION_SD:
//					imgIconDefinition = imgIconStandard;
					break;
				case TvProgram.DEFINITION_HD:
					imgIconDefinition = imgIconHD;
					break;
				case TvProgram.DEFINITION_4K:
					imgIconDefinition = imgIconUHD;
					break;
			}
			if (imgIconDefinition != null) {
				g.drawImage(imgIconDefinition, posX, 191 - BASE_Y, resDetInfo);
			}
			
			String chName = searchedPvr.getChannelName();
			if (chName != null) {
				g.setFont(Rs.F20);
				g.setColor(Rs.C254);
				chName = TextUtil.shorten(chName, Rs.FM20, 236);
				g.drawString(chName, 653 - BASE_X, 222 - BASE_Y);
			}
		}
		
		if (!isGroup) {
			posX = 650 - BASE_X;
			int category = searchedPvr.getCategory();
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.drawDetailInformationPvr] category: " + category);
			}
			String txtCategory = DataCenter.getInstance().getString("Categories." + category);
			if(txtCategory != null) {
				posX = 653 - BASE_X;
				g.setFont(Rs.F18);
				g.setColor(Rs.C235);
				g.drawString(txtCategory, posX, 268 - BASE_Y);
				posX += Rs.FM18.stringWidth(txtCategory) + 6;
			}
			Image imgRating = null;
			String rating = searchedPvr.getParentalRating();
			if (rating != null) {
				if (rating.equalsIgnoreCase(Definition.RATING_G)) {
					imgRating = imgIconRatingG;
				} else if (rating.equalsIgnoreCase(Definition.RATING_8)) {
					imgRating = imgIconRating08;
				} else if (rating.equalsIgnoreCase(Definition.RATING_13)) {
					imgRating = imgIconRating13;
				} else if (rating.equalsIgnoreCase(Definition.RATING_16)) {
					imgRating = imgIconRating16;
				} else if (rating.equalsIgnoreCase(Definition.RATING_18)) {
					imgRating = imgIconRating18;
				}
			}
			if  (imgRating != null) {
				g.drawImage(imgRating, posX, 257 - BASE_Y, resDetInfo);
			}
		}
		
		// draws Synopsis
		String description = null;
		if (isGroup) {
			String txtSeriesDetail = null;
			if (itemCount > 1) {
				txtSeriesDetail = DataCenter.getInstance().getString("Descriptions.Series_Detail_Plural");
			} else {
				txtSeriesDetail = DataCenter.getInstance().getString("Descriptions.Series_Detail_Singular");
			}
			description = itemCount + txtSeriesDetail;
		} else {
			description = searchedPvr.getSynopsis();
		}
		int descriptionW = 0;
		if (description != null) {
			descriptionW = Rs.FM16.stringWidth(description);
		}
		if (descriptionW > 0) {
			String[] desciptions = TextUtil.split(description, Rs.FM16, 230, 7);
			if (desciptions != null && desciptions.length > 0) {
				g.setFont(Rs.F16);
				g.setColor(Rs.C215);
				for (int i = 0; i < desciptions.length; i++) {
					g.drawString(desciptions[i], 653 - BASE_X, 295 - BASE_Y + (i * 18));
				}
			}
		}
		if (Log.EXTRA_ON) {
			g.setColor(Color.green);
			g.setFont(Rs.F18);
			g.drawString("PVR ID: \"" + Util.getExtraString(searchedPvr.getId()) +"\"", 652 - BASE_X, 235 - BASE_Y);
			g.drawString("PVR Genre: " + DataCenter.getInstance().getString("Categories."+searchedPvr.getCategory()), 652 - BASE_X, 250 - BASE_Y);
		}
	}
}

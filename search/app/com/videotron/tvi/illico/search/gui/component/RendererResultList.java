/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.ui.component.ResultList;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>RendererResultListLive</code>
 * 
 * @version $Id: RendererResultList.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 1.
 * @description
 */
abstract public class RendererResultList extends Renderer{
	protected static final int VALID_TITLE_W = 280;
	
	private Image imgArrowU, imgArrowD, imgIconAlert;
	private String txtInvalidTitle;

	public Rectangle getPreferredBounds(UIComponent c) {
		return new Rectangle(54, 158, 576, 319);
	}

	public void prepare(UIComponent c) {
		imgArrowU = DataCenter.getInstance().getImage("02_ars_t.png");
		imgArrowD = DataCenter.getInstance().getImage("02_ars_b.png");
		imgIconAlert = DataCenter.getInstance().getImage("icon_noti_or.png");
		txtInvalidTitle = DataCenter.getInstance().getString("Descriptions.Unavailable_Source_Detail_Title");
		prepareSub(c);
	}
	
	abstract protected void prepareSub(UIComponent c);

	public void paint(Graphics g, UIComponent c) {
		ResultList resultList = (ResultList) c;
		drawBackground(g, resultList);
		for (int i = 0; i<resultList.curPageCount; i++) {
			if (i != resultList.curPageCount - 1) {
				drawItemLine(g, i);
			}
		}
		for (int i = 0; i<resultList.curPageCount; i++) {
			if (resultList.adapter != null && i < resultList.adapter.getTotalCount()) {
				int index = resultList.curPageStartIdx + i;
				Resource resource = resultList.adapter.getResource(index);
				drawItem(g, resource, i, resultList);
			}
		}
	}
	
	private void drawBackground(Graphics g, ResultList resultList) {
		g.setColor(Rs.C56);
		g.fillRect(54 - ResultList.BASE_X, 167 - ResultList.BASE_Y, 576, 301);
		if (resultList.totalCount > ResultList.ROW && resultList.curPageStartIdx > 0) {
			g.drawImage(imgArrowU, 330 - ResultList.BASE_X, 158 - ResultList.BASE_Y, resultList);
		}
		if (resultList.totalCount > ResultList.ROW && resultList.curPageStartIdx + resultList.curPageCount < resultList.totalCount) {
			g.drawImage(imgArrowD, 330 - ResultList.BASE_X, 460 - ResultList.BASE_Y, resultList);
		}
	}
	
	private void drawItemLine(Graphics g, int i) {
		g.setColor(Rs.C39);
		g.fillRect(69 - ResultList.BASE_X, 217 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), 546, 1);
	}
	
	protected void drawInvalidTitle(Graphics g, int index, ResultList resultList, boolean isFocused) {
		int posX = 70 - ResultList.BASE_X;
		g.drawImage(imgIconAlert, posX, 180 - ResultList.BASE_Y + (index * ResultList.ROW_GAP), resultList);
		posX += imgIconAlert.getWidth(resultList) + 8;
		
		g.setFont(Rs.F20);
		if (isFocused) {
			g.setColor(Rs.C0);
		} else {
			g.setColor(Rs.C255);
		}
		g.drawString(txtInvalidTitle, posX, 197 - ResultList.BASE_Y + (index * ResultList.ROW_GAP));
	}
	
	/**
	 * Draw title.
	 * @param g Graphics
	 * @param resourceList ResourceList
	 * @param index int
	 * @param resultList ResultList
	 * @param isFocused boolean
	 */
	protected void drawTitle(Graphics g, ResourceList resourceList, int index, ResultList resultList, boolean isFocused) {
		g.setFont(Rs.F20);
		int posX = resultList.startPosX;
		int posY = resultList.startPosY + (index * ResultList.ROW_GAP);
		if (isFocused) {
			String realTitle = resourceList.getRealTitle();
			int realTitleW = Rs.FM20.stringWidth(realTitle);
			g.setColor(Rs.C0);
			if (realTitleW > resultList.validWth) {
				int cnt = Math.max(resultList.scroll - 10, 0);
				Rectangle ori = g.getClipBounds();
				try {
					g.clipRect(posX, posY - 20, resultList.validWth, 20);
					g.drawString(realTitle.substring(cnt), posX, posY);
				} catch (IndexOutOfBoundsException e) {
					resultList.scroll = 0;
				}
				g.setClip(ori);
			} else {
				g.drawString(realTitle, posX, posY);
			}
		} else {
			boolean isPersonFilter = resultList.searchParameter.getSearchFilterId() == SearchFilterDef.ID_PERSON;
			boolean useEmphasisTag = !isPersonFilter;
			if (useEmphasisTag) {
				/* Using <em>, </em> tag */
				int curTxtW = 0;
				String remain = resourceList.getTitle();
				int emStartIdx = remain.indexOf(Rs.TAG_EM_START);
				int emEndIdx = remain.indexOf(Rs.TAG_EM_END);
				while(emStartIdx != -1 && emEndIdx != -1) {
					String drawTxt = null;
					int drawTxtW = 0;
					boolean isExceedValidWth = false;
					if (emStartIdx > 0) {
						drawTxt = remain.substring(0, emStartIdx);
						drawTxtW = Rs.FM20.stringWidth(drawTxt);
						g.setColor(Rs.C255);
						isExceedValidWth = (curTxtW + drawTxtW > resultList.validWth);
						if (isExceedValidWth) {
							drawTxt = TextUtil.shorten(drawTxt, Rs.FM20, resultList.validWth - curTxtW);
							drawTxtW = Rs.FM20.stringWidth(drawTxt);
						}
						g.drawString(drawTxt, posX, posY);
						if (isExceedValidWth) {
							remain = "";
							break;
						}
						curTxtW += drawTxtW;
						posX += drawTxtW;
					}
					drawTxt = remain.substring(emStartIdx + Rs.TAG_EM_START_LTH, emEndIdx);
					drawTxtW = Rs.FM20.stringWidth(drawTxt);
					g.setColor(Rs.C246_199_0);
					isExceedValidWth = (curTxtW + drawTxtW > resultList.validWth);
					if (isExceedValidWth) {
						drawTxt = TextUtil.shorten(drawTxt, Rs.FM20, resultList.validWth - curTxtW);
						drawTxtW = Rs.FM20.stringWidth(drawTxt);
					}
					g.drawString(drawTxt, posX, posY);
					if (isExceedValidWth) {
						remain = "";
						break;
					}
					curTxtW += drawTxtW;
					posX += drawTxtW;
					
					remain = remain.substring(emEndIdx + Rs.TAG_EM_END_LTH);
					emStartIdx = remain.indexOf(Rs.TAG_EM_START);
					emEndIdx = remain.indexOf(Rs.TAG_EM_END);
				}
				if (remain.length() > 0) {
					int remainW = Rs.FM20.stringWidth(remain);
					if (curTxtW + remainW > resultList.validWth) {
						remain =  TextUtil.shorten(remain, Rs.FM20, resultList.validWth - curTxtW);
					}
					g.setColor(Rs.C255);
					g.drawString(remain, posX, posY);
				}
			} else {
				String shortenedRealTitle = resourceList.getShortenedRealTitle(Rs.FM20, resultList.validWth);
				g.setFont(Rs.F20);
				g.setColor(Rs.C255);
				g.drawString(shortenedRealTitle, posX, posY);
			}
		}
	}
	
	abstract protected void drawItem(Graphics g, Resource resource, int i, ResultList resultList);
}

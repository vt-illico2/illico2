/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.gui.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.data.resource.SearchedPerson;
import com.videotron.tvi.illico.search.data.resource.SearchedPvr;
import com.videotron.tvi.illico.search.data.resource.SearchedPvrGroup;
import com.videotron.tvi.illico.search.ui.component.ResultList;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>RendererResultListPvr</code>
 * 
 * @version $Id: RendererResultListPvr.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 1.
 * @description
 */
public class RendererResultListPvr extends RendererResultList{
	private Image imgIconGroupF, imgIconGroupB;
	private String txtEpisode, txtRecordings;
	
	protected void prepareSub(UIComponent c) {
		imgIconGroupF = DataCenter.getInstance().getImage("07_series_f.png");
		imgIconGroupB = DataCenter.getInstance().getImage("07_series.png");
		
		txtEpisode = DataCenter.getInstance().getString("Label.Episodes");
		txtRecordings = DataCenter.getInstance().getString("Label.Recordings");
	}

	protected void drawItem(Graphics g, Resource resource, int i, ResultList resultList) {
		SearchedPvrGroup searchedPvrGroup = (SearchedPvrGroup)resource;
		SearchedPvr searchedPvr = searchedPvrGroup.getItem(0);
		
		boolean isFocused = resultList.isFocused() && resultList.curIdx == resultList.curPageStartIdx + i;
		int itemCount = searchedPvrGroup.getItemCount();
		boolean isGroup = itemCount > 1;
		boolean isSeries = searchedPvr.getSeriesId() != null;
		boolean isMultipleCh = searchedPvrGroup.isMultipleChannelSource();
				
		Color cDisplayInfo = null;
		Color cDate = null;
		
		if (isFocused) {
			g.setColor(Rs.C246_199_0);
			g.fillRect(54 - ResultList.BASE_X, 166 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), 576, 52);
			if (!isMultipleCh) {
				g.setColor(Rs.C56);
				g.fillRect(526 - ResultList.BASE_X, 167 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), 103, 50);
			}
			cDisplayInfo = Rs.C0;
			cDate = Rs.C0;
		} else {
			cDisplayInfo = Rs.C139;
			cDate = Rs.C214;
		}

		drawTitle(g, searchedPvr, i, resultList, isFocused);
		
		int posX = 63 - ResultList.BASE_X;
		if (isGroup) {
			Image imgIconGroup = null;
			if (isFocused) {
				imgIconGroup = imgIconGroupF;
			} else {
				imgIconGroup = imgIconGroupB;
			}
			g.drawImage(imgIconGroup, 63 - ResultList.BASE_X, 191 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), resultList);
			posX += imgIconGroup.getWidth(resultList) + 4;
		}
		
		StringBuffer buffer = new StringBuffer();
		if (isGroup) {
			buffer.append(itemCount);
			if (isSeries) {
				buffer.append(" ").append(txtEpisode);
			} else {
				buffer.append(" ").append(txtRecordings);
			}
		} else {
			posX = 70 - ResultList.BASE_X;
			int programType = searchedPvr.getProgramType();
			buffer.append(DataCenter.getInstance().getString("ProgramType."+programType));
		}
		g.setFont(Rs.F17);
		g.setColor(cDisplayInfo);
		g.drawString(buffer.toString(), posX, 207 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		
		if (!isGroup) {
			g.setColor(cDate);
			String date = searchedPvr.getRecordingStartTimeText();
			g.drawString(date, 408 - ResultList.BASE_X, 186 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
			
			String durationTxt = Formatter.getCurrent().getDurationText(searchedPvr.getDuration());
			g.drawString(durationTxt, 408 - ResultList.BASE_X, 206 - ResultList.BASE_Y + (i * ResultList.ROW_GAP));
		}
		
		if (!isMultipleCh) {
			String chCallLetter = searchedPvr.getChannelName();
			Image imgChLogo = (Image) SharedMemory.getInstance().get("logo." + chCallLetter);
			if (imgChLogo == null) {
				imgChLogo = (Image) SharedMemory.getInstance().get("logo.default");
			}
			int imgChLogoW = imgChLogo.getWidth(resultList);
			g.drawImage(imgChLogo, 580 - ResultList.BASE_X - (imgChLogoW/2), 180 - ResultList.BASE_Y + (i * ResultList.ROW_GAP), resultList);
		}
	}
	
	protected void drawTitle(Graphics g, ResourceList resourceList, int index, ResultList resultList, boolean isFocused) {
		g.setFont(Rs.F20);
		int posX = 70 - ResultList.BASE_X;
		int posY = 190 - ResultList.BASE_Y + (index * ResultList.ROW_GAP);
		if (isFocused) {
			String title = resourceList.getTitle();
			int titleW = Rs.FM20.stringWidth(title);
			g.setColor(Rs.C0);
			if (titleW > resultList.validWth) {
				int cnt = Math.max(resultList.scroll - 10, 0);
				Rectangle ori = g.getClipBounds();
				try {
					g.clipRect(posX, posY - 20, resultList.validWth, 20);
					g.drawString(title.substring(cnt), posX, posY);
				} catch (IndexOutOfBoundsException e) {
					resultList.scroll = 0;
				}
				g.setClip(ori);
			} else {
				g.drawString(title, posX, posY);
			}
		} else {
			boolean isPersonFilter = resultList.searchParameter.getSearchFilterId() == SearchFilterDef.ID_PERSON;
			boolean useEmphasis = !isPersonFilter;
			String remain = resourceList.getTitle();
			if (useEmphasis) {
				String compare = resultList.searchParameter.getSearchKeyword().toUpperCase();
				int compareLth = compare.length();
				int compareIdx = remain.toUpperCase().indexOf(compare);
				
				int curTxtW = 0;
				boolean isExceedValidWth = false;
				while(compareIdx != -1) {
					String drawTxt = null;
					int drawTxtW = 0;
					if (compareIdx > 0) {
						drawTxt = remain.substring(0, compareIdx);
						drawTxtW = Rs.FM20.stringWidth(drawTxt);
						g.setColor(Rs.C255);
						isExceedValidWth = (curTxtW + drawTxtW > resultList.validWth);
						if (isExceedValidWth) {
							drawTxt = TextUtil.shorten(drawTxt, Rs.FM20, resultList.validWth - curTxtW);
							drawTxtW = Rs.FM20.stringWidth(drawTxt);
						}
						g.drawString(drawTxt, posX, posY);
						if (isExceedValidWth) {
							remain = "";
							break;
						}
						curTxtW += drawTxtW;
						posX += drawTxtW;
					}
					drawTxt = remain.substring(compareIdx, compareIdx + compareLth);
					drawTxtW = Rs.FM20.stringWidth(drawTxt);
					g.setColor(Rs.C246_199_0);
					isExceedValidWth = (curTxtW + drawTxtW > resultList.validWth);
					if (isExceedValidWth) {
						drawTxt = TextUtil.shorten(drawTxt, Rs.FM20, resultList.validWth - curTxtW);
						drawTxtW = Rs.FM20.stringWidth(drawTxt);
					}
					g.drawString(drawTxt, posX, posY);
					if (isExceedValidWth) {
						remain = "";
						break;
					}
					posX += drawTxtW;
					remain = remain.substring(compareIdx + compareLth);
					compareIdx = remain.toUpperCase().indexOf(compare);
				}
				if (remain.length() > 0) {
					int remainW = Rs.FM20.stringWidth(remain);
					if (curTxtW + remainW > resultList.validWth) {
						remain =  TextUtil.shorten(remain, Rs.FM20, resultList.validWth - curTxtW);
					}
					g.setColor(Rs.C255);
					g.drawString(remain, posX, posY);
				}
			} else {
				g.setColor(Rs.C255);
				g.drawString(remain, posX, posY);
			}
		}
	}
}

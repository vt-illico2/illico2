package com.videotron.tvi.illico.search;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.controller.ConfigurationController;
import com.videotron.tvi.illico.search.controller.SearchController;
import com.videotron.tvi.illico.search.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.search.controller.communication.PreferenceProxy;

/**
 * Search App.
 *
 * @author Jinjoo Ok
 * @version
 */
public class App implements Xlet, ApplicationConfig {

    /** XletContext. */
    private XletContext context;

    /**
     * called by Monitor when STB booted.
     * @param ctx XletContext
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public final void initXlet(final XletContext ctx) throws XletStateChangeException {
        context = ctx;
        if (Log.INFO_ON) {
            Log.printInfo("Search App.initXlet");
        }
        FrameworkMain.getInstance().init(this);
    }
    
    /**
     * Initialize Search Application.
     */
    public void init() {
        ConfigurationController.getInstance().init();
        Log.setApplication(this);
        CommunicationManager.getInstance().init(context);
        PreferenceProxy.getInstance().init(context);
    }

    /**
     * called by Monitor when STB booted.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public final void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("Search App.startXlet");
        }
        FrameworkMain.getInstance().start();
        SearchController.getInstance();
    }

    /**
     * called when application paused.
     */
    public final void pauseXlet() {
        FrameworkMain.getInstance().pause();
    }

    /**
     * called when application destroyed.
     * @param ctx XletContext
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *                 state). This exception is ignored if unconditional is equal to true.
     */
    public final void destroyXlet(final boolean ctx) throws XletStateChangeException {
        SearchController.getInstance().destroy();
        FrameworkMain.getInstance().destroy();
    }

    /**
     * Returns name of application. The name can be used as a unique key to find <code>AppProxy</code> from
     * <code>AppsDatabase</code>.
     *
     * @return name of application
     */
    public final String getApplicationName() {
        return Rs.APP_NAME;
    }

    /**
     * Returns version of application.
     *
     * @return version of application
     */
    public final String getVersion() {
        return Rs.APP_VERSION;
    }

    /**
     * Returns the XletContext of application.
     *
     * @return XletContext
     */
    public final XletContext getXletContext() {
        return context;
    }

}

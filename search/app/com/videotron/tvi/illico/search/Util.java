/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search;

import java.awt.FontMetrics;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <code>Util</code>
 * 
 * @version $Id: Util.java,v 1.14 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 26.
 * @description
 */
public class Util {
    public static String readFile(String fileName) {
        return readFile(new File(fileName));
    }
    
    public static String readFile(File file) {
        if (file == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (bout != null) {
                bout.reset();
                try {
                    bout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bout = null;
            }
            if (in != null) {
                try{
                    in.close();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                in = null;
            }
        }
        return new String(result);
    }
    
    public static String shortenR(String src, FontMetrics fm, int width) {
        if (src == null || src.length() <= 0) {
            return "";
        }
        if (fm.stringWidth(src) <= width) {
            return src;
        }
        int len;
        do {
            len = src.length();
            src = src.substring(1, len);
        } while (fm.stringWidth(src) > width);
        return src;
    }
    
    public static String getStringExceptString(String src, String exceptString) {
    	if (src == null) {
    		return null;
    	}
    	if (exceptString == null) {
    		return src;
    	}
    	int exceptStringLth = exceptString.length();
    	String remain = src;
    	int index = remain.indexOf(exceptString);
    	while(index != -1) {
    		if (index == 0) {
    			remain = remain.substring(index + exceptStringLth);
    		} else {
    			remain = remain.substring(0, index) + remain.substring(index + exceptStringLth);
    		}
    		index = remain.indexOf(exceptString);
    	}
//    	StringTokenizer st = new StringTokenizer(src, exceptString);
//    	StringBuffer result = new StringBuffer();
//    	int stSize = st.countTokens();
//    	for (int i = 0; i<stSize; i++) {
//    		String stt = st.nextToken();
//    		if (Log.DEBUG_ON) {
//				Log.printDebug("[Util2.getStringExceptString] stt["+i+"]:" + stt);
//			}
//    		result.append(stt);
//    	}
//    	return result.toString();
    	return remain;
    }
    
    public static int getContentInt(Object obj) {
    	if (obj == null) {
    		return -1;
    	}
    	int res = -1;
    	try{
    		res = Integer.parseInt(String.valueOf(obj));
    	}catch(Exception e) {
//    		e.printStackTrace();
    	}
    	return res;
    }
    public static long getContentLong(Object obj) {
    	if (obj == null) {
    		return -1;
    	}
    	long res = -1;
    	try{
    		res = Long.parseLong((String)obj);
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	return res;
    }
    
    
    public static String getExtraString(String value) {
    	if (value == null) {
    		return "&null";
    	}
    	return value;
    }
    
}

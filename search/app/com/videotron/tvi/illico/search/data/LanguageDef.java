/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;

/**
 * <code>LanguageDef</code>
 * 
 * @version $Id: LanguageDef.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 13.
 * @description
 */
public class LanguageDef {
    public static String getCurrentLanguage() {
    	String language = DataCenter.getInstance().getString(PreferenceNames.LANGUAGE);
    	if (language.equals(Definitions.LANGUAGE_ENGLISH)) {
    		return "en";
    	}
    	return "fr";
    }
}

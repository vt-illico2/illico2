/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import org.json.simple.JSONObject;

import com.videotron.tvi.illico.search.DateUtil;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;

/**
 * <code>SearchedLive</code>
 * 
 * @version $Id: SearchedPvr.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 30.
 * @description
 */
public class SearchedPvr extends ResourceList{
	private String id;
	private String channelName;
	private int channelNumber;
	private int contentState;
	private long duration;
//	private String location;
	private int programType;
	private long programEndMillis;
	private long programStartMillis;
	private int recordingRequestState;
	private long recordingStartMillis;
//	private long scheduledDurationMillis;
//	private long scheduledStartMillis;
	private String title;
	private int audioType;
	private int category;
	private int definition;
//	private String episodeTitle;
	private String synopsis;
//	private String productionYear;
	private String parentalRating;
	private String seriesId;
	
	private String recordingStartTimeTxt;
	
	public Resource parse(JSONObject jsonObj) {
		return this;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public int getChannelNumber() {
		return channelNumber;
	}

	public void setChannelNumber(int channelNumber) {
		this.channelNumber = channelNumber;
	}

//	public int getContentState() {
//		return contentState;
//	}
//
//	public void setContentState(int contentState) {
//		this.contentState = contentState;
//	}

	public long getDuration() {
		return duration;
	}
	
	public int getDurationMinute() {
		return (int) Math.ceil(((double)duration / (double)60000));
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

//	public String getLocation() {
//		return location;
//	}
//
//	public void setLocation(String location) {
//		this.location = location;
//	}
	
	public int getProgramType() {
		return programType;
	}

	public void setProgramType(int programType) {
		this.programType = programType;
	}

//	public long getProgramEndMillis() {
//		return programEndMillis;
//	}
//
//	public void setProgramEndMillis(long programEndMillis) {
//		this.programEndMillis = programEndMillis;
//	}

	public long getProgramStartMillis() {
		return programStartMillis;
	}

	public void setProgramStartMillis(long programStartMillis) {
		this.programStartMillis = programStartMillis;
	}

//	public int getRecordingRequestState() {
//		return recordingRequestState;
//	}
//
//	public void setRecordingRequestState(int recordingRequestState) {
//		this.recordingRequestState = recordingRequestState;
//	}

	public long getRecordingStartMillis() {
		return recordingStartMillis;
	}
	
	public String getRecordingStartTimeText() {
		if (recordingStartTimeTxt == null) {
			recordingStartTimeTxt = DateUtil.getDateString(recordingStartMillis);
		}
		return recordingStartTimeTxt;
	}

	public void setRecordingStartMillis(long recordingStartMillis) {
		this.recordingStartMillis = recordingStartMillis;
	}

//	public long getScheduledDurationMillis() {
//		return scheduledDurationMillis;
//	}
//
//	public void setScheduledDurationMillis(long scheduledDurationMillis) {
//		this.scheduledDurationMillis = scheduledDurationMillis;
//	}

//	public long getScheduledStartMillis() {
//		return scheduledStartMillis;
//	}
//
//	public void setScheduledStartMillis(long scheduledStartMillis) {
//		this.scheduledStartMillis = scheduledStartMillis;
//	}

//	public int getAudioType() {
//		return audioType;
//	}

//	public void setAudioType(int audioType) {
//		this.audioType = audioType;
//	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getDefinition() {
		return definition;
	}

	public void setDefinition(int definition) {
		this.definition = definition;
	}

//	public String getEpisodeTitle() {
//		return episodeTitle;
//	}
//
//	public void setEpisodeTitle(String episodeTitle) {
//		this.episodeTitle = episodeTitle;
//	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

//	public String getProductionYear() {
//		return productionYear;
//	}
//
//	public void setProductionYear(String productionYear) {
//		this.productionYear = productionYear;
//	}

	public String getParentalRating() {
		return parentalRating;
	}

	public void setParentalRating(String parentalRating) {
		this.parentalRating = parentalRating;
	}

	public String getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}

	public String toString() {
		StringBuffer toStr = new StringBuffer();
		toStr.append("id: " + id +"\n" )
		.append("channelName: " + channelName +"\n" )
		.append("channelNumber: " + channelNumber +"\n" )
		.append("contentState: " + contentState +"\n" )
		.append("duration: " + duration +"\n" )
		.append("programEndMillis: " + programEndMillis +"\n" )
		.append("programStartMillis: " + programStartMillis +"\n" )
		.append("recordingRequestState: " + recordingRequestState +"\n" )
		.append("recordingStartMillis: " + recordingStartMillis +"\n" )
//		.append("scheduledDurationMillis: " + scheduledDurationMillis +"\n" )
//		.append("scheduledStartMillis: " + scheduledStartMillis +"\n" )
		.append("title: " + title +"\n" )
		.append("audioType: " + audioType +"\n" )
		.append("category: " + category +"\n" )
		.append("definition: " + definition +"\n" )
//		.append("episodeTitle: " + episodeTitle +"\n" )
		.append("synopsis: " + synopsis +"\n" )
//		.append("productionYear: " + productionYear +"\n" )
		.append("parentalRating: " + parentalRating +"\n" )
		.append("seriesId: " + seriesId +"\n" );
		return super.toString();
	}
}

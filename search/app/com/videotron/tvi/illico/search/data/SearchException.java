/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import com.videotron.tvi.illico.search.Rs;

/**
 * <code>SearchException</code>
 * 
 * @version $Id: SearchException.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 11.
 * @description
 */
public class SearchException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private final String code;
	private final String msg;
	
	public SearchException(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
	public SearchException(Exception ioe) {
		super(ioe);
		this.code = Rs.ERROR_SCH505;
		this.msg = ioe.getMessage();
	}
	
	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return msg;
	}

}

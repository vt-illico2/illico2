/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import java.awt.FontMetrics;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.http.HttpResponse;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ResourceList</code>
 * 
 * @version $Id: ResourceList.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 2.
 * @description
 */
public class ResourceList implements Resource {
	private String title;
	private String realTitle;
	private String shortenedRealTitle;
	private String[] emphasisLetters;
	
    public String hash;

    public Object[] data;

    public Resource[] listData;
    
    public String jvmName;
    private int cacheTimeStamp;
    private int totalElementCount;
    private int returnedElementCount;
    private int nextFrom;
    
    private String pKeyword;
    private int pFrom;
    private int pTo;
    private int pFilterId;
    private String pSortValue;
    
    /**
     * Parsing data by Simple Json.
     *
     * @param clazz
     * @param response
     * @throws WindmillException
     */
    public void parse(Class clazz, HttpResponse response) throws SearchException {
        if (clazz == null || response == null) {
            return;
        }
        parse(clazz, response.asString());
    }

    public void parse(Class clazz, String response) throws SearchException {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[ResourceList.parse] Called.");
		}
        if (clazz == null || response == null || response.length() <= 0) {
            return;
        }
        JSONObject jsonObj = null;
        JSONParser jsonParser = null;
        try {
			jsonParser = new JSONParser();
			jsonObj = (JSONObject) jsonParser.parse(response);
			setHead((JSONObject) jsonObj.get(JsonDef.HEAD));
			
			int returnedElementCount = getReturnedElementCount();
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResourceList.parse] count: " + returnedElementCount);
			}
			if (returnedElementCount > 0) {
				Object obj = jsonObj.get(JsonDef.BODY);
				if (Log.DEBUG_ON) {
					Log.printDebug("[ResourceList.parse] obj: " + obj);
				}
				if (obj instanceof JSONArray) {
					JSONArray jsonArray = (JSONArray) obj;
					ArrayList list = new ArrayList();
					if (jsonArray != null) {
						Iterator iter = jsonArray.iterator();
						while (iter.hasNext()) {
							Object tempObj = iter.next();
							if (Log.DEBUG_ON) {
								Log.printDebug("[ResourceList.parse] tempObj: " + tempObj);
							}
//							if (tempObj instanceof JSONObject) {
//								JSONObject dataObj = (JSONObject) tempObj;
//								Resource resource = (Resource) clazz.newInstance();
//								resource.parse(dataObj);
//								list.add(resource);
//								dataObj.clear();
//							} else if (tempObj instanceof String) {
//								String dataObj = (String) tempObj;
//								Resource resource = (Resource) clazz.newInstance();
//								resource.parse(dataObj);
//								list.add(resource);
//							}
							Resource resource = (Resource) clazz.newInstance();
							resource.parse(tempObj);
							list.add(resource);
						}
						setListData((Resource[]) list.toArray(new Resource[list.size()]));

						jsonArray.clear();
						list.clear();
						jsonArray = null;
						list = null;
					}
				} else if (obj instanceof JSONObject) {
					ResourceList resourceList = (ResourceList) clazz.newInstance();
					resourceList.parse((JSONObject) obj);
					Resource[] resouces = resourceList.getListData();
					setListData(resouces);
				}
			}
        } catch (SearchException e) {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[ResourceList.parse] SearchException occurred.");
			}
        	e.printStackTrace();
            throw e;
        } catch (Exception e) {
        	e.printStackTrace();
            throw new SearchException(Rs.ERROR_SCH502, e.getMessage());
        } finally {
        	if(jsonObj != null) {
        		jsonObj.clear();
        	}
            jsonObj = null;
            jsonParser = null;
        }
    }
    
//    public void parse(Class clazz, String response, String name) throws WindmillException {
//         if (clazz == null || response == null || response.length() <= 0) {
//             return;
//         }
//         JSONObject jsonObj = (JSONObject)JsonUtil.getResource(clazz, response, name);
//         try {
//             
//             ArrayList list = new ArrayList();
//             if (jsonArray != null) {
//                 Iterator iter = jsonArray.iterator();
//                 while (iter.hasNext()) {
//                     JSONObject dataObj = (JSONObject)iter.next();
//                     Resource resource = (Resource)clazz.newInstance();
//                     resource.parse(dataObj);
//                     list.add(resource);
//                     dataObj.clear();
//                 }
//                 setListData((Resource[]) list.toArray(new Resource[list.size()]));
//
//                 jsonArray.clear();
//                 list.clear();
//                 jsonArray = null;
//                 list = null;
//             }
//         } catch (Exception e) {
//             // TODO 공통 오류 setting 할 것
//             e.printStackTrace();
//             throw new WindmillException(e);
//         } finally {
//         	if(jsonObj != null) {
//         		jsonObj.clear();
//         	}
//             jsonObj = null;
//         }
//    }
    /**
     * Parsing data by Gson.
     *
     * @param className
     * @param response
     * @throws WindmillException
     */
    public void parseList(String className, HttpResponse response) throws SearchException {
        Class clazz;
        try {
            clazz = Class.forName(className);
            parseList(clazz, response);
        }
        catch (ClassNotFoundException e) {
            throw new SearchException(e);
        }
    }

    public void parseList(Class clazz, HttpResponse response) throws SearchException {
        parseList(clazz, response.asString());
    }

    public void parseList(Class clazz, String jsonString) throws SearchException {
//        Gson gson = new Gson();
//        try {
//            ResourceList dataList = (ResourceList) gson.fromJson(jsonString, ResourceList.class);
//            if (dataList != null ) {
//                this.total = dataList.total;
//                if (dataList.data != null && dataList.data.length >= 0) {
//                    listData = new Resource[dataList.data.length];
//                    Resource tmp = null;
//                    for (int i = 0; i < dataList.data.length; i++) {
//                        tmp = (Resource) gson.fromJson(gson.toJson(dataList.data[i]), clazz);
//                        listData[i] = tmp;
//                        dataList.data[i] = null;
//                    }
//                }
//            }
//            dataList = null;
//            jsonString = null;
//            gson = null;
//        }
//        catch (JsonSyntaxException e) {
//        	e.printStackTrace();
//            throw new WindmillException(e);
//        }
    }

    public void parseList(Class clazz, String jsonString, int offset, int limit) throws SearchException {
//        Gson gson = new Gson();
//        try {
//            ResourceList dataList = (ResourceList) gson.fromJson(jsonString, ResourceList.class);
//            if (dataList != null ) {
//                this.total = dataList.total;
//                if (dataList.data != null && dataList.data.length > 0) {
//                	int loopCnt = dataList.data.length;
//                	int startIdx = offset;
//                	if (offset == -1 || limit == -1) {
//                		startIdx = 0;
//                	} else {
//                    	if (offset < this.total) {
//                        	if (offset + limit > this.total) {
//                        		loopCnt = this.total - offset;
//                        	} else {
//                        		loopCnt = limit;
//                        	}
//                    	}
//                	}
//                    listData = new Resource[loopCnt];
//                    Resource tmp = null;
//                    for (int i = startIdx; i < startIdx+loopCnt; i++) {
//                        tmp = (Resource) gson.fromJson(gson.toJson(dataList.data[i]), clazz);
//                        listData[i - startIdx] = tmp;
//                        dataList.data[i] = null;
//                    }
//                }
//            }
//            dataList = null;
//            jsonString = null;
//            gson = null;
//        }
//        catch (JsonSyntaxException e) {
//        	e.printStackTrace();
//            throw new WindmillException(e);
//        }
    }

    /**
     * 독립적으로 total, data[] 형태의 결과를 받은 것이 아니라 다른 결과와 함께 특정 member name 으로 ResourceList 가 할당되어 진 경우
     * 즉 total 과 data 가 이미 setting 되었을 때 이를 parsing 해주기 위한 method.
     *
     * @param clazz the clazz
     * @throws WindmillException the windmill exception
     */
    public void parseList(Class clazz) throws SearchException {
//        Gson gson = new Gson();
//        try {
//            if (data == null || data.length == 0) {
//                return;
//            }
//            listData = new Resource[data.length];
//            Resource tmp = null;
//            for (int i = 0; i < data.length; i++) {
//                tmp = (Resource) gson.fromJson(gson.toJson(data[i]), clazz);
//                listData[i] = tmp;
//                data[i] = null;
//            }
//            gson = null;
//        }
//        catch (JsonSyntaxException e) {
//            throw new WindmillException(e);
//        }
    }
    
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Resource[] getListData() {
        return listData;
    }

    public void setListData(Resource[] listData) {
        this.listData = listData;
    }

    /* (non-Javadoc)
     * @see cappl.dm.wind.resource.Resource#dispose()
     */
    public void dispose() {
        // TODO Auto-generated method stub
    }

    public Resource parse(Object obj) {
        return null;
    }

    /* (non-Javadoc)
     * @see cappl.dm.wind.resource.Resource#getCreatedTime()
     */
    public Date getCreatedTime() {
        // TODO Auto-generated method stub
        return null;
    }
    
	public void setHead(JSONObject jsonObj) throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResourceList.setHead] Called.");
		}
		JSONObject error = (JSONObject) jsonObj.get(JsonDef.ERROR);
		if (error != null) {
			String code = JsonUtil.getString(error, JsonDef.CODE);
			String msg = JsonUtil.getString(error, JsonDef.MESSAGE);
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResourceList.setHead] Error occured code: " + code +", Message: " + msg);
			}
			throw new SearchException(code, msg);
		}
		
		setJVMName(JsonUtil.getString(jsonObj, JsonDef.JVM_NAME));
//		setCacheTimeStamp(JsonUtil.getInt(jsonObj, JsonDef.CACHE_TIME_STAMP));
		setTotalElementCount(JsonUtil.getInt(jsonObj, JsonDef.TOTAL_ELEMENT_COUNT));
		setReturnedElementCount(JsonUtil.getInt(jsonObj, JsonDef.RETURNED_ELEMENT_COUNT));
		setNextFrom(JsonUtil.getInt(jsonObj, JsonDef.NEXT_FROM));
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResourceList.setHead] " + 
					"jvmName: " + jvmName + 
					", cacheTimeStamp: " + cacheTimeStamp + 
					", totalElementCount: " + totalElementCount + 
					", returnedElementCount: " + returnedElementCount + 
					", nextFrom: " + nextFrom);
		}
	}
    
    public String getJVMName() {
    	return jvmName;
    }
    
    public void setJVMName(String reqJvmName) {
    	jvmName = reqJvmName;
    }
    
    public int getCacheTimeStamp() {
    	return cacheTimeStamp;
    }
    
    public void setCacheTimeStamp(int reqCacheTimeStamp) {
    	cacheTimeStamp = reqCacheTimeStamp;
    }
    
    public int getTotalElementCount(){
    	return totalElementCount;
    }
    
    public void setTotalElementCount(int reqtTotalElementCount) {
    	totalElementCount = reqtTotalElementCount;
    }
    
    public int getReturnedElementCount() {
    	return returnedElementCount;
    }
    
    public void setReturnedElementCount(int reqtReturnedElementCount) {
    	returnedElementCount = reqtReturnedElementCount;
    }
    
    public int getNextFrom() {
    	return nextFrom;
    }
    
    public void setNextFrom(int reqtNextFrom) {
    	nextFrom = reqtNextFrom;
    }
    
    public void setTitle(String title) {
    	this.title = title;
    }
    
    public String getTitle() {
    	return title;
    }
    
    public String getRealTitle() {
    	if (realTitle == null) {
    		realTitle = Util.getStringExceptString(title, Rs.TAG_EM_START);
    		realTitle = Util.getStringExceptString(realTitle, Rs.TAG_EM_END);
    	}
    	return realTitle;
    }
    
    public String getShortenedRealTitle(FontMetrics reqFm, int validWidth) {
		if (shortenedRealTitle == null) {
			String temp = getRealTitle();
			if (temp != null) {
				int tempW = reqFm.stringWidth(temp);
				if (tempW > validWidth) {
					shortenedRealTitle = TextUtil.shorten(temp, reqFm, validWidth);
				} else {
					shortenedRealTitle = temp;
				}
			}
		}
		return shortenedRealTitle;
    }
    
    public String[] getEmphasisLetters() {
    	if (emphasisLetters == null) {
    		String remain = title;
    		int startIdx = -1;
    		if (remain != null) {
    			startIdx = remain.indexOf(Rs.TAG_EM_START);
    		}
    		if (startIdx<0) {
    			emphasisLetters = new String[0];
    			return emphasisLetters;
    		}
    		
    		remain = remain.substring(startIdx);
    		StringTokenizer stStart = new StringTokenizer(remain, Rs.TAG_EM_START);
    		int stStartCount = stStart.countTokens();
    		emphasisLetters = new String[stStartCount];
    		for (int i=0; i<stStartCount; i++) {
    			String tokenedStr = stStart.nextToken();
    			int endIdx = -1;
    			if (tokenedStr != null) {
    				endIdx = tokenedStr.indexOf(Rs.TAG_EM_END);
    			}
    			if (endIdx >= 0) {
    				emphasisLetters[i] = tokenedStr.substring(0, endIdx);
    			}
    		}
    	}
    	return emphasisLetters;
    }
    
    
    
	
	public class HashData implements Resource {
		private int code;
		private String description;
		
		public Resource parse(Object obj) {
			JSONObject jsonObj = (JSONObject)obj;
			if (jsonObj != null) {
				code = JsonUtil.getInt(jsonObj, JsonDef.CODE);
				description = JsonUtil.getString(jsonObj, JsonDef.DESCRIPTION);
			}
			jsonObj.clear();
			return this;
		}
		public int getCode() {
			return code;
		}
		public String getTitle() {
			return description;
		}
		public String toString() {
			return code +" - " + description;
		}
	}
	
	public void setParameterKeyword(String reqKeyword) {
		pKeyword = reqKeyword;
	}
	
	public String getParameterKeyword() {
		return pKeyword;
	}
	
	public void setParameterFrom(int reqFrom) {
		pFrom = reqFrom;
	}
	
	public int getParameterFrom() {
		return pFrom;
	}
	
	public void setParameterTo(int reqTo) {
		pTo = reqTo;
	}
	
	public int getParameterTo() {
		return pTo;
	}
	
	public void setParameterFilterId(int reqFilterId) {
		pFilterId = reqFilterId;
	}
	
	public int getParameterFilterId() {
		return pFilterId;
	}
	
	public void setParameterSortValue(String reqSortValue) {
		pSortValue = reqSortValue;
	}
	
	public String getParameterSortValue() {
		return pSortValue;
	}
}

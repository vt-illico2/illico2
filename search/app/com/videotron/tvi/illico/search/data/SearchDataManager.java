/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import java.awt.Image;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import com.videotron.tvi.illico.search.controller.DebugScreen;
import org.ocap.hardware.Host;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.pvr.AppDataKeys;
import com.videotron.tvi.illico.ixc.pvr.RecordedContent;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.data.resource.STBRegion;
import com.videotron.tvi.illico.search.data.resource.SearchedEpg;
import com.videotron.tvi.illico.search.data.resource.SearchedPerson;
import com.videotron.tvi.illico.search.data.resource.SearchedPvr;
import com.videotron.tvi.illico.search.data.resource.SearchedPvrGroup;
import com.videotron.tvi.illico.search.data.resource.SearchedVod;
import com.videotron.tvi.illico.search.data.resource.SuggestedKeyword;
import com.videotron.tvi.illico.search.data.resource.SuggestedKeywordPerson;
import com.videotron.tvi.illico.search.http.HttpResponse;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>SearchDataManager</code>
 * 
 * @version $Id: SearchDataManager.java,v 1.143 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 26.
 * @description
 */
public class SearchDataManager {
	private static SearchDataManager instance;
	
	private static final int REQUEST_LINEAR = 0;
	private static final int REQUEST_VOD = 1;
	private static final int REQUEST_PVR = 2;
	private static final int REQUEST_PERSON = 3;
	
	private String urlPath;
	private String[] proxyIps;
	private int[] proxyPorts;
	
	private int curProxyIdx;

	// VDTRMASTER-5949
	private String macAddress;
	private String chGridId;
	
	private int maxRetryCount;
	
	private int curRetryCount;

    private int yearShift = 0;
	
	private static final String HEADER_BACKEND_ERROR_CODE = "ICO";
	private static final String DEFAULT_ERROR_CODE = "SCH500";
	private final int INDEX_BACKEND_ERROR_CODE = 0;
	private final int INDEX_SEARCH_ERROR_CODE = 1;
	private static final String [][] ERROR_CODE_TABLE = {
		{"ICO-001", "SCH500"},
		{"ICO-002", "SCH200"},
		{"ICO-003", "SCH201"},
		{"ICO-100", "SCH202"},
		{"ICO-101", "SCH203"},
		{"ICO-400", "SCH204"},
		{"ICO-401", "SCH205"},
		{"ICO-402", "SCH206"},
		{"ICO-403", "SCH207"},
		{"ICO-404", "SCH208"},
		{"ICO-405", "SCH209"},
		{"ICO-406", "SCH210"}
	};

    public int getYearShift() {
        if (Log.INFO_ON) Log.printInfo("yearShift = "+yearShift);
        if (yearShift > 0) return yearShift;
        try {
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            if (monitor != null) {
                yearShift = monitor.getYearShift();
                if (Log.INFO_ON) Log.printInfo("monitor's yearShift = "+yearShift);
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return yearShift;
    }
	
	private SearchDataManager() {
		maxRetryCount = DataCenter.getInstance().getInt(Rs.DCKEY_SEARCH_SERVICE_REQUEST_RETRY);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.SearchDataManager2] MaxRetryCount: " + maxRetryCount);
		}
//		if (Environment.EMULATOR) {
//			urlPath = "http://dev04stb.int.videotron.com:80/illicoservice/stateless/search/";
//		}
//		urlPath = "http://dev04stb.int.videotron.com:80/illicoservice/stateless/search/";
		if (Environment.EMULATOR) {
			urlPath = "http://dev04stb.int.videotron.com:80/illicoservice/stateless/search/";
		}
	}
	
	public void dispose() {
		chGridId = null;
		macAddress = null;
		proxyIps = null;
		proxyPorts = null;
		urlPath = null;
	}
	
    /**
     * Gets error message from the Error Message.
     * @param code error code
     * @return error message
     */
    public String getErrorMsg(String code) {
        if (Log.DEBUG_ON) {
            Log.printDebug("getErrorMsg " + code);
        }
        ErrorMessageService errorService = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        String text = null;
        if (errorService != null) {
            try {
                ErrorMessage msg = errorService.getErrorMessage(code);
                if (msg != null) {
                    if (DataCenter.getInstance().getString(PreferenceNames.LANGUAGE)
                            .equals(Definitions.LANGUAGE_ENGLISH)) {
                        text = msg.getEnglishContent();
                    } else {
                        text = msg.getFrenchContent();
                    }
                    if (text == null || text.length() == 0) {
                        text = "Unknown error";
                        if (Log.WARNING_ON) {
                            Log.printWarning("Error message is not defined in Error Message Service, code :" + code);
                        }
                    } else if (Log.INFO_ON) {
                        Log.printInfo("getErrorMessage() code : " + code + ", msg : " + msg);
                    }
                } else if (Log.ERROR_ON) {
                    Log.printError("Search cannot get ErrorMessage form ErrorMessageService");
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
        return text;
    }
	
	public void setUrlPath(String reqUrlPath) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.setUrlPath] reqUrlPath: " + reqUrlPath);
		}
		urlPath = reqUrlPath;
		SharedMemory.getInstance().put(SearchService.KEY_SEARCH_URL, urlPath);
	}
	
	public String getUrlPath() {
		return urlPath;
	}
	
	public void setProxyInfo(String[] proxyIps, int[] proxyPorts) {
		this.proxyIps = proxyIps;
		this.proxyPorts = proxyPorts;
		
		if (proxyIps != null) {
			SharedMemory.getInstance().put(SearchService.KEY_SEARCH_PROXY_IP, proxyIps);
		} else {
			SharedMemory.getInstance().remove(SearchService.KEY_SEARCH_PROXY_IP);
		}
		if (proxyPorts != null) {
			SharedMemory.getInstance().put(SearchService.KEY_SEARCH_PROXY_PORT, proxyPorts);
		} else {
			SharedMemory.getInstance().remove(SearchService.KEY_SEARCH_PROXY_PORT);
		}
	}
	
	public String[] getProxyInfo() {
		String[] proxyInfos = null;
		if (proxyIps != null && proxyPorts!= null) {
			proxyInfos = new String[proxyIps.length];
			for (int i=0; i<proxyInfos.length; i++) {
				proxyInfos[i] = proxyIps[i] + ":" + proxyPorts[i];
			}
		}
		return proxyInfos;
	}
	
	public static synchronized SearchDataManager getInstance() {
		if (instance == null) {
			instance = new SearchDataManager();
		}
		return instance;
	}
	
	private void setChannelGridId() throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.setChannelGridId] Called.");
		}
		if (urlPath == null) {
			throw new SearchException(Rs.ERROR_SCH504, "There is no URL Path(GridId).");
		}
		if (macAddress == null) {
			setMacAddress();
		}
		StringBuffer query = new StringBuffer(urlPath);
		query.append("terminal?");
		query.append("&macAddress=").append(macAddress);
		String urlStr = query.toString();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.setChannelGridId] Query: " + urlStr);
		}
		STBRegion stbRegion = null;
		try {
			stbRegion = (STBRegion)request(STBRegion.class, urlStr, false);
			chGridId = stbRegion.getChannelGridId();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ResourceList getSuggestedKeyword(String reqSearchKeyword, int reqSearchFilterId) throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSuggestedKeyword] Called. reqSearchKeyword: " + reqSearchKeyword + ", Request SearchFilter: " + SearchFilterDef.getSectionFilterText(reqSearchFilterId));
		}
		
//		if (true) {
//			throw new SearchException(Rs.ERROR_SCH500, "There is no service information.");
//			throw new SearchException("ICO-001", "Unknown problem.");
//		}
		
		if (urlPath == null) {
			throw new SearchException(Rs.ERROR_SCH504, "There is no URL Path(Suggest).");
		}
		if (reqSearchKeyword == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getSuggestedKeyword] SearchKeyword is invalid. so reutrn.");
			}
			throw new SearchException(Rs.ERROR_SCH501, "There is no request keyword(Suggest).");
		}
		boolean isSuggestedPersonKeyword = reqSearchFilterId == SearchFilterDef.ID_PERSON;
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSuggestedKeyword] This request is suggested person keyword? " + isSuggestedPersonKeyword);
		}
		StringBuffer query = new StringBuffer(urlPath);
		if (isSuggestedPersonKeyword) {
			query.append("person/name?");
			if (reqSearchKeyword != null) {
				query.append("keywords=").append(TextUtil.urlEncode(reqSearchKeyword));
				query.append("&from=").append(0);
				query.append("&to=").append(6);
				query.append("&criteria=").append(SearchFilterDef.getSectionFilterCriteriaKey(SearchFilterDef.ID_ALL));
			}
		} else {
			query.append("terms/suggestion?");
			if (reqSearchKeyword != null) {
				query.append("keywords=").append(TextUtil.urlEncode(reqSearchKeyword));
				query.append("&criteria=").append(SearchFilterDef.getSectionFilterCriteriaKey(reqSearchFilterId));
			}
		}
		String urlStr = query.toString();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSuggestedKeyword] Query: " + urlStr);
		}
		
        Class clazz = null;
		if (isSuggestedPersonKeyword) {
			clazz = SuggestedKeywordPerson.class;
		} else {
			clazz = SuggestedKeyword.class;
		}
		ResourceList resourceList = request(clazz, urlStr, false);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSuggestedKeyword] resourceList: " + resourceList);
		}
		return resourceList;
	}
	
	public ResourceList getSearchLive(String reqKeyword, int reqFrom, int reqTo, int reqFilterId, String reqSortValue, boolean isLetters) throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchLive] Called. Keyword: " +reqKeyword +", From: " + reqFrom +
					", To: " + reqTo + ", Criteria: " + SearchFilterDef.getSectionFilterText(reqFilterId) +", Sort: " + reqSortValue);
		}
//		if (true) {
//			throw new SearchException(Rs.ERROR_SCH501, "There is no service information.");
//		}
		
		if (urlPath == null) {
			throw new SearchException(Rs.ERROR_SCH504, "There is no URL Path(EPG).");
		}
		if (reqKeyword == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getSearchLive] SearchKeyword is invalid. so reutrn.");
			}
			throw new SearchException(Rs.ERROR_SCH501, "There is no request keyword(EPG).");
		}
		
		if (chGridId == null) {
			setChannelGridId();
		}
		
		StringBuffer query = new StringBuffer(urlPath);
		query.append("linear?");
		query.append("keywords=").append(TextUtil.urlEncode(reqKeyword));
		// VDTRMASTER-5949
		if (chGridId == null) {
			query.append("&userChannelGridId=").append(STBRegion.DEFAULT_CH_GRID_ID);
		} else {
			query.append("&userChannelGridId=").append(chGridId);
		}
		query.append("&from=").append(reqFrom);
		query.append("&to=").append(reqTo);
		query.append("&language=").append(LanguageDef.getCurrentLanguage());
		// VDTRMASTER-5939
		query.append("&matchAllKeywords=").append(false);
		query.append("&criteria=").append(SearchFilterDef.getSectionFilterCriteriaKey(reqFilterId));
		if (reqSortValue != null && !reqSortValue.equalsIgnoreCase(Rs.DATA_CENTER_NULL_STRING)) {
			query.append("&sort=").append(reqSortValue);
		}
		String urlStr = query.toString();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchLive] Query: " + urlStr);
		}
		ResourceList resourceList = request(SearchedEpg.class, urlStr, true);
		resourceList.setParameterKeyword(reqKeyword);
		resourceList.setParameterFrom(reqFrom);
		resourceList.setParameterTo(reqTo);
		resourceList.setParameterFilterId(reqFilterId);
		resourceList.setParameterSortValue(reqSortValue);
		return resourceList;
	}
	
	public ResourceList getSearchVod(String reqKeyword, int reqFrom, int reqTo, int reqFilterId, String reqSortValue, boolean isLetters) throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchVod] Called. Keyword: " +reqKeyword +", From: " + reqFrom +
					", To: " + reqTo + ", Criteria: " + SearchFilterDef.getSectionFilterText(reqFilterId) +", Sort: " + reqSortValue);
		}	
//		if (true) {
//			throw new SearchException(Rs.ERROR_SCH502, "There is no service information.");
//		}
		if (urlPath == null) {
			throw new SearchException(Rs.ERROR_SCH504, "There is no URL Path(VOD).");
		}
		if (reqKeyword == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getSearchVod] SearchKeyword is invalid. so reutrn.");
			}
			throw new SearchException(Rs.ERROR_SCH501, "There is no keyword (VOD).");
		}
		
		String services = (String)SharedMemory.getInstance().get(VODService.KEY_SVOD_LIST);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchVod] Services: " + services);
		}
		if (services == null) {
			if (Environment.EMULATOR) {
				services = "s_1,s_1011,s_4821";
			} else {
				throw new SearchException(Rs.ERROR_SCH501, "There is no service information(VOD).");
			}
		}
		
		StringBuffer query = new StringBuffer(urlPath);
		query.append("vod?");
		query.append("keywords=").append(TextUtil.urlEncode(reqKeyword));
		query.append("&from=").append(reqFrom);
		query.append("&to=").append(reqTo);
		query.append("&language=").append(LanguageDef.getCurrentLanguage());
		// VDTRMASTER-5939
		query.append("&matchAllKeywords=").append(false);
		query.append("&criteria=").append(SearchFilterDef.getSectionFilterCriteriaKey(reqFilterId));
		if (reqSortValue != null && !reqSortValue.equalsIgnoreCase(Rs.DATA_CENTER_NULL_STRING)) {
			query.append("&sort=").append(reqSortValue);
		}
		query.append("&currentPlatformOnly=true");
		if (services != null) {
			query.append("&services=").append(services);
		}
		
		String urlStr = query.toString();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchVod] Query: " + urlStr);
		}
//		SearchedVod searchedVod = (SearchedVod)request(SearchedVod.class, urlStr, true);
//		return searchedVod;
		ResourceList resourceList = request(SearchedVod.class, urlStr, true);
		resourceList.setParameterKeyword(reqKeyword);
		resourceList.setParameterFrom(reqFrom);
		resourceList.setParameterTo(reqTo);
		resourceList.setParameterFilterId(reqFilterId);
		resourceList.setParameterSortValue(reqSortValue);
		return resourceList;
		
//		String query = makeSearchQuery(REQUEST_VOD, keyword, from, to, criteria, showAdultContent);
//		if (Log.DEBUG_ON) {
//			Log.printDebug("[SearchDataManager.getSearchVod] Query: " + query);
//		}
		
//		SearchedVod searchedVod = null;
//		
//        ResourceList data = new ResourceList();
//        try {
//			data.parse(com.videotron.tvi.illico.search.data.resource.SearchedVod.class, Util2.readFile("resource/sample/searched_vod.json"));
//		} catch (WindmillException e) {
//			e.printStackTrace();
//		}
//		try {
//			searchedlinear = (SearchedLinear) JsonUtil.getResource(SuggestedKeyword.class, );
//		} catch (WindmillException e) {
//			e.printStackTrace();
//		}
//		if (Log.DEBUG_ON) {
//			Log.printDebug("[SearchDataManager.getSuggestedKeyword] suggestedKeyword: " + suggestedKeyword);
//		}
//		return data;
	}
	
	public ResourceList getSearchPerson(String reqKeyword, int reqFrom, int reqTo, int reqFilterId, String reqSortValue, boolean isLetters) throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchPerson] Called. Keyword: " +reqKeyword +", From: " + reqFrom +
					", To: " + reqTo + ", Criteria: " + SearchFilterDef.getSectionFilterText(reqFilterId) +", Sort: " + reqSortValue);
		}	
//		if (true) {
//			throw new SearchException(Rs.ERROR_SCH503, "There is no service information.");
//		}
		if (urlPath == null) {
			throw new SearchException(Rs.ERROR_SCH504, "There is no URL Path (Person).");
		}
		if (reqKeyword == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getSearchPerson] SearchKeyword is invalid. so reutrn.");
			}
			throw new SearchException(Rs.ERROR_SCH501, "There is no request keyword (Person).");
		}
		
		StringBuffer query = new StringBuffer(urlPath);
		query.append("person/name?");
		query.append("keywords=").append(TextUtil.urlEncode(reqKeyword));
		query.append("&from=").append(reqFrom);
		query.append("&to=").append(reqTo);
		query.append("&language=").append(LanguageDef.getCurrentLanguage());
		// VDTRMASTER-5939
		query.append("&matchAllKeywords=").append(false);
		query.append("&criteria=").append(SearchFilterDef.getSectionFilterCriteriaKey(reqFilterId));
		if (reqSortValue != null && !reqSortValue.equalsIgnoreCase(Rs.DATA_CENTER_NULL_STRING)) {
			query.append("&sort=").append(reqSortValue);
		}
		String urlStr = query.toString();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchPerson] Query: " + urlStr);
		}
		ResourceList resourceList = request(SearchedPerson.class, urlStr, true);
		resourceList.setParameterKeyword(reqKeyword);
		resourceList.setParameterFrom(reqFrom);
		resourceList.setParameterTo(reqTo);
		resourceList.setParameterFilterId(reqFilterId);
		resourceList.setParameterSortValue(reqSortValue);
		return resourceList;
	}
	
	public ResourceList getSearchPvr(String reqKeyword, int reqFrom, int reqTo, int reqFilterId, String reqSortValue) throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchPvr] Called. Keyword: " +reqKeyword +", From: " + reqFrom +
					", To: " + reqTo + ", Criteria: " + SearchFilterDef.getSectionFilterText(reqFilterId) +", Sort: " + reqSortValue);
		}	
		if (reqKeyword == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getSearchPvr] SearchKeyword is invalid. so reutrn.");
			}
			throw new SearchException(Rs.ERROR_SCH501, "There is no request keyword(PVR).");
		}
		
		RecordedContent[] contents = PvrDataManager.getInstance().getRecordedContent(reqKeyword, reqFilterId);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchPvr] contents: " + contents);
		}
		Vector searchedGroupPvrVec = null;
		if (reqSortValue.equals(Rs.SORT_VALUE_PVR_DATE_RECORDED)) {
			searchedGroupPvrVec = getSearchedPvrGroupByDate(contents);
		} else if (reqSortValue.equals(Rs.SORT_VALUE_PVR_TITLE)) {
			searchedGroupPvrVec = getSearchedPvrGroupByAlphabet(contents);
		}
		int searchedGroupPvrVecSize = searchedGroupPvrVec.size();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchPvr] searchedGroupPvrVecSize: " + searchedGroupPvrVecSize);
		}
		Resource[] resources = new Resource[searchedGroupPvrVecSize];
		resources = (Resource[]) searchedGroupPvrVec.toArray(resources);
		ResourceList resourceList = new ResourceList();
		resourceList.setListData(resources);
		resourceList.setTotalElementCount(searchedGroupPvrVecSize);
		resourceList.setReturnedElementCount(searchedGroupPvrVecSize);
		return resourceList;
	}
	
	private Vector getSearchedPvrGroupByDate(RecordedContent[] reqContents) {
		Vector temp = new Vector();
		
		// grouping by series id
		Hashtable seriesGroupingHash = new Hashtable();
		if (reqContents != null && reqContents.length > 0) {
			for (int i=0; i<reqContents.length; i++) {
				SearchedPvr searchedPvr = convertToSearchedPvr(reqContents[i]);	
				String seriesId = searchedPvr.getSeriesId();
				if (Log.DEBUG_ON) {
					Log.printDebug("[SearchDataManager.getSearchedPvrGroupByDate] seriesId: " + seriesId);
				}
				SearchedPvrGroup searchedGroupPvr = null;
				if (seriesId != null) {
					searchedGroupPvr = (SearchedPvrGroup)seriesGroupingHash.get(seriesId); 
				}
				if (searchedGroupPvr == null) {
					searchedGroupPvr = new SearchedPvrGroup();
				}
				searchedGroupPvr.add(searchedPvr);
				int itemCount = searchedGroupPvr.getItemCount();
				if (Log.DEBUG_ON) {
					Log.printDebug("[SearchDataManager.getSearchedPvrGroupByDate] itemCount: " + itemCount);
				}
				if (seriesId != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("[SearchDataManager.getSearchedPvrGroupByDate]a: " + seriesId);
					}
					seriesGroupingHash.put(seriesId, searchedGroupPvr);
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("[SearchDataManager.getSearchedPvrGroupByDate]b: " + seriesId);
					}
					temp.add(searchedGroupPvr);
				}
			}
		}
		temp.addAll(seriesGroupingHash.values());
		// grouping by title
		Hashtable titleGroupingHash = new Hashtable();
		int tempSize = temp.size();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchedPvrGroupByDate] searchedGroupPvrVecSize: " + tempSize);
		}
		for (int i = 0;i <tempSize; i++) {
			SearchedPvrGroup seriesGroupPvr = (SearchedPvrGroup) temp.get(i);
			SearchedPvr searchedPvr = (SearchedPvr) seriesGroupPvr.getItem(0);
			String searchedPvrTitle = searchedPvr.getTitle();
			int itemCount = seriesGroupPvr.getItemCount();
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getSearchedPvrGroupByDate] searchedPvrTitle - count: " + searchedPvrTitle + " - " + itemCount);
			}
			SearchedPvrGroup titleGroupPvr = (SearchedPvrGroup)titleGroupingHash.get(searchedPvrTitle);
			if (titleGroupPvr == null) {
				titleGroupPvr = new SearchedPvrGroup();
			}
			int seriesGroupPvrItemCount = seriesGroupPvr.getItemCount();
			for (int ii = 0; ii<seriesGroupPvrItemCount; ii++) {
				titleGroupPvr.add((SearchedPvr) seriesGroupPvr.getItem(ii));
			}
//			titleGroupPvr.add(searchedPvr);
			titleGroupingHash.put(searchedPvrTitle, titleGroupPvr);
		}
		temp.clear();
		
		Iterator iter = titleGroupingHash.values().iterator(); // filter group by count
		while (iter.hasNext()) {
			SearchedPvrGroup titlePvrGroup = (SearchedPvrGroup) iter.next();
			titlePvrGroup.sort(Rs.SORT_VALUE_PVR_DATE_RECORDED);
			titlePvrGroup.setMultipleChannelSource(isMultipleChannelSource(titlePvrGroup));
			temp.add(titlePvrGroup);
		}
		Collections.sort(temp, new SortByDate());
		return temp;
	}
	
	private Vector getSearchedPvrGroupByAlphabet(RecordedContent[] reqContents) {
		Vector temp = new Vector();
		
		// grouping by series id
		Hashtable seriesGroupingHash = new Hashtable();
		if (reqContents != null && reqContents.length > 0) {
			for (int i=0; i<reqContents.length; i++) {
				SearchedPvr searchedPvr = convertToSearchedPvr(reqContents[i]);	
				String seriesId = searchedPvr.getSeriesId();
				if (Log.DEBUG_ON) {
					Log.printDebug("[SearchDataManager.getSearchedPvrGroupByAlphabet] seriesId: " + seriesId);
				}
				SearchedPvrGroup searchedGroupPvr = null;
				if (seriesId != null) {
					searchedGroupPvr = (SearchedPvrGroup)seriesGroupingHash.get(seriesId); 
				}
				if (searchedGroupPvr == null) {
					searchedGroupPvr = new SearchedPvrGroup();
				}
				searchedGroupPvr.add(searchedPvr);
				if (seriesId != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("[SearchDataManager.getSearchedPvrGroupByAlphabet]a: " + seriesId);
					}
					seriesGroupingHash.put(seriesId, searchedGroupPvr);
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("[SearchDataManager.getSearchedPvrGroupByAlphabet]b: " + seriesId);
					}
					temp.add(searchedGroupPvr);
				}
			}
		}
		temp.addAll(seriesGroupingHash.values());
		// grouping by title
		Hashtable titleGroupingHash = new Hashtable();
		int tempSize = temp.size();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getSearchedPvrGroupByAlphabet] searchedGroupPvrVecSize: " + tempSize);
		}
		for (int i = 0;i <tempSize; i++) {
			SearchedPvrGroup seriesGroupPvr = (SearchedPvrGroup) temp.get(i);
			SearchedPvr searchedPvr = (SearchedPvr) seriesGroupPvr.getItem(0);
			String searchedPvrTitle = searchedPvr.getTitle();
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getSearchedPvrGroupByAlphabet] searchedPvrTitle: " + searchedPvrTitle);
			}
			SearchedPvrGroup titleGroupPvr = (SearchedPvrGroup)titleGroupingHash.get(searchedPvrTitle);
			if (titleGroupPvr == null) {
				titleGroupPvr = new SearchedPvrGroup();
			}
			int seriesGroupPvrItemCount = seriesGroupPvr.getItemCount();
			for (int ii = 0; ii<seriesGroupPvrItemCount; ii++) {
				titleGroupPvr.add((SearchedPvr) seriesGroupPvr.getItem(ii));
			}
//			titleGroupPvr.add(searchedPvr);
			titleGroupingHash.put(searchedPvrTitle, titleGroupPvr);
		}
		temp.clear();
		
		Iterator iter = titleGroupingHash.values().iterator(); // filter group by count
		while (iter.hasNext()) {
			SearchedPvrGroup titlePvrGroup = (SearchedPvrGroup) iter.next();
			titlePvrGroup.sort(Rs.SORT_VALUE_PVR_TITLE);
			titlePvrGroup.setMultipleChannelSource(isMultipleChannelSource(titlePvrGroup));
			temp.add(titlePvrGroup);
		}
		
		if (!Rs.PVR_TEST) {
			Collections.sort(temp, new SortByAlphabet());
		}
		return temp;
	}
	
	private SearchedPvr convertToSearchedPvr(RecordedContent reqContent) {
		if (reqContent == null) {
			return null;
		}
		SearchedPvr searchedPvr = null;
		try {
			String id = reqContent.getId();
			String channelName = reqContent.getChannelName();
			int channelNumber = reqContent.getChannelNumber();
//			int contentState = reqContent.getContentState();
			long duration = reqContent.getDuration();
//			String location = reqContent.getLocation();
//			long programEndMillis = reqContent.getProgramEndTime();
			long programStartMillis = reqContent.getProgramStartTime();
//			int recordingRequestState = reqContent.getRecordingRequestState();
			long recordingStartMillis = reqContent.getRecordingStartTime();
//			long scheduledDurationMillis = reqContent.getScheduledDuration();
//			long scheduledStartMillis = reqContent.getScheduledStartTime();
			String title = reqContent.getTitle();
//			int audioType = Util.getContentInt(reqContent.getAppData(AppDataKeys.AUDIO_TYPE));
			int category =  Util.getContentInt(reqContent.getAppData(AppDataKeys.CATEGORY));
			int definition = Util.getContentInt(reqContent.getAppData(AppDataKeys.DEFINITION));
//			String episodeTitle = (String) reqContent.getAppData(AppDataKeys.EPISODE_TITLE);
	        String synopsis = (String) reqContent.getAppData(AppDataKeys.FULL_DESCRIPTION);
//	        String productionYear = (String) reqContent.getAppData(AppDataKeys.PRODUCTION_YEAR);
	        String parentalRating = (String) reqContent.getAppData(AppDataKeys.PARENTAL_RATING);
	        String seriesId = (String) reqContent.getAppData(AppDataKeys.SERIES_ID);
	        int programType = Util.getContentInt(reqContent.getAppData(AppDataKeys.PROGRAM_TYPE));
	        if (Log.DEBUG_ON) {
	        	Log.printDebug("[SearchDataManager.convertToSearchedPvr] ccccc: " + reqContent.getAppData(AppDataKeys.CATEGORY));
	        	Log.printDebug("[SearchDataManager.convertToSearchedPvr] ppppp: " + reqContent.getAppData(AppDataKeys.PROGRAM_TYPE));
				Log.printDebug("[SearchDataManager.convertToSearchedPvr] programType: " + programType);
				Log.printDebug("[SearchDataManager.convertToSearchedPvr] category: " + category);
			}
	        searchedPvr = new SearchedPvr();
	        searchedPvr.setId(id);
	        searchedPvr.setChannelName(channelName);
	        searchedPvr.setChannelNumber(channelNumber);
//	        searchedPvr.setContentState(contentState);
	        searchedPvr.setDuration(duration);
//	        searchedPvr.setLocation(location);
//	        searchedPvr.setProgramEndMillis(programEndMillis);
	        searchedPvr.setProgramStartMillis(programStartMillis);
//	        searchedPvr.setRecordingRequestState(recordingRequestState);
	        searchedPvr.setRecordingStartMillis(recordingStartMillis);
//	        searchedPvr.setScheduledDurationMillis(scheduledDurationMillis);
//	        searchedPvr.setScheduledStartMillis(scheduledStartMillis);
	        searchedPvr.setTitle(title);
//	        searchedPvr.setAudioType(audioType);
	        searchedPvr.setCategory(category);
	        searchedPvr.setDefinition(definition);
//	        searchedPvr.setEpisodeTitle(episodeTitle);
	        searchedPvr.setSynopsis(synopsis);
//	        searchedPvr.setProductionYear(productionYear);
	        searchedPvr.setParentalRating(parentalRating);
	        searchedPvr.setSeriesId(seriesId);
	        searchedPvr.setProgramType(programType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return searchedPvr;
	}
	
	private boolean isMultipleChannelSource(SearchedPvrGroup reqSearchedPvrGroup) {
		if (reqSearchedPvrGroup == null) {
			return false;
		}
		int itemCount = reqSearchedPvrGroup.getItemCount();
		if (itemCount <= 1) {
			return false;
		}
		
		int tempChNum = -1;
		for (int i = 0; i<itemCount; i++) {
			SearchedPvr searchedPvr = (SearchedPvr)reqSearchedPvrGroup.getItem(i);
			int chNum = searchedPvr.getChannelNumber();
			if (i != 0 && tempChNum != chNum) {
				return true;
			}
			tempChNum = chNum;
		}
		return false;
	}

	
    /**
     * Get poster image.
     * @param name title name
     * @return Image
     */
    public Image getVODPosterImage(String name) {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getVODPosterImage] Called. VOD Poseter Image Name: " + name);
		}
        Image poster = null;
//        if (!ConfigurationController.getInstance().isActivePosterDownload()) {
//            return null;
//        }
		// VDTRMASTER-5995
		String posterUrl = (String) SharedMemory.getInstance().get(SharedDataKeys.VOD_POSTER_URL);

		DebugScreen.getInstance().addLog(posterUrl + name);

        try {
            Object obj = SharedMemory.getInstance().get(SharedDataKeys.VOD_POSTER_IMAGE);
            if (obj == null || name == null) {
                return null;
            }
            Method m = obj.getClass().getMethod("getImage", new Class[]{String.class, String.class});
            if (m != null) {
                poster = (Image) m.invoke(obj, new Object[]{null, name});
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (poster == null) {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getVODPosterImage] Create Default VOD Poster Image.");
			}
        	poster = DataCenter.getInstance().getImage("poster_size_s.png");
        }
        if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getVODPosterImage] result: " + poster);
		}
        return poster;
    }
	
	private ResourceList request(Class clazz, String reqUrlStr, boolean hasReturnedCount) throws SearchException{
		curRetryCount = 0;
		return requestData(clazz, reqUrlStr, hasReturnedCount);
	}
	
	private ResourceList requestData(Class clazz, String reqUrlStr, boolean hasReturnedCount) throws SearchException{
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.requestData] Called. Retry count: " + curRetryCount + ", hasReturnedCount: " + hasReturnedCount);
		}
		HttpResponse httpRes = null;
		HttpURLConnection con = null;
		
		ResourceList resourceList = null;
        try {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.requestData] urlStrb: " +reqUrlStr);
			}
        	
        	URL url = getURL(reqUrlStr);

			// R7.2
			DebugScreen.getInstance().addLog(url.toString());

        	con = (HttpURLConnection) url.openConnection();
        	int code = con.getResponseCode();
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.requestData] Open connection code: " + code);
			}
//	        if (code == HttpURLConnection.HTTP_NOT_FOUND || code == HttpURLConnection.HTTP_INTERNAL_ERROR) {
//	        	throw new SearchException(Rs.ERROR_SCH500, con.getResponseMessage());
//	        }
        	httpRes = new HttpResponse(con);
        	if (hasReturnedCount) {
            	resourceList = new ResourceList();
            	resourceList.parse(clazz, httpRes);
        	} else {
        		resourceList = (ResourceList)JsonUtil.parseResponse(clazz, httpRes);
        	}
		} catch (SearchException e1) {
			boolean isAvailableRetry = isAvailableRetry();
			if (isAvailableRetry) {
				return requestData(clazz, reqUrlStr, hasReturnedCount);
			}
			
			// Convert from Backend error code to Search Error Code.
			throw new SearchException(covertToSearchErrorCode(e1.getCode()), e1.getMessage());
		} catch (Exception e1) {
			boolean isAvailableRetry = isAvailableRetry();
			if (isAvailableRetry) {
				return requestData(clazz, reqUrlStr, hasReturnedCount);
			}
			throw new SearchException(Rs.ERROR_SCH500, e1.getMessage());
		} finally {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.requestData] finnally called.");
			}
			if (httpRes != null) {
			    try {
					httpRes.disconnect();
					httpRes = null;
			    } catch (Exception e) {
			        Log.print(e);
			    }
			}
			if (con != null) {
			    try {
			    	con.disconnect();
			    	con = null;
			    } catch (Exception e) {
			        Log.print(e);
			    }
			}
		}
		return resourceList;
	}
	
	private boolean isAvailableRetry() {
		boolean isAvailableRetry = false;
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.isAvailableRetry] Current - Max: " + curRetryCount + " - " + maxRetryCount);
		}
		if (curRetryCount < maxRetryCount) {
			curRetryCount ++;
			setNextProxyInfo();
			isAvailableRetry = true;
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.isAvailableRetry] isAvailableRetry: " + isAvailableRetry);
		}
		return isAvailableRetry;
	}
	
	private void setNextProxyInfo() {
        if (proxyIps != null && proxyIps.length != 0) {
            curProxyIdx = (curProxyIdx + 1) % proxyIps.length;
        }
        if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.setNextProxyInfo] Next Proxy index: " + curProxyIdx);
		}
	}

	private URL getURL(String query) throws Exception {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getURL] Called. Current proxy index: "+curProxyIdx);
		}
		URL url = null;
		if (!Environment.EMULATOR && proxyIps != null && proxyIps.length > 0) {
			int proxyPort = 0;
			if (proxyPorts != null && curProxyIdx < proxyPorts.length) {
				proxyPort = proxyPorts[curProxyIdx];
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getURL] Using proxy information.: " + proxyIps[curProxyIdx]+":"+proxyPort);
			}
			url = new URL("HTTP", proxyIps[curProxyIdx], proxyPort, query);
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.getURL] No Proxy information");
			}
			url = new URL(query);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchDataManager.getURL] url: " + url);
		}
		return url;
	}

    public void setMacAddress() {
    	if (Log.INFO_ON) {
			Log.printInfo("[SearchDataManager.setMacAddress] Called.");
		}
        if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
                || Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.setMacAddress] Cisco or Emulator vendor");
			}
            macAddress = Host.getInstance().getReverseChannelMAC();
            String[] token = TextUtil.tokenize(macAddress, ":");
            StringBuffer address = new StringBuffer();
            for (int i = 0; i < token.length; i++) {
                address.append(token[i]);
            }
            macAddress = address.toString().toUpperCase();
        } else {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.setMacAddress] Other vendor");
			}
            try {
                MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
                if (monitor != null) {
                    byte[] addr = monitor.getCableCardMacAddress();
                    macAddress = getStringBYTEArrayOffset(addr, 0);
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
    	if (Log.INFO_ON) {
			Log.printInfo("[SearchDataManager.setMacAddress] macAddress: " + macAddress);
		}
    }
    
    private String getStringBYTEArrayOffset(byte[] data, int offset) {
        if (data == null) {
            return "";
        }

        StringBuffer buff = new StringBuffer(4096);
        for (int i = offset; i < offset + data.length; i++) {
            buff.append(getHexStringBYTE(data[i]));
            if (i != data.length - 1) {
                buff.append("");
            }
        }
        return buff.toString();
    }
    
    private String getHexStringBYTE(byte b) {
        String str = "";

        // handle the case of -1
        if (b == -1) {
            str = "FF";
            return str;
        }

        int value = (((int) b) & 0xFF);

        if (value < 16) {
            // pad out string to make it look nice
            str = "0";
        }
        str += (Integer.toHexString(value)).toUpperCase();
        return str;
    }
	
    private int getInteger(Object o) {
        try {
            return ((Integer) o).intValue();
        } catch (ClassCastException ex) {
            try {
                return Integer.parseInt(o.toString());
            } catch (Exception e) {
            }
        } catch (Exception ex) {
        }
        return 0;
    }
	
	private String makeSearchQuery(int reqType, String keyword, int from, int to, String criteria, boolean showAudltContent) {
		switch(reqType) {
			case REQUEST_LINEAR:
				break;
			case REQUEST_VOD:
				break;
			case REQUEST_PVR:
				break;
			case REQUEST_PERSON:
				break;
		}
		return null;
	}
	
	private void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private String covertToSearchErrorCode(String backendErrorCode) {
		String errorCode = DEFAULT_ERROR_CODE;
		if (backendErrorCode == null) {
			return errorCode;
		}
		String upperBackendErrorCode = backendErrorCode.toUpperCase();
		if (upperBackendErrorCode.startsWith(HEADER_BACKEND_ERROR_CODE)) {
			for (int i = 0; i<ERROR_CODE_TABLE.length; i++ ) {
				if (ERROR_CODE_TABLE[i][INDEX_BACKEND_ERROR_CODE].equals(upperBackendErrorCode)) {
					errorCode = ERROR_CODE_TABLE[i][INDEX_SEARCH_ERROR_CODE];
					break;
				}
			}
		} else {
			errorCode = backendErrorCode;
		}
		return errorCode;
	}
	
    class SortByDate implements Comparator {
        public int compare(Object arg0, Object arg1) {
        	SearchedPvrGroup t0 = (SearchedPvrGroup)arg0;
        	SearchedPvrGroup t1 = (SearchedPvrGroup)arg1;
        	String title0 = t0.getTitle();
        	String title1 = t1.getTitle();
            long recordingStartTimeMillis0 = t0.getRecordingStartMillis();
            long recordingStartTimeMillis1 = t1.getRecordingStartMillis();
            int res = -1;
            if (recordingStartTimeMillis1 > recordingStartTimeMillis0) {
            	res = 1;
            }
            if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.SortByDate.compare]["+title0+"] recordingStartTimeMillis0: " + recordingStartTimeMillis0);
				Log.printDebug("[SearchDataManager.SortByDate.compare]["+title1+"] recordingStartTimeMillis1: " + recordingStartTimeMillis1);
				Log.printDebug("[SearchDataManager.SortByDate.compare] res: " + res);
			}
            return res; 
        }
    }
    class SortByAlphabet implements Comparator {
        Collator collator = Collator.getInstance(Locale.CANADA_FRENCH);
        /**
         * Compares its two arguments for order.
         * @param arg0 1st argument
         * @param arg1 2nd argument
         * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or
         *         greater than the second.
         */
        public int compare(Object arg0, Object arg1) {
        	SearchedPvrGroup t0 = (SearchedPvrGroup)arg0;
        	SearchedPvrGroup t1 = (SearchedPvrGroup)arg1;
        	String title0 = t0.getTitle();
        	String title1 = t1.getTitle();
        	int result = collator.compare(title0, title1);
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SearchDataManager.SortByAlphabet.compare] title0: " + title0);
				Log.printDebug("[SearchDataManager.SortByAlphabet.compare] title1: " + title1);
				Log.printDebug("[SearchDataManager.SortByAlphabet.compare] result: " + result);
			}
            return result;
        }
    }
}

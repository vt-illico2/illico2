/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import java.util.Iterator;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.data.JsonDef;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;

/**
 * <code>SuggestedKeyword</code>
 * 
 * @version $Id: SuggestedKeyword.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 26.
 * @description
 */
public class SuggestedKeyword extends ResourceList {
	private String[] suggestedKeywords;
	
	public Resource parse(Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SuggestedKeyword.parse] Called.");
		}
		
		JSONObject jsonObj = (JSONObject) obj;
		
		JSONObject body = (JSONObject)jsonObj.get(JsonDef.BODY);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SuggestedKeyword.parse] body: " + body);
		}
		JSONArray jsonArray = (JSONArray)body.get(JsonDef.SUGGESTED_TERMS);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SuggestedKeyword.parse] jsonArray: " + jsonArray);
		}
		Vector vec = new Vector();
		if (jsonArray != null) {
            Iterator iter = jsonArray.iterator();
            while (iter.hasNext()) {
                String suggestedKeyword = (String)iter.next();
                if (Log.DEBUG_ON) {
					Log.printDebug("[SuggestedKeyword.parse] suggestedKeyword: " + suggestedKeyword);
				}
                vec.add(suggestedKeyword);
            }
            int vecSize = vec.size();
            if (vecSize > 0) {
                suggestedKeywords = new String[vecSize];
                suggestedKeywords = (String[])vec.toArray(suggestedKeywords);
            }
            jsonObj.clear();
            jsonArray.clear();
            vec.clear();
            jsonArray = null;
            vec = null;
        }
		return this;
	}

	public void setSuggestedKeywords(String[] reqSuggestedKeywords) {
		suggestedKeywords = reqSuggestedKeywords;
	}

	public String[] getSuggestedKeywords() {
		return suggestedKeywords;
	}
}

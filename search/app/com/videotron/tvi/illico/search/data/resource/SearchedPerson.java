/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;

/**
 * <code>SearchedPerson</code>
 * 
 * @version $Id: SearchedPerson.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author ZioN
 * @since 2015. 3. 30.
 * @description
 */
public class SearchedPerson extends ResourceList {
	public Resource parse(Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedPerson.parse] Called.");
		}
		setTitle(String.valueOf(obj));
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedPerson.parse] title: " + getTitle());
		}
//		String[] names = JsonUtil.getStringArrays(jsonObj, JsonDef.NAMES);
//		if (Log.DEBUG_ON) {
//			Log.printDebug("[SearchedPerson.parse] names: " + names);
//		}
//		Vector vec = new Vector();
//		if (names != null) {
//			for (int i = 0; i<names.length; i++) {
//				if (Log.DEBUG_ON) {
//					Log.printDebug("[SearchedPerson.parse] names[" +i + "]: " + names[i]);
//				}
//				SearchedPerson person = new SearchedPerson();
//				person.setName(names[i]);
//				vec.add(person);
//			}
//		}
//		int vecSize = vec.size();
//		SearchedPerson[] searchedPersons = new SearchedPerson[vecSize];
//		searchedPersons = (SearchedPerson[]) vec.toArray(searchedPersons);
//		setListData(searchedPersons);
		return this;
	}
}

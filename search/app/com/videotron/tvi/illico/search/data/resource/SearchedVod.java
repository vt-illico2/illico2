/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import java.util.Iterator;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.DateUtil;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.JsonDef;
import com.videotron.tvi.illico.search.data.JsonUtil;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;

/**
 * <code>SearchedVod</code>
 * 
 * @version $Id: SearchedVod.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author ZioN
 * @since 2015. 3. 30.
 * @description
 */
public class SearchedVod extends ResourceList {
	public static final int IMAGE_TYPE_PORTRAIT = 0;
	public static final int IMAGE_TYPE_LANDSCAPE = 1;
	
	private String vodId;
	private String seasonNo;
	private HashData type;
	private HashData[] genres;
	private int duration;
	private String[] definitions;
	private String rating;
	private String originalTitle;
	private String realOriginalTitle;
	private String description;
	private String contentImageName;
	private String[] languages;
	private String channelCallLetter;
	private String channelName;
	private boolean isChannelFreePreview;
	private boolean isChannelOnDemandContent;
	private boolean isFreeContent;
	private boolean isPayPerViewContent;
	private boolean isClubContent;
	private int productionYear = -1;
	
	private int imageWidth;
	private int imageHeight;
	private int imageType;
	
	public Resource parse(Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedVod.parse] Called.");
		}
		JSONObject jsonObj = (JSONObject) obj;
		
		vodId = JsonUtil.getString(jsonObj, JsonDef.VOD_ID);
		seasonNo = JsonUtil.getString(jsonObj, JsonDef.SEASON_NO);
		Object typeObj = jsonObj.get(JsonDef.TYPE);
		if (typeObj != null) {
			type = (HashData)new HashData().parse(typeObj);
		}
		String tempProductionYear = JsonUtil.getString(jsonObj, JsonDef.PRODUCTION_YEAR);
		if (tempProductionYear != null) {
			long productionYearMillis = DateUtil.parse(tempProductionYear).getTime();	
			productionYear = DateUtil.getYear(productionYearMillis);
		}
		setGenre((JSONArray)jsonObj.get(JsonDef.GENRES));
		duration = JsonUtil.getInt(jsonObj, JsonDef.DURATION);
		String tempDefinitions = JsonUtil.getString(jsonObj, JsonDef.DEFINITIONS);
		if (tempDefinitions != null) {
			definitions = JsonUtil.getTokenString(tempDefinitions, "|");
		}
		rating = JsonUtil.getString(jsonObj, JsonDef.RATING);
		
		String tempTitle = JsonUtil.getString(jsonObj, JsonDef.TITLE);
		if (type.getCode() == Definition.VOD_TYPE_MOVIE && productionYear > 0) {
			tempTitle += " (" + productionYear +")";
		}
		setTitle(tempTitle);
		originalTitle = JsonUtil.getString(jsonObj, JsonDef.ORIGINAL_TITLE);
		description = JsonUtil.getString(jsonObj, JsonDef.DESCRIPTION);
		setImageInformation(JsonUtil.getString(jsonObj, JsonDef.CONTENT_IMAGE_NAME));
		String tempLanguages = JsonUtil.getString(jsonObj, JsonDef.LANGUAGES);
		if (tempLanguages != null) {
			languages = JsonUtil.getTokenString(tempLanguages, "|");
		}
		channelCallLetter = JsonUtil.getString(jsonObj, JsonDef.CHANNEL_CALL_LETTERS);
		channelName = JsonUtil.getString(jsonObj, JsonDef.CHANNEL_NAME);
		isChannelFreePreview = JsonUtil.getBoolean(jsonObj, JsonDef.CHANNEL_FREE_PREVIEW_INDICATOR);
		isFreeContent = JsonUtil.getBoolean(jsonObj, JsonDef.FREE_CONTENT_INDICATOR);
		isPayPerViewContent = JsonUtil.getBoolean(jsonObj, JsonDef.PAY_PER_VIEW_CONTENT_INDICATOR);
		isClubContent = JsonUtil.getBoolean(jsonObj, JsonDef.CLUB_CONTENT_INDICATOR);
		
		jsonObj.clear();
		
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedVod.parse] *************************************"+ "\n"+
					"vodId: " + vodId + "\n"+
					", productionYear: " + productionYear + "\n"+
					", type: " + type + "\n"+
					", seasonNo : " + seasonNo + "\n"+
					", duration : " + duration + "\n"+
					", rating : " + rating + "\n"+
					", title : " + getTitle() + "\n"+
					", originalTitle : " + originalTitle + "\n"+
					", description : " + description + "\n"+
					", contentImage : " + contentImageName + "\n"+
					", channelCallLetter : " + channelCallLetter + "\n"+
					", channelName : " + channelName + "\n"+
					", isChannelFreePreview : " + isChannelFreePreview + "\n"+
					", isChannelFreePreview : " + isChannelOnDemandContent + "\n"+
					", isFreeContent : " + isFreeContent + "\n"+
					", isPayPerViewContent : " + isPayPerViewContent + "\n"+
					", isClubContent : " + isClubContent + "\n"+
					"***********************************************************"
			);
			if (type != null) {
				Log.printDebug("[SearchedVod.parse] type.code : " + type.getCode() + ", type.description: " + type.getTitle());
			}
			
			if (genres != null) {
				for (int i = 0 ;i<genres.length; i++) {
					Log.printDebug("[SearchedVod.parse] Genre["+i+"].code : " + genres[i].getCode() + ", Genre["+i+"].description: " + genres[i].getTitle());
				}
			}
			
			if (definitions != null) {
				for (int i = 0 ;i<definitions.length; i++) {
					Log.printDebug("[SearchedVod.parse] Definition["+i+"] : " + definitions[i]);
				}
			}
			
			if (languages != null) {
				for (int i = 0 ;i<languages.length; i++) {
					Log.printDebug("[SearchedVod.parse] Languages["+i+"] : " + languages[i]);
				}
			}
		}
		return this;
	}
	
	public void setImageInformation(String reqImageInfo) {
		if (reqImageInfo != null) {
			int idx = reqImageInfo.indexOf("/");
			if (idx != -1) {
				String typeInfo = reqImageInfo.substring(0,  idx);
				int wIdx = reqImageInfo.indexOf("W");
				int hIdx = reqImageInfo.indexOf("H");
				if (wIdx >=0 && hIdx >=0) {
					try{
						imageWidth = Integer.parseInt(typeInfo.substring(wIdx+1,  hIdx));
					}catch(Exception e) {
					}
					try{
						imageHeight = Integer.parseInt(typeInfo.substring(hIdx+1));
					}catch(Exception e) {
					}
					if (imageHeight >  imageWidth) {
						imageType = IMAGE_TYPE_PORTRAIT;
					} else {
						imageType = IMAGE_TYPE_LANDSCAPE;
					}
				}
			}
		}
		contentImageName = reqImageInfo;
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedVod.setImageInformation] contentImageName: " + contentImageName);
			Log.printDebug("[SearchedVod.setImageInformation] imageWidth: " + imageWidth);
			Log.printDebug("[SearchedVod.setImageInformation] imageHeight: " + imageHeight);
		}
	}
	
	public int getImageType() {
		return imageType;
	}

	public void setGenre(JSONArray jsonArray) {
		Vector tempVec = new Vector();
		if (jsonArray != null) {
            Iterator iter = jsonArray.iterator();
            while (iter.hasNext()) {
                JSONObject dataObj = (JSONObject)iter.next();
                if (dataObj != null) {
                	tempVec.add(new HashData().parse(dataObj));
                }
                dataObj.clear();
            }
            int tempVecSize = tempVec.size();
            genres = new HashData[tempVecSize];
            genres = (HashData[]) tempVec.toArray(genres);
            jsonArray.clear();
            tempVec.clear();
            jsonArray = null;
            tempVec = null;
        }
	}

	public String getVodId() {
		return vodId;
	}

	public String getSeasonNo() {
		return seasonNo;
	}

	public HashData getType() {
		return type;
	}

	public HashData[] getGenres() {
		return genres;
	}

	public int getDuration() {
		return duration;
	}

	public String[] getDefinitions() {
		return definitions;
	}
	
	public boolean isAdaptedDefinition(String reqDefinition) {
		if (definitions == null || reqDefinition ==null) {
			return false;
		}
		for (int i=0; i<definitions.length; i++) {
			if (definitions[i] != null && reqDefinition.equalsIgnoreCase(definitions[i])) {
				return true;
			}
		}
		return false;
	}

	public String getRating() {
		return rating;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}
	
	public String getRealOriginalTitle() {
    	if (realOriginalTitle == null) {
    		realOriginalTitle = Util.getStringExceptString(originalTitle, Rs.TAG_EM_START);
    		realOriginalTitle = Util.getStringExceptString(realOriginalTitle, Rs.TAG_EM_END);
    	}
    	return realOriginalTitle;
	}
	
	public String getDescription() {
		return description;
	}

	public String getContentImage() {
		return contentImageName;
	}

	public String[] getLanguages() {
		return languages;
	}
	
	public boolean isAdaptedLanguage(String reqLanguage) {
		if (languages == null || reqLanguage ==null) {
			return false;
		}
		for (int i=0; i<languages.length; i++) {
			if (languages[i] != null && reqLanguage.equalsIgnoreCase(languages[i])) {
				return true;
			}
		}
		return false;
	}

	public String getChannelCallLetter() {
		return channelCallLetter;
	}

	public String getChannelName() {
		return channelName;
	}

	public boolean isChannelFreePreview() {
		return isChannelFreePreview;
	}

//	public boolean isChannelOnDemandContent() {
//		return isChannelOnDemandContent;
//	}
	
	public int getProductionYear() {
		return productionYear;
	}

	public boolean isFreeContent() {
		return isFreeContent;
	}

	public boolean isPayPerViewContent() {
		return isPayPerViewContent;
	}

	public boolean isClubContent() {
		return isClubContent;
	}
	
	public boolean isChannelOnDemandContent() {
		return channelCallLetter != null;
	}
	
	public boolean isRegularContent() {
		return !isChannelOnDemandContent() && !isClubContent;
	}
}

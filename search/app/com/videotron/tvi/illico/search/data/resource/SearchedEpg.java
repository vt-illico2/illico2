/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Calendar;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.DateUtil;
import com.videotron.tvi.illico.search.data.JsonDef;
import com.videotron.tvi.illico.search.data.JsonUtil;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;
import com.videotron.tvi.illico.search.data.SearchDataManager;

/**
 * <code>SearchedEpg</code>
 * 
 * @version $Id: SearchedEpg.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 30.
 * @description
 */
public class SearchedEpg extends ResourceList{
	private String epgId;
	private String seasonNo;
	private String episodeNo;
	private HashData type;
	private HashData[] genres;
	private int duration;
	private String definition;
	private String rating;
//	private String title;
	private String description;
	private String channelCallLetter;
	private String channelName;
	private String channelGridPositions;
	private boolean isFreePreview;
	private boolean isPayPerView;
	private long startTimeMillis;
	private long endTimeMillis;
	
	public Resource parse(Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedLinear.parse] Called.");
		}
		JSONObject jsonObj = (JSONObject)obj;
		
		epgId = JsonUtil.getString(jsonObj, JsonDef.EPG_ID);
		seasonNo = JsonUtil.getString(jsonObj, JsonDef.SEASON_NO);
		episodeNo = JsonUtil.getString(jsonObj, JsonDef.EPISODE_NO);
		Object typeObj = jsonObj.get(JsonDef.TYPE);
		if (typeObj != null) {
			type = (HashData)new HashData().parse(typeObj);
		}
		setGenre((JSONArray)jsonObj.get(JsonDef.GENRES));
		duration = JsonUtil.getInt(jsonObj, JsonDef.DURATION);
		definition = JsonUtil.getString(jsonObj, JsonDef.DEFINITION);
		rating = JsonUtil.getString(jsonObj, JsonDef.RATING);
		setTitle(JsonUtil.getString(jsonObj, JsonDef.TITLE));
		description = JsonUtil.getString(jsonObj, JsonDef.DESCRIPTION);
		channelCallLetter = JsonUtil.getString(jsonObj, JsonDef.CHANNEL_CALL_LETTERS);
		channelName = JsonUtil.getString(jsonObj, JsonDef.CHANNEL_NAME);
//		channelGridPositions = JsonUtil.getString(jsonObj, JsonDef.CHANNEL_GRID_POSITIONS);
		setChannelGridPositions(JsonUtil.getString(jsonObj, JsonDef.CHANNEL_GRID_POSITIONS));
		isFreePreview = JsonUtil.getBoolean(jsonObj, JsonDef.CHANNEL_FREE_PREVIEW_INDICATOR);
		isPayPerView = JsonUtil.getBoolean(jsonObj, JsonDef.PAY_PER_VIEW_CONTENT_INDICATOR);
		
		String tempStartTime = JsonUtil.getString(jsonObj, JsonDef.STARTTIME);
		if (tempStartTime != null) {
			startTimeMillis = DateUtil.parse(tempStartTime).getTime();			
		}
		
		String tempEndTime = JsonUtil.getString(jsonObj, JsonDef.ENDTIME);
		if (tempEndTime != null) {
			endTimeMillis = DateUtil.parse(tempEndTime).getTime();
		}
		
		jsonObj.clear();
		
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedLinear.parse] *************************************"+ "\n"+
					"epgId: " + epgId + "\n"+
					", seasonNo : " + seasonNo + "\n"+
					", episodeNo : " + episodeNo + "\n"+
					", type : " + type + "\n"+
					", duration : " + duration + "\n"+
					", rating : " + rating + "\n"+
					", title : " + getTitle() + "\n"+
					", description : " + description + "\n"+
					", channelCallLetter : " + channelCallLetter + "\n"+
					", channelName : " + channelName + "\n"+
					", channelGridPositions : " + channelGridPositions + "\n"+
					", freePreviewIndicator : " + isFreePreview + "\n"+
					", payPerViewIndicator : " + isPayPerView + "\n"+
					", tempStartTime : " + tempStartTime + "\n"+
					", startTimeMillis : " + startTimeMillis + "\n"+
					", tempEndTime : " + tempEndTime + "\n"+
					", endTimeMillis : " + endTimeMillis  + "\n"+
					"***********************************************************"
			);
			if (type != null) {
				Log.printDebug("[SearchedLinear.parse] type.code : " + type.getCode() + ", type.description: " + type.getTitle());
			}
			
			if (genres != null) {
				for (int i = 0 ;i<genres.length; i++) {
					Log.printDebug("[SearchedLinear.parse] Genre["+i+"].code : " + genres[i].getCode() + ", Genre["+i+"].description: " + genres[i].getTitle());
				}
			}
		}
		return this;
	}
	
	private void setChannelGridPositions(String reqChannelGridPositions) {
		channelGridPositions ="";
		if (reqChannelGridPositions != null) {
			StringTokenizer st = new StringTokenizer(reqChannelGridPositions, "|");
			int stCount = st.countTokens();
			for (int i=0; i<stCount; i++) {
				channelGridPositions+= st.nextToken();
				if (i != stCount - 1) {
					channelGridPositions += " | ";
				}
			}
		}
	}

	public String getEpgId() {
		return epgId;
	}

	public String getSeasonNo() {
		return seasonNo;
	}

	public String getEpisodeNo() {
		return episodeNo;
	}

	public HashData getType() {
		return type;
	}

	public HashData[] getGenres() {
		return genres;
	}

	public int getDuration() {
		return duration;
	}

	public String getDefinition() {
		return definition;
	}

	public String getRating() {
		return rating;
	}

//	public String getTitle() {
//		return title;
//	}
	
	public String getDescription() {
		return description;
	}

	public String getChannelCallLetter() {
		return channelCallLetter;
	}

	public String getChannelName() {
		return channelName;
	}

	public String getChannelGridPositions() {
		return channelGridPositions;
	}

	public boolean isFreePreview() {
		return isFreePreview;
	}

	public boolean isPayPerView() {
		return isPayPerView;
	}

    private static long applyYearShift(long timeMillis) {
        if (Log.INFO_ON) Log.printInfo("applyYearShift : before "+timeMillis);
        int yearShift = SearchDataManager.getInstance().getYearShift();
        if (yearShift == 0) {
            return timeMillis;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);
        calendar.add(Calendar.YEAR, -yearShift);

        long applied = calendar.getTimeInMillis();
        if (Log.INFO_ON) Log.printInfo("applyYearShift : after "+applied);
        return applied;
    }

	public long getStartTimeMillis() {
        return applyYearShift(startTimeMillis);
	}

	public long getEndTimeMillis() {
		return endTimeMillis;
	}
	
	public void setGenre(JSONArray jsonArray) {
		Vector tempVec = new Vector();
		if (jsonArray != null) {
            Iterator iter = jsonArray.iterator();
            while (iter.hasNext()) {
                JSONObject dataObj = (JSONObject)iter.next();
                if (dataObj != null) {
                	tempVec.add(new HashData().parse(dataObj));
                }
                dataObj.clear();
            }
            int tempVecSize = tempVec.size();
            genres = new HashData[tempVecSize];
            genres = (HashData[]) tempVec.toArray(genres);
            jsonArray.clear();
            tempVec.clear();
            jsonArray = null;
            tempVec = null;
        }
	}
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.pvr.AppDataKeys;
import com.videotron.tvi.illico.ixc.pvr.ContentsChangedListener;
import com.videotron.tvi.illico.ixc.pvr.PvrContent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.pvr.RecordedContent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.util.Environment;

/**
 * <code>PvrDataManager</code>
 * 
 * @version $Id: PvrDataManager.java,v 1.4.6.2 2017/09/21 18:58:57 freelife Exp $
 * @author Administrator
 * @since 2015. 4. 2.
 * @description
 */
public class PvrDataManager implements ContentsChangedListener {
	private static PvrDataManager instance;
	
	private Hashtable recordings;
	
	private boolean updatedPvrContents = false; // if false, reset all PVR contents
	
	private PvrDataManager() {
	}
	
	public synchronized static PvrDataManager getInstance() {
		if (instance == null) {
			instance = new PvrDataManager(); 
		}
		return instance;
	}

	private void setPvrData() {
		if (Log.ALL_ON) {
			Log.printDebug("[PvrDataManager.setPvrData] Called.");
		}
		if (updatedPvrContents) {
			if (Log.ALL_ON) {
				Log.printDebug("[PvrDataManager.setPvrData] No updated PVR information.");
			}
			return;
		}
		updatedPvrContents = true;
		if (Environment.EMULATOR) {
			createTmpPvrData();
			return;
		}
        try {
			PvrService pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
			if (pvrService == null) {
				if (Log.WARNING_ON) {
					Log.printWarning("[PvrDataManager.setPvrData] PvrService is null. PvrDataManager can not make pvr data.");
				}
				return;
			}
			
            String[] ids = pvrService.getRecordedContents();
            if (Log.ALL_ON) {
				Log.printDebug("[PvrDataManager.setPvrData] Recorded contents count is " + ids.length);
			}
            if (recordings != null) {
                recordings.clear();
            } else {
                recordings = new Hashtable();
            }
            if (Log.ALL_ON) {
            	if (ids != null) {
                	StringBuffer st = new StringBuffer();
                	for (int i = 0; i<ids.length; i++) {
                		st.append(ids[i]);
                		if (i != ids.length-1) {
                			st.append(",");
                		}
                	}
                	Log.printDebug("[PvrDataManager.setPvrData] IDS="+st.toString());
            	}
			}
            for (int i = 0; i < ids.length; i++) {
            	RecordedContent content =  (RecordedContent) pvrService.getContent(ids[i]);
            	printRecordedContent(content);
                recordings.put(ids[i],content);
            }
        } catch (Exception e) {
        	updatedPvrContents = false;
        }
	}
	
	public RecordedContent[] getRecordedContent(String keyword, int filterId) {
		if (Log.ALL_ON) {
			Log.printDebug("[PvrDataManager.getRecordedContent] Called. Keyword: " + keyword +", filterId: " + SearchFilterDef.getSectionFilterText(filterId));
		}
		setPvrData();
		if (filterId == SearchFilterDef.ID_PERSON) {
			return new RecordedContent[0];
		}
		boolean isShowAdultContent = (filterId == SearchFilterDef.ID_ADULT);
		
		Vector searchedRecordedContents = new Vector();
		String upperedKeyword = keyword.toUpperCase();
		Enumeration enu = recordings.keys();
		while(enu.hasMoreElements()) {
			String key = (String) enu.nextElement();
			RecordedContent content =  (RecordedContent) recordings.get(key);
			if (Log.ALL_ON) {
				Log.printDebug("[PvrDataManager.getRecordedContent] key-content: " + key +"-" + content);
			}
			try {
                //->Kenneth[2017.8.22] R7.4 : Title 대신에 EnglishTitle 을 사용한다. 그래야 french accents 관계없이 filtering 됨
				// Check matched keyword.
				//R7.4 VDTRMASTER-6207 sykim : keyword로 부터 검색 할 경우 French가 들어올 수 있으므로, Eng, Fr 둘다 검색해야함 
				String engContentTitle = content.getEnglishTitle();				
				if (engContentTitle == null || engContentTitle.length() == 0) {
					engContentTitle = content.getTitle();
	            }
				String upperedEngContentTitle = engContentTitle.toUpperCase();
				String contentTitle = content.getTitle();				
                //if (contentTitle == null || contentTitle.length() == 0) {
                //   contentTitle = content.getTitle();
                //}
				//String contentTitle = content.getTitle();
                //<-
				String upperedContentTitle = contentTitle.toUpperCase();
				if (Log.ALL_ON) {
					Log.printDebug("[PvrDataManager.getRecordedContent] upperedContentTitle: " + upperedContentTitle);
					Log.printDebug("[PvrDataManager.getRecordedContent] upperedEngContentTitle: " + upperedEngContentTitle);
				}
				if (upperedContentTitle.indexOf(upperedKeyword) < 0 && 
						upperedEngContentTitle.indexOf(upperedKeyword) < 0) {
		        	if (!Environment.EMULATOR && !Rs.PVR_TEST) {
						if (Log.ALL_ON) {
							Log.printDebug("[PvrDataManager.getRecordedContent][" + contentTitle +"] did not match. keyword");
						}
		        		continue;
		        	}
				}
				
				// Check content state.
				int contentState = content.getContentState();
				if (Log.ALL_ON) {
					Log.printDebug("[PvrDataManager.getRecordedContent] contentState: " + contentState);
				}
		        if (contentState == PvrContent.STATE_SCHEDULED || contentState == PvrContent.STATE_RECORDING) {
		        	if (!Environment.EMULATOR && !Rs.PVR_TEST) {
		        		if (Log.ALL_ON) {
							Log.printDebug("[PvrDataManager.getRecordedContent]["+contentTitle+"] state is not valid.");
						}
		        		continue;
		        	}
		        }
				
				// Check Adult Content
				if (Log.ALL_ON) {
					Log.printDebug("[PvrDataManager.getRecordedContent] isShowAdultContent: " + isShowAdultContent);
				}
				String parentalRating = (String) content.getAppData(AppDataKeys.PARENTAL_RATING);
				if (Log.ALL_ON) {
					Log.printDebug("[PvrDataManager.getRecordedContent] parentalRating: " + parentalRating);
				}
				
				if (isShowAdultContent) {
					if (parentalRating == null || !parentalRating.equals(Definition.RATING_18)) {
		        		if (Log.ALL_ON) {
							Log.printDebug("[PvrDataManager.getRecordedContent]["+contentTitle+"] Rating is not valid. 18+ or less" + isShowAdultContent);
						}
						continue;
					}
				} else {
					if (parentalRating != null && parentalRating.equals(Definition.RATING_18)) {
		        		if (Log.ALL_ON) {
							Log.printDebug("[PvrDataManager.getRecordedContent]["+contentTitle+"] Rating is not valid. adult content");
						}
						continue;
					}
				}
                
                // filter Program type.
				int programType = Util.getContentInt(content.getAppData(AppDataKeys.PROGRAM_TYPE));
				if (Log.ALL_ON) {
					Log.printDebug("[PvrDataManager.getRecordedContent] programType: " + programType);
				}
				switch (filterId) {
					case SearchFilterDef.ID_EVENT:
						if (programType != Definition.PROGRAM_TYPE_EVENT) {
				        	if (!Environment.EMULATOR && !Rs.PVR_TEST) {
				        		if (Log.ALL_ON) {
									Log.printDebug("[PvrDataManager.getRecordedContent]["+contentTitle+"] type is not ID_EVENT.");
								}
				        		continue;
				        	}
						}
						break;
					case SearchFilterDef.ID_MOVIE:
						if (programType != Definition.PROGRAM_TYPE_FILM) {
				        	if (!Environment.EMULATOR && !Rs.PVR_TEST) {
				        		if (Log.ALL_ON) {
									Log.printDebug("[PvrDataManager.getRecordedContent]["+contentTitle+"] type is not ID_MOVIE.");
								}
				        		continue;
				        	}
						}
						break;
					case SearchFilterDef.ID_SERIES_AND_SHOW:
						if (programType != Definition.PROGRAM_TYPE_SERIES && programType != Definition.PROGRAM_TYPE_PROGRAM) {
				        	if (!Environment.EMULATOR && !Rs.PVR_TEST) {
				        		if (Log.ALL_ON) {
									Log.printDebug("[PvrDataManager.getRecordedContent]["+contentTitle+"] type is not ID_SERIES_AND_SHOW.");
								}
				        		continue;
				        	}
						}
						break;
					case SearchFilterDef.ID_YOUTH:
						int genre = Util.getContentInt(content.getAppData(AppDataKeys.CATEGORY));
						switch(genre) {
							case Definition.GENRE_CHILDREN:
								break;
							default:
					        	if (!Environment.EMULATOR && !Rs.PVR_TEST) {
					        		if (Log.ALL_ON) {
										Log.printDebug("[PvrDataManager.getRecordedContent]["+contentTitle+"] type is not ID_YOUTH.");
									}
					        		continue;
					        	}
						}
						break;
				}
				if (Log.DEBUG_ON) {
					Log.printDebug("[PvrDataManager.getRecordedContent][" + contentTitle +"] is matched content.");
				}
				searchedRecordedContents.add(content);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
		int searchedRecordedContentsSize = searchedRecordedContents.size();
		if (Log.ALL_ON) {
			Log.printDebug("[PvrDataManager.getRecordedContent] searchedRecordedContentsSize: " + searchedRecordedContentsSize);
		}
		RecordedContent[] contents = new RecordedContent[searchedRecordedContentsSize];
		contents = (RecordedContent[]) searchedRecordedContents.toArray(contents);
		return contents;
	}
	
	public void contentsChanged(String[] contents) throws RemoteException {
		updatedPvrContents = false;
	}

	public void contentsRescheduled(String[] contents) throws RemoteException {
		// Nothing to do.
	}
	
	private void printRecordedContent(RecordedContent content) {
		try {
			if (Log.ALL_ON) {
				String id = content.getId();
				//if (!Environment.SUPPORT_UHD && ar != null && ar.getDefinition() == TvChannel.DEFINITION_4K) {
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]*************************************************************");
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]ChannelName=" + content.getChannelName());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]ChannelNumber=" + content.getChannelNumber());
				int contentState = content.getContentState();
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]ContentState=" + contentState +"("+PvrContentDef.getContentStateText(contentState)+")");
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]Duration=" + content.getDuration());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]Location=" + content.getLocation());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]ProgramEndTime=" + content.getProgramEndTime());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]ProgramStartTime=" + content.getProgramStartTime());
				int recordingRequestState = content.getRecordingRequestState();
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]RecordingRequestState=" + recordingRequestState +"("+PvrContentDef.getRecordingRequestStateText(recordingRequestState)+")");
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]RecordingStartTime=" + content.getRecordingStartTime());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]ScheduledDuration=" + content.getScheduledDuration());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]ScheduledStartTime=" + content.getScheduledStartTime());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]Title=" + content.getTitle());
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]AUDIO_TYPE=" + content.getAppData(AppDataKeys.AUDIO_TYPE));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]BLOCK=" + content.getAppData(AppDataKeys.BLOCK));
				int category = Util.getContentInt(content.getAppData(AppDataKeys.CATEGORY));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]CATEGORY=" + category +"("+PvrContentDef.getCategoryText(category)+")");
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]CHANNEL_NAME=" + content.getAppData(AppDataKeys.CHANNEL_NAME));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]CHANNEL_NUMBER=" + content.getAppData(AppDataKeys.CHANNEL_NUMBER));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]CLOSED_CAPTION=" + content.getAppData(AppDataKeys.CLOSED_CAPTION));
				int creationMethod = Util.getContentInt(content.getAppData(AppDataKeys.CREATION_METHOD));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]CREATION_METHOD=" + creationMethod  + "("+PvrContentDef.getCreationMethodText(creationMethod)+")" );
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]CREATION_TIME=" + content.getAppData(AppDataKeys.CREATION_TIME));
				int definition = Util.getContentInt(content.getAppData(AppDataKeys.DEFINITION));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]DEFINITION=" + definition+ "("+PvrContentDef.getDefinitionText(definition)+")" );
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]EPISODE_TITLE=" + content.getAppData(AppDataKeys.EPISODE_TITLE));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]FAILED_REASON=" + content.getAppData(AppDataKeys.FAILED_REASON));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]FULL_DESCRIPTION=" + content.getAppData(AppDataKeys.FULL_DESCRIPTION));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]GROUP_KEY=" + content.getAppData(AppDataKeys.GROUP_KEY));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]KEEP_COUNT=" + content.getAppData(AppDataKeys.KEEP_COUNT));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]LANGUAGE=" + content.getAppData(AppDataKeys.LANGUAGE));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]LIVE=" + content.getAppData(AppDataKeys.LIVE));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]LOCAL_ID=" + content.getAppData(AppDataKeys.LOCAL_ID));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]NI_INDEX=" + content.getAppData(AppDataKeys.NI_INDEX));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]PARENTAL_RATING=" + content.getAppData(AppDataKeys.PARENTAL_RATING));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]PPV_EID=" + content.getAppData(AppDataKeys.PPV_EID));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]PRODUCTION_YEAR=" + content.getAppData(AppDataKeys.PRODUCTION_YEAR));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]PROGRAM_END_TIME=" + content.getAppData(AppDataKeys.PROGRAM_END_TIME));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]PROGRAM_ID=" + content.getAppData(AppDataKeys.PROGRAM_ID));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]PROGRAM_START_TIME=" + content.getAppData(AppDataKeys.PROGRAM_START_TIME));
				int programType = Util.getContentInt(content.getAppData(AppDataKeys.PROGRAM_TYPE));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]PROGRAM_TYPE=" + programType + "("+PvrContentDef.getProgramTypeText(programType)+")");
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SAP=" + content.getAppData(AppDataKeys.SAP));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SCHEDULED_DURATION=" + content.getAppData(AppDataKeys.SCHEDULED_DURATION));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SCHEDULED_START_TIME=" + content.getAppData(AppDataKeys.SCHEDULED_START_TIME));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SDV_STATUS=" + content.getAppData(AppDataKeys.SDV_STATUS));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SEGMENT_TIMES=" + content.getAppData(AppDataKeys.SEGMENT_TIMES));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SERIES_ID=" + content.getAppData(AppDataKeys.SERIES_ID));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SOURCE_ID=" + content.getAppData(AppDataKeys.SOURCE_ID));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]STAR_RATING=" + content.getAppData(AppDataKeys.STAR_RATING));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SUBCATEGORY=" + content.getAppData(AppDataKeys.SUBCATEGORY));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]SUPPRESS_CONFLICT=" + content.getAppData(AppDataKeys.SUPPRESS_CONFLICT));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]TITLE=" + content.getAppData(AppDataKeys.TITLE));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]UNSET=" + content.getAppData(AppDataKeys.UNSET));
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]VIEW_COUNT=" + content.getAppData(AppDataKeys.VIEW_COUNT));
				
				Log.printDebug("[PvrDataManager.printRecordedContent]["+id+"]*************************************************************\n");
			}
		} catch (RemoteException e1) {
			e1.printStackTrace();
		}
	}
	
	private void createTmpPvrData() {
		BufferedInputStream bufferedInputStream=null;
		FileInputStream fileInputStream=null;
		Properties prop=new Properties();
		try{
			fileInputStream=new FileInputStream("resource/sample/TempPvr.prop");
			bufferedInputStream=new BufferedInputStream(fileInputStream);
			prop.load(bufferedInputStream);
		} catch(Exception e){
			e.printStackTrace();
		} finally{
			if (bufferedInputStream!=null){
				try{
					bufferedInputStream.close();
				} catch(Exception e1){
					e1.printStackTrace();
				}
				bufferedInputStream=null;
			}
			if (fileInputStream!=null){
				try{
					fileInputStream.close();
				} catch(Exception e1){
					e1.printStackTrace();
				}
				fileInputStream=null;
			}
		}
		
		String tempIds = prop.getProperty("IDS");
		if (Log.ALL_ON) {
			Log.printDebug("[PvrDataManager.createTmpPvrData] IDs: " + tempIds);
			Log.printDebug("[PvrDataManager.createTmpPvrData] tt: " + prop.getProperty("[101]PARENTAL_RATING"));
		}
		StringTokenizer st = new StringTokenizer(tempIds, ",");
		int stCount = st.countTokens();
		String[] ids = new String[stCount];
		for (int i=0; i<stCount; i++) {
			ids[i] = st.nextToken();
		}
		
    	if (recordings == null) {
			recordings = new Hashtable();
		}
    	for (int i=0; i<ids.length; i++) {
    		RecordedContent rc = getTempRecordedConter(prop, ids[i]);
    		String id = null;
    		try {
				id = rc.getId();
				if (Log.ALL_ON) {
					Log.printDebug("[PvrDataManager.createTmpPvrData] Title: " + rc.getTitle());
				}
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
    		printRecordedContent(rc);
    		recordings.put(id, rc);
    	}
	}
	
	private RecordedContent getTempRecordedConter(final Properties prop, final String id) {
		RecordedContent rc = new RecordedContent(){
			private String key = "[" + id +"]";
			private String AUDIO_TYPE = prop.getProperty(key+"AUDIO_TYPE");
			private String BLOCK = prop.getProperty(key+"BLOCK");
			private String CATEGORY = prop.getProperty(key+"CATEGORY");
			private String CHANNEL_NAME = prop.getProperty(key+"CHANNEL_NAME");
			private String CHANNEL_NUMBER = prop.getProperty(key+"CHANNEL_NUMBER");
			private String CLOSED_CAPTION = prop.getProperty(key+"CLOSED_CAPTION");
			private String CREATION_METHOD = prop.getProperty(key+"CREATION_METHOD");
			private String CREATION_TIME = prop.getProperty(key+"CREATION_TIME");
			private String DEFINITION = prop.getProperty(key+"DEFINITION");
			private String EPISODE_TITLE = prop.getProperty(key+"EPISODE_TITLE");
			private String FAILED_REASON = prop.getProperty(key+"FAILED_REASON");
			private String FULL_DESCRIPTION = prop.getProperty(key+"FULL_DESCRIPTION");
			private String GROUP_KEY = prop.getProperty(key+"GROUP_KEY");
			private String KEEP_COUNT = prop.getProperty(key+"KEEP_COUNT");
			private String LANGUAGE = prop.getProperty(key+"LANGUAGE");
			private String LIVE = prop.getProperty(key+"LIVE");
			private String LOCAL_ID = prop.getProperty(key+"LOCAL_ID");
			private String NI_INDEX = prop.getProperty(key+"NI_INDEX");
			private String PARENTAL_RATING = prop.getProperty(key+"PARENTAL_RATING");
			private String PPV_EID = prop.getProperty(key+"PPV_EID");
			private String PRODUCTION_YEAR = prop.getProperty(key+"PRODUCTION_YEAR");
			private String PROGRAM_END_TIME = prop.getProperty(key+"PROGRAM_END_TIME");
			private String PROGRAM_ID = prop.getProperty(key+"PROGRAM_ID");
			private String PROGRAM_START_TIME = prop.getProperty(key+"PROGRAM_START_TIME");
			private String PROGRAM_TYPE = prop.getProperty(key+"PROGRAM_TYPE");
			private String SAP = prop.getProperty(key+"SAP");
			private String SCHEDULED_DURATION = prop.getProperty(key+"SCHEDULED_DURATION");
			private String SCHEDULED_START_TIME = prop.getProperty(key+"SCHEDULED_START_TIME");
			private String SDV_STATUS = prop.getProperty(key+"SDV_STATUS");
			private String SEGMENT_TIMES = prop.getProperty(key+"SEGMENT_TIMES");
			private String SERIES_ID = prop.getProperty(key+"SERIES_ID");
			private String SOURCE_ID = prop.getProperty(key+"SOURCE_ID");
			private String STAR_RATING = prop.getProperty(key+"STAR_RATING");
			private String SUBCATEGORY = prop.getProperty(key+"SUBCATEGORY");
			private String SUPPRESS_CONFLICT = prop.getProperty(key+"SUPPRESS_CONFLICT");
			private String TITLE = prop.getProperty(key+"TITLE");
            //->Kenneth[2017.8.22] R7.4
			private String ENGLISH_TITLE = prop.getProperty(key+"ENGLISH_TITLE");
            //<-
			private String VIEW_COUNT = prop.getProperty(key+"VIEW_COUNT");
			private String UNSET = prop.getProperty(key+"UNSET");
			
			private String pvrId = id;
			private String recordingRequestState = prop.getProperty(key+"RecordingRequestState");
			
			public String getId() throws RemoteException {
				return pvrId;
			}

			public int getRecordingRequestState() throws RemoteException {
				int res = 0;
				try{
					res = Integer.parseInt(recordingRequestState);
				}catch(Exception e) {
				}
				return res;
			}

			public short getContentState() throws RemoteException {
				String temp = prop.getProperty(key+"ContentState");
				return Short.parseShort(temp);
			}

			public String getTitle() throws RemoteException {
				return prop.getProperty(key+"Title");
			}

            //->Kenneth[2017.8.22] R7.4
			public String getEnglishTitle() throws RemoteException {
				return prop.getProperty(key+"ENGLISH_Title");
			}
            //<-

			public String getChannelName() throws RemoteException {
				return prop.getProperty(key+"ChannelName");
			}

			public long getProgramStartTime() throws RemoteException {
				String temp = prop.getProperty(key+"ProgramStartTime");
				return Long.parseLong(temp);
			}

			public long getProgramEndTime() throws RemoteException {
				String temp = prop.getProperty(key+"ProgramEndTime");
				return Long.parseLong(temp);
			}

			public int getChannelNumber() throws RemoteException {
				String temp = prop.getProperty(key+"ChannelNumber");
				return Integer.parseInt(temp);
			}

			public long getScheduledStartTime() throws RemoteException {
				String temp = prop.getProperty(key+"ScheduledStartTime");
				return Long.parseLong(temp);
			}

			public long getScheduledDuration() throws RemoteException {
				String temp = prop.getProperty(key+"ScheduledDuration");
				return Long.parseLong(temp);
			}

			public Serializable getAppData(String key) throws RemoteException {
				String result = null;
				if (AppDataKeys.AUDIO_TYPE.equals(key)) {
					result = AUDIO_TYPE;
				} else if (AppDataKeys.BLOCK.equals(key)) {
					result = BLOCK;
				} else if (AppDataKeys.CATEGORY.equals(key)) {
					result = CATEGORY;
				} else if (AppDataKeys.CHANNEL_NAME.equals(key)) {
					result = CHANNEL_NAME;
				} else if (AppDataKeys.CHANNEL_NUMBER.equals(key)) {
					result = CHANNEL_NUMBER;
				} else if (AppDataKeys.CLOSED_CAPTION.equals(key)) {
					result = CLOSED_CAPTION;
				} else if (AppDataKeys.CREATION_METHOD.equals(key)) {
					result = CREATION_METHOD;
				} else if (AppDataKeys.CREATION_TIME.equals(key)) {
					result = CREATION_TIME;
				} else if (AppDataKeys.DEFINITION.equals(key)) {
					result = DEFINITION;
				} else if (AppDataKeys.EPISODE_TITLE.equals(key)) {
					result = EPISODE_TITLE;
				} else if (AppDataKeys.FAILED_REASON.equals(key)) {
					result = FAILED_REASON;
				} else if (AppDataKeys.FULL_DESCRIPTION.equals(key)) {
					result = FULL_DESCRIPTION;
				} else if (AppDataKeys.GROUP_KEY.equals(key)) {
					result = GROUP_KEY;
				} else if (AppDataKeys.KEEP_COUNT.equals(key)) {
					result = KEEP_COUNT;
				} else if (AppDataKeys.LANGUAGE.equals(key)) {
					result = LANGUAGE;
				} else if (AppDataKeys.LIVE.equals(key)) {
					result = LIVE;
				} else if (AppDataKeys.LOCAL_ID.equals(key)) {
					result = LOCAL_ID;
				} else if (AppDataKeys.NI_INDEX.equals(key)) {
					result = NI_INDEX;
				} else if (AppDataKeys.PARENTAL_RATING.equals(key)) {
					result = PARENTAL_RATING;
				} else if (AppDataKeys.PPV_EID.equals(key)) {
					result = PPV_EID;
				} else if (AppDataKeys.PRODUCTION_YEAR.equals(key)) {
					result = PRODUCTION_YEAR;
				} else if (AppDataKeys.PROGRAM_END_TIME.equals(key)) {
					result = PROGRAM_END_TIME;
				} else if (AppDataKeys.PROGRAM_ID.equals(key)) {
					result = PROGRAM_ID;
				} else if (AppDataKeys.PROGRAM_START_TIME.equals(key)) {
					result = PROGRAM_START_TIME;
				} else if (AppDataKeys.PROGRAM_TYPE.equals(key)) {
					result = PROGRAM_TYPE;
				} else if (AppDataKeys.SAP.equals(key)) {
					result = SAP;
				} else if (AppDataKeys.SCHEDULED_DURATION.equals(key)) {
					result = SCHEDULED_DURATION;
				} else if (AppDataKeys.SCHEDULED_START_TIME.equals(key)) {
					result = SCHEDULED_START_TIME;
				} else if (AppDataKeys.SDV_STATUS.equals(key)) {
					result = SDV_STATUS;
				} else if (AppDataKeys.SEGMENT_TIMES.equals(key)) {
					result = SEGMENT_TIMES;
				} else if (AppDataKeys.SERIES_ID.equals(key)) {
					result = SERIES_ID;
				} else if (AppDataKeys.SOURCE_ID.equals(key)) {
					result = SOURCE_ID;
				} else if (AppDataKeys.STAR_RATING.equals(key)) {
					result = STAR_RATING;
				} else if (AppDataKeys.SUBCATEGORY.equals(key)) {
					result = SUBCATEGORY;
				} else if (AppDataKeys.SUPPRESS_CONFLICT.equals(key)) {
					result = SUPPRESS_CONFLICT;
				} else if (AppDataKeys.TITLE.equals(key)) {
					result = TITLE;
				} else if (AppDataKeys.VIEW_COUNT.equals(key)) {
					result = VIEW_COUNT;
				} else if (AppDataKeys.UNSET.equals(key)) {
					result = UNSET;
				}
				if (result != null && result.trim().equalsIgnoreCase("NULL")) {
					return null;
				}
				return result;
			}

			public String getLocation() throws RemoteException {
				return prop.getProperty(key+"Location");
			}

			public long getRecordingStartTime() throws RemoteException {
				String temp = prop.getProperty(key+"RecordingStartTime");
				return Long.parseLong(temp);
			}

			public long getDuration() throws RemoteException {
				String temp = prop.getProperty(key+"Duration");
				return Long.parseLong(temp);
			}
		};
		return rc;
	}
}

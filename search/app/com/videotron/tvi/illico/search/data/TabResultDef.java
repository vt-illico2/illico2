/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import com.videotron.tvi.illico.framework.DataCenter;

/**
 * <code>TabResultDef</code>
 * 
 * @version $Id: TabResultDef.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 28.
 * @description
 */
public class TabResultDef {
	public static final int ID_INVALID = -1;
	public static final int ID_LIVE = 0;
	public static final int ID_ON_DEMAND = 1;
	public static final int ID_RECORDINGS = 2;
	public static final int ID_PERSON = 3;
	
	private static final String[] TAB_RESULT_TOC_KEYS = {
		"Label.Tab_Live",
		"Label.Tab_On_Demand",
		"Label.Tab_Recordings",
		"Label.Tab_People"
	};
	
	public static String getTabResultText(int reqId) {
		if (reqId < 0 || reqId >= TAB_RESULT_TOC_KEYS.length) {
			return "Unknown";
		}
		return DataCenter.getInstance().getString(TAB_RESULT_TOC_KEYS[reqId]);
	}
}

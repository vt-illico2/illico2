/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import com.videotron.tvi.illico.framework.DataCenter;

/**
 * <code>SearchKeypadDef</code>
 * 
 * @version $Id: SearchKeypadDef.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 26.
 * @description
 */
public class SearchKeypadDef {
	public static final int ID_A = 0;
	public static final int ID_B = 1;
	public static final int ID_C = 2;
	public static final int ID_D = 3;
	public static final int ID_E = 4;
	public static final int ID_F = 5;
	public static final int ID_G = 6;
	public static final int ID_H = 7;
	public static final int ID_I = 8;
	public static final int ID_J = 9;
	public static final int ID_K = 10;
	public static final int ID_L = 11;
	public static final int ID_M = 12;
	public static final int ID_N = 13;
	public static final int ID_O = 14;
	public static final int ID_P = 15;
	public static final int ID_Q = 16;
	public static final int ID_R = 17;
	public static final int ID_S = 18;
	public static final int ID_T = 19;
	public static final int ID_U = 20;
	public static final int ID_V = 21;
	public static final int ID_W = 22;
	public static final int ID_X = 23;
	public static final int ID_Y = 24;
	public static final int ID_Z = 25;
	public static final int ID_0 = 26;
	public static final int ID_1 = 27;
	public static final int ID_2 = 28;
	public static final int ID_3 = 29;
	public static final int ID_4 = 30;
	public static final int ID_5 = 31;
	public static final int ID_6 = 32;
	public static final int ID_7 = 33;
	public static final int ID_8 = 34;
	public static final int ID_9 = 35;
	public static final int ID_APOSTROPHE  = 36;
	public static final int ID_DASH = 37;
	
	public static final int ID_SPACE = 38;
	public static final int ID_DELETE = 39;
	
	private static final String[] KEYPAD_DISPLAY_TEXT = {
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
		"k", "l", "m", "n", "o", "p", "q", "r", "s", "t", 
		"u", "v", "w", "x", "y", "z", "0", "1", "2", "3", 
		"4", "5", "6", "7", "8", "9", "\'", "-"
	};
	
	private static final String[] KEYPAD_VALUE = {
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", 
		"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", 
		"\'", "-", " "
	};
	
	public static String getDisplayText(int reqKeypadId) {
		String displayTxt = null;
		switch(reqKeypadId) {
			case ID_SPACE:
				displayTxt = DataCenter.getInstance().getString("Label.Space");
				break;
			case ID_DELETE:
				displayTxt = DataCenter.getInstance().getString("Label.Delete");
				break;
			default:
				if (reqKeypadId < KEYPAD_DISPLAY_TEXT.length) {
					displayTxt = KEYPAD_DISPLAY_TEXT[reqKeypadId];
				}
				break;
		}
		return displayTxt;
	}
	
	public static String getValue(int reqKeypadId) {
		if (reqKeypadId >= KEYPAD_VALUE.length) {
			return null;
		}
		return KEYPAD_VALUE[reqKeypadId];
	}
}

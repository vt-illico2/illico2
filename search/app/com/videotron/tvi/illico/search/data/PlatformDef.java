/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.log.Log;

/**
 * <code>PlatformDef</code>
 * 
 * @version 1.0
 * @author ZioN
 * @since 2015. 5. 13.
 * @description
 */
public class PlatformDef {
	public static String covertPlatform(String reqPlatform) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[PlatformDef.covertPlatform] reqPlatform: " + reqPlatform);
		}
		if (reqPlatform == null) {
			return null;
		}
		String destPlatform = null;
		if (reqPlatform.equals(SearchService.PLATFORM_PPV) || reqPlatform.equals(SearchService.PLATFORM_EPG)) {
			destPlatform = SearchService.PLATFORM_EPG;
    	} else if (reqPlatform.equals(SearchService.PLATFORM_KARAOKE) || reqPlatform.equals(SearchService.PLATFORM_VOD)) {
			destPlatform = SearchService.PLATFORM_VOD;
    	} else if (reqPlatform.equals(SearchService.PLATFORM_PVR)) {
    		destPlatform = SearchService.PLATFORM_PVR;
    	} else {
    		destPlatform = SearchService.PLATFORM_MONITOR;
    	}
		if (Log.DEBUG_ON) {
			Log.printDebug("[PlatformDef.covertPlatform] destPlatform: " + destPlatform);
		}
		return destPlatform;
	}
}

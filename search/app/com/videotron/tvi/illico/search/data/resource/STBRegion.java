/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import org.json.simple.JSONObject;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.data.JsonDef;
import com.videotron.tvi.illico.search.data.JsonUtil;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;

/**
 * <code>STBRegion</code>
 * 
 * @version $Id: STBRegion.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 13.
 * @description
 */
public class STBRegion extends ResourceList {
	// VDTRMASTER-5949
	public static final String DEFAULT_CH_GRID_ID = "99";
	private String channelGridId;
	
	public Resource parse(Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[STBRegion.parse] Called.");
		}
		
		JSONObject jsonObj = (JSONObject) obj;
		
		JSONObject body = (JSONObject)jsonObj.get(JsonDef.BODY);
		if (Log.DEBUG_ON) {
			Log.printDebug("[STBRegion.parse] body: " + body);
		}
		if (body != null) {
			JSONObject terminal = (JSONObject)body.get(JsonDef.TERMINAL);
			String tempChGridId = JsonUtil.getString(terminal, JsonDef.CHANNEL_GRID_ID);
			if (Log.DEBUG_ON) {
				Log.printDebug("[STBRegion.parse] Channel Grid ID: " + tempChGridId);
			}
			setChannelGridId(tempChGridId);
			
			terminal.clear();
			body.clear();
			jsonObj.clear();
		}
		return this;
	}

	public void setChannelGridId(String reqChGridId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[STBRegion.setChannelGridId] Called. reqChGridId: " + reqChGridId);
		}
		if (reqChGridId == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[STBRegion.setChannelGridId] set Channel grid id by Default Channel Grid Id.");
			}
			channelGridId = DEFAULT_CH_GRID_ID;
			return;
		}
		channelGridId = reqChGridId;
	}

	public String getChannelGridId() {
		return channelGridId;
	}
}

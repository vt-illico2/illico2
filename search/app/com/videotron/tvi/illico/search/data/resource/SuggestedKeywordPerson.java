/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import org.json.simple.JSONObject;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.data.JsonDef;
import com.videotron.tvi.illico.search.data.JsonUtil;
import com.videotron.tvi.illico.search.data.Resource;

/**
 * <code>SuggestedKeyword</code>
 * 
 * @version $Id: SuggestedKeywordPerson.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 26.
 * @description
 */
public class SuggestedKeywordPerson extends SuggestedKeyword {
	public Resource parse(Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SuggestedKeywordPerson.parse] Called.");
		}
		JSONObject jsonObj = (JSONObject) obj;
//		String[] names = JsonUtil.getStringArrays((JSONObject)jsonObj.get(JsonDef.BODY), JsonDef.NAMES);
		String[] names = JsonUtil.getStringArrays(jsonObj, JsonDef.BODY);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SuggestedKeywordPerson.parse] names: " + names);
		}
		setSuggestedKeywords(names);
		return this;
	}
}

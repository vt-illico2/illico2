/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

/**
 * <code>JsonDef</code>
 * 
 * @version $Id: JsonDef.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 2.
 * @description
 */
public class JsonDef {
    public static final String ID = "id";
    public static final String TICKER = "ticker";
    public static final String EXCLUDE_CHANNEL = "exclude_channel";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String CONFIG = "config";
    public static final String REPEAT = "repeat";
    public static final String DURATION = "duration";
    public static final String TYPE = "type";
    
    public static final String HEAD = "head";
    public static final String BODY = "body";
    public static final String ERROR = "error";
    
    public static final String TERMINAL = "terminal";
    public static final String MAC_ADDRESS = "macAddress";
    public static final String CHANNEL_GRID_ID = "channelGridId";
    
    public static final String JVM_NAME = "JVMName";
    public static final String CACHE_TIME_STAMP = "cacheTimeStamp";
    public static final String TOTAL_ELEMENT_COUNT = "totalElementCount";
    public static final String RETURNED_ELEMENT_COUNT = "returnedElementCount";
    public static final String NEXT_FROM = "nextFrom";
    
    public static final String SUGGESTED_TERMS = "suggestedTerms";
    
    public static final String EPG_ID = "epgId";
    public static final String SEASON_NO = "seasonNo";
    public static final String EPISODE_NO = "episodeNo";
    public static final String CODE = "code";
    public static final String DESCRIPTION = "description";
    public static final String GENRES = "genres";
    public static final String DEFINITION = "definition";
    public static final String RATING = "rating";
    public static final String TITLE = "title";
    public static final String CHANNEL_CALL_LETTERS = "channelCallLetters";
    public static final String CHANNEL_NAME = "channelName";
    public static final String CHANNEL_GRID_POSITIONS = "channelGridPositions";
    
    public static final String STARTTIME = "startTime";
    public static final String ENDTIME = "endTime";
    
    public static final String PRODUCTION_YEAR = "productionYear";
    
    public static final String VOD_ID = "vodId";
    public static final String DEFINITIONS = "definitions";
    public static final String ORIGINAL_TITLE = "originalTitle";
    public static final String CONTENT_IMAGE_NAME = "contentImageName";
    public static final String LANGUAGES = "languages";
    public static final String CHANNEL_FREE_PREVIEW_INDICATOR = "channelFreePreviewIndicator";
    public static final String FREE_CONTENT_INDICATOR = "freeContentIndicator";
    public static final String PAY_PER_VIEW_CONTENT_INDICATOR = "payPerViewContentIndicator";
    public static final String CLUB_CONTENT_INDICATOR = "clubContentIndicator";
    
    public static final String NAMES = "names";
    
    public static final String MESSAGE = "message";
    
	
//    public static final String ACCESS_TOKEN = "access_token";
//    public static final String ACTION = "action";
//    public static final String ACTION_URL = "action_url";
//    public static final String ACTORS_TEXT = "actors_text";
//    public static final String AD = "ad";
//    public static final String ADMIN = "admin";
//    public static final String ADULT_BLOCK = "adult_block";
//    public static final String ALIAS = "alias";
//    public static final String APP = "app";
//    public static final String ASSET_ID = "asset_id";
//    public static final String AUDIENCE = "audience";
//    public static final String AUDIO_TYPE = "audioType";
//    public static final String AUTHENTICATED = "authenticated";
//    public static final String AVAILABLE_CHECK_IN = "available_check_in";
//    public static final String BALANCE = "balance";
//    public static final String BIRTHDAY = "birthday";
//    public static final String BUILDING_NUMBERS = "building_numbers";
//    public static final String CAMPAIGN = "campaign";
//    public static final String CAT = "cat";
//    public static final String CATEGORIES = "categories";
//    public static final String CATEGORY = "category";
//    public static final String CATEGORY_ID = "category_id";
//    public static final String CATE_GRP_ID = "cate_grp_id";
//    public static final String CCLASS = "cclass";
//    public static final String CHANNEL = "channel";
//    public static final String CHANNELS = "channels";
//    public static final String CHECKPOINT = "checkpoint";
//    public static final String CHECK_IN_DATE = "check_in_date";
//    public static final String CHECK_OUT_DATE = "check_out_date";
//    public static final String CHILD = "child";
//    public static final String CLICK_TOKEN = "click_token";
//    public static final String CLUBS = "clubs";
//    public static final String CODE = "code";
//    public static final String COLOR_KEY = "color_key";
//    public static final String COMBINATION = "combination";
//    public static final String COMMAND = "command";
//    public static final String CONTENT_FORMAT = "content_format";
//    public static final String COUNTRY = "country";
//    public static final String COUPON = "coupon";
//    public static final String COUPON_ID = "coupon_id";
//    public static final String COUPON_LIST = "coupon_list";
//    public static final String COUPON_NAME = "coupon_name";
//    public static final String COUPON_REMAIN_VALUE = "coupon_remain_value";
//    public static final String COUPON_TYPE = "coupon_type";
//    public static final String COUPON_VALUE = "coupon_value";
//    public static final String CREATED_TIME = "created_time";
//    public static final String CREDIT = "credit";
//    public static final String CURRENCY = "currency";
//    public static final String CUSTOM = "custom";
//    public static final String DATA = "data";
//    public static final String DATE = "date";
//    public static final String DEFAULT_FAVORITE = "default_favorite";
//    public static final String DEPTH = "depth";
//    public static final String DESCRIPTION = "description";
//    public static final String DEVICE = "device";
//    public static final String DEVICES = "devices";
//    public static final String DEVICE_GROUP = "device_group";
//    public static final String DEVICE_ID = "device_id";
//    public static final String DEVICE_NAME = "device_name";
//    public static final String DEVICE_TYPE = "device_type";
//    public static final String DEVICE_UDID = "device_udid";
//    public static final String DIRECTORS_TEXT = "directors_text";
//    public static final String DISCOUNT_RATE = "discount_rate";
//    public static final String DOMAIN = "domain";
//    public static final String ENCRYPT_KEY = "encrypt_key";
//    public static final String END_DATE = "end_date";
//    public static final String ENTRY_PATH = "entrypath";
//    public static final String EPISODE = "episode";
//    public static final String EPISODE_END = "episode_end";
//    public static final String EPISODE_TITLE = "episode_title";
//    public static final String ERROR_CODE = "error_code";
//    public static final String ERROR_MSG = "error_msg";
//    public static final String EVENT_END = "event_end";
//    public static final String EVENT_START = "event_start";
//    public static final String EXPIRED = "expired";
//    public static final String EXPIRED_DATE = "expired_date";
//    public static final String EXPIRE_DATE = "expire_date";
//    public static final String FAMILY = "family";
//    public static final String FILES = "files";
//    public static final String FILTER = "filter";
//    public static final String FILTER_VALUE = "filter_value";
//    public static final String FROM = "from";
//    public static final String FS = "fs";
//    public static final String GENDER = "gender";
//    public static final String GENRES = "genres";
//    public static final String GROUP = "group";
//    public static final String GUEST_ID = "guest_id";
//    public static final String GUEST_NAME = "guest_name";
//    public static final String HASH = "hash";
//    public static final String HIDDEN = "hidden";
//    public static final String HISTORY = "history";
//    public static final String HISTROY_COUNT = "histroy_count";
//    public static final String HITS = "hits";
//    public static final String HYBRID = "hybrid";
//    public static final String ICONS = "icons";
//    public static final String ID = "id";
//    public static final String IMAGE = "image";
//    public static final String INDEX = "index";
//    public static final String IS_PACKAGE = "ispackage";
//    public static final String KEYWORD = "keyword";
//    public static final String LANG = "lang";
//    public static final String LANGUAGE = "language";
//    public static final String LGD_AMOUNT = "LGD_AMOUNT";
//    public static final String LGD_ANSIMMEMBER = "LGD_ANSIMMEMBER";
//    public static final String LGD_BILLKEY = "LGD_BILLKEY";
//    public static final String LGD_FINANCECODE = "LGD_FINANCECODE";
//    public static final String LGD_FINANCENAME = "LGD_FINANCENAME";
//    public static final String LGD_ISLGPWDUSER = "LGD_ISLGPWDUSER";
//    public static final String LGD_LIMIT = "LGD_LIMIT";
//    public static final String LGD_MID = "LGD_MID";
//    public static final String LGD_OID = "LGD_OID";
//    public static final String LGD_PAYDATE = "LGD_PAYDATE";
//    public static final String LGD_PAYTYPE = "LGD_PAYTYPE";
//    public static final String LGD_TID = "LGD_TID";
//    public static final String LICENSE_END = "license_end";
//    public static final String LICENSE_START = "license_start";
//    public static final String LICENSE_TYPE = "license_type";
//    public static final String LIKE = "like";
//    public static final String LIKES = "likes";
//    public static final String LIMIT = "limit";
//    public static final String LINK = "link";
//    public static final String LINK_CATEGORY_ID = "link_category_id";
//    public static final String LOCATION = "location";
//    public static final String LOCATOR = "locator";
//    public static final String LOGIN_ID = "login_id";
//    public static final String LOGO = "logo";
//    public static final String MAIL = "mail";
//    public static final String MAILS = "mails";
//    public static final String MEMBERS = "members";
//    public static final String MENU = "menu";
//    public static final String MESSAGE = "message";
//    public static final String METHOD = "method";
//    public static final String MODEL_NUMBER = "model_number";
//    public static final String MOVIE = "movie";
//    public static final String MY_REGIONAL = "myRegional";
//    public static final String M_FIELD = "m_field";
//    public static final String NAME = "name";
//    public static final String NORMAL = "normal";
//    public static final String NUMBER = "number";
//    public static final String OFFSET = "offset";
//    public static final String OTP = "otp";
//    public static final String OWNER = "owner";
//    public static final String PAIRED = "paired";
//    public static final String PARAMETERS = "parameters";
//    public static final String PARENTAL_RATING = "parental_rating";
//    public static final String PARENT_ID = "parent_id";
//    public static final String PARENT_ORG_ID = "parentOrgId";
//    public static final String PATH = "path";
//    public static final String PATH_ID = "path_id";
//    public static final String PAYLOAD = "payload";
//    public static final String PAYMENT = "payment";
//    public static final String PCLASS = "pclass";
//    public static final String PHONE = "phone";
//    public static final String PHONE_NUMBER = "phone_number";
//    public static final String PHOTO = "photo";
//    public static final String PID = "pid";
//    public static final String PKG = "pkg";
//    public static final String PLATFORM = "platform";
//    public static final String POINT = "point";
//    public static final String POLICY = "policy";
//    public static final String PREVIEW_PERIOD = "preview_period";
//    public static final String PRICE = "price";
//    public static final String PRIORITY = "priority";
//    public static final String PRIOR_RANKING = "prior_ranking";
//    public static final String PRODUCT = "product";
//    public static final String PRODUCTION_COUNTRY = "production_country";
//    public static final String PRODUCTION_DATE = "production_date";
//    public static final String PRODUCT_ID = "product_id";
//    public static final String PROGRAM = "program";
//    public static final String PROVIDER = "provider";
//    public static final String PURCHASE = "purchase";
//    public static final String PURCHASER_ID = "purchaser_id";
//    public static final String PURCHASES = "purchases";
//    public static final String PURCHASE_BLOCK = "purchase_block";
//    public static final String PURCHASE_END = "purchase_end";
//    public static final String PURCHASE_ENDT = "purchase_endt";
//    public static final String PURCHASE_STDT = "purchase_stdt";
//    public static final String P_RATING = "p_rating";
//    public static final String Q = "q";
//    public static final String QUERY = "query";
//    public static final String RANKING = "ranking";
//    public static final String REFUND_PRORATED_DAILY = "refund_prorated_daily";
//    public static final String REGIONAL = "regional";
//    public static final String REG_DATE = "reg_date";
//    public static final String RELEASE_DATE = "release_date";
//    public static final String RENTAL_DEVICE_PAYMENT_TYPE = "rental_device_payment_type";
//    public static final String RENTAL_DURATION = "rental_duration";
//    public static final String RENTAL_PERIOD = "rental_period";
//    public static final String RESULT_COUNT = "result_count";
//    public static final String RES_CODE = "res_code";
//    public static final String RES_MSG = "res_msg";
//    public static final String ROOM_NUMBER = "room_number";
//    public static final String SCHEDULE = "schedule";
//    public static final String SCHEMA = "schema";
//    public static final String SEASON = "season";
//    public static final String SERIES = "series";
//    public static final String SERVICE_ID = "service_id";
//    public static final String SERVICE_TYPE = "service_type";
//    public static final String SITE_URL = "siteurl";
//    public static final String SKIPPED_CHANNELS = "skipped_channels";
//    public static final String SNS = "sns";
//    public static final String SNS_ID = "sns_id";
//    public static final String SORT = "sort";
//    public static final String SOURCE = "source";
//    public static final String SO_ID = "so_id";
//    public static final String SSPT_ID = "sspt_id";
//    public static final String START_DATE = "start_date";
//    public static final String SUBSCRIBE = "subscribe";
//    public static final String SUBSCRIBER = "subscriber";
//    public static final String SUBSCRIPTION = "subscription";
//    public static final String SUGGESTION = "suggestion";
//    public static final String SYNOPSIS = "synopsis";
//    public static final String TAG = "tag";
//    public static final String TARGET = "target";
//    public static final String TEXT = "text";
//    public static final String THREAD_ID = "thread_id";
//    public static final String TIMEOUT = "timeout";
//    public static final String TIME_OFFSET = "time_offset";
//    public static final String TITLE = "title";
//    public static final String TO = "to";
//    public static final String TOKEN = "token";
//    public static final String TOOK = "took";
//    
//
//    public static final String TOTAL_AMOUNT = "total_amount";
//    public static final String TVSHOW = "tvshow";
//    public static final String UNSUBSCRIPTION_BUTTON_ON = "unsubscription_button_on";
//    public static final String UPDATED_TIME = "updated_time";
//    public static final String USABLE_CATEGORY = "usable_category";
//    public static final String USABLE_COUPON_POINT = "usable_coupon_point";
//    public static final String USABLE_START_DATE = "usable_start_date";
//    public static final String USER_ID = "user_id";
//    public static final String VALUE = "value";
//    public static final String VAT = "vat";
//    public static final String VAT_RATE = "vat_rate";
//    public static final String VERSION = "version";
//    public static final String VGROUP = "vgroup";
//    public static final String VISIBLE = "visible";
//    public static final String VOD = "vod";
//    public static final String VSERIES = "vseries";
//    public static final String VSORT = "vsort";
//    public static final String VSUPPRESS = "vsuppress";
//    public static final String WATCH_END = "watch_end";
//    public static final String WATCH_START = "watch_start";
//    public static final String WATCH_USER_ID = "watch_user_id";
//    public static final String WINDMILLACCESSTOKEN = "windmillAccessToken";
//    public static final String ZIP_CODE = "zip_code";
//    public static final String _PARAMS = "_params";
    
    

    
}

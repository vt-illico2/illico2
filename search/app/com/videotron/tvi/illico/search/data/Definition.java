package com.videotron.tvi.illico.search.data;

/**
 * This class define the category of source, status, form, language and type as constant used in search application.
 * @author Jinjoo Ok
 */
public final class Definition {
    /**
     * Constructor.
     */
    private Definition() {

    }

    /** The XML tag - actors. */
    public static final String TAG_ACTORS = "actors";
    /** The XML tag - actor. */
    public static final String TAG_ACTOR = "actor";
    /** The XML tag - directors. */
    public static final String TAG_DIRECTORS = "directors";
    /** The XML tag - director. */
    public static final String TAG_DIRECTOR = "director";
    /** The XML tag - platform. */
    public static final String TAG_PLATFORM = "platform";
    /** The XML tag - titles. */
    public static final String TAG_TITLES = "titles";
    /** The XML tag - title. */
    public static final String TAG_TITLE = "title";
    /** The XML tag - name. */
    public static final String TAG_NAME = "name";
    /** The XML tag - episode name. */
    public static final String TAG_EPISODE_NAME = "episode_name";
    /** The XML tag - description. */
    public static final String TAG_DESCRIPTION = "description";
    /** Indicates the source is EPG. */
    public static final String SRC_EPG = "EPG";
    /** Indicates the source is VOD. */
    public static final String SRC_VOD = "VOD";
    /** Indicates the source is PPV. */
    public static final String SRC_PPV = "PPV";
    /** Indicates the source is PVR. */
    public static final String SRC_PVR = "PVR";
    /** Indicates the source is SVOD. */
    // TODO SVOD
    public static final String SRC_SVOD = "SVOD";
    public static final String SRC_PC = "PC";

    /** Indicates the source is Monitor. */
    public static final String SRC_MONITOR = "MONITOR";
    /** Indicates the source is Monitor. */
    public static final String SRC_KARAOKE = "KARAOKE";
    /** Indicates the source is Menu. */
    public static final String SRC_MENU = "Menu";
    /** Indicates HTTP response status - success. */
    public static final int STS_SUCCESS = 100;
    /** Indicates HTTP response status - warning. */
    public static final int STS_WARNING = 101;
    /** Indicates HTTP response status - error. */
    public static final int STS_ERROR = 102;
    /** Indicates Definition is UHD. */
    public static final String FORM_UHD = "UHD";
    /** Indicates Definition is HD. */
    public static final String FORM_HD = "HD";
    /** Indicates Definition is 3D. */
    public static final String FORM_3D = "3D";
    /** Indicates Definition is HD1080P. */
    public static final String FORM_HD1080P = "1080p";
    /** Indicates Definition is Full-HD. */
    public static final String FORM_FHD = "FHD";
    /** Indicates Definition is STANDARD. */
    public static final String FORM_SD = "SD";
    /** Indicates Language is French. */
    public static final String LANG_FR = "fr";
    /** Indicates Language is English. */
    public static final String LANG_EN = "en";
    /** Indicates Type is Film. */
    public static final String TYPE_FILM = "FILM";
    /** Indicates Type is Series. */
    public static final String TYPE_SERIE = "SERIE";
    /** Indicates Type is Program. */
    public static final String TYPE_PROGRAM = "PROGRAM";
    /** Indicates Type is Event. */
    public static final String TYPE_EVENT = "EVENT";
    /** Indicates Type is Youth. */
    public static final String TYPE_YOUTH = "YOUTH";
    /** Indicates Type is Show. */
    public static final String TYPE_SHOW = "SHOW";
    /** Indicates Type is music. */
    public static final String TYPE_MUSIC = "MUSIC";
    /** Indicates Type is karaoke. */
    public static final String TYPE_KARAOKE = "KARAOKE";
    /** Indicates Sort by alphabet. */
    public static final String SORT_ALPHABET = "ALPHABETICAL";
    /** Indicates Sort by date. */
    public static final String SORT_DATE = "DATE";
    /** Indicates rating is 18+. */
    public static final String RATING_18 = "18+";
    /** Indicates rating is 16+. */
    public static final String RATING_16 = "16+";
    /** Indicates rating is 13+. */
    public static final String RATING_13 = "13+";
    /** Indicates rating is 8+. */
    public static final String RATING_8 = "8+";
    /** Indicates rating is G. */
    public static final String RATING_G = "G";
    
    /** VOD_Type - MOVIE */
    public static final int VOD_TYPE_MOVIE = 102;
    public static final int VOD_TYPE_SERIES = 103;
    
    /** Type - Program */
    public static final int PROGRAM_TYPE_PROGRAM = 1;
    /** Type - Film */
    public static final int PROGRAM_TYPE_FILM = 2;
    /** Type - Event */
    public static final int PROGRAM_TYPE_EVENT = 3;
    /** Type - series */
    public static final int PROGRAM_TYPE_SERIES = 4;
    
    /** Genre - Other */
    public static final int GENRE_OTHER = 0;
    /** Genre - Action/Adventure */
    public static final int GENRE_ACTION_ADVENTURE = 0;
    /** Genre - Adult */
    public static final int GENRE_ADULT = 2;
    /** Genre - Biography */
    public static final int GENRE_BIOGRAPHY = 3;
    /** Genre - Cartoons */
    public static final int GENRE_CARTOONS = 4;
    /** Genre - Children */
    public static final int GENRE_CHILDREN = 5;
    /** Genre - Comedy */
    public static final int GENRE_COMEDY = 6;
    /** Genre - Documentary */
    public static final int GENRE_DOCUMENTARY = 7;
    /** Genre - Drama */
    public static final int GENRE_DRAMA = 8;
    /** Genre - Family */
    public static final int GENRE_FAMILY = 9;
    /** Genre - Fitness */
    public static final int GENRE_FITNESS = 10;
    /** Genre - Game Shows */
    public static final int GENRE_GAME_SHOWS = 11;
    /** Genre - History */
    public static final int GENRE_HISTORY = 12;
    /** Genre - Horror */
    public static final int GENRE_HORROR = 13;
    /** Genre - Instructional */
    public static final int GENRE_INSTRUCTIONAL = 14;;
    /** Genre - Movies */
    public static final int GENRE_MOVIES = 15;
    /** Genre - Music */
    public static final int GENRE_MUSIC = 16;
    /** Genre - Suspense */
    public static final int GENRE_SUSPENSE = 17;
    /** Genre - Nature */
    public static final int GENRE_NATURE = 18;
    /** Genre - News */
    public static final int GENRE_NEWS = 19;
    /** Genre - Performance */
    public static final int GENRE_PERFORMANCE = 20;
    /** Genre - Reality */
    public static final int GENRE_REALITY = 21;
    /** Genre - Religious */
    public static final int GENRE_RELIGIOUS = 22;
    /** Genre - Romance */
    public static final int GENRE_ROMANCE = 23;
    /** Genre - Sci-Fi */
    public static final int GENRE_SCI_FI = 24;
    /** Genre - Soap Operas */
    public static final int GENRE_SOAP_OPERAS = 25;
    /** Genre - Sports Miscellanous */
    public static final int GENRE_SPORTS_MISCELLANOUS = 26;
    /** Genre - Baseball */
    public static final int GENRE_BASEBALL = 27;
    /** Genre - Basketball */
    public static final int GENRE_BASKETBALL = 28;
    /** Genre - Football */
    public static final int GENRE_FOOTBALL = 29;
    /** Genre - Golf */
    public static final int GENRE_GOLF = 30;
    /** Genre - Hockey */
    public static final int GENRE_HOCKEY = 31;
    /** Genre - Racing */
    public static final int GENRE_RACING = 32;
    /** Genre - Soccer */
    public static final int GENRE_SOCCER = 33;
    /** Genre - Talk Shows */
    public static final int GENRE_TALK_SHOWS = 34;
    /** Genre - Variety */
    public static final int GENRE_VARIETY = 35;
    /** Genre - Western */
    public static final int GENRE_WESTERN = 36;
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import java.util.StringTokenizer;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;

/**
 * <code>SearchSortDef</code>
 * 
 * @version $Id: SearchSortDef.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 3.
 * @description
 */
public class SearchSortDef {
	public static final int ID_INVALID = -1;
	public static final int ID_BROADCASTING_DATE = 0; // Linear
	public static final int ID_TITLE = 1; //Linear, On Demand, Pvr
	public static final int ID_MOST_RECENT_RELEASE_DATE = 2; //On Demand
	public static final int ID_SEASON_NUMBER = 3; //On Demand
	public static final int ID_PERSON_NAME = 4; //Person
	public static final int ID_RELEVANCY = 5; //Linear, On Deman, Person = value is null.
	public static final int ID_DATE_RECORDED = 6; //Pvr
	
	
	private static final String[] SORT_VALUES = {
		"SORT_BY_BROADCASTING_DATE",
		"SORT_BY_TITLE",
		"SORT_BY_MOST_RECENT_RELEASE_DATE",
		"SORT_BY_SEASON_NUMBER",
		"SORT_BY_PERSON_NAME",
		null,
		"SORT_BY_DATE_RECORDED"
	};
	
	public static String getSearchSortValue(int reqId) {
		return SORT_VALUES[reqId];
	}
	
	public static String getSortOrderKey(int reqTabResultId, int reqSearchFilterId) {
		String sortOrderKey = null;
		switch(reqTabResultId) {
			case TabResultDef.ID_LIVE:
				sortOrderKey = "SORT_ORDER_LIVE";
				break;
			case TabResultDef.ID_ON_DEMAND:
				if (reqSearchFilterId == SearchFilterDef.ID_SERIES_AND_SHOW) {
					sortOrderKey = "SORT_ORDER_VOD_SERIES";
				} else {
					sortOrderKey = "SORT_ORDER_VOD_NORMAL";
				}
				break;
			case TabResultDef.ID_RECORDINGS:
				sortOrderKey = "SORT_ORDER_PVR";
				break;
			case TabResultDef.ID_PERSON:
				sortOrderKey = "SORT_ORDER_PERSON";
				break;
		}
		return sortOrderKey;
	}
	
	public static String getSortDefalutKey(int reqTabResultId, int reqSearchFilterId) {
		String sortDefaultKey = null;
		switch(reqTabResultId) {
			case TabResultDef.ID_LIVE:
				sortDefaultKey = "SORT_DEFAULT_LIVE";
				break;
			case TabResultDef.ID_ON_DEMAND:
				if (reqSearchFilterId == SearchFilterDef.ID_SERIES_AND_SHOW) {
					sortDefaultKey = "SORT_DEFAULT_VOD_SERIES";
				} else {
					sortDefaultKey = "SORT_DEFAULT_VOD_NORMAL";
				}
				break;
			case TabResultDef.ID_RECORDINGS:
				sortDefaultKey = "SORT_DEFAULT_PVR";
				break;
			case TabResultDef.ID_PERSON:
				sortDefaultKey = "SORT_DEFAULT_PERSON";
				break;
		}
		return sortDefaultKey;
	}
	
	public static String getInitialSortValue(int reqTabResultId, int reqSearchFilterId) {
		String sortDefaultKey = getSortDefalutKey(reqTabResultId, reqSearchFilterId);
		String sortDefaultValue = DataCenter.getInstance().getString(sortDefaultKey);
		String sortVlaue = null;
		if (sortDefaultValue != null) {
			sortVlaue = DataCenter.getInstance().getString("SORT_OPTION_VALUE_"+sortDefaultValue);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchSortDef.getInitialSortValue] sortVlaue: " + sortVlaue);
		}
		return sortVlaue;
	}
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data.resource;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Vector;

import org.json.simple.JSONObject;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;
import com.videotron.tvi.illico.search.data.SearchSortDef;

/**
 * <code>SearchedLive</code>
 * 
 * @version $Id: SearchedPvrGroup.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 30.
 * @description
 */
public class SearchedPvrGroup extends ResourceList{
	private Vector searchedPvrs = new Vector();
	private boolean isMultipleChSrc;
	
	public Resource parse(JSONObject jsonObj) {
		return this;
	}
	
	public void add(SearchedPvr reqSearchedPvr) {
		searchedPvrs.add(reqSearchedPvr);
	}
	
	public boolean isMultipleChannelSource() {
		return isMultipleChSrc;
	}

	public void setMultipleChannelSource(boolean isMultipleChSrc) {
		this.isMultipleChSrc = isMultipleChSrc;
	}

	public int getItemCount() {
		return searchedPvrs.size();
	}
	
	public SearchedPvr getItem(int index) {
		return (SearchedPvr) searchedPvrs.get(index);
	}
	
	public long getStartTimeMillis() {
		SearchedPvr searchedPvr = getItem(0);
		return searchedPvr.getProgramStartMillis();
	}
	
	public long getRecordingStartMillis() {
		SearchedPvr searchedPvr = getItem(0);
		return searchedPvr.getRecordingStartMillis();
	}
	
	public String getTitle() {
		SearchedPvr searchedPvr = getItem(0);
		return searchedPvr.getRealTitle();
	}

	public void sort(String reqSortValue) {
		Comparator comparator = null;
		if (reqSortValue.equals(SearchSortDef.getSearchSortValue(SearchSortDef.ID_DATE_RECORDED))) {
			comparator = new SortByDate();
		} else if (reqSortValue.equals(SearchSortDef.getSearchSortValue(SearchSortDef.ID_TITLE))) {
			comparator = new SortByAlphabet();
		}
		int searchedPvrsSize = searchedPvrs.size();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchedPvrGroup.sort] searchedPvrsSize: " + searchedPvrsSize);
		}
		if (searchedPvrsSize > 1) {
			Collections.sort(searchedPvrs, comparator);
		}
	}
	
    class SortByDate implements Comparator {
        public int compare(Object arg0, Object arg1) {
        	SearchedPvr t0 = (SearchedPvr)arg0;
        	SearchedPvr t1 = (SearchedPvr)arg1;
            long recordingStartMillis0 = t0.getRecordingStartMillis();
            long recordingStartMillis1 = t1.getRecordingStartMillis();
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SortByDate.SortByDate.compare] recordingStartMillis0-recordingStartMillis1: " + recordingStartMillis0 +"-" + recordingStartMillis1);
			}
            int res = -1;
            if (recordingStartMillis1 > recordingStartMillis0) {
            	res = 1;
            }
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SortByDate.SortByDate.compare] res: " + res);
			}
            return res;
        }
    }
    class SortByAlphabet implements Comparator {
        Collator collator = Collator.getInstance(Locale.CANADA_FRENCH);
        /**
         * Compares its two arguments for order.
         * @param arg0 1st argument
         * @param arg1 2nd argument
         * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or
         *         greater than the second.
         */
        public int compare(Object arg0, Object arg1) {
        	SearchedPvr t0 = (SearchedPvr)arg0;
        	SearchedPvr t1 = (SearchedPvr)arg1;
        	String title0 = t0.getTitle();
        	String title1 = t1.getTitle();
        	int result = collator.compare(title0, title1);
        	if (Log.DEBUG_ON) {
				Log.printDebug("[SearchedPvrGroup.SortByAlphabet.compare] title0: " + title0);
				Log.printDebug("[SearchedPvrGroup.SortByAlphabet.compare] title1: " + title1);
				Log.printDebug("[SearchedPvrGroup.SortByAlphabet.compare] result: " + result);
			}
            return result;
        }
    }
}

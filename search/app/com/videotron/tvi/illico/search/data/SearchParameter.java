/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

/**
 * <code>SearchParameter</code>
 * 
 * @version $Id: SearchParameter.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 27.
 * @description
 */
public class SearchParameter {
	private final String searchKeyword;
	private final int searchFilterId;
	
	private final boolean isSearchedLetters;
	
	public SearchParameter(String searchKeyword, int searchFilterId, boolean isSearchedLetters) {
		this.searchKeyword = searchKeyword;
		this.searchFilterId = searchFilterId;
		this.isSearchedLetters = isSearchedLetters;
	}
	
	public int getSearchFilterId() {
		return searchFilterId;
	}
	public String getSearchKeyword() {
		return searchKeyword;
	}
	public boolean isSearchedLetters() {
		return isSearchedLetters;
	}
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import com.videotron.tvi.illico.framework.DataCenter;

/**
 * <code>PvrContentDef</code>
 * 
 * @version 1.0
 * @author ZioN
 * @since 2015. 4. 24.
 * @description
 */
public class PvrContentDef {
	private static final String[] CONTENT_STATE_TEXT = {
		null,
		"STATE_RECORDED",
		"STATE_RECORDING",
		"STATE_SCHEDULED"
	};
	
	private static final String[] RECORDING_REQUEST_STATE_TEXT = {
		null,
		"PENDING_NO_CONFLICT_STATE", //1
		"PENDING_WITH_CONFLICT_STATE", //2
		null,
		"IN_PROGRESS_STATE", //4
		"IN_PROGRESS_INSUFFICIENT_SPACE_STATE", //5
		"INCOMPLETE_STATE", //6
		"COMPLETED_STATE", //7
		"FAILED_STATE", //8
		null, //9
		null, //10
		null, //11
		null, //12
		null, //13
		"DELETED_STATE", //14
		"IN_PROGRESS_WITH_ERROR_STATE", //15
		"IN_PROGRESS_INCOMPLETE_STATE" //16
	};
	
	private static final String[] CREATION_METHOD_TEXT = {
		null,
		"BY_USER",
		"BY_REMOTE_REQUEST",
		null,
		"NEW_SERIES",
		"NEW_EPISODE_FROM_EPG_DATA",
		"NEW_TIMESLOT_BY_CONFLICT"
	};
	private static final String[] DEFINITION_TEXT = {
		"DEFINITION_SD",
		"DEFINITION_HD",
		"DEFINITION_4K"
	};
	
	public static String getContentStateText(int reqContentState) {
		if (reqContentState < 0 || reqContentState >= CONTENT_STATE_TEXT.length) {
			return null;
		}
		return CONTENT_STATE_TEXT[reqContentState];
	}
	
	public static String getRecordingRequestStateText(int reqRecordingRequestState) {
		if (reqRecordingRequestState < 0 || reqRecordingRequestState >= RECORDING_REQUEST_STATE_TEXT.length) {
			return null;
		}
		return RECORDING_REQUEST_STATE_TEXT[reqRecordingRequestState];
	}
	
	public static String getCategoryText(int reqCategory) {
		return DataCenter.getInstance().getString("Categories."+reqCategory);
	}
	
	public static String getProgramTypeText(int reqProgramType) {
		return DataCenter.getInstance().getString("ProgramType."+reqProgramType);
	}
	
	public static String getCreationMethodText(int reqCreationMethod) {
		if (reqCreationMethod < 0 || reqCreationMethod >= CREATION_METHOD_TEXT.length) {
			return null;
		}
		return CREATION_METHOD_TEXT[reqCreationMethod];
	}
	
	public static String getDefinitionText(int reqDefinition) {
		if (reqDefinition < 0 || reqDefinition >= DEFINITION_TEXT.length) {
			return null;
		}
		return DEFINITION_TEXT[reqDefinition];
	}
}

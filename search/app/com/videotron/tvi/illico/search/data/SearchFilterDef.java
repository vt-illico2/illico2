/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import com.videotron.tvi.illico.framework.DataCenter;

/**
 * <code>SearchFilterConstants</code>
 * 
 * @version $Id: SearchFilterDef.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 25.
 * @description
 */
public class SearchFilterDef {
	public static final int ID_INVALID = -1;
	public static final int ID_ALL = 0;
	public static final int ID_MOVIE = 1;
	public static final int ID_SERIES_AND_SHOW = 2;
	public static final int ID_YOUTH = 3;
	public static final int ID_EVENT = 4;
	public static final int ID_PERSON = 5;
	public static final int ID_ADULT = 6;
	
	private static final String[] SEARCH_FILTER_TOC_KEYS = {
		"Label.All",
		"Label.Movie",
		"Label.SeriesAndShow",
		"Label.Youth",
		"Label.Event",
		"Label.Person",
		"Label.Adult"
	};
	
	private static final String[] SEARCH_FILTER_CRITERIA_KEYS = {
		"CRITERIA_VALUE_ALL", 
		"CRITERIA_VALUE_MOVIE",
		"CRITERIA_VALUE_SERIES_AND_SHOW",
		"CRITERIA_VALUE_YOUTH", 
		"CRITERIA_VALUE_EVENT",
		"CRITERIA_VALUE_ALL", 
		"CRITERIA_VALUE_ADULT"
	};
	
	public static String getSectionFilterText(int reqId) {
		return DataCenter.getInstance().getString(SEARCH_FILTER_TOC_KEYS[reqId]);
	}
	
	public static String getSectionFilterCriteriaKey(int reqId) {
		String criteriaKey = null;
		if (SEARCH_FILTER_CRITERIA_KEYS[reqId] != null) {
			criteriaKey = DataCenter.getInstance().getString(SEARCH_FILTER_CRITERIA_KEYS[reqId]);
		}
		return criteriaKey;
	}
}

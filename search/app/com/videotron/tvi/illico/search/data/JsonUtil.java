/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.http.HttpResponse;

/**
 * <code>JsonUtil</code>
 * 
 * @version $Id: JsonUtil.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 2.
 * @description
 */
public class JsonUtil {
    private static final String TAG = "JsonUtil";
    /**
     * JSON object에서 key 값에 해당하는 String value를 얻는다.
     * @param src   JSONObject
     * @param key   String key
     * @return  String value for the key.
     */
    public static String getString(JSONObject src, String key) {
        return checkObj(src.get(key));
    }

    /**
     * JSON object에서 key 값에 해당하는 int value를 얻는다.
     * @param src   JSONObject
     * @param key   String key
     * @return  int value for the key.
     */
    public static int getInt(JSONObject src, String key){
        int intValue = 0;
        String value = checkObj(src.get(key));
        if ( value != null) {
            try {
                intValue = new Integer(value).intValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            intValue = -1;
        }
        return intValue;
    }

    /**
     * JSON object에서 key 값에 해당하는 long value를 얻는다.
     * @param src   JSONObject
     * @param key   String key
     * @return  long value for the key.
     */
    public static long getLong(JSONObject src, String key){
        long longValue = 0;
        String value = checkObj(src.get(key));
        if ( value != null) {
            try {
                longValue = new Long(value).longValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            longValue = -1;
        }
        return longValue;
    }

    /**
     * JSON object에서 key 값에 해당하는 float value를 얻는다.
     * @param src   JSONObject
     * @param key   String key
     * @return  float value for the key.
     */
    public static float getFloat(JSONObject src, String key){
        float floatValue = 0;
        String value = checkObj(src.get(key));
        if ( value != null) {
            try {
                floatValue = new Float(value).floatValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            floatValue = -1;
        }
        return floatValue;
    }

    /**
     * JSON object에서 key 값에 해당하는 boolean value를 얻는다.
     * @param src   JSONObject
     * @param key   String key
     * @return  boolean value for the key.
     */
    public static boolean getBoolean(JSONObject src, String key){
        String value = checkObj(src.get(key));
        boolean boolVal = false;
        if ( value != null) {
            boolVal = new Boolean(value).booleanValue();
        }
        return boolVal;
    }

    /**
     * Object를 String 으로 변환한다.
     * @param obj
     * @return
     */
    private static String checkObj(Object obj) {
        if (obj == null) {
            return null;
        }
        return String.valueOf(obj);
    }

    public static Object parseResponse(Class clazz, HttpResponse response) throws SearchException {
        if (response == null) {
            return null;
        }
        return getResource(clazz, response.asString());
    }

//    public static Object parseResponse(Class clazz, HttpResponse response, String memberName) throws WindmillException {
//        if (response == null) {
//            return null;
//        }
//        return getResource(clazz, response.asString(), memberName);
//    }

    public static Object getResource(Class clazz, String jsonStr) throws SearchException {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[JsonUtil.getResource] jsonStr: " + jsonStr);
		}
        Resource resource;

        if (clazz == null || jsonStr == null || jsonStr.length() <= 0) {
            return null;
        }
        JSONParser jsonParser = null;
        JSONObject jsonObj = null;
        try {
            jsonParser = new JSONParser();
            jsonObj = (JSONObject)jsonParser.parse(jsonStr);
            setHead((JSONObject) jsonObj.get(JsonDef.HEAD));
            Resource tmp = (Resource)clazz.newInstance();
            resource = tmp.parse(jsonObj);

        } catch (SearchException e) {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[ResourceList.parse] SearchException occurred.");
			}
        	e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            // TODO 차후 오류 코드 및 메시지 set 할 것.
            throw new SearchException(e);
        } finally {
            jsonObj.clear();
            jsonObj = null;
            jsonParser = null;
        }

        return resource;
    }
    
	public static void setHead(JSONObject jsonObj) throws SearchException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResourceList.setHead] Called.");
		}
		JSONObject error = (JSONObject) jsonObj.get(JsonDef.ERROR);
		if (error != null) {
			String code = JsonUtil.getString(error, JsonDef.CODE);
			String msg = JsonUtil.getString(error, JsonDef.MESSAGE);
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResourceList.setHead] Error occured code: " + code +", Message: " + msg);
			}
			throw new SearchException(code, msg);
		}
	}

    public static Object getResource(Class clazz, String jsonStr, String key) throws SearchException {
        Resource resource;

        if (clazz == null || jsonStr == null || jsonStr.length() <= 0) {
            return null;
        }
        JSONParser jsonParser = null;
        JSONObject jsonObj = null;
        try {
            jsonParser = new JSONParser();
            jsonObj = (JSONObject)jsonParser.parse(jsonStr);
            Resource tmp = (Resource)clazz.newInstance();
            resource = tmp.parse((JSONObject)jsonObj.get(key));

        } catch (Exception e) {
            e.printStackTrace();
            // TODO 차후 오류 코드 및 메시지 set 할 것.
            throw new SearchException(e);
        } finally {
            jsonObj.clear();
            jsonObj = null;
            jsonParser = null;
        }

        return resource;
    }

    public static Object getResources(Class clazz, JSONObject jsonObject, String memberName) {
        if (clazz == null || jsonObject == null) {
            return null;
        }
        Resource[] resources = null;
        JSONArray jsonArray = (JSONArray)jsonObject.get(memberName);
        ArrayList list = new ArrayList();
        if (jsonArray != null) {
            Iterator iter = jsonArray.iterator();
            while (iter.hasNext()) {
                JSONObject dataObj = (JSONObject)iter.next();
                Resource resource = null;
                try {
                    resource = (Resource)clazz.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                    return  null;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return  null;
                }
                resource.parse(dataObj);
                list.add(resource);
                dataObj.clear();
            }
            resources = ((Resource[]) list.toArray(new Resource[list.size()]));

            jsonArray.clear();
            jsonArray = null;
            list.clear();
            list = null;
        }
        return resources;
    }

    public static String[] getStringArrays(JSONObject jsonObject, String key) {
        String[] strArrays = null;
        JSONArray jsonArray = (JSONArray)jsonObject.get(key);
        if (Log.DEBUG_ON) {
			Log.printDebug("[JsonUtil.getStringArrays] jsonArray" + jsonArray);
		}
        if (jsonArray != null) {
            int size = jsonArray.size();
            strArrays = new String[size];
            for (int i = 0; i < size; i++) {
                strArrays[i] = (String)jsonArray.get(i);
                if (Log.DEBUG_ON) {
					Log.printDebug("[JsonUtil.getStringArrays] adsfasf" + strArrays[i]);
				}
            }
            jsonArray.clear();
            jsonArray = null;
        }
        return strArrays;
    }

    public static int[] getIntArrays(JSONObject jsonObject, String key) {
        int[] ret = null;
        JSONArray jsonArray = (JSONArray)jsonObject.get(key);
        if (jsonArray != null) {
            int size = jsonArray.size();
            ret = new int[size];
            for (int i = 0; i < size; i++) {
                ret[i] = ((Long)jsonArray.get(i)).intValue();
            }
            jsonArray.clear();
            jsonArray = null;
        }
        return ret;
    }
    
    public static String[] getTokenString(String src, String delim) {
    	StringTokenizer st = new StringTokenizer(src, delim);
    	int stCount = st.countTokens();
    	String[] res = new String[stCount];
    	for (int i = 0; i<stCount; i++) {
    		res[i] = st.nextToken();
    	}
    	return res;
    }
}
 
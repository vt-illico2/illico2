/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.http;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Map;

import com.videotron.tvi.illico.search.data.SearchException;

/**
 * <code>HttpResponse</code>
 * 
 * @version $Id: HttpResponse.java,v 1.4 2017/01/09 20:46:12 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 2.
 * @description
 */
public class HttpResponse implements Runnable {
    private static final String TAG = "HttpResponse";

    // read time out 설정값
    private static final int TIMEOUT = 30; // 9초
    private boolean timeout = true;
    private boolean isValid = true;
    private byte[] ret;

    private HttpURLConnection con;
    protected int statusCode;
    protected String statusMessage;
    protected String contentType;
    protected String type;
    protected String code;
    protected String message;
    protected String responseAsString = null;
    protected InputStream is;
    private boolean streamConsumed = false;
    
    
    public HttpResponse() {
    }
    
    public HttpResponse(HttpURLConnection con) throws IOException {
        this.con = con;
        this.statusCode = con.getResponseCode();
        this.statusMessage = con.getResponseMessage();
        this.contentType = con.getContentType();
        if (statusCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
    		is = new BufferedInputStream(con.getErrorStream());
    	} else {
    		is = new BufferedInputStream(con.getInputStream());
    	}
    }

    
    public int getStatusCode() {
        return statusCode;
    }
    
    public String getResponseMessage() {
        return statusMessage;
    }
    
    public String getContentType() {
        return contentType;
    }
    
    public String getResponseHeader(String name) {
        return con.getHeaderField(name);
    }

    public Map getResponseHeaderFields() {
        return con.getHeaderFields();
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getCode() {
        return code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * Returns the response stream.<br>
     * This method cannot be called after calling asString() or asDcoument()<br>
     * It is suggested to call disconnect() after consuming the stream.
     * <p/>
     * Disconnects the internal HttpURLConnection silently.
     *
     * @return response body stream
     * @throws WindmillException
     * @see #disconnect()
     */
    public InputStream asStream() {
        if (streamConsumed) {
            throw new IllegalStateException("Stream has already been consumed.");
        }
        return is;
    }

    public String asString() throws SearchException {
//        if (Log.DBG) {
//            Log.debug(TAG, "called [asString]");
//        }
        if (responseAsString == null) {
            try {
                responseAsString = new String(getData(), "UTF-8");
            } catch (Exception e) {
                if (e instanceof SocketTimeoutException) {
//                    throw new WindmillException(WindmillResourceBundle.CODE_SOCKET_ERROR,
//                        WindmillResourceBundle.MSG_W0001);
                    throw new SearchException("", "");
                } else if (e instanceof IOException) {
//                    throw new WindmillException(WindmillResourceBundle.CODE_ETC_ERROR,
//                        WindmillResourceBundle.MSG_ERROR);
                    throw new SearchException("", "");
                }
                throw new SearchException(e);
            }
        }
//        if (Log.DBG) {
//            Log.debug(TAG, "[asString], asString(), responseAsString : " + responseAsString);
//        }
        return responseAsString;
    }

    private byte[] getData() throws IOException {
        Thread t = new Thread(this, "HttpResponse|getData()");
        t.start();
        int timer = TIMEOUT;

        while((--timer) > 0 && timeout == true) {
            try {
//                if (Log.DBG) {
//                    Log.debug(TAG, "[getData], timer : " + timer);
//                }
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (timeout) {
//            if (Log.DBG) {
//                Log.debug(TAG, "[getData], Time's up !!!");
//            }
            throw new SocketTimeoutException("Time's up !!!");
        }
        if (!isValid) {
            throw new IOException("Error occurs during execution !!!");
        }

        return ret;
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
//        if (Log.DBG) {
//            Log.debug(TAG, "[run], process start !!!");
//        }
        try {
            ret = receive();
            timeout = false;
            isValid = true;
        } catch (SocketException se) {
            timeout = true;
        } catch (SocketTimeoutException ste) {
            timeout = true;
        } catch (IOException ioe) {
            isValid = false;
        }
    }

    private byte[] receive() throws IOException {
//        if (Log.DBG) {
//            Log.debug(TAG, "called [receive]");
//        }
        ByteArrayOutputStream baos = null;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        byte[] buf = null;
        byte[] response = null;

        try {
        	if (statusCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
        		bis = new BufferedInputStream(con.getErrorStream());
        	} else {
        		bis = new BufferedInputStream(con.getInputStream());
        	}
            
            baos = new ByteArrayOutputStream();
            bos = new BufferedOutputStream(baos);

            int count = bis.available();

            if (count == 0) {
//                if (Log.DBG) {
//                    Log.debug(TAG, "[receive], Data is not available !!!");
//                }
                return null;
            }

            buf = new byte[count];
            int read = 0;
            while((read = bis.read(buf)) != -1) {
                bos.write(buf, 0, read);
            }
            bos.flush();

            response = baos.toByteArray();
        } catch (SocketException se) {
            throw se;

        } catch (IOException ioe) {
            throw ioe;

        } finally {
            try {
                if (bis != null) {
                    bis.close();
                    bis = null;
                }
                if (bos != null) {
                    bos.close();
                    bos = null;
                }
                if (baos != null) {
                    baos.close();
                    baos = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            disconnectForcibly();
        }

        return response;
    }

    /**
     * Returns the response body as string.<br>
     * Disconnects the internal HttpURLConnection silently.
     *
     * @return response body
     * @throws WindmillException
     */
//    public String asString() throws WindmillException {
//        if (null == responseAsString) {
//            BufferedReader br = null;
//            InputStream stream = null;
//            try {
//                stream = asStream();
//                if (null == stream) {
//                    return null;
//                }
//                br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
//                StringBuffer buf = new StringBuffer();
//                String line;
//                while ((line = br.readLine()) != null) {
//                    buf.append(line).append("\n");
//                }
//                this.responseAsString = buf.toString();
//                stream.close();
//                streamConsumed = true;
//            } catch (IOException ioe) {
//                throw new WindmillException(ioe);
//
//            } finally {
//                if (stream != null) {
//                    try {
//                        stream.close();
//                    } catch (IOException ignore) {
//                    }
//                }
//                if (br != null) {
//                    try {
//                        br.close();
//                    } catch (IOException ignore) {
//                    }
//                }
//                disconnectForcibly();
//            }
//        }
//        return responseAsString;
//    }

    public Reader asReader() {
        try {
            return new BufferedReader(new InputStreamReader(is, "UTF-8"));
        } catch (java.io.UnsupportedEncodingException uee) {
            return new InputStreamReader(is);
        }
    }

    private void disconnectForcibly() {
        try {
            disconnect();
        } catch (Exception ignore) {
        }
    }

    /**
     * {@inheritDoc}
     */
    public void disconnect() {
        if (con != null) {
            con.disconnect();
            con = null;
        }
        
    }

    //@Override
    public String toString() {
        return "HttpResponse{" +
                "statusCode=" + statusCode +
                ", contentType=" + contentType +
                ", responseAsString='" + responseAsString + '\'' +
                ", is=" + is +
                ", streamConsumed=" + streamConsumed +
                '}';
    }
}

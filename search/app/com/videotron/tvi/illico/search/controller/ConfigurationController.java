package com.videotron.tvi.illico.search.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.SearchDataManager;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class integrated with Management System and Application Property to apply the setting values of Search
 * Application.
 * @author Jinjoo Ok
 */
public class ConfigurationController implements DataUpdateListener {

    /** This class instance. */
    private static ConfigurationController instance = new ConfigurationController();

    /** Whether VCDS activate or not to download poster. */
    private boolean activePosterDownload;
    /** Type of sort - by alphabetical. */
    public static final String SORT_ALPHABET = "2";
    /** Type of sort - by channel. */
    public static final String SORT_DATE = "3";
    /**
     * This parameter is used to sort results at one level below the platform order.
     */
    private HashMap sortParam;
    /**
     * Display order depending to Platform.*/
    private HashMap priorityDisplayOrder;
    /** The version of configuration file. */
    private int versionSearchConfig;
    /** The number of platform. (General(Menu,Monitor), EPG, VOD, PVR, PPV, PC) */
    private static final int PLATFORM_NUMBER = 6;
    /** Set of IPG category. */
    private HashMap tvCategory;
    /** Set of VOD category. */
    private HashMap vodCategory;
    /** Type of Category - IPG. */
    public static final int TV_CATEGORY = 0;
    /** Type of Category - VOD. */
    public static final int VOD_CATEGORY = 1;
    /** Indicates platform launched Search applicaiton is Monitor or Menu. */
    private static final String SRC_GENERAL = "Gereral";

    /**
     * Gets instance of ConfigurationController.
     * @return ConfigurationController
     */
    public static ConfigurationController getInstance() {
        return instance;
    }

    /**
     * Initialize.
     */
    public void init() {
        if (Log.DEBUG_ON) {
            Log.printDebug("init, addDataUpdateListener() "
                    + DataCenter.getInstance().getString(Rs.DCKEY_INBAND));
        }
        priorityDisplayOrder = new HashMap();
        sortParam = new HashMap();
        DataCenter.getInstance().addDataUpdateListener(Rs.DCKEY_INBAND, this);
     }

    /**
     * Destroy.
     */
    public void destroy() {
        sortParam.clear();
        sortParam = null;
        priorityDisplayOrder.clear();
        priorityDisplayOrder = null;
        DataCenter.getInstance().removeDataUpdateListener(Rs.DCKEY_INBAND, this);
        tvCategory.clear();
        tvCategory = null;
        vodCategory.clear();
        vodCategory = null;
    }

    /**
     * Gets Category name.
     * @param type category type
     * @param id number string id received form Search Server
     * @return category name
     */
    public String getCategoryName(int type, String id) {
        if (Log.DEBUG_ON) {
            Log.printDebug("getCategoryName id :" + id);
        }
        String[] categoryNum = TextUtil.tokenize(id, ",");
        StringBuffer sb = new StringBuffer();
        if (type == TV_CATEGORY) {
            Object[] keys = tvCategory.keySet().toArray();
            String value = null;
            String[] numbers = null;

            for (int i = 0; i < categoryNum.length; i++) {
                for (int j = 0; j < keys.length; j++) {
                    value = (String) tvCategory.get(keys[j]);
                    numbers = TextUtil.tokenize(value, "|");
                    for (int k = 0; k < numbers.length; k++) {
                        if (numbers[k].equals(categoryNum[i])) {
                            StringBuffer temp = new StringBuffer((String) keys[j]);
                            temp.delete(0, "TvCategory".length());
                            sb.append(DataCenter.getInstance().getString("Category." + temp.toString()));
                            sb.append(",");
                            break;
                        }
                    }
                }
            }
        } else {
            Object[] keys = vodCategory.keySet().toArray();
            String value = null;
            String[] numbers = null;
            StringBuffer temp = null;
            for (int i = 0; i < categoryNum.length; i++) {
                for (int j = 0; j < keys.length; j++) {
                    value = (String) vodCategory.get(keys[j]);
                    numbers = TextUtil.tokenize(value, "|");
                    for (int k = 0; k < numbers.length; k++) {
                        if (numbers[k].equals(categoryNum[i])) {
                            temp = new StringBuffer((String) keys[j]);
                            temp.delete(0, "VODCategory".length());
                            sb.append(DataCenter.getInstance().getString("Category." + temp.toString()));
                            sb.append(",");
                            break;
                        }
                    }
                }
            }
        }
        if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
            sb.deleteCharAt(sb.length() - 1);
        }
        if (sb.length() > 0) {
            return sb.toString();
        } else {
            return null;
        }
    }

     /**
     * As received events, parse Inband data.
     * @param file inband data
     */
    private void parseData(File file) {
    	if (Log.INFO_ON) {
			Log.printInfo("[ConfigurationController.parseData] Called.");
		}
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            // File
            fis = new FileInputStream(file);
            baos = new ByteArrayOutputStream();
            int len = 0;
            while ((len = fis.read()) != -1) {
                baos.write(len);
            }
            byte[] arr = baos.toByteArray();
            len = arr.length;

            int index = 0;
            int version = oneByteToInt(arr, index++);
            if (Log.INFO_ON) {
				Log.printInfo("[ConfigurationController.parseData] Version - VersionSearchConfig: " + version + " - " + versionSearchConfig);
			}
            if (version != versionSearchConfig) {
                versionSearchConfig = version;
                priorityDisplayOrder.clear();
                String[] platform = new String[]{SRC_GENERAL, "EPG", "VOD", "PVR", "PPV", "PC"};
                String[] order = new String[]{"VOD", "PPV", "EPG", "PVR", "PC", "Convergence"};
                String[] orderPlatform;
                for (int loop = 0, size; loop < PLATFORM_NUMBER; loop++) {
                    size = oneByteToInt(arr, index++);
                    orderPlatform = new String[size];
                    for (int i = 0, tmpPlatform; i < size; i++) {
                        tmpPlatform = oneByteToInt(arr, index++);
                        orderPlatform[i] =  order[tmpPlatform - 1];
                        if (Log.DEBUG_ON) {
							Log.printDebug("[ConfigurationController.parseData] parseData platform order["+i+"] = " + platform[loop] + ": " +  orderPlatform[i]);
						}
                    }
                    priorityDisplayOrder.put(platform[loop], orderPlatform);
                    sortParam.put(platform[loop], String.valueOf(oneByteToInt(arr, index++)));
                    if (Log.DEBUG_ON) {
						Log.printDebug("[ConfigurationController.parseData] parseData platform sort= " + platform[loop] + ": " + sortParam.get(platform[loop]));
					}
                }
                String download = new String(arr, index++, 1);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] download: " + download);
				}
                if (download.equals("T")) {
                    activePosterDownload = true;
                } else {
                    activePosterDownload = false;
                }
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] activePosterDownload: " + activePosterDownload);
				}
                len = oneByteToInt(arr, index++); //ip or dns length
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] ip or dns length : " +  len);
				}
                StringBuffer path = new StringBuffer();
                String name = new String(arr, index, len);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] ip or dns : " +  name);
				}
                if (name.indexOf("http") == -1) {
                    path.append("http://");
                }
                path.append(name);
                path.append(":");
                index += len;
                len = twoBytesToInt(arr, index);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] port : " +  len);
				}
                path.append(len); // port
                index += 2;
                len = oneByteToInt(arr, index++);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] context lth : " +  len);
				}
                String context = new String(arr, index, len);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] context : " +  context);
				}
                index += len;
                path.append(context); //context root
                if (context.lastIndexOf("/") != context.length() - 1) {
                    path.append("/");
                }
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] Search Server URL:  " + path.toString());
				}
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] Search Server arr : " + arr.length + " " + index);
				}
                len = oneByteToInt(arr, index++);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] proxy count : " +  len);
				}
                if (len > 0) {
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_USE, "true");
                    String[] proxyIP = new String[len];
                    int[] proxyPort = new int[len];
                    for (int i = 0, ipLen = 0; i < len; i++) {
                        ipLen = oneByteToInt(arr, index++);
                        if (Log.DEBUG_ON) {
        					Log.printDebug("[ConfigurationController.parseData] proxy ip lth["+ i +"] : " +  ipLen);
        				}
                        proxyIP[i] = new String(arr, index, ipLen);
                        if (Log.DEBUG_ON) {
         					Log.printDebug("[ConfigurationController.parseData] proxy ip["+ i +"] : " +  proxyIP[i]);
         				}
                        index += ipLen;
                        proxyPort[i] = twoBytesToInt(arr, index);
                        index += 2;
                        if (Log.DEBUG_ON) {
         					Log.printDebug("[ConfigurationController.parseData] proxy port["+ i +"] : " +  proxyPort[i]);
         				}
                    }
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_HOST, proxyIP);
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_PORT, proxyPort);
                } else {
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_USE, "false");
                    DataCenter.getInstance().remove(Rs.DCKEY_PROXY_HOST);
                    DataCenter.getInstance().remove(Rs.DCKEY_PROXY_PORT);
                    if (Log.DEBUG_ON) {
    					Log.printDebug("[ConfigurationController.parseData] Search parseData() proxy server not used!!");
    				}
                }
//                String[] proxyIP = TextUtil.tokenize(DataCenter.getInstance().getString("R1_" + Rs.DCKEY_PROXY_HOST), "|");
//                String[] proxyPort = TextUtil.tokenize(DataCenter.getInstance().getString("R1_" + Rs.DCKEY_PROXY_PORT), "|");
//                int[] port = new int[proxyPort.length];
//                for (int i = 0; i < port.length; i++) {
//                    port[i] = Integer.parseInt(proxyPort[i]);
//                }
//                DataCenter.getInstance().put(Rs.DCKEY_PROXY_HOST, proxyIP);
//                DataCenter.getInstance().put(Rs.DCKEY_PROXY_PORT, port);
                
                //Additional Info
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] Start New URL Info.");
				}
                len = oneByteToInt(arr, index++); //ip or dns length
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData]New ip_dns_length: " + len);
				}
                path = new StringBuffer();
                name = new String(arr, index, len);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData]New name: " + name);
				}
                if (name.indexOf("http") == -1) {
                    path.append("http://");
                }
                path.append(name);
                path.append(":");
                index += len;
                len = twoBytesToInt(arr, index);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData]New port: " + len);
				}
                path.append(len); // port
                index += 2;
                len = oneByteToInt(arr, index++);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData]New context root lth: " + len);
				}
                context = new String(arr, index, len);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData]New context: " + context);
				}
                index += len;
                path.append(context); //context root
                if (context.lastIndexOf("/") != context.length() - 1) {
                    path.append("/");
                }
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] New Search Server URL:  " + path.toString());
				}
                SearchDataManager.getInstance().setUrlPath(path.toString());
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData] New Search Server arr : " + arr.length + " " + index);
				}
                len = oneByteToInt(arr, index++);
                if (Log.DEBUG_ON) {
					Log.printDebug("[ConfigurationController.parseData]New proxy count: " + len);
				}
                if (len > 0) {
                    String[] proxyIP = new String[len];
                    int[] proxyPort = new int[len];
                    for (int i = 0, ipLen = 0; i < len; i++) {
                        ipLen = oneByteToInt(arr, index++);
                        if (Log.DEBUG_ON) {
        					Log.printDebug("[ConfigurationController.parseData]New proxy ip["+i+"] lth: " + ipLen);
        				}
                        proxyIP[i] = new String(arr, index, ipLen);
                        if (Log.DEBUG_ON) {
        					Log.printDebug("[ConfigurationController.parseData]New proxy ip["+i+"]: " + proxyIP[i]);
        				}
                        index += ipLen;
                        proxyPort[i] = twoBytesToInt(arr, index);
                        index += 2;
                        if (Log.DEBUG_ON) {
        					Log.printDebug("[ConfigurationController.parseData]New proxy port["+i+"]: " + proxyPort[i]);
        				}
                    }
                    SearchDataManager.getInstance().setProxyInfo(proxyIP, proxyPort);
                } else {
                	SearchDataManager.getInstance().setProxyInfo(null, null);
                    if (Log.DEBUG_ON) {
    					Log.printDebug("[ConfigurationController.parseData] New Search parseData() proxy server not used!!");
    				}
                }
            }
            if (Log.INFO_ON) {
				Log.printInfo("[ConfigurationController.parseData] ConfigurationController: end of parsing file.");
			}
        } catch (Exception ex) {
            SearchController.getInstance().showErrorPopup(Rs.ERROR_SCH503);
            Log.print(ex);
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
    }

    /**
     * Changes one byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]) & 0xFF);
    }

    /**
     * Changes two byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int twoBytesToInt(byte[] data, int offset) {
        return (((((int) data[offset]) & 0xFF) << 8) + (((int) data[offset + 1]) & 0xFF));
    }



    /**
     * Depending on platform, return display order.
     * @param platform platform launched Search.
     * @return 1st priority platform.
     */
    public String[] getDisplayOrder(String platform) {
        if (platform.equals(Definition.SRC_MONITOR) || platform.equals(Definition.SRC_MENU)) {
            platform = SRC_GENERAL;
        } else if (platform.equals(Definition.SRC_KARAOKE)) {
            platform = Definition.SRC_VOD;
        }
        String[] order = null;
        if (priorityDisplayOrder != null && priorityDisplayOrder.size() > 0) {
            order = (String[]) priorityDisplayOrder.get(platform);
        }
        if (order == null) {
            if (!Environment.EMULATOR) {
                SearchController.getInstance().showErrorPopup(Rs.ERROR_SCH504);
            }
            if (Log.WARNING_ON) {
                Log.printWarning("there is no Inband data for priority result, so use the default ");
            }
            if (platform.equals(SRC_GENERAL)) {
                order = new String[] {Definition.SRC_EPG, Definition.SRC_PPV, Definition.SRC_VOD, Definition.SRC_PVR};
            } else {
                order = new String[] {Definition.SRC_VOD, Definition.SRC_PPV, Definition.SRC_EPG, Definition.SRC_PVR};
            }
//            if (Environment.EMULATOR) {
//            	order = new String[] {Definition.SRC_PVR};
//            }
        } else {
            if (Log.DEBUG_ON) {
                for (int i = 0; i < order.length; i++) {
                	Log.printDebug("getDisplayOrder platform = " + platform);
                    Log.printDebug("getDisplayOrder order[" + i + "/" + order.length + "] " + order[i]);
                }
            }
        }
        return order;
    }

    /**
     * Gets sort order according to platform.
     * @param platform platform name
     * @return price/alphabet/date
     */
    public String getSort(String platform) {
        if (platform.equals(Definition.SRC_MONITOR) || platform.equals(Definition.SRC_MENU)) {
            platform = SRC_GENERAL;
        } else if (platform.equals(Definition.SRC_KARAOKE)) {
            platform = Definition.SRC_VOD;
        }
        if (sortParam.size() > 0) {
            String order = (String) sortParam.get(platform);
            if (Log.INFO_ON) {
                Log.printInfo("Search getSort() platform: " + platform + ", order: " + order);
            }
            if (order != null) {
                if (order.equals(SORT_ALPHABET)) {
                    return Definition.SORT_ALPHABET;
                } else {
                    return Definition.SORT_DATE;
                }
            }
        } else {
            if (Log.WARNING_ON) {
                Log.printWarning("there is no Inband data for sorting, so use the default ");
            }
            if (!Environment.EMULATOR) {
                SearchController.getInstance().showErrorPopup(Rs.ERROR_SCH504);
            }
        }
        return Definition.SORT_ALPHABET;
    }

    /**
     * Called when data is removed at DataCenter.
     * @param key data's key
     */
    public void dataRemoved(String key) {

    }

    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        if (value instanceof File) {
            if (key.equals(Rs.DCKEY_INBAND) && value != null) {
                parseData((File) value);
            }
        }
    }

    /**
     * Gets version of configuration file version.
     * @return version number
     */
    public int getVersion() {
        return versionSearchConfig;
    }
    /**
     * Returns whether poster image can be downloaded from VCDS.
     * @return true or false
     */
    public boolean isActivePosterDownload() {
        return activePosterDownload;
    }

}

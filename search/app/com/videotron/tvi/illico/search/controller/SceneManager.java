/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.controller;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.Vector;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.ui.Scene;
import com.videotron.tvi.illico.search.ui.SceneSearchKeypad;
import com.videotron.tvi.illico.search.ui.SceneSearchResult;
import com.videotron.tvi.illico.search.ui.component.Breadcrumbs;
import com.videotron.tvi.illico.search.ui.component.DateAndTime;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * <code>SceneManager</code>
 * 
 * @version $Id: SceneManager.java,v 1.4.2.1 2017/03/27 15:28:48 freelife Exp $
 * @author ZioN
 * @since 2015. 3. 25.
 * @description This class manages scenes.
 */
public class SceneManager {
    private static SceneManager instance;
    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    private LayeredUI lUISearch;
    private LayeredWindowSearch lWinSearch;
    private LayeredKeySearch lKeySearch;

    private Scene curScene;
    private Vector prevScenesVec;

    private SceneManager(){
    	init();
    }
    
    public static synchronized SceneManager getInstance() {
        if (instance == null) {
            instance = new SceneManager();
        }
        return instance;
    }
    
    public void init() {
        if (lWinSearch == null) {
            lWinSearch = new LayeredWindowSearch();
            lWinSearch.init();
            lWinSearch.setVisible(true);
        }
        if (lKeySearch == null) {
            lKeySearch = new LayeredKeySearch();
        }
        lUISearch = WindowProperty.SEARCH.createLayeredDialog(lWinSearch, lWinSearch.getBounds(), lKeySearch);
        lUISearch.deactivate();
        if (prevScenesVec == null) {
        	prevScenesVec = new Vector();
        }
    }
    
    public void dispose() {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[SceneManager.dispose] Called.");
		}
    	stop();
    	disposeSceneVector();
        curScene = null;
        if (lUISearch != null) {
            lUISearch.deactivate();
            WindowProperty.dispose(lUISearch);
            lUISearch = null;
        }
        lKeySearch = null;
        if (lWinSearch != null) {
            lWinSearch.setVisible(false);
            lWinSearch.dispose();
            lWinSearch = null;
        }
        ImagePool imgPool = FrameworkMain.getInstance().getImagePool();
        if (imgPool != null) {
            imgPool.clear();
        }
        instance = null;
    }
    
    private void disposeSceneVector() {
        if (prevScenesVec != null) {
        	int scenesVecSz = prevScenesVec.size();
        	for (int i=0; i<scenesVecSz; i++) {
        		 Scene scene = (Scene)prevScenesVec.get(i);
        		 scene.stop();
        		 scene.dispose();
        	}
        	prevScenesVec.clear();
        	prevScenesVec = null;
        }
    }
    
    public void start(Object param) {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[SceneManager.start] Called. param: " + param);
		}
    	if (lUISearch != null) {
            lUISearch.activate();
        }
        if (lUISearch != null) {
            lUISearch.activate();
        }
        if (Log.DEBUG_ON) {
			Log.printDebug("[SceneManager.start] curScene: " + curScene);
		}
        if (curScene != null) {
        	setCurrentScene(curScene, false, null);
        } else {
        	goToNextScene(SceneTemplate.SCENE_ID_KEYPAD, true, param);
        }
    }
    
    public void stop() {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[SceneManager.stop] called.");
		}
        if (curScene != null) {
            curScene.stop();
            lWinSearch.remove(curScene);
        }
        if (lUISearch != null) {
            lUISearch.deactivate();
        }

		// R7.2
		DebugScreen.getInstance().dispose();
    }
    
    public boolean isSearchActive() {
    	if (Log.DEBUG_ON) {
    		Log.printDebug("[SceneManager.isSearchActive] lUISearch " + lUISearch);
    		Log.printDebug("[SceneManager.isSearchActive] lUISearch.isActive " + lUISearch.isActive());
    	}
    	return lUISearch.isActive();
    }
    
    public void backToPreviousScene() {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[SceneManager.backToPreviousScene] Called.");
		}
    	disposeCurrentScene();
    	Scene scene = (Scene)prevScenesVec.get(prevScenesVec.size()-1);
    	setCurrentScene(scene, false, null);
    	prevScenesVec.remove(prevScenesVec.size()-1);
    	lWinSearch.repaint();
    }
    
    private void disposeCurrentScene() {
        if (curScene != null) {
            curScene.stop();
            curScene.dispose();
            lWinSearch.remove(curScene);
        }
    }
    
    public void goToNextScene(int reqSceneId, boolean isReset, Object param) {
        //Stop before scene
        if (curScene != null) {
            curScene.stop();
            lWinSearch.remove(curScene);
            prevScenesVec.addElement(curScene);
        }

        //Start current scene
        Scene scene = getScene(reqSceneId);
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.goToNextScene]scene : "+scene);
        }
        setCurrentScene(scene, isReset, param);
        lWinSearch.repaint();
    }
    
    private void setCurrentScene(Scene reqScene, boolean isReset, Object param) {
        if (reqScene != null) {
            lWinSearch.add(reqScene);
        	reqScene.start(isReset, param);
            curScene = reqScene;
        }
    }

    private Scene getScene(int reqSceneId) {
        Scene res = createScene(reqSceneId);
        return res;
    }

    private Scene createScene(int reqSceneId) {
        Scene scene = null;
        switch(reqSceneId) {
            case SceneTemplate.SCENE_ID_KEYPAD:
                scene = new SceneSearchKeypad();
                break;
            case SceneTemplate.SCENE_ID_RESULT:
                scene = new SceneSearchResult();
                break;
            case SceneTemplate.SCENE_ID_PEOPLE_RESULT:
                scene = new SceneSearchResult();
                break;        
        }
        if (scene != null) {
            scene.init();
        }
        return scene;
    }

    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowSearch extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        
        private Breadcrumbs bc;
        private DateAndTime dateAndTime;
        
        public LayeredWindowSearch() {
            setBounds(Constants.SCREEN_BOUNDS);
        }
        public void notifyShadowed() {
        }
        public void init() {
            if (bc == null) {
                bc = new Breadcrumbs();
                bc.setBounds(52, 17, 590, 72);
                add(bc);
                bc.start(new String[]{DataCenter.getInstance().getString("Label.Search")});
            }
            if (dateAndTime == null) {
                dateAndTime = new DateAndTime();
                dateAndTime.setBounds(700, 43, 210, 18);
                add(dateAndTime);
                dateAndTime.start();
            }
        }
        
        public void dispose() {
            if (dateAndTime != null) {
            	remove(dateAndTime);
            	dateAndTime.stop();
            	dateAndTime = null;
            }
            if (bc != null) {
            	remove(bc);
            	bc.stop();
            	bc.dispose();
            	bc = null;
            }
        }
        
        public void paint(Graphics g) {
        	g.setColor(Rs.C40);
        	g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        	super.paint(g);
        }
    }
    class LayeredKeySearch implements LayeredKeyHandler {
        public boolean handleKeyEvent(UserEvent userEvent) {
            if (userEvent == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredKeyWizard.handleKeyEvent]User Event is null.");
                }
                return false;
            }
            if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
                return false;
            }

			// R7.2
			DebugScreen.getInstance().checkKeyCode(userEvent.getCode());

            if (curScene == null) {
                return false;
            }
            return curScene.handleKey(userEvent.getCode());
        }
    }
}

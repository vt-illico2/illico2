/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.controller;

/**
 * <code>SceneTemplate</code>
 * 
 * @version $Id: SceneTemplate.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author ZioN
 * @since 2015. 3. 25.
 * @description
 */
public class SceneTemplate {
    public static final int SCENE_ID_INVALID = -1;
    public static final int SCENE_ID_KEYPAD = 0;
    public static final int SCENE_ID_RESULT = 1;
    public static final int SCENE_ID_PEOPLE_RESULT = 2;
}

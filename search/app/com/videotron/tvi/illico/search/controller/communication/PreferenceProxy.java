package com.videotron.tvi.illico.search.controller.communication;

import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * PreferenceProxy integrated with User Profile Application to apply the setting values of Search Application.
 * @author Jinjoo Ok
 */
public class PreferenceProxy implements PreferenceListener {
    /** This class instance. */
    private static PreferenceProxy instance = new PreferenceProxy();

    /** Whether if adult content is allowed or not. */
    // private boolean allowAdultContent;
    /** Whether if PIN certification is succeed or not. */
    // private boolean pinSuccess;
    private XletContext xletContext;
    private PreferenceService preferenceService;
    private final String[] preferenceKeys ={
    		MonitorService.SUPPORT_UHD
    };
    /**
     * Get instance.
     * @return PreferenceProxy
     */
    public static PreferenceProxy getInstance() {
        return instance;
    }

    /**
     * Initialize.
     * @param xlet XletContext
     */
    public void init(XletContext xlet) {
    	xletContext = xlet;
    	lookUpService();
    }
    
    private void lookUpService() {
		/** IXC Binding */
		try {
			new Thread("lookUpService - PreferenceService.IXC_NAME") {
				public void run() {
					String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
					while (true) {
						if (preferenceService == null) {
							try {
								preferenceService = (PreferenceService) IxcRegistry
										.lookup(xletContext, lookupName);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if (preferenceService != null) {
							if (Log.DEBUG_ON) {
								Log.printDebug("PreferenceProxy.lookUpService: preferenceService ");
							}
							try {
								String[] preferences = preferenceService
										.addPreferenceListener(
												PreferenceProxy.this,
												Rs.APP_NAME,
												preferenceKeys,
												null); // null array for default values
								setPreference(preferenceKeys, preferences);
							} catch (Exception ignore) {
								ignore.printStackTrace();
							}
							break;
						}
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	private void setPreference(String[] keys, String[] preference) {
		for (int i = 0; i < keys.length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("setPreference key[" + keys[i] + "] ,  preference : " + preference[i]);
			}
			if (preference[i] == null) {
				continue;
			}
			if (keys[i].equals(MonitorService.SUPPORT_UHD)) {
				DataCenter.getInstance().put(keys[i], preference[i]);
			}
		}
	}

    public void setPreferenceService(PreferenceService ps) {
        addPreferenceListener(ps);
    }


    /**
     * Depending on Block_by_rating of UPP, decides to show lock icon.
     * @param rating Title's rating
     * @return whether if lock icon shows or not.
     */
    public boolean isLock(String rating) {
        if (rating.equals(TextUtil.EMPTY_STRING) || rating.equals(Definition.RATING_G)) {
            return false;
        }

        if (DataCenter.getInstance().getString(RightFilter.PARENTAL_CONTROL).equals(Definitions.OPTION_VALUE_ON)) {
            String uppRating = DataCenter.getInstance().getString(RightFilter.BLOCK_BY_RATINGS);
            String[] ratingArr = new String[]{Definition.RATING_G, Definition.RATING_8, Definition.RATING_13,
                    Definition.RATING_16, Definition.RATING_18};
            if (uppRating.equals(Definition.RATING_G)) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("RightFilter.BLOCK_BY_RATINGS : G, so do not show lock icon");
                }
                return false;
            }
            int uppIndex = -1;
            int contentIndex = -1;
            for (int i = 0; i < ratingArr.length; i++) {
                if (ratingArr[i].equals(uppRating)) {
                    uppIndex = i;
                    break;
                }
            }
            for (int i = 0; i < ratingArr.length; i++) {
                if (ratingArr[i].equals(rating)) {
                    contentIndex = i;
                    break;
                }
            }
            if (contentIndex >= uppIndex) {
                return true;
            }
        }
        return false;
    }

    /**
     * Registers listener to UPP.
     * @param preferenceService PreferenceService
     */

    private void addPreferenceListener(PreferenceService preferenceService) {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, addPreferenceListener");
        }
        try {
            String[] preferenceKeys = new String[]{PreferenceNames.LANGUAGE, RightFilter.PARENTAL_CONTROL,
                    RightFilter.BLOCK_BY_RATINGS};
            String[] values = preferenceService.addPreferenceListener(this, Rs.APP_NAME, preferenceKeys, new String[]{
                    Definitions.LANGUAGE_ENGLISH, Definitions.OPTION_VALUE_ON, null});
            for (int i = 0; i < values.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("PreferenceProxy, setValues() preferenceName: " + preferenceKeys[i] + " value :"
                            + values[i]);
                }
                DataCenter.getInstance().put(preferenceKeys[i], values[i]);
            }

        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * Destroy.
     */
    public void destroy() {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.removePreferenceListener(Rs.APP_NAME, this);
            } catch (Exception e) {
                Log.print(e);
            }
        }
        DataCenter.getInstance().remove(PreferenceService.IXC_NAME);
    }

    /**
     * Receives preferences from Profile.
     * @param name preference name
     * @param value value of preference
     * @exception RemoteException RemoteException
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("receiveUpdatedPreference preferenceName: " + name + " value: " + value);
        }
        if (name.equals(PreferenceNames.LANGUAGE) || name.equals(RightFilter.PARENTAL_CONTROL)
                || name.equals(RightFilter.BLOCK_BY_RATINGS)) {
            DataCenter.getInstance().put(name, value);
        }
    }

    /**
     * Requests show pin enabler to UPP.
     * @param pinListner PinEnablerListener
     */
    public void showPinPopup(PinEnablerListener pinListner) {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.showPinEnabler(
                        pinListner,
                        TextUtil.split(DataCenter.getInstance().getString("Descriptions.PIN_DESCRIPTION"),
                                FontResource.getFontMetrics(Rs.F18), 310));
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Sets whether if PIN's certification.
     * @param success true means success
     */
    // public void setPinSuccess(boolean success) {
    // pinSuccess = success;
    // }
    //
    // /**
    // * Gets value of PIN certification.
    // * @return true or false
    // */
    // public boolean isPinSuccess() {
    // return pinSuccess;
    // }
}

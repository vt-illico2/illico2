package com.videotron.tvi.illico.search.controller;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.util.*;
import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 *
 * Created by zesyman on 2016-07-07.
 */
public class DebugScreen implements LayeredKeyHandler {

	private static DebugScreen instance;

	private ArrayList debugArray = new ArrayList();
	private LayeredUI debugUI;

	public static final SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss.S");

	int hiddenIdx = 0;
	int[] hidden = new int[] { KeyEvent.VK_0, KeyEvent.VK_0, KeyEvent.VK_0, KeyEvent.VK_0, KeyEvent.VK_0,
			KeyEvent.VK_PAGE_UP, KeyEvent.VK_PAGE_UP, KeyEvent.VK_PAGE_DOWN, };

	Font f16 = Rs.F16;
	FontMetrics fm16 = FontResource.getFontMetrics(f16);

	private DebugScreen() {
	}

	public static DebugScreen getInstance() {
		if (instance == null) {
			instance = new DebugScreen();
		}

		return instance;
	}

	public void activate() {
		debugUI = WindowProperty.DEBUG_SCREEN.createLayeredDialog(hiddenWindow, Constants.SCREEN_BOUNDS, this);
		hiddenWindow.setBounds(Constants.SCREEN_BOUNDS);
		debugUI.activate();
	}

	public void deactivate() {
		if (debugUI != null) {
			debugUI.deactivate();
			WindowProperty.dispose(debugUI);
			debugUI = null;
		}
	}

	public void dispose() {
		deactivate();
		debugArray.clear();
	}

	public void checkKeyCode(int keyCode) {
		if (hidden[hiddenIdx] == keyCode) {
			if (debugUI != null && debugUI.isActive() && keyCode == KeyEvent.VK_0) {
				hiddenWindow.repaint();
			}
			hiddenIdx++;
			if (hiddenIdx >= hidden.length) {
				if (debugUI == null) {
					activate();
				} else {
					deactivate();
				}
				hiddenIdx = 0;
			}
		} else {
			hiddenIdx = 0;
		}

		if (keyCode == KeyCodes.STOP) {
			deactivate();
		}
	}

	public void addLog(String src) {

		if (fm16.stringWidth(src) > 850) {
			String[] splitStr = TextUtil.split(SDF.format(new Date()) + ", " + src, fm16, 850);

			if (splitStr != null) {
				for (int i = splitStr.length - 1; i > -1; i--) {
					debugArray.add(splitStr[i]);
				}
			}
		} else {
			debugArray.add(src);
		}
		hiddenWindow.repaint();
	}

	LayeredWindow hiddenWindow = new LayeredWindow() {
		public void paint(Graphics g) {
			g.setColor(new DVBColor(80, 170, 80, 150));
			//g.fillRoundRect(40, 50, 880, 440, 40, 40);
			g.fillRect(0, 50, 960, 440);
			g.setColor(new DVBColor(132, 96, 255, 128));
			g.fillRect(0, 30, 960, 20);
			g.fillRect(0, 490, 960, 20);

			g.setFont(FontResource.BLENDER.getFont(16));
			FontMetrics fm = g.getFontMetrics();
			int len = debugArray.size();
			for (int i = 0; i < len && i < 25; i++) {
				String str = (String) debugArray.get(len - 1 - i);
				str = TextUtil.shorten(str, fm, 860);
				g.setColor(Color.black);
				g.drawString(str, 57, 67 + i * 17);
				g.setColor(Color.yellow);
				g.drawString(str, 55, 65 + i * 17);
			}
			while (debugArray.size() > 25) {
				debugArray.remove(0);
			}
			String title = "illico core";

			g.setColor(Color.black);
			g.drawString(title, 52, 47);
			g.setColor(Color.cyan);
			g.drawString(title, 50, 45);
			super.paint(g);
		}
	};

	public boolean handleKeyEvent(UserEvent userEvent) {

		return false;
	}
}

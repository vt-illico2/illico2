package com.videotron.tvi.illico.search.controller;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Vector;

import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;

public class VbmController implements LogCommandTriggerListener {

    protected static VbmController instance = new VbmController();
    /** The Constant MID_APP_START. */
    private static final long MID_START_APPLICATION = 1015000001L;
    /** The Constant MID_APP_EXIT. */
    private static final long MID_END_APPLICATION = 1015000002L;
    /** The Constant MID_PARENT_APP. */
    private static final long MID_PARENT_APPLICATION = 1015000003L;
    /** The Constant MID_NO_RESULT. */
    private static final long MID_SEARCH_NO_RESULT = 1015000004L;
    /** The Constant MID_ACCESS_DETAILS_PAGE_FROM_SEARCH. */
    private static final long MID_ACCESS_DETAILS_PAGE = 1015000005L;
    /** The Constant MID_BACK_FROM_SEARCH_RESULT. */
    private static final long MID_BACK_FROM_SEARCH_RESULT = 1015000006L;
    /** The Constant MID_SEARCH_QUERY_START. */
    private static final long MID_SEARCH_QUERY_START = 1015000007L;
    /** The Constant MID_SEARCH_QUERY_END. */
    private static final long MID_SEARCH_QUERY_END = 1015000008L;
    /** The Constant MID_KEYWORD_SEARCH_QUERY_START. */
    private static final long MID_KEYWORD_SEARCH_QUERY_START = 1015000009L;
    /** The Constant MID_KEYWORD_SEARCH_QUERY_END. */
    private static final long MID_KEYWORD_SEARCH_QUERY_END = 1015000010L;
    
    /** The Constant MID_START_APPLICATION_FROM_DOPTION */
    private static final long MID_START_APPLICATION_FROM_DOPTION = 1015000011L;

    /** The session id. */
    private String sessionId;

    public static VbmController getInstance() {
        return instance;
    }

    public static boolean ENABLED = false;

    protected VbmService vbmService;
    protected HashSet idSet = new HashSet();
    protected Vector vbmLogBuffer;

    private VbmController() {
    }

    public void init(VbmService service) {
    	if (Log.INFO_ON) {
			Log.printInfo("[VbmController.init] Called.");
		}
        vbmService = service;
        long[] ids = null;
        try {
            ids = vbmService.checkLogCommand(Rs.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        try {
            vbmService.addLogCommandChangeListener(Rs.APP_NAME, this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }

        if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
            	if (Log.DEBUG_ON) {
					Log.printDebug("[VbmController.init] Ids["+i+"]: " + ids[i]);
				}
                idSet.add(new Long(ids[i]));
            }
        }

        vbmLogBuffer = (Vector) SharedMemory.getInstance().get(VbmService.VBM_SM_INTF);
        checkEmpty();
        if (Log.DEBUG_ON) {
            if (vbmLogBuffer == null) {
            	Log.printDebug("[VbmController.init] found vbmLogBuffer");
            } else {
            	Log.printDebug("[VbmController.init] not found vbmLogBuffer.");
            }
		}
    }

    public void dispose() {
        try {
            vbmService.removeLogCommandChangeListener(Rs.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

    /**
     * Check empty.
     */
    protected void checkEmpty() {
        ENABLED = vbmLogBuffer != null && !idSet.isEmpty();
    }

    /**
     * Write start log.
     * @param app application name
     */
    public void writeStartApplication(String app) {
        sessionId = Long.toString(System.currentTimeMillis());
        write(MID_PARENT_APPLICATION, app);
        write(MID_START_APPLICATION, sessionId);
    }
    
    public void writeStartApplicationWithParam(String param) {
        write(MID_START_APPLICATION_FROM_DOPTION, param);
    }

    /**
     * Write end log.
     */
    public void writeEndApplication() {
    	if (sessionId != null) {
            write(MID_END_APPLICATION, sessionId);
            sessionId = null;
    	}
    }
    
    /**
     * Write search no result.
     */
    public void writeSearchNoResult() {
    	String noResultTime = Long.toString(System.currentTimeMillis());
    	write(MID_SEARCH_NO_RESULT, noResultTime);
    }

    /**
     * Write access detail page.
     */
    public void writeAccessDetailPage() {
    	write(MID_ACCESS_DETAILS_PAGE, sessionId);
    }
    
    /**
     * Write back from search result.
     */
    public void writeBackToSearchResult() {
    	write(MID_BACK_FROM_SEARCH_RESULT, sessionId);
    }
    
    /**
     * Write search query start.
     */
    //R7.4 freelife new VBM identifier
    public void writeSearchQueryStart(boolean isSearchLetter) {
    	write(MID_SEARCH_QUERY_START, sessionId + (isSearchLetter?"ALL":"KEYWORD"));
    }
    
    /**
     * Write search query end.
     */
    //R7.4 freelife new VBM identifier
    public void writeSearchQueryEnd(boolean isSearchLetter) {
    	write(MID_SEARCH_QUERY_END, sessionId + (isSearchLetter?"ALL":"KEYWORD"));
    }
    
    /**
     * Write keyword search query start.
     */
    public String writeKeywordSearchQueryStart() {
    	if (sessionId == null) {
    		return null;
    	}
    	write(MID_KEYWORD_SEARCH_QUERY_START, sessionId);
    	return sessionId;
    }
    
    /**
     * Write keyword search query end.
     */
    public void writeKeywordSearchQueryEnd(String reqSessionId) {
    	if (reqSessionId != null) {
    		write(MID_KEYWORD_SEARCH_QUERY_END, reqSessionId);    		
    	}
    }
    
    private void write(long id, String value) {
        write(id, new String[]{value});
    }

    public boolean write(long id, String[] value) {
        if (!ENABLED) {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[VbmController.write] Endable is false about " + id);
			}
            return false;
        }
        if (!idSet.contains(new Long(id))) {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[VbmController.write] " + id + " did not contains in ID set.");
			}
            return false;
        }
        String str = Long.toString(id);
        if (value != null) {
            StringBuffer buffer = new StringBuffer(100);
            buffer.append(str);
            buffer.append(VbmService.SEPARATOR);
            buffer.append(-1); //Group Id
            for (int i = 0; i < value.length; i++) {
                buffer.append(VbmService.SEPARATOR);
                buffer.append(value[i]);
            }
            str = buffer.toString();
        }
        if (Log.DEBUG_ON) {
			Log.printDebug("[VbmController.write] Write String is " + str);
		}
        vbmLogBuffer.addElement(str);
        return true;
    }

    /**
     * event invoked when logLevel data updated. it is only applied to action measurements
     * @param Measurement to notify measurement an frequencies
     * @param command - true to start logging, false to stop logging
     * @throws RemoteException - occur during the execution of a remote method call
     */
    public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
        Log.printInfo("VbmController.logCommandChanged: " + measurementId + ", " + command);
        if (command) {
            idSet.add(new Long(measurementId));
        } else {
            idSet.remove(new Long(measurementId));
        }
        checkEmpty();
    }

    /**
     * event invoked when log of trigger measurement should be collected
     * @param measurementId trigger measurement
     * @throws RemoteException - occur during the execution of a remote method call
     */
    public void triggerLogGathering(long measurementId) {
        Log.printInfo("VbmController.triggerLogGathering: " + measurementId);
    }

}

package com.videotron.tvi.illico.search.controller;

import java.awt.Point;
import java.rmi.RemoteException;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.search.SearchActionEventListener;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.search.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.PlatformDef;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.ui.SceneSearchKeypadMemento;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;

/**
 * SearchController class responsible for updating Search Application data
 * according to the user input and the screen.
 * 
 * @author Jinjoo Ok
 */
public final class SearchController implements ScreenSaverConfirmationListener, PinEnablerListener {
	/** This class instance. */
	private static SearchController instance = new SearchController();
	/** SearchActionListener. */
	private SearchActionListener actionListener = new SearchActionListener();
	/** Application authority. It's related with DNCS package. */
	private Hashtable appRight;

	/**
	 * launchRealPlatform EPG, VOD, PPV, PVR, SVOD, PC, MONITOR, KARAOKE, Menu
	 * */
	private String launchRealPlatform = "";
	/** Platform name that launched Search Application. */
	private String launchConvertedPlatform = "";
	private boolean init;
	/** Previous platform name that launched Search Application. */
	private String lastLaunchPlatform;
	/**
	 * When the Search app started by back key, the platform launched Search
	 * already is launched and Search is launched above the platform. In this
	 * case, if the platform and destination platform is same, the parameters
	 * are passed by SearchActionLister not Monitor.
	 */
	private String backLaunchPlatform;
	/** Current status of showing of adult content. */
	private static boolean showAdultContent;
	/**
	 * The certification of PIN is asked only the first time after the Search
	 * application has launched. variable for that.
	 */
	private static boolean initialShowAdultContent;
	/**
	 * When the Search application is launched, this value is increased.
	 */
	private int launchingCount;
	/**
	 * If a request is success, this value is increased.
	 */
	private int requestingCount;
	/**
	 * To compare launching and requesting, each parameter launchingCount and
	 * requestingCount are saved in Hashtable. Request to server is completed
	 * when launchingCount and requestingCount is same. Maximum of the two value
	 * is 10.
	 */
	private Hashtable pairLoadingData;
	/**
	 * Whether if state is advanced search or not.
	 */
	private boolean showFilters;

	private boolean isActivatedLoadingAnimation;

	// public static long startTime;
	// public static long endTime;
	// public static long parseTime;
	/** Constructor. */
	private SearchController() {
		appRight = new Hashtable();
		pairLoadingData = new Hashtable();
	}

	/**
	 * Gets SearchController instance.
	 * 
	 * @return SearchController
	 */
	public static synchronized SearchController getInstance() {
		return instance;
	}

	/**
	 * Resource and data destroy.
	 */
	public void destroy() {
		removeScreenSaverListener();
		PreferenceProxy.getInstance().destroy();
		ConfigurationController.getInstance().destroy();
		FrameworkMain.getInstance().getImagePool().clear();
	}

	public void showLoadingAnimation(boolean isKeyMoving) {
		if (Log.DEBUG_ON) {
			Log.printDebug("showLoadingAnimation");
		}
		try {
			LoadingAnimationService loading = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
			if (loading != null) {
				isActivatedLoadingAnimation = true;
				loading.showLoadingAnimation(new Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2));
			} else {
				isActivatedLoadingAnimation = false;
				//R7.4 VDTRMASTER-6140
				//showErrorPopup(Rs.ERROR_SCH507);
			}
		} catch (Exception e) {
			isActivatedLoadingAnimation = false;
			//R7.4 VDTRMASTER-6140
			//showErrorPopup(Rs.ERROR_SCH507);
			Log.print(e);
		}
	}

	/**
	 * Hide loading animation.
	 */
	public void hideLoadingAnimation() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.hideLoadingAnimation] Called.");
		}
		try {
			LoadingAnimationService loading = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
			if (loading != null) {
				isActivatedLoadingAnimation = false;
				loading.hideLoadingAnimation();
			} else {
				//R7.4 VDTRMASTER-6140
				//showErrorPopup(Rs.ERROR_SCH507);
			}
		} catch (Exception e) {
			//R7.4 VDTRMASTER-6140
			//showErrorPopup(Rs.ERROR_SCH507);
			Log.print(e);
		}
	}

	public boolean isActivatedLoadingAnimation() {
		return isActivatedLoadingAnimation;
	}

	/**
	 * Get platform launched the Search.
	 * 
	 * @return platform name
	 */
	public String getLaunchPlatform() {
		return launchConvertedPlatform;
	}

	public boolean getInitValue() {
		return init;
	}

	/**
	 * Get previous platform launched the Search.
	 * 
	 * @return platform name
	 */
	public String getLastLaunchPlatform() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.getLastLaunchPlatform] lastLaunchPlatform: " + lastLaunchPlatform);
		}
		return lastLaunchPlatform;
	}
	
	public String getLaunchRealPlatform() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.getLaunchRealPlatform] launchRealPlatform: " + launchRealPlatform);
		}
		return launchRealPlatform;
	}
	
	public synchronized void startSearch(String platform, boolean init, String param) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.startSearch] platform: " + platform + ", launchPlatform: " + launchConvertedPlatform + ", init: " + init);
		}
		// Check search authority.
		if (!platform.equals(Definition.SRC_MONITOR)) {
			boolean check = checkSearchAuthority();
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchController.startSearch] checkSearchAuthority: " + check);
			}
			if (!check) {
				showErrorPopup(Rs.ERROR_MOT501);
				return;
			}
		}
		String convertedPlatform = PlatformDef.covertPlatform(platform);
		lastLaunchPlatform = convertedPlatform;
		writeStartApplicationLog(platform, param);
		
		// Check cleare memento.
		boolean isClearMemento = isClearMemento(convertedPlatform, init);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.startSearch] isClearMemento: " + isClearMemento);
		}
		if (isClearMemento) {
			DataCenter.getInstance().remove(Rs.DCKEY_SEARCH_MEMENTO);
		}
		boolean isClearData = init;
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.startSearch] isClearData: " + isClearData);
		}
		//2015.12.10 부팅 후 오류로 인해 아래의 launchRealPlatform 코드을 이곳으로 옮김.
		launchRealPlatform = platform;
		if (isClearData) {
			SceneManager.getInstance().dispose();
			clearSearchClient(true);
		}
		boolean isInputResetState = launchConvertedPlatform.equals(convertedPlatform) && init;
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.startSearch] isInputResetState: " + isInputResetState);
		}
		//2015.12.10 부팅 후 오류로 인해 아래의 launchRealPlatform 코드을 이곳으로 옮김. - 주석
//		launchRealPlatform = platform;
		if (init) {
			launchConvertedPlatform = convertedPlatform;
			backLaunchPlatform = null;
		} else {
			backLaunchPlatform = convertedPlatform;
		}

		if (isInputResetState) {
			SceneSearchKeypadMemento memento = (SceneSearchKeypadMemento) DataCenter.getInstance().get(Rs.DCKEY_SEARCH_MEMENTO);
			SceneManager.getInstance().start(memento);
		} else {
			SceneManager.getInstance().start(null);
		}
		CommunicationManager.getInstance().setScreenSaverListener();
		this.init = init;
	}

	private void writeStartApplicationLog(String platform, String param) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.writeStartApplicationLog] platform: " + platform);
		}
        if (platform.equals(Definition.SRC_MONITOR)) {
            MonitorService m = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            String parent = "Remote";
            if (m != null) {
                try {
                  String[] params =  m.getParameter(Rs.APP_NAME);
                  if (Log.DEBUG_ON) {
                      Log.printDebug("Search start getParameter() param[0] " + params[0]);
                  }
                  if (params[0] != null && !params[0].equalsIgnoreCase(MonitorService.REQUEST_APPLICATION_HOT_KEY)) {
                      parent = params[0];
                  }
                  if (Log.DEBUG_ON) {
                      Log.printDebug("Search start getParameter() parent " + parent);
                  }
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            VbmController.getInstance().writeStartApplication(parent);
        } else {
        	VbmController.getInstance().writeStartApplication(platform);
        	if (param != null)
        		VbmController.getInstance().writeStartApplicationWithParam(param);
        }
	}

	private boolean isClearMemento(String reqPlatform, boolean isInit) {
		boolean isClearMemento = false;
		if (isInit) {
			isClearMemento = !launchConvertedPlatform.equals(reqPlatform);
			if (!isClearMemento) {
				SceneSearchKeypadMemento memento = (SceneSearchKeypadMemento) DataCenter.getInstance().get(Rs.DCKEY_SEARCH_MEMENTO);
				isClearMemento = (memento != null && memento.getFilterId() == SearchFilterDef.ID_ADULT);
			}
		}
		return isClearMemento;
	}

	/**
	 * Check application authority.
	 */
	private void checkAppAuthority() {
		MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
		String[] appName = new String[] { SearchService.PLATFORM_EPG, SearchService.PLATFORM_VOD };
		if (monitor != null) {
			try {
				int check = -1;
				for (int i = 0; i < appName.length; i++) {
					check = monitor.checkAppAuthorization(appName[i]);
					if (check != MonitorService.NOT_AUTHORIZED) {
						appRight.put(appName[i], "T");
					} else {
						appRight.put(appName[i], "F");
					}
				}
				if (Environment.SUPPORT_DVR) {
					check = monitor.checkAppAuthorization(SearchService.PLATFORM_PVR);
					if (check != MonitorService.NOT_AUTHORIZED) {
						appRight.put(SearchService.PLATFORM_PVR, "T");
					} else {
						appRight.put(SearchService.PLATFORM_PVR, "F");
					}
				} else { // NON_PVR
					boolean multiroom = monitor.isMultiroomEnabled();
					if (Log.DEBUG_ON) {
						Log.printDebug("isMultiroomEnabled=" + multiroom);
					}
					if (multiroom) {
						appRight.put(SearchService.PLATFORM_PVR, "T");
					} else {
						appRight.put(SearchService.PLATFORM_PVR, "F");
					}
				}
			} catch (Exception e) {
				Log.print(e);
			}
		}
	}

	/**
	 * Check package of the Search.
	 * 
	 * @return false means that Search application has not the authority. The
	 *         application will not be launched.
	 */
	private boolean checkSearchAuthority() {
		MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
		if (monitor != null) {
			try {
				int check = monitor.checkAppAuthorization(Rs.APP_NAME);
				if (check == MonitorService.NOT_AUTHORIZED) {
					return false;
				}
			} catch (Exception e) {
				Log.print(e);
			}
		}
		return true;
	}

	/**
	 * Returns whether if the Application can run or not.
	 * 
	 * @param name
	 *            application name.
	 * @return true means the Application can run.
	 */
	public boolean hasAppAuthority(String name) {
		if (name.equals(Definition.SRC_PPV)) {
			name = Definition.SRC_EPG;
		}
		String check = (String) appRight.get(name);
		if (Log.DEBUG_ON) {
			Log.printDebug("hasAppAuthority appName " + name + " check : " + check);
		}
		if (Environment.EMULATOR || check != null && check.equals("T")) {
			return true;
		}
		return false;
	}
	
	public void resetSearchClient() {
		SceneManager.getInstance().dispose();
		SceneManager.getInstance().start(null);
	}

	/**
	 * Stop the Search.
	 */
	public void stopSearchClient() {
		pauseSearchClient(true);
	}

	public void pauseSearchClient(boolean isDisposed) {
		// if (!isSearchActive) {
		// return;
		// }
		clearSearchClient(isDisposed);
		VbmController.getInstance().writeEndApplication();
	}

	private void clearSearchClient(boolean isDisposed) {
		boolean isSearchActive = SceneManager.getInstance().isSearchActive();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchController.clearSearchClient] called - isSearchActive: " + isSearchActive);
		}
		CommunicationManager.getInstance().clearScreenSaverListener();
		if (isDisposed) {
			DataCenter.getInstance().remove(Rs.DCKEY_IS_AUTHORIZED);
		}
//		lastLaunchPlatform = launchRealPlatform;
		SceneManager.getInstance().stop();
		hideLoadingAnimation();
		passAction(launchRealPlatform, new String[] { SearchActionEventListener.SEARCH_CLOSED });
		removeScreenSaverListener();
	}

	/**
	 * Remove ScreenSaverConfirmListener.
	 */
	private void removeScreenSaverListener() {
		MonitorService ms = ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME));
		if (ms != null) {
			try {
				ms.removeScreenSaverConfirmListener(this, Rs.APP_NAME);
			} catch (RemoteException e) {
				Log.print(e);
			}
		}
	}

	/**
	 * Response Monitor's confirm.
	 * 
	 * @return if it returns true, ScreenSaver will be started.
	 * @throws RemoteException
	 *             remote exception
	 * @see com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener#confirmScreenSaver()
	 */
	public boolean confirmScreenSaver() throws RemoteException {
		return true;
	}

	/**
	 * Pass Action to platform like EPG,VOD,PPV,PVR.
	 * 
	 * @param platform
	 *            platform name
	 * @param action
	 *            Action
	 */
	public void passAction(String platform, String[] action) {
		if (Log.DEBUG_ON) {
			Log.printDebug("passAction to " + platform + " through listener");
		}
		if (action == null) {
			return;
		}
		try {
			if (Log.DEBUG_ON) {
				Log.printDebug("passAction param[0] " + action[0]);
				if (action.length > 1) {
					Log.printDebug("passAction param[1] " + action[1]);
				}
			}
			if (platform.equals(Definition.SRC_PPV)) {
				platform = Definition.SRC_EPG;
			} else if (platform.equals(Definition.SRC_KARAOKE)) {
				platform = Definition.SRC_VOD;
			}
			actionListener.actionRequested(platform, action);
		} catch (Exception e) {
			Log.print(e);
		}
	}

	/**
	 * The class inherits SearchActionEventListener. Describe function of
	 * actionRequested()
	 * 
	 * @author Jinjoo Ok
	 */
	class SearchActionListener implements SearchActionEventListener {

		/**
		 * Passes Action to the platform.
		 * 
		 * @param platform
		 *            platform name
		 * @param action
		 *            parameters of Action
		 * @throws RemoteException
		 *             remote exception
		 */
		public void actionRequested(String platform, String[] action) throws RemoteException {
			SearchActionEventListener i = CommunicationManager.getInstance().getListeners(platform);
			if (i != null) {
				i.actionRequested(platform, action);
			} else {
				if (Log.INFO_ON) {
					Log.printInfo("SearchActionEventListener name: " + platform + " is null");
				}
			}
		}
	}

	/**
	 * Receives PinEnalber's result.
	 * 
	 * @param response
	 *            response code
	 * @param str
	 *            describe for fail
	 * @exception RemoteException
	 *                any remote exception
	 */
	public void receivePinEnablerResult(int response, String str) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("receivePinEnablerResult result is " + response);
		}
		if (response == PreferenceService.RESPONSE_SUCCESS) {
			showAdultContent = true;
			initialShowAdultContent = true;
		}
	}

	/**
	 * Gets platform that launched the Search by Back key.
	 * 
	 * @return platform name
	 */
	public String getBackLaunchPlatform() {
		return backLaunchPlatform;
	}

	/**
	 * Shows error Pop-up through ErrorMessageService.
	 * 
	 * @param code
	 *            error code.
	 */
	public void showErrorPopup(String code) {
		ErrorMessageService errorService = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
		if (errorService != null) {
			try {
				errorService.showErrorMessage(code, null);
			} catch (Exception e) {
				Log.print(e);
			}
		} else {
			if (Log.ERROR_ON) {
				Log.printError("Search can't get ErrorMessageService");
			}
		}
	}
}

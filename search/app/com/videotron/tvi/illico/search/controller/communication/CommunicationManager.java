package com.videotron.tvi.illico.search.controller.communication;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.search.SearchActionEventListener;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.controller.ConfigurationController;
import com.videotron.tvi.illico.search.controller.SceneManager;
import com.videotron.tvi.illico.search.controller.SearchController;
import com.videotron.tvi.illico.search.controller.VbmController;
import com.videotron.tvi.illico.search.data.PvrDataManager;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.IxcLookupWorker;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * CommunicationManager looks up communication interface provided by other applications and handles calling. Also, it
 * implements interface provided by Search Application
 * @author Jinjoo Ok
 */
public final class CommunicationManager implements SearchService, DataUpdateListener {
    /** class instance. */
    private static CommunicationManager instance = new CommunicationManager();
    /** XletContext. */
    private XletContext xletCtx;
    /** Listener for Receiving inband data. */
    private InbandListener inbandListener = new InbandListener();
    /** Listener for log level change. */
    private LogLevelAdapter logListener = new LogLevelAdapter();
    /** Listener for ScreenSaverConfirmation. */
    private ScreenSaverConfirmationListenerImpl sscListener = new ScreenSaverConfirmationListenerImpl();
    
    /** Set of SearchActionListener. */
    private Hashtable listeners;

    private IxcLookupWorker ixcWorker;
    /** Constructor. */
    private CommunicationManager() {

    }

    /**
     * Get Communication instance.
     * @return CommunicationManager
     */
    public static CommunicationManager getInstance() {
        return instance;
    }

    /**
     * lookup Monitor and Keyboard. bind SearchService to IXC.
     * @param xCtx XletContext
     */
    public void init(XletContext xCtx) {
        this.xletCtx = xCtx;
        ixcWorker = new IxcLookupWorker(xCtx);
        listeners = new Hashtable();
        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME, this);
        if (!Environment.EMULATOR) {
            ixcWorker.lookup("/1/20a0/", PvrService.IXC_NAME, this);
        }
        ixcWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME, this);
        ixcWorker.lookup("/1/2026/", EpgService.IXC_NAME, this);
        ixcWorker.lookup("/1/2085/", VbmService.IXC_NAME, this);
        ixcWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME, this);
        if (!Environment.EMULATOR) {
            ixcWorker.lookup("/1/2100/", StcService.IXC_NAME, this);
        }
        ixcWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, this);
        ixcWorker.lookup("/1/1020/", VODService.IXC_NAME, this);

        bindToIxc();
    }

    /**
     * Start.
     * @param xCtx XletContext
     */
    public void start(XletContext xCtx) {
        this.xletCtx = xCtx;
    }

    /**
     * Called when application destroy.
     */
    public void destroy() {
        unbind();
        DataCenter dataCenter = DataCenter.getInstance();
        dataCenter.remove(VbmService.IXC_NAME);
        dataCenter.remove(EpgService.IXC_NAME);
        dataCenter.remove(PvrService.IXC_NAME);
        dataCenter.remove(MonitorService.IXC_NAME);
        dataCenter.remove(LoadingAnimationService.IXC_NAME);
        dataCenter.remove(ErrorMessageService.IXC_NAME);
        dataCenter.remove(PreferenceService.IXC_NAME);
        listeners.clear();
        listeners = null;
    }
    
    public void setScreenSaverListener() {
    	MonitorService monitorSvc = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
    	if (monitorSvc != null) {
            try {
            	monitorSvc.removeScreenSaverConfirmListener(sscListener, Rs.APP_NAME);
            	monitorSvc.addScreenSaverConfirmListener(sscListener, Rs.APP_NAME);
            } catch (Exception e) {
                Log.print(e);
            }
    	}
    }
    
    public void clearScreenSaverListener() {
    	MonitorService monitorSvc = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
    	if (monitorSvc != null) {
            try {
            	monitorSvc.removeScreenSaverConfirmListener(sscListener, Rs.APP_NAME);
            } catch (Exception e) {
                Log.print(e);
            }
    	}
    }

    /**
     * Bind SearchService to IXC.
     */
    private void bindToIxc() {
        try {
            if (Log.INFO_ON) {
                Log.printInfo("SearchService: bind to IxcRegistry");
            }
            IxcRegistry.bind(xletCtx, SearchService.IXC_NAME, this);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * Unbind SearchService to IXC.
     */
    private void unbind() {
        try {
            IxcRegistry.unbind(xletCtx, SearchService.IXC_NAME);
        } catch (NotBoundException e) {
            Log.print(e);
        }
    }

    /**
     * Called when data is removed at DataCenter.
     * @param key data's key
     */
    public void dataRemoved(String key) {
    }

    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String name, Object old, Object remote) {
        if (name.equals(MonitorService.IXC_NAME)) {
            try {
                ((MonitorService) remote).addInbandDataListener(inbandListener, Rs.APP_NAME);
            } catch (Exception e) {
                Log.print(e);
            }
        } else if (name.equals(PvrService.IXC_NAME)) {
            try {
            	PvrService pvrService = (PvrService) remote;
            	pvrService.removeContentsChangedListener(PvrDataManager.getInstance());
            	pvrService.addContentsChangedListener(PvrDataManager.getInstance());
            } catch (Exception e) {
                Log.print(e);
            }
        } else if (name.equals(StcService.IXC_NAME)) {
            try {
                int currentLevel = ((StcService) remote).registerApp(Rs.APP_NAME);
                if (Log.DEBUG_ON) {
                    Log.printDebug("stc log level : " + currentLevel);
                }
                Log.setStcLevel(currentLevel);
                Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                if (o != null) {
                    DataCenter.getInstance().put("LogCache", o);
                }
                try {
                    ((StcService) remote).addLogLevelChangeListener(Rs.APP_NAME, logListener);
                } catch (RemoteException ex) {
                    Log.print(ex);
                }

            } catch (Exception e) {
                Log.print(e);
            }
        } else if (name.equals(VbmService.IXC_NAME)) {
            VbmController.getInstance().init((VbmService) remote);
        } else if (name.equals(PreferenceService.IXC_NAME)) {
            PreferenceProxy.getInstance().setPreferenceService((PreferenceService) remote);
        }
    }
    
    /**
     * Start search application.
     * @param platformType platform name
     * @param init whether search initialize or not
     * @param param for measurement via d-option(R7.3 Sangyong)
     */
    
    public void launchSearchApplicationWithParam(final String platformType, final boolean init, final String param) {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[CommunicationManager.launchSearchApplicationWithParam] Called. : " + 
					platformType + ", init: " + init + ", param: " + param);
		}
    	
    	startSearchApplication(platformType, init, param);
    }
    
    /**
     * Start search application.
     * @param platformType platform name
     * @param init whether search initialize or not
     */
    public void launchSearchApplication(final String platformType, final boolean init) {
    	if (Log.DEBUG_ON) {
			Log.printDebug("[CommunicationManager.startSearchApplication] Called. : " + 
					platformType + ", init: " + init);
		}
    	startSearchApplication(platformType, init, null);
    }

    /**
     * R7.3 Sangyong
     * for measurement via d-option
     */
    private void startSearchApplication(final String platformType, final boolean init, final String param) {
        if (Log.DEBUG_ON) {
			Log.printDebug("[CommunicationManager.startSearchApplication] Called. : " + platformType + ", init: " + init);
		}
        if (SceneManager.getInstance().isSearchActive()) {
        	if (Log.DEBUG_ON) {
				Log.printDebug("[CommunicationManager.launchSearchApplication] Search Application still alive. so return.");
			}
            return;
        }
        VODService vodService = (VODService)DataCenter.getInstance().get(VODService.IXC_NAME);
        if (vodService != null) {
	        try {
				if (vodService.getVODState() == VODService.APP_WATCHING) {
					if (Log.DEBUG_ON) {
						Log.printDebug("launchSearchApplication, vod state is WATCHING, so request to show cannotIcon.");
					}
					vodService.showCannotIcon();
					return;
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
        }
        
        new Thread() {
            public void run() {
//                String changedPlatformType = platformType;
//                if (Log.DEBUG_ON) {
//					Log.printDebug("[CommunicationManager.launchSearchApplication(...).new Thread() {...}.run] platformType: " + platformType);
//				}
//                if (platformType != null) {
//                	if (changedPlatformType.equals(SearchService.PLATFORM_PPV) || changedPlatformType.equals(SearchService.PLATFORM_EPG)) {
//						if (Log.DEBUG_ON) {
//							Log.printDebug("[CommunicationManager.launchSearchApplication(...).new Thread() {...}.run] Changed platform type from PPV to EPG");
//						}
//        	        	changedPlatformType = SearchService.PLATFORM_EPG;
//                	} else if (changedPlatformType.equals(SearchService.PLATFORM_KARAOKE) || changedPlatformType.equals(SearchService.PLATFORM_VOD)) {
//						if (Log.DEBUG_ON) {
//							Log.printDebug("[CommunicationManager.launchSearchApplication(...).new Thread() {...}.run] Changed platform type from KARAOKE to VOD");
//						}
//                		changedPlatformType = SearchService.PLATFORM_VOD;
//                	} else if (changedPlatformType.equals(SearchService.PLATFORM_PVR)) {
//                		changedPlatformType = SearchService.PLATFORM_PVR;
//                	} else {
//                		changedPlatformType = SearchService.PLATFORM_MONITOR;
//                	}
//                } 
//                SearchController.getInstance().startSearch(changedPlatformType, init);
                SearchController.getInstance().startSearch(platformType, init, param);
            }
        }.start();
        
        if (init == false) {
        	try {
        		Thread.sleep(1000);
        	} catch (Exception e) {}
        }
    }

    /**
     * Listener for receiving Inband data.
     */
    class InbandListener implements InbandDataListener {
        /**
         * called when Monitor tuned Inband channel.
         * @param locator string Locator
         */
        public void receiveInbandData(String locator) {

            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            int version = ConfigurationController.getInstance().getVersion();
            int monitorFileVersion = -1;
            try {
                monitorFileVersion = monitor.getInbandDataVersion(MonitorService.search_config);
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                    Log.printWarning(e);
                }
            }
            if (monitorFileVersion != version) {
                DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
            }
            try {
                if (monitor != null) {
                    monitor.completeReceivingData(Rs.APP_NAME);
                }
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                    Log.printWarning(e);
                }
            }
        }
    }

    /**
     * Log level change listener.
     * @author Jinjoo Ok
     */
    class LogLevelAdapter implements LogLevelChangeListener {
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    /**
     * Adds Listener to SearchService.
     * @param platform platform
     * @param i SearchActionEventListener
     * @throws RemoteException remote exception
     */
    public void addSearchActionEventListener(String platform, SearchActionEventListener i) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("addSearchActionEventListener platform: " + platform);
        }
        listeners.put(platform, i);
    }

    /**
     * Remove SearchActionEventListener.
     * @param platform platform
     * @throws RemoteException remote exception
     */
    public void removeSearchAcitonEventListener(String platform) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("CommunicationManager.removeSearchAcitonEventListener " + platform);
        }
        listeners.remove(platform);
    }

    /**
     * Get SearchActionEventListener.
     * @param platform platform name
     * @return SearchActionEventListener
     */
    public SearchActionEventListener getListeners(String platform) {
        if (Log.INFO_ON) {
            Log.printInfo("CommunicationManager.getListeners " + platform);
        }
        return (SearchActionEventListener) listeners.get(platform);
    }

    /**
     * Stop Search application.
     */
    public void stopSearchApplication() {
        if (Log.INFO_ON) {
            Log.printInfo("CommunicationManager.stopSearchApplication");
        }
        SearchController.getInstance().stopSearchClient();
    }
    
    class ScreenSaverConfirmationListenerImpl implements ScreenSaverConfirmationListener{
        public boolean confirmScreenSaver() throws RemoteException {
            return true;
        }
    }

}

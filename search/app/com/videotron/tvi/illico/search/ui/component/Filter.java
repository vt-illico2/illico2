/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.gui.component.RendererFilter;
import com.videotron.tvi.illico.search.ui.component.event.FilterListener;
import com.videotron.tvi.illico.util.Constants;

/**
 * <code>SearchFilter</code>
 * 
 * @version $Id: Filter.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 25.
 * @description SearchFilter implemented.
 */
public class Filter extends SearchComponent {
	private static final long serialVersionUID = 1L;
	
	public static final int FOCUS_MARGIN_EN = 18;
	public static final int FOCUS_MARGIN_FR = 13;
	
	public static final int ITEM_BASE_X_EN = 150;
	public static final int ITEM_BASE_X_FR = 160;
	
	public static final int ITEM_ADULT_BASE_X_EN = 800;
	public static final int ITEM_ADULT_BASE_X_FR = 819;
	
	public static final int GENERAL_FILTER_VALID_W = 600;
	
	public static final int[] FILTER_IDS = {
		SearchFilterDef.ID_ALL,
		SearchFilterDef.ID_MOVIE,
		SearchFilterDef.ID_SERIES_AND_SHOW,
		SearchFilterDef.ID_YOUTH,
		SearchFilterDef.ID_EVENT,
		SearchFilterDef.ID_PERSON,
		SearchFilterDef.ID_ADULT
	};
	
	public boolean isLangEnglish;
	
	public String[] filterStr;
	public int generalFilterTotalW;
	public int[] filterStrW;
	
	public int selectedIdx;
	
	public int curIdx;
	
	public PinEnablerListenerImpl pinEnablerListenerImpl;
	
	public FilterListener searchFilterListener;
	public int margin;
	
	protected void initComponent() {
		setRenderer(new RendererFilter());
		setResource();
		setFilterText();
	}
	
	protected void disposeComponent() {
		filterStr = null;
		filterStrW = null;
		compWatcher = null;
		searchFilterListener = null;
		pinEnablerListenerImpl = null;
	}
	
	public void reset() {
		curIdx = 0;
		selectedIdx = 0;
		setFilterText();
	}
	
    protected void focused() {
    	curIdx = selectedIdx;
    }
	
	private void setResource() {
		pinEnablerListenerImpl = new PinEnablerListenerImpl();
		String curLang = FrameworkMain.getInstance().getCurrentLanguage();
		isLangEnglish = (curLang != null && curLang.equals(Constants.LANGUAGE_ENGLISH));
		if (isLangEnglish) {
			margin = FOCUS_MARGIN_EN;
		} else {
			margin = FOCUS_MARGIN_FR;
		}
	}
	
	private void setFilterText() {
		generalFilterTotalW = 0;
		filterStr = new String[FILTER_IDS.length];
		filterStrW = new int[FILTER_IDS.length];
		for (int i=0; i<FILTER_IDS.length; i++) {
			filterStr[i] = SearchFilterDef.getSectionFilterText(FILTER_IDS[i]);
			filterStrW[i] = Rs.FM19.stringWidth(filterStr[i]);
			if (FILTER_IDS[i] != SearchFilterDef.ID_ADULT) {
				generalFilterTotalW += filterStrW[i] + (margin  * 2);
			}
		}
	}

	public void keyActionLeft() {
		curIdx = (FILTER_IDS.length + curIdx - 1) %FILTER_IDS.length;
		repaint();
	}
	
	public void keyActionRight() {
		curIdx = (curIdx + 1) % FILTER_IDS.length;
		repaint();
	}
	
	public void keyActionUp() {
		if (compWatcher != null) {
			compWatcher.goOverUp(this);
		}
	}
	
	public void keyActionDown() {
		if (compWatcher != null) {
			compWatcher.goOverDown(this);
		}
	}
	
	public void keyActionOk() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchFilter.keyActionOk] Current Index: " + curIdx + ", FILTER_IDS: " + FILTER_IDS[curIdx]);
		}
		
		clickEffectAction();
		if (FILTER_IDS[curIdx] == SearchFilterDef.ID_ADULT && !DataCenter.getInstance().getBoolean(Rs.DCKEY_IS_AUTHORIZED)) {
			PreferenceProxy.getInstance().showPinPopup(pinEnablerListenerImpl);
			return;
		}
		selectedIdx = curIdx;
		repaint();
		if (searchFilterListener != null) {
			searchFilterListener.changedSelectedItem(curIdx);
		}
		if (compWatcher != null) {
			compWatcher.goOverDown(this);
		}
	}
	
	public void setSelectedFilterId(int selectedIdx) {
		this.selectedIdx = selectedIdx;
	}
	
	public int getSearchFilterId() {
		return curIdx;
	}
	
	public void setSearchFilterListener(FilterListener reqSearchFilterListener) {
		searchFilterListener = reqSearchFilterListener;
	}
	
	public void removeSearchFilterListener() {
		searchFilterListener = null;
	}
	
	private void clickEffectAction() {
		int filterStrX = (GENERAL_FILTER_VALID_W / 2) - (generalFilterTotalW / 2);
		if (isLangEnglish) {
			filterStrX += ITEM_BASE_X_EN;
		} else {
			filterStrX += ITEM_BASE_X_FR;
		}
		int filterStrAreaW = filterStrW[curIdx] + (margin * 2);
		// adult filter area is different from other filters.
		if (curIdx == FILTER_IDS.length - 1) {
			if (isLangEnglish) {
				filterStrX = ITEM_ADULT_BASE_X_EN;
			} else {
				filterStrX = ITEM_ADULT_BASE_X_FR;
			}
		} else {
			for (int i = 0; i < curIdx; i++) {
				filterStrX += filterStrW[i] + (margin * 2);
			}
		}
		new ClickingEffect(this, 5).start(filterStrX, 0, filterStrAreaW, getHeight());
	}
	
	class PinEnablerListenerImpl implements PinEnablerListener{
		public void receivePinEnablerResult(int response, String detail) throws RemoteException {
	        if (Log.INFO_ON) {
				Log.printInfo("[SearchFilter.PinEnablerListenerImpl.receivePinEnablerResult] response: " + response);
			}
	        if (response == PreferenceService.RESPONSE_SUCCESS) {
	        	DataCenter.getInstance().put(Rs.DCKEY_IS_AUTHORIZED, "true");
				selectedIdx = curIdx;
				repaint();
	    		if (searchFilterListener != null) {
	    			searchFilterListener.changedSelectedItem(curIdx);
	    		}
	    		if (compWatcher != null) {
	    			compWatcher.goOverDown(Filter.this);
	    		}
	        }
		}
	}
}
 
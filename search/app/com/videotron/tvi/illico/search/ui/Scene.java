package com.videotron.tvi.illico.search.ui;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.controller.SearchController;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.PlatformDef;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;

abstract public class Scene extends UIComponent {
    private static final long serialVersionUID = 1L;
    
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 93, Constants.SCREEN_WIDTH, Constants.SCREEN_WIDTH - 93);
    
    protected static final int STATE_INITED = 0;
    protected static final int STATE_DISPOSED = 1;
    protected static final int STATE_STARTED = 2;
    protected static final int STATE_STOPED = 3;
    private final String[] STATE_NAMES = {"SCENE_INITED", "SCENE_DISPOSED", "SCENE_STARTED", "SCENE_STOPED"}; 
    private int curSceneState = STATE_DISPOSED;
    
    /*****************************************************************
     * methods - Life Cycle
     *****************************************************************/
    public void init() {
    	curSceneState = STATE_INITED;
        initScene();
    }
    public void dispose() {
    	curSceneState = STATE_DISPOSED;
        stopScene();
        disposeScene();
    }
    public void start(boolean reset, Object param) {
    	curSceneState = STATE_STARTED;
        startScene(reset, param);
		if (Log.EXTRA_ON) {
			repaint();
		}
    }
    public void stop() {
    	curSceneState = STATE_STOPED;
        stopScene();
    }
    
    protected int getCurrentSceneState() {
    	return curSceneState;
    }
    
    protected String getCurrentSceneStateName(int reqSceneState) {
    	return STATE_NAMES[reqSceneState];
    }
    /*****************************************************************
     * implement KeyListener
     *****************************************************************/
    public boolean handleKey(int code) {
    	boolean isUsed = false;
        switch(code) {
	        case KeyCodes.WIDGET:
	        case KeyCodes.SETTINGS:
				SearchController.getInstance().stopSearchClient();
				return false;
	        case OCRcEvent.VK_INFO:
	        case KeyCodes.RECORD:
	        case OCRcEvent.VK_CHANNEL_UP:
	        case OCRcEvent.VK_CHANNEL_DOWN:
	        case KeyCodes.PAUSE:
	        case KeyCodes.REWIND:
	        case KeyCodes.FAST_FWD:
	        case KeyCodes.FORWARD:
	        case KeyCodes.PLAY:
	        case KeyCodes.STOP:
	        case KeyCodes.FAV:
	        case KeyCodes.BACK:
	        case KeyCodes.COLOR_D:
	        case KeyCodes.LITTLE_BOY:
	        case KeyCodes.STAR:
	            return true;
			case KeyCodes.LIST:
				SearchController.getInstance().stopSearchClient();
				if (SearchController.getInstance().getLastLaunchPlatform().equals(Definition.SRC_PVR)) {
					isUsed = true;
				} else {
					isUsed = false;
				}
				return isUsed;
			case OCRcEvent.VK_GUIDE:
				SearchController.getInstance().stopSearchClient();
				if (PlatformDef.covertPlatform(SearchController.getInstance().getLastLaunchPlatform()).equals(Definition.SRC_EPG)) {
					isUsed = true;
				} else {
					isUsed = false;
				}
				return isUsed;
			case KeyCodes.MENU:
				SearchController.getInstance().stopSearchClient();
				/* Menu do not convert last launch platform.. */
				if (Log.DEBUG_ON) {
					Log.printDebug("[Scene.handleKey] SearchController.getInstance().getLaunchRealPlatform(): " + SearchController.getInstance().getLaunchRealPlatform());
				}
				if (SearchController.getInstance().getLaunchRealPlatform().equals(Definition.SRC_MENU)) {
					isUsed = true;
				} else {
					isUsed = false;
				}
				return isUsed;
			case KeyCodes.VOD:
				SearchController.getInstance().stopSearchClient();
				if (PlatformDef.covertPlatform(SearchController.getInstance().getLastLaunchPlatform()).equals(Definition.SRC_VOD)) {
					isUsed = true;
				} else {
					isUsed = false;
				}
				return isUsed;
	        case KeyCodes.PIP:
	        	SearchController.getInstance().stopSearchClient();
	            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
	            boolean full = false;
	            if (monitor != null) {
	                try {
	                    int state = monitor.getState();
	                    if (state != MonitorService.TV_VIEWING_STATE) {
	                        full = true;
	                    }
	                } catch (Exception e) {
	                    Log.print(e);
	                }
	            }
				if (!PlatformDef.covertPlatform(SearchController.getInstance().getLastLaunchPlatform()).equals(Definition.SRC_MONITOR) || !full) {
					isUsed = false;
				} else {
					isUsed = true;
				}
				return isUsed;
	        case KeyCodes.SEARCH:
            case Rs.KEY_PAGE_UP:
            case Rs.KEY_PAGE_DOWN:
            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case KeyEvent.VK_ENTER:
            case Rs.KEY_EXIT:
            case KeyCodes.COLOR_A:
            case KeyCodes.COLOR_B:
            case KeyCodes.COLOR_C:
            case KeyCodes.LAST:
//                boolean isActivePopup = PopupController.getInstance().isActivePopup();
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[Scene.handleKey]isActivePopup : "+isActivePopup);
//                }
//                if (isActivePopup) {
//                    Popup activePop = PopupController.getInstance().getLastPopup();
//                    activePop.keyAction(code);
//                    return true;
//                }
//                if (code == Rs.KEY_EXIT) {
//                    CommunicationManager.getInstance().requestExitToChannel(CommunicationManager.EXIT_TYPE_EXIT_KEY);
//                } else {
//                    keyAction(code);
//                }
            	keyAction(code);
                return true;
        }
        return false;
    }
    /*****************************************************************
     * methods - abstract
     *****************************************************************/
    abstract protected void initScene();

    abstract protected void disposeScene();

    abstract protected void startScene(boolean resetScene, Object param);

    abstract protected void stopScene();

    abstract protected boolean keyAction(int keyCode);
}

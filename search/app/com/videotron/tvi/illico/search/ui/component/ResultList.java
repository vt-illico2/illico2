/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import java.awt.Rectangle;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.SearchParameter;
import com.videotron.tvi.illico.search.data.TabResultDef;
import com.videotron.tvi.illico.search.gui.component.RendererResultListLive;
import com.videotron.tvi.illico.search.gui.component.RendererResultListPerson;
import com.videotron.tvi.illico.search.gui.component.RendererResultListPvr;
import com.videotron.tvi.illico.search.gui.component.RendererResultListVod;
import com.videotron.tvi.illico.search.ui.component.event.ResultListAdpater;
import com.videotron.tvi.illico.search.ui.component.event.SelectorListener;

/**
 * <code>ResultList</code>
 * 
 * @version $Id: ResultList.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 31.
 * @description
 */
public class ResultList extends SearchComponent {
	private static final long serialVersionUID = 1L;
	
    /** Valid width for EPG title. */
    public static final int EPG_TITLE_VALID_WIDTH = 282;
    
    /** Valid width for PVR title. */
    public static final int PVR_TITLE_VALID_WIDTH = 316;
    
    /** Valid width for VOD title. */
    public static final int VOD_TITLE_VALID_WIDTH = 440;
    
    /** Valid width for Person title. */
    public static final int PERSON_TITLE_VALID_WIDTH = 750;
	
	public static final int BASE_X = 54;
	public static final int BASE_Y = 158;
	
	public static final int ROW = 6;
	public static final int ROW_GAP = 50;

	public int scroll;
	public Rectangle itemBounds = new Rectangle();
	public static final int SCROLL_DELAY = 175;
	
	public int startPosX;
	public int startPosY;
	public int validWth;
	
	public SearchParameter searchParameter;
	
	private final int tabResultId;
	public final int totalCount;
	
	private SelectorListener listener;
	public ResultListAdpater adapter;
	
	public int curPageStartIdx = -1, curPageCount = -1;
	public int curIdx = -1;
	
	private TVTimerWentOffListenerImpl scrollTimerImpl;
	private TVTimerSpec scrollTimerSpec;
	
	public ResultList(int reqTabResultId, int reqTotalCount) {
		tabResultId = reqTabResultId;
		totalCount = reqTotalCount;
	}

	protected void initComponent() {
		Renderer renderer = null;
		switch(tabResultId) {
			case TabResultDef.ID_LIVE:
				renderer = new RendererResultListLive();
				validWth = EPG_TITLE_VALID_WIDTH;
				startPosX = 70 - BASE_X;
				startPosY = 190 - BASE_Y;
				break;
			case TabResultDef.ID_ON_DEMAND:
				renderer = new RendererResultListVod();
				validWth = VOD_TITLE_VALID_WIDTH;
				startPosX = 70 - BASE_X;
				startPosY = 190 - BASE_Y;
				break;
			case TabResultDef.ID_RECORDINGS:
				renderer = new RendererResultListPvr();
				validWth = PVR_TITLE_VALID_WIDTH;
				startPosX = 70 - BASE_X;
				startPosY = 190 - BASE_Y;
				break;
			case TabResultDef.ID_PERSON:
				renderer = new RendererResultListPerson();
				validWth = PERSON_TITLE_VALID_WIDTH;
				startPosX = 136 - BASE_X;
				startPosY = 197 - BASE_Y;
				break;
		}
		setRenderer(renderer);
		initResource();
	}
	
	protected void disposeComponent() {
		disposeResource();
	}
	
	public void start() {
		super.start();
		
		checkScroll(curIdx);
	}

	public void stop() {
		stopScrollTimer();
	}

	private void initResource() {
		resetItemIndex(0);
		
		//Init scroll bound value.
		itemBounds.x = 54 - BASE_X;
		itemBounds.width = 576;
		itemBounds.height = 52;
	}
	
	private void disposeResource() {
		itemBounds = null;
		searchParameter = null;
		
		stopScrollTimer();
		scrollTimerSpec = null;
		scrollTimerImpl = null;
		listener = null;
		adapter = null;
	}
	
	private void setItemBound() {
		int focusLineCount = curIdx - curPageStartIdx;
		itemBounds.y = 166 - BASE_Y + (focusLineCount * ResultList.ROW_GAP);
	}
	
	private void createScrollTimer() {
		if (scrollTimerSpec == null) {
			scrollTimerImpl = new TVTimerWentOffListenerImpl();
			scrollTimerSpec  = new TVTimerSpec();
			scrollTimerSpec.setDelayTime(SCROLL_DELAY);
			scrollTimerSpec.setRepeat(true);
			scrollTimerSpec.addTVTimerWentOffListener(scrollTimerImpl);
		}
	}
	
	private void disposeScrollTimer() {
		if (scrollTimerSpec != null) {
			scrollTimerSpec.removeTVTimerWentOffListener(scrollTimerImpl);
			scrollTimerImpl = null;
			scrollTimerSpec = null;
		}
	}

	private void startScrollTimer() {
		scroll = 0;
		createScrollTimer();
		if (scrollTimerSpec != null) {
	        try {
	            TVTimer.getTimer().scheduleTimerSpec(scrollTimerSpec);
	        } catch (TVTimerScheduleFailedException e) {
	            Log.print(e);
	        }
		}
	}
	
	private void stopScrollTimer() {
		if (scrollTimerSpec != null) {
			TVTimer.getTimer().deschedule(scrollTimerSpec);
		}
		disposeScrollTimer();
	}
	
	public void setSearchParameter(SearchParameter searchParameter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.setSearchParameter] Called. searchParameter: " + searchParameter);
		}
		this.searchParameter = searchParameter;
	}
	
	public void setSelectorListener(SelectorListener reqListener) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.setResultListListener] ResultListListener: " + reqListener);
		}
		listener = reqListener;
	}
	
	public void removeResultListListener() {
		listener = null;
	}
	
	public void setResultListAdapter(ResultListAdpater reqAdapter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.setResultListAdapter] ResultListAdpater: " + reqAdapter);
		}
		adapter = reqAdapter;
	}
	
	public void removeResultListAdapter() {
		adapter = null;
	}
	
	public int getCurrentIndex() {
		return curIdx;
	}
	
	public int getCurrentPageStartIndex() {
		return curPageStartIdx;
	}
	
	public int getCurrentPageCount() {
		return curPageCount;
	}
	
	public void keyActionLeft() {
		//Nothing to do.
	}

	public void keyActionRight() {
		//Nothing to do.
	}
					
	public void keyActionUp() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.keyActionUp] Called.");
		}
		int tempIdx = curIdx - 1;
		if (tempIdx < 0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResultList.keyActionUp] Index is over zero index.");
			}
			return;
		}
		resetItemIndex(tempIdx);
		doCheckPreviouseDataSet(curPageStartIdx - 1);
	}

	public void keyActionDown() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.keyActionDown] Called.");
		}
		int tempIdx = curIdx + 1;
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.keyActionDown] tempIdx: " + tempIdx);
		}
		if (tempIdx >= totalCount) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResultList.keyActionDown] Index is over total count.");
			}
			return;
		}
		resetItemIndex(tempIdx);
		doCheckNextDataSet(curPageStartIdx + curPageCount);
	}
	
	public void keyActionPageUp() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.keyActionPageUp] Called.");
		}
		if (curIdx <= 0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResultList.keyActionPageUp] Current Index is less than 0. return");
			}
			return;
		}
		int reqIdx = -1;
		for (int i = 0; i<ROW; i++) {
			reqIdx = curIdx - 1;
			if (reqIdx < 0) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[ResultList.keyActionPageUp] breaked["+i+"]");
				}
				break;
			}
			ItemInformation information = getCurrentItemInformation(reqIdx);
			int tempPageStartIdx = information.getPageStartIdx();
			int tempPageCount = information.getPageCount();
			if (curPageStartIdx != tempPageStartIdx || curPageCount != tempPageCount) {
				curPageStartIdx = tempPageStartIdx;
				curPageCount = tempPageCount;
			}
			if (reqIdx != curIdx) {
				curIdx = reqIdx;
			}
		}
		setItemBound();
		checkScroll(curIdx);
		doEventOfIndexChanged(curIdx);
		doCheckPreviouseDataSet(curPageStartIdx - 1);
		
//		int tempPageCount = ROW;
//		if (adapter.getTotalCount() < ROW) {
//			tempPageCount = adapter.getTotalCount();
//		}
//		int tempPageStartIdx = curPageStartIdx - ROW;
//		if (tempPageStartIdx < 0) {
//			tempPageStartIdx = 0;
//		}
//		int tempIdx = curIdx - ROW;
//		if (tempIdx < 0) {
//			tempIdx = 0;
//		}
//		if (curPageStartIdx != tempPageStartIdx || curPageCount != tempPageCount) {
//			curPageStartIdx = tempPageStartIdx;
//			curPageCount = tempPageCount;
//		}
//		if (tempIdx != curIdx) {
//			curIdx = tempIdx;
//			setItemBound();
//			checkScroll(curIdx);
//			doEventOfIndexChanged(curIdx);
//		}
//		doCheckPreviouseDataSet(curPageStartIdx - 1);
	}

	public void keyActionPageDown() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.keyActionPageDown] Called.");
		}
		if (curIdx >= totalCount - 1) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResultList.keyActionPageDown] Current Index is over totalCount. return");
			}
			return;
		}
		
		int reqIdx = -1;
		for (int i = 0; i<ROW; i++) {
			reqIdx = curIdx+1;
			if (reqIdx >= totalCount) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[ResultList.keyActionPageDown] breaked["+i+"]");
				}
				break;
			}
			ItemInformation information = getCurrentItemInformation(reqIdx);
			int tempPageStartIdx = information.getPageStartIdx();
			int tempPageCount = information.getPageCount();
			if (curPageStartIdx != tempPageStartIdx || curPageCount != tempPageCount) {
				curPageStartIdx = tempPageStartIdx;
				curPageCount = tempPageCount;
			}
			if (reqIdx != curIdx) {
				curIdx = reqIdx;
			}
		}
		setItemBound();
		checkScroll(curIdx);
		doEventOfIndexChanged(curIdx);
		doCheckNextDataSet(curPageStartIdx + curPageCount);
		
//		int totalCount = adapter.getTotalCount();
//		int tempPageCount = ROW;
//		if (adapter.getTotalCount() < ROW) {
//			tempPageCount = adapter.getTotalCount();
//		}
//		int tempPageStartIdx = curPageStartIdx + ROW;
//		if (tempPageStartIdx + tempPageCount > totalCount) {
//			tempPageStartIdx = totalCount - tempPageCount;
//		}
//		int tempIdx = curIdx + ROW;
//		if (tempIdx >= totalCount) {
//			tempIdx = totalCount - 1;
//		}
//		if (curPageStartIdx != tempPageStartIdx || curPageCount != tempPageCount) {
//			curPageStartIdx = tempPageStartIdx;
//			curPageCount = tempPageCount;
//		}
//		if (tempIdx != curIdx) {
//			curIdx = tempIdx;
//			setItemBound();
//			checkScroll(curIdx);
//			doEventOfIndexChanged(curIdx);
//		}
//		doCheckNextDataSet(curPageStartIdx + curPageCount);
	}

	public void keyActionOk() {
		clickEffectAction();
		if (listener != null) {
			listener.itemSelected(this, curIdx);
		}
	}
	
	private void clickEffectAction() {
		new ClickingEffect(this, 5).start(itemBounds.x, itemBounds.y, itemBounds.width, itemBounds.height);
	}
	
	private void doCheckNextDataSet(int from) {
		if (adapter != null) {
			adapter.checkNextDataSet(from);
		}
	}
	
	private void doCheckPreviouseDataSet(int from) {
		if (adapter != null) {
			adapter.checkPreviouseDataSet(from);
		}
	}
	
	protected void focused() {
		setItemBound();
		checkScroll(curIdx);
		doEventOfIndexChanged(curIdx);
	}
	
	protected void unfocused() {
		stopScrollTimer();
	}

	public void resetItemIndex(int reqIdx) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.resetItemIndex] called. request index: " + reqIdx);
		}
		ItemInformation information = getCurrentItemInformation(reqIdx);
		int tempPageStartIdx = information.getPageStartIdx();
		int tempPageCount = information.getPageCount();
		if (curPageStartIdx != tempPageStartIdx || curPageCount != tempPageCount) {
			curPageStartIdx = tempPageStartIdx;
			curPageCount = tempPageCount;
		}
		if (reqIdx != curIdx) {
			curIdx = reqIdx;
			setItemBound();
			checkScroll(curIdx);
			doEventOfIndexChanged(curIdx);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.resetItemIndex] curPageStartIdx: " + curPageStartIdx +", curPageCount: " + curPageCount +", curIdx: " + curIdx);
		}
	}
	
	private ItemInformation getCurrentItemInformation(int reqIdx) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.getCurrentItemInformation] Called. reqIdx: " + reqIdx);
		}
		int pageStartIdx = 0;
		int pageCount = 0;
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.getCurrentItemInformation] totalCount: " + totalCount);
		}
		if (totalCount <= ROW) {
			pageStartIdx = 0;
			pageCount = totalCount;
		} else {
			pageCount = ROW;
			if (curPageStartIdx == -1 || curPageCount ==-1) {
				if (reqIdx >= totalCount - ROW) {
					pageStartIdx = totalCount - ROW;
					if (Log.DEBUG_ON) {
						Log.printDebug("[ResultList.getCurrentItemInformation] a pageStartIdx: " + pageStartIdx);
					}
				} else if (reqIdx < ROW) {
					pageStartIdx = 0;
					if (Log.DEBUG_ON) {
						Log.printDebug("[ResultList.getCurrentItemInformation] b pageStartIdx: " + pageStartIdx);
					}
				} else {
					pageStartIdx = reqIdx;
					if (Log.DEBUG_ON) {
						Log.printDebug("[ResultList.getCurrentItemInformation] c pageStartIdx: " + pageStartIdx);
					}
				}
	    	} else {
	            if (curPageStartIdx == totalCount - ROW && reqIdx >= curPageStartIdx + 3) {
	            	pageStartIdx = curPageStartIdx;
					if (Log.DEBUG_ON) {
						Log.printDebug("[ResultList.getCurrentItemInformation] d pageStartIdx: " + pageStartIdx);
					}
	            } else if (curPageStartIdx == 0  && reqIdx < ROW - 2) {
	            	pageStartIdx = curPageStartIdx;
					if (Log.DEBUG_ON) {
						Log.printDebug("[ResultList.getCurrentItemInformation] e pageStartIdx: " + pageStartIdx);
					}
	            } else {
					if (Log.DEBUG_ON) {
						Log.printDebug("[ResultList.getCurrentItemInformation] reqIdx - curIdx : " + reqIdx + " - " + curIdx);
					}
	            	if (reqIdx <= curIdx) {
						pageStartIdx = curPageStartIdx - 1;	 
	            	} else if (reqIdx > curIdx) {
						pageStartIdx = curPageStartIdx + 1;	   
	            	}
	            }
	    	}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ResultList.getCurrentItemInformation] Result: pageStartIdx - pageCount: " + pageStartIdx +" - " + pageCount);
		}
		return new ItemInformation(pageStartIdx, pageCount);
	}
	
	private void checkScroll(int reqIdx) {
		stopScrollTimer();
		if (adapter == null || reqIdx < 0 || reqIdx >= adapter.getTotalCount()) {
			return;
		}
		Resource resource = adapter.getResource(reqIdx);
		if (resource != null) {
			String title = resource.getTitle();
			String displayTitle = Util.getStringExceptString(title, Rs.TAG_EM_START);
			displayTitle = Util.getStringExceptString(displayTitle, Rs.TAG_EM_END);
			int displayTitleW = Rs.FM20.stringWidth(displayTitle);
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResultList.checkScroll] displayTitleW-validWth: " + displayTitleW + "-" + validWth);
			}
			if (displayTitleW > validWth) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[ResultList.checkScroll] request startScrollTimer : " + reqIdx);
				}
				startScrollTimer();
			}
		}
	}
	
	private void doEventOfIndexChanged(int reqIdx) {
		if (listener != null) {
			listener.itemChanged(this, reqIdx);
		}
	}
	
	class ItemInformation {
		private final int startIdx;
		private final int limit;
		
		public ItemInformation(int reqStartIdx, int reqLimit) {
			startIdx = reqStartIdx;
			limit = reqLimit;
		}

		public int getPageStartIdx() {
			return startIdx;
		}

		public int getPageCount() {
			return limit;
		}
	}
	
	class TVTimerWentOffListenerImpl implements TVTimerWentOffListener {
		public void timerWentOff(TVTimerWentOffEvent arg0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ResultList.TVTimerWentOffListenerImpl.timerWentOff] Scroll: " +scroll);
			}
			scroll++;
			repaint(itemBounds.x, itemBounds.y, itemBounds.width, itemBounds.height);
		}
	}
}

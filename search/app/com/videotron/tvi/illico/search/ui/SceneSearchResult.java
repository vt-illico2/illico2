/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui;

import java.awt.Container;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.epg.ChannelContext;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.controller.SceneManager;
import com.videotron.tvi.illico.search.controller.SceneTemplate;
import com.videotron.tvi.illico.search.controller.SearchController;
import com.videotron.tvi.illico.search.controller.VbmController;
import com.videotron.tvi.illico.search.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.search.data.Definition;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.ResourceList;
import com.videotron.tvi.illico.search.data.SearchDataManager;
import com.videotron.tvi.illico.search.data.SearchException;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.data.SearchParameter;
import com.videotron.tvi.illico.search.data.SearchSortDef;
import com.videotron.tvi.illico.search.data.TabResultDef;
import com.videotron.tvi.illico.search.data.resource.SearchedEpg;
import com.videotron.tvi.illico.search.data.resource.SearchedPerson;
import com.videotron.tvi.illico.search.data.resource.SearchedPvr;
import com.videotron.tvi.illico.search.data.resource.SearchedPvrGroup;
import com.videotron.tvi.illico.search.data.resource.SearchedVod;
import com.videotron.tvi.illico.search.gui.RendererSearchResult;
import com.videotron.tvi.illico.search.ui.component.DetailInformationPanel;
import com.videotron.tvi.illico.search.ui.component.ErrorPanel;
import com.videotron.tvi.illico.search.ui.component.ResultList;
import com.videotron.tvi.illico.search.ui.component.SearchComponent;
import com.videotron.tvi.illico.search.ui.component.TabResult;
import com.videotron.tvi.illico.search.ui.component.event.ComponentWatcher;
import com.videotron.tvi.illico.search.ui.component.event.ResultListAdpater;
import com.videotron.tvi.illico.search.ui.component.event.SelectorListener;
import com.videotron.tvi.illico.search.ui.component.event.TabResultListener;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>SceneResult</code>
 * 
 * @version $Id: SceneSearchResult.java,v 1.4.2.1.2.1 2017/08/31 19:46:29 freelife Exp $
 * @author ZioN
 * @since 2015. 3. 25.
 * @description
 */
public class SceneSearchResult extends Scene {
	private static final long serialVersionUID = 2236000117180052446L;
	
	/* Parameter from input */
	private SearchParameter searchParameter;
	public String pSearchKeyword;
	public String displaySearchKeyword;
	public int pSearchFilterId;
	public int displaySearchKeywordW;
	public boolean isSearchedLetters;
	
	private static final String REQUEST_TYPE_INIT_DATA = "REQUEST_TYPE_INIT_DATA";
	private static final String REQUEST_TYPE_UPDATE_DATA = "REQUEST_TYPE_UPDATE_DATA";
	private static final String REQUEST_TYPE_RESET_DATA = "REQUEST_TYPE_RESET_DATA";
    
    private static final int[] OTEHR_RESULT_TAB_PRIORITY = {
    	TabResultDef.ID_LIVE, TabResultDef.ID_ON_DEMAND, TabResultDef.ID_RECORDINGS, TabResultDef.ID_PERSON
    };
    
    private static final int[] PVR_RESULT_TAB_PRIORITY = {
    	TabResultDef.ID_RECORDINGS, TabResultDef.ID_LIVE, TabResultDef.ID_ON_DEMAND, TabResultDef.ID_PERSON
    };
    
    private static final int[] VOD_RESULT_TAB_PRIORITY = {
    	TabResultDef.ID_ON_DEMAND, TabResultDef.ID_LIVE, TabResultDef.ID_RECORDINGS, TabResultDef.ID_PERSON
    };
    
    private static final int[] PERSON_RESULT_TAB_PRIORITY = {
    	TabResultDef.ID_PERSON
    };
    
    private static final int VALID_SEARCH_PARAMETER_W = 240;
    
    /** Component - OptionScreen */
	private MenuListenerImpl menuListenerImpl;
	private OptionScreen optionScreen;
	
	private Footer footer;
	
	private ComponentWatcherImpl compWatcherImpl;
	private TabResult tabResult;
	private TabResultListenerImpl tabResultListenerImpl;
	private ResultList curResultList;
	private SelectorListenerImpl selectorListenerImpl;
	private DetailInformationPanel resultDetailInfo;
	private ErrorPanel errorPanel;
	
	private int initialTabResult = TabResultDef.ID_ON_DEMAND;
	
	private int updateCount;
	private int dataCacheCount;
	private int requestedCount;
	private int receivedCount;
	
	private int curTabResultId = -1;
	private Resource[] curRsc;
	private MenuItem curRootMenuItem;
	
	public String curSortValue;
	
	private ResultListAdapterImpl searchListAdapterImpl;
	private Hashtable resultListHash;
	private Hashtable reulstListDataHash;
	private Hashtable menuItemHash;
	
	private boolean isInitialRequestActivated;
	private int[] resultTabPriority;
	
	private int receivedDataCount = 0;
	private boolean isInvalidInitData;
	public boolean isNoResult = false;
	
	protected void initScene() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.initScene] Called.");
		}
		setRenderer(new RendererSearchResult());
		prepare();
		initResource();
		initComponent();
	}
	
	protected void disposeScene() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.disposeScene] Called.");
		}
		SearchController.getInstance().hideLoadingAnimation();
		disposeResource();
		disposeComponent();
	}
	
	private void initResource() {
		updateCount = DataCenter.getInstance().getInt("REQUEST_UPDATE_COUNT");
		dataCacheCount = DataCenter.getInstance().getInt("DATA_CACHE_COUNT");
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.initResource] dataCacheCount: " + dataCacheCount);
		}
		reulstListDataHash = new Hashtable();
		resultListHash = new Hashtable();
		menuItemHash = new Hashtable();
	}
	
	private void disposeResource() {
		if (resultListHash != null) {
			Enumeration  enu = resultListHash.keys();
			while(enu.hasMoreElements()) {
				Integer key = (Integer) enu.nextElement();
				ResultList rl = (ResultList) resultListHash.get(key);
				rl.dispose();
				rl = null;
			}
			resultListHash.clear();
			resultListHash = null;
		}
		
		if (reulstListDataHash != null) {
			reulstListDataHash.clear();
			reulstListDataHash = null;
		}
		
		if (menuItemHash != null) {
			menuItemHash.clear();
			menuItemHash = null;
		}
		curRsc = null;
		searchParameter = null;
		pSearchKeyword = null;
	}
	
	private void initComponent() {
		compWatcherImpl = new ComponentWatcherImpl();
		searchListAdapterImpl = new ResultListAdapterImpl();
		/*
		 * Initialize SearchFilter
		 */
		resultDetailInfo = new DetailInformationPanel();
		resultDetailInfo.init();
		resultDetailInfo.setComponentWatcher(compWatcherImpl);
		add(resultDetailInfo);
		resultDetailInfo.setVisible(false);
		
		/*
		 * Initialize SearchFilter
		 */
		tabResultListenerImpl = new TabResultListenerImpl();
		tabResult = new TabResult();
		tabResult.init();
		tabResult.setComponentWatcher(compWatcherImpl);
		tabResult.setSearchResultTabListener(tabResultListenerImpl);
		add(tabResult);
		
		selectorListenerImpl = new SelectorListenerImpl();
		
		/*
		 * Initialize Footer
		 */
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        
        /*
         * Initialize OptionScreen
         */
        menuListenerImpl = new MenuListenerImpl();
        if (optionScreen == null) {
        	optionScreen = new OptionScreen(KeyCodes.COLOR_A, PreferenceService.BTN_A, "close_options");
        }
        
        /*
         * Initialize ErrorPanel
         */
        errorPanel = new ErrorPanel();
        errorPanel.init();
        errorPanel.setComponentWatcher(compWatcherImpl);
        errorPanel.setSelectorListener(selectorListenerImpl);
		add(errorPanel);
		errorPanel.setVisible(false);
	}
	
	private void disposeComponent() {
		if (tabResult != null) {
			remove(tabResult);
			tabResult.dispose();
			tabResult = null;
		}
		tabResultListenerImpl = null;
		
		if (curResultList != null) {
			remove(curResultList);
			curResultList.dispose();
			curResultList = null;
		}
		selectorListenerImpl = null;
		searchListAdapterImpl = null;
		
		if (resultDetailInfo != null) {
			remove(resultDetailInfo);
			resultDetailInfo.dispose();
			resultDetailInfo = null;
		}
		
		if (footer != null) {
			remove(footer);
			footer = null;
		}
		compWatcherImpl = null;
		
		optionScreen = null;
		menuListenerImpl = null;
		curRootMenuItem = null;
	}

	/**
	 * start Scene.
	 */
	protected void startScene(boolean resetScene, Object param) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.startScene] resetScene: " + resetScene);
		}
		if (resetScene) {
			searchParameter = (SearchParameter)param;
			pSearchKeyword = searchParameter.getSearchKeyword();
			if (pSearchKeyword != null) {
				int tempW = Rs.FM21.stringWidth(pSearchKeyword);
				if (tempW > VALID_SEARCH_PARAMETER_W) {
//					displaySearchKeyword = "..."+Util.shortenR(pSearchKeyword, Rs.FM21, VALID_SEARCH_PARAMETER_W);
					displaySearchKeyword = TextUtil.shorten(pSearchKeyword, Rs.FM21, VALID_SEARCH_PARAMETER_W);
				} else {
					displaySearchKeyword = pSearchKeyword;
				}
			}
			displaySearchKeywordW = Rs.FM21.stringWidth(displaySearchKeyword);
			pSearchFilterId = searchParameter.getSearchFilterId();
			isSearchedLetters = searchParameter.isSearchedLetters();
			isInitialRequestActivated = false;
			setInitialTab();
			requestInitialData(isSearchedLetters);
		} else {
			VbmController.getInstance().writeBackToSearchResult();
	        if (curResultList != null) {
	        	curResultList.start();
	        }
			changeTab(curTabResultId);
			repaint();
		}
	}
	
	/**
	 * Stop scene.
	 */
	protected void stopScene() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.stopScene] Called.");
		}
        if (optionScreen != null) {
        	optionScreen.stop();
        }
        if (curResultList != null) {
        	curResultList.stop();
        }
//        changeTab(TabResultDef.ID_INVALID);
	}
	
	protected boolean keyAction(int keyCode) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.keyAction] keyCode: " + keyCode);
		}
		if (isNoResult) {
			switch(keyCode) {
				case Rs.KEY_UP:
				case Rs.KEY_DOWN:
				case KeyCodes.COLOR_A:
				case KeyCodes.COLOR_B:
				case KeyCodes.COLOR_C:
					return true;
				case Rs.KEY_OK:
				case KeyCodes.SEARCH:
					backToPreviouseScene();
					return true;
			}
		}
		if (SearchController.getInstance().isActivatedLoadingAnimation()) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.keyAction] ActivatedLoadingAnimation()");
			}
			switch(keyCode) {
		        case Rs.KEY_PAGE_UP:
		        case Rs.KEY_PAGE_DOWN:
		        case KeyEvent.VK_UP:
		        case KeyEvent.VK_DOWN:
		        case KeyEvent.VK_LEFT:
		        case KeyEvent.VK_RIGHT:
		        	return true;
			}
		}
		
		switch(keyCode) {
			case Rs.KEY_LEFT:
				if (tabResult != null) {
					tabResult.keyActionLeft();
					repaint();
				}
				return true;
			case Rs.KEY_RIGHT:
				if (tabResult != null) {
					tabResult.keyActionRight();
					repaint();
				}
				return true;
			case Rs.KEY_UP:
				if (curResultList != null) {
					curResultList.keyActionUp();
					repaint();
				}
				return true;
			case Rs.KEY_DOWN:
				if (curResultList != null) {
					curResultList.keyActionDown();
					repaint();
				}
				return true;
				
			case Rs.KEY_PAGE_UP:
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.keyAction] KEY_PAGE_UP called.");
				}
				if (curRsc == null || curRsc.length <= ResultList.ROW) {
					return true;
				}
				if (footer != null) {
					footer.clickAnimation("Label.Footer_Change_Page");
				}
				if (curResultList != null) {
					curResultList.keyActionPageUp();
					repaint();
				}
				return true;
				
			case Rs.KEY_PAGE_DOWN:
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.keyAction] KEY_PAGE_DOWN called.");
				}
				if (curRsc == null || curRsc.length <= ResultList.ROW) {
					return true;
				}
				if (footer != null) {
					footer.clickAnimation("Label.Footer_Change_Page");
				}
				if (curResultList != null) {
					curResultList.keyActionPageDown();
					repaint();
				}
				return true;
				
			case Rs.KEY_OK:
				if (errorPanel != null && errorPanel.isVisible()) {
					errorPanel.keyActionOk();
					repaint();
					return true;
				}
				if (curResultList != null) {
					curResultList.keyActionOk();
					repaint();
				}
				return true;
			case KeyCodes.COLOR_A:
				if (optionScreen.isShowing()) {
					return true;
				}
				if (curRootMenuItem != null) {
					if (footer != null) {
						footer.clickAnimation("Label.Footer_Sort");
					}
					optionScreen.start(curRootMenuItem, menuListenerImpl);
				}
                return true;
			case Rs.KEY_EXIT:
                if (footer != null) {
                    footer.clickAnimation("Label.Footer_Exit");
                }
                SearchController.getInstance().stopSearchClient();
                return true;
            case KeyCodes.LAST:
            case KeyCodes.SEARCH:
            	backToPreviouseScene();
            	return true;
		}
		return false;
	}
	
	private void backToPreviouseScene() {
    	SceneManager.getInstance().backToPreviousScene();
	}
	
	//R7.4 freelife new VBM identifier
	private void requestInitialData(boolean isSearchLetter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.requestInitialData] Called.");
		}
		//R7.4 freelife new VBM identifier
		VbmController.getInstance().writeSearchQueryStart(isSearchLetter);
		receivedDataCount = 0;
		isInvalidInitData = false;
		isInitialRequestActivated = false;
		if (pSearchFilterId == SearchFilterDef.ID_PERSON) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.requestInitialData] Filter ID is PERSON. isSearchedLetters: " + isSearchedLetters);
			}
			if (isSearchedLetters) {
				requestedCount = 1;
				requestData(TabResultDef.ID_PERSON, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_PERSON, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
			} else {
				requestedCount = 3;
				requestData(TabResultDef.ID_LIVE, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_LIVE, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_ON_DEMAND, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_ON_DEMAND, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_RECORDINGS, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_RECORDINGS, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
			}
			return;
		}
		requestedCount = 4;
		switch(initialTabResult) {
			case TabResultDef.ID_LIVE:
				requestData(TabResultDef.ID_LIVE, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_LIVE, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_ON_DEMAND, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_ON_DEMAND, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_RECORDINGS, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_RECORDINGS, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_PERSON, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_PERSON, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				break;
			case TabResultDef.ID_ON_DEMAND:
				requestData(TabResultDef.ID_ON_DEMAND, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_ON_DEMAND, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_LIVE, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_LIVE, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_RECORDINGS, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_RECORDINGS, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_PERSON, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_PERSON, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				break;
			case TabResultDef.ID_RECORDINGS:
				requestData(TabResultDef.ID_RECORDINGS, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_RECORDINGS, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_LIVE, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_LIVE, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_ON_DEMAND, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_ON_DEMAND, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_PERSON, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_PERSON, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				break;
			case TabResultDef.ID_PERSON:
				requestData(TabResultDef.ID_PERSON, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_PERSON, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_LIVE, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_LIVE, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_ON_DEMAND, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_ON_DEMAND, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				requestData(TabResultDef.ID_RECORDINGS, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_RECORDINGS, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
				break;
		}
//		requestedCount = 1;
//		requestData(TabResultDef.ID_LIVE, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_LIVE, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
//		requestData(TabResultDef.ID_ON_DEMAND, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_ON_DEMAND, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
//		requestData(TabResultDef.ID_RECORDINGS, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_RECORDINGS, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
//		requestData(TabResultDef.ID_PERSON, 0, SearchSortDef.getInitialSortValue(TabResultDef.ID_PERSON, pSearchFilterId), REQUEST_TYPE_INIT_DATA);
	}

	private void setInitialTab() {
		if (pSearchFilterId == SearchFilterDef.ID_PERSON && isSearchedLetters) {
			initialTabResult = TabResultDef.ID_PERSON;	
			resultTabPriority = PERSON_RESULT_TAB_PRIORITY;
		} else {
			initialTabResult = TabResultDef.ID_LIVE;
			resultTabPriority = OTEHR_RESULT_TAB_PRIORITY;
			
			String flatform = SearchController.getInstance().getLaunchPlatform();
			if (flatform != null) {
				if (flatform.equalsIgnoreCase(SearchService.PLATFORM_PVR)) {
					initialTabResult = TabResultDef.ID_RECORDINGS;
					resultTabPriority = PVR_RESULT_TAB_PRIORITY;
				} else if (flatform.equalsIgnoreCase(SearchService.PLATFORM_VOD)) {
					initialTabResult = TabResultDef.ID_ON_DEMAND;
					resultTabPriority = VOD_RESULT_TAB_PRIORITY;
				}
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.setInitialTab] InitialTab is " + TabResultDef.getTabResultText(initialTabResult));
		}
	}
	
	private int getActiveTabResultId() {
		int activeTabResultId = -1;
		if (resultTabPriority != null) {
			for (int i = 0; i<resultTabPriority.length; i++) {
				Integer key = new Integer(resultTabPriority[i]);
				Object obj = reulstListDataHash.get(key);
				if (obj instanceof Resource[]) {
					Resource[] rscs = (Resource[]) obj;
					if (rscs != null && rscs.length > 0) {
						activeTabResultId = resultTabPriority[i];
						break;
					}
				}
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.getActiveTab] Active Tab: " + activeTabResultId + " (" + TabResultDef.getTabResultText(activeTabResultId) +")");
		}
		if (activeTabResultId == -1 && resultTabPriority != null && resultTabPriority.length > 0) {
			if (resultTabPriority != null) {
				for (int i = 0; i<resultTabPriority.length; i++) {
					Integer key = new Integer(resultTabPriority[i]);
					Object obj = reulstListDataHash.get(key);
					if (obj instanceof SearchException) {
						activeTabResultId = resultTabPriority[i];
						break;
					}
				}
			}
		}
		if (activeTabResultId == -1 && resultTabPriority != null && resultTabPriority.length > 0) {
			activeTabResultId = resultTabPriority[0];
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.getActiveTab] Active Tab: " + activeTabResultId + " (" + TabResultDef.getTabResultText(activeTabResultId) +")");
		}
		return activeTabResultId;
	}
	
	private synchronized void changeTab(int reqTabResultId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.changeTab] Requested tab result: " + reqTabResultId);
		}
		Integer key = new Integer(reqTabResultId);
		Object obj = reulstListDataHash.get(key);
		if (obj instanceof SearchException) {
			changeTabResult(reqTabResultId);
			showErrorPanel((SearchException) obj);
			changeFooterByTabResult(TabResultDef.ID_INVALID);
			changeOptionMenuItem(TabResultDef.ID_INVALID);
		} else if (obj instanceof Resource[]) {
			curRsc = (Resource[]) obj;
			changeTabResult(reqTabResultId);
			setResultData(reqTabResultId);
			changeResultList(reqTabResultId);
			changeFooterByTabResult(reqTabResultId);
			changeOptionMenuItem(reqTabResultId);
		}
		curTabResultId = reqTabResultId;
	}
	
	private void showErrorPanel(SearchException reqException) {
		if (curResultList != null) {
			curResultList.setVisible(false);
			remove(curResultList);
			curResultList = null;
		}
		if (resultDetailInfo != null) {
			resultDetailInfo.setVisible(false);
		}
		if (errorPanel != null) {
			if (reqException == null) {
				errorPanel.setNoResult();
			} else {
				errorPanel.setException(reqException);
			}
			errorPanel.setVisible(true);
		}
	}
	
	private void showNoResultErrorPanel() {
		showErrorPanel(null);
	}
	
	private synchronized void changeTabResult(int reqTabResultId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.changeTabResult] Called. Request TabResult: " + TabResultDef.getTabResultText(reqTabResultId));
		}
		
		if (curTabResultId == reqTabResultId) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.changeTabResult] Request TabResult is same as current TabResult");
			}
			return;
		}
		tabResult.setTabResult(reqTabResultId);
	}
	
	private synchronized void changeResultList(int reqTabResultId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.changeResultList] Called. Request TabResult: " + TabResultDef.getTabResultText(reqTabResultId));
		}
		if (curTabResultId == reqTabResultId) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.changeResultList] Request TabResult is same as current TabResult");
			}
			return;
		}
		
		if (curResultList != null) {
			curResultList.setFocus(false);
			remove(curResultList);
			curResultList = null;
		}
		
		if (errorPanel != null) {
			errorPanel.setVisible(false);
		}

		boolean isResultDetailInfoVisible = reqTabResultId != TabResultDef.ID_PERSON;
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.changeResultList] isResultDetailInfoVisible: " + isResultDetailInfoVisible);
		}
		resultDetailInfo.setVisible(isResultDetailInfoVisible);
		
		Integer hashKey = new Integer(reqTabResultId);
		curResultList = (ResultList)resultListHash.get(hashKey);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.changeResultList] curResultList: " + curResultList);
		}
		if (curResultList != null) {
			curResultList.setVisible(true);
			curResultList.setFocus(true);
			add(curResultList, 0);
		}
		repaint();
	}
	
	private void changeFooterByTabResult(int reqTabResultId) {
		footer.reset();
		switch(reqTabResultId) {
			case TabResultDef.ID_LIVE:
			case TabResultDef.ID_ON_DEMAND:
			case TabResultDef.ID_RECORDINGS:
			case TabResultDef.ID_PERSON:
				if (curRsc != null && curRsc.length > ResultList.ROW) {
					footer.addButton(PreferenceService.BTN_PAGE, "Label.Footer_Change_Page");
				}
		        footer.addButton(PreferenceService.BTN_A, "Label.Footer_Sort");
		        footer.addButton(PreferenceService.BTN_EXIT, "Label.Footer_Close");
				break;
			default:
				footer.addButton(PreferenceService.BTN_EXIT, "Label.Footer_Close");
				break;
		}
	}
	
	private void changeOptionMenuItem(int reqTabResultId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.changeOptionMenuItem] Called. reqTabResultId: " + TabResultDef.getTabResultText(reqTabResultId));
		}
		String sortOrderKey = null;
		Integer key = new Integer(reqTabResultId);
		MenuItem tempMenuItem = (MenuItem)menuItemHash.get(key);		
		MenuItem checkedMenuItem = null;
		if (tempMenuItem == null) {
			tempMenuItem = new MenuItem("Label.Sort");
			sortOrderKey = SearchSortDef.getSortOrderKey(reqTabResultId, pSearchFilterId);
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.changeOptionMenuItem] sortOrderKey: " + sortOrderKey);
			}
			if (sortOrderKey != null) {
				String defaultValue = DataCenter.getInstance().getString(SearchSortDef.getSortDefalutKey(reqTabResultId, pSearchFilterId));
				String sortIds = DataCenter.getInstance().getString(sortOrderKey);
				StringTokenizer st = new StringTokenizer(sortIds, ",");
				int stCount = st.countTokens();
				for (int i=0; i<stCount; i++) {
					String value = st.nextToken();
					MenuItem menuItem = new MenuItem(value);
					if (defaultValue == null && i == 0) {
						defaultValue = value;
					}
					if (value.equals(defaultValue)) {
						checkedMenuItem = menuItem;
						checkedMenuItem.setChecked(true);
						setCurrentSortId(tempMenuItem, checkedMenuItem);
					}
					tempMenuItem.add(menuItem);
				}
				menuItemHash.put(key, tempMenuItem);
			} else {
				tempMenuItem = null;
			}
		} else {
			checkedMenuItem = getCurrentSortId(tempMenuItem);
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.changeOptionMenuItem] checkedMenuItem: " + checkedMenuItem);
			}
			setCurrentSortId(tempMenuItem, checkedMenuItem);
		}
		curRootMenuItem = tempMenuItem;
	}
	
	private void setCurrentSortId (MenuItem reqRootMenuItem, MenuItem selectedMenuItem) {
		curSortValue = null;
		if (reqRootMenuItem != null) {
			int childSize = reqRootMenuItem.size();
			for (int i=0; i<childSize; i++) {
				MenuItem child = (MenuItem) reqRootMenuItem.getItemAt(i);
				child.setChecked(child.equals(selectedMenuItem));
				if (child.isChecked()) {
					String key = child.getKey();
					curSortValue = DataCenter.getInstance().getString("SORT_OPTION_VALUE_"+key);
				}
			}
		}
		if (Log.EXTRA_ON) {
			repaint();
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.setCurrentSortId] Current Sort Value: " + curSortValue);
		}
	}
	
	private MenuItem getCurrentSortId (MenuItem reqRootMenuItem) {
		MenuItem checked = null;
		if (reqRootMenuItem != null) {
			int childSize = reqRootMenuItem.size();
			for (int i=0; i<childSize; i++) {
				MenuItem child = (MenuItem) reqRootMenuItem.getItemAt(i);
				if (child != null && child.isChecked()) {
					checked = child;
					break;
				}
			}
		}
		return checked;
	}
	
	private void requestData(int reqTabResultId, int from, String reqSortValue, String reqRequestType) {
		boolean isLoadingAnimationMovingKey = false;
		if (reqRequestType.equals(REQUEST_TYPE_INIT_DATA)) {
			isLoadingAnimationMovingKey = true;
		}
		SearchController.getInstance().showLoadingAnimation(isLoadingAnimationMovingKey);
		new SearchRequestor(pSearchKeyword, pSearchFilterId, reqSortValue, reqTabResultId, from, reqRequestType, isSearchedLetters).start();
	}
	
	synchronized private void receiveInitData(int reqTabResultId, Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.receiveInitData] reqTabResultId: " + TabResultDef.getTabResultText(reqTabResultId) + ", Object: " + obj);
		}
		int curState = getCurrentSceneState();
		if (getCurrentSceneState() != STATE_STARTED) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.receiveInitData] Current state is " + getCurrentSceneStateName(curState) + ". That state is invalid.");
			}
			SearchController.getInstance().hideLoadingAnimation();
			return;
		}
		receivedCount ++;
		if (obj == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.receiveInitData] obj is null.");
			}
			SearchController.getInstance().hideLoadingAnimation();
			return;
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.receiveInitData] request count - recieved count: " + requestedCount +" - " + receivedCount);
		}
		int returnedElementCount = 0;
		if (obj instanceof ResourceList) {
			ResourceList resourceList = (ResourceList) obj;
			returnedElementCount = resourceList.getReturnedElementCount();
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.receiveInitData] Received Count -  Received Data Count: (" + receivedCount + " - " + returnedElementCount +")");
		}
		receivedDataCount += returnedElementCount;
		if (obj instanceof SearchException) {
			isInvalidInitData = true;
			putExceptionData(reqTabResultId, (SearchException)obj);
			createTabResult(reqTabResultId, obj);
//			createResultList(reqTabResultId, obj);
		} else if (obj instanceof ResourceList) {
			ResourceList resourceList = (ResourceList)obj;
			putResultData(reqTabResultId, resourceList);
			createTabResult(reqTabResultId, obj);
			createResultList(reqTabResultId, resourceList);
		}
		
		if (initialTabResult == reqTabResultId) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.receiveInitData] received  the data of Initial tab");
			}
			/*
			 * If received data length is 0, keep continuing util request count same as receved count.
			 * Otherwise, Result list did not visible. 
			 */
			if (returnedElementCount > 0) {
				changeTab(reqTabResultId);
				isInitialRequestActivated = true;
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.receiveInitData] received initialTabResult");
				}
				SearchController.getInstance().hideLoadingAnimation();
				repaint();
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.receiveInitData] Returned element count of Initial tab is zero. so can't execute initial tab process. ");
				}
			}
		}
		if (requestedCount == receivedCount) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.receiveInitData] End of request.");
			}
			//R7.4 freelife new VBM identifier
			VbmController.getInstance().writeSearchQueryEnd(searchParameter.isSearchedLetters());
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.receiveInitData] receivedDataCount: " + receivedDataCount + ", isInvalidInitData: " + isInvalidInitData +", isInitialRequestActivated: " + isInitialRequestActivated);
			}
			if (receivedDataCount == 0 && !isInvalidInitData) {
				VbmController.getInstance().writeSearchNoResult();
				isNoResult = true;
				showNoResultErrorPanel();
			} else {
				if (!isInitialRequestActivated) {
					int activeTab = getActiveTabResultId();
					changeTab(activeTab);					
				}
			}
			SearchController.getInstance().hideLoadingAnimation();
			repaint();
		}
	}
	
	synchronized private void receiveUpdateData(int reqTabResultId, Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.receiveUpdateData] reqTabResultId: " + TabResultDef.getTabResultText(reqTabResultId) + ", Object: " + obj);
		}
		int curState = getCurrentSceneState();
		if (getCurrentSceneState() != STATE_STARTED) {
			if (Log.INFO_ON) {
				Log.printInfo("[SceneSearchResult.receiveUpdateData] Current state is " + getCurrentSceneStateName(curState) + ". That state is invalid.");
			}
			SearchController.getInstance().hideLoadingAnimation();
			return;
		}
		
		if (obj instanceof SearchException) {
			// item error.
//			putExceptionData(reqTabResultId, (SearchException)obj);
//			SearchException exception = (SearchException)obj;
//			SearchController.getInstance().showErrorPopup(exception.getCode());

			// Tab error.
			removeResultData(reqTabResultId);
			removeResultList(reqTabResultId);
			changeTab(TabResultDef.ID_INVALID);
			putExceptionData(reqTabResultId, (SearchException)obj);
			changeTab(reqTabResultId);
		} else if (obj instanceof ResourceList) {
			ResourceList resourceList = (ResourceList)obj;
			if (isOverCacheCount(reqTabResultId, resourceList)) {
				cacheResultData(reqTabResultId, resourceList);
			}
			putResultData(reqTabResultId, resourceList);
			setResultData(reqTabResultId);
		}
		SearchController.getInstance().hideLoadingAnimation();
		repaint();
	}
	
	synchronized private void receiveResetData(int reqTabResultId, Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.receiveResetData] reqTabResultId: " + TabResultDef.getTabResultText(reqTabResultId) + ", Object: " + obj);
		}
		int curState = getCurrentSceneState();
		if (getCurrentSceneState() != STATE_STARTED) {
			if (Log.INFO_ON) {
				Log.printInfo("[SceneSearchResult.receiveResetData] Current state is " + getCurrentSceneStateName(curState) + ". That state is invalid.");
			}
			SearchController.getInstance().hideLoadingAnimation();
			return;
		}
		removeResultData(reqTabResultId);
		removeResultList(reqTabResultId);
		changeTab(TabResultDef.ID_INVALID);
		if (obj instanceof SearchException) {
			putExceptionData(reqTabResultId, (SearchException)obj);
		} else if (obj instanceof ResourceList) {
			ResourceList resourceList = (ResourceList)obj;
			putResultData(reqTabResultId, resourceList);
			changeTabResult(reqTabResultId, resourceList);
			createResultList(reqTabResultId, resourceList);
		}
		changeTab(reqTabResultId);
		SearchController.getInstance().hideLoadingAnimation();
		repaint();
	}
	
	private void putExceptionData(int reqTabResultId, SearchException exception) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.putExceptionData] reqTabResultId: " + TabResultDef.getTabResultText(reqTabResultId));
			Log.printDebug("[SceneSearchResult.putExceptionData] Exception: " + exception);
		}
		Integer key = new Integer(reqTabResultId);
		reulstListDataHash.remove(key);
		reulstListDataHash.put( key, exception);
	}
	
	private void setResultData(int reqTabResultId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.setResultData] reqTabResultId: " + reqTabResultId);
		}
		Integer hashKey = new Integer(reqTabResultId);
		curRsc = (Resource[]) reulstListDataHash.get(hashKey);
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.setResultData] curRsc: " + curRsc);
		}
	}
	
	private void putResultData(int reqTabResultId, ResourceList resourceList) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.putResultData] reqTabResultId: " + TabResultDef.getTabResultText(reqTabResultId));
			Log.printDebug("[SceneSearchResult.putResultData] ResourceList: " + resourceList);
		}
		
		int returnedElementCount = 0;
		if (resourceList != null) {
			returnedElementCount = resourceList.getReturnedElementCount();
		}
		if (returnedElementCount <= 0) {
			return;
		}
		Integer key = new Integer(reqTabResultId);
		Resource[] putResultListData = (Resource[]) reulstListDataHash.get(key);
		int totalElementCount = resourceList.getTotalElementCount();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.putResultData] Total Element Count: " + totalElementCount);
		}
		if (putResultListData == null) {
			putResultListData = new ResourceList[resourceList.getTotalElementCount()];
		}
		int forCount = returnedElementCount;
		int startIdx = resourceList.getParameterFrom();
		Resource[] returnedElement = resourceList.getListData();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.putResultData] startIdx: "+startIdx+", returnedElement: " + returnedElement.length);
		}
		if (returnedElement != null && returnedElement.length > 0) {
			for (int i = 0; i < forCount; i++) {
				int index = startIdx + i;
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.putResultData] index: " + index);
				}
				putResultListData[index] = returnedElement[i];
			}
			reulstListDataHash.put(key, putResultListData);
		}
	}
	
	private boolean isOverCacheCount(int reqTabResultId, ResourceList resourceList) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.isOverCacheCount] called.");
		}
		boolean isOverCacheCount = false;
		int count = resourceList.getReturnedElementCount();
		Integer key = new Integer(reqTabResultId);
		Resource[] putResultListData = (Resource[]) reulstListDataHash.get(key);
		for (int i = 0; i<putResultListData.length; i++) {
			if (putResultListData[i] != null) {
				count ++;
				if (count > dataCacheCount) {
					if (Log.DEBUG_ON) {
						Log.printDebug("[SceneSearchResult.isOverCacheCount] i: " + i);
					}
					isOverCacheCount = true;
					break;
				}
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.isOverCacheCount] isOverCacheCount: " + isOverCacheCount);
		}
		return isOverCacheCount;
	}
	
	private void removeResultData(int reqTabResultId) {
		Integer key = new Integer(reqTabResultId);
		Object obj = reulstListDataHash.get(key);
		reulstListDataHash.remove(key);
		obj = null;
		curRsc = null;
	}
	
	public void cacheResultData(int reqTabResultId, ResourceList reqResourceList) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.cacheResultData] Called. reqTabResultId: " + TabResultDef.getTabResultText(reqTabResultId));
		}
		Resource[] temp = new Resource[curRsc.length];
		
//		int curPageStartIdx = curResultList.getCurrentPageStartIndex();
//		int curPageToIdx = curPageStartIdx + curResultList.getCurrentPageCount() - 1;
//		int from = reqResourceList.getParameterFrom();
//		int to = reqResourceList.getParameterTo();
//		
//		int cacheFromIdx = -1;
//		int cacheToIdx = -1;
//		if (curPageStartIdx > from) {
//			cacheFromIdx = to + 1;
//			cacheToIdx = curPageToIdx + ResultList.ROW  -1;
//		} else {
//			cacheFromIdx = curPageStartIdx - ResultList.ROW;
//			cacheToIdx = from - 1;
//		}
//		if (Log.DEBUG_ON) {
//			Log.printDebug("[SceneSearchResult.cacheResultData] from - to: " + from +" - " + to);
//			Log.printDebug("[SceneSearchResult.cacheResultData] cacheFromIdx - cacheTo: " + cacheFromIdx +" - " + cacheToIdx);
//		}
//		for (int i=cacheFromIdx; i <= cacheToIdx; i++) {
//			temp[i] = curRsc[i];
//		}
//		removeResultData(reqTabResultId);
//		Integer key = new Integer(reqTabResultId);
//		reulstListDataHash.put(key, temp);
//		curRsc = temp;
		
		int curPageStartIdx = curResultList.getCurrentPageStartIndex();
		int curPageToIdx = curPageStartIdx + curResultList.getCurrentPageCount() - 1;
		
		int cacheFromIdx = curPageStartIdx - updateCount;
		int cacheToIdx = curPageToIdx + updateCount;
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.cacheResultData] curPageStartIdx - curPageToIdx: " + curPageStartIdx +" - " + curPageToIdx);
			Log.printDebug("[SceneSearchResult.cacheResultData] cacheFromIdx - cacheTo: " + cacheFromIdx +" - " + cacheToIdx);
		}
		for (int i=cacheFromIdx; i <= cacheToIdx; i++) {
			if (i < 0 || i >= curRsc.length) {
				break;
			}
			temp[i] = curRsc[i];
		}
		removeResultData(reqTabResultId);
		Integer key = new Integer(reqTabResultId);
		reulstListDataHash.put(key, temp);
		curRsc = temp;
	}
	
	private void createTabResult(int reqTabResultId, Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.createTabResult] Called. TabResult is " + TabResultDef.getTabResultText(reqTabResultId) +", Object" + obj) ;
		}
		if (obj == null) {
			return;
		}
		if (obj instanceof Exception) {
			tabResult.addTabResult(reqTabResultId, 0);
		}else if (obj instanceof ResourceList) {
			ResourceList resourceList = (ResourceList) obj;
			if (resourceList != null && resourceList.getTotalElementCount() > 0 && !tabResult.existTabResult(reqTabResultId)) {
				tabResult.addTabResult(reqTabResultId, resourceList.getTotalElementCount());
			}
		}
	}
	
	private void changeTabResult(int reqTabResultId, Object obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.createTabResult] Called. TabResult is " + TabResultDef.getTabResultText(reqTabResultId) +", Object" + obj) ;
		}
		if (obj == null) {
			return;
		}
		if (obj instanceof Exception) {
			tabResult.changeTabResoultTotalCount(reqTabResultId, 0);
		}else if (obj instanceof ResourceList) {
			ResourceList resourceList = (ResourceList) obj;
			if (resourceList != null && resourceList.getTotalElementCount() > 0) {
				tabResult.changeTabResoultTotalCount(reqTabResultId, resourceList.getTotalElementCount());
			}
		}
	}
	
	private void createResultList(int reqTabResultId, ResourceList resourceList) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchResult.createResultList] Called.");
		}
		
		if (resourceList != null && resourceList.getTotalElementCount() > 0) {
			Integer hashKey = new Integer(reqTabResultId);
			ResultList resultList = (ResultList) resultListHash.get(hashKey);
			if (resultList == null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.createResultList] Create new ResultList ("+ TabResultDef.getTabResultText(reqTabResultId) +")");
				}
				resultList = new ResultList(reqTabResultId, resourceList.getTotalElementCount());
				resultList.init();
				resultList.setSearchParameter(searchParameter);
				resultList.setComponentWatcher(compWatcherImpl);
				resultList.setResultListAdapter(searchListAdapterImpl);
				resultList.setSelectorListener(selectorListenerImpl);
				resultListHash.put(hashKey, resultList);
			}
		}
	}
	
	private void removeResultList(int reqTabResultId) {
		Integer hashKey = new Integer(reqTabResultId);
		ResultList resultList = (ResultList) resultListHash.get(hashKey);
		resultListHash.remove(hashKey);
		if (curResultList != null) {
			remove(curResultList);
			curResultList.dispose();
			curResultList = null;
		}
	}
	
	class ComponentWatcherImpl implements ComponentWatcher {
		public void goOverUp(Container comp) {
			
		}

		public void goOverDown(Container comp) {
			
		}

		public void goOverLeft(Container comp) {
			
		}

		public void goOverRight(Container comp) {
			
		}
	}
	
	class TabResultListenerImpl implements TabResultListener {
		public void changedSearchResultTab(int changedSearchResultTabId) {
			changeTab(changedSearchResultTabId);
			repaint();
		}
	}
	
	class ResultListAdapterImpl implements ResultListAdpater {
		public Resource getResource(int index) {
			Resource rsc = curRsc[index];
			return rsc;
		}

		public int getTotalCount() {
			if (curRsc == null) {
				return 0;
			}
			return curRsc.length;
		}
		
		synchronized public void checkPreviouseDataSet(int requestIdx) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListAdapterImpl.checkPreviouseDataSet] Called. Request Index: " + requestIdx);
			}
			/*
			 * Check empty data from (requestIdx - updateCount + 1) to requestIdx
			 */
			if (requestIdx <= 0) {
				return;
			}
			//Reverse
			int checkStartIdx = requestIdx;
			if (requestIdx >= curRsc.length) {
				checkStartIdx = curRsc.length - 1;
			}
			/*
			 * If empty index found, request data from (emptyIdx - ResultList.ROW + 1) to emptyIdx
			 */
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListAdapterImpl.checkPreviouseDataSet]requestIdx: "+requestIdx+", checkStartIdx: " + checkStartIdx +", checkCount: " + ResultList.ROW);
			}
			int emptyIdx = -1;
			for (int i = checkStartIdx; i >= checkStartIdx - ResultList.ROW; i--) {
				if (i < 0) {
					break;
				}
				if (curRsc[i] == null) {
					emptyIdx = i;
					break;
				}
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListAdapterImpl.checkPreviouseDataSet] emptyIdx: " + emptyIdx);
			}
			if (emptyIdx != -1) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.ResultListAdapterImpl.checkPreviouseDataSet] Empty index is " + emptyIdx + ", request data");
				}
				requestedCount = 1;
				requestData(curTabResultId, emptyIdx - updateCount + 1, curSortValue, REQUEST_TYPE_UPDATE_DATA);
			}
		}
		
		synchronized public void checkNextDataSet(int requestIdx) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListAdapterImpl.checkNextDataSet] Called. Request Index: " + requestIdx);
			}
			int emptyIdx = -1;
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListAdapterImpl.checkNextDataSet] requestIdx: " + requestIdx);
			}
			for (int i = requestIdx; i < requestIdx + ResultList.ROW; i++) {
				if (i >= curRsc.length) {
					break;
				}
				if (curRsc[i] == null) {
					emptyIdx = i;
					break;
				}
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListAdapterImpl.checkNextDataSet] emptyIdx: " + emptyIdx);
			}
			if (emptyIdx != -1) {
				requestedCount = 1;
				requestData(curTabResultId, emptyIdx, curSortValue, REQUEST_TYPE_UPDATE_DATA);
			}
		}
	}
	
	class SelectorListenerImpl implements SelectorListener {
		public void itemChanged(SearchComponent comp, int reqIdx) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListListenerImpl.indexChanged] reqIdx: " + reqIdx);
			}
			if (resultDetailInfo != null && curRsc != null && reqIdx < curRsc.length) {
				resultDetailInfo.setResource(curRsc[reqIdx]);
			}
		}

		public void itemSelected(SearchComponent comp, int reqIdx) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] Called. Request index: " + reqIdx);
			}
			
			if (comp == null) {
				return;
			}
			
			if (comp.equals(errorPanel)) {
				SceneManager.getInstance().backToPreviousScene();
				return;
			}
			
			Resource selectedRsc = curRsc[reqIdx];
			if (selectedRsc == null) {
				return;
			}
			String destPlatform = null;
			String[] param = null;
			if (selectedRsc instanceof SearchedEpg) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] Resource is EPG type");
				}
				SearchedEpg searchedLinear = (SearchedEpg) selectedRsc;
				String channelCallLetter = searchedLinear.getChannelCallLetter();
				String startTimeMillis = String.valueOf(searchedLinear.getStartTimeMillis());
				if (Log.INFO_ON) {
					Log.printInfo("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM0(AppName): " + Rs.APP_NAME);
					Log.printInfo("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM1(Channel Call Letter): " + channelCallLetter);
					Log.printInfo("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM2(Start Time Millis): " + startTimeMillis);
				}
				param = new String[]{Rs.APP_NAME, channelCallLetter, startTimeMillis};
				destPlatform = Definition.SRC_EPG;
			} else if (selectedRsc instanceof SearchedVod) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] Resource is VOD type");
				}
				SearchedVod searchedVod = (SearchedVod) selectedRsc;
				String vodId = searchedVod.getVodId();
				String flatform = Definition.SRC_VOD;
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM0(AppName): " + Rs.APP_NAME);
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM1(VOD ID): " + vodId);
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM2(Flatform): " + flatform);
				}
				param = new String[]{Rs.APP_NAME, vodId, flatform, DataCenter.getInstance().getBoolean(Rs.DCKEY_IS_AUTHORIZED) + ""};
				destPlatform = Definition.SRC_VOD;
			} else if (selectedRsc instanceof SearchedPvrGroup) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] Resource is PVR type");
				}
				SearchedPvrGroup searchedGroupPvr = (SearchedPvrGroup) selectedRsc;
				String pvrTargetPage = String.valueOf(PvrService.MENU_RECORDING_LIST);
				SearchedPvr searchedPvr = searchedGroupPvr.getItem(0);
				String pvrId = searchedPvr.getId();
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM0(AppName): " + Rs.APP_NAME);
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM1(PVR Target Page): " + pvrTargetPage);
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] PARAM2(PVR ID): " + pvrId);
				}
				param = new String[]{Rs.APP_NAME, pvrTargetPage, pvrId};
				destPlatform = Definition.SRC_PVR;
			} else if (selectedRsc instanceof SearchedPerson) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] Resource is Person type");
				}
				SearchedPerson searchedPerson = (SearchedPerson)selectedRsc;
				String name = searchedPerson.getTitle();
				if (name != null) {
					name = Util.getStringExceptString(name, Rs.TAG_EM_START);
					name = Util.getStringExceptString(name, Rs.TAG_EM_END);
					SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_RESULT, true, 
							new SearchParameter(name, SearchFilterDef.ID_PERSON, false));
				}
				return;
			}
			VbmController.getInstance().writeAccessDetailPage();
			SearchController.getInstance().pauseSearchClient(false);
	        String launchPlatform = SearchController.getInstance().getLaunchPlatform();
	        String backLaunchPlatform = SearchController.getInstance().getBackLaunchPlatform();
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.ResultListListenerImpl.selectedListItem] destPlatform: " + destPlatform +", launchPlatform: " + launchPlatform + ", backLaunchPlatform: " + backLaunchPlatform);
			}
	        if ((destPlatform.equals(launchPlatform) && backLaunchPlatform == null) || destPlatform.equals(backLaunchPlatform)) {
	        	
	        	// sangyong VDTRMASTER-6131
	        	// AV상태에서 Search로 진입한 경우 R7.3 요구사항에 의해서 VBM로그를 EPG로 남기기로 한 사항에대한 SideEffect.
	        	// Search가 EPG(FULL_SCREEN) 상태로 인지하여 startUnbound를 호출하지 않음
	        	// 현재 스크린상태를 체크하여 AV상태인경우는 startUnbound를 호출하도록 처리, 단 다른 영향을 고려해 EPG에 한해서만 처리함 
	        	try {
	    			MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
	    			if (mService.getState() == MonitorService.TV_VIEWING_STATE &&
	    					destPlatform.equals(SearchService.PLATFORM_EPG) && 
	    					launchPlatform.equals(SearchService.PLATFORM_EPG)) {
	    				if (Log.DEBUG_ON) {
	    					Log.printDebug("[SceneSearchResult.ResultListListenerImpl] current state is TV_VIEWING_STATE");
	    				}
	    				try {
	    	                ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME)).startUnboundApplication(destPlatform, param);
	    	            } catch (Exception e) {
	    	                Log.print(e);
	    	            }
	    	            if (Log.DEBUG_ON) {
	    					Log.printDebug("[SceneSearchResult.SelectorListenerImpl.itemSelected] startUnboundApplication");
	    				}
	    	            
	    	            return;
	    			}
	    		} catch (Exception e) {
	    			Log.print(e);
	    		}
	        	
	            SearchController.getInstance().passAction(destPlatform, param);
	            if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.SelectorListenerImpl.itemSelected] PassAction");
				}
	        } else {
	            try {
	                ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME)).startUnboundApplication(destPlatform, param);
	            } catch (Exception e) {
	                Log.print(e);
	            }
	            if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.SelectorListenerImpl.itemSelected] startUnboundApplication");
				}
	        }
		}
	}
	
	/**
	 * 
	 * <code>MenuListenerImpl</code>
	 * 
	 * @version 1.0
	 * @author ZioN
	 * @since 2015. 4. 22.
	 * @description Sort - related
	 */
	class MenuListenerImpl implements MenuListener {
		public void selected(MenuItem item) {
			setCurrentSortId(curRootMenuItem, item);
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.MenuListenerImpl.selected] curTabResultId: " + curTabResultId);
			}
			requestData(curTabResultId, 0, curSortValue, REQUEST_TYPE_RESET_DATA);
		}

		public void canceled() {
			if (optionScreen != null) {
				optionScreen.stop();
			}
		}
	}
	
	class SearchRequestor extends Thread {
		private final String pSearchText;
		private final int pSearchFilterId;
		private final int pSearchTabResultId;
		private final int pFrom;
		private final int pTo;
		private final String pReqType;
		private final String pSearchSortValue;
		private final boolean pIsLetters;
		
		private boolean isValidRequest;
		
		public SearchRequestor(String reqSearchText, int reqSearchFilterId, String reqSearchSortValue, int reqSearchTabResultId, int from, String reqReqType, boolean isLetters) {
			pSearchText = reqSearchText;
			pSearchFilterId = reqSearchFilterId;
			pSearchSortValue = reqSearchSortValue;
			pSearchTabResultId = reqSearchTabResultId;
			pReqType = reqReqType;
			pIsLetters = isLetters;
			int count = 0;
			if (from < 0) {
				count = updateCount + from;
				pFrom = 0;
			} else {
				pFrom = from;
				count = updateCount;
			}
			pTo = pFrom + count;
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.SearchRequestor.SearchRequestor] Request data SearchText: "+ pSearchText 
						+ ", from: " + pFrom 
						+ ", SearchFilter: " + SearchFilterDef.getSectionFilterText(pSearchFilterId) 
						+ ", SearchSort: " + pSearchSortValue 
						+ ", isLetters: " + isLetters 
						+ ", TabResult: " + TabResultDef.getTabResultText(pSearchTabResultId) 
						+ ", to: " + pTo
						+ ", Request type: " + pReqType);
			}
		}
		
		public void run() {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchResult.SearchRequestor.run] Start Request.");
			}
			Object obj = null;
			try {
				switch(pSearchTabResultId) {
					case TabResultDef.ID_LIVE:
						obj = SearchDataManager.getInstance().getSearchLive(pSearchText, pFrom, pTo, pSearchFilterId, pSearchSortValue, pIsLetters);
						break;
					case TabResultDef.ID_ON_DEMAND:
						obj = SearchDataManager.getInstance().getSearchVod(pSearchText, pFrom, pTo, pSearchFilterId, pSearchSortValue, pIsLetters);
						break;
					case TabResultDef.ID_RECORDINGS:
						obj = SearchDataManager.getInstance().getSearchPvr(pSearchText, pFrom, pTo, pSearchFilterId, pSearchSortValue);
						break;
					case TabResultDef.ID_PERSON:
						obj = SearchDataManager.getInstance().getSearchPerson(pSearchText, pFrom, pTo, pSearchFilterId, pSearchSortValue, pIsLetters);
						break;
				}
			}catch (Exception e) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SceneSearchResult.SearchRequestor.run] Exception ocurred. TabResult: " + TabResultDef.getTabResultText(pSearchTabResultId));
				}
				obj = e;
				e.printStackTrace();
			}
//			if (!isValidRequest) {
//				return;
//			}
			if (pReqType.equals(REQUEST_TYPE_INIT_DATA)) {
				receiveInitData(pSearchTabResultId, obj);
			} else if (pReqType.equals(REQUEST_TYPE_UPDATE_DATA)) {
				receiveUpdateData(pSearchTabResultId, obj);
			} else if (pReqType.equals(REQUEST_TYPE_RESET_DATA)) {
				receiveResetData(pSearchTabResultId, obj);
			}
		}
		
		public void setValidRequest(boolean isValidRequest) {
			this.isValidRequest = isValidRequest;
		}
		
		public String getSearchText() {
			return pSearchText;
		}
		
		public int getSearchFilterId() {
			return pSearchFilterId;
		}

		public int getSearchTabResultId() {
			return pSearchTabResultId;
		}
	}
}

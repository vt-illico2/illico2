/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import java.awt.Image;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.data.Resource;
import com.videotron.tvi.illico.search.data.SearchDataManager;
import com.videotron.tvi.illico.search.data.resource.SearchedVod;
import com.videotron.tvi.illico.search.gui.component.RendererDetailInformationPanel;

/**
 * <code>DetailInformationPanel</code>
 * 
 * @version $Id: DetailInformationPanel.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 1.
 * @description
 */
public class DetailInformationPanel extends SearchComponent {
	private static final long serialVersionUID = 1L;
	
	public Resource rsc;
	public VodPosterRequestor requestor;

	protected void initComponent() {
		setRenderer(new RendererDetailInformationPanel());
		initResource();
	}

	protected void disposeComponent() {
		requestor = null;
		rsc = null;
	}
	
	private void initResource() {
		
	}

	public void keyActionLeft() {
		//Nothing to do.
	}

	public void keyActionRight() {
		//Nothing to do.
	}

	public void keyActionUp() {
		//Nothing to do.
	}

	public void keyActionDown() {
		//Nothing to do.
	}

	public void keyActionOk() {
		//Nothing to do.
	}
	
	public void setResource(Resource reqResource) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[DetailInformationPanel.setResource] Called.");
		}
		rsc = reqResource;
		if (rsc!= null && rsc instanceof SearchedVod) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.setResource] SearchedVod changed.");
			}
			disposeVodPoster();
			createVodPoster((SearchedVod)rsc);
		}
	}
	
	private void disposeVodPoster() {
	}
	
	private void createVodPoster(SearchedVod searchedVod) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[DetailInformationPanel.createVodPoster] Called.");
		}
		if (requestor != null) {
			requestor.setValidRequest(false);
			requestor = null;
		}
		requestor = new VodPosterRequestor(searchedVod.getContentImage());
		requestor.setValidRequest(true);
		requestor.start();
	}
	
	public class VodPosterRequestor extends Thread {
		private final String posterImageName;
		private boolean isValidRequest;
		
		public Image imgVodPoster;
		
		public VodPosterRequestor(String reqPosterImageName) {
			posterImageName = reqPosterImageName;
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.VodPosterRequestor.VodPosterRequestor] posterImageName: " + posterImageName);
			}
		}
		
		public void setValidRequest(boolean isValidRequest) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.VodPosterRequestor.setValidRequest] isValidRequest: " + isValidRequest);
			}
			this.isValidRequest = isValidRequest;
		}
		
		public void run() {
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.VodPosterRequestor.run] A-isValidRequest: " + isValidRequest);
			}
			if (!isValidRequest) {
				return;
			}
			Image img = SearchDataManager.getInstance().getVODPosterImage(posterImageName);
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.VodPosterRequestor.run] img: " + img);
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.VodPosterRequestor.run] B-isValidRequest: " + isValidRequest);
			}
			if (isValidRequest) {
				if (img != null) {
			        FrameworkMain.getInstance().getImagePool().waitForAll();
				}
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("[DetailInformationPanel.VodPosterRequestor.run] C-isValidRequest: " + isValidRequest);
			}
			if (isValidRequest) {
				if (img != null) {
			        imgVodPoster = img;
			        if (Log.DEBUG_ON) {
						Log.printDebug("[DetailInformationPanel.VodPosterRequestor.run] End of request");
					}
			        repaint();
				}
			}
		}
		
		public Image getPosterImage() {
			return imgVodPoster;
		}
	}
}

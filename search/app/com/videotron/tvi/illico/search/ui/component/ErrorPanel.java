/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.SearchDataManager;
import com.videotron.tvi.illico.search.data.SearchException;
import com.videotron.tvi.illico.search.gui.component.RendererErrorPanel;
import com.videotron.tvi.illico.search.ui.component.event.SelectorListener;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ErrorPanel</code>
 * 
 * @version $Id: ErrorPanel.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 14.
 * @description
 */
public class ErrorPanel extends SearchComponent {
	private static final long serialVersionUID = 1L;
	private SelectorListener listener;
	public String errorCode;
//	public String[] errorMsgs;
	public String itemMessage;
	public String detailTitle;
	public String[] detailDescs;

	protected void initComponent() {
		setRenderer(new RendererErrorPanel());
	}

	protected void disposeComponent() {
		detailDescs = null;
		detailTitle = null;
		itemMessage = null;
		errorCode = null;
		listener = null;
	}
	
	public void setException(SearchException reqException) {
		itemMessage = DataCenter.getInstance().getString("Descriptions.Unavailable_Source_Item_Title");
		detailTitle = DataCenter.getInstance().getString("Descriptions.Unavailable_Source_Detail_Title");
		String detailDesc = DataCenter.getInstance().getString("Descriptions.Unavailable_Source_Item_Title");
		detailDescs = TextUtil.split(detailDesc, Rs.FM16, 236);
		errorCode = reqException.getCode();
		String tempErrMsg = SearchDataManager.getInstance().getErrorMsg(errorCode);
		detailDescs = TextUtil.split(tempErrMsg, Rs.FM16, 236);
	}
	
	public void setNoResult() {
		itemMessage = DataCenter.getInstance().getString("Descriptions.No_Result_Item_Title");
		detailTitle = DataCenter.getInstance().getString("Descriptions.No_Result_Detail_Title");
		String detailDesc = DataCenter.getInstance().getString("Descriptions.No_Result_Detail_Desc");
		detailDescs = TextUtil.split(detailDesc, Rs.FM16, 236);
		errorCode = null;
	}
	
	public void setSelectorListener(SelectorListener listener) {
		this.listener = listener;
	}

	public void keyActionLeft() {
		// TODO Auto-generated method stub
	}

	public void keyActionRight() {
		// TODO Auto-generated method stub
	}

	public void keyActionUp() {
		// TODO Auto-generated method stub
		
	}

	public void keyActionDown() {
		// TODO Auto-generated method stub
		
	}

	public void keyActionOk() {
		clickEffectAction();
		if (listener != null) {
			listener.itemSelected(this, 0);
		}
	}
	
	private void clickEffectAction() {
		new ClickingEffect(this, 5).start(0, 0, 576, 52);
	}
}

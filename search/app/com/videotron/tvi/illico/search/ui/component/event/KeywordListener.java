/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component.event;

/**
 * <code>SearchKeywordListener</code>
 * 
 * @version $Id: KeywordListener.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 28.
 * @description
 */
public interface KeywordListener {
	public void selectedKeyword(boolean isInputboxFocused, String selectedSearchKeyword, int selectedSearchFilterId, boolean isSearchedLetters);
	public void changedComponent(boolean isInputboxFocused);
}

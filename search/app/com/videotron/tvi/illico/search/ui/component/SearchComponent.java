/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.search.ui.component.event.ComponentWatcher;

/**
 * <code>SearchComponent</code>
 * 
 * @version $Id: SearchComponent.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 26.
 * @description
 */
abstract public class SearchComponent extends UIComponent{
	private static final long serialVersionUID = 1L;
	
	private boolean isFocus = false;
	private boolean isSelectable = true;
	protected ComponentWatcher compWatcher;
	
	public void init() {
		initComponent();
		prepare();
	}
	
	public void dispose() {
		disposeComponent();
	}
	
	abstract protected void initComponent() ;
	
	abstract protected void disposeComponent();
	
//	abstract public void start() ;
//	
//	abstract public void stop();
	
	public void setComponentWatcher(ComponentWatcher reqCompWatcher) {
		compWatcher = reqCompWatcher;
	}
	
	public void removeComponentWatcher() {
		compWatcher = null;
	}
	
    public final boolean isFocused() {
        return isFocus;
    }
    
    public final void setFocus(boolean isFocus) {
        this.isFocus = isFocus;
        if (isFocus) {
            focused();
        } else {
            unfocused();
        }
    }
    
    protected void focused() {
    }
    
    protected void unfocused() {
    }
    
    public boolean isSelectable() {
        return isSelectable;
    }
    
    public void setSelectable(boolean isSelectable) {
        this.isSelectable = isSelectable;
    }
	
	abstract public void keyActionLeft() ;
	
	abstract public void keyActionRight();
	
	abstract public void keyActionUp() ;
	
	abstract public void keyActionDown() ;
	
	abstract public void keyActionOk() ;
	
	public void keyActionPageUp() {}
	public void keyActionPageDown() {}
}

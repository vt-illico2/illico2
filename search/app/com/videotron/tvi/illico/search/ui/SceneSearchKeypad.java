/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui;

import java.awt.Container;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.controller.SceneManager;
import com.videotron.tvi.illico.search.controller.SceneTemplate;
import com.videotron.tvi.illico.search.controller.SearchController;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.data.SearchKeypadDef;
import com.videotron.tvi.illico.search.data.SearchParameter;
import com.videotron.tvi.illico.search.gui.RendererSearchKeypad;
import com.videotron.tvi.illico.search.ui.component.SearchComponent;
import com.videotron.tvi.illico.search.ui.component.Filter;
import com.videotron.tvi.illico.search.ui.component.Keypad;
import com.videotron.tvi.illico.search.ui.component.InputBox;
import com.videotron.tvi.illico.search.ui.component.event.ComponentWatcher;
import com.videotron.tvi.illico.search.ui.component.event.FilterListener;
import com.videotron.tvi.illico.search.ui.component.event.KeypadListener;
import com.videotron.tvi.illico.search.ui.component.event.KeywordListener;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>SceneKeypad</code>
 * 
 * @version $Id: SceneSearchKeypad.java,v 1.4.2.1 2017/03/27 15:28:48 freelife Exp $
 * @author Administrator
 * @since 2015. 3. 25.
 * @description
 */
public class SceneSearchKeypad extends Scene {
	private static final long serialVersionUID = -7605747880603707343L;
	
	private Footer footer;
	private InputBox inputBox;
	private Filter filter;
	private Keypad keypad;
	
	private SearchComponent latestSearchComp, curSearchComp;
	
	private ComponentWatcherImpl compWatcherImpl;
	private FilterListenerImpl filterListenerImpl;
	private KeypadListenerImpl keypadListenerImpl;
	private InputBoxListenerImpl inputboxListenerImpl;
	
	public int selectedFilterId;
	public String selectedKeyword = "";
//	private boolean isReadyToClear = false;
	
	/* 
	 * Simple Back - Keypad screen returned back from result page.
	 * true: one character delete when user input letter at keypad.
	 * false: all character delete when user input letter at keypad.
	 *  */
	private boolean isSimpleBack;
	
//	private String lastedSearchedKeyword;
	
	protected void initScene() {
		setRenderer(new RendererSearchKeypad());
		initComponent();
	}
	
	private void initComponent() {
		compWatcherImpl = new ComponentWatcherImpl();
		
		/*
		 * Initialize SearchFilter
		 */
		filterListenerImpl = new FilterListenerImpl();
		filter = new Filter();
		filter.init();
		filter.setComponentWatcher(compWatcherImpl);
		filter.setSearchFilterListener(filterListenerImpl);
//		searchFilter.setBounds(0, 94, 960, 44);
		add(filter);
		
		/*
		 * Initialize SearchKeypad
		 */
		keypadListenerImpl = new KeypadListenerImpl();
		keypad = new Keypad();
		keypad.init();
		keypad.setComponentWatcher(compWatcherImpl);
		keypad.setSearchKeypadListener(keypadListenerImpl);
//		searchKeypad.setBounds(53, 172, 340, 244);
		add(keypad);
		
		/*
		 * Initialize SearchKeyword
		 */
		inputboxListenerImpl = new InputBoxListenerImpl();
		inputBox = new InputBox();
		inputBox.init();
		inputBox.setComponentWatcher(compWatcherImpl);
		inputBox.setSearchKeywordListener(inputboxListenerImpl);
//		inputBox.setBounds(444, 175, 480, 285);
		selectedFilterId = filter.getSearchFilterId();
		inputBox.setSearchFilterId(selectedFilterId);
		add(inputBox);
		
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
		changeComponent(keypad);
	}
	
	private void disposeComponent() {
		/*
		 * Dispose SearchFilter
		 */
		if (filter != null) {
			remove(filter);
			filter.dispose();
			filter = null;
		}
		
		/*
		 * Dispose SearchKeypad
		 */
		if (keypad != null) {
			remove(keypad);
			keypad.dispose();
			keypad = null;
		}
		
		/*
		 * Dispose SearchKeyword
		 */
		if (inputBox != null) {
			remove(inputBox);
			inputBox.dispose();
			inputBox = null;
		}
		
		/*
		 * Dispose Footer
		 */
        if (footer != null) {
            remove(footer);
            footer = null;
        }   
	}

	protected void disposeScene() {
		disposeComponent();
		renderer = null;
	}

	protected void startScene(boolean resetScene, Object param) {
		isSimpleBack = !resetScene;
		if (resetScene) {
			filter.reset();
			if (param != null && param instanceof SceneSearchKeypadMemento) {
				restoreMemento((SceneSearchKeypadMemento)param);
			}
		} else {
			SceneSearchKeypadMemento memento = (SceneSearchKeypadMemento)DataCenter.getInstance().get(Rs.DCKEY_SEARCH_MEMENTO);
			restoreMemento(memento);
		}
		
		if (isSimpleBack) {
			if (inputBox != null) {
				Log.printDebug("[SceneSearchKeypad.startScene] isSimpleBack " + isSimpleBack);
				inputBox.setBackStatus();
			}
		}			
	}
	
	protected void stopScene() {
		if (inputBox != null) {
			inputBox.stop();
		}
	}
	
	private void saveMemento(int reqFilterId, String reqKeyword) {
		SceneSearchKeypadMemento memento = new SceneSearchKeypadMemento();
		memento.setFilterId(reqFilterId);
		memento.setKeyword(reqKeyword);
		DataCenter.getInstance().put(Rs.DCKEY_SEARCH_MEMENTO, memento);
	}
	
	private void restoreMemento(SceneSearchKeypadMemento memento) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchKeypad.restoreMemento] Called.");
		}
		if (memento == null) {
			filter.reset();
			selectedFilterId = SearchFilterDef.ID_ALL;
			selectedKeyword = "";
			inputBox.setSearchFilterId(selectedFilterId);
			inputBox.setSearchText(selectedKeyword);
			changeComponent(keypad);
		} else {
			int filterId = memento.getFilterId();
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneSearchKeypad.restoreMemento] filterId: " + filterId +", Filter: " + SearchFilterDef.getSectionFilterText(filterId));
			}
			filter.setSelectedFilterId(filterId);
			String searchedKeyword = memento.getKeyword();
			selectedFilterId = filterId;
			selectedKeyword = searchedKeyword;
			inputBox.setSearchFilterId(selectedFilterId);
			inputBox.setSearchText(selectedKeyword);
			changeComponent(inputBox);
		}
	}
	
	protected boolean keyAction(int keyCode) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneKeypad.keyAction] keyCode: " + keyCode);
		}
		switch(keyCode) {
			case KeyCodes.SEARCH:
				SearchController.getInstance().stopSearchClient();
				return true;
			case Rs.KEY_LEFT:
				curSearchComp.keyActionLeft();
				return true;
			case Rs.KEY_RIGHT:
				curSearchComp.keyActionRight();
				return true;
			case Rs.KEY_UP:
				curSearchComp.keyActionUp();
				return true;
			case Rs.KEY_DOWN:
				curSearchComp.keyActionDown();
				return true;
			case Rs.KEY_OK:
				curSearchComp.keyActionOk();
				return true;
			case KeyCodes.COLOR_A:
                if (footer != null) {
                    footer.clickAnimation("Label.Footer_Filter");
                }
                if (filter.isFocused()) {
                	curSearchComp.keyActionRight();
                } else {
                	changeComponent(filter);                	
                }
                return true;
			case KeyCodes.COLOR_B:
                if (footer != null) {
                    footer.clickAnimation("Label.Footer_Space");
                }
                if (!isValidKey(keyCode)) {
                	return true;
                }
                addKeypadIdValue(SearchKeypadDef.ID_SPACE);
                inputBox.setSearchText(selectedKeyword);
                return true;
			case KeyCodes.COLOR_C:
                if (footer != null) {
                    footer.clickAnimation("Label.Footer_Delete");
                    footer.clickAnimation("Label.Footer_Delete_All");
                }
                if (!isValidKey(keyCode)) {
                	return true;
                }
                deleteKeyword();
                inputBox.setSearchText(selectedKeyword);
                return true;
                
			case Rs.KEY_EXIT:
                if (footer != null) {
                    footer.clickAnimation("Label.Footer_Exit");
                }
                SearchController.getInstance().stopSearchClient();
                return true;
		}
		return false;
	}
	
	private boolean isValidKey(int reqKeyCode) {
		boolean isValid = false;
		
		if (curSearchComp == null) {
			return false;
		}
		
		if (curSearchComp.equals(filter)) {
			switch(reqKeyCode){
				case KeyCodes.COLOR_B:
					isValid = true;
					break;
				case KeyCodes.COLOR_C:
					isValid = true;
					break;
			}
		} else if (curSearchComp.equals(keypad)) {
			switch(reqKeyCode){
				case KeyCodes.COLOR_B:
					isValid = true;
					break;
				case KeyCodes.COLOR_C:
					isValid = true;
					break;
			}
		} else if (curSearchComp.equals(inputBox)) {
			switch(reqKeyCode){
				case KeyCodes.COLOR_B:
					isValid = false;
					break;
				case KeyCodes.COLOR_C:
					isValid = inputBox.isInputboxFocused;
					break;
			}
		}
		return isValid;
	}
	
	private void changeComponent(SearchComponent reqComp) {
		if (reqComp == null || !reqComp.isSelectable()) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneKeypad.changeComponent] Request component is invalid. so it does not change request component.");
			}
			return;
		}
		if (curSearchComp != null) {
			curSearchComp.setFocus(false);
		}
		latestSearchComp = curSearchComp;
		curSearchComp = reqComp;
		curSearchComp.setFocus(true);
		if (reqComp.equals(keypad)) {
			keypad.reset();
		}
		
		if (reqComp.equals(inputBox)) {
			footer.reset();
	        footer.addButton(PreferenceService.BTN_A, "Label.Footer_Filter");
	        footer.addButton(PreferenceService.BTN_C, "Label.Footer_Delete_All");
	        footer.addButton(PreferenceService.BTN_EXIT, "Label.Footer_Exit");
	        
	        inputBox.setReadyToClear(true);
		} else {
			footer.reset();
	        footer.addButton(PreferenceService.BTN_A, "Label.Footer_Filter");
	        footer.addButton(PreferenceService.BTN_B, "Label.Footer_Space");
	        footer.addButton(PreferenceService.BTN_C, "Label.Footer_Delete");
	        footer.addButton(PreferenceService.BTN_EXIT, "Label.Footer_Exit");
	        
	        inputBox.setReadyToClear(false);
		}
		repaint();
	}
	
	private void deleteKeyword() {
		setReadyToClear();
		if (selectedKeyword == null || selectedKeyword.length() == 0) {
			return;
		}
		selectedKeyword = selectedKeyword.substring(0, selectedKeyword.length() - 1);
	}
	
	private void setReadyToClear() {
		if (inputBox == null) {
			return;
		}
		boolean isReadyToClear = inputBox.isReadyToClear();
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneSearchKeypad.setReadyToClear] isReadyToClear: " + isReadyToClear);
		}
		if (isReadyToClear) {
			selectedKeyword = "";
		}
	}
	
	private void addKeypadIdValue(int reqKeypadId) {
		String selectedKeypadValue = SearchKeypadDef.getValue(reqKeypadId);
		setReadyToClear();
		if (selectedKeyword == null) {
			selectedKeyword = selectedKeypadValue;
		} else {
			selectedKeyword += selectedKeypadValue;
		}
	}
	
	private void requestSearch(String reqSearchKeyword, int reqSearchFilterId, boolean isSearchedLetters) {
		boolean isReadyToSearch = isReadyToSearch(reqSearchKeyword, reqSearchFilterId);
		if (!isReadyToSearch) {
			return;
		}
		saveMemento(reqSearchFilterId, reqSearchKeyword);
		SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_RESULT, true, new SearchParameter(reqSearchKeyword, reqSearchFilterId, isSearchedLetters));
	}
	
	private boolean isReadyToSearch(String reqSearchKeyword, int reqSearchFilterId) {
		if (reqSearchKeyword == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneKeypad.isReadyToSearch] Search Keyword is invalid (null). return false;");
			}
			return false;
		}
		
		if (reqSearchKeyword.trim().length() < 1) { //R7.3 Search
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneKeypad.isReadyToSearch] Search Keyword is invalid (valid length is under 2 character or less). return false;");
			}
			return false;
		}
		
		if (reqSearchFilterId == SearchFilterDef.ID_INVALID) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[SceneKeypad.isReadyToSearch] Search Filter ID is invalid. return false;");
			}
			return false;
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("[SceneKeypad.isReadyToSearch] ready to search.");
		}
		return true;
	}
	
	class ComponentWatcherImpl implements ComponentWatcher {
		public void goOverUp(Container comp) {
			if (comp.equals(filter)) {
				// Nothing to do.
			} else if (comp.equals(keypad)) {
				changeComponent(filter);
			} else if (comp.equals(inputBox)) {
				changeComponent(filter);
			}
		}

		public void goOverDown(Container comp) {
			if (comp.equals(filter)) {
				if (latestSearchComp != null && !latestSearchComp.isSelectable()) {
					latestSearchComp = keypad;
				}
				changeComponent(latestSearchComp);
			} else if (comp.equals(keypad)) {
				// Nothing to do.
			} else if (comp.equals(inputBox)) {
				// Nothing to do.
			}
		}

		public void goOverLeft(Container comp) {
			if (comp.equals(filter)) {
				
			} else if (comp.equals(keypad)) {
				
			} else if (comp.equals(inputBox)) {
				changeComponent(keypad);
			}
		}
 
		public void goOverRight(Container comp) {
			if (comp.equals(filter)) {
				
			} else if (comp.equals(keypad)) {
				changeComponent(inputBox);
			} else if (comp.equals(inputBox)) {
				
			}
		}
	}
	
	class FilterListenerImpl implements FilterListener {
		public void changedSelectedItem(int filterId) {
			selectedFilterId = filterId;
			inputBox.setSearchFilterId(selectedFilterId);
		}
	}
	
	class KeypadListenerImpl implements KeypadListener {
		public void selectedKeypad(int selectedKeypadId) {
			switch(selectedKeypadId) {
				case SearchKeypadDef.ID_DELETE:
					deleteKeyword();
					break;
				default:
					addKeypadIdValue(selectedKeypadId);
					break;
			}
			inputBox.setSearchText(selectedKeyword);
		}
	}
	
	class InputBoxListenerImpl implements KeywordListener {
		
		public void selectedKeyword(boolean isInputboxFocused, String selectedSearchKeyword, int selectedSearchFilterId, boolean isSearchedLetters) {
//			if (isInputboxFocused) {
//				requestSearch(selectedSearchKeyword, selectedSearchFilterId, isSearchedLetters);
//			} else {
//				selectedKeyword = selectedSearchKeyword;
//				inputBox.setSearchText(selectedKeyword);
//			}
			requestSearch(selectedSearchKeyword, selectedSearchFilterId, isSearchedLetters);
		}

		public void changedComponent(boolean isInputboxFocused) {
			if (curSearchComp == null || !curSearchComp.equals(inputBox)) {
				return;
			}
			
			if (isInputboxFocused) {
				footer.reset();
		        footer.addButton(PreferenceService.BTN_A, "Label.Footer_Filter");
		        footer.addButton(PreferenceService.BTN_C, "Label.Footer_Delete_All");
		        footer.addButton(PreferenceService.BTN_EXIT, "Label.Footer_Exit");
		        
		        inputBox.setReadyToClear(true);
			} else {
				footer.reset();
		        footer.addButton(PreferenceService.BTN_A, "Label.Footer_Filter");
		        footer.addButton(PreferenceService.BTN_EXIT, "Label.Footer_Exit");
		        
		        inputBox.setReadyToClear(false);
			}
			repaint();
		}

//		public void selectedKeyword(String reqSearchKeyword, int reqSearchFilterId, boolean isSearchedLetters) {
//			requestSearch(reqSearchKeyword, reqSearchFilterId, isSearchedLetters);
//		}
	}
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component.event;

import com.videotron.tvi.illico.search.ui.component.SearchComponent;

/**
 * <code>ResultListListener</code>
 * 
 * @version $Id: SelectorListener.java,v 1.4 2017/01/09 20:46:11 zestyman Exp $
 * @author Administrator
 * @since 2015. 4. 2.
 * @description
 */
public interface SelectorListener {
	void itemChanged(SearchComponent comp, int reqIdx);
	void itemSelected(SearchComponent comp, int reqIdx);
}

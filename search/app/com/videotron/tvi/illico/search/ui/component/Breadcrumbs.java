package com.videotron.tvi.illico.search.ui.component;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.SharedMemory;

public class Breadcrumbs extends Container {
    private static final long serialVersionUID = 1L;
    private final Color C236236237 = new Color(236, 236, 237);
    private final Font F18 = FontResource.BLENDER.getFont(18, true);
    private final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    
    private final int gap = 11;
    private Image imgHisOver;
    
    private String[] breadCrumbsTxts;
    
    public Breadcrumbs() {
        imgHisOver = DataCenter.getInstance().getImage("his_over.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public void dispose() {
        imgHisOver = null;
    }
    public void start(String[] breadCrumbsTxts) {
        this.breadCrumbsTxts = breadCrumbsTxts;
    }
    public void stop() {
        breadCrumbsTxts = null;
    }
    public void paint(Graphics g) {
        Image imgLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        int startX = 0;
        if(imgLogo != null){
            g.drawImage(imgLogo, 0, 0, this);
            startX = imgLogo.getWidth(this);
            startX += gap;
        }
        if (breadCrumbsTxts != null) {
            g.setFont(F18);
            int breadCrumbsTxtsLth = breadCrumbsTxts.length;
            for (int i=0; i < breadCrumbsTxtsLth; i++) {
                if (i !=0 && i!= breadCrumbsTxtsLth-1) {
                    continue;
                }
                Image imgHis = imgHisOver;
                g.setColor(C236236237);
                if (imgHis != null) {
                    int imgHisWth = imgHis.getWidth(this);
                    g.drawImage(imgHis, startX, 24, this);
                    startX+=imgHisWth;
                    startX+=gap;
                }
                if (breadCrumbsTxts[i] != null) {
                    g.drawString(breadCrumbsTxts[i], startX, 36);
                    startX += FM18.stringWidth(breadCrumbsTxts[i]);
                    startX += gap;
                }
            }
        }
    }
}

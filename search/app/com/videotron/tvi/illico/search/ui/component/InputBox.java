/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.Util;
import com.videotron.tvi.illico.search.controller.VbmController;
import com.videotron.tvi.illico.search.data.SearchDataManager;
import com.videotron.tvi.illico.search.data.SearchException;
import com.videotron.tvi.illico.search.data.SearchFilterDef;
import com.videotron.tvi.illico.search.data.resource.SuggestedKeyword;
import com.videotron.tvi.illico.search.gui.component.RendererInputBox;
import com.videotron.tvi.illico.search.ui.component.event.KeywordListener;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>SearchKeyword</code>
 * 
 * @version $Id: InputBox.java,v 1.4.2.1 2017/03/27 15:28:48 freelife Exp $
 * @author Administrator
 * @since 2015. 3. 25.
 * @description
 */
public class InputBox extends SearchComponent {
	private static final long serialVersionUID = 1L;
	
	private int searchFilterId = SearchFilterDef.ID_INVALID;
	public String searchText;
	
	public boolean isOverInputboxWidth;
	public String searchDisplayText;
	public int searchDisplayTextW;
	
	public String[] suggestedKeywords;
	public String[] suggestedShortenKeywords;
	public SearchException exception;
	private SuggestedKeywordRequestor requestor;
	
	public boolean isInputboxFocused;
	public int suggestedKeywordIdx;
	
	private KeywordListener searchKeywordListener;
	
	public String requestState;
	public boolean fromSearchKeyword = false; //R7.3
	private String searchSuggestedKeyword = null; //R7.3
	
	private boolean isReadyToClear;
	
	protected void initComponent() {
		setRenderer(new RendererInputBox());
		setResource();
		reset();
	}
	
	public void stop() {
		reset();
		exception = null;
		searchText = null;
	}

	private void setResource() {
	}
	
	private void setInputboxFocused(boolean reqValue) {
		if (isInputboxFocused == reqValue) {
			return;
		}
		isInputboxFocused = reqValue;
		if (searchKeywordListener != null) {
			searchKeywordListener.changedComponent(isInputboxFocused);
		}
	}
	
	private void reset() {
		Log.printDebug("[InputBox.reset] called ");
		fromSearchKeyword = false;
		exception = null;
		suggestedKeywords = null;
		suggestedShortenKeywords = null;
		setInputboxFocused(true);
		checkSelectable();
	}

	protected void disposeComponent() {
		fromSearchKeyword = false;
		searchText = null;
		searchDisplayText = null;
		suggestedKeywords = null;
		suggestedShortenKeywords = null;
		exception = null;
		requestor = null;
		searchKeywordListener = null;
	}
	
	protected void focused() {
		setInputboxFocused(true);
		suggestedKeywordIdx = 0;
	}

	public void keyActionLeft() {
		if (compWatcher != null) {
			compWatcher.goOverLeft(this);
		}
	}

	public void keyActionRight() {
		// Nothing to do.
	}

	public void keyActionUp() {
		if (isInputboxFocused) {
			if (compWatcher != null) {
				compWatcher.goOverUp(this);
			}
			return;
		}
		if (!isExistSuggestedKeywords()) {
			return;
		}
		int tempIdx = suggestedKeywordIdx - 1;
		if (tempIdx < 0) {
			setInputboxFocused(true);
		} else {
			suggestedKeywordIdx = tempIdx;
		}
		repaint();
	}

	public void keyActionDown() {
		if (!isExistSuggestedKeywords()) {
			return;
		}
		
		if (isInputboxFocused) {
			setInputboxFocused(false);
			suggestedKeywordIdx = 0;
		} else {
			int tempIdx = suggestedKeywordIdx + 1;
			if (tempIdx >= suggestedKeywords.length || tempIdx >= 5) {
				return;
			}
			suggestedKeywordIdx = tempIdx;
		}
		repaint();
	}

	public void keyActionOk() {
		clickEffectAction();
		String selectedSearchKeyword = null;
		boolean isSearchedLetters = false;
		if (isInputboxFocused) {
			selectedSearchKeyword = searchText;
			isSearchedLetters = true;
			searchSuggestedKeyword = "";
		} else {
			if (suggestedKeywords != null && suggestedKeywordIdx < suggestedKeywords.length) {
				selectedSearchKeyword = suggestedKeywords[suggestedKeywordIdx];
				isSearchedLetters = false;				
			}
		}
		
		if (searchKeywordListener != null) {
			selectedSearchKeyword = Util.getStringExceptString(selectedSearchKeyword, Rs.TAG_EM_START);
			selectedSearchKeyword = Util.getStringExceptString(selectedSearchKeyword, Rs.TAG_EM_END);
			searchKeywordListener.selectedKeyword(isInputboxFocused, selectedSearchKeyword, searchFilterId, isSearchedLetters);
		}
		
		if (!isSearchedLetters)
			searchSuggestedKeyword = selectedSearchKeyword;
	}
	
	public void setBackStatus() {
		Log.printDebug("[InputBox.setBackStatus] searchText " + searchText);
		Log.printDebug("[InputBox.setBackStatus] searchSuggestedKeyword " + searchSuggestedKeyword);
		if (searchText == searchSuggestedKeyword) {						
			fromSearchKeyword = true;
			Log.printDebug("[InputBox.setBackStatus] fromSearchKeyword " + fromSearchKeyword);
			return;			
		}			
		fromSearchKeyword = false;		
	}
	
	private void clickEffectAction() {
		if (isInputboxFocused) {
			new ClickingEffect(this, 5).start(0, 0, 465, 38);
		} else {
			new ClickingEffect(this, 5).start(0, 39 + (40 * suggestedKeywordIdx), 465, 41);
		}
	}
	
	public void setSearchKeywordListener(KeywordListener reqSearchKeywordListener) {
		searchKeywordListener = reqSearchKeywordListener;
	}
	
	public void removeSearchKeywordListener() {
		searchKeywordListener = null;
	}
	
	private boolean isExistSuggestedKeywords() {
		boolean isExistSuggestedKeywords = suggestedKeywords != null && suggestedKeywords.length > 0;
		return isExistSuggestedKeywords;
	}
	
	public void setSearchFilterId(int reqSearchFilterId) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[InputBox.setSearchFilterId] Called. Requested Filter is " + SearchFilterDef.getSectionFilterText(reqSearchFilterId));
		}
		if (reqSearchFilterId == searchFilterId) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.setSearchFilterId] Requested search filter id is same as current search filter id. so return.");
			}
			return;
		}
		reset();
		repaint();
		
		searchFilterId = reqSearchFilterId;
		if (isAvaliableRequest()) {
			request();
		}
	}
	
//	public void setSearchText(String reqSearchText) {
//		if (Log.DEBUG_ON) {
//			Log.printDebug("[InputBox.setSearchText] Called. Request Text is " + reqSearchText);
//		}
//		if (reqSearchText != null && searchText != null && reqSearchText.equals(searchText)) {
//			if (Log.DEBUG_ON) {
//				Log.printDebug("[InputBox.setSearchText] Requested search text is same as current search test. so return.");
//			}
//			return;
//		}
//		isReadyToClear = false;
//		/*
//		 * If user input space, isSimilarSearchText valiable is not allow repaint
//		 */
//		boolean isSimilarSearchText = (reqSearchText != null && searchText != null && reqSearchText.trim().equals(searchText.trim()));
//		if (!isSimilarSearchText) {
//			reset();
//		}
//		searchText = reqSearchText;
//		makeDisplaySearchText(searchText);
//		checkSelectable();
//		repaint();
//		if (!isSimilarSearchText && isAvaliableRequest()) {
//			request();
//		}
//	}
	
	public void setSearchText(String reqSearchText) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[InputBox.setSearchText] Called. Request Text is " + reqSearchText);
		}
		reset();
		searchText = reqSearchText;
		fromSearchKeyword = false;
		makeDisplaySearchText(searchText);
		checkSelectable();
		repaint();
		if (isAvaliableRequest()) {
			request();
		}
	}
	
	public void setReadyToClear(boolean isReadyToClear) {
		this.isReadyToClear = isReadyToClear;
	}
	
	public boolean isReadyToClear() {
		return isReadyToClear;
	}
	
	private void makeDisplaySearchText(String reqSearchText) {
		int reqSearchTextW = 0;
		if (reqSearchText != null) {
			reqSearchTextW = Rs.FM22.stringWidth(reqSearchText);			
		}
		if (reqSearchTextW > 240) {
			isOverInputboxWidth = true;
			searchDisplayText = Util.shortenR(reqSearchText, Rs.FM22, 240);
			searchDisplayTextW = Rs.FM22.stringWidth(searchDisplayText);
		} else {
			isOverInputboxWidth = false;
			searchDisplayText = reqSearchText;
			searchDisplayTextW = reqSearchTextW;
		}
	}
	
	private void checkSelectable() {
		if (searchText != null && searchText.trim().length() >= 1) {
			setSelectable(true);
		} else {
			setSelectable(false);
			if(isFocused()) {
				if (compWatcher != null) {
					compWatcher.goOverLeft(this);
				}
			}
		}
	}
	
	synchronized private void resetInputBox() {		
//		isInputboxFocused = true;
//		repaint();
//		String reqSearchText = requestor.getSearchText();
//		int reqSearchFilterId = requestor.getSearchFilterId();
//		requestor = null;
//		// check next request.
//		if (hasNextRequest(reqSearchText, reqSearchFilterId)) {
//			request();
//		} else {
//			requestor = null;
//		}
		requestor = null;
		setInputboxFocused(true);
		repaint();
	}
	
	private boolean hasNextRequest(String reqSearchText, int reqSearchFilterId) {
		// searchText is null. it does not request.
		if (!isAvaliableRequest()) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.hasNextRequest] Requestor is not available.");
			}
			return false;
		}
		
		if (reqSearchText.equals(searchText) && reqSearchFilterId == searchFilterId) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.hasNextRequest] Requested data is same as current data.");
			}
			return false;
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("[InputBox.hasNextRequest] has next request.");
		}
		return true;
	}
	
	private boolean isAvaliableRequest() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[InputBox.isAvaliableRequest] Called.");
		}
		if (searchFilterId == SearchFilterDef.ID_INVALID) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.isAvaliableRequest] SearchFilterId does not set.");
			}
			return false;
		}
		
		if (searchText == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.isAvaliableRequest] SearchText does not set.");
			}
			return false;
		}
		
		if (searchText.trim().length() < 2) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.isAvaliableRequest] SearchText is not satisfactory for length.");
			}
			return false;
		}
		
//		if (requestor != null) {
//			if (Log.DEBUG_ON) {
//				Log.printDebug("[SearchKeyword.isAvaliableRequest] current SuggestedKeywordRequestor is working.");
//			}
//			return false;
//		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[InputBox.isAvaliableRequest] isAvaliableRequest is true");
		}
		return true;
	}
	
	private void request() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[InputBox.request] Called.");
		}
		if (requestor != null) {
			requestor.setValidRequest(false);
			requestor = null;
		}
		requestor = new SuggestedKeywordRequestor(searchText, searchFilterId);
		requestor.setValidRequest(true);
		requestor.start();
	}
	
	class SuggestedKeywordRequestor extends Thread {
		private final String pSearchText;
		private final int pSearchFilterId;
		private final String requestorId;
		
		private boolean isValidRequest;
		
		public SuggestedKeywordRequestor(String reqSearchText, int reqSearchFilterId) {
			pSearchText = reqSearchText;
			pSearchFilterId = reqSearchFilterId;
			requestorId = pSearchText +"_" + SearchFilterDef.getSectionFilterText(pSearchFilterId);
		}
		
		public void setValidRequest(boolean isValidRequest) {
			this.isValidRequest = isValidRequest;
		}
		
		public void run() {
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.SuggestedKeywordRequestor.run]["+requestorId+"] Start Request.");
			}
			if (Log.EXTRA_ON) {
				requestState = "["+requestorId+"] start.";
				repaint();
			}
			String sessionId = VbmController.getInstance().writeKeywordSearchQueryStart();
			Object obj = null;
			if (!isValidRequest) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[InputBox.SuggestedKeywordRequestor.run]["+requestorId+"] This request is canceled. Process 1");
				}
				if (Log.EXTRA_ON) {
					requestState = "["+requestorId+"] is canceled. before requesting to server.";
					repaint();
				}
				return;
			}
			try {
				obj = SearchDataManager.getInstance().getSuggestedKeyword(searchText, searchFilterId);
			} catch (SearchException e) {
				e.printStackTrace();
				obj = e;
			}
			if (!isValidRequest) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[InputBox.SuggestedKeywordRequestor.run]["+requestorId+"] This request is canceled. Process 2");
				}
				if (Log.EXTRA_ON) {
					requestState = "["+requestorId+"] is canceled. before drawing to inputbox.";
					repaint();
				}
				return;
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("[InputBox.SuggestedKeywordRequestor.run] Object: " + obj);
			}
			if (obj instanceof SuggestedKeyword) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[InputBox.SuggestedKeywordRequestor.run]SuggestedKeyword");
				}
				if (Log.EXTRA_ON) {
					requestState = "["+requestorId+"] is complete. got from server.";
					repaint();
				}
				SuggestedKeyword latestSuggestedKeyword = (SuggestedKeyword) obj;
				if (latestSuggestedKeyword != null) {
					exception = null;
					suggestedKeywords = latestSuggestedKeyword.getSuggestedKeywords();
					suggestedShortenKeywords = new String[suggestedKeywords.length];
					
					if (suggestedKeywords != null && suggestedKeywords.length > 0) {
						for (int i=0; i<suggestedKeywords.length; i++) {
							suggestedShortenKeywords[i] = suggestedKeywords[i];
							suggestedShortenKeywords[i] = Util.getStringExceptString(suggestedShortenKeywords[i], Rs.TAG_EM_START);
							suggestedShortenKeywords[i] = Util.getStringExceptString(suggestedShortenKeywords[i], Rs.TAG_EM_END);
							suggestedShortenKeywords[i] = TextUtil.shorten(suggestedShortenKeywords[i], Rs.FM19, 245);
							
						}
					}
					if (Log.DEBUG_ON) {
						Log.printDebug("[InputBox.SuggestedKeywordRequestor.run]["+requestorId+"] This request is completed.");
					}
					resetInputBox();
				}
			} else if (obj instanceof SearchException) {
				suggestedKeywords = null;
				suggestedShortenKeywords = null;
				exception = (SearchException)obj;
				if (Log.EXTRA_ON) {
					requestState = "["+requestorId+"] is complete. but occured SearchException. Code:" + exception.getCode() +", message: "+exception.getMessage();
					repaint();
				}
				resetInputBox();
			}
			VbmController.getInstance().writeKeywordSearchQueryEnd(sessionId);
		}
		
		public String getSearchText() {
			return pSearchText;
		}
		
		public int getSearchFilterId() {
			return pSearchFilterId;
		}
	}
}

/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import java.util.Vector;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.TabResultDef;
import com.videotron.tvi.illico.search.gui.component.RendererTabResult;
import com.videotron.tvi.illico.search.ui.component.event.TabResultListener;

/**
 * <code>SearchResultTab</code>
 * 
 * @version $Id: TabResult.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 28.
 * @description
 */
public class TabResult extends SearchComponent{
	private static final long serialVersionUID = 1L;
	
	private static final int[] TAB_RESULT_PRIORITY = {
		TabResultDef.ID_LIVE,
		TabResultDef.ID_ON_DEMAND,
		TabResultDef.ID_RECORDINGS,
		TabResultDef.ID_PERSON
	};

	public Vector tabResultVec;
	
	public int curTabResultIdx;
	public int curTabResultId;
	
	private TabResultListener searchResultTabListener;
	
	protected void initComponent() {
		setRenderer(new RendererTabResult());
		initResource();
	}
	
	private void initResource() {
		tabResultVec = new Vector();
	}

	protected void disposeComponent() {
		// TODO Auto-generated method stub
		
	}

	public void keyActionLeft() {
		int tabResultVecSize = tabResultVec.size();
		if (tabResultVecSize > 0) {
			int tempIdx = (tabResultVec.size() + curTabResultIdx - 1) % tabResultVec.size();
			if (tempIdx != curTabResultIdx) {
				curTabResultIdx = tempIdx;
				repaint();
				if (searchResultTabListener != null) {
					TabResultInfo trInfo = (TabResultInfo)tabResultVec.get(curTabResultIdx);
					searchResultTabListener.changedSearchResultTab(trInfo.getTabResultId());
				}
			}
		}
	}

	public void keyActionRight() {
		int tabResultVecSize = tabResultVec.size();
		if (tabResultVecSize > 0) {
			int tempIdx = (tabResultVec.size() + curTabResultIdx + 1) % tabResultVec.size();
			if (tempIdx != curTabResultIdx) {
				curTabResultIdx = tempIdx;
				repaint();
				if (searchResultTabListener != null) {
					TabResultInfo trInfo = (TabResultInfo)tabResultVec.get(curTabResultIdx);
					searchResultTabListener.changedSearchResultTab(trInfo.getTabResultId());
				}
			}
		}
	}

	public void keyActionUp() {
		// Nothing to do.
	}

	public void keyActionDown() {
		// Nothing to do.
	}

	public void keyActionOk() {
		// TODO Auto-generated method stub
		
	}
	
	public void setSearchResultTabListener(TabResultListener reqSearchResultTabListener) {
		searchResultTabListener = reqSearchResultTabListener;
	}
	
	public void removeSearchFilterListener() {
		searchResultTabListener = null;
	}
	
	public void resetTabResult() {
		tabResultVec.clear();
	}
	
	synchronized public void setTabResult(int reqTabResultId) {
		int tabResultIdx = getTabResultIndex(reqTabResultId);
		if (tabResultIdx != -1) {
			curTabResultId = reqTabResultId;
			curTabResultIdx = tabResultIdx;
		}
	}
	
	private int getTabResultIndex(int reqTabResultId) {
		int tabResultVecSize = tabResultVec.size();
		for (int i = 0; i < tabResultVecSize; i++) {
			TabResultInfo info = (TabResultInfo)tabResultVec.get(i);
			if (info.getTabResultId() == reqTabResultId){
				return i;
			}
		}
		return -1;
	}
	
	public boolean existTabResult(int reqTabResultId) {
		int tabResultVecSize = tabResultVec.size();
		for (int i = 0; i < tabResultVecSize; i++) {
			TabResultInfo info = (TabResultInfo)tabResultVec.get(i);
			if (info.getTabResultId() == reqTabResultId){
				return true;
			}
		}
		return false;
	}
	
	synchronized public void addTabResult(int reqTabResultId, int reqTabResultCount) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchTabResult.addTabResult] Called.");
		}
		int reqTabResultPriority = getTabResultPriorityIndex(reqTabResultId);
		int tabResultVecSize = tabResultVec.size();
		
		int reqIdx = tabResultVecSize;
		for (int i = 0 ; i<tabResultVecSize; i++) {
			TabResultInfo info = (TabResultInfo)tabResultVec.get(i);
			int infoPriority = getTabResultPriorityIndex(info.getTabResultId());
			if (Log.DEBUG_ON) {
				Log.printDebug("[SearchTabResult.addTabResult] infoPriority-reqTabResultPriority: " + infoPriority +" - " + reqTabResultPriority);
			}
			if (reqTabResultPriority <= infoPriority) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SearchTabResult.addTabResult] "+i+" is sutable index.");
				}
				reqIdx = i;
				break;
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchTabResult.addTabResult] reqIdx: " + reqIdx);
		}
		tabResultVec.add(reqIdx, new TabResultInfo(reqTabResultId, reqTabResultCount));
		setTabResult(curTabResultId);
		repaint();
	}
	
	public void changeTabResoultTotalCount(int reqTabResultId, int reqTabResultCount) {
		int tabResultVecSize = tabResultVec.size();
		
		int targetIdx = -1;
		for (int i=0; i<tabResultVecSize; i++) {
			TabResultInfo tabResultInfo = (TabResultInfo)tabResultVec.get(i);
			if (tabResultInfo != null && tabResultInfo.getTabResultId() == reqTabResultId) {
				targetIdx = i;
				break;
			}
		}
		
		if (targetIdx > 0) {
			tabResultVec.set(targetIdx, new TabResultInfo(reqTabResultId, reqTabResultCount));
		}
		repaint();
	}
	
	private int getTabResultPriorityIndex(int reqTabResultId) {
		int idx = -1;
		for (int i=0; i<TAB_RESULT_PRIORITY.length; i++) {
			if (reqTabResultId == TAB_RESULT_PRIORITY[i]) {
				idx = i;
				break;
			}
		}
		return idx;
	}
	
	public class TabResultInfo {
		private final int tabResultId;
		private final int tabResultCount;
		private final String tabResultTxt;
		private final String tabResultDisplayTxt;
		private final int tabResultDisplayTxtW;
		
		public TabResultInfo(int reqTabResultId, int reqTabResultCount) {
			tabResultId = reqTabResultId;
			tabResultCount = reqTabResultCount;
			tabResultTxt = TabResultDef.getTabResultText(tabResultId);
			tabResultDisplayTxt = tabResultTxt + "  ("+ tabResultCount + ")";
			tabResultDisplayTxtW = Rs.FM19.stringWidth(tabResultDisplayTxt);
		}

		public int getTabResultId() {
			return tabResultId;
		}

		public int getTabResultCount() {
			return tabResultCount;
		}

		public String getTabResultTxt() {
			return tabResultTxt;
		}
		
		public String getTabResultDisplayText() {
			return tabResultDisplayTxt;
		}
		
		public int getTabResultDisplayTextWidth() {
			return tabResultDisplayTxtW;
		}
	}

}

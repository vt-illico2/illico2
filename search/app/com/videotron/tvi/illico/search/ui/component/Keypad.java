/*
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.search.ui.component;

import java.awt.Point;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.search.Rs;
import com.videotron.tvi.illico.search.data.SearchKeypadDef;
import com.videotron.tvi.illico.search.gui.component.RendererKeypad;
import com.videotron.tvi.illico.search.ui.component.event.KeypadListener;

/**
 * <code>SearchKeypad</code>
 * 
 * @version $Id: Keypad.java,v 1.4 2017/01/09 20:46:10 zestyman Exp $
 * @author Administrator
 * @since 2015. 3. 25.
 * @description
 */
public class Keypad extends SearchComponent {
	private static final long serialVersionUID = 1L;
	
	public static final int H_GAP = 48;
	public static final int V_GAP = 40;
	
	public static final int[][] KEYPAD_DATA = { 
		{ 
			SearchKeypadDef.ID_A, SearchKeypadDef.ID_B, SearchKeypadDef.ID_C, SearchKeypadDef.ID_D,
			SearchKeypadDef.ID_E, SearchKeypadDef.ID_F, SearchKeypadDef.ID_G 
		}, 
		{ 
			SearchKeypadDef.ID_H, SearchKeypadDef.ID_I, SearchKeypadDef.ID_J, SearchKeypadDef.ID_K, 
			SearchKeypadDef.ID_L, SearchKeypadDef.ID_M, SearchKeypadDef.ID_N
		},
		{ 
			SearchKeypadDef.ID_O, SearchKeypadDef.ID_P, SearchKeypadDef.ID_Q, SearchKeypadDef.ID_R, 
			SearchKeypadDef.ID_S, SearchKeypadDef.ID_T, SearchKeypadDef.ID_U 
		}, 
		{ 
			SearchKeypadDef.ID_V, SearchKeypadDef.ID_W, SearchKeypadDef.ID_X, SearchKeypadDef.ID_Y, 
			SearchKeypadDef.ID_Z, SearchKeypadDef.ID_APOSTROPHE, SearchKeypadDef.ID_DASH 
		}, 
		{ 
			SearchKeypadDef.ID_1, SearchKeypadDef.ID_2, SearchKeypadDef.ID_3, 
			SearchKeypadDef.ID_4, SearchKeypadDef.ID_5, SearchKeypadDef.ID_SPACE 
		},
		{ 
			SearchKeypadDef.ID_6, SearchKeypadDef.ID_7, SearchKeypadDef.ID_8, 
			SearchKeypadDef.ID_9, SearchKeypadDef.ID_0, SearchKeypadDef.ID_DELETE
		}, 
	};
	public String[][] keypadDataTxts;
	public int[][] keypadDataW;
	
	private Point latestPoint;
	public Point curPoint; //x is row index, y is column index

	private KeypadListener searchKeypadListener;

	protected void initComponent() {
		setRenderer(new RendererKeypad());
		setResource();
		reset();
	}

	protected void disposeComponent() {
		keypadDataTxts = null;
		keypadDataW = null;
		
		latestPoint = null;
		curPoint = null;
		searchKeypadListener = null;
	}
	
	public void reset() {
		latestPoint = null;
		curPoint = getPoint(SearchKeypadDef.ID_K);
	}
	
	private void setResource() {
		keypadDataTxts = new String[KEYPAD_DATA.length][];
		keypadDataW = new int[KEYPAD_DATA.length][];
		
		for (int i=0; i<KEYPAD_DATA.length; i++) {
			keypadDataTxts[i] = new String[KEYPAD_DATA[i].length];
			keypadDataW[i] = new int[KEYPAD_DATA[i].length];
			for (int ii=0; ii<KEYPAD_DATA[i].length; ii++) {
				keypadDataTxts[i][ii] = SearchKeypadDef.getDisplayText(KEYPAD_DATA[i][ii]);
				if (keypadDataTxts[i][ii] != null) {
					keypadDataW[i][ii] = Rs.FM22.stringWidth(keypadDataTxts[i][ii]);
				}
			}
		}
		
		curPoint = getPoint(SearchKeypadDef.ID_K);
	}

	public void keyActionLeft() {
		Point tempPoint = new Point(curPoint.x, (KEYPAD_DATA[curPoint.x].length + curPoint.y - 1) % KEYPAD_DATA[curPoint.x].length);
		latestPoint = curPoint;
		curPoint = tempPoint;
		repaint();
	}

	public void keyActionRight() {
		int tempIdx = curPoint.y + 1;
		if (tempIdx >= KEYPAD_DATA[curPoint.x].length) {
			if (compWatcher != null) {
				compWatcher.goOverRight(this);
			}
			repaint();
			return;
		}
		Point tempPoint = new Point(curPoint.x, curPoint.y + 1);
		latestPoint = curPoint;
		curPoint = tempPoint;
		repaint();
	}

	public void keyActionUp() {
		if (curPoint == null) {
			return;
		}
		if (curPoint.x == 0) {
			if (compWatcher != null) {
				compWatcher.goOverUp(this);
			}
		} else {
			Point tempPoint = getProperPointByUpKey(curPoint);
			if (tempPoint == curPoint) {
				return;
			}
			latestPoint = curPoint;
			curPoint = tempPoint;
		}
		repaint();
	}

	public void keyActionDown() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchKeypad.keyActionDown] Called. curPoint: " + curPoint);
		}
		if (curPoint == null) {
			return;
		}
		if (curPoint.x >= KEYPAD_DATA.length - 1) {
//			if (compWatcher != null) {
//				compWatcher.goOverDown(this);
//			}
		} else {
			Point tempPoint = getProperPointByDownKey(curPoint);
			if (tempPoint == curPoint) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[SearchKeypad.keyActionDown] Same.");
				}
				return;
			}
			latestPoint = curPoint;
			curPoint = tempPoint;
		}
		repaint();
	}

	public void keyActionOk() {
		clickEffectAction();
		if (searchKeypadListener != null) {
			searchKeypadListener.selectedKeypad(KEYPAD_DATA[curPoint.x][curPoint.y]);
		}
	}
	
	public void setSearchKeypadListener(KeypadListener reqSearchKeypadListener) {
		searchKeypadListener = reqSearchKeypadListener;
	}
	
	public void removeSearchKeypadListener() {
		searchKeypadListener = null;
	}
	
	private void clickEffectAction() {
		int posX = 3 + (curPoint.y * H_GAP);
		int posY = 3 + (curPoint.x * V_GAP);
		int keypadAreaW = 46;
		switch(KEYPAD_DATA[curPoint.x][curPoint.y]) {
			case SearchKeypadDef.ID_SPACE:
			case SearchKeypadDef.ID_DELETE:
				keypadAreaW = 94;
				break;
		}
		new ClickingEffect(this, 5).start(posX, posY, keypadAreaW, 38);
	}
	
	private Point getPoint(int reqId) {
		for (int i=0; i<KEYPAD_DATA.length; i++) {
			for (int ii=0; ii<KEYPAD_DATA[i].length; ii++) {
				if (KEYPAD_DATA[i][ii] == reqId) {
					return new Point(i, ii);
				}
			}
		}
		return null;
	}
	
	private Point getProperPointByUpKey(Point reqPoint) {
		Point resPoint = reqPoint;
		switch(KEYPAD_DATA[reqPoint.x][reqPoint.y]) {
			case SearchKeypadDef.ID_SPACE:
				if (KEYPAD_DATA[latestPoint.x][latestPoint.y] == SearchKeypadDef.ID_DASH) {
					resPoint = latestPoint;
				} else {
					resPoint = getPoint(SearchKeypadDef.ID_APOSTROPHE);
				}
				break;
			case SearchKeypadDef.ID_DELETE:
				resPoint = getPoint(SearchKeypadDef.ID_SPACE);
				break;
			default:
				if (reqPoint.x > 0) {
					resPoint = new Point(reqPoint.x - 1, reqPoint.y);
				}
				break;
		}
		return resPoint;
	}
	
	private Point getProperPointByDownKey(Point reqPoint) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchKeypad.getProperPointByDownKey] Called. x: " + reqPoint.x + ", y: " + reqPoint.y);
		}
		Point resPoint = reqPoint;
		int reqId = KEYPAD_DATA[reqPoint.x][reqPoint.y];
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchKeypad.getProperPointByDownKey] reqId: " + SearchKeypadDef.getDisplayText(reqId));
		}
		switch(reqId) {
			case SearchKeypadDef.ID_APOSTROPHE:
			case SearchKeypadDef.ID_DASH:
				resPoint = getPoint(SearchKeypadDef.ID_SPACE);
				break;
			default:
				if (reqPoint.x < KEYPAD_DATA.length - 1) {
					resPoint = new Point(reqPoint.x + 1, reqPoint.y);
				}
				break;
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[SearchKeypad.getProperPointByDownKey] resPoint. x: " + resPoint.x + ", y: " + resPoint.y);
		}
		return resPoint;
	}
}

package com.videotron.tvi.illico.search;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.FontResource;
/**
 * Resource Class.
 * @author Jinjoo Ok
 */
public final class Rs {
    /**
     * Constructor.
     */
    private Rs() {

    }
    /** Application Version. */
    //->Kenneth[2017.8.22] R7.4 : Recording 검색시 EnglishTitle 사용하게 함
    //public static final String APP_VERSION = "(R7.4).0.1_01";
    //public static final String APP_VERSION = "(R7.4).0.1";
    public static final String APP_VERSION = "(R7.5).1.1";
    /** Application Name. */
    public static final String APP_NAME = "Search";
    
    public static final boolean PVR_TEST = false;
    
    /** Error code when the connection failed to server. */
    public static final String ERROR_SCH500 = "SCH500";
    /** Error code. */
    public static final String ERROR_SCH501 = "SCH501";
    /** Error code when XML parsing error occurred. */
    public static final String ERROR_SCH502 = "SCH502";
    /** Error code when IB data parsing error occurred. */
    public static final String ERROR_SCH503 = "SCH503";
    /** Error code when IB data does not exist. */
    public static final String ERROR_SCH504 = "SCH504";
    /** Error code when XML data file is invalid. */
    public static final String ERROR_SCH505 = "SCH505";
    /** Error code when Cannot create the Keyboard.*/
    public static final String ERROR_SCH506 = "SCH506";
    /** Error code when cannot communicated with the Loading Animation.*/
    public static final String ERROR_SCH507 = "SCH507";
    /** Error code(authorization of platform). */
    public static final String ERROR_MOT501 = "MOT501";
    
    
     /** Data Center Key - inband data path. */
    public static final String DCKEY_INBAND = "INBAND";
    /** Data Center Key - whether proxy is used or not. */
    public static final String DCKEY_PROXY_USE = "PROXY_USE";
    /** Data Center Key - proxy ip information. */
    public static final String DCKEY_PROXY_HOST = "PROXY_HOST";
    /** Data Center Key - porxy port information. */
    public static final String DCKEY_PROXY_PORT = "PROXY_PORT";
    
    /** Data Center Key - number of request retry. */
    public static final String DCKEY_SEARCH_SERVICE_REQUEST_RETRY = "SEARCH_REQUEST_RETRY_COUNT";
    /** Data Center Key - number of waiting delay when request retry. */
    public static final String DCKEY_SEARCH_SERVICE_REQUEST_RETRY_DELAY = "SEARCH_REQUEST_WAIT";
    /** Data Center Key - DCKEY_IS_AUTHORIZED. */
    public static final String DCKEY_IS_AUTHORIZED = "DCKEY_IS_AUTHORIZED";
    /** Data Center Key - number of search results for the group header which has the context. */
    public static final String DCKEY_SEARCH_MEMENTO = "DCKEY_SEARCH_MEMENTO";
    
    /** Sort Value. */
    public static final String SORT_VALUE_PVR_DATE_RECORDED = "SORT_BY_DATE_RECORDED";
    public static final String SORT_VALUE_PVR_TITLE = "SORT_BY_TITLE";
    
    public static final String DATA_CENTER_NULL_STRING = "NONE";
    
    public static final String TAG_EM_START = "<em>";
    public static final String TAG_EM_END = "</em>";
    public static final int TAG_EM_START_LTH = TAG_EM_START.length();
    public static final int TAG_EM_END_LTH = TAG_EM_END.length();

    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    /** The constant KEY_PAGE_UP. */
    public static final int KEY_PAGE_UP = HRcEvent.VK_PAGE_UP;
    /** The constant KEY_PAGE_DOWN. */
    public static final int KEY_PAGE_DOWN = HRcEvent.VK_PAGE_DOWN;
    /** The number of a line height for display. */
    public static final int LINE_HEIGHT = 28;
    
    
    public static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    
    
    /** Color 0*/
    public static final Color C0 = new Color(0, 0, 0);
    /** Color 23*/
    public static final Color C23 = new Color(23, 23, 23);
    /** Color 30*/
    public static final Color C30 = new Color(30, 30, 30);
    /** Color 39*/
    public static final Color C39 = new Color(39, 39, 39);
    /** Color 40*/
    public static final Color C40 = new Color(40, 40, 40);
    /** Color 54*/
    public static final Color C54 = new Color(54, 54, 54);
    /** Color. */
    public static final Color C55 = new Color(55, 55, 55);
    /** Color 56. */
    public static final Color C56 = new Color(56, 56, 56);
    /** Color 60. */
    public static final Color C60 = new Color(60, 60, 60);
    /** Color 77. */
    public static final Color C77 = new Color(77, 77, 77);
    /** Color 78. */
    public static final Color C78 = new Color(78, 78, 78);
    /** Color 118. */
    public static final Color C118 = new Color(118, 118, 118);   
    /** Color 139. */
    public static final Color C139 = new Color(139, 139, 139);   
    /** Color 149. */
    public static final Color C149 = new Color(149, 149, 149);   
    /** Color 170. */
    public static final Color C170 = new Color(170, 170, 170);
    /** Color 192. */
    public static final Color C192 = new Color(192, 192, 192);
    /** Color 215. */
    public static final Color C215 = new Color(215, 215, 215); 
    /** Color 200. */
    public static final Color C200 = new Color(200, 200, 200); 
    /** Color. */
    public static final Color C230 = new Color(230, 230, 230);
    /** Color 214 */
    public static final Color C214 = new Color(214, 214, 214);
    /** Color 222 */
    public static final Color C222 = new Color(222, 222, 222);
    /** Color 224 */
    public static final Color C224 = new Color(224, 224, 224);
    /** Color 222 */
    public static final Color C227 = new Color(227, 227, 227);
    /** Color 235 */
    public static final Color C235 = new Color(235, 235, 235);   
    /** Color 240 */
    public static final Color C240 = new Color(240, 240, 240);
    /** Color 241 */
    public static final Color C241 = new Color(241, 241, 241);
    /** Color 241, 182, 61 */
    public static final Color C241_182_61 = new Color(241, 182, 61);
    /** Color 246, 199, 0 */
    public static final Color C246_199_0 = new Color(246, 199, 0);
    /** Color 254 */
    public static final Color C254 = new Color(254, 254, 254);
    /** Color 255 */
    public static final Color C255 = new Color(255, 255, 255);
    /** DVBColor 246, 199, 0 */
    public static final DVBColor DC29_29_29_128 = new DVBColor(29, 29, 29, 128);
    
    
    /** Font 15. */
    public static final Font F15 = FontResource.BLENDER.getFont(15);
    /** Font. */
    public static final Font F16 = FontResource.BLENDER.getFont(16);
    /** Font. */
    public static final Font F17 = FontResource.BLENDER.getFont(17);
    /** Font. */
    public static final Font F19 = FontResource.BLENDER.getFont(19);
    public static final Font F19_B = FontResource.BLENDER.getBoldFont(19);
    /** Font 21. */
    public static final Font F21 = FontResource.BLENDER.getFont(21);
    /** Font 22. */
    public static final Font F22 = FontResource.BLENDER.getFont(22);
    /** Font. */
    public static final Font F23 = FontResource.BLENDER.getFont(23);
    /** Font. */
    public static final Font F20 = FontResource.BLENDER.getFont(20);
    /** Font. */
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    
    /** FontMetrics made by F15 */
    public static final FontMetrics FM15 = FontResource.getFontMetrics(F15);
    /** FontMetrics made by F16 */
    public static final FontMetrics FM16 = FontResource.getFontMetrics(F16);
    /** FontMetrics made by F17 */
    public static final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    /** FontMetrics made by F18 */
    public static final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    /** FontMetrics made by F19 */
    public static final FontMetrics FM19 = FontResource.getFontMetrics(F19);
    /** FontMetrics made by F20 */
    public static final FontMetrics FM20 = FontResource.getFontMetrics(F20);
    /** FontMetrics made by F21 */
    public static final FontMetrics FM21 = FontResource.getFontMetrics(F21);
    /** FontMetrics made by F22 */
    public static final FontMetrics FM22 = FontResource.getFontMetrics(F22);
}

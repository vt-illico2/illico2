/**
 * @(#)PlayListRequestor.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.communication;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.galaxie.controller.ChannelEventController;
import com.videotron.tvi.illico.galaxie.data.ProxyInfo;
import com.videotron.tvi.illico.log.Log;

/**
 * The SongInfoRequestor request a XML includes a song list for current Galaxie channel and a cover to back-end.
 * @author Woojung Kim
 * @version 1.1
 */
public final class SongInfoRequestor {

    /** The instance of SongInfoRequestor. */
    private static SongInfoRequestor instance = new SongInfoRequestor();

    /** The prefix part of URL to request. */
    private static final String URL_PRE = "http://";
    /** The middle part of URL to request for XML. */
    private static final String URL_MID_CHANNEL_INFO = "/ubiquicast/metadata?file=";
    /** The last part of URL to request for XML. */
    private static final String URL_END_CHANNEL_INFO = "_NT.xml";
    /** The middle part of URL to request for cover. */
    private static final String URL_MID_ARTWORK = "/ubiquicast/cover?songId=";
    /** The end part of UTL to request for cover. */
    private static final String URL_END_ARTWORK = "&bgId=";
    /** The max size of buffer. */
    private static int BUFFER_MAX = 1024;

    private String serverIP;
    private int serverPort;
    private String context;

    private ProxyInfo[] proxyInfos;

    /**
     * Instantiates a Controller for singleton.
     */
    private SongInfoRequestor() {
    }

    /**
     * Gets a singleton instance of SongInfoRequestor.
     * @return the instance of SongInfoRequestor.
     */
    public static SongInfoRequestor getInstance() {
        return instance;
    }

    /**
     * Request a song information for Galaxie channel to ubiquicast server. URL is :
     * http://<proxyServer>/ubiquicast/metadata?file=GXAB_NT.xml
     * @param galaxieChannelId the id of current Galaxie channel.
     * @return The String to receive from back-end,
     * @throws IOException The IOException to occur when communication with back-end.
     */
    public String requestSongList(String galaxieChannelId) throws IOException {
        if (Log.DEBUG_ON) {
            Log.printDebug("SongInfoRequestor: requestSongList(): " + galaxieChannelId);
        }
        String playListString = null;
        StringBuffer sb = new StringBuffer();

        sb.append(URL_PRE);
        sb.append(serverIP);
        sb.append(":");
        sb.append(serverPort);
        sb.append(context);
        sb.append(URL_MID_CHANNEL_INFO);
        sb.append(galaxieChannelId);
        sb.append(URL_END_CHANNEL_INFO);

        if (Log.DEBUG_ON) {
            Log.printDebug("SongInfoRequestor: requestSongList(): url : " + sb.toString());
        }

        if (proxyInfos != null && proxyInfos.length > 0) {
            for (int i = 0; i < proxyInfos.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("SongInfoRequestor: requestArtworkalbum(): proxyInfos[i].ip : "
                            + proxyInfos[i].getIp());
                }

                URL url = null;
                BufferedInputStream bis = null;
                ByteArrayOutputStream baos = null;
                try {
                    if (Log.EXTRA_ON) {
                        Log.printDebug("RUN_TIME" + Resources.HTTP_START_TIME + " = " + System.currentTimeMillis());
                        DataCenter.getInstance().put(Resources.HTTP_START_TIME, new Long(System.currentTimeMillis()));
                    }
                    url = new URL("HTTP", proxyInfos[i].getIp(), proxyInfos[i].getPort(), sb.toString());
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.connect();
                    int code = con.getResponseCode();

                    if (Log.DEBUG_ON) {
                        Log.printDebug("SongInforequestor: requestSongList(): response code : " + code);
                    }

                    if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                        return null;
                    }

                    if (Log.EXTRA_ON) {
                        Log.printDebug("RUN_TIME" + Resources.XML_REQUEST_TIME + " = " + System.currentTimeMillis());
                        DataCenter.getInstance().put(Resources.XML_REQUEST_TIME, new Long(System.currentTimeMillis()));
                    }

                    bis = new BufferedInputStream(con.getInputStream());
                    baos = new ByteArrayOutputStream();

                    byte[] buf = new byte[BUFFER_MAX];
                    int count = 0;
                    while ((count = bis.read(buf)) != -1) {
                        baos.write(buf, 0, count);
                    }

                    playListString = baos.toString("UTF-8");

                    con.disconnect();

                    if (Log.EXTRA_ON) {
                        Log.printDebug("RUN_TIME" + Resources.XML_RECEIVED_TIME + " = " + System.currentTimeMillis());
                        DataCenter.getInstance().put(Resources.XML_RECEIVED_TIME, new Long(System.currentTimeMillis()));
                    }
                } catch (MalformedURLException e) {
                    Log.printError(e);
                } catch (IOException e) {
                    Log.printDebug("GAL503");
                    Log.printError(e);
                    if (i < proxyInfos.length - 1) {
                        ChannelEventController.getInstance().setErrorData();
                    }
                } catch (Exception e) {
                    Log.printDebug("GAL502");
                    Log.printError(e);
                    if (i < proxyInfos.length - 1) {
                        ChannelEventController.getInstance().setErrorData();
                    }
                } finally {
                    try {
                        if (baos != null) {
                            baos.close();
                        }
                        if (bis != null) {
                            bis.close();
                        }
                        url = null;
                    } catch (Exception e) {
                        Log.printError(e);
                    }
                }
                if (playListString != null) {
                    break;
                }
            } // for
        } else {// if (proxyInfo.length
            URL url = null;
            BufferedInputStream bis = null;
            ByteArrayOutputStream baos = null;
            try {
                if (Log.EXTRA_ON) {
                    Log.printDebug("RUN_TIME" + Resources.HTTP_START_TIME + " = " + System.currentTimeMillis());
                    DataCenter.getInstance().put(Resources.HTTP_START_TIME, new Long(System.currentTimeMillis()));
                }
                url = new URL(sb.toString());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.connect();
                int code = con.getResponseCode();

                if (Log.DEBUG_ON) {
                    Log.printDebug("SongInforequestor: requestSongList(): response code : " + code);
                }

                if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                    return null;
                }

                if (Log.EXTRA_ON) {
                    Log.printDebug("RUN_TIME" + Resources.XML_REQUEST_TIME + " = " + System.currentTimeMillis());
                    DataCenter.getInstance().put(Resources.XML_REQUEST_TIME, new Long(System.currentTimeMillis()));
                }

                bis = new BufferedInputStream(con.getInputStream());
                baos = new ByteArrayOutputStream();

                byte[] buf = new byte[BUFFER_MAX];
                int count = 0;
                while ((count = bis.read(buf)) != -1) {
                    baos.write(buf, 0, count);
                }

                playListString = baos.toString("UTF-8");

                con.disconnect();

                if (Log.EXTRA_ON) {
                    Log.printDebug("RUN_TIME" + Resources.XML_RECEIVED_TIME + " = " + System.currentTimeMillis());
                    DataCenter.getInstance().put(Resources.XML_RECEIVED_TIME, new Long(System.currentTimeMillis()));
                }

            } catch (MalformedURLException e) {
                Log.printError(e);
            } catch (IOException e) {
                Log.printDebug("GAL503");
                Log.printError(e);
                ChannelEventController.getInstance().setErrorData();
            } catch (Exception e) {
                Log.printDebug("GAL502");
                Log.printError(e);
                ChannelEventController.getInstance().setErrorData();
            } finally {
                try {
                    if (baos != null) {
                        baos.close();
                    }
                    if (bis != null) {
                        bis.close();
                    }
                    url = null;
                } catch (Exception e) {
                    Log.printError(e);
                }
            }
        }

        return playListString;
    }

    /**
     * Request the album artwork cover to ubiquicast server.
     * @param songId the id to combine a Category and id from song information on XML data.
     * @param bgId the id of background type from Management System.
     * @return the byte array for album artwork cover.
     * @throws IOException if there has Exception, Galaxie application show a default cover.
     */
    public byte[] requestArtworkalbum(String songId, String bgId) throws IOException {
        if (Log.DEBUG_ON) {
            Log.printDebug("SongIngoRequestor: requestArtworkalbum(): songId=" + songId + "\tbgId=" + bgId);
        }
        byte[] retVal = null;

        StringBuffer sb = new StringBuffer();

        sb.append(URL_PRE);
        sb.append(serverIP);
        sb.append(":");
        sb.append(serverPort);
        sb.append(context);
        sb.append(URL_MID_ARTWORK);
        sb.append(songId);
        sb.append(URL_END_ARTWORK);
        sb.append(bgId);

        if (Log.DEBUG_ON) {
            Log.printDebug("SongInfoRequestor: requestArtworkalbum(): url : " + sb.toString());
        }

        if (proxyInfos != null && proxyInfos.length > 0) {
            for (int i = 0; i < proxyInfos.length; i++) {
                URL url = null;
                BufferedInputStream bis = null;
                ByteArrayOutputStream baos = null;
                try {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("SongInfoRequestor: requestArtworkalbum(): proxyInfos[i].ip : "
                                + proxyInfos[i].getIp());
                    }

                    if (Log.EXTRA_ON) {
                        Log.printDebug("RUN_TIME" + Resources.IMAGE_REQUEST_TIME + " = " + System.currentTimeMillis());
                        DataCenter.getInstance()
                                .put(Resources.IMAGE_REQUEST_TIME, new Long(System.currentTimeMillis()));
                    }

                    url = new URL("HTTP", proxyInfos[i].getIp(), proxyInfos[i].getPort(), sb.toString());
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.connect();
                    int code = con.getResponseCode();

                    if (Log.DEBUG_ON) {
                        Log.printDebug("SongInforequestor: requestArtworkalbum(): response code : " + code);
                    }

                    if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                        return null;
                    }

                    bis = new BufferedInputStream(con.getInputStream());
                    baos = new ByteArrayOutputStream();

                    byte[] buf = new byte[BUFFER_MAX];
                    int count = 0;
                    while ((count = bis.read(buf)) != -1) {
                        baos.write(buf, 0, count);
                    }

                    retVal = baos.toByteArray();

                    if (Log.DEBUG_ON && retVal != null) {
                        Log.printDebug("SongInfoRequestor: requestArtworkalbum(): retVal size : " + retVal.length);
                    } else if (retVal == null) {
                        Log.printDebug("SongInfoRequestor: requestArtworkalbum(): retVal is null");
                    }

                    con.disconnect();

                    if (Log.EXTRA_ON) {
                        Log.printDebug("RUN_TIME" + Resources.IMAGE_RECEIVED_TIME + " = " + System.currentTimeMillis());
                        DataCenter.getInstance().put(Resources.IMAGE_RECEIVED_TIME,
                                new Long(System.currentTimeMillis()));
                    }
                } catch (MalformedURLException e) {
                    Log.printError(e);
                } catch (IOException e) {
                    Log.printDebug("GAL501");
                    Log.printError(e);
                    if (i < proxyInfos.length - 1) {
                        ChannelEventController.getInstance().setErrorData();
                    }
                } catch (Exception e) {
                    Log.printDebug("GAL502");
                    Log.printError(e);
                    if (i < proxyInfos.length - 1) {
                        ChannelEventController.getInstance().setErrorData();
                    }
                } finally {
                    try {
                        if (baos != null) {
                            baos.close();
                        }
                        if (bis != null) {
                            bis.close();
                        }
                        url = null;
                    } catch (Exception e) {
                        Log.printError(e);
                    }
                }
                if (retVal != null) {
                    break;
                }
            } // for
        } else {
            URL url = null;
            BufferedInputStream bis = null;
            ByteArrayOutputStream baos = null;
            try {
                if (Log.EXTRA_ON) {
                    Log.printDebug("RUN_TIME" + Resources.IMAGE_REQUEST_TIME + " = " + System.currentTimeMillis());
                    DataCenter.getInstance().put(Resources.IMAGE_REQUEST_TIME, new Long(System.currentTimeMillis()));
                }

                url = new URL(sb.toString());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.connect();
                int code = con.getResponseCode();

                if (Log.DEBUG_ON) {
                    Log.printDebug("SongInforequestor: requestArtworkalbum(): response code : " + code);
                }

                if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                    return null;
                }

                bis = new BufferedInputStream(con.getInputStream());
                baos = new ByteArrayOutputStream();

                byte[] buf = new byte[BUFFER_MAX];
                int count = 0;
                while ((count = bis.read(buf)) != -1) {
                    baos.write(buf, 0, count);
                }

                retVal = baos.toByteArray();

                if (Log.DEBUG_ON && retVal != null) {
                    Log.printDebug("SongInfoRequestor: requestArtworkalbum(): retVal size : " + retVal.length);
                } else if (retVal == null) {
                    Log.printDebug("SongInfoRequestor: requestArtworkalbum(): retVal is null");
                }

                con.disconnect();

                if (Log.EXTRA_ON) {
                    Log.printDebug("RUN_TIME" + Resources.IMAGE_RECEIVED_TIME + " = " + System.currentTimeMillis());
                    DataCenter.getInstance().put(Resources.IMAGE_RECEIVED_TIME, new Long(System.currentTimeMillis()));
                }
            } catch (MalformedURLException e) {
                Log.printError(e);
            } catch (IOException e) {
                Log.printDebug("GAL501");
                Log.printError(e);
                ChannelEventController.getInstance().setErrorData();
            } catch (Exception e) {
                Log.printDebug("GAL502");
                Log.printError(e);
                ChannelEventController.getInstance().setErrorData();
            } finally {
                try {
                    if (baos != null) {
                        baos.close();
                    }
                    if (bis != null) {
                        bis.close();
                    }
                    url = null;
                } catch (Exception e) {
                    Log.printError(e);
                }
            }
        }
        return retVal;
    }

    /**
     * Set a server information from MS.
     * @param ip server ip
     * @param port server port
     * @param c context in URL.
     */
    public void setServerInfo(String ip, int port, String c) {
        if (Log.DEBUG_ON) {
            Log.printDebug("SongInfoRequestor: setServerInfo(): ip=" + ip + "\tport=" + port + "\tcontext=" + c);
        }
        serverIP = ip;
        serverPort = port;
        context = c;
    }

    /**
     * Set a proxy server information from MS.
     * @param ip
     * @param port
     */
    public void setProxyInfo(ProxyInfo[] info) {
        if (Log.DEBUG_ON) {
            Log.printDebug("SongInfoRequestor: setProxyInfo()");
            if (info != null) {
                Log.printDebug("SongInfoRequestor: setProxyInfo(): info length =" + info.length);
            }
        }
        proxyInfos = info;
    }
    
    /**
     * Get a proxy server information.
     * @return {@link ProxyInfo} to has a information about proxy server.
     */
    public ProxyInfo[] getProxyInfo() {
        return proxyInfos;
    }
    
    /**
     * Get a cover URL.
     * @param songId a id of song
     * @param bgId a background id for theme.
     * @return URL
     */
    public String getCoverUrl(String songId, String bgId) {
        if (Log.INFO_ON) {
            Log.printInfo("SongInfoRequestor: getCoverUrl(): songId = " + songId + "\t bgId = " + bgId);
        }
        StringBuffer sb = new StringBuffer();

        sb.append(URL_PRE);
        sb.append(serverIP);
        sb.append(":");
        sb.append(serverPort);
        sb.append(context);
        sb.append(URL_MID_ARTWORK);
        sb.append(songId);
        sb.append(URL_END_ARTWORK);
        sb.append(bgId);
        
        if (Log.DEBUG_ON) {
            Log.printDebug("SongInfoRequestor: getCoverUrl(): url = " + sb.toString());
        }
        return sb.toString();
    }

}

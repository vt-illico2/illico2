/**
 * @(#)GalaxieServiceImpl.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.galaxie.data.EnrichedMetaData;
import com.videotron.tvi.illico.galaxie.data.GalaxieChannelDataManager;
import com.videotron.tvi.illico.galaxie.data.SongInfoXMLParser;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelDataUpdateListener;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieService;
import com.videotron.tvi.illico.log.Log;

/**
 * This class implement a GalaxieService.
 * @author Woojung Kim
 * @version 1.1
 */
public class GalaxieServiceImpl implements GalaxieService {

    /**
     * Get a current song & next song data for channel.
     * @param chNo channel number to want to know a songs.
     * @return a data for current & next song.
     * @throws RemoteException Remote exception.
     */
    public GalaxieChannelData getGalaxieData(int chNo) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieServiceImpl: getGalaxieData()");
        }

        long delayTime = Long.valueOf((String) DataCenter.getInstance().get("delayTime")).longValue();

        String xmlString = null;
        String galaixeId = DataCenter.getInstance().getString("no" + chNo);

        if (Log.DEBUG_ON) {
            Log.printDebug("GalaxieServiceImpl: getEnrichedMetaData(int): chNo = " + chNo);
            Log.printDebug("GalaxieServiceImpl: getEnrichedMetaData(int): galaixeId = " + galaixeId);
            Log.printDebug("GalaxieServiceImpl: getEnrichedMetaData(int): delayTime = " + delayTime);
        }

        try {
            if (galaixeId != null) {
                xmlString = SongInfoRequestor.getInstance().requestSongList(galaixeId);
                EnrichedMetaData[] songs = SongInfoXMLParser.parseSongInfo(xmlString, delayTime);

                GalaxieChannelData data = GalaxieChannelDataManager.getInstance().getGalaxieChannelData(songs, chNo);

                return data;

            }
        } catch (Exception e) {
            Log.printError(e);
        }
        return null;
    }

    /**
     * Add a listener to retrieve a data for song.
     * @param l listener to implement GalaxieChannelDataUpdateListener.
     * @throws RemoteException Remote exception.
     */
    public void addGalaxieChannelDataUpdatedListener(GalaxieChannelDataUpdateListener l) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieServiceImpl: addGalaxieChannelDataUpdatedListener()");
        }
        GalaxieChannelDataManager.getInstance().addGalaxieChannelDataUpdateListener(l);
    }

    /**
     * remove a listener to retrieve a data for song.
     * @throws RemoteException Remote exception.
     */
    public void removeGalaxieChannelDataUpdatedListener() throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieServiceImpl: removeGalaxieChannelDataUpdatedListener()");
        }
        GalaxieChannelDataManager.getInstance().removeGalaxieChannelDataUpdateListener();
    }

}

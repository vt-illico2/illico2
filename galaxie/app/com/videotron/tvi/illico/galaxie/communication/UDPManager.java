/**
 * @(#)UDPReceiver.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.communication;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.galaxie.data.EnrichedDataManager;
import com.videotron.tvi.illico.galaxie.data.GalaxieChannelDataManager;
import com.videotron.tvi.illico.log.Log;

/**
 * The UDPManager implements a functions to receive a multicast for next song informations and cover image.
 * @author Woojung Kim
 * @version 1.1
 */
public final class UDPManager {

    /** The instance of UDPManager. */
    private static UDPManager instance = new UDPManager();
    /** Indicates a type to receive a song information. */
    private final String TYPE_DATA = "SONG_INFO";
    /** Indicates a type to receive a cover image. */
    private final String TYPE_COVER = "COVER";

    /** The thread to receive a cover image. */
    private UDPReceiver coverThread;
    /** The thread to receive a song informations. */
    private UDPReceiver songInfoThread;

    private int coverPort = 0;
    private int dataPort = 0;

    /** Instantiates a UDPManager for singleton. */
    private UDPManager() {
    }

    /**
     * Returns a singleton instance of UDPManager.
     * @return The instance of UDPManager.
     */
    public static synchronized UDPManager getInstance() {
        return instance;
    }

    /**
     * Starts a UDPManager. Load a back-end ip and port. Create a two threads to receive a song informations and cover.
     * Start the threads.
     * @param galaxieId The id of current Galaxie channel.
     */
    public void start(String galaxieId) {
        if (Log.DEBUG_ON) {
            Log.printDebug("UDPManager: start() : galaxieId=" + galaxieId);
        }

        String group = DataCenter.getInstance().getString(galaxieId + "Group");

        if (group != null) {
            stop();
            songInfoThread = new UDPReceiver(group, dataPort, TYPE_DATA);
            coverThread = new UDPReceiver(group, coverPort, TYPE_COVER);
            
            songInfoThread.start();
            coverThread.start();
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("UDPManager: start() : group is null");
            }
        }
    }

    /**
     * Stop a DUPManager. Stop a two threads to receive a song informations and cover image.
     */
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("UDPManager: stop()");
        }
        if (songInfoThread != null) {
            //songInfoThread.interrupt();
            songInfoThread.dispose();
            songInfoThread = null;
        }

        if (coverThread != null) {
            //coverThread.interrupt();
            coverThread.dispose();
            coverThread = null;
        }
    }

    /**
     * Set a ports for data and cover from MS.
     * @param dPort data port
     * @param cPort cover port
     */
    public void setPorts(int dPort, int cPort) {
        dataPort = dPort;
        coverPort = cPort;
    }

    /**
     * The UDPReceiver implements a functions to receive a data via UDP.
     * @author Woojung Kim
     * @version 1.1
     */
    class UDPReceiver extends Thread {

        public static final int SIGNATURE = 0xDECA;
        /** The header size of UDP packets. */
        public static final int HEADER_SIZE = 7;
        /** The max size of payload. */
        public static final int MAX_PAYLOAD_SIZE = 8189;
        /** max payload + 3 extra bytes for counters. */
        public static final int MAX_PACKET_SIZE = HEADER_SIZE + MAX_PAYLOAD_SIZE;
        /** The ip of back-end. */
        private String host;
        /** The port of back-end. */
        private int port;
        /** The type of thread. see {@link UDPManager#TYPE_DATA}, {@link UDPManager#TYPE_COVER} */
        private String type;
        /** The byte array to have a received data. */
        private byte[][] filePackets;

        private MulticastSocket mSocket;

        private boolean isStart;

        public UDPReceiver(String host, int port, String type) {
            super("Multicast_Receive_Thread_" + type);
            isStart = true;
            this.host = host;
            this.port = port;
            this.type = type;

            if (Log.DEBUG_ON) {
                Log.printDebug(type + ":UDPReceiver: host=" + host + "\t port=" + port + "\t type=" + type);
            }
            
            if (Log.DEBUG_ON) {
                Log.printDebug(type + ": UDPReceiver: multicastSocket.joinGroup()");
            }

            try {
                mSocket = new MulticastSocket(port);
                InetAddress hostAdd = InetAddress.getByName(host);
                mSocket.joinGroup(hostAdd);

            } catch (Exception e) {
                if (Log.WARNING_ON) {
                    Log.printWarning(type + ": UDPReceiver: Could not join multicast group");
                }
                Log.print(e);
                return;
            }
            
            Log.printDebug(type + ": UDPReceiver: MulticastSocket.joinGroup() success.");
            try {
                long soTime = mSocket.getSoTimeout();
                Log.printDebug(type + ": UDPReceiver: MulticastSocket.getSoTimeout() : " + soTime);
                if (soTime != 0) {
                    mSocket.setSoTimeout(0);
                    Log.printDebug(type + ": UDPReceiver: MulticastSocket.setSoTimeout(0).");
                } else {
                    mSocket.setSoTimeout(2 * 60 * 60 * 1000);
                    Log.printDebug(type + ": UDPReceiver: set a 2 hours for SoTimeout");
                    Log.printDebug(type + ": UDPReceiver: MulticastSocket.getSoTimeout() : " + mSocket.getSoTimeout());
                }
                
            } catch (Exception e) {
                Log.print(e);
            }
        }

        /**
         * destroy a thread.
         */
        public void dispose() {
            if (Log.DEBUG_ON) {
                Log.printDebug(type + ": UDPReceiver: dispose()");
            }
            try {
                isStart = false;
                if (mSocket != null) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(type + ": UDPReceiver: multicastSocket.leaveGroup()");
                    }
                    InetAddress hostAdd = InetAddress.getByName(host);
                    mSocket.leaveGroup(hostAdd);
                    mSocket.close();
                    mSocket = null;
                    if (Log.DEBUG_ON) {
                        Log.printDebug(type + ": UDPReceiver: multicastSocket.leaveGroup() success");
                    }
                }
                if (filePackets != null) {
                    for (int i = 0; i < filePackets.length; i++) {
                        filePackets[i] = null;
                    }
                    filePackets = null;
                }
            } catch (UnknownHostException e) {
                Log.print(e);
            } catch (IOException e) {
                Log.print(e);
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                    Log.printWarning(type + ": UDPReceiver: fail to destory.");
                }
                Log.print(e);
            }
        }

        /**
         * join IGMP group and receive a data.
         */
        public void run() {
            if (Log.DEBUG_ON) {
                Log.printDebug("UDPReceiver: run()");
            }
            byte[] buf = new byte[MAX_PACKET_SIZE];
            int currentSequenceNumber = Integer.MAX_VALUE;

            ByteArrayOutputStream bos = null;

            try {
                while (isStart && mSocket != null) {
                    try {
                        Log.printDebug(type + ": UDPReceiver: create DatagramPacket.");
                        DatagramPacket packet = new DatagramPacket(buf, MAX_PACKET_SIZE);
                        do {
                            Log.printDebug(type + ": UDPReceiver: wait to receive a packet.");
                            mSocket.receive(packet);
                        } while (getSignature(packet) != SIGNATURE);

                        if (Log.DEBUG_ON) {
                            Log.printDebug(type + ":UDPReceiver: host=" + host + "\t port=" + port + "\t type=" + type);
                            Log.printDebug(type + ": UDPReceiver: packet " + packet.getLength());
                        }

                        // Check that we have a valid length
                        if (packet.getLength() >= HEADER_SIZE) {

                            byte sequenceNumber = buf[4];
                            byte packetNumber = buf[5];
                            byte numberOfPackets = buf[6];

                            if (Log.DEBUG_ON) {
                                Log.printDebug(type + ": UDPReceiver: Seq: " + sequenceNumber + " - Received packet "
                                        + (packetNumber + 1) + "/" + numberOfPackets);
                            }

                            if (sequenceNumber != currentSequenceNumber) {
                                currentSequenceNumber = sequenceNumber;

                                // Initialize sequence
                                if (!allPacketsReceived()) {
                                    Log.printInfo(type
                                            + ": UDPReceiver: UDPReceiver: ERROR : Did not receive all packets from"
                                            + " sequence");
                                }

                                if (packetNumber == 0 && numberOfPackets == 0) {
                                    Log.printInfo(type + ": UDPReceiver: Recieved an empty packet, no cover is "
                                            + "available for this song.");
                                    continue;
                                }

                                if (Log.DEBUG_ON) {
                                    Log.printDebug(type + ": UDPReceiver: Initializing a new sequence of "
                                            + numberOfPackets + " packets.");
                                }

                                filePackets = new byte[numberOfPackets][];
                            }

                            // Copy the data in the packet
                            filePackets[packetNumber] = new byte[packet.getLength() - HEADER_SIZE];
                            System.arraycopy(packet.getData(), HEADER_SIZE, filePackets[packetNumber], 0,
                                    packet.getLength() - HEADER_SIZE);

                            if (!isStart) {
                                if (Log.DEBUG_ON) {
                                    Log.printDebug(type + ": UDPReceiver: isStart is " + isStart + ", so break");
                                }
                                break;
                            }
                            if (allPacketsReceived()) {
                                if (Log.DEBUG_ON) {
                                    Log.printDebug(type + ": UDPReceiver: Got all packets for sequence #"
                                            + sequenceNumber + ", update data...");
                                }

                                bos = new ByteArrayOutputStream();

                                for (int i = 0; i < filePackets.length; i++) {
                                    bos.write(filePackets[i]);
                                }

                                // update
                                if (bos.size() > 0) {
                                    if (type == TYPE_DATA) {
                                        if (Log.DEBUG_ON) {
                                            Log.printDebug(type + ": UDPReceiver: next song info size : " + bos.size());
                                        }
                                        // song Info
                                        EnrichedDataManager.getInstance().updateNextMetaData(bos.toString("UTF-8"));
                                    } else {

                                        if (Log.DEBUG_ON) {
                                            Log.printDebug(type + ": UDPReceiver: next cover size : " + bos.size());
                                        }
                                        FrameworkMain.getInstance().getImagePool().removeAbsolutely(Resources.NEXT_COVER);
                                        // cover
                                        FrameworkMain.getInstance().getImagePool()
                                                .createImage(bos.toByteArray(), Resources.NEXT_COVER);

                                        if (GalaxieChannelDataManager.getInstance().isAddedListener()) {
                                            DataCenter.getInstance().put("NEXT_COVER", bos.toByteArray());
                                        }
                                    }
                                }
                                bos.close();

                                for (int i = 0; i < filePackets.length; i++) {
                                    filePackets[i] = null;
                                }
                                filePackets = null;
                            }
                        }

                    } catch (Exception e) {
                        if (Log.WARNING_ON) {
                            Log.printWarning(type + ": UDPReceiver: fail to recevie a packets.");
                        }
                        Log.print(e);
                    } finally {
                        if (bos != null) {
                            try {
                                bos.close();
                            } catch (Exception e) {
                                if (Log.WARNING_ON) {
                                    Log.printWarning(type + ": UDPReceiver: fail to close a stream.");
                                }
                                Log.print(e);
                            }
                        }
                    }
                }
            } finally {
                if (Log.DEBUG_ON) {
                    Log.printDebug(type + ":UDPReceiver: last finally");
                }
                if (mSocket != null) {
                    try {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(type + ": UDPReceiver: multicastSocket.leaveGroup()");
                        }
                        InetAddress hostAdd = InetAddress.getByName(host);
                        mSocket.leaveGroup(hostAdd);
                        mSocket.close();
                        mSocket = null;
                        if (Log.DEBUG_ON) {
                            Log.printDebug(type + ": UDPReceiver: multicastSocket.leaveGroup() success");
                        }
                    } catch (UnknownHostException e) {
                        Log.print(e);
                    } catch (IOException e) {
                        Log.print(e);
                    }
                }
            }
        }

        /**
         * Return a signature from packet.
         * @param packet The packet to be receive from UDP
         * @return The signature value.
         */
        private int getSignature(DatagramPacket packet) {
            if (Log.WARNING_ON) {
                Log.printWarning(type + ": UDPReceiver: getSignature(): packet " + packet.getLength());
            }
            int signature = 0x0;
            byte[] data = packet.getData();
            signature |= ((int) data[0] << 12 & 0xF000);
            signature |= ((int) data[1] << 8 & 0x0F00);
            signature |= ((int) data[2] << 4 & 0x00F0);
            signature |= ((int) data[3] << 0 & 0x000F);
            return signature;
        }

        /**
         * Check all packets that is received or not.
         * @return true if all packets received, false otherwise.
         */
        private boolean allPacketsReceived() {
            if (Log.WARNING_ON) {
                Log.printWarning(type + ": UDPReceiver: allPacketsReceived()");
            }
            if (filePackets == null) {
                return false;
            }
            for (int i = 0; i < filePackets.length; i++) {
                if (filePackets[i] == null) {
                    return false;
                }
            }
            return true;
        }
    }

}

/**
 * @(#)CommunicationManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.communication;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;

/**
 * The CommunicationManager class lookup a remote Service of Other Applcation.
 * @author Woojung Kim
 * @version 1.1
 */
public class CommunicationManager {

    /** The XletContext of Galaxie Application. */
    private XletContext xletContext;
    /** The instance of CommunicationManager. */
    private static CommunicationManager instance = new CommunicationManager();

    /** The wait time to retry to lookup. */
    private static final long RETRY_TIME = 2000L;

    /** The MonitorService to communicate with Monitor App. */
    MonitorService monitorService;

    /** The EpgService to communication with EPG App. */
    EpgService epgService;

    /** The Galaxie Service which apply a interface to other application. */
    GalaxieService galaxieService;

    /**
     * Instantiates a new CommunicationManager.
     * @return CommunicationManager object.
     */
    public static CommunicationManager getInstance() {
        return instance;
    }

    /**
     * Start a lookups.
     * @param xContext The XletContext of Galaxie Application.
     */
    public void start(XletContext xContext) {
        this.xletContext = xContext;

        lookUpService("/1/1/", MonitorService.IXC_NAME);
        lookUpService("/1/2030/", PreferenceService.IXC_NAME);
        lookUpService("/1/2026/", EpgService.IXC_NAME);
        lookUpService("/1/2100/", StcService.IXC_NAME);
        lookUpService("/1/2025/", ErrorMessageService.IXC_NAME);

        bindService();
    }

    /**
     * Dispose a resources.
     */
    public void dispose() {
        unbindService();

        monitorService = null;
        epgService = null;
    }

    private void bindService() {
        if (galaxieService == null) {
            galaxieService = new GalaxieServiceImpl();
        }
        try {
            IxcRegistry.bind(xletContext, GalaxieService.IXC_NAME, galaxieService);
        } catch (AlreadyBoundException e) {
            Log.printWarning(e);
        }
    }

    private void unbindService() {
        try {
            IxcRegistry.unbind(xletContext, GalaxieService.IXC_NAME);
        } catch (NotBoundException e) {
            Log.printWarning(e);
        }
    }

    /**
     * Look up a Service to communicate.
     * @param path The path of Service.
     * @param name The Key name of Service.
     */
    private void lookUpService(String path, final String name) {
        final String ixcName = path + name;
        Thread thread = new Thread("GALAXIE_LOOKUP_" + name) {
            public void run() {
                Remote remote = null;
                while (remote == null) {
                    try {
                        remote = IxcRegistry.lookup(xletContext, ixcName);
                    } catch (Exception ex) {
                        if (Log.INFO_ON) {
                            Log.printInfo("CommunicationManager: not found - " + name);
                        }
                    }
                    if (remote != null) {
                        if (Log.INFO_ON) {
                            Log.printInfo("CommunicationManager: found - " + name);
                        }
                        DataCenter.getInstance().put(name, remote);
                        return;
                    }
                    try {
                        Thread.sleep(RETRY_TIME);
                    } catch (Exception e) {
                        Log.printError(e);
                    }
                }
            }
        };
        thread.start();
    }

    /**
     * Return a MonitorService.
     * @return The MonitorService object to be lookup.
     */
    public MonitorService getMonitorService() {
        if (monitorService == null) {
            monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        }
        return monitorService;
    }

    /**
     * Return a EpgService.
     * @return The EpgService object to be lookup.
     */
    public EpgService getEpgService() {
        if (epgService == null) {
            epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        }

        return epgService;
    }
}

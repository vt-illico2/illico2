/**
 * @(#)GalaxieScreenSaverRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.text.DecimalFormat;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.galaxie.data.EnrichedMetaData;
import com.videotron.tvi.illico.galaxie.ui.GalaxieScreen;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GalaxieScreenSaverRenderer extends Renderer {

    private Rectangle bounds = new Rectangle(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

    /** The size 24 of font. */
    private final Font font24 = FontResource.BLENDER.getFont(24);

    /** The size 15 of font. */
    private final Font font16 = FontResource.BLENDER.getFont(16);
    private final FontMetrics fm16 = FontResource.BLENDER.getFontMetrics(font16);

//    private Color subColor = new Color(228, 26, 34);
    private Color subColor = new Color(65, 195, 255);

    /** The data includes a song informations. */
    private EnrichedMetaData currentSongdata;

    /** The duration for current song. */
    private String currentDuration;

    private Image galaxieImage;

    private int galaxieImageX, galaxieImageY, titleX, nextX;

    private String titleStr, nextStr;

    public GalaxieScreenSaverRenderer() {
    }

    /**
     * Returns a bounds of GalaxieScreenSaverRenderer.
     * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.UIComponent)
     * @return The bounds of GalaxieScreenSaverRenderer.
     * @param c The UIComponent to handle GalaxieScreenSaverRenderer.
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Paint a back color on screen and a image for screen saver.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics,
     *      com.videotron.tvi.illico.framework.UIComponent)
     * @param g The Graphics to paint
     * @param c The UIComponent to handle GalaixeScreenSaverRenderer.
     */
    protected void paint(Graphics g, UIComponent c) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        g.drawImage(galaxieImage, galaxieImageX, galaxieImageY, c);

        if (currentSongdata != null) {
            g.setFont(font16);
            g.setColor(subColor);
            g.drawString(titleStr, galaxieImageX + 46, galaxieImageY + 6);
            g.drawString(nextStr, galaxieImageX + 46, galaxieImageY + 45);

            g.setFont(font24);
            g.setColor(Color.WHITE);

            // current song title
            g.drawString(currentSongdata.getTitle() + currentDuration, titleX, galaxieImageY + 8);
            // next song title
            if (!currentSongdata.getNextSong().equals("Galaxie")) {
                g.drawString(currentSongdata.getNextSong(), nextX, galaxieImageY + 47);
            }
        }

    }

    /**
     * Prepare before rendering.
     * @param c The component of GalaxieScreen.
     * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
     */
    public void prepare(UIComponent c) {
        String imgName = DataCenter.getInstance().getString("screenSaverImageName");
        if (imgName != null) {
            galaxieImage = DataCenter.getInstance().getImage(imgName);
        }
        
        currentSongdata = ((GalaxieScreen) c).getCurrentSongData();

        titleStr = DataCenter.getInstance().getString("itemNames.title");
        nextStr = DataCenter.getInstance().getString("itemNames.next");

        if (currentSongdata != null && currentSongdata.getDuration() > 0) {
            StringBuffer sf = new StringBuffer();
            DecimalFormat df = new DecimalFormat("00");

            String min = df.format(currentSongdata.getDuration() / 60000);
            String second = df.format((currentSongdata.getDuration() % 60000) / 1000);
            sf.append("  [");
            sf.append(min);
            sf.append(":");
            sf.append(second);
            sf.append("]");
            currentDuration = sf.toString();
        } else {
            currentDuration = TextUtil.EMPTY_STRING;
        }
    }

    /**
     * Get a image to paint for screen saver.
     * @return the image.
     */
    public Image getGalaxieImage() {
        return galaxieImage;
    }

    /**
     * Sets a new coordinate for galaxie image.
     * @param x The x coordinate to paint a new position.
     * @param y The x coordinate to paint a new position.
     */
    public void setGalaxieImagePosition(int x, int y) {
        galaxieImageX = x;
        galaxieImageY = y;

        titleX = galaxieImageX + 46 + fm16.stringWidth(titleStr) + 13;
        nextX = galaxieImageX + 46 + fm16.stringWidth(nextStr) + 13;
    }
}

/**
 * @(#)GalaxieScreenRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.text.DecimalFormat;
import java.util.Date;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.galaxie.data.EnrichedDataManager;
import com.videotron.tvi.illico.galaxie.data.EnrichedMetaData;
import com.videotron.tvi.illico.galaxie.ui.GalaxieScreen;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The Class GalaxieScreenRenderer paint a informations of song for current Galaxie channel.
 * @author Woojung Kim
 * @version 1.1
 */
public class GalaxieScreenRenderer extends Renderer {

    /** The bounds of GalaxieScreen. */
    private Rectangle bounds = new Rectangle(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

    /** The size 24 of font. */
    private final Font font24 = FontResource.BLENDER.getFont(24);

    /** The size 15 of font. */
    private final Font font15 = FontResource.BLENDER.getFont(15);
    /** The size 17 of font. */
    private final Font font17 = FontResource.BLENDER.getFont(17);

    /** The FontMetrics for size 24. */
    private final FontMetrics fm24 = FontResource.BLENDER.getFontMetrics(font24);

    /** The FontMetrics for size 24. */
    private final FontMetrics fm17 = FontResource.BLENDER.getFontMetrics(font17);

    /** The coordinate of channel item. */
    private final Point channelPoint = new Point(493, 168);

    /** The coordinate of title item. */
    private final Point titlePoint = new Point(493, 229);

    /** The coordinate of artist item. */
    private final Point artistPoint = new Point(493, 257);

    /** The coordinate of album item. */
    private final Point albumPoint = new Point(493, 284);

    /** The coordinate of nextArtist item.. */
    private final Point nextArtistPoint = new Point(493, 332);

    /** The coordinate of poster item.. */
    private final Point posterPoint = new Point(96, 540);// new Point(94, 84);

    /** The color of title items. */
//    private final Color classicColor = new Color(255, 226, 0);
//    private final Color countryColor = new Color(251, 152, 0);
//    private final Color galaxiePlusColor = new Color(254, 118, 204);
//    private final Color jazzColor = new Color(0, 255, 249);
//    private final Color popColor = new Color(253, 84, 184);
//    private final Color rockColor = new Color(58, 220, 254);
    private final Color brandingColor = new Color(65, 195, 255);
    private Color titleColor = brandingColor;

    private Image clockImg;

    /** The x coordinate for item subjects. */
    private final int ITEM_SUBJECT_X = 480;

    /** The String for channel name. */
    private String channelName;

    /** The String for channel no. */
    private String channelNo;

    /** The x coordinate for channel no. */
    private int channelNoPosX;

    /** The background image for current channel for M1. */
    private Image backImage;

    /** The data includes a song informations. */
    private EnrichedMetaData currentSongdata;

    /** The duration for current song. */
    private String currentDuration;

    /**
     * Instantiates a new GalaxieScreenRenderer.
     */
    public GalaxieScreenRenderer() {
    }

    /**
     * Paints the GalaxieScreen include background and a informations of song for current Galaxie channel if PIDMetadata
     * is null, don't paint a information of song.
     * @param g the Graphics to paint the GalaxieScreen
     * @param c the UICompoent to implement GalaxieScreen.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics,
     *      com.videotron.tvi.illico.framework.UIComponent)
     */
    protected void paint(Graphics g, UIComponent c) {
        if (backImage != null) {
            g.drawImage(backImage, 0, 0, c);
        }

        g.setFont(font17);
        g.setColor(Color.WHITE);
        String clock = ((GalaxieScreen) c).getClockString();
        if (clock != null) {
            GraphicUtil.drawStringRight(g, clock, 910, 57);
            g.drawImage(clockImg, 910 - fm17.stringWidth(clock) - 24, 44, c);
        }

        if (currentSongdata != null && !"true".equals(currentSongdata.getAnnouncement())) {

            g.setFont(font15);
            g.setColor(titleColor);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.title"), ITEM_SUBJECT_X,
                    titlePoint.y);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.artist"), ITEM_SUBJECT_X,
                    artistPoint.y);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.album"), ITEM_SUBJECT_X,
                    albumPoint.y);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.next"), ITEM_SUBJECT_X,
                    nextArtistPoint.y);

            // 장르
            g.setFont(font24);
            if (channelName != null) {
                g.drawString(channelName, channelPoint.x, channelPoint.y);
            }
            g.setFont(font15);
            g.setColor(Color.WHITE);
            if (channelNo != null) {
                g.drawString(channelNo, channelNoPosX, channelPoint.y);
            }

            if (currentSongdata.getTitle() != null) {
                g.drawString(currentSongdata.getTitle() + currentDuration, titlePoint.x, titlePoint.y);
            }
            if (currentSongdata.getArtist() != null) {
                g.drawString(currentSongdata.getArtist(), artistPoint.x, artistPoint.y);
            }
            if (currentSongdata.getAlbum() != null) {
                g.drawString(currentSongdata.getAlbum(), albumPoint.x, albumPoint.y);
            }
            if (currentSongdata.getNextSong() != null && !currentSongdata.getNextSong().equals("Galaxie")) {
                g.drawString(currentSongdata.getNextSong(), nextArtistPoint.x, nextArtistPoint.y);
            }
            
            Image cover = DataCenter.getInstance().getImage(Resources.CURRENT_COVER);
            if (cover != null) {
                g.drawImage(cover, posterPoint.x, posterPoint.y - cover.getHeight(c), c);
            }

        } else if (((GalaxieScreen) c).isStarted()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("GalaxieScreenRenderer: data is null");
            }
            
            g.setFont(font15);
            g.setColor(titleColor);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.title"), ITEM_SUBJECT_X,
                    titlePoint.y);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.artist"), ITEM_SUBJECT_X,
                    artistPoint.y);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.album"), ITEM_SUBJECT_X,
                    albumPoint.y);
            GraphicUtil.drawStringRight(g, DataCenter.getInstance().getString("itemNames.next"), ITEM_SUBJECT_X,
                    nextArtistPoint.y);
            
            Image cover = DataCenter.getInstance().getImage(Resources.CURRENT_COVER);
            if (cover != null) {
                g.drawImage(cover, posterPoint.x, posterPoint.y - cover.getHeight(c), c);
            }
        }
        
        if (Log.EXTRA_ON) {
            g.drawString(FrameworkMain.getInstance().getApplicationVersion(), 520, 393);
            
            EnrichedMetaData[] songList = EnrichedDataManager.getInstance().getSongList();
            
            if (songList != null) {
	            for (int i = 0; i < songList.length; i++) {
	            	if (currentSongdata != null && currentSongdata.getStartTime().equals(songList[i].getStartTime())) {
	            		g.setColor(Color.red);
	            	} else {
	            		g.setColor(Color.cyan);
	            	}
	            	g.drawString("title=" + songList[i].getTitle() + ", Artist=" + songList[i].getArtist(), 50, 50 + i * 30);
	            	g.setColor(Color.green);
	            	g.drawString(songList[i].getStartTime(), 50, 65 + i * 30);
	            }
            }
            
//            long httpStart = DataCenter.getInstance().getLong(Resources.HTTP_START_TIME, 0);
//            long xmlRequest = DataCenter.getInstance().getLong(Resources.XML_REQUEST_TIME, 0);
//            long xmlReceived = DataCenter.getInstance().getLong(Resources.XML_RECEIVED_TIME, 0);
//            long xmlparsed = DataCenter.getInstance().getLong(Resources.XML_PARSED_TIME, 0);
//            long total = DataCenter.getInstance().getLong(Resources.TOTAL_TIME, 0);
//            
//            long imgRequest = DataCenter.getInstance().getLong(Resources.IMAGE_REQUEST_TIME, 0);
//            long imgReceived = DataCenter.getInstance().getLong(Resources.IMAGE_RECEIVED_TIME, 0);
//            
//            long sec = 0;
//            long ms = 0;
//            
//            g.drawString(Resources.HTTP_START_TIME + " : " + 0 + "     " + new Date(httpStart), 80, 100);
//            sec = (xmlRequest - httpStart) / 1000;
//            ms = (xmlRequest - httpStart) % 1000;
//            g.drawString(Resources.XML_REQUEST_TIME + " : " + sec + "sec  " + ms + "ms     " + new Date(xmlRequest), 80, 120);
//            sec = (xmlReceived - xmlRequest) / 1000;
//            ms = (xmlReceived - xmlRequest) % 1000;
//            g.drawString(Resources.XML_RECEIVED_TIME + " : " + sec + "sec  " + ms + "ms     " + new Date(xmlReceived), 80, 140);
//            sec = (xmlparsed - xmlReceived) / 1000;
//            ms = (xmlparsed - xmlReceived) % 1000;
//            g.drawString(Resources.XML_PARSED_TIME + " : " + sec + "sec  " + ms + "ms     " + new Date(xmlparsed), 80, 160);
//            sec = (imgRequest - xmlparsed) / 1000;
//            ms = (imgRequest - xmlparsed) % 1000;
//            g.drawString(Resources.IMAGE_REQUEST_TIME + " : " + sec + "sec  " + ms + "ms     " + new Date(imgRequest), 80, 180);
//            sec = (imgReceived - imgRequest) / 1000;
//            ms = (imgReceived - imgRequest) % 1000;
//            g.drawString(Resources.IMAGE_RECEIVED_TIME + " : " + sec + "sec  " + ms + "ms     " +  new Date(imgReceived), 80, 200);
//            sec = (total - httpStart) / 1000;
//            ms = (total - httpStart) % 1000;
//            g.drawString(Resources.TOTAL_TIME + " : " + sec + "sec  " + ms + "ms     " + new Date(total), 80, 220);
            
            
            
        }
    }

    /**
     * Prepare before rendering. Retrieve a data from GalaxieScreen to paint.
     * @param c the UICompoent to implement GalaxieScreen.
     * @see com.videotron.tvi.illico.framework.Renderer#prepare(com.videotron.tvi.illico.framework.UIComponent)
     */
    public void prepare(UIComponent c) {
        clockImg = DataCenter.getInstance().getImage("clock.png");
        
        currentSongdata = ((GalaxieScreen) c).getCurrentSongData();

        if (currentSongdata != null && currentSongdata.getDuration() > 0) {
            StringBuffer sf = new StringBuffer();
            DecimalFormat df = new DecimalFormat("00");

            String min = df.format(currentSongdata.getDuration() / 60000);
            String second = df.format((currentSongdata.getDuration() % 60000) / 1000);
            sf.append("  [");
            sf.append(min);
            sf.append(":");
            sf.append(second);
            sf.append("]");
            currentDuration = sf.toString();
        } else {
            currentDuration = TextUtil.EMPTY_STRING;
        }
    }

    /**
     * Return a bounds to paint.
     * @param c the UICompoent to implement GalaxieScreen.
     * @return the bounds of GalaxieScreen.
     * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds(com.videotron.tvi.illico.framework.UIComponent)
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Sets the channel data to show from EPG.
     * @param chName the channel genre of current Galaxie channel.
     * @param chNo the channel no of current Galaxie channel.
     */
    public void setChannelData(String chName, String chNo) {
        if (Log.DEBUG_ON) {
            Log.printDebug("GalaxieScreenRenderer: called setChannelData()");
        }
        this.channelName = chName;
        this.channelNo = chNo;
        int channelNameGap = 20;
        if (channelName != null) {
            channelNoPosX = channelPoint.x + fm24.stringWidth(this.channelName) + channelNameGap;
        } else {
            channelNoPosX = channelPoint.x + channelNameGap;
        }
    }

    /**
     * Sets a background image for current Galaxie channel and sets a title color .
     * @param img the img to paint for background.
     * @param bgId the id of background.
     */
    public void setBackgroundImage(Image img, String bgId) {
        backImage = img;

        // comment out for DDC-095 (new Branding)
//        if (bgId.equals("1")) {
//            titleColor = classicColor;
//        } else if (bgId.equals("2")) {
//            titleColor = countryColor;
//        } else if (bgId.equals("3")) {
//            titleColor = galaxiePlusColor;
//        } else if (bgId.equals("4")) {
//            titleColor = jazzColor;
//        } else if (bgId.equals("5")) {
//            titleColor = popColor;
//        } else if (bgId.equals("6")) {
//            titleColor = rockColor;
//        } else {
//            titleColor = classicColor;
//        }
        
        titleColor = brandingColor;
    }
}

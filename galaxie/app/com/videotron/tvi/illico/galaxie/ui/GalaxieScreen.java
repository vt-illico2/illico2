/**
 * @(#)GalaxieSccreen.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.ui;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredUIInfo;
import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.galaxie.data.EnrichedMetaData;
import com.videotron.tvi.illico.galaxie.gui.GalaxieScreenRenderer;
import com.videotron.tvi.illico.galaxie.gui.GalaxieScreenSaverRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Formatter;

/**
 * The GalaxieScreen class setup Galaxie screen components and updates data for
 * the screen. .
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class GalaxieScreen extends UIComponent implements TVTimerWentOffListener, KeyListener, ClockListener {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3473042443057160867L;

	/** Indicate a mode to display a informations of current song. */
	private static final int MODE_SONG_INFORMATIONS = 0x00;
	/** Indicate a mode to display a Galaxie Screen Saver. */
	private static final int MODE_SCREEN_SAVER = 0x01;

	/** The renderer to paint Galaxie Screen. */
	private GalaxieScreenRenderer renderer = new GalaxieScreenRenderer();
	/** The renderer to paint Galaxie Screen Saver. */
	private GalaxieScreenSaverRenderer saverRenderer = new GalaxieScreenSaverRenderer();

	private final Rectangle ZERO_BOUNDS = new Rectangle(0, 0, 0, 0);

	/** The background image for current channel for M1. */
	private Image backImage;

	/** The data includes a song informations. */
	private EnrichedMetaData currentSongdata;

	/**
	 * The TVTimerSpec to update a new position for Galaxie screen saver's
	 * image.
	 */
	private TVTimerSpec saverTimerSpec = new TVTimerSpec();
	/** The TVTimerSpec to wait a start time of Galaxie screen saver. */
	private TVTimerSpec waitTimerSpec = new TVTimerSpec();

	/** The update period. */
	private long updateTime = 2000L;
	/** The wait time to start Galaxie screen saver. */
	private long screenSaverWaitTimeToStart = 60 * 1000;
	/** The x and y coordinate for Galaxie screen saver's image. */
	private int screenSaverImageX, screenSaverImageY;

	/**
	 * The mode of Galaxie Screen. It is a {@link #MODE_SONG_INFORMATIONS} or
	 * {@link #MODE_SCREEN_SAVER}.
	 */
	private int mode = MODE_SONG_INFORMATIONS;

	/** The clock. */
	private static String clockString;

	private boolean isStarted = false;

	/**
	 * Instantiates a new GalaxieScreen. set a renderer.
	 */
	public GalaxieScreen() {
		screenSaverWaitTimeToStart = Long.valueOf((String) DataCenter.getInstance().get("screenSaverWaitTimeToStart"))
				.longValue();
		updateTime = Long.valueOf((String) DataCenter.getInstance().get("screenSaverUpdateTime")).longValue();
	}

	/**
	 * Call before showing GalaxieScreen. Add GalaxieScreen in HScene.
	 * 
	 * @see com.videotron.tvi.illico.framework.UIComponent#start()
	 */
	public void start() {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: start()");
		}
		isStarted = true;
		Formatter formatter = Formatter.getCurrent();
		clockString = formatter.getLongDate();
		Clock.getInstance().addClockListener(this);

		setRenderer(renderer);
		FrameworkMain.getInstance().addComponent(this);
		Log.printDebug("GalaxieScreen: start(): hscene component count is "
				+ FrameworkMain.getInstance().getHScene().getComponentCount());
		addKeyListener(this);
		requestFocus();
		mode = MODE_SONG_INFORMATIONS;
		startWaitTimer();

		updateScreenSaverCoordinate();
	}

	/**
	 * Call when Galaxie be closed. Remove GalaxieScreen in HScene and clear a
	 * resources.
	 * 
	 * @see com.videotron.tvi.illico.framework.UIComponent#stop()
	 */
	public void stop() {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: stop()");
		}
		isStarted = false;
		FrameworkMain.getInstance().removeComponent(this);

		if (mode == MODE_SCREEN_SAVER) {
			stopScreenSaverTimer();
		} else {
			stopWaitTimer();
		}

		Clock.getInstance().removeClockListener(this);
		currentSongdata = null;
		DataCenter.getInstance().removeImage(Resources.CURRENT_COVER);
		setChannelData(null, null);
		updateData(null);

		removeKeyListener(this);
		if (backImage != null) {
			backImage = null;
			FrameworkMain.getInstance().getImagePool().remove(Resources.BACKGROUND);
		}

		String imgName = DataCenter.getInstance().getString("screenSaverImageName");
		if (imgName != null) {
			DataCenter.getInstance().removeImage(imgName);
		}
		DataCenter.getInstance().removeImage("clock.png");
	}

	public boolean isStarted() {
		return isStarted;
	}

	/**
	 * Update a clock.
	 * 
	 * @param date
	 *            updated date.
	 */
	public void clockUpdated(Date date) {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: clockUpdated(): date = " + date.toString());
		}
		Formatter formatter = Formatter.getCurrent();
		clockString = formatter.getLongDate();
		repaint();
	}

	/**
	 * Get a clock string.
	 * 
	 * @return a string for current time.
	 */
	public String getClockString() {
		return clockString;
	}

	/**
	 * Update EnrichedMetadata from Galaxie back-end.
	 * 
	 * @param data
	 *            the data of song informations.
	 */
	public void updateData(EnrichedMetaData data) {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: update()");
			if (data != null) {
				Log.printDebug("GalaxieScreen: title = " + data.getTitle());
				Log.printDebug("GalaxieScreen: album = " + data.getAlbum());
				Log.printDebug("GalaxieScreen: artist = " + data.getArtist());
				Log.printDebug("GalaxieScreen: next = " + data.getNextSong());
				Log.printDebug("GalaxieScreen: announcement = " + data.getAnnouncement());
			}
		}
		if (currentSongdata == null || currentSongdata != data) {
			this.currentSongdata = data;
			renderer.prepare(this);
			saverRenderer.prepare(this);

			if (data != null) {
				FrameworkMain.getInstance().getImagePool().waitForAll();
			} else {
				DataCenter.getInstance().put(Resources.HTTP_START_TIME, new Long(0));
				DataCenter.getInstance().put(Resources.XML_REQUEST_TIME, new Long(0));
				DataCenter.getInstance().put(Resources.XML_RECEIVED_TIME, new Long(0));
				DataCenter.getInstance().put(Resources.XML_PARSED_TIME, new Long(0));
				DataCenter.getInstance().put(Resources.TOTAL_TIME, new Long(0));
			}

			if (Log.EXTRA_ON) {
				Log.printDebug("RUN_TIME" + Resources.TOTAL_TIME + " = " + System.currentTimeMillis());
				DataCenter.getInstance().put(Resources.TOTAL_TIME, new Long(System.currentTimeMillis()));
			}
			repaint();
		}
	}

	/**
	 * Sets the channel data from EPG.
	 * 
	 * @param channelName
	 *            the channel name of Galaxie
	 * @param channelNo
	 *            the channel no
	 */
	public void setChannelData(String channelName, String channelNo) {
		if (Log.DEBUG_ON) {
			Log.printDebug("GalaxieScreen: called setChannelData()");
		}
		renderer.setChannelData(channelName, channelNo);
	}

	/**
	 * Display a background image for M1.
	 * 
	 * @param img
	 *            the byte array of cover.
	 * @param bgId
	 *            the id of theme to draw the color of texts.
	 */
	public void displayBackbround(byte[] img, String bgId) {
		if (backImage != null) {
			FrameworkMain.getInstance().getImagePool().remove(Resources.BACKGROUND);
		}
		backImage = FrameworkMain.getInstance().getImagePool().createImage(img, Resources.BACKGROUND);
		renderer.setBackgroundImage(backImage, bgId);
		FrameworkMain.getInstance().getImagePool().waitForAll();
		setRenderer(renderer);
		FrameworkMain.getInstance().addComponent(this);
		repaint();
	}

	/**
	 * Returns the enriched metadata of current song.
	 * 
	 * @return the enriched metadata.
	 */
	public EnrichedMetaData getCurrentSongData() {
		return currentSongdata;
	}

	/**
	 * Start the timer to paint a image for Galaxie Screen saver. Stop a last
	 * schedule of saverTimerSpec and start a new schedule of saverTimerSpec.
	 */
	private void startScreenSaverTimer() {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: startScreenSaverTimer()");
		}
		stopScreenSaverTimer();

		saverTimerSpec.setDelayTime(updateTime);
		saverTimerSpec.setRepeat(true);
		saverTimerSpec.addTVTimerWentOffListener(this);

		try {
			TVTimer.getTimer().scheduleTimerSpec(saverTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.printError(e);
		}
	}

	/**
	 * Stop the timer to paint a image for Galaxie screen.
	 */
	private void stopScreenSaverTimer() {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: stopScreenSaverTimer()");
		}
		saverTimerSpec.removeTVTimerWentOffListener(this);
		TVTimer.getTimer().deschedule(saverTimerSpec);
	}

	/**
	 * Start the timer to wait a start time to start a Galaxie Screen saver.
	 * Stop a last schedule of waitTimerSpec and start a new schedule of
	 * waitTimerSpec.
	 */
	private void startWaitTimer() {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: startWaitTimer()");
		}

		stopWaitTimer();

		if (screenSaverWaitTimeToStart == 0) {
			if (Log.INFO_ON) {
				Log.printInfo("GalaxieScreen: startWaitTimer(): screenSaverWaitTimeToStart = 0.");
			}
			return;
		}

		waitTimerSpec.setDelayTime(screenSaverWaitTimeToStart);
		waitTimerSpec.addTVTimerWentOffListener(this);

		try {
			TVTimer.getTimer().scheduleTimerSpec(waitTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.printError(e);
		}
	}

	/**
	 * Stop the timer to wait a start time to start a Galaxie Screen Saver.
	 */
	private void stopWaitTimer() {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: stopWaitTimer()");
		}
		waitTimerSpec.removeTVTimerWentOffListener(this);
		TVTimer.getTimer().deschedule(waitTimerSpec);
	}

	/**
	 * Receive a event to paint a image for Galaxie screen.
	 * 
	 * @param event
	 *            The event of registered TimerSpec to paint a image for Galaxie
	 *            Screen Saver.
	 */
	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo("GalaxieScreen: timerWentOff()");
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("GalaxieScreen: timerWentOff(): event spec = " + event.getTimerSpec().toString());
			Log.printDebug("GalaxieScreen: timerWentOff(): isActive = " + isActive());
		}
		if (event.getTimerSpec().equals(waitTimerSpec)) {

			if (!isActive()) {
				mode = MODE_SCREEN_SAVER;
				setRenderer(saverRenderer);
				repaint();
				startScreenSaverTimer();
			} else {
				startWaitTimer();
			}
		} else if (event.getTimerSpec().equals(saverTimerSpec)) {
			updateScreenSaverCoordinate();
			repaint();
		}
	}

	private void updateScreenSaverCoordinate() {
		int imageWidth = 200;
		int imageHeight = 100;
		Image galaxieImage = saverRenderer.getGalaxieImage();
		if (galaxieImage != null) {
			imageWidth = galaxieImage.getWidth(this) + 80;
			imageHeight = galaxieImage.getHeight(this) + 60;
		}

		// (Math.random()*(max - min + start))
		//
		screenSaverImageX = (int) (Math.random() * (880 - imageWidth)) + 60;
		screenSaverImageY = (int) (Math.random() * (480 - imageHeight)) + 40;

		if (Log.DEBUG_ON) {
			Log.printDebug("GalaxieScreen: timerWentOff(): screenSaverImageX=" + screenSaverImageX);
			Log.printDebug("GalaxieScreen: timerWentOff(): screenSaverImageY=" + screenSaverImageY);
		}
		saverRenderer.setGalaxieImagePosition(screenSaverImageX, screenSaverImageY);
	}

	/**
	 * Check a active ui over Galaxie.
	 * 
	 * @return true if active UI exist, false otherwise.
	 */
	private boolean isActive() {
		boolean isActive = false;
		LayeredUIInfo[] infos = LayeredUIManager.getInstance().getAllLayeredUIInfos();
		if (infos != null) {
			for (int i = 0; i < infos.length; i++) {

				if (Log.DEBUG_ON) {
					Log.printDebug("GalaxieScreen: timerWentOff(): infos = " + infos[i].getName());
					Log.printDebug("GalaxieScreen: timerWentOff(): infos = " + infos[i].getPriority());
					Log.printDebug("GalaxieScreen: timerWentOff(): infos = " + infos[i].getBounds().toString());
					Log.printDebug("GalaxieScreen: timerWentOff(): infos = " + infos[i].isActive());
				}

				if (infos[i].isActive() && !infos[i].getBounds().equals(ZERO_BOUNDS)) {
					isActive = true;
					break;
				}
			}
		}
		return isActive;
	}

	/**
	 * Receives a key event and reset a Galaxie screen saver's wait timer.
	 * 
	 * @param code
	 *            the code of key event.
	 * @return true if handle a key code, false otherwise.
	 */
	public boolean handleKey(int code) {

		if (code == OCRcEvent.VK_ENTER) {
			if (mode == MODE_SONG_INFORMATIONS) {
				if (!isActive()) {
					mode = MODE_SCREEN_SAVER;
					setRenderer(saverRenderer);
					repaint();
					startScreenSaverTimer();
				} else {
					startWaitTimer();
				}
			} else {
				mode = MODE_SONG_INFORMATIONS;
				setRenderer(renderer);
				stopScreenSaverTimer();
				startWaitTimer();
				repaint();
			}
		} else {
			if (mode == MODE_SONG_INFORMATIONS) {
				startWaitTimer();
			} else {
				mode = MODE_SONG_INFORMATIONS;
				setRenderer(renderer);
				stopScreenSaverTimer();
				startWaitTimer();
				repaint();
			}
		}
		return true;
	}

	/**
	 * Receives a KeyEvent of keyPressed and pass keyCode to GalaxieScreen via
	 * handleKey(int).
	 * 
	 * @param e
	 *            The key event to occur.
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
		if (Log.DEBUG_ON) {
			Log.printDebug("GalaxieScreen: keyPressed: keyCode = " + e.getKeyCode());
		}

		int keyCode = e.getKeyCode();

		handleKey(keyCode);

	}

	/**
	 * Don't use keyReleased() in the case of Keyboard application.
	 * 
	 * @param e
	 *            tThe key event to occur
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
	}

	/**
	 * Don't use keyTyped() in the case of Keyboard application.
	 * 
	 * @param e
	 *            The key event to occur
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}
}

/**
 * @(#)ChannelEventController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.controller;

import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import javax.tv.xlet.XletContext;

import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.galaxie.communication.CommunicationManager;
import com.videotron.tvi.illico.galaxie.communication.SongInfoRequestor;
import com.videotron.tvi.illico.galaxie.communication.UDPManager;
import com.videotron.tvi.illico.galaxie.data.DataParser;
import com.videotron.tvi.illico.galaxie.data.EnrichedDataManager;
import com.videotron.tvi.illico.galaxie.data.EnrichedMetaData;
import com.videotron.tvi.illico.galaxie.data.GalaxieChannel;
import com.videotron.tvi.illico.galaxie.ui.GalaxieScreen;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.monitor.ServiceStateListener;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * The ChannelEventController class handles the default behaviour of Galaxie Application. Receives EPG Channel Event and
 * determines Channel Type to show/hide Galaxie Application.
 * @author Woojung Kim
 * @version 1.1
 */
public final class ChannelEventController implements ChannelEventListener, TVTimerWentOffListener, DataUpdateListener,
        PreferenceListener, PowerModeChangeListener {

    /** The instance of ChannelEventController. */
    private static ChannelEventController instance = new ChannelEventController();
    /** The point to draw a loading animation. */
    private Point loadingPoint = new Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2);
    /** The Remote Object of EpgService. */
    private EpgService epgService;
    /** The remote object of MonitorService. */
    private MonitorService monitorService;
    /** The XletContext of Galaxie application. */
    private XletContext xletContext;
    /** The UIComponent to show Galaxie application. */
    private GalaxieScreen galaxieScreen;
    /** The current channel is Galaxie channel or not . */
    private boolean isGalaxieChannel;
    /** The current source id. */
    private TvChannel currentChannel;

    /** The waiting time to get an XML file by http request. */
    private long waitingTime = 2000L;

    /** The thread to request a enriched data from galaxie back-end. */
    private SongInfoUpdater updaterThread;

    /** The StatusChangeReceiver to monitor a current mode of TV View. */
    private StatusChangeReceiver statusChangeReceiver;

    /** The Listener to return a confirmation to launch Screen Saver. */
    private ScreenSaverConfirmationListenerImpl sListener;

    /** When timer event occur, It means the time to change from current song to next song. */
    private TVTimerSpec changeSongTimerSpec;

    /** The implement class for InbandDataListener. */
    private InbandDataListenerImpl inbandListner;

    /** The implement class for LogLevelListener. */
    private LogLevelAdapter logListener;

    /** The path name of Galaxie wallpaper data in DataCenter. */
    private static final String IN_BAND_DATA_PATH = "GALAXIE_WALLPAPER";
    /** The current version number. */
    private int currentVersion;

    private EnrichedMetaData errorData;

    /**
     * Instantiates a new channel event controller for singleton.
     */
    private ChannelEventController() {
        errorData = new EnrichedMetaData();
    }

    /**
     * Gets the singleton instance of ChannelEventController.
     * @return single instance of ChannelEventController
     */
    public static ChannelEventController getInstance() {
        return instance;
    }

    /**
     * Inits the ChannelEventController. Creates {@link GalaxieScreen} to show a information of Galaxie channel.
     * @param xc the XletContext received from {@link com.videotron.tvi.illico.galaxie.App} is used to bind and lookup
     *            IXC
     */
    public void init(XletContext xc) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: init()");
        }
        this.xletContext = xc;

        changeSongTimerSpec = new TVTimerSpec();
        changeSongTimerSpec.addTVTimerWentOffListener(this);
        inbandListner = new InbandDataListenerImpl();
        galaxieScreen = new GalaxieScreen();
        statusChangeReceiver = new StatusChangeReceiver();
        sListener = new ScreenSaverConfirmationListenerImpl();
        logListener = new LogLevelAdapter();

        waitingTime = Long.valueOf((String) DataCenter.getInstance().get("waitTime")).longValue();

        DataCenter.getInstance().addDataUpdateListener(IN_BAND_DATA_PATH, inbandListner);
        DataCenter.getInstance().addDataUpdateListener(EpgService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(MonitorService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(PreferenceService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(StcService.IXC_NAME, this);
    }

    /**
     * Called when Galaxie application start. run to lookup a PreferenceService and EpgService and MonitorService.
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: start()");
        }
        BackgroundController.getInstance().start();
        CommunicationManager.getInstance().start(xletContext);
        isGalaxieChannel = false;

        Host.getInstance().addPowerModeChangeListener(this);

        // code for test
        // try {
        // Thread.sleep(20000);
        // // 1150, 1159
        // selectionRequested(1234, (short) 2);
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // }

    }

    /**
     * Called when Galaxie application stop. clear resource.
     */
    public void stop() {
        isGalaxieChannel = false;
        BackgroundController.getInstance().stop();
    }

    /**
     * Called when Galaxie application destory. clear resource and remove a every listener to add.
     */
    public void dispose() {
        stop();

        Host.getInstance().removePowerModeChangeListener(this);
        FrameworkMain.getInstance().destroy();
        EnrichedDataManager.getInstance().flushData();
        UDPManager.getInstance().stop();

        try {
            epgService.getChannelContext(0).removeChannelEventListener(this);
        } catch (RemoteException e) {
            Log.print(e);
        }

        try {
            monitorService.removeStateListener(statusChangeReceiver, "Galaxie");
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * Gets the display status of Galaxie application. if isShow is true, it means that Galaxie application display a
     * song informations on screen.
     * @return if Galaxie application is displayed, return true. false otherwise.
     */
    public boolean getDisplayed() {
        return isGalaxieChannel;
    }

    /**
     * Called from EPG when selected channel is blocked.
     * @param id The source id of changed Channel.
     * @param type The channel type of changed channel.
     * @see com.videotron.tvi.illico.ixc.epg.ChannelEventListener#selectionBlocked(int, short)
     */
    public void selectionBlocked(int id, short type) {
        if (Log.INFO_ON) {
            Log.printInfo("ChannelEventController: selectionBlocked");
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("sourceID : " + id);
            Log.printDebug("type : " + type);
        }

        if (updaterThread != null) {
            updaterThread.interrupt();
            updaterThread = null;
        }

        stopTimer();
        UDPManager.getInstance().stop();
        BackgroundController.getInstance().cleanBackground();
        galaxieScreen.stop();
        EnrichedDataManager.getInstance().flushData();

        TvChannel currentCh = null;
        int number = 0;
        if (epgService != null) {
            try {
                currentCh = epgService.getChannel(id);
                number = currentCh.getNumber();
                if (Log.DEBUG_ON) {
                    Log.printDebug("ChannelEventController: selectionRequested: number : " + number);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        isGalaxieChannel = false;
        removeScreenSaverConfirmListner();
        FrameworkMain.getInstance().getImagePool().removeAbsolutely(Resources.CURRENT_COVER);
        FrameworkMain.getInstance().getImagePool().removeAbsolutely(Resources.NEXT_COVER);
        FrameworkMain.getInstance().getImagePool().remove(Resources.BACKGROUND);
        currentChannel = null;
    }

    /**
     * Called from EPG when request a selection of new channel. If type equals TvChannel.TYPE_GALAXIE, prepare to show a
     * galaxie screen of song informations and display a background still image. If not, hides galaxie screen and clear
     * a background still image and stop to receive a section for song informations.
     * @param id The source id of changed channel.
     * @param type The channel type of changed channel.
     * @see com.videotron.tvi.illico.ixc.epg.ChannelEventListener#selectionRequested(int, short)
     */
    public synchronized void selectionRequested(int id, short type) {
        if (Log.INFO_ON) {
            Log.printInfo("ChannelEventController: selectionRequested");
        }

        TvChannel currentCh = null;
        int number = 0;
        if (epgService != null) {
            try {
                currentCh = epgService.getChannel(id);
                currentChannel = currentCh;
                number = currentCh.getNumber();
                if (Log.DEBUG_ON && currentChannel != null) {
                    Log.printDebug("channel no : " + number + "\t call letter : " + currentChannel.getCallLetter());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (updaterThread != null) {
            updaterThread.interrupt();
            updaterThread = null;
        }

        stopTimer();
        UDPManager.getInstance().stop();
        galaxieScreen.stop();
        FrameworkMain.getInstance().getHScene().setVisible(false);
        BackgroundController.getInstance().cleanBackground();
        EnrichedDataManager.getInstance().flushData();
        
//        try {
//            Thread.sleep(10000L);
//        } catch (Exception e) {
//        }
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: selectionRequested: before : "
                    + FrameworkMain.getInstance().getImagePool().toString());
        }
        FrameworkMain.getInstance().getImagePool().removeAbsolutely(Resources.CURRENT_COVER);
        FrameworkMain.getInstance().getImagePool().removeAbsolutely(Resources.NEXT_COVER);

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: selectionRequested: after : "
                    + FrameworkMain.getInstance().getImagePool().toString());
        }

        if (monitorService != null) {
            String appName = FrameworkMain.getInstance().getApplicationName();
            int authorization = MonitorService.NOT_AUTHORIZED;
            try {
                authorization = monitorService.checkAppAuthorization(appName);
            } catch (RemoteException e) {
                Log.printWarning(e);
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("ChannelEventController: selectionRequested: authorization = " + authorization);
            }

            if (authorization == MonitorService.NOT_AUTHORIZED) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("ChannelEventController: selectionRequested: Galaxie not work because"
                            + " authorization");
                }
                return;
            }
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("sourceID : " + id);
            Log.printDebug("type : " + type);
        }

        // channelNO(source id, call letter)
        if (type == TvChannel.TYPE_GALAXIE || (number > 500 && number < 546)) {
            resetErrorData();
            isGalaxieChannel = true;

            if (currentChannel != null) {
                try {
                    BackgroundController.getInstance().displayTheme(String.valueOf(currentChannel.getNumber()));
                } catch (RemoteException e) {
                    Log.print(e);
                }
                updaterThread = new SongInfoUpdater(currentChannel);
                updaterThread.start();
            } else {
                if (Log.INFO_ON) {
                    Log.printInfo("CahnnelEventController: selectionRequested: currentChannel is null.");
                }
            }
            
//            if (monitorService != null) {
//                try {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("service state : " + monitorService.getState());
//                    }
//
//                    if (monitorService.getState() == MonitorService.FULL_SCREEN_APP_STATE) {
//                        galaxieScreen.start();
//                        addScreenSaverConfrimListener();
//                        FrameworkMain.getInstance().getHScene().setVisible(false);
//                        Log.printInfo("getHScene().setVisible(false)");
//                        // hideLoadingAnimation();
//                    } else {
//                        galaxieScreen.start();
//                        addScreenSaverConfrimListener();
//                    }
//                } catch (RemoteException e) {
//                    Log.printWarning(e);
//                }
//            }
        } else {
            isGalaxieChannel = false;
            removeScreenSaverConfirmListner();
            FrameworkMain.getInstance().getImagePool().remove(Resources.BACKGROUND);
            currentChannel = null;
        }
    }

    /**
     * Add a ScreenSaver confirm listener.
     */
    private void addScreenSaverConfrimListener() {
        if (monitorService == null) {
            monitorService = CommunicationManager.getInstance().getMonitorService();
        }

        if (monitorService != null) {
            try {
                monitorService.addScreenSaverConfirmListener(sListener, FrameworkMain.getInstance()
                        .getApplicationName());
            } catch (RemoteException e) {
                Log.printError(e);
            }
        }
    }

    /**
     * Remove a ScreenSaver confirm listener.
     */
    private void removeScreenSaverConfirmListner() {
        if (monitorService == null) {
            monitorService = CommunicationManager.getInstance().getMonitorService();
        }

        if (monitorService != null) {
            try {
                monitorService.removeScreenSaverConfirmListener(sListener, FrameworkMain.getInstance()
                        .getApplicationName());
            } catch (RemoteException e) {
                Log.printError(e);
            }
        }

    }

    /**
     * Gets the galaxie screen to show a song informations.
     * @return the galaxie screen
     */
    public GalaxieScreen getGalaxieScreen() {
        return galaxieScreen;
    }

    private void resetErrorData() {
        String error = DataCenter.getInstance().getString("itemNames.error");
        errorData.setAlbum(error);
        errorData.setArtist(error);
        errorData.setComposer(error);
        errorData.setNextSong(error);
        errorData.setTitle(error);
    }

    /**
     * The StatusChangeReceiver class implements ServiceStateListener to define by Monitor. The Monitor send newState to
     * application to add ServiceStateListener. if new state equals TV_VIEWING_STATE, Galaxie application's visible set
     * true when Galaxie application is shown. if new status is others, Galaixe application know that Galaixe
     * Application don't show a song information and Galaxie application's visible set false.
     */
    class StatusChangeReceiver implements ServiceStateListener {
        /**
         * Called by monitor if service state changed.
         * @param newState The new state from Monitor.
         * @throws RemoteException the remote exception if a remote stub class cannot be called.
         * @see com.videotron.tvi.illico.ixc.monitor.ServiceStateListener#stateChanged(int)
         */
        public void stateChanged(int newState) throws RemoteException {
            if (Log.DEBUG_ON) {
                Log.printDebug("StatusChangeReceiver: stateChanged: " + newState);
                Log.printDebug("StatusChangeReceiver: isGalaxieChannel: " + isGalaxieChannel);
            }

            boolean canVisible = false;
            if (monitorService != null) {
                String vcName = monitorService.getVideoContextName();
                if (Log.DEBUG_ON) {
                    Log.printDebug("StatusChangeReceiver: vcName = " + vcName);
                }
                if (vcName == null || vcName.equals("EPG")) {
                    canVisible = true;
                }
            }

            if (isGalaxieChannel) {
                Log.printDebug("before HScene visible is " + FrameworkMain.getInstance().getHScene().isShowing());
                if (newState != MonitorService.FULL_SCREEN_APP_STATE && canVisible) {
                    addScreenSaverConfrimListener();
                    FrameworkMain.getInstance().getHScene().setVisible(true);
                    galaxieScreen.requestFocus();
                    Log.printInfo("getHScene().setVisible(true)");
                    
                } else {
                    removeScreenSaverConfirmListner();
                    FrameworkMain.getInstance().getHScene().setVisible(false);
                    Log.printInfo("getHScene().setVisible(false)");
                }
                Log.printDebug("after HScene visible is " + FrameworkMain.getInstance().getHScene().isShowing());
            }
        }
    }

    /**
     * Starts a timer to use a delay time.
     * @param time a delay time.
     */
    private void startTimer(long time) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: startTimer: delayTime = " + time);
        }
        stopTimer();
        changeSongTimerSpec.setDelayTime(time);
        try {
            TVTimer.getTimer().scheduleTimerSpec(changeSongTimerSpec);
        } catch (TVTimerScheduleFailedException e) {
            e.printStackTrace();
        }
    }

    /**
     * stop timer.
     */
    private void stopTimer() {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: stopTimer()");
        }
        TVTimer.getTimer().deschedule(changeSongTimerSpec);
    }

    /**
     * Called when the current time reached a time to change next song.
     * @param event the event for changeSongTimerSpec
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: timerWentOff() : " + event.getTimerSpec().getTime() );
        }

        DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.CANADA);
        Date songDate;

        EnrichedMetaData data = EnrichedDataManager.getInstance().updateCurrentMedaData();
        if (data != null && event.getTimerSpec().getTime() != 2111L) {
            FrameworkMain.getInstance().getImagePool().remove(Resources.CURRENT_COVER);
        }
        
        EnrichedMetaData currentData = galaxieScreen.getCurrentSongData();
        
        if (currentData != null && data != null && currentData.getStartTime().equals(data.getStartTime())) {
        	startTimer(2111L);
        	return;
        } else if (data == null) {
        	startTimer(2111L);
        	return;
        }

        // Credit.
        if (data != null && ( data.getAnnouncement().equals("true") || (data.getArtist().equals("Galaxie") && data.getTitle().equals("Galaxie")))) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ChannelEventController: timerWentOff(): It's a station ad for Galaxie.");
            }
            galaxieScreen.updateData(null);
            FrameworkMain.getInstance().getImagePool().remove(Resources.CURRENT_COVER);

            try {
                songDate = df.parse(data.getStartTime());
                long currentTime = Calendar.getInstance().getTimeInMillis();
                long updateAfterTime = songDate.getTime() + data.getDuration() - currentTime;

                if (currentTime % 1000 != 0) {
                    updateAfterTime += 1000;
                }

                Log.printDebug("ChannelEventController: station ad updateAfterTime = " + updateAfterTime);

                if (updateAfterTime > 0) {
                    startTimer(updateAfterTime);
                } else {
                    startTimer(2111L);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return;
        }

        Image img = FrameworkMain.getInstance().getImagePool().get(Resources.NEXT_COVER);
        Log.printDebug("ChannelEventController: NEXT_COVER = " + img);
        if (img != null && img.getWidth(galaxieScreen) > -1) {
            FrameworkMain.getInstance().getImagePool().put(Resources.CURRENT_COVER, img);
            FrameworkMain.getInstance().getImagePool().removeAbsolutely(Resources.NEXT_COVER);

        } else if (data != null && event.getTimerSpec().getTime() != 2111L) {
            try {
                String backgroundId = BackgroundController.getInstance().getBackgroundId(
                        String.valueOf(currentChannel.getNumber()));
                byte[] imageData = BackgroundController.getInstance().getDefaultCover(backgroundId);
                FrameworkMain.getInstance().getImagePool().createImage(imageData, Resources.CURRENT_COVER);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        if (data != null) {
            galaxieScreen.updateData(data);
            try {
                songDate = df.parse(data.getStartTime());
                long currentTime = Calendar.getInstance().getTimeInMillis();
                long updateAfterTime = songDate.getTime() + data.getDuration() - currentTime;

                if (currentTime % 1000 != 0) {
                    updateAfterTime += 1000;
                } else {
                	updateAfterTime += 500;
                }

                Log.printDebug("ChannelEventController: updateAfterTime = " + updateAfterTime);

                if (updateAfterTime > 0) {
                    startTimer(updateAfterTime);
                } else {
                    startTimer(2111L);
                }
            } catch (ParseException e) {
                Log.print(e);
            }
        } else {
            galaxieScreen.updateData(errorData);
            startTimer(2111L);
        }

        // if (data == null) {
        // updaterThread.interrupt();
        // updaterThread = null;
        // updaterThread = new Thread(new SongInfoUpdater(currentChannel));
        // updaterThread.start();
        // }
    }

    public void setErrorData() {
        if (Log.INFO_ON) {
            Log.printInfo("ChannelEventController: setErrorData()");
        }

        galaxieScreen.updateData(errorData);
    }

    /**
     * The SongInfoUpdater class implements a functions to receive a song informations and cover data from back-end via
     * HTTP.
     * @author Woojung Kim
     * @version 1.1
     */
    class SongInfoUpdater extends Thread {
        /** The Galaxie channel when tuned. */
        private TvChannel myChannel;
        private boolean isLoadingState;

        /**
         * A Constructor of SongInfoUpdater.
         * @param TvChannel The current channel
         */
        public SongInfoUpdater(TvChannel channel) {
            this.myChannel = channel;
        }

        public boolean getIsLoadingState() {
            return isLoadingState;
        }

        /**
         * Updates a song informations from Galaxie back-end. Waits for waitTime because avoid making Unicast requests
         * for temporarily Galaxie tunned channels. Gets a EnrichedMetaData for song informations and a image for
         * current song's cover and updates a galaxieScreen.
         * @see java.lang.Runnable#run()
         */
        public void run() {

            DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.CANADA);
            Date songDate;

            byte[] imageData = null;
            EnrichedMetaData data = null;

            try {
                isLoadingState = true;
                // request loading animation
                if (Log.DEBUG_ON) {
                    Log.printDebug("SongInfoUpdater: isGalaxie = " + isGalaxieChannel + "\t isShowing = "
                            + FrameworkMain.getInstance().getHScene().isShowing());
                }

                String backgroundId = BackgroundController.getInstance().getBackgroundId(myChannel.getNumber() + "");
                
                imageData = BackgroundController.getInstance().getDefaultCover(backgroundId);
                FrameworkMain.getInstance().getImagePool().createImage(imageData, Resources.CURRENT_COVER);

                if (monitorService != null) {
                    try {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("service state : " + monitorService.getState());
                        }

                        if (monitorService.getState() == MonitorService.FULL_SCREEN_APP_STATE) {
                            galaxieScreen.start();
                            removeScreenSaverConfirmListner();
                            FrameworkMain.getInstance().getHScene().setVisible(false);
                            Log.printInfo("getHScene().setVisible(false)");
                            // hideLoadingAnimation();
                        } else {
                            galaxieScreen.start();
                            addScreenSaverConfrimListener();
                        }
                    } catch (RemoteException e) {
                        Log.printWarning(e);
                    }
                }

                Thread.sleep(waitingTime);

                if (myChannel != null) {
                    EnrichedDataManager.getInstance().flushData();
                    data = EnrichedDataManager.getInstance().getEnrichedMetaData(myChannel.getNumber());
                    if (Log.DEBUG_ON && data == null) {
                        Log.printDebug("SongInfoUpdater: data is null");
                    }
                    if (!currentChannel.getCallLetter().equals(myChannel.getCallLetter())) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("SongInfoUpdater: currentChannel call Letter = "
                                    + currentChannel.getCallLetter() + "\t myChannel call Letter = "
                                    + myChannel.getCallLetter());
                        }
                        return;
                    }
                    // Credit.
                    if (Log.DEBUG_ON) {
                    	Log.printDebug("SongInfoUpdater: run(): data announcement = " + data.getAnnouncement());
                    	Log.printDebug("SongInfoUpdater: run(): data artist = " + data.getArtist() + ", title = " + data.getTitle());
                    }
                    
                    if (data != null && ( data.getAnnouncement().equals("true") || (data.getArtist().equals("Galaxie") && data.getTitle().equals("Galaxie")))) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("SongInfoUpdater: run(): It's a station ad for Galaxie.");
                        }
                        galaxieScreen.updateData(null);

                        try {
                            songDate = df.parse(data.getStartTime());
                            long currentTime = Calendar.getInstance().getTimeInMillis();
                            long updateAfterTime = songDate.getTime() + data.getDuration() - currentTime;

                            if (currentTime % 1000 != 0) {
                                updateAfterTime += 1000;
                            }

                            Log.printDebug("SongInfoUpdater: station ad updateAfterTime = " + updateAfterTime);

                            if (updateAfterTime > 0) {
                                startTimer(updateAfterTime);
                            } else {
                                startTimer(2111L);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return;
                    }

                    if (data != null) {
                        imageData = SongInfoRequestor.getInstance().requestArtworkalbum(
                                data.getCategory() + data.getId(), backgroundId);
                    }

                    if (imageData == null) {
                        if (Log.WARNING_ON) {
                            Log.printWarning("SongInfoUpdater: Cannot fetch song poster.");
                        }
                    } else {
                        FrameworkMain.getInstance().getImagePool().createImage(imageData, Resources.CURRENT_COVER);
                    }
                    
                    String galaxieName = myChannel.getFullName();
                    if (isGalaxieChannel && data != null) {
                        // String galaxieName = DataCenter.getInstance().getString(galaixeId + "Name");

                        if (myChannel.getSourceId() == currentChannel.getSourceId()) {
                            galaxieScreen.setChannelData(galaxieName, "[ Ch." + myChannel.getNumber() + " ]");
                            galaxieScreen.updateData(data);

                            try {
                                songDate = df.parse(data.getStartTime());
                                long updateAfterTime = songDate.getTime() + data.getDuration()
                                        - Calendar.getInstance().getTimeInMillis();
                                if (updateAfterTime > 0) {
                                    startTimer(updateAfterTime);
                                } else {
                                    startTimer(2000L);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String galaxieId = "";
                            try {
                                galaxieId = DataCenter.getInstance().getString("no" + myChannel.getNumber());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            UDPManager.getInstance().start(galaxieId);

                        }
                    } else if (isGalaxieChannel) {
                        if (myChannel.getSourceId() == currentChannel.getSourceId()) {
                            galaxieScreen.setChannelData(galaxieName, "[ Ch." + myChannel.getNumber() + " ]");
                            galaxieScreen.updateData(errorData);
                        }
                    }
                }
            } catch (IOException e) {
                Log.printWarning(e);
            } catch (InterruptedException e) {
                Log.printWarning(e);
            } finally {
                isLoadingState = false;
            }
        }
    }

    /**
     * Implements ScreenSaverConfirmationListener. Galaxie returns always false if Galaxie is showing because Galaxie
     * has a self Screen Saver.
     * @author Woojung Kim
     * @version 1.1
     */
    class ScreenSaverConfirmationListenerImpl implements ScreenSaverConfirmationListener {

        /**
         * Confirm to launch Screen Saver.
         * @return Return true if Galaxie is not showing, false otherwise.
         * @throws RemoteException The Remote Exception
         */
        public boolean confirmScreenSaver() throws RemoteException {
            if (Log.DEBUG_ON) {
                Log.printDebug("ScreenSaverConfirmationListenerImpl: confirmScreenSaver: isGalaxie = "
                        + isGalaxieChannel + "\t isShowing = " + FrameworkMain.getInstance().getHScene().isShowing());
            }
            if (isGalaxieChannel && FrameworkMain.getInstance().getHScene().isShowing()) {
                return false;
            }
            return true;
        }
    }

    /**
     * Implements a InbandDataListener.
     * @author Woojung Kim
     * @version 1.1
     */
    class InbandDataListenerImpl implements InbandDataListener, DataUpdateListener {

        /**
         * Update a data via in-band when Monitor tuned a In-band channel for Data.
         * @param locator The locator for in-band data channel.
         */
        public void receiveInbandData(String locator) throws RemoteException {
            if (Log.DEBUG_ON) {
                Log.printDebug("InbandDataListenerImpl: receiveInbandData: locator = " + locator);
            }

            int newVersion = monitorService.getInbandDataVersion(MonitorService.galaxie_wallpapers);

            if (Log.DEBUG_ON) {
                Log.printDebug("InbandDataListenerImpl: receiveInbandData: currentVersion = " + currentVersion);
                Log.printDebug("InbandDataListenerImpl: receiveInbandData: newVersion = " + newVersion);
            }

            if (currentVersion != newVersion) {
                currentVersion = newVersion;
                DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
            }

            try {
                monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
            } catch (RemoteException e) {
                Log.printError(e);
            }
        }

        public void dataRemoved(String key) {
        }

        /**
         * Process a updated data.
         * @param key The key of update data.
         * @param old The old value
         * @param value The new value
         */
        public void dataUpdated(String key, Object old, Object value) {
            if (Log.DEBUG_ON) {
                Log.printDebug("InbandDataListenerImpl: dataUpdated: key = " + key);
            }
            if (key.equals(IN_BAND_DATA_PATH) && value != null) {
                DataParser.getInstance().updateData((File) value);
            }
        }
    }

    public void dataRemoved(String key) {

    }

    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelEventController: dataUpdated(): key = " + key + "\t old = " + old + "\t value = "
                    + value);
        }
        if (key.equals(EpgService.IXC_NAME)) {
            epgService = (EpgService) value;
            try {
                epgService.getChannelContext(0).addChannelEventListener(ChannelEventController.getInstance());
                if (Log.INFO_ON) {
                    Log.printInfo("EpgService Lookup completed");
                }
            } catch (RemoteException e) {
                Log.print(e);
            }
        } else if (key.equals(MonitorService.IXC_NAME)) {
            monitorService = (MonitorService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                monitorService.addInbandDataListener(inbandListner, appName);
                monitorService.addStateListener(statusChangeReceiver, appName);
                // monitorService.addScreenSaverConfirmListener(sListener, appName);
                if (Log.INFO_ON) {
                    Log.printInfo("MonitorService Lookup completed");
                }
            } catch (RemoteException e) {
                Log.print(e);
            }

        } else if (key.equals(PreferenceService.IXC_NAME)) {
            PreferenceService pService = (PreferenceService) value;
            try {
                String[] values = pService.addPreferenceListener(this,
                        FrameworkMain.getInstance().getApplicationName(), new String[] {PreferenceNames.LANGUAGE},
                        new String[] {Definitions.LANGUAGE_FRENCH});

                if (Log.DEBUG_ON) {
                    for (int i = 0; values != null && i < values.length; i++) {
                        Log.printDebug("ChannelEventController: dataUpdated(): values = " + values[i]);
                    }

                }

                DataCenter.getInstance().put(PreferenceNames.LANGUAGE, values[0]);
            } catch (RemoteException e) {
                Log.print(e);
            }
        } else if (key.equals(StcService.IXC_NAME)) {
            StcService stcService = (StcService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                int currentLevel = stcService.registerApp(appName);
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            Object o = SharedMemory.getInstance().get("stc-" + appName);
            if (o != null) {
                DataCenter.getInstance().put("LogCache", o);
            }
            try {
                stcService.addLogLevelChangeListener(appName, logListener);
            } catch (RemoteException ex) {
                Log.print(ex);
            }

        }
    }

    public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
        if (preferenceName.equals(PreferenceNames.LANGUAGE)) {
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
        }
    }

    /**
     * Receives a log level from STC application and apply.
     * @author Woojung Kim
     * @version 1.1
     */
    class LogLevelAdapter implements LogLevelChangeListener {

        /**
         * Receives a log level from STC application and apply.
         * @param logLevel logLevel to be updated.
         * @throws RemoteException throws exception
         */
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    /**
     * Receive a event of Power mode changed because if power mode change a LOW_POWER, Galaxie app will be hidden.
     * @param mode a mode of STB
     */
    public void powerModeChanged(int mode) {
        if (Log.INFO_ON) {
            Log.printInfo("ChannelEventController: powerModeChanged: mode = " + mode);
        }
        if (mode == Host.LOW_POWER) {
            if (isGalaxieChannel) {
                if (Log.DEBUG_ON) {
                    Log.printInfo("ChannelEventController: pwerModeChanged: isGalaxie = " + isGalaxieChannel);
                }
                FrameworkMain.getInstance().getHScene().setVisible(false);
                if (Log.DEBUG_ON) {
                    Log.printInfo("ChannelEventController: pwerModeChanged: Galaxie app is hided");
                }
            }
        }
    }
}

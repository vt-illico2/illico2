/**
 * @(#)BackgroundController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.controller;

import java.util.Hashtable;
import org.davic.resources.ResourceClient;
import org.davic.resources.ResourceProxy;
import org.havi.ui.HBackgroundConfiguration;
import org.havi.ui.HBackgroundDevice;
import org.havi.ui.HBackgroundImage;
import org.havi.ui.HConfigurationException;
import org.havi.ui.HPermissionDeniedException;
import org.havi.ui.HScreen;
import org.havi.ui.HStillImageBackgroundConfiguration;
import org.havi.ui.event.HBackgroundImageEvent;
import org.havi.ui.event.HBackgroundImageListener;

import com.videotron.tvi.illico.galaxie.data.BackgroundData;
import com.videotron.tvi.illico.galaxie.data.GalaxieChannel;
import com.videotron.tvi.illico.log.Log;

/**
 * The BackgroundController class applies different Background Theme based on channel change.
 * @author Woojung Kim
 * @version 1.1
 */
public final class BackgroundController implements HBackgroundImageListener, ResourceClient {

    /** The instance of BackgroundController. */
    private static BackgroundController instance = new BackgroundController();

    /** The BackgroundData class that stores Background theme. */
    private BackgroundData backgroundData;

    /**
     * Instantiates a new BackgroundController for Singleton.
     */
    private BackgroundController() {
        backgroundData = new BackgroundData();
        backgroundData.loadBackgroundTheme();
    }

    /**
     * Gets the single instance of BackgroundController.
     * @return single instance of BackgroundController
     */
    public static BackgroundController getInstance() {
        return instance;
    }

    /**
     * Called when {@link ChannelEventController] start.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundController: start");
        }
    }

    /**
     * Called When {@link ChannelEventController] stop.
     */
    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundController: stop");
        }
    }

    /**
     * Called when {@link ChannelEventController} dispose or stop(). backgroundData is flushed.
     */
    public void dispose() {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundController: dispose");
        }
        backgroundData.flushData();
    }

    /**
     * Load background theme.
     */
    public void loadBackgroundTheme() {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundController: loadBackgroundTheme");
        }
        backgroundData.loadBackgroundTheme();
    }

    /**
     * Sets the I-frame data. The sourceTable has a theme names id for sourceId. The themeTable has a iframe data for
     * theme name.
     * @param sourceTable the source table
     * @param themeTable the theme table
     */
    public void setIFrameData(Hashtable sourceTable, Hashtable themeTable) {
        if (Log.DEBUG_ON) {
            Log.printDebug("tememTable size : " + themeTable.size());
        }
        backgroundData.setIFrameData(sourceTable, themeTable);
    }

    /**
     * Display theme for sourceId.
     * @param chNo the channel number of current channel.
     */
    public void displayTheme(String chNo) {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundController: displayTheme");
        }

        byte[] data = backgroundData.getBackgroundTheme(chNo);

        String bgId = getBackgroundId(chNo);

        if (data != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("BackgroundController: displayTheme: data size : " + data.length);
            }
            ChannelEventController.getInstance().getGalaxieScreen().displayBackbround(data, bgId);
            // reserve code for next milestone
            // HBackgroundImage image = new HBackgroundImage(data);
            // image.load(this);
        } else {
            if (Log.INFO_ON) {
                Log.printInfo("BackgroundController: displayTheme: data is null");
            }
        }
    }

    /**
     * Returns a background id for source id.
     * @param chNo the channel number to know a background id.
     * @return the background id.
     */
    public String getBackgroundId(String chNo) {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundController: getBackgroundId : chNo = " + chNo);
        }
        GalaxieChannel gChannel = backgroundData.getGalaxieChannel(chNo);

        if (gChannel != null) {
            return gChannel.getBackgroundId();
        } else {
            return "1";
        }
    }

    /**
     * Return a byte array of default cover image for background Id(theme).
     * @param bgId the background id (theme)
     * @return The byte array of default cover image.
     */
    public byte[] getDefaultCover(String bgId) {
        return backgroundData.getDefaultCoverImage(bgId);
    }

    /**
     * Clean background. when enter channel type is not Galaxie, the Galaxie application must clean a background.
     */
    public void cleanBackground() {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundController: cleanBackground");
        }
        HStillImageBackgroundConfiguration bgConfig = getBackConfig();
        try {
            bgConfig.setColor(bgConfig.getColor());
        } catch (HPermissionDeniedException e) {
            Log.print(e);
        } catch (HConfigurationException e) {
            Log.print(e);
        }
    }

    /**
     * Invoked when loading of an HBackgroundImage fails.
     * @param event the event describing the failure
     * @see org.havi.ui.event.HBackgroundImageListener#imageLoadFailed(org.havi.ui.event.HBackgroundImageEvent)
     */
    public void imageLoadFailed(HBackgroundImageEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("BackgoundController: called imageLoadFailed");
        }
    }

    /**
     * Invoked when the data for an HBackgroundImage has been loaded.
     * @param event the event describing the loading
     * @see org.havi.ui.event.HBackgroundImageListener#imageLoaded(org.havi.ui.event.HBackgroundImageEvent)
     */
    public void imageLoaded(HBackgroundImageEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("BackgoundController: called imageLoaded");
        }
        HBackgroundImage image = (HBackgroundImage) event.getSource();
        drawBackground(image);
        image.flush();
    }

    /**
     * Draw background via HStillImageBackgroundConfiguration.
     * @param image the image to display.
     */
    private void drawBackground(HBackgroundImage image) {
        HStillImageBackgroundConfiguration bgConfig = getBackConfig();
        if (bgConfig == null) {
            return;
        }
        try {
            bgConfig.displayImage(image);
        } catch (Exception e) {
            Log.print(e);
        }
        releaseDevice();
    }

    /**
     * Gets the HStillImageBackgroundConfiguration. This class represents a background configuration which supports the
     * installation of still images.
     * @return the HStillImageBackgroundConfiguration to use to display HBackgroundImage.
     */
    private HStillImageBackgroundConfiguration getBackConfig() {
        HScreen screen = HScreen.getDefaultHScreen();
        HBackgroundDevice backDevice = screen.getDefaultHBackgroundDevice();
        if (!backDevice.reserveDevice(this)) {
            return null;
        }
        HBackgroundConfiguration config = backDevice.getCurrentConfiguration();
        if (config instanceof HStillImageBackgroundConfiguration) {
            return (HStillImageBackgroundConfiguration) config;
        }
        config = backDevice.getDefaultConfiguration();
        if (config instanceof HStillImageBackgroundConfiguration) {
            try {
                backDevice.setBackgroundConfiguration(config);
            } catch (Exception e) {
                backDevice.releaseDevice();
                return null;
            }
            return (HStillImageBackgroundConfiguration) config;
        }
        return null;
    }

    /**
     * Release device to control HBackgroundDevice.
     */
    private void releaseDevice() {
        HScreen screen = HScreen.getDefaultHScreen();
        HBackgroundDevice backDevice = screen.getDefaultHBackgroundDevice();
        backDevice.releaseDevice();
    }

    /**
     * A call to this operation informs the ResourceClient that proxy is about to lose access to a resource.
     * @param proxy the ResourceProxy representing the scarce resource to the application
     * @see org.davic.resources.ResourceClient#notifyRelease(org.davic.resources.ResourceProxy)
     */
    public void notifyRelease(ResourceProxy proxy) {
    }

    /**
     * A call to this operation notifies the ResourceClient that proxy has lost access to a resource.
     * @param proxy the ResourceProxy representing the scarce resource to the application
     * @see org.davic.resources.ResourceClient#release(org.davic.resources.ResourceProxy)
     */
    public void release(ResourceProxy proxy) {
    }

    /**
     * A call to this operation informs the ResourceClient that another application has requested the resource accessed
     * via the proxy parameter.
     * @param proxy the ResourceProxy representing the scarce resource to the application
     * @param requestData application specific data
     * @return If the ResourceClient decides to give up the resource following this call, it should terminate its usage
     *         of proxy and return True, otherwise False.
     * @see org.davic.resources.ResourceClient#requestRelease(org.davic.resources.ResourceProxy, java.lang.Object)
     */
    public boolean requestRelease(ResourceProxy proxy, Object requestData) {
        return false;
    }
}

/**
 * 
 */
package com.videotron.tvi.illico.galaxie.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zesyman
 *
 */
public class GalaxieXMLHandler extends DefaultHandler {

	/** The DateFormat instance to handle a start time of song in XML. */
	static DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.CANADA);
	
	Vector enrichVector = new Vector();
	
	long delay;
	
	public GalaxieXMLHandler(long delay) {
		this.delay = delay;
	}
	
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		if (qName.equalsIgnoreCase("ENTRY")) {
			EnrichedMetaData data = new EnrichedMetaData();
			
			String value = decodeXML(attributes.getValue("Album"));
			if (value != null) {
				data.setAlbum(value);
			}
			
			value = decodeXML(attributes.getValue("Artist"));
			if (value != null) {
				data.setArtist(value);
			}
			
			value = attributes.getValue("Category");
			if (value != null) {
				data.setCategory(value);
			}
			
			value = decodeXML(decodeXML(attributes.getValue("Composer")));
			if (value != null) {
				data.setComposer(value);
			}
			
			value = attributes.getValue("Duration");
			if (value != null) {
				data.setDuration(Integer.valueOf(value).intValue());
			}
			
			value = attributes.getValue("Event");
			if (value != null) {
				data.setEvent(value);
			}
			
			value = attributes.getValue("Id");
			if (value != null) {
				data.setId(value);
			}
			
			value = attributes.getValue("MusicId");
			if (value != null) {
				data.setMusicId(value);
			}
			
			value = attributes.getValue("StartTime");
			if (value != null) {
				data.setStartTime(value);
			}
			Date songDate = null;
			
			try {
				songDate = df.parse(data.getStartTime());

				if (Log.DEBUG_ON) {
					Log.printDebug("songDate's time : " + songDate.getTime());
					Log.printDebug("songDate : " + songDate.toString());
				}

				songDate.setTime(songDate.getTime() + delay);

				if (Log.DEBUG_ON) {
					Log.printDebug("compensated songDate's time : " + songDate.getTime());
					Log.printDebug("compensated songDate : " + songDate.toString());
				}
				data.setStartTime(df.format(songDate));

			} catch (ParseException e) {
				Log.printError(e);
			}
			
			value = decodeXML(attributes.getValue("Title"));
			if (value != null) {
				data.setTitle(value);
			}
			
			value = attributes.getValue("Type");
			if (value != null) {
				data.setType(value);
			}
			
			value = attributes.getValue("Announcement");
			if (value != null) {
				data.setAnnouncement(value);
			}
				
			enrichVector.add(data);
		}
	}
	
	public EnrichedMetaData[] getEnrichedMetaData() {
		EnrichedMetaData[] retVal = new EnrichedMetaData[enrichVector.size()];
		
		enrichVector.copyInto(retVal);
		
		enrichVector.clear();
		
		return retVal;
	}
	
	/**
	 * Decode a several special characters in XML.
	 * 
	 * @param src
	 *            token.
	 * @return token to be changed.
	 */
	private static String decodeXML(String src) {
		if (src == null) return null;
		
		String retVal = src;
		retVal = TextUtil.replace(retVal, "&apos;", "\'");
		retVal = TextUtil.replace(retVal, "&quot;", "\"");
		retVal = TextUtil.replace(retVal, "&amp;", "&");
		retVal = TextUtil.replace(retVal, "&lt;", "<");
		retVal = TextUtil.replace(retVal, "&gt;", ">");

		return retVal;
	}
}

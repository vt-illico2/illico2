/**
 * @(#)GalaxieChannelDataImpl.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GalaxieChannelDataImpl implements GalaxieChannelData {

    String currentTitle;
    String currentArtist;
    String currentAlbum;
    String nextTitle;
    String nextArtist;
    String nextAlbum;
    String coverUrl;
    String[] proxyIPs;
    int[] port;
    byte[] defaultCover;

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getCurrentTitle()
     */
    public String getCurrentTitle() throws RemoteException {
        return currentTitle;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getCurrentArtist()
     */
    public String getCurrentArtist() throws RemoteException {
        return currentArtist;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getCurrentAlbum()
     */
    public String getCurrentAlbum() throws RemoteException {
        return currentAlbum;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getNextTitle()
     */
    public String getNextTitle() throws RemoteException {
        return nextTitle;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getNextArtist()
     */
    public String getNextArtist() throws RemoteException {
        return nextArtist;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getNextAlbum()
     */
    public String getNextAlbum() throws RemoteException {
        return nextAlbum;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getCoverUrl()
     */
    public String getCoverUrl() throws RemoteException {
        return coverUrl;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getProxyIPs()
     */
    public String[] getProxyIPs() throws RemoteException {
        return proxyIPs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData#getProxyPorts()
     */
    public int[] getProxyPorts() throws RemoteException {
        return port;
    }

    public int[] getPort() throws RemoteException {
        return port;
    }

    public byte[] getDefaultCover() throws RemoteException {
        return defaultCover;
    }

    public void setPort(int[] port) {
        this.port = port;
    }

    public void setCurrentTitle(String title) {
        this.currentTitle = title;
    }

    public void setCurrentArtist(String artist) {
        this.currentArtist = artist;
    }

    public void setCurrentAlbum(String album) {
        this.currentAlbum = album;
    }

    public void setNextTitle(String title) {
        this.nextTitle = title;
    }

    public void setNextArtist(String artist) {
        this.nextArtist = artist;
    }

    public void setNextAlbum(String album) {
        this.nextAlbum = album;
    }

    public void setCoverUrl(String url) {
        this.coverUrl = url;
    }

    public void setProxyIPs(String[] ips) {
        this.proxyIPs = ips;
    }
    
    public void setDefaultCover(byte[] src) {
        defaultCover = src;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("[currentTitle : " + currentTitle);
        sb.append(", currentArtist : " + currentArtist);
        sb.append(", currentAlbum : " + currentAlbum);
        sb.append(", nextTitle : " + nextTitle);
        sb.append(", nextArtist : " + nextArtist);
        sb.append(", nextAlbum : " + nextAlbum);
        sb.append(", coverUrl : " + coverUrl);

        return sb.toString();
    }
}

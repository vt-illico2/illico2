/**
 * @(#)EnrichedMetadata.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

/**
 * The data structure of Enriched Metadata.
 * @author Woojung Kim
 * @version 1.1
 */
public class EnrichedMetaData {
    /** The album name. */
    private String album = "";
    /** The artist name. */
    private String artist = "";
    /** The category id. */
    private String category = "";
    /** The compoer name. */
    private String composer = "";
    /** The duration time. */
    private int duration = 0;
    /** The event id. */
    private String event = "";
    /** The id. */
    private String id = "";
    /** The music id. */
    private String musicId = "";
    /** The start time. */
    private String startTime = "";
    /** The title. */
    private String title = "";
    /** The type. */
    private String type = "";
    /** The next song title. */
    private String nextSong = "";
    
    /** The Announcement. */
    private String announcement = "";
    
    /**
     * Gets a album name.
     * @return the album name
     */
    public String getAlbum() {
        return album;
    }
    
    /**
     * Sets a album name.
     * @param name the album name
     */
    public void setAlbum(String name) {
        this.album = name;
    }
    
    /**
     * Gets a artist name.
     * @return the artist name 
     */
    public String getArtist() {
        return artist;
    }
    
    /**
     * Sets a artist name.
     * @param name the artist name.
     */
    public void setArtist(String name) {
        this.artist = name;
    }
    
    /**
     * Gets a category id.
     * @return The cateogry id.
     */
    public String getCategory() {
        return category;
    }
    
    /**
     * Set a category id.
     * @param c The category id.
     */
    public void setCategory(String c) {
        this.category = c;
    }
    
    /**
     * Get a composer name.
     * @return The composer name.
     */
    public String getComposer() {
        return composer;
    }
    
    /**
     * Sets a composer name.
     * @param name The composer name.
     */
    public void setComposer(String name) {
        this.composer = name;
    }
    
    /**
     * Gets a duration.
     * @return the duration.
     */
    public int getDuration() {
        return duration;
    }
    
    /**
     * Sets a duration.
     * @param d the duration Of song.
     */
    public void setDuration(int d) {
        this.duration = d;
    }
    
    /**
     * Gets a event id.
     * @return the event id.
     */
    public String getEvent() {
        return event;
    }
    
    /**
     * Sets a event id. 
     * @param e The event id.
     */
    public void setEvent(String e) {
        this.event = e;
    }
    
    /**
     * Gets a id.
     * @return The id.
     */
    public String getId() {
        return id;
    }
    
    /**
     * Sets a id.
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     * Get a music id.
     * @return The music id.
     */
    public String getMusicId() {
        return musicId;
    }
    
    /**
     * Sets a music id.
     * @param mId The music id.
     */
    public void setMusicId(String mId) {
        this.musicId = mId;
    }
    
    /**
     * Get a start time.
     * @return The start time.
     */
    public String getStartTime() {
        return startTime;
    }
    
    /**
     * Sets a start time.
     * @param time The start time.
     */
    public void setStartTime(String time) {
        this.startTime = time;
    }
    
    /**
     * Gets a title.
     * @return The title.
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * Sets a title.
     * @param t The title.
     */
    public void setTitle(String t) {
        this.title = t;
    }
    
    /**
     * Gets a type (Played, Playing, Not Play).
     * @return The type
     */
    public String getType() {
        return type;
    }
    
    /**
     * Sets a type.
     * @param t The type.
     */
    public void setType(String t) {
        this.type = t;
    }
    
    /**
     * Gets a next song.
     * @return the next song.
     */
    public String getNextSong() {
        return nextSong;
    }
    
    /**
     * Sets a next song.
     * @param next the next song.
     */
    public void setNextSong(String next) {
        this.nextSong = next;
    }
    
    /**
     * get a announcement.
     * @return the annoucement
     */
    public String getAnnouncement() {
    	return this.announcement;
    }
    
    public void setAnnouncement(String announcement) {
    	this.announcement = announcement;
    }
}

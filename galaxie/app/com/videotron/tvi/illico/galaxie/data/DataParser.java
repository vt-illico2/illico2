/**
 * @(#)DataParser.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.galaxie.communication.SongInfoRequestor;
import com.videotron.tvi.illico.galaxie.communication.UDPManager;
import com.videotron.tvi.illico.galaxie.controller.BackgroundController;
import com.videotron.tvi.illico.log.Log;

/**
 * The DataParser class parse a data for Galaxie from Management System.
 * @author Woojung Kim
 * @version 1.1
 */
public class DataParser extends ConvertUtil {

    /** The instance of DataParser for singleton. */
    private static DataParser instance = new DataParser();

    /**
     * Instantiates a new DataParser.
     */
    private DataParser() {
    }

    /**
     * Gets the single instance of DataParser.
     * @return the instance of DataParser
     */
    public static DataParser getInstance() {
        return instance;
    }

    /** The file name of informations for Galaxie channel in galaxie_wallpapers.zip. */
    private static final String FILE_GALAXIE_CHANNEL = "galaxie_wallpapers.txt";

    /** The buffer size. */
    private static final int BUFFER_SIZE = 1024;

    /** The current data that is a byte array to read galaxie_wallpapers.zip. */
    private byte[] currentData;

    /**
     * Finds a file of informations for Galaxie and request to build a data with informations file.
     * @param data the data
     */
    public void process(byte[] data) {
        if (Log.DEBUG_ON) {
            Log.printDebug("Dataparser: process()" + data.length);
        }

        currentData = data;

        int cnt = 0;
        byte[] buffer = new byte[BUFFER_SIZE];
        byte[] b = null;

        ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(data));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ZipEntry entry = null;
        try {
            while ((entry = zis.getNextEntry()) != null) {
                if (!entry.getName().equals(FILE_GALAXIE_CHANNEL)) {
                    continue;
                }
                try {
                    while ((cnt = zis.read(buffer)) != -1) {
                        baos.write(buffer, 0, cnt);
                    }

                    b = baos.toByteArray();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    baos.close();
                }
            }
            if (b != null) {
                buildGalaxieChannelInfo(b);
            }
        } catch (Exception e) {
            Log.printDebug("GAL506");
            Log.printError(e);
        } finally {
            try {
                baos.close();
            } catch (Exception e) {
                Log.print(e);
            }

            try {
                zis.close();
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Gets a IFrame or image data in zip file and return data in byte array.
     * @param name the name want to get.
     * @return the byte array of data name.
     */
    private byte[] getDataFromZip(String name) {
        if (Log.INFO_ON) {
            Log.printInfo("DataParser: getIframeData()");
        }

        int cnt = 0;
        byte[] buffer = new byte[BUFFER_SIZE];

        ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(currentData));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ZipEntry entry = null;
        byte[] b = null;
        try {
            while ((entry = zis.getNextEntry()) != null) {
                if (!entry.getName().equals(name)) {
                    continue;
                }
                try {
                    while ((cnt = zis.read(buffer)) != -1) {
                        baos.write(buffer, 0, cnt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            b = baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                baos.close();
            } catch (Exception e) {
                Log.print(e);
            }

            try {
                zis.close();
            } catch (Exception e) {
                Log.print(e);
            }
        }
        return b;
    }

    /**
     * builds the Galaxie channel information. the data will be handled in BackgroundController.
     * @param data the data of informations for Galaxie channel.
     */
    private void buildGalaxieChannelInfo(byte[] data) {
        if (Log.INFO_ON) {
            Log.printInfo("DataParser: buildGalaxieChannelInfo()");
        }
        try {
            int index = 0;
    
            // Version
            int version = oneByteToInt(data, index++);
            if (Log.DEBUG_ON) {
                Log.printDebug("version:" + version);
            }
    
            // prepare a iframe and default cover
            Hashtable themeTable = new Hashtable();
            int backgroundSize = oneByteToInt(data, index++);
            for (int i = 0; i < backgroundSize; i++) {
                int backgroundId = oneByteToInt(data, index++);
                int iframeNameLength = oneByteToInt(data, index++);
                String iframeName = byteArrayToString(data, index, iframeNameLength);
                index += iframeNameLength;
                themeTable.put(backgroundId + Resources.IFRAME, getDataFromZip(iframeName));
    
                int coverNameLength = oneByteToInt(data, index++);
                String coverName = byteArrayToString(data, index, coverNameLength);
                index += coverNameLength;
                themeTable.put(backgroundId + Resources.COVER, getDataFromZip(coverName));
    
                if (Log.DEBUG_ON) {
                    Log.printDebug("backgroundId : " + backgroundId + "\t iframeName : " + iframeName
                            + "\t cover : " + coverName);
    
                }
            }
    
            // prepare a channel information data
            Hashtable sourceTable = new Hashtable();
            int galaxieChannelSize = oneByteToInt(data, index++);
            for (int i = 0; i < galaxieChannelSize; i++) {
                String chNo = String.valueOf(twoBytesToInt(data, index));
                index += 2;
                int chNameLength = oneByteToInt(data, index++);
                String chName = byteArrayToString(data, index, chNameLength);
                index += chNameLength;
                int backgroundId = oneByteToInt(data, index++);
    
                if (Log.DEBUG_ON) {
                    Log.printDebug("GalxieChannel: chName : " + chName + "\t backgroundId : " + backgroundId);
                }
    
                GalaxieChannel channel = new GalaxieChannel();
                channel.setChannelNo(chNo);
                channel.setChannelName(chName);
                channel.setBackgroundId(String.valueOf(backgroundId));
    
                if (i == 0) {
                    sourceTable.put("DEFAULT", channel);
                }
                sourceTable.put(chNo, channel);
            }
    
            BackgroundController.getInstance().setIFrameData(sourceTable, themeTable);
    
            if (index >= data.length) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Dataparser: process(): index is over data length");
                }
                return;
            }
    
            // server info
            int serverIPLength = oneByteToInt(data, index++);
            String serverIP = byteArrayToString(data, index, serverIPLength);
            index += serverIPLength;
            int serverPort = twoBytesToInt(data, index);
            index += 2;
            int contextLength = oneByteToInt(data, index++);
            String context = byteArrayToString(data, index, contextLength);
            index += contextLength;
            SongInfoRequestor.getInstance().setServerInfo(serverIP, serverPort, context);
    
            // proxy info
            int proxyListLength = oneByteToInt(data, index++);
            Vector proxyVector = new Vector();
            for (int i = 0; i < proxyListLength; i++) {
                int proxyIpLength = oneByteToInt(data, index++);
                String ip = byteArrayToString(data, index, proxyIpLength);
                index += proxyIpLength;
                int port = twoBytesToInt(data, index);
                index += 2;
    
                ProxyInfo proxy = new ProxyInfo();
                proxy.setIp(ip);
                proxy.setPort(port);
                proxyVector.add(proxy);
            }
    
            ProxyInfo[] proxyInfos = new ProxyInfo[proxyVector.size()];
            proxyVector.copyInto(proxyInfos);
            SongInfoRequestor.getInstance().setProxyInfo(proxyInfos);
    
            int udpDataPort = twoBytesToInt(data, index);
            index += 2;
            int udpCoverPort = twoBytesToInt(data, index);
    
            UDPManager.getInstance().setPorts(udpDataPort, udpCoverPort);
        } catch (Exception e) {
            Log.printDebug("GAL505");
            Log.printError(e);
        }
    }

    /**
     * Update a data.
     * @param f The file that is loaded via in-band.
     */
    public void updateData(File f) {
        if (Log.DEBUG_ON) {
            Log.printDebug("DataParser: updateData() " + f.getName());
        }

        byte[] data = getByte(f);

        process(data);
    }

    public byte[] getByte(File f) {
        byte[] retVal = null;

        int size;
        byte[] buffer = new byte[BUFFER_SIZE];
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            fis = new FileInputStream(f);
            baos = new ByteArrayOutputStream();
            while ((size = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, size);
            }

            retVal = baos.toByteArray();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.close();
                }
            } catch (Exception e) {
                Log.print(e);
            }

            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
        return retVal;
    }
}

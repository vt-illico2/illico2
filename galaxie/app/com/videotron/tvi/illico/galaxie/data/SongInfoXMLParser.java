package com.videotron.tvi.illico.galaxie.data;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.parsers.SAXParserFactory;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.galaxie.controller.ChannelEventController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class handle a data to include a song informations in XML.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class SongInfoXMLParser {
	/** The DateFormat instance to handle a start time of song in XML. */
	static DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.CANADA);

	/**
	 * Return a EnrichedMetaData array to be parsed from XML.
	 * 
	 * @param src
	 *            The text of XML to include a song informations.
	 * @param delay
	 *            The delay time to synchronize with in-band audio content.
	 * @return The {@link EnrichedMetaData} arrays.
	 */
	public static EnrichedMetaData[] parseSongInfo(String src, long delay) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SongInfoXMLParser: parseSongInfo(): delay = " + delay);
			Log.printDebug(src);
		}

		EnrichedMetaData[] songs = null;
		try {
//			Vector songInfos = new Vector();
//			StringTokenizer st = new StringTokenizer(src, "<?/>");
//
//			EnrichedMetaData songInfo = null;
//			String type = "";
//			while (st.hasMoreElements()) {
//				String token = st.nextToken().trim();
//
//				// if (Log.DEBUG_ON) {
//				// Log.printDebug("|" + token + "|\t");
//				// }
//
//				if (token.startsWith("Entry ")) {
//					if (songInfo != null) {
//						songInfos.add(songInfo);
//					}
//					songInfo = new EnrichedMetaData();
//					type = "";
//				}
//				
//				int posEquals = token.indexOf("=");
//				
//				if (posEquals == -1) continue;
//				
//				String attributeKey = token.substring(0, posEquals);
//				String attributeValue = token.substring(posEquals);
//
//				if (attributeKey.equals("")) {
//				} else if (attributeKey.equals("Entry Album") || attributeKey.equals("Album")) {
//					type = "Album";
//				} else if (attributeKey.equals("Entry Artist") || attributeKey.equals("Artist")) {
//					type = "Artist";
//				} else if (attributeKey.equals("Entry Category") || attributeKey.equals("Category")) {
//					type = "Category";
//				} else if (attributeKey.equals("Entry Composer") || attributeKey.equals("Composer")) {
//					type = "Composer";
//				} else if (attributeKey.equals("Entry Duration") || attributeKey.equals("Duration")) {
//					type = "Duration";
//				} else if (attributeKey.equals("Entry Event") || attributeKey.equals("Event")) {
//					type = "Event";
//				} else if (attributeKey.equals("Entry Id") || attributeKey.equals("Id")) {
//					type = "Id";
//				} else if (attributeKey.equals("Entry MusicId") || attributeKey.equals("MusicId")) {
//					type = "MusicId";
//				} else if (attributeKey.equals("Entry StartTime") || attributeKey.equals("StartTime")) {
//					type = "StartTime";
//				} else if (attributeKey.equals("Entry Title") || attributeKey.equals("Title")) {
//					type = "Title";
//				} else if (attributeKey.equals("Entry Type") || attributeKey.equals("Type")) {
//					type = "Type";
//				} else if (attributeKey.equals("Entry Announcement") || attributeKey.equals("Announcement")) {
//					type = "Announcement";
//				}
//				
//				if (type != "") {
//					if (attributeValue == null || attributeValue.equals("")) {
//						continue;
//					} if (type.equals("Album")) {
//						attributeValue = decodeXML(attributeValue.trim());
//						songInfo.setAlbum(attributeValue);
//					} else if (type.equals("Artist")) {
//						attributeValue = decodeXML(attributeValue.trim());
//						songInfo.setArtist(attributeValue);
//					} else if (type.equals("Category")) {
//						songInfo.setCategory(attributeValue);
//					} else if (type.equals("Composer")) {
//						attributeValue = decodeXML(attributeValue);
//						songInfo.setComposer(attributeValue);
//					} else if (type.equals("Duration")) {
//						songInfo.setDuration(Integer.valueOf(attributeValue).intValue());
//					} else if (type.equals("Event")) {
//						songInfo.setEvent(attributeValue);
//					} else if (type.equals("Id")) {
//						songInfo.setId(attributeValue);
//					} else if (type.equals("MusicId")) {
//						songInfo.setMusicId(attributeValue);
//					} else if (type.equals("StartTime")) {
//						songInfo.setStartTime(attributeValue);
//	
//						Date songDate = null;
//	
//						try {
//							songDate = df.parse(songInfo.getStartTime());
//	
//							if (Log.DEBUG_ON) {
//								Log.printDebug("songDate's time : " + songDate.getTime());
//								Log.printDebug("songDate : " + songDate.toString());
//							}
//	
//							songDate.setTime(songDate.getTime() + delay);
//	
//							if (Log.DEBUG_ON) {
//								Log.printDebug("compensated songDate's time : " + songDate.getTime());
//								Log.printDebug("compensated songDate : " + songDate.toString());
//							}
//							songInfo.setStartTime(df.format(songDate));
//	
//						} catch (ParseException e) {
//							Log.printError(e);
//						}
//	
//					} else if (type.equals("Title")) {
//						attributeValue = decodeXML(attributeValue.trim());
//						songInfo.setTitle(attributeValue);
//					} else if (type.equals("Type")) {
//						songInfo.setType(attributeValue);
//					} else if (type.equals("Announcement")) {
//						songInfo.setAnnouncement(attributeValue);
//					}
//				}
//
//				type = "";
//			}
//
//			if (songInfo != null) {
//				songInfos.add(songInfo);
//			}
//
//			songs = new EnrichedMetaData[songInfos.size()];
//
//			songs = (EnrichedMetaData[]) songInfos.toArray(songs);
//
//			songInfos.clear();
			
			InputStream is = new ByteArrayInputStream(src.getBytes());
			
			GalaxieXMLHandler gHandler = new GalaxieXMLHandler(delay);
			SAXParserFactory.newInstance().newSAXParser().parse(is, gHandler);
			
			
			songs = gHandler.getEnrichedMetaData();

			if (Log.DEBUG_ON) {
				for (int i = 0; i < songs.length; i++) {
					Log.printDebug("=========================");
					Log.printDebug("album : " + songs[i].getAlbum());
					Log.printDebug("artist : " + songs[i].getArtist());
					Log.printDebug("category : " + songs[i].getCategory());
					Log.printDebug("composer : " + songs[i].getComposer());
					Log.printDebug("duration " + songs[i].getDuration());
					Log.printDebug("event : " + songs[i].getEvent());
					Log.printDebug("id : " + songs[i].getId());
					Log.printDebug("musicId : " + songs[i].getMusicId());
					Log.printDebug("startTime : " + songs[i].getStartTime());
					Log.printDebug("title : " + songs[i].getTitle());
					Log.printDebug("type : " + songs[i].getType());
				}
			}
		} catch (Exception e) {
			Log.printDebug("GAL504");
			Log.printError(e);
		}
		if (Log.EXTRA_ON) {
			Log.printDebug("RUN_TIME" + Resources.XML_PARSED_TIME + " = " + System.currentTimeMillis());
			DataCenter.getInstance().put(Resources.XML_PARSED_TIME, new Long(System.currentTimeMillis()));
		}
		return songs;
	}

	/**
	 * Return a {@link EnrichedMetaData} to include a current song informations.
	 * 
	 * @param songs
	 *            The array of song informations.
	 * @return The {@link EnrichedMetaData} to include a current song
	 *         informations.
	 */
	public static EnrichedMetaData getCurrentSongInfo(EnrichedMetaData[] songs) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): " + songs);
		}

		if (songs == null) {
			return null;
		}

		int index = -1;

		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		for (int i = 0; i < songs.length; i++) {

			Date songDate = null;
			try {
				songDate = df.parse(songs[i].getStartTime());
			} catch (ParseException e) {
				Log.printError(e);
				continue;
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): date time : " + date.getTime());
				Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): date : " + date.toString());
				Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): songDate's time : " + songDate.getTime());
				Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): songDate : " + songDate.toString());
			}

			if (songDate.after(date)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("i = " + i);
				}

				if (i > 0) {
					index = i - 1;

					if ("true".equals(songs[i].getAnnouncement())
							|| (songs[i].getArtist().equals("Galaxie") || songs[i].getTitle().equals("Galaxie"))) {
						if (Log.DEBUG_ON) {
							Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): next song is station AD.");
						}
						if (i + 1 < songs.length) {
							songs[index].setNextSong(songs[i + 1].getTitle());
						} else {
							songs[index].setNextSong(TextUtil.EMPTY_STRING);
						}
					} else {
						songs[index].setNextSong(songs[i].getTitle());
					}
				} else {
					index = i;
					if (i + 1 < songs.length) {
						if ("true".equals(songs[i + 1].getAnnouncement())
								|| (songs[i + 1].getArtist().equals("Galaxie") && songs[i + 1].getTitle().equals(
										"Galaxie"))) {
							songs[index].setNextSong(TextUtil.EMPTY_STRING);
						} else {
							songs[index].setNextSong(songs[i + 1].getTitle());
						}
					}
				}
				break;
			}
		}

		if (index == -1) {
			if (Log.DEBUG_ON) {
				Log.printDebug("index : " + index);
			}
			// index = songs.length - 1;
			return null;
		}

		if (Log.DEBUG_ON && songs[index] != null) {
			Log.printDebug("================================================================");
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): album : " + songs[index].getAlbum());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): artist : " + songs[index].getArtist());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): category : " + songs[index].getCategory());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): composer : " + songs[index].getComposer());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): duration " + songs[index].getDuration());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): event : " + songs[index].getEvent());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): id : " + songs[index].getId());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): musicId : " + songs[index].getMusicId());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): startTime : " + songs[index].getStartTime());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): title : " + songs[index].getTitle());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): type : " + songs[index].getType());
			Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): next : " + songs[index].getNextSong());
		}

		return songs[index];
	}

	/**
	 * Return a {@link EnrichedMetaData} to include a next song informations.
	 * 
	 * @param songs
	 *            The array of song informations.
	 * @return The {@link EnrichedMetaData} to include a next song informations.
	 */
	public static EnrichedMetaData getNextSongInfo(EnrichedMetaData[] songs) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): " + songs);
		}

		if (songs == null) {
			return null;
		}

		int index = -1;

		Calendar cal = Calendar.getInstance();
		// TODO temporarily apply a summer time.
		// cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + 1);

		// cal.setTimeInMillis(1266539236000L);
		Date date = cal.getTime();

		for (int i = 0; i < songs.length; i++) {

			Date tempDate = null;
			try {
				tempDate = df.parse(songs[i].getStartTime());
			} catch (ParseException e) {
				Log.printError(e);
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("SongInfoXMLParser: getNextSongInfo(): date time : " + date.getTime());
				Log.printDebug("SongInfoXMLParser: getNextSongInfo(): date : " + date.toString());
				Log.printDebug("SongInfoXMLParser: getNextSongInfo(): tempDate time : " + tempDate.getTime());
				Log.printDebug("SongInfoXMLParser: getNextSongInfo(): tempDate : " + tempDate.toString());
			}

			if (tempDate.after(date)) {
				index = i;

				if (i < songs.length - 1) {

					if ("true".equals(songs[i + 1].getAnnouncement())
							|| (songs[i + 1].getArtist().equals("Galaxie") && songs[i + 1].getTitle().equals("Galaxie"))) {
						if (Log.DEBUG_ON) {
							Log.printDebug("SongInfoXMLParser: getNextSongInfo(): next song is station AD.");
						}
						if (i + 2 < songs.length) {
							songs[index].setNextSong(songs[i + 2].getTitle());
						} else {
							songs[index].setNextSong(TextUtil.EMPTY_STRING);
						}
					} else {
						if ("true".equals(songs[i + 1].getAnnouncement())
								|| (songs[i + 1].getArtist().equals("Galaxie") && songs[i + 1].getTitle().equals(
										"Galaxie"))) {
							songs[index].setNextSong(TextUtil.EMPTY_STRING);
						} else {
							songs[index].setNextSong(songs[i + 1].getTitle());
						}

					}
				}
				break;
			}
		}

		if (index == -1) {
			if (Log.DEBUG_ON) {
				Log.printDebug("index : " + index);
			}
			// index = songs.length - 1;
			return null;
		}

		if (Log.DEBUG_ON && songs[index] != null) {
			Log.printDebug("================================================================");
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): album : " + songs[index].getAlbum());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): artist : " + songs[index].getArtist());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): category : " + songs[index].getCategory());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): composer : " + songs[index].getComposer());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): duration " + songs[index].getDuration());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): event : " + songs[index].getEvent());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): id : " + songs[index].getId());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): musicId : " + songs[index].getMusicId());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): startTime : " + songs[index].getStartTime());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): title : " + songs[index].getTitle());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): type : " + songs[index].getType());
			Log.printDebug("SongInfoXMLParser: getNextSongInfo(): next : " + songs[index].getNextSong());
		}

		return songs[index];
	}

	/**
	 * Decode a several special characters in XML.
	 * 
	 * @param src
	 *            token.
	 * @return token to be changed.
	 */
	private static String decodeXML(String src) {
		String retVal = src;
		retVal = TextUtil.replace(retVal, "&apos;", "\'");
		retVal = TextUtil.replace(retVal, "&quot;", "\"");
		retVal = TextUtil.replace(retVal, "&amp;", "&");
		retVal = TextUtil.replace(retVal, "&lt;", "<");
		retVal = TextUtil.replace(retVal, "&gt;", ">");

		return retVal;
	}
}

/**
 * @(#)GalaxieChannelDataManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.galaxie.communication.SongInfoRequestor;
import com.videotron.tvi.illico.galaxie.communication.UDPManager;
import com.videotron.tvi.illico.galaxie.controller.BackgroundController;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelDataUpdateListener;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class handle a {@link GalaxieChannelData}.
 * @author Woojung Kim
 * @version 1.1
 */
public class GalaxieChannelDataManager implements TVTimerWentOffListener {

    private static GalaxieChannelDataManager instance;

    /** The DateFormat instance to handle a start time of song in XML. */
    private DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.CANADA);

    /** When timer event occur, It means the time to change from current song to next song. */
    private TVTimerSpec changeSongTimerSpec;

    private GalaxieChannelDataUpdateListener listener;
    private int currenChannelNo;
    private EnrichedMetaData curEnrichedMetaData;

    private GalaxieChannelDataManager() {
        changeSongTimerSpec = new TVTimerSpec();
        changeSongTimerSpec.addTVTimerWentOffListener(this);
    }

    public static synchronized GalaxieChannelDataManager getInstance() {
        if (instance == null) {
            instance = new GalaxieChannelDataManager();
        }

        return instance;
    }

    public GalaxieChannelData getGalaxieChannelData(EnrichedMetaData[] songs, int chNo) {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: getEnrichedMetaData()");
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("GalaxieChannelDataManager: getEnrichedMetaData: songs = " + songs);
            Log.printDebug("GalaxieChannelDataManager: getEnrichedMetaData: channelNo = " + chNo);
        }

        if (chNo > -1) {
            currenChannelNo = chNo;
        }

        if (songs != null) {
            int index = -1;

            Calendar cal = Calendar.getInstance();
            Date date = cal.getTime();

            for (int i = 0; i < songs.length; i++) {
                Date songDate = null;
                try {
                    songDate = df.parse(songs[i].getStartTime());
                } catch (ParseException e) {
                    Log.printError(e);
                    continue;
                }

                if (Log.DEBUG_ON) {
                    Log.printDebug("GalaxieChannelDataManager: date time : " + date.getTime());
                    Log.printDebug("GalaxieChannelDataManager: date : " + date.toString());
                    Log.printDebug("GalaxieChannelDataManager: songDate's time : " + songDate.getTime());
                    Log.printDebug("GalaxieChannelDataManager: songDate : " + songDate.toString());
                }

                if (songDate.after(date)) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("i = " + i);
                    }

                    if (i > 0) {
                        index = i - 1;
                        songs[index].setNextSong(songs[i].getTitle());
                    } else {
                        index = i;
                        if (i + 1 < songs.length) {
                            songs[index].setNextSong(songs[i + 1].getTitle());
                        }
                    }
                    break;
                }
            }

            if (index == -1) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("index : " + index);
                }
                index = songs.length - 1;
            }

            if (Log.DEBUG_ON && songs[index] != null) {
                Log.printDebug("GalaxieChannelDataManager: ======= current song =========");
                Log.printDebug("GalaxieChannelDataManager: album : " + songs[index].getAlbum());
                Log.printDebug("GalaxieChannelDataManager: artist : " + songs[index].getArtist());
                Log.printDebug("GalaxieChannelDataManager: category : " + songs[index].getCategory());
                Log.printDebug("GalaxieChannelDataManager: composer : " + songs[index].getComposer());
                Log.printDebug("GalaxieChannelDataManager: duration " + songs[index].getDuration());
                Log.printDebug("GalaxieChannelDataManager: event : " + songs[index].getEvent());
                Log.printDebug("GalaxieChannelDataManager: id : " + songs[index].getId());
                Log.printDebug("GalaxieChannelDataManager: musicId : " + songs[index].getMusicId());
                Log.printDebug("GalaxieChannelDataManager: startTime : " + songs[index].getStartTime());
                Log.printDebug("GalaxieChannelDataManager: title : " + songs[index].getTitle());
                Log.printDebug("GalaxieChannelDataManager: type : " + songs[index].getType());
            }

            curEnrichedMetaData = songs[index];

            GalaxieChannelDataImpl data = new GalaxieChannelDataImpl();

            // current
            if (songs[index].getArtist().equals("Galaxie") && songs[index].getTitle().equals("Galaxie")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("SongInfoXMLParser: getCurrentSongInfo(): next song is station AD.");
                }
                data.setCurrentTitle(TextUtil.EMPTY_STRING);
                data.setCurrentArtist(TextUtil.EMPTY_STRING);
                data.setCurrentAlbum(TextUtil.EMPTY_STRING);
            } else {
                data.setCurrentTitle(songs[index].getTitle());
                data.setCurrentArtist(songs[index].getArtist());
                data.setCurrentAlbum(songs[index].getAlbum());
            }

            // next
            int nextIndex = index + 1;
            if (nextIndex < songs.length) {
                if (songs[nextIndex].getArtist().equals("Galaxie") && songs[nextIndex].getTitle().equals("Galaxie")) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("GalaxieChannelDataManager: getCurrentSongInfo(): next song is station AD.");
                    }
                    if (nextIndex + 1 < songs.length) {
                        data.setNextTitle(songs[nextIndex + 1].getTitle());
                        data.setNextArtist(songs[nextIndex + 1].getArtist());
                        data.setNextAlbum(songs[nextIndex + 1].getAlbum());
                    }
                } else {
                    data.setNextTitle(songs[nextIndex].getTitle());
                    data.setNextArtist(songs[nextIndex].getArtist());
                    data.setNextAlbum(songs[nextIndex].getAlbum());
                }
            }

            if (chNo > -1) {
                // cover url
                String backgroundId = BackgroundController.getInstance().getBackgroundId(chNo + "");
                String url = SongInfoRequestor.getInstance().getCoverUrl(
                        songs[index].getCategory() + songs[index].getId(), backgroundId);
                data.setCoverUrl(url);
            }

            // proxy server
            ProxyInfo[] info = SongInfoRequestor.getInstance().getProxyInfo();
            if (info != null) {
                int size = info.length;

                String[] ips = new String[size];
                int[] ports = new int[size];

                for (int i = 0; i < size; i++) {
                    ips[i] = info[i].getIp();
                    ports[i] = info[i].getPort();
                }

                data.setProxyIPs(ips);
                data.setPort(ports);
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("GalaxieChannelDataManager: data : " + data.toString());
            }
            
            String backgroundId = BackgroundController.getInstance().getBackgroundId(
                    String.valueOf(currenChannelNo));
            byte[] imageData = BackgroundController.getInstance().getDefaultCover(backgroundId);
            data.setDefaultCover(imageData);

            return data;
        }

        return null;
    }

    /**
     * Add a listener to update a data of Galaxie channel.
     * @param l listener to receive a updated data.
     */
    public void addGalaxieChannelDataUpdateListener(GalaxieChannelDataUpdateListener l) {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: addGalaxieChannelDataUpdateListener()");
        }

        listener = l;

        String galaxieId = DataCenter.getInstance().getString("no" + currenChannelNo);

        if (Log.DEBUG_ON) {
            Log.printDebug("GalaxieChannelDataManager: addGalaxieChannelDataUpdateListener(): listener = " + l);
            Log.printDebug("GalaxieChannelDataManager: addGalaxieChannelDataUpdateListener(): curEnrichedMetaData = "
                    + curEnrichedMetaData);
            Log.printDebug("GalaxieChannelDataManager: addGalaxieChannelDataUpdateListener(): galaxieId = " + galaxieId);
        }

        UDPManager.getInstance().start(galaxieId);

        if (curEnrichedMetaData != null) {
            Date songDate;
            try {
                songDate = df.parse(curEnrichedMetaData.getStartTime());
                long currentTime = Calendar.getInstance().getTimeInMillis();
                long updateAfterTime = songDate.getTime() + curEnrichedMetaData.getDuration() - currentTime;

                if (currentTime % 1000 != 0) {
                    updateAfterTime += 1000;
                }

                Log.printDebug("GalaxieChannelDataManager: updateAfterTime = " + updateAfterTime);

                if (updateAfterTime > 0) {
                    startTimer(updateAfterTime);
                } else {
                    startTimer(2000L);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            startTimer(2000L);
        }
    }

    public void removeGalaxieChannelDataUpdateListener() {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: removeGalaxieChannelDataUpdateListener()");
        }

        stopTimer();
        listener = null;
        UDPManager.getInstance().stop();
        DataCenter.getInstance().remove("NEXT_COVER");
    }

    public boolean isAddedListener() {
        if (listener != null) {
            return true;
        }

        return false;
    }

    public void notifyGalaxieChannelDataUpdated(GalaxieChannelData data) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: notifyGalaxieChannelDataUpdated : " + data);
        }
        if (listener != null) {
            listener.notifyGalaxieChannelDataUpdated(data);
        }
    }

    public void notifyCoverDataUpdated() throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: notifyCoverDataUpdated");
        }
        if (listener != null) {
            byte[] img = (byte[]) DataCenter.getInstance().get("NEXT_COVER");
            if (img != null) {
                listener.notifyCoverImageUpdated(img);
                DataCenter.getInstance().remove("NEXT_COVER");
            }
        }
    }

    /**
     * Starts a timer to use a delay time.
     * @param time a delay time.
     */
    private void startTimer(long time) {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: startTimer: delayTime = " + time);
        }
        stopTimer();
        changeSongTimerSpec.setDelayTime(time);
        try {
            TVTimer.getTimer().scheduleTimerSpec(changeSongTimerSpec);
        } catch (TVTimerScheduleFailedException e) {
            e.printStackTrace();
        }
    }

    /**
     * stop timer.
     */
    private void stopTimer() {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: stopTimer()");
        }
        TVTimer.getTimer().deschedule(changeSongTimerSpec);
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.INFO_ON) {
            Log.printInfo("GalaxieChannelDataManager: timerWentOff()");
        }

        EnrichedMetaData[] songList = EnrichedDataManager.getInstance().getSongList();

        GalaxieChannelData data = getGalaxieChannelData(songList, -1);

        if (data != null) {
            try {
                notifyGalaxieChannelDataUpdated(data);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            try {
                notifyCoverDataUpdated();
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            try {
                Date songDate = df.parse(curEnrichedMetaData.getStartTime());
                long currentTime = Calendar.getInstance().getTimeInMillis();
                long updateAfterTime = songDate.getTime() + curEnrichedMetaData.getDuration() - currentTime;

                if (currentTime % 1000 != 0) {
                    updateAfterTime += 1000;
                }

                Log.printDebug("GalaxieChannelDataManager: updateAfterTime = " + updateAfterTime);

                if (updateAfterTime > 0) {
                    startTimer(updateAfterTime);
                } else {
                    startTimer(2000L);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            startTimer(2000L);
        }

    }
}

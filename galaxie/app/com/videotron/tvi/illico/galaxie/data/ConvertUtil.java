/**
 * @(#)ConvertUtil.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

/**
 * The ConverUtil class provide to convert byte to int or String.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class ConvertUtil {
	/**
	 * Converts one byte to integer.
	 * @param data The byte array to use for converting.
	 * @param offset the offset that Converting start.
	 * @return The integer which is converted
	 */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]) & 0xFF);
    }

    /**
     * Converts two bytes to integer.
     * @param data The byte array to use for converting
     * @param offset the offset that Converting start.
     * @param byNegative if value is negative, byNegative is true, false otherwise.
     * @return The integer which is converted
     */
    public static int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
        return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) +
                (((int) data[offset + 1]) & 0xFF));
    }
    
    /**
     * Converts two bytes to integer.
     * @param data The byte array to use for converting
     * @param offset the offset that Converting start.
     * @return The integer which is converted
     */
    public static int twoBytesToInt(byte[] data, int offset) {
    	return twoBytesToInt(data, offset, false);
    }

    /**
     * Converts three bytes to integer.
     * @param data The byte array to use for converting.
     * @param offset The offset that converting start.
     * @return The integer which is converted.
     */
    public static int threeBytesToInt(byte[] data, int offset) {
    	return threeBytesToInt(data, offset, false);
    }
    
    /**
     * Converts three bytes to integer.
     * @param data The byte array to use for converting.
     * @param offset The offset that converting start.
     * @param byNegative if value is negative, byNegative is true, false otherwise.
     * @return The integer which is converted.
     */
    public static int threeBytesToInt(byte[] data, int offset, boolean byNegative) {
        return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 16) +
        		((((int) data[offset + 1]) & 0xFF) << 8) +
                (((int) data[offset + 2]) & 0xFF));
    }
    
    /**
     * Converts four bytes to integer.
     * @param data The byte array to use for converting.
     * @param offset The offset that converting start.
     * @param byNegative if value is negative, byNegative is true, false otherwise.
     * @return The integer which is converted.
     */
    public static int fourBytesToInt(byte[] data, int offset, boolean byNegative) {
        return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 24) +
        		((((int) data[offset + 1]) & 0xFF) << 16) +
        		((((int) data[offset + 2]) & 0xFF) << 8) +
                (((int) data[offset + 3]) & 0xFF));
    }
    
    /**
     * Converts four bytes to integer.
     * @param data The byte array to use for converting.
     * @param offset The offset that converting start.
     * @return The integer which is converted.
     */
    public static int fourBytesToInt(byte[] data, int offset) {
    	return fourBytesToInt(data, offset, false);
    }

    /**
     * Converts bytes to String.
     * @param data The byte array to user for converting.
     * @param offset The offset that converting start.
     * @param length The length that want to convert.
     * @return The String which is converted.
     */
    public static String byteArrayToString(byte[] data, int offset, int length) {
        String result = null;
        try {
            result = new String(data, offset, length, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

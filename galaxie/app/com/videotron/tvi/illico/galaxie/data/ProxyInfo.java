/**
 * @(#)ProxyInfo.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

/**
 * This class have a information for Proxy IP and Port.
 * @author Woojung Kim
 * @version 1.1
 */
public class ProxyInfo {
    private String ip;
    private int port;
    
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }
}

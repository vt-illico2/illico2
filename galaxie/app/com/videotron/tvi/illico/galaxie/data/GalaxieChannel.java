/**
 * @(#)GalaxieChannel.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

/**
 * The GalaxieChannel class represent a Galaxie channel information to determine a theme.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class GalaxieChannel {
    
    /** The channel number of Galaxie channel. */
    private String chNo;

    /** The channel name of Galaxie channel. */
    private String chName;

    /** The theme id of Galaxie channel. */
    private String backgroundId;

    /**
     * Gets a channel number.
     * 
     * @return the channel number.
     */
    public String getChannelNo() {
        return chNo;
    }

    /**
     * Sets a channel number.
     * 
     * @param name
     *            The channel number of Galaxie channel.
     */
    public void setChannelNo(String no) {
        this.chNo = no;
    }
    
    /**
     * Gets a channel name.
     * 
     * @return the channel name.
     */
    public String getChannelName() {
        return chName;
    }

    /**
     * Sets a channel name.
     * 
     * @param name
     *            The channel name of Galaxie channel.
     */
    public void setChannelName(String name) {
        this.chName = name;
    }

    /**
     * Gets a background id of Galaxie channel.
     * 
     * @return The id of background theme.
     */
    public String getBackgroundId() {
        return backgroundId;
    }

    /**
     * Sets a id of background theme for Galxie channel.
     * 
     * @param bId
     *            the id of background theme.
     */
    public void setBackgroundId(String bId) {
        this.backgroundId = bId;
    }
}

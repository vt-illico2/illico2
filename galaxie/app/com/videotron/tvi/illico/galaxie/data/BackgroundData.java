/**
 * @(#)BackgroundData.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

import java.io.File;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.galaxie.Resources;
import com.videotron.tvi.illico.log.Log;

/**
 * The BackgroundData class stores Background themes and source to map themes.
 * @author Woojung Kim
 * @version 1.1
 */
public class BackgroundData {

    /** The source table to map a themes. */
    private Hashtable sourceTable = new Hashtable();

    /** The theme table for Galaxie channel. */
    private Hashtable themeTable = new Hashtable();

    private byte[] defaultTheme;
    private byte[] defaultCover;

    /**
     * Gets the background theme for source id and language type.
     * @param chNo the channel number of current channel.
     * @return the byte array to find a background theme
     */
    public byte[] getBackgroundTheme(String chNo) {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundData: getBackgoundTheme");
        }
        if (Log.DEBUG_ON && themeTable != null) {
            Log.printDebug("BackgroundData: getBackgoundTheme: themeTable size = " + themeTable.size());
        }

        GalaxieChannel gChannel = getGalaxieChannel(chNo);

        byte[] data = null;
        if (gChannel != null) {
            data = (byte[]) themeTable.get(gChannel.getBackgroundId() + Resources.IFRAME);
        }

        if (data != null) {
            return data;
        } else {
            Log.printDebug("GAL505");
            return defaultTheme;
        }
    }

    /**
     * Returns a byte array to paint a default cover.
     * @param bgId The id of background theme.
     * @return The byte array to find a default cover.
     */
    public byte[] getDefaultCoverImage(String bgId) {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundData: getDefaultCoverImage");
        }

        if (Log.DEBUG_ON && themeTable != null) {
            Log.printDebug("BackgroundData: theme: " + themeTable.toString());
        }

        byte[] data = (byte[]) themeTable.get(bgId + Resources.COVER);

        if (data != null) {
            Log.printDebug("BackgroundData: getDefaultCoverImage: data size " + data.length);
            return data;
        } else {
            Log.printDebug("GAL505");
            Log.printDebug("BackgroundData: getDefaultCoverImage: data is null, so use default to be "
                    + "includes in Galaixe");
            return defaultCover;
        }
    }

    /**
     * Returns a found Galaxie channel via sourceId.
     * @param chNo the channel number to find a Galaxie channel.
     * @return The found Galaxie channel
     */
    public GalaxieChannel getGalaxieChannel(String chNo) {
        GalaxieChannel gChannel = null;

        if (sourceTable.containsKey(chNo)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("BackgroundData: getGalaxieChannel: find galaxieChannel for callLetter");
            }
            gChannel = (GalaxieChannel) sourceTable.get(chNo);
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("BackgroundData: getGalaxieChannel: can't find galaxieChannel, so use"
                        + " default");
            }
            gChannel = (GalaxieChannel) sourceTable.get("DEFAULT");
        }

        return gChannel;
    }

    /**
     * Load a default background theme.
     */
    public void loadBackgroundTheme() {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundData: loadBackgroundTheme");
        }
        String defaultThemeName = (String) DataCenter.getInstance().get("defaultThemeName");
        String defaultCoverName = (String) DataCenter.getInstance().get("defaultCoverName");
        defaultTheme = DataParser.getInstance().getByte(new File("resource/image/" + defaultThemeName));
        defaultCover = DataParser.getInstance().getByte(new File("resource/image/" + defaultCoverName));
    }

    /**
     * Sets the Iframe data for sourceId and themes.
     * @param sTable the Hashtable has a mapping information for sourceId.
     * @param tTable the Hashtable has a mapping information for theme.
     */
    public void setIFrameData(Hashtable sTable, Hashtable tTable) {
        if (Log.INFO_ON) {
            Log.printInfo("BackgroundData: setIFrameData");
        }
        this.sourceTable = sTable;
        this.themeTable = tTable;
    }

    /**
     * Flush data which is a mapping table for sourceId and themes.
     */
    public void flushData() {
        if (sourceTable != null) {
            sourceTable.clear();
        }
        if (themeTable != null) {
            themeTable.clear();
        }
    }
}

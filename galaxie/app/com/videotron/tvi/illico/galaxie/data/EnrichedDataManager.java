/**
 * @(#)EnrichedDataReceiver.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie.data;

import java.io.IOException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.galaxie.communication.SongInfoRequestor;
import com.videotron.tvi.illico.log.Log;

/**
 * The EnrichedDataReceiver class used to request and receive Enriched Metadata from Galaxie Backend System.
 * @author Woojung Kim
 * @version 1.1
 */
public final class EnrichedDataManager {

    /** The instance of EnrichedDataReceiver for singleton. */
    private static EnrichedDataManager instance = new EnrichedDataManager();

    /** The data of the enriched metadata for current song. */
    private EnrichedMetaData currentMetaData;
    /** the data of the enriched metadata for next song. */
    private EnrichedMetaData nextMetaData;
    /** The data list of the last. */
    private EnrichedMetaData[] songList;
    /** The delay time from H/E. */
    private long delayTime = 17000;

    /**
     * Instantiates a new enriched data receiver.
     */
    private EnrichedDataManager() {
        delayTime = Long.valueOf((String) DataCenter.getInstance().get("delayTime")).longValue();
    }

    /**
     * Gets the single instance of EnrichedDataReceiver.
     * @return the instance of EnrichedDataReceiver
     */
    public static EnrichedDataManager getInstance() {
        return instance;
    }

    /**
     * Gets the enriched metadata.
     * @param channelNo the channel number of current galaxie channel
     * @return the enriched metadata to receive from Galaxie back-end.
     * @throws IOException the exception to occur for requesting data to back-end.
     */
    public EnrichedMetaData getEnrichedMetaData(int channelNo) throws IOException {
        if (Log.DEBUG_ON) {
            Log.printDebug("EnrichedDataManager: getEnrichedMetaData(int): channelNo = " + channelNo);
        }
        if (currentMetaData == null) {
            String xmlString = null;
            String galaixeId = DataCenter.getInstance().getString("no" + channelNo);

            if (Log.DEBUG_ON) {
                Log.printDebug("EnrichedDataManager: getEnrichedMetaData(int): galaixeId = " + galaixeId);
            }
            try {
                if (galaixeId != null) {
                    xmlString = SongInfoRequestor.getInstance().requestSongList(galaixeId);
                    EnrichedMetaData[] songs = SongInfoXMLParser.parseSongInfo(xmlString, delayTime);

                    if (songs != null) {
                        songList = songs;
                    }
                    currentMetaData = SongInfoXMLParser.getCurrentSongInfo(songList);
                    if (Log.DEBUG_ON && currentMetaData != null) {
                        Log.printDebug("EnrichedDataManager: getEnrichedMetaData(int): currentMetaData = "
                                + currentMetaData.getTitle());
                    }

                    nextMetaData = SongInfoXMLParser.getNextSongInfo(songList);

                    if (Log.DEBUG_ON && nextMetaData != null) {
                        Log.printDebug("EnrichedDataManager: getEnrichedMetaData(int): nextMetaData = "
                                + nextMetaData.getTitle());
                    }

                    return currentMetaData;
                }
            } catch (IOException e) {
                throw e;
            }
        }
        return currentMetaData;
    }

    /**
     * Returns a current metadata from cached songList.
     * @return The current metadata
     */
    public EnrichedMetaData getEnrichedMetaData() {
        if (Log.DEBUG_ON) {
            Log.printDebug("EnrichedDataManager: getEnrichedMetaData()");
        }
        if (songList != null) {
            currentMetaData = SongInfoXMLParser.getCurrentSongInfo(songList);
            if (Log.DEBUG_ON) {
                Log.printDebug("EnrichedDataManager: getEnrichedMetaData(): currentMetaData = "
                        + currentMetaData.getTitle());
            }
            return currentMetaData;
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("EnrichedDataManager: getEnrichedMetaData(): songList is null.");
            }
        }
        return null;
    }

    /**
     * Update the next song data to receive from Galaxie back-end by UDP.
     * @param data the new enriched metadata from Galaxie back-end
     */
    public void updateNextMetaData(String data) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EnrichedDataManager: updateNextMetadata(): data = " + data);
        }

        EnrichedMetaData[] songs = SongInfoXMLParser.parseSongInfo(data, delayTime);
        if (songs != null) {
            songList = songs;
        }
        nextMetaData = SongInfoXMLParser.getNextSongInfo(songList);

        if (Log.DEBUG_ON && nextMetaData != null) {
            Log.printDebug("EnrichedDataManager: updateNextMetadata(): nextMetaData = " + nextMetaData.getTitle());
        }

        if (nextMetaData != null && !nextMetaData.getAnnouncement().equals("true")&& currentMetaData != null ) {
            currentMetaData.setNextSong(nextMetaData.getTitle());
        } else if (nextMetaData != null && nextMetaData.getAnnouncement().equals("true")&& currentMetaData != null ) {
            
        	if (songList != null) {
        		int index = 0;
	        	for (int i = 0; i < songList.length; i++) {
	        		if (songList[i].getStartTime().equals(nextMetaData.getStartTime())) {
	        			index = i;
	        			break;
	        		}
	        	}
	        	if (index + 1 < songList.length) {
	        		currentMetaData.setNextSong(songList[index + 1].getTitle());
	        	}
        	} else {
        		currentMetaData.setNextSong("");
        	}
        }
    }

    /**
     * Update a nextMetaData to currentMetaData.
     * @return The currentMetaData.
     */
    public EnrichedMetaData updateCurrentMedaData() {
        if (Log.DEBUG_ON) {
            Log.printDebug("EnrichedDataManager: updateCurrentMetaData()");
        }
        currentMetaData = null;

        if (nextMetaData != null && !nextMetaData.getAnnouncement().equals("true")) {
            currentMetaData = new EnrichedMetaData();
            currentMetaData.setAlbum(nextMetaData.getAlbum());
            currentMetaData.setArtist(nextMetaData.getArtist());
            currentMetaData.setCategory(nextMetaData.getCategory());
            currentMetaData.setComposer(nextMetaData.getComposer());
            currentMetaData.setDuration(nextMetaData.getDuration());
            currentMetaData.setEvent(nextMetaData.getEvent());
            currentMetaData.setId(nextMetaData.getId());
            currentMetaData.setMusicId(nextMetaData.getMusicId());
            currentMetaData.setNextSong(nextMetaData.getNextSong());
            currentMetaData.setStartTime(nextMetaData.getStartTime());
            currentMetaData.setTitle(nextMetaData.getTitle());
            currentMetaData.setType(nextMetaData.getType());

            nextMetaData = null;
            if (Log.DEBUG_ON) {
                Log.printDebug("EnrichedDataManager: updateCurrentMedaData(): currentMetaData updated.");
            }
        } else {
            currentMetaData = SongInfoXMLParser.getCurrentSongInfo(songList);
            
            if (currentMetaData == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("EnrichedDataManager: updateCurrentMedaData(): nextMetaData is null so currentMetaData" +
                    		" can't be updated.");
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("EnrichedDataManager: updateCurrentMedaData(): currentMetaData updated.");
                }
            }
        }

        return currentMetaData;
    }

    /**
     * Get a song list.
     * @return song list.
     */
    public EnrichedMetaData[] getSongList() {
        return songList;
    }

    /**
     * Flush data to receive from Galaxie back-end.
     */
    public void flushData() {
        currentMetaData = null;
        nextMetaData = null;
        songList = null;
    }
}

/**
 * @(#)App.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;
import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.galaxie.controller.ChannelEventController;
import com.videotron.tvi.illico.log.Log;

/**
 * implements Xlet of Galaxie Application.
 * @author zestyman
 * @version 1.1
 */
public class App implements Xlet, ApplicationConfig {

    /**
     * The instance of XletContext.
     */
    private XletContext xletContext;

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional If unconditional is true when this method is called, requests by the Xlet to not enter the
     *            destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *             state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("destroyXlet");
        }
        FrameworkMain.getInstance().destroy();
        ChannelEventController.getInstance().dispose();
    }

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param xc The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext xc) throws XletStateChangeException {
        this.xletContext = xc;
        FrameworkMain.getInstance().init(this);
        if (Log.INFO_ON) {
            Log.printInfo("initXlet");
        }
        ChannelEventController.getInstance().init(xletContext);
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        if (Log.INFO_ON) {
            Log.printInfo("pauseXlet");
        }
        FrameworkMain.getInstance().pause();
        ChannelEventController.getInstance().stop();
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("startXlet");
        }
        FrameworkMain.getInstance().start();
        ChannelEventController.getInstance().start();
    }

    /**
     * Implements getApplicationName of {@link ApplicationConfig}.
     * @return the name of Galaxie Application, "Galaxie"
     * @link {@link com.videotron.tvi.illico.framework.ApplicationConfig#getApplicationName()}
     */
    public String getApplicationName() {
        return "Galaxie";
    }

    /**
     * Implements getVersion of ApplicationConfig.
     * @return the version of Galaxie Application
     * @link {@link com.videotron.illico.framework.ApplicationConfig#getVersion()}
     */
    public String getVersion() {
        return "R7.1.1";
    }

    /**
     * Implements getXletContext of ApplicationConfig.
     * @return the XletContext of Galaxie Application
     * @link {@link com.videotron.tvi.illico.framework.ApplicationConfig#getXletContext()}
     */
    public XletContext getXletContext() {
        return xletContext;
    }

	public void init() {
		
	}
}

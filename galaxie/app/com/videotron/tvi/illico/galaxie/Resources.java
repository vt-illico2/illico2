/**
 * @(#)Resources.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.galaxie;

/**
 * The Resources class has a common texts and constants.
 * @author Woojung Kim
 * @version 1.1
 */
public final class Resources {

    /** The name of cover images from back-end. */
    public static final String COVER = "COVER";
    /** The suffix of i-frame name from MS. */
    public static final String IFRAME = "IFRAME";
    /** The name of current song's cover. */
    public static final String CURRENT_COVER = "CURRENT_COVER";
    /** The name of next song's cover. */
    public static final String NEXT_COVER = "NEXT_COVER";
    /** The name of background image. */
    public static final String BACKGROUND = "BACKGROUND";

    // TO performance test.
    public static final String HTTP_START_TIME = "HTTP_START";
    public static final String XML_REQUEST_TIME = "XML_REQUEST";
    public static final String XML_RECEIVED_TIME = "XML_RECEIVE";
    public static final String XML_PARSED_TIME = "XML_PARSED";
    public static final String TOTAL_TIME = "TOTAL";
    public static final String IMAGE_REQUEST_TIME = "IMG_REQUEST";
    public static final String IMAGE_RECEIVED_TIME = "IMG_RECEIVE";
    
    /**
     * The Constructor of Util class.
     */
    private Resources() {
    }
}

This is Galaxie App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version 1.2.0.0_P1
    - Release App file Name
      : R3_Galaxie.zip
    - Release type (official, debug, test,...)
      : test
    - Release date
      : 2013.09.13

***
version  1.2.0.0_P1
    - New Feature
        + implements that a new field called Announcement="true" should be used from the XML playlist to identify any station announcements (DDC-i2-072) 
    - Resolved Issues
        N/A
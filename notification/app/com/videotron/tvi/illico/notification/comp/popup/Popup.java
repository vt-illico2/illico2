package com.videotron.tvi.illico.notification.comp.popup;

import java.util.Calendar;
import java.util.Date;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.controller.Controller;
import com.videotron.tvi.illico.notification.data.DataManager;
import com.videotron.tvi.illico.notification.data.ScheduleData;

/**
 * The Class Popup.
 * @author pellos
 */
abstract public class Popup extends UIComponent implements TVTimerWentOffListener {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private TVTimerSpec searchTimer;
    protected ScheduleData scheduleData;

    protected static final int FORCE_START = 0;
    protected static final int ORDER_START = 1;

    protected static final long ONE_SCEND = 1000L;
    protected long startTime = 0;

    /** is a second. */
    protected long popupBetween = 0;
    protected int reminderSum = 0;
    protected long delayTimeOfDisplay;
    protected long deleteTime;
    protected long allDisplayDelayTime;
    protected static long startedTime;

    protected int reminderCount = 0;

    protected boolean isAction = false;
    protected boolean acceptCategories = false;
    protected long countDown = 0;

    protected int priority = 1;

    private Object ob = new Object();

    /**
     * If it have already shown a pop-up on screen, It is true
     */
    private boolean doublePopupDelay = false;

    /**
     * If it is the time-sleep state, It is true.
     */
    private static boolean blocked = false;

    private static String blockedMessageID = "";

    protected static boolean ForceStart = false;

    public void init(ScheduleData data) {
        Log.printInfo("popup create init" + data.getMessageID());
        scheduleData = data;
        priority = scheduleData.getPriority();
        reminderSum = scheduleData.getRemainderTime();
        if (reminderSum == 0) {
            reminderSum = 1;
        }

        // 팝업간의 사이 간격
        popupBetween = scheduleData.getRemainderDelay();
        // 팝업 지속 시간. property file
        delayTimeOfDisplay = DataManager.getInstance().getDisplayTime();

        // 팝업이 죽기전까지 시간.
        allDisplayDelayTime = (delayTimeOfDisplay * reminderSum + popupBetween * (reminderSum - 1));
        
        if (scheduleData.getCountTime() != ScheduleData.DEFAULT_TIME) {
            delayTimeOfDisplay = scheduleData.getCountTime();
            allDisplayDelayTime = allDisplayDelayTime - (ScheduleData.DEFAULT_TIME - delayTimeOfDisplay);
            if (allDisplayDelayTime > ScheduleData.DEFAULT_TIME) {
                scheduleData.setCountTime(allDisplayDelayTime);
            } else {
                scheduleData.setCountTime(ScheduleData.DEFAULT_TIME);
            }
            startTime = scheduleData.getDisplayTime();
        } else {
            long gap = scheduleData.getDisplayTime() - System.currentTimeMillis();
            if (gap < delayTimeOfDisplay) {
                if (gap < 0) {
                    gap = 0;
                }
                startTime = scheduleData.getDisplayTime() - gap;
                Log.printDebug("gap = " + gap);
                allDisplayDelayTime = allDisplayDelayTime - (ScheduleData.DEFAULT_TIME - gap);
                delayTimeOfDisplay = delayTimeOfDisplay - (ScheduleData.DEFAULT_TIME - gap);
                if (allDisplayDelayTime == 0) {
                    allDisplayDelayTime = DataManager.getInstance().getDisplayTime();
                }

                if (delayTimeOfDisplay == 0) {
                    delayTimeOfDisplay = DataManager.getInstance().getDisplayTime();
                }

            } else {
                startTime = scheduleData.getDisplayTime() - delayTimeOfDisplay;
            }
        }

        countDown = delayTimeOfDisplay / ONE_SCEND;
        deleteTime = startTime + allDisplayDelayTime;

        Log.printDebug("reminderSum = " + reminderSum);
        Log.printDebug("delayTimeOfDisplay = " + delayTimeOfDisplay);
        Log.printDebug("delay = " + popupBetween);
        Log.printDebug("allDisplayDelayTime = " + allDisplayDelayTime);
        Log.printDebug("startTime = " + new Date(startTime));
        Log.printDebug("createTime = " + new Date(System.currentTimeMillis()));

        long curTime = Calendar.getInstance().getTimeInMillis();
        if (searchTimer == null && startTime != 0 && startTime > curTime - (delayTimeOfDisplay + (ONE_SCEND * 2))) {
            Log.printDebug("set Time");
            startTimer();
        }
    }

    /**
     * Start.
     */
    public void start() {
        startPopup(scheduleData);
    }

    /**
     * Stop.
     */
    public void stop() {
        Log.printDebug("popup stop");
        stopTimer();
        stopPopup(scheduleData);
    }

    public void dispose() {
        Log.printDebug("popup dispose = " + scheduleData);
        stopTimer();
        if (blockedMessageID != null && blockedMessageID.equals(scheduleData.getMessageID())) {
            blocked = false;
            blockedMessageID = "";
        }
        disposePopup();
        
        scheduleData = null;
    }

    public void startTimer() {
        searchTimer = new TVTimerSpec();
        searchTimer.setRepeat(false);
        searchTimer.setAbsolute(true);
        searchTimer.addTVTimerWentOffListener(this);
        searchTimer.setAbsoluteTime(startTime);
        try {
            searchTimer = TVTimer.getTimer().scheduleTimerSpec(searchTimer);
        } catch (Exception e) {
            Log.printError(e);
        }
    }

    public void stopTimer() {
        if (searchTimer != null) {
            try {
                TVTimer.getTimer().deschedule(searchTimer);
                searchTimer.removeTVTimerWentOffListener(this);
                searchTimer = null;
            } catch (Exception e) {                
                Log.print(e);
            }
        }
    }

    public void reStrart() {
        Log.printInfo("reStrart");
        stop();
        Log.printDebug("currentTime = " + Calendar.getInstance().getTime());
        Log.printDebug("deleteTime = " + deleteTime);
        Log.printDebug("startTime = " + startTime);
        Log.printDebug("reminderSum = " + reminderSum + " reminderCount = " + reminderCount);
        Log.printDebug("delay = " + popupBetween);

        if (isAction || startTime >= deleteTime || popupBetween == 0 || reminderCount >= reminderSum) {
            Log.printDebug("Stop Popup");
            if (acceptCategories) {
                action();
            } else {
                PopupController.getInstance().removePopup(scheduleData, isAction);
            }
            isAction = false;
        } else {
            Log.printDebug("ReStart Popup");
            if (doublePopupDelay) {
                Log.printDebug("doublePopupDelay");
                doublePopupDelay = false;
                startTime = startTime - delayTimeOfDisplay;
                delayTimeOfDisplay = DataManager.getInstance().getDisplayTime();
            }
            delayTimeOfDisplay = DataManager.getInstance().getDisplayTime();
            countDown = delayTimeOfDisplay / ONE_SCEND;
            startTime = startTime + popupBetween + delayTimeOfDisplay;
            Log.printDebug("startTime = " + new Date(startTime));
            stopTimer();
            startTimer();
            PopupController.getInstance().closePopup(this);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {

        synchronized (ob) {
            Log.printDebug("startedTime =  " + startedTime);
            Log.printDebug("startTime =  " + startTime);
            if (blocked) {
                return;
            }

            if (priority == FORCE_START) {
                ForceStart = true;

                try {
                    Thread.sleep(ONE_SCEND * 2);
                } catch (InterruptedException e1) {
                    Log.printError(e1);
                }

                blocked = checkBlock();
                startedTime = startTime;
                countDown = delayTimeOfDisplay / ONE_SCEND;
                scheduleData.setDisplayCount(reminderCount);
                reminderCount++;
                Controller.getInstance().changeScheduleData(scheduleData);
                PopupController.getInstance().openPopup(this);
                stopTimer();
                Thread th = new Thread("ReStart") {
                    public void run() {
                        startPopup(scheduleData);
                        Log.printInfo("PopupLength = " + PopupController.getInstance().getPopupLength());
                    }
                };
                th.start();
            } else if (startedTime == startTime) {
                doublePopupDelay = true;
                startTime = startedTime + delayTimeOfDisplay;
                stopTimer();
                startTimer();
                Log.printDebug("startedTime1 =  " + startedTime);
                Log.printDebug("startTime1 =  " + startTime);
            } else if (startedTime + delayTimeOfDisplay > startTime) {
                doublePopupDelay = true;
                long tempStartTime = startTime;
                startTime = startedTime + delayTimeOfDisplay;
                delayTimeOfDisplay = tempStartTime - startedTime;

                if (delayTimeOfDisplay < 1000) {
                    delayTimeOfDisplay = DataManager.getInstance().getDisplayTime();
                }

                long TempAllDisplayDelayTime = (delayTimeOfDisplay * reminderSum + popupBetween * (reminderSum - 1));

                stopTimer();
                startTimer();
                Log.printDebug("startedTime2 =  " + startedTime);
                Log.printDebug("startTime2 =  " + startTime);
                Log.printDebug("onceOfDisplay =  " + delayTimeOfDisplay);
            } else {
                blocked = checkBlock();
                startedTime = startTime;
                countDown = delayTimeOfDisplay / ONE_SCEND;
                scheduleData.setDisplayCount(reminderCount);
                reminderCount++;
                Controller.getInstance().changeScheduleData(scheduleData);
                PopupController.getInstance().openPopup(this);
                Thread th = new Thread("ReStart") {
                    public void run() {
                        startPopup(scheduleData);
                        Log.printInfo("PopupLength = " + PopupController.getInstance().getPopupLength());
                    }
                };
                th.start();
            }
        }
    }

    private boolean checkBlock() {
        boolean check = false;
        if (scheduleData.getNotificationAction() == NotificationOption.ACTION_PIN_POPUP
                && scheduleData.getNotificationPopupType() == NotificationOption.SLEEP_TIMER_REMAINER_POPUP) {
            check = true;
            blockedMessageID = scheduleData.getMessageID();
        }
        return check;
    }

    /**
     * *************************************************************** methods - abstract
     * ***************************************************************.
     */
    abstract protected void disposePopup();

    /**
     * Start popup.
     */
    abstract protected void startPopup(ScheduleData popupOption);

    /**
     * Stop popup.
     */
    abstract protected void stopPopup(ScheduleData popupOption);

    /**
     * Key action.
     * @param keyCode the key code
     * @return true, if successful
     */
    abstract public boolean keyAction(int keyCode);

    abstract public void action();
}

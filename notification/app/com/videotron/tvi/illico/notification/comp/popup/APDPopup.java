package com.videotron.tvi.illico.notification.comp.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Hashtable;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.Rs;
import com.videotron.tvi.illico.notification.controller.Controller;
import com.videotron.tvi.illico.notification.data.CommunicationManager;
import com.videotron.tvi.illico.notification.data.DataManager;
import com.videotron.tvi.illico.notification.data.ScheduleData;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The Class AutoPowerDownPopup.
 * @author pellos
 */
public class APDPopup extends Popup implements Runnable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    private APDPopupRenderer apdPopupRenderer;
    private Thread th;
    private String position;

    private Image reminderBg2;

    private Image notiStandyTime;
    private Image notiIconPower;
    private Image dButton;
    private Image cButton;
    
    /** The title. */
    private String title = "";

    private int popupType;
    private String message;
    private int categories;
    private int action;
    private String[] actionData;

    private boolean threadInterrupted = false;

    private Effect showingEffect;
    /** The animation frame count. */
    private final int ANIFRAMECOUNT = 30;

    private boolean keylock = false;
    
    private String dBtnLabel;
    private String cBtnLabel;
    private int	wCBtnLabel;
    private int wDBtnLabel;
    
    /**
     * Instantiates a new SmallPopup.
     */
    public APDPopup() {
        apdPopupRenderer = new APDPopupRenderer();
        setRenderer(apdPopupRenderer);
        prepare();
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#startPopup()
     */
    protected void startPopup(ScheduleData popupOption) {
        Log.printDebug("[APDPopup]startPopup = " + popupOption);
        this.setVisible(false);
        
        Controller.getInstance().layeredUI.activate();
        position = DataManager.getInstance().getPosition();

        Point from = null;
        Point to = null;
        
        from = new Point(508, 0);
        to = new Point(0, 0);
        this.setBounds(508, 0, 452, 227);
        
        showingEffect = new MovingEffect(this, ANIFRAMECOUNT, from, to, MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);

        action = popupOption.getNotificationAction();
        actionData = popupOption.getNotificationActionData();

        priority = scheduleData.getPriority();
        Log.printDebug("[APDPopup]priority = " + priority);
        popupType = popupOption.getNotificationPopupType();
        Log.printDebug("[APDPopup]popupType = " + popupType);
        
    	title = DataCenter.getInstance().getString("popup.apd.title");
    	message = DataCenter.getInstance().getString("popup.apd.message");
    	
    	cBtnLabel = DataCenter.getInstance().getString("popup.apd.cbtn");
    	dBtnLabel = DataCenter.getInstance().getString("popup.apd.dbtn");
    	
    	wCBtnLabel = Rs.FM24.stringWidth(cBtnLabel);
    	wDBtnLabel = Rs.FM18.stringWidth(dBtnLabel);
    			
        threadInterrupted = false;

        // 현재 상태를 보고 멈춰야 할지...
        categories = popupOption.getCategories();
        if (categories == 0) {
            String cate = (String) DataManager.getInstance().getAppList().get(popupOption.getApplicationName());;
            if (cate == null) {
                categories = 1;
            } else {
                categories = Integer.parseInt(cate);
            }
        }

        acceptCategories = true;
        Log.printDebug("[APDPopup]categories = " + categories);
        switch (categories) {
        case NotificationOption.All_Application_CATEGORIES:
            break;
        case NotificationOption.NOT_PPV_CATEGORIES:
            EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            try {
                boolean ppvWatching = epgService.isPpvWatching();
                Log.printDebug("[APDPopup]ppvWatching = " + ppvWatching);
                if (ppvWatching) {
                    threadInterrupted = true;
                    this.setVisible(false);
                    acceptCategories = false;
                }
            } catch (RemoteException e1) {
                Log.printError(e1);
            }
            break;
        }

        if (Controller.getInstance().iSleepTimerStarted()) {
            threadInterrupted = true;
            this.setVisible(false);
            acceptCategories = false;
        }

        th = null;
        th = new Thread(this, "DisplayTimeThread");
        if (acceptCategories) {
            showingEffect.start();
        } else {
            th.start();
        }
    }

    public void closePopup() {
        Controller.getInstance().layeredUI.deactivate();
        PopupController.getInstance().closePopup(this);
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#stopPopup()
     */
    protected void stopPopup(ScheduleData popupOption) {
        Log.printDebug("[APDPopup]stopPopup");
        this.setVisible(false);
        Controller.getInstance().layeredUI.deactivate();
        keylock = false;
        repaint();
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#disposePopup()
     */
    protected void disposePopup() {
        Log.printDebug("[APDPopup] disposePopup");
        actionData = null;
        scheduleData = null;
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#keyAction(int)
     */
    public boolean keyAction(int keyCode) {
        Log.printDebug("[APDPopup] keyAction keyCode = " + keyCode);

        if (keylock) {
            return false;
        }
        return false;
    }    

    public void action() {
        threadInterrupted = true;
        Log.printDebug("[APDPopup]action = " + action);        
    }

    public boolean skipAnimation(Effect effect) {
        return false;
    }

    public void animationStarted(Effect effect) {
        keylock = true;
    }

    public void animationEnded(Effect effect) {
        Log.printDebug("[APDPopup] visible = " + this.isVisible());

        if (acceptCategories) {
            Controller.getInstance().stopScreenSaver();
            Log.printDebug("[APDPopup] start animation");
        }
        Controller.getInstance().layeredUI.activate();
        this.setVisible(true);
        keylock = false;
        th.start();
        repaint();
    }

    class APDPopupRenderer extends Renderer {

        public Rectangle getPreferredBounds(UIComponent c) {
            return Rs.SCENE_BOUND;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (position == null) {
                return;
            }

            g.translate(-508, 0);
            rightPopupPaint(g, c);
            g.translate(508, 0);
        }

        public void rightPopupPaint(Graphics g, UIComponent c) {

            g.drawImage(reminderBg2, 508, 35, c);
            g.drawImage(notiStandyTime, 821, 60, c);
            g.drawImage(notiIconPower, 522, 48, c);
        	
    		g.drawImage(cButton, 920-wCBtnLabel-30, 116, c);
    		g.setFont(Rs.F24);
            g.setColor(Rs.C255204000);
            g.drawString(cBtnLabel, 920-wCBtnLabel, 135);

            g.drawImage(dButton, 920-wDBtnLabel-25, 156, c);
            g.setFont(Rs.F18);
            g.setColor(Rs.C241241241);
            g.drawString(dBtnLabel, 920-wDBtnLabel, 172);
            
            g.setFont(Rs.F22);
            g.setColor(Rs.C255255255);
            g.drawString(title, 549, 64);

            int min = (int) (countDown / 60);
            int sec = (int) (countDown % 60);

            String minute = "00";
            if (min < 10) {
                minute = "0" + String.valueOf(min);
            } else {
                minute = String.valueOf(min);
            }

            String second = "00";
            if (sec < 10) {
                second = "0" + String.valueOf(sec);
            } else {
                second = String.valueOf(sec);
            }

            g.drawString(minute + " : " + second, 841, 91);

        	g.setFont(Rs.F16);
            g.setColor(Rs.C161160160);
        	String[] str = TextUtil.split(message, g.getFontMetrics(), 263);
        	int strLength = str.length;
        	 for (int a = 0; a < strLength; a++) {
                 g.drawString(str[a], 548, 87 + a * 17);
             }
        }

        public void prepare(UIComponent c) {

            reminderBg2 = DataCenter.getInstance().getImage("reminder_bg2.png");
            notiIconPower = DataCenter.getInstance().getImage("noti_icn_power.png");
            notiStandyTime = DataCenter.getInstance().getImage("noti_stanby_time.png");
            cButton = DataCenter.getInstance().getImage("b_btn_c_big.png");
            
            Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
            if (imgTable != null) {
               dButton = (Image) imgTable.get(PreferenceService.BTN_D);
            } else {
                dButton = DataCenter.getInstance().getImage("b_btn_d.png");
            }

            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    public void run() {
        int count = 0;
        ForceStart = false;
        Log.printDebug("[APDPopup] onceOfDisplay = " + delayTimeOfDisplay);

        while (!threadInterrupted && acceptCategories && ((delayTimeOfDisplay > (count * ONE_SCEND)))) {
            Log.printDebug("[APDPopup] count * SLEEP_TIME = " + (count * ONE_SCEND));

            if (ForceStart) {
                acceptCategories = false;
                isAction = true;
                break;
            }

            count++;
            repaint();
            if (countDown > 0) {
                countDown = (delayTimeOfDisplay - (System.currentTimeMillis() - startTime)) / ONE_SCEND;
            }
           
            try {
                Thread.sleep(ONE_SCEND);
            } catch (InterruptedException e) {
                Log.printError(e);
            }
            
            if(countDown <= 0) {
                count = 120;
                break;
            }
        }

        // 시간이 지났을때 행동
        if (delayTimeOfDisplay <= (count * ONE_SCEND)) {
            acceptCategories = false;
        }
        threadInterrupted = true;
        if (threadInterrupted) {
            if (priority == FORCE_START) {
                ForceStart = false;
            }
            reStrart();
        }
    }
}

package com.videotron.tvi.illico.notification.comp.popup;

import java.awt.Container;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.Rs;
import com.videotron.tvi.illico.notification.controller.Controller;
import com.videotron.tvi.illico.notification.data.ScheduleData;

/**
 * The Class PopupController.
 * @author pellos
 */
public class PopupController {

    /** The instance. */
    private static PopupController instance;

    /** The popup list. */
    private ArrayList popupList;

    /** The popup list size. */
    private int popupListSize;

    private LargePopup largePopup;

    private Hashtable popups;
    private ScheduleData scheduleData;
    /**
     * If it is the time-sleep state, It is true.
     */
    private static boolean blocked = false;

    /**
     * Gets the single instance of PopupController.
     * @return single instance of PopupController
     */
    public static synchronized PopupController getInstance() {
        if (instance == null) {
            instance = new PopupController();
        }
        return instance;
    }

    /**
     * Instantiates a new PopupController.
     */
    private PopupController() {
        if (popupList == null) {
            popupList = new ArrayList();
        }

        if (popups == null) {
            popups = new Hashtable();
        }
    }

    /**
     * Init the manager.
     */
    public void initManager() {
    }

    /**
     * Dispose manager.
     */
    public void disposeManager() {
        removeAllPopup();
    }

    public void createPopup(ScheduleData data) {
        if (data == null) {
            return;
        } else if(Controller.stbMode != MonitorService.STB_MODE_NORMAL) {
            Log.printDebug("Controller.stbMode = " + Controller.stbMode);
            return;
        }

        String key = null;
        key = data.getMessageID();
        Log.printDebug("PopupController key = " + key);

        if (popups.get(key) != null) {
            popups.remove(key);
        }

        String appName = null;
        appName = data.getApplicationName();
        Log.printDebug("PopupController appName = " + appName);

        Popup popup = null;
        if (data.getNotificationPopupType() == NotificationOption.APD_POPUP)
        	popup = new APDPopup();
        else
        	popup = new SmallPopup();

        popups.put(key, popup);
        if (popup != null) {
            popup.init(data);
        }
    }

    /**
     * finished popup event.
     * @param data
     * @param isAction If it does action, It is a true.
     */
    public void removePopup(ScheduleData data, boolean isAction) {
        if (data == null) {
            return;
        }
        String key = null;
        key = data.getMessageID();

        if (key != null) {
            Popup closePopup = (Popup) popups.remove(key);
            if(closePopup != null){
                closePopup(closePopup);
                closePopup.dispose();
            }
            Controller.getInstance().notifyToCallerActionEvent(data, isAction);
        }
    }

    public void setScheduleDataOfRemoveReady(ScheduleData data){
        scheduleData = data;
    }

    public void removeReady(){
        removePopup(scheduleData, false);
        scheduleData = null;
    }

    public void deletePopup(ScheduleData data){
        if (data == null) {
            return;
        }
        String key = null;
        key = data.getMessageID();

        if (key != null) {
            Popup closePopup = (Popup) popups.remove(key);
            if(closePopup != null){
                closePopup(closePopup);
                closePopup.dispose();
            }
        }
    }

    /**
     * open pop-up.
     * @param openPopup the open popup
     * @param target the target
     */
    public void openPopup(Popup openPopup) {
        if (popupList == null || openPopup == null) {
            return;
        }

        // openPopup.start();
        popupList.add(openPopup);
        popupListSize = popupList.size();

        Controller.getInstance().getLayeredWindow().add(openPopup, 0);
        openPopup.setVisible(true);
//        Controller.getInstance().getLayeredWindow().repaint();
    }

    /**
     * close the last of pop-up.
     * @param closePopup the close popup
     * @param target the target
     */
    public void closePopup(Popup closePopup) {
        if (popupList == null || closePopup == null) {
            return;
        }
        closePopup.setVisible(false);
        int popupIndex = popupList.indexOf(closePopup);
        if (popupIndex != -1) {
            popupList.remove(popupIndex);
            popupListSize = popupList.size();
        }
        Controller.getInstance().getLayeredWindow().remove(closePopup);
        Controller.getInstance().getLayeredWindow().repaint();
    }

    public void openLargePopup(ScheduleData data) {

        if(largePopup == null){
            largePopup = new LargePopup();
            largePopup.setBounds(Rs.SCENE_BOUND);
        }

        Controller.getInstance().getLayeredWindow().add(largePopup, 0);
        largePopup.startPopup(data);
    }

    public void closeLaregePopup(){
        largePopup.stop();
        Controller.getInstance().getLayeredWindow().remove(largePopup);
        largePopup = null;
    }

    /**
     * Close all popup.
     * @param target the target
     */
    public void closeAllPopup(Container target) {
        if (popupList == null || popupListSize <= 0 || target == null) {
            return;
        }
        Iterator iterator = popupList.iterator();
        while (iterator.hasNext()) {
            Popup closePopup = (Popup) iterator.next();
            if (closePopup == null) {
                continue;
            }
            target.remove(closePopup);
            closePopup.stop();
        }
        popupList.clear();
        popupListSize = popupList.size();
        target.repaint();
    }

    /**
     * get the length of a pop-up.
     * @return the popup length
     */
    public int getPopupLength() {
        return popupListSize;
    }

    /**
     * Gets the popup.
     * @param popupIdx the popup idx
     * @return the popup
     */
    public Popup getPopup(int popupIdx) {
        if (popupList == null)
            return null;
        return (Popup) popupList.get(popupIdx);
    }

    /**
     * Gets the last popup.
     * @return the last popup
     */
    public Popup getLastPopup() {
        int popupListSize = -1;
        if (popupList == null || (popupListSize = popupList.size()) <= 0)
            return null;
        return (Popup) popupList.get(popupListSize - 1);
    }

    /**
     * Checks if is active popup.
     * @return true, if is active popup
     */
    public boolean isActivePopup() {
        if (popupListSize > 0)
            return true;
        return false;
    }

    /**
     * Removes the all popup.
     */
    public void removeAllPopup() {
        if (popupList == null || popupListSize <= 0)
            return;
        Iterator iterator = popupList.iterator();
        while (iterator.hasNext()) {
            Popup popup = (Popup) iterator.next();
            if (popup == null)
                continue;
            popup.dispose();
        }
        popupList.clear();
        popupList = null;
    }

    public boolean keyAction(int key) {
        if(largePopup != null && largePopup.isVisible()){
            return largePopup.keyAction(key);
        }

        Popup popup = getLastPopup();
        if (popup != null) {
            return popup.keyAction(key);
        } else {
            return false;
        }
    }
}

package com.videotron.tvi.illico.notification.comp.popup;

/**
 * The Class PopupEvent.
 * @author pellos
 */
public class PopupEvent {

    /** The popup. */
    private Popup popup;

    /** The popup param. */
    private String popupParam;

    /**
     * Instantiates a new popup event.
     * @param popup the popup
     */
    public PopupEvent(Popup popup) {
        this.popup = popup;
    }

    /**
     * Sets the popup.
     * @param popup the new popup
     */
    public void setPopup(Popup popup) {
        this.popup = popup;
    }

    /**
     * Gets the popup.
     * @return the popup
     */
    public Popup getPopup() {
        return popup;
    }

    /**
     * Sets the popup param.
     * @param popupParam the new popup param
     */
    public void setPopupParam(String popupParam) {
        this.popupParam = popupParam;
    }

    /**
     * Gets the popup param.
     * @return the popup param
     */
    public String getPopupParam() {
        return popupParam;
    }
}

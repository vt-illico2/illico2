package com.videotron.tvi.illico.notification.comp.popup;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.Rs;
import com.videotron.tvi.illico.notification.comp.popup.SmallPopup.SmallPopupRenderer;
import com.videotron.tvi.illico.notification.controller.Controller;
import com.videotron.tvi.illico.notification.data.CommunicationManager;
import com.videotron.tvi.illico.notification.data.ScheduleData;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The Class NotificationTwoButtonPopup.
 * @author pellos
 */
public class LargePopup extends UIComponent {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private LargePopupRenderer largePopupRenderer;
    private ScheduleData scheduleData;

    private Image largebg;
    private Image hotkeybg;
    private Image icon;
    private Image exit;
    private Image focus;
    private Image focusDim;

    /** The ok. */
    private String close = DataCenter.getInstance().getString("popup.close");

    private String notification = DataCenter.getInstance().getString("popup.Notification");

    private int focusSum = 0;

    /** The index. */
    private int index = 0;

    /** The title. */
    private String title = "";

    /** The content1. */
    private String message = "";

    /** The content2. */
    private String messageSub = "";

    private String[][] buttonInfo;

    private int action;
    private String[][] actionData;

    private Effect showingEffect;
    /** The animation frame count. */
    private final int ANIFRAMECOUNT = 10;

    /**
     * Instantiates a new notification two button popup.
     */
    public LargePopup() {
        largePopupRenderer = new LargePopupRenderer();
        setRenderer(largePopupRenderer);
        prepare();
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#disposePopup()
     */
    protected void disposePopup() {
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#keyAction(int)
     */
    public boolean keyAction(int keyCode) {

        switch (keyCode) {
        case HRcEvent.VK_ENTER:
            stop();
            action();
            PopupController.getInstance().closeLaregePopup();
            break;
        case KeyEvent.VK_LEFT:
            index--;
            if (index < 0) {
                index = 0;
            }
            action = Integer.parseInt(buttonInfo[index][1]);
            repaint();
            break;
        case KeyEvent.VK_RIGHT:
            index++;
            if (focusSum <= index) {
                index = focusSum - 1;
            }
            action = Integer.parseInt(buttonInfo[index][1]);
            repaint();
            break;
        case KeyCodes.MENU:
        case KeyCodes.VOD:
        case OCRcEvent.VK_GUIDE:
        case KeyCodes.FAV:
        case KeyCodes.WIDGET:
             stop();
            return false;
        case KeyCodes.SEARCH:
            return false;
        case OCRcEvent.VK_INFO:
        case KeyCodes.REWIND:
        case KeyCodes.FAST_FWD:
        case KeyCodes.RECORD:
        case KeyCodes.COLOR_A:
        case KeyCodes.COLOR_B:
        case KeyCodes.COLOR_C:
        case KeyCodes.COLOR_D:
        case KeyCodes.PIP:
        case KeyEvent.VK_0:
        case KeyEvent.VK_1:
        case KeyEvent.VK_2:
        case KeyEvent.VK_3:
        case KeyEvent.VK_4:
        case KeyEvent.VK_5:
        case KeyEvent.VK_6:
        case KeyEvent.VK_7:
        case KeyEvent.VK_8:
        case KeyEvent.VK_9:
        case OCRcEvent.VK_CHANNEL_UP:
        case OCRcEvent.VK_CHANNEL_DOWN:
        case HRcEvent.VK_PAGE_UP:
        case HRcEvent.VK_PAGE_DOWN:
        case KeyCodes.LAST:
            return true;
        default:
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#startPopup()
     */
    protected void startPopup(ScheduleData popupOption) {
        scheduleData = popupOption;
        Controller.getInstance().layeredUI.activate();
        if (popupOption == null) {
            return;
        }
        setVisible(false);

        Point from = null;
        Point to = null;

        from = new Point(0, 540);
        to = new Point(0, 270);

        showingEffect = new MovingEffect(this, ANIFRAMECOUNT, from, to, MovingEffect.FADE_IN,
                MovingEffect.ANTI_GRAVITY);
        showingEffect.setTargetBounds(to.x, to.y, largebg.getWidth(this), largebg.getHeight(this));

        String name = DataCenter.getInstance().getString("appName." + popupOption.getApplicationName());
        if (name != null && !name.equals(TextUtil.EMPTY_STRING)) {
        	title = DataCenter.getInstance().getString("popup.notiAppTitle") + " " + name;
        } else {
        	title = DataCenter.getInstance().getString("popup.notiAppTitle") + " " + popupOption.getApplicationName();
        }
        message = popupOption.getLargePopupMessage();
        messageSub = popupOption.getLargePopupSubMessage();

        // * [0][0] is a button Name.
        // * [0][1] is ActionType.
        // * [0][2] is application name or channel name,
        // * [0][n] is data.
        buttonInfo = null;
        buttonInfo = popupOption.getButtonNameAction();
        index = 0;
        if (buttonInfo != null) {
            action = Integer.parseInt(buttonInfo[index][1]);
            focusSum = buttonInfo.length;
            actionData = new String[focusSum][];

            for (int a = 0; a < focusSum; a++) {
                int dataLength = buttonInfo[a].length - 1;
                actionData[a] = new String[dataLength];
                for (int b = 0; b < dataLength; b++) {
                    actionData[a][b] = buttonInfo[a][1 + b];
                    Log.printDebug("actionData[" + a +"][" + b + "] = " + actionData[a][b]);
                }
            }

            Log.printDebug("focusSum = " + focusSum);
        }
        showingEffect.start();
    }

    public void stop() {
        Controller.getInstance().layeredUI.deactivate();
        this.setVisible(false);
        buttonInfo = null;
        scheduleData = null;
        actionData = null;

        DataCenter.getInstance().removeImage("12_largebg.png");
        DataCenter.getInstance().removeImage("01_hotkeybg.png");
        DataCenter.getInstance().removeImage("noti_icn_03.png");
        DataCenter.getInstance().removeImage("btn_exit.png");
        DataCenter.getInstance().removeImage("btn_210_foc.png");
        DataCenter.getInstance().removeImage("btn_210_l.png");

        largebg = null;
        hotkeybg = null;
        icon = null;
        exit = null;
        focus = null;
        focusDim = null;
    }

    public void action() {
        switch (action) {
        case NotificationOption.ACTION_NONE:
            break;
        case NotificationOption.ACTION_APPLICATION:
            if (actionData != null) {
                String appName = actionData[index][0];
                String[] parameters = new String[actionData.length - 1];

                for (int a = 0; parameters != null && a < parameters.length; a++) {
                    parameters[a] = actionData[index][a + 1];
                }
                CommunicationManager.getInstance().unboundApplicastionStart(appName, parameters);
                Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
            }
            break;
        case NotificationOption.ACTION_TUNE_CHANNEL:
            if (actionData != null) {
                String number = actionData[index][0];
                CommunicationManager.getInstance().channelSelect(number);
                Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
            }
            break;

        case NotificationOption.ACTION_SLEEP_TIMER:
        	//R7.4 Block Admin PIN VDTRMASTER-6205
            CommunicationManager.getInstance().showPinEnabler(null, new PinEnabler(), -1);
            break;
        case NotificationOption.ACTION_LARGE_POPUP:
            break;
        case NotificationOption.ACTION_PIN_POPUP:
        	//R7.4 Block Admin PIN VDTRMASTER-6205
            CommunicationManager.getInstance().showPinEnabler(null, new PinEnabler(), -1);
            break;
        }

        Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
    }

    class PinEnabler implements PinEnablerListener {
        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            Log.printDebug("result = " + response + " detail = " + detail);
            if (PreferenceService.RESPONSE_SUCCESS == response) {
                if (action == NotificationOption.ACTION_SLEEP_TIMER) {

                } else if (action == NotificationOption.ACTION_PIN_POPUP) {
                    PreferenceService service = (PreferenceService) DataCenter.getInstance().get(
                            PreferenceService.IXC_NAME);
                    service.setPreferenceValue(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_OFF);
                }
            } else {
                if (action == NotificationOption.ACTION_SLEEP_TIMER) {

                } else if (action == NotificationOption.ACTION_PIN_POPUP) {

                }
            }
        }
    }

    class LargePopupRenderer extends Renderer {

        public Rectangle getPreferredBounds(UIComponent c) {
            return Rs.SCENE_BOUND;
        }

        protected void paint(Graphics g, UIComponent c) {

            g.drawImage(largebg, 0, 270, c);
            g.drawImage(hotkeybg, 236, 466, c);
            g.drawImage(icon, 43, 328, c);
            g.drawImage(exit, 848, 488, c);

            g.setFont(Rs.F17);
            g.setColor(Rs.C241241241);
            g.drawString(close, 879, 503);

            if (focusSum == 2) {
                g.drawImage(focusDim, 266, 435, c);
                g.drawImage(focusDim, 485, 435, c);
                if (index == 0) {
                    g.drawImage(focus, 266, 435, c);
                } else {
                    g.drawImage(focus, 485, 435, c);
                }

                g.setFont(Rs.F18);
                g.setColor(Rs.C000000000);
                for (int a = 0; a < focusSum; a++) {
                    GraphicUtil.drawStringCenter(g, buttonInfo[a][0], 371 + a * 220, 456);
                }

            } else {
                g.drawImage(focusDim, 152, 435, c);
                g.drawImage(focusDim, 372, 435, c);
                g.drawImage(focusDim, 591, 435, c);

                if (index == 0) {
                    g.drawImage(focus, 152, 435, c);
                } else if (index == 1) {
                    g.drawImage(focus, 372, 435, c);
                } else if (index == 2) {
                    g.drawImage(focus, 591, 435, c);
                }

                g.setFont(Rs.F17);
                g.setColor(Rs.C241241241);
                for (int a = 0; a < focusSum; a++) {
                    GraphicUtil.drawStringCenter(g, buttonInfo[a][0], 257 + a * 220, 456);
                }
            }

            g.setColor(Rs.C255255255);
            g.setFont(Rs.F24);
            g.drawString(notification + " " + title, 79, 351);

            g.setFont(Rs.F19);
            g.setColor(Rs.C224224224);
            String str[] = null;
            str = TextUtil.split(message, g.getFontMetrics(), 340);
            if (str != null) {
                if (str.length == 1) {
                    GraphicUtil.drawStringCenter(g, str[0], 480, 389 + 8);
                } else {
                    for (int a = 0; str != null && a < str.length; a++) {
                        GraphicUtil.drawStringCenter(g, str[a], 480, 389 + 19 * a);
                    }
                }
            }

            g.setFont(Rs.F18);
            g.setColor(Rs.C161160160);
            if (messageSub != null) {
                GraphicUtil.drawStringCenter(g, messageSub, 480, 503);
            }

        }

        public void prepare(UIComponent c) {
            close = DataCenter.getInstance().getString("popup.close");
            notification = DataCenter.getInstance().getString("popup.Notification");
            largebg = DataCenter.getInstance().getImage("12_largebg.png");
            hotkeybg = DataCenter.getInstance().getImage("01_hotkeybg.png");
            icon = DataCenter.getInstance().getImage("noti_icn_03.png");
            exit = DataCenter.getInstance().getImage("btn_exit.png");
            focus = DataCenter.getInstance().getImage("btn_210_foc.png");
            focusDim = DataCenter.getInstance().getImage("btn_210_l.png");

            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    public void animationEnded(Effect effect) {
        this.setVisible(true);
        repaint();
    }
}

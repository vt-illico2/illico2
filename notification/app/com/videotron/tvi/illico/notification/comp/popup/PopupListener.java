package com.videotron.tvi.illico.notification.comp.popup;

/**
 * The listener interface for receiving popup events. The class that is interested in processing a popup event
 * implements this interface, and the object created with that class is registered with a component using the
 * component's <code>addPopupListener<code> method. When
 * the popup event occurs, that object's appropriate
 * method is invoked.
 * @see PopupEvent
 */
public interface PopupListener {

    /**
     * Request.
     * @param pe the PopupEvent
     * @return the object
     * @throws Exception the exception
     */
    public Object request(PopupEvent pe) throws Exception;

    /**
     * Request completed.
     * @param pe the PopupEvent
     * @param resultObj the result obj
     */
    public void requestCompleted(PopupEvent pe, Object resultObj);

    /**
     * Request failed.
     * @param pe the PopupEvent
     * @param errorMessage the error message
     */
    public void requestFailed(PopupEvent pe, String errorMessage);

    /**
     * Popup ok.
     * @param pe the PopupEvent
     */
    public void popupOK(PopupEvent pe);

    /**
     * Popup cancel.
     * @param pe the PopupEvent
     */
    public void popupCancel(PopupEvent pe);
}

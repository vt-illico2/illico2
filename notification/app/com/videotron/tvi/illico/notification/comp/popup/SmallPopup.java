package com.videotron.tvi.illico.notification.comp.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Hashtable;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.Rs;
import com.videotron.tvi.illico.notification.controller.Controller;
import com.videotron.tvi.illico.notification.data.CommunicationManager;
import com.videotron.tvi.illico.notification.data.DataManager;
import com.videotron.tvi.illico.notification.data.ScheduleData;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The Class NotificationOneButtonPopup.
 * @author pellos
 */
public class SmallPopup extends Popup implements Runnable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private static final String TOPLEFT = "TopLeft";
    private static final String TOPRIGHT = "TopRight";
    private static final String BOTTOMLEFT = "BottomLeft";
    private static final String BOTTOMRIGHT = "BottomRight";

    private SmallPopupRenderer smallPopupRenderer;
    private Thread th;
    private String position;

    private Image reminderBg;
    private Image reminderBg2;

    private Image notiIconClock;
    private Image notiIconSTC;
    private Image notiIconInfo;
    private Image notiIconLock;
    private Image notiIconCallerID[] = new Image[3];
    private Image okButton;
    private Image exitButton;

    /** The title. */
    private String title = "";

    /** The more detail. */
    private String moreDetail = DataCenter.getInstance().getString("popup.More Detail");
    private String tuneNow = DataCenter.getInstance().getString("popup.Tune Now");
    private String stayTuned = DataCenter.getInstance().getString("popup.Stay Tuned");
    private String sleepStayTuned = DataCenter.getInstance().getString("popup.sleepStayTuned");
    private String unBlocked = DataCenter.getInstance().getString("popup.Unblocked");
    private String close = DataCenter.getInstance().getString("popup.close");

    private int popupType;
    private String message;
    private String messageAdd;
    private String subMessage;
    private int categories;
    private int action;
    private String[] actionData;

    private boolean isCountDown;

    private int titleX = 599;
    private int titleY = 64;

    boolean okKey = false;
    boolean hasLargeNotification = false;
    String largePopupMsg = null;
    private boolean threadInterrupted = false;

    private Effect showingEffect;
    /** The animation frame count. */
    private final int ANIFRAMECOUNT = 30;

    private boolean keylock = false;
    
    private String okName;

    /**
     * Instantiates a new SmallPopup.
     */
    public SmallPopup() {
        smallPopupRenderer = new SmallPopupRenderer();
        setRenderer(smallPopupRenderer);
        prepare();
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#startPopup()
     */
    protected void startPopup(ScheduleData popupOption) {
        Log.printDebug("startPopup = " + popupOption);
        this.setVisible(false);
        moreDetail = DataCenter.getInstance().getString("popup.More Detail");
        tuneNow = DataCenter.getInstance().getString("popup.Tune Now");
        stayTuned = DataCenter.getInstance().getString("popup.Stay Tuned");
        sleepStayTuned = DataCenter.getInstance().getString("popup.sleepStayTuned");
        unBlocked = DataCenter.getInstance().getString("popup.Unblocked");
        close = DataCenter.getInstance().getString("popup.close");

        Controller.getInstance().layeredUI.activate();
        position = DataManager.getInstance().getPosition();

        Point from = null;
        Point to = null;
        if (position.equals(TOPLEFT)) {
            from = new Point(-402, 0);
            to = new Point(0, 0);
            this.setBounds(0, 0, 484, 177);
        } else if (position.equals(BOTTOMLEFT)) {
            from = new Point(-402, 0);
            to = new Point(0, 0);
            this.setBounds(0, 378, 484, 177);
        } else if (position.equals(BOTTOMRIGHT)) {
            from = new Point(482, 0);
            to = new Point(0, 0);
            this.setBounds(477, 378, 484, 177);
        } else if (position.equals(TOPRIGHT)) {
            from = new Point(482, 0);
            to = new Point(0, 0);
            this.setBounds(477, 0, 484, 177);
        }
        showingEffect = new MovingEffect(this, ANIFRAMECOUNT, from, to, MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);

        action = popupOption.getNotificationAction();
        actionData = popupOption.getNotificationActionData();

        priority = scheduleData.getPriority();
        Log.printDebug("priority = " + priority);
        popupType = popupOption.getNotificationPopupType();
        subMessage = null;
        Log.printDebug("popupType = " + popupType);
        switch (popupType) {
        case NotificationOption.CALLER_ID_POPUP:
            okKey = false;
            message = popupOption.getMessage();
            title = popupOption.getSubMessage();
            messageAdd = null;
            break;
        case NotificationOption.PPV_REMINDER_POPUP:
            okKey = true;
            title = DataCenter.getInstance().getString("popup.ppvTitle");
            message = popupOption.getMessage();
            messageAdd = DataCenter.getInstance().getString("popup.startMsg");
            Log.printDebug("actionData " + actionData);
            if (actionData != null && actionData.length > 1) {
                String age = actionData[1];
                Log.printDebug("age = " + age);
                if (CommunicationManager.isLock(age)) {
                    message = "";
                    messageAdd = DataCenter.getInstance().getString("popup.ppvBlock");
                }
            }
            break;
        case NotificationOption.PPV_ORDER_REMINDER_POPUP:
        	 okKey = true;
             title = DataCenter.getInstance().getString("popup.ppvTitle");
             message = popupOption.getMessage();
             messageAdd = DataCenter.getInstance().getString("popup.ppvOrdering");
             Log.printDebug("actionData " + actionData);
             if (actionData != null && actionData.length > 1) {
                 String age = actionData[1];
                 Log.printDebug("age = " + age);
                 if (CommunicationManager.isLock(age)) {
                     message = "";
                     messageAdd = DataCenter.getInstance().getString("popup.ppvBlock");
                 }
             }
        	break;
        case NotificationOption.STC_POPUP:
            okKey = false;
            title = DataCenter.getInstance().getString("popup.stcTitle");
            message = DataCenter.getInstance().getString("popup.stcMsg");
            messageAdd = null;
            break;
        case NotificationOption.PROGRAM_REMINDER_POPUP:
            okKey = true;
            title = DataCenter.getInstance().getString("popup.programTitle");
            message = popupOption.getMessage();
            messageAdd = DataCenter.getInstance().getString("popup.startMsg");
            Log.printDebug("actionData " + actionData);
            if (actionData != null && actionData.length > 1) {
                String age = actionData[1];
                Log.printDebug("age = " + age);
                if (CommunicationManager.isLock(age)) {
                    message = "";
                    messageAdd = DataCenter.getInstance().getString("popup.epgBlock");
                }
            }

            break;
        case NotificationOption.SLEEP_TIMER_REMAINER_POPUP:
            okKey = true;
            title = DataCenter.getInstance().getString("popup.sleepTitle");
            message = DataCenter.getInstance().getString("popup.closeMsg");
            messageAdd = null;
            break;
        case NotificationOption.NOTIFICAION_APPNAME_POPUP:
            okKey = true;
            String name = DataCenter.getInstance().getString("appName." + popupOption.getApplicationName());
            if (name != null && !name.equals(TextUtil.EMPTY_STRING)) {
            	title = DataCenter.getInstance().getString("popup.notiAppTitle") + " " + name;
            } else {
            	title = DataCenter.getInstance().getString("popup.notiAppTitle") + " " + popupOption.getApplicationName();
            }

            message = popupOption.getMessage();
            messageAdd = null;
            largePopupMsg = popupOption.getLargePopupMessage();
            if (largePopupMsg != null) {
                hasLargeNotification = true;
            }
            break;
        case NotificationOption.TIMEBASED_RESTRICTIONS_POPUP:
            okKey = true;
            title = DataCenter.getInstance().getString("popup.timebased");
            message = DataCenter.getInstance().getString("popup.timeBlockMsg");
            messageAdd = null;
            break;
        case NotificationOption.PARENTAL_CONTROLS_POPUP:
            okKey = true;
            title = DataCenter.getInstance().getString("popup.parentalTitle");
            message = DataCenter.getInstance().getString("popup.blockMsg");
            messageAdd = null;
            break;
        case NotificationOption.APD_POPUP:
        	okKey = true;
        	title = DataCenter.getInstance().getString("popup.apd.title");
        	message = DataCenter.getInstance().getString("popup.apd.message");
        	okName = DataCenter.getInstance().getString("popup.apd.ok");
        	messageAdd = null;
        	break;
        default:
            break;
        }
        isCountDown = popupOption.isCountDown();
        threadInterrupted = false;

        // 현재 상태를 보고 멈춰야 할지...
        categories = popupOption.getCategories();
        if (categories == 0) {
            String cate = (String) DataManager.getInstance().getAppList().get(popupOption.getApplicationName());;
            if (cate == null) {
                categories = 1;
            } else {
                categories = Integer.parseInt(cate);
            }
        }

        acceptCategories = true;
        Log.printDebug("categories = " + categories);
        switch (categories) {
        case NotificationOption.All_Application_CATEGORIES:
            break;
        case NotificationOption.NOT_PPV_CATEGORIES:
            EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            try {
                boolean ppvWatching = epgService.isPpvWatching();
                Log.printDebug("ppvWatching = " + ppvWatching);
                if (ppvWatching) {
                    threadInterrupted = true;
                    this.setVisible(false);
                    acceptCategories = false;
                }
            } catch (RemoteException e1) {
                Log.printError(e1);
            }
            break;
        }

        if (Controller.getInstance().iSleepTimerStarted()) {
            threadInterrupted = true;
            this.setVisible(false);
            acceptCategories = false;
        }

        th = null;
        th = new Thread(this, "DisplayTimeThread");
        if (acceptCategories) {
            showingEffect.start();
        } else {
            th.start();
        }
    }

    public void closePopup() {
        Controller.getInstance().layeredUI.deactivate();
        PopupController.getInstance().closePopup(this);
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#stopPopup()
     */
    protected void stopPopup(ScheduleData popupOption) {
        Log.printDebug("small stopPopup");
        this.setVisible(false);
        Controller.getInstance().layeredUI.deactivate();
        keylock = false;
        repaint();
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#disposePopup()
     */
    protected void disposePopup() {
        Log.printDebug("small disposePopup");
        actionData = null;
        scheduleData = null;
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.itv.comp.popup.Popup#keyAction(int)
     */
    public boolean keyAction(int keyCode) {
        Log.printDebug("keyCode = " + keyCode);

        if (keylock) {
            return false;
        }

        switch (keyCode) {
        case HRcEvent.VK_ENTER:
            if (okKey) {
                isAction = true;
                acceptCategories = true;
                threadInterrupted = true;
            }
            break;
        case OCRcEvent.VK_EXIT:
            if (popupType == NotificationOption.SLEEP_TIMER_REMAINER_POPUP) {
                Controller.getInstance().layeredUI.deactivate();
                this.setVisible(false);
            } else {
                acceptCategories = false;
                threadInterrupted = true;
            }
            return true;
        case KeyCodes.MENU:
        case KeyCodes.VOD:
        case OCRcEvent.VK_GUIDE:
        case KeyCodes.FAV:
        case KeyCodes.WIDGET:
            if (popupType != NotificationOption.CALLER_ID_POPUP) {
                acceptCategories = false;
                threadInterrupted = true;
                return false;
            } else {
                return true;
            }
        case KeyCodes.SEARCH:
            return false;
        case OCRcEvent.VK_INFO:
        case KeyCodes.REWIND:
        case KeyCodes.FAST_FWD:
        case KeyCodes.RECORD:
        case KeyCodes.COLOR_A:
        case KeyCodes.COLOR_B:
        case KeyCodes.COLOR_C:
        case KeyCodes.COLOR_D:
        case KeyCodes.PIP:
        case KeyEvent.VK_0:
        case KeyEvent.VK_1:
        case KeyEvent.VK_2:
        case KeyEvent.VK_3:
        case KeyEvent.VK_4:
        case KeyEvent.VK_5:
        case KeyEvent.VK_6:
        case KeyEvent.VK_7:
        case KeyEvent.VK_8:
        case KeyEvent.VK_9:
        case OCRcEvent.VK_CHANNEL_UP:
        case OCRcEvent.VK_CHANNEL_DOWN:
        case HRcEvent.VK_PAGE_UP:
        case HRcEvent.VK_PAGE_DOWN:
        case KeyCodes.LAST:
            return true;
        default:
            return false;
        }
        return true;
    }

    public void action() {
        threadInterrupted = true;
        Log.printDebug("action = " + action);
        switch (action) {
        case NotificationOption.ACTION_NONE:
            break;
        case NotificationOption.ACTION_APPLICATION:

            if (actionData != null) {
                String appName = actionData[0];
                Log.printDebug("appName = " + appName);
                String[] parameters = new String[actionData.length - 1];

                for (int a = 0; parameters != null && a < parameters.length; a++) {
                    parameters[a] = actionData[a + 1];
                }

                MonitorService service = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
                String startedAppName = null;
                if (service != null) {
                    try {
                        startedAppName = service.getCurrentActivatedApplicationName();
                    } catch (RemoteException e) {
                        Log.print(e);
                    }
                }
                Log.printDebug("startedAppName = " + startedAppName);
                // getCurrentActivatedApplicationName
                if (startedAppName == null || !startedAppName.equals("PVR")) {
                    CommunicationManager.getInstance().unboundApplicastionStart(appName, parameters);
                } else {
                    Controller.getInstance().directShowMenu(appName, parameters);
                }
                Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
                PopupController.getInstance().removePopup(scheduleData, isAction);
            }
            break;
        case NotificationOption.ACTION_TUNE_CHANNEL:
            if (actionData != null) {
                String number = actionData[0];
                CommunicationManager.getInstance().channelSelect(number);
                Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
                PopupController.getInstance().removePopup(scheduleData, isAction);
            }
            break;
        case NotificationOption.ACTION_SLEEP_TIMER:
        	//R7.3 sangyong VDTRMASTER-6030
        	Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
            //CommunicationManager.getInstance().showRightFilterPinEnabler(new PinEnabler());
            PopupController.getInstance().setScheduleDataOfRemoveReady(scheduleData);
            break;
        case NotificationOption.ACTION_LARGE_POPUP:
            if (hasLargeNotification) {
                PopupController.getInstance().openLargePopup(scheduleData);
            }
            PopupController.getInstance().removePopup(scheduleData, isAction);
            break;
        case NotificationOption.ACTION_PIN_POPUP:
        	//R7.4 Block Admin PIN VDTRMASTER-6205
            CommunicationManager.getInstance().showPinEnabler(null, new PinEnabler(), popupType);
            PopupController.getInstance().setScheduleDataOfRemoveReady(scheduleData);
            break;
        }
    }

    public boolean skipAnimation(Effect effect) {
        return false;
    }

    public void animationStarted(Effect effect) {
        keylock = true;
    }

    public void animationEnded(Effect effect) {
        Log.printDebug("small visible = " + this.isVisible());

        if (acceptCategories) {
            Controller.getInstance().stopScreenSaver();
            Log.printDebug("small start animation");
        }
        Controller.getInstance().layeredUI.activate();
        this.setVisible(true);
        keylock = false;
        th.start();
        repaint();
    }

    class PinEnabler implements RightFilterListener, PinEnablerListener {
        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            Log.printDebug("result = " + response + " detail = " + detail);
            if (PreferenceService.RESPONSE_SUCCESS == response) {
                if (action == NotificationOption.ACTION_SLEEP_TIMER) {
                    Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
                } else if (action == NotificationOption.ACTION_PIN_POPUP) {
                    if (popupType == NotificationOption.PARENTAL_CONTROLS_POPUP) {
                        PreferenceService service = (PreferenceService) DataCenter.getInstance().get(
                                PreferenceService.IXC_NAME);
                        service.setPreferenceValue(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_OFF);
                    } else if (popupType == NotificationOption.TIMEBASED_RESTRICTIONS_POPUP) {
                        Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
                    }
                }
                CommunicationManager.getInstance().hidePinEnabler();
                PopupController.getInstance().removePopup(scheduleData, isAction);
            } else {
                if (action == NotificationOption.ACTION_SLEEP_TIMER) {

                } else if (action == NotificationOption.ACTION_PIN_POPUP) {

                }
            }
        }

        public String[] getPinEnablerExplain() throws RemoteException {
            // TODO Auto-generated method stub
            return null;
        }

        public void receiveCheckRightFilter(int response) throws RemoteException {
            Log.printDebug("result = " + response);
            if (PreferenceService.RESPONSE_SUCCESS == response) {
                if (action == NotificationOption.ACTION_SLEEP_TIMER) {
                    Controller.getInstance().notifyToCallerActionEvent(scheduleData, true);
                } else if (action == NotificationOption.ACTION_PIN_POPUP) {
                    if (popupType == NotificationOption.PARENTAL_CONTROLS_POPUP) {
                        PreferenceService service = (PreferenceService) DataCenter.getInstance().get(
                                PreferenceService.IXC_NAME);
                        service.setPreferenceValue(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_OFF);
                    }
                }
                CommunicationManager.getInstance().hidePinEnabler();
                PopupController.getInstance().removePopup(scheduleData, isAction);
            } else {
                if (action == NotificationOption.ACTION_SLEEP_TIMER) {

                } else if (action == NotificationOption.ACTION_PIN_POPUP) {

                }
            }
        }
    }

    class SmallPopupRenderer extends Renderer {

        public Rectangle getPreferredBounds(UIComponent c) {
            return Rs.SCENE_BOUND;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (position == null) {
                return;
            }

            if (position.equals(TOPLEFT)) {
                leftPopupPaint(g, c);
            } else if (position.equals(BOTTOMLEFT)) {
                g.translate(0, 0);
                leftPopupPaint(g, c);
                g.translate(0, 0);
            } else if (position.equals(BOTTOMRIGHT)) {
                g.translate(-477, 0);
                rightPopupPaint(g, c);
                g.translate(477, 0);
            } else if (position.equals(TOPRIGHT)) {
                g.translate(-477, 0);
                rightPopupPaint(g, c);
                g.translate(477, 0);
            }
        }

        public void leftPopupPaint(Graphics g, UIComponent c) {
            titleX = 83;
            titleY = 64;
            g.drawImage(reminderBg2, -1, 0, c);

            g.setFont(Rs.F17);
            g.setColor(Rs.C241241241);
            boolean closeKey = true;
            switch (popupType) {
            case NotificationOption.CALLER_ID_POPUP:
                g.drawImage(notiIconCallerID[(int) countDown % notiIconCallerID.length], 47, 40, c);
                break;
            case NotificationOption.PPV_REMINDER_POPUP:
            case NotificationOption.PPV_ORDER_REMINDER_POPUP:
                g.drawImage(notiIconClock, 47, 40, c);
                if (okKey) {
                    g.drawImage(okButton, 51, 109, c);
                    g.drawString(stayTuned, 82, 124);
                }

                break;
            case NotificationOption.STC_POPUP:
                g.drawImage(notiIconSTC, 47, 40, c);
                break;
            case NotificationOption.PROGRAM_REMINDER_POPUP:
                g.drawImage(notiIconClock, 47, 40, c);
                g.drawImage(okButton, 51, 109, c);
                g.drawString(tuneNow, 82, 124);
                break;
            case NotificationOption.SLEEP_TIMER_REMAINER_POPUP:
                g.drawImage(notiIconClock, 47, 40, c);
                g.drawImage(okButton, 51, 109, c);
                g.drawString(sleepStayTuned, 82, 124);
            	closeKey = false;
                break;
            case NotificationOption.NOTIFICAION_APPNAME_POPUP:
                g.drawImage(notiIconInfo, 47, 40, c);
                g.drawImage(okButton, 51, 109, c);
                g.drawString(moreDetail, 82, 124);
                break;
            case NotificationOption.TIMEBASED_RESTRICTIONS_POPUP:
            case NotificationOption.PARENTAL_CONTROLS_POPUP:
                g.drawImage(notiIconLock, 47, 40, c);
                g.drawImage(okButton, 51, 109, c);
                g.drawString(unBlocked, 82, 124);
                break;
            case NotificationOption.APD_POPUP:
            	if (isCountDown) {
            		g.drawImage(notiIconLock, 47, 40, c);
            	}
            	if (okKey) {
            		g.drawImage(okButton, 51, 109, c);
            		g.drawString(okName, 82, 124);
            	}
            	closeKey = false;
            	break;
            default:
                break;
            }

            if (closeKey) {
	            g.drawImage(exitButton, 326, 109, c);
	            g.drawString(close, 357, 124);
            }

            g.setFont(Rs.F24);
            g.setColor(Rs.C255255255);
            g.drawString(title, titleX, titleY);

            if (isCountDown) {
                int min = (int) (countDown / 60);
                int sec = (int) (countDown % 60);

                String minute = "00";
                if (min < 10) {
                    minute = "0" + String.valueOf(min);
                } else {
                    minute = String.valueOf(min);
                }

                String second = "00";
                if (sec < 10) {
                    second = "0" + String.valueOf(sec);
                } else {
                    second = String.valueOf(sec);
                }

                g.drawString(" - " + minute + " : " + second, titleX + g.getFontMetrics().stringWidth(title) + 5,
                        titleY);
            }

            if (messageAdd == null) {
            	g.setFont(Rs.F19);
                g.setColor(Rs.C255204000);
            	String[] str = TextUtil.split(message, g.getFontMetrics(), 340);
            	int strLength = str.length;
            	 for (int a = 0; a < strLength; a++) {
                     g.drawString(str[a], 85, 88 + a * 17);
                 }
            } else {
            	if (message == null || message.equals(TextUtil.EMPTY_STRING)) {
            		g.setFont(Rs.F18);
                    g.setColor(Rs.C255255255);
                    String[] str = TextUtil.split(messageAdd, g.getFontMetrics(Rs.F19), 340);
                    for (int a = 0; a < str.length; a++) {
                    	g.drawString(str[a], 85, 88 + a * 17);
                    }
            	} else {
            		String[] str = TextUtil.split(message + "| " + messageAdd, g.getFontMetrics(Rs.F19), 340);
            		boolean foundMessageAdd = false;
            		for (int a = 0; a < str.length; a++) {
            			int idx = str[a].indexOf("|");
            			
            			if (foundMessageAdd) {
            				g.setFont(Rs.F18);
                            g.setColor(Rs.C255255255);
            			} else {
            				g.setFont(Rs.F19);
                            g.setColor(Rs.C255204000);
            			}
            			
            			if (idx != -1) {
            				String preMessage = str[a].substring(0, idx);
            				String subMessage = str[a].substring(idx + 1);
            				int preMessageWidth = g.getFontMetrics(Rs.F19).stringWidth(preMessage);
            				g.setFont(Rs.F19);
                            g.setColor(Rs.C255204000);
                            g.drawString(preMessage, 85, 88 + a * 17);
                            g.setFont(Rs.F18);
                            g.setColor(Rs.C255255255);
                            g.drawString(subMessage, 85 + preMessageWidth, 88 + a * 17);
                            foundMessageAdd = true;
            			} else {
            				g.drawString(str[a], 85, 88 + a * 17);
            			}
                    }
            	}
            }
//            g.setFont(Rs.F19);
//            g.setColor(Rs.C255204000);
//            String str[] = null;
//            str = TextUtil.split(message, g.getFontMetrics(Rs.F19), 340);
//            int strLength = str.length;
//
//            for (int a = 0; a < strLength; a++) {
//                g.drawString(str[a], 85, 88 + a * 17);
//            }
//
//            g.setFont(Rs.F18);
//            g.setColor(Rs.C255255255);
//            if (messageAdd != null) {
//                String temp = "";
//                if (str.length > 0) {
//                    temp = str[strLength - 1];
//                }
//                String[] strMessageAdd = null;
//                if (str.length > 0) {
//                    strMessageAdd = TextUtil.split(temp + "|" + messageAdd, g.getFontMetrics(Rs.F19), 340);
//                } else {
//                    strMessageAdd = TextUtil.split(messageAdd, g.getFontMetrics(Rs.F19), 340);
//                }
//
//                if (strMessageAdd.length < 2) {
//                    int index = strMessageAdd[0].indexOf("|");
//                    String firstMessage = strMessageAdd[0].substring(index + 1, strMessageAdd[0].length());
//                    int messageAddX = g.getFontMetrics(Rs.F19).stringWidth(temp);
//                    if (strLength == 0) {
//                        strLength = 1;
//                    }
//                    g.drawString(" " + firstMessage, 85 + messageAddX, 88 + (strLength - 1) * 17);
//                } else {
//                    for (int a = 0; a < strMessageAdd.length; a++) {
//                    	Log.printDebug("a = " + a +  "strMessageAdd[" + a + "] = " + strMessageAdd[a]);
//                    	
//                        if (a == 0) {
//                            int index = strMessageAdd[0].indexOf("|");
//                            String firstMessage = strMessageAdd[0].substring(index + 1, strMessageAdd[0].length());
//                            Log.printDebug("a = " + a +  "firstMessage = " + firstMessage);
//                            
//                            if (index == -1 && firstMessage != null) {
//                            	g.drawString(strMessageAdd[a], 85, 88 + a * 17);
//                            } else if (index == -1 || firstMessage == null || firstMessage.length() == 0) {
//                            } else {
//                                int messageAddX = g.getFontMetrics(Rs.F19).stringWidth(temp);
//                                g.drawString(" " + firstMessage, 85 + messageAddX, 88 + (strLength - 1) * 17);
//                            }
//                        } else {
//                            int index = strMessageAdd[a].indexOf("|");
//                            if (index == -1) {
//                                g.drawString(strMessageAdd[a], 85, 88 + a * 17);
//                            } else {
//                                String firstMessage = strMessageAdd[a].substring(0, index);
//                                String SecondMessage = strMessageAdd[a].substring(index + 1, strMessageAdd[a].length());
//                                Log.printDebug("a = " + a +  "firstMessage = " + firstMessage);
//                                Log.printDebug("a = " + a +  "SecondMessage = " + SecondMessage);
//                                int width = g.getFontMetrics(Rs.F19).stringWidth(firstMessage);
//                                g.drawString(" " + SecondMessage, 85 + width, 88 + a * 17);
//                            }
//                        }
//                    }
//                }
//            }
        }

        public void rightPopupPaint(Graphics g, UIComponent c) {

            g.drawImage(reminderBg, 477, 0, c);
            g.setFont(Rs.F17);
            g.setColor(Rs.C241241241);
            boolean closeKey = true;
            switch (popupType) {
            case NotificationOption.CALLER_ID_POPUP:
                g.drawImage(notiIconCallerID[(int) countDown % notiIconCallerID.length], 563, 40, c);
                break;
            case NotificationOption.PPV_REMINDER_POPUP:
            case NotificationOption.PPV_ORDER_REMINDER_POPUP:
                g.drawImage(notiIconClock, 563, 40, c);
                if (okKey) {
                    g.drawImage(okButton, 567, 109, c);
                    g.drawString(stayTuned, 598, 124);
                }
                break;
            case NotificationOption.STC_POPUP:
                g.drawImage(notiIconSTC, 563, 40, c);
                break;
            case NotificationOption.PROGRAM_REMINDER_POPUP:
                g.drawImage(notiIconClock, 563, 40, c);
                g.drawImage(okButton, 567, 109, c);
                g.drawString(tuneNow, 598, 124);
                break;
            case NotificationOption.SLEEP_TIMER_REMAINER_POPUP:
                g.drawImage(notiIconClock, 563, 40, c);
                g.drawImage(okButton, 567, 109, c);
                g.drawString(sleepStayTuned, 598, 124);
                closeKey = false;
                break;
            case NotificationOption.NOTIFICAION_APPNAME_POPUP:
                g.drawImage(notiIconInfo, 563, 40, c);
                g.drawImage(okButton, 567, 109, c);
                g.drawString(moreDetail, 598, 124);
                break;
            case NotificationOption.PARENTAL_CONTROLS_POPUP:
            case NotificationOption.TIMEBASED_RESTRICTIONS_POPUP:
                g.drawImage(notiIconLock, 563, 40, c);
                g.drawImage(okButton, 567, 109, c);
                g.drawString(unBlocked, 598, 124);
                break;
            case NotificationOption.APD_POPUP:
            	closeKey = false;
            	if (isCountDown) {
            		g.drawImage(notiIconClock, 563, 40, c);
            	}
            	
            	if (okKey) {
            		g.drawImage(okButton, 567, 109, c);
                    g.drawString(okName, 598, 124);
            	}
            	break;
            default:
                break;
            }

            if (closeKey) {
	            g.drawImage(exitButton, 842, 109, c);
	            g.drawString(close, 873, 124);
            }

            g.setFont(Rs.F24);
            g.setColor(Rs.C255255255);
            g.drawString(title, titleX, titleY);

            if (isCountDown) {
                int min = (int) (countDown / 60);
                int sec = (int) (countDown % 60);

                String minute = "00";
                if (min < 10) {
                    minute = "0" + String.valueOf(min);
                } else {
                    minute = String.valueOf(min);
                }

                String second = "00";
                if (sec < 10) {
                    second = "0" + String.valueOf(sec);
                } else {
                    second = String.valueOf(sec);
                }

                g.drawString(" - " + minute + " : " + second, titleX + g.getFontMetrics().stringWidth(title) + 5,
                        titleY);
            }

            if (messageAdd == null) {
            	g.setFont(Rs.F19);
                g.setColor(Rs.C255204000);
            	String[] str = TextUtil.split(message, g.getFontMetrics(), 340);
            	int strLength = str.length;
            	 for (int a = 0; a < strLength; a++) {
                     g.drawString(str[a], 601, 88 + a * 17);
                 }
            } else {
            	if (message == null || message.equals(TextUtil.EMPTY_STRING)) {
            		g.setFont(Rs.F18);
                    g.setColor(Rs.C255255255);
                    String[] str = TextUtil.split(messageAdd, g.getFontMetrics(Rs.F19), 340);
                    for (int a = 0; a < str.length; a++) {
                    	g.drawString(str[a], 601, 88 + a * 17);
                    }
            	} else {
            		String[] str = TextUtil.split(message + "| " + messageAdd, g.getFontMetrics(Rs.F19), 340);
            		boolean foundMessageAdd = false;
            		for (int a = 0; a < str.length; a++) {
            			int idx = str[a].indexOf("|");
            			
            			if (foundMessageAdd) {
            				g.setFont(Rs.F18);
                            g.setColor(Rs.C255255255);
            			} else {
            				g.setFont(Rs.F19);
                            g.setColor(Rs.C255204000);
            			}
            			
            			if (idx != -1) {
            				String preMessage = str[a].substring(0, idx);
            				String subMessage = str[a].substring(idx + 1);
            				int preMessageWidth = g.getFontMetrics(Rs.F19).stringWidth(preMessage);
            				g.setFont(Rs.F19);
                            g.setColor(Rs.C255204000);
                            g.drawString(preMessage, 601, 88 + a * 17);
                            g.setFont(Rs.F18);
                            g.setColor(Rs.C255255255);
                            g.drawString(subMessage, 601 + preMessageWidth, 88 + a * 17);
                            foundMessageAdd = true;
            			} else {
            				g.drawString(str[a], 601, 88 + a * 17);
            			}
                    }
            	}
            }
            
//            g.setFont(Rs.F19);
//            g.setColor(Rs.C255204000);
//            String str[] = null;
//            message = "Talk to me: The story of James Patrick Peek.";
//            if (messageAdd != null) {
//            	str = TextUtil.split(message + " |", g.getFontMetrics(), 340, "|");
//            } else {
//            	str = TextUtil.split(message, g.getFontMetrics(), 340);
//            }
//            int strLength = str.length;
//
//            for (int a = 0; a < strLength; a++) {
//                g.drawString(str[a], 601, 88 + a * 17);
//            }
//
//            //g.setFont(Rs.F18);
//            g.setColor(Rs.C255255255);
//            if (messageAdd != null) {
//                String temp = "";
//                if (str.length > 0) {
//                    temp = str[strLength - 1];
//                }
//                String[] strMessageAdd = null;
//                if (str.length > 0) {
//                    strMessageAdd = TextUtil.split(temp + "|" + messageAdd, g.getFontMetrics(Rs.F19), 340);
//                } else {
//                    strMessageAdd = TextUtil.split(messageAdd, g.getFontMetrics(Rs.F19), 340);
//                }
//                if (strMessageAdd.length < 2) {
//                    int index = strMessageAdd[0].indexOf("|");
//                    String firstMessage = strMessageAdd[0].substring(index + 1, strMessageAdd[0].length());
//                    int messageAddX = g.getFontMetrics(Rs.F19).stringWidth(temp);
//                    if (strLength == 0) {
//                        strLength = 1;
//                    }
//                    g.drawString(" " + firstMessage, 601 + messageAddX, 88 + (strLength - 1) * 17);
//                } else {
//                    for (int a = 0; a < strMessageAdd.length; a++) {
//                    	Log.printDebug("a = " + a +  ", strMessageAdd[" + a + "] = " + strMessageAdd[a]);
//                        if (a == 0) {
//                            int index = strMessageAdd[0].indexOf("|");
//                            String firstMessage = strMessageAdd[0].substring(index + 1, strMessageAdd[0].length());
//                            Log.printDebug("a = " + a +  ", firstMessage = " + firstMessage);
//                            if (index == -1 && firstMessage != null) {
//                            	g.drawString(strMessageAdd[a], 601, 88 + a * 17);
//                            } else if (index == -1 || firstMessage == null || firstMessage.length() == 0) {
//                            } else {
//                                int messageAddX = g.getFontMetrics(Rs.F19).stringWidth(temp);
//                                g.drawString(" " + firstMessage, 601 + messageAddX, 88 + (strLength - 1) * 17);
//                            }
//                        } else {
//                            int index = strMessageAdd[a].indexOf("|");
//                            if (index == -1) {
//                                g.drawString(strMessageAdd[a], 601, 88 + a * 17);
//                            } else {
//                                String firstMessage = strMessageAdd[a].substring(0, index);
//                                String secondMessage = strMessageAdd[a].substring(index + 1, strMessageAdd[a].length());
//                                Log.printDebug("a = " + a +  ", firstMessage = " + firstMessage);
//                                Log.printDebug("a = " + a +  ", secondMessage = " + secondMessage);
//                                int width = g.getFontMetrics(Rs.F19).stringWidth(firstMessage);
//                                g.drawString(" " + secondMessage, 601 + width, 88 + a * 17);
//                            }
//                        }
//                    }
//                }
//            }
        }

        public void prepare(UIComponent c) {

            notiIconCallerID[0] = DataCenter.getInstance().getImage("16_icon003.png");
            notiIconCallerID[1] = DataCenter.getInstance().getImage("16_icon002.png");
            notiIconCallerID[2] = DataCenter.getInstance().getImage("16_icon001.png");

            reminderBg = DataCenter.getInstance().getImage("reminder_bg.png");
            reminderBg2 = DataCenter.getInstance().getImage("reminder_bg_l.png");
            notiIconClock = DataCenter.getInstance().getImage("noti_icn_01.png");
            notiIconSTC = DataCenter.getInstance().getImage("noti_icn_02.png");
            notiIconInfo = DataCenter.getInstance().getImage("noti_icn_03.png");
            notiIconLock = DataCenter.getInstance().getImage("noti_icn_04.png");

            Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
            if (imgTable != null) {
                okButton = (Image) imgTable.get(PreferenceService.BTN_OK);
                exitButton = (Image) imgTable.get(PreferenceService.BTN_EXIT);
            } else {
                okButton = DataCenter.getInstance().getImage("ok.png");
                exitButton = DataCenter.getInstance().getImage("btn_exit.png");
            }

            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    public void run() {
        int count = 0;
        ForceStart = false;
        Log.printDebug("onceOfDisplay = " + delayTimeOfDisplay);

        while (!threadInterrupted && acceptCategories && ((delayTimeOfDisplay > (count * ONE_SCEND)))) {
            Log.printDebug("count * SLEEP_TIME = " + (count * ONE_SCEND));

            if (ForceStart) {
                acceptCategories = false;
                isAction = true;
                break;
            }

            count++;
            repaint();
            if (countDown > 0) {
                countDown = (delayTimeOfDisplay - (System.currentTimeMillis() - startTime)) / ONE_SCEND;
            }
           
            try {
                Thread.sleep(ONE_SCEND);
            } catch (InterruptedException e) {
                Log.printError(e);
            }
            
            if(isCountDown && countDown <= 0) {
                count = 120;
                break;
            }
        }

        // 시간이 지났을때 행동
        if (delayTimeOfDisplay <= (count * ONE_SCEND)) {
            acceptCategories = false;
        }
        threadInterrupted = true;
        if (threadInterrupted) {
            if (priority == FORCE_START) {
                ForceStart = false;
            }
            reStrart();
        }
    }
}

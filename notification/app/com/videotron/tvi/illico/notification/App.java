package com.videotron.tvi.illico.notification;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.controller.Controller;

/**
 * The initial class of Notification.
 *
 * @author PellOs
 */
public class App implements Xlet, ApplicationConfig {
    /** XletContext instance. */
    public static XletContext xletContext;
    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx
     *            The XletContext of this Xlet.
     * @throws XletStateChangeException
     *             If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        Log.printInfo("Notification initXlet start");
        xletContext = ctx;
        Log.setApplication(this);
        FrameworkMain.getInstance().init(this);
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException
     *             is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        Log.printInfo("Notification startXlet start");
        FrameworkMain.getInstance().start();
        Controller.getInstance().start();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional
     *            If unconditional is true when this method is called, requests by the Xlet to not enter the destroyed
     *            state will be ignored.
     * @throws XletStateChangeException
     *             is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed state). This exception
     *             is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();
//        SceneManager.getInstance().dispose();
    }

    /**
     * Gets the version of this application.
     * @return the version of this application.
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }

    /**
     * Gets the name of this application.
     * @return the name of this application.
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }

    /**
     * Gets the XletContext instance of this application.
     * @return XletContext instance of this application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

	public void init() {
        try {
		    Controller.getInstance().init(xletContext);
        } catch (Exception ex) {
            Log.print(ex);
        }

	}
}

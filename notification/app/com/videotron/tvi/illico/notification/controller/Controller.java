package com.videotron.tvi.illico.notification.controller;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.tv.xlet.XletContext;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.monitor.BlockedTimeListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.Rs;
import com.videotron.tvi.illico.notification.comp.popup.PopupController;
import com.videotron.tvi.illico.notification.data.CommunicationManager;
import com.videotron.tvi.illico.notification.data.DataManager;
import com.videotron.tvi.illico.notification.data.FlashMemoryController;
import com.videotron.tvi.illico.notification.data.ScheduleData;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.util.Constants;

public class Controller implements DataUpdateListener, ClockListener, BlockedTimeListener, STBModeChangeListener {

    private static Controller controller = new Controller();
    /** The xlet context. */
    private XletContext xletContext;

    /*****************************************************************
     * Variables - Layered-related
     *****************************************************************/
    public LayeredUI layeredUI;
    private LayeredWindow layeredWindow;
    private LayeredKeyNotification layeredKeyWizard;

    /**
     * key = messageID, value = ScheduleData.
     */
    private static Hashtable scheduleData = null;
    private static ScheduleData[] historyScheduleData;

    public static int stbMode = MonitorService.STB_MODE_NORMAL;

    /**
     * key = ApplicationName, value = Listener.
     */
    private Hashtable listenerHashtable = new Hashtable();

    private int checkCount = 0;

    private boolean sleepTimerStarted = false;

    public static Controller getInstance() {
        return controller;
    }

    public void init(XletContext xc) {
        xletContext = xc;
        DataManager.getInstance().readProperty();
        Object ob = FlashMemoryController.getInstance().readData();
        scheduleData = new Hashtable();

        if (ob != null) {
            historyScheduleData = (ScheduleData[]) ob;
        }

        // DataCenter.getInstance().addDataUpdateListener(inband1, this);
        PopupController.getInstance().initManager();

        Clock.getInstance().addClockListener(this);
    }

    public static Hashtable getScheduleData() {
        return scheduleData;
    }

    public void start() {
        if (layeredWindow == null) {
            layeredWindow = new LayeredWindowNotification();
            layeredWindow.setBounds(Rs.SCENE_BOUND);
            layeredWindow.setVisible(true);
        }

        if (layeredKeyWizard == null) {
            layeredKeyWizard = new LayeredKeyNotification();
        }

        if (layeredUI == null) {
            layeredUI = WindowProperty.NOTIFICATION
                    .createLayeredDialog(layeredWindow, Rs.SCENE_BOUND, layeredKeyWizard);
            // layeredUI.activate();
        }

        if (historyScheduleData != null) {
            for (int a = 0; a < historyScheduleData.length; a++) {
                ScheduleData sd = historyScheduleData[a];
                if (Log.DEBUG_ON) {
                    Log.printDebug(sd);
                }
                if (sd != null && sd.getNotificationPopupType() != NotificationOption.SLEEP_TIMER_REMAINER_POPUP) {
                    if (System.currentTimeMillis() <= sd.getDisplayTime()) {
                        scheduleData.put(sd.getMessageID(), sd);
                        PopupController.getInstance().createPopup(sd);
                    }
                }
            }
        }
        CommunicationManager.getInstance().start(xletContext, this);
    }

    public LayeredWindow getLayeredWindow() {
        return layeredWindow;
    }

    public Hashtable getListenerHashtable() {
        return listenerHashtable;
    }

    public void dispose() {
        PopupController.getInstance().disposeManager();
        CommunicationManager.getInstance().destroy();
        listenerHashtable.clear();
        historyScheduleData = null;
        scheduleData = null;
    }

    public String createID(String name, int num) {
        String creatID = null;
        while (scheduleData.containsKey(name + "|" + String.valueOf(num))) {
            num++;
        }
        creatID = name + "|" + String.valueOf(num);
        return creatID;
    }

    public void notifyToCallerActionEvent(ScheduleData data, boolean action) {
        String key = null;
        if (data == null) {
            return;
        }
        key = data.getApplicationName();

        if (key != null) {
            NotificationListener listener = (NotificationListener) listenerHashtable.get(key);
            String id;
            if (listener == null) {
                return;
            }
            try {
                id = data.getMessageID();
                if (action) {
                    listener.action(id);
                } else {
                    listener.cancel(id);
                }
            } catch (RemoteException e) {
                Log.print(e);
                showErrorMessage("NOT501");
            }
        }
    }

    public void directShowMenu(String appName, String[] parameters) {
        if (appName != null) {
            NotificationListener listener = (NotificationListener) listenerHashtable.get(appName);
            if (listener == null) {
                return;
            }
            try {
                listener.directShowRequested(appName, parameters);
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    public void changeScheduleData(ScheduleData data) {
        String key = null;
        key = data.getMessageID();

        Log.printDebug("changeScheduleData = " + key);
        scheduleData.put(key, data);
    }

    public void checkForDeleteOfScheduleData() {
        Log.printDebug("checkForDeleteOfScheduleData");
        Enumeration eData = scheduleData.elements();
        long currentDate = System.currentTimeMillis();
        long storedTime = DataManager.getInstance().getDeleteTime() * Constants.MS_PER_MINUTE;
        Vector messageIDs = new Vector();

        while (eData.hasMoreElements()) {
            ScheduleData data = (ScheduleData) eData.nextElement();
            long displayDate = data.getDisplayTime();
//            long createDate = data.getCreateDate();
            Log.printDebug("displayDate = " + new Date(displayDate));
            Log.printDebug("storedTime = " + storedTime);
            Log.printDebug("currentDate = " + new Date(currentDate));

            if (displayDate < currentDate - storedTime) {
                messageIDs.add(data.getMessageID());
                Log.printDebug("delete check data = " + data.getMessageID());
            }
        }

        for (int a = 0; a < messageIDs.size(); a++) {
            Log.printDebug("delete data = " + (String) messageIDs.elementAt(a));
            ScheduleData data = (ScheduleData) scheduleData.remove((String) messageIDs.elementAt(a));
            PopupController.getInstance().deletePopup(data);
            notifyToCallerActionEvent(data, false);
        }

        if (messageIDs.size() != 0) {
            FlashMemoryController.getInstance().writeData(Controller.getScheduleData());
        }
    }

    /**
     * LayerUI. Is Key Controller.
     * @author pellos.
     */
    class LayeredKeyNotification implements LayeredKeyHandler {

        /**
         * is received key event and sends to scene.
         */
        public boolean handleKeyEvent(UserEvent userEvent) {
            if (userEvent == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredKeyWizard.handleKeyEvent]User Event is null.");
                }
                return false;
            }
            if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
                return false;
            }

            return PopupController.getInstance().keyAction(userEvent.getCode());
        }
    }

    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowNotification extends LayeredWindow {
        private static final long serialVersionUID = 1L;

        /**
         * Constructor. Sets LayersWindow.
         */
        public LayeredWindowNotification() {
            setBounds(Rs.SCENE_BOUND);
        }
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (key.equals(MonitorService.IXC_NAME)) {
//            CommunicationManager.getInstance().addInbandDataListener();

            MonitorService monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            try {
                monitorService.addBlockedTimeListener(this, Rs.APP_NAME);
                monitorService.addSTBModeChangeListener(this, Rs.APP_NAME);
                stbMode = monitorService.getSTBMode();
                Log.printDebug("stbMode = " + stbMode);
            } catch (RemoteException e) {
                Log.print(e);
            }

        } else if (key.equals(PreferenceService.IXC_NAME)) {
            CommunicationManager.getInstance().addPreferenceListener();

        } else if (key.equals(StcService.IXC_NAME)) {

            try {
                StcService stcService = (StcService) DataCenter.getInstance().get(StcService.IXC_NAME);
                int currentLevel = stcService.registerApp(Rs.APP_NAME);
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);

                Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                if (o != null) {
                    DataCenter.getInstance().put("LogCache", o);
                }
                try {
                    stcService.addLogLevelChangeListener(Rs.APP_NAME, new LogLevelAdapter());
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
            } catch (Exception e) {
                Log.printError(e);
            }
        }
    }

    public void stopScreenSaver() {
        Log.printDebug("ResetScreenSaverTimer");
        MonitorService monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        try {
            if(monitorService != null) {
                monitorService.resetScreenSaverTimer();
            }
        } catch (RemoteException e) {
            Log.print(e);
        }

    }

    public void dataRemoved(String key) {
    }

    public void clockUpdated(Date date) {
        checkCount++;
        // 10minutes
//        if (checkCount % 10 == 0) {
            Thread th = new Thread() {
                public void run() {
                    checkForDeleteOfScheduleData();
                }
            };
            th.start();
            checkCount = 0;
//        }
    }

    public boolean iSleepTimerStarted() {
        Log.printInfo("sleepTimerStarted = " + sleepTimerStarted);
        return sleepTimerStarted;
    }

    public void startBlockedTime() throws RemoteException {
        Log.printInfo("startBlockedTime");
        if (CommunicationManager.getInstance().isPinPopupStarted()) {
            PopupController.getInstance().removeReady();
            CommunicationManager.getInstance().hidePinEnabler();

        }
        sleepTimerStarted = true;
    }

    public void stopBlockedTime() throws RemoteException {
        Log.printInfo("stopBlockedTime");
        sleepTimerStarted = false;
    }

    public void showErrorMessage(String code) {

        ErrorMessageService errorMgsService = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        if (errorMgsService != null) {
            try {
                errorMgsService.showErrorMessage(code, null);
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    class LogLevelAdapter implements LogLevelChangeListener {
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    public void modeChanged(int newMode) throws RemoteException {
        Log.printDebug("new stbMode = " + stbMode);
        stbMode = newMode;
    }
}

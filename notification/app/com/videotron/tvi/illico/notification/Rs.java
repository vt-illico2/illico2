package com.videotron.tvi.illico.notification;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of Notification.
 *
 * @author Pellos
 */
public class Rs {
    /*********************************************************************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */
    public static final String APP_VERSION = "(R7.4).1.1";
    /** Application Name. */
    public static final String APP_NAME = "Notification";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;

    /*********************************************************************************
     * Font-related
     **********************************************************************************/
    public static final Font F14 = FontResource.BLENDER.getFont(14);
    public static final Font F15 = FontResource.BLENDER.getFont(15);
    public static final Font F16 = FontResource.BLENDER.getFont(16);
    public static final Font F17 = FontResource.BLENDER.getFont(17);
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    public static final Font F19 = FontResource.BLENDER.getFont(19);
    public static final Font F20 = FontResource.BLENDER.getFont(20);
    public static final Font F21 = FontResource.BLENDER.getFont(21);
    public static final Font F22 = FontResource.BLENDER.getFont(22);
    public static final Font F23 = FontResource.BLENDER.getFont(23);
    public static final Font F24 = FontResource.BLENDER.getFont(24);
    public static final Font F25 = FontResource.BLENDER.getFont(25);
    public static final Font F26 = FontResource.BLENDER.getFont(26);
    public static final Font F28 = FontResource.BLENDER.getFont(28);
    public static final Font F30 = FontResource.BLENDER.getFont(30);
    public static final Font F32 = FontResource.BLENDER.getFont(32);
    public static final Font F34 = FontResource.BLENDER.getFont(34);

    public static final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    public static final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    public static final FontMetrics FM19 = FontResource.getFontMetrics(F19);
    public static final FontMetrics FM20 = FontResource.getFontMetrics(F20);
    public static final FontMetrics FM21 = FontResource.getFontMetrics(F21);
    public static final FontMetrics FM22 = FontResource.getFontMetrics(F22);
    public static final FontMetrics FM24 = FontResource.getFontMetrics(F24);
    public static final FontMetrics FM25 = FontResource.getFontMetrics(F25);
    public static final FontMetrics FM30 = FontResource.getFontMetrics(F30);
    public static final FontMetrics FM32 = FontResource.getFontMetrics(F32);
    public static final FontMetrics FM34 = FontResource.getFontMetrics(F34);

    /*********************************************************************************
     * Color-related
     *********************************************************************************/
    public static final Color C000000000 = new Color(0, 0, 0);
    public static final Color C003003003 = new Color(3, 3, 3);
    public static final Color C035035035 = new Color(35, 35, 35);
    public static final Color C041041041 = new Color(41, 41, 41);
    public static final Color C046046046 = new Color(46, 46, 46);
    public static final Color C055055055 = new Color(55, 55, 55);
    public static final Color C133133133 = new Color(133, 133, 133);
    public static final Color C143143143 = new Color(143, 143, 143);
    public static final Color C161160160 = new Color(161, 160, 160);
    public static final Color C164164164 = new Color(164, 164, 164);
    public static final Color C171171171 = new Color(171, 171, 171);
    public static final Color C182182182 = new Color(182, 182, 182);
    public static final Color C184184184 = new Color(184, 184, 184);
    public static final Color C187189192 = new Color(187, 189, 192);
    public static final Color C194192192 = new Color(194, 192, 192);
    public static final Color C198198198 = new Color(198, 198, 198);
    public static final Color C200200200 = new Color(200, 200, 200);
    public static final Color C202174097 = new Color(202, 174, 97);
    public static final Color C229229229 = new Color(229, 229, 229);
    public static final Color C224224224 = new Color(224, 224, 224);
    public static final Color C230230230 = new Color(230, 230, 230);
    public static final Color C236211143 = new Color(236, 211, 143);
    public static final Color C236236237 = new Color(236, 236, 237);
    public static final Color C238238238 = new Color(238, 238, 238);
    public static final Color C241241241 = new Color(241, 241, 241);
    public static final Color C255204000 = new Color(255, 204, 0);
    public static final Color C252202004 = new Color(252, 202, 4);
    public static final Color C255234160 = new Color(255, 234, 160);
    public static final Color C255255255 = new Color(255, 255, 255);
    public static final Color C236236236 = new Color(236, 236, 236);
    public static final Color C124124124 = new Color(124, 124, 124);

    public static final Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    public static final Color DVB102102102179 = new DVBColor(102, 102, 102, 179);
    public static final Color DVB255255255128 = new DVBColor(255, 255, 255, 128);
    public static final Color DVB255255255102 = new DVBColor(255, 255, 255, 102);

    /*********************************************************************************
     * Key Code-related
     *********************************************************************************/
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    public static final int KEY_PAGEUP = OCRcEvent.VK_PAGE_UP;
    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;

    /** Screen Size. */
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, SCREEN_SIZE_WIDTH, SCREEN_SIZE_HEIGHT);
}

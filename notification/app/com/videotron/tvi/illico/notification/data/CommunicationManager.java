package com.videotron.tvi.illico.notification.data;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * This class connects the IXC.
 * @author pellos
 */
public class CommunicationManager implements Remote {

    /** XletContext. */
    private XletContext xletContext;
    /** Class instance. */
    private static CommunicationManager instance = new CommunicationManager();

    private NotificationService notificationService;

//    /** turning channel listener for receiving inbandData. */
//    private InbandListener inbandListener = new InbandListener();

    /** Key set. */
    public static String[] preferenceKeys;
    private int version = -1;

    private boolean pinPopupStarted = false;

    private RightFilterListener RListener;
    private PinEnablerListener pListener;

    PreferenceService preferenceService;

    private IxcLookupWorker lookupWorker;
    
    private MonitorService monitorService;

    /**
     * Get CommunicationManager instance.
     * @return CommunicationManager
     */
    public static CommunicationManager getInstance() {
        return instance;
    }

    /**
     * lookup Monitor, EPG, UPP, LoadingAnimation. bind CallerIDService to IXC.
     * @param xCtx XletContext
     */
    public void start(XletContext xletContext, DataUpdateListener l) {
        this.xletContext = xletContext;
        notificationService = new NotificationSerivceImpl();

        try {
            IxcRegistry.bind(xletContext, NotificationService.IXC_NAME, notificationService);
        } catch (Exception ex) {
            Log.print(ex);
        }

        lookupWorker = new IxcLookupWorker(xletContext);
        lookupWorker.lookup("/1/1/", MonitorService.IXC_NAME, l);
        lookupWorker.lookup("/1/2026/", EpgService.IXC_NAME);
        lookupWorker.lookup("/1/1020/", VODService.IXC_NAME);
        lookupWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, l);
        lookupWorker.lookup("/1/2100/", StcService.IXC_NAME, l);
        lookupWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME, l);
    }

    /**
     * Look up service.
     * @param path path string
     * @param name IXC name
    private void lookUpService(String path, final String name) {
        final String ixcName = path + name;
        Thread thread = new Thread("Cinema.lookUp." + name) {
            public void run() {
                Remote remote = null;
                while (remote == null) {
                    try {
                        remote = IxcRegistry.lookup(xletContext, ixcName);
                    } catch (Exception ex) {
                    }
                    if (remote != null) {
                        if (Log.INFO_ON) {
                            Log.printInfo("CommunicationManager: found - " + name);
                        }
                        DataCenter.getInstance().put(name, remote);
                        return;
                    }
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                    }
                }
            }
        };
        thread.start();
    }
     */


//    public void addInbandDataListener() {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("lookupMonitorService: called");
//        }
//        try {
//            MonitorService monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
//            String appName = FrameworkMain.getInstance().getApplicationName();
//            monitorService.addInbandDataListener(inbandListener, "notification");
//
//            Log.printDebug("monitorService Lookup completed = " + appName);
//            Log.printDebug("monitorService Lookup completed inbandListener = " + inbandListener);
//
//            if (Log.INFO_ON) {
//                Log.printInfo("monitorService Lookup completed");
//            }
//
//        } catch (Exception e) {
//            Log.printError(e);
//        }
//    }

    /**
     * Called by {@link App#destroyXlet(boolean)} when LoadingAnimation Application destroy.
     */
    public void destroy() {
        try {
            IxcRegistry.unbind(xletContext, NotificationService.IXC_NAME);
        } catch (Exception e) {
            Log.print(e);
        }
    }

//    /**
//     * This class is turning channel listener for receiving inbandData.
//     * @author pellos
//     */
//    class InbandListener implements InbandDataListener {
//
//        /**
//         * channel turn Listener.
//         * @param locator InbandData Name.
//         */
//        public void receiveInbandData(String locator) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("receiveInbandData = " + locator);
//            }
//            int newVersion = 0;
//            MonitorService monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
//            try {
//                newVersion = monitorService.getInbandDataVersion(MonitorService.notification);
//                Log.printDebug("receiveInbandData newVersion = " + newVersion);
//            } catch (RemoteException e1) {
//                Log.printError(e1);
//            }
//
//            if (newVersion != version) {
//                version = newVersion;
//            } else {
//                return;
//            }
//
//            try {
//                monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
//            } catch (Exception e) {
//                Log.printError(e);
//            }
//        }
//    }

    /**
     * Start unbound Applicastion.
     * @param appName is name
     * @param parameters is parameter
     * @return result is boolean
     */
    public boolean unboundApplicastionStart(String appName, String[] parameters) {
        boolean result = false;
        try {
            MonitorService monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            result = monitorService.startUnboundApplication(appName, parameters);
        } catch (RemoteException e) {
            Log.print(e);
        }
        return result;
    }

    /**
     * select Channel.
     * @param number channel number.
     */
    public void channelSelect(int number) {
        Log.printInfo("channelSelect number = " + number);
        try {

            MonitorService monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

            int state = monitorService.getState();
            if (state == MonitorService.FULL_SCREEN_APP_STATE) {
                try {
                    monitorService.exitToChannel();
                } catch (RemoteException e) {
                    Log.print(e);
                }
            }
            EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            epgService.changeChannel(number);
        } catch (RemoteException e) {
            Log.printError(e);
        }
    }

    /**
     * select Channel.
     * @param number channel name
     */
    public void channelSelect(String name) {
        Log.printInfo("channel name = " + name);
        try {
            MonitorService monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

            int state = monitorService.getState();
            if (state == MonitorService.FULL_SCREEN_APP_STATE) {
                try {
                    monitorService.exitToChannel();
                } catch (RemoteException e) {
                    Log.print(e);
                }
            }
            EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            TvChannel tv = epgService.getChannel(name);
            epgService.changeChannel(tv);
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    public void showRightFilterPinEnabler(RightFilterListener listener) {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);

        try {
            RListener = listener;
            preferenceService.checkRightFilter(listener, FrameworkMain.getInstance().getApplicationName(),
                    new String[] {RightFilter.ALLOW_ADULT_CATEGORY}, null, Definitions.RATING_G);
            pinPopupStarted = true;
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    public boolean showPinEnabler(String[] msg, PinEnablerListener listener, int popupType) {
        boolean result = false;
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        pListener = listener;
        if (preferenceService == null) {
            return result;
        } else {
            result = true;;
        }

        if (msg == null) {
        	//R7.3 sangyong VDTRMASTER-6030
        	if (popupType == NotificationOption.TIMEBASED_RESTRICTIONS_POPUP) {
        		String lang = (String) DataCenter.getInstance().get(PreferenceNames.LANGUAGE);
        		if (lang.equals(Definitions.LANGUAGE_ENGLISH)) {
        			msg = new String[] {"Enter the Admin PIN to cancel the terminal block."};
        		} else {
        			msg = new String[] {"Entrez le NIP admin pour annuler le blocage du terminal."};
        		}
        	} else
        		msg = new String[] {"Enter Your PIN"};
        }
        try {
            preferenceService.showPinEnabler(listener, msg);
            pinPopupStarted = true;
        } catch (RemoteException e) {
            Log.print(e);
        }

        return result;
    }

    public void hidePinEnabler() {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.hidePinEnabler();
                pinPopupStarted = false;
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    class PreferenceListenerImpl implements PreferenceListener {
        public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
            DataCenter.getInstance().put(preferenceName, value);
        }
    }

    public boolean isPinPopupStarted() {
        return pinPopupStarted;
    }

    /**
     * register listener to UPP.
     */
    public void addPreferenceListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, addPreferenceListener");
        }
        if (preferenceService == null) {
            preferenceService = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        }
        if (preferenceService == null)
            return;
        try {
            String[] keys = {PreferenceNames.LANGUAGE, RightFilter.PARENTAL_CONTROL, RightFilter.BLOCK_BY_RATINGS,
                    RightFilter.HIDE_ADULT_CONTENT};
            String[] values = new String[keys.length];

            String[] result = preferenceService.addPreferenceListener(new PreferenceListenerImpl(), FrameworkMain
                    .getInstance().getApplicationName(), keys, values);

            if (result != null) {
                for (int a = 0; a < result.length; a++) {
                    Log.printDebug("key[" + a + "] = " + keys[a] + " values = " + result[a]);
                    DataCenter.getInstance().put(keys[a], result[a]);
                }
            }

        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    public static boolean isLock(String rating) {
        DataCenter dc = DataCenter.getInstance();
        if (!Definitions.OPTION_VALUE_ON.equals(dc.get(RightFilter.PARENTAL_CONTROL))) {
            return false;
        }

        if (Definitions.OPTION_VALUE_YES.equals(dc.get(RightFilter.HIDE_ADULT_CONTENT)) && rating.equals(Definitions.RATING_18)) {
            return true;
        }

        String uppRating = DataCenter.getInstance().getString(RightFilter.BLOCK_BY_RATINGS);
        String[] ratingArr = new String[] {Definitions.RATING_G, Definitions.RATING_8,
                Definitions.RATING_13, Definitions.RATING_16, Definitions.RATING_18};

        if (uppRating.equals(Definitions.RATING_G)) {
            return false;
        }

        int uppIndex = -1;
        int contentIndex = -1;
        for (int i = 0; i < ratingArr.length; i++) {
            if (ratingArr[i].equals(uppRating)) {
                uppIndex = i;
                break;
            }
        }

        for (int i = 0; i < ratingArr.length; i++) {
            if (ratingArr[i].equals(rating)) {
                contentIndex = i;
                break;
            }
        }

        Log.printDebug("uppIndex put2 = " + uppIndex);
        Log.printDebug("contentIndex put2 = " + contentIndex);
        if (contentIndex >= uppIndex || contentIndex == ratingArr.length - 1) {
            return true;
        }

        return false;
    }

    public static int getRatingIndex(String rating) {
        for (int i = 0; i < Constants.PARENTAL_RATING_NAMES.length; i++) {
            if (Constants.PARENTAL_RATING_NAMES[i].equals(rating)) {
                return i;
            }
        }
        return 0;
    }
}

package com.videotron.tvi.illico.notification.data;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.notification.NotificationOption;

public class ReturnScheduleData implements NotificationOption {
    
    private long createDate;    
    private boolean viewed = false;    
    private int displayCount = 0;
    private String applicationName;

    private String messageID;
    private String message;
    private int categories;
    private int notificationType;
    private long displayTime;
    private int remainderTime;
    private long remainerDelay;
    private int notificationPopupType;
    private int notificationAction;
    private String[] notificationActionData;
    private String largepopupMessage;
    private String largepopupSubMessage;    
    private String subMessage;
    private boolean countDown;
    
    private String[][] largeButtonInfo;
    
    private long notifyTime;
    
    public String getApplicationName() throws RemoteException {
        return applicationName;
    }

    public int getCategories() throws RemoteException {
        return categories;
    }

    public long getCreateDate() throws RemoteException {
        return createDate;
    }

    public int getDisplayCount() throws RemoteException {
        return displayCount;
    }

    public long getDisplayTime() throws RemoteException {
        return displayTime;
    }

    public String getMessage() throws RemoteException {
        return message;
    }

    public String getMessageID() throws RemoteException {
        return messageID;
    }

    public int getNotificationAction() throws RemoteException {
        return notificationAction;
    }

    public String[] getNotificationActionData() throws RemoteException {
        return notificationActionData;
    }

    public int getNotificationPopupType() throws RemoteException {
        return notificationPopupType;
    }

    public int getNotificationType() throws RemoteException {
        return notificationType;
    }

    public long getRemainderDelay() throws RemoteException {
        return remainerDelay;
    }

    public int getRemainderTime() throws RemoteException {
        return remainderTime;
    }

    public boolean isViewed() throws RemoteException {
        return viewed;
    }

    public void setViewed(boolean b) throws RemoteException {
        viewed = b;
    }

    public void setApplicationName(String name) throws RemoteException {
        applicationName = name;
        
    }

    public void setCategories(int categories) throws RemoteException {
        this.categories = categories;
        
    }

    public void setCreateDate(long createDate) throws RemoteException {
        this.createDate = createDate;
    }   

    public void setDisplayCount(int count) throws RemoteException {
        displayCount = count;
    }

    public void setDisplayTime(long time) throws RemoteException {
        displayTime = time;        
    }

    public void setMessage(String message) throws RemoteException {
        this.message = message;
    }

    public void setMessageID(String id) throws RemoteException {
        messageID = id;
    }

    public void setNotificationAction(int action) throws RemoteException {
        notificationAction = action;
    }

    public void setNotificationActionData(String[] param) throws RemoteException {
        notificationActionData = param;
    }

    public void setNotificationPopupType(int status) throws RemoteException {
        notificationPopupType = status;
    }

    public void setNotificationType(int type) throws RemoteException {
        notificationType = type;
    }

    public void setRemainderDelay(long delay) throws RemoteException {
        remainerDelay = delay;
    }

    public void setRemainderTime(int time) throws RemoteException {
        remainderTime = time;   
    }
    
    public String getLargePopupMessage() throws RemoteException {
        return largepopupMessage;
    }
    
    public void setLargePopupSubMessage(String message) throws RemoteException {
        largepopupSubMessage = message;
    }
    
    public String getLargePopupSubMessage() throws RemoteException {
        return largepopupSubMessage;
    }
    
    public void setLargePopupMessage(String message) throws RemoteException {
        largepopupMessage = message;
    }

    public String getSubMessage() throws RemoteException {
        return subMessage;
    }
    public void setSubMessage(String message) throws RemoteException {
        subMessage = message;
    }

    public String[][] getButtonNameAction() throws RemoteException {
        return largeButtonInfo;
    }

    public void setButtonNameAction(String[][] action) throws RemoteException {
        largeButtonInfo = action;        
    }

    public boolean isCountDown() throws RemoteException {
        return countDown;
    }

    public void setCountDown(boolean b) throws RemoteException {
        countDown = b;
    }

}

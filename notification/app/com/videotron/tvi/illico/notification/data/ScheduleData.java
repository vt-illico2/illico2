package com.videotron.tvi.illico.notification.data;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Date;

import com.videotron.tvi.illico.log.Log;

public class ScheduleData implements Serializable {

    public static final long serialVersionUID = 1L;
    public final static long DEFAULT_TIME = DataManager.getInstance().getDisplayTime();

    private long createDate;
    private boolean viewed = false;
    private int displayCount = 0;
    private String applicationName;

    private String messageID;
    private String message;
    private String subMessage;
    private int categories;
    private int notificationType;
    private long displayTime;
    private int remainderTime;
    private long remainderDelay;
    private int notificationPopupType;
    private int notificationAction;
    // [0] = applcation name, [1~n] = data
    private String[] notificationActionData;

    private String largePopupMsg;
    private String largePopupSubMsg;

    private long countTime;
    private boolean countDown;

    //[0][0] button Name, [0][1] action Type, [0][2] application name or channel name,
    //[0][3] data.
    private String[][] largeButtonInfo;

    private int priority = 1;

    /**
     * Constructor.
     */
    public ScheduleData() {
    }

    public String toString() {
        return "ScheduleData[displayTime=" + new Date(displayTime) + ", priority=" + priority + ", applicationName=" + applicationName + "]";
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getCountTime() {
        return countTime;
    }

    public void setCountTime(long time) {
        this.countTime = time;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCategories() {
        return categories;
    }

    public void setCategories(int categories) {
        this.categories = categories;
    }

    public long getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(long displayTime) {
        this.displayTime = displayTime;
    }

    public int getRemainderTime() {
        return remainderTime;
    }

    public void setRemainderTime(int remainderTime) {
        this.remainderTime = remainderTime;
    }

    public long getRemainderDelay() {
        return remainderDelay;
    }

    public void setRemainderDelay(long remainderDelay) {
        Log.printDebug("getRemainderDelay =" + remainderDelay);
        this.remainderDelay = remainderDelay;
    }

    public int getNotificationPopupType() {
        return notificationPopupType;
    }

    public void setNotificationPopupType(int setNotificationPopupType) {
        this.notificationPopupType = setNotificationPopupType;
    }

    public int getNotificationAction() {
        return notificationAction;
    }

    public void setNotificationAction(int notificationAction) {
        this.notificationAction = notificationAction;
    }

    public String[] getNotificationActionData() {
        return notificationActionData;
    }

    public void setNotificationActionData(String[] notificationActionData) {
        this.notificationActionData = notificationActionData;
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean b){
        viewed = b;
    }

    public int getDisplayCount() {
        return displayCount;
    }

    public void setDisplayCount(int count){
        displayCount = count;
    }

    public String getSubMessage() {
        return subMessage;
    }

    public void setSubMessage(String subMessage) {
        this.subMessage = subMessage;
    }

    public String getLargePopupMessage() {
        return largePopupMsg;
    }

    public void setLargePopupMessage(String largePopupMsg) {
        this.largePopupMsg = largePopupMsg;
    }

    public String getLargePopupSubMessage() {
        return largePopupMsg;
    }

    public void setLargePopupSubMessage(String largePopupSubMsg) {
        this.largePopupSubMsg = largePopupSubMsg;
    }

    public String[][] getButtonNameAction() {
        return largeButtonInfo;
    }

    public void setButtonNameAction(String[][] action) {
        largeButtonInfo = action;
    }

    public boolean isCountDown() {
        return countDown;
    }

    public void setCountDown(boolean b) {
        countDown = b;
    }

    public void setPriority(int p){
        priority = p;
    }

    public int getPriority(){
        return priority;
    }
}

package com.videotron.tvi.illico.notification.data;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.log.Log;

public class FlashMemoryController {

    private static FlashMemoryController flashMemoryController = new FlashMemoryController();

    /** The repository root. */
    private static final String REPOSITORY_ROOT = System.getProperty("dvb.persistent.root");
    private static final String DIRECTORY = REPOSITORY_ROOT + "/notification/";
    public static final String FILE = DIRECTORY + "noti.dat";

    public static FlashMemoryController getInstance() {
        return flashMemoryController;
    }

    /**
     * It write a data in flash memory.
     * @param data data.
     * @param fileName path/fileName.
     */
    public synchronized void writeData(Object data) {
        Log.printInfo("WriteData fileName");

        if (data == null) {
            return;
        }
        Hashtable hash = (Hashtable) data;
        Enumeration elements = hash.elements();

        ScheduleData[] datas = new ScheduleData[hash.size()];

        int count = 0;
        while (elements.hasMoreElements()) {
            datas[count] = (ScheduleData) elements.nextElement();
            count++;
        }

        BufferedOutputStream bos = null;
        ObjectOutputStream out = null;
        File file = null;
        try {
            file = new File(DIRECTORY);
            if (!file.exists()) {
                file.mkdirs();
            }
            bos = new BufferedOutputStream(new FileOutputStream(FILE));
            out = new ObjectOutputStream(bos);
            out.writeObject(datas);
            Log.printInfo("WriteData End");
        } catch (FileNotFoundException e) {
            Log.print(e);          
        } catch (IOException e) {
            Log.print(e);          
        } finally {
            try {
                file = null;
                if(bos != null) {
                    bos.close();
                }
                
                if(out != null) {
                    out.close();
                }
            } catch (IOException e1) {
                Log.print(e1);
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Reads a data.
     * @param data
     * @return Object.
     */
    public synchronized Object readData() {
        File f = new File(FILE);
        if (!f.canRead()) {
            return null;
        }

        BufferedInputStream bis = null;
        ObjectInputStream out = null;
        Object ob = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(f));
            out = new ObjectInputStream(bis);
            ob = out.readObject();
        } catch (Exception e) {
            Log.print(e);            
        } finally {
            try {
                if(bis != null) {
                    bis.close();
                }
                
                if(out != null) {
                    out.close();
                }
            } catch (IOException e) {
                Log.print(e);
            } catch (Exception e) {
                Log.print(e);
            }
        }

        return ob;
    }
}

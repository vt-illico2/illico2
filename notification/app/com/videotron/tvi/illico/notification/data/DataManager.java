package com.videotron.tvi.illico.notification.data;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.framework.DataCenter;
//import com.videotron.tvi.illico.keyboard.Controller;
import com.videotron.tvi.illico.log.Log;

public class DataManager {

    /** instance of DataManager. */
    private static DataManager dataManager = new DataManager();

    /** IB data version. */
    private int version = 0;

    /** notification type.*/
    private int notificationType = 0;

    /** persistence tiem. */
    private int persistenceTime = 0;

    /** application list. */
    private Hashtable appList = new Hashtable();

    private String category;
    private int deleteTime;
    private String position;
    private long displayTime;

    /**
     * read and parser LoadingAnimation config file.
     * @param data is InbandData.
     */
    public void readTextFile(Object data) {
        Log.printInfo("readTextFile");
        if (data == null) {
            return;
        }

        byte[] textData = readFile(data);

        if (textData != null) {
            decodeConfigData(textData);
        }
    }

    public static void main(String[] ag){
        File f1 = new File("C:/notification.txt");
        DataManager.getInstance().readTextFile(f1);
    }

    /**
     * read config file from data.
     * @param data is InbandData
     * @return buf byte is read from inbandData
     */
    public synchronized byte[] readFile(Object data) {

        File file = (File) data;
        BufferedInputStream bis = null;

        byte[] buf = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            int c;
            while ((c = bis.read()) != -1) {
                baos.write(c);
            }
            buf = baos.toByteArray();

        } catch (Exception e) {
            Log.print(e);
        } finally {
            try {
                bis.close();
            } catch (IOException e) {
                Log.print(e);
            }
        }
        return buf;
    }

    /**
     * parser config file from byte data.
     * @param data is byte data of inbandData
     */
    public void decodeConfigData(byte[] data) {
        int index = -1;

        int newVersion = oneByteToInt(data, ++index);
        Log.printDebug("newVersion = " + newVersion);
        if(version == newVersion){
            return;
        } else {
            version = newVersion;
        }

        int notificationType = oneByteToInt(data, ++index);
        Log.printDebug("notificationType = " + notificationType);

        int persistenceTime = oneByteToInt(data, ++index);
        Log.printDebug("persistenceTime = " + persistenceTime);

        int appListSize = oneByteToInt(data, ++index);
        Log.printDebug("appListSize = " + appListSize);

        for (int a = 0; a < appListSize; a++) {
            int appNameLength = oneByteToInt(data, ++index);
            String appName = byteArrayToString(data, ++index, appNameLength);
            Log.printDebug(" appName[" + a + "] = " + appName);
            index += appNameLength - 1;
            if(!appList.containsKey(appName)){
                appList.put(appName, String.valueOf(0));
            }
        }
    }

    /**
     * convert byte to int.
     * @param data is all byte date.
     * @param offset is offset byte.
     * @return int read byte
     */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]));
    }

    /**
     * convert byte to String.
     * @param data is all byte date.
     * @param offset is offset byte.
     * @param length is read byte length.
     * @return result is String that converting byte
     */
    public static String byteArrayToString(byte[] data, int offset, int length) {
        String result = null;
        try {
            result = new String(data, offset, length);
        } catch (Exception uee) {
            Log.print(uee);
            return null;
        }
        return result;
    }

    /**
     * Get Application name list
     * @return appList
     */
    public Hashtable getAppList() {
        return appList;
    }

    /**
     * Gest DataManager.
     * @return dataManager
     */
    public static DataManager getInstance() {
        return dataManager;
    }

    /**
     * Get Notification Type.
     * @return notificationType
     */
    public int getNotificationType() {
        return notificationType;
    }

    /**
     * Get Persistence Time
     * @return persistenceTime
     */
    public int getPersistenceTime() {
        return persistenceTime;
    }
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(int deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public long getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(long displayTime) {
        this.displayTime = displayTime;
    }

    public void readProperty(){
        category = (String) DataCenter.getInstance().get("Category");
        deleteTime = Integer.parseInt((String)DataCenter.getInstance().get("DeleteTime"));
        position = (String) DataCenter.getInstance().get("Position");
        displayTime = Long.parseLong((String) DataCenter.getInstance().get("DisplayTime"));

        Log.printDebug("category = " + category);
        Log.printDebug("deleteTime = " + deleteTime);
        Log.printDebug("position = " + position);
        Log.printDebug("displayTime = " + displayTime);

        StringTokenizer strValue = new StringTokenizer(category, "|");
        while (strValue.hasMoreElements()) {
            String value = strValue.nextToken();

            int index = value.indexOf("=");
            String name = value.substring(0, index);
            String layer = value.substring(index + 1);
            Log.printDebug("name = " + name + " layer = " + layer);
            appList.put(name, layer);
        }
        setDisplayTime(displayTime);

    }
}

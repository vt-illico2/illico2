package com.videotron.tvi.illico.notification.data;

import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.notification.comp.popup.PopupController;
import com.videotron.tvi.illico.notification.controller.Controller;

public class NotificationSerivceImpl implements NotificationService {

    private int prioriy = 1;

    public NotificationOption getNotificationDetail(String id) throws RemoteException {

        ScheduleData o = (ScheduleData) Controller.getScheduleData().get(id);
        ReturnScheduleData item = null;
        if (o != null) {
            item = new ReturnScheduleData();
            item.setCreateDate(System.currentTimeMillis());
            item.setViewed(o.isViewed());
            item.setDisplayCount(o.getDisplayCount());
            item.setApplicationName(o.getApplicationName());
            item.setMessageID(o.getMessageID());
            item.setMessage(o.getMessage());
            item.setCategories(o.getCategories());
            item.setDisplayTime(o.getDisplayTime());
            item.setRemainderTime(o.getRemainderTime());
            item.setRemainderDelay(o.getRemainderDelay());
            item.setNotificationPopupType(o.getNotificationPopupType());
            item.setNotificationAction(o.getNotificationAction());
            item.setNotificationActionData(o.getNotificationActionData());
            item.setLargePopupMessage(o.getLargePopupMessage());
            item.setLargePopupSubMessage(o.getLargePopupSubMessage());
            item.setButtonNameAction(o.getButtonNameAction());
            item.setSubMessage(o.getSubMessage());
            item.setCountDown(o.isCountDown());
        }
        return item;
    }

    public String[] setRegisterNotificaiton(String appName, NotificationListener l) throws RemoteException {
        Log.printDebug("appName = " + appName);
        Log.printDebug("SetNotify NotificationListener = " + l);

        if (l != null) {
            Controller.getInstance().getListenerHashtable().put(appName, l);
        }

        String[] names = null;
        Vector idList = new Vector();
        Enumeration en = Controller.getScheduleData().keys();

        while (en != null && en.hasMoreElements()) {
            String keyName = (String) en.nextElement();
            int index = keyName.indexOf("|");
            String name = keyName.substring(0, index);
            if (appName.equals(name)) {
                idList.add(keyName);
                Log.printDebug("keyName = " + name);
            }
        }

        if (idList.size() > 0) {
            names = new String[idList.size()];
            for (int a = 0; a < idList.size(); a++) {
                names[a] = (String) idList.elementAt(a);
            }
        }
        return names;
    }

    public void removeNotify(String messageID) throws RemoteException {
        Log.printInfo("removeNotify = " + messageID);
        if (messageID == null) {
            return;
        }
        ScheduleData item = (ScheduleData) Controller.getScheduleData().remove(messageID);
        if (item == null) {
            return;
        }

        PopupController.getInstance().removePopup(item, false);

        Thread th = new Thread("flashWriteThread") {
            public void run() {
                FlashMemoryController.getInstance().writeData(Controller.getScheduleData());
            }
        };
        th.start();
    }

    public String setNotify(NotificationOption o) throws RemoteException {
        return setNotifyAndTime(o, ScheduleData.DEFAULT_TIME);
    }

    public long getDisplayTime() {
        return DataManager.getInstance().getDisplayTime();
    }

    public String setNotifyAndTime(NotificationOption o, long time) throws RemoteException {
        Log.printInfo("setNotify = " + o);
        String key = null;
        int id = 0;
        synchronized (Controller.getScheduleData()) {
            try {
                String appName = o.getApplicationName();
                String messageID = o.getMessageID();
                ScheduleData item = null;
                Log.printDebug("appName = " + appName);
                Log.printDebug("messageID = " + messageID);

                if (appName != null && messageID != null) {
                    Hashtable hAppList = DataManager.getInstance().getAppList();
                    if (!hAppList.containsKey(appName)) {
                        Log.printDebug(appName + " don't save in hAppList");
                        return null;
                    }
                    item = (ScheduleData) Controller.getScheduleData().get(messageID);
                    key = messageID;
                    
                    Controller.getScheduleData().put(key, item);
                    PopupController.getInstance().createPopup(item);
                } else {
                    item = new ScheduleData();
                    key = Controller.getInstance().createID(appName, id);

                    item.setCreateDate(System.currentTimeMillis());
                    item.setViewed(false);
                    // first;
                    item.setDisplayCount(0);
                    item.setApplicationName(o.getApplicationName());
                    item.setMessageID(key);
                    item.setMessage(o.getMessage());
                    item.setCategories(o.getCategories());
                    item.setDisplayTime(o.getDisplayTime());
                    item.setRemainderTime(o.getRemainderTime());
                    item.setRemainderDelay(o.getRemainderDelay());
                    item.setNotificationPopupType(o.getNotificationPopupType());
                    item.setNotificationAction(o.getNotificationAction());
                    item.setNotificationActionData(o.getNotificationActionData());
                    item.setLargePopupMessage(o.getLargePopupMessage());
                    item.setLargePopupSubMessage(o.getLargePopupSubMessage());
                    item.setButtonNameAction(o.getButtonNameAction());
                    item.setSubMessage(o.getSubMessage());
                    item.setCountDown(o.isCountDown());
                    item.setCountTime(time);
                    item.setPriority(prioriy);

                    if (Log.DEBUG_ON) {
                        Log.printDebug(item);
                        Log.printDebug("prioriy = " + prioriy);
                    }

                    // remove a saved sleep timer noti
                    if (o.getNotificationPopupType() == NotificationOption.SLEEP_TIMER_REMAINER_POPUP) {
                    	Hashtable scheduleData = Controller.getScheduleData();
                    	
                    	if (scheduleData != null && scheduleData.size() > 0) {
	                    	Enumeration keys = scheduleData.keys();

	                    	while (keys.hasMoreElements()) {
	                            Object keyValue = keys.nextElement();
	                            ScheduleData sd = (ScheduleData) scheduleData.get(keyValue);
	                            if (sd.getNotificationPopupType() == NotificationOption.SLEEP_TIMER_REMAINER_POPUP) {
	                            	PopupController.getInstance().removePopup(sd, false);
	                            	scheduleData.remove(keyValue);
	                            }
	                        }
                    	}
                    }
                    
                    Controller.getScheduleData().put(key, item);
                    PopupController.getInstance().createPopup(item);
                    
                    Thread th = new Thread("flashWriteThread") {
                        public void run() {
                            FlashMemoryController.getInstance().writeData(Controller.getScheduleData());
                        }
                    };
                    th.start();
                }
               
            } catch (Exception e) {
                Controller.getInstance().showErrorMessage("NOT500");
            }
        }
        return key;
    }

    public void setPriority(int id) throws RemoteException {
        prioriy = id;
    }
}

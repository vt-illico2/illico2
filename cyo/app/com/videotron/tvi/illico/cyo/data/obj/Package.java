/**
 * @(#)Package.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

import java.util.Vector;

/**
 * A package can include several subscription items.
 * @author Woojung Kim
 * @version 1.1
 */
public class Package extends Subscription {

    private Channel[] channel;
    private Game[] game;
    
    /** boolean . */
    private String hd;
    
    /** single / multiple. */
    private String selection;
    
    private String url;

    /** number of channels that can be chosen in the package */
    private String numberOfChannels;

    private Vector packageCatalog;
    private PackageCatalog[] packageCatalogArr;
    private ExtraList extraList;
    private BlockList blockList;

    private int numberOfPremiumCatalogs;
    private int numberOfPremiumChannels;
    private int numberOfNormalChannels;

    public Channel[] getChannel() {
        return channel;
    }

    public void setChannel(CyoObject[] channel) {
        if (channel == null) {
            return;
        }
        this.channel = new Channel[channel.length];

        for (int i = 0; i < channel.length; i++) {
            this.channel[i] = (Channel) channel[i];
        }
    }

    public Game[] getGame() {
        return game;
    }

    public void setGame(CyoObject[] game) {
        if (game == null) {
            return;
        }
        this.game = new Game[game.length];

        for (int i = 0; i < game.length; i++) {
            this.game[i] = (Game) game[i];
        }
    }

    public String getHd() {
        return hd;
    }

    public void setHd(String hd) {
        this.hd = hd;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNumberOfChannels() {
        return numberOfChannels;
    }

    public void setNumberOfChannels(String noc) {
        this.numberOfChannels = noc;
    }

    public PackageCatalog[] getPackageCatalogs() {
        return packageCatalogArr;
    }

    public ExtraList getExtraList() {
        return extraList;
    }

    public BlockList getBlockList() {
        return blockList;
    }

    public boolean hasPremium() {
        if (packageCatalogArr != null) {
            for (int i = 0 ; i < packageCatalogArr.length; i++) {
                if (packageCatalogArr[i].isPremium()) {
                    return true;
                }
            }
        }

        return false;
    }

    public int getNumberOfPremiumCatalogs() {
        return numberOfPremiumCatalogs;
    }

    public int getNumberOfPremiumChannels() {
        return numberOfPremiumChannels;
    }

    public int getNumberOfNormalChannels() {
        return numberOfNormalChannels;
    }

    public void setCyoObject(CyoObject[] objects) {
        super.setCyoObject(objects);

        for (int i = 0 ; i < objects.length; i++) {
            if (objects[i] instanceof PackageCatalog) {
                if (packageCatalog == null) {
                    packageCatalog = new Vector();
                }
                packageCatalog.add(objects[i]);
            } else if (objects[i] instanceof ExtraList) {
                extraList = (ExtraList) objects[i];
            } else if (objects[i] instanceof BlockList) {
                blockList = (BlockList) objects[i];
            }
        }

        if (packageCatalog != null) {
            packageCatalogArr = new PackageCatalog[packageCatalog.size()];
            packageCatalog.copyInto(packageCatalogArr);
        }
    }

    public void rearrangePackageCatalog() {
//        int premiumIdx = -1;
//
//        for (int i = 0 ; i < packageCatalogArr.length; i++) {
//            if (packageCatalogArr[i].isPremium()) {
//                premiumIdx = i;
//                break;
//            }
//        }
//
//        // if premiumIdx == 0, don't need rearrange.
//        if (premiumIdx > 0) {
//            PackageCatalog temp = packageCatalogArr[0];
//            packageCatalogArr[0] = packageCatalogArr[premiumIdx];
//            packageCatalogArr[premiumIdx] = temp;
//        }

        if (packageCatalogArr != null) {

            packageCatalog.clear();
            numberOfPremiumCatalogs = 0;
            numberOfPremiumChannels = 0;
            numberOfNormalChannels = 0;

            for (int i = 0; i < packageCatalogArr.length; i++) {
                if (packageCatalogArr[i].isPremium()) {
                    packageCatalog.add(0, packageCatalogArr[i]);
                    numberOfPremiumCatalogs++;
                    numberOfPremiumChannels += packageCatalogArr[i].getNumberOfChannelForInt();
                } else {
                    packageCatalog.add(packageCatalogArr[i]);
                    numberOfNormalChannels += packageCatalogArr[i].getNumberOfChannelForInt();
                }
            }

            PackageCatalog[] orderedPC = new PackageCatalog[packageCatalog.size()];
            packageCatalog.copyInto(orderedPC);

            packageCatalogArr = orderedPC;
        }
    }

    public PackageCatalog getNonPremiumPacakgeCatalog() {

        for (int i = 0; i < packageCatalogArr.length; i++) {
            if (!packageCatalogArr[i].isPremium()) {
                return packageCatalogArr[i];
            }
        }

        return null;
    }
}

/**
 * @(#)Fee.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * Type of subscription specifically used for fees and rebates. Fees extends Service rather than Subscription since
 * additional fields from subscription are not needed.
 * @author Woojung Kim
 * @version 1.1
 */
public class Fee extends Service {

    
}

/**
 * @(#)CyoXMLHandler.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data;

import java.util.Stack;
import java.util.Vector;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.log.Log;

/**
 * Handle a XML. Refer a CYO interface document.
 * @author Woojung Kim
 * @version 1.1
 */
public class CyoXMLHandler extends DefaultHandler {

    /** a Stack to get a parent object. */
    private Stack stackObj = new Stack();
    /** a Stack to get a Vector to include a child object. */
    private Stack stackVector = new Stack();

    /** a parent CyoObject. */
    private CyoObject parentObj;
    /** a Vector of current CyoObject. */
    private Vector vector;

    /** a type of current URL. */
    private int type;

    /**
     * Constructor.
     * @param type a type of URL.
     */
    public CyoXMLHandler(int type) {
        super();
        this.type = type;
    }

    /**
     * Report a fatal XML parsing error.
     * @param e The error information encoded as an exception.
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    /**
     * Receive notification of a recoverable parser error.
     * @param e The warning information encoded as an exception.
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    /**
     * Receive notification of a parser warning.
     * @param e The warning information encoded as an exception
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void warning(SAXParseException e) throws SAXException {
    }

    /**
     * Ignorable whitespace event.
     * @param buf An array of characters
     * @param offset The starting position in the array
     * @param len The number of characters to use from the character
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void ignorableWhitespace(char[] buf, int offset, int len) throws SAXException {
    }

    /**
     * Receive notification of a processing instruction.
     * @param target The processing instruction target.
     * @param data The processing instruction data, or null if none is supplied
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void processingInstruction(String target, String data) throws SAXException {
    }

    /**
     * Receive notification of a skipped entity.
     * @param name The name of the skipped entity.
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void skippedEntity(String name) throws SAXException {
    }

    /**
     * Receive Receive notification of the beginning of the document.
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void startDocument() throws SAXException {
        if (Log.ALL_ON) {
            Log.printInfo("CyoXMLHandler: startDocument()");
        }
    }

    /**
     * Receive notification of the end of the document.
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void endDocument() throws SAXException {
        if (Log.ALL_ON) {
            Log.printInfo("CyoXMLHandler: endDocument()");
        }

        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: stackObj = " + stackObj);
        // }
        //
        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: parent = " + parentObj);
        // }
        // DataCenter.getInstance().put(Integer.toString(type), parent);
        CyoDataManager.getInstance().updateCyoDataListener(type, CyoDataListener.SUCCESS, parentObj);
    }

    /**
     * Receive notification of the start of an element.
     * @param uri URI
     * @param localName The local name (without prefix)
     * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
     * @param attributes The specified or defaulted attributes
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (Log.ALL_ON) {
            Log.printInfo("CyoXMLHandler: startElement()");
        }
        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: uri = " + uri);
        // Log.printDebug("CyoXMLHandler: localName = " + localName);
        // Log.printDebug("CyoXMLHandler: qName = [" + qName + "]");
        // }

        // if (curTag != null) {
        // if (!curTag.equals(qName)) {
        //
        // curTag = qName;
        //
        // preIndentLevel = indentLevel;
        // indentLevel++;
        //
        // stackVector.push(vector);
        // vector = new Vector();
        //
        // stackObj.push(obj);
        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: stackObj = " + stackObj);
        // }
        // } else {
        // preIndentLevel = indentLevel;
        // indentLevel++;
        // }
        // } else {
        // curTag = qName;
        // }
        CyoObject obj;
        if (qName.equals("shortcuts")) {
            obj = new Shortcuts();
        } else if (qName.equals("group")) {
            obj = new Group();
        } else if (qName.equals("shortcut")) {
            obj = new Shortcut();
        } else if (qName.equals("messages")) {
            obj = new Messages();
        } else if (qName.equals("message")) {
            obj = new Message();
        } else if (qName.equals("channel")) {
            obj = new Channel();
        } else if (qName.equals("service")) {
            obj = new Service();
        } else if (qName.equals("fee")) {
            obj = new Fee();
        } else if (qName.equals("game")) {
            obj = new Game();
        } else if (qName.equals("hardware")) {
            obj = new Hardware();
        } else if (qName.equals("package")) {
            obj = new Package();
        } else if (qName.equals("product")) {
            obj = new Product();
        } else if (qName.equals("services")) {
            obj = new Services();
        } else if (qName.equals("Subscription")) {
            obj = new Subscription();
        } else if (qName.equals("channel-list")) {
            obj = new ChannelList();
        } else if(qName.equals("confirmation")) {
            obj = new Message();
        } else if (qName.equals("block-list")) {
            obj = new BlockList();
        } else if (qName.equals("catalog")) {
            obj = new Catalog();
        } else if (qName.equals("catalog-list")) {
            obj = new CatalogList();
        } else if (qName.equals("extra-list")) {
            obj = new ExtraList();
        } else if (qName.equals("package-catalog")) {
            obj = new PackageCatalog();
        } else if (qName.equals("package-list")) {
            obj = new PackageList();
        } else {
            throw new SAXException("unknown object.");
        }

        if (obj != null) {
            int num = attributes.getLength();
            if (Log.ALL_ON) {
                Log.printDebug("    Length  : " + num);
            }

            if (num > 0)
                setAttributes(obj, attributes);
        }

        vector = new Vector();
        stackVector.push(vector);

        stackObj.push(obj);
        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: stackObj = " + stackObj);
        // Log.printDebug("CyoXMLHandler: stackObj = " + stackVector);
        // }
    }

    /**
     * Receive notification of the end of an element.
     * @param uri uri
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed
     * @param qName The qualified XML 1.0 name (with prefix), or the empty string if qualified names are not available.
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (Log.ALL_ON) {
            Log.printInfo("CyoXMLHandler: endElement()");
        }
        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: uri = " + uri);
        // Log.printDebug("CyoXMLHandler: localName = " + localName);
        // Log.printDebug("CyoXMLHandler: qName = [" + qName + "]");
        // }
        //
        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: stackObj = " + stackObj);
        // Log.printDebug("CyoXMLHandler: stackVector = " + stackVector);
        // }

        CyoObject currentObj = (CyoObject) stackObj.pop();
        vector = (Vector) stackVector.pop();

        if (vector.size() > 0) {
            CyoObject[] objs = getObjects(vector);
            currentObj.setCyoObject(objs);
        }

        if (stackVector.size() > 0) {
            Vector pVector = (Vector) stackVector.peek();
            CyoObject currentParentObj = (CyoObject) stackObj.peek();
            currentObj.setParentObj(currentParentObj);
            pVector.add(currentObj);
        } else {
            parentObj = currentObj;
        }

        // if (!qName.equals(curTag)) {
        // if (Log.DEBUG_ON) {621384
        // Log.printDebug("CyoXMLHandler: preIndentLevel = " + preIndentLevel);
        // Log.printDebug("CyoXMLHandler: indentLevel = " + indentLevel);
        // }
        // if (preIndentLevel > indentLevel) {
        // if (stackObj.size() > 0) {
        // if (Log.DEBUG_ON) {
        // Log.printDebug("CyoXMLHandler: stackObj = " + stackObj);
        // }
        // parent = (CyoObject) stackObj.pop();
        //
        // if (Log.DEBUG_ON) {
        // Log.printDebug("obj : " + parent);
        // Log.printDebug("vector : " + vector);
        // }
        //
        // if (vector.size() > 0) {
        // CyoObject[] objs = getObjects(vector);
        // String className = objs[0].getClass().getName();
        // int lastDot = className.lastIndexOf('.');
        // className = className.substring(lastDot + 1);
        //
        // // parent.setMethodName(changeMethodName(className), true);
        // // parent.setValue(objs);
        // parent.setCyoObject(objs);
        //
        // // resource clear
        // vector.clear();
        // vector = null;
        // }
        // curTag = qName;
        // vector = (Vector) stackVector.pop();
        // vector.add(parent);
        // if (Log.DEBUG_ON) {
        // Log.printDebug("pop vector size : " + vector.size());
        // }
        // preIndentLevel = indentLevel;
        // indentLevel--;
        //
        // }
        // } else {
        // indentLevel--;
        // }
        //
        // } else {
        // vector.add(obj);
        // indentLevel--;
        // }
    }

    /**
     * Receive notification of the start of a Namespace mapping.
     * @param prefix The Namespace prefix being declared.
     * @param uri The Namespace URI mapped to the prefix.
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
    }

    /**
     * Receive notification of the end of a Namespace mapping.
     * @param prefix - The Namespace prefix being declared
     * @see org.xml.sax.helpers.DefaultHandler#endPrefixMapping(java.lang.String)
     * @throws SAXException Any SAX exception, possibly wrapping another
     */
    public void endPrefixMapping(String prefix) throws SAXException {
    }

    /**
     * Adapt a SAX1 characters event.
     * @param ch An array of characters.
     * @param start The starting position in the array.
     * @param length The number of characters to use.
     * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
     * @throws SAXException The client may raise a processing exception.
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
        String s = new String(ch, start, length);
        try {
            s = new String(s.getBytes(), "UTF-8");
        } catch (Exception e) {
            Log.printError(e);
        }

        s = s.trim();
    }

    /**
     * Get a CyoObject from Vector.
     * @param vec a Vector to have a CyoObject's
     * @return a CyoObject array to be got in Vector.
     */
    private CyoObject[] getObjects(Vector vec) {
        CyoObject[] obj = new CyoObject[vec.size()];
        for (int i = 0; i < obj.length; i++) {
            obj[i] = (CyoObject) vec.elementAt(i);
        }
        return obj;
    }

    /**
     * Change a attribute name to method name.
     * @param methodName a method name to want.
     * @return a method name to be added a prefix ('set').
     */
    private String changeMethodName(String methodName) {
        StringBuffer sf = new StringBuffer();
        String prefix = methodName.toUpperCase().substring(0, 1);
        sf.append("set");
        sf.append(prefix);
        sf.append(methodName.substring(1));

        return sf.toString();
    }

    /**
     * 해당 하는 객체에 Attributes의 값들을 set 한다. Set a value of attribute in CyoObject.
     * @param obj a current object.
     * @param attr a attribute of current tag.
     */
    private void setAttributes(CyoObject obj, Attributes attr) {
        int num = attr.getLength();
        for (int i = 0; i < num; i++) {
            // if (Log.DEBUG_ON) {
            // Log.printDebug("    Name : " + attr.getLocalName(i));
            // Log.printDebug("    Value : " + attr.getValue(i));
            // }

            obj.setMethodName(changeMethodName(attr.getLocalName(i)), false);
            obj.setValue(attr.getValue(i));
        }

    }
}

package com.videotron.tvi.illico.cyo.data;

/**
 * This class define a values of parameter.
 * @author Woojung Kim
 * @version 1.1
 */
public final class Param {

    /**
     * Ordered alphabetically with the channel full name.
     */
    public static final String SORT_ALPHABETICAL = "1";

    /**
     * Ordered by channel position.
     */
    public static final String SORT_NUMBER = "2";

    /** Constructor for Util class. */
    private Param() {
    }
}

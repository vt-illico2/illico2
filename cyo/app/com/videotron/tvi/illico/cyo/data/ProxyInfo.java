/**
 * @(#)ProxyInfo.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data;

/**
 * This class have a information for Proxy IP and Port.
 * @author Woojung Kim
 * @version 1.1
 */
public class ProxyInfo {
    /** a IP of proxy. */
    private String ip;
    /** a port of proxy. */
    private int port;

    /**
     * Get a IP.
     * @return a String of IP.
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set a IP of proxy.
     * @param ip a String to have a information of IP.
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Get a port of proxy.
     * @return a number of port.
     */
    public int getPort() {
        return port;
    }

    /**
     * Set a Port.
     * @param port a number of port.
     */
    public void setPort(int port) {
        this.port = port;
    }
}

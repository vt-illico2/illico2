/**
 * @(#)Hardware.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * Type of subscription specifically used for hardware. Hardware extends Service rather than subscription since
 * additional fields from Subscription are not needed.
 * @author Woojung Kim
 * @version 1.1
 */
public class Hardware extends Service {

}

package com.videotron.tvi.illico.cyo.data.obj;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-07-13.
 */
public class PackageCatalog extends CyoObject {

	private String catalogId;
	private String numberOfChannels;
	private boolean isPremium;
	private Catalog catalog;

	public void setCatalogId(String id) {
		this.catalogId = id;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setNumberOfChannels(String number) {
		this.numberOfChannels = number;
	}

	public String getNumberOfChannels() {
		return numberOfChannels;
	}

	public int getNumberOfChannelForInt() {
		return Integer.valueOf(numberOfChannels).intValue();
	}

	public void setPremium(boolean premium) {
		this.isPremium = premium;
	}

	public boolean isPremium() {
		return this.isPremium;
	}

	public void setCatalog(Catalog c) {
		this.catalog = c;
	}

	public Catalog getCatalog() {
		return catalog;
	}
}

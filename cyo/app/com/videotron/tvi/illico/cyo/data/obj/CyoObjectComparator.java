package com.videotron.tvi.illico.cyo.data.obj;

import java.util.Comparator;

public class CyoObjectComparator implements Comparator {
    public static CyoObjectComparator comparator = new CyoObjectComparator();

    public int compare(Object o1, Object o2) {
        int num1, num2;

        if (o1 instanceof Service && o2 instanceof Service) {
            Service s1 = (Service) o1;
            Service s2 = (Service) o2;

            if (s1.getSequenceNo() != null && s2.getSequenceNo() != null) {
                num1 = Integer.parseInt(s1.getSequenceNo());
                num2 = Integer.parseInt(s2.getSequenceNo());
                if (num1 < num2)
                    return -1;
                if (num1 > num2)
                    return 1;
            }
        }
        
        return 0;
    }
    
    
}

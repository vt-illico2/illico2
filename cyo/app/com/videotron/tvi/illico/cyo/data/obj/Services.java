/**
 * @(#)Services.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class Services extends CyoObject {

    /** Total price to be billed on a monthly basis.*/
    private String pricetotalM;
    /** Total price to be billed on a yearly basis. */
    private String pricetotalY;
    /** Total price to be billed only once. */
    private String pricetotalO;
    
    private String lpifpricetotalM;
    
    public String getLpifpricetotalM() {
        return lpifpricetotalM;
    }
    public void setLpifpricetotalM(String lpifpricetotalM) {
        this.lpifpricetotalM = lpifpricetotalM;
    }
    public String getPricetotalM() {
        return pricetotalM;
    }
    public void setPricetotalM(String pricetotalM) {
        this.pricetotalM = pricetotalM;
    }
    public String getPricetotalY() {
        return pricetotalY;
    }
    public void setPricetotalY(String pricetotalY) {
        this.pricetotalY = pricetotalY;
    }
    public String getPricetotalO() {
        return pricetotalO;
    }
    public void setPricetotalO(String pricetotalO) {
        this.pricetotalO = pricetotalO;
    }
    
    
}

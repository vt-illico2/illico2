/**
 * @(#)Group.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class Group extends CyoObject {

    /**
     * single "Single" should be graphically representing with radio buttons. multiple "Multiple" should be graphically
     * representing with checkboxes.
     */
    protected String selection;

    protected String title;

    /**
     * If a URL is specified, this URL will be use to execute a command to the backend when the group is activated.
     */
    protected String url;

    protected String no;

    protected String pricetotal;
    /** m/y/o */
    protected String pricefreq;
    /** boolean */
    protected String modifiable;

    private String lpifpricetotal;

    private String desc;
    
    public String getLpifpricetotal() {
        return lpifpricetotal;
    }
    public void setLpifpricetotal(String lpifpricetotal) {
        this.lpifpricetotal = lpifpricetotal;
    }
    
    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getPricetotal() {
    	if (lpifpricetotal != null) {
    		return lpifpricetotal;
    	} else {
    		return pricetotal;
    	}
        
    }

    public void setPricetotal(String pricetotal) {
        this.pricetotal = pricetotal;
    }

    public String getPricefreq() {
        return pricefreq;
    }

    public void setPricefreq(String pricefreq) {
        this.pricefreq = pricefreq;
    }

    public String getModifiable() {
        return modifiable;
    }

    public void setModifiable(String modifiable) {
        this.modifiable = modifiable;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}

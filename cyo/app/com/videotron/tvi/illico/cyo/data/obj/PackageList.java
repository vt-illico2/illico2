package com.videotron.tvi.illico.cyo.data.obj;

import java.util.Vector;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-07-13.
 */
public class PackageList extends CyoObject {

	private Vector packageVec = new Vector();
	private Package[] packageArr;
	private BlockList blockList;
	private CatalogList catalogList;

	public Package[] getPackages() {
		return packageArr;
	}

	public BlockList getBlockList() {
		return blockList;
	}

	public CatalogList getCatalogList() {
		return catalogList;
	}

	public void setCyoObject(CyoObject[] objects) {
		super.setCyoObject(objects);

		for (int i = 0; i < objects.length; i++) {
			if (objects[i] instanceof BlockList) {
				blockList = (BlockList) objects[i];
			} else if (objects[i] instanceof CatalogList) {
				catalogList = (CatalogList) objects[i];
			} else if (objects[i] instanceof Package) {
				packageVec.add(objects[i]);
			}
		}

		packageArr = new Package[packageVec.size()];
		packageVec.copyInto(packageArr);

		// check premium catalog
		CyoObject[] catalogs = catalogList.getCyoObject();
		for (int i = 0; i < packageArr.length; i++) {
			Package p = packageArr[i];
			PackageCatalog[] pCatalogs = p.getPackageCatalogs();

			if (pCatalogs != null) {
				for (int pcIdx = 0; pcIdx < pCatalogs.length; pcIdx++) {

					for (int cIdx = 0; cIdx < catalogs.length; cIdx++) {
						Catalog c = (Catalog) catalogs[cIdx];

						if (c.getCatalogId().equals(pCatalogs[pcIdx].getCatalogId())) {
							pCatalogs[pcIdx].setCatalog(c);
							if (c.isPremium()) {
								pCatalogs[pcIdx].setPremium(true);
							}
						}
					}
				}
			}

			p.rearrangePackageCatalog();
		}
	}
}

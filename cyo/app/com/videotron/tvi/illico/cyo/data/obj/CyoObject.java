/**
 * @(#)CyoObject.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.videotron.tvi.illico.log.Log;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class CyoObject {

    Method method = null;
    public String methodName = "";
    protected String sessionId;
    protected String success;
    protected Message[] message;

    protected CyoObject[] objs;

    protected CyoObject parentObj;

    public void setMethodName(String method, boolean isArray) {
        methodName = method;

        try {
            if (isArray) {
                this.getClass().getMethod(methodName, new Class[] {CyoObject[].class});
            } else {
                this.getClass().getMethod(methodName, new Class[] {String.class});
            }
        } catch (SecurityException e) {
            methodName = null;
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            methodName = null;
            if (Log.DEBUG_ON) {
                Log.printDebug("setMethodName : not Method - " + method);
            }
        }
    }

    public void setValue(String value) {

        if (methodName == null || value == null || value.trim().length() == 0)
            return;

        try {
            method = this.getClass().getMethod(methodName, new Class[] {String.class});
        } catch (SecurityException e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("SecurityException : Method - " + method);
            }
            return;
        } catch (NoSuchMethodException e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("NoSuchMethodException : Method - " + method);
            }
            return;
        }

        try {
            method.invoke(this, new Object[] {value});
        } catch (IllegalArgumentException e1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("IllegalArgumentException : invoke - " + method);
            }
            return;
        } catch (IllegalAccessException e1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("IllegalAccessException : invoke - " + method);
            }
            return;
        } catch (InvocationTargetException e1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("InvocationTargetException : invoke - " + method);
            }
            return;
        }
        if (Log.ALL_ON) {
            Log.printDebug("setValue :  " + methodName + " = " + value);
        }
    }

    public void setValue(CyoObject[] objs) {

        if (methodName == null || objs == null || objs.length == 0)
            return;

        try {
            method = this.getClass().getMethod(methodName, new Class[] {CyoObject[].class});
        } catch (SecurityException e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("SecurityException : Method - " + method);
            }
            return;
        } catch (NoSuchMethodException e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("NoSuchMethodException : Method - " + method);
            }
            return;
        }

        try {
            method.invoke(this, new Object[] {objs});
        } catch (IllegalArgumentException e1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("IllegalArgumentException : invoke - " + method);
            }
            return;
        } catch (IllegalAccessException e1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("IllegalAccessException : invoke - " + method);
            }
            return;
        } catch (InvocationTargetException e1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("InvocationTargetException : invoke - " + method);
            }
            e1.getCause().printStackTrace();
            return;
        }
        if (Log.ALL_ON) {
            Log.printDebug("setValue :  " + methodName + " = " + objs);
        }
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Message[] getMessage() {
        return message;
    }

    public void setMessage(CyoObject[] message) {
        if (message == null) {
            return;
        }
        this.message = new Message[message.length];

        for (int i = 0; i < message.length; i++) {
            this.message[i] = (Message) message[i];
        }
    }

    public CyoObject[] getCyoObject() {
        return objs;
    }

    public void setCyoObject(CyoObject[] objects) {
        this.objs = objects;
    }

    public CyoObject getParentObj() {
        return parentObj;
    }

    public void setParentObj(CyoObject parentObj) {
        this.parentObj = parentObj;
    }
}

/**
 * @(#)Message.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class Message extends CyoObject {
    /** Value of '0' is for information message. */
    public static final String TYPE_INFORMATION = "0";
    /** Value of '1' is for warning. Warning may be displayed differently than information. */
    public static final String TYPE_WARNING = "1";
    /** Value of '2' is for screen error. Screen errors will prevent the user from navigation outside of the screen. */
    public static final String TYPE_SCREEN_ERROR = "2";
    /**
     * Value of '3' is for Validation error. Validation errors will allow the user to navigate to any screen but
     * indicates that the validation is rejected.
     */
    public static final String TYPE_VALIDATION_ERROR = "3";
    /**
     * Value of '4' is for System error. System errors represent errors compromising its behavior. The user should be
     * warned to try again later.
     */
    public static final String TYPE_SYSTEM_ERROR = "4";

    public static final String OPTIMIZATION_TYPE_PACKAGE = "0";
    public static final String OPTIMIZATION_TYPE_BLOCK = "1";
    public static final String OPTIMIZATION_TYPE_A_LA_CARTE = "2";

    private String id;
    private String type;
    private String text;
    
    // R5-SM5
    private String title;

    // Bundling
	/**
     * Specifies the subscription identifying number of the proposed package, block or channel.
     */
    private String optimizationCode;

	/**
     * Used to specify the type of optimization.
     *
     * Value of '0' is for package optimization.
     * Value of '1' is for block optimization.
     * Value of '2' is for 'à la carte' optimization.
     */
    private String optimizationType;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    
    // R5-SM5
    public void setTitle(String title) {
    	this.title = title;
    }
    // R5-SM5
    public String getTitle() {
    	return title;
    }

    // Bundling
    public void setOptimizationCode(String optimizationCode) {
        this.optimizationCode = optimizationCode;
    }

    public String getOptimizationCode() {
        return optimizationCode;
    }

    public void setOptimizationType(String optimizationType) {
        this.optimizationType = optimizationType;
    }

    public String getOptimizationType() {
        return optimizationType;
    }

}

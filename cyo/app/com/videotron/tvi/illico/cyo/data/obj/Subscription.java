/**
 * @(#)Subscription.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class Subscription extends Service {
	
	public static final String ILLICO_0 = "0";
	public static final String ILLICO_1 = "1";
	public static final String ILLICO_2 = "2";
	

    protected String no;
    /**
     * The value '1' is used for illico1 and '2' for illico2. The value 0 is used when the element applies to both
     * version.
     */
    protected String illico;
    protected String desc;

    /**
     * boolean in interface Doc but in real data, it is '0' or '1'.<br>
     * Flag indicating if the subscription value can be modified by the user
     */
    protected String modifiable;
    /**
     * Flag indicating the reason why the service is modifiable or not. The value 0 is used for a modifiable service The
     * value 1 is used for a non modifiable service - No reason provided The value 2 is used for a non modifiable
     * service - Service is included in another service The value 3 is used for a non modifiable service - Customer has
     * been subscribed to account for less than 30 days The value 4 is used for a non modifiable service - Customer has
     * been subscribed to account for less than 60 days
     */
    protected String modifiableStatus;
    /**
     * Boolean value indicating if the user is subscribed to the service. This value represent the user's current
     * subscription defined in Videotron's backend. True' means the user is subscribed to the service.
     */
    protected String subscribedInAccount;
    /**
     * boolean True' means the user selected this services in his ongoing modifications to his subscription. The delta
     * between subscribedInAccount and subscribedInSession should be used to distinguish the active subscription and the
     * ongoing modifications.
     */
    protected String subscribedInSession;
    
    protected String subscribedOrginalInSession;

    /**
     * List of categories to which the group is associated. The format is CSV (Comma-Separated Values).
     */
    protected String categories;

    protected String date;

    // Bundling
    /**
     * The type of this subscription
     *
     * Value of '0' is for subscription from base.
     * Value of '1' is for subscription from offer package.
     * Value of '2' is for subscription form 'à la carte'.
     */
    private String subscriptionType;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getIllico() {
        return illico;
    }

    public void setIllico(String illico) {
        this.illico = illico;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getModifiable() {
        return modifiable;
    }

    public void setModifiable(String modifiable) {
        this.modifiable = modifiable;
    }

    public String getModifiableStatus() {
        return modifiableStatus;
    }

    public void setModifiableStatus(String modifiableStatus) {
        this.modifiableStatus = modifiableStatus;
    }

    public boolean getSubscribedInAccount() {
        return Boolean.valueOf(subscribedInAccount).booleanValue();
    }

    public void setSubscribedInAccount(String subscribedInAccount) {
        this.subscribedInAccount = subscribedInAccount;
    }

    public void setSubscribedInAccount(boolean subscribedInAccount) {
        this.subscribedInAccount = Boolean.toString(subscribedInAccount);
    }
    
    public boolean getSubscribedOrginalInSession() {
        return Boolean.valueOf(subscribedOrginalInSession).booleanValue();
    }

    public void setSubscribedOrginalInSession(String subscribedOrginalInSession) {
        this.subscribedOrginalInSession = subscribedOrginalInSession;
    }
    
    public boolean getSubscribedInSession() {
        return Boolean.valueOf(subscribedInSession).booleanValue();
    }

    public void setSubscribedInSession(String subscribedInSession) {
        this.subscribedInSession = subscribedInSession;
    }

    public void setSubscribedInSession(boolean subscribedInSession) {
        this.subscribedInSession = Boolean.toString(subscribedInSession);
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    // Bundling
    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    // Bundling
    public String getSubscriptionType() {
        return subscriptionType;
    }

    public boolean isSubscriptionTypeOfALaCarte() {
        return "2".equals(subscriptionType);
    }
}

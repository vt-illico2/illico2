package com.videotron.tvi.illico.cyo.data;

/**
 * Define a type of URL for CYO.
 * @author Woojung Kim
 * @version 1.1
 */
public final class URLType {

    /** 2.4.1 Log in to the application. */
    public static final int LOGIN = 0;

    /** 2.4.2 Load Overview. */
    public static final int LOAD_OVERVIEW = 1;

    /** 2.4.3 Load « À la Carte» subscriptions. */
    public static final int LOAD_A_LA_CARTE = 2;

    /** 2.4.4 Modify « À la Carte» subscriptions. */
    public static final int MODIFY_A_LA_CARTE = 3;

    /** 2.4.5 Load subscription for channel groups. */
    public static final int LOAD_GROUP = 4;

    /** 2.4.6 Modify subscription for channel packages. */
    public static final int SAVE_GROUP = 5;

    /** 2.4.7 Load game subscriptions. */
    public static final int LOAD_GAME = 6;

    /** 2.4.8 Modify games subscription. */
    public static final int SAVE_GAME = 7;

    /** 2.4.9 Load interactive applications. */
    public static final int LOAD_ITV = 8;

    /** 2.4.10 Modify product. */
    public static final int SAVE_ITV = 9;

    /** 2.4.11 Load Basic package, fees and rebates. */
    public static final int LOAD_FEES = 10;

    /** 2.4.12 Load hardware. */
    public static final int LOAD_HARDWARE = 11;

    /** 2.4.13 Validation of TV modifications. */
    public static final int VALIDATION = 12;

    /** 2.4.14 Channel list of new subscription. */
    public static final int LIST_CHANNEL = 13;

    /** 2.4.15 Confirmation of TV modifications. */
    public static final int CONFIRMATION = 14;

    /** 2.4.16 Load Channel Information. */
    public static final int LOAD_CHANNEL_INFO = 15;

    /** 2.4.17 Compare Services. */
    public static final int COMPARE_SERVICES = 16;

    public static final int LOAD_PACKAGE_OFFER = 17;

    public static final int LOAD_PACKAGE_CATALOG = 18;

    public static final int SAVE_SELF_SERVE = 19;

    public static final int DEBUG_LOG = 98;

    /** R5 - SM5 <br> Log out*/
    public static final int LOG_OUT = 99;

    /** a constructor for Util class. */
    private URLType() {
    }
}

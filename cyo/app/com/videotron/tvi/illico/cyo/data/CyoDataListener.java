/**
 * @(#)CyoDataListener.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;

/**
 * Define a interface to receive a data from back-end.
 * @author Woojung Kim
 * @version 1.1
 */
public interface CyoDataListener {

    /** Indicate a request is failed. */
    int FAIL = 0;
    /** Indicate a request is a success. */
    int SUCCESS = 1;

    /**
     * Receive a data to be requested from back-end.
     * @param type a request type.
     * @param state a state of request.
     * @param obj a data to get.
     */
    void receiveCyoData(int type, int state, CyoObject obj);
    
    void receiveCyoError(int type);
}

/**
 * @(#)Service.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class Service  extends CyoObject {

    protected String title;
    protected String price;
    protected String sequenceNo;
    protected String modified;
    protected String pricefreq;
    protected String id;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public String getSequenceNo() {
        return sequenceNo;
    }
    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }
    public String getModified() {
        return modified;
    }
    public void setModified(String modified) {
        this.modified = modified;
    }
    public String getPricefreq() {
        return pricefreq;
    }
    public void setPricefreq(String pricefreq) {
        this.pricefreq = pricefreq;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
}

/**
 * @(#)Shortcuts.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * The list of shortcut links to be displayed.
 * @author Woojung Kim
 * @version 1.1
 */
public class Shortcuts extends CyoObject {

    private Group[] group;

    public Group[] getGroup() {
        return group;
    }

    public void setGroup(CyoObject[] group) {
        if (group == null) {
            return;
        }
        this.group = new Group[group.length];

        for (int i = 0; i < group.length; i++) {
            this.group[i] = (Group) group[i];
        }
    }
    
}

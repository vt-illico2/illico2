/**
 * @(#)URLManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * URLManager manager a URL to request a data.
 * @author Woojung Kim
 * @version 1.1
 */
public class URLManager {

    /** a singleton instance. */
    private static URLManager instance;

    /** a private constructor for singleton. */
    private URLManager() {
    }

    /**
     * Get a singleton instance.
     * @return a singleton instance.
     */
    public static URLManager getInstance() {
        if (instance == null) {
            instance = new URLManager();
        }

        return instance;
    }

    /**
     * Get a URL string.
     * @param type a type of URL. Refer {@link URLType}
     * @param param a Hashtable to be include a parameters for URL.
     * @return a completely URL.
     */
    public String getURL(int type, Hashtable param) {
        if (Log.INFO_ON) {
            Log.printInfo("URLManager: getURL()");
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("URLManager: type = " + type);
            if (param != null) {
                Log.printDebug("URLManager: param = " + param.toString());
            } else {
                Log.printDebug("URLManager: param = " + param);
            }
        }

        StringBuffer sf = new StringBuffer();

        if (type == URLType.LOGIN) {
            sf.append("Login.action?macId=");
            // mac
            // special case. one menu.(not left button) 
//            sf.append("0021BE8809AE");
            sf.append(CyoDataManager.getInstance().getMacAddress());
            // Bunndling - FOR-CYO-0020
            sf.append("&version=2");

        } else if (type == URLType.LOAD_OVERVIEW) {
            sf.append("LoadOverview.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.LOAD_A_LA_CARTE) {
            if (param.containsKey("url")) {
                sf.setLength(0);
                String url = (String) param.get("url");
                sf.append(url);
            } else {
                sf.append("LoadSelfServe.action;jsessionid=");
                sf.append(CyoDataManager.getInstance().getSessionID());
                String groupNo = (String) param.get("groupNo");
                if (groupNo != null) {
                    sf.append("?groupNo=");
                    sf.append(groupNo);
                }
                String sort = (String) param.get("sort");
                if (sort != null) {
                    if (groupNo == null) {
                        sf.append("?sort=");
                    } else {
                        sf.append("&sort=");
                    }
                    sf.append(sort);
                }
            }
        } else if (type == URLType.MODIFY_A_LA_CARTE) {
            sf.append("SaveSelfServe.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
            String add = (String) param.get("add");
            if (add != null) {
                sf.append("?add=");
                sf.append(add);
            }
            String remove = (String) param.get("remove");
            if (remove != null) {
                if (add == null) {
//                    sf.append("&remove=");
                    sf.append("?remove=");
                } else {
                    sf.append("&remove=");
                }
                sf.append(remove);
            }
            CyoDataManager.changedData = true;
        } else if (type == URLType.LOAD_GROUP) {
            if (param.containsKey("url")) {
                sf.setLength(0);
                String url = (String) param.get("url");
                sf.append(url);
            } else {
                sf.append("LoadGroup.action;jsessionid=");
                sf.append(CyoDataManager.getInstance().getSessionID());
                sf.append("?groupNo=");
                String groupNo = (String) param.get("groupNo");
                sf.append(groupNo);
                String sort = (String) param.get("sort");
                if (sort != null) {
                    sf.append("&sort=");
                    sf.append(sort);
                }
            }
        } else if (type == URLType.SAVE_GROUP) {
            sf.append("Save.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
//            sf.append("?groupno=");
//            String groupNo = (String) param.get("groupNo");
//            sf.append(groupNo);
            String add = (String) param.get("add");
            if (add != null) {
                sf.append("?add=");
                sf.append(add);
            }
            String remove = (String) param.get("remove");
            if (remove != null) {
                if (add == null) {
//                    sf.append("?remove=");
                    sf.append("?remove=");
                } else {
                    sf.append("&remove=");
                }
                sf.append(remove);
            }

            String requiresUpsell = (String) param.get("requiresUpsell");
            if (requiresUpsell != null) {
                sf.append("&requiresUpsell=false");
            } else {
                sf.append("&requiresUpsell=true");
            }
            CyoDataManager.changedData = true;
        } else if (type == URLType.LOAD_GAME) {
            sf.append("LoadGames.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.SAVE_GAME) {
            sf.append("SaveGames.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
            String add = (String) param.get("add");
            if (add != null) {
                sf.append("?add=");
                sf.append(add);
            }
            String remove = (String) param.get("remove");
            if (remove != null) {
                if (add == null) {
//                    sf.append("&remove=");
                    sf.append("?remove=");
                } else {
                    sf.append("&remove=");
                }
                sf.append(remove);
            }
            CyoDataManager.changedData = true;
        } else if (type == URLType.LOAD_ITV) {
            sf.append("LoadInteractiveProduct.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.SAVE_ITV) {
            sf.append("SaveInteractiveProduct.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
            String add = (String) param.get("add");
            if (add != null) {
                sf.append("?add=");
                sf.append(add);
            }
            String remove = (String) param.get("remove");
            if (remove != null) {
                if (add == null) {
                    sf.append("&remove=");
//                    sf.append("?remove=");
                } else {
                    sf.append("&remove=");
                }
                sf.append(remove);
            }
            CyoDataManager.changedData = true;
        } else if (type == URLType.LOAD_FEES) {
            sf.append("Fees.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.LOAD_HARDWARE) {
            sf.append("LoadHardware.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.VALIDATION) {
            sf.append("Validate.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());

            if (param != null) {
                String requiresUpsell = (String) param.get("requiresUpsell");
                if (requiresUpsell != null) {
                    sf.append("?requiresUpsell=false");
                }
            }

        } else if (type == URLType.LIST_CHANNEL) {
            sf.append("ListChannels.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
            String sort = (String) param.get("sort");
            if (sort != null) {
                sf.append("?sort=");
//                sf.append("&sort=");
                sf.append(sort);
            }
        } else if (type == URLType.CONFIRMATION) {
            sf.append("Confirm.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.LOAD_CHANNEL_INFO) {
            sf.append("LoadChannelOffer.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.COMPARE_SERVICES) {
            sf.append("CompareServices.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.LOG_OUT) {
        	sf.append("logout.action;jsessionid=");
        	if (Log.DEBUG_ON) {
            	Log.printDebug("URLManager: getUrl(): logout, sf = " + sf.toString());
            }
        	sf.append(CyoDataManager.getInstance().getSessionID());
        	return sf.toString();
        } else if (type == URLType.LOAD_PACKAGE_OFFER) {
            sf.append("LoadPackageOffer.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
        } else if (type == URLType.LOAD_PACKAGE_CATALOG) {
            sf.append("LoadCatalog.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());

            String catalogId = (String) param.get("catalogId");
            sf.append("?catalogId=");
            sf.append(catalogId);
            String sort = (String) param.get("sort");
            if (sort != null) {
                sf.append("&sort=");
                sf.append(sort);
            }
        } else if (type == URLType.SAVE_SELF_SERVE) {
            sf.append("SaveSelfServe.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());
            //            sf.append("?groupno=");
            //            String groupNo = (String) param.get("groupNo");
            //            sf.append(groupNo);
            String add = (String) param.get("add");
            if (add != null) {
                sf.append("?add=");
                sf.append(add);
            }
            String remove = (String) param.get("remove");
            if (remove != null) {
                if (add == null) {
                    //                    sf.append("?remove=");
                    sf.append("?remove=");
                } else {
                    sf.append("&remove=");
                }
                sf.append(remove);
            }

            // VDTRMASTER-5970
            sf.append("&requiresUpsell=true");

            CyoDataManager.changedData = true;
        }  else if (type == URLType.DEBUG_LOG) {
            sf.append("DebugLog.action;jsessionid=");
            sf.append(CyoDataManager.getInstance().getSessionID());

            String message = (String) param.get("message");
            sf.append("?message=");
            try {
                sf.append(URLEncoder.encode(message, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            return sf.toString();
        }

        // locale - all url should includes local - VDTRMASTER-4094
        if (Log.DEBUG_ON) {
        	Log.printDebug("URLManager: getUrl(): before add locale, sf = " + sf.toString());
        }
        if (sf.toString().indexOf("?") != -1) {
        	sf.append("&request_locale=");
        } else {
        	sf.append("?request_locale=");
        }
        
        if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_ENGLISH)) {
            sf.append("en_CA");
        } else {
            sf.append("fr_CA");
        }
        
        return sf.toString();
    }
}

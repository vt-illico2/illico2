/**
 * @(#)Channel.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class Channel extends Subscription {

    /** Indicate a a-la-carte. */
    private String aLaCarte;
    /** a URL of logo image. */
    private String logo;

    /** a call letter. */
    private String callLetters;
    /**
     * boolean Flag to indicate if this is a part of the Illico Web offering.
     */
    private String telecino;

    /** a language. */
    private String language;
    /** a position. */
    private String position;
    /** a HD position. */
    private String hdPosition;

    /** Indicates if this is a canadian channel. */
    private String canadian;
    
    // R5 - SM5
    private String chaineSurMesure5;
    private String isChainePrestige;

    public String getaLaCarte() {
        return aLaCarte;
    }

    public void setaLaCarte(String aLaCarte) {
        this.aLaCarte = aLaCarte;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCallLetters() {
        return callLetters;
    }

    public void setCallLetters(String callLetters) {
        this.callLetters = callLetters;
    }

    public String getTelecino() {
        return telecino;
    }

    public void setTelecino(String telecino) {
        this.telecino = telecino;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHdPosition() {
//        return hdPosition;
        // jira 2588
        return hdPosition != null ? position : null;
    }
    
    public boolean hasHDPosition() {
        return hdPosition != null ? true : false;

    }
    
    public boolean isHDPosition() {
    	return position != null && hdPosition != null ? position.equals(hdPosition) : false;
    }
    
    public void setHdPosition(String hdPosition) {
        this.hdPosition = hdPosition;
    }

    public boolean getCanadian() {
        return Boolean.valueOf(canadian).booleanValue();
    }

    public void setCanadian(String canadian) {
        this.canadian = canadian;
    }
    
    // R5 - SM5
    public void setChaineSurMesure5(String chaineSurMesure5) {
    	this.chaineSurMesure5 = chaineSurMesure5;
    }
    
    // R5 - SM5
    public boolean isChaineSurMesure5() {
    	return Boolean.valueOf(chaineSurMesure5).booleanValue();
    }
    
    // R5 - SM5
    public void setIsChainePrestige(String isChainePrestige) {
    	this.isChainePrestige = isChainePrestige;
    }
    
    // R5 - SM5
    public boolean isChainePrestige() {
    	return Boolean.valueOf(isChainePrestige).booleanValue();
    }
}

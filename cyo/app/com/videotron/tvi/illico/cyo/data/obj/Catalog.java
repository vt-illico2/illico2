package com.videotron.tvi.illico.cyo.data.obj;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-07-13.
 */
public class Catalog extends CyoObject {

	private String catalogId;
	private String title;
	private String premium;
	private String url;

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public boolean isPremium() {
		return Boolean.valueOf(premium).booleanValue();
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}

/**
 * @(#)Shortcut.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data.obj;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class Shortcut extends CyoObject {

    private String no;
    private String desc;
    private String subDesc;
    private String url;
    
    public String getNo() {
        return no;
    }
    public void setNo(String no) {
        this.no = no;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getSubDesc() {
        return subDesc;
    }
    public void setSubDesc(String subDesc) {
        this.subDesc = subDesc;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
}

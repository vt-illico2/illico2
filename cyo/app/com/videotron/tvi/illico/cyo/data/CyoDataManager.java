/**
 * @(#)CyoDataManager.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.data;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import org.ocap.hardware.Host;
import org.xml.sax.SAXException;

import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class CyoDataManager {

	/**
	 * a key of default channel logo in SharedMemmory.
	 */
	public static final String DEFAULT_CHANNLE_LOGO = "logo.default";
	/**
	 * The buffer size.
	 */
	private static final int BUFFER_SIZE = 1024;

	/**
	 * a singleton object.
	 */
	private static CyoDataManager instance;

	/**
	 * a MAC address of STB.
	 */
	private String macAddress;
	/**
	 * a root of CYO connection with back-end via Login API.
	 */
	private CyoObject root;
	/**
	 * a listener to be received a data from back-end.
	 */
	private CyoDataListener cyoListener;
	/**
	 * a channel-list to include a channel information.
	 */
	private ChannelList channelList;
	/**
	 * a image of default channel logo.
	 */
	private Image defaultChannelLogo;

	private Hashtable logoImageHash = new Hashtable();

	/**
	 * Stack for target URL.
	 */
	private Stack urlStack;
	/**
	 * thread of current request.
	 */
	private Thread curThread;

	/**
	 * a IP of back-end.
	 */
	private String serverIP = "dev06net.int.videotron.com";
	/**
	 * a port of back-end.
	 */
	private int serverPort = 80;
	/**
	 * a context of back-end.
	 */
	private String context = "/client/illico";
	//    private String context2 = "/client/illico";
	private String context2 = "/client/residentiel/illico";

	/**
	 * a proxy informations.
	 */
	private ProxyInfo[] proxyInfos;

	/**
	 * a current HttpUrlConnection.
	 */
	private HttpURLConnection currentConnect;

	public static boolean changedData = false;

	public static final int MODE_START = 0;
	public static final int MODE_STOP = 1;
	public int mode = MODE_START;

	private PackageList packageList;
	private String subscribedPackageNumberOfChannel = "0";
	private String blockPackageNumberOfChannel = "0";
	// subscribedPackageNumberOfChannel + blockPackageNumberofChannel
	private int myNumberOfChannel = 0;
	private boolean needParamForRequiresUpsell;
	private Package selectedPackage;

	/**
	 * a Hidden constructor.
	 */
	private CyoDataManager() {
		urlStack = new Stack();
		defaultChannelLogo = (Image) SharedMemory.getInstance().get(DEFAULT_CHANNLE_LOGO);
	}

	/**
	 * Get a singleton object.
	 *
	 * @return a object of this class.
	 */
	public static CyoDataManager getInstance() {
		if (instance == null) {
			instance = new CyoDataManager();
		}

		return instance;
	}

	/**
	 * Initialize a manger.
	 */
	public void init() {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: init()");
		}
		setMacAddress();

		// set defult server & port
		serverIP = DataCenter.getInstance().getString("cyoServer");
		serverPort = DataCenter.getInstance().getInt("cyoPort", 80);
		context = DataCenter.getInstance().getString("cyoContext");

		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: init(): serverIP : " + serverIP);
			Log.printDebug("CyoDataManager: init(): serverPort : " + serverPort);
			Log.printDebug("CyoDataManager: init(): context : " + context);
		}

		if (Environment.EMULATOR) {
			ProxyInfo pInfo = new ProxyInfo();

			pInfo.setIp("10.247.51.112");
			pInfo.setPort(8000);

			if (Log.DEBUG_ON) {
				Log.printDebug("CyoDataManager: init(): proxy IP : " + pInfo.getIp());
				Log.printDebug("CyoDataManager: init(): proxy Port : " + pInfo.getPort());
			}

			setProxyInfo(new ProxyInfo[] { pInfo });
		}
	}

	public void start() {
		mode = MODE_START;
		Object[][] array = (Object[][]) SharedMemory.getInstance().get(EpgService.DATA_KEY_CHANNELS);

		if (logoImageHash == null) {
			logoImageHash = new Hashtable();
		}

		if (array != null) {
			for (int a = 0; a < array.length; a++) {
				//                Log.printDebug("channels = " + array[a][0] + " " + array[a][1] + " " + array[a][2]);
				logoImageHash.put(array[a][0], array[a][1]);
			}
		}
		defaultChannelLogo = (Image) SharedMemory.getInstance().get(DEFAULT_CHANNLE_LOGO);
	}

	/**
	 * Stop a manager.
	 */
	public void stop() {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: stop()");
		}
		mode = MODE_STOP;

		channelList = null;
		cyoListener = null;
		if (curThread != null && curThread.isAlive()) {
			curThread.interrupt();
			curThread = null;
		}

		if (currentConnect != null) {
			currentConnect.disconnect();
			currentConnect = null;
		}

		if (defaultChannelLogo != null) {
			defaultChannelLogo.flush();
			defaultChannelLogo = null;
		}

		if (logoImageHash != null) {
			logoImageHash.clear();
			logoImageHash = null;
		}

		// R5-SM5
		if (root != null) {
			requestLogout();
		}
		root = null;
	}

	public void requestLogout() {
		Log.printInfo("CyoDataManager, requestLogout");
		try {
			String urlString = URLManager.getInstance().getURL(URLType.LOG_OUT, null);

			StringBuffer sb = new StringBuffer();
			sb.append("http://");
			sb.append(serverIP);
			sb.append(":");
			sb.append(serverPort);
			if (Environment.EMULATOR) {
				sb.append(context2);
			} else {
				sb.append(context);
			}

			sb.append("/");
			sb.append(urlString);

			if (Log.DEBUG_ON) {
				Log.printDebug("CyoDataManager, requestLogout, url=" + urlString);
				Log.printDebug("CyoDataManager, requestLogout, proxyInfos=" + proxyInfos);
			}

			if (proxyInfos != null) {

				String[] proxyHosts = new String[proxyInfos.length];
				int[] proxyPorts = new int[proxyInfos.length];

				for (int i = 0; i < proxyInfos.length; i++) {
					proxyHosts[i] = proxyInfos[i].getIp();
					proxyPorts[i] = proxyInfos[i].getPort();
				}

				byte[] ret = URLRequestor.getBytes(proxyHosts, proxyPorts, sb.toString(), null);

				Log.printDebug("CyoDataManager, requestLogout, with proxy, ret=" + ret);
			} else {
				byte[] ret = URLRequestor.getBytes(sb.toString(), null);

				Log.printDebug("CyoDataManager, requestLogout, without proxy, ret=" + ret);
			}

		} catch (Exception e) {
			Log.printError("CyoDataManager, requestLogout, e=" + e.getMessage());
			e.printStackTrace();
		}

	}

	public void requestDebug() {
		Log.printInfo("CyoDataManager, requestDebug");
		try {
			Hashtable param = new Hashtable();
			param.put("message", "Login completed MAC " + getMacAddress() + " jsessionid " + getSessionID());

			String urlString = URLManager.getInstance().getURL(URLType.DEBUG_LOG, param);

			StringBuffer sb = new StringBuffer();
			sb.append("http://");
			sb.append(serverIP);
			sb.append(":");
			sb.append(serverPort);
			if (Environment.EMULATOR) {
				sb.append(context2);
			} else {
				sb.append(context);
			}

			sb.append("/");
			sb.append(urlString);

			if (Log.DEBUG_ON) {
				Log.printDebug("CyoDataManager, requestDebug, url=" + urlString);
				Log.printDebug("CyoDataManager, requestDebug, proxyInfos=" + proxyInfos);
			}

			if (proxyInfos != null) {

				String[] proxyHosts = new String[proxyInfos.length];
				int[] proxyPorts = new int[proxyInfos.length];

				for (int i = 0; i < proxyInfos.length; i++) {
					proxyHosts[i] = proxyInfos[i].getIp();
					proxyPorts[i] = proxyInfos[i].getPort();
				}

				byte[] ret = URLRequestor.getBytes(proxyHosts, proxyPorts, sb.toString(), null);

				Log.printDebug("CyoDataManager, requestDebug, with proxy, ret=" + ret);
			} else {
				byte[] ret = URLRequestor.getBytes(sb.toString(), null);

				Log.printDebug("CyoDataManager, requestDebug, without proxy, ret=" + ret);
			}

		} catch (Exception e) {
			Log.printError("CyoDataManager, requestDebug, e=" + e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * Request a data to back-end.
	 *
	 * @param type  a type of URL.
	 * @param param a parameter to be added in URL
	 */
	public void request(final int type, final Hashtable param) {
		Log.printInfo("CyoDataManager, request, type=" + type + ", param=" + param);
		curThread = new Thread("CYO.Requst.type-" + type) {
			/**
			 * Run.
			 */
			public void run() {

				if (type == URLType.LOGIN || type == URLType.LOAD_CHANNEL_INFO) {
					try {
						Thread.sleep(500L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				if (mode == MODE_STOP) {
					if (Log.DEBUG_ON) {
						Log.printDebug(
								"CyoDataManager: request: CyoDataManager is stoped, so current thread is canceled.");
					}
					return;
				}

				CyoController.getInstance().showLoadingAnimation();

				// R5-SM5
				if (type == URLType.LOGIN && root != null) {
					requestLogout();
				}

				StringBuffer sb = new StringBuffer();
				sb.append("http://");
				sb.append(serverIP);
				sb.append(":");
				sb.append(serverPort);
				if (Environment.EMULATOR) {
					sb.append(context2);
				} else {
					sb.append(context);
				}

				sb.append("/");

				if (proxyInfos != null && proxyInfos.length > 0) {

					for (int i = 0; i < proxyInfos.length; i++) {
						String urlString = URLManager.getInstance().getURL(type, param);
						// Log.printInfo("param.containsKey = " + param.containsKey("url"));
						if (param != null && param.containsKey("url")) {
							sb.setLength(0);
							sb.append((String) param.get("url"));

							if (sb.toString().indexOf("?") != -1) {
								sb.append("&request_locale=");
							} else {
								sb.append("?request_locale=");
							}

							if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_ENGLISH)) {
								sb.append("en_CA");
							} else {
								sb.append("fr_CA");
							}

						} else {
							sb.append(urlString);
						}

						if (Log.DEBUG_ON) {
							Log.printDebug("CyoDataManager: url = " + sb.toString());
						}

						HttpURLConnection connect = null;
						BufferedInputStream bis = null;
						try {
							URL url = new URL("HTTP", proxyInfos[i].getIp(), proxyInfos[i].getPort(), sb.toString());
							ByteArrayOutputStream baos = new ByteArrayOutputStream();

							long startTime = System.currentTimeMillis();
							if (Log.DEBUG_ON) {
								Log.printDebug("CyoDataManager: request, proxy, url=" + url);
								Log.printDebug("CyoDataManager: request before~!!");
							}

							connect = (HttpURLConnection) url.openConnection();
							currentConnect = connect;

							int code = connect.getResponseCode();
							if (Log.DEBUG_ON) {
								Log.printDebug("CyoDataManager: ResponseCode = " + code);
							}
							if (code == HttpURLConnection.HTTP_NOT_FOUND) {
								if (Log.DEBUG_ON) {
									Log.printDebug("CyoDataManager: HTTP_NOT_FOUND!");
								}
								// return;
								throw new Exception("HTTP_NOT_FOUND");
							} else if (code == HttpURLConnection.HTTP_INTERNAL_ERROR) {
								if (Log.DEBUG_ON) {
									Log.printDebug("CyoDataManager: HTTP_INTERNAL_ERROR!");
								}
								//return;
								throw new Exception("HTTP_INTERNAL_ERROR");
							}
							bis = new BufferedInputStream(connect.getInputStream());

							if (Log.DEBUG_ON) {
								Log.printDebug("CyoDataManager: request page download!! pass parser~!" + (
										System.currentTimeMillis() - startTime));
							}

							int size = 0;
							byte[] buf = new byte[1024];
							while ((size = bis.read(buf)) > -1) {
								baos.write(buf, 0, size);
							}

							String src = baos.toString("UTF-8");
							if (Log.DEBUG_ON) {
								Log.printDebug("CyoDataManager: received data");
								Log.printDebug(src);
							}
							baos.flush();
							baos.close();
							ByteArrayInputStream bais = new ByteArrayInputStream(src.getBytes());

							SAXParserFactory factory = SAXParserFactory.newInstance();
							SAXParser parser = factory.newSAXParser();
							CyoXMLHandler handler = new CyoXMLHandler(type);
							parser.parse(bais, handler);

						} catch (SAXException e) {
							e.printStackTrace();
							Log.printError(e);
							if (i < proxyInfos.length - 1) {
								continue;
							} else {
								CyoController.getInstance().showErrorMessage("CYO503");
							}
						} catch (Exception e) {
							e.printStackTrace();
							Log.printError(e);
							if (i < proxyInfos.length - 1) {
								continue;
							} else {
								CyoController.getInstance().showErrorMessage("CYO500");
							}
						} finally {
							if (connect != null) {
								connect.disconnect();
								connect = null;
							}
						}
					}
				} else {
					String urlString = URLManager.getInstance().getURL(type, param);

					// Log.printInfo("param.containsKey = " + param.containsKey("url"));
					if (param != null && param.containsKey("url")) {
						sb.setLength(0);
						sb.append((String) param.get("url"));

						if (sb.toString().indexOf("?") != -1) {
							sb.append("&request_locale=");
						} else {
							sb.append("?request_locale=");
						}

						if (FrameworkMain.getInstance().getCurrentLanguage().equals(Constants.LANGUAGE_ENGLISH)) {
							sb.append("en_CA");
						} else {
							sb.append("fr_CA");
						}
					} else {
						sb.append(urlString);
					}

					if (Log.DEBUG_ON) {
						Log.printDebug("CyoDataManager: url = " + sb.toString());
					}

					HttpURLConnection connect = null;
					BufferedInputStream bis = null;
					try {
						URL url = new URL(sb.toString());

						ByteArrayOutputStream baos = new ByteArrayOutputStream();

						long startTime = System.currentTimeMillis();
						if (Log.DEBUG_ON) {
							Log.printDebug("CyoDataManager: request before~!!");
						}

						if (Environment.EMULATOR) {
							// //////////////////////////////////////////////////////////
							if (type == URLType.LOGIN) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/LoginGP.xml")));
							} else if (type == URLType.LOAD_CHANNEL_INFO) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/LoadChannelOffer.xml")));
							} else if (type == URLType.LOAD_GROUP) {
								if (param.size() == 1) {
									if (url.toString().indexOf("1516") > -1 || url.toString().indexOf("1999") > -1) {
										bis = new BufferedInputStream(new FileInputStream(
												new File("resource/test_xml/LoadGroup_Packages.xml")));
									} else {
										bis = new BufferedInputStream(
												new FileInputStream(new File("resource/test_xml/LoadGroup.xml")));
									}
								} else {
									bis = new BufferedInputStream(
											new FileInputStream(new File("resource/test_xml/LoadGroupAction-1.xml")));
								}
							} else if (type == URLType.SAVE_GROUP) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/SaveAction.xml")));
							} else if (type == URLType.VALIDATION || type == URLType.CONFIRMATION) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/Validate2.xml")));
							} else if (type == URLType.LOAD_OVERVIEW) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/LoadOverview.xml")));
							} else if (type == URLType.LOAD_FEES) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/FeesAction.xml")));
							} else if (type == URLType.LIST_CHANNEL) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/ListChannels.xml")));
							} else if (type == URLType.LOAD_GAME) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/LoadGamesAction.xml")));
							} else if (type == URLType.COMPARE_SERVICES) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/CompareServices.xml")));
							} else if (type == URLType.LOAD_PACKAGE_OFFER) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/LoadPackageOffer.xml")));
							} else if (type == URLType.LOAD_PACKAGE_CATALOG) {
								if (urlString.indexOf("catalogId=1") > -1) {
									bis = new BufferedInputStream(
											new FileInputStream(new File("resource/test_xml/LoadPackageCatalog1.xml")));
								} else if (urlString.indexOf("catalogId=2") > -1) {
									bis = new BufferedInputStream(
											new FileInputStream(new File("resource/test_xml/LoadPackageCatalog2.xml")));
								} else if (urlString.indexOf("catalogId=3") > -1) {
									bis = new BufferedInputStream(
											new FileInputStream(new File("resource/test_xml/LoadPackageCatalog3.xml")));
								}
							} else if (type == URLType.LOAD_A_LA_CARTE) {
								bis = new BufferedInputStream(
										new FileInputStream(new File("resource/test_xml/LoadSelfServe.xml")));
							}
							// ///////////////////////////////////////////////////////////
						} else {
							connect = (HttpURLConnection) url.openConnection();
							currentConnect = connect;

							int code = connect.getResponseCode();
							if (Log.DEBUG_ON) {
								Log.printDebug("CyoDataManager: ResponseCode = " + code);
							}
							if (code == HttpURLConnection.HTTP_NOT_FOUND) {
								if (Log.DEBUG_ON) {
									Log.printDebug("CyoDataManager: HTTP_NOT_FOUND!");
								}
								// return;
								throw new Exception("HTTP_NOT_FOUND");
							} else if (code == HttpURLConnection.HTTP_INTERNAL_ERROR) {
								if (Log.DEBUG_ON) {
									Log.printDebug("CyoDataManager: HTTP_INTERNAL_ERROR!");
								}
								//return;
								throw new Exception("HTTP_INTERNAL_ERROR");
							}
							bis = new BufferedInputStream(connect.getInputStream());
						}

						if (Log.DEBUG_ON) {
							Log.printDebug("CyoDataManager: request page download!! pass parser~!" + (
									System.currentTimeMillis() - startTime));
						}

						int size = 0;
						byte[] buf = new byte[1024];
						while ((size = bis.read(buf)) > -1) {
							baos.write(buf, 0, size);
						}

						String src = baos.toString("UTF-8");
						if (Log.DEBUG_ON) {
							Log.printDebug("CyoDataManager: received data");
							Log.printDebug(src);
						}
						baos.flush();
						baos.close();
						ByteArrayInputStream bais = new ByteArrayInputStream(src.getBytes());

						SAXParserFactory factory = SAXParserFactory.newInstance();
						SAXParser parser = factory.newSAXParser();
						CyoXMLHandler handler = new CyoXMLHandler(type);
						parser.parse(bais, handler);

					} catch (SAXException e) {
						e.printStackTrace();
						Log.printError(e);
						CyoController.getInstance().showErrorMessage("CYO503");
					} catch (Exception e) {
						e.printStackTrace();
						Log.printError(e);
						CyoController.getInstance().showErrorMessage("CYO500");
					} finally {
						if (connect != null) {
							connect.disconnect();
							connect = null;
						}
					}
				}
				if (type != URLType.LOGIN) {
					CyoController.getInstance().hideLoadingAnimation();
				}
			}
		};
		curThread.start();
	}

	/**
	 * Add a CyoDataLisener.
	 *
	 * @param l a listener to be receive a event.
	 */
	public void addCyoDataListener(CyoDataListener l) {
		cyoListener = l;
	}

	/**
	 * remove a CyoDataListener.
	 */
	public void removeCyoDataListener() {
		cyoListener = null;
	}

	/**
	 * Push a URL in Stack.
	 *
	 * @param url a string of URL.
	 */
	public void pushUrl(String url) {
		urlStack.push(url);
	}

	/**
	 * Pop a string of URL.
	 *
	 * @return a string of URL.
	 */
	public String popUrl() {
		return (String) urlStack.pop();
	}

	/**
	 * Clear a URL stack.
	 */
	public void clearUrlStack() {
		urlStack.clear();
	}

	/**
	 * Update a data from back-end. CyoDataListener to be added has a event.
	 *
	 * @param type  a type of url.
	 * @param state a state of request.
	 * @param obj   a data to be parsed.
	 */
	public void updateCyoDataListener(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: updateCyoDataListener()");
		}
		if (type == URLType.LOAD_CHANNEL_INFO && state == CyoDataListener.SUCCESS) {
			channelList = (ChannelList) obj;
		} else if (type == URLType.LOGIN && state == CyoDataListener.SUCCESS) {
			root = obj;

			try {
				requestDebug();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (type == URLType.LOAD_PACKAGE_OFFER && state == CyoDataListener.SUCCESS) {
			packageList = (PackageList) obj;
			processMyPackage();
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: updateCyoDataListener: success = " + obj.getSuccess());
			Log.printDebug("CyoDataManager: updateCyoDataListener: obj's sub = " + obj.getCyoObject());
		}

		if (obj.getSuccess().equals("0") && obj.getCyoObject() != null) {
			CyoObject cyoObj = obj.getCyoObject()[0];
			if (cyoObj instanceof Message && ((Message) cyoObj).getType().equals("4")) {
				if (Log.ERROR_ON) {
					Log.printError("CyoDataManager: updateCyoDataListener: back-end system error.");
				}
				CyoController.getInstance().showErrorMessage("CYO504");
				if (cyoListener != null) {
					cyoListener.receiveCyoError(type);
				}

				return;
			}
		}

		if (cyoListener != null) {
			cyoListener.receiveCyoData(type, state, obj);
			FrameworkMain.getInstance().getHScene().repaint();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("CyoDataManager: updateCyoDataListener: cyoListener is null.");
			}
		}
	}

	public void processMyPackage() {
		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: processMyPackage");
		}

		if (packageList != null) {
			CyoObject[] objs = packageList.getCyoObject();
			for (int i = 0; i < objs.length; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug("CyoDataManager: processMyPackage, objs[" + i + "]=" + objs[i]);
				}
				if (objs[i] instanceof Package) {
					if (((Package) objs[i]).getSubscribedInAccount()) {
						subscribedPackageNumberOfChannel = ((Package) objs[i]).getNumberOfChannels();
						if (Log.DEBUG_ON) {
							Log.printDebug("CyoDataManager, processMyPackage, subscribedPackageNumberOfChannel="
									+ subscribedPackageNumberOfChannel);
						}
					}
				} else if (objs[i] instanceof BlockList) {
					CyoObject[] blockObjs = objs[i].getCyoObject();

					for (int bIdx = 0; bIdx < blockObjs.length; bIdx++) {
						if (((Package) blockObjs[bIdx]).getSubscribedInAccount()) {
							blockPackageNumberOfChannel = ((Package) blockObjs[bIdx]).getNumberOfChannels();
							if (Log.DEBUG_ON) {
								Log.printDebug("CyoDataManager, processMyPackage, blockPackageNumberOfChannel="
										+ blockPackageNumberOfChannel);
							}
						}
					}
				}
			}
		}

		int myPackageNumber = 0;
		if (subscribedPackageNumberOfChannel != null) {
			myPackageNumber = Integer.valueOf(subscribedPackageNumberOfChannel).intValue();
		}
		int myBlockPackageNumber = 0;
		if (blockPackageNumberOfChannel != null) {
			myBlockPackageNumber = Integer.valueOf(blockPackageNumberOfChannel).intValue();
		}

		myNumberOfChannel = myPackageNumber + myBlockPackageNumber;
	}

	public int getMyNumberOfChannel() {
		return myNumberOfChannel;
	}

	public Package getPackageInPackageList(String packageNo) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: getPackageInPackageList, packageNo=" + packageNo);
		}

		if (packageList != null) {
			CyoObject[] objs = packageList.getCyoObject();
			for (int i = 0; i < objs.length; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug("CyoDataManager: getPackageInPackageList, objs[" + i + "]=" + objs[i]);
				}
				if (objs[i] instanceof Package) {
					if (Log.DEBUG_ON) {
						Log.printDebug(
								"CyoDataManager, getPackageInPackageList, package.no=" + ((Package) objs[i]).getNo());
					}
					if (((Package) objs[i]).getNo().equals(packageNo)) {
						if (Log.DEBUG_ON) {
							Log.printDebug("CyoDataManager, getPackageInPackageList, find it");
						}

						return (Package) objs[i];
					}
				}
			}
		}
		return null;
	}

	public Package getCurrentPackageInPackageList() {
		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: getCurrentPackageInPackageList");
		}

		if (packageList != null) {
			CyoObject[] objs = packageList.getCyoObject();
			for (int i = 0; i < objs.length; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug("CyoDataManager: getCurrentPackageInPackageList, objs[" + i + "]=" + objs[i]);
				}
				if (objs[i] instanceof Package) {
					if (Log.DEBUG_ON) {
						Log.printDebug(
								"CyoDataManager, getCurrentPackageInPackageList, package.no=" + ((Package) objs[i])
										.getNo());
					}
					if (((Package) objs[i]).getSubscribedInSession()) {
						if (Log.DEBUG_ON) {
							Log.printDebug("CyoDataManager, getCurrentPackageInPackageList, find it");
						}

						return (Package) objs[i];
					}
				}
			}
		}

		return null;
	}

	public Package getBlocPackageInCurrentPackage(String no) {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: getBlocPackageInCurrentPackage(): no = " + no);
		}
		Package currentPackage = getCurrentPackageInPackageList();

		BlockList bList = currentPackage.getBlockList();

		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager, getBlocPackageInCurrentPackage, bList=" + bList);
		}

		if (bList != null && bList.getCyoObject() != null) {
			for (int i = 0; i < bList.getCyoObject().length; i++) {
				Package p = (Package) bList.getCyoObject()[i];
				if (p != null && p.getNo().equals(no)) {
					if (Log.DEBUG_ON) {
						Log.printDebug("CyoDataManager, getBlocPackageInCurrentPackage, find it");
					}
					// VDTRMASTER-5981
					return p;
				}
			}
		}

		return null;
	}

	public Package getCurrentBlocPackageInCurrentPackage() {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: getCurrentBlocPackageInCurrentPackage()");
		}
		Package currentPackage = getCurrentPackageInPackageList();

		BlockList bList = currentPackage.getBlockList();

		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager, getBlocPackageInCurrentPackage, bList=" + bList);
		}

		if (bList != null && bList.getCyoObject() != null) {
			for (int i = 0; i < bList.getCyoObject().length; i++) {
				Package p = (Package) bList.getCyoObject()[i];
				if (p != null && p.getSubscribedInSession()) {
					if (Log.DEBUG_ON) {
						Log.printDebug("CyoDataManager, getBlocPackageInCurrentPackage, find it");
					}
					// VDTRMASTER-5981
					return p;
				}
			}
		}

		return null;
	}

	// VDTRMASTER-5965
	public boolean isIncludedInExtraList(Subscription s) {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: isIncludedInExtraList(), s=" + s);
		}

		if (s == null) {
			return false;
		}

		String target = "";
		if (s instanceof Product || s instanceof Package) {
			target = s.getNo();
		} else {
			target = s.getId();
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: isIncludedInExtraList(), target=" + target);
		}

		if (selectedPackage != null) {
			ExtraList exList = selectedPackage.getExtraList();

			if (exList != null) {
				CyoObject[] objs = exList.getCyoObject();

				if (Log.DEBUG_ON) {
					Log.printDebug("CyoDataManager: isIncludedInExtraList(), exList.getCyoObject()=" + objs);
				}

				if (objs != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("CyoDataManager: isIncludedInExtraList(), exList.length=" + objs.length);
					}
					for (int i = 0; i < objs.length; i++) {
						Subscription sObj = (Subscription) objs[i];
						if (sObj instanceof Package || sObj instanceof Product) {
							if (sObj.getNo() != null && sObj.getNo().equals(target)) {
								if (Log.DEBUG_ON) {
									Log.printDebug("CyoDataManager: isIncludedInExtraList(), find sObj.no="
											+ sObj.getNo());
								}
								return true;
							}
						} else if (objs[i] instanceof Channel) {
							if (sObj.getId() != null && sObj.getId().equals(target)) {
								if (Log.DEBUG_ON) {
									Log.printDebug("CyoDataManager: isIncludedInExtraList(), find sObj.id="
											+ sObj.getId());
								}
								return true;
							}
						} else {
							if (Log.DEBUG_ON) {
								Log.printDebug("CyoDataManager: isIncludedInExtraList(), unknown obj[" + i + "]="
										+ objs[i]);
							}
						}
					}
				}
			}
		}

		return false;
	}

	// VDTRMASTER-5965
	public void setSelectedPackage(Package selectedPackage) {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: setSelectedPackage(): selectedPackage = "
					+ selectedPackage.getNo());
		}
		this.selectedPackage = selectedPackage;
	}

	public boolean needParamForRequiresUpsell() {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: needParamForRequiresUpsell(): needParamForRequiresUpsell = "
					+ needParamForRequiresUpsell);
		}
		return needParamForRequiresUpsell;
	}

	public void setNeedParamForRequiresUpsell(boolean value) {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: setNeedParamForRequiresUpsell(): value = " + value);
		}
		needParamForRequiresUpsell = value;
	}

	/**
	 * Get a channel information of callLetter.
	 *
	 * @param callLetter a callLetter to get a information.
	 * @return a {@link Channel} to have a information.
	 */
	public Channel getChannelInfo(String callLetter) {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: getChannelInfo(): callLetter = " + callLetter);
		}
		if (channelList == null) {
			return null;
		}
		Channel ch = null;

		int chIndex = channelList.getCyoObject().length / 2;
		while (true) {
			Channel tempCh = (Channel) channelList.getCyoObject()[chIndex];

			if (tempCh.getCallLetters().equals(callLetter)) {
				ch = tempCh;
				break;
			} else {
				int tempValue = Integer.parseInt(tempCh.getCallLetters().trim());
				int value = Integer.parseInt(callLetter);
				if (tempValue > value) {
					chIndex = chIndex - chIndex / 2;
				} else {
					chIndex = chIndex + chIndex / 2;
				}
			}
		}

		return ch;
	}

	/**
	 * Get a channel information of id.
	 *
	 * @param id a channel id. ID had got from back-end data.
	 * @return a {@link Channel} to have a information.
	 */
	public Channel getChannelInfoById(String id) {
		// if (Log.INFO_ON) {
		// Log.printInfo("CyoDataManager: getChannelInfoById(): id = " + id);
		// }
		if (channelList == null) {
			return null;
		}
		Channel ch = null;

		int length = channelList.getCyoObject().length;

		for (int i = 0; i < length; i++) {
			Channel tempCh = (Channel) channelList.getCyoObject()[i];

			if (tempCh.getId().equals(id)) {
				ch = tempCh;
				break;
			}
		}

		return ch;
	}

	//    /**
	//     * Get a image of channel logo in SharedMemory. if can't find a logo, return a default channel logo.
	//     * @param name a call letter of channel.
	//     * @return a channel logo image to be found.
	//     */
	// public Image getChannelLogo(String name) {
	// Image logo;
	// logo = (Image) SharedMemory.getInstance().get("logo." + name);
	//
	// if (logo == null) {
	// return defaultChannelLogo;
	// } else {
	// return logo;
	// }
	// }

	public Image getChannelLogo(String chNumber) {
		Image logo = null;
		Integer i1 = new Integer(chNumber);
		// logo = (Image) logoImageHash.get(i1);
		String logoStr = (String) logoImageHash.get(i1);
		//        Log.printInfo("chNumber = " + chNumber + " logoStr = " + logoStr);
		if (logoStr == null) {
			return defaultChannelLogo;
		} else {
			logo = (Image) SharedMemory.getInstance().get("logo." + logoStr);
			//            Log.printInfo("chNumber1 = " + chNumber + " logo = " + logo);
			return logo == null ? defaultChannelLogo : logo;
		}
	}

	/**
	 * set MacAddress.
	 */
	private void setMacAddress() {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: setMacAddress()");
		}

		boolean useTestMac = DataCenter.getInstance().getBoolean("useTestMac");

		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: useTestMac = " + useTestMac);
			Log.printDebug("CyoDataManager: Environment.HW_VENDOR = " + Environment.HW_VENDOR);
			Log.printDebug("CyoDataManager: VENDOR NAME = " + Environment.VENDOR_NAME);
		}

		if (Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR || useTestMac) {
			macAddress = DataCenter.getInstance().getString("testMac");

			if (Log.DEBUG_ON) {
				Log.printDebug("CyoDataManager: set a mac with testMac");
			}
		} else if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO) {
			macAddress = Host.getInstance().getReverseChannelMAC();
			String[] token = TextUtil.tokenize(macAddress, ":");
			StringBuffer address = new StringBuffer();
			for (int i = 0; i < token.length; i++) {
				address.append(token[i]);
			}
			macAddress = address.toString();
		} else {
			try {
				MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
				if (monitor != null) {
					byte[] addr = monitor.getCableCardMacAddress();

					macAddress = getStringBYTEArray(addr);
				}
			} catch (Exception e) {
				if (Log.EXTRA_ON) {
					Log.print(e);
				}
				e.printStackTrace();
			}
		}

		if (macAddress != null) {
			macAddress = macAddress.toUpperCase();
		}

		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager, macAddress : " + macAddress);
		}
	}

	public boolean needToDisplay5CBanner() {

		// VDTRMASTER-5951
		if (packageList != null) {
			Package[] packageArr = packageList.getPackages();

			if (packageArr != null && packageArr.length > 0 && packageArr[0] != null) {
				if (packageArr[0].getPackageCatalogs() != null) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Return a current session id.
	 *
	 * @return session id.
	 */
	public String getSessionID() {
		if (root != null) {
			return root.getSessionId();
		}
		return null;
	}

	/**
	 * Return a MAC address.
	 *
	 * @return address.
	 */
	public String getMacAddress() {
		return macAddress;
	}

	/**
	 * Set a server information from MS.
	 *
	 * @param ip   server ip
	 * @param port server port
	 * @param c    context in URL.
	 */
	public void setServerInfo(String ip, int port, String c) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: setServerInfo(): ip=" + ip + ", port=" + port + ", context=" + c);
		}
		serverIP = ip;
		serverPort = port;
		context = c;

		//        Log.printDebug("CyoDataManager: setServerInfo(): temporarily ignore a config from MS");

		//        if (Log.DEBUG_ON) {
		//        	Log.printDebug("CyoDataManager: setServerInfo(): temporarily use server information for skinny");
		//
		//        	serverIP = "vl113493.int.videotron.com";
		//        	serverPort = 9085;
		//
		//        	Log.printDebug("CyoDataManager: setServerInfo(): serverIP=" + serverIP + "\t serverPort=" + serverPort + "\tcontext=" + c);
		//        }
	}

	/**
	 * Set a proxy server information from MS.
	 *
	 * @param info
	 */
	public void setProxyInfo(ProxyInfo[] info) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CyoDataManager: setProxyInfo()");
			if (info != null) {
				Log.printDebug("CyoDataManager: setProxyInfo(): info length =" + info.length);
			}
		}
		proxyInfos = info;
	}

	/**
	 * Update a information from MS.
	 *
	 * @param f a file to include a information.
	 */
	public void updateInformationData(File f) {
		if (Log.INFO_ON) {
			Log.printInfo("CyoDataManager: updateInformationData()");
		}

		try {
			byte[] data = getByte(f);

			int index = 0;

			// Version
			int version = oneByteToInt(data, index++);
			if (Log.DEBUG_ON) {
				Log.printDebug("version:" + version);
			}

			// server info
			int serverIPLength = oneByteToInt(data, index++);
			String serverIP = byteArrayToString(data, index, serverIPLength);
			index += serverIPLength;
			int serverPort = twoBytesToInt(data, index);
			index += 2;
			int contextLength = oneByteToInt(data, index++);
			String context = byteArrayToString(data, index, contextLength);
			index += contextLength;
			setServerInfo(serverIP, serverPort, context);

			// proxy info
			int proxyListLength = oneByteToInt(data, index++);
			Vector proxyVector = new Vector();
			for (int i = 0; i < proxyListLength; i++) {
				int proxyIpLength = oneByteToInt(data, index++);
				String ip = byteArrayToString(data, index, proxyIpLength);
				index += proxyIpLength;
				int port = twoBytesToInt(data, index);
				index += 2;

				ProxyInfo proxy = new ProxyInfo();
				proxy.setIp(ip);
				proxy.setPort(port);
				proxyVector.add(proxy);
			}

			ProxyInfo[] pInfos = new ProxyInfo[proxyVector.size()];
			proxyVector.copyInto(pInfos);
			this.proxyInfos = pInfos;

			// VDTRMASTER-5745 & VDTRMASTER-5746
			//            // R5 - use new server Information.
			//            if (Log.DEBUG_ON) {
			//                Log.printDebug("CyoDataManager: updateInformationData(): start to parse a new CYO server Info");
			//            }
			//            // server info
			//            serverIPLength = oneByteToInt(data, index++);
			//            serverIP = byteArrayToString(data, index, serverIPLength);
			//            index += serverIPLength;
			//            serverPort = twoBytesToInt(data, index);
			//            index += 2;
			//            contextLength = oneByteToInt(data, index++);
			//            context = byteArrayToString(data, index, contextLength);
			//            index += contextLength;
			//            setServerInfo(serverIP, serverPort, context);
			//
			//            // proxy info
			//            proxyListLength = oneByteToInt(data, index++);
			//            proxyVector = new Vector();
			//            for (int i = 0; i < proxyListLength; i++) {
			//                int proxyIpLength = oneByteToInt(data, index++);
			//                String ip = byteArrayToString(data, index, proxyIpLength);
			//                index += proxyIpLength;
			//                int port = twoBytesToInt(data, index);
			//                index += 2;
			//
			//                ProxyInfo proxy = new ProxyInfo();
			//                proxy.setIp(ip);
			//                proxy.setPort(port);
			//                proxyVector.add(proxy);
			//            }
			//
			//            pInfos = new ProxyInfo[proxyVector.size()];
			//            proxyVector.copyInto(pInfos);
			//            this.proxyInfos = pInfos;

		} catch (Exception e) {
			Log.printError(e);
			CyoController.getInstance().showErrorMessage("CYO502");
		}
	}

	/**
	 * Get a byte array.
	 *
	 * @param f a file to want.
	 * @return a byte array to get from file.
	 */
	public byte[] getByte(File f) {
		byte[] retVal = null;

		int size;
		byte[] buffer = new byte[BUFFER_SIZE];
		FileInputStream fis = null;
		ByteArrayOutputStream baos = null;
		try {
			fis = new FileInputStream(f);
			baos = new ByteArrayOutputStream();
			while ((size = fis.read(buffer)) != -1) {
				baos.write(buffer, 0, size);
			}

			retVal = baos.toByteArray();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (baos != null) {
					baos.close();
				}
			} catch (Exception e) {
				Log.print(e);
			}

			try {
				if (fis != null) {
					fis.close();
				}
			} catch (Exception e) {
				Log.print(e);
			}
		}
		return retVal;
	}

	/**
	 * Converts one byte to integer.
	 *
	 * @param data   The byte array to use for converting.
	 * @param offset the offset that Converting start.
	 * @return The integer which is converted
	 */
	public static int oneByteToInt(byte[] data, int offset) {
		return (((int) data[offset]) & 0xFF);
	}

	/**
	 * Converts two bytes to integer.
	 *
	 * @param data       The byte array to use for converting
	 * @param offset     the offset that Converting start.
	 * @param byNegative if value is negative, byNegative is true, false otherwise.
	 * @return The integer which is converted
	 */
	public static int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
		return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF));
	}

	/**
	 * Converts two bytes to integer.
	 *
	 * @param data   The byte array to use for converting
	 * @param offset the offset that Converting start.
	 * @return The integer which is converted
	 */
	public static int twoBytesToInt(byte[] data, int offset) {
		return twoBytesToInt(data, offset, false);
	}

	/**
	 * Converts three bytes to integer.
	 *
	 * @param data   The byte array to use for converting.
	 * @param offset The offset that converting start.
	 * @return The integer which is converted.
	 */
	public static int threeBytesToInt(byte[] data, int offset) {
		return threeBytesToInt(data, offset, false);
	}

	/**
	 * Converts three bytes to integer.
	 *
	 * @param data       The byte array to use for converting.
	 * @param offset     The offset that converting start.
	 * @param byNegative if value is negative, byNegative is true, false otherwise.
	 * @return The integer which is converted.
	 */
	public static int threeBytesToInt(byte[] data, int offset, boolean byNegative) {
		return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 16) + ((((int) data[offset + 1]) & 0xFF) << 8)
				+ (((int) data[offset + 2]) & 0xFF));
	}

	/**
	 * Converts four bytes to integer.
	 *
	 * @param data       The byte array to use for converting.
	 * @param offset     The offset that converting start.
	 * @param byNegative if value is negative, byNegative is true, false otherwise.
	 * @return The integer which is converted.
	 */
	public static int fourBytesToInt(byte[] data, int offset, boolean byNegative) {
		return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 24) + ((((int) data[offset + 1]) & 0xFF) << 16)
				+ ((((int) data[offset + 2]) & 0xFF) << 8) + (((int) data[offset + 3]) & 0xFF));
	}

	/**
	 * Converts four bytes to integer.
	 *
	 * @param data   The byte array to use for converting.
	 * @param offset The offset that converting start.
	 * @return The integer which is converted.
	 */
	public static int fourBytesToInt(byte[] data, int offset) {
		return fourBytesToInt(data, offset, false);
	}

	/**
	 * Converts bytes to String.
	 *
	 * @param data   The byte array to user for converting.
	 * @param offset The offset that converting start.
	 * @param length The length that want to convert.
	 * @return The String which is converted.
	 */
	public static String byteArrayToString(byte[] data, int offset, int length) {
		String result = null;
		try {
			result = new String(data, offset, length, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Returns a String from the byte array.
	 *
	 * @param data byte array
	 * @return String
	 */
	public String getStringBYTEArray(byte[] data) {
		if (data == null) {
			return "";
		}
		return getStringBYTEArrayOffset(data, 0);
	}

	/**
	 * Returns a String from the byte array.
	 *
	 * @param data   byte array
	 * @param offset offset
	 * @return String
	 */
	public String getStringBYTEArrayOffset(byte[] data, int offset) {
		if (data == null) {
			return "";
		}

		StringBuffer buff = new StringBuffer(4096);
		for (int i = offset; i < offset + data.length; i++) {
			buff.append(getHexStringBYTE(data[i]));
			if (i != data.length - 1) {
				buff.append("");
			}
		}
		return buff.toString();
	}

	/**
	 * Returns a String from the byte array.
	 *
	 * @param b byte
	 * @return String
	 */
	public String getHexStringBYTE(byte b) {
		String str = "";

		// handle the case of -1
		if (b == -1) {
			str = "FF";
			return str;
		}

		int value = (((int) b) & 0xFF);

		if (value < 16) {
			// pad out string to make it look nice
			str = "0";
		}
		str += (Integer.toHexString(value)).toUpperCase();
		return str;
	}
}

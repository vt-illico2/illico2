/**
 * @(#)App.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

/**
 * implements Xlet of CYO Application.
 * @author zestyman
 * @version 1.1
 */
public class App implements Xlet, ApplicationConfig {
    
    public static String NAME = "Options";
    /**
     * The instance of XletContext.
     */
    private XletContext xletContext;

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional If unconditional is true when this method is called, requests by the Xlet to not enter the
     *            destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *             state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("CYO: destroyXlet");
        }
        FrameworkMain.getInstance().destroy();
        CyoController.getInstance().dispose();
    }

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param xContext The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext xContext) throws XletStateChangeException {
        this.xletContext = xContext;
        FrameworkMain.getInstance().init(this);
        if (Log.INFO_ON) {
            Log.printInfo("CYO: initXlet");
        }
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        if (Log.INFO_ON) {
            Log.printInfo("CYO: pauseXlet");
        }
        FrameworkMain.getInstance().pause();
        CyoController.getInstance().stop();
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("CYO: startXlet");
        }
        FrameworkMain.getInstance().start();
        CyoController.getInstance().start();
    }

    /**
     * Return a name of CYO.
     * @return The name of application
     */
    public String getApplicationName() {
        return NAME;
    }

    /**
     * Return a version of CYO.
     * @return The version of application
     */
    public String getVersion() {
        return "(R7.3).3.1";
    }

    /**
     * Return a XletContext of CYO application.
     * @return The XletContext for Application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

	public void init() {
		CyoController.getInstance().init(xletContext);
	}
}

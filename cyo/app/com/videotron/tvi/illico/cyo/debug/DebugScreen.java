/**
 * @(#)DebugScreen.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.debug;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Vector;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Service;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class DebugScreen extends Component {
    DVBColor background = new DVBColor(0, 0, 230, 128);
    Font font = FontResource.BLENDER.getFont(18);

    public DebugScreen() {
        setBounds(Constants.SCREEN_BOUNDS);
        setVisible(true);
    }

    public void paint(Graphics g) {
        g.setFont(font);
        g.setColor(background);
        g.fillRect(0, 0, 960, 540);

        Vector dataVec = DebugScreenManager.getInstance().getDataVector();

        int size = dataVec.size();

        for (int i = 0; i < size && i < 13; i++) {
            int step = i * 40;
            Object obj = dataVec.get(i);

            if (obj instanceof Subscription) {
                String title = null;
                String no = null;
                if (obj instanceof Channel) {
                    Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());
                    title = chInfo.getTitle();

                    no = chInfo.getHdPosition();
                    if (no == null) {
                        no = chInfo.getPosition();
                    }

                } else {
                    title = ((Subscription) obj).getTitle();
                }

                g.setColor(Color.cyan);
                if (title != null) {
                    g.drawString(title, 50, 30 + step);
                }

                g.setColor(Color.cyan);

                String info = makeString(((Subscription) obj), no);

                g.drawString(info, 300, 30 + step);

                Object parent = ((Subscription) obj).getParentObj();

                if (parent != null && parent instanceof Subscription) {
                    g.setColor(Color.PINK);
                    g.drawString(((Subscription) parent).getTitle(), 50, 50 + step);

                    g.setColor(Color.PINK);

                    String pInfo = makeString(((Subscription) parent), null);

                    g.drawString(pInfo, 300, 50 + step);
                }
            } else if (obj instanceof Group) {
                g.setColor(Color.red);
                if (obj != null) {
                    g.drawString(((Group) obj).getTitle(), 50, 30 + step);
                    g.drawString("Modifiable=" + ((Group) obj).getModifiable(), 300, 30 + step);
                }
            } else if (obj instanceof Service) {
                g.setColor(Color.blue);
                if (obj != null) {
                    g.drawString(((Service) obj).getTitle(), 50, 30 + step);
                }
            }
        }
    }

    private String makeString(Subscription obj, String no) {

        StringBuffer sb = new StringBuffer();

        sb.append("No=");
        if (no == null) {
            sb.append(obj.getNo());
        } else {
            sb.append(no);
        }
        sb.append("   ");
        sb.append("Modifiable = ");
        sb.append(obj.getModifiable());
        sb.append("   ");
        sb.append("SubscribedInAccount = ");
        sb.append(obj.getSubscribedInAccount());
        sb.append("   ");
        sb.append("SubscribedInSession = ");
        sb.append(obj.getSubscribedInSession());

        return sb.toString();
    }
}

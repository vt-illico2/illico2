package com.videotron.tvi.illico.cyo.debug;

import java.awt.event.KeyEvent;
import java.util.Vector;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class manager a data to be displayed and whether debug screen display or not.<br>
 * Debug screen is displayed by using a LayeredUI because debug screen is displayed on a normal screen.
 */
public final class DebugScreenManager implements LayeredKeyHandler {

    /** The LayerdUI. */
    LayeredUI ui;

    /** The LayeredWindow. */
    LayeredWindow window;

    /** The debug screen. */
    DebugScreen debugScreen = new DebugScreen();

    /** The data vector. */
    private Vector dataVector = new Vector();

    /** The instance for singleton. */
    private static DebugScreenManager instance = new DebugScreenManager();

    /**
     * Gets the single instance of DebugScreenManager.
     * @return single instance of DebugScreenManager
     */
    public static DebugScreenManager getInstance() {
        return instance;
    }

    /**
     * Instantiates a new debug screen manager.
     */
    private DebugScreenManager() {
        LayeredUI handler = WindowProperty.SDV_USER_ACTIVITY.createLayeredKeyHandler(this);
        handler.activate();

        window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        ui = WindowProperty.DEBUG_SCREEN.createLayeredWindow(window, Constants.SCREEN_BOUNDS);
        ui.deactivate();
        window.add(debugScreen);
    }

    /**
     * Hide a debug screen.
     */
    public void hide() {
        if (ui.isActive()) {
            ui.deactivate();
            dataVector.clear();
        }
    }

    /**
     * Toggle a dubug screen.
     */
    public void toggle() {
        if (ui.isActive()) {
            ui.deactivate();
        } else {
            ui.activate();
            window.repaint();
        }

        Log.printDebug("DebugScreenManager: ui active is " + ui.isActive());
    }

    /**
     * Adds the data which is displayed on debug screen.
     * @param obj the obj
     */
    public void addData(Object obj) {

        if (dataVector.contains(obj)) {
            dataVector.remove(obj);
        }
        dataVector.add(0, obj);
        dataVector.setSize(20);
    }

    /**
     * Gets the data vector.
     * @return the data vector
     */
    public Vector getDataVector() {
        return dataVector;
    }

    /**
     * Handle a key code.
     * @param event UserEvent
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKeyEvent(UserEvent event) {
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }

        int code = event.getCode();
        // for debug
        switch (code) {
        case KeyCodes.COLOR_D:
            if (FrameworkMain.getInstance().getHScene().isShowing() && Log.EXTRA_ON) {
                toggle();
            }
            return false;
        default:
            return false;
        }
    }
}

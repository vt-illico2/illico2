/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Product;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class MyPicksListRenderer extends BaseRenderer {

    private final int LINE_STEP = 32;
    private final int LIST_HEIGHT = 291;

    private Image lineImg;
    private Image focusImg;
    private Image delImg;
    private Image delFocImg;
//    private Image shTopImg;
//    private Image shBotImg;
//    private Image groupShTopImg;
//    private Image groupShBotImg;
    private Image arrowTopImg;
    private Image arrowBotImg;

    private Font f23 = FontResource.BLENDER.getFont(23);

    private Color blackColor = new Color(0, 0, 0);
    private Color unFocusedColor = new Color(230, 230, 230);
    private Color deleteDimColor = new Color(200, 200, 200);
    private Color focusColor = blackColor;
    private StringBuffer sf = new StringBuffer();
    
    private String deleteStr;

    protected void paint(Graphics g, UIComponent c) {

        ListUI parentUI = (ListUI) c;
        // g.translate(-56, -169);
        // 56, 169
        // 638 x 32 * N 개
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(57, 167, 636, 291);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Group) {
                g.setColor(blackColor);
                if (currentIdx - 1 == startIdx + parentUI.getFocus()) {
                    g.fillRect(56, 171 + heightOfDrawn, 638, 29);
                } else {
                    g.fillRect(56, 167 + heightOfDrawn, 638, 32);
                }
//                g.drawImage(groupShTopImg, 56, 160 + heightOfDrawn, c);
//                g.drawImage(groupShBotImg, 56, 200 + heightOfDrawn, c);
                g.setFont(f21);
                g.setColor(Color.white);                
                GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 375, 192 + heightOfDrawn);
                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Package) {

                int focusType = parentUI.getFocusType(currentIdx);

                if (heightOfDrawn + 46 < LIST_HEIGHT) {
                    g.drawImage(lineImg, 64, 200 + heightOfDrawn, c);
                }
                if (parentUI.hasFocus()) {

                    if (focusType == ListUI.FOCUS_ON_ME) {
                        g.drawImage(focusImg, 57, 164 + heightOfDrawn, c);
                        g.drawImage(delFocImg, 547, 172 + heightOfDrawn, parentUI);
                        g.setFont(f18);
                        g.setColor(focusColor);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);

                        g.setFont(f20);
                        g.setColor(focusColor);
                    } else {
                        g.drawImage(delImg, 547, 172 + heightOfDrawn, parentUI);
                        g.setFont(f18);
                        g.setColor(unFocusedColor);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                    }
                } else {
                    if (focusType == ListUI.FOCUS_ON_ME) {
                        g.drawImage(focusImg, 57, 164 + heightOfDrawn, c);
                        g.setFont(f18);
                        g.setColor(deleteDimColor);
                        g.drawImage(delImg, 547, 172 + heightOfDrawn, parentUI);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                        g.setFont(f20);
                        g.setColor(focusColor);
                    } else {
                        g.setFont(f18);
                        g.setColor(deleteDimColor);
                        g.drawImage(delImg, 547, 172 + heightOfDrawn, parentUI);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                        g.setColor(unFocusedColor);
                    }
                }

                sf.setLength(0);
                String title = ((Package) obj).getTitle();
                sf.append(title);
                sf.append(" (");
                sf.append(obj.getCyoObject().length);
                sf.append(" ");
                sf.append(BaseUI.getMenuText(KeyNames.CHANNELS));
                sf.append(") : ");
                CyoObject[] children = obj.getCyoObject();
                
                for (int i = 0; children != null && i < 3 && i < children.length; i++) {
                    if (children[i] instanceof Channel) {
                        if (i != 0) {
                            sf.append(", ");
                        }
                        Channel child = (Channel) children[i];
                        Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(child.getId());
                        sf.append(chInfo.getTitle());
                        sf.append(" (");
                        String no = chInfo.getHdPosition();
                        if (no == null) {
                            no = chInfo.getPosition();
                        }
                        sf.append(no);
                        sf.append(")");
                    }
                }
                title = TextUtil.shorten(sf.toString(), g.getFontMetrics(), 440);
                g.drawString(title, 75, 191 + heightOfDrawn);

                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Channel) {

                int focusType = parentUI.getFocusType(currentIdx);

                if (heightOfDrawn + 46 < LIST_HEIGHT) {
                    g.drawImage(lineImg, 64, 200 + heightOfDrawn, c);
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    g.drawImage(focusImg, 57, 164 + heightOfDrawn, c);

                    if (parentUI.hasFocus()) {
                        g.drawImage(delFocImg, 547, 172 + heightOfDrawn, parentUI);
                        g.setFont(f18);
                        g.setColor(focusColor);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                    } else {
                        g.drawImage(delImg, 547, 172 + heightOfDrawn, parentUI);
                        g.setFont(f18);
                        g.setColor(unFocusedColor);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                    }
                    g.setFont(f20);
                    g.setColor(focusColor);
                } else {
                    g.drawImage(delImg, 547, 172 + heightOfDrawn, parentUI);
                    g.setFont(f18);
                    g.setColor(unFocusedColor);
                    GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                }

                Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

                if (chInfo != null) {
                    String no = chInfo.getHdPosition();
                    if (no != null) {
                        g.drawString(no, 75, 191 + heightOfDrawn);
                    } else {
                        no = chInfo.getPosition();
                        g.drawString(no, 75, 191 + heightOfDrawn);
                    }

                    // Logo - get from epg via SharedMemory
//                    Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
                    Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
                    if (logoImg != null) {
                        g.drawImage(logoImg, 121, 173 + heightOfDrawn, 80, 24, c);
                    }

                    // title
                    String title = chInfo.getTitle();
                    title = TextUtil.shorten(title, g.getFontMetrics(), 300);
                    if (title != null) {
                        g.drawString(title, 220, 191 + heightOfDrawn);
                    }
                }

                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Product) {

                int focusType = parentUI.getFocusType(currentIdx);

                if (heightOfDrawn + 46 < LIST_HEIGHT) {
                    g.drawImage(lineImg, 64, 200 + heightOfDrawn, c);
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    g.drawImage(focusImg, 57, 164 + heightOfDrawn, c);

                    if (parentUI.hasFocus()) {
                        g.drawImage(delFocImg, 547, 172 + heightOfDrawn, parentUI);
                        g.setFont(f18);
                        g.setColor(focusColor);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                    } else {
                        g.drawImage(delImg, 547, 172 + heightOfDrawn, parentUI);
                        g.setFont(f18);
                        g.setColor(unFocusedColor);
                        GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                    }
                    g.setFont(f20);
                    g.setColor(focusColor);
                } else {
                    g.drawImage(delImg, 547, 172 + heightOfDrawn, parentUI);
                    g.setFont(f18);
                    g.setColor(unFocusedColor);
                    GraphicUtil.drawStringCenter(g, deleteStr, 615, 190 + heightOfDrawn);
                }

                // title
                String title = ((Product) obj).getTitle();
                if (title != null) {
                    g.drawString(title, 75, 191 + heightOfDrawn);
                }

                heightOfDrawn += LINE_STEP;
            }
            currentIdx++;
        }

        // shadow
//        if (parentUI.isDisplayTopDeco()) {
//            g.drawImage(shTopImg, 56, 166, c);
//        }
//
//        if (parentUI.isDisplayBottomDeco()) {
//            g.drawImage(shBotImg, 56, 395, c);
//        }

        g.setClip(beforeRec);

        // arrow.
        if (parentUI.isDisplayTopDeco()) {
            g.drawImage(arrowTopImg, 364, 158, c);
        }
        if (parentUI.isDisplayBottomDeco()) {
            g.drawImage(arrowBotImg, 364, 449, c);
        }
    }

    public void prepare(UIComponent c) {
        ListUI parentUI = (ListUI) c;
        loadImages(parentUI);

        deleteStr = BaseUI.getMenuText(KeyNames.DELETE);

        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        lineImg = ui.getImage("17_list_line.png");
        focusImg = ui.getImage("17_list_foc_dim.png");
        delImg = ui.getImage("17_del.png");
        delFocImg = ui.getImage("17_del_foc.png");
//        shTopImg = ui.getImage("17_sha_top.png");
//        shBotImg = ui.getImage("17_sha_bottom.png");
//        groupShTopImg = ui.getImage("17_list_glow_t.png");
//        groupShBotImg = ui.getImage("17_list_glow_b.png");
        arrowTopImg = ui.getImage("02_ars_t.png");
        arrowBotImg = ui.getImage("02_ars_b.png");
    }
}

/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class NewChannelListRenderer extends ListRenderer {

    private final int LINE_STEP = 32;
    private final int LIST_HEIGHT = 324;

    private Image lineImg;
    private Image focusImg;
    private Image focusDimImg;
    private Image iconCanadaImg;
    private Image iconHDImg;
    private Image iconHDFocImg;
    private Image iconIllicoImg;
    private Image iconFrImg;
    private Image iconEnImg;
    private Image iconFrFocImg;
    private Image iconEnFocImg;
//    private Image shTopImg;
//    private Image shBotImg;
//    private Image groupShTopImg;
//    private Image groupShBotImg;
    private Image addImg;
    private Image removeImg;
    private Image arrowTopImg;
    private Image arrowBotImg;

    private Font f23 = FontResource.BLENDER.getFont(23);

    private Color blackColor = new Color(0, 0, 0);
    private Color unFocusedColor = new Color(230, 230, 230);
    private Color focusBgColor = new Color(132, 132, 132);
    private Color modifyDimColor = new Color(200, 200, 200);
    private Color infoColor = new Color(39, 39, 39);
    private Color infoDimColor = new Color(160, 160, 160);
    private Color focusColor = blackColor;

    protected void paint(Graphics g, UIComponent c) {

        ListUI parentUI = (ListUI) c;
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(57, 145, 638, 324);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Group) {
                g.setColor(blackColor);
                if (currentIdx - 1 == startIdx + parentUI.getFocus()) {
                    g.fillRect(56, 147 + heightOfDrawn, 638, 29);
                } else {
                    g.fillRect(56, 143 + heightOfDrawn, 638, 32);
                }
//                g.drawImage(groupShTopImg, 56, 160 + heightOfDrawn, c);
//                g.drawImage(groupShBotImg, 56, 200 + heightOfDrawn, c);
                g.setFont(f23);
                g.setColor(Color.white);
                GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 375, 168 + heightOfDrawn);
                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Channel) {

                int focusType = parentUI.getFocusType(currentIdx);

                if (focusType == ListUI.FOCUS_ON_ME) {

                    g.setFont(f20);
                    g.setColor(focusColor);

                    if (parentUI.hasFocus()) {
                        g.drawImage(focusImg, 57, 139 + heightOfDrawn, parentUI);
                    } else {
                        g.drawImage(focusDimImg, 57, 139 + heightOfDrawn, parentUI);
                    }
                } else {
                    if (heightOfDrawn + 46 < LIST_HEIGHT) {
                        g.drawImage(lineImg, 64, 176 + heightOfDrawn, c);
                    }
                    g.setFont(f18);
                    g.setColor(unFocusedColor);
                }

                if (((Channel) obj).getSubscribedInSession() && !((Channel) obj).getSubscribedInAccount()) {
                    g.drawImage(addImg, 66, 148 + heightOfDrawn, c);
                } else if (!((Channel) obj).getSubscribedInSession() && ((Channel) obj).getSubscribedInAccount()) {
                    g.drawImage(removeImg, 66, 148 + heightOfDrawn, c);
                }

                Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

                if (chInfo != null) {
                    String no = chInfo.getHdPosition();
                    if (no != null) {
                        g.drawString(no, 100, 167 + heightOfDrawn);
                    } else {
                        no = chInfo.getPosition();
                        g.drawString(no, 100, 167 + heightOfDrawn);
                    }

                    // Logo - get from epg via SharedMemory
//                    Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
                    Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
                    if (logoImg != null) {
                        g.drawImage(logoImg, 149, 149 + heightOfDrawn, 80, 24, c);
                    }

                    // title
                    String title = chInfo.getTitle();
                    if (parentUI.isWithPrice()) {
                        title = TextUtil.shorten(title, g.getFontMetrics(), 175);
                    } else {
                        title = TextUtil.shorten(title, g.getFontMetrics(), 300);
                    }
                    if (title != null) {
                        g.drawString(title, 248, 167 + heightOfDrawn);
                    }

                    // R5 - SM5
                    // 5C
                    // VDTRMASTER-5899
                    if (chInfo.isChaineSurMesure5() && CyoDataManager.getInstance().needToDisplay5CBanner()) {
                    	Image fiveIcon = null;
                    	if (focusType == ListUI.FOCUS_ON_ME) {
                    		fiveIcon = icon5CFocusImg;
                    	} else {
                    		fiveIcon = icon5CImg;
                    	}
                    	g.drawImage(fiveIcon, 569, 154 + heightOfDrawn, c);
                    }
                    
                    // language
                    String language = chInfo.getLanguage();
                    Image langIcon = null;
                    if (language != null) {
                        if (language.equals(Constants.LANGUAGE_FRENCH)) {
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                langIcon = iconFrFocImg;
                            } else {
                                langIcon = iconFrImg;
                            }
                        } else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                langIcon = iconEnFocImg;
                            } else {
                                langIcon = iconEnImg;
                            }
                        }
                    }
                    g.drawImage(langIcon, 594, 154 + heightOfDrawn, c);

                    // R5 - SM5 remove
//                    // illico
//                    String illico = chInfo.getTelecino();
//                    if (illico != null) {
//                        if (!illico.equals("false")) { // not illico 1
//                            g.drawImage(iconIllicoImg, 601, 176 + heightOfDrawn, c);
//                        }
//                    }

                    // HD
                    boolean hd = chInfo.hasHDPosition();
                    Image hdIcon = null;
                    if (hd) {
                        if (focusType == ListUI.FOCUS_ON_ME) {
                            hdIcon = iconHDFocImg;
                        } else {
                            hdIcon = iconHDImg;
                        }
                        g.drawImage(hdIcon, 624, 154 + heightOfDrawn, c);
                    }
                    
                    // R5 - SM5
                    // pay
                    // 	VDTRMASTER-5894
                    if (chInfo.isChainePrestige() || ((Channel) obj).isSubscriptionTypeOfALaCarte()) {
                    	Image payIcon = null;
                    	 if (focusType == ListUI.FOCUS_ON_ME) {
                    		 payIcon = iconPayFocusImg;
                         } else {
                        	 payIcon = iconPayImg;
                         }
                         g.drawImage(payIcon, 654, 154 + heightOfDrawn, c);
                    }
                    

                    // R5 - SM5 remove
//                    // Canadian
//                    if (chInfo.getCanadian()) {
//                        g.drawImage(iconCanadaImg, 653, 176 + heightOfDrawn, c);
//                    }
                }

                heightOfDrawn += LINE_STEP;
            }
            currentIdx++;
        }

        // shadow
//        if (parentUI.isDisplayTopDeco()) {
//            g.drawImage(shTopImg, 56, 166, c);
//        }
//
//        if (parentUI.isDisplayBottomDeco()) {
//            g.drawImage(shBotImg, 56, 395, c);
//        }

        g.setClip(beforeRec);

        // arrow.
        if (parentUI.isDisplayTopDeco()) {
            g.drawImage(arrowTopImg, 364, 158, c);
        }
        if (parentUI.isDisplayBottomDeco()) {
            g.drawImage(arrowBotImg, 364, 449, c);
        }
    }

    public void prepare(UIComponent c) {
        ListUI parentUI = (ListUI) c;
        loadImages(parentUI);

        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        lineImg = ui.getImage("17_list_line.png");
        focusImg = ui.getImage("17_slist_foc.png");
        focusImg = ui.getImage("17_list_foc.png");
        focusDimImg = ui.getImage("17_list_foc_dim.png");
        iconCanadaImg = ui.getImage("icon_canada.png");
        iconHDImg = ui.getImage("icon_hd.png");
        iconHDFocImg = ui.getImage("icon_hd_foc.png");
        iconIllicoImg = ui.getImage("icon_illico.png");
        iconFrImg = ui.getImage("icon_fr.png");
        iconEnImg = ui.getImage("icon_en.png");
        iconFrFocImg = ui.getImage("icon_fr_foc.png");
        iconEnFocImg = ui.getImage("icon_en_foc.png");
//        shTopImg = ui.getImage("17_sha_top.png");
//        shBotImg = ui.getImage("17_sha_bottom.png");
//        groupShTopImg = ui.getImage("17_list_glow_t.png");
//        groupShBotImg = ui.getImage("17_list_glow_b.png");
        addImg = ui.getImage("17_plus.png");
        removeImg = ui.getImage("17_remove.png");
        arrowTopImg = ui.getImage("02_ars_t.png");
        arrowBotImg = ui.getImage("02_ars_b.png");
        
        // R5 - SM5
        icon5CImg = ui.getImage("icon_5.png");
        icon5CFocusImg = ui.getImage("icon_5_foc.png");
        iconPayImg = ui.getImage("icon_pay.png");
        iconPayFocusImg = ui.getImage("icon_pay_foc.png");
        
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
}

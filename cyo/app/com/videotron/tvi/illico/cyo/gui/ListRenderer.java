/**
 * @(#)ListRenderer.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChangeChannelSelectionUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ListRenderer extends BaseRenderer {

	private final int LINE_STEP = 32;
	private final int LIST_HEIGHT = 324;

	private Image lineImg;
	private Image checkboxImg;
	private Image checkboxFocusImg;
	private Image checkImg;
	private Image checkFocusImg;
	private Image focusBgTopImg;
	private Image focusBgMidImg;
	private Image focusBgBotImg;
	private Image focusImg;
	private Image focusBgTopDimImg;
	private Image focusBgMidDimImg;
	private Image focusBgBotDimImg;
	private Image focusDimImg;
	//    private Image explainShImg;
	private Image iconCanadaImg;
	private Image iconHDImg;
	private Image iconHDFocImg;
	private Image iconIllicoImg;
	private Image iconFrImg;
	private Image iconEnImg;
	private Image iconFrFocImg;
	private Image iconEnFocImg;
	//    private Image shTopImg;
	//    private Image shBotImg;
	//    private Image groupShTopImg;
	//    private Image groupShBotImg;
	private Image scrBarImg;
	private Image scrBgTopImg;
	private Image scrBgMidImg;
	private Image scrBgBotImg;
	private Image arrowTopImg;
	private Image arrowBotImg;

	protected Color bgColor = new Color(166, 166, 166);
	protected Color blackColor = new Color(0, 0, 0);
	protected Color unFocusedColor = new Color(230, 230, 230);
	protected Color focusBgColor = new Color(132, 132, 132);
	protected Color viewBtnColor = new Color(92, 92, 92);
	protected Color focusColor = blackColor;

	private int iconGap = 0;

	//    private String subFixForPrice;

	// R5 - SM5
	protected Image icon5CImg;
	protected Image icon5CFocusImg;
	protected Image iconPayImg;
	protected Image iconPayFocusImg;

	// Bundling
	protected Image iconUHDImg;
	protected Image iconUHDFocusImg;

	protected void paint(Graphics g, UIComponent c) {

		ListUI parentUI = (ListUI) c;
		// g.translate(-56, -169);
		// 56, 169
		// 638 x 32 * N 개
		Rectangle beforeRec = g.getClipBounds();
		g.clipRect(57, 145, 638, 324);
		int startIdx = parentUI.getStartIdx();
		int currentIdx = startIdx;
		int heightOfDrawn = 0;
		for (; heightOfDrawn < LIST_HEIGHT; ) {
			CyoObject obj = parentUI.getCyoObject(currentIdx);

			if (obj == null) {
				break;
			}

			if (obj instanceof Group) {
				g.setColor(blackColor);
				g.fillRect(56, 143 + heightOfDrawn, 638, 32);
				//                g.drawImage(groupShTopImg, 56, 160 + heightOfDrawn, c);
				//                g.drawImage(groupShBotImg, 56, 200 + heightOfDrawn, c);
				g.setFont(f21);
				g.setColor(Color.white);
				GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 375, 168 + heightOfDrawn);
				heightOfDrawn += LINE_STEP;
			} else if (obj instanceof Package) {

				int focusType = parentUI.getFocusType(currentIdx);
				int descripHeight = 0;

				if (focusType == ListUI.FOCUS_ON_ME || focusType == ListUI.FOCUS_ON_CHILD) {
					descripHeight = getDescriptionHeight(parentUI, heightOfDrawn);
				} else {
					if (heightOfDrawn + 46 < LIST_HEIGHT) {
						g.drawImage(lineImg, 64, 178 + heightOfDrawn, c);
					}
					g.setFont(f18);
					g.setColor(unFocusedColor);

					g.drawImage(checkboxImg, 71, 154 + heightOfDrawn, c); // 76, 176
					if (((Package) obj).getSubscribedInSession()) {
						g.drawImage(checkImg, 74, 153 + heightOfDrawn, c);
					}

					String title = ((Package) obj).getTitle();
					int count = 0;

					// Log.printInfo("((Package) obj).getChannel() = " + ((Package) obj).getCyoObject());
					if (((Package) obj).getCyoObject() != null) {
						count = (((Package) obj).getCyoObject()).length;
						if (count != 0) {
							title = title + " (" + count + " " + dataCenter.getString("menu.channels") + ")";
						}
					}

					if (parentUI.isWithPrice()) {
						title = TextUtil.shorten(title, g.getFontMetrics(), 510);
					} else {
						title = TextUtil.shorten(title, g.getFontMetrics(), 575);
					}
					g.drawString(title, 100, 168 + heightOfDrawn);

					String price = ((Package) obj).getPrice();
					// VDTRMASTER-5895
					if (price != null && parentUI.isWithPrice() &&
							(((Package) obj).isSubscriptionTypeOfALaCarte()
									|| CyoController.getInstance().getCurrentId()
									!= BaseUI.UI_CHANGE_CHANNEL_SELECTION)) {
						GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
					} else if (CyoController.getInstance().getCurrentId() == BaseUI.UI_PACKAGE_CHANNEL_LIST) { // VDTRMASTER-5965
						if (CyoDataManager.getInstance().isIncludedInExtraList(((Package) obj))) {
							if (price != null) {
								GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
							} else {
								Image payIcon = null;
								if (focusType == ListUI.FOCUS_ON_ME) {
									payIcon = iconPayFocusImg;
								} else {
									payIcon = iconPayImg;
								}
								g.drawImage(payIcon, 614 + iconGap, 156 + heightOfDrawn, c);
							}
						}
					}
				}

				if (focusType == ListUI.FOCUS_ON_ME || focusType == ListUI.FOCUS_ON_CHILD) {
					heightOfDrawn = descripHeight;
				} else {
					heightOfDrawn += LINE_STEP;
				}
			} else if (obj instanceof Channel) {
				CyoObject parentObj = obj.getParentObj();

				int focusType = parentUI.getFocusType(currentIdx);
				int descHeight = 0;

				if (!((Subscription) obj).getModifiable().equals("0")) {
					if (focusType == ListUI.FOCUS_ON_ME) {
						descHeight = getDescriptionHeight(parentUI, heightOfDrawn);
					} else {
						if (heightOfDrawn + 46 < LIST_HEIGHT) {
							g.drawImage(lineImg, 64, 178 + heightOfDrawn, c);
						}

						g.setFont(f20);
						g.setColor(viewBtnColor);

						Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

						if (chInfo != null) {
							// Logo - get from epg via SharedMemory
							// Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
							Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
							if (logoImg != null) {
								g.drawImage(logoImg, 149, 150 + heightOfDrawn, 80, 24, c);
							}

							// title
							String title = chInfo.getTitle();
							String subFixStr = BaseUI.getExplainText(KeyNames.UNSELECT_SUBFIX_FOR_A_LA_CARTE);
							title = TextUtil.replace(subFixStr, "[CHN]", title);
							title = TextUtil.shorten(title, g.getFontMetrics(), 400);
							if (title != null) {
								g.drawString(title, 247, 169 + heightOfDrawn);
							}
						}
					}
				} else {
					if (focusType == ListUI.FOCUS_ON_ME) {
						descHeight = getDescriptionHeight(parentUI, heightOfDrawn);
					} else {
						if (heightOfDrawn + 46 < LIST_HEIGHT) {
							g.drawImage(lineImg, 64, 178 + heightOfDrawn, c);
						}

						// cImg = checkImg;
						g.setFont(f18);
						g.setColor(unFocusedColor);

						g.drawImage(checkboxImg, 71, 154 + heightOfDrawn, c); // 76, 176
						if (((Channel) obj).getSubscribedInSession()) {
							g.drawImage(checkImg, 74, 153 + heightOfDrawn, c);
						}

						Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

						if (chInfo != null) {
							String no = chInfo.getHdPosition();
							if (no != null) {
								g.drawString(no, 99, 168 + heightOfDrawn);
							} else {
								no = chInfo.getPosition();
								g.drawString(no, 99, 168 + heightOfDrawn);
							}

							// Logo - get from epg via SharedMemory
							// Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
							Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
							if (logoImg != null) {
								g.drawImage(logoImg, 149, 150 + heightOfDrawn, 80, 24, c);
							}

							// title
							String title = chInfo.getTitle();
							if (parentUI.isWithPrice()) {
								title = TextUtil.shorten(title, g.getFontMetrics(), 220);
							} else {
								title = TextUtil.shorten(title, g.getFontMetrics(), 290);
							}
							if (title != null) {
								g.drawString(title, 247, 169 + heightOfDrawn);
							}

							// R5-SM5
							// 5C
							// VDTRMASTER-5899
							if (chInfo.isChaineSurMesure5() && CyoDataManager.getInstance().needToDisplay5CBanner()) {
								Image iconFive = null;
								if (focusType == ListUI.FOCUS_ON_ME) {
									iconFive = icon5CFocusImg;
								} else {
									iconFive = icon5CImg;
								}
								g.drawImage(iconFive, 533 + iconGap, 156 + heightOfDrawn, c);
							}

							// language
							String language = chInfo.getLanguage();
							Image langIcon = null;
							if (language != null) {
								if (language.equals(Constants.LANGUAGE_FRENCH)) {
									if (focusType == ListUI.FOCUS_ON_ME) {
										langIcon = iconFrFocImg;
									} else {
										langIcon = iconFrImg;
									}
								} else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
									if (focusType == ListUI.FOCUS_ON_ME) {
										langIcon = iconEnFocImg;
									} else {
										langIcon = iconEnImg;
									}
								}
							}
							g.drawImage(langIcon, 557 + iconGap, 156 + heightOfDrawn, c);

							// R5-SM5 remove
							//                            // illico
							//                            String illico = chInfo.getTelecino();
							//                            if (illico != null) {
							//                                if (!illico.equals("false")) { // not illico 1
							//                                    g.drawImage(iconIllicoImg, 469 + iconGap, 176 + heightOfDrawn, c);
							//                                }
							//                            }

							// HD
							boolean hd = chInfo.hasHDPosition();
							Image hdIcon = null;
							if (hd) {
								if (focusType == ListUI.FOCUS_ON_ME) {
									hdIcon = iconHDFocImg;
								} else {
									hdIcon = iconHDImg;
								}
								g.drawImage(hdIcon, 586 + iconGap, 156 + heightOfDrawn, c);
							}

							// R5-SM5
							// Bunding remove
							//                            if (chInfo.isChainePrestige()) {
							//                            	Image payIcon = null;
							//                            	if (focusType == ListUI.FOCUS_ON_ME) {
							//                            		payIcon = iconPayFocusImg;
							//                                } else {
							//                                	payIcon = iconPayImg;
							//                                }
							//                                g.drawImage(payIcon, 521 + iconGap, 178 + heightOfDrawn, c);
							//                            }

							// R5-SM5 remove
							//                            // Canadian
							//                            if (chInfo.getCanadian()) {
							//                                g.drawImage(iconCanadaImg, 521 + iconGap, 176 + heightOfDrawn, c);
							//                            }
						}

						String price = ((Channel) obj).getPrice();
						// VDTRMASTER-5895
						if (price != null && parentUI.isWithPrice() &&
								(((Channel) obj).isSubscriptionTypeOfALaCarte()
										|| CyoController.getInstance().getCurrentId()
										!= BaseUI.UI_CHANGE_CHANNEL_SELECTION)) {
							GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
						} else if (CyoController.getInstance().getCurrentId() == BaseUI.UI_PACKAGE_CHANNEL_LIST) { // VDTRMASTER-5965
							if (CyoDataManager.getInstance().isIncludedInExtraList(((Channel) obj))) {
								if (price != null) {
									GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
								} else {
									Image payIcon = null;
									if (focusType == ListUI.FOCUS_ON_ME) {
										payIcon = iconPayFocusImg;
									} else {
										payIcon = iconPayImg;
									}
									g.drawImage(payIcon, 614 + iconGap, 156 + heightOfDrawn, c);
								}
							}
						} else if (chInfo.isChainePrestige()) {
							if (price != null) {
								GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
							} else {
								Image payIcon = null;
								if (focusType == ListUI.FOCUS_ON_ME) {
									payIcon = iconPayFocusImg;
								} else {
									payIcon = iconPayImg;
								}
								g.drawImage(payIcon, 614 + iconGap, 156 + heightOfDrawn, c);
							}
						}
					}
				}

				if (focusType == ListUI.FOCUS_ON_ME) {
					heightOfDrawn = descHeight;
				} else {
					heightOfDrawn += LINE_STEP;
				}
			} else if (obj instanceof Product) {
				int cPosX = 74;

				int focusType = parentUI.getFocusType(currentIdx);
				int descHeight = 0;

				if (focusType == ListUI.FOCUS_ON_ME) {
					descHeight = getDescriptionHeight(parentUI, heightOfDrawn);
				} else {
					if (heightOfDrawn + 46 < LIST_HEIGHT) {
						g.drawImage(lineImg, 64, 178 + heightOfDrawn, c);
					}
					// cImg = checkImg;
					g.setFont(f18);
					g.setColor(unFocusedColor);

					g.drawImage(checkboxImg, 71, 154 + heightOfDrawn, c); // 76, 176
					if (((Product) obj).getSubscribedInSession()) {
						g.drawImage(checkImg, cPosX, 153 + heightOfDrawn, c);
					}

					// title
					String title = ((Product) obj).getTitle();
					if (title != null) {
						g.drawString(title, 100, 168 + heightOfDrawn);
					}

					String price = ((Product) obj).getPrice();
					if (price != null && parentUI.isWithPrice()) {
						GraphicUtil.drawStringRight(g, "+ " + price + " $", 671, 168 + heightOfDrawn);
					}
				}

				if (focusType == ListUI.FOCUS_ON_ME) {
					heightOfDrawn = descHeight;
				} else {
					heightOfDrawn += LINE_STEP;
				}
			}
			currentIdx++;
		}

		// shadow
		//        if (parentUI.isDisplayTopDeco()) {
		//            g.drawImage(shTopImg, 56, 166, c);
		//        }
		//
		//        if (parentUI.isDisplayBottomDeco()) {
		//            g.drawImage(shBotImg, 56, 395, c);
		//        }

		startIdx = parentUI.getStartIdx();
		currentIdx = startIdx;
		heightOfDrawn = 0;

		for (; heightOfDrawn < LIST_HEIGHT; ) {
			CyoObject obj = parentUI.getCyoObject(currentIdx);

			if (obj == null) {
				break;
			}

			if (obj instanceof Group) {
				heightOfDrawn += LINE_STEP;
			} else if (obj instanceof Package) {
				Image cBoxImg;
				Image cImg;

				int focusType = parentUI.getFocusType(currentIdx);
				int descripHeight = 131;

				if (focusType == ListUI.FOCUS_ON_ME || focusType == ListUI.FOCUS_ON_CHILD) {

					if (parentUI.hasFocus()) {
						cBoxImg = checkboxFocusImg;
						// cImg = checkWithFocusImg;
						cImg = checkFocusImg;
						descripHeight = drawDescription(g, parentUI, heightOfDrawn, focusType);

						if (focusType == ListUI.FOCUS_ON_ME) {

							g.drawImage(focusImg, 57, 142 + heightOfDrawn, c);

						} else {
							g.setColor(focusBgColor);
							g.fillRect(61, 149 + heightOfDrawn, 628, 32);
						}
					} else {
						cBoxImg = checkboxFocusImg;
						cImg = checkFocusImg;
						descripHeight = drawDescription(g, parentUI, heightOfDrawn, focusType);

						g.setColor(focusBgColor);
						g.fillRect(61, 149 + heightOfDrawn, 628, 32);
					}

					g.setFont(f20);
					g.setColor(blackColor);

					g.drawImage(cBoxImg, 71, 154 + heightOfDrawn, c); // 76, 176
					if (((Package) obj).getSubscribedInSession()) {
						g.drawImage(cImg, 74, 153 + heightOfDrawn, c);
					}

					String title = ((Package) obj).getTitle();
					int count = 0;
					// Log.printInfo("((Package) obj).getChannel() = " + ((Package) obj).getCyoObject());
					if (((Package) obj).getCyoObject() != null) {
						count = (((Package) obj).getCyoObject()).length;
						if (count != 0) {
							title = title + " (" + count + " " + dataCenter.getString("menu.channels") + ")";
						}
					}
					if (parentUI.isWithPrice()) {
						title = TextUtil.shorten(title, g.getFontMetrics(), 510);
					} else {
						title = TextUtil.shorten(title, g.getFontMetrics(), 575);
					}
					g.drawString(title, 100, 168 + heightOfDrawn);

					String price = ((Package) obj).getPrice();
					// VDTRMASTER-5895
					if (price != null && parentUI.isWithPrice() &&
							(((Package) obj).isSubscriptionTypeOfALaCarte()
									|| CyoController.getInstance().getCurrentId()
									!= BaseUI.UI_CHANGE_CHANNEL_SELECTION)) {
						GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
					} else if (CyoController.getInstance().getCurrentId() == BaseUI.UI_PACKAGE_CHANNEL_LIST) { // VDTRMASTER-5965
						if (CyoDataManager.getInstance().isIncludedInExtraList(((Package) obj))) {
							if (price != null) {
								GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
							} else {
								Image payIcon = null;
								if (focusType == ListUI.FOCUS_ON_ME) {
									payIcon = iconPayFocusImg;
								} else {
									payIcon = iconPayImg;
								}
								g.drawImage(payIcon, 614 + iconGap, 156 + heightOfDrawn, c);
							}
						}
					}
				}

				if (focusType == ListUI.FOCUS_ON_ME) {
					heightOfDrawn = descripHeight;
				} else {
					heightOfDrawn += LINE_STEP;
				}
			} else if (obj instanceof Channel) {

				int focusType = parentUI.getFocusType(currentIdx);
				int descHeight = 99;

				if (!((Subscription) obj).getModifiable().equals("0")) {
					if (focusType == ListUI.FOCUS_ON_ME) {

						descHeight = drawDescription(g, parentUI, heightOfDrawn, focusType);

						if (parentUI.hasFocus()) {
							g.drawImage(focusImg, 57, 142 + heightOfDrawn, parentUI);
						} else {
							g.setColor(focusBgColor);
							g.fillRect(61, 147 + heightOfDrawn, 628, 32);
						}

						g.setFont(f20);
						g.setColor(blackColor);

						Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

						if (chInfo != null) {
							// Logo - get from epg via SharedMemory
							// Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
							Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
							if (logoImg != null) {
								g.drawImage(logoImg, 149, 150 + heightOfDrawn, 80, 24, c);
							}

							// title
							String title = chInfo.getTitle();
							String subFixStr = BaseUI.getExplainText(KeyNames.UNSELECT_SUBFIX_FOR_A_LA_CARTE);
							title = TextUtil.replace(subFixStr, "[CHN]", title);
							title = TextUtil.shorten(title, g.getFontMetrics(), 400);
							if (title != null) {
								g.drawString(title, 247, 169 + heightOfDrawn);
							}
						}
					}
				} else {

					if (focusType == ListUI.FOCUS_ON_ME) {

						descHeight = drawDescription(g, parentUI, heightOfDrawn, focusType);

						if (parentUI.hasFocus()) {
							g.drawImage(focusImg, 57, 142 + heightOfDrawn, parentUI);
						} else {
							g.setColor(focusBgColor);
							g.fillRect(61, 147 + heightOfDrawn, 628, 32);
						}

						g.drawImage(checkboxFocusImg, 71, 154 + heightOfDrawn, c); // 76, 176
						if (((Channel) obj).getSubscribedInSession()) {
							g.drawImage(checkFocusImg, 74, 153 + heightOfDrawn, c);
						}

						g.setFont(f20);
						g.setColor(blackColor);

						Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

						if (chInfo != null) {
							String no = chInfo.getHdPosition();
							if (no != null) {
								g.drawString(no, 99, 168 + heightOfDrawn);
							} else {
								no = chInfo.getPosition();
								g.drawString(no, 99, 168 + heightOfDrawn);
							}

							// Logo - get from epg via SharedMemory
							// Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
							Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
							if (logoImg != null) {
								g.drawImage(logoImg, 149, 150 + heightOfDrawn, 80, 24, c);
							}

							// title
							String title = chInfo.getTitle();
							if (parentUI.isWithPrice()) {
								title = TextUtil.shorten(title, g.getFontMetrics(), 220);
							} else {
								title = TextUtil.shorten(title, g.getFontMetrics(), 290);
							}
							if (title != null) {
								g.drawString(title, 247, 169 + heightOfDrawn);
							}

							// R5-SM5
							// 5C
							// VDTRMASTER-5899
							if (chInfo.isChaineSurMesure5() && CyoDataManager.getInstance().needToDisplay5CBanner()) {
								Image iconFive = null;
								if (focusType == ListUI.FOCUS_ON_ME) {
									iconFive = icon5CFocusImg;
								} else {
									iconFive = icon5CImg;
								}
								g.drawImage(iconFive, 533 + iconGap, 156 + heightOfDrawn, c);
							}

							// language
							String language = chInfo.getLanguage();
							Image langIcon = null;
							if (language != null) {
								if (language.equals(Constants.LANGUAGE_FRENCH)) {
									if (focusType == ListUI.FOCUS_ON_ME) {
										langIcon = iconFrFocImg;
									} else {
										langIcon = iconFrImg;
									}
								} else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
									if (focusType == ListUI.FOCUS_ON_ME) {
										langIcon = iconEnFocImg;
									} else {
										langIcon = iconEnImg;
									}
								}
							}
							g.drawImage(langIcon, 557 + iconGap, 156 + heightOfDrawn, c);

							// R5-SM5 remove
							//                            // illico
							//                            String illico = chInfo.getTelecino();
							//                            if (illico != null) {
							//                                if (!illico.equals("false")) { // not illico 1
							//                                    g.drawImage(iconIllicoImg, 469 + iconGap, 176 + heightOfDrawn, c);
							//                                }
							//                            }

							// HD
							boolean hd = chInfo.hasHDPosition();
							Image hdIcon = null;
							if (hd) {
								if (focusType == ListUI.FOCUS_ON_ME) {
									hdIcon = iconHDFocImg;
								} else {
									hdIcon = iconHDImg;
								}
								g.drawImage(hdIcon, 586 + iconGap, 156 + heightOfDrawn, c);
							}

							// R5-SM5
							// bundling remove
							//                            if (chInfo.isChainePrestige()) {
							//                            	Image payIcon = null;
							//                            	if (focusType == ListUI.FOCUS_ON_ME) {
							//                            		payIcon = iconPayFocusImg;
							//                                } else {
							//                                	payIcon = iconPayImg;
							//                                }
							//                                g.drawImage(payIcon, 521 + iconGap, 178 + heightOfDrawn, c);
							//                            }

							// R5-SM5 remove
							//                            // Canadian
							//                            if (chInfo.getCanadian()) {
							//                                g.drawImage(iconCanadaImg, 521 + iconGap, 176 + heightOfDrawn, c);
							//                            }
						}

						String price = ((Channel) obj).getPrice();
						// VDTRMASTER-5895
						if (price != null && parentUI.isWithPrice() &&
								(((Channel) obj).isSubscriptionTypeOfALaCarte()
										|| CyoController.getInstance().getCurrentId()
										!= BaseUI.UI_CHANGE_CHANNEL_SELECTION)) {
							GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
						} else if (CyoController.getInstance().getCurrentId() == BaseUI.UI_PACKAGE_CHANNEL_LIST) { // VDTRMASTER-5965
							if (CyoDataManager.getInstance().isIncludedInExtraList(((Channel) obj))) {
								if (price != null) {
									GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
								} else {
									Image payIcon = null;
									if (focusType == ListUI.FOCUS_ON_ME) {
										payIcon = iconPayFocusImg;
									} else {
										payIcon = iconPayImg;
									}
									g.drawImage(payIcon, 614 + iconGap, 156 + heightOfDrawn, c);
								}
							}
						} else if (chInfo.isChainePrestige()) {
							if (price != null) {
								GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
							} else {
								Image payIcon = null;
								if (focusType == ListUI.FOCUS_ON_ME) {
									payIcon = iconPayFocusImg;
								} else {
									payIcon = iconPayImg;
								}
								g.drawImage(payIcon, 614 + iconGap, 156 + heightOfDrawn, c);
							}
						}
					}
				}

				if (focusType == ListUI.FOCUS_ON_ME) {
					heightOfDrawn = descHeight;
				} else {
					heightOfDrawn += LINE_STEP;
				}
			} else if (obj instanceof Product) {

				int focusType = parentUI.getFocusType(currentIdx);
				int descHeight = 0;

				if (focusType == ListUI.FOCUS_ON_ME) {

					descHeight = drawDescription(g, parentUI, heightOfDrawn, focusType);

					if (parentUI.hasFocus()) {
						g.drawImage(focusImg, 57, 142 + heightOfDrawn, parentUI);
					} else {
						g.setColor(focusBgColor);
						g.fillRect(61, 147 + heightOfDrawn, 628, 32);

					}

					g.drawImage(checkboxFocusImg, 71, 154 + heightOfDrawn, c); // 76, 176
					if (((Product) obj).getSubscribedInSession()) {
						g.drawImage(checkFocusImg, 74, 153 + heightOfDrawn, c);
					}

					g.setFont(f20);
					g.setColor(focusColor);

					// title
					String title = ((Product) obj).getTitle();
					if (title != null) {
						g.drawString(title, 100, 169 + heightOfDrawn);
					}

					String price = ((Product) obj).getPrice();
					if (price != null && parentUI.isWithPrice()) {
						GraphicUtil.drawStringRight(g, "+ " + price + " $", 677, 168 + heightOfDrawn);
					}
				}

				if (focusType == ListUI.FOCUS_ON_ME) {
					heightOfDrawn = descHeight;
				} else {
					heightOfDrawn += LINE_STEP;
				}
			}
			currentIdx++;
		}
		g.setClip(beforeRec);
		// g.translate(56, 169);

		// arrow.
		if (parentUI.isDisplayTopDeco()) {
			g.drawImage(arrowTopImg, 364, 134, c);
		}
		if (parentUI.isDisplayBottomDeco()) {
			g.drawImage(arrowBotImg, 364, 461, c);
		}
	}

	//    private int drawChannel(Graphics g, ListUI parentUI, CyoObject obj, int currentIdx, int heightOfDrawn) {
	//
	//        if (obj instanceof Channel) {
	//
	//            CyoObject[] objs = obj.getParentObj().getCyoObject();
	//            Channel lastChannel = (Channel) objs[objs.length - 1];
	//            boolean isLast = ((Channel) obj).getId().equals(lastChannel.getId());
	//            int focusType = parentUI.getFocusType(currentIdx);
	//            int descHeight = 0;
	//            g.setFont(f18);
	//
	//            if (focusType != ListUI.FOCUS_NONE) {
	//
	//                if (focusType == ListUI.FOCUS_ON_PARENT || focusType == ListUI.FOCUS_ON_CHILD) {
	//                    g.setColor(focusBgColor);
	//                    g.fillRect(61, 167 + heightOfDrawn, 628, 36);
	//                }
	//                g.setColor(focusColor);
	//                if (isLast) {
	//                    if (parentUI.hasFocus()) {
	//                        g.drawImage(focusBgBotImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    } else {
	//                        g.drawImage(focusBgBotDimImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    }
	//                } else {
	//                    if (parentUI.hasFocus()) {
	//                        g.drawImage(focusBgMidImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    } else {
	//                        g.drawImage(focusBgMidDimImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    }
	//                }
	//
	//                if (focusType == ListUI.FOCUS_ON_ME) {
	//                    descHeight = drawDescription(g, parentUI, heightOfDrawn, isLast);
	//
	//                    if (parentUI.hasFocus()) {
	//                        g.drawImage(focusImg, 57, 163 + heightOfDrawn, parentUI);
	//                    } else {
	//                        g.drawImage(focusDimImg, 57, 163 + heightOfDrawn, parentUI);
	//                    }
	//                    g.setFont(f20);
	//                    g.setColor(focusColor);
	//                } else {
	//                    if (heightOfDrawn + 46 < LIST_HEIGHT) {
	//                        g.drawImage(groupLineImg, 98, 198 + heightOfDrawn, parentUI);
	//                    }
	//                    g.setFont(f18);
	//                }
	//            } else {
	//                if (heightOfDrawn + 46 < LIST_HEIGHT) {
	//                    g.drawImage(groupLineImg, 98, 198 + heightOfDrawn, parentUI);
	//                }
	//                g.setColor(unFocusedColor);
	//            }
	//
	//            if (((Channel) obj).getSubscribedInSession()) {
	//                CyoObject parentObj = obj.getParentObj();
	//
	//                if (focusType == ListUI.FOCUS_ON_ME) {
	//                    if (((Package) parentObj).getModifiable().equals("0")) {
	//                        g.drawImage(checkBoxSmallFocusImg, 103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                        g.drawImage(checkSmallFocusImg, 105, 177 + heightOfDrawn, parentUI);
	//                    } else {
	//                        g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkBoxSmallFocusImg
	//                                : checkBoxSmallFocusImgIn, 103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                        g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkSmallFocusImg
	//                                : checkSmallFocusImgIn, 105, 177 + heightOfDrawn, parentUI);
	//                    }
	//                } else {
	//                    if (((Package) parentObj).getModifiable().equals("0")) {
	//
	//                    } else {
	//                        g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkBoxSmallImg
	//                                : checkBoxSmallImgIn, 103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                        g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkSmallImg : checkSmallImgIn,
	//                                105, 177 + heightOfDrawn, parentUI);
	//                    }
	//                }
	//            } else {
	//                g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkBoxSmallImg : checkBoxSmallImgIn, 103,
	//                        178 + heightOfDrawn, parentUI); // 76, 176
	//            }
	//
	//            Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());
	//
	//            if (chInfo != null) {
	//                String no = chInfo.getHdPosition();
	//                if (no != null) {
	//                    g.drawString(no, 123, 190 + heightOfDrawn);
	//                } else {
	//                    no = chInfo.getPosition();
	//                    g.drawString(no, 123, 190 + heightOfDrawn);
	//                }
	//
	//                // Logo - get from epg via SharedMemory
	//                // Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
	//                Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
	//
	//                if (logoImg != null) {
	//                    g.drawImage(logoImg, 166, 170 + heightOfDrawn, 80, 24, parentUI);
	//                }
	//
	//                // title
	//                String title = chInfo.getTitle();
	//                if (parentUI.isWithPrice()) {
	//                    title = TextUtil.shorten(title, g.getFontMetrics(), 175);
	//                } else {
	//                    title = TextUtil.shorten(title, g.getFontMetrics(), 300);
	//                }
	//                if (title != null) {
	//                    g.drawString(title, 256, 190 + heightOfDrawn);
	//                }
	//
	//                // R5-SM5
	//                // 5C
	//                if (chInfo.isChaineSurMesure5()) {
	//                	Image iconFive = null;
	//                	if (focusType == ListUI.FOCUS_ON_ME) {
	//                		iconFive = icon5CFocusImg;
	//                    } else {
	//                    	iconFive = icon5CImg;
	//                    }
	//                	g.drawImage(iconFive, 436 + iconGap, 178 + heightOfDrawn, parentUI);
	//                }
	//
	//                // language
	//                String language = chInfo.getLanguage();
	//                Image langIcon = null;
	//                if (language != null) {
	//                    if (language.equals(Constants.LANGUAGE_FRENCH)) {
	//                        if (focusType == ListUI.FOCUS_ON_ME) {
	//                            langIcon = iconFrFocImg;
	//                        } else {
	//                            langIcon = iconFrImg;
	//                        }
	//                    } else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
	//                        if (focusType == ListUI.FOCUS_ON_ME) {
	//                            langIcon = iconEnFocImg;
	//                        } else {
	//                            langIcon = iconEnImg;
	//                        }
	//                    }
	//                }
	//                g.drawImage(langIcon, 461 + iconGap, 178 + heightOfDrawn, parentUI);
	//
	//                // R5-SM5 remove
	////                // illico
	////                String illico = chInfo.getTelecino();
	////                if (illico != null) {
	////                    if (!illico.equals("false")) { // not illico 1
	////                        g.drawImage(iconIllicoImg, 469 + iconGap, 176 + heightOfDrawn, c);
	////                    }
	////                }
	//
	//                // HD
	//                boolean hd = chInfo.hasHDPosition();
	//                Image hdIcon = null;
	//                if (hd) {
	//                    if (focusType == ListUI.FOCUS_ON_ME) {
	//                        hdIcon = iconHDFocImg;
	//                    } else {
	//                        hdIcon = iconHDImg;
	//                    }
	//                    g.drawImage(hdIcon, 491 + iconGap, 178 + heightOfDrawn, parentUI);
	//                }
	//
	//                // R5-SM5
	//                if (chInfo.isChainePrestige()) {
	//                	Image payIcon = null;
	//                	if (focusType == ListUI.FOCUS_ON_ME) {
	//                		payIcon = iconPayFocusImg;
	//                    } else {
	//                    	payIcon = iconPayImg;
	//                    }
	//                    g.drawImage(payIcon, 521 + iconGap, 178 + heightOfDrawn, parentUI);
	//                }
	//
	//                // R5-SM5 remove
	////                // Canadian
	////                if (chInfo.getCanadian()) {
	////                    g.drawImage(iconCanadaImg, 521 + iconGap, 176 + heightOfDrawn, c);
	////                }
	//
	//                String price = ((Channel) obj).getPrice();
	//                if (price != null && parentUI.isWithPrice()) {
	//                    GraphicUtil.drawStringRight(g, price + subFixForPrice, 671, 190);
	//                }
	//            }
	//
	//            if (focusType == ListUI.FOCUS_ON_ME) {
	//                heightOfDrawn = descHeight;
	//            } else {
	//                heightOfDrawn += LINE_STEP;
	//            }
	//        }
	//        return heightOfDrawn;
	//    }
	//
	//    private int drawChannel2(Graphics g, ListUI parentUI, CyoObject obj, int currentIdx, int heightOfDrawn) {
	//
	//        if (obj instanceof Channel) {
	//
	//            CyoObject[] objs = obj.getParentObj().getCyoObject();
	//            Channel lastChannel = (Channel) objs[objs.length - 1];
	//            CyoObject parentObj = obj.getParentObj();
	//            boolean isLast = ((Channel) obj).getId().equals(lastChannel.getId());
	//            int focusType = parentUI.getFocusType(currentIdx);
	//            int descHeight = 0;
	//            g.setFont(f18);
	//
	//            if (focusType != ListUI.FOCUS_NONE) {
	//
	//                if (focusType == ListUI.FOCUS_ON_PARENT || focusType == ListUI.FOCUS_ON_CHILD) {
	//                    g.setColor(focusBgColor);
	//                    g.fillRect(61, 167 + heightOfDrawn, 628, 36);
	//                }
	//                g.setColor(focusColor);
	//                if (isLast) {
	//                    if (parentUI.hasFocus()) {
	//                        g.drawImage(focusBgBotImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    } else {
	//                        g.drawImage(focusBgBotDimImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    }
	//                } else {
	//                    if (parentUI.hasFocus()) {
	//                        g.drawImage(focusBgMidImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    } else {
	//                        g.drawImage(focusBgMidDimImg, 57, 167 + heightOfDrawn, 636, 36, parentUI);
	//                    }
	//                }
	//
	//                if (focusType == ListUI.FOCUS_ON_ME) {
	//                    descHeight = drawDescription(g, parentUI, heightOfDrawn, isLast);
	//
	//                    if (parentUI.hasFocus()) {
	//                        g.drawImage(focusImg, 57, 163 + heightOfDrawn, parentUI);
	//                    } else {
	//                        g.drawImage(focusDimImg, 57, 163 + heightOfDrawn, parentUI);
	//                    }
	//                    g.setFont(f20);
	//                    g.setColor(focusColor);
	//                } else {
	//                    if (heightOfDrawn + 46 < LIST_HEIGHT) {
	//                        g.drawImage(groupLineImg, 98, 198 + heightOfDrawn, parentUI);
	//                    }
	//                    g.setFont(f18);
	//                }
	//
	//                if (((Channel) obj).getSubscribedInSession()) {
	//                    if (focusType == ListUI.FOCUS_ON_ME) {
	//                        if (((Package) parentObj).getModifiable().equals("0")) {
	//                            g.drawImage(checkBoxSmallFocusImg, 103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                            g.drawImage(checkSmallFocusImg, 105, 177 + heightOfDrawn, parentUI);
	//                        } else {
	//                            g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkBoxSmallFocusImg
	//                                    : checkBoxSmallFocusImgIn, 103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                            g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkSmallFocusImg
	//                                    : checkSmallFocusImgIn, 105, 177 + heightOfDrawn, parentUI);
	//                        }
	//                    } else {
	//                        if (((Package) parentObj).getModifiable().equals("0")) {
	//                            g.drawImage(checkBoxSmallImg, 103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                            g.drawImage(checkSmallImg, 105, 177 + heightOfDrawn, parentUI);
	//                        } else {
	//                            g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkBoxSmallImg
	//                                    : checkBoxSmallImgIn, 103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                            g.drawImage(
	//                                    (((Channel) obj).getModifiable()).equals("0") ? checkSmallImg : checkSmallImgIn,
	//                                    105, 177 + heightOfDrawn, parentUI);
	//                        }
	//                    }
	//                } else {
	//                    g.drawImage((((Channel) obj).getModifiable()).equals("0") ? checkBoxSmallImg : checkBoxSmallImgIn,
	//                            103, 178 + heightOfDrawn, parentUI); // 76, 176
	//                }
	//
	//                Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());
	//
	//                if (chInfo != null) {
	//                    String no = chInfo.getHdPosition();
	//                    if (no != null) {
	//                        g.drawString(no, 123, 190 + heightOfDrawn);
	//                    } else {
	//                        no = chInfo.getPosition();
	//                        g.drawString(no, 123, 190 + heightOfDrawn);
	//                    }
	//
	//                    // Logo - get from epg via SharedMemory
	//                    // Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
	//                    Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
	//                    if (logoImg != null) {
	//                        g.drawImage(logoImg, 166, 170 + heightOfDrawn, 80, 24, parentUI);
	//                    }
	//
	//                    // title
	//                    String title = chInfo.getTitle();
	//                    if (parentUI.isWithPrice()) {
	//                        title = TextUtil.shorten(title, g.getFontMetrics(), 175);
	//                    } else {
	//                        title = TextUtil.shorten(title, g.getFontMetrics(), 300);
	//                    }
	//                    if (title != null) {
	//                        g.drawString(title, 256, 190 + heightOfDrawn);
	//                    }
	//
	//                 // R5-SM5
	//                    // 5C
	//                    if (chInfo.isChaineSurMesure5()) {
	//                    	Image iconFive = null;
	//                    	if (focusType == ListUI.FOCUS_ON_ME) {
	//                    		iconFive = icon5CFocusImg;
	//                        } else {
	//                        	iconFive = icon5CImg;
	//                        }
	//                    	g.drawImage(iconFive, 436 + iconGap, 178 + heightOfDrawn, parentUI);
	//                    }
	//
	//                    // language
	//                    String language = chInfo.getLanguage();
	//                    Image langIcon = null;
	//                    if (language != null) {
	//                        if (language.equals(Constants.LANGUAGE_FRENCH)) {
	//                            if (focusType == ListUI.FOCUS_ON_ME) {
	//                                langIcon = iconFrFocImg;
	//                            } else {
	//                                langIcon = iconFrImg;
	//                            }
	//                        } else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
	//                            if (focusType == ListUI.FOCUS_ON_ME) {
	//                                langIcon = iconEnFocImg;
	//                            } else {
	//                                langIcon = iconEnImg;
	//                            }
	//                        }
	//                    }
	//                    g.drawImage(langIcon, 461 + iconGap, 178 + heightOfDrawn, parentUI);
	//
	//                    // R5-SM5 remove
	////                    // illico
	////                    String illico = chInfo.getTelecino();
	////                    if (illico != null) {
	////                        if (!illico.equals("false")) { // not illico 1
	////                            g.drawImage(iconIllicoImg, 469 + iconGap, 176 + heightOfDrawn, c);
	////                        }
	////                    }
	//
	//                    // HD
	//                    boolean hd = chInfo.hasHDPosition();
	//                    Image hdIcon = null;
	//                    if (hd) {
	//                        if (focusType == ListUI.FOCUS_ON_ME) {
	//                            hdIcon = iconHDFocImg;
	//                        } else {
	//                            hdIcon = iconHDImg;
	//                        }
	//                        g.drawImage(hdIcon, 491 + iconGap, 178 + heightOfDrawn, parentUI);
	//                    }
	//
	//                    // R5-SM5
	//                    if (chInfo.isChainePrestige()) {
	//                    	Image payIcon = null;
	//                    	if (focusType == ListUI.FOCUS_ON_ME) {
	//                    		payIcon = iconPayFocusImg;
	//                        } else {
	//                        	payIcon = iconPayImg;
	//                        }
	//                        g.drawImage(payIcon, 521 + iconGap, 178 + heightOfDrawn, parentUI);
	//                    }
	//
	//                    // R5-SM5 remove
	////                    // Canadian
	////                    if (chInfo.getCanadian()) {
	////                        g.drawImage(iconCanadaImg, 521 + iconGap, 176 + heightOfDrawn, c);
	////                    }
	//
	//                    String price = ((Channel) obj).getPrice();
	//                    if (price != null && parentUI.isWithPrice()) {
	//                        GraphicUtil.drawStringRight(g, price + subFixForPrice, 671, 190);
	//                    }
	//                }
	//            }
	//
	//            if (focusType == ListUI.FOCUS_ON_ME) {
	//                heightOfDrawn = descHeight;
	//            } else {
	//                heightOfDrawn += LINE_STEP;
	//            }
	//        }
	//        return heightOfDrawn;
	//    }

	public int drawDescription(Graphics g, ListUI parentUI, int heightOfDrawn, int focusType) {
		String[] desc = parentUI.getDesc();
		CyoObject currentObj = parentUI.getCurrentCyoObject();

		if (desc == null || !((Subscription) currentObj).getModifiable().equals("0")) {
			if (parentUI.hasFocus()) {
				if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
					g.setColor(bgColor);
					g.fillRect(61, 147 + heightOfDrawn, 628, 94);
					g.drawImage(focusBgMidImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
					g.drawImage(focusBgTopImg, 57, 145 + heightOfDrawn, parentUI);
					g.drawImage(focusBgBotImg, 57, 209 + heightOfDrawn, parentUI);

					if (focusType == ListUI.FOCUS_ON_ME) {
						// View channel included
						g.setColor(viewBtnColor);
						g.fillRect(98, 197 + heightOfDrawn, 180, 26);
						g.setColor(hisOverColor);
						g.setFont(f17);
					} else {
						// View channel included
						g.setColor(C249195000);
						g.fillRect(98, 197 + heightOfDrawn, 180, 26);
						g.setColor(btnColor);
						g.setFont(f17);
					}

					// View channel included
					GraphicUtil.drawStringCenter(g, BaseUI.getMenuText(KeyNames.BTN_VIEW_CHANNEL_INCLUDED), 188,
							215 + heightOfDrawn);

					return heightOfDrawn + 99;
				}
			} else {
				if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
					g.setColor(bgColor);
					g.fillRect(61, 147 + heightOfDrawn, 628, 94);
					g.drawImage(focusBgMidDimImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
					g.drawImage(focusBgTopDimImg, 57, 145 + heightOfDrawn, parentUI);
					g.drawImage(focusBgBotDimImg, 57, 209 + heightOfDrawn, parentUI);

					// View channel included
					g.setColor(viewBtnColor);
					g.fillRect(98, 197 + heightOfDrawn, 180, 26);
					g.setColor(hisOverColor);
					g.setFont(f17);
					GraphicUtil.drawStringCenter(g, BaseUI.getMenuText(KeyNames.BTN_VIEW_CHANNEL_INCLUDED), 188,
							215 + heightOfDrawn);
					return heightOfDrawn + 99;
				} else if (!((Subscription) currentObj).getModifiable().equals("0")) { // case of unselected channel
					// VDTRMASTER-5968
					g.setColor(focusBgColor);
					g.fillRect(57, 147 + heightOfDrawn, 636, 32);
				} else {
					g.setColor(bgColor);
					g.fillRect(61, 147 + heightOfDrawn, 628, 94);
					g.drawImage(focusBgMidDimImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
					g.drawImage(focusBgTopDimImg, 57, 145 + heightOfDrawn, parentUI);
					g.drawImage(focusBgBotDimImg, 57, 209 + heightOfDrawn, parentUI);
				}
			}
			return heightOfDrawn + 32;
		}

		if (parentUI.hasFocus()) {
			if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
				if (focusType == ListUI.FOCUS_ON_ME) {
					g.setColor(bgColor);
					g.fillRect(61, 147 + heightOfDrawn, 628, 126);
					g.drawImage(focusBgMidImg, 57, 149 + heightOfDrawn, 636, 92, parentUI);
					g.drawImage(focusBgTopImg, 57, 145 + heightOfDrawn, parentUI);
					g.drawImage(focusBgBotImg, 57, 241 + heightOfDrawn, parentUI);

					// View channel included
					g.setColor(viewBtnColor);
					g.fillRect(98, 240 + heightOfDrawn, 180, 26);
					g.setColor(hisOverColor);
					g.setFont(f17);
				} else {
					g.setColor(bgColor);
					g.fillRect(61, 147 + heightOfDrawn, 628, 126);
					g.drawImage(focusBgMidDimImg, 57, 149 + heightOfDrawn, 636, 92, parentUI);
					g.drawImage(focusBgTopDimImg, 57, 145 + heightOfDrawn, parentUI);
					g.drawImage(focusBgBotDimImg, 57, 241 + heightOfDrawn, parentUI);
					// View channel included
					g.setColor(C249195000);
					g.fillRect(98, 240 + heightOfDrawn, 180, 26);
					g.setColor(btnColor);
					g.setFont(f17);
				}

				// View channel included
				GraphicUtil.drawStringCenter(g, BaseUI.getMenuText(KeyNames.BTN_VIEW_CHANNEL_INCLUDED), 188,
						258 + heightOfDrawn);
			} else {
				if (focusType == ListUI.FOCUS_ON_ME) {
					g.setColor(bgColor);
					g.fillRect(61, 147 + heightOfDrawn, 628, 94);
					g.drawImage(focusBgMidImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
					g.drawImage(focusBgTopImg, 57, 145 + heightOfDrawn, parentUI);
					g.drawImage(focusBgBotImg, 57, 209 + heightOfDrawn, parentUI);
				} else {
					g.setColor(bgColor);
					g.fillRect(61, 147 + heightOfDrawn, 628, 94);
					g.drawImage(focusBgMidDimImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
					g.drawImage(focusBgTopDimImg, 57, 145 + heightOfDrawn, parentUI);
					g.drawImage(focusBgBotDimImg, 57, 209 + heightOfDrawn, parentUI);
				}
			}
		} else {
			if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
				g.setColor(bgColor);
				g.fillRect(61, 147 + heightOfDrawn, 628, 126);
				g.drawImage(focusBgMidDimImg, 57, 149 + heightOfDrawn, 636, 92, parentUI);
				g.drawImage(focusBgTopDimImg, 57, 145 + heightOfDrawn, parentUI);
				g.drawImage(focusBgBotDimImg, 57, 241 + heightOfDrawn, parentUI);

				// View channel included
				g.setColor(viewBtnColor);
				g.fillRect(98, 240 + heightOfDrawn, 180, 26);
				g.setColor(hisOverColor);
				g.setFont(f17);
				GraphicUtil.drawStringCenter(g, BaseUI.getMenuText(KeyNames.BTN_VIEW_CHANNEL_INCLUDED), 188,
						258 + heightOfDrawn);
			} else {
				g.setColor(bgColor);
				g.fillRect(61, 147 + heightOfDrawn, 628, 94);
				g.drawImage(focusBgMidDimImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
				g.drawImage(focusBgTopDimImg, 57, 145 + heightOfDrawn, parentUI);
				g.drawImage(focusBgBotDimImg, 57, 209 + heightOfDrawn, parentUI);
			}
		}

		g.setFont(f14d);
		g.setColor(blackColor);
		int descStartIdx = parentUI.getDescStartIdx();
		int count = 0;
		for (int i = descStartIdx; i < descStartIdx + 3; i++) {
			if (i < desc.length && desc[i] != null) {
				g.drawString(desc[i], 99, 199 + heightOfDrawn + count * 17);
				count++;
			}
		}

		if (desc.length > 3) {
			//            if (descStartIdx + 3 < desc.length) {
			//                g.drawImage(explainShImg, 61, 238 + heightOfDrawn, parentUI);
			//            } else if (descStartIdx > 0) {
			//                g.drawImage(explainShImg, 61, 193 + heightOfDrawn, parentUI);
			//            }

			// scroll
			g.drawImage(scrBgTopImg, 668, 189 + heightOfDrawn, 7, 10, parentUI);
			g.drawImage(scrBgMidImg, 668, 199 + heightOfDrawn, 7, 25, parentUI);
			g.drawImage(scrBgBotImg, 668, 224 + heightOfDrawn, parentUI);

			int step = parentUI.getScrollGap();
			g.drawImage(scrBarImg, 665, 189 + heightOfDrawn + step, parentUI);
		}
		if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
			heightOfDrawn += 131;
		} else {
			heightOfDrawn += 99;
		}

		return heightOfDrawn;
	}

	/**
	 * Return a height of description but do not draw a description.
	 * @param parentUI
	 * @param heightOfDrawn
	 * @return
	 */
	public int getDescriptionHeight(ListUI parentUI, int heightOfDrawn) {
		String[] desc = parentUI.getDesc();
		CyoObject currentObj = parentUI.getCurrentCyoObject();

		if (desc == null || !((Subscription) currentObj).getModifiable().equals("0")) {
			if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
				return heightOfDrawn + 99;
			}
			return heightOfDrawn + 32;
		}

		if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
			heightOfDrawn += 131;
		} else {
			heightOfDrawn += 99;
		}
		return heightOfDrawn;
	}

	public void prepare(UIComponent c) {
		ListUI parentUI = (ListUI) c;
		loadImages(parentUI);

		if (parentUI.isWithPrice()) {
			iconGap = 0;
		} else {
			iconGap = 70;
		}

		//        subFixForPrice = BaseUI.getMenuText(KeyNames.LIST_MONTH);//s + "*";
		super.prepare(c);
	}

	protected void loadImages(BaseUI ui) {
		lineImg = ui.getImage("17_list_line.png");

		checkboxImg = ui.getImage("check_box.png");
		checkboxFocusImg = ui.getImage("check_box_foc.png");
		checkImg = ui.getImage("check.png");
		checkFocusImg = ui.getImage("check_foc.png");

		focusBgTopImg = ui.getImage("17_rim01_t.png");
		focusBgMidImg = ui.getImage("17_rim01_m.png");
		focusBgBotImg = ui.getImage("17_rim01_b.png");
		focusImg = ui.getImage("17_list_foc.png");
		focusBgTopDimImg = ui.getImage("17_rim01_dim_t.png");
		focusBgMidDimImg = ui.getImage("17_rim01_dim_m.png");
		focusBgBotDimImg = ui.getImage("17_rim01_dim_b.png");
		focusDimImg = ui.getImage("17_list_foc_dim.png");
		//        explainShImg = ui.getImage("17_list_sha_b.png");
		iconCanadaImg = ui.getImage("icon_canada.png");
		iconHDImg = ui.getImage("icon_hd.png");
		iconHDFocImg = ui.getImage("icon_hd_foc.png");
		iconIllicoImg = ui.getImage("icon_illico.png");
		iconFrImg = ui.getImage("icon_fr.png");
		iconEnImg = ui.getImage("icon_en.png");
		iconFrFocImg = ui.getImage("icon_fr_foc.png");
		iconEnFocImg = ui.getImage("icon_en_foc.png");
		//        shTopImg = ui.getImage("17_sha_top.png");
		//        shBotImg = ui.getImage("17_sha_bottom.png");
		//        groupShTopImg = ui.getImage("17_list_glow_t.png");
		//        groupShBotImg = ui.getImage("17_list_glow_b.png");
		scrBarImg = ui.getImage("scr_bar.png");
		scrBgTopImg = ui.getImage("scrbg_up.png");
		scrBgMidImg = ui.getImage("scr_m.png");
		scrBgBotImg = ui.getImage("scrbg_dn.png");
		arrowTopImg = ui.getImage("02_ars_t.png");
		arrowBotImg = ui.getImage("02_ars_b.png");

		// R5 - SM5
		icon5CImg = ui.getImage("icon_5.png");
		icon5CFocusImg = ui.getImage("icon_5_foc.png");
		iconPayImg = ui.getImage("icon_pay.png");
		iconPayFocusImg = ui.getImage("icon_pay_foc.png");

		iconUHDImg = ui.getImage("icon_uhd_ppv.png");
		iconUHDFocusImg = ui.getImage("icon_uhd_ppv_foc.png");

		FrameworkMain.getInstance().getImagePool().waitForAll();
	}
}

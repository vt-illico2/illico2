/**
 * @(#)ValidationErrorRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ValidationErrorPopupUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ValidationErrorRenderer extends BaseRenderer {

//    private Image bgTopImg;
//    private Image bgMidImg;
//    private Image bgBotImg;
    private Image titleImg;
//    private Image highImg;
    private Image btnFocusImg;
    private Image boxImg;
    private Image iconOrangeImg;
    private Image iconCanadaImg;

    private Color bgColor = new Color(33, 33, 33);

    private String btnOK;
    private String noti;
    private String iconStr;

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        btnOK = BaseUI.getMenuText(KeyNames.BTN_OK);
        noti = BaseUI.getMenuText(KeyNames.NOTIFICATION);
        iconStr = BaseUI.getMenuText(KeyNames.ICON_CANADIAN);
        super.prepare(c);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.cyo.gui.BaseRenderer#loadImages(com.videotron.tvi.illico.cyo.ui.BaseUI)
     */
    protected void loadImages(BaseUI ui) {
//        bgTopImg = ui.getImage("pglow610_t.png");
//        bgMidImg = ui.getImage("pglow610_m.png");
//        bgBotImg = ui.getImage("pglow610_b.png");
        titleImg = ui.getImage("07_respop_title.png");
//        highImg = ui.getImage("07_respop_high.png");
        btnFocusImg = ui.getImage("05_focus.png");
        boxImg = ui.getImage("17_pop_box.png");
        iconOrangeImg = ui.getImage("icon_noti_or.png");
        iconCanadaImg = ui.getImage("icon_canada.png");
    }

    protected void paint(Graphics g, UIComponent c) {
        g.setColor(dimmedBgColor);
        g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

//        g.drawImage(bgMidImg, 174, 87, 610, 350, c);
//        g.drawImage(bgBotImg, 174, 437, c);
//        g.drawImage(bgTopImg, 174, 44, c);
        g.setColor(bgColor);
        g.fillRect(204, 74, 550, 387);
        g.drawImage(titleImg, 203, 112, c);
//        g.drawImage(highImg, 204, 74, c);
        g.drawImage(boxImg, 222, 162, c);

        g.setColor(btnColor);
        g.setFont(f18);
        g.drawImage(btnFocusImg, 400, 414, c);
        GraphicUtil.drawStringCenter(g, btnOK, 476, 436);
        
        g.setFont(f24);
        g.setColor(popupTitleColor);
        int x = GraphicUtil.drawStringCenter(g, noti, 489, 101);
        
        g.drawImage(iconOrangeImg, x - 30, 81, c);
        ValidationErrorPopupUI vep = (ValidationErrorPopupUI) c;
        
        if(vep.iconViewing) {
            g.drawImage(iconCanadaImg, 239, 358, c);
            g.setColor(titleColor);
            g.setFont(f18);
            g.drawString(iconStr, 263, 372);
        }

    }
}

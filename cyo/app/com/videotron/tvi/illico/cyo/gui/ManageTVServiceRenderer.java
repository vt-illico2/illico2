/**
 * @(#)ManageTVServiceRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.ui.ManageTVServiceUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ManageTVServiceRenderer extends BaseRenderer {

    private Image listBgImg;

    private Image btnTopImg;
    private Image btnMidImg;
    private Image btnBotImg;
    private Image btnLineImg;
    private Image btnFocImg;
    private Image btnArrowImg;

    private Image infoBgImg;

    private String titleStr;
    private String[] infoStr = new String[2];
    private String[] btnStr = new String[3];

    protected void paint(Graphics g, UIComponent c) {
        ManageTVServiceUI ui = (ManageTVServiceUI) c;
        super.paint(g, c);

        // list bg
        g.drawImage(listBgImg, 2, 134, c);

        g.setFont(f28);
        g.setColor(titleColor);
        g.drawString(titleStr, TITLE_X, TITLE_Y);

        // button bg
        g.drawImage(btnTopImg, 626, 341, c);
        g.drawImage(btnMidImg, 626, 384, c);
        g.drawImage(btnBotImg, 626, 420, 284, 40, c);
//        g.drawImage(btnGlowImg, 625, 439, c);
//        g.drawImage(btnShImg, 579, 451, c);
        g.drawImage(btnLineImg, 641, 381, c);
        g.drawImage(btnLineImg, 641, 419, c);

        // bill and list button
        g.drawImage(infoBgImg, 626, 211, c);

        g.setFont(f18);
        g.setColor(btnColor);
        for (int i = 0; i < infoStr.length; i++) {
            int step = i * 42;
            if (ui.getState() == ManageTVServiceUI.STATE_MENU && ui.getFocus() == i) {
                g.setColor(C246193001);
                g.fillRect(642, 227 + step, 252, 36);
                g.setColor(btnColor);
            } else {
                g.setColor(dimmedMenuInColor);
                g.fillRect(642, 227 + step, 252, 36);
                g.setColor(dimmedMenuColor);
            }
            GraphicUtil.drawStringCenter(g, infoStr[i], 768, 250 + step);
        }

        // button
        for (int i = 0; i < btnStr.length; i++) {
            int step = i * 39;
            if (ui.getState() == ManageTVServiceUI.STATE_BUTTON && ui.getBtnFocus() == i) {
                g.drawImage(btnFocImg, 624, 341 + step, c);
                g.setColor(btnColor);
                g.setFont(f21);
                g.drawString(btnStr[i], 671, 368 + step);
            } else {
                g.drawImage(btnArrowImg, 649, 359 + step, c);
                g.setColor(dimmedMenuColor);
                g.setFont(f18);
                g.drawString(btnStr[i], 671, 367 + step);
            }
        }
        
        ListUI listUI = ui.getList();
        if (listUI != null) {
            listUI.paint(g);
        }
    }

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        titleStr = BaseUI.getMenuText(KeyNames.MANAGE_TV_SERVICE);
        infoStr[0] = BaseUI.getMenuText(KeyNames.SEE_DETAILS);
        infoStr[1] = BaseUI.getMenuText(KeyNames.LIST_OF_CHANNELS);

        btnStr[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
        btnStr[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        btnStr[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);
        super.prepare(c);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.cyo.gui.BaseRenderer#loadImages(com.videotron.tvi.illico.cyo.ui.BaseUI)
     */
    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg2.png");

        btnTopImg = ui.getImage("01_acbg_1st.png");
        btnMidImg = ui.getImage("01_acbg_2nd.png");
        btnBotImg = ui.getImage("01_acbg_m.png");
        btnLineImg = ui.getImage("01_acline.png");
        btnFocImg = ui.getImage("02_detail_bt_foc.png");
        btnArrowImg = ui.getImage("02_detail_ar.png");

        infoBgImg = ui.getImage("17_mang_btbox.png");
    }

}

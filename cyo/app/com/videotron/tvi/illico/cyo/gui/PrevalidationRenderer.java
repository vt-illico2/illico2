/**
 * @(#)InitialStateRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class PrevalidationRenderer extends BaseRenderer {

    private Image boxFocusImg;
//    private Image boxShImg;
    private Image boxBgImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image arrowLeftImg;
    private Image arrowRightImg;

    private String[] title;
    private String modify;
    private String manage;

    private String[] leftSubTitle;
    private String[] rightSubTitle;


    /**
     * Prepare a text and images.
     */
    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        String tmp = BaseUI.getMenuText(KeyNames.PREVALIDATION_TITLE);
        title = TextUtil.split(tmp, fm26, 850, '|');

        modify = BaseUI.getMenuText(KeyNames.BTN_MODIFY);
        manage = BaseUI.getMenuText(KeyNames.BTN_CONFIRM2);

        tmp = BaseUI.getMenuText(KeyNames.PREVALIDATION_LEFT);
        leftSubTitle = TextUtil.split(tmp, fm22, 295, '|');

        tmp = BaseUI.getMenuText(KeyNames.PREVALIDATION_RIGHT);
        rightSubTitle = TextUtil.split(tmp, fm22, 295, '|');

        super.prepare(c);
    }

    /**
     * Paint a Items.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        g.setFont(f26);
        g.setColor(Color.white);

        g.drawString(title[0], 60, 98);
        g.drawString(title[1], 60, 116);

        // bg of two box
        g.drawImage(boxBgImg, 85, 154, c);
        g.drawImage(boxBgImg, 514, 154, c);
//        g.drawImage(boxShImg, 38, 425, c);
//        g.drawImage(boxShImg, 467, 425, c);

        // left items in focus
        g.setColor(titleColor);
        g.setFont(f22);
        for (int i = 0; i < leftSubTitle.length; i++) {
            int step = i * 28;
            g.drawString(leftSubTitle[i], 115, 222 + step);
        }

        // right items in focus
        for (int i = 0; i < rightSubTitle.length; i++) {
            int step = i * 28;
            g.drawString(rightSubTitle[i], 547, 222 + step);
        }

        // focus & arrow & buttons
        if (c.getFocus() == 0) {
            g.drawImage(boxFocusImg, 81, 150, c);
            g.drawImage(arrowRightImg, 454, 284, c);

            // buttons
            g.drawImage(btnFocusImg, 187, 378, c);
            g.drawImage(btnDimImg, 618, 378, c);
        } else {
            g.drawImage(boxFocusImg, 510, 150, c);
            g.drawImage(arrowLeftImg, 491, 284, c);

            // buttons
            g.drawImage(btnDimImg, 187, 378, c);
            g.drawImage(btnFocusImg, 618, 378, c);
        }

        g.setColor(btnColor);
        g.setFont(f18);

        GraphicUtil.drawStringCenter(g, modify, 266, 400);
        GraphicUtil.drawStringCenter(g, manage, 697, 400);

    }

    protected final void loadImages(BaseUI ui) {
        boxFocusImg = ui.getImage("17_box_foc.png");
//        boxShImg = ui.getImage("17_box_shadow.png");
        boxBgImg = ui.getImage("17_box.png");
        arrowLeftImg = ui.getImage("02_ars_l.png");
        arrowRightImg = ui.getImage("02_ars_r.png");
        btnFocusImg = ui.getImage("05_focus.png");
        btnDimImg = ui.getImage("05_focus_dim.png");
    }
}

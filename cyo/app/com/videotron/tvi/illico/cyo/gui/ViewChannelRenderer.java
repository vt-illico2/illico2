/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ViewChannelUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.cyo.data.obj.Package;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ViewChannelRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image menuLineImg;
//    private Image menuGlowImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image countBgImg;

    private String title;
    private String explain;
    private String[] buttons;

    /**
     * 
     */
    public ViewChannelRenderer() {
        buttons = new String[2];

    }

    public void prepare(UIComponent c) {
        ViewChannelUI ui = (ViewChannelUI) c;

        loadImages((BaseUI) c);

        Package curPackage = ui.getCurrentPackage();
        StringBuffer sf = new StringBuffer();

        sf.append("'");
        sf.append(curPackage.getTitle());
        sf.append("' | ");
        sf.append(curPackage.getPrice());
        sf.append(BaseUI.getPriceFreq(curPackage.getPricefreq()));

        title = sf.toString();
//        explain = BaseUI.getExplainText(KeyNames.EXPLAIN);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_BACK);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        ViewChannelUI ui = (ViewChannelUI) c;

        g.drawImage(listBgImg, 50, 138, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
//        g.drawString(explain, EXPLAIN_X, EXPLAIN_Y);

        // list
        ListUI currentList = ui.getCurrentList();
        if (currentList != null) {
            
            // sort
            g.setFont(f17);
            GraphicUtil.drawStringRight(g, ui.getSortString(), SORTED_BY_X, SORTED_BY_Y);
            
            currentList.paint(g);
        }

        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getButtonFocus() == i && ui.getState() == ViewChannelUI.STATE_BUTTON) {
                g.drawImage(btnFocusImg, 717, 383 + step, c);
                g.setColor(btnColor);
            } else {
                g.drawImage(btnDimImg, 718, 384 + step, c);
                g.setColor(dimmedMenuColor);
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 404 + step);
        }
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        menuLineImg = ui.getImage("17_menu_line.png");
//        menuGlowImg = ui.getImage("17_menu_glow.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        countBgImg = ui.getImage("17_cus_tab.png");
    }
}

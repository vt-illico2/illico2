/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChangeChannelSelectionUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.ui.PopularPackagesUI;
import com.videotron.tvi.illico.cyo.ui.SelfServeUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class PopularPackagesRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image menuLineImg;
//    private Image menuGlowImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image countBgImg;
    private Image btnGrayDimImg;
    private String title;
    private String[] explains;
    private String menu;
    private String[] buttons;
    
    private String fund;

    /**
     * 
     */
    public PopularPackagesRenderer() {
        buttons = new String[3];

    }

    public void prepare(UIComponent c) {
        PopularPackagesUI ui = (PopularPackagesUI) c;

        loadImages((BaseUI) c);

        title = BaseUI.getMenuText(KeyNames.POPULAR_PACKAGES);
        String temp = BaseUI.getExplainText(KeyNames.POPULAR_PACKAGES);
        explains = TextUtil.split(temp, fm17, 840);
        fund = BaseUI.getExplainText(KeyNames.FUND);
        menu = BaseUI.getMenuText(KeyNames.LIST_OF_CHANNELS);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        PopularPackagesUI ui = (PopularPackagesUI) c;

        g.drawImage(listBgImg, 3, 158, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        if (explains != null) {
            for (int i = 0; i < explains.length; i++) {
                int step = i * 20;
                g.drawString(explains[i], EXPLAIN_X, EXPLAIN_Y + step);
            }
        }

        // list
        ListUI currentList = ui.getCurrentList();
        if (currentList != null) {
            currentList.paint(g);
        }

        if(currentList != null) {
            CyoObject curObj = currentList.getCurrentCyoObject();
            Package curPackage = ((Package) curObj);
            if(curPackage.getSelection() == null){ // && !curPackage.getSelection().equals(KeyNames.SINGLE)
//                g.drawImage(menuGlowImg, 698, 309, c);
    
                g.setFont(f18);
                if (ui.getState() == PopularPackagesUI.STATE_MENU) {
                    g.drawImage(btnFocusImg, 717, 236, c);
                    g.setColor(btnColor);
                } else {
                    g.drawImage(btnDimImg, 718, 237, c);
                    g.setColor(dimmedMenuColor);
                }
                GraphicUtil.drawStringCenter(g, menu, 804, 257);
            }
        }
        
        boolean isChanged = ui.getChanged();
        g.setFont(f18);
        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getButtonFocus() == i && ui.getState() == PopularPackagesUI.STATE_BUTTON) {
                g.drawImage(btnFocusImg, 717, 346 + step, c);
                g.setColor(btnColor);
            } else {
                if (!isChanged && i == 0) {                   
                    g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuInColor);
                } else {
                    g.drawImage(btnDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuColor);
                }
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 367 + step);
        }
        
        // comment by VDTRMASTER-5278 [CQ][PO1/2/3][R4.1][CYO] FAPL Text removal
//        g.setFont(f15);
//        g.setColor(taxColor);
//        g.drawString(fund, 56, 476);
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg_origin.png");
        menuLineImg = ui.getImage("17_menu_line.png");
//        menuGlowImg = ui.getImage("17_menu_glow.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        countBgImg = ui.getImage("17_cus_tab.png");
        btnGrayDimImg = ui.getImage("17_btn_gray_in.png");
    }
}

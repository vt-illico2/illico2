/**
 * @(#)InitialStateRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.sun.corba.se.impl.orbutil.graph.Graph;
import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Shortcut;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.InitialStateUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class InitialStateRenderer extends BaseRenderer {

    private Image focusImg, focus2Img;
    private Image[] iconImgArr;
    private Image imgButtonIconExit;
    
    private String[] titleStrArr;

    // VDTRMASTER-5967
    private String[] menuStrArr;
    private String[] menuExplainStrArr;

    private final int yPos = 58;
    /**
     * 
     */
    public InitialStateRenderer() {

    }

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);
        String title = BaseUI.getMenuText(KeyNames.CUSTOMIZE_YOUR_TV_PROGRAMMING);

        titleStrArr = TextUtil.split(title, fm28, 600, "|");

        // VDTRMASTER-5967
        if (menuStrArr == null) {
            menuStrArr = new String[3];
        }

        if (menuExplainStrArr == null) {
            menuExplainStrArr = new String[3];
        }

        for (int i = 0; i < 3; i++) {
            menuStrArr[i] = BaseUI.getMenuText(KeyNames.MAIN_MENU + i);
            menuExplainStrArr[i] = BaseUI.getMenuText(KeyNames.MAIN_EXPLAIN + i);
        }

        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        InitialStateUI ui = (InitialStateUI) c;

        // Redirection screen
        if (CyoController.getInstance().getRedirectionCode() != CyoController.REDIRECTION_CODE_NONE) {
            g.drawImage(ui.getImage("bg.jpg"), 0, 0, c);
            // VDTRMASTER-5892
            if (CyoController.getInstance().getRedirectionCode() == CyoController.REDIRECTION_CODE_PACAKGE
                    && CyoController.getInstance().getTargetCallLetter() == null) {
                g.setFont(f19);
                GraphicUtil.drawStringCenter(g, BaseUI.getExplainText(KeyNames.REDIRECTION_EXPLAIN), 481, 211);
            }
        } else {
            // Normal Main screen
            super.paint(g, c);

            if (ui.successLogin()) {

                g.setFont(f28);
                g.setColor(Color.white);
                g.drawString(titleStrArr[0], 63, 112);
                g.drawString(titleStrArr[1], 63, 140);

                if (InitialStateUI.shortcuts.getCyoObject().length > 2) {
                    // three menus
                    CyoObject[] groups = InitialStateUI.shortcuts.getCyoObject();
                    int focus = ui.getFocus();

                    int stepX = 285;

                    for (int i = 0; i < groups.length; i++) {
                        stepX = i * 285;
                        // bg
                        g.setColor(C021021021);
                        g.fillRect(60 + stepX, 170, 270, 288);

                        DVBAlphaComposite lastComposite = ((DVBGraphics) g).getDVBComposite();
                        if (i != focus) {
                            DVBAlphaComposite opp = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.7f);
                            try {
                                ((DVBGraphics) g).setDVBComposite(opp);
                            } catch (UnsupportedDrawingOperationException e) {
                            }
                        }
                        // icon
                        g.drawImage(iconImgArr[i], 125 + stepX, 226, c);

                        if (i != focus && lastComposite != null) {
                            try {
                                ((DVBGraphics) g).setDVBComposite(lastComposite);
                            } catch (UnsupportedDrawingOperationException e) {
                            }
                        }

                        // VDTRMASTER-5967
                        String titleStr = menuStrArr[i];
                        String[] explainStrArr = TextUtil.split(menuExplainStrArr[i], fm18, 180, "|");

                        g.setFont(f26);
                        if (i == focus) {
                            g.drawImage(focusImg, 60 + stepX, 170, c);
                            g.setColor(C249195000);
                            GraphicUtil.drawStringCenter(g, titleStr, 195 + stepX, 337);
                            g.setColor(titleColor);
                        } else {
                            g.setColor(titleColor);
                            GraphicUtil.drawStringCenter(g, titleStr, 195 + stepX, 337);
                            g.setColor(C170170170);
                        }

                        g.setFont(f18);
                        g.setColor(explainColor);
                        for (int j = 0; j < explainStrArr.length; j++) {
                            GraphicUtil.drawStringCenter(g, explainStrArr[j], 195 + stepX, 362 + j * 18);
                        }
                    }

                } else {
                    // two menus
                    CyoObject[] groups = InitialStateUI.shortcuts.getCyoObject();
                    int focus = ui.getFocus();

                    int stepX = 285;

                    for (int i = 0; i < groups.length; i++) {
                        stepX = i * 428;
                        // bg
                        g.setColor(C021021021);
                        g.fillRect(60 + stepX, 170, 412, 288);

                        DVBAlphaComposite lastComposite = ((DVBGraphics) g).getDVBComposite();
                        if (i != focus) {
                            DVBAlphaComposite opp = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.7f);
                            try {
                                ((DVBGraphics) g).setDVBComposite(opp);
                            } catch (UnsupportedDrawingOperationException e) {
                            }
                        }
                        // icon
                        g.drawImage(iconImgArr[i + 1], 196 + stepX, 226, c);

                        if (i != focus && lastComposite != null) {
                            try {
                                ((DVBGraphics) g).setDVBComposite(lastComposite);
                            } catch (UnsupportedDrawingOperationException e) {
                            }
                        }

                        // VDTRMASTER-5967
                        String titleStr = menuStrArr[i+1];
                        String[] explainStrArr = TextUtil.split(menuExplainStrArr[i+1], fm18, 350, "|");

                        g.setFont(f26);
                        if (i == focus) {
                            g.drawImage(focus2Img, 60 + stepX, 170, c);
                            g.setColor(C249195000);
                            GraphicUtil.drawStringCenter(g, titleStr, 268 + stepX, 337);
                            g.setColor(titleColor);
                        } else {
                            g.setColor(titleColor);
                            GraphicUtil.drawStringCenter(g, titleStr, 268 + stepX, 337);
                            g.setColor(C170170170);
                        }

                        g.setFont(f18);
                        g.setColor(explainColor);
                        for (int j = 0; j < explainStrArr.length; j++) {
                            GraphicUtil.drawStringCenter(g, explainStrArr[j], 267 + stepX, 362 + j * 18);
                        }
                    }

                }
            }

            g.setFont(f18);
            g.setColor(exitBtnColor);
            String txtKeyInfo = (String) dataCenter.get("menu.btn.exit");
            if (txtKeyInfo != null) {
                g.drawString(txtKeyInfo, 64, 553 - yPos);
            }
            int infoWidth = g.getFontMetrics().stringWidth(txtKeyInfo);
            if (imgButtonIconExit != null) {
                g.drawImage(imgButtonIconExit, 64 + infoWidth + 5, 539 - yPos, c);
            }
        }
    }

    protected final void loadImages(BaseUI ui) {
        imgButtonIconExit = ui.getFooterImage(PreferenceService.BTN_EXIT);

        focusImg = ui.getImage("17_main+_foc.png");
        focus2Img = ui.getImage("17_main+_foc2.png");

        iconImgArr = new Image[3];
        for (int i = 0; i < 3; i++) {
            iconImgArr[i] = ui.getImage("17_main_icon0" + (i+1) + ".png");
        }
    }
}

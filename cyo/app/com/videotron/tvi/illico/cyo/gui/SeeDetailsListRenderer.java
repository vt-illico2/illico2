/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Fee;
import com.videotron.tvi.illico.cyo.data.obj.Game;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Product;
import com.videotron.tvi.illico.cyo.data.obj.Service;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.ui.SeeDetailsListUI;
import com.videotron.tvi.illico.cyo.ui.ValidateListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class SeeDetailsListRenderer extends BaseRenderer {

    private final int LINE_STEP = 32;
    private final int LIST_HEIGHT = 323;

    private Image sLineImg;
    private Image packageLineImg;
//    private Image shTopImg;
//    private Image shBotImg;
//    private Image groupShTopImg;
//    private Image groupShBotImg;
    private Image scrBarImg;
    private Image scrBgTopImg;
    private Image scrBgMidImg;
    private Image scrBgBotImg;
    private Image scrBgImg;

    protected Color bgColor = new Color(166, 166, 166);
    protected Color blackColor = new Color(0, 0, 0);
    protected Color unFocusedColor = new Color(230, 230, 230);
    protected Color focusBgColor = new Color(132, 132, 132);
    protected Color focusColor = blackColor;
    private Color modifiedColor = new Color(121, 230, 130);

    protected void paint(Graphics g, UIComponent c) {

        SeeDetailsListUI parentUI = (SeeDetailsListUI) c;
        // g.translate(-56, -169);
        // 56, 169
        // 638 x 32 * N 개
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(50, 135, 529, 323);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;

        g.setFont(f18);
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Group) {
                String title = ((Group) obj).getTitle();
                title = TextUtil.shorten(title, g.getFontMetrics(), 320);
                g.setColor(dimmedMenuColor);

                g.drawString(title, 65, 157 + heightOfDrawn);
                if (currentIdx > 0) {
                    g.drawImage(packageLineImg, 64, 134 + heightOfDrawn, c);
                }
                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Fee) {
                String title = ((Fee) obj).getTitle();
                title = TextUtil.shorten(title, g.getFontMetrics(), 320);
                g.setColor(dimmedPriceColor);

                g.drawString(title, 106, 157 + heightOfDrawn);
                if (heightOfDrawn + 46 < LIST_HEIGHT) {
                    g.drawImage(sLineImg, 96, 134 + heightOfDrawn, c);
                }

                int priceStep = 22;
                if (parentUI.needScroll()) {
                    priceStep = 0;
                }

                String subFix = BaseUI.getPriceFreq(((Fee) obj).getPricefreq());

                GraphicUtil.drawStringRight(g, ((Fee) obj).getPrice() + subFix, 536 + priceStep, 160 + heightOfDrawn);

                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Package) {
                String title = ((Package) obj).getTitle();
                title = TextUtil.shorten(title, g.getFontMetrics(), 320);
                g.setColor(dimmedPriceColor);

                g.drawString(title, 106, 157 + heightOfDrawn);
                
                if (heightOfDrawn + LINE_STEP < LIST_HEIGHT) {
                    g.drawImage(sLineImg, 96, 134 + heightOfDrawn, c);
                }

                int priceStep = 22;
                if (parentUI.needScroll()) {
                    priceStep = 0;
                }

                String subFix = BaseUI.getPriceFreq(((Package) obj).getPricefreq());

                GraphicUtil.drawStringRight(g, ((Package) obj).getPrice() + subFix, 536 + priceStep, 160 + heightOfDrawn);

                heightOfDrawn += LINE_STEP;
            }
            currentIdx++;
        }

        // scroll
        if (parentUI.needScroll()) {
            g.drawImage(scrBgImg, 530, 135, c);
            g.drawImage(scrBgTopImg, 560, 147, c);
            g.drawImage(scrBgMidImg, 560, 157, 7, 282, c);
            g.drawImage(scrBgBotImg, 560, 438, c);

            int scrGap = parentUI.getScrollGap();

            g.drawImage(scrBarImg, 557, 143 + scrGap, c);
        }

        g.setClip(beforeRec);
        // g.translate(56, 169);
    }

    public void prepare(UIComponent c) {
        SeeDetailsListUI parentUI = (SeeDetailsListUI) c;
        loadImages(parentUI);

        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        sLineImg = ui.getImage("17_slist_line_s.png");
        packageLineImg = ui.getImage("17_slist_line_b_s.png");
//        shTopImg = ui.getImage("17_sha_top.png");
//        shBotImg = ui.getImage("17_sha_bottom.png");
//        groupShTopImg = ui.getImage("17_list_glow_t.png");
//        groupShBotImg = ui.getImage("17_list_glow_b.png");
        scrBarImg = ui.getImage("scr_bar.png");
        scrBgTopImg = ui.getImage("scrbg_up.png");
        scrBgMidImg = ui.getImage("scr_m.png");
        scrBgBotImg = ui.getImage("scrbg_dn.png");
        scrBgImg = ui.getImage("16_scr_bg.png");
    }
}

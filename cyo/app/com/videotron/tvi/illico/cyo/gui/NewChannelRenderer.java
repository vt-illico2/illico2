/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.NewChannelUI;
import com.videotron.tvi.illico.cyo.ui.ViewChannelUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class NewChannelRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image addImg;
    private Image removeImg;

    private Image btnAImg;

    private String title;
    private String explain;
    private String addChannelsStr;
    private String removeChannelsStr;
    private String[] buttons;

    /**
     * 
     */
    public NewChannelRenderer() {
        buttons = new String[2];

    }

    public void prepare(UIComponent c) {

        loadImages((BaseUI) c);

        addChannelsStr = ": " + BaseUI.getMenuText(KeyNames.ADDED_CHANNEL);
        removeChannelsStr = ": " + BaseUI.getMenuText(KeyNames.REMOVED_CHANNEL);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_BACK);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        Hashtable footerHashtable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        btnAImg = (Image) footerHashtable.get(PreferenceService.BTN_A);

        super.prepare(c);
    }

    public void update(BaseUI c) {
        if (c.getLastUIId() == BaseUI.UI_VALIDATION) {
            title = BaseUI.getMenuText(KeyNames.MODIFIED_CHANNEL_LIST);
            explain = BaseUI.getExplainText(KeyNames.MODIFIED_CHANNEL_LIST);
        } else {
            title = BaseUI.getMenuText(KeyNames.NEW_CHANNEL_LIST_TITLE);
            explain = BaseUI.getMenuText(KeyNames.NEW_CHANNEL_LIST_EXPLAIN);
        }
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        NewChannelUI ui = (NewChannelUI) c;

        g.drawImage(listBgImg, 50, 138, c);

        // title
        if (title != null) {
            g.setColor(titleColor);
            g.setFont(f26);
            g.drawString(title, TITLE_X, TITLE_Y);
        }

        // explain
        if (explain != null) {
            g.setFont(f17);
            g.setColor(explainColor);
            g.drawString(explain, EXPLAIN_X, EXPLAIN_Y);
        }
        
        // sort
        g.setFont(f17);
        int x = GraphicUtil.drawStringRight(g, ui.getSortString(), SORTED_BY_X, SORTED_BY_Y);
        g.drawImage(btnAImg, x - 25, 105, c);

        g.drawImage(addImg, 705, 170, c);
        g.drawImage(removeImg, 705, 200, c);
        g.setFont(f18);
        g.drawString(addChannelsStr, 731, 188);
        g.drawString(removeChannelsStr, 731, 218);

        // list
        ListUI currentList = ui.getCurrentList();
        if (currentList != null) {
            currentList.paint(g);
        }

        g.setFont(f18);
        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getButtonFocus() == i && ui.getState() == NewChannelUI.STATE_BUTTON) {
                g.drawImage(btnFocusImg, 717, 383 + step, c);
                g.setColor(btnColor);
            } else {
                g.drawImage(btnDimImg, 718, 384 + step, c);
                g.setColor(dimmedMenuColor);
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 404 + step);
        }
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        addImg = ui.getImage("17_plus.png");
        removeImg = ui.getImage("17_remove.png");
    }
}

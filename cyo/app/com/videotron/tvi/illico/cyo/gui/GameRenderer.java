/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChangeChannelSelectionUI;
import com.videotron.tvi.illico.cyo.ui.GameUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GameRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image countBgImg;
    private Image btnGrayDimImg;

    private String title;
    private String explain;
    private String gamePlusExplain;
    private String selectedGames;
    private String[] buttons;

    /**
     * CYO_31
     */
    public GameRenderer() {
        buttons = new String[3];
    }

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        GameUI ui = (GameUI) c;

        if (ui.canSelect()) {

            title = BaseUI.getMenuText(KeyNames.GAME_A_LA_CARTE) + ui.getPackageName();

            buttons[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
            buttons[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
            buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);
        } else {
            title = ui.getPackageName();

            buttons[0] = null;
            buttons[1] = BaseUI.getMenuText(KeyNames.BTN_BACK);
            buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);
        }
        explain = BaseUI.getExplainText(KeyNames.GAME_EXPLAIN);
        gamePlusExplain = BaseUI.getExplainText(KeyNames.GAME_PLUS);
        selectedGames = BaseUI.getMenuText(KeyNames.SELECTED_GAMES);
        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        GameUI ui = (GameUI) c;

        g.drawImage(listBgImg, 3, 158, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        
        Package curPackage = (Package) ui.getPackage();
        
        if(ui.canSelect()) {
            g.drawString(explain, EXPLAIN_X, EXPLAIN_Y);
        } else if (curPackage.getNo().equals("460")) {
        	g.drawString(gamePlusExplain, EXPLAIN_X, EXPLAIN_Y);
        }

        // list
        ListUI currentList = ui.getCurrentList();
        if (currentList != null) {
            currentList.paint(g);

            if (ui.canSelect()) {
                g.drawImage(countBgImg, 50, 129, c);
                g.setFont(f22);
                g.setColor(countColor);
                GraphicUtil.drawStringCenter(g, currentList.getSelectedGame(), 80, 154);
                g.setFont(f18);
                g.setColor(dimmedMenuColor);
                g.drawString(selectedGames, 110, 153);
            }
        }

        // right menu
        // g.drawImage(menuGlowImg, 698, 309, c);

        g.setFont(f18);

        for (int i = 0; i < buttons.length; i++) {
            if (buttons[i] == null) {
                continue;
            }
            int step = i * 37;
            if (ui.getButtonFocus() == i && ui.getState() == GameUI.STATE_BUTTON) {
                g.drawImage(btnFocusImg, 717, 346 + step, c);
                g.setColor(btnColor);
            } else {
                CyoObject curObj = ui.getGameListUI().getCurrentCyoObject();
                if (!ui.isChanged() && i == 0) {                   
                    g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuInColor);
                } else {
                    g.drawImage(btnDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuColor);
                }
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 367 + step);
        }
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        countBgImg = ui.getImage("17_cus_tab.png");
        btnGrayDimImg = ui.getImage("17_btn_gray_in.png");
    }
}

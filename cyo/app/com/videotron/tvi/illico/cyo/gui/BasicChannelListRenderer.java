/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.BasicChannelListUI;
import com.videotron.tvi.illico.cyo.ui.ViewChannelUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class render a basic channel list.
 * @author Woojung Kim
 * @version 1.1
 */
public class BasicChannelListRenderer extends BaseRenderer {

    /** a boundground of list. */
    private Image listBgImg;
    /** a button focus. */
    private Image btnFocusImg;
    /** a button dimmed. */
    private Image btnDimImg;

    /** a title. */
    private String title;
    /** a explain. */
    private String explain;
    /** a button name array. */
    private String[] buttons;

    private Image btnAImg;

    /**
     * Constructor. create a array of button names.
     */
    public BasicChannelListRenderer() {
        buttons = new String[2];

    }

    /**
     * Prepare a renderer.
     * @param c a UI to have a this renderer.
     */
    public void prepare(UIComponent c) {
        BasicChannelListUI ui = (BasicChannelListUI) c;

        loadImages((BaseUI) c);

        title = BaseUI.getMenuText(KeyNames.BASIC_CHANNEL_LIST_TITLE);
        explain = BaseUI.getMenuText(KeyNames.BASIC_CHANNEL_LIST_EXPLAIN);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_BACK);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        Hashtable footerHashtable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        btnAImg = (Image) footerHashtable.get(PreferenceService.BTN_A);

        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g a Graphics to paint.
     * @param c a UI to have a this renderer.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        BasicChannelListUI ui = (BasicChannelListUI) c;

        g.drawImage(listBgImg, 50, 138, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f20);
        g.setColor(explainColor);
        g.drawString(explain, EXPLAIN_X, EXPLAIN_Y);

        
        // sort
        g.setFont(f17);
        int x = GraphicUtil.drawStringRight(g, ui.getSortString(), SORTED_BY_X, SORTED_BY_Y);
        g.drawImage(btnAImg, x - 25, 105, c);

        // list
        ListUI currentList = ui.getCurrentList();
        if (currentList != null) {
            currentList.paint(g);
        }

        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getButtonFocus() == i && ui.getState() == ViewChannelUI.STATE_BUTTON) {
                g.drawImage(btnFocusImg, 717, 383 + step, c);
                g.setColor(btnColor);
            } else {
                g.drawImage(btnDimImg, 718, 384 + step, c);
                g.setColor(dimmedMenuColor);
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 404 + step);
        }
    }

    /**
     * Load a images.
     * @param ui a UI to have a this renderer.
     */
    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
    }
}

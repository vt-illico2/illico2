/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Game;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.GameListUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GameListRenderer extends ListRenderer {

    private final int LINE_STEP = 32;
    private final int LIST_HEIGHT = 291;

    private Image lineImg;
    private Image focusImg;
    private Image checkBoxImg;
    private Image checkImg;
    private Image checkWithFocusImgIn;
    private Image checkBoxWithFocusImg;
    private Image checkBoxWithFocusImgIn;
    private Image checkWithFocusImg;
    private Image checkBoxDimImg;
    private Image focusBgTopImg;
    private Image focusBgMidImg;
    private Image focusBgBotImg;
    private Image focusBgTopDimImg;
    private Image focusBgMidDimImg;
    private Image focusBgBotDimImg;
    private Image focusDimImg;
//    private Image explainShImg;
//    private Image shTopImg;
//    private Image shBotImg;
    private Image scrBarImg;
    private Image scrBgTopImg;
    private Image scrBgMidImg;
    private Image scrBgBotImg;
    private Image barYFocImg;
    private Image barYDimImg;
//    private Image gameGlowImg;
    private Image arrowTopImg;
    private Image arrowBotImg;
    private Image defaultImg;

    private Color blackColor = new Color(0, 0, 0);
    private Color unFocusedColor = new Color(230, 230, 230);
    private Color focusColor = blackColor;

    private boolean canSelect;

    protected void paint(Graphics g, UIComponent c) {

        GameListUI parentUI = (GameListUI) c;
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(57, 167, 636, 291);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Game) {
                Image cBoxImg;
                Image cImg;

                int focusType = parentUI.getFocusType(currentIdx);
                int descHeight = 0;

                if (focusType == ListUI.FOCUS_ON_ME) {

                    descHeight = getDescriptionHeight(parentUI, heightOfDrawn);// drawDescription(g, parentUI,

                } else {
                    if (heightOfDrawn + 46 < LIST_HEIGHT) {
                        g.drawImage(lineImg, 64, 198 + heightOfDrawn, c);
                    }
                    cBoxImg = checkBoxImg;
                    cImg = checkImg;
                    g.setFont(f18);
                    g.setColor(unFocusedColor);

                    if (canSelect) {
                        g.drawImage(cBoxImg, 71, 176 + heightOfDrawn, c); // 76, 176
                        if (((Game) obj).getSubscribedInSession()) {
                            g.drawImage(cImg, 72, 175 + heightOfDrawn, c);
                        }
                        g.drawString(((Game) obj).getTitle(), 100, 190 + heightOfDrawn);
                    } else {
                        g.drawString(((Game) obj).getTitle(), 72, 190 + heightOfDrawn);
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            }
            currentIdx++;
        }

        // shadow
//        if (parentUI.isDisplayTopDeco()) {
//            g.drawImage(shTopImg, 56, 166, c);
//        }
//
//        if (parentUI.isDisplayBottomDeco()) {
//            g.drawImage(shBotImg, 56, 395, c);
//        }

        startIdx = parentUI.getStartIdx();
        currentIdx = startIdx;
        heightOfDrawn = 0;

        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Game) {
                Image cBoxImg;
                Image cImg;

                int focusType = parentUI.getFocusType(currentIdx);
                int descHeight = 0;

                if (focusType == ListUI.FOCUS_ON_ME) {

                    descHeight = drawDescription(g, parentUI, heightOfDrawn);

                    g.setFont(f20);
                    g.setColor(focusColor);

                    if (parentUI.hasFocus()) {
                         cBoxImg = checkBoxWithFocusImg;
                         cImg = checkWithFocusImg;

//                        cBoxImg = (((Game) obj).getModifiable()).equals("0") ? checkBoxWithFocusImg
//                                : checkBoxWithFocusImgIn;
//                        cImg = (((Game) obj).getModifiable()).equals("0") ? checkWithFocusImg : checkWithFocusImgIn;

                        g.drawImage(focusImg, 57, 163 + heightOfDrawn, parentUI);
                        g.drawImage(barYFocImg, 538, 201 + heightOfDrawn, parentUI);
                    } else {
                        cBoxImg = checkBoxWithFocusImg;
                        cImg = checkWithFocusImg;
                        
//                        cBoxImg = (((Game) obj).getModifiable()).equals("0") ? checkBoxWithFocusImg
//                                : checkBoxWithFocusImgIn;
//                        cImg = (((Game) obj).getModifiable()).equals("0") ? checkWithFocusImg : checkWithFocusImgIn;
                        
                        g.drawImage(focusDimImg, 57, 163 + heightOfDrawn, parentUI);
                        g.drawImage(barYDimImg, 538, 201 + heightOfDrawn, parentUI);
                    }

                    if (parentUI.getGameImage() != null) {
                        g.drawImage(parentUI.getGameImage(), 542, 170 + heightOfDrawn, 147, 91, parentUI);
//                        g.drawImage(gameGlowImg, 543, 171 + heightOfDrawn, parentUI);
                    } else {
                        g.drawImage(defaultImg, 542, 170 + heightOfDrawn, 147, 91, parentUI);
                    }
                    g.setFont(f20);
                    g.setColor(focusColor);

                    if (canSelect) {
                        g.drawImage(cBoxImg, 71, 176 + heightOfDrawn, c); // 76, 176
                        if (((Game) obj).getSubscribedInSession()) {
                            g.drawImage(cImg, 72, 175 + heightOfDrawn, c);
                        }
                        g.drawString(((Game) obj).getTitle(), 100, 190 + heightOfDrawn);
                    } else {
                        g.drawString(((Game) obj).getTitle(), 72, 190 + heightOfDrawn);
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            }
            currentIdx++;
        }

        g.setClip(beforeRec);

        // arrow.
        if (parentUI.isDisplayTopDeco()) {
            g.drawImage(arrowTopImg, 364, 158, c);
        }
        if (parentUI.isDisplayBottomDeco()) {
            g.drawImage(arrowBotImg, 364, 449, c);
        }
    }

    public int drawDescription(Graphics g, ListUI parentUI, int heightOfDrawn) {
        String[] desc = parentUI.getDesc();

        if (desc == null) {
            return heightOfDrawn + 32;
        }

        g.setColor(bgColor);
        g.fillRect(61, 201 + heightOfDrawn, 628, 63);

        if (parentUI.hasFocus()) {
            g.drawImage(focusBgMidImg, 57, 201 + heightOfDrawn, 636, 63, parentUI);
            g.drawImage(focusBgBotImg, 57, 201 + heightOfDrawn + 30, parentUI);
        } else {
            g.drawImage(focusBgMidDimImg, 57, 201 + heightOfDrawn, 636, 63, parentUI);
            g.drawImage(focusBgBotDimImg, 57, 201 + heightOfDrawn + 30, parentUI);
        }

        g.setFont(f14d);
        g.setColor(blackColor);
        int descStartIdx = parentUI.getDescStartIdx();
        int count = 0;
        int posX = 72;
        if (canSelect) {
            posX = 100;
        }
        for (int i = descStartIdx; i < descStartIdx + 3; i++) {
            if (i < desc.length && desc[i] != null) {
                g.drawString(desc[i], posX, 220 + heightOfDrawn + count * 17);
                count++;
            }
        }

        if (desc.length > 3) {
            if (descStartIdx + 3 < desc.length) {
//                g.drawImage(explainShImg, 61, 238 + heightOfDrawn, parentUI);
            }

            // scroll
            g.drawImage(scrBgTopImg, 517, 211 + heightOfDrawn, 7, 10, parentUI);
            g.drawImage(scrBgMidImg, 517, 221 + heightOfDrawn, 7, 27, parentUI);
            g.drawImage(scrBgBotImg, 517, 246 + heightOfDrawn, parentUI);

            int step = parentUI.getScrollGap();
            g.drawImage(scrBarImg, 514, 210 + heightOfDrawn + step, parentUI);
        }
        heightOfDrawn += 95;
        return heightOfDrawn;
    }

    public void prepare(UIComponent c) {
        ListUI parentUI = (ListUI) c;

        if (parentUI.getState() == 0) {
            canSelect = false;
        } else {
            canSelect = true;
        }

        loadImages(parentUI);

        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        lineImg = ui.getImage("17_list_line.png");
        focusImg = ui.getImage("17_slist_foc.png");
        checkBoxImg = ui.getImage("check_box.png");
        checkBoxImg = ui.getImage("check_box.png");
        checkImg = ui.getImage("check.png");
        checkBoxWithFocusImg = ui.getImage("check_box_foc.png");
        checkWithFocusImg = ui.getImage("check_foc.png");
        checkBoxWithFocusImgIn = ui.getImage("check_box_foc_in.png");
        checkWithFocusImgIn = ui.getImage("check_foc_in.png");
        checkBoxDimImg = ui.getImage("check_box_foc_dim.png");
        focusBgTopImg = ui.getImage("17_rim01_t.png");
        focusBgMidImg = ui.getImage("17_rim01_m.png");
        focusBgBotImg = ui.getImage("17_rim01_b.png");
        focusImg = ui.getImage("17_list_foc.png");
        focusBgTopDimImg = ui.getImage("17_rim01_dim_t.png");
        focusBgMidDimImg = ui.getImage("17_rim01_dim_m.png");
        focusBgBotDimImg = ui.getImage("17_rim01_dim_b.png");
        focusDimImg = ui.getImage("17_list_foc_dim.png");
//        explainShImg = ui.getImage("17_list_sha_b_s.png");
//        shTopImg = ui.getImage("17_sha_top.png");
//        shBotImg = ui.getImage("17_sha_bottom.png");
        scrBarImg = ui.getImage("scr_bar.png");
        scrBgTopImg = ui.getImage("scrbg_up.png");
        scrBgMidImg = ui.getImage("scr_m.png");
        scrBgBotImg = ui.getImage("scrbg_dn.png");
        barYFocImg = ui.getImage("17_game_foc_y.png");
        barYDimImg = ui.getImage("17_game_foc_g.png");
//        gameGlowImg = ui.getImage("17_game_glow.png");
        arrowTopImg = ui.getImage("02_ars_t.png");
        arrowBotImg = ui.getImage("02_ars_b.png");
        defaultImg = ui.getImage("cyo_default.png");
    }
}

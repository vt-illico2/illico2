/**
 * @(#)ConfirmationRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ConfirmationUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a comfirmation screen.
 * @author Woojung Kim
 * @version 1.1
 */
public class ConfirmationRenderer extends BaseRenderer {

    /** Image for button. */
    private Image btnImg;
    /** Image for green icon. */
    private Image greenIcon;
    /** Image for shadow on text. */
//    private Image txtShaImg;
    /** String for confirmation main. */
    private String mainStr;
    /** String array for confirmation explain. */
    private String[] explainStr;
    /** String for exit. */
    private String exitStr;
    /** X coordinate for main string. */
    private int mainPosX = 0;
    /** X coordinate for green icon. */
    private int greenPosX = 0;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        mainStr = BaseUI.getMenuText(KeyNames.CONFIRMATION_MAIN);

        int mainLength = BaseRenderer.fm33.stringWidth(mainStr);

        mainPosX = 504;

        greenPosX = 504 - mainLength / 2 - 50;

        String tempStr = BaseUI.getMenuText(KeyNames.CONFIRMATION_EXPLAIN);
        explainStr = TextUtil.split(tempStr, BaseRenderer.fm19, 800, '|');

        exitStr = BaseUI.getMenuText(KeyNames.EXIT);
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

//        g.drawImage(txtShaImg, 108, 216, c);

        // main
        g.setColor(priceColor);
        g.setFont(f33);
        GraphicUtil.drawStringCenter(g, mainStr, mainPosX, 175);
        g.drawImage(greenIcon, greenPosX, 149, c);

        // explain
        g.setColor(subListColor);
        g.setFont(f19);
        for (int i = 0; i < explainStr.length; i++) {
            int step = i * 22;
            GraphicUtil.drawStringCenter(g, explainStr[i], 480, 206 + step);
        }

        // button
        g.drawImage(btnImg, 402, 381, c);
        g.setColor(btnColor);
        g.setFont(f18);
        GraphicUtil.drawStringCenter(g, exitStr, 480, 403);

        // the message come from backend.
        ConfirmationUI ui = (ConfirmationUI) c;

        String[] cMessages = ui.getConfirmationMessge();
        if (cMessages != null) {
            int posY = 310 - (cMessages.length - 1) * 11;

            g.setFont(f22);
            g.setColor(titleColor);
            for (int i = 0; i < cMessages.length; i++) {
                GraphicUtil.drawStringCenter(g, cMessages[i], 480, posY + i * 22);
            }
        }
    }

    /**
     * Loads a images.
     * @param ui parent UI.
     */
    protected final void loadImages(BaseUI ui) {

        btnImg = ui.getImage("05_focus.png");
        greenIcon = ui.getImage("icon_g_check_big.png");
//        txtShaImg = ui.getImage("12_txtsha.png");
    }
}

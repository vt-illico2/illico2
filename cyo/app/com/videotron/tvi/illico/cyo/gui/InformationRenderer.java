/**
 * @(#)InformationRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.InformationUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a information. (Help / Rules)
 * @author Woojung Kim
 * @version 1.1
 */
public class InformationRenderer extends BaseRenderer {
    /** Image for background of list. */
    private Image listBgImg;
    /** Image for top part of button background. */
    private Image btnTopImg;
    /** Image for glow of button background. */
//    private Image btnGlowImg;
    /** Image for shadow of button background. */
//    private Image btnShImg;
    /** Image for button to have a focus. */
    private Image btnFocImg;
    /** Image for arrow on focused button. */
    private Image btnArrowImg;
    /** Image for background of information. */
    private Image infoBgImg;
    /** Image for button on information. */
    private Image infoBtnImg;
    /** Image for button to have a focus on information. */
    private Image infoBtnFocImg;
    /** String for title. */
    private String titleStr;
    /** String for explain. */
    private String explainStr;
    /** String for subtitle. */
    private String subTitleStr;
    /** String array for service information. */
    private String[] serviceStr;
    /** String array for button name on inforamtion. */
    private String[] infoBtnStr;
    /** String for button. */
    private String btnStr;

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        InformationUI ui = (InformationUI) c;
        super.paint(g, c);

        g.setFont(f26);
        g.setColor(titleColor);
        g.drawString(titleStr, TITLE_X, TITLE_Y);
        g.setFont(f17);
        g.setColor(explainColor);
        g.drawString(explainStr, EXPLAIN_X, EXPLAIN_Y);
        g.drawImage(listBgImg, 3, 133, c);

        g.setFont(f20);
        g.setColor(titleColor);
        g.drawString(subTitleStr, 77, 167);

        // button bg
        g.drawImage(btnTopImg, 626, 418, c);
//        g.drawImage(btnGlowImg, 625, 439, c);
//        g.drawImage(btnShImg, 579, 451, c);

        // info
        g.drawImage(infoBgImg, 626, 134, c);

        if (serviceStr != null) {
            g.setFont(f18);
            g.setColor(countColor);
            g.drawString(serviceStr[0], 645, 164);
            g.setColor(titleColor);
            g.drawString(serviceStr[1], 645, 186);

            g.setFont(f16);

            g.setColor(subListColor);
            g.drawString(serviceStr[2], 648, 223);
            g.drawString(serviceStr[4], 648, 269);

            g.setColor(titleColor);
            g.drawString(serviceStr[3], 648, 241);
            g.drawString(serviceStr[5], 648, 287);

        }

        g.setFont(f18);
        g.setColor(btnColor);

        if (ui.getState() == InformationUI.STATE_MENU) {
            g.drawImage(infoBtnFocImg, 622, 352, c);
        } else {
            g.drawImage(infoBtnImg, 622, 352, c);
        }

        if (infoBtnStr.length > 1) {
            GraphicUtil.drawStringCenter(g, infoBtnStr[0], 766, 370);
            GraphicUtil.drawStringCenter(g, infoBtnStr[1], 766, 388);
        } else {
            GraphicUtil.drawStringCenter(g, infoBtnStr[0], 766, 378);
        }

        // button
        if (ui.getState() == InformationUI.STATE_BUTTON) {
            g.drawImage(btnFocImg, 624, 419, c);
            g.setColor(btnColor);
            g.setFont(f21);
            g.drawString(btnStr, 671, 443);
        } else {
            g.drawImage(btnArrowImg, 649, 434, c);
            g.setColor(dimmedMenuColor);
            g.setFont(f18);
            g.drawString(btnStr, 671, 442);
        }
    }

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        InformationUI ui = (InformationUI) c;

        if (ui.getMode() == InformationUI.MODE_HELP) {
            titleStr = BaseUI.getMenuText(KeyNames.HELP);
            String tmp = BaseUI.getMenuText(KeyNames.BTN_RULES);
            infoBtnStr = TextUtil.tokenize(tmp, '|');
        } else {
            titleStr = BaseUI.getMenuText(KeyNames.RULES);
            String tmp = BaseUI.getMenuText(KeyNames.BTN_VIEW_HELP);
            infoBtnStr = TextUtil.tokenize(tmp, '|');
        }

        String tmp = BaseUI.getMenuText(KeyNames.INFORMATION_CONTACT);
        serviceStr = TextUtil.tokenize(tmp, '|');

        explainStr = BaseUI.getMenuText(KeyNames.USEFUL_INFORMATION);
        subTitleStr = BaseUI.getMenuText(KeyNames.INFOMATION_SUB_TITLE);

        btnStr = BaseUI.getMenuText(KeyNames.BTN_BACK);
        super.prepare(c);
    }

    /**
     * Loads a images.
     * @param ui parent UI.
     */
    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg2.png");

        btnTopImg = ui.getImage("01_acbg_1st.png");
//        btnGlowImg = ui.getImage("01_acglow.png");
//        btnShImg = ui.getImage("01_ac_shadow.png");
        btnFocImg = ui.getImage("02_detail_bt_foc.png");
        btnArrowImg = ui.getImage("02_detail_ar.png");

        infoBgImg = ui.getImage("17_infobg.png");
        infoBtnImg = ui.getImage("12_pos_focus_dim.png");
        infoBtnFocImg = ui.getImage("12_pos_focus.png");
    }

}

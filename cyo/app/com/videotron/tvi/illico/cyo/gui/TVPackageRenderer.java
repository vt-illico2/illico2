package com.videotron.tvi.illico.cyo.gui;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.PackageCatalog;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.TVPackageUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-08-08.
 */
public class TVPackageRenderer extends BaseRenderer {

	private Image focusImg, basicBgImg, premiumBgImg, checkFocusImg;

	private Color itemBgColor = new Color(60, 60, 60);
	private Color itemBTColor = new Color(0, 0, 0);
	private Color productExplainColor = new Color(138, 138, 138);
	private DecimalFormat df = new DecimalFormat("#.00");

	private String titleStr, explainStr, hdBasicStr, premiumStr, chooseMyChannelsStr, modifyMychannelsStr, channelsStr
			, monthStr;
	private String[] productExplainStrArr;

	public void prepare(UIComponent c) {
		loadImages((BaseUI) c);

		titleStr = BaseUI.getMenuText(KeyNames.TV_PACKAGE_LIST);
		explainStr = BaseUI.getExplainText(KeyNames.TV_PACKAGE_LIST);

		String productStr = BaseUI.getExplainText(KeyNames.TV_PACKAGE_LIST_PRODUCT);
		productExplainStrArr = TextUtil.split(productStr, fm14d, 480, "|");

		hdBasicStr = BaseUI.getMenuText(KeyNames.HD_BASIC);
		premiumStr = BaseUI.getMenuText(KeyNames.PREMIUM);
		chooseMyChannelsStr = BaseUI.getMenuText(KeyNames.CHOOSE_MY_CHANNELS);
		modifyMychannelsStr = BaseUI.getMenuText(KeyNames.MODIFY_MY_CHANNELS);
		channelsStr = BaseUI.getMenuText(KeyNames.CHANNELS);
		monthStr = BaseUI.getMenuText(KeyNames.MONTH2);

		super.prepare(c);
	}

	protected void loadImages(BaseUI ui) {
		focusImg = ui.getImage("17_tvp_foc.png");
		basicBgImg = ui.getImage("17_tvp_gen.png");
		premiumBgImg = ui.getImage("17_tvp_pre.png");
		checkFocusImg = ui.getImage("check_foc.png");
	}

	protected void paint(Graphics g, UIComponent c) {
		TVPackageUI ui = (TVPackageUI) c;
		super.paint(g, c);

		// title
		g.setColor(titleColor);
		g.setFont(f26);
		g.drawString(titleStr, TITLE_X, TITLE_Y);

		// explain
		g.setFont(f17);
		g.setColor(explainColor);
		g.drawString(explainStr, EXPLAIN_X, EXPLAIN_Y);

		// list
		Package[] packages = ui.getPackageData();
		if (packages != null) {
			int limitLength = packages.length > 4 ? 4 : packages.length;
			int startIdx = ui.getStartIdx();

			int startX = 50;

			int focusIdx = startIdx + ui.getFocus();

			if (ui.getFocus() == 3 || ui.isMovingToFirst()) {
				startX = -95;
			} else if (focusIdx == 0) {
				startX = 50;
			}

			for (int i = 0; i < limitLength; i++) {
				int idx = startIdx + i;

				Package p = packages[idx];

				String priceStr = p.getPrice();
				String dollarStr = "0";
				String centStr = "00 $";
				if (priceStr != null) {
					float priceFloat = Float.valueOf(priceStr).floatValue();
					String formattedPriceStr = df.format(priceFloat);

					int dotIdx = formattedPriceStr.indexOf('.');
					if (dotIdx > -1) {
						dollarStr = formattedPriceStr.substring(0, dotIdx);
						centStr = formattedPriceStr.substring(dotIdx + 1) + " $";
					}
				}

				if (idx == focusIdx) {
					// item bg
					g.setColor(itemBgColor);
					g.fillRect(startX, 134, 260, 326);
					g.setColor(itemBTColor);
					g.fillRect(startX, 348, 260, 112);
					g.setColor(DC255255255026);
					g.fillRect(startX + 14, 199, 232, 1);

					// item title
					g.setColor(priceColor);
					String[] titles = TextUtil.split(p.getTitle(), fm24, 190, 2);

					if (titles.length == 1) {
						g.setFont(f24);
						GraphicUtil.drawStringCenter(g, p.getTitle(), startX + 131, 175);
					} else {
						g.setFont(f22);
						GraphicUtil.drawStringCenter(g, titles[0], startX + 131, 165);
						GraphicUtil.drawStringCenter(g, titles[1], startX + 131, 185);
					}

					if (p.getPackageCatalogs().length > 1 && p.hasPremium()) {
						// display two catalogs
						g.drawImage(basicBgImg, startX + 40, 215, c);

						PackageCatalog pc = p.getPackageCatalogs()[p.getNumberOfPremiumCatalogs()];

						// hd basic
						g.setFont(f17);
						g.setColor(countColor);
						GraphicUtil.drawStringCenter(g, hdBasicStr, startX + 83, 231);

						// numberOfChannels
						g.setColor(btnColor);
						String numberOfChannelsStr = String.valueOf(p.getNumberOfNormalChannels());
						g.setFont(f46);
						GraphicUtil.drawStringCenter(g, numberOfChannelsStr, startX + 91, 273);
						g.setFont(f36);
						GraphicUtil.drawStringCenter(g, "+", startX + 60, 271);

						// channels
						g.setFont(f16);
						g.setColor(C050050050);
						GraphicUtil.drawStringCenter(g, channelsStr, startX + 83, 290);

						// item explain
						g.setFont(f16);
						g.setColor(titleColor);
						String cTitleStr = pc.getCatalog().getTitle();
						String[] cTitleArr = TextUtil.split(cTitleStr, fm16, 86, 2);

						if (cTitleArr.length == 1) {
							GraphicUtil.drawStringCenter(g, cTitleStr, startX + 83, 325);
						} else {
							GraphicUtil.drawStringCenter(g, cTitleArr[0], startX + 83, 318);
							GraphicUtil.drawStringCenter(g, cTitleArr[1], startX + 83, 334);
						}

						// Premium item
						g.drawImage(premiumBgImg, startX + 134, 215, c);

						PackageCatalog pc2 = p.getPackageCatalogs()[0];

						// numberOfChannels
						g.setFont(f46);
						g.setColor(countColor);
						GraphicUtil
								.drawStringCenter(g, String.valueOf(p.getNumberOfPremiumChannels()), startX + 187, 268);
						g.setFont(f36);
						GraphicUtil.drawStringCenter(g, "+", startX + 165, 266);

						// premium tag
						g.setFont(f16);
						GraphicUtil.drawStringCenter(g, premiumStr, startX + 180, 290);

						// item explain
						g.setFont(f16);
						g.setColor(titleColor);
						cTitleStr = pc2.getCatalog().getTitle();
						cTitleArr = TextUtil.split(cTitleStr, fm16, 86, 2);

						if (cTitleArr.length == 1) {
							GraphicUtil.drawStringCenter(g, cTitleStr, startX + 180, 325);
						} else {
							GraphicUtil.drawStringCenter(g, cTitleArr[0], startX + 180, 318);
							GraphicUtil.drawStringCenter(g, cTitleArr[1], startX + 180, 334);
						}

					} else {
						// display one catalog
						g.drawImage(basicBgImg, startX + 87, 215, c);

						PackageCatalog pc = p.getPackageCatalogs()[0];

						// hd basic
						g.setFont(f17);
						g.setColor(countColor);
						GraphicUtil.drawStringCenter(g, hdBasicStr, startX + 130, 231);

						// numberOfChannels
						g.setColor(btnColor);
						String numberOfChannelsStr = pc.getNumberOfChannels();
						g.setFont(f46);
						GraphicUtil.drawStringCenter(g, numberOfChannelsStr, startX + 138, 273);
						g.setFont(f36);
						GraphicUtil.drawStringCenter(g, "+", startX + 107, 271);

						// channels
						g.setFont(f16);
						g.setColor(C050050050);
						GraphicUtil.drawStringCenter(g, channelsStr, startX + 130, 290);

						// item explain
						g.setFont(f16);
						g.setColor(titleColor);
						String cTitleStr = pc.getCatalog().getTitle();
						String[] cTitleArr = TextUtil.split(cTitleStr, fm16, 86, 2);

						if (cTitleArr.length == 1) {
							GraphicUtil.drawStringCenter(g, cTitleStr, startX + 130, 325);
						} else {
							GraphicUtil.drawStringCenter(g, cTitleArr[0], startX + 130, 318);
							GraphicUtil.drawStringCenter(g, cTitleArr[1], startX + 130, 334);
						}
					}

					// price
					g.setFont(f38);
					g.setColor(titleColor);
					GraphicUtil.drawStringRight(g, dollarStr, startX + 125, 390);
					g.setFont(f16);
					g.drawString(centStr, startX + 137, 377);
					g.setColor(dimmedMenuInColor);
					;
					g.drawString(monthStr, startX + 126, 390);

					// button
					g.setColor(C249195000);
					g.fillRect(startX + 14, 409, 232, 36);
					g.setFont(f18);
					g.setColor(btnColor);

					if (p.getSubscribedInSession()) {
						GraphicUtil.drawStringCenter(g, modifyMychannelsStr, startX + 130, 432);
						g.setColor(C249195000);
						g.fillRect(startX + 240, 134, 20, 20);
						g.drawImage(checkFocusImg, startX + 243, 137, c);
					} else {
						GraphicUtil.drawStringCenter(g, chooseMyChannelsStr, startX + 130, 432);
					}

					g.drawImage(focusImg, startX, 134, c);
					startX += 272;
				} else {
					// item bg
					g.setColor(itemBgColor);
					g.fillRect(startX, 134, 220, 326);
					g.setColor(itemBTColor);
					g.fillRect(startX, 348, 220, 112);
					g.setColor(DC255255255026);
					g.fillRect(startX + 14, 199, 192, 1);

					// item title
					g.setColor(priceColor);
					String[] titles = TextUtil.split(p.getTitle(), fm24, 190, 2);

					if (titles.length == 1) {
						g.setFont(f24);
						GraphicUtil.drawStringCenter(g, p.getTitle(), startX + 111, 175);
					} else {
						g.setFont(f22);
						GraphicUtil.drawStringCenter(g, titles[0], startX + 111, 165);
						GraphicUtil.drawStringCenter(g, titles[1], startX + 111, 185);
					}

					if (p.getPackageCatalogs().length > 1 && p.hasPremium()) {
						// display two catalogs
						g.drawImage(basicBgImg, startX + 20, 229, c);

						// hd basic
						g.setFont(f17);
						g.setColor(countColor);
						GraphicUtil.drawStringCenter(g, hdBasicStr, startX + 63, 245);

						// numberOfChannels
						g.setColor(btnColor);
						String numberOfChannelsStr = String.valueOf(p.getNumberOfNormalChannels());
						g.setFont(f46);
						GraphicUtil.drawStringCenter(g, numberOfChannelsStr, startX + 71, 287);
						g.setFont(f36);
						GraphicUtil.drawStringCenter(g, "+", startX + 40, 285);

						// channels
						g.setFont(f16);
						g.setColor(C050050050);
						GraphicUtil.drawStringCenter(g, channelsStr, startX + 63, 304);

						// Premium item
						g.drawImage(premiumBgImg, startX + 114, 229, c);

						// numberOfChannels
						g.setFont(f46);
						g.setColor(countColor);
						GraphicUtil
								.drawStringCenter(g, String.valueOf(p.getNumberOfPremiumChannels()), startX + 167, 282);
						g.setFont(f36);
						GraphicUtil.drawStringCenter(g, "+", startX + 145, 280);

						// premium tag
						g.setFont(f16);
						GraphicUtil.drawStringCenter(g, premiumStr, startX + 160, 304);

					} else {
						// display one catalog
						g.drawImage(basicBgImg, startX + 67, 229, c);

						PackageCatalog pc = p.getPackageCatalogs()[0];

						// hd basic
						g.setFont(f17);
						g.setColor(countColor);
						GraphicUtil.drawStringCenter(g, hdBasicStr, startX + 110, 245);

						// numberOfChannels
						g.setColor(btnColor);
						String numberOfChannelsStr = pc.getNumberOfChannels();
						g.setFont(f46);
						GraphicUtil.drawStringCenter(g, numberOfChannelsStr, startX + 118, 287);
						g.setFont(f36);
						GraphicUtil.drawStringCenter(g, "+", startX + 87, 285);

						// channels
						g.setFont(f16);
						g.setColor(C050050050);
						GraphicUtil.drawStringCenter(g, channelsStr, startX + 110, 304);
					}

					// price
					g.setFont(f38);
					g.setColor(titleColor);
					GraphicUtil.drawStringRight(g, dollarStr, startX + 105, 390);
					g.setFont(f16);
					g.drawString(centStr, startX + 117, 377);
					g.setColor(dimmedMenuInColor);
					;
					g.drawString(monthStr, startX + 106, 390);

					// button
					g.setColor(C092092092);
					g.fillRect(startX + 14, 409, 192, 36);
					g.setFont(f18);
					g.setColor(dimmedMenuColor);

					if (p.getSubscribedInSession()) {
						GraphicUtil.drawStringCenter(g, modifyMychannelsStr, startX + 110, 432);
						g.setColor(C249195000);
						g.fillRect(startX + 200, 134, 20, 20);
						g.drawImage(checkFocusImg, startX + 203, 137, c);
					} else {
						GraphicUtil.drawStringCenter(g, chooseMyChannelsStr, startX + 110, 432);
					}

					startX += 232;
				}
			}
		}
		// end list

		// product explain
		g.setFont(f14d);
		g.setColor(productExplainColor);
		for (int i = 0; i < productExplainStrArr.length; i++) {
			g.drawString(productExplainStrArr[i], 59, 488 + i * 16);
		}
	}
}

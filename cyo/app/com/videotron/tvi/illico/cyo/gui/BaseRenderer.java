/**
 * @(#)BaseRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * The BaseRenderer is a parent class of all renderer.
 * @author Woojung Kim
 * @version 1.1
 */
public abstract class BaseRenderer extends Renderer {

    /** a refer object of DataCenter. */
    protected DataCenter dataCenter = DataCenter.getInstance();

    /** a image of clock. */
    private Image clockImg;
    /** a image of clock background. */
//    private Image clockBgImg;
    /** a image of background. */
    private Image bgImg;
    /** a image of next in bread crumb. */
    private Image hisOverImg;
    /** a image of hotkey background. */
    private Image hotkeyBgImg;

    /** a logo image of illico 2. */
    private Image logoImg = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);

    /** The font for dinmed size 14. */
    protected static Font f14d = FontResource.DINMED.getFont(14);
    /** The font for blender size 14. */
    protected static Font f14 = FontResource.BLENDER.getFont(14);
    /** The font for blender size 15. */
    protected static Font f15 = FontResource.BLENDER.getFont(15);
    /** The font for blender size 16. */
    protected static Font f16 = FontResource.BLENDER.getFont(16);
    /** The font for blender size 17. */
    protected static Font f17 = FontResource.BLENDER.getFont(17);
    /** The font for blender size 18. */
    protected static Font f18 = FontResource.BLENDER.getFont(18);
    /** The font for blender size 19. */
    protected static Font f19 = FontResource.BLENDER.getFont(19);
    /** The font for blender size 20. */
    protected static Font f20 = FontResource.BLENDER.getFont(20);
    /** The font for blender size 21. */
    protected static Font f21 = FontResource.BLENDER.getFont(21);
    /** The font for blender size 22. */
    protected static Font f22 = FontResource.BLENDER.getFont(22);
    /** The font for blender size 25. */
    protected static Font f24 = FontResource.BLENDER.getFont(24);
    /** The font for blender size 26. */
    protected static Font f26 = FontResource.BLENDER.getFont(26);
    /** The font for blender size 28. */
    protected static Font f28 = FontResource.BLENDER.getFont(28);
    /** The font for blender size 33. */
    protected static Font f33 = FontResource.BLENDER.getFont(33);
    /** The font for blender size 35. */
    protected static Font f35 = FontResource.BLENDER.getFont(35);
    /** The font for blender size 36. */
    protected static Font f36 = FontResource.BLENDER.getFont(36);
    /** The font for blender size 38. */
    protected static Font f38 = FontResource.BLENDER.getFont(38);
    /** The font for blender size 43. */
    protected static Font f43 = FontResource.BLENDER.getFont(43);
    /** The font for blender size 45. */
    protected static Font f45 = FontResource.BLENDER.getFont(45);
    /** The font for blender size 46. */
    protected static Font f46 = FontResource.BLENDER.getFont(46);

    protected static Font f60 = FontResource.BLENDER.getFont(60);

    /** The FontMetics for dinmed font 14 . */
    public static FontMetrics fm14d = FontResource.BLENDER.getFontMetrics(f14d);
    /** The FontMetics for font 14. */
    public static FontMetrics fm14 = FontResource.BLENDER.getFontMetrics(f14);
    /** The FontMetics for font 16. */
    protected static FontMetrics fm16 = FontResource.BLENDER.getFontMetrics(f16);
    /** The FontMetics for font 17. */
    protected static FontMetrics fm17 = FontResource.BLENDER.getFontMetrics(f17);
    /** The FontMetics for font 18. */
    public static FontMetrics fm18 = FontResource.BLENDER.getFontMetrics(f18);
    /** The FontMetics for font 19. */
    public static FontMetrics fm19 = FontResource.BLENDER.getFontMetrics(f19);
    /** The FontMetics for font 20. */
    protected static FontMetrics fm20 = FontResource.BLENDER.getFontMetrics(f20);
    /** The FontMetics for font 22. */
    protected static FontMetrics fm22 = FontResource.BLENDER.getFontMetrics(f22);
    /** The FontMetics for font 24. */
    protected static FontMetrics fm24 = FontResource.BLENDER.getFontMetrics(f24);
    /** The FontMetics for font 26. */
    protected static FontMetrics fm26 = FontResource.BLENDER.getFontMetrics(f26);
    /** The FontMetics for font 28. */
    protected static FontMetrics fm28 = FontResource.BLENDER.getFontMetrics(f28);

    /** The FontMetics for font 33. */
    protected static FontMetrics fm33 = FontResource.BLENDER.getFontMetrics(f33);
    protected static FontMetrics fm60 = FontResource.BLENDER.getFontMetrics(f60);

    /** The color of 255, 255, 255. */
    protected static Color titleColor = new Color(255, 255, 255);
    /** The color of 200, 200, 200. */
    protected static Color subListColor = new Color(200, 200, 200);
    /** The color of 230, 230, 230. */
    protected static Color dimmedMenuColor = new Color(230, 230, 230);
    protected static Color dimmedMenuInColor = new Color(97, 95, 95);
    /** The color of 3, 3, 3. */
    protected static Color btnColor = new Color(3, 3, 3);
    /** The color of 239, 239, 239. */
    protected static Color hisOverColor = new Color(239, 239, 239);
    /** The color of 250, 202, 0. */
    protected static Color popupTitleColor = new Color(250, 202, 0);
    /** The color of 245, 212, 16. */
    protected static Color countColor = new Color(245, 212, 16);
    /** The color of 255, 203, 0. */
    protected static Color priceColor = new Color(255, 203, 0);
    /** The color of 182, 182, 182. */
    protected static Color dimmedPriceColor = new Color(182, 182, 182);
    /** The color of 187, 187, 187. */
    protected static Color taxColor = new Color(187, 187, 187);
   
    /** The color of 187, 187, 187. */
    protected static Color exitBtnColor = new Color(236, 211, 143);

    protected static Color C246193001 = new Color(246,193, 1);
    protected static Color C249195000 = new Color(249,195, 0);
    protected static Color C067067067 = new Color(67, 67, 67);
    protected static Color C217071000 = new Color(217, 71, 0);
    protected static Color C050050050 = new Color(50, 50, 50);
    protected static Color C092092092 = new Color(92, 92, 92);
    protected static Color C021021021 = new Color(21, 21, 21);
    protected static Color C170170170 = new Color(170, 170, 170);
    protected static Color C044044044 = new Color(44, 44, 44);
    protected static Color C041041041 = new Color(41, 41, 41);
    protected static Color C066066066 = new Color(66, 66, 66);
    protected static Color C255191000 = new Color(255, 191, 0);
    protected static Color C157157157 = new Color(157, 157, 157);
    
    /** The color of 227, 227, 227, 171. */
    protected static DVBColor explainColor = new DVBColor(227, 227, 227, 171);
    /** The color of 17, 17, 17, 204. */
    protected static DVBColor dimmedBgColor = new DVBColor(17, 17, 17, 204);

    protected static DVBColor DC255255255026 = new DVBColor(255, 255, 255, 26);

    /** Constant of X coordinate screen title. */
    protected static final int TITLE_X = 60;
    /** Constant of Y coordinate for screen title. */
    protected static final int TITLE_Y = 98;
    /** Constant of X coordinate for explain under screen title. */
    protected static final int EXPLAIN_X = 60;
    /** Constant of Y coordinate for explain under screen title. */
    protected static final int EXPLAIN_Y = 120;
    /** Constant of X coordinate for 'Sorted by XXX'. */
    protected static final int SORTED_BY_X = 900;
    /** Constant of Y coordinate for 'Sorted by XXX'. */
    protected static final int SORTED_BY_Y = 120;

    /** a name of Cyo application. */
    private String cyoName;

    /**
     * Constructor.
     */
    public BaseRenderer() {
    }

    /**
     * Loads a images to display.
     * @param ui a ui to have a this renderer.
     */
    protected abstract void loadImages(BaseUI ui);

    /**
     * Get a bounds.
     * @param c a ui to have a this renderer.
     * @return a bounds
     * @see com.videotron.tvi.illico.framework.Renderer#getPreferredBounds
     *      (com.videotron.tvi.illico.framework.UIComponent)
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    /**
     * Paint.
     * @param g a Graphics object to paint.
     * @param c a ui to have a this renderer.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(java.awt.Graphics,
     *      com.videotron.tvi.illico.framework.UIComponent)
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        if (logoImg == null) {
            if (Log.INFO_ON) {
                Log.printInfo("log image from main menu via SharedMemory is null.");
            }
            logoImg = dataCenter.getImage("logo_en.png");
        }
        g.drawImage(logoImg, 53, 21, c);
        int stepX = 68 + logoImg.getWidth(c);

        if (cyoName != null) {
            g.setFont(f18);
            g.setColor(hisOverColor);
            g.drawImage(hisOverImg, stepX, 45, c);
            g.drawString(cyoName, stepX + 16, 57);
        }

        g.setFont(f17);
        g.setColor(Color.WHITE);
        String clock = ((BaseUI) c).getClockString();
        if (clock != null) {
//            g.drawImage(clockBgImg, 749, 37, c);
            GraphicUtil.drawStringRight(g, clock, 910, 57);
            g.drawImage(clockImg, 910 - fm17.stringWidth(clock) - 24, 44, c);
        }

        g.drawImage(hotkeyBgImg, 367, 466, c);
    }

    /**
     * Prepare a images and texts.
     * @param c a UI to have a this renderer.
     */
    public void prepare(UIComponent c) {
        loadCommonImages((BaseUI) c);
        logoImg = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        cyoName = BaseUI.getMenuText(KeyNames.CHANGE_YOUR_OPTION);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    /**
     * Load a common images.
     * @param ui a ui to have a this renderer.
     */
    private void loadCommonImages(BaseUI ui) {
        if (clockImg == null) {
            clockImg = ui.getImage("clock.png");
        }
//        if (clockBgImg == null) {
//            clockBgImg = ui.getImage("clock_bg.png");
//        }
        if (hisOverImg == null) {
            hisOverImg = ui.getImage("his_over.png");
        }
        if (bgImg == null) {
            bgImg = ui.getImage("bg.jpg");
        }
        if (hotkeyBgImg == null) {
            hotkeyBgImg = ui.getImage("01_hotkeybg.png");
        }
    }

}

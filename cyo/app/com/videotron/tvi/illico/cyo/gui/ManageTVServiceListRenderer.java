/**
 * @(#)ListRenderer.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ManageTVServiceListRenderer extends BaseRenderer {

	private final int LINE_STEP = 32;
	private final int LIST_HEIGHT = 326;

	private Image lineImg;
	private Image focusImg;
	private Image focus2Img;
	private Image delImg;
	private Image delFocImg;
	//    private Image shTopImg;
	//    private Image shBotImg;
	//    private Image cateShTopImg;
	//    private Image cateShBotImg;
	private Image arrowTopImg;
	private Image arrowBotImg;

	private Color blackColor = new Color(0, 0, 0);
	private Color unFocusedColor = new Color(230, 230, 230);
	private Color modifyDimColor = new Color(200, 200, 200);
	private Color infoColor = new Color(39, 39, 39);
	private Color infoDimColor = new Color(160, 160, 160);
	private Color focusColor = blackColor;

	private String viewStr;
	private String modifyStr;
	private String monthStr;

	private int focusHeight = 0;

	protected void paint(Graphics g, UIComponent c) {

		ListUI parentUI = (ListUI) c;
		Rectangle beforeRec = g.getClipBounds();
		g.clipRect(52, 135, 524, 326);
		int startIdx = parentUI.getStartIdx();
		int currentIdx = startIdx;
		int heightOfDrawn = 0;

		for (; heightOfDrawn < LIST_HEIGHT; ) {
			CyoObject obj = parentUI.getCyoObject(currentIdx);

			if (obj == null) {
				break;
			}

			if (obj instanceof Group && ((Group) obj).getUrl() == null) {
				g.setColor(blackColor);
				if (startIdx > 0) {
					heightOfDrawn += 3;
				}
				g.fillRect(52, 135 + heightOfDrawn, 524, 35);
				//                g.drawImage(cateShTopImg, 51, 128 + heightOfDrawn, c);
				//                g.drawImage(cateShBotImg, 52, 170 + heightOfDrawn, c);
				g.setFont(f21);
				g.setColor(Color.white);
				GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 311, 158 + heightOfDrawn);

				heightOfDrawn += 35;
			} else if (obj instanceof Group && ((Group) obj).getDesc() != null) {
				Group curObj = (Group) obj;
				String price = "";

				if (curObj.getPricetotal() != null) {
					String subFix = BaseUI.getPriceFreq(curObj.getPricefreq());
					price = curObj.getPricetotal() + subFix;
				}

				if (parentUI.hasFocus() && parentUI.getFocusType(currentIdx) == ListUI.FOCUS_ON_ME) {
					g.drawImage(focusImg, 48, 129 + heightOfDrawn, c);
					g.setFont(f20);
					g.setColor(focusColor);
					g.drawString(curObj.getTitle(), 64, 158 + heightOfDrawn);
					g.setFont(f18);
					GraphicUtil.drawStringRight(g, price, 562, 158 + heightOfDrawn);

					if (curObj.getCyoObject() != null) {
						g.setColor(infoColor);

						CyoObject[] cobj = curObj.getCyoObject();
						String title = "";
						for (int a = 0; cobj != null && a < cobj.length; a++) {
							Package p = (Package) cobj[a];
							if (p.getTitle() != null && p.getTitle().length() > 0) {
								if (a == cobj.length - 1) {
									title = title + p.getTitle();
								} else {
									title = title + p.getTitle() + ", ";
								}
							}
						}
						if (title != null && title.length() > 0) {
							String strTitle = TextUtil.shorten(title, g.getFontMetrics(), 345);
							g.drawString(strTitle, 66, 190 + heightOfDrawn);
						}
						// g.drawString("(" + curObj.getCyoObject().length + " packages)", 66, 190 + heightOfDrawn);
					} else {
						// g.setColor(infoColor);
						// g.drawString("(0 packages)", 66, 190 + heightOfDrawn);
					}

					g.drawImage(delFocImg, 441, 172 + heightOfDrawn, c);
					focusHeight = 172 + heightOfDrawn;
					// 1514 - Basic service
					// 1534 - HD defnition
					if ((((Group) obj).getNo()).equals("1534") || (((Group) obj).getNo()).equals("1514") && "false"
							.equals(((Group) obj).getModifiable())) {
						GraphicUtil.drawStringCenter(g, viewStr, 508, 188 + heightOfDrawn);
					} else {
						GraphicUtil.drawStringCenter(g, modifyStr, 508, 188 + heightOfDrawn);
					}
				} else {
					if (parentUI.getFocusType(currentIdx) == ListUI.FOCUS_ON_ME) {
						g.setFont(f20);
						g.drawImage(focusImg, 48, 129 + heightOfDrawn, c);
						g.setColor(focusColor);
					} else {
						g.setFont(f18);
						g.setColor(unFocusedColor);
					}

					g.drawString(curObj.getTitle(), 64, 158 + heightOfDrawn);
					g.setFont(f18);
					GraphicUtil.drawStringRight(g, price, 562, 158 + heightOfDrawn);

					g.setFont(f17);
					if (parentUI.getFocusType(currentIdx) == ListUI.FOCUS_ON_ME) {
						g.setColor(infoColor);
					} else {
						g.setColor(infoDimColor);
					}

					if (((Group) obj).getDesc() != null) {
						g.drawString(((Group) obj).getDesc(), 74, 185 + heightOfDrawn);
					} else if (curObj.getCyoObject() != null) {
						CyoObject[] cobj = curObj.getCyoObject();
						String title = "";
						for (int a = 0; cobj != null && a < cobj.length; a++) {
							Package p = (Package) cobj[a];
							if (p.getTitle() != null && p.getTitle().length() > 0) {
								if (a == cobj.length - 1) {
									title = title + p.getTitle();
								} else {
									title = title + p.getTitle() + ", ";
								}
							}
						}
						if (title != null && title.length() > 0) {
							String strTitle = TextUtil.shorten(title, g.getFontMetrics(), 345);
							g.drawString(strTitle, 66, 190 + heightOfDrawn);
						}
					}

					g.setColor(modifyDimColor);
					g.drawImage(delImg, 441, 170 + heightOfDrawn, c);

					// 1514 - Basic service
					// 1534 - HD defnition
					if ((((Group) obj).getNo()).equals("1534") || (((Group) obj).getNo()).equals("1514") && "false"
							.equals(((Group) obj).getModifiable())) {
						GraphicUtil.drawStringCenter(g, viewStr, 508, 188 + heightOfDrawn);
					} else {
						GraphicUtil.drawStringCenter(g, modifyStr, 508, 188 + heightOfDrawn);
					}
					if (heightOfDrawn + 46 < LIST_HEIGHT) {
						g.drawImage(lineImg, 64, 204 + heightOfDrawn, c);
					}
				}
				heightOfDrawn += 64;
			} else if (obj instanceof Group) {
				Group curObj = (Group) obj;

				if (parentUI.hasFocus() && parentUI.getFocusType(currentIdx) == ListUI.FOCUS_ON_ME) {
					g.drawImage(focus2Img, 51, 132 + heightOfDrawn, c);
					g.setFont(f20);
					g.setColor(focusColor);
					String titleStr = TextUtil.shorten(curObj.getTitle(), g.getFontMetrics(), 235);
					g.drawString(titleStr, 64, 160 + heightOfDrawn);

					// price
					g.setFont(f18);
					if (((Group) obj).getPricetotal() != null) {
						GraphicUtil.drawStringRight(g, ((Group) obj).getPricetotal() + " $" + monthStr, 425,
								159 + heightOfDrawn);
					}

					g.drawImage(delFocImg, 439, 141 + heightOfDrawn, c);

					// 1514 - Basic service
					// 1534 - HD defnition
					if ((((Group) obj).getNo()).equals("1534") || (((Group) obj).getNo()).equals("1514") && "false"
							.equals(((Group) obj).getModifiable())) {
						GraphicUtil.drawStringCenter(g, viewStr, 506, 158 + heightOfDrawn);
					} else {
						GraphicUtil.drawStringCenter(g, modifyStr, 506, 158 + heightOfDrawn);
					}

					focusHeight = 140 + heightOfDrawn;
				} else {
					if (parentUI.getFocusType(currentIdx) == ListUI.FOCUS_ON_ME) {
						g.setFont(f20);
						g.drawImage(focus2Img, 51, 132 + heightOfDrawn, c);
						g.setColor(focusColor);
					} else {
						g.setFont(f18);
						g.setColor(unFocusedColor);
					}
					String titleStr = TextUtil.shorten(curObj.getTitle(), g.getFontMetrics(), 235);
					g.drawString(titleStr, 64, 160 + heightOfDrawn);

					// price
					g.setFont(f18);
					if (((Group) obj).getPricetotal() != null) {
						if (parentUI.getFocusType(currentIdx) != ListUI.FOCUS_ON_ME) {
							g.setColor(dimmedPriceColor);
						}
						GraphicUtil.drawStringRight(g, ((Group) obj).getPricetotal() + " $" + monthStr, 425,
								159 + heightOfDrawn);
					}

					g.setColor(modifyDimColor);
					g.drawImage(delImg, 439, 141 + heightOfDrawn, c);
					if ((((Group) obj).getNo()).equals("1534") || (((Group) obj).getNo()).equals("1514") && "false"
							.equals(((Group) obj).getModifiable())) {
						GraphicUtil.drawStringCenter(g, viewStr, 508, 158 + heightOfDrawn);
					} else {
						GraphicUtil.drawStringCenter(g, modifyStr, 508, 158 + heightOfDrawn);
					}

					if (heightOfDrawn + 46 < LIST_HEIGHT) {
						g.drawImage(lineImg, 64, 168 + heightOfDrawn, c);
					}
				}
				heightOfDrawn += LINE_STEP;
			}
			currentIdx++;
		}

		// shadow
		//        if (parentUI.isDisplayTopDeco()) {
		//            g.drawImage(shTopImg, 52, 135, c);
		//        }
		//
		//        if (parentUI.isDisplayBottomDeco()) {
		//            g.drawImage(shBotImg, 52, 400, c);
		//        }

		g.setClip(beforeRec);

		// arrow.
		if (parentUI.isDisplayTopDeco()) {
			g.drawImage(arrowTopImg, 299, 126, c);
		}
		if (parentUI.isDisplayBottomDeco()) {
			g.drawImage(arrowBotImg, 299, 449, c);
		}
	}

	public void prepare(UIComponent c) {
		ListUI parentUI = (ListUI) c;
		loadImages(parentUI);

		modifyStr = BaseUI.getMenuText(KeyNames.MODIFY);
		viewStr = BaseUI.getMenuText(KeyNames.VIEW);

		monthStr = BaseUI.getMenuText(KeyNames.MONTH2);

		super.prepare(c);
	}

	public int getFocusHeight() {
		return focusHeight;
	}

	protected void loadImages(BaseUI ui) {
		lineImg = ui.getImage("17_slist_line_l.png");
		focusImg = ui.getImage("17_slist_foc.png");
		focus2Img = ui.getImage("17_slist_foc2.png");
		delImg = ui.getImage("17_del.png");
		delFocImg = ui.getImage("17_del_foc.png");
		//        shTopImg = ui.getImage("17_slist_sha_t.png");
		//        shBotImg = ui.getImage("17_slist_sha_b.png");
		//        cateShTopImg = ui.getImage("17_cate_glow_t.png");
		//        cateShBotImg = ui.getImage("17_cate_glow_b.png");
		arrowTopImg = ui.getImage("02_ars_t.png");
		arrowBotImg = ui.getImage("02_ars_b.png");
	}
}

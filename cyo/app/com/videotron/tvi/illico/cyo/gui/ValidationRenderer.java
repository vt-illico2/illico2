/**
 * @(#)InitialStateRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ValidationUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ValidationRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image infoBgImg;
    private Image btnTopImg;
    private Image btnMidImg;
    private Image btnBotImg;
    private Image btnArrowImg;
    private Image btnFocusImg;
    private Image btnLineImg;

    private String titleStr;
    private String explainStr;
    private String priceTitleStr;
    private String monthStr;
    private String[] btnStrs = new String[3];
    private String[] menuStrs = new String[2];
    private String fund;

    /**
     * 
     */
    public ValidationRenderer() {

    }

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        titleStr = BaseUI.getMenuText(KeyNames.VALIDATION_TITLE);
        explainStr = BaseUI.getMenuText(KeyNames.VALIDATION_EXPLAIN);
        monthStr = BaseUI.getMenuText(KeyNames.MONTH2);
        priceTitleStr = BaseUI.getMenuText(KeyNames.CHANGE_YOUR_BILL);

        menuStrs[0] = BaseUI.getMenuText(KeyNames.SEE_DETAIL2);
        menuStrs[1] = BaseUI.getMenuText(KeyNames.VIEW_MODIFIED_CHANNELS);

        btnStrs[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM3);
        btnStrs[1] = BaseUI.getMenuText(KeyNames.BTN_MAKE_OTHER_CHANGES);
        // VDTRMASTER-6037
        btnStrs[2] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        fund = BaseUI.getExplainText(KeyNames.FUND);
        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        ValidationUI ui = (ValidationUI) c;

        super.paint(g, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(titleStr, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        g.drawString(explainStr, EXPLAIN_X, EXPLAIN_Y);

        g.drawImage(listBgImg, 3, 133, c);
        if (ui.getList() != null) {
            ui.getList().paint(g);
        }
        
        
        // info
        String priceDollarStr = ((ValidationUI) c).getPriceDollar();
        String priceCentStr = ((ValidationUI) c).getPriceCent();

        g.drawImage(infoBgImg, 626, 134, c);
        g.setColor(C067067067);
        g.fillRect(637, 166, 261, 1);

        g.setColor(dimmedPriceColor);
        g.setFont(f18);
        g.drawString(priceTitleStr, 640, 157);

        // price
        g.setFont(f45);
        g.setColor(priceColor);
        GraphicUtil.drawStringRight(g, priceDollarStr, 765, 208);
        g.setFont(f14);
        g.drawString(priceCentStr, 767, 193);
        g.drawString(monthStr, 767, 208);

        // menu
        g.setFont(f18);
        g.setColor(btnColor);
        for (int i = 0; i < menuStrs.length; i++) {
            int step = i * 42;
            if (ui.getState() == ValidationUI.STATE_MENU && ui.getFocus() == i) {
                g.setColor(C246193001);
                g.fillRect(642, 227 + step, 252, 36);
                g.setColor(btnColor);
            } else {
                g.setColor(dimmedMenuInColor);
                g.fillRect(642, 227 + step, 252, 36);
                g.setColor(dimmedMenuColor);
            }
            GraphicUtil.drawStringCenter(g, menuStrs[i], 768, 250 + step);
        }

        // button
        g.drawImage(btnTopImg, 626, 341, c);
        g.drawImage(btnMidImg, 626, 384, c);
        g.drawImage(btnBotImg, 626, 420, 284, 40, c);
        g.drawImage(btnLineImg, 641, 381, c);
        g.drawImage(btnLineImg, 641, 419, c);

        for (int i = 0; i < btnStrs.length; i++) {
            int step = i * 39;
            if (ui.getState() == ValidationUI.STATE_BUTTON && ui.getBtnFocus() == i) {
                g.drawImage(btnFocusImg, 624, 341 + step, c);
                g.setColor(btnColor);
                g.setFont(f21);
                g.drawString(btnStrs[i], 671, 368 + step);
            } else {
                g.drawImage(btnArrowImg, 649, 359 + step, c);
                g.setColor(dimmedMenuColor);
                g.setFont(f18);
                g.drawString(btnStrs[i], 671, 367 + step);
            }
        }

        // comment by VDTRMASTER-5278 [CQ][PO1/2/3][R4.1][CYO] FAPL Text removal
//        g.setFont(f15);
//        g.setColor(taxColor);
//        g.drawString(fund, 56, 476);
    }

    protected final void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg2.png");

        infoBgImg = ui.getImage("17_mang_btbox2.png");

        btnTopImg = ui.getImage("01_acbg_1st.png");
        btnMidImg = ui.getImage("01_acbg_2nd.png");
        btnBotImg = ui.getImage("01_acbg_m.png");
        btnArrowImg = ui.getImage("02_detail_ar.png");
        btnFocusImg = ui.getImage("02_detail_bt_foc.png");
        btnLineImg = ui.getImage("01_acline.png");
    }
}

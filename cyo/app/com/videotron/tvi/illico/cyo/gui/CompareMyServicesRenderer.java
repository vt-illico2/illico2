/**
 * @(#)InitialStateRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.CompareListUI;
import com.videotron.tvi.illico.cyo.ui.CompareMyServicesUI;
import com.videotron.tvi.illico.cyo.ui.ValidationUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class CompareMyServicesRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image boxImg;
    private Image btnBigImg;
    private Image btnBigFocusImg;
    private Image btnImg;
    private Image btnFocusImg;
    private Image lineImg;
    private Image lineMidImg;
//    private Image shLeftImg;
//    private Image shRightImg;

    private String titleStr;
    private String explainStr;
    private String currentStr;
    private String modifiedStr;
    private String totalStr;
    private String monthStr;
    private String[] btnStrs = new String[3];
    private String fund;
    
    private Color subColor = new Color(214, 182, 61);

    /**
     * 
     */
    public CompareMyServicesRenderer() {

    }

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        titleStr = BaseUI.getMenuText(KeyNames.COMPARE_MY_SERVICES_TITLE);
        explainStr = BaseUI.getMenuText(KeyNames.COMPARE_MY_SERVICES_EXPLAIN);
        monthStr = BaseUI.getMenuText(KeyNames.MONTH);
        currentStr = BaseUI.getMenuText(KeyNames.YOUR_CURRENT_SERVICE);
        modifiedStr = BaseUI.getMenuText(KeyNames.YOUR_MODIFIED_SERVICE);
        totalStr = BaseUI.getMenuText(KeyNames.TOTAL);

        btnStrs[0] = BaseUI.getMenuText(KeyNames.BTN_LIST_MODIFIED_CHANNELS);
        btnStrs[1] = BaseUI.getMenuText(KeyNames.BTN_HELP);
        btnStrs[2] = BaseUI.getMenuText(KeyNames.BTN_BACK);
        fund = BaseUI.getExplainText(KeyNames.FUND);
        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        CompareMyServicesUI ui = (CompareMyServicesUI) c;

        super.paint(g, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(titleStr, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        g.drawString(explainStr, EXPLAIN_X, EXPLAIN_Y);

        g.drawImage(listBgImg, 3, 133, c);
        g.drawImage(boxImg, 69, 148, c);
        g.drawImage(lineMidImg, 469, 157, c);

        // left
        g.drawImage(lineImg, 81, 176, c);
        g.drawImage(lineImg, 81, 318, c);
        g.drawImage(lineImg, 81, 356, c);
        g.setFont(f20);
        g.setColor(Color.black);
        g.drawString(currentStr, 86, 169);
        g.setColor(subColor);
        g.drawString(currentStr, 85, 168);

        // left list
        CompareListUI leftList = ui.getLeftList();
        if (leftList != null) {
            leftList.paint(g);
        }

//        g.drawImage(shLeftImg, 81, 293, c);

        g.setColor(titleColor);
        g.drawString(totalStr, 83, 344);
        GraphicUtil.drawStringRight(g, ui.getCurrentPrice() + BaseUI.getPriceFreq("m"), 447, 344);

        // right
        g.drawImage(lineImg, 490, 176, c);
        g.drawImage(lineImg, 490, 318, c);
        g.drawImage(lineImg, 490, 356, c);
        g.setFont(f20);
        g.setColor(Color.black);
        g.drawString(modifiedStr, 497, 169);
        g.setColor(subColor);
        g.drawString(modifiedStr, 496, 168);

        // right list
        CompareListUI rightList = ui.getRightList();

        if (rightList != null) {
            rightList.paint(g);
        }

//        g.drawImage(shRightImg, 490, 294, c);

        g.setColor(titleColor);
        g.drawString(totalStr, 494, 344);
        GraphicUtil.drawStringRight(g, ui.getAfterPrice() + BaseUI.getPriceFreq("m"), 856, 344);

        g.setFont(f18);
        if (ui.getFocus() == 0) {
            g.drawImage(btnBigFocusImg, 487, 381, c);
            g.setColor(btnColor);
            GraphicUtil.drawStringCenter(g, btnStrs[0], 689, 401);

            g.setColor(dimmedMenuColor);
            g.drawImage(btnImg, 487, 420, c);
            GraphicUtil.drawStringCenter(g, btnStrs[1], 584, 440);
            g.drawImage(btnImg, 695, 420, c);
            GraphicUtil.drawStringCenter(g, btnStrs[2], 794, 440);

        } else if (ui.getFocus() == 1) {
            g.drawImage(btnBigImg, 487, 381, c);
            g.setColor(dimmedMenuColor);
            GraphicUtil.drawStringCenter(g, btnStrs[0], 689, 401);

            g.setColor(btnColor);
            g.drawImage(btnFocusImg, 487, 420, c);
            GraphicUtil.drawStringCenter(g, btnStrs[1], 584, 440);
            g.drawImage(btnImg, 695, 420, c);
            g.setColor(dimmedMenuColor);
            GraphicUtil.drawStringCenter(g, btnStrs[2], 794, 440);
        } else {
            g.drawImage(btnBigImg, 487, 381, c);
            g.setColor(dimmedMenuColor);
            GraphicUtil.drawStringCenter(g, btnStrs[0], 689, 401);

            g.drawImage(btnImg, 487, 420, c);
            GraphicUtil.drawStringCenter(g, btnStrs[1], 584, 440);
            g.drawImage(btnFocusImg, 695, 420, c);
            g.setColor(btnColor);
            GraphicUtil.drawStringCenter(g, btnStrs[2], 794, 440);
        }
        
        // comment by VDTRMASTER-5278 [CQ][PO1/2/3][R4.1][CYO] FAPL Text removal
//        g.setFont(f15);
//        g.setColor(taxColor);
//        g.drawString(fund, 56, 476);
    }

    protected final void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_compare_bg.png");
        boxImg = ui.getImage("17_com_box.png");
        btnBigImg = ui.getImage("17_btn_406.png");
        btnImg = ui.getImage("17_btn_198.png");
        btnBigFocusImg = ui.getImage("17_focus_414.png");
        btnFocusImg = ui.getImage("17_focus_206.png");
        lineImg = ui.getImage("17_com_line.png");
        lineMidImg = ui.getImage("17_com_line_m.png");
//        shLeftImg = ui.getImage("17_com_sha_l.png");
//        shRightImg = ui.getImage("17_com_sha_r.png");

    }
}

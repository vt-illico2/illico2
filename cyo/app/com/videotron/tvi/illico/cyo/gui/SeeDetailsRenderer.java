/**
 * @(#)InitialStateRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.SeeDetailsUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * CYO_032
 * @author Woojung Kim
 * @version 1.1
 */
public class SeeDetailsRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image listBtnImg;
    private Image listBtnFocusImg;
    private Image btn1stImg;
//    private Image btnShImg;
//    private Image btnGlowImg;
    private Image btnArrowImg;
    private Image btnFocusImg;
    private Image infoBgImg;

    private String titleStr;
    private String explainStr;
    private String btnStr;
    private String menuBtnStr;
    private String totalStr;
    private String monthStr;

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        titleStr = BaseUI.getMenuText(KeyNames.SEE_DETAILS_TITLE);
        explainStr = BaseUI.getMenuText(KeyNames.SEE_DETAILS_EXPLAIN);

        btnStr = BaseUI.getMenuText(KeyNames.BTN_BACK);

        menuBtnStr = BaseUI.getMenuText(KeyNames.LIST_OF_CHANNELS2);
        totalStr = BaseUI.getMenuText(KeyNames.TOTAL);
        monthStr = BaseUI.getMenuText(KeyNames.MONTH);
        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        SeeDetailsUI ui = (SeeDetailsUI) c;

        super.paint(g, c);

        if(titleStr == null)  return;
        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(titleStr, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        g.drawString(explainStr, EXPLAIN_X, EXPLAIN_Y);

        g.drawImage(listBgImg, 3, 133, c);
        if (ui.getList() != null) {
            ui.getList().paint(g);
        }

        // info
        g.drawImage(infoBgImg, 626, 134, c);
        g.setColor(C067067067);
        g.fillRect(637, 166, 261, 1);
        g.setFont(f16);
        g.setColor(dimmedPriceColor);
        g.drawString(totalStr, 640, 157);

        String priceDollarStr = ui.getPriceDollar();
        String priceCentStr = ui.getPriceCent();

        g.setFont(f45);
        g.setColor(priceColor);
        GraphicUtil.drawStringRight(g, priceDollarStr, 773, 227);
        g.setFont(f19);
        g.drawString(priceCentStr, 775, 212);
        g.drawString(monthStr, 775, 226);

        // menu button
        g.setFont(f18);
        if (ui.getFocus() == 0) {
            g.setColor(C246193001);
            g.fillRect(642, 269, 252, 36);
            g.setColor(btnColor);
        } else {
            g.setColor(dimmedMenuInColor);
            g.fillRect(642, 269, 252, 36);
            g.setColor(dimmedMenuColor);
        }
        GraphicUtil.drawStringCenter(g, menuBtnStr, 768, 292);

        // button
        g.drawImage(btn1stImg, 626, 418, c);
//        g.drawImage(btnShImg, 579, 451, c);
//        g.drawImage(btnGlowImg, 625, 439, c);

        if (ui.getFocus() == 1) {
            g.drawImage(btnFocusImg, 624, 419, c);
            g.setColor(btnColor);
            g.setFont(f21);
            g.drawString(btnStr, 671, 446);
        } else {
            g.drawImage(btnArrowImg, 649, 434, c);
            g.setColor(dimmedMenuColor);
            g.setFont(f18);
            g.drawString(btnStr, 671, 443);
        }
        
        // comment by VDTRMASTER-5278 [CQ][PO1/2/3][R4.1][CYO] FAPL Text removal
//        g.setFont(f15);
//        g.setColor(taxColor);
//        g.drawString(fund, 56, 476);

    }

    protected final void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg2.png");

        listBtnImg = ui.getImage("12_pos_focus_dim.png");
        listBtnFocusImg = ui.getImage("12_pos_focus.png");
        btn1stImg = ui.getImage("01_acbg_1st.png");
//        btnShImg = ui.getImage("01_ac_shadow.png");
//        btnGlowImg = ui.getImage("01_acglow.png");
        btnArrowImg = ui.getImage("02_detail_ar.png");
        btnFocusImg = ui.getImage("02_detail_bt_foc.png");

        infoBgImg = ui.getImage("17_mang_btbox2.png");
    }
}

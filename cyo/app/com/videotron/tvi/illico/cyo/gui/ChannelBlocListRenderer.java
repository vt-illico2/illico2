/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

import java.awt.*;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ChannelBlocListRenderer extends ListRenderer {

    private final int LINE_STEP = 32;
    private final int LIST_HEIGHT = 292;

    private Image lineImg;
    private Image focusImg;
    private Image checkBoxImg;
    private Image checkBoxFocImg;
    private Image checkFocImg;
    private Image checkImg;
    private Image focusBgTopImg;
    private Image focusBgMidImg;
    private Image focusBgBotImg;
    private Image focusBgTopDimImg;
    private Image focusBgMidDimImg;
    private Image focusBgBotDimImg;
    private Image focusDimImg;
//    private Image explainShImg;
//    private Image shTopImg;
//    private Image shBotImg;
    private Image scrBarImg;
    private Image scrBgTopImg;
    private Image scrBgMidImg;
    private Image scrBgBotImg;
    private Image arrowTopImg;
    private Image arrowBotImg;

    private Color blackColor = new Color(0, 0, 0);
    private Color unFocusedColor = new Color(230, 230, 230);
    private Color focusBgColor = new Color(132, 132, 132);
    private Color modifyDimColor = new Color(200, 200, 200);
    private Color infoColor = new Color(39, 39, 39);
    private Color infoDimColor = new Color(160, 160, 160);
    private Color focusColor = blackColor;

    protected void paint(Graphics g, UIComponent c) {

        ListUI parentUI = (ListUI) c;
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(57, 145, 638, 324);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Package) {
                Package curObj = (Package) obj;
                int focusType = parentUI.getFocusType(currentIdx);

                Image checkBImg = null;
                Image cImg = null;
                if (focusType == ListUI.FOCUS_ON_ME) {
                    
                    checkBImg = checkBoxFocImg;
                    if (parentUI.hasFocus()) {
                        // g.drawImage(focusBgBotImg, 57, 201 + heightOfDrawn, c);
                        g.drawImage(focusImg, 57, 143 + heightOfDrawn, c);
                    } else {
                        g.setColor(focusBgColor);
                        g.fillRect(57, 142 + heightOfDrawn, 636, 36);

                    }
                    cImg = checkFocImg;
                    g.setFont(f20);
                    g.setColor(blackColor);
                } else {
                    checkBImg = checkBoxImg;
                    cImg = checkImg;
                    if (heightOfDrawn + 46 < LIST_HEIGHT) {
                        g.drawImage(lineImg, 64, 178 + heightOfDrawn, c);
                    }
                    g.setFont(f18);
                    g.setColor(unFocusedColor);
                }

                g.drawImage(checkBImg, 71, 154 + heightOfDrawn, c); // 76, 176
                if (curObj.getSubscribedInSession()) {
                    g.drawImage(cImg, 73, 153 + heightOfDrawn, c);
                }

                String title = curObj.getTitle();
                if (parentUI.isWithPrice()) {
                    title = TextUtil.shorten(title, g.getFontMetrics(), 440);
                } else {
                    title = TextUtil.shorten(title, g.getFontMetrics(), 550);
                }
                g.drawString(title, 99, 169 + heightOfDrawn);

                String price = curObj.getPrice();
                if (price != null) {
                    GraphicUtil.drawStringRight(g, price + BaseUI.getPriceFreq(curObj.getPricefreq()), 677,
                            168 + heightOfDrawn);
                }

                heightOfDrawn += LINE_STEP;
            }
            currentIdx++;
        }

        // shadow
//        if (parentUI.isDisplayTopDeco()) {
//            g.drawImage(shTopImg, 56, 166, c);
//        }
//
//        if (parentUI.isDisplayBottomDeco()) {
//            g.drawImage(shBotImg, 56, 395, c);
//        }

        g.setClip(beforeRec);

        // arrow.
        if (parentUI.isDisplayTopDeco()) {
            g.drawImage(arrowTopImg, 364, 158, c);
        }
        if (parentUI.isDisplayBottomDeco()) {
            g.drawImage(arrowBotImg, 364, 449, c);
        }
    }

    public void prepare(UIComponent c) {
        ListUI parentUI = (ListUI) c;
        loadImages(parentUI);

        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        lineImg = ui.getImage("17_list_line.png");
//        focusImg = ui.getImage("17_slist_foc.png");
        checkBoxImg = ui.getImage("check_box.png");
        checkBoxFocImg = ui.getImage("check_box_foc.png");
        checkFocImg = ui.getImage("check_foc.png");
        checkImg = ui.getImage("check.png");
        focusBgTopImg = ui.getImage("17_rim01_t.png");
        focusBgMidImg = ui.getImage("17_rim01_m.png");
        focusBgBotImg = ui.getImage("17_rim01_b.png");
        focusImg = ui.getImage("17_list_foc.png");
        focusBgTopDimImg = ui.getImage("17_rim01_dim_t.png");
        focusBgMidDimImg = ui.getImage("17_rim01_dim_m.png");
        focusBgBotDimImg = ui.getImage("17_rim01_dim_b.png");
        focusDimImg = ui.getImage("17_list_foc_dim.png");
//        explainShImg = ui.getImage("17_list_sha_b.png");
//        shTopImg = ui.getImage("17_sha_top.png");
//        shBotImg = ui.getImage("17_sha_bottom.png");
        scrBarImg = ui.getImage("scr_bar.png");
        scrBgTopImg = ui.getImage("scrbg_up.png");
        scrBgMidImg = ui.getImage("scr_m.png");
        scrBgBotImg = ui.getImage("scrbg_dn.png");
        arrowTopImg = ui.getImage("02_ars_t.png");
        arrowBotImg = ui.getImage("02_ars_b.png");
    }
}

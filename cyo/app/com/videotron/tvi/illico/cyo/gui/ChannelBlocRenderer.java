/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChannelBlocUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

import java.awt.*;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ChannelBlocRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image btnGrayDimImg;
    private String title;
    private String[] explains;
    private String[] buttons;

    /**
     *
     */
    public ChannelBlocRenderer() {
        buttons = new String[3];

    }

    public void prepare(UIComponent c) {
        ChannelBlocUI ui = (ChannelBlocUI) c;

        loadImages((BaseUI) c);

        title = BaseUI.getMenuText(KeyNames.CHANNEL_BLOC_LIST);
        String temp = BaseUI.getExplainText(KeyNames.CHANNEL_BLOC_LIST);
        explains = TextUtil.split(temp, fm17, 840);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        ChannelBlocUI ui = (ChannelBlocUI) c;

        g.drawImage(listBgImg, 50, 138, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        if (explains != null) {
            for (int i = 0; i < explains.length; i++) {
                int step = i * 20;
                g.drawString(explains[i], EXPLAIN_X, EXPLAIN_Y + step);
            }
        }

        // list
        ListUI currentList = ui.getCurrentList();
        if (currentList != null) {
            currentList.paint(g);
        }

        boolean isChanged = ui.getChanged();
        g.setFont(f18);
        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getButtonFocus() == i && ui.getState() == ChannelBlocUI.STATE_BUTTON) {
                g.drawImage(btnFocusImg, 717, 346 + step, c);
                g.setColor(btnColor);
            } else {
                if (!isChanged && i == 0) {                   
                    g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuInColor);
                } else {
                    g.drawImage(btnDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuColor);
                }
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 367 + step);
        }
        
        // comment by VDTRMASTER-5278 [CQ][PO1/2/3][R4.1][CYO] FAPL Text removal
//        g.setFont(f15);
//        g.setColor(taxColor);
//        g.drawString(fund, 56, 476);
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        btnGrayDimImg = ui.getImage("17_btn_gray_in.png");
    }
}

/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Fee;
import com.videotron.tvi.illico.cyo.data.obj.Game;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Product;
import com.videotron.tvi.illico.cyo.data.obj.Service;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.ui.ValidateListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ValidateListRenderer extends BaseRenderer {

    private final int LINE_STEP = 64;
    private final int LIST_HEIGHT = 325;

    private Image sLineImg;
    private Image packageLineImg;
//    private Image shTopImg;
//    private Image shBotImg;
    private Image scrBarImg;
    private Image scrBgTopImg;
    private Image scrBgMidImg;
    private Image scrBgBotImg;
    private Image scrBgImg;
//    private Image cateShTopImg;
//    private Image cateShBotImg;

    protected Color bgColor = new Color(166, 166, 166);
    protected Color blackColor = new Color(0, 0, 0);
    protected Color unFocusedColor = new Color(230, 230, 230);
    protected Color focusBgColor = new Color(132, 132, 132);
    protected Color focusColor = blackColor;
    private Color modifiedColor = new Color(121, 230, 130);

    protected void paint(Graphics g, UIComponent c) {

        ValidateListUI parentUI = (ValidateListUI) c;
        // g.translate(-56, -169);
        // 56, 169
        // 638 x 32 * N 개
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(50, 135, 529, 323);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;

        g.setFont(f18);
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

//            if (obj instanceof Group) {
//                String title = ((Group) obj).getTitle();
//                if(title.equals(BaseUI.getMenuText(KeyNames.COMPARE_GROUP_TITLE))) {
////                    g.setColor(blackColor);
////                    g.fillRect(52, 135 + heightOfDrawn, 524, 32);
////                    g.drawImage(cateShTopImg, 51, 128 + heightOfDrawn, c);
////                    g.drawImage(cateShBotImg, 52, 170 + heightOfDrawn, c);
//                    g.setFont(f21);
//                    g.setColor(Color.white);
//                    GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 311, 158 + heightOfDrawn);
//                } else {
//                    g.setColor(dimmedMenuColor);
//                    g.setFont(f18);
//                    g.drawString(title, 65, 157 + heightOfDrawn);
//                    if (currentIdx > 0) {
//                        g.drawImage(packageLineImg, 64, 134 + heightOfDrawn, c);
//                    }
//                }
//                heightOfDrawn += LINE_STEP;
//
//            } else if (obj instanceof Fee) {
//                String title = ((Fee) obj).getTitle();
//                String price = ((Fee) obj).getPrice() + BaseUI.getPriceFreq(((Fee) obj).getPricefreq());
//                String modified = ((Fee) obj).getModified();
//
//                if (modified != null && modified.equals("true")) {
//                    g.setColor(modifiedColor);
//                } else {
//                    g.setColor(dimmedPriceColor);
//                }
//                title = TextUtil.shorten(title, fm18, 320);
//                g.drawString(title, 106, 157 + heightOfDrawn);
//                GraphicUtil.drawStringRight(g, price, 553, 157 + heightOfDrawn);
//                if (heightOfDrawn + LINE_STEP < LIST_HEIGHT) {
//                    g.drawImage(sLineImg, 96, 134 + heightOfDrawn, c);
//                }
//                heightOfDrawn += LINE_STEP;
//            }

            if (obj instanceof Group) {
                Group group = (Group) obj;

                String titleStr = group.getTitle();
                String descStr = group.getDesc();

                g.setFont(f18);
                g.setColor(dimmedMenuColor);
                g.drawString(titleStr, 64, 163 + heightOfDrawn);

//                descStr = "testsetsetsetsetr";
                if (descStr != null) {
                    descStr = TextUtil.shorten(descStr, fm18, 485);
                    g.setFont(f17);
                    g.setColor(C246193001);
                    g.drawString(descStr, 74, 185 + heightOfDrawn);
                }

                if (heightOfDrawn + LINE_STEP < LIST_HEIGHT) {
                    g.drawImage(sLineImg, 64, 201 + heightOfDrawn, c);
                }
                heightOfDrawn += LINE_STEP;
            }
            currentIdx++;
        }

        // scroll
        if (parentUI.needScroll()) {
            g.drawImage(scrBgImg, 530, 135, c);
            g.drawImage(scrBgTopImg, 560, 147, c);
            g.drawImage(scrBgMidImg, 560, 157, 7, 282, c);
            g.drawImage(scrBgBotImg, 560, 438, c);

            int scrGap = parentUI.getScrollGap();

            g.drawImage(scrBarImg, 557, 143 + scrGap, c);
        }

        g.setClip(beforeRec);
        // g.translate(56, 169);
    }

    public void prepare(UIComponent c) {
        ValidateListUI parentUI = (ValidateListUI) c;
        loadImages(parentUI);

        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        sLineImg = ui.getImage("17_slist_line_l.png");
        scrBarImg = ui.getImage("scr_bar.png");
        scrBgTopImg = ui.getImage("scrbg_up.png");
        scrBgMidImg = ui.getImage("scr_m.png");
        scrBgBotImg = ui.getImage("scrbg_dn.png");
        scrBgImg = ui.getImage("16_scr_bg.png");
//        cateShTopImg = ui.getImage("17_cate_glow_t.png");
//        cateShBotImg = ui.getImage("17_cate_glow_b.png");
    }
}

/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.*;
import java.util.Hashtable;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChangeChannelSelectionUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a channel selection.
 * @author Woojung Kim
 * @version 1.1
 */
public class ChangeChannelSelectionRenderer extends BaseRenderer {

    /** a image of list background. */
    private Image listBgImg;
    /** a image of menu line. */
    private Image menuLineImg;
    /** a image of menu glow. */
//    private Image menuGlowImg;
    /** a image of menu focus. */
    private Image menuFocusImg;
    /** a image of menu dimmed. */
    private Image menuDimImg;
    /** a image of button focus. */
    private Image btnFocusImg;
    /** a image of button dimmed. */
    private Image btnDimImg;
    /** a image of count background. */
    private Image countBgImg;
    
    private Image btnGrayDimImg;

    /** a array of titles. */
    private String titleStr;
    /** a explain. */
    private String explainStr;
    /** a button name array. */
    private String[] buttons;
    /** a string of selected channels. */
    private String selectedChannels;
    /** a string about tax. */
    private String tax;

    private String fund;
    
    // R5 - SM5
    private Image icon5CImg;
    private String custom5CPackgeStr;

    private Image selectChannelsBoxImg;
    private Image slashImg;
    private Image btnAImg;

    /**
     * create a arrays and load a strings.
     */
    public ChangeChannelSelectionRenderer() {
        buttons = new String[3];
    }

    /**
     * Prepare a renderer.
     * @param c a UI to have a this renderer.
     */
    public void prepare(UIComponent c) {
        ChangeChannelSelectionUI ui = (ChangeChannelSelectionUI) c;
        loadImages(ui);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        tax = BaseUI.getExplainText(KeyNames.TAX);
        fund = BaseUI.getExplainText(KeyNames.FUND);
        selectedChannels = BaseUI.getMenuText(KeyNames.SELECTED_CHANNELS);

        // R5 - SM5
        custom5CPackgeStr = BaseUI.getMenuText(KeyNames.CUSTOM_5PACKGE);

        titleStr = BaseUI.getMenuText(KeyNames.CHANGE_CUSTOM_CHANNELS);
        explainStr = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS);

        Hashtable footerHashtable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        btnAImg = (Image) footerHashtable.get(PreferenceService.BTN_A);
        
        super.prepare(c);
    }

    public void update(ChangeChannelSelectionUI ui) {

        titleStr = BaseUI.getMenuText(KeyNames.CHANGE_CUSTOM_CHANNELS);
        explainStr = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS);

        if (CyoDataManager.getInstance().getCurrentPackageInPackageList() != null) {
            // explain
            int numberOfChannel = CyoDataManager.getInstance().getMyNumberOfChannel();
            int selectedChannel = Integer.valueOf(ui.getCurrentList().getSelectedChannel()).intValue();

            if (numberOfChannel > selectedChannel) {
                if ((numberOfChannel - selectedChannel) > 1) {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_ADD_MULTI);
                    explainStr = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                } else {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_ADD_SINGLE);
                    explainStr = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                }
            } else if (numberOfChannel < selectedChannel) {
                if ((numberOfChannel - selectedChannel) < -1) {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_REMOVE_MULTI);
                    explainStr = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                } else {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_REMOVE_SINGLE);
                    explainStr = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                }
            }
        }
    }

    /**
     * Paint a renderer.
     * @param g a Graphics to paint.
     * @param c a UI to have a this renderer.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        ChangeChannelSelectionUI ui = (ChangeChannelSelectionUI) c;

        g.drawImage(listBgImg, 50, 138, c);

        // title
        if (titleStr != null) {
            g.setColor(titleColor);
            g.setFont(f26);
            g.drawString(titleStr, TITLE_X, TITLE_Y);
        }

        // explain
        if (explainStr != null) {
            g.setFont(f17);
            g.setColor(explainColor);

            if (ui.getFocus() > 0) {
                String[] splits = TextUtil.split(explainStr, fm17, 840, "|");
                for (int i = 0; i < splits.length; i++) {
                    int step = i * 20;
                    g.drawString(splits[i], EXPLAIN_X, EXPLAIN_Y + step);
                }
            } else {
                g.drawString(explainStr, EXPLAIN_X, EXPLAIN_Y);
            }
        }

        // list
        ListUI currentList = ui.getCurrentList();

        if (currentList != null) {
            // sort
            g.setFont(f17);
            int x = GraphicUtil.drawStringRight(g, ui.getSortString(), SORTED_BY_X, SORTED_BY_Y);
            g.drawImage(btnAImg, x - 25, 105, c);
            currentList.paint(g);
        }

        if (CyoDataManager.getInstance().getCurrentPackageInPackageList() != null) {
            // selected channels
            g.drawImage(selectChannelsBoxImg, 719, 156, c);
            g.setFont(f18);
            g.setColor(hisOverColor);
            GraphicUtil.drawStringCenter(g, selectedChannels, 804, 187);

            String selectedCount = "0";
            if (currentList != null) {
                selectedCount = currentList.getSelectedChannel();
            }
            int selectedCountInt = Integer.valueOf(selectedCount).intValue();
            // VDTRMASTER-6118
            int myNumberOfChannel = ui.getNumberOfChannels();

            int selectedCountLength = fm60.stringWidth(String.valueOf(selectedCount));
            int myNumberOfChannelLength = fm60.stringWidth(String.valueOf(myNumberOfChannel));
            int halfLength = (selectedCountLength + myNumberOfChannelLength + 22 + 8) / 2;

            int startX = 804 - halfLength;

            if (selectedCountInt > myNumberOfChannel) {
                g.setColor(C217071000);
            } else {
                g.setColor(C246193001);
            }
            g.setFont(f60);
            g.drawString(selectedCount, startX, 245);
            startX += selectedCountLength + 4;
            g.drawImage(slashImg, startX, 209, c);
            startX += 26;
            g.setColor(C246193001);
            g.drawString(myNumberOfChannel + "", startX, 245);
        }

        // buttons
        g.setFont(f18);
        boolean isChanged = ui.getChanged();
        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getState() == ChangeChannelSelectionUI.STATE_BUTTON) {
                if (ui.getButtonFocus() == i) {
                    g.drawImage(btnFocusImg, 717, 346 + step, c);
                    g.setColor(btnColor);
                } else {                    
                    g.setFont(f18);
                    if (!isChanged && i == 0) { 
                        g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                        g.setColor(dimmedMenuInColor);
                    } else {
                        g.setColor(dimmedMenuColor);
                        g.drawImage(btnDimImg, 718, 347 + step, c);
                    }
                }
            } else {                
                if (!isChanged && i == 0) { 
                    g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuInColor);
                } else {
                    g.drawImage(btnDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuColor);
                }
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 367 + step);
        }

        if (CyoDataManager.getInstance().needToDisplay5CBanner()) {
            // R5 - SM5
            g.setFont(f18);
            g.setColor(dimmedMenuColor);
            int x = GraphicUtil.drawStringCenter(g, custom5CPackgeStr, 816, 291);
            g.drawImage(icon5CImg, x - 23, 279, c);
        }
    }

    /**
     * Load a images.
     * @param ui a UI to have this renderer.
     */
    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        menuLineImg = ui.getImage("17_menu_line.png");
//        menuGlowImg = ui.getImage("17_menu_glow.png");
        menuFocusImg = ui.getImage("17_m_foc.png");
        menuDimImg = ui.getImage("17_m_foc_dim.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        countBgImg = ui.getImage("17_cus_tab.png");
        btnGrayDimImg = ui.getImage("17_btn_gray_in.png");
        
        // R5 - SM5
        icon5CImg = ui.getImage("icon_5.png");

        selectChannelsBoxImg = ui.getImage("17_selch_box.png");
        slashImg = ui.getImage("17_selch_slash.png");

    }
}

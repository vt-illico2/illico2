/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChangeChannelSelectionUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.ui.SelfServeUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class SelfServeRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image menuLineImg;
//    private Image menuGlowImg;
    private Image menuFocusImg;
    private Image menuDimImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image btnGrayDimImg;
    private Image countBgImg;

    private String title;
    private String explain;
    private String myTitle;
    private String myExplain;
    private String[] menus;
    private String[] buttons;
    private String selectedChannels;
    
    // R5 - SM5
    private Image icon5CImg;
    private String custom5CPackgeStr;

    private Image selectChannelsBoxImg;
    private Image slashImg;
    private Image btnAImg;

    /**
     * 
     */
    public SelfServeRenderer() {
        menus = new String[2];
        buttons = new String[3];

        title = BaseUI.getMenuText(KeyNames.CHANGE_CUSTOM_CHANNELS);
        explain = BaseUI.getExplainText(KeyNames.EXPLAIN);
        
        myTitle = BaseUI.getMenuText(KeyNames.CHANGE_CUSTOM_MYPICKS);
        myExplain = BaseUI.getExplainText(KeyNames.EXPLAIN_MYPICKS);
    }

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        menus[1] = BaseUI.getMenuText(KeyNames.MY_PICKS);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        selectedChannels = BaseUI.getMenuText(KeyNames.SELECTED_CHANNELS);
        
        // R5 - SM5
        custom5CPackgeStr = BaseUI.getMenuText(KeyNames.CUSTOM_5PACKGE);

        Hashtable footerHashtable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        btnAImg = (Image) footerHashtable.get(PreferenceService.BTN_A);

        super.prepare(c);
    }

    public void update(UIComponent c) {

        explain = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS);
        // explain
        if (((SelfServeUI) c).getCurrentList() != null) {
            int numberOfChannel = CyoDataManager.getInstance().getMyNumberOfChannel();
            int selectedChannel = Integer.valueOf(((SelfServeUI) c).getCurrentList().getSelectedChannel()).intValue();

            if (numberOfChannel > selectedChannel) {
                if ((numberOfChannel - selectedChannel) > 1) {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_ADD_MULTI);
                    explain = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                } else {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_ADD_SINGLE);
                    explain = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                }
            } else if (numberOfChannel < selectedChannel) {
                if ((numberOfChannel - selectedChannel) < -1) {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_REMOVE_MULTI);
                    explain = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                } else {
                    String temp = BaseUI.getExplainText(KeyNames.CHANGE_CUSTOM_CHANNELS_REMOVE_SINGLE);
                    explain = TextUtil.replace(temp, "[N]", String.valueOf(numberOfChannel - selectedChannel));
                }
            }
        }
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        SelfServeUI ui = (SelfServeUI) c;

        g.drawImage(listBgImg, 50, 138, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(ui.getFocus() == 0 ? title : myTitle, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        
        if(ui.getFocus() == 0) {
            g.drawString(explain, EXPLAIN_X, EXPLAIN_Y);
        } else {
           String my[] =  TextUtil.split(myExplain, g.getFontMetrics(), 840, "|");
            
           for(int a= 0; a < my.length; a++) {
               g.drawString(my[a], EXPLAIN_X, EXPLAIN_Y + (20 * a));
           }
        }

        // list
        ListUI currentList = ui.getCurrentList();

        if (currentList != null) {
            if (ui.getFocus() == 0) {
                // sort
                g.setFont(f17);
                int x = GraphicUtil.drawStringRight(g, ui.getSortString(), SORTED_BY_X, SORTED_BY_Y);
                g.drawImage(btnAImg, x - 25, 105, c);
            }
            currentList.paint(g);
        }

        // selected channels
        g.drawImage(selectChannelsBoxImg, 719, 156, c);
        g.setFont(f18);
        g.setColor(hisOverColor);
        GraphicUtil.drawStringCenter(g, selectedChannels, 804, 187);

        String selectedCount ="0";
        if (currentList != null) {
            selectedCount = currentList.getSelectedChannel();
        }
        int selectedCountInt = Integer.valueOf(selectedCount).intValue();
        int myNumberOfChannel = CyoDataManager.getInstance().getMyNumberOfChannel();

        int selectedCountLength = fm60.stringWidth(String.valueOf(selectedCount));
        int myNumberOfChannelLength = fm60.stringWidth(String.valueOf(myNumberOfChannel));
        int halfLength = (selectedCountLength + myNumberOfChannelLength + 22 + 8) / 2;

        int startX = 804 - halfLength;

        if (selectedCountInt > myNumberOfChannel) {
            g.setColor(C217071000);
        } else {
            g.setColor(C246193001);
        }
        g.setFont(f60);
        g.drawString(selectedCount, startX, 245);
        startX += selectedCountLength + 4;
        g.drawImage(slashImg, startX, 209, c);
        startX += 26;
        g.setColor(C246193001);
        g.drawString(myNumberOfChannel + "", startX, 245);

        g.setFont(f18);
        boolean isChanged = ui.getChanged();
        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getState() == ChangeChannelSelectionUI.STATE_BUTTON) {
                if (ui.getButtonFocus() == i) {
                    g.drawImage(btnFocusImg, 717, 346 + step, c);
                    g.setColor(btnColor);
                } else {                    
                    if (!(isChanged) && i == 0) {                                    
                        g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                        g.setColor(dimmedMenuInColor);
                    } else {
                        g.drawImage(btnDimImg, 718, 347 + step, c);
                        g.setColor(dimmedMenuColor);
                    }
                }
            } else {
                if (!(isChanged) && i == 0) {                
                    g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuInColor);
                } else {
                    g.drawImage(btnDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuColor);
                }
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 367 + step);
        }

        if (CyoDataManager.getInstance().needToDisplay5CBanner()) {
            // R5 - SM5
            g.setFont(f18);
            g.setColor(dimmedMenuColor);
            int x = GraphicUtil.drawStringCenter(g, custom5CPackgeStr, 816, 318);
            g.drawImage(icon5CImg, x - 23, 306, c);
        }
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        menuLineImg = ui.getImage("17_menu_line.png");
//        menuGlowImg = ui.getImage("17_menu_glow.png");
        menuFocusImg = ui.getImage("17_m_foc.png");
        menuDimImg = ui.getImage("17_m_foc_dim.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        btnGrayDimImg = ui.getImage("17_btn_gray_in.png");
        countBgImg = ui.getImage("17_cus_tab.png");
        
        // R5 - SM5
        icon5CImg = ui.getImage("icon_5.png");

        selectChannelsBoxImg = ui.getImage("17_selch_box.png");
        slashImg = ui.getImage("17_selch_slash.png");
    }
}

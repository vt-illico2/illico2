/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChangeChannelSelectionUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.ui.ManageGroupUI;
import com.videotron.tvi.illico.cyo.ui.PopularPackagesUI;
import com.videotron.tvi.illico.cyo.ui.SelfServeUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ManageGroupRenderer extends BaseRenderer {

    private Image listBgImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image btnGrayDimImg;
//    private Image menuGlowImg;

    private String title;
    private String[] explains;
    private String[] buttons;
    
    private String fund;
    private String menu;

    private Image btnAImg;

    /**
     * 
     */
    public ManageGroupRenderer() {
        buttons = new String[3];
    }

    public void prepare(UIComponent c) {
        ManageGroupUI ui = (ManageGroupUI) c;
        loadImages((BaseUI) c);

        title = ui.getTitle();
        String temp = ui.getExplain();
        explains = TextUtil.split(temp, fm17, 840, "|");
        fund = BaseUI.getExplainText(KeyNames.FUND);
        menu = BaseUI.getMenuText(KeyNames.LIST_OF_CHANNELS);
        
        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        Hashtable footerHashtable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        btnAImg = (Image) footerHashtable.get(PreferenceService.BTN_A);

        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        ManageGroupUI ui = (ManageGroupUI) c;

        g.drawImage(listBgImg, 50, 138, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, TITLE_X, TITLE_Y);

        // explain
        g.setColor(explainColor);
        if (explains != null) {
            if (explains.length > 1) {
                g.setFont(f17);
                for (int i = 0; i < explains.length; i++) {
                    int step = i * 17;
                    g.drawString(explains[i], EXPLAIN_X, EXPLAIN_Y + step - 2);
                }
            } else {
                g.setFont(f20);
                for (int i = 0; i < explains.length; i++) {
                    int step = i * 20;
                    g.drawString(explains[i], EXPLAIN_X, EXPLAIN_Y + step);
                }
            }
        }

        // list
        ListUI currentList = ui.getCurrentList();

        if (currentList != null) {

            if (ui.isALaCarte()) {
                // sort
                g.setFont(f17);
                int x = GraphicUtil.drawStringRight(g, ui.getSortString(), SORTED_BY_X, SORTED_BY_Y);
                g.drawImage(btnAImg, x - 25, 105, c);
            }

            currentList.paint(g);
        }
        
        if(currentList != null && ui.isBasicService()) {
            CyoObject curObj = currentList.getCurrentCyoObject();
            Package curPackage = ((Package) curObj);
            if(curPackage.getSelection() == null){ // && !curPackage.getSelection().equals(KeyNames.SINGLE)
//                g.drawImage(menuGlowImg, 698, 309, c);
    
                g.setFont(f18);
                if (ui.getState() == ManageGroupUI.STATE_MENU) {
                    g.drawImage(btnFocusImg, 717, 236, c);
                    g.setColor(btnColor);
                } else {
                    g.drawImage(btnDimImg, 718, 237, c);
                    g.setColor(dimmedMenuColor);
                }
                GraphicUtil.drawStringCenter(g, menu, 804, 257);
            }
        }

        boolean isChanged = ui.getChanged();
        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getState() == ManageGroupUI.STATE_BUTTON) {
                if (ui.getButtonFocus() == i) {
                    g.drawImage(btnFocusImg, 717, 346 + step, c);
                    g.setColor(btnColor);
                    g.setFont(f21);
                } else {
                    g.setFont(f18);
                    if (!isChanged && i == 0) { 
                        g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                        g.setColor(dimmedMenuInColor);
                    } else {
                        g.setColor(dimmedMenuColor);
                        g.drawImage(btnDimImg, 718, 347 + step, c);
                    }
                }
            } else {
                g.setFont(f18);
                if (!isChanged && i == 0) { 
                    g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuInColor);
                } else {
                    g.drawImage(btnDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuColor);
                }
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 367 + step);
        }
        
        // comment by VDTRMASTER-5278 [CQ][PO1/2/3][R4.1][CYO] FAPL Text removal
//        g.setFont(f15);
//        g.setColor(taxColor);
//        g.drawString(fund, 56, 476);
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        btnGrayDimImg = ui.getImage("17_btn_gray_in.png");
//        menuGlowImg = ui.getImage("17_menu_glow.png");
    }
}

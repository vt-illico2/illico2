/**
 * @(#)NotificationRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.NotificationPopupUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class display a Notification PopCallerIDUI.
 * @author Woojung Kim
 * @version 1.1
 */
public class NotificationRenderer extends BaseRenderer {

//    private Image glowTopImg;
//    private Image glowMidImg;
//    private Image glowBotImg;
//    private Image glowBgImg;
//    private Image shaImg;
//    private Image highImg;
//    private Image high402Img;
    private Image gapImg;
    private Image notiIconImg;
    private Image btnImg;
    private Image btnFocusImg;
    private Image btn293Img;
    private Image btn293FocusImg;
    private Image notiIconOrangeImg;
    private Image btnExit;
    private Image iconMaple;

    private Color bgColor = new Color(23, 23, 23);
    private Color titleColor = new Color(252, 202, 4);

    private NotificationPopupUI parent;

    private String yesStr;
    private String noStr;
    private String okStr;
    private String cancelStr;
    private String[] explainStr;
    private String addRemoveChannelStr;
    private String keepMySelectionStr;
    
    private String mapleString;

    private int explainPosY;

    protected void paint(Graphics g, UIComponent c) {
        NotificationPopupUI noti = (NotificationPopupUI)c;
        g.setColor(dimmedBgColor);
        g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        if (c.getState() == NotificationPopupUI.STATE_CANCEL || c.getState() == NotificationPopupUI.STATE_NORMAL_CANCEL
                || c.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL || c.getState() == NotificationPopupUI.STATE_CANCEL_CHANGE) {
            g.setColor(bgColor);
            g.fillRect(306, 143, 350, 224);
//            g.drawImage(glowTopImg, 276, 113, c);
//            g.drawImage(glowMidImg, 276, 193, 410, 124, c);
//            g.drawImage(glowBotImg, 276, 317, c);
//            g.drawImage(shaImg, 306, 364, 350, 79, c);
//            g.drawImage(highImg, 306, 143, c);
            g.drawImage(gapImg, 285, 181, c);

            // title
            g.setFont(f24);
            g.setColor(titleColor);
            int x = GraphicUtil.drawStringCenter(g, parent.getTitle(), 499, 169);
            if (c.getState() == NotificationPopupUI.STATE_CANCEL) {
                g.drawImage(notiIconImg, x - 31, 151, c);
            } else {
                g.drawImage(notiIconOrangeImg, x - 31, 149, c);
            }
            // explain
            g.setFont(f18);
            g.setColor(Color.white);

            String[] explainTexts;
            if (c.getState() == NotificationPopupUI.STATE_CANCEL && parent.getExplain() == null) {
                explainTexts = explainStr;
            } else {
                explainTexts = parent.getExplain();
                Log.printInfo("explainTexts "  + explainTexts);
                for(int a = 0; explainTexts != null && a < explainTexts.length; a++) {
                    Log.printInfo(explainTexts[a]);
                }
            }

            if (explainTexts != null) {
                explainPosY = 253 - explainStr.length / 2 * 20;
                for (int i = 0; i < explainTexts.length; i++) {
                    int step = i * 20;
                    GraphicUtil.drawStringCenter(g, explainTexts[i], 483, explainPosY + step);
                }
            }

            // button
            g.setColor(btnColor);
            g.setFont(f18);
            if (c.getFocus() == 0) {
                g.drawImage(btnFocusImg, 321, 318, c);
                g.drawImage(btnImg, 485, 318, c);
            } else {
                g.drawImage(btnImg, 321, 318, c);
                g.drawImage(btnFocusImg, 485, 318, c);
            }

            GraphicUtil.drawStringCenter(g, yesStr, 399, 339);
            GraphicUtil.drawStringCenter(g, noStr, 564, 339);

            if (c.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
                g.setColor(Color.white);
                GraphicUtil.drawStringRight(g, cancelStr, 648, 386);
                int strCancelWidth = g.getFontMetrics().stringWidth(cancelStr);
                if (btnExit != null) {
                    int btnExitW = btnExit.getWidth(c);
                    g.drawImage(btnExit, 648 - strCancelWidth - 5 - btnExitW, 371, c);
                }
            }

        } else if (c.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
//            g.drawImage(glowBgImg, 250, 95, c);
//            g.drawImage(shaImg, 280, 414, 402, 79, c);
            g.setColor(bgColor);
            g.fillRect(280, 125, 402, 292);
//            g.drawImage(high402Img, 280, 125, c);
            g.drawImage(gapImg, 292, 163, c);

            // title
            g.setFont(f24);
            g.setColor(titleColor);
            int x = GraphicUtil.drawStringCenter(g, parent.getTitle(), 494, 152);
            g.drawImage(notiIconOrangeImg, x - 31, 132, c);

            // explain
            String[] explains = parent.getExplain();
            if (explains != null) {
                int posY = 250 - explains.length / 2 * 20;

                g.setFont(f18);
                g.setColor(Color.white);
                for (int i = 0; i < explains.length; i++) {
                    int step = i * 20;
                    GraphicUtil.drawStringCenter(g, explains[i], 483, posY + step);
                }
            }

            g.setColor(btnColor);
            g.setFont(f18);
            if (c.getFocus() == 0) {
                g.drawImage(btn293FocusImg, 333, 331, c);
                g.drawImage(btn293Img, 334, 368, c);
            } else {
                g.drawImage(btn293Img, 334, 331, c);
                g.drawImage(btn293FocusImg, 333, 368, c);
            }

            GraphicUtil.drawStringCenter(g, addRemoveChannelStr, 480, 352);
            GraphicUtil.drawStringCenter(g, keepMySelectionStr, 480, 389);
        } else {
//            g.drawImage(glowBgImg, 250, 95, c);
//            g.drawImage(shaImg, 280, 414, 402, 79, c);
            g.setColor(bgColor);
            g.fillRect(280, 125, 402, 292);
//            g.drawImage(high402Img, 280, 125, c);
            g.drawImage(gapImg, 292, 163, c);

            // title
            g.setFont(f24);
            g.setColor(titleColor);
            int x = GraphicUtil.drawStringCenter(g, parent.getTitle(), 494, 152);
            if (c.getState() != NotificationPopupUI.STATE_UNSELECT_MESSAGE_FOR_A_LA_CARTE) {
                g.drawImage(notiIconOrangeImg, x - 31, 132, c);
            }

            // explain
            String[] explains = parent.getExplain();
            if (explains != null) {
                int posY = 270 - explains.length / 2 * 20;

                g.setFont(f18);
                g.setColor(Color.white);
                for (int i = 0; i < explains.length; i++) {
                    int step = i * 20;
                    GraphicUtil.drawStringCenter(g, explains[i], 483, posY + step);
                }
            }

            g.setColor(btnColor);
            g.setFont(f18);

            g.drawImage(btnFocusImg, 403, 368, c);
            GraphicUtil.drawStringCenter(g, okStr, 480, 389);
        }
    }

    public void prepare(UIComponent c) {
        parent = (NotificationPopupUI) c;

        loadImages((BaseUI) c);

        String tmp = BaseUI.getExplainText(KeyNames.CANCEL_YOUR_SELECTION);
        explainStr = TextUtil.split(tmp, fm18, 310, '|');
        explainPosY = 253 - explainStr.length / 2 * 20;

        okStr = BaseUI.getMenuText(KeyNames.BTN_OK);
        cancelStr = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        yesStr = BaseUI.getMenuText(KeyNames.BTN_YES);
        noStr = BaseUI.getMenuText(KeyNames.BTN_NO);
        addRemoveChannelStr = BaseUI.getMenuText(KeyNames.ADD_REMOVE_CHANNELS);
        keepMySelectionStr = BaseUI.getMenuText(KeyNames.KEEP_MY_SELECTION);
        super.prepare(c);
    }

    protected static Hashtable imgButtonTable;

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.cyo.gui.BaseRenderer#loadImages(com.videotron.tvi.illico.cyo.ui.BaseUI)
     */
    protected void loadImages(BaseUI ui) {
        iconMaple = ui.getImage("icon_canada.png");
//        glowTopImg = ui.getImage("05_pop_glow_t.png");
//        glowMidImg = ui.getImage("05_pop_glow_m.png");
//        glowBotImg = ui.getImage("05_pop_glow_b.png");
//        shaImg = ui.getImage("pop_sha.png");
//        highImg = ui.getImage("pop_high_350.png");
        gapImg = ui.getImage("pop_gap_379.png");
        notiIconImg = ui.getImage("icon_noti_red.png");
        btnImg = ui.getImage("05_focus_dim.png");
        btnFocusImg = ui.getImage("05_focus.png");

        Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        if (imgTable != null) {
            btnExit = (Image) imgTable.get(PreferenceService.BTN_EXIT);
        }
//        glowBgImg = ui.getImage("05_pop_glow_353.png");
//        high402Img = ui.getImage("pop_high_402.png");
        btn293Img = ui.getImage("btn_293.png");
        btn293FocusImg = ui.getImage("btn_293_foc.png");
        notiIconOrangeImg = ui.getImage("icon_noti_or.png");
    }

}

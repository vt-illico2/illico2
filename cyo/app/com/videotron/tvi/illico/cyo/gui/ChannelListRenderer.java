/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a channel list.
 * @author Woojung Kim
 * @version 1.1
 */
public class ChannelListRenderer extends ListRenderer {

    /** a step by line. */
    private final int LINE_STEP = 32;
    /** a height of list. */
    private final int LIST_HEIGHT = 324;

    /** a image of line. */
    private Image lineImg;
    /** a image of focus. */
    private Image focusImg;
    /** a image of focus background Top. */
    private Image focusBgTopImg;
    /** a image of focus background Middle. */
    private Image focusBgMidImg;
    /** a image of focus background Bottom. */
    private Image focusBgBotImg;
    /** a image of focus background dimmed Top. */
    private Image focusBgTopDimImg;
    /** a image of focus background dimmed Middle. */
    private Image focusBgMidDimImg;
    /** a image of focus background dimmed Bottom. */
    private Image focusBgBotDimImg;
    /** a image of focus dimmed. */
    private Image focusDimImg;
    /** a image of explain shadow. */
//    private Image explainShImg;
    /** a image of shadow on top. */
//    private Image shTopImg;
    /** a image of shadow on bottom. */
//    private Image shBotImg;
    /** a image of scroll bar. */
    private Image scrBarImg;
    /** a image of scroll background on top. */
    private Image scrBgTopImg;
    /** a image of scroll background on middle. */
    private Image scrBgMidImg;
    /** a image of scroll background on bottom. */
    private Image scrBgBotImg;
    /** a image of canada icon. */
    private Image iconCanadaImg;
    /** a image of hd icon. */
    private Image iconHDImg;
    /** a image of focused hd icon. */
    private Image iconHDFocImg;
    /** a image of illico icon. */
    private Image iconIllicoImg;
    /** a image of french icon. */
    private Image iconFrImg;
    /** a image of english icon. */
    private Image iconEnImg;
    /** a image of focused french icon. */
    private Image iconFrFocImg;
    /** a image of focused english icon. */
    private Image iconEnFocImg;
    /** a image of arrow on top. */
    private Image arrowTopImg;
    /** a image of arrow on bottom. */
    private Image arrowBotImg;

    /** a black color. */
    private Color blackColor = new Color(0, 0, 0);
    /** a unfocused color. */
    private Color unFocusedColor = new Color(230, 230, 230);
    /** a focus background color. */
    private Color focusBgColor = new Color(132, 132, 132);
    /** a modify dimmed color. */
    private Color modifyDimColor = new Color(200, 200, 200);
    /** a information color. */
    private Color infoColor = new Color(39, 39, 39);
    /** a dimmed information color. */
    private Color infoDimColor = new Color(160, 160, 160);
    /** a focus color. */
    private Color focusColor = blackColor;

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c a UI to have a this renderer.
     */
    protected void paint(Graphics g, UIComponent c) {

        ListUI parentUI = (ListUI) c;
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(57, 145, 638, 324);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Channel) {

                int focusType = parentUI.getFocusType(currentIdx);
                int descHeight = 0;

                if (focusType == ListUI.FOCUS_ON_ME) {
                    descHeight = getDescriptionHeight(parentUI, heightOfDrawn);// drawDescription(g, parentUI,
                                                                               // heightOfDrawn);
                } else {
                    if (heightOfDrawn + 46 < LIST_HEIGHT) {
                        g.drawImage(lineImg, 64, 178 + heightOfDrawn, c);
                    }
                    g.setFont(f18);
                    g.setColor(unFocusedColor);

                    Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

                    if (chInfo != null) {
                        String no = chInfo.getHdPosition();
                        if (no != null) {
                            g.drawString(no, 71, 168 + heightOfDrawn);
                        } else {
                            no = chInfo.getPosition();
                            g.drawString(no, 71, 168 + heightOfDrawn);
                        }

                        // Logo - get from epg via SharedMemory
//                        Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
                        Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());

                        if (logoImg != null) {
                            g.drawImage(logoImg, 149, 150 + heightOfDrawn, 80, 24, c);
                        }

                        // title
                        String title = chInfo.getTitle();
                        if (title != null) {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 330);
                            g.drawString(title, 248, 169 + heightOfDrawn);
                        }

                        // R5 - SM5
                        // 5C
                        // VDTRMASTER-5899
                        if (chInfo.isChaineSurMesure5() && CyoDataManager.getInstance().needToDisplay5CBanner()) {
                        	Image fiveIcon = null;
                        	if (focusType == ListUI.FOCUS_ON_ME) {
                        		fiveIcon = icon5CFocusImg;
                        	} else {
                        		fiveIcon = icon5CImg;
                        	}
                        	g.drawImage(fiveIcon, 569, 156 + heightOfDrawn, c);
                        }

                        // language
                        String language = chInfo.getLanguage();
                        Image langIcon = null;
                        if (language != null) {
                            if (language.equals(Constants.LANGUAGE_FRENCH)) {
                                if (focusType == ListUI.FOCUS_ON_ME) {
                                    langIcon = iconFrFocImg;
                                } else {
                                    langIcon = iconFrImg;
                                }
                            } else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
                                if (focusType == ListUI.FOCUS_ON_ME) {
                                    langIcon = iconEnFocImg;
                                } else {
                                    langIcon = iconEnImg;
                                }
                            }
                        }
                        g.drawImage(langIcon, 594, 156 + heightOfDrawn, c);

                        // R5 - SM5 remove
//                        // illico
//                        String illico = chInfo.getTelecino();
//                        if (illico != null) {
//                            if (!illico.equals("false")) { // not illico 1
//                                g.drawImage(iconIllicoImg, 601, 176 + heightOfDrawn, c);
//                            }
//                        }

                        // HD
                        boolean hd = chInfo.hasHDPosition();
                        Image hdIcon = null;
                        if (hd) {
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                hdIcon = iconHDFocImg;
                            } else {
                                hdIcon = iconHDImg;
                            }
                            g.drawImage(hdIcon, 624, 156 + heightOfDrawn, c);
                        }

                        // R5 - SM5
                        // pay
                        if (chInfo.isChainePrestige()) {
                        	Image payIcon = null;
                        	 if (focusType == ListUI.FOCUS_ON_ME) {
                        		 payIcon = iconPayFocusImg;
                             } else {
                            	 payIcon = iconPayImg;
                             }
                             g.drawImage(payIcon, 654, 156 + heightOfDrawn, c);
                        }


                        // R5 - SM5 remove
//                        // Canadian
//                        if (chInfo.getCanadian()) {
//                            g.drawImage(iconCanadaImg, 653, 176 + heightOfDrawn, c);
//                        }
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            }
            currentIdx++;
        }

        // shadow
//        if (parentUI.isDisplayTopDeco()) {
//            g.drawImage(shTopImg, 56, 166, c);
//        }
//
//        if (parentUI.isDisplayBottomDeco()) {
//            g.drawImage(shBotImg, 56, 395, c);
//        }

        startIdx = parentUI.getStartIdx();
        currentIdx = startIdx;
        heightOfDrawn = 0;

        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Channel) {

                int focusType = parentUI.getFocusType(currentIdx);
                int descHeight = 0;

                if (focusType == ListUI.FOCUS_ON_ME) {

                    descHeight = drawDescription(g, parentUI, heightOfDrawn);

                    if (parentUI.hasFocus()) {
                        g.drawImage(focusImg, 57, 142 + heightOfDrawn, parentUI);
                    } else {
                        g.setColor(focusBgColor);
                        g.fillRect(61, 147 + heightOfDrawn, 628, 32);
                    }
                    // VDTRMASTER-5918
                    g.setFont(f20);
                    g.setColor(focusColor);

                    Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(((Channel) obj).getId());

                    if (chInfo != null) {
                        String no = chInfo.getHdPosition();
                        if (no != null) {
                            g.drawString(no, 71, 168 + heightOfDrawn);
                        } else {
                            no = chInfo.getPosition();
                            g.drawString(no, 71, 168 + heightOfDrawn);
                        }

                        // Logo - get from epg via SharedMemory
//                        Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getCallLetters());
                        Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());

                        if (logoImg != null) {
                            g.drawImage(logoImg, 149, 150 + heightOfDrawn, 80, 24, c);
                        }

                        // title
                        String title = chInfo.getTitle();
                        if (title != null) {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 330);
                            g.drawString(title, 248, 169 + heightOfDrawn);
                        }

                     // R5 - SM5
                        // 5C
                        // VDTRMASTER-5899
                        if (chInfo.isChaineSurMesure5() && CyoDataManager.getInstance().needToDisplay5CBanner()) {
                        	Image fiveIcon = null;
                        	if (focusType == ListUI.FOCUS_ON_ME) {
                        		fiveIcon = icon5CFocusImg;
                        	} else {
                        		fiveIcon = icon5CImg;
                        	}
                        	g.drawImage(fiveIcon, 569, 156 + heightOfDrawn, c);
                        }
                        
                        // language
                        String language = chInfo.getLanguage();
                        Image langIcon = null;
                        if (language != null) {
                            if (language.equals(Constants.LANGUAGE_FRENCH)) {
                                if (focusType == ListUI.FOCUS_ON_ME) {
                                    langIcon = iconFrFocImg;
                                } else {
                                    langIcon = iconFrImg;
                                }
                            } else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
                                if (focusType == ListUI.FOCUS_ON_ME) {
                                    langIcon = iconEnFocImg;
                                } else {
                                    langIcon = iconEnImg;
                                }
                            }
                        }
                        g.drawImage(langIcon, 594, 156 + heightOfDrawn, c);

                        // R5 - SM5 remove
//                        // illico
//                        String illico = chInfo.getTelecino();
//                        if (illico != null) {
//                            if (!illico.equals("false")) { // not illico 1
//                                g.drawImage(iconIllicoImg, 601, 176 + heightOfDrawn, c);
//                            }
//                        }

                        // HD
                        boolean hd = chInfo.hasHDPosition();
                        Image hdIcon = null;
                        if (hd) {
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                hdIcon = iconHDFocImg;
                            } else {
                                hdIcon = iconHDImg;
                            }
                            g.drawImage(hdIcon, 624, 156 + heightOfDrawn, c);
                        }
                        
                        // R5 - SM5
                        // pay
                        if (chInfo.isChainePrestige()) {
                        	Image payIcon = null;
                        	 if (focusType == ListUI.FOCUS_ON_ME) {
                        		 payIcon = iconPayFocusImg;
                             } else {
                            	 payIcon = iconPayImg;
                             }
                             g.drawImage(payIcon, 654, 156 + heightOfDrawn, c);
                        }
                        

                        // R5 - SM5 remove
//                        // Canadian
//                        if (chInfo.getCanadian()) {
//                            g.drawImage(iconCanadaImg, 653, 176 + heightOfDrawn, c);
//                        }
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            }
            currentIdx++;
        }

        g.setClip(beforeRec);

        // arrow.
        if (parentUI.isDisplayTopDeco()) {
            g.drawImage(arrowTopImg, 364, 134, c);
        }
        if (parentUI.isDisplayBottomDeco()) {
            g.drawImage(arrowBotImg, 364, 461, c);
        }
    }

    // VDTRMASTER-5902
    // To use for Basic channel instead it in ListRenderer.
    public int getDescriptionHeight(ListUI parentUI, int heightOfDrawn) {
        String[] desc = parentUI.getDesc();
        CyoObject currentObj = parentUI.getCurrentCyoObject();

        if (desc == null) {
            if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
                return heightOfDrawn + 99;
            }
            return heightOfDrawn + 32;
        }

        if (currentObj instanceof Package && currentObj.getCyoObject() != null) {
            heightOfDrawn += 131;
        } else {
            heightOfDrawn += 99;
        }
        return heightOfDrawn;
    }

    /**
     * Paint a description.
     * @param g Graphics to paint.
     * @param parentUI a UI to have a this renderer.
     * @param heightOfDrawn a last height to be drawn.
     * @return a height to add a height of description.
     */
    public int drawDescription(Graphics g, ListUI parentUI, int heightOfDrawn) {
        String[] desc = parentUI.getDesc();

        if (desc == null) {
            return heightOfDrawn + 32;
        }

        if (parentUI.hasFocus()) {
            g.setColor(bgColor);
            g.fillRect(61, 147 + heightOfDrawn, 628, 94);
            g.drawImage(focusBgMidImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
            g.drawImage(focusBgTopImg, 57, 145 + heightOfDrawn, parentUI);
            g.drawImage(focusBgBotImg, 57, 209 + heightOfDrawn, parentUI);
        } else {
            g.setColor(bgColor);
            g.fillRect(61, 147 + heightOfDrawn, 628, 94);
            g.drawImage(focusBgMidDimImg, 57, 149 + heightOfDrawn, 636, 60, parentUI);
            g.drawImage(focusBgTopDimImg, 57, 145 + heightOfDrawn, parentUI);
            g.drawImage(focusBgBotDimImg, 57, 209 + heightOfDrawn, parentUI);
        }

        g.setFont(f14d);
        g.setColor(blackColor);
        int descStartIdx = parentUI.getDescStartIdx();
        int count = 0;
        for (int i = descStartIdx; i < descStartIdx + 3; i++) {
            if (i < desc.length && desc[i] != null) {
                g.drawString(desc[i], 99, 199 + heightOfDrawn + count * 17);
                count++;
            }
        }

        if (desc.length > 3) {
//            if (descStartIdx + 3 < desc.length) {
//                g.drawImage(explainShImg, 61, 238 + heightOfDrawn, parentUI);
//            } else if (descStartIdx > 0) {
//                g.drawImage(explainShImg, 61, 193 + heightOfDrawn, parentUI);
//            }

            // scroll
            g.drawImage(scrBgTopImg, 668, 189 + heightOfDrawn, 7, 10, parentUI);
            g.drawImage(scrBgMidImg, 668, 199 + heightOfDrawn, 7, 25, parentUI);
            g.drawImage(scrBgBotImg, 668, 224 + heightOfDrawn, parentUI);

            int step = parentUI.getScrollGap();
            g.drawImage(scrBarImg, 665, 189 + heightOfDrawn + step, parentUI);
        }
        heightOfDrawn += 99;
        return heightOfDrawn;
    }

    /**
     * Prepare a renderer.
     * @param c a UI to have this renderer.
     */
    public void prepare(UIComponent c) {
        ListUI parentUI = (ListUI) c;
        loadImages(parentUI);

        super.prepare(c);
    }

    /**
     * Load a images.
     */
    protected void loadImages(BaseUI ui) {
        lineImg = ui.getImage("17_list_line.png");
        focusImg = ui.getImage("17_slist_foc.png");
        focusBgTopImg = ui.getImage("17_rim01_t.png");
        focusBgMidImg = ui.getImage("17_rim01_m.png");
        focusBgBotImg = ui.getImage("17_rim01_b.png");
        focusImg = ui.getImage("17_list_foc.png");
        focusBgTopDimImg = ui.getImage("17_rim01_dim_t.png");
        focusBgMidDimImg = ui.getImage("17_rim01_dim_m.png");
        focusBgBotDimImg = ui.getImage("17_rim01_dim_b.png");
        focusDimImg = ui.getImage("17_list_foc_dim.png");
//        explainShImg = ui.getImage("17_list_sha_b.png");
//        shTopImg = ui.getImage("17_sha_top.png");
//        shBotImg = ui.getImage("17_sha_bottom.png");
        scrBarImg = ui.getImage("scr_bar.png");
        scrBgTopImg = ui.getImage("scrbg_up.png");
        scrBgMidImg = ui.getImage("scr_m.png");
        scrBgBotImg = ui.getImage("scrbg_dn.png");
        iconCanadaImg = ui.getImage("icon_canada.png");
        iconHDImg = ui.getImage("icon_hd.png");
        iconHDFocImg = ui.getImage("icon_hd_foc.png");
        iconIllicoImg = ui.getImage("icon_illico.png");
        iconFrImg = ui.getImage("icon_fr.png");
        iconEnImg = ui.getImage("icon_en.png");
        iconFrFocImg = ui.getImage("icon_fr_foc.png");
        iconEnFocImg = ui.getImage("icon_en_foc.png");
        arrowTopImg = ui.getImage("02_ars_t.png");
        arrowBotImg = ui.getImage("02_ars_b.png");
        
        // R5 - SM5
        icon5CImg = ui.getImage("icon_5.png");
        icon5CFocusImg = ui.getImage("icon_5_foc.png");
        iconPayImg = ui.getImage("icon_pay.png");
        iconPayFocusImg = ui.getImage("icon_pay_foc.png");
        
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
}

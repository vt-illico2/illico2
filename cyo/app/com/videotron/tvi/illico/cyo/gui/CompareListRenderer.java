/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Fee;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.CompareListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class CompareListRenderer extends BaseRenderer {

    private Image scrBgTopImg;
    private Image scrBgMidImg;
    private Image scrBgBotImg;
    private Image scrBarImg;
    
//    private Image cateShTopImg;
//    private Image cateShBotImg;
    
    private Color blackColor = new Color(0, 0, 0);
    private int gapX;

    protected void paint(Graphics g, UIComponent c) {

        CompareListUI parentUI = (CompareListUI) c;
        int startIdx = parentUI.getStartIdx();
        g.setFont(f18);
        boolean checkGroup = false;
        for (int i = 0; i < 6; i++) {
            CyoObject obj = parentUI.getCyoObject(startIdx + i);

            if (obj == null) {
                break;
            }
            int step = 0;
            String title = "", price = "";
            Log.printDebug("obj == " + obj);
            if (obj instanceof Group) {
                g.setColor(blackColor);
                g.fillRect(52, 135 + step, 524, 35);
//                g.drawImage(cateShTopImg, 51, 128 + step, c);
//                g.drawImage(cateShBotImg, 52, 170 + step, c);
                g.setFont(f21);
                g.setColor(Color.white);
                GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 311, 158 + step);
                checkGroup = true;
//                step += 35;
            } else {
                step = checkGroup ? i * 22 + 13 : i * 22;  
                
                if (obj instanceof Fee) {
                    title = ((Fee) obj).getTitle();
                    price = ((Fee) obj).getPrice() + BaseUI.getPriceFreq(((Fee) obj).getPricefreq());
                } else if (obj instanceof Package) {
                    title = ((Package) obj).getTitle();
                    price = ((Package) obj).getPrice() + BaseUI.getPriceFreq(((Package) obj).getPricefreq());
                }
    
                title = TextUtil.shorten(title, g.getFontMetrics(), 230);
                g.setColor(dimmedMenuColor);
                g.drawString(title, 83 + gapX, 200 + step);
                g.setColor(titleColor);
                GraphicUtil.drawStringRight(g, price, 442 + gapX, 200 + step);
            }
        }

        // scroll
        if (parentUI.needScroll()) {
            g.drawImage(scrBgTopImg, 876, 187, c);
            g.drawImage(scrBgMidImg, 876, 197, 7, 102, c);
            g.drawImage(scrBgBotImg, 876, 299, c);

            int scrGap = parentUI.getScrollGap();

            g.drawImage(scrBarImg, 873, 186 + scrGap, c);
        }
    }

    public void prepare(UIComponent c) {
        CompareListUI parentUI = (CompareListUI) c;
        loadImages(parentUI);

        if (parentUI.showScroll()) {
            gapX = 409;
        } else {
            gapX = 0;
        }
        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        scrBarImg = ui.getImage("scr_bar.png");
        scrBgTopImg = ui.getImage("scrbg_up.png");
        scrBgMidImg = ui.getImage("scr_m.png");
        scrBgBotImg = ui.getImage("scrbg_dn.png");
//        cateShTopImg = ui.getImage("17_cate_glow_t.png");
//        cateShBotImg = ui.getImage("17_cate_glow_b.png");
    }
}

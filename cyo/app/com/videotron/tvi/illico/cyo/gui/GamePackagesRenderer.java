/**
 * @(#)ChangeChannelSelectionRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Game;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.ChangeChannelSelectionUI;
import com.videotron.tvi.illico.cyo.ui.GamePackagesUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.cyo.ui.PopularPackagesUI;
import com.videotron.tvi.illico.cyo.ui.SelfServeUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GamePackagesRenderer extends BaseRenderer {

    private Image listBgImg;
//    private Image menuGlowImg;
    private Image btnFocusImg;
    private Image btnDimImg;
    private Image btnGrayDimImg;
    
    private String title;
    private String[] explains;
    private String menu;
    private String menu2;
    private String[] buttons;

    /**
     * 
     */
    public GamePackagesRenderer() {
        buttons = new String[3];
    }

    public void prepare(UIComponent c) {
        loadImages((BaseUI) c);

        menu = BaseUI.getMenuText(KeyNames.LIST_OF_GAMES);
        menu2 = BaseUI.getMenuText(KeyNames.SELECT_GAMES);

        title = BaseUI.getMenuText(KeyNames.GAME_PACKAGES);
        String temp = BaseUI.getExplainText(KeyNames.GAME_PACKAGES);
        
        explains = TextUtil.split(temp, fm17, 840);

        buttons[0] = BaseUI.getMenuText(KeyNames.BTN_CONFIRM);
        buttons[1] = BaseUI.getMenuText(KeyNames.BTN_CANCEL);
        buttons[2] = BaseUI.getMenuText(KeyNames.BTN_HELP);

        super.prepare(c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        GamePackagesUI ui = (GamePackagesUI) c;

        g.drawImage(listBgImg, 3, 158, c);

        // title
        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, TITLE_X, TITLE_Y);

        // explain
        g.setFont(f17);
        g.setColor(explainColor);
        if (explains != null) {
            for (int i = 0; i < explains.length; i++) {
                int step = i * 20;
                g.drawString(explains[i], EXPLAIN_X, EXPLAIN_Y + step);
            }
        }

        // right menu
//        g.drawImage(menuGlowImg, 698, 309, c);

        // list
        ListUI currentList = ui.getCurrentList();
        if (currentList != null) {
            currentList.paint(g);

            if (ui.getState() == GamePackagesUI.STATE_MENU) {
            	if (currentList.getCurrentCyoObject() != null) {
            		g.drawImage(btnFocusImg, 717, 236, c);
            	}
                g.setColor(btnColor);
            } else {
            	if (currentList.getCurrentCyoObject() != null) {
            		g.drawImage(btnDimImg, 718, 237, c);
            	}
                g.setColor(dimmedMenuColor);
            }
            g.setFont(f18);
          
            if (currentList.getCurrentCyoObject() != null) {
            	
	            Group parentObj = (Group)currentList.getCurrentCyoObject().getParentObj();
	            Subscription curObj = (Subscription) currentList.getCurrentCyoObject();
	          
	//            if (currentList.getCurrentCyoObject() instanceof Game) {
	            if(parentObj.getNo().equals("1540") && !curObj.getNo().equals("460")) {
	                GraphicUtil.drawStringCenter(g, menu2, 804, 257);
	            } else {
	                GraphicUtil.drawStringCenter(g, menu, 804, 257);
	            }
            }

        }

        for (int i = 0; i < buttons.length; i++) {
            int step = i * 37;
            if (ui.getButtonFocus() == i && ui.getState() == ChangeChannelSelectionUI.STATE_BUTTON) {
                g.drawImage(btnFocusImg, 717, 346 + step, c);
                g.setColor(btnColor);
            } else {
                if (!ui.getChanged() && i == 0) {                   
                    g.drawImage(btnGrayDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuInColor);
                } else {
                    g.drawImage(btnDimImg, 718, 347 + step, c);
                    g.setColor(dimmedMenuColor);
                }
            }
            GraphicUtil.drawStringCenter(g, buttons[i], 804, 367 + step);
        }
    }

    protected void loadImages(BaseUI ui) {
        listBgImg = ui.getImage("17_list_bg.png");
//        menuGlowImg = ui.getImage("17_menu_glow.png");
        btnFocusImg = ui.getImage("17_btn_180.png");
        btnDimImg = ui.getImage("17_btn_gray.png");
        btnGrayDimImg = ui.getImage("17_btn_gray_in.png");
    }
}

/**
 * @(#)ListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Game;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Product;
import com.videotron.tvi.illico.cyo.data.obj.Service;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.cyo.ui.BaseUI;
import com.videotron.tvi.illico.cyo.ui.GamePackagesListUI;
import com.videotron.tvi.illico.cyo.ui.ListUI;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GamePackagesListRenderer extends ListRenderer {

    private final int LINE_STEP = 32;
    private final int LIST_HEIGHT = 291;

    private Image lineImg;
    private Image groupLineImg;
    private Image focusImg;
    private Image checkBoxImg;
    private Image checkImg;
    private Image checkBoxWithFocusImg;
    private Image checkBoxWithFocusImgIn;
    private Image checkWithFocusImg;
    private Image radioImg;
    private Image radioFocusImg;
    private Image focusBgTopImg;
    private Image focusBgMidImg;
    private Image focusBgBotImg;
    private Image focusBgTopDimImg;
    private Image focusBgMidDimImg;
    private Image focusBgBotDimImg;
    private Image focusDimImg;
//    private Image explainShImg;
//    private Image shTopImg;
//    private Image shBotImg;
//    private Image groupShTopImg;
//    private Image groupShBotImg;
    private Image scrBarImg;
    private Image scrBgTopImg;
    private Image scrBgMidImg;
    private Image scrBgBotImg;
    private Image arrowTopImg;
    private Image arrowBotImg;

    private Color blackColor = new Color(0, 0, 0);
    private Color unFocusedColor = new Color(230, 230, 230);
    private Color focusBgColor = new Color(132, 132, 132);
    private Color modifyDimColor = new Color(200, 200, 200);
    private Color infoColor = new Color(39, 39, 39);
    private Color infoDimColor = new Color(160, 160, 160);
    private Color focusColor = blackColor;

    protected void paint(Graphics g, UIComponent c) {

        GamePackagesListUI parentUI = (GamePackagesListUI) c;
        Rectangle beforeRec = g.getClipBounds();
        g.clipRect(57, 167, 636, 291);
        int startIdx = parentUI.getStartIdx();
        int currentIdx = startIdx;
        int heightOfDrawn = 0;
        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Group) {
                g.setColor(blackColor);
                if (currentIdx - 1 == startIdx + parentUI.getFocus()) {
                    g.fillRect(56, 171 + heightOfDrawn, 638, 29);
                } else {
                    g.fillRect(56, 167 + heightOfDrawn, 638, 32);
                }
//                g.drawImage(groupShTopImg, 56, 160 + heightOfDrawn, c);
//                g.drawImage(groupShBotImg, 56, 200 + heightOfDrawn, c);
                g.setFont(f21);
                g.setColor(Color.white);
                GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 375, 191 + heightOfDrawn);
                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Package) {
                Package curObj = (Package) obj;
                Group parentObj = (Group) obj.getParentObj();
                int focusType = parentUI.getFocusType(currentIdx);
                int descripHeight = 0;

                // TV-Web no : 1541
                // TV Game no : 1540
                if (parentObj.getNo().equals("1541")) {
                    Image checkImg = null;
                    if (focusType == ListUI.FOCUS_ON_ME) {
                        descripHeight = getDescriptionHeight(parentUI, heightOfDrawn);// drawDescription(g, parentUI,
                                                                                      // heightOfDrawn, true);
                    } else {
                        descripHeight = 32;
                        checkImg = checkBoxImg;
                        if (heightOfDrawn + 46 < LIST_HEIGHT) {
                            g.drawImage(lineImg, 64, 200 + heightOfDrawn, c);
                        }
                        g.setFont(f18);
                        g.setColor(unFocusedColor);

                        g.drawImage(checkImg, 71, 176 + heightOfDrawn, c); // 76, 176
                        if (curObj.getSubscribedInSession()) {
                            g.drawImage(radioImg, 75, 180 + heightOfDrawn, c);
                        }

                        String title = curObj.getTitle();
                        if (parentUI.isWithPrice()) {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 440);
                        } else {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 550);
                        }
                        g.drawString(title, 100, 191 + heightOfDrawn);

                        String price = curObj.getPrice();
                        if (price != null) {
                            GraphicUtil.drawStringRight(g, price + BaseUI.getPriceFreq(curObj.getPricefreq()), 671,
                                    190 + heightOfDrawn);
                        }
                    }

                } else {
                    Image cBoxImg;
                    Image cImg;
                    int cPosX = 75;
                    if (focusType == ListUI.FOCUS_ON_ME || focusType == ListUI.FOCUS_ON_CHILD) {

                        if (focusType == ListUI.FOCUS_ON_ME) {
                            descripHeight = getDescriptionHeight(parentUI, heightOfDrawn);
                        }
                    } else {
                        if (heightOfDrawn + 46 < LIST_HEIGHT) {
                            g.drawImage(lineImg, 64, 198 + heightOfDrawn, c);
                        }
                        cBoxImg = checkBoxImg;
//                        cImg = checkImg;
                        cImg = radioImg;
                        g.setFont(f18);
                        g.setColor(unFocusedColor);

                        g.drawImage(cBoxImg, 71, 176 + heightOfDrawn, c); // 76, 176
                        if (((Package) obj).getSubscribedInSession()) {
                            g.drawImage(cImg, cPosX, 180 + heightOfDrawn, c);
                        }

                        String title = curObj.getTitle();
                        if (parentUI.isWithPrice()) {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 440);
                        } else {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 550);
                        }
                        g.drawString(title, 100, 191 + heightOfDrawn);

                        String price = curObj.getPrice();
                        if (price != null) {
                            GraphicUtil.drawStringRight(g, price + BaseUI.getGamePriceFreq(curObj.getPricefreq()), 671,
                                    190 + heightOfDrawn);
                        }
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descripHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            } else if (obj instanceof Game) {
                Image cBoxImg;
                Image cImg;

                Game curObj = (Game) obj;
                CyoObject[] objs = obj.getParentObj().getCyoObject();
                Game lastGame = (Game) objs[objs.length - 1];
                boolean isLast = ((Game) obj).getNo().equals(lastGame.getNo());
                int focusType = parentUI.getFocusType(currentIdx);
                int descripHeight = 0;

                if (focusType != ListUI.FOCUS_NONE) {

                    // if (focusType == ListUI.FOCUS_ON_PARENT || focusType == ListUI.FOCUS_ON_CHILD) {
                    // g.setColor(focusBgColor);
                    // g.fillRect(61, 167 + heightOfDrawn, 628, 34);
                    // }
                    // g.setColor(focusColor);
                    // if (isLast) {
                    // if (parentUI.hasFocus()) {
                    // g.drawImage(focusBgBotImg, 57, 169 + heightOfDrawn, 636, 34, parentUI);
                    // } else {
                    // g.drawImage(focusBgBotDimImg, 57, 169 + heightOfDrawn, 636, 34, parentUI);
                    // }
                    // } else {
                    // if (parentUI.hasFocus()) {
                    // g.drawImage(focusBgMidImg, 57, 167 + heightOfDrawn, 636, 34, parentUI);
                    // } else {
                    // g.drawImage(focusBgMidDimImg, 57, 167 + heightOfDrawn, 636, 34, parentUI);
                    // }
                    // }
                    //
                    // if (focusType == ListUI.FOCUS_ON_ME) {
                    // descripHeight = drawDescription(g, parentUI, heightOfDrawn, isLast);
                    // if (parentUI.hasFocus()) {
                    // g.drawImage(focusImg, 57, 163 + heightOfDrawn, parentUI);
                    // cBoxImg = checkBoxWithFocusImg;
                    // cImg = checkWithFocusImg;
                    // } else {
                    // g.drawImage(focusDimImg, 57, 163 + heightOfDrawn, parentUI);
                    // cBoxImg = checkBoxImg;
                    // cImg = checkImg;
                    // }
                    // g.setFont(f20);
                    // g.setColor(focusColor);
                    // } else {
                    // cBoxImg = checkBoxImg;
                    // cImg = checkImg;
                    // if (heightOfDrawn + 46 < LIST_HEIGHT) {
                    // g.drawImage(groupLineImg, 98, 198 + heightOfDrawn, parentUI);
                    // }
                    // g.setFont(f18);
                    // }
                    if (focusType == ListUI.FOCUS_ON_ME) {
                        descripHeight = getDescriptionHeight(parentUI, heightOfDrawn);// drawDescription(g, parentUI,
                                                                                      // heightOfDrawn, isLast);
                    }
                } else {
                    cBoxImg = checkBoxImg;
                    cImg = checkImg;
                    if (heightOfDrawn + 46 < LIST_HEIGHT) {
                        g.drawImage(groupLineImg, 98, 198 + heightOfDrawn, parentUI);
                    }
                    g.setColor(unFocusedColor);

                    g.drawImage(cBoxImg, 95, 176 + heightOfDrawn, c); // 76, 176
                    if (((Game) obj).getSubscribedInSession()) {
                        g.drawImage(cImg, 98, 175 + heightOfDrawn, c);
                    }

                    String title = curObj.getTitle();
                    title = TextUtil.shorten(title, g.getFontMetrics(), 430);
                    g.drawString(title, 126, 191 + heightOfDrawn);

                    String price = curObj.getPrice();
                    if (price != null) {
                        GraphicUtil.drawStringRight(g, price + BaseUI.getPriceFreq(curObj.getPricefreq()), 671,
                                190 + heightOfDrawn);
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descripHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            }
            currentIdx++;
        }

        // shadow
//        if (parentUI.isDisplayTopDeco()) {
//            g.drawImage(shTopImg, 56, 166, c);
//        }
//
//        if (parentUI.isDisplayBottomDeco()) {
//            g.drawImage(shBotImg, 56, 395, c);
//        }

        startIdx = parentUI.getStartIdx();
        currentIdx = startIdx;
        heightOfDrawn = 0;

        for (; heightOfDrawn < LIST_HEIGHT;) {
            CyoObject obj = parentUI.getCyoObject(currentIdx);

            if (obj == null) {
                break;
            }

            if (obj instanceof Group) {
                g.setColor(blackColor);
                if (currentIdx - 1 == startIdx + parentUI.getFocus()) {
                    g.fillRect(56, 171 + heightOfDrawn, 638, 29);
                } else {
                    g.fillRect(56, 167 + heightOfDrawn, 638, 32);
                }
//                g.drawImage(groupShTopImg, 56, 160 + heightOfDrawn, c);
//                g.drawImage(groupShBotImg, 56, 200 + heightOfDrawn, c);
                g.setColor(Color.white);
                GraphicUtil.drawStringCenter(g, ((Group) obj).getTitle(), 375, 191 + heightOfDrawn);
                heightOfDrawn += LINE_STEP;
            } else if (obj instanceof Package) {
                Package curObj = (Package) obj;
                Group parentObj = (Group) obj.getParentObj();
                int focusType = parentUI.getFocusType(currentIdx);
                int descripHeight = 0;

                // TV-Web no : 1541
                // TV Game no : 1540

                if (parentObj.getNo().equals("1541")) {
                    Image checkImg = null;
                    if (focusType == ListUI.FOCUS_ON_ME) {

                        if (parentUI.hasFocus()) {
                            // g.drawImage(focusBgBotImg, 57, 201 + heightOfDrawn, c);
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                checkImg = checkBoxWithFocusImg;                        
                                descripHeight = drawDescription(g, parentUI, heightOfDrawn, true);
                                g.drawImage(focusImg, 57, 163 + heightOfDrawn, c);
                            }
                        } else {
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                checkImg = checkBoxWithFocusImg;
                                g.drawImage(focusBgTopDimImg, 57, 167 + heightOfDrawn, c);
                                g.drawImage(focusBgMidDimImg, 57, 171 + heightOfDrawn, 636, 34, c);
                                descripHeight = drawDescription(g, parentUI, heightOfDrawn, true);
                                g.drawImage(focusDimImg, 57, 163 + heightOfDrawn, c);
                            }
                        }

                        g.setFont(f20);
                        g.setColor(blackColor);

                        g.drawImage(checkImg, 71, 176 + heightOfDrawn, c); // 76, 176
                        if (curObj.getSubscribedInSession()) {
                            if (parentUI.hasFocus()) {
                                g.drawImage(radioFocusImg, 75, 180 + heightOfDrawn, c);
                            } else {
                                g.drawImage(radioImg, 75, 180 + heightOfDrawn, c);
                            }
                        }

                        String title = curObj.getTitle();
                        if (parentUI.isWithPrice()) {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 440);
                        } else {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 550);
                        }
                        g.drawString(title, 100, 191 + heightOfDrawn);

                        String price = curObj.getPrice();
                        if (price != null) {
                            GraphicUtil.drawStringRight(g, price + BaseUI.getPriceFreq(curObj.getPricefreq()), 671,
                                    190 + heightOfDrawn);
                        }
                    }
                } else {
                    Image cBoxImg;
                    Image cImg;
                    int cPosX = 75;
                    if (focusType == ListUI.FOCUS_ON_ME || focusType == ListUI.FOCUS_ON_CHILD) {

                        if (parentUI.hasFocus()) {

                            // g.drawImage(focusBgBotImg, 57, 201 + heightOfDrawn, c);
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                cBoxImg = checkBoxWithFocusImg;
//                                cImg = checkWithFocusImg;
                                cImg = radioFocusImg;
                                cPosX = 75;
                                descripHeight = drawDescription(g, parentUI, heightOfDrawn, true);
                                g.drawImage(focusImg, 57, 163 + heightOfDrawn, c);
                            } else {
                                cBoxImg = checkBoxImg;
//                                cImg = checkImg;
                                cImg = radioImg;
                                cPosX = 75;
                                g.setColor(focusBgColor);
                                g.fillRect(61, 169 + heightOfDrawn, 628, 34);
                                g.drawImage(focusBgTopImg, 57, 167 + heightOfDrawn, c);
                                g.drawImage(focusBgMidImg, 57, 171 + heightOfDrawn, 636, 34, c);

                                if (heightOfDrawn + 46 < LIST_HEIGHT) {
                                    g.drawImage(lineImg, 64, 198 + heightOfDrawn, c);
                                }
                            }
                        } else {
                            cBoxImg = checkBoxWithFocusImg;
//                            cImg = checkWithFocusImg;
                            cImg = radioImg;
                            if (focusType == ListUI.FOCUS_ON_ME) {
                                g.drawImage(focusBgTopDimImg, 57, 167 + heightOfDrawn, c);
                                g.drawImage(focusBgMidDimImg, 57, 171 + heightOfDrawn, 636, 34, c);
                                descripHeight = drawDescription(g, parentUI, heightOfDrawn, false);
                                g.drawImage(focusDimImg, 57, 163 + heightOfDrawn, c);
                            } else {
                                g.setColor(focusBgColor);
                                g.fillRect(61, 169 + heightOfDrawn, 628, 32);
                                g.drawImage(focusBgTopDimImg, 57, 167 + heightOfDrawn, c);
                                g.drawImage(focusBgMidDimImg, 57, 171 + heightOfDrawn, 636, 34, c);

                                if (heightOfDrawn + 46 < LIST_HEIGHT) {
                                    g.drawImage(lineImg, 64, 200 + heightOfDrawn, c);
                                }
                            }
                        }

                        g.setFont(f20);
                        g.setColor(blackColor);

                        g.drawImage(cBoxImg, 71, 176 + heightOfDrawn, c); // 76, 176
                        if (((Package) obj).getSubscribedInSession()) {
                            g.drawImage(cImg, cPosX, 180 + heightOfDrawn, c);
                        }

                        String title = curObj.getTitle();
                        if (parentUI.isWithPrice()) {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 440);
                        } else {
                            title = TextUtil.shorten(title, g.getFontMetrics(), 550);
                        }
                        g.drawString(title, 100, 191 + heightOfDrawn);

                        String price = curObj.getPrice();
                        if (price != null) {
                            GraphicUtil.drawStringRight(g, price + BaseUI.getGamePriceFreq(curObj.getPricefreq()), 671,
                                    190 + heightOfDrawn);
                        }
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descripHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            } else if (obj instanceof Game) {
                Image cBoxImg;
                Image cImg;

                Game curObj = (Game) obj;
                CyoObject[] objs = obj.getParentObj().getCyoObject();
                Game lastGame = (Game) objs[objs.length - 1];
                boolean isLast = ((Game) obj).getNo().equals(lastGame.getNo());
                int focusType = parentUI.getFocusType(currentIdx);
                int descripHeight = 0;

                if (focusType != ListUI.FOCUS_NONE) {

                    if (focusType == ListUI.FOCUS_ON_PARENT || focusType == ListUI.FOCUS_ON_CHILD) {
                        g.setColor(focusBgColor);
                        g.fillRect(61, 167 + heightOfDrawn, 628, 34);
                    }
                    g.setColor(focusColor);
                    if (isLast) {
                        if (parentUI.hasFocus()) {
                            g.drawImage(focusBgBotImg, 57, 169 + heightOfDrawn, 636, 34, parentUI);
                        } else {
                            g.drawImage(focusBgBotDimImg, 57, 169 + heightOfDrawn, 636, 34, parentUI);
                        }
                    } else {
                        if (parentUI.hasFocus()) {
                            g.drawImage(focusBgMidImg, 57, 167 + heightOfDrawn, 636, 34, parentUI);
                        } else {
                            g.drawImage(focusBgMidDimImg, 57, 167 + heightOfDrawn, 636, 34, parentUI);
                        }
                    }

                    if (focusType == ListUI.FOCUS_ON_ME) {
                        descripHeight = drawDescription(g, parentUI, heightOfDrawn, isLast);
                        if (parentUI.hasFocus()) {
                            g.drawImage(focusImg, 57, 163 + heightOfDrawn, parentUI);
                            cBoxImg = checkBoxWithFocusImg;
                            cImg = checkWithFocusImg;
                        } else {
                            g.drawImage(focusDimImg, 57, 163 + heightOfDrawn, parentUI);
                            cBoxImg = checkBoxImg;
                            cImg = checkImg;
                        }
                        g.setFont(f20);
                        g.setColor(focusColor);
                    } else {
                        cBoxImg = checkBoxImg;
                        cImg = checkImg;
                        if (heightOfDrawn + 46 < LIST_HEIGHT) {
                            g.drawImage(groupLineImg, 98, 198 + heightOfDrawn, parentUI);
                        }
                        g.setFont(f18);
                    }

                    g.drawImage(cBoxImg, 95, 176 + heightOfDrawn, c); // 76, 176
                    if (((Game) obj).getSubscribedInSession()) {
                        g.drawImage(cImg, 98, 175 + heightOfDrawn, c);
                    }

                    String title = curObj.getTitle();
                    title = TextUtil.shorten(title, g.getFontMetrics(), 430);
                    g.drawString(title, 126, 191 + heightOfDrawn);

                    String price = curObj.getPrice();
                    if (price != null) {
                        GraphicUtil.drawStringRight(g, price + BaseUI.getPriceFreq(curObj.getPricefreq()), 671,
                                190 + heightOfDrawn);
                    }
                }

                if (focusType == ListUI.FOCUS_ON_ME) {
                    heightOfDrawn = descripHeight;
                } else {
                    heightOfDrawn += LINE_STEP;
                }
            }
            currentIdx++;
        }
        g.setClip(beforeRec);

        // arrow.
        if (parentUI.isDisplayTopDeco()) {
            g.drawImage(arrowTopImg, 364, 158, c);
        }
        if (parentUI.isDisplayBottomDeco()) {
            g.drawImage(arrowBotImg, 364, 449, c);
        }
    }

    public int drawDescription(Graphics g, ListUI parentUI, int heightOfDrawn, boolean isLast) {
        String[] desc = parentUI.getDesc();

        if (desc == null) {
            return heightOfDrawn + 32;
        }

        g.setColor(bgColor);
        g.fillRect(61, 201 + heightOfDrawn, 628, 63);

        if (parentUI.hasFocus()) {
            g.drawImage(focusBgMidImg, 57, 201 + heightOfDrawn, 636, 63, parentUI);
            if (isLast) {
                g.drawImage(focusBgBotImg, 57, 201 + heightOfDrawn + 30, parentUI);
            }
        } else {
            g.drawImage(focusBgMidDimImg, 57, 201 + heightOfDrawn, 636, 63, parentUI);
            if (isLast) {
                g.drawImage(focusBgBotDimImg, 57, 201 + heightOfDrawn + 30, parentUI);
            }
        }

        g.setFont(f14d);
        g.setColor(blackColor);
        int descStartIdx = parentUI.getDescStartIdx();
        int count = 0;
        for (int i = descStartIdx; i < descStartIdx + 3; i++) {
            if (i < desc.length && desc[i] != null) {
                g.drawString(desc[i], 99, 220 + heightOfDrawn + count * 17);
                count++;
            }
        }

        if (desc.length > 3) {
//            if (descStartIdx + 3 < desc.length) {
//                g.drawImage(explainShImg, 61, 238 + heightOfDrawn, parentUI);
//            } else if (descStartIdx > 0) {
//                g.drawImage(explainShImg, 61, 193 + heightOfDrawn, parentUI);
//            }

            // scroll
            g.drawImage(scrBgTopImg, 662, 211 + heightOfDrawn, 7, 10, parentUI);
            g.drawImage(scrBgMidImg, 662, 221 + heightOfDrawn, 7, 27, parentUI);
            g.drawImage(scrBgBotImg, 662, 246 + heightOfDrawn, parentUI);

            int step = parentUI.getScrollGap();
            g.drawImage(scrBarImg, 659, 210 + heightOfDrawn + step, parentUI);
        }
        heightOfDrawn += 95;
        return heightOfDrawn;
    }

    public void prepare(UIComponent c) {
        ListUI parentUI = (ListUI) c;
        loadImages(parentUI);

        super.prepare(c);
    }

    protected void loadImages(BaseUI ui) {
        lineImg = ui.getImage("17_list_line.png");
        groupLineImg = ui.getImage("17_group_line.png");
        focusImg = ui.getImage("17_slist_foc.png");
        checkBoxImg = ui.getImage("check_box.png");
        checkImg = ui.getImage("check.png");
        checkBoxWithFocusImg = ui.getImage("check_box_foc.png");
        checkWithFocusImg = ui.getImage("check_foc.png");
        checkWithFocusImg = ui.getImage("check_foc.png");
        checkBoxWithFocusImgIn = ui.getImage("check_box_foc_in.png");
        radioImg = ui.getImage("radio_btn.png");
        radioFocusImg = ui.getImage("radio_btn_foc.png");
        focusBgTopImg = ui.getImage("17_rim01_t.png");
        focusBgMidImg = ui.getImage("17_rim01_m.png");
        focusBgBotImg = ui.getImage("17_rim01_b.png");
        focusImg = ui.getImage("17_list_foc.png");
        focusBgTopDimImg = ui.getImage("17_rim01_dim_t.png");
        focusBgMidDimImg = ui.getImage("17_rim01_dim_m.png");
        focusBgBotDimImg = ui.getImage("17_rim01_dim_b.png");
        focusDimImg = ui.getImage("17_list_foc_dim.png");
//        explainShImg = ui.getImage("17_list_sha_b.png");
//        shTopImg = ui.getImage("17_sha_top.png");
//        shBotImg = ui.getImage("17_sha_bottom.png");
//        groupShTopImg = ui.getImage("17_list_glow_t.png");
//        groupShBotImg = ui.getImage("17_list_glow_b.png");
        scrBarImg = ui.getImage("scr_bar.png");
        scrBgTopImg = ui.getImage("scrbg_up.png");
        scrBgMidImg = ui.getImage("scr_m.png");
        scrBgBotImg = ui.getImage("scrbg_dn.png");
        arrowTopImg = ui.getImage("02_ars_t.png");
        arrowBotImg = ui.getImage("02_ars_b.png");
    }
}

/**
 * @(#)GamePackagesUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Services;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.cyo.gui.GamePackagesRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GamePackagesUI extends BaseUI implements CyoDataListener {
	public static final int STATE_LIST = 0x0;
	public static final int STATE_MENU = 0x1;
	public static final int STATE_BUTTON = 0x2;

	private ClickingEffect clickEffect;

	private ListUI gamePacList = new GamePackagesListUI();

	private Vector addVector = new Vector();
	private Vector removeVector = new Vector();

	private boolean isModified = false;
	private boolean needStayHere = false;
	private boolean isUpdated = false;
	private boolean isCanceling = false;

	private Group loadedGroup;

	private int btnFocus = 0;
	private Services services;

	private ListUI currentList;
	// public boolean changed = false;
	private boolean saveMessageView = false;

	public GamePackagesUI() {
		renderer = new GamePackagesRenderer();
		setRenderer(renderer);
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);
		if (!back) {
			prepare();

			isUpdated = false;
			DataCenter.getInstance().put("GAME_PACKAGES_UPDATED", new Boolean(false));
			DataCenter.getInstance().remove("GAME_ADD");
			DataCenter.getInstance().remove("GAME_REMOVE");
			currentList = null;
			CyoObject obj = (CyoObject) DataCenter.getInstance().get("Game");
			Group group = (Group) obj;
			Hashtable param = new Hashtable();
			param.put("url", group.getUrl());
			CyoDataManager.getInstance().request(URLType.LOAD_GAME, param);
		} else {
			isUpdated = DataCenter.getInstance().getBoolean("GAME_PACKAGES_UPDATED");

			if (isUpdated) {
				CyoObject curObj = currentList.getCurrentCyoObject();
				Log.printInfo("curObj = " + curObj);
				Group parentObj = (Group) curObj.getParentObj();

				Log.printInfo("parentObj = " + parentObj);

				Package curPackage = ((Package) curObj);
				String modiable = curPackage.getModifiable();
				if (Log.DEBUG_ON) {
					Log.printDebug("GamePackagesUI: start(): parent no = " + parentObj.getNo());
					Log.printDebug("GamePackagesUI: start(): Package modiable = " + modiable);
				}

				// if (parentObj.getNo().equals("1541")) {
				if (modiable.equals("0")) {
					// Package
					curPackage.setSubscribedInSession(true);
					
					if (!shouldOnlyPackage(curPackage)) {
						CyoObject[] childObjs = curPackage.getCyoObject();
	
						for (int i = 0; childObjs != null && i < childObjs.length; i++) {
							Subscription child = (Subscription) childObjs[i];
							if (child.getModifiable().equals("1")) {
								child.setSubscribedInSession(curPackage.getSubscribedInSession());
							}
						}
					}

					// if (parentObj.getNo().equals("1541")) {
					CyoObject[] subObjs = parentObj.getCyoObject();
					// CyoObject[] subObjs = parentObj.getCyoObject();
					for (int i = 0; i < subObjs.length; i++) {
						if (!subObjs[i].equals(curPackage)) {
							((Subscription) subObjs[i]).setSubscribedInSession(false);
							removeCyoObject(subObjs[i]);
						}
					}
					// }

					if (curPackage.getSubscribedInSession()) {
						addCyoObject(curObj);
						//addSubObject();
					}

					isModified = false;
					needExitPopup = true;
				}
			}

			gamePacList.start(true);
		}

		needStayHere = false;
		saveMessageView = false;
		isCanceling = false;
		
		super.start(back);
		repaint();
	}

	public void prepare() {
		state = STATE_LIST;
		btnFocus = 0;
		focus = 0;

		setFooter();
		super.prepare();
	}

	public void stop() {
		clearPopup();
		super.stop();
	}

	public void dispose() {
		clickEffect = null;
		addVector.clear();
		removeVector.clear();
		gamePacList.stop();
		super.dispose();
	}

	public boolean handleKey(int code) {

		if (getPopup() != null && getPopup().isVisible()) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case OCRcEvent.VK_PAGE_UP:
			pageUp();
			return true;
		case OCRcEvent.VK_PAGE_DOWN:
			pageDown();
			return true;
		case OCRcEvent.VK_ENTER:
			keyEnter();
			return true;
		case KeyCodes.LAST:
			footer.clickAnimation(0);
			if (getChanged()) {
				setPopup(notiPop);
				// notiPop.show(this, NotificationPopupUI.STATE_CANCEL,
				// BaseUI.getMenuText(KeyNames.CANCEL_VALIDATE_TITLE), BaseUI
				// .getExplainText(KeyNames.CANCEL_VALIDATE_EXPLAIN));
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			} else {
				dispose();
				CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
			}
			repaint();
			return true;
		default:
			return false;
		}
	}

	private void keyLeft() {
		if (state == STATE_BUTTON && currentList.getCyoObjectLength() > 0) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
		} else if (state == STATE_MENU) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
		} else if (state == STATE_BUTTON && currentList.getCyoObjectLength() > 0) {
			state = STATE_LIST;
			btnFocus = 0;
		}

		repaint();

	}

	private void keyRight() {
		if (state == STATE_LIST) {
			state = STATE_MENU;
			currentList.setHasFocus(false);
			repaint();
		}
	}

	private void keyUp() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_UP);
		} else if (state == STATE_BUTTON) {
			if (--btnFocus < 0 || (btnFocus == 0 && !getChanged())) {
				if (currentList.getCyoObjectLength() > 0) {
					btnFocus = 0;
					state = STATE_MENU;
				} else {
					btnFocus = 1;
					state = STATE_BUTTON;
				}
			}
		}
		repaint();
	}

	private void keyDown() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_DOWN);
		} else if (state == STATE_MENU) {
			state = STATE_BUTTON;
			btnFocus = getChanged() ? 0 : 1;
		} else if (state == STATE_BUTTON) {
			if (++btnFocus > 2) {
				btnFocus = 2;
			}
		}
		repaint();
	}

	private void pageUp() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_UP);
			repaint();
		}
	}

	private void pageDown() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
			repaint();
		}
	}

	private void keyEnter() {
		Log.printDebug("GamePackagesUI : keyEnter(): state = " + state);
		if (state == STATE_LIST) {
			CyoObject curObj = currentList.getCurrentCyoObject();
			Log.printInfo("curObj = " + ((Subscription) curObj).getNo());
			Group parentObj = (Group) curObj.getParentObj();

			Log.printDebug("parentObj = " + parentObj.getNo());

			updateSubscribed(curObj);
			needExitPopup = true;

		} else if (state == STATE_MENU) {
			Log.printDebug("");
			DataCenter.getInstance().put("GamePackage", currentList.getCurrentCyoObject());

			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}
			clickEffect.start(717, 237, 172, 30);

			CyoController.getInstance().showUI(BaseUI.UI_VIEW_GAMES, false);
		} else if (state == STATE_BUTTON) {
			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}

			clickEffect.start(717, 346 + btnFocus * 37, 180, 35);

			if (btnFocus == 0) { // Confirm
				if (Log.DEBUG_ON) {
					Log.printDebug("GamePackagesUI: isModified is " + isModified);
				}
				if (isModified) {
					requestSaveAction();
				} else {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else if (btnFocus == 1) { // Cancel
				if (!getChanged()) {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				} else {
					// setPopup(notiPop);
					// notiPop.show(this, NotificationPopupUI.STATE_CANCEL,
					// BaseUI
					// .getMenuText(KeyNames.CANCEL_VALIDATE_TITLE), BaseUI
					// .getExplainText(KeyNames.CANCEL_VALIDATE_EXPLAIN));
					setPopup(notiPop);
					notiPop.show(this, NotificationPopupUI.STATE_CANCEL);

				}
			} else if (btnFocus == 2) { // Help
				DataCenter.getInstance().put("MODE", "HELP");
				CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
			}
		}
		repaint();
	}

	private void setFooter() {
		footer.reset();
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
	}

	public String getGroupName() {
		if (loadedGroup != null) {
			return loadedGroup.getTitle();
		}
		return TextUtil.EMPTY_STRING;
	}

	private void updateSubscribed(CyoObject obj) {
		if (obj instanceof Package) {
			// TV-Web no : 1541
			// TV Game no : 1540
			Package curPackage = ((Package) obj);
			Group parentObj = (Group) curPackage.getParentObj();
			String modiable = curPackage.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("GamePackagesUI: parent no = " + parentObj.getNo());
				Log.printDebug("GamePackagesUI: Package modiable = " + modiable);
			}

			// if (parentObj.getNo().equals("1541")) {
			if (modiable.equals("0")) {
				// Package
				curPackage.setSubscribedInSession(!curPackage.getSubscribedInSession());
				
				if (!shouldOnlyPackage(curPackage)) {
					CyoObject[] childObjs = curPackage.getCyoObject();
	
					for (int i = 0; childObjs != null && i < childObjs.length; i++) {
						Subscription child = (Subscription) childObjs[i];
						if (child.getModifiable().equals("1")) {
							child.setSubscribedInSession(curPackage.getSubscribedInSession());
							
							if (!child.getSubscribedInSession()) {
								removeCyoObject(child);
							}
						} else if (child.getModifiable().equals("0")
								&& child.getSubscribedInSession() != child.getSubscribedOrginalInSession()) {
							child.setSubscribedInSession(curPackage.getSubscribedInSession());
	
							if (curPackage.getSubscribedInSession()) {
								addCyoObject(child);
							} else {
								removeCyoObject(child);
							}
						}
					}
				}

				CyoObject[] subObjs = parentObj.getCyoObject();
				// CyoObject[] subObjs = parentObj.getCyoObject();
				for (int i = 0; i < subObjs.length; i++) {
					if (!subObjs[i].equals(curPackage) && ((Subscription) subObjs[i]).getSubscribedInSession() == true) {
						((Subscription) subObjs[i]).setSubscribedInSession(false);
						removeCyoObject(subObjs[i]);

						if (!shouldOnlyPackage((Package)subObjs[i])) {
							CyoObject[] childObjs2 = subObjs[i].getCyoObject();
	
							for (int j = 0; childObjs2 != null && j < childObjs2.length; j++) {
								Subscription child = (Subscription) childObjs2[j];
								if (child.getModifiable().equals("1")) {
									child.setSubscribedInSession(false);
									removeCyoObject(child);
								}
							}
						}
					}
				}

				if (curPackage.getSubscribedInSession()) {
					addCyoObject(obj);
				} else {
					removeCyoObject(obj);
				}

				if (curPackage.getSubscribedInSession()) {
					state = STATE_MENU;
					currentList.setHasFocus(false);
					repaint();
				}

				isModified = true;
			}
		}
	}
	
	private boolean shouldOnlyPackage(Package pack) {
		Group parentObj = (Group) pack.getParentObj();
		
		if (parentObj.getNo().equals("1541") || pack.getNo().equals("460")) {
			return true;
		}
		
		return false;
	}

	public int getButtonFocus() {
		return btnFocus;
	}

	public ListUI getCurrentList() {
		return currentList;
	}

	private void requestSaveAction() {
		String addParams = null, removeParams = null, groupNo = null;
		Hashtable param = new Hashtable();

		addParams = getNumbers(addVector);
		removeParams = getNumbers(removeVector);
		groupNo = loadedGroup.getNo();

		if (addParams != null) {
			param.put("add", addParams);
		}
		if (removeParams != null) {
			param.put("remove", removeParams);
		}
		param.put("groupNo", groupNo);

		if (addParams != null || removeParams != null) {
			CyoDataManager.getInstance().request(URLType.SAVE_GAME, param);
		}
	}

	private String getNumbers(Vector vec) {
		if (vec.size() == 0) {
			return null;
		}

		StringBuffer sf = new StringBuffer();

		for (int i = 0; i < vec.size(); i++) {
			Subscription obj = (Subscription) vec.get(i);
			
			if (obj.getNo() != null) {
				if (i > 0) {
					sf.append(",");
				}
				
				
				sf.append(obj.getNo());
			}
		}

		return sf.toString();
	}

	private void addCyoObject(CyoObject obj) {
		Vector currentAddVector = null;
		Vector currentRemoveVector = null;
		if (currentList.equals(gamePacList)) {
			currentAddVector = addVector;
			currentRemoveVector = removeVector;
		} else {
			// My Picks
			Group parentObj = (Group) obj.getParentObj();

			if (loadedGroup.getNo().equals(parentObj.getNo())) {
				currentAddVector = addVector;
				currentRemoveVector = removeVector;
			}
		}

		if (currentAddVector.contains(obj)) {
			currentAddVector.remove(obj);
			currentAddVector.add(obj);
			currentRemoveVector.remove(obj);
		} else {
			currentAddVector.add(obj);
			currentRemoveVector.remove(obj);
		}
	}

	private void addSubObject() {
		Vector addGameVector = (Vector) DataCenter.getInstance().get("GAME_ADD");
		Vector removeGameVector = (Vector) DataCenter.getInstance().get("GAME_REMOVE");

		if (addGameVector != null && addGameVector.size() > 0) {
			for (int i = 0; i < addGameVector.size(); i++) {
				addVector.remove(addGameVector.get(i));
				removeVector.remove(addGameVector.get(i));
				addVector.add(addGameVector.get(i));
			}
		}

		if (removeGameVector != null && removeGameVector.size() > 0) {
			for (int i = 0; i < removeGameVector.size(); i++) {
				addVector.remove(removeGameVector.get(i));
				removeVector.remove(removeGameVector.get(i));
				removeVector.add(removeGameVector.get(i));
			}
		}

		DataCenter.getInstance().remove("GAME_ADD");
		DataCenter.getInstance().remove("GAME_REMOVE");
	}

	private void removeCyoObject(CyoObject obj) {
		Vector currentAddVector = null;
		Vector currentRemoveVector = null;
		if (currentList.equals(gamePacList)) {
			currentAddVector = addVector;
			currentRemoveVector = removeVector;
		} else {
			// My Picks
			Group parentObj = (Group) obj.getParentObj();

			if (loadedGroup.getNo().equals(parentObj.getNo())) {
				currentAddVector = addVector;
				currentRemoveVector = removeVector;
			}
		}

		if (currentRemoveVector.contains(obj)) {
			currentAddVector.remove(obj);
			currentRemoveVector.remove(obj);
			currentRemoveVector.add(obj);
		} else {
			currentAddVector.remove(obj);
			currentRemoveVector.add(obj);
		}
	}

	private void buildDataForList(CyoObject[] data) {

		CyoObject[] groups = data;

		for (int i = 0; i < groups.length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("GamePackagesUI: buildDataForList(): data[" + i + "] + " + groups[i]);
			}

			CyoObject[] subObjs = groups[i].getCyoObject();

			if (subObjs != null) {
				for (int j = 0; j < subObjs.length; j++) {
					if (Log.DEBUG_ON) {
						Log.printDebug("GamePackagesUI: buildDataForList(): subObjs[" + j + "] "
								+ ((Subscription) subObjs[j]).getTitle());
					}
					if (subObjs[j] instanceof Subscription) {
						Subscription ss = (Subscription) subObjs[j];
						ss.setSubscribedOrginalInSession(String.valueOf(ss.getSubscribedInSession()));
						if (Log.DEBUG_ON) {
							Log.printDebug("GamePackagesUI: buildDataForList(): ss.getNo = " + ss.getNo());
							Log.printDebug("GamePackagesUI: buildDataForList(): ss.getSubscribedOrginalInSession = "
									+ ss.getSubscribedOrginalInSession());
							Log.printDebug("GamePackagesUI: buildDataForList(): ss.getSubscribedInSession = "
									+ ss.getSubscribedInSession());
						}
		
						CyoObject[] ssuobjs = ss.getCyoObject();
		
						if (ssuobjs != null) {
							for (int k = 0; k < ssuobjs.length; k++) {
								Subscription sss = (Subscription) ssuobjs[k];
								sss.setSubscribedOrginalInSession(String.valueOf(sss.getSubscribedInSession()));
								if (Log.DEBUG_ON) {
									Log.printDebug("GamePackagesUI: buildDataForList(): sss.getNo = " + sss.getNo());
									Log.printDebug("GamePackagesUI: buildDataForList(): sss.getSubscribedOrginalInSession = "
											+ sss.getSubscribedOrginalInSession());
									Log.printDebug("GamePackagesUI: buildDataForList(): sss.getSubscribedInSession = "
											+ sss.getSubscribedInSession());
								}
							}
						}
		
					}
				}
			}
		}
		
		Vector dataVector = new Vector();
		boolean hasIllico2 = addCyoObject(dataVector, data);
		CyoObject[] rearrangeData = null;
		
		if (hasIllico2) {
			rearrangeData = new CyoObject[dataVector.size()];
			dataVector.copyInto(rearrangeData);
			
			gamePacList.setCyoObject(rearrangeData);
			dataVector.clear();
		} else {
			gamePacList.setCyoObject(null);
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("GamePackagesUI: buildDataForList(): gamePacList.getCyoObjectLength() = " + gamePacList.getCyoObjectLength());
		}
		
		if (gamePacList.getCyoObjectLength() == 0) {
			gamePacList.setHasFocus(false);
			btnFocus = 1;
			state = STATE_BUTTON;
		} else {
			gamePacList.setHasFocus(true);
			gamePacList.start(false);
		}
		
		currentList = gamePacList;
	}

	private boolean addCyoObject(Vector vec, CyoObject[] data) {
		boolean hasIllico2Data = false;
		for (int i = 0; i < data.length; i++) {
			
			Log.printInfo("data[" + i + "]" + data[i]);
			if (data[i] instanceof Group) {
				vec.add(data[i]);
			} else if (data[i] instanceof Package) {
				Subscription ss = (Subscription) data[i];
				Log.printInfo("ss1s.getNo = " + ss.getNo());
				Log.printInfo("ss1s.getIllico = " + ss.getIllico());
				
				if (ss.getIllico() != null && !ss.getIllico().equals(Subscription.ILLICO_1)) {
					ss.setSubscribedOrginalInSession(String.valueOf(ss.getSubscribedInSession()));
					vec.add(data[i]);
					hasIllico2Data = true;
					removeIllico1((Subscription) data[i]);
				}
			}
			CyoObject[] subObjs = data[i].getCyoObject();

			if (subObjs != null) {
				for (int j = 0; j < subObjs.length; j++) {
					Log.printInfo("subObjs[" + j + "]" + subObjs[j]);
					if (subObjs[j] instanceof Package) {
						Subscription ss = (Subscription) subObjs[j];
						Log.printInfo("ss2.getNo = " + ss.getNo());
						Log.printInfo("ss2.getIllico = " + ss.getIllico());
						if (ss.getIllico() != null && !ss.getIllico().equals(Subscription.ILLICO_1)) {
							ss.setSubscribedOrginalInSession(String.valueOf(ss.getSubscribedInSession()));
							vec.add(subObjs[j]);
							hasIllico2Data = true;
							removeIllico1((Subscription) subObjs[j]);
						}
					} else if (subObjs[j] instanceof Subscription) {
						Subscription ss = (Subscription) subObjs[j];
						if (ss.getIllico() != null && !ss.getIllico().equals(Subscription.ILLICO_1)) {
							vec.add(subObjs[j]);
							hasIllico2Data = true;
						}
					}
					

					// TV Game no : 1540
					// if (group.getNo().equals("1540")) {
					// CyoObject[] games = subObjs[j].getCyoObject();
					// for (int k = 0; k < games.length; k++) {
					// vec.add(games[k]);
					// }
					// }
				}
			}
		}
		
		return hasIllico2Data;
	}
	
	private void removeIllico1(Subscription sub) {
		
		CyoObject[] objs = sub.getCyoObject();
		
		if (objs != null) {
		
			Vector dataVector = new Vector();
			
			for (int i = 0; i < objs.length; i++) {
				if (objs[i] instanceof Subscription) {
					Subscription ss = (Subscription) objs[i];
					if (ss.getIllico() != null && !ss.getIllico().equals(Subscription.ILLICO_1)) {
						dataVector.add(objs[i]);
					}
				}
			}
	
			CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
			dataVector.copyInto(rearrangeData);
			
			sub.setCyoObject(rearrangeData);
		}
	}

	public void requestReset() {
		if (Log.INFO_ON) {
			Log.printInfo("GamePackagesUI: requestReset()");
		}

		Vector addVector = new Vector();
		Vector removeVector = new Vector();

		CyoObject[] groups = services.getCyoObject();

		for (int i = 0; i < groups.length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("GamePackagesUI: requestReset(): data[" + i + "] + " + groups[i]);
			}

			CyoObject[] subObjs = groups[i].getCyoObject();

			if (subObjs != null) {
				
				// step1 : remove
				for (int j = 0; j < subObjs.length; j++) {
					if (Log.DEBUG_ON) {
						Log.printDebug("GamePackagesUI: requestReset(): subObjs[" + j + "] "
								+ ((Subscription) subObjs[j]).getTitle());
					}
					if (subObjs[j] instanceof Package) {
						Subscription ss = (Subscription) subObjs[j];
						if (Log.DEBUG_ON) {
							Log.printDebug("GamePackagesUI: requestReset(): ss.getNo = " + ss.getNo());
							Log.printDebug("GamePackagesUI: requestReset(): ss.getSubscribedOrginalInSession = "
									+ ss.getSubscribedOrginalInSession());
							Log.printDebug("GamePackagesUI: requestReset(): ss.getSubscribedInSession = "
									+ ss.getSubscribedInSession());
						}

						if (!ss.getSubscribedOrginalInSession()) {
							removeVector.add(ss);
						}

						CyoObject[] ssuobjs = ss.getCyoObject();

						if (ssuobjs != null) {
							for (int k = 0; k < ssuobjs.length; k++) {
								Subscription sss = (Subscription) ssuobjs[k];
								if (Log.DEBUG_ON) {
									Log.printDebug("GamePackagesUI: requestReset(): sss.getNo = " + sss.getNo());
									Log.printDebug("GamePackagesUI: requestReset(): sss.getSubscribedOrginalInSession = "
											+ sss.getSubscribedOrginalInSession());
									Log.printDebug("GamePackagesUI: requestReset(): sss.getSubscribedInSession = "
											+ sss.getSubscribedInSession());
								}

								if (!sss.getSubscribedOrginalInSession()) {
									removeVector.add(sss);
								}
							}
						}

					}
				}
					
				// step2 : add
				for (int j = 0; j < subObjs.length; j++) {
					if (Log.DEBUG_ON) {
						Log.printDebug("GamePackagesUI: requestReset(): subObjs[" + j + "] "
								+ ((Subscription) subObjs[j]).getTitle());
					}
					if (subObjs[j] instanceof Package) {
						Subscription ss = (Subscription) subObjs[j];
						if (Log.DEBUG_ON) {
							Log.printDebug("GamePackagesUI: requestReset(): ss.getNo = " + ss.getNo());
							Log.printDebug("GamePackagesUI: requestReset(): ss.getSubscribedOrginalInSession = "
									+ ss.getSubscribedOrginalInSession());
							Log.printDebug("GamePackagesUI: requestReset(): ss.getSubscribedInSession = "
									+ ss.getSubscribedInSession());
						}

						if (ss.getSubscribedOrginalInSession()) {
							addVector.add(ss);
						}

						CyoObject[] ssuobjs = ss.getCyoObject();

						if (ssuobjs != null) {
							for (int k = 0; k < ssuobjs.length; k++) {
								Subscription sss = (Subscription) ssuobjs[k];
								if (Log.DEBUG_ON) {
									Log.printDebug("GamePackagesUI: requestReset(): sss.getNo = " + sss.getNo());
									Log.printDebug("GamePackagesUI: requestReset(): sss.getSubscribedOrginalInSession = "
											+ sss.getSubscribedOrginalInSession());
									Log.printDebug("GamePackagesUI: requestReset(): sss.getSubscribedInSession = "
											+ sss.getSubscribedInSession());
								}

								if (sss.getSubscribedOrginalInSession()) {
									addVector.add(sss);
								}
							}
						}

					}
				}
			}

		}

		String addParams = null, removeParams = null, groupNo = null;
		Hashtable param = new Hashtable();

		addParams = getNumbers(addVector);
		removeParams = getNumbers(removeVector);
		groupNo = loadedGroup.getNo();

		if (addParams != null) {
			param.put("add", addParams);
		}
		if (removeParams != null) {
			param.put("remove", removeParams);
		}
		param.put("groupNo", groupNo);

		if (Log.DEBUG_ON) {
			Log.printDebug("GamePackagesUI: requestReset(): addParams = " + addParams);
			Log.printDebug("GamePackagesUI: requestReset(): removeParams = " + removeParams);

		}

		if (addParams != null || removeParams != null) {
			CyoDataManager.getInstance().request(URLType.SAVE_GAME, param);
		}

	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);
		removePopup(pop);

		if (gotoHotKey) {
			gotoHotKey = false;
			super.popupClosed(pop, msg);
			return;
		}

		if (expired) {
			cyoReStart();
		} else if (pop instanceof ValidationErrorPopupUI) {
			dispose();
			CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
			repaint();
		} else if (pop instanceof NotificationPopupUI) {
			if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
				Boolean result = (Boolean) msg;
				Log.printDebug("GamePackagesUI: noti's result = " + result.booleanValue());
				// if (result.booleanValue()) {
				// requestSaveAction();
				// } else {
				// dispose();
				// CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE,
				// true);
				// }
				if (result.booleanValue()) {
					isCanceling = true;
					requestReset();
//					dispose();
//					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
				Integer result = (Integer) msg;
				if (result.intValue() == 0) {
					requestSaveAction();
					// CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE,
					// true);
				} else if (result.intValue() == 1) {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				} else if (result.intValue() == 2) {

				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL) {

				if (!needStayHere) {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					focus = 0;
					btnFocus = 0;
					state = STATE_MENU;
				} else {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			}
			repaint();
		}
	}

	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("GamePackagesUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
					+ obj);
		}

		if (type == URLType.LOAD_GAME) {
			if (obj instanceof Services) {
				services = (Services) obj;
				if (Log.DEBUG_ON) {
					Log.printDebug("GamePackagesUI: receiveCyoData: cyoData length = " + services.getCyoObject().length);
				}

				if (services.getSuccess().equals("1")) {
					CyoObject[] groups = services.getCyoObject();
					if (groups != null && groups[0] instanceof Group) {
						loadedGroup = (Group) groups[0];
						if (Log.DEBUG_ON) {
							Log.printDebug("GamePackagesUI: receiveCyoData: groups length = " + groups.length);
							Log.printDebug("GamePackagesUI: receiveCyoData: groups[0].getCyoObject length = "
									+ groups[0].getCyoObject().length);
						}

						buildDataForList(groups);
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("GamePackagesUI: receiveCyoData: obj = " + obj);
						}
						setPopup(notiPop);
						needStayHere = false;
						notiPop.setExplain(KeyNames.NOT_GOTO_GAME);
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					}
				} else {
					CyoObject[] objs = obj.getCyoObject();

					if (objs != null && objs[0] != null) {
						Message msg = (Message) objs[0];

						setPopup(notiPop);
						needStayHere = false;
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

						if ((msg.getType()).equals("5")) {
							expired = true;
						}
					}
				}
			}

			repaint();
		} else if (type == URLType.SAVE_GAME) {
			if (Log.DEBUG_ON) {
				Log.printDebug("GamePackagesUI: receiveCyoData(): needStayHere =" + needStayHere);
			}
			if (obj.getSuccess().equals("1")) {

				if (needStayHere || isCanceling) {
					dispose();
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
					return;
				}

				CyoObject[] subObjs = obj.getCyoObject();
				if (subObjs != null) {
					StringBuffer sf = new StringBuffer();
					Message message = (Message) subObjs[0];
					Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());
					if (message.getType().equals("0")) {// &&
														// message.getId().equals("140")
						sf.append(message.getText());
						sf.append("\n");
						if (!saveMessageView) {
							saveMessageView = true;
							needStayHere = false;
							setPopup(notiPop);
							notiPop.setExplain(sf.toString());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
					} else if (message.getType().equals("1")) {
						setPopup(notiPop);
						notiPop.setExplain(message.getText());
						notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
					} else {
						ManageTVServiceUI.subDataChanged = true;
						dispose();
						CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
					}
				} else {
					ManageTVServiceUI.subDataChanged = true;
					dispose();
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					setPopup(notiPop);
					notiPop.setExplain(msg.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

					/*
					 * if receive a following message, stay in current screen.
					 * 
					 * @0x00001348|JAVA|<messages success="0"
					 * 
					 * @0x00001348|JAVA|
					 * sessionId="00000btXB-8BHgbCLp1HtetMLk_:14ubho8dg"
					 * ><message
					 * 
					 * @0x00001348|JAVA| text=
					 * "To complete your selection of 10 games ࡬a carte, you must choose 10 game(s)."
					 * 
					 * @0x00001348|JAVA| type="3" id="282"/></messages>
					 * 
					 * @0x00001348|JAVA|
					 */
					if (msg.getType().equals("3") && msg.getId().equals("282")) {
						needStayHere = true;
					} else {
						needStayHere = false;
					}

					if ((msg.getType()).equals("5")) {
						expired = true;
					}
				}
			}
		}
	}

	public void receiveCyoError(int type) {
	}

	public ListUI getGamePacList() {
		return gamePacList;
	}

	public boolean getChanged() {
		if (Log.DEBUG_ON) {
			Log.printDebug("GamePackagesUI: getChanged: isUPdated = " + isUpdated);
		}

		// It means a selection of Packages changed in GameUI (CYO_031)
		if (isUpdated)
			return true;

		if (addVector.size() > 0 || removeVector.size() > 0) {
			for (int a = 0; a < addVector.size(); a++) {
				CyoObject obj = (CyoObject) addVector.elementAt(a);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}

			for (int a = 0; a < removeVector.size(); a++) {
				CyoObject obj = (CyoObject) removeVector.elementAt(a);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("r getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("r getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}
		}

		return false;
	}
}

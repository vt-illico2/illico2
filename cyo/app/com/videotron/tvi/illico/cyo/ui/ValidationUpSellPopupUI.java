package com.videotron.tvi.illico.cyo.ui;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import org.ocap.ui.event.OCRcEvent;

import java.awt.*;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zestyman on 2016-08-17.
 */
public class ValidationUpSellPopupUI extends BaseUI {

	private ClickingEffect clickEffect;
	private BaseUI parentUI;
	private Message message;

	public ValidationUpSellPopupUI() {
		setRenderer(new ValidationUpSellRenderer());
		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI parentUI, Message message) {
		this.parentUI = parentUI;
		this.message = message;
		prepare();

		start();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case OCRcEvent.VK_LEFT:
			if (focus != 0) {
				focus = 0;
				repaint();
			}
			return true;
		case OCRcEvent.VK_RIGHT:
			if (focus != 1) {
				focus = 1;
				repaint();
			}
			return true;

		case OCRcEvent.VK_ENTER:

			clickEffect.start(223 + focus * 263, 355, 255, 32);

			if (focus == 0) {
				parentUI.popupClosed(this, Boolean.TRUE);
			} else if (focus == 1) {
				parentUI.popupClosed(this, Boolean.FALSE);
			}

			return true;

		case KeyCodes.LAST:
		case OCRcEvent.VK_EXIT:
			return true;

		}
		return super.handleKey(code);
	}

	public Message getMessage() {
		return message;
	}

	private class ValidationUpSellRenderer extends BaseRenderer {

		private String titleStr, questionStr, packageBtnStr, blockBtnStr, continueBtnStr, myBtnStr;

		protected void loadImages(BaseUI ui) {
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Constants.SCREEN_BOUNDS;
		}

		public void prepare(UIComponent c) {
			loadImages((BaseUI) c);

			titleStr = BaseUI.getMenuText(KeyNames.VALIDATION_UPSELL_TITLE);
			questionStr = BaseUI.getMenuText(KeyNames.VALIDATION_UPSELL_QUESTION);
			packageBtnStr = BaseUI.getMenuText(KeyNames.BTN_MODIFY_MY_PACKAGE);
			blockBtnStr = BaseUI.getMenuText(KeyNames.BTN_ADD_BLOCK_CHANNELS);
			continueBtnStr = BaseUI.getMenuText(KeyNames.BTN_CONTINUE_TRANSACTION);

			if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {
				myBtnStr = blockBtnStr;
			} else {
				myBtnStr = packageBtnStr;
			}
		}

		protected void paint(Graphics g, UIComponent c) {
			// dimmed bg
			g.setColor(dimmedBgColor);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			// background
			g.setColor(C044044044);
			g.fillRect(200, 113, 565, 299);
			g.setColor(C066066066);
			g.fillRect(220, 152, 525, 1);

			// title
			g.setFont(f24);
			g.setColor(popupTitleColor);
			GraphicUtil.drawStringCenter(g, titleStr, 481, 140);

			// message
			int startY = 217;
			int stepY = 20;
			g.setFont(f19);
			g.setColor(dimmedMenuColor);
			String modifiedStr = TextUtil.replace(message.getText(), "\\n", "|");
			String[] msgStr = TextUtil.split(modifiedStr, g.getFontMetrics(), 490, "|");

			if (msgStr.length % 2 == 0) {
				startY -= (msgStr.length / 2 + 10);
			} else {
				startY -= (msgStr.length / 2);
			}

			for (int i = 0; i < msgStr.length; i++) {
				GraphicUtil.drawStringCenter(g, msgStr[i], 482, startY + i * stepY);
			}

			// question
			GraphicUtil.drawStringCenter(g, questionStr, 482, 312);

			// button
			if (focus == 0) {
				g.setColor(C255191000);
				g.fillRect(223, 355, 255, 32);
				g.setColor(C157157157);
				g.fillRect(486, 355, 255, 32);
			} else {
				g.setColor(C157157157);
				g.fillRect(223, 355, 255, 32);
				g.setColor(C255191000);
				g.fillRect(486, 355, 255, 32);
			}
			g.setFont(f18);
			g.setColor(btnColor);
			GraphicUtil.drawStringCenter(g, myBtnStr, 353, 377);
			GraphicUtil.drawStringCenter(g, continueBtnStr, 614, 377);
		}
	}
}

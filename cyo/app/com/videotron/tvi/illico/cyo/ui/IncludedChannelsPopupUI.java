package com.videotron.tvi.illico.cyo.ui;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import org.ocap.ui.event.OCRcEvent;

import java.awt.*;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-08-01.
 */
public class IncludedChannelsPopupUI extends BaseUI {

	private BaseUI parent;
	private Package targetPackage;
	private CyoObject[] channels;

	private int startIdx;
	private int descStartIdx;
	private String[] currentDesc;

	public IncludedChannelsPopupUI() {
		setRenderer(new IncludedChannelsRenderer());
	}

	public void show(BaseUI parent, Package p) {
		prepare();
		this.parent = parent;

		this.targetPackage = p;
		this.channels = targetPackage.getCyoObject();

		focus = 0;
		startIdx = 0;
		// VDTRMASTER-5882
		descStartIdx = 0;

		footer.reset();
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_EXIT), BaseUI.getMenuText(KeyNames.BTN_CLOSE));

		footer.setBounds(10, 448, 755, 20);
		add(footer);

		start();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case OCRcEvent.VK_UP:
			if (targetPackage != null && channels != null) {
				if (focus > 3) {
					focus--;
				} else if (startIdx > 0) {
					if (focus < 4) {
						startIdx--;
					} else {
						focus--;
					}
				} else {
					focus--;
				}
				if (focus < 0) {
					focus = 0;
				}
				repaint();
			}
			return true;

		case OCRcEvent.VK_DOWN:
			if (targetPackage != null && channels != null) {
				if (focus < 3 && startIdx + focus < channels.length - 1) {
					focus++;
				} else if (startIdx + 7 < channels.length) {
					if (focus > 2) {
						startIdx++;
					} else {
						focus++;
					}
				} else if (startIdx + focus < channels.length - 1) {
					focus++;
				}

				repaint();
			}
			return true;

		case OCRcEvent.VK_PAGE_UP:
			footer.clickAnimation(0);
			// VDTRMASTER-5882
			if (currentDesc != null) {
				if (--descStartIdx < 0) {
					descStartIdx = 0;
				}
			}
			repaint();
			return true;

		case OCRcEvent.VK_PAGE_DOWN:
			footer.clickAnimation(0);
			// VDTRMASTER-5882

			if (currentDesc != null) {
				if (++descStartIdx > currentDesc.length - 3) {
					descStartIdx--;
				}
			}
			repaint();
			return true;

		case OCRcEvent.VK_EXIT:
		case KeyCodes.LAST:
			footer.clickAnimation(1);
			stop();
			parent.popupClosed(this, Boolean.TRUE);
			return true;
		}

		return true;
	}

	class IncludedChannelsRenderer extends BaseRenderer {

		private Image listBgTopImg, listBgMidImg, listBgBtImg, descShadowImg;
		private Image scrollTopImg, scrollBtImg, scrollMidImg, scrollBarImg;
		private Image iconEnImg, iconFocusEnImg, iconFrImg, iconFocusFrImg, iconHdImg, iconFocusHdImg;
		private Image arrowUpImg, arrowDownImg;

		private Channel lastChInfo;

		public Rectangle getPreferredBounds(UIComponent c) {
			return Constants.SCREEN_BOUNDS;
		}

		public void prepare(UIComponent c) {
			loadImages((BaseUI) c);
			super.prepare(c);
		}

		protected void paint(Graphics g, UIComponent c) {
			g.setColor(dimmedBgColor);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			// background
			g.setColor(C044044044);
			g.fillRect(199, 83, 566, 359);
			g.setColor(C066066066);
			g.fillRect(219, 122, 526, 1);

			// title
			g.setColor(C249195000);
			g.setFont(f24);
			GraphicUtil.drawStringCenter(g, targetPackage.getTitle(), 481, 110);

			// list background
			g.drawImage(listBgTopImg, 224, 135, c);
			g.drawImage(listBgMidImg, 224, 145, 516, 270, c);
			g.drawImage(listBgBtImg, 224, 415, c);
			g.setColor(C041041041);
			for (int i = 0; i < 4; i++) {
				g.fillRect(225, 168 + i * 64, 514, 32);
			}

			// channel list
			int limitLength = channels.length > 7 ? 7 : channels.length;
			int stepY = 0;
			for (int i = 0; i < limitLength; i++) {

				Channel chInfo = CyoDataManager.getInstance()
						.getChannelInfoById(((Channel) channels[startIdx + i]).getId());

				if (i == focus) {
					// focus bg
					g.setColor(C170170170);
					g.fillRect(224, 138 + stepY, 516, 94);
					g.setColor(C246193001);
					g.fillRect(224, 135 + stepY, 516, 34);

					g.setColor(btnColor);
					if (chInfo != null) {
						// channel no
						g.setFont(f21);
						String no = chInfo.getHdPosition();
						if (no != null) {
							g.drawString(no, 238, 158 + stepY);
						}

						// ch logo
						Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
						if (logoImg != null) {
							g.drawImage(logoImg, 288, 138 + stepY, 80, 24, c);
						}

						// ch name
						g.setFont(f20);
						String title = TextUtil.shorten(chInfo.getTitle(), fm20, 275);
						g.drawString(title, 386, 158 + stepY);

						// language
						String language = chInfo.getLanguage();
						Image langIcon = null;
						if (language != null) {
							if (language.equals(Constants.LANGUAGE_FRENCH)) {
								langIcon = iconFocusFrImg;
							} else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
								langIcon = iconFocusEnImg;
							}
						}
						g.drawImage(langIcon, 676, 145 + stepY, c);

						// definition
						boolean hd = chInfo.hasHDPosition();
						if (hd) {
							g.drawImage(iconFocusHdImg, 705, 145 + stepY, c);
						}

						// description
						g.setFont(f14);
						// VDTRMASTER-5882
						currentDesc = TextUtil.split(chInfo.getDesc(), fm14, 465);

						if (lastChInfo == null || lastChInfo != chInfo) {
							descStartIdx = 0;
							lastChInfo = chInfo;
						}

						if (currentDesc != null) {
							int count = 0;
							for (int descIdx = descStartIdx;
								 descIdx < currentDesc.length && descIdx < descStartIdx + 3; descIdx++) {
								g.drawString(currentDesc[descIdx], 238, 190 + stepY + count * 14);
								count++;
							}
							if (descStartIdx + 3 < currentDesc.length) {
								g.drawImage(descShadowImg, 228, 205 + stepY, 511, 23, c);
							}

							// scroll
							g.drawImage(scrollTopImg, 719, 178 + stepY, c);
							g.drawImage(scrollMidImg, 719, 188 + stepY, 7, 25, c);
							g.drawImage(scrollBtImg, 719, 213 + stepY, c);

							int scrollGap = 0;
							if (descStartIdx == 0) {
								scrollGap = 0;
							} else {
								scrollGap = 21 * (descStartIdx * 100 / (currentDesc.length - 3)) / 100;
							}

							g.drawImage(scrollBarImg, 716, 178 + stepY + scrollGap, c);
						}
					}
					stepY += 94;
				} else {
					g.setColor(titleColor);
					if (chInfo != null) {
						// channel no
						g.setFont(f18);
						String no = chInfo.getHdPosition();
						if (no != null) {
							g.drawString(no, 238, 158 + stepY);
						}

						// ch logo
						Image logoImg = CyoDataManager.getInstance().getChannelLogo(chInfo.getPosition());
						if (logoImg != null) {
							g.drawImage(logoImg, 288, 138 + stepY, 80, 24, c);
						}

						// ch name
						String title = TextUtil.shorten(chInfo.getTitle(), fm18, 275);
						g.drawString(title, 386, 158 + stepY);

						// language
						String language = chInfo.getLanguage();
						Image langIcon = null;
						if (language != null) {
							if (language.equals(Constants.LANGUAGE_FRENCH)) {
								langIcon = iconFrImg;
							} else if (language.equals(Constants.LANGUAGE_ENGLISH)) {
								langIcon = iconEnImg;
							}
						}
						g.drawImage(langIcon, 676, 145 + stepY, c);

						// definition
						boolean hd = chInfo.hasHDPosition();
						if (hd) {
							g.drawImage(iconHdImg, 705, 145 + stepY, c);
						}
					}
					stepY += 32;
				}
			}

			if (startIdx > 0) {
				g.drawImage(arrowUpImg, 469, 126, c);
			}
			if (startIdx < channels.length - 7) {
				g.drawImage(arrowDownImg, 469, 416, c);
			}
		}

		protected void loadImages(BaseUI ui) {
			listBgTopImg = ui.getImage("07_res_pgbg_t.png");
			listBgMidImg = ui.getImage("07_res_pgbg_m.png");
			listBgBtImg = ui.getImage("07_res_pgbg_b.png");
			descShadowImg = ui.getImage("17_list_sha_b.png");
			scrollTopImg = ui.getImage("scrbg_up.png");
			scrollBtImg = ui.getImage("scrbg_dn.png");
			scrollMidImg = ui.getImage("scr_m.png");
			scrollBarImg = ui.getImage("scr_bar.png");
			iconEnImg = ui.getImage("icon_en.png");
			iconFocusEnImg = ui.getImage("icon_en_foc.png");
			iconFrImg = ui.getImage("icon_fr.png");
			iconFocusFrImg = ui.getImage("icon_fr_foc.png");
			iconHdImg = ui.getImage("icon_hd.png");
			iconFocusHdImg = ui.getImage("icon_hd_foc.png");
			arrowUpImg = ui.getImage("02_ars_t.png");
			arrowDownImg = ui.getImage("02_ars_b.png");
		}
	}
}

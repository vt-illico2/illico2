/**
 * @(#)ChangeChannelSelectionUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.rmi.RemoteException;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.popup.MenuItem;
import com.videotron.tvi.illico.cyo.popup.MenuListener;
import com.videotron.tvi.illico.cyo.popup.OptionScreen;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.gui.ChangeChannelSelectionRenderer;
import com.videotron.tvi.illico.cyo.gui.ManageGroupRenderer;
import com.videotron.tvi.illico.cyo.gui.SelfServeRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class manage a group to use LoadGroup.action (2.4.5) in STATE C.
 * @author Woojung Kim
 * @version 1.1
 */
public class ManageGroupUI extends BaseUI implements CyoDataListener, MenuListener {
    public static final int STATE_LIST = 0x0;
    public static final int STATE_BUTTON = 0x1;
    public static final int STATE_MENU = 0x02;

    private ClickingEffect clickEffect;
    private ListUI groupList = new ListUI(true);

    private Group loadedGroup;

    private Vector customAddVector = new Vector();
    private Vector customRemoveVector = new Vector();

    private boolean isModified = false;

    private int btnFocus = 0;
    private Services services;

    private ListUI currentList;

    private String title;
    private String explain;

    private boolean saveMessageView = false;
    private boolean channelInfoBoolean = false;
    private boolean isBasicService;

    public OptionScreen optScr;
    public static MenuItem sortOption = new MenuItem("menu.sort");
    public static MenuItem nSORT = new MenuItem("menu.sorted.number");
    public static MenuItem aSORT = new MenuItem("menu.sorted.alphabetical");

    public static final String A_LA_CARTE_GROUP_NO = "1518";
    private boolean isALaCarte;
    private String sort;
    private int lastRequestedURLType;
    private CyoObject lastObject;
    private Vector messagePopupVector = new Vector();

    public ManageGroupUI() {
        renderer = new ManageGroupRenderer();
        setRenderer(renderer);
    }

    public void start(boolean back) {
        CyoDataManager.getInstance().addCyoDataListener(this);
        if (!back) {
            prepare();
            currentList = null;
            channelInfoBoolean = false;
            saveMessageView = false;

            customAddVector.clear();
            customRemoveVector.clear();

            isALaCarte = false;

            CyoObject obj = (CyoObject) DataCenter.getInstance().get("LoadGroup");

            if (obj != null) {
                DataCenter.getInstance().remove("LoadGroup");
                Group group = (Group) obj;

                if (Log.DEBUG_ON) {
                    Log.printDebug("ManageGroupUI: start: group no = " + group.getNo());
                }

                if (group.getNo() != null && group.getNo().equals("1514")) {
                    isBasicService = true;
                    groupList.setIsBasicService(true);
                } else {
                    isBasicService = false;
                    groupList.setIsBasicService(false);
                }

                if (group.getNo() != null && group.getNo().equals(A_LA_CARTE_GROUP_NO)) {
                    isALaCarte = true;
                    sort = Param.SORT_ALPHABETICAL;
                }

                Hashtable param = new Hashtable();
                param.put("url", group.getUrl());
                CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
            } else {
                // from isa for A la carte
                if (Log.DEBUG_ON) {
                    Log.printDebug("ManageGroupUI: start: no group info - A la carte");
                }

                isBasicService = false;
                groupList.setIsBasicService(false);

                isALaCarte = true;
                sort = Param.SORT_ALPHABETICAL;

                Hashtable param = new Hashtable();
                param.put("groupNo", A_LA_CARTE_GROUP_NO);
                param.put("sort", sort);
                CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
            }
        }

        sortOption.clear();
        sortOption.add(aSORT);
        sortOption.add(nSORT);

        super.start(back);
    }

    public void prepare() {
        state = STATE_LIST;
        btnFocus = 0;
        focus = 0;
        state = STATE_LIST;
        isModified = false;
        title = TextUtil.EMPTY_STRING;
        explain = TextUtil.EMPTY_STRING;
        setFooter();
        super.prepare();
    }

    public void stop() {
        if (messagePopupVector != null) {
            messagePopupVector.clear();
            messagePopupVector = null;
        }

        optScr = null;

        clearPopup();

        super.stop();
    }

    public void dispose() {
    	title = TextUtil.EMPTY_STRING;
        explain = TextUtil.EMPTY_STRING;
        clearPopup();
        clickEffect = null;
        customAddVector.clear();
        customRemoveVector.clear();
        groupList.stop();
        super.dispose();
    }

    public boolean handleKey(int code) {

        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_LEFT:
            keyLeft();
            return true;
        case OCRcEvent.VK_RIGHT:
            keyRight();
            return true;
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case OCRcEvent.VK_PAGE_UP:
            pageUp();
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            pageDown();
            return true;
        case OCRcEvent.VK_ENTER:
            keyEnter();
            return true;
        case KeyCodes.COLOR_A:
            if (loadedGroup != null && isALaCarte) {

                footer.clickAnimation(2);

                if (optScr == null) {
                    optScr = new OptionScreen();
                }

                if (sort == Param.SORT_NUMBER) {
                    nSORT.setChecked(true);
                    aSORT.setChecked(false);
                } else {
                    nSORT.setChecked(false);
                    aSORT.setChecked(true);
                }

                optScr.start(sortOption, this);
            }

            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0);
            if (getChanged()) {
                setPopup(notiPop);
//                notiPop.show(this, NotificationPopupUI.STATE_CANCEL,
//                        BaseUI.getMenuText(KeyNames.CANCEL_VALIDATE_TITLE), BaseUI
//                                .getExplainText(KeyNames.CANCEL_VALIDATE_EXPLAIN));
                setPopup(notiPop);
                notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
            } else {
                CyoController.getInstance().showUI(lastUIId, true);
            }
            repaint();
            return true;
        default:
            return false;
        }
    }

    private void keyLeft() {
        if ((state == STATE_BUTTON || state == STATE_MENU)&& currentList.getCyoObjectLength() > 0) {
            state = STATE_LIST;
            currentList.setHasFocus(true);
            repaint();
        } else if (state == STATE_BUTTON) {
            state = STATE_LIST;
            btnFocus = 0;
            repaint();
        }

    }

    private void keyRight() {
        if (state == STATE_LIST) {
        	if (isBasicService) {
        		state = STATE_MENU;
	            btnFocus = getChanged() ? 0 : 1;
	            currentList.setHasFocus(false);
        	} else {
	            state = STATE_BUTTON;
	            btnFocus = getChanged() ? 0 : 1;
	            currentList.setHasFocus(false);
        	}
        	 repaint();
        }
    }

    private void keyUp() {
        if (state == STATE_LIST) {
            currentList.handleKey(OCRcEvent.VK_UP);
            repaint();
        } else if (state == STATE_BUTTON) {
        	if (isBasicService) {
        		boolean isChanged = getChanged();
        		if (btnFocus == 2) {
        			btnFocus = 1;
        		} else if (btnFocus == 1 && isChanged) {
        			btnFocus = 0;
        		} else if (isBasicService){ //if (btnFocus == 1 && !isChanged) {
        			state = STATE_MENU;
    	            //btnFocus = getChanged() ? 0 : 1;
    	            //currentList.setHasFocus(false);
        		}
        		repaint();
        	} else {
	            if (--btnFocus <= 0) {
	                btnFocus = getChanged() ? 0 : 1;
	            }
        	}
            repaint();
        }
    }

    private void keyDown() {
        if (state == STATE_LIST) {
            currentList.handleKey(OCRcEvent.VK_DOWN);
            repaint();
        } else if (state == STATE_BUTTON) {
            if (++btnFocus > 2) {
                btnFocus = 2;
            }
            repaint();
        } else if (state == STATE_MENU) {
        	state = STATE_BUTTON;
			btnFocus = getChanged() ? 0 : 1;
			repaint();
        }
    }

    private void pageUp() {
        if (state == STATE_LIST) {
            footer.clickAnimation(1);
            currentList.handleKey(OCRcEvent.VK_PAGE_UP);
            repaint();
        }
    }

    private void pageDown() {
        if (state == STATE_LIST) {
            footer.clickAnimation(1);
            currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
            repaint();
        }
    }

    private void keyEnter() {
        if (state == STATE_LIST) {
            CyoObject curObj = currentList.getCurrentCyoObject();
            if (curObj instanceof Package && currentList.isFocusOnViewChanenlIncluded()) {
                // show included channels popup
                if (Log.DEBUG_ON) {
                    Log.printDebug("ManageGroupUI, keyEnter, show included channels popup");
                }

                setPopup(includedChannelsPopup);
                includedChannelsPopup.show(this, (Package) curObj);
            } else {
                updateSubscribed(curObj);
                needExitPopup = true;
            }
        } else if (state == STATE_BUTTON) {

            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }

            clickEffect.start(717, 346 + btnFocus * 37, 180, 35);

            if (btnFocus == 0) { // Confirm
                if (isModified) {
                    // VDTRMASTER-5958
                    CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                    requestSaveAction();
                } else {
                    CyoController.getInstance().showUI(lastUIId, true);
                }
            } else if (btnFocus == 1) { // Cancel
            	if (isModified) {
            		 setPopup(notiPop);
                     notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
            	} else {
                    CyoController.getInstance().showUI(lastUIId, true);
            	}
            } else if (btnFocus == 2) { // Help
                DataCenter.getInstance().put("MODE", "HELP");
                CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
            }
        } else if (state == STATE_MENU) {
        	CyoObject curObj = currentList.getCurrentCyoObject();

			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}
			clickEffect.start(718, 237, 172, 30);

			DataCenter.getInstance().put("Package", curObj);
			CyoController.getInstance().showUI(BaseUI.UI_VIEW_CHANNELS, false);
        }
        repaint();
    }

    private void setFooter() {

        if (loadedGroup != null && isALaCarte) {
            footer.reset();
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A), BaseUI.getMenuText(KeyNames.BTN_SORTING));
        } else {
            footer.reset();
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
        }

    }

    public boolean isALaCarte() {
		return isALaCarte;
	}

    private void updateInfomation() {

        String no = loadedGroup.getNo();

        if (Log.DEBUG_ON) {
            Log.printDebug("ManageGroupUI: prepare(): no = " + no);
        }

        if (no != null) {
            if (no.equals("1534")) {
                title = BaseUI.getMenuText(KeyNames.HD_CHANNEL_PACAKGES);
                explain = BaseUI.getExplainText(KeyNames.HD_CHANNEL_PACAKGES_EXPLAIN);
            } else if (no.equals("1517")) {
                title = BaseUI.getMenuText(KeyNames.THEME_PACKAGES);
                explain = BaseUI.getExplainText(KeyNames.THEME_PACKAGES);
            } else if (no.equals("1518")) {
//                title = BaseUI.getMenuText(KeyNames.SPECIALTY_PACKAGES);
//                explain = BaseUI.getExplainText(KeyNames.SPECIALTY_PACKAGES_EXPLAIN);
                title = BaseUI.getMenuText(KeyNames.A_LA_CARTE_CHANNELS);
                explain = BaseUI.getExplainText(KeyNames.A_LA_CARTE_CHANNELS);
            } else if (no.equals("1535")) {
                title = BaseUI.getMenuText(KeyNames.MOVIE_SERIES_PACKAGES);
                explain = BaseUI.getExplainText(KeyNames.MOVIE_SERIES_PACKAGES_EXPLAIN);
            } else if (no.equals("1516")) {
                title = BaseUI.getMenuText(KeyNames.POPULAR_PACKAGES);
                explain = BaseUI.getExplainText(KeyNames.POPULAR_PACKAGES);
            } else if (no.equals("1514")) {
            	title = BaseUI.getMenuText(KeyNames.BASIC_CHANNEL_LIST_TITLE);
                explain = BaseUI.getMenuText(KeyNames.BASIC_CHANNEL_LIST_EXPLAIN);
            } else {
                title = loadedGroup.getTitle();

                title = BaseUI.getMenuText(KeyNames.A_LA_CARTE_CHANNELS);
                explain = BaseUI.getExplainText(KeyNames.A_LA_CARTE_CHANNELS);
            }
        } else {
            if (loadedGroup != null) {
                title = loadedGroup.getTitle();
                explain = BaseUI.getExplainText(KeyNames.POPULAR_PACKAGES);
            }
        }
    }

    public String getTitle() {
        return title;
    }

    public String getExplain() {
        return explain;
    }

    public boolean isBasicService() {
    	return isBasicService;
    }

    private void updateSubscribed(CyoObject obj) {
        if (obj instanceof Package) {
            Package curPackage = ((Package) obj);
            String modiable = curPackage.getModifiable();
            if (Log.DEBUG_ON) {
                Log.printDebug("ManageGroupUI: Package modiable = " + modiable);
                Log.printDebug("ManageGroupUI: isBasicService = " + isBasicService);
            }
            if (modiable.equals("0")) {

            	if (isBasicService && curPackage.getSubscribedInSession()) {
            		if (Log.DEBUG_ON) {
                        Log.printDebug("ManageGroupUI: Package SubscribedInSession = " + curPackage.getSubscribedInSession());
                    }
            		return;
            	}

                // Package
                curPackage.setSubscribedInSession(!curPackage.getSubscribedInSession());
                CyoObject[] childObjs = curPackage.getCyoObject();

                for (int i = 0; childObjs != null && i < childObjs.length; i++) {
                    Subscription child = (Subscription) childObjs[i];
                    if (child.getModifiable().equals("1")) {
                        child.setSubscribedInSession(curPackage.getSubscribedInSession());
                    }
                }

                if (curPackage.getSubscribedInSession()) {
                    addCyoObjectInMyPicks(obj);
                } else {
                    removeCyoObjectInMyPicks(obj);
                }

                if (isBasicService) {
                	CyoObject[] subObjs = curPackage.getParentObj().getCyoObject();
    				// CyoObject[] subObjs = parentObj.getCyoObject();
    				for (int i = 0; i < subObjs.length; i++) {
    					if (!subObjs[i].equals(curPackage) && ((Subscription) subObjs[i]).getSubscribedInSession()) {
    						((Subscription) subObjs[i]).setSubscribedInSession(false);
    						removeCyoObjectInMyPicks(subObjs[i]);
    					} else if (!subObjs[i].equals(curPackage) && !((Subscription) subObjs[i]).getSubscribedInSession()) {
    						addCyoObjectInMyPicks(subObjs[i]);
    					}
    				}
                }

                isModified = true;
            } else if (modiable.equals("1")) {
                channelInfoBoolean = true;
                Log.printInfo("UNSELECT_MESSAGE_1");
                setPopup(notiPop);
                notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
            } else if (modiable.equals("2")) {
                channelInfoBoolean = true;
                Log.printInfo("UNSELECT_MESSAGE_2");
                setPopup(notiPop);
                notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
            } else if (modiable.equals("3")) {
                channelInfoBoolean = true;
                Log.printInfo("UNSELECT_MESSAGE_3");
                setPopup(notiPop);
                notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
            }

        } else if (obj instanceof Subscription) {
            Subscription subObj = ((Subscription) obj);
            String modiable = subObj.getModifiable();
            if (Log.DEBUG_ON) {
                Log.printDebug("ManageGroupUI: Channel modiable = " + modiable);
            }

            CyoObject parentObj = subObj.getParentObj();
            if (parentObj instanceof Package) {
                Package parentPackage = (Package) parentObj;
                String pModiable = parentPackage.getModifiable();
                if (Log.DEBUG_ON) {
                    Log.printDebug("ManageGroupUI: Parent Package modiable = " + pModiable);
                }
                if (pModiable.equals("0") && modiable.equals("0")) {
                    /*
                     * Package = 0; Channel = 0: The user can add / remove a package, as well as channels in it. In this
                     * case, the user can do anything (add/remove a package, add/remove a channel from within the
                     * package with no consequences)
                     */
                    subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

                    if (subObj.getSubscribedInSession()) {
                        addCyoObjectInMyPicks(obj);
                    } else {
                        removeCyoObjectInMyPicks(obj);
                    }
                    isModified = true;
                } else if (pModiable.equals("0") && !modiable.equals("0")) {
                    /*
                     * Package = 0; Channel 1: The user can remove/add the package but not individual channels. If the
                     * user unchecks a channel, the whole package is automatically unchecked with all other channels in
                     * it.
                     */

//                	if (subObj.getSubscribedInSession() && !parentPackage.getSubscribedOrginalInSession()) {
//                		// this case can remove a channel when package is not subscribed but some sub channel is subscribed
//                		subObj.setSubscribedInSession(!subObj.getSubscribedInSession());
//
//                        if (subObj.getSubscribedInSession()) {
//                            addCyoObjectInMyPicks(subObj);
//                        } else {
//                            removeCyoObjectInMyPicks(subObj);
//                        }
//                	} else {
//	                    subObj.setSubscribedInSession(!subObj.getSubscribedInSession());
//	                    parentPackage.setSubscribedInSession(subObj.getSubscribedInSession());
//	                    CyoObject[] childObjs = parentPackage.getCyoObject();
//
//	                    for (int i = 0; childObjs != null && i < childObjs.length; i++) {
//	                        Subscription child = (Subscription) childObjs[i];
//	                        if (child.getModifiable().equals("1")) {
//	                            child.setSubscribedInSession(parentPackage.getSubscribedInSession());
//	                        }
//	                    }
//
//	                    if (parentPackage.getSubscribedInSession()) {
//	                        addCyoObjectInMyPicks(parentPackage);
//	                    } else {
//	                        removeCyoObjectInMyPicks(parentPackage);
//	                    }
//                	}

                	subObj.setSubscribedInSession(!subObj.getSubscribedInSession());
                    parentPackage.setSubscribedInSession(subObj.getSubscribedInSession());
                    CyoObject[] childObjs = parentPackage.getCyoObject();

                    for (int i = 0; childObjs != null && i < childObjs.length; i++) {
                        Subscription child = (Subscription) childObjs[i];
                        if (child.getModifiable().equals("1")) {
                            child.setSubscribedInSession(parentPackage.getSubscribedInSession());
                        }
                    }

                    if (parentPackage.getSubscribedInSession()) {
                        addCyoObjectInMyPicks(parentPackage);
                    } else {
                        removeCyoObjectInMyPicks(parentPackage);
                    }
                    isModified = true;
                } else if (!pModiable.equals("0") && modiable.equals("0")) {
                    /*
                     * Package = 1; Channel 0: The user cannot add/remove the package, but he can change individual
                     * channels in it. If the user checks/unckecks a channel, it has an effect only on that channel.
                     */
                    subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

                    if (subObj.getSubscribedInSession()) {
                        addCyoObjectInMyPicks(subObj);
                    } else {
                        removeCyoObjectInMyPicks(subObj);
                    }
                    isModified = true;
                } else if (modiable.equals("1")) {
                    channelInfoBoolean = true;
                    Log.printInfo("UNSELECT_MESSAGE_1");
                    setPopup(notiPop);
                    notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                } else if (modiable.equals("2")) {
                    channelInfoBoolean = true;
                    Log.printInfo("UNSELECT_MESSAGE_2");
                    setPopup(notiPop);
                    notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                } else if (modiable.equals("3")) {
                    channelInfoBoolean = true;
                    Log.printInfo("UNSELECT_MESSAGE_3");
                    setPopup(notiPop);
                    notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                }
//            } else if (subObj.getNo().equals("926")) {
//        		Log.printInfo("This is HD Network Fees");
//        		setPopup(notiPop);
//                notiPop.setExplain(BaseUI.getExplainText(KeyNames.HD_NETWORK_FEES));
//                notiPop.show(this, NotificationPopupUI.STATE_HD_NETWORK_FEES);
            } else {

            	if (subObj.getNo().equals("926") && !modiable.equals("0") && subObj.getSubscribedInAccount()) {
					Log.printInfo("This is HD Network Fees");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.HD_NETWORK_FEES));
					notiPop.show(this, NotificationPopupUI.STATE_HD_NETWORK_FEES);
				} else if (modiable.equals("0")) {
                    subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

                    if (subObj.getSubscribedInSession()) {
                        addCyoObjectInMyPicks(subObj);
                    } else {
                        removeCyoObjectInMyPicks(subObj);
                    }
                    isModified = true;
                } else if (subObj.isSubscriptionTypeOfALaCarte()) {
                    Log.printDebug("UNSELECT_MESSAGE_FOR_A_LA_CARTE");
                    channelInfoBoolean = true;
                    setPopup(notiPop);
                    notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_FOR_A_LA_CARTE));
                    notiPop.show(this, NotificationPopupUI.STATE_UNSELECT_MESSAGE_FOR_A_LA_CARTE);
                }  else if (modiable.equals("1")) {
                    channelInfoBoolean = true;
                    Log.printInfo("UNSELECT_MESSAGE_1");
                    setPopup(notiPop);
                    notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                } else if (modiable.equals("2")) {
                    Log.printInfo("UNSELECT_MESSAGE_2");
                    channelInfoBoolean = true;
                    setPopup(notiPop);
                    notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                } else if (modiable.equals("3")) {
                    Log.printInfo("UNSELECT_MESSAGE_3");
                    channelInfoBoolean = true;
                    setPopup(notiPop);
                    notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                }

            }
        }
    }

    public int getButtonFocus() {
        return btnFocus;
    }

    public ListUI getCurrentList() {
        return currentList;
    }

    private void addCyoObjectInMyPicks(CyoObject obj) {

        if (customAddVector.contains(obj)) {
            customAddVector.remove(obj);
            customAddVector.add(obj);
            customRemoveVector.remove(obj);
        } else {
            customAddVector.add(obj);
            customRemoveVector.remove(obj);
        }
    }

    private void removeCyoObjectInMyPicks(CyoObject obj) {
        if (customRemoveVector.contains(obj)) {
            customAddVector.remove(obj);
            customRemoveVector.remove(obj);
            customRemoveVector.add(obj);
        } else {
            customAddVector.remove(obj);
            customRemoveVector.add(obj);
        }
    }

    private void requestSaveAction() {
        String addParams = null, removeParams = null, groupNo = null;
        Hashtable param = new Hashtable();

        addParams = getNumbers(customAddVector);
        removeParams = getNumbers(customRemoveVector);
        groupNo = loadedGroup.getNo();

        if (addParams != null) {
            param.put("add", addParams);
        }
        if (removeParams != null) {
            param.put("remove", removeParams);
        }
        param.put("groupNo", groupNo);

        if (addParams != null || removeParams != null) {

            if (CyoDataManager.getInstance().needParamForRequiresUpsell()) {
                param.put("requiresUpsell", "false");
            }

            CyoDataManager.getInstance().request(URLType.SAVE_GROUP, param);
        } else {
            CyoController.getInstance().showUI(lastUIId, true);
        }
    }

    private String getNumbers(Vector vec) {
        if (vec.size() == 0) {
            return null;
        }

        StringBuffer sf = new StringBuffer();

        for (int i = 0; i < vec.size(); i++) {
            if (i > 0) {
                sf.append(",");
            }
            Subscription obj = (Subscription) vec.get(i);

            sf.append(obj.getNo());
//            if (obj.getNo() != null) {
//            	sf.append(obj.getNo());
//            } else if (obj instanceof Channel) {
//            	Channel ch = CyoDataManager.getInstance().getChannelInfoById(obj.getId());
//
//            	if (ch.getNo() != null) {
//            		sf.append(ch.getNo());
//            	}
//            }
        }

        return sf.toString();
    }

    public String getSortString() {
        if (sort.equals(Param.SORT_ALPHABETICAL)) {
            return BaseUI.getMenuText(KeyNames.SORTED_BY_ALPHABETICAL);
        } else {
            return BaseUI.getMenuText(KeyNames.SORTED_BY_CHANNEL_NUMBER);
        }
    }

    public void selected(MenuItem item) {
        // sort
        Hashtable param = new Hashtable();
        param.put("groupNo", loadedGroup.getNo());
        boolean checkSort = false;

        if (item.equals(nSORT)) {
            param.put("sort", Param.SORT_NUMBER);
            checkSort = sort == Param.SORT_NUMBER ? true : false;
            sort = Param.SORT_NUMBER;
        } else {
            param.put("sort", Param.SORT_ALPHABETICAL);
            checkSort = sort == Param.SORT_ALPHABETICAL ? true : false;
            sort = Param.SORT_ALPHABETICAL;
        }

        if (!checkSort) {
            CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
        }
    }

    public void canceled() {

    }

    private void buildDataForList(CyoObject[] data) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ManageGroupUI, buildDataForList, data=" + data + ", isALaCarte=" + isALaCarte);
        }

        if (isALaCarte) {

            Vector dataVector = new Vector();
            addCyoObject(dataVector, data);

            data = new CyoObject[dataVector.size()];
            dataVector.copyInto(data);

            for (int a = 0; data != null && a < data.length; a++) {
                if (data[a] instanceof Subscription) {
                    if (((Subscription) data[a]).getSubscribedInAccount() != ((Subscription) data[a]).getSubscribedInSession()) {
                        // update SubscribedInSession
                        String modifiable = ((Subscription) data[a]).getModifiable();
                        if (modifiable.equals("0")) {
                            // changed the one more.
                            ((Subscription) data[a]).setSubscribedInSession(!((Subscription) data[a]).getSubscribedInSession());

                            updateSubscribed(data[a]);
                        }
                        ManageTVServiceUI.subDataChanged = true;
                    }
                }
            }

            Hashtable addHash = new Hashtable();
            Hashtable removeHash = new Hashtable();
            Log.printDebug("ManageGroupUI, buildDataForList, customAddVector");
            for (int a = 0; a < customAddVector.size(); a++) {
                Subscription ss = (Subscription) customAddVector.elementAt(a);
                String key = ss instanceof Package || ss instanceof Product ?
                        ((Subscription) ss).getNo() : ((Subscription) ss).getId();
                Log.printDebug("ManageGroupUI, buildDataForList, customAddVector, ss.getNo() = " + key);
                addHash.put(key, ss);
            }

            Log.printDebug("ManageGroupUI, buildDataForList, customAddVector, addHash=" + addHash);

            Log.printDebug("ManageGroupUI, buildDataForList, customRemoveVector");
            for (int a = 0; a < customRemoveVector.size(); a++) {
                Subscription ss = (Subscription) customRemoveVector.elementAt(a);
                String key = ss instanceof Package || ss instanceof Product ?
                        ((Subscription) ss).getNo() : ((Subscription) ss).getId();
                Log.printDebug("ManageGroupUI, buildDataForList, customRemoveVector, ss.getNo() = " + key);
                removeHash.put(key, ss);
            }
            Log.printDebug("ManageGroupUI, buildDataForList, customRemoveVector, removeHash= " + removeHash);

            CyoObject[] cur = groupList.getAllCyoObject();
            Hashtable orignalHash = new Hashtable();
            Log.printDebug("ManageGroupUI, buildDataForList, make the previous data");
            for (int a = 0; cur != null && a < cur.length; a++) {
                if (cur[a] instanceof Subscription) {
                    Subscription ss = (Subscription) cur[a];
                    if (ss != null) {
                        Log.printDebug("ManageGroupUI, buildDataForList, ss.getNo() " + ss.getNo() + " " + ss.getId());
                        if (ss instanceof Package || ss instanceof Product) {
                            if (ss.getNo() != null) {
                                orignalHash.put(ss.getNo(), ss);
                            }
                        } else if (ss instanceof Channel) {
                            if (ss.getId() != null) {
                                orignalHash.put(ss.getId(), ss);
                            }
                        }

                    }
                }
            }
            Log.printDebug("ManageGroupUI, buildDataForList, check a update state");
            for (int a = 0; data != null && a < data.length; a++) {
                if (data[a] instanceof Subscription) {
                    String key = data[a] instanceof Package || data[a] instanceof Product ?
                            ((Subscription) data[a]).getNo() : ((Subscription) data[a]).getId();

                    Subscription cyoObj = (Subscription) orignalHash.get(key);

                    if (Log.DEBUG_ON) {
                        Log.printDebug("ManageGroupUI, buildDataForList, cyoObj=" + cyoObj);
                        Log.printDebug("ManageGroupUI, buildDataForList, subscription.key=" + key);
                    }

                    if (cyoObj == null)
                        continue;

                    cyoObj.setSequenceNo(((Subscription) data[a]).getSequenceNo());

                    if (addHash.containsKey(key) && !((Subscription) data[a]).getSubscribedInSession()) {
                        Log.printDebug("ManageGroupUI, buildDataForList, contains in addHash, key=" + key);
                        updateSubscribed(data[a]);

                        customAddVector.remove(cyoObj);
                        customRemoveVector.remove(cyoObj);
                    } else if (removeHash.containsKey(key) && ((Subscription) data[a]).getSubscribedInSession()) {
                        Log.printDebug("ManageGroupUI, buildDataForList, contains in removeHash, key=" + key);
                        updateSubscribed(data[a]);
                        customAddVector.remove(cyoObj);
                        customRemoveVector.remove(cyoObj);
                    }
                }
            }

            groupList.setCyoObject(data);
            groupList.setHasFocus(true);
            groupList.start(true);

        } else {
            for (int k = 0; k < data.length; k++) {

                if (data[k] instanceof Subscription) {
                    Subscription sub = (Subscription)data[k];
                    if (sub.getNo().equals("926")) {
                        Log.printDebug("ManageGroupUI: buildDataForList(): HD fee found");
                        sub.setSubscribedOrginalInSession(String.valueOf(sub.getSubscribedInSession()));
                    }
                }
            }

            Vector dataVector = new Vector();
            addCyoObject(dataVector, data);

            CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
            dataVector.copyInto(rearrangeData);

            groupList.setCyoObject(rearrangeData);
            groupList.setHasFocus(true);
            groupList.start(true);

            dataVector.clear();
        }

        currentList = groupList;

        if (isALaCarte && CyoController.getInstance().getRedirectionCode() != CyoController.REDIRECTION_CODE_NONE) {
            CyoController.getInstance().resetRedirectionCode();
            boolean found = false;
            String targetCallLetter = CyoController.getInstance().getTargetCallLetter();
            String sisterCallLetter = null;
            try {
                EpgService eService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
                if (eService != null) {
                    TvChannel channel = eService.getChannel(targetCallLetter);
                    if (channel != null && channel.getSisterChannel() != null) {
                        sisterCallLetter = channel.getSisterChannel().getCallLetter();
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("ManageGroupUI, buildDataForList, targetCallLetter=" + targetCallLetter);
                Log.printDebug("ManageGroupUI, buildDataForList, sisterCallLetter=" + sisterCallLetter);
            }

            for (int i = 0; i < groupList.getCyoObjectLength(); i++) {
                if (groupList.getCurrentCyoObject() instanceof Channel) {
                    Channel ch = (Channel) groupList.getCurrentCyoObject();
                    // VDTRMASTER-5880
                    Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(ch.getId());
                    if (chInfo.getCallLetters() != null
                            && (chInfo.getCallLetters().equals(targetCallLetter)
                            || chInfo.getCallLetters().equals(sisterCallLetter))) {
                        updateSubscribed(ch);
                        needExitPopup = true;
                        found = true;
                        break;
                    }
                } else if (groupList.getCurrentCyoObject() instanceof Package) {
                    Package p = (Package) groupList.getCurrentCyoObject();
                    if (p.getCyoObject() != null) {
                        for (int pIdx = 0; pIdx < p.getCyoObject().length; pIdx++) {
                            Channel ch = (Channel) p.getCyoObject()[pIdx];

                            // VDTRMASTER-5880
                            Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(ch.getId());
                            if (chInfo.getCallLetters() != null && (chInfo.getCallLetters().equals(targetCallLetter)
                                    || chInfo.getCallLetters().equals(sisterCallLetter))) {
                                updateSubscribed(p);
                                needExitPopup = true;
                                found = true;
                                break;
                            }
                        }

                        if (found) {
                            break;
                        }
                    }
                }
                groupList.handleKey(OCRcEvent.VK_DOWN);
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("ManageGroupUI, buildDataForList, found=" + found);
            }

            if (!found) {
                groupList.start(true);
            }
        }
    }

    private void addCyoObject(Vector vec, CyoObject[] data) {

        for (int i = 0; i < data.length; i++) {
            if(!(data[i] instanceof Group)) {
                Subscription ss = (Subscription)data[i];
                ss.setSubscribedOrginalInSession(String.valueOf(ss.getSubscribedInSession()));
            }
            vec.add(data[i]);

            if (!isBasicService) {
	            if (!(data[i] instanceof Package) && data[i].getCyoObject() != null) {
	                addCyoObject(vec, data[i].getCyoObject());
	            }
            }
        }
    }

    public void popupClosed(BaseUI pop, Object msg) {
        super.popupClosed(pop, msg);
        removePopup(pop);
        if(gotoHotKey) {
            gotoHotKey = false;
            super.popupClosed(pop, msg);
            return;
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ManageGroupUI: pop=" + pop);
            Log.printDebug("ManageGroupUI: pop state = " + pop.getState());

        }

        if (messagePopupVector != null && messagePopupVector.size() > 0) {
            Message message = (Message) messagePopupVector.remove(0);

            // Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
            notiPop.setExplain(message.getText());
            String[] proccessedMsg = notiPop.getExplain();

            if (message.getType().equals("3")) {
                // show CYO_13
                if (message.getId().equals("35")) {
                    // VDTRMASTER-5747
                    notiPop.stop();

                    vPop.setIcon(true);
                    setPopup(vPop);
                    vPop.show(this, message.getText());
                } else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
                    // VDTRMASTER-5747
                    notiPop.stop();

                    setPopup(infoPopup);
                    infoPopup.show(this, message);
                } else if (proccessedMsg == null) {
                    // VDTRMASTER-5747
                    notiPop.stop();

                    vPop.setIcon(false);
                    setPopup(vPop);
                    vPop.show(this, message.getText());
                } else {
                    setPopup(notiPop);
                    //notiPop.setExplain(message.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                }
            } else if (message.getType().equals("5")) {
                setPopup(notiPop);
                notiPop.setExplain(message.getText());
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                expired = true;
            } else if (message.getType().equals("1")) {
                // show CYO_14
                setPopup(notiPop);
                notiPop.setExplain(message.getText());
                notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
            } else {
                setPopup(notiPop);
                notiPop.setExplain(message.getText());
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
            }
            repaint();
            return;
        }

        if (expired) {
            cyoReStart();
        } else if (pop instanceof ValidationErrorPopupUI) {
            dispose();
            CyoController.getInstance().showUI(lastUIId, true);
            repaint();
        } else if (pop instanceof NotificationPopupUI) {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("ManageGroupUI: pop instanceof NotificastionPopupUI");
        		Log.printDebug("ManageGroupUI: pop state = " + pop.getState());

        	}
            if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
                Boolean result = (Boolean) msg;
                if (result.booleanValue()) {
                	dispose();
                    CyoController.getInstance().showUI(lastUIId, true);
                }
            } else if (pop.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
                Integer result = (Integer) msg;
                if (result.intValue() == 0) {
                    requestSaveAction();
                    // CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
                } else if (result.intValue() == 1) {
                    CyoController.getInstance().showUI(lastUIId, true);
                } else if (result.intValue() == 2) {

                }
            } else if (pop.getState() == NotificationPopupUI.STATE_NORMAL) {
                if (!channelInfoBoolean) {
                    if (saveMessageView) {
                        ManageTVServiceUI.subDataChanged = true;
                    }

                    if (lastRequestedURLType == URLType.VALIDATION && lastObject.getSuccess().equals("1")) {
                        if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
                            CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
                        } else {
                            CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
                        }
                    } else {
                        // Save.action
                        // VTBACKEND-945
                        if (lastObject.getSuccess().equals("1")) {
                            if (lastUIId != BaseUI.UI_INITIAL_STATE) {
                                CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
                            } else {
                                if (CyoDataManager.getInstance().needParamForRequiresUpsell()) {
                                    Hashtable param = new Hashtable();
                                    param.put("requiresUpsell", "false");
                                    CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                                    CyoDataManager.getInstance().request(URLType.VALIDATION, param);
                                } else {
                                    CyoDataManager.getInstance().request(URLType.VALIDATION, null);
                                }
                            }
                        }
                    }
                } else {
                    channelInfoBoolean = true;
                }
            }  else if (pop.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
                Boolean result = (Boolean) msg;
                if (result.booleanValue()) {
                    repaint();
                } else {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ManageGroupUI, popupClosed, lastRequestedURLType=" + lastRequestedURLType);
                        Log.printDebug("ManageGroupUI, popupClosed, lastUIId=" + lastUIId);
                    }
                    ManageTVServiceUI.subDataChanged = true;
                    if (lastRequestedURLType == URLType.SAVE_GROUP && lastUIId == BaseUI.UI_INITIAL_STATE) {
                        if (CyoDataManager.getInstance().needParamForRequiresUpsell()) {
                            Hashtable param = new Hashtable();
                            param.put("requiresUpsell", "false");
                            CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                            CyoDataManager.getInstance().request(URLType.VALIDATION, param);
                        } else {
                            CyoDataManager.getInstance().request(URLType.VALIDATION, null);
                        }
                    } else {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ManageGroupUI, popupClosed, lasttUIId=" + lastUIId);
                        }
                        if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
                            CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
                        } else {
                            CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
                        }
                    }
                }
            }
            repaint();
        } else if (pop.equals(validationUpSellPopupUI)) {
            Boolean result = (Boolean) msg;
            Log.printDebug("ManageGroupUI, popupClosed, validationUpSellPopupUI, result=" + result);
            if (result.booleanValue()) {
                Message message = validationUpSellPopupUI.getMessage();

                DataCenter.getInstance().put("UPSELL_MESSAGE", message);

                if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_PACKAGE) || message
                        .getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {

                    CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);

                } else if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_A_LA_CARTE)) {
                    // NOTHING;
                    Log.printDebug("ManageGroupUI, popupClosed, optimizationType is"
                            + " OPTIMIZATION_TYPE_A_LA_CARTE");
                }

            } else {
                CyoDataManager.getInstance().setNeedParamForRequiresUpsell(true);
                requestSaveAction();
            }
        }
    }

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.INFO_ON) {
            Log.printInfo("ManageGroupUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
                    + obj);
        }

        lastRequestedURLType = type;
        lastObject = obj;

        if (type == URLType.LOAD_GROUP) {
            if (obj.getSuccess().equals("1")) {
                if (obj instanceof Services) {
                    services = (Services) obj;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ManageGroupUI: receiveCyoData: cyoData length = "
                                + services.getCyoObject().length);
                    }
                    CyoObject[] groups = services.getCyoObject();
                    if (groups[0] instanceof Group) {
                        loadedGroup = (Group) groups[0];
                        updateInfomation();
                        renderer.prepare(this);
						setFooter();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ManageGroupUI: receiveCyoData: groups length = " + groups.length);
                            Log.printDebug("ManageGroupUI: receiveCyoData: groups[0].getCyoObject length = "
                                    + groups[0].getCyoObject().length);
                        }

                        buildDataForList(groups[0].getCyoObject());
                    } else {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ManageGroupUI: receiveCyoData: obj = " + obj);
                        }
                    }
                }
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    if ((msg.getType()).equals("5")) {
                        setPopup(notiPop);
                        notiPop.setExplain(msg.getText());
                        notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                        Log.printInfo("msg.getType() = " + msg.getType());
                        expired = true;
                    }
                }
            }
            repaint();
        } else if (type == URLType.SAVE_GROUP) {
            if (obj.getSuccess().equals("1")) {

                CyoObject[] subObjs = obj.getCyoObject();
                if (subObjs != null) {
                    StringBuffer sf = new StringBuffer();
                    Message message = (Message) subObjs[0];
                    Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());
                    if (message.getType().equals("0")) {//&& message.getId().equals("140")
                        sf.append(message.getText());
                        sf.append("\n");
                        if (!saveMessageView) {
                            saveMessageView = true;
                            setPopup(notiPop);
                            notiPop.setExplain(sf.toString());
                            notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                        }
                    } else if (message.getType().equals("1")) {
                        if (message.getOptimizationType() != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("message.optimizationType=" + message.getOptimizationType());
                                Log.printDebug("message.optimizationCode=" + message.getOptimizationCode());
                            }
                            CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                            setPopup(validationUpSellPopupUI);
                            validationUpSellPopupUI.show(this, message);
                        } else {
                            setPopup(notiPop);
                            notiPop.setExplain(message.getText());
                            notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
                        }
                    } else {
                        ManageTVServiceUI.subDataChanged = true;
                        Log.printDebug("receiveCyoData, lastUIId=" + lastUIId);
                        if (lastUIId != BaseUI.UI_INITIAL_STATE) {
                            CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
                        } else {

                            if (CyoDataManager.getInstance().needParamForRequiresUpsell()) {
                                Hashtable param = new Hashtable();
                                param.put("requiresUpsell", "false");
                                CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                                CyoDataManager.getInstance().request(URLType.VALIDATION, param);
                            } else {
                                CyoDataManager.getInstance().request(URLType.VALIDATION, null);
                            }
                        }
                    }
                } else {
                    ManageTVServiceUI.subDataChanged = true;
                    if (lastUIId != BaseUI.UI_INITIAL_STATE) {
                        CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
                    } else {
                        if (CyoDataManager.getInstance().needParamForRequiresUpsell()) {
                            Hashtable param = new Hashtable();
                            param.put("requiresUpsell", "false");
                            CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                            CyoDataManager.getInstance().request(URLType.VALIDATION, param);
                        } else {
                            CyoDataManager.getInstance().request(URLType.VALIDATION, null);
                        }
                    }
                }
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];

                    if (msg.getType().equals("1")) {
                        notiPop.setExplain(msg.getText());
                        notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
                    } else if (msg.getType().equals("3") && msg.getId().equals("62")) {
                    	setPopup(infoPopup);
						infoPopup.show(this, msg);
                    } else {
	                    setPopup(notiPop);
	                    notiPop.setExplain(msg.getText());
	                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

	                    if ((msg.getType()).equals("5")) {
	                        expired = true;
	                    }
                    }
                }
            }
        } else if (type == URLType.VALIDATION) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ManageGroupUI: receiveCyoData: VALIDATION success = " + obj.getSuccess());
            }
            if (obj instanceof Services) {
                Services services = (Services) obj;
                if (services.getSuccess().equals("1")) {
                    stop();
                    if (services.getMessage() != null) {
                        setPopup(notiPop);
                        notiPop.setExplain(services.getMessage()[0].getText());
                        notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
                    } else {
                        DataCenter.getInstance().put("VALIDATION", obj);
                        CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
                    }
                } else {
                    CyoObject[] subObjs = services.getCyoObject();
                    if (subObjs != null) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ManageGroupUI: receiveCyoData: subObjs = " + subObjs);
                        }
                        for (int i = 0; i < subObjs.length; i++) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug(
                                        "ManageGroupUI: receiveCyoData: subObjs[" + i + "] = " + subObjs[i]);
                            }

                            if (subObjs[i] instanceof Message) {
                                messagePopupVector.add(subObjs[i]);
                            }
                        }
                        if (messagePopupVector != null && messagePopupVector.size() > 0) {
                            Message message = (Message) messagePopupVector.remove(0);

                            if (Log.DEBUG_ON) {
                                Log.printDebug("ManageGroupUI: receiveCyoData: message = " + message);
                                Log.printDebug(
                                        "ManageGroupUI: receiveCyoData: message = " + message.getId());
                            }

                            notiPop.setExplain(message.getText());
                            String[] proccessedMsg = notiPop.getExplain();

                            if (message.getType().equals("3")) {
                                // show CYO_13
                                if (message.getId().equals("35")) {
                                    // VDTRMASTER-5747
                                    notiPop.stop();

                                    vPop.setIcon(true);
                                    setPopup(vPop);
                                    vPop.show(this, message.getText());
                                } else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
                                    // VDTRMASTER-5747
                                    notiPop.stop();

                                    setPopup(infoPopup);
                                    infoPopup.show(this, message);
                                    notiPop.setExplain(null);
                                } else if (proccessedMsg == null) {
                                    // VDTRMASTER-5747
                                    notiPop.stop();

                                    vPop.setIcon(false);
                                    setPopup(vPop);
                                    vPop.show(this, message.getText());
                                } else {
                                    setPopup(notiPop);
                                    //notiPop.setExplain(message.getText());
                                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                                }
                            } else if ((message.getType()).equals("5")) {
                                setPopup(notiPop);
                                notiPop.setExplain(message.getText());
                                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                                expired = true;
                            } else if (message.getId().equals("1")) {
                                setPopup(notiPop);
                                notiPop.setExplain(services.getMessage()[0].getText());
                                notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
                            } else {
                                // show CYO_14
                                setPopup(notiPop);
                                notiPop.setExplain(message.getText());
                                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                            }
                        }
                        repaint();
                    } else {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ManageGroupUI: receiveCyoData: subObjs is null");
                        }
                    }
                }
            }
        }
    }

    public void receiveCyoError(int type) {
    }

    public ListUI getGroupList() {
        return groupList;
    }

    public boolean getChanged() {
        if (customAddVector.size() > 0 || customRemoveVector.size() > 0) {

            for (int a = 0; a < customAddVector.size(); a++) {
                CyoObject obj = (CyoObject) customAddVector.elementAt(a);
                if (obj instanceof Subscription) {
                    Subscription sobj = (Subscription) obj;
                    Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
                    Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
                    if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
                        return true;
                    }
                } else if (obj instanceof Package) {
                    Package sobj = (Package) obj;
                    Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
                    Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
                    if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
                        return true;
                    }
                }
            }

            for (int a = 0; a < customRemoveVector.size(); a++) {
                CyoObject obj = (CyoObject) customRemoveVector.elementAt(a);
                if (obj instanceof Subscription) {
                    Subscription sobj = (Subscription) obj;
                    Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
                    Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
                    if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
                        return true;
                    }
                } else if (obj instanceof Package) {
                    Package sobj = (Package) obj;
                    Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
                    Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
                    if (sobj.getSubscribedInAccount() != sobj.getSubscribedInSession()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}

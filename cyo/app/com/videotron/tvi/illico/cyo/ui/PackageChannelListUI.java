/**
 * @(#)PackageChannelListUI.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.gui.PackageChannelListRenderer;
import com.videotron.tvi.illico.cyo.popup.MenuItem;
import com.videotron.tvi.illico.cyo.popup.MenuListener;
import com.videotron.tvi.illico.cyo.popup.OptionScreen;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import org.ocap.ui.event.OCRcEvent;

import java.rmi.RemoteException;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

/**
 * This class display CYO_005.
 *
 * @author Woojung Kim
 * @version 1.1
 */
public class PackageChannelListUI extends BaseUI implements CyoDataListener, MenuListener {
	public static final int STATE_LIST = 0x0;
	public static final int STATE_BUTTON = 0x1;

	public static final int STEP_1 = 0x1;
	public static final int STEP_N = 0x2;

	private PackageChannelListRenderer renderer = new PackageChannelListRenderer();
	private ClickingEffect clickEffect;

	private ListUI[] listUI;
	private Catalog[] loadedCatalog;

	private Vector addVector;
	private Vector removeVector;

	private Vector messagePopupVector = new Vector();

	private boolean isModified;

	private int customIdx = -1;
	//private int specialIdx = -1;

	private int btnFocus = 0;
	private Package selectedPacakge;

	private ListUI currentList;

	private String sort;

	public OptionScreen optScr;
	public static MenuItem sortOption = new MenuItem("menu.sort");
	public static MenuItem nSORT = new MenuItem("menu.sorted.number");
	public static MenuItem aSORT = new MenuItem("menu.sorted.alphabetical");
	public boolean buttonClick = false;

	private boolean channelInfoBoolean = false;

	private boolean backButton = false;

	private boolean isRequestedValidate = false;

	private boolean comeFromBack;
	// 서버에 저장 했는지 확인
	private Vector savedParam = new Vector();
	private Vector removedParam = new Vector();

	private int lastRequestedURLType = URLType.LOAD_GROUP;
	private CyoObject lastObject;

	private int targetStep = 1;
	private int currentStep = STEP_1;
	private Services services;
	private int[] numberOfChannelArr;
	private Package blocPackage;
	private Package lastBlocPackage;
	private CyoObject lastSubscribedObj;

	public PackageChannelListUI() {
		setRenderer(renderer);
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);

		Log.printInfo("PackageChannelListUI, start = " + back);
		comeFromBack = back;
		if (!back) {
			isRequestedValidate = false;
			clearAddAndRemoveVectors();

			prepare();
			customIdx = -1;
			//specialIdx = -1;
			sort = Param.SORT_ALPHABETICAL;
			currentList = null;
			blocPackage = null;
			lastBlocPackage = null;

			lastSubscribedObj = null;

			if (CyoController.getInstance().getCurrentId() != UI_TV_PACKAGE_LIST) {
				CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_OFFER, null);
			} else {
				startAfterLoadPackageOffer();
			}

		} else {

			if (currentList != null) {
				currentList.prepare();
			}

			state = STATE_LIST;
			focus = 0;
			currentList = listUI[currentStep - 1];
			currentList.setHasFocus(true);
			setFooter();
		}
		if (messagePopupVector == null) {
			messagePopupVector = new Vector();
		} else {
			messagePopupVector.clear();
		}
		backButton = back;
		channelInfoBoolean = false;
		buttonClick = false;
		sortOption.clear();
		sortOption.add(aSORT);
		sortOption.add(nSORT);

		super.start(back);

		repaint();
	}

	public void startAfterLoadPackageOffer() {
		Object validatioObj = DataCenter.getInstance().get("UPSELL_MESSAGE");

		if (validatioObj != null) {
			// come from ValidataionUpSellPopup
			Message message = (Message) validatioObj;
			DataCenter.getInstance().remove("UPSELL_MESSAGE");

			if (Log.DEBUG_ON) {
				Log.printDebug(
						"PackageChannelListUI, start, message.optimizationType=" + message.getOptimizationType());
				Log.printDebug(
						"PackageChannelListUI, start, message.optimizationCode=" + message.getOptimizationCode());
			}

			if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_PACKAGE)) {
				// find package and use it
				selectedPacakge = CyoDataManager.getInstance().getPackageInPackageList(message.getOptimizationCode());
				CyoDataManager.getInstance().setSelectedPackage(selectedPacakge);

				// check step count
				targetStep = selectedPacakge.getPackageCatalogs().length;
				numberOfChannelArr = new int[targetStep];
				listUI = new ListUI[targetStep];
				loadedCatalog = new Catalog[targetStep];

				currentStep = STEP_1;

				comparePackages();

				requestLoadCatalogForCurrentStep();

			} else if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {
				// VDTRMASTER-5905
				blocPackage = CyoDataManager.getInstance()
						.getBlocPackageInCurrentPackage(message.getOptimizationCode());
				lastBlocPackage = CyoDataManager.getInstance().getCurrentBlocPackageInCurrentPackage();

				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI, start, blocPackage=" + blocPackage);
					Log.printDebug("PackageChannelListUI, start, lastBlocPackage=" + lastBlocPackage);
				}

				selectedPacakge = CyoDataManager.getInstance().getCurrentPackageInPackageList();
				CyoDataManager.getInstance().setSelectedPackage(selectedPacakge);

				PackageCatalog pCatalog = selectedPacakge.getNonPremiumPacakgeCatalog();

				targetStep = selectedPacakge.getPackageCatalogs().length;

				numberOfChannelArr = new int[targetStep];
				listUI = new ListUI[targetStep];
				loadedCatalog = new Catalog[targetStep];

				currentStep = targetStep;

				String catalogId = pCatalog.getCatalog().getCatalogId();
				Hashtable param = new Hashtable();
				param.put("catalogId", catalogId);

				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI, start, catalogId=" + catalogId);
				}

				CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_CATALOG, param);
			}

		} else {

			Object blocPackageObj = DataCenter.getInstance().get("BLOCK_PACKAGE");
			Object lastBlocPackageObj = DataCenter.getInstance().get("BLOCK_LAST_PACKAGE");

			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI, start, blocPackageObj=" + blocPackageObj);
				Log.printDebug("PackageChannelListUI, start, lastBlocPackageObj=" + lastBlocPackageObj);
			}

			if (blocPackageObj != null) {
				blocPackage = (Package) blocPackageObj;
				DataCenter.getInstance().remove("BLOCK_PACKAGE");

				if (lastBlocPackageObj != null) {
					lastBlocPackage = (Package) lastBlocPackageObj;
					DataCenter.getInstance().remove("BLOCK_LAST_PACKAGE");
				}

				selectedPacakge = CyoDataManager.getInstance().getCurrentPackageInPackageList();
				CyoDataManager.getInstance().setSelectedPackage(selectedPacakge);

				PackageCatalog pCatalog = selectedPacakge.getNonPremiumPacakgeCatalog();

				targetStep = selectedPacakge.getPackageCatalogs().length;

				numberOfChannelArr = new int[targetStep];
				listUI = new ListUI[targetStep];
				loadedCatalog = new Catalog[targetStep];

				currentStep = targetStep;

				String catalogId = pCatalog.getCatalog().getCatalogId();
				Hashtable param = new Hashtable();
				param.put("catalogId", catalogId);

				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI, start, catalogId=" + catalogId);
				}

				CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_CATALOG, param);
			} else {
				// come from TV Package list.
				selectedPacakge = (Package) DataCenter.getInstance().get("TV_PACKAGE");
				CyoDataManager.getInstance().setSelectedPackage(selectedPacakge);

				// check step count
				targetStep = selectedPacakge.getPackageCatalogs().length;
				numberOfChannelArr = new int[targetStep];
				listUI = new ListUI[targetStep];
				loadedCatalog = new Catalog[targetStep];

				currentStep = STEP_1;

				comparePackages();

				requestLoadCatalogForCurrentStep();
			}
		}
	}

	public void comparePackages() {
		// compare current package and selected package
		// if need, add or remove each for parameter of Save.action

		Package currentPackage = CyoDataManager.getInstance().getCurrentPackageInPackageList();

		if (Log.DEBUG_ON) {
			Log.printDebug("PackageChannelListUI, comparePackages, currentPackage=" + currentPackage);
		}

		if (currentPackage == null) {
			addCyoObjectInMyPicks(selectedPacakge);
		} else {

			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI, comparePackages, currentPackage.no=" + currentPackage.getNo());
				Log.printDebug("PackageChannelListUI, comparePackages, selectedPacakge.no=" + selectedPacakge.getNo());
			}

			if (!selectedPacakge.getNo().equals(currentPackage.getNo())) {
				addCyoObjectInMyPicks(selectedPacakge);
				removeCyoObjectInMyPicks(currentPackage);

				// check block channel
				Package previousBlocPackage = CyoDataManager.getInstance().getCurrentBlocPackageInCurrentPackage();
				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI, comparePackages, previousBlocPackage=" + previousBlocPackage);
				}

				// VDTRMASTER-5981 & VDTRMASTER-6007
				if (previousBlocPackage != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug(
								"PackageChannelListUI, comparePackages, previousBlocPackage.no=" + previousBlocPackage
										.getNo());
						Log.printDebug(
								"PackageChannelListUI, comparePackages, previousBlocPackage.desc=" + previousBlocPackage
										.getTitle());
					}
					BlockList bList = selectedPacakge.getBlockList();
					CyoObject[] pObjs = bList.getCyoObject();

					for (int i = 0; i < pObjs.length; i++) {
						if (((Package) pObjs[i]).getNo().equals(previousBlocPackage.getNo())) {
							blocPackage = (Package) pObjs[i];
							if (Log.DEBUG_ON) {
								Log.printDebug(
										"PackageChannelListUI, comparePackages, find blocPackage.no=" + blocPackage.getNo());
								Log.printDebug(
										"PackageChannelListUI, comparePackages, find blocPackage.title=" + blocPackage
												.getTitle());

							}
							break;
						} else if (((Package) pObjs[i]).getNumberOfChannels()
								.equals(previousBlocPackage.getNumberOfChannels())) {
							blocPackage = (Package) pObjs[i];
							lastBlocPackage = previousBlocPackage;
							if (Log.DEBUG_ON) {
								Log.printDebug(
										"PackageChannelListUI, comparePackages, with numberOfChannel, find blocPackage.no="
												+ blocPackage.getNo());
								Log.printDebug(
										"PackageChannelListUI, comparePackages, with numberOfChannel, find blocPackage.title="
												+ blocPackage.getTitle());
								Log.printDebug(
										"PackageChannelListUI, comparePackages, with numberOfChannel, find lastBlocPackage.no="
												+ lastBlocPackage.getNo());
								Log.printDebug(
										"PackageChannelListUI, comparePackages, with numberOfChannel, find lastBlocPackage.title="
												+ lastBlocPackage.getTitle());

							}
							break;
						}
					}
				}
			}
		}
	}

	public void prepare() {
		state = STATE_LIST;
		btnFocus = 0;
		focus = 0;
		state = STATE_LIST;
		setFooter();

		super.prepare();
	}

	public void stop() {
		if (optScr != null) {
			optScr.stop();
			optScr.dispose();
		}
		if (messagePopupVector != null) {
			messagePopupVector.clear();
			messagePopupVector = null;
		}
		optScr = null;

		// if(!gotoHelp) {
		// customAddVector.clear();
		// specialAddVector.clear();
		// movieAddVector.clear();
		// hdAddVector.clear();
		//
		// customRemoveVector.clear();
		// specialRemoveVector.clear();
		// movieRemoveVector.clear();
		// hdRemoveVector.clear();
		// }

		clearPopup();
		super.stop();
	}

	public void dispose() {
		clickEffect = null;
		if (messagePopupVector != null) {
			messagePopupVector.clear();
			messagePopupVector = null;
		}
		clearAddAndRemoveVectors();

		if (listUI != null) {
			for (int i = 0; i < listUI.length; i++) {
				if (listUI[i] != null) {
					listUI[i].stop();
					listUI[i] = null;
				}
			}
		}
		super.dispose();
	}

	public boolean handleKey(int code) {
		if (getPopup() != null && getPopup().isVisible()) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case OCRcEvent.VK_PAGE_UP:
			pageUp();
			return true;
		case OCRcEvent.VK_PAGE_DOWN:
			pageDown();
			return true;
		case OCRcEvent.VK_ENTER:
			keyEnter();
			return true;
		case KeyCodes.LAST:
			footer.clickAnimation(0);
			if (backButton) {
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			} else if (getChanged()) {
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			} else {
				if (lastUIId != UI_TV_PACKAGE_LIST && lastUIId != UI_INITIAL_STATE) {
					CyoController.getInstance().showUI(UI_MANAGE_TV_SERVICE, true);
				} else {
					CyoController.getInstance().showUI(lastUIId, true);
				}
			}
			repaint();
			return true;
		case KeyCodes.COLOR_A:
			if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {

				footer.clickAnimation(2);

				if (optScr == null) {
					optScr = new OptionScreen();
				}

				if (sort == Param.SORT_NUMBER) {
					nSORT.setChecked(true);
					aSORT.setChecked(false);
				} else {
					nSORT.setChecked(false);
					aSORT.setChecked(true);
				}

				optScr.start(sortOption, this);
			}

			return true;
		case KeyCodes.COLOR_B:
			if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {
				footer.clickAnimation(3);

				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL_CANCEL,
						BaseUI.getMenuText(KeyNames.UNSELECT_CHANNEL_TITLE),
						BaseUI.getExplainText(KeyNames.UNSELECT_CHANNEL));
				repaint();
			}
			return true;
		default:
			return false;
		}
	}

	private void keyLeft() {
		if (state == STATE_BUTTON) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
			btnFocus = 0;
			repaint();
		}
	}

	private void keyRight() {
		if (state == STATE_LIST) {
			state = STATE_BUTTON;
			currentList.setHasFocus(false);
			if (!isSatisfying()) {
				btnFocus = 1;
			} else {
				btnFocus = 0;
			}

			repaint();
		}
	}

	private void keyUp() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_UP);
			repaint();
		} else if (state == STATE_BUTTON) {
			--btnFocus;
			if (!isSatisfying() && btnFocus < 1) {
				btnFocus = 1;
			} else if (btnFocus < 0) {
				btnFocus = 0;
			}

			repaint();
		}
	}

	private void keyDown() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_DOWN);
			repaint();
		} else if (state == STATE_BUTTON) {
			if (currentStep == STEP_1) {
				if (++btnFocus > 2) {
					btnFocus = 2;
				}
			} else {
				if (++btnFocus > 3) {
					btnFocus = 3;
				}
			}
			repaint();
		}
	}

	private void pageUp() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_UP);
			repaint();
		}
	}

	private void pageDown() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
			repaint();
		}
	}

	private void keyEnter() {
		if (state == STATE_LIST) {
			CyoObject curObj = currentList.getCurrentCyoObject();
			if (curObj instanceof Package && currentList.isFocusOnViewChanenlIncluded()) {
				// show included channels popup
				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI, keyEnter, show included channels popup");
				}

				setPopup(includedChannelsPopup);
				includedChannelsPopup.show(this, (Package) curObj);
			} else {
				boolean isSubscribed = updateSubscribed(curObj);

				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI, keyEnter, isSubscribed=" + isSubscribed);
				}

				// VDTRMASTER-5884
				if (loadedCatalog[currentStep - 1].isPremium()) {
					int numberOfChannel = getNumberOfChannels();
					int selectedChannel = Integer.valueOf(getCurrentList().getSelectedChannelForPremium()).intValue();

					if (Log.DEBUG_ON) {
						Log.printDebug("PackageChannelListUI, keyEnter, numberOfChannel=" + numberOfChannel);
						Log.printDebug("PackageChannelListUI, keyEnter, selectedChannel=" + selectedChannel);
						Log.printDebug("PackageChannelListUI, keyEnter, isPremium=" + getCurrentCatalog().isPremium());
						Log.printDebug("PackageChannelListUI, keyEnter, lastSubscribedObj=" + lastSubscribedObj);
						Log.printDebug("PackageChannelListUI, keyEnter, lastSubscribedObj.subscribedInSession=" + ((Subscription) lastSubscribedObj).getSubscribedInSession());
						Log.printDebug("PackageChannelListUI, keyEnter, lastSubscribedObj.modifiable=" + ((Subscription) lastSubscribedObj).getModifiable());
						Log.printDebug("PackageChannelListUI, keyEnter, lastSubscribedObj.subscriptionType=" + ((Subscription) lastSubscribedObj).isSubscriptionTypeOfALaCarte());
					}

					if (isSubscribed && selectedChannel > numberOfChannel && curObj!= lastSubscribedObj &&
							lastSubscribedObj != null && ((Subscription) lastSubscribedObj).getSubscribedInSession()
							&& !((Subscription) lastSubscribedObj).getModifiable().equals("2")
							&& !((Subscription) lastSubscribedObj).isSubscriptionTypeOfALaCarte()) {
						updateSubscribed(lastSubscribedObj);
					}

					if (isSubscribed) {
						lastSubscribedObj = curObj;
					}
				}
				needExitPopup = true;
			}
		} else if (state == STATE_BUTTON) {

			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}
			if (currentStep == STEP_1) {
				clickEffect.start(719, 357 + btnFocus * 37, 172, 30);
			} else {
				clickEffect.start(719, 320 + btnFocus * 37, 172, 30);
			}

			if (targetStep == 1) {
				if (btnFocus == 0) {
					buttonClick = true;
					isRequestedValidate = true;
					// VDTRMASTER-5958
					CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
					setData();
				} else if (btnFocus == 1) { // Cancel
					if (backButton) {
						setPopup(notiPop);
						notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
					} else if (checkAddVector()) {
						setPopup(notiPop);
						notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
					} else {
						if (isRequestedValidate) {
							String titleStr = BaseUI.getMenuText(KeyNames.CANCEL_CHANGE_TITLE);
							String explainStr = BaseUI.getExplainText(KeyNames.CANCEL_CHANGE);
							setPopup(notiPop);
							notiPop.show(this, NotificationPopupUI.STATE_CANCEL_CHANGE, titleStr, explainStr);
						} else {
							// VDTRMASTER-5969
							if (lastUIId != UI_TV_PACKAGE_LIST && lastUIId != UI_INITIAL_STATE) {
								CyoController.getInstance().showUI(UI_MANAGE_TV_SERVICE, true);
							} else {
								CyoController.getInstance().showUI(lastUIId, true);
							}
						}
					}
				} else if (btnFocus == 2) { // Help
					DataCenter.getInstance().put("MODE", "HELP");
					CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
				}
			} else {
				if (currentStep == STEP_1) {
					if (btnFocus == 0) {
						// next_step
						moveToNextStep();
						repaint();
					} else if (btnFocus == 1) { // Cancel
						if (backButton) {
							setPopup(notiPop);
							notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
						} else if (checkAddVector()) {
							setPopup(notiPop);
							notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
						} else {
							if (isRequestedValidate) {
								String titleStr = BaseUI.getMenuText(KeyNames.CANCEL_CHANGE_TITLE);
								String explainStr = BaseUI.getExplainText(KeyNames.CANCEL_CHANGE);
								setPopup(notiPop);
								notiPop.show(this, NotificationPopupUI.STATE_CANCEL_CHANGE, titleStr, explainStr);
							} else {
								// VDTRMASTER-5969
								if (lastUIId != UI_TV_PACKAGE_LIST && lastUIId != UI_INITIAL_STATE) {
									CyoController.getInstance().showUI(UI_MANAGE_TV_SERVICE, true);
								} else {
									CyoController.getInstance().showUI(lastUIId, true);
								}
							}
						}
					} else if (btnFocus == 2) { // Help
						DataCenter.getInstance().put("MODE", "HELP");
						CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
					}
				} else if (currentStep == targetStep) {
					// last step

					if (btnFocus == 0) {
						// validate
						buttonClick = true;
						isRequestedValidate = true;
						setData();
					} else if (btnFocus == 1) {
						// previous
						moveToPrevious();
					} else if (btnFocus == 2) {
						if (backButton) {
							setPopup(notiPop);
							notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
						} else if (checkAddVector()) {
							setPopup(notiPop);
							notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
						} else {
							if (isRequestedValidate) {
								String titleStr = BaseUI.getMenuText(KeyNames.CANCEL_CHANGE_TITLE);
								String explainStr = BaseUI.getExplainText(KeyNames.CANCEL_CHANGE);
								setPopup(notiPop);
								notiPop.show(this, NotificationPopupUI.STATE_CANCEL_CHANGE, titleStr, explainStr);
							} else {
								// VDTRMASTER-5969
								if (lastUIId != UI_TV_PACKAGE_LIST && lastUIId != UI_INITIAL_STATE) {
									CyoController.getInstance().showUI(UI_MANAGE_TV_SERVICE, true);
								} else {
									CyoController.getInstance().showUI(lastUIId, true);
								}
							}
						}
					} else if (btnFocus == 3) { // Help
						DataCenter.getInstance().put("MODE", "HELP");
						CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
					}
				} else {
					// middle step - previous & next button
					if (btnFocus == 0) {
						// NEXT
						moveToNextStep();
					} else if (btnFocus == 1) {
						// previous
						moveToPrevious();
					} else if (btnFocus == 2) {
						if (backButton) {
							setPopup(notiPop);
							notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
						} else if (checkAddVector()) {
							setPopup(notiPop);
							notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
						} else {
							if (isRequestedValidate) {
								String titleStr = BaseUI.getMenuText(KeyNames.CANCEL_CHANGE_TITLE);
								String explainStr = BaseUI.getExplainText(KeyNames.CANCEL_CHANGE);
								setPopup(notiPop);
								notiPop.show(this, NotificationPopupUI.STATE_CANCEL_CHANGE, titleStr, explainStr);
							} else {
								// VDTRMASTER-5969
								if (lastUIId != UI_TV_PACKAGE_LIST && lastUIId != UI_INITIAL_STATE) {
									CyoController.getInstance().showUI(UI_MANAGE_TV_SERVICE, true);
								} else {
									CyoController.getInstance().showUI(lastUIId, true);
								}
							}
						}
					} else if (btnFocus == 3) { // Help
						DataCenter.getInstance().put("MODE", "HELP");
						CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
					}
				}
			}
		}
		repaint();
	}

	private boolean checkAddVector() {

		if (addVector != null) {
			if (addVector != null && addVector.size() > 0) {
				return true;
			}
		}

		return false;
	}

	private boolean checkRemoveVector() {
		if (removeVector != null) {
			if (removeVector != null && removeVector.size() > 0) {
				return true;
			}
		}
		return false;
	}

	public void setData() {
		// Confirm
		dataParam.clear();
		addNumber = null;
		addNumber = new StringBuffer();
		removeNumber = null;
		removeNumber = new StringBuffer();

		if (addVector != null) {
			if (addVector.size() > 0 || removeVector.size() > 0) {
				requestSaveAction();
			}
		}

		sendServerSaveAction();
	}

	public void resetData() {
		if (Log.INFO_ON) {
			Log.printInfo("PackageChannelListUI: resetData()");
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("PackageChannelListUI: resetData(): dataParam size = " + dataParam.size());
		}

		if (dataParam.size() > 0) {
			Hashtable resetParam = new Hashtable();

			String addParams = (String) dataParam.get("add");

			if (addParams != null) {
				resetParam.put("remove", addParams);
			}

			String removeParams = (String) dataParam.get("remove");

			if (removeParams != null) {
				resetParam.put("add", removeParams);
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI: resetData(): resetParam " + resetParam);
			}

			CyoDataManager.getInstance().request(URLType.SAVE_SELF_SERVE, resetParam);
		}
	}

	private void clearAddAndRemoveVectors() {
		if (Log.INFO_ON) {
			Log.printInfo("PackageChannelListUI: clearAddAndRemoveVectors()");
		}

		if (addVector != null) {
			addVector.clear();
		}

		if (removeVector != null) {
			removeVector.clear();
		}
	}

	private void setFooter() {
		footer.reset();
		if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK),
					BaseUI.getMenuText(KeyNames.BTN_BACK));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE),
					BaseUI.getMenuText(KeyNames.BTN_SCROLL));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A),
					BaseUI.getMenuText(KeyNames.BTN_SORTING));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_B),
					BaseUI.getMenuText(KeyNames.BTN_DESELECT_ALL));
		} else {
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK),
					BaseUI.getMenuText(KeyNames.BTN_BACK));
		}
	}

	private boolean updateSubscribed(CyoObject obj) {
		if (Log.DEBUG_ON) {
			if (obj instanceof Subscription) {
				Log.printDebug("PackageChannelListUI: obj no = " + ((Subscription) obj).getNo());
			}
		}

		boolean isSubscribed = false;

		if (obj instanceof Package) {
			Package curPackage = ((Package) obj);
			String modiable = curPackage.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI: Package modiable = " + modiable);
			}
			if (modiable.equals("0")) {
				// Package
				curPackage.setSubscribedInSession(!curPackage.getSubscribedInSession());
				CyoObject[] childObjs = curPackage.getCyoObject();

				for (int i = 0; childObjs != null && i < childObjs.length; i++) {
					Subscription child = (Subscription) childObjs[i];
					if (child.getModifiable().equals("1")) {
						child.setSubscribedInSession(curPackage.getSubscribedInSession());
					}
				}

				if (curPackage.getSubscribedInSession()) {
					addCyoObjectInMyPicks(obj);
					isSubscribed = true;
				} else {
					removeCyoObjectInMyPicks(obj);
				}

				if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {
					isModified = true;
				}
			} else if (modiable.equals("1")) {
				Log.printInfo("UNSELECT_MESSAGE_1");
				channelInfoBoolean = true;
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else if (modiable.equals("2")) {
				if (curPackage.isSubscriptionTypeOfALaCarte()) {
					Log.printDebug("UNSELECT_MESSAGE_FOR_A_LA_CARTE");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_FOR_A_LA_CARTE));
					notiPop.show(this, NotificationPopupUI.STATE_UNSELECT_MESSAGE_FOR_A_LA_CARTE);
				} else {
					Log.printInfo("UNSELECT_MESSAGE_2");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
			} else if (modiable.equals("3")) {
				Log.printInfo("UNSELECT_MESSAGE_3");
				channelInfoBoolean = true;
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			}

		} else if (obj instanceof Subscription) {
			Subscription subObj = ((Subscription) obj);
			String modiable = subObj.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI: Channel modiable = " + modiable);
			}
			Log.printDebug("PackageChannelListUI: subObj = " + subObj);
			CyoObject parentObj = subObj.getParentObj();
			Log.printDebug("PackageChannelListUI: parentObj = " + parentObj);
			if (parentObj instanceof Package) {
				Package parentPackage = (Package) parentObj;
				String pModiable = parentPackage.getModifiable();
				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI: Parent Package modiable = " + pModiable);
				}
				if (pModiable.equals("0") && modiable.equals("0")) {
					/*
					 * Package = 0; Channel = 0: The user can add / remove a
					 * package, as well as channels in it. In this case, the
					 * user can do anything (add/remove a package, add/remove a
					 * channel from within the package with no consequences)
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(obj);
						isSubscribed = true;
					} else {
						removeCyoObjectInMyPicks(obj);
					}
					if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {
						isModified = true;
					}
				} else if (pModiable.equals("0") && !modiable.equals("0")) {
					/*
					 * Package = 0; Channel 1: The user can remove/add the
					 * package but not individual channels. If the user unchecks
					 * a channel, the whole package is automatically unchecked
					 * with all other channels in it.
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());
					parentPackage.setSubscribedInSession(subObj.getSubscribedInSession());
					CyoObject[] childObjs = parentPackage.getCyoObject();

					for (int i = 0; childObjs != null && i < childObjs.length; i++) {
						Subscription child = (Subscription) childObjs[i];
						if (child.getModifiable().equals("1")) {
							child.setSubscribedInSession(parentPackage.getSubscribedInSession());
						}
					}

					if (parentPackage.getSubscribedInSession()) {
						addCyoObjectInMyPicks(parentPackage);
						isSubscribed = true;
					} else {
						removeCyoObjectInMyPicks(parentPackage);
					}

					if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {
						isModified = true;
					}
				} else if (!pModiable.equals("0") && modiable.equals("0")) {
					/*
					 * Package = 1; Channel 0: The user cannot add/remove the
					 * package, but he can change individual channels in it. If
					 * the user checks/unckecks a channel, it has an effect only
					 * on that channel.
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(subObj);
						isSubscribed = true;
					} else {
						removeCyoObjectInMyPicks(subObj);
					}
					if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {
						isModified = true;
					}
				} else if (modiable.equals("1")) {
					Log.printInfo("UNSELECT_MESSAGE_1");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("2")) {
					Log.printInfo("UNSELECT_MESSAGE_2");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("3")) {
					Log.printInfo("UNSELECT_MESSAGE_3");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
				//			} else if (!(subObj instanceof Package || subObj instanceof Channel) && !subObj.getNo().equals("926")) {
				////				if (subObj.getNo().equals("926")) {
				////					Log.printInfo("This is HD Network Fees");
				////					setPopup(notiPop);
				////					notiPop.setExplain(BaseUI.getExplainText(KeyNames.HD_NETWORK_FEES));
				////					notiPop.show(this, NotificationPopupUI.STATE_HD_NETWORK_FEES);
				////				} else {
				////					Log.printInfo("UNSELECT_MESSAGE_4");
				////					channelInfoBoolean = true;
				////					setPopup(notiPop);
				////					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_4));
				////					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				////				}
				//
				//				Log.printInfo("UNSELECT_MESSAGE_4");
				//				channelInfoBoolean = true;
				//				setPopup(notiPop);
				//				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_4));
				//				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else {
				Log.printDebug("PackageChannelListUI: subObj no = " + subObj.getNo());
				Log.printDebug(
						"PackageChannelListUI: subObj.getSubscribedInAccount = " + subObj.getSubscribedInAccount());
				Log.printDebug(
						"PackageChannelListUI: subObj.getSubscribedInSession = " + subObj.getSubscribedInSession());
				Log.printDebug("PackageChannelListUI: subObj.getSubscribedOrginalInSession = " + subObj
						.getSubscribedOrginalInSession());

				if (subObj.getNo().equals("926") && !modiable.equals("0") && subObj.getSubscribedInAccount()) {
					Log.printInfo("This is HD Network Fees");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.HD_NETWORK_FEES));
					notiPop.show(this, NotificationPopupUI.STATE_HD_NETWORK_FEES);
				} else if (modiable.equals("0")) {

					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(subObj);
						isSubscribed = true;
					} else {
						removeCyoObjectInMyPicks(subObj);
					}
					if (loadedCatalog != null && loadedCatalog[currentStep - 1] != null) {
						isModified = true;
					}
				} else if (subObj.isSubscriptionTypeOfALaCarte()) {
					Log.printDebug("UNSELECT_MESSAGE_FOR_A_LA_CARTE");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_FOR_A_LA_CARTE));
					notiPop.show(this, NotificationPopupUI.STATE_UNSELECT_MESSAGE_FOR_A_LA_CARTE);
				} else if (modiable.equals("1")) {
					Log.printInfo("UNSELECT_MESSAGE_1");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("2")) {
					Log.printInfo("UNSELECT_MESSAGE_2");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("3")) {
					Log.printInfo("UNSELECT_MESSAGE_3");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}

			}
		}
		renderer.update(this);

		return isSubscribed;
	}

	public int getButtonFocus() {
		return btnFocus;
	}

	public ListUI getCurrentList() {
		return currentList;
	}

	Hashtable dataParam = new Hashtable();
	StringBuffer addNumber = new StringBuffer();
	StringBuffer removeNumber = new StringBuffer();

	private void requestSaveAction() {
		// private void requestSaveAction(int focus) {
		if (Log.INFO_ON) {
			Log.printInfo("requestSaveAction()");
		}

		if (blocPackage != null) {
			if (Log.INFO_ON) {
				Log.printInfo("requestSaveAction(), blocPackage add, blocPacakge.no=" + blocPackage.getNo());
			}
			// VDTRMASTER-5946
			if (!blocPackage.getNo().equals("0")) {
				addVector.add(blocPackage);
			}
		}

		if (lastBlocPackage != null) {
			if (Log.INFO_ON) {
				Log.printInfo(
						"requestSaveAction(), lastBlocPackage remove, lastBlocPackage.no=" + lastBlocPackage.getNo());
			}
			// VDTRMASTER-5946
			if (!lastBlocPackage.getNo().equals("0")) {

				removeVector.add(lastBlocPackage);
			}
		}

		String addParams = null, removeParams = null;

		addParams = getNumbers(addVector, addNumber);
		removeParams = getNumbers(removeVector, removeNumber);

		if (addParams != null) {
			dataParam.put("add", addParams);
		}
		if (removeParams != null) {
			dataParam.put("remove", removeParams);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("addParams = " + addParams);
			Log.printDebug("removeParams = " + removeParams);
		}
	}

	public void sendServerSaveAction() {
		if (dataParam.size() > 0) {
			CyoDataManager.getInstance().request(URLType.SAVE_SELF_SERVE, dataParam);
		} else if (savedParam.size() == 0 && removedParam.size() == 0) {
			if (lastUIId == UI_INITIAL_STATE) {
				CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
			} else {
				if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				} else {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
				}
			}
		}
	}

	private String getNumbers(Vector vec, StringBuffer sb) {
		if (vec.size() == 0) {
			return null;
		}

		if (sb.length() > 0) {
			sb.append(",");
		}

		for (int i = 0; i < vec.size(); i++) {
			if (i > 0) {
				sb.append(",");
			}
			Subscription obj = (Subscription) vec.get(i);
			sb.append(obj.getNo());
		}
		return sb.toString();
	}

	private void buildDataForList(CyoObject[] groups) {

		//		clearAddAndRemoveVectors();

		loadedCatalog[currentStep - 1] = (Catalog) groups[0];

        // VDTRMASTER-6133
        if (currentStep == STEP_1) {
            // VDTRMASTER-6052
            isModified = false;
        }

		if (addVector == null) {
			addVector = new Vector();
		}
		if (removeVector == null) {
			removeVector = new Vector();
		}

		if (listUI[currentStep - 1] == null) {
			listUI[currentStep - 1] = new ListUI(true);
		}

		Vector dataVector = new Vector();

		CyoObject[] data = loadedCatalog[currentStep - 1].getCyoObject();

		if (data != null) {
			// VDTRMASTER-5962
			if (data[0] instanceof Group) {

				for (int i = 0; i < data.length; i++) {
					Group curGroup = (Group) data[i];

					dataVector.add(curGroup);

					CyoObject[] groupData = curGroup.getCyoObject();

					Vector sortData = new Vector();
					for (int a = 0; groupData != null && a < groupData.length; a++) {

						if (groupData[a] instanceof Subscription) {
							if (((Subscription) groupData[a]).getSubscribedInAccount() != ((Subscription) groupData[a])
									.getSubscribedInSession()) {
								// changed the one more.
								((Subscription) groupData[a]).setSubscribedInSession(
										!((Subscription) groupData[a]).getSubscribedInSession());
								// update SubscribedInSession
								String modifiable = ((Subscription) groupData[a]).getModifiable();
								if (modifiable.equals("0")) {
									updateSubscribed(groupData[a]);
								}
							}
						}

						sortData.add(groupData[a]);
					}

					Collections.sort(sortData, CyoObjectComparator.comparator);

					groupData = (CyoObject[]) sortData.toArray(groupData);

					addCyoObject(dataVector, groupData);
				}

				data = new CyoObject[dataVector.size()];
				dataVector.copyInto(data);

			} else {
				addCyoObject(dataVector, data);

				data = new CyoObject[dataVector.size()];
				dataVector.copyInto(data);

				dataVector.clear();

				// CyoObject[] data = groups[0].getCyoObject();
				Vector sortData = new Vector();
				for (int a = 0; data != null && a < data.length; a++) {

					if (data[a] instanceof Subscription) {
						if (((Subscription) data[a]).getSubscribedInAccount() != ((Subscription) data[a])
								.getSubscribedInSession()) {
							// changed the one more.
							((Subscription) data[a])
									.setSubscribedInSession(!((Subscription) data[a]).getSubscribedInSession());
							// update SubscribedInSession
							String modifiable = ((Subscription) data[a]).getModifiable();
							if (modifiable.equals("0")) {
								updateSubscribed(data[a]);
							}
						}
					}

					sortData.add(data[a]);
				}

				Collections.sort(sortData, CyoObjectComparator.comparator);

				data = (CyoObject[]) sortData.toArray(data);
			}

			Hashtable addHash = new Hashtable();
			Hashtable removeHash = new Hashtable();
			Log.printDebug("PackageChannelListUI, buildDataForList, addVector");
			for (int a = 0; a < addVector.size(); a++) {
				Subscription ss = (Subscription) addVector.elementAt(a);
				String key = ss instanceof Package || ss instanceof Product ?
						((Subscription) ss).getNo() :
						((Subscription) ss).getId();
				Log.printDebug("PackageChannelListUI, buildDataForList, addVector, ss.getNo() = " + key);
				addHash.put(key, ss);
			}

			Log.printDebug("PackageChannelListUI, buildDataForList, removeVector");
			for (int a = 0; a < removeVector.size(); a++) {
				Subscription ss = (Subscription) removeVector.elementAt(a);
				String key = ss instanceof Package || ss instanceof Product ?
						((Subscription) ss).getNo() :
						((Subscription) ss).getId();
				Log.printDebug("PackageChannelListUI, buildDataForList, removeVector, ss.getNo() = " + key);
				removeHash.put(key, ss);
			}

			CyoObject[] cur = listUI[currentStep - 1].getAllCyoObject();
			Hashtable orignalHash = new Hashtable();
			Log.printDebug("PackageChannelListUI, buildDataForList, make the previous data");
			for (int a = 0; cur != null && a < cur.length; a++) {
				if (cur[a] instanceof Subscription) {
					Subscription ss = (Subscription) cur[a];
					if (ss != null) {
						Log.printDebug(
								"PackageChannelListUI, buildDataForList, ss.getNo() " + ss.getNo() + " " + ss.getId());
						if (ss instanceof Package || ss instanceof Product) {
							if (ss.getNo() != null) {
								orignalHash.put(ss.getNo(), ss);
							}
						} else if (ss instanceof Channel) {
							if (ss.getId() != null) {
								orignalHash.put(ss.getId(), ss);
							}
						}

					}
				}
			}
			Log.printDebug("PackageChannelListUI, buildDataForList, check a update state");
			for (int a = 0; data != null && a < data.length; a++) {
				if (data[a] instanceof Subscription) {
					String key = data[a] instanceof Package || data[a] instanceof Product ?
							((Subscription) data[a]).getNo() :
							((Subscription) data[a]).getId();

					Subscription cyoObj = (Subscription) orignalHash.get(key);

					if (Log.DEBUG_ON) {
						Log.printDebug("PackageChannelListUI, buildDataForList, cyoObj=" + cyoObj);
						Log.printDebug("PackageChannelListUI, buildDataForList, subscription.key=" + key);
					}

					if (cyoObj == null)
						continue;

					cyoObj.setSequenceNo(((Subscription) data[a]).getSequenceNo());

					if (addHash.containsKey(key) && !((Subscription) data[a]).getSubscribedInSession()) {
						Log.printDebug("PackageChannelListUI, buildDataForList, contains in addHash, key=" + key);
						updateSubscribed(data[a]);
						addVector.remove(cyoObj);
						removeVector.remove(cyoObj);
					} else if (removeHash.containsKey(key) && ((Subscription) data[a]).getSubscribedInSession()) {
						Log.printDebug("PackageChannelListUI, buildDataForList, contains in removeHash, key=" + key);
						updateSubscribed(data[a]);
						addVector.remove(cyoObj);
						removeVector.remove(cyoObj);
					}
				}
			}
		}

		//		addCyoObject(dataVector, data);
		//		rearrangeData = new CyoObject[dataVector.size()];
		//		dataVector.copyInto(rearrangeData);

		listUI[currentStep - 1].setCyoObject(data);

		listUI[currentStep - 1].start(true);

		dataVector.clear();

		listUI[currentStep - 1].setHasFocus(true);

		// 	VDTRMASTER-5884
		if (loadedCatalog[currentStep - 1].isPremium()) {
			for (int i = 0; i < data.length; i++) {
				if (data[i] instanceof Subscription && ((Subscription) data[i]).getSubscribedInSession()) {
					lastSubscribedObj = data[i];
					if (Log.DEBUG_ON) {
						Log.printDebug(
								"PackageChannelListUI, buildDataForList, lastSubscribedObj=" + lastSubscribedObj);
					}
					break;
				}
			}
		}

		if (CyoController.getInstance().getRedirectionCode() != CyoController.REDIRECTION_CODE_NONE) {
			CyoController.getInstance().resetRedirectionCode();
			boolean found = false;
			String targetCallLetter = CyoController.getInstance().getTargetCallLetter();
			String sisterCallLetter = null;
			try {
				EpgService eService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
				if (eService != null) {
					TvChannel channel = eService.getChannel(targetCallLetter);
					if (channel != null && channel.getSisterChannel() != null) {
						sisterCallLetter = channel.getSisterChannel().getCallLetter();
					}
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI, buildDataForList, targetCallLetter=" + targetCallLetter);
				Log.printDebug("PackageChannelListUI, buildDataForList, sisterCallLetter=" + sisterCallLetter);
			}

			for (int i = 0; i < listUI[currentStep - 1].getCyoObjectLength(); i++) {
				if (listUI[currentStep - 1].getCurrentCyoObject() instanceof Channel) {
					Channel ch = (Channel) listUI[currentStep - 1].getCurrentCyoObject();

					if (ch.getCallLetters() != null && (ch.getCallLetters().equals(targetCallLetter) || ch
							.getCallLetters().equals(sisterCallLetter))) {
						updateSubscribed(ch);
						needExitPopup = true;
						found = true;
					} else {
						listUI[currentStep - 1].handleKey(OCRcEvent.VK_DOWN);
					}
				}
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI, buildDataForList, found=" + found);
			}

			if (found) {
				state = STATE_BUTTON;
				btnFocus = getChanged() ? 0 : 1;
				currentList.setHasFocus(false);
			} else {
				listUI[currentStep - 1].start(true);
			}
		}

		currentList = listUI[currentStep - 1];

		processNumberOfChannel();
	}

	private void addCyoObject(Vector vec, CyoObject[] data) {
		for (int i = 0; i < data.length; i++) {
			vec.add(data[i]);
			if (data[i] instanceof Package) {
				Package p = (Package) data[i];
				p.setSubscribedOrginalInSession(String.valueOf(p.getSubscribedInSession()));
			} else if (data[i] instanceof Channel) {
				Channel p = (Channel) data[i];
				p.setSubscribedOrginalInSession(String.valueOf(p.getSubscribedInSession()));
			} else if (data[i] instanceof Product) {
				Product p = (Product) data[i];
				p.setSubscribedOrginalInSession(String.valueOf(p.getSubscribedInSession()));
			}

			if (data[i] instanceof Group && data[i].getCyoObject() != null) {
				addCyoObject(vec, data[i].getCyoObject());
			}
		}
	}

	public int getNumberOfChannels() {

		return numberOfChannelArr[currentStep - 1];
	}

	private void processNumberOfChannel() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PackageChannelListUI, processNumberOfChannel");
		}
		PackageCatalog[] pcs = selectedPacakge.getPackageCatalogs();

		for (int i = 0; i < pcs.length; i++) {
			if (pcs[i].isPremium()) {
				numberOfChannelArr[i] = pcs[i].getNumberOfChannelForInt();
			} else {
				String packageCatalogNumberOfChannel = pcs[i].getNumberOfChannels();

				int numberOfChannel = Integer.valueOf(packageCatalogNumberOfChannel).intValue();

				BlockList bList = selectedPacakge.getBlockList();
				CyoObject[] pObjs = bList.getCyoObject();

				if (Log.DEBUG_ON) {
					Log.printDebug("PackageChannelListUI, processNumberOfChannel, blocPackage=" + blocPackage);
				}
				if (blocPackage != null) {
					for (int pIdx = 0; pIdx < pObjs.length; pIdx++) {
						if (((Package) pObjs[pIdx]).getNo().equals(blocPackage.getNo())) {
							if (Log.DEBUG_ON) {
								Log.printDebug("PackageChannelListUI, processNumberOfChannel, find blocPacakge="
										+ blocPackage);
							}
							String bNumberOfChannel = ((Package) pObjs[pIdx]).getNumberOfChannels();

							numberOfChannel += Integer.valueOf(bNumberOfChannel).intValue();
							break;
						}
					}
				} else {
					for (int pIdx = 0; pIdx < pObjs.length; pIdx++) {
						// VDTRMASTER-6004
						if (((Package) pObjs[pIdx]).getSubscribedInSession()) {
							String bNumberOfChannel = ((Package) pObjs[pIdx]).getNumberOfChannels();

							numberOfChannel += Integer.valueOf(bNumberOfChannel).intValue();
							break;
						}
					}
				}

				numberOfChannelArr[i] = numberOfChannel;
			}
		}
	}

	//	private int findGroupIdx(String no) {
	//
	//		for (int i = 0; i < loadedCatalogs.length; i++) {
	//			if (loadedCatalogs[i].getNo().equals(no)) {
	//				return i;
	//			}
	////			else if (no.equals(INTERNATIONAL_GROUP_NO) && loadedCatalogs[i].getNo().equals(SPECIAL_GROUP_NO)) {
	////				return i;
	////			}
	//		}
	//
	//		return -1;
	//	}

	private void addCyoObjectInMyPicks(CyoObject obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("addCyoObjectInMyPicks = " + obj);
			Log.printDebug("currentList = " + currentList);
		}

		if (addVector == null) {
			addVector = new Vector();
		}
		if (removeVector == null) {
			removeVector = new Vector();
		}

		Log.printDebug("addCyoObjectInMyPicks-1 size " + addVector.size());
		Log.printDebug("addCyoObjectInMyPicks-1 size " + removeVector.size());
		Log.printDebug("addCyoObjectInMyPicks-1 removedParam " + removedParam);
		Log.printDebug("addCyoObjectInMyPicks-1 savedParam " + savedParam);

		removeVector.remove(obj);
		removedParam.remove(((Subscription) obj).getNo());

		addVector.remove(obj);
		savedParam.remove(((Subscription) obj).getNo());

		addVector.add(obj);
		savedParam.add(((Subscription) obj).getNo());

		Log.printDebug("addCyoObjectInMyPicks-2 size " + addVector.size());
		Log.printDebug("addCyoObjectInMyPicks-2 size " + removeVector.size());
		Log.printDebug("addCyoObjectInMyPicks-2 removedParam " + removedParam);
		Log.printDebug("addCyoObjectInMyPicks-2 savedParam " + savedParam);
	}

	private void removeCyoObjectInMyPicks(CyoObject obj) {
		Log.printInfo("removeCyoObjectInMyPicks = " + obj);
		Log.printDebug("currentList = " + currentList);

		Log.printDebug("removeCyoObjectInMyPicks " + obj);
		Log.printDebug("removeCyoObjectInMyPicks-1 addVector.size " + addVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-1 removeVector.size " + removeVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-1 removedParam " + removedParam);
		Log.printDebug("removeCyoObjectInMyPicks-1 savedParam " + savedParam);

		addVector.remove(obj);
		savedParam.remove(((Subscription) obj).getNo());

		removeVector.remove(obj);
		removedParam.remove(((Subscription) obj).getNo());

		removeVector.add(obj);
		removedParam.add(((Subscription) obj).getNo());

		Log.printDebug("removeCyoObjectInMyPicks-2 addVector.size " + addVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-2 removeVector.size " + removeVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-2 removedParam " + removedParam);
		Log.printDebug("removeCyoObjectInMyPicks-2 savedParam " + savedParam);
	}

	public String getSortString() {
		if (sort.equals(Param.SORT_ALPHABETICAL)) {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_ALPHABETICAL);
		} else {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_CHANNEL_NUMBER);
		}
	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);
		removePopup(pop);

		if (gotoHotKey) {
			gotoHotKey = false;
			super.popupClosed(pop, msg);
			return;
		}

		if (messagePopupVector != null && messagePopupVector.size() > 0) {
			Message message = (Message) messagePopupVector.remove(0);

			// Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
			notiPop.setExplain(message.getText());
			String[] proccessedMsg = notiPop.getExplain();

			if (message.getType().equals("3")) {
				// show CYO_13
				if (message.getId().equals("35")) {
					// VDTRMASTER-5747
					notiPop.stop();

					vPop.setIcon(true);
					setPopup(vPop);
					vPop.show(this, message.getText());
				} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
					// VDTRMASTER-5747
					notiPop.stop();

					setPopup(infoPopup);
					infoPopup.show(this, message);
				} else if (proccessedMsg == null) {
					// VDTRMASTER-5747
					notiPop.stop();

					vPop.setIcon(false);
					setPopup(vPop);
					vPop.show(this, message.getText());
				} else {
					setPopup(notiPop);
					//notiPop.setExplain(message.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
			} else if (message.getType().equals("5")) {
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				expired = true;
			} else if (message.getType().equals("1")) {
				// show CYO_14
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
			} else {
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			}
			repaint();
			return;
		}

		if (expired) {
			cyoReStart();
		} else if (pop.equals(vPop)) {
			repaint();
		} else if (pop.equals(notiPop)) {
			if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					// if (backButton) {
					// CyoDataManager.getInstance().request(URLType.LOGIN,
					// null);
					// } else {
					// //resetData();
					// dispose();
					// savedParam.clear();
					// removedParam.clear();
					// CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE,
					// true);
					// }

					if (lastUIId != UI_TV_PACKAGE_LIST && lastUIId != UI_INITIAL_STATE) {
						CyoController.getInstance().showUI(UI_MANAGE_TV_SERVICE, true);
					} else {
						CyoController.getInstance().showUI(lastUIId, true);
					}
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
				Integer result = (Integer) msg;
				if (result.intValue() == 0) {
					buttonClick = false;
					setData();
				} else if (result.intValue() == 1) {
					// resetData();
					dispose();
					savedParam.clear();
					removedParam.clear();
					CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
				} else if (result.intValue() == 2) {
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					focus = 0;
					currentList = listUI[currentStep - 1];
					setFooter();
					state = STATE_LIST;
				} else {
					if (lastRequestedURLType == URLType.SAVE_SELF_SERVE) {
						if (lastUIId == BaseUI.UI_INITIAL_STATE) {
							// VDTRMASTER-5970
							CyoDataManager.getInstance().request(URLType.VALIDATION, null);
						} else {
							if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
								CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
							} else {
								CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
							}
						}
					} else {
						CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
					}
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL_CANCEL) {
				Boolean result = (Boolean) msg;

				if (result.booleanValue()) {

					for (int a = 0; a < listUI[currentStep - 1].getCyoObjectLength(); a++) {
						CyoObject obj = listUI[currentStep - 1].getCyoObject(a);

						if (obj instanceof Package) {
							if (obj.getCyoObject() != null) {
								CyoObject[] subObj = obj.getCyoObject();
								for (int j = 0; j < subObj.length; j++) {
									Subscription ssubObj = (Subscription) subObj[j];
									if (((Package) obj).getSubscribedInSession() && ((Package) obj).getModifiable()
											.equals("0") && (ssubObj.getModifiable().equals("1") || ssubObj
											.getModifiable().equals("0"))) {
										Log.printInfo("1getid = " + ssubObj.getId());
										if (addVector.contains(ssubObj)) {
											addVector.remove(ssubObj);
										}

										if (!removeVector.contains(ssubObj) && ssubObj.getNo() != null) {
											removeVector.add(ssubObj);
										}
										ssubObj.setSubscribedInSession(false);
									}
								}

								if (addVector.contains(obj)) {
									addVector.remove(obj);
								}

								if (!removeVector.contains(obj) && obj != null) {
									removeVector.add(obj);
								}

								if (((Package) obj).getModifiable().equals("0")) {
									((Package) obj).setSubscribedInSession(false);
								}
							}
						} else if (obj instanceof Channel) {
							Channel ch = (Channel) obj;
							if ((ch.getModifiable().equals("0")) && ch.getSubscribedInSession()) {
								Log.printInfo("2getid = " + ch.getId());

								if (addVector.contains(obj)) {
									addVector.remove(obj);
								}
								if (!removeVector.contains(obj) && ch.getNo() != null) {
									removeVector.add(obj);
								}
								ch.setSubscribedInSession(false);
							}
						} else if (obj instanceof Product) {
							Product p = (Product) obj;
							if ((p.getModifiable().equals("0")) && p.getSubscribedInSession()) {
								Log.printInfo("2getNo = " + p.getNo());

								if (addVector.contains(obj)) {
									addVector.remove(obj);
								}
								if (!removeVector.contains(obj) && obj != null) {
									removeVector.add(obj);
								}
								p.setSubscribedInSession(false);
							}
						}
					}
					addVector.clear();
					renderer.update(this);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL) {
				// if (saveMessageView) {
				// saveMessageView = false;

				if (lastRequestedURLType == URLType.SAVE_SELF_SERVE && lastObject.getSuccess().equals("1")) {
					if (!comeFromBack) {
						if (!channelInfoBoolean) {
							if (lastUIId == BaseUI.UI_INITIAL_STATE) {
								// VDTRMASTER-5970
								CyoDataManager.getInstance().request(URLType.VALIDATION, null);
							} else {
								if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
									CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
								} else {
									CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
								}
							}
						} else {
							channelInfoBoolean = false;
						}
					} else {
						comeFromBack = false;
					}
				}
				// }
			} else if (pop.getState() == NotificationPopupUI.STATE_CANCEL_CHANGE) {

				Integer result = (Integer) msg;
				if (result.intValue() == 0) {
					CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
				}
			}
			repaint();
		} else if (pop.equals(validationUpSellPopupUI)) {
			Boolean result = (Boolean) msg;
			Log.printDebug("PackageChannelListUI, popupClosed, validationUpSellPopupUI, result=" + result);
			if (result.booleanValue()) {
				Message message = validationUpSellPopupUI.getMessage();

				DataCenter.getInstance().put("UPSELL_MESSAGE", message);

				if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_PACKAGE) || message
						.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {

					CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);

				} else if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_A_LA_CARTE)) {
					// NOTHING;
					Log.printDebug(
							"PackageChannelListUI, popupClosed, optimizationType is" + " OPTIMIZATION_TYPE_A_LA_CARTE");
				}

			} else {
				// VDTRMASTER-5970
				if (lastUIId == BaseUI.UI_INITIAL_STATE) {
					CyoDataManager.getInstance().request(URLType.VALIDATION, null);
				} else {
					if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
						CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
					} else {
						CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
					}
				}
			}
		}
	}

	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("PackageChannelListUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
					+ obj);
		}

		lastRequestedURLType = type;
		lastObject = obj;

		if (type == URLType.LOAD_PACKAGE_OFFER) {
			if (obj.getSuccess().equals("1")) {
				startAfterLoadPackageOffer();
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					setPopup(notiPop);
					notiPop.setExplain(msg.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					Log.printInfo("msg.getType() = " + msg.getType());
					if ((msg.getType()).equals("5")) {
						expired = true;
					}
				}
			}
		} else if (type == URLType.LOAD_PACKAGE_CATALOG) {
			if (obj.getSuccess().equals("1")) {
				if (obj instanceof Services) {
					services = (Services) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug("PackageChannelListUI: receiveCyoData: cyoData length = " + services
								.getCyoObject().length);
					}
					CyoObject[] cyoObjs = services.getCyoObject();

					Message message = null;

					if (cyoObjs[0] instanceof Message) {
						message = (Message) cyoObjs[0];
						if (Log.DEBUG_ON) {
							Log.printDebug("PackageChannelListUI: receiveCyoData: found message");
						}
					} else if (cyoObjs[0] instanceof Catalog) {
						buildDataForList(cyoObjs);
						setFooter();
						renderer.update(this);
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("PackageChannelListUI: receiveCyoData: obj = " + obj);
						}
					}

					if (message != null && needDisplay5CPopup) {
						setPopup(infoPopup);
						infoPopup.show(this, message);
						needDisplay5CPopup = false;
					}
				} else if (obj instanceof Group) {
					Vector dataVector = new Vector();
					Group group = (Group) obj;
					CyoObject[] data = group.getCyoObject();

					addCyoObject(dataVector, data);

					CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
					dataVector.copyInto(rearrangeData);

					dataVector.clear();

					listUI[currentStep - 1].setCyoObject(rearrangeData);
					listUI[currentStep - 1].setHasFocus(true);
					listUI[currentStep - 1].start(true);
				}
				repaint();
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					setPopup(notiPop);
					notiPop.setExplain(msg.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					Log.printInfo("msg.getType() = " + msg.getType());
					if ((msg.getType()).equals("5")) {
						expired = true;
					}
				}
			}

		} else if (type == URLType.SAVE_SELF_SERVE) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI: receiveCyoData: SAVE_SELF_SERVE success = " + obj.getSuccess());
			}

			Messages messages = (Messages) obj;

			if (messages.getSuccess().equals("1")) {
				CyoObject[] subObjs = messages.getCyoObject();

				if (subObjs != null) {
					messagePopupVector.clear();
					for (int i = 0; i < subObjs.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug("PackageChannelListUI: receiveCyoData: SAVE_SELF_SERVE subObjs[" + i + "] = "
									+ subObjs[i]);
						}

						if (subObjs[i] instanceof Message) {
							messagePopupVector.add(subObjs[i]);
						}
					}
					StringBuffer sf = new StringBuffer();
					Message message = (Message) messagePopupVector.remove(0);
					Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());
					if (message.getType().equals("0")) {// &&
						// message.getId().equals("140")
						sf.append(message.getText());
						sf.append("\n");
						setPopup(notiPop);
						notiPop.setExplain(sf.toString());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					} else if (message.getType().equals("1")) {
						if (message.getOptimizationType() != null) {
							if (Log.DEBUG_ON) {
								Log.printDebug("message.optimizationType=" + message.getOptimizationType());
								Log.printDebug("message.optimizationCode=" + message.getOptimizationCode());
							}
							setPopup(validationUpSellPopupUI);
							validationUpSellPopupUI.show(this, message);
						} else {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
						}
					} else {
						if (lastUIId == BaseUI.UI_INITIAL_STATE) {
							CyoDataManager.getInstance().request(URLType.VALIDATION, null);
						} else {
							if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
								CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
							} else {
								CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
							}
						}
					}
				} else {
					if (lastUIId == BaseUI.UI_INITIAL_STATE) {
						CyoDataManager.getInstance().request(URLType.VALIDATION, null);
					} else {
						if (lastUIId == BaseUI.UI_MANAGE_TV_SERVICE) {
							CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
						} else {
							CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
						}
					}
				}
			} else {
				CyoObject[] subObjs = obj.getCyoObject();

				if (subObjs != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("PackageChannelListUI: receiveCyoData: subObjs = " + subObjs);
					}
					for (int i = 0; i < subObjs.length; i++) {
						Log.printDebug("PackageChannelListUI: receiveCyoData: subObjs = " + subObjs[i]);
						if (subObjs[i] instanceof Message) {
							messagePopupVector.add(subObjs[i]);
						}
					}

					if (messagePopupVector != null && messagePopupVector.size() > 0) {
						Message message = (Message) messagePopupVector.remove(0);

						// Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
						notiPop.setExplain(message.getText());
						String[] proccessedMsg = notiPop.getExplain();

						if (message.getType().equals("3")) {
							// show CYO_13
							if (message.getId().equals("35")) {
								// VDTRMASTER-5747
								notiPop.stop();

								vPop.setIcon(true);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
								// VDTRMASTER-5747
								notiPop.stop();

								setPopup(infoPopup);
								infoPopup.show(this, message);
								notiPop.setExplain(null);
							} else if (proccessedMsg == null) {
								// VDTRMASTER-5747
								notiPop.stop();

								vPop.setIcon(false);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else {
								setPopup(notiPop);
								//notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else if ((message.getType()).equals("5")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							expired = true;
						} else {
							// show CYO_14
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
					}
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("PackageChannelListUI: receiveCyoData: subObjs is null");
					}
				}
			}
			repaint();
		} else if (type == URLType.VALIDATION) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PackageChannelListUI: receiveCyoData: VALIDATION success = " + obj.getSuccess());
			}
			savedParam.clear();
			removedParam.clear();
			if (obj instanceof Services) {
				Services services = (Services) obj;
				if (services.getSuccess().equals("1")) {
					stop();
					if (services.getMessage() != null) {
						setPopup(notiPop);
						notiPop.setExplain(services.getMessage()[0].getText());
						notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
					} else {
						if (buttonClick) {
							// goto CYO_015
							if (btnFocus == 0) {
								DataCenter.getInstance().put("VALIDATION", obj);
								if (lastUIId == UI_INITIAL_STATE) {
									CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
								} else {
									CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
								}
							}
						} else {
							CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
						}
					}
				} else {
					CyoObject[] subObjs = services.getCyoObject();
					if (subObjs != null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("PackageChannelListUI: receiveCyoData: subObjs = " + subObjs);
						}
						for (int i = 0; i < subObjs.length; i++) {
							if (Log.DEBUG_ON) {
								Log.printDebug(
										"PackageChannelListUI: receiveCyoData: subObjs[" + i + "] = " + subObjs[i]);
							}

							if (subObjs[i] instanceof Message) {
								messagePopupVector.add(subObjs[i]);
							}
						}
						if (messagePopupVector != null && messagePopupVector.size() > 0) {
							Message message = (Message) messagePopupVector.remove(0);

							if (Log.DEBUG_ON) {
								Log.printDebug("PackageChannelListUI: receiveCyoData: message = " + message);
								Log.printDebug("PackageChannelListUI: receiveCyoData: message = " + message.getId());
							}

							notiPop.setExplain(message.getText());
							String[] proccessedMsg = notiPop.getExplain();

							if (message.getType().equals("3")) {
								// show CYO_13
								if (message.getId().equals("35")) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(true);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
									// VDTRMASTER-5747
									notiPop.stop();

									setPopup(infoPopup);
									infoPopup.show(this, message);
									notiPop.setExplain(null);
								} else if (proccessedMsg == null) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(false);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else {
									setPopup(notiPop);
									//notiPop.setExplain(message.getText());
									notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								}
							} else if ((message.getType()).equals("5")) {
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								expired = true;
							} else if (message.getId().equals("1")) {
								setPopup(notiPop);
								notiPop.setExplain(services.getMessage()[0].getText());
								notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
							} else {
								// show CYO_14
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						}
						repaint();
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("PackageChannelListUI: receiveCyoData: subObjs is null");
						}
					}
				}
			}
		}
	}

	public void receiveCyoError(int type) {
		if (type == URLType.LOAD_GROUP) {
			sort = sort == Param.SORT_NUMBER ? Param.SORT_ALPHABETICAL : Param.SORT_NUMBER;
		}
	}

	public void selected(MenuItem item) {
		// sort
		Hashtable param = new Hashtable();
		param.put("catalogId", loadedCatalog[currentStep - 1].getCatalogId());
		boolean checkSort = false;

		if (item.equals(nSORT)) {
			param.put("sort", Param.SORT_NUMBER);
			checkSort = sort == Param.SORT_NUMBER ? true : false;
			sort = Param.SORT_NUMBER;
		} else {
			param.put("sort", Param.SORT_ALPHABETICAL);
			checkSort = sort == Param.SORT_ALPHABETICAL ? true : false;
			sort = Param.SORT_ALPHABETICAL;
		}

		if (!checkSort) {
			CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_CATALOG, param);
		}
	}

	public void canceled() {
	}

	public boolean getChanged() {
		Log.printDebug("PackageChannelListUI: getChanged()");

		if (checkAddVector() || checkRemoveVector()) {
			for (int j = 0; j < addVector.size(); j++) {
				CyoObject obj = (CyoObject) addVector.elementAt(j);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				} else if (obj instanceof Package) {
					Package sobj = (Package) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}

			for (int j = 0; j < removeVector.size(); j++) {
				CyoObject obj = (CyoObject) removeVector.elementAt(j);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				} else if (obj instanceof Package) {
					Package sobj = (Package) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}
			return false;
		} else {
			return false;
		}
	}

	/**
	 * to use the next and validate buttons are ma active or inactive.
	 *
	 * @return if true selected number of channel same with number of channel of current package catalog.
	 */
	public boolean isSatisfying() {
		if (currentList != null) {
			if (targetStep == 1) {
				int selectedCount = Integer.valueOf(currentList.getSelectedChannel()).intValue();
				if (getCurrentCatalog().isPremium()) {
					selectedCount = Integer.valueOf(currentList.getSelectedChannelForPremium()).intValue();
				}
                // VDTRMASTER-6052
				if (lastUIId == UI_INITIAL_STATE) {
                    if (numberOfChannelArr[0] == selectedCount && isModified) {
                        return true;
                    }
                } else {
                    if (numberOfChannelArr[0] == selectedCount) {
                        return true;
                    }
                }
			} else {
				int selectedCount = Integer.valueOf(currentList.getSelectedChannel()).intValue();
				if (getCurrentCatalog().isPremium()) {
					selectedCount = Integer.valueOf(currentList.getSelectedChannelForPremium()).intValue();
				}

				if (currentStep == STEP_1) {
                    if (numberOfChannelArr[0] == selectedCount) {
                        return true;
                    }
				} else {
				    // VDTRMASTER-6052
                    if (lastUIId == UI_INITIAL_STATE) {
                        if (numberOfChannelArr[1] == selectedCount && isModified) {
                            return true;
                        }
                    } else {
                        if (numberOfChannelArr[1] == selectedCount) {
                            return true;
                        }
                    }
				}
			}
		}

		return false;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public int getTargetStep() {
		return targetStep;
	}

	public Catalog getCurrentCatalog() {
		return loadedCatalog[currentStep - 1];
	}

	public void requestLoadCatalogForCurrentStep() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PackageChannelListUI, requestLoadCatalogForCurrentStep");
		}
		String catalogId = selectedPacakge.getPackageCatalogs()[currentStep - 1].getCatalog().getCatalogId();
		Hashtable param = new Hashtable();
		param.put("catalogId", catalogId);

		if (Log.DEBUG_ON) {
			Log.printDebug("PackageChannelListUI, requestLoadCatalogForCurrentStep, catalogId=" + catalogId);
		}

		CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_CATALOG, param);
	}

	public void moveToNextStep() {
		currentStep++;
		if (listUI[currentStep - 1] != null && listUI[currentStep - 1].getCyoObjectLength() > 0) {
			currentList = listUI[currentStep - 1];
            currentList.setHasFocus(true);
			renderer.update(this);
			state = STATE_LIST;
			repaint();
		} else {
			requestLoadCatalogForCurrentStep();
			state = STATE_LIST;
		}
	}

	public void moveToPrevious() {
		if (currentStep == STEP_1) {
			return;
		}
		currentStep--;

		if (listUI[currentStep - 1] != null && listUI[currentStep - 1].getCyoObjectLength() > 0) {
			currentList = listUI[currentStep - 1];
			currentList.setHasFocus(true);
			state = STATE_LIST;
			renderer.update(this);
			repaint();
		} else {
			requestLoadCatalogForCurrentStep();
			state = STATE_LIST;
		}
	}
}

/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Enumeration;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.gui.ManageTVServiceListRenderer;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;

/**
 * ListUI display a list of service, group, package, channel etc.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class ManageTVServiceListUI extends ListUI {

	public static final int FOCUS_NONE = 0;
	public static final int FOCUS_ON_PARENT = 1;
	public static final int FOCUS_ON_CHILD = 2;
	public static final int FOCUS_ON_ME = 3;
	private ClickingEffect clickEffect;

	public ManageTVServiceListUI() {
		super(false);
		renderer = new ManageTVServiceListRenderer();
		setRenderer(renderer);
	}

	public void start(boolean back) {
		prepare();
		startIdx = 0;
		focus = 0;
		while (getCyoObject(startIdx + focus) instanceof Group) {
			Group g = (Group) getCyoObject(startIdx + focus);
			Log.printDebug("ListUI: start(): startIdx = " + startIdx + ", focus = " + focus);
			if (g.getUrl() == null) {
				keyDown();
			} else {
				break;
			}
		}
		start();
	}

	public void stop() {
		if (imageKeys.size() > 0) {
			Enumeration enums = imageKeys.elements();

			while (enums.hasMoreElements()) {
				String key = (String) enums.nextElement();
				dataCenter.removeImage(key);
			}
			imageKeys.clear();
		}
		super.stop();
	}

	public boolean handleKey(int code) {
		CyoObject curObj;

		switch (code) {
		case OCRcEvent.VK_UP:
			keyUp();

			// handle a case of group
			curObj = getCyoObject(startIdx + focus);
			if (curObj instanceof Group && ((Group) curObj).getUrl() == null) {
				if (startIdx == 0 && focus == 0) {
					keyDown();
				} else {
					keyUp();
				}
			}
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();

			// handle a case of group
			curObj = getCyoObject(startIdx + focus);
			if (curObj instanceof Group && ((Group) curObj).getUrl() == null) {
				keyDown();
			}
			return true;
		default:
			return false;
		}
	}

	private void keyUp() {
		if (objs != null) {
			if (startIdx + focus >= objs.length) {
				startIdx--;
			} else if (startIdx + focus > 0) {
				if (focus > 5) {
					focus--;
				} else if (startIdx > 0) {
					if (focus < 5) {
						startIdx--;
					} else {
						focus--;
					}
				} else {
					focus--;
				}
			}
		}
	}

	public void keyEnter() {
		if (clickEffect == null) {
			clickEffect = new ClickingEffect(this, 5);
		}
		Log.printInfo("focus = " + focus);
		clickEffect.start(441, ((ManageTVServiceListRenderer) renderer).getFocusHeight(), 131, 26);
	}

	public void keyDown() {
		if (objs != null) {
			if (startIdx + focus < objs.length - 1) {
				if (focus < 5) {
					focus++;
				} else if (startIdx + 9 < objs.length) {
					startIdx++;
				} else {
					focus++;
					if (startIdx + focus >= objs.length) {
						focus--;
					}
				}
			}
		}
	}

	public int getStartIdx() {
		return startIdx;
	}

	public CyoObject getCyoObject(int idx) {
		if (objs != null && idx < objs.length && objs[idx] != null) {
			return objs[idx];
		}

		return null;
	}

	public CyoObject getCurrentCyoObject() {
		int idx = startIdx + focus;
		if (objs != null && idx < objs.length && objs[idx] != null) {
			return objs[idx];
		}
		return null;
	}

	public int getCyoObjectLength() {
		if (objs != null) {
			return objs.length;
		} else {
			return 0;
		}
	}

	public void setCyoObject(CyoObject[] cyoObjs) {
		this.objs = cyoObjs;
	}

	public boolean hasFocus() {
		return hasFocus;
	}

	public void setHasFocus(boolean hasFocus) {
		this.hasFocus = hasFocus;
	}

	public boolean onFocus(int idx) {
		if (startIdx + focus == idx) {
			return true;
		}

		return false;
	}

	public int getFocusType(int idx) {

		if ((startIdx + focus) == idx) {
			return FOCUS_ON_ME;
		} else {
			return FOCUS_NONE;
		}
	}

	public boolean isDisplayBottomDeco() {
		if (objs == null) {
			return false;
		}
		if (startIdx < objs.length - 10) {
			return true;
		}

		return false;
	}
}

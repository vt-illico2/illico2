/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.cyo.gui.ValidateListRenderer;

/**
 * ListUI display a list of service, group, package, channel etc.
 * @author Woojung Kim
 * @version 1.1
 */
public class ValidateListUI extends BaseUI {

    public static final int FOCUS_NONE = 0;
    public static final int FOCUS_ON_PARENT = 1;
    public static final int FOCUS_ON_CHILD = 2;
    public static final int FOCUS_ON_ME = 3;

    protected int startIdx;
    private boolean hasFocus;
    private boolean withPrice;

    protected String[] currentDesc;

    public ValidateListUI() {
        renderer = new ValidateListRenderer();
        setRenderer(renderer);
    }

    /** The current items of List. */
    private CyoObject[] objs;

    public void start(boolean back) {
        prepare();
        startIdx = 0;
        focus = 0;
        if (getCyoObject(0) instanceof Group) {
            focus = 1;
        }

        super.start(back);
    }

    public void stop() {
        super.stop();
    }

    public boolean handleKey(int code) {
        CyoObject curObj;

        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();

            // handle a case of group
            curObj = getCyoObject(startIdx + focus);
            if (curObj instanceof Group) {
                if (startIdx == 0 && focus == 0) {
                    keyDown();
                } else {
                    keyUp();
                }
            }
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();

            // handle a case of group
            curObj = getCyoObject(startIdx + focus);
            if (curObj instanceof Group) {
                keyDown();
            }
            return true;
        case OCRcEvent.VK_PAGE_UP:
            if (needScroll()) {
                if (--startIdx < 0) {
                    startIdx = 0;
                }
            }
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            if (needScroll()) {
                if (++startIdx == objs.length - 9) {
                    startIdx--;
                }
            }
            return true;
        default:
            return false;
        }
    }

    private void keyUp() {
        if (objs != null) {
            if (startIdx + focus >= objs.length) {
                startIdx--;
            } else if (startIdx + focus > 0) {
                if (focus > 3) {
                    focus--;
                } else if (startIdx > 0) {
                    if (focus < 4) {
                        startIdx--;
                    } else {
                        focus--;
                    }
                } else {
                    focus--;
                }
            }
        }
    }

    private void keyDown() {
        if (objs != null) {
            if (startIdx + focus < objs.length - 1) {
                if (focus < 4) {
                    focus++;
                } else if (startIdx + 7 < objs.length) {
                    if (focus > 3) {
                        startIdx++;
                    } else {
                        focus++;
                    }
                } else {
                    if (startIdx + focus > objs.length - 9) {
                        focus++;
                    } else {
                        startIdx++;
                        focus++;
                        if (startIdx + focus >= objs.length) {
                            startIdx--;
                        }
                    }
                }
            }
        }
    }

    public int getStartIdx() {
        return startIdx;
    }

    public CyoObject getCyoObject(int idx) {
        if (objs != null && idx < objs.length && objs[idx] != null) {
            return objs[idx];
        }

        return null;
    }

    public CyoObject getCurrentCyoObject() {
        int idx = startIdx + focus;
        if (objs != null && idx < objs.length && objs[idx] != null) {
            return objs[idx];
        }
        return null;
    }

    public int getCyoObjectLength() {
        if (objs != null) {
            return objs.length;
        } else {
            return 0;
        }
    }

    public void setCyoObject(CyoObject[] cyoObjs) {
        this.objs = cyoObjs;
    }

    public boolean hasFocus() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    public boolean onFocus(int idx) {
        if (startIdx + focus == idx) {
            return true;
        }

        return false;
    }

    public boolean isWithPrice() {
        return withPrice;
    }

    public int getScrollGap() {
        if (startIdx == 0) {
            return 0;
        } else {
            int gap = 282 * (startIdx * 100 / (objs.length - 10)) / 100;
            return gap;
        }
    }
    
    public boolean needScroll() {
        if (objs != null && objs.length > 10) {
            return true;
        }
        return false;
    }

    public int getFocusType(int idx) {
        CyoObject obj = getCyoObject(idx);
        CyoObject currentObj = getCyoObject(startIdx + focus);

        if (obj instanceof Package) {

            if (obj == currentObj.getParentObj()) {
                return FOCUS_ON_CHILD;
            } else if (obj == currentObj) {
                return FOCUS_ON_ME;
            } else {
                return FOCUS_NONE;
            }

        } else {
            if (obj.getParentObj() == currentObj) {
                return FOCUS_ON_PARENT;
            } else if (obj == currentObj) {
                return FOCUS_ON_ME;
            } else if (obj.getParentObj() == currentObj.getParentObj()) {
                return FOCUS_ON_CHILD;
            } else {
                return FOCUS_NONE;
            }
        }
    }

    public String getSelectedChannel() {
        int count = 0;

        if (objs != null) {
            for (int i = 0; i < objs.length; i++) {
                if (objs[i] instanceof Channel && ((Subscription) objs[i]).getSubscribedInSession()) {
                    count++;
                }
            }
        }

        return String.valueOf(count);
    }
}

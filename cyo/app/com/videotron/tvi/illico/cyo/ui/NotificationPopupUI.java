/**
 * @(#)NotificationPopupUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.awt.Color;
import java.util.Enumeration;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.cyo.gui.NotificationRenderer;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class NotificationPopupUI extends BaseUI {

    /** The indication to display a normal text with only OK. */
    public static final int STATE_NORMAL = 0;
    /** The indication to display a normal text with ok and cancel. */
    public static final int STATE_NORMAL_CANCEL = 1;
    /** The indication to display a cancel of current selection. CYO_016. */
    public static final int STATE_CANCEL = 2;
    /** The indication to display a keeping selection. CYO_14. */
    public static final int STATE_KEEP_SELECTIION = 3;

    /** The indication to display a cancel of current selection. CYO_016. */
    public static final int STATE_VALIDATE_CANCEL = 4;
    
    public static final int STATE_HD_NETWORK_FEES = 5;
    
    public static final int STATE_CANCEL_CHANGE = 6;

    public static final int STATE_UNSELECT_MESSAGE_FOR_A_LA_CARTE = 7;

    private ClickingEffect clickEffect;

    private Renderer renderer = new NotificationRenderer();
    private ScrollTexts scrollTexts = new ScrollTexts();

    private BaseUI parent;

    private String[] explainStr;
    private String titleStr;

    /**
     * Constructor.
     */
    public NotificationPopupUI() {
        setRenderer(renderer);

        scrollTexts.setForeground(Color.white);
        scrollTexts.setRowHeight(20);
        scrollTexts.setFont(FontResource.BLENDER.getFont(18));
        scrollTexts.setInsets(0, 0, 0, 0);
        scrollTexts.setBounds(310, 183, 360, 180);
        scrollTexts.setRows(8);
        scrollTexts.setVisible(true);

        footer.setBounds(306, 370, 306, 24);

        Log.printInfo("scrollTexts.getPageCount()" + scrollTexts.getPageCount());
        if (scrollTexts.getPageCount() > 1) {
            footer.setVisible(true);
        } else {
            footer.setVisible(false);
        }

        this.add(scrollTexts);
    }

    public void show(BaseUI p, int s) {
    	if (s == STATE_CANCEL_CHANGE) {
    		focus = 1;
    	} else {
    		focus = 0;
    	}
        this.parent = p;
        this.state = s;
        prepare();
        super.start(false);
    }

    public void show(BaseUI p, int s, String title, String explain) {
    	if (s == STATE_CANCEL_CHANGE) {
    		focus = 1;
    	} else {
    		focus = 0;
    	}
        this.parent = p;
        this.state = s;
        prepare();
        setTitle(title);
        setExplain(explain);
        super.start(false);
    }

    public void stop() {
        titleStr = null;
        explainStr = null;
        if (scrollTexts != null) {
            scrollTexts.setVisible(false);
        }
        super.stop();
    }

    public void dispose() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                dataCenter.removeImage(key);
            }
            imageKeys.clear();
        }
    }

    public boolean handleKey(int code) {
        Log.printInfo("NotificationPopupUI = " + code);
        switch (code) {
        case OCRcEvent.VK_PAGE_UP:
            if (scrollTexts.isVisible()) {
                footer.clickAnimation(0);
                scrollTexts.showPreviousPage();
            }
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            if (scrollTexts.isVisible()) {
                footer.clickAnimation(0);
                scrollTexts.showNextPage();
            }
            return true;
        case OCRcEvent.VK_UP:
            if (state == STATE_KEEP_SELECTIION) {
                keyLeft();
            }
            return true;
        case OCRcEvent.VK_DOWN:
            if (state == STATE_KEEP_SELECTIION) {
                keyRight();
            }
            return true;
        case OCRcEvent.VK_LEFT:
            if (state != STATE_KEEP_SELECTIION) {
                keyLeft();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (state != STATE_KEEP_SELECTIION) {
                keyRight();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            keyEnter();
            return true;
        case OCRcEvent.VK_EXIT:
            if (state == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
                Integer num = new Integer(2);
                parent.popupClosed(this, num);
                return true;
            } else {
                return false;
            }
        default:
            return true;
        }
    }

    private void keyLeft() {
        if (state != STATE_NORMAL) {
            if (focus == 1) {
                focus = 0;
                repaint();
            }
        }
    }

    private void keyRight() {
        if (state != STATE_NORMAL) {
            if (focus == 0) {
                focus = 1;
                repaint();
            }
        }
    }

    public void keyEnter() {
        stop();
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
        }
        if (state == STATE_NORMAL || state == STATE_HD_NETWORK_FEES) {
            clickEffect.start(403, 368, 162, 40);
            parent.popupClosed(this, Boolean.TRUE);
        } else if (state != STATE_VALIDATE_CANCEL) {
            if (focus == 0) {
                if (state == STATE_KEEP_SELECTIION) {
                    clickEffect.start(333, 331, 301, 40);
                } else {
                    clickEffect.start(321, 318, 162, 40);
                }
                parent.popupClosed(this, Boolean.TRUE);
            } else {
                if (state == STATE_KEEP_SELECTIION) {
                    clickEffect.start(333, 368, 301, 40);
                } else {
                    clickEffect.start(485, 318, 162, 40);
                }
                parent.popupClosed(this, Boolean.FALSE);
            }
        } else {
            if (focus == 0) {
                clickEffect.start(187, 378, 162, 40);
                Integer num = new Integer(0);
                parent.popupClosed(this, num);
            } else {
                clickEffect.start(485, 318, 162, 40);
                Integer num = new Integer(1);
                parent.popupClosed(this, num);
            }
        }
    }

    public void setExplain(String src) {
        footer.reset();
        if (src != null) {
        	src = TextUtil.replace(src, "\\n", "|");
            explainStr = TextUtil.split(src, BaseRenderer.fm18, 310, "|");
            for(int a = 0; explainStr != null && a < explainStr.length; a++) {
                Log.printDebug(explainStr[a]);
            }
            
            if (explainStr.length > 8) {
                scrollTexts.setContents(src);
                scrollTexts.setVisible(true);
                explainStr = null;

                footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI
                        .getMenuText(KeyNames.BTN_SCROLL));

            } else {
                scrollTexts.setVisible(false);
            }
        } else {
            explainStr = null;
        }
    }
    
    public String[] getExplain() {
        return explainStr;
    }

    public String getTitle() {
        if (titleStr == null) {
            if (state == NotificationPopupUI.STATE_CANCEL) {
                return BaseUI.getMenuText(KeyNames.CANCEL_YOUR_SELECTION_TITLE);
            } else if (state == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
                return BaseUI.getMenuText(KeyNames.CANCEL_VALIDATE_TITLE);
            } else if (state == NotificationPopupUI.STATE_KEEP_SELECTIION) {
                return BaseUI.getMenuText(KeyNames.OTHER_SUGGESTION_TITLE);
            } else if (state == NotificationPopupUI.STATE_HD_NETWORK_FEES) {
            	return BaseUI.getMenuText(KeyNames.HD_NETWORK_FEES);
            } else {
                return BaseUI.getMenuText(KeyNames.NOTIFICATION);
            }
        } else {
            return titleStr;
        }
    }

    public void setTitle(String t) {
        titleStr = t;
    }
}

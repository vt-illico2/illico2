/**
 * @(#)ChangeChannelSelectionUI.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.ChannelList;
import com.videotron.tvi.illico.cyo.data.obj.CyoObjectComparator;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.Services;
import com.videotron.tvi.illico.cyo.gui.NewChannelRenderer;
import com.videotron.tvi.illico.cyo.popup.MenuItem;
import com.videotron.tvi.illico.cyo.popup.MenuListener;
import com.videotron.tvi.illico.cyo.popup.OptionScreen;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class NewChannelUI extends BaseUI implements CyoDataListener, MenuListener {
	public static final int STATE_LIST = 0x0;
	public static final int STATE_BUTTON = 0x1;

	private ListUI list = new NewChannelListUI();
	private ClickingEffect clickEffect;
	private int btnFocus = 0;

	private String sort;

	private ListUI currentList;

	public OptionScreen optScr;
	public static MenuItem sortOption = new MenuItem("menu.sort");
	public static MenuItem nSORT = new MenuItem("menu.sorted.number");
	public static MenuItem aSORT = new MenuItem("menu.sorted.alphabetical");

	private Services services;

	public NewChannelUI() {
		renderer = new NewChannelRenderer();
		setRenderer(renderer);
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);
		prepare();

		sort = Param.SORT_ALPHABETICAL;

		Hashtable param = new Hashtable();
		param.put("short", sort);

		sortOption.clear();
		sortOption.add(aSORT);
		sortOption.add(nSORT);

		CyoDataManager.getInstance().request(URLType.LIST_CHANNEL, param);

		super.start(back);
	}

	public void prepare() {
		state = STATE_BUTTON;
		btnFocus = 0;
		focus = 0;
		list.setHasFocus(false);
		list.setCyoObject(null);
		setFooter();
		super.prepare();
	}

	public void stop() {
		super.stop();
		if (optScr != null) {
			optScr.stop();
			optScr.dispose();
		}
		optScr = null;
	}

	public void dispose() {
		list.stop();
		super.dispose();
	}

	public boolean handleKey(int code) {

		if (getPopup() != null && getPopup().isVisible()) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case OCRcEvent.VK_ENTER:
			keyEnter();
			return true;
		case KeyCodes.COLOR_A:
			footer.clickAnimation(1);
			if (optScr == null) {
				optScr = new OptionScreen();
			}

			if (sort == Param.SORT_NUMBER) {
				nSORT.setChecked(true);
				aSORT.setChecked(false);
			} else {
				nSORT.setChecked(false);
				aSORT.setChecked(true);
			}

			optScr.start(sortOption, this);

			return true;
		case KeyCodes.LAST:
			footer.clickAnimation(0);
			CyoController.getInstance().showUI(getLastUIId(), true);
			return true;
		default:
			return false;
		}
	}

	private void keyLeft() {
		if (state == STATE_BUTTON && currentList.getCyoObjectLength() > 0) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
		} else if (state == STATE_BUTTON) {
			state = STATE_LIST;
			btnFocus = 0;
		}
		repaint();

	}

	private void keyRight() {
		if (state == STATE_LIST) {
			state = STATE_BUTTON;
			currentList.setHasFocus(false);
			repaint();
		}
	}

	private void keyUp() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_UP);
		} else if (state == STATE_BUTTON) {
			if (--btnFocus < 0) {
				btnFocus = 0;
			}
		}
		repaint();
	}

	private void keyDown() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_DOWN);
		} else if (state == STATE_BUTTON) {
			if (++btnFocus > 1) {
				btnFocus = 1;
			}
		}
		repaint();
	}

	private void keyEnter() {

		if (state == STATE_BUTTON) {
			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}

			if (btnFocus == 0) { // Back
				clickEffect.start(718, 384, 172, 30);
				CyoController.getInstance().showUI(getLastUIId(), true);
			} else if (btnFocus == 1) { // Help
				// help
				clickEffect.start(718, 421, 172, 30);
				DataCenter.getInstance().put("MODE", "HELP");
				CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
			}
		}
		repaint();
	}

	private void setFooter() {
		footer.reset();
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
		// footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE),
		// BaseUI.getMenuText(KeyNames.BTN_SCROLL));
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A), BaseUI.getMenuText(KeyNames.BTN_SORTING));
	}

	public int getButtonFocus() {
		return btnFocus;
	}

	public ListUI getCurrentList() {
		return currentList;
	}

	public String getSortString() {
		if (sort.equals(Param.SORT_ALPHABETICAL)) {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_ALPHABETICAL);
		} else {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_CHANNEL_NUMBER);
		}
	}

	private void buildDataForList(CyoObject[] data) {
		Vector dataVector = new Vector();
		addCyoObject(dataVector, data);

		CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
		dataVector.copyInto(rearrangeData);

		list.setCyoObject(rearrangeData);
		// list.setHasFocus(true);
		list.start(true);

		dataVector.clear();

		currentList = list;
		currentList.setHasFocus(true);
		state = STATE_LIST;

		((NewChannelRenderer) renderer).update(this);
	}

	private void addCyoObject(Vector vec, CyoObject[] data) {

		if (lastUIId == UI_VALIDATION) {
			Vector channelList = new Vector();

			for (int i = 0; i < data.length; i++) {
				if (data[i] instanceof Channel) {
					if (!((Channel) data[i]).getSubscribedInSession() && ((Channel) data[i]).getSubscribedInAccount()) {
						channelList.add(data[i]);
					} else if (((Channel) data[i]).getSubscribedInSession() && !((Channel) data[i])
							.getSubscribedInAccount()) {
						channelList.add(data[i]);
					}
				}
			}

			if (channelList.size() > 0) {
				Collections.sort(channelList, CyoObjectComparator.comparator);
				for (int a = 0; a < channelList.size(); a++) {
					vec.add(channelList.elementAt(a));
				}
			}
		} else {
			Vector channelList = new Vector();
			Vector removeChannelList = new Vector();

			for (int i = 0; i < data.length; i++) {
				if (data[i] instanceof Channel) {
					if (!((Channel) data[i]).getSubscribedInSession() && ((Channel) data[i]).getSubscribedInAccount()) {
						removeChannelList.add(data[i]);
					} else {
						channelList.add(data[i]);
					}
				}
			}

			if (channelList.size() > 0) {
				Group group = new Group();
				group.setTitle(BaseUI.getMenuText(KeyNames.MY_CHANNEL_LIST));
				vec.add(group);
				Collections.sort(channelList, CyoObjectComparator.comparator);
				for (int a = 0; a < channelList.size(); a++) {
					vec.add(channelList.elementAt(a));
				}
			}

			if (removeChannelList.size() > 0) {
				Group group = new Group();
				group.setTitle(BaseUI.getMenuText(KeyNames.REMOVED_CHANNEL));
				vec.add(group);
				Collections.sort(removeChannelList, CyoObjectComparator.comparator);
				for (int a = 0; a < removeChannelList.size(); a++) {
					vec.add(removeChannelList.elementAt(a));
				}
			}
		}

	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);
		removePopup(pop);
		if (gotoHotKey) {
			gotoHotKey = false;
			super.popupClosed(pop, msg);
			return;
		}
		if (expired) {
			cyoReStart();
		} else if (pop instanceof NotificationPopupUI) {
			repaint();
		}
	}

	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("NewChannelUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = " + obj);
		}

		if (type == URLType.LIST_CHANNEL) {
			if (obj instanceof ChannelList) {
				if (obj.getSuccess().equals("1")) {
					ChannelList chList = (ChannelList) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug(
								"BasicChannelListUI: receiveCyoData: cyoData length = " + chList.getCyoObject().length);
					}

					CyoObject[] objs = chList.getCyoObject();
					buildDataForList(objs);
				} else {
					CyoObject[] objs = obj.getCyoObject();

					if (objs != null && objs[0] != null) {
						Message msg = (Message) objs[0];
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

						Log.printInfo("msg.getType() = " + msg.getType());
						if ((msg.getType()).equals("5")) {
							expired = true;
						}
					}
				}
			}
		} else if (type == URLType.LOAD_A_LA_CARTE) {

			if (obj.getSuccess().equals("1")) {
				if (obj instanceof Services) {
					Vector vData = new Vector();

					services = (Services) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: cyoData length = " + services
								.getCyoObject().length);
					}

					CyoObject[] groups = services.getCyoObject();
					Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: groups = " + groups);

					Vector vChannel = new Vector();

					if (groups != null) {
						CyoObject[] data = groups[0].getCyoObject();
						buildDataForList(data);
					}
					renderer.prepare(this);
					vData.clear();

				}
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					Log.printInfo("msg.getType() = " + msg.getType());
					if ((msg.getType()).equals("5")) {
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						expired = true;
					}
				}
			}

		}
	}

	public void canceled() {
		// TODO Auto-generated method stub

	}

	public void selected(MenuItem item) {
		// sort
		Hashtable param = new Hashtable();
		boolean checkSort = false;
		if (item.equals(nSORT)) {
			param.put("sort", Param.SORT_NUMBER);
			checkSort = sort == Param.SORT_NUMBER ? true : false;
			sort = Param.SORT_NUMBER;
		} else {
			param.put("sort", Param.SORT_ALPHABETICAL);
			checkSort = sort == Param.SORT_ALPHABETICAL ? true : false;
			sort = Param.SORT_ALPHABETICAL;
		}
		if (!checkSort) {
			CyoDataManager.getInstance().request(URLType.LIST_CHANNEL, param);
		}
	}

	public void receiveCyoError(int type) {
	}
}
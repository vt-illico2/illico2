/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Enumeration;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.debug.DebugScreenManager;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.cyo.gui.ListRenderer;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * ListUI display a list of service, group, package, channel etc.
 * @author Woojung Kim
 * @version 1.1
 */
public class ListUI extends BaseUI {

    public static final int FOCUS_NONE = 0;
    public static final int FOCUS_ON_PARENT = 1;
    public static final int FOCUS_ON_CHILD = 2;
    public static final int FOCUS_ON_ME = 3;

    protected int startIdx;
    protected boolean hasFocus;
    protected boolean withPrice;
    protected int descStartIdx;
    protected boolean isBasicService;

    protected String[] currentDesc;
    private CyoObject lastObject;

    private boolean focusOnViewChanenlIncluded;

    public ListUI(boolean withPrice) {
        renderer = new ListRenderer();
        setRenderer(renderer);
        this.withPrice = withPrice;
        isBasicService = false;
    }
    
    public ListUI(boolean withPrice, boolean isBasicService) {
        renderer = new ListRenderer();
        setRenderer(renderer);
        this.withPrice = withPrice;
        this.isBasicService = isBasicService;
    }

    /** The current items of List. */
    protected CyoObject[] objs;

    public void start(boolean back) {
        prepare();

        startIdx = 0;
        focus = 0;
        descStartIdx = 0;
        focusOnViewChanenlIncluded = false;
        
        int lastIndex = startIdx + focus;
        while (getCyoObject(startIdx + focus) instanceof Group) {
        	Log.printDebug("ListUI: start(): startIdx = " + startIdx + ", focus = " + focus);
        	keyDown();
        	
        	if (lastIndex == (startIdx + focus)) {
        		break;
        	} else {
        		lastIndex = startIdx + focus;
        	}
        }
       
        super.start(back);
    }

    public void stop() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                dataCenter.removeImage(key);
            }
            imageKeys.clear();
        }

        super.stop();
    }

    public boolean handleKey(int code) {
        CyoObject curObj;

        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();

            // handle a case of group
            curObj = getCyoObject(startIdx + focus);
            if (curObj instanceof Group) {
                if (startIdx == 0 && focus == 0) {
                	while (getCyoObject(startIdx + focus) instanceof Group) {
                    	keyDown();
                    }
                } else {
                	while (getCyoObject(startIdx + focus) instanceof Group && !(startIdx == 0 && focus == 0)) {
                    	keyUp();
                    }
                	
                	if ((startIdx == 0 && focus == 0) && getCyoObject(startIdx + focus) instanceof Group) {
	                	while (getCyoObject(startIdx + focus) instanceof Group) {
	                    	keyDown();
	                	}
                    }
                }
            }
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();

            // handle a case of group
            while (getCyoObject(startIdx + focus) instanceof Group) {
            	keyDown();
            }
            return true;
        case OCRcEvent.VK_PAGE_UP:
            if (currentDesc != null) {
                if (--descStartIdx < 0) {
                    descStartIdx = 0;
                }
            }
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            if (currentDesc != null) {
                if (++descStartIdx > currentDesc.length - 3) {
                    descStartIdx--;
                }
            }
            return true;
        case OCRcEvent.VK_ENTER:
            return true;
        default:
            return false;
        }
    }

    private void keyUp() {
        if (objs != null) {
            if (startIdx + focus >= objs.length) {
                if (needViewButton() && focusOnViewChanenlIncluded) {
                    focusOnViewChanenlIncluded = false;
                } else {
                    startIdx--;
                    if (needViewButton() && !focusOnViewChanenlIncluded) {
                        focusOnViewChanenlIncluded = true;
                    }
                }
            } else if (startIdx + focus > 0) {
                if (focus > 3) {
                    if (needViewButton() && focusOnViewChanenlIncluded) {
                        focusOnViewChanenlIncluded = false;
                    } else {
                        focus--;
                        if (needViewButton() && !focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = true;
                        }
                    }
                } else if (startIdx > 0) {
                    if (focus < 4) {
                        if (needViewButton() && focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = false;
                        } else {
                            startIdx--;
                            if (needViewButton() && !focusOnViewChanenlIncluded) {
                                focusOnViewChanenlIncluded = true;
                            }
                        }
                    } else {
                        if (needViewButton() && focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = false;
                        } else {
                            focus--;
                            if (needViewButton() && !focusOnViewChanenlIncluded) {
                                focusOnViewChanenlIncluded = true;
                            }
                        }
                    }
                } else {
                    if (needViewButton() && focusOnViewChanenlIncluded) {
                        focusOnViewChanenlIncluded = false;
                    } else {
                        focus--;
                        if (needViewButton() && !focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = true;
                        }
                    }
                }
            } else {
                if (needViewButton() && focusOnViewChanenlIncluded) {
                    focusOnViewChanenlIncluded = false;
                }
            }
        } else {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("ListUI: keyUp(): objs is null");
        	}
        }
    }

    private void keyDown() {
        if (objs != null) {
            if (startIdx + focus < objs.length - 1) {
                if (focus < 3) {
                    if (needViewButton() && !focusOnViewChanenlIncluded) {
                        focusOnViewChanenlIncluded = true;
                    } else {
                        focus++;
                        focusOnViewChanenlIncluded = false;
                    }
                } else if (startIdx + 7 < objs.length) {
                    if (focus > 2) {
                        if (needViewButton() && !focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = true;
                        } else {
                            startIdx++;
                            focusOnViewChanenlIncluded = false;
                        }
                    } else {
                        if (needViewButton() && !focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = true;
                        } else {
                            focus++;
                            focusOnViewChanenlIncluded = false;
                        }
                    }
                } else {
                    if (startIdx + focus > objs.length - 7) {
                        if (needViewButton() && !focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = true;
                        } else {
                            focus++;
                            focusOnViewChanenlIncluded = false;
                        }
                    } else {
                        if (needViewButton() && !focusOnViewChanenlIncluded) {
                            focusOnViewChanenlIncluded = true;
                        } else {
                            startIdx++;
                            focus++;
                            if (startIdx + focus >= objs.length) {
                                startIdx--;
                            }
                            focusOnViewChanenlIncluded = false;
                        }
                    }
                }
            } else {
                if (needViewButton() && !focusOnViewChanenlIncluded) {
                    focusOnViewChanenlIncluded = true;
                }
            }
        } else {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("ListUI: keyDown(): objs is null");
        	}
        }
    }

    public boolean needViewButton() {
        if (getCurrentCyoObject() instanceof Package && getCurrentCyoObject().getCyoObject() != null) {
            return true;
        }

        return false;
    }

    public int getStartIdx() {
        return startIdx;
    }

    public CyoObject getCyoObject(int idx) {
        if (objs != null && idx < objs.length && objs[idx] != null) {
            DebugScreenManager.getInstance().addData(objs[idx]);
            return objs[idx];
        }

        return null;
    }
    
    public CyoObject[] getAllCyoObject() {
        return objs;
    }

    public CyoObject getCurrentCyoObject() {
        int idx = startIdx + focus;
        return getCyoObject(idx);
    }

    public int getCyoObjectLength() {
        if (objs != null) {
            return objs.length;
        } else {
            return 0;
        }
    }

    public void setCyoObject(CyoObject[] cyoObjs) {
        this.objs = cyoObjs;
    }

    public boolean hasFocus() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
        repaint();
    }

    public boolean onFocus(int idx) {
        if (startIdx + focus == idx) {
            return true;
        }

        return false;
    }

    public boolean isWithPrice() {
        return withPrice;
    }
    
    public boolean isBasicService() {
    	return isBasicService;
    }
    
    public void setIsBasicService(boolean flag) {
    	this.isBasicService = flag;
    }

    public String[] getDesc() {
        if (Log.INFO_ON) {
            // Log.printInfo("ListUI: getDesc: startIdx = " + startIdx + "\t focus = " + focus);
        }
        String src = null;
        CyoObject currentObj = getCyoObject(startIdx + focus);

        if (lastObject == null || lastObject != currentObj) {
            descStartIdx = 0;
            if (currentObj instanceof Channel) {
                Channel chInfoObj = CyoDataManager.getInstance()
                        .getChannelInfoById(((Subscription) currentObj).getId());
                if (chInfoObj != null) {
                    src = chInfoObj.getDesc();
                }
            } else {
                src = ((Subscription) currentObj).getDesc();
            }
            lastObject = currentObj;

            if (src != null) {
                currentDesc = TextUtil.split(src, BaseRenderer.fm14d, 530);
                return currentDesc;
            } else {
                currentDesc = null;
            }
        } else if (currentDesc != null) {
            return currentDesc;
        }

        return null;
    }

    public int getScrollGap() {
        if (descStartIdx == 0) {
            return 0;
        } else {
            int gap = 21 * (descStartIdx * 100 / (currentDesc.length - 3)) / 100;
            return gap;
        }
    }

    public int getDescStartIdx() {
        return descStartIdx;
    }

    public int getFocusType(int idx) {
        CyoObject obj = getCyoObject(idx);
        CyoObject currentObj = getCyoObject(startIdx + focus);

//        if (obj instanceof Package) {
//
//            if (obj == currentObj.getParentObj()) {
//                return FOCUS_ON_CHILD;
//            } else if (obj == currentObj) {
//                return FOCUS_ON_ME;
//            } else {
//                return FOCUS_NONE;
//            }
//        } else {
//            if (obj.getParentObj() == currentObj) {
//                return FOCUS_ON_PARENT;
//            } else if (obj == currentObj) {
//                return FOCUS_ON_ME;
//            } else if (obj.getParentObj() == currentObj.getParentObj()) {
//                return FOCUS_ON_CHILD;
//            } else {
//                return FOCUS_NONE;
//            }
//        }

        if (obj == currentObj) {
            if (obj instanceof Package) {
                if (focusOnViewChanenlIncluded) {
                    return FOCUS_ON_CHILD;
                }
                return FOCUS_ON_ME;
            } else {
                return FOCUS_ON_ME;
            }
        } else {
            return FOCUS_NONE;
        }
    }

    public boolean isDisplayTopDeco() {
        if (startIdx > 0) {
            return true;
        }
        return false;
    }

    public boolean isDisplayBottomDeco() {
        if (objs == null) {
            return false;
        }
        if (startIdx < objs.length - 7) {
            return true;
        }

        return false;
    }

    public String getSelectedChannel() {
        int count = 0;

        if (objs != null) {
            for (int i = 0; i < objs.length; i++) {
                if(objs[i] instanceof Package && ((Subscription) objs[i]).getSubscribedInSession() && ((Package) objs[i]).getModifiable().equals("0")) {
                    count++;
                } else if (objs[i] instanceof Channel && ((Subscription) objs[i]).getSubscribedInSession()) {
                    if(!(objs[i].getParentObj() instanceof Package) && (((Subscription) objs[i]).getModifiable().equals("0") || ((Subscription) objs[i]).getModifiable().equals("3"))) {
                        count++;
                    } 
                } else if (objs[i] instanceof Product && ((Subscription) objs[i]).getSubscribedInSession()) { // VDTRMASTER-5920
                    count++;
                }
            }
        }

        return String.valueOf(count);
    }

    public String getSelectedChannelForPremium() {
        int count = 0;

        if (objs != null) {
            for (int i = 0; i < objs.length; i++) {
                if(objs[i] instanceof Package && ((Subscription) objs[i]).getSubscribedInSession() && (((Package) objs[i]).getModifiable().equals("0") || ((Package) objs[i]).getModifiable().equals("2"))) {
                    count++;
                } else if (objs[i] instanceof Channel && ((Subscription) objs[i]).getSubscribedInSession()) {
                    if(!(objs[i].getParentObj() instanceof Package) && (((Subscription) objs[i]).getModifiable().equals("0") || ((Subscription) objs[i]).getModifiable().equals("3"))) {
                        count++;
                    }
                } else if (objs[i] instanceof Product && ((Subscription) objs[i]).getSubscribedInSession()) { // VDTRMASTER-5920
                    count++;
                }
            }
        }

        return String.valueOf(count);
    }

    public String getSelectedGame() {
        int count = 0;

        if (objs != null) {
            for (int i = 0; i < objs.length; i++) {
                if (objs[i] instanceof Game && ((Subscription) objs[i]).getSubscribedInSession()) {
                    count++;
                }
            }
        }

        return String.valueOf(count);
    }

    // Bundling
    public boolean isFocusOnViewChanenlIncluded() {
        return focusOnViewChanenlIncluded;
    }
}

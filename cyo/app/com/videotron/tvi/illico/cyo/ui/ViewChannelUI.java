/**
 * @(#)ChangeChannelSelectionUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObjectComparator;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Services;
import com.videotron.tvi.illico.cyo.gui.ViewChannelRenderer;
import com.videotron.tvi.illico.cyo.popup.MenuItem;
import com.videotron.tvi.illico.cyo.popup.MenuListener;
import com.videotron.tvi.illico.cyo.popup.OptionScreen;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ViewChannelUI extends BaseUI implements CyoDataListener, MenuListener {
    public static final int STATE_LIST = 0x0;
    public static final int STATE_BUTTON = 0x1;

    private ListUI customList = new ChannelListUI();

    private Package curPackage;

    private int btnFocus = 0;

    private String sort;

    private ListUI currentList;

    public OptionScreen optScr;
    public static MenuItem sortOption = new MenuItem("menu.sort");
    public static MenuItem nSORT = new MenuItem("menu.sorted.number");
    public static MenuItem aSORT = new MenuItem("menu.sorted.alphabetical");

    public ViewChannelUI() {
        renderer = new ViewChannelRenderer();
        setRenderer(renderer);
    }

    public void start(boolean back) {
        if (!back) {
            sort = Param.SORT_ALPHABETICAL;
            curPackage = (Package) DataCenter.getInstance().get("Package");
            if (curPackage != null) {
                buildDataForList(curPackage.getCyoObject());
            }
            prepare();
        }

        sortOption.clear();
        sortOption.add(aSORT);
        sortOption.add(nSORT);

        CyoDataManager.getInstance().addCyoDataListener(this);
        super.start(back);
    }

    public void prepare() {
        state = STATE_LIST;
        btnFocus = 0;
        focus = 0;
        setFooter();
        super.prepare();
    }

    public void stop() {
        if (optScr != null) {
            optScr.stop();
            optScr.dispose();
        }
        optScr = null;
        super.stop();
    }

    public void dispose() {
        super.dispose();
    }

    public boolean handleKey(int code) {
        
        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_LEFT:
            keyLeft();
            return true;
        case OCRcEvent.VK_RIGHT:
            keyRight();
            return true;
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case OCRcEvent.VK_PAGE_UP:
            pageUp();
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            pageDown();
            return true;
        case OCRcEvent.VK_ENTER:
            keyEnter();
            return true;
        case KeyCodes.COLOR_A:
            footer.clickAnimation(2); 
            if (optScr == null) {
                optScr = new OptionScreen();
            }
            if (sort == Param.SORT_NUMBER) {
                nSORT.setChecked(true);
                aSORT.setChecked(false);
            } else {
                nSORT.setChecked(false);
                aSORT.setChecked(true);
            }
            optScr.start(sortOption, this);
            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0); 
//            CyoController.getInstance().showUI(BaseUI.UI_POPULAR_PACKAGES, true);
            CyoController.getInstance().showUI(lastUIId, true);
            return true;
        default:
            return false;
        }
    }

    private void keyLeft() {
        if (state == STATE_BUTTON && currentList.getCyoObjectLength() > 0) {
            state = STATE_LIST;
            currentList.setHasFocus(true);
        } else if (state == STATE_BUTTON) {
            state = STATE_LIST;
            btnFocus = 0;
        }
        repaint();

    }

    private void keyRight() {
        if (state == STATE_LIST) {
            state = STATE_BUTTON;
            currentList.setHasFocus(false);
            repaint();
        }
    }

    private void keyUp() {
        if (state == STATE_LIST) {
            currentList.handleKey(OCRcEvent.VK_UP);
        } else if (state == STATE_BUTTON) {
            if (--btnFocus < 0) {
                btnFocus = 0;
            }
        }
        repaint();
    }

    private void keyDown() {
        if (state == STATE_LIST) {
            currentList.handleKey(OCRcEvent.VK_DOWN);
        } else if (state == STATE_BUTTON) {
            if (++btnFocus > 1) {
                btnFocus = 1;
            }
        }
        repaint();
    }

    private void pageUp() {
        if (state == STATE_LIST) {
            footer.clickAnimation(1); 
            currentList.handleKey(OCRcEvent.VK_PAGE_UP);
            repaint();
        }
    }

    private void pageDown() {
        if (state == STATE_LIST) {
            footer.clickAnimation(1); 
            currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
            repaint();
        }
    }

    private void keyEnter() {
        if (state == STATE_BUTTON) {
            if (btnFocus == 0) { // Back
                //CyoController.getInstance().showUI(BaseUI.UI_POPULAR_PACKAGES, true);
            	CyoController.getInstance().showUI(lastUIId, true);
            } else if (btnFocus == 1) { // Help
                // help
                DataCenter.getInstance().put("MODE", "HELP");
                CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
            }
        }
        repaint();
    }

    private void setFooter() {
        footer.reset();
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A), BaseUI.getMenuText(KeyNames.BTN_SORTING));
    }

    public int getButtonFocus() {
        return btnFocus;
    }

    public ListUI getCurrentList() {
        return currentList;
    }

    public Package getCurrentPackage() {
        return curPackage;
    }

    private void buildDataForList(CyoObject[] data) {
        Vector dataVector = new Vector();
        addCyoObject(dataVector, data);

        CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
        dataVector.copyInto(rearrangeData);

        customList.setCyoObject(rearrangeData);
        customList.setHasFocus(true);
        customList.start(true);

        dataVector.clear();

        currentList = customList;

    }

    private void addCyoObject(Vector vec, CyoObject[] data) {

        for (int i = 0; i < data.length; i++) {
            vec.add(data[i]);
        }
    }

    public String getSortString() {
        if (sort.equals(Param.SORT_ALPHABETICAL)) {
            return BaseUI.getMenuText(KeyNames.SORTED_BY_ALPHABETICAL);
        } else {
            return BaseUI.getMenuText(KeyNames.SORTED_BY_CHANNEL_NUMBER);
        }
    }

    public void popupClosed(BaseUI pop, Object msg) {
        super.popupClosed(pop, msg);
        removePopup(pop);
        if(gotoHotKey) {
            gotoHotKey = false;
            super.popupClosed(pop, msg);
            return;
        }
        if (expired) {
            cyoReStart();
        }
        repaint();
    }

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.INFO_ON) {
            Log
                    .printInfo("ViewChannelUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
                            + obj);
        }

        if (type == URLType.LOAD_GROUP) {
            if (obj.getSuccess().equals("1")) {
                if (obj instanceof Services) {
                    Services services = (Services) obj;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ViewChannelUI: receiveCyoData: cyoData length = "
                                + services.getCyoObject().length);
                    }
                    CyoObject[] groups = services.getCyoObject();
                    if (groups[0] instanceof Group) {
                        renderer.prepare(this);
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ViewChannelUI: receiveCyoData: groups length = " + groups.length);
                            Log.printDebug("ViewChannelUI: receiveCyoData: groups[0].getCyoObject length = "
                                    + groups[0].getCyoObject().length);
                        }

                        CyoObject[] packages = groups[0].getCyoObject();
                        Log.printDebug("ViewChannelUI: receiveCyoData: packages = " + packages);
                        Log.printDebug("ViewChannelUI: receiveCyoData: curPackage = " + curPackage);
                        for (int i = 0; i < packages.length; i++) {
                            Package pck = (Package) packages[i];
                            Log.printDebug("ViewChannelUI: receiveCyoData: pck = " + pck.getNo());
                            Log.printDebug("ViewChannelUI: receiveCyoData: curPackage = " + curPackage.getNo());
                            if (pck.getNo().equals(curPackage.getNo())) {
                                curPackage = pck;
                                break;
                            }
                        }
                        buildDataForList(curPackage.getCyoObject());
                    } else {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ViewChannelUI: receiveCyoData: obj = " + obj);
                        }
                    }
                }
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

                    Log.printInfo("msg.getType() = " + msg.getType());
                    if ((msg.getType()).equals("5")) {
                        expired = true;
                    }
                }
            }
        }
    }

    public void canceled() {
        // TODO Auto-generated method stub

    }

    public void selected(MenuItem item) {

        // sort
        Group parentGroup = (Group) curPackage.getParentObj();
        Hashtable param = new Hashtable();
        param.put("groupNo", parentGroup.getNo());
        boolean checkSort = false;
        if (item.equals(nSORT)) {
            param.put("sort", Param.SORT_NUMBER);
            checkSort = sort == Param.SORT_NUMBER ? true : false;
            sort = Param.SORT_NUMBER;
        } else {
            param.put("sort", Param.SORT_ALPHABETICAL);
            checkSort = sort == Param.SORT_ALPHABETICAL ? true : false;
            sort = Param.SORT_ALPHABETICAL;
        }
        if (!checkSort) {
//            CyoDataManager.getInstance().request(URLType.LOAD_A_LA_CARTE, param);
            CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
        }
    }

    public void receiveCyoError(int type) {
        if (type == URLType.LOAD_GROUP) {
            sort = sort == Param.SORT_NUMBER ? Param.SORT_ALPHABETICAL : Param.SORT_NUMBER;
        }
    }
}

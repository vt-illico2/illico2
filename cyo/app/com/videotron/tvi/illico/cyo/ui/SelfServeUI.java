/**
 * @(#)ChangeChannelSelectionUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.gui.SelfServeRenderer;
import com.videotron.tvi.illico.cyo.popup.MenuItem;
import com.videotron.tvi.illico.cyo.popup.MenuListener;
import com.videotron.tvi.illico.cyo.popup.OptionScreen;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * CYO_021 & CYO_022
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class SelfServeUI extends BaseUI implements CyoDataListener, MenuListener {
	public static final int STATE_LIST = 0x0;
	public static final int STATE_BUTTON = 0x1;

	private ClickingEffect clickEffect;
	private ListUI customList = new ListUI(true);

	private Vector messagePopupVector = new Vector();

	private ListUI listUI;
	private Group loadedGroup;

	private Vector addVector = new Vector();
	private Vector removeVector = new Vector();
	private Vector clonedAddVector = new Vector();

	private boolean isModified = false;

	private int btnFocus = 0;
	private Services services;

	private ListUI currentList;

	private String sort;

	public OptionScreen optScr;
	public static MenuItem sortOption = new MenuItem("menu.sort");
	public static MenuItem nSORT = new MenuItem("menu.sorted.number");
	public static MenuItem aSORT = new MenuItem("menu.sorted.alphabetical");
	private boolean saveMessageView = false;

	private String loadGroupTitle;

	private boolean channelInfoBoolean = false;

	private boolean startLoad = false;

	private int lastRequestedURLType = URLType.LOAD_A_LA_CARTE;
	private CyoObject lastObject;
	private Message lastMessage;
	private boolean needParamForRequiresUpsell = false;
	
	public SelfServeUI() {
		renderer = new SelfServeRenderer();
		setRenderer(renderer);
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);
		startLoad = true;
		needParamForRequiresUpsell = false;
		if (!back) {
			prepare();
			sort = Param.SORT_ALPHABETICAL;
			currentList = null;
			CyoObject obj = (CyoObject) DataCenter.getInstance().get("SelfServe");

			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: start: obj = " + obj);
			}

			if (obj != null) {
				Group group = (Group) obj;
				Hashtable param = new Hashtable();
				param.put("groupNo", group.getNo());
				param.put("sort", sort);
				CyoDataManager.getInstance().request(URLType.LOAD_A_LA_CARTE, param);
			} else {
				String url = CyoDataManager.getInstance().popUrl();

				if (Log.DEBUG_ON) {
					Log.printDebug("SelfServeUI: start: url = " + url);
				}

				if (url != null) {
					Hashtable param = new Hashtable();
					param.put("url", url);
					CyoDataManager.getInstance().request(URLType.LOAD_A_LA_CARTE, param);
				}
			}
			
			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: start: before clonedAddVector size = " + clonedAddVector.size());
			}
			addVector.clear();
			removeVector.clear();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: start: after clonedAddVector size = " + clonedAddVector.size());
			}
		}
		if (messagePopupVector == null) {
			messagePopupVector = new Vector();
		}
		channelInfoBoolean = false;
		saveMessageView = false;
		sortOption.clear();
		sortOption.add(aSORT);
		sortOption.add(nSORT);

		super.start(back);
	}

	public void prepare() {
		btnFocus = 0;
		focus = 0;
		state = STATE_LIST;
		isModified = false;
		setFooter();
		super.prepare();
	}

	public void stop() {
		if (optScr != null) {
			optScr.stop();
			optScr.dispose();
		}
		if (messagePopupVector != null) {
			messagePopupVector.clear();
			messagePopupVector = null;
		}
		startLoad = false;
		optScr = null;
		clearPopup();
		super.stop();
	}

	public void dispose() {
		clickEffect = null;
		addVector.clear();
		removeVector.clear();
		
		customList.stop();

		super.dispose();
	}

	public boolean handleKey(int code) {

		if (getPopup() != null && getPopup().isVisible()) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				Log.printInfo("SelfServeUI code= " + code);
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case OCRcEvent.VK_PAGE_UP:
			pageUp();
			return true;
		case OCRcEvent.VK_PAGE_DOWN:
			pageDown();
			return true;
		case OCRcEvent.VK_ENTER:
			keyEnter();
			return true;
		case KeyCodes.LAST:
			if (getChanged()) {// addVector removeVector
				footer.clickAnimation(0);
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			} else {
				footer.clickAnimation(0);
				CyoController.getInstance().showUI(lastUIId, true);

			}
			repaint();
			return true;
		case KeyCodes.COLOR_A:
			if (currentList.equals(customList)) {
				footer.clickAnimation(2);
				if (optScr == null) {
					optScr = new OptionScreen();
				}

				if (sort == Param.SORT_NUMBER) {
					nSORT.setChecked(true);
					aSORT.setChecked(false);
				} else {
					nSORT.setChecked(false);
					aSORT.setChecked(true);
				}

				optScr.start(sortOption, this);
			}
			return true;
		case KeyCodes.COLOR_B:
			// deselect
			if (currentList.equals(customList)) {
				// if (addVector.size() > 0) {
				footer.clickAnimation(3);
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL_CANCEL,
						BaseUI.getMenuText(KeyNames.UNSELECT_CHANNEL_TITLE),
						BaseUI.getExplainText(KeyNames.UNSELECT_CHANNEL));
				// setPopup(notiPop);
				// notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
				repaint();
			}
			return true;
		default:
			return false;
		}
	}

	private void keyLeft() {
		if (state == STATE_BUTTON) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
			btnFocus = 0;
			repaint();
		}

	}

	private void keyRight() {
		if (state == STATE_LIST) {
			state = STATE_BUTTON;
			currentList.setHasFocus(false);
			if (!getChanged()) {
				btnFocus = 1;
			} else {
				btnFocus = 0;
			}
			repaint();
		}
	}

	private void keyUp() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_UP);
			repaint();
		} else if (state == STATE_BUTTON) {
			--btnFocus;
			if (!getChanged() && btnFocus < 1) {
				btnFocus = 1;
			} else if (btnFocus < 0) {
				btnFocus = 0;
			}

			repaint();
		}
	}

	private void keyDown() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_DOWN);
			repaint();
		} else if (state == STATE_BUTTON) {
			if (++btnFocus > 2) {
				btnFocus = 2;
			}
			repaint();
		}
	}

	private void pageUp() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_UP);
			repaint();
		}
	}

	private void pageDown() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
			repaint();
		}
	}

	private void keyEnter() {
		if (state == STATE_LIST) {
			CyoObject curObj = currentList.getCurrentCyoObject();
			if (curObj instanceof Package && currentList.isFocusOnViewChanenlIncluded()) {
				// show included channels popup
				if (Log.DEBUG_ON) {
					Log.printDebug("SelfServeUI, keyEnter, show included channels popup");
				}

				setPopup(includedChannelsPopup);
				includedChannelsPopup.show(this, (Package) curObj);
			} else {
				updateSubscribed(curObj);
				needExitPopup = true;
			}
		} else if (state == STATE_BUTTON) {

			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}

			clickEffect.start(717, 346 + btnFocus * 37, 180, 35);

			if (btnFocus == 0) { // Confirm
				// VDTRMASTER-5958
				CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
				requestSaveAction();
			} else if (btnFocus == 1) { // Cancel
				if (getChanged()) {
					setPopup(notiPop);
					notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
				} else {
					CyoController.getInstance().showUI(lastUIId, true);
				}
			} else if (btnFocus == 2) { // Help
				DataCenter.getInstance().put("MODE", "HELP");
				CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
			}
		}
		repaint();
	}

	private void setFooter() {
		footer.reset();
		if (focus == 0) {
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE),
					BaseUI.getMenuText(KeyNames.BTN_SCROLL));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A), BaseUI.getMenuText(KeyNames.BTN_SORTING));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_B),
					BaseUI.getMenuText(KeyNames.BTN_DESELECT_ALL));
		} else {
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
		}
	}

	public String getGroupName() {
		if (loadGroupTitle != null) {
			return loadGroupTitle;
		}
		return TextUtil.EMPTY_STRING;
	}

	private void updateSubscribed(CyoObject obj) {
		if (obj instanceof Package) {
			Package curPackage = ((Package) obj);
			String modiable = curPackage.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: Package modiable = " + modiable);
			}
			if (modiable.equals("0")) {
				// Package
				curPackage.setSubscribedInSession(!curPackage.getSubscribedInSession());
				CyoObject[] childObjs = curPackage.getCyoObject();

				for (int i = 0; childObjs != null && i < childObjs.length; i++) {
					Subscription child = (Subscription) childObjs[i];
					if (child.getModifiable().equals("1")) {
						child.setSubscribedInSession(curPackage.getSubscribedInSession());
					}
				}

				if (curPackage.getSubscribedInSession()) {
					addCyoObjectInMyPicks(obj);
				} else {
					removeCyoObjectInMyPicks(obj);
				}

				if (focus < 1) {
					isModified = true;
				}
			} else if (modiable.equals("1")) {
				channelInfoBoolean = true;
				Log.printInfo("UNSELECT_MESSAGE_1");
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else if (modiable.equals("2")) {
				channelInfoBoolean = true;
				Log.printInfo("UNSELECT_MESSAGE_2");
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else if (modiable.equals("3")) {
				channelInfoBoolean = true;
				Log.printInfo("UNSELECT_MESSAGE_3");
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			}
		} else if (obj instanceof Subscription) {
			Subscription subObj = ((Subscription) obj);
			String modiable = subObj.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: Channel modiable = " + modiable);
			}

			CyoObject parentObj = subObj.getParentObj();
			if (parentObj instanceof Package) {
				Package parentPackage = (Package) parentObj;
				String pModiable = parentPackage.getModifiable();
				if (Log.DEBUG_ON) {
					Log.printDebug("SelfServeUI: Parent Package modiable = " + pModiable);
				}
				if (pModiable.equals("0") && modiable.equals("0")) {
					/*
					 * Package = 0; Channel = 0: The user can add / remove a
					 * package, as well as channels in it. In this case, the
					 * user can do anything (add/remove a package, add/remove a
					 * channel from within the package with no consequences)
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(obj);
					} else {
						removeCyoObjectInMyPicks(obj);
					}
					if (focus < 1) {
						isModified = true;
					}
				} else if (pModiable.equals("0") && !modiable.equals("0")) {
					/*
					 * Package = 0; Channel 1: The user can remove/add the
					 * package but not individual channels. If the user unchecks
					 * a channel, the whole package is automatically unchecked
					 * with all other channels in it.
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());
					parentPackage.setSubscribedInSession(subObj.getSubscribedInSession());
					CyoObject[] childObjs = parentPackage.getCyoObject();

					for (int i = 0; childObjs != null && i < childObjs.length; i++) {
						Subscription child = (Subscription) childObjs[i];
						if (child.getModifiable().equals("1")) {
							child.setSubscribedInSession(parentPackage.getSubscribedInSession());
						}
					}

					if (parentPackage.getSubscribedInSession()) {
						addCyoObjectInMyPicks(parentPackage);
					} else {
						removeCyoObjectInMyPicks(parentPackage);
					}
					if (focus < 1) {
						isModified = true;
					}
				} else if (!pModiable.equals("0") && modiable.equals("0")) {
					/*
					 * Package = 1; Channel 0: The user cannot add/remove the
					 * package, but he can change individual channels in it. If
					 * the user checks/unckecks a channel, it has an effect only
					 * on that channel.
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(subObj);
					} else {
						removeCyoObjectInMyPicks(subObj);
					}
					if (focus < 1) {
						isModified = true;
					}
				} else if (modiable.equals("1")) {
					channelInfoBoolean = true;
					Log.printInfo("UNSELECT_MESSAGE_1");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("2")) {
					channelInfoBoolean = true;
					Log.printInfo("UNSELECT_MESSAGE_2");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("3")) {
					channelInfoBoolean = true;
					Log.printInfo("UNSELECT_MESSAGE_3");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
			} else if (!(subObj instanceof Package || subObj instanceof Channel)) {
				Log.printInfo("UNSELECT_MESSAGE_4");
				channelInfoBoolean = true;
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_4));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else {
				Log.printInfo("modiable = " + modiable);
				Log.printInfo("no = " + subObj.getNo());

				if (modiable.equals("0")) {
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());
					Log.printInfo("subObj.getSubscribedInSession() : " + subObj.getSubscribedInSession());
					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(subObj);
					} else {
						removeCyoObjectInMyPicks(subObj);
					}
					if (focus < 1) {
						isModified = true;
					}
				}  else if (subObj.isSubscriptionTypeOfALaCarte()) {
					Log.printDebug("UNSELECT_MESSAGE_FOR_A_LA_CARTE");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_FOR_A_LA_CARTE));
					notiPop.show(this, NotificationPopupUI.STATE_UNSELECT_MESSAGE_FOR_A_LA_CARTE);
				} else if (modiable.equals("1")) {
					channelInfoBoolean = true;
					Log.printInfo("UNSELECT_MESSAGE_1");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("2")) {
					channelInfoBoolean = true;
					Log.printInfo("UNSELECT_MESSAGE_2");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("3")) {
					channelInfoBoolean = true;
					Log.printInfo("UNSELECT_MESSAGE_3");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
				repaint();
			}
		}
		((SelfServeRenderer)renderer).update(this);
	}

	public int getButtonFocus() {
		return btnFocus;
	}

	public ListUI getCurrentList() {
		return currentList;
	}

	private void requestSaveAction() {
		String addParams = null, removeParams = null, groupNo = null;
		Hashtable param = new Hashtable();

		addParams = getNumbers(addVector);
		removeParams = getNumbers(removeVector);
		groupNo = loadedGroup.getNo();

		if (addParams != null) {
			param.put("add", addParams);
		}
		if (removeParams != null) {
			param.put("remove", removeParams);
		}
		param.put("groupNo", groupNo);

		if (needParamForRequiresUpsell) {
			param.put("requiresUpsell", "false");
		}

		if (addParams != null || removeParams != null) {
			clonedAddVector = (Vector) addVector.clone();
			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: requestSaveAction: addVector size = " + addVector.size());
				Log.printDebug("SelfServeUI: requestSaveAction: clonedAddVector size = " + clonedAddVector.size());
				Log.printDebug("SelfServeUI: requestSaveAction: needParamForRequiresUpsell = "
						+ needParamForRequiresUpsell);
			}
			
			saveMessageView = false;
			DataCenter.getInstance().put("ADDED_CUSTOM_CHANNELS", clonedAddVector);
			CyoDataManager.getInstance().request(URLType.MODIFY_A_LA_CARTE, param);
		}

	}

	private String getNumbers(Vector vec) {
		if (vec.size() == 0) {
			return null;
		}

		StringBuffer sf = new StringBuffer();

		for (int i = 0; i < vec.size(); i++) {
			if (i > 0) {
				sf.append(",");
			}
			Subscription obj = (Subscription) vec.get(i);
			sf.append(obj.getNo());
		}

		return sf.toString();
	}

	private void buildDataForList(CyoObject[] data) {
		Vector dataVector = new Vector();
		addCyoObject(dataVector, data);

		CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
		dataVector.copyInto(rearrangeData);

		customList.setCyoObject(rearrangeData);
		customList.setHasFocus(true);
		customList.start(true);

		dataVector.clear();

		currentList = customList;

	}

	private void addCyoObject(Vector vec, CyoObject[] data) {

		for (int i = 0; i < data.length; i++) {
			Subscription ss = (Subscription) data[i];
			ss.setSubscribedOrginalInSession(String.valueOf(ss.getSubscribedInSession()));

			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI, addCyoObject, SubscribedOrginalInSession() " + " " + ss.getNo() + " = "
						+ ss.getSubscribedOrginalInSession());
				Log.printDebug("SelfServeUI, addCyoObject, SubscribedInSession() " + ss.getSubscribedInSession());
			}

			vec.add(data[i]);
//			if (data[i].getCyoObject() != null) {
//				addCyoObject(vec, data[i].getCyoObject());
//			}
		}
	}

	private void addCyoObjectInMyPicks(CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("SelfServeUI: addCyoObjectInMyPicks() " + obj);
		}
		
		Vector currentAddVector = null;
		Vector currentRemoveVector = null;

		if (Log.DEBUG_ON) {
			Log.printDebug("SelfServeUI: currentList : " + currentList);
			Log.printDebug("SelfServeUI: customList : " + customList);
		}
		if (currentList != null && currentList.equals(customList)) {
			currentAddVector = addVector;
			currentRemoveVector = removeVector;
		} else {
			// My Picks
			Group parentObj = (Group) obj.getParentObj();

			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: loadedGroup.getNo() : " + loadedGroup.getNo());
				Log.printDebug("SelfServeUI: parentObj.getNo() : " + parentObj.getNo());
			}

			if (loadedGroup.getNo().equals(parentObj.getNo())) {
				currentAddVector = addVector;
				currentRemoveVector = removeVector;
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("SelfServeUI: currentAddVector " + currentAddVector);
			Log.printDebug("SelfServeUI: currentRemoveVector " + currentRemoveVector);
		}

		
		
//		currentRemoveVector.remove(obj);
//		if (currentAddVector.contains(obj)) {
//			currentAddVector.remove(obj);
//			
//		}
//		
//		currentAddVector.add(obj);
		
		removeInVector(currentRemoveVector, ((Subscription)obj).getNo());
		removeInVector(currentAddVector, ((Subscription)obj).getNo());
		currentAddVector.add(obj);
	}

	private void removeCyoObjectInMyPicks(CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("SelfServeUI: removeCyoObjectInMyPicks() " + obj);
		}
		
		Vector currentAddVector = null;
		Vector currentRemoveVector = null;
		if (currentList != null && currentList.equals(customList)) {
			currentAddVector = addVector;
			currentRemoveVector = removeVector;
		} else {
			// My Picks
			Group parentObj = (Group) obj.getParentObj();

			if (loadedGroup.getNo().equals(parentObj.getNo())) {
				currentAddVector = addVector;
				currentRemoveVector = removeVector;
			}
		}

//		Log.printInfo("removeCyoObjectInMyPicks1 " + obj);
//		currentAddVector.remove(obj);
//		if (currentRemoveVector.contains(obj)) {
//			// if (currentRemoveVector.contains(obj)) {
//			Log.printDebug("removeCyoObjectInMyPicks2 ");
//			currentRemoveVector.remove(obj);
//		}
//		
//		currentRemoveVector.add(obj);
		
		removeInVector(currentAddVector, ((Subscription)obj).getNo());
		removeInVector(currentRemoveVector, ((Subscription)obj).getNo());
		currentRemoveVector.add(obj);
	}

	private CyoObject[] buildMyPicks() {
		Vector groupVec = new Vector();

		if (addVector.size() > 0) {
			CyoObject[] data = makeCyoObject(addVector);
			
			groupVec.add(loadedGroup);

			for (int i = 0; i < data.length; i++) {
//				Subscription subScriptionObj = (Subscription) data[i];
//				if (subScriptionObj.getSubscribedInSession() == subScriptionObj.getSubscribedOrginalInSession()) {
//					Log.printDebug("Subscription's subscribedInSession equals subscribedOriginalInSession.");
//				} else {
//					groupVec.add(data[i]);
//					count++;
//				}
				
				groupVec.add(data[i]);
			}
			//loadedGroup.setTitle(loadGroupTitle + " (" + count + ")");
			loadedGroup.setTitle(loadGroupTitle + " (" + data.length + ")");
		}

		CyoObject[] myPicksObjs = makeCyoObject(groupVec);

		return myPicksObjs;
	}

	private CyoObject[] makeCyoObject(Vector vec) {
		if (vec.size() == 0) {
			return null;
		}

		CyoObject[] objs = new CyoObject[vec.size()];
		vec.copyInto(objs);

		return objs;
	}

	public String getSortString() {
		if (sort.equals(Param.SORT_ALPHABETICAL)) {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_ALPHABETICAL);
		} else {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_CHANNEL_NUMBER);
		}
	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);
		removePopup(pop);
		
		if (gotoHotKey) {
			gotoHotKey = false;
			super.popupClosed(pop, msg);
			return;
		}
		if (messagePopupVector != null && messagePopupVector.size() > 0) {
			Message message = (Message) messagePopupVector.remove(0);
			lastMessage = message;
			// Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
			notiPop.setExplain(message.getText());
			String[] proccessedMsg = notiPop.getExplain();
			
			if (message.getType().equals("3")) {
				// show CYO_13
				if (message.getId().equals("35")) {
					// VDTRMASTER-5747
					notiPop.stop();
					
					vPop.setIcon(true);
					setPopup(vPop);
					vPop.show(this, message.getText());
				} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
					// VDTRMASTER-5747
					notiPop.stop();
					
					setPopup(infoPopup);
					infoPopup.show(this, message);
					notiPop.setExplain(null);
				} else if (proccessedMsg == null) {
					// VDTRMASTER-5747
					notiPop.stop();
					
					vPop.setIcon(false);
					setPopup(vPop);
					vPop.show(this, message.getText());
				} else {
					setPopup(notiPop);
					//notiPop.setExplain(message.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
			} else if (message.getId().equals("307_MEILLEURE_OFFRE")) {
				setPopup(notiPop);

				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
			} else if ((message.getType()).equals("5")) {
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				expired = true;
			} else if ((message.getType()).equals("1")) {
				// show CYO_14
				setPopup(notiPop);
//				notiPop.setExplain(services.getMessage()[0].getText());
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
			} else {
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			}
			repaint();
			return;
		}

		if (expired) {
			cyoReStart();
		} else if (pop instanceof ValidationErrorPopupUI) {
			if (lastUIId != BaseUI.UI_INITIAL_STATE) {
				dispose();
				CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				repaint();
			}
		} else if (pop instanceof NotificationPopupUI) {
			if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					dispose();
					CyoController.getInstance().showUI(lastUIId, true);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
				Integer result = (Integer) msg;
				if (result.intValue() == 0) {
					requestSaveAction();
				} else if (result.intValue() == 1) {
					dispose();
					CyoController.getInstance().showUI(lastUIId, true);
				} else if (result.intValue() == 2) {

				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL_CANCEL) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {

					for (int a = 0; a < customList.getCyoObjectLength(); a++) {
						CyoObject obj = customList.getCyoObject(a);

						if (obj instanceof Package) {
							if (obj.getCyoObject() != null) {
								CyoObject[] subObj = obj.getCyoObject();
								for (int j = 0; j < subObj.length; j++) {
									Subscription ssubObj = (Subscription) subObj[j];
									if (((Package) obj).getModifiable().equals("0")
											&& (ssubObj.getModifiable().equals("1") || ssubObj.getModifiable().equals(
													"0"))) {
										Log.printInfo("1getid = " + ssubObj.getId());
										if (addVector.contains(ssubObj)) {
											addVector.remove(ssubObj);
										}

										if (!removeVector.contains(ssubObj) && ssubObj.getNo() != null) {
											removeVector.add(ssubObj);
										}
										ssubObj.setSubscribedInSession(false);
									}
								}

								if (addVector.contains(obj)) {
									addVector.remove(obj);
								}

								if (!removeVector.contains(obj) && obj != null) {
									removeVector.add(obj);
								}

								if (((Package) obj).getModifiable().equals("0")) {
									((Package) obj).setSubscribedInSession(false);
								}
							}
						} else if (obj instanceof Channel) {
							Channel ch = (Channel) obj;
							if ((ch.getModifiable().equals("0")) && ch.getSubscribedInSession()) {
								Log.printInfo("2getid = " + ch.getId());

								if (addVector.contains(obj)) {
									addVector.remove(obj);
								}
								if (!removeVector.contains(obj) && ch.getNo() != null) {
									removeVector.add(obj);
								}
								ch.setSubscribedInSession(false);
							}
						}
					}
					addVector.clear();
					((SelfServeRenderer)renderer).update(this);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL) {
				if (lastRequestedURLType == URLType.MODIFY_A_LA_CARTE && lastObject.getSuccess().equals("1")) {
					if (!channelInfoBoolean) {
						if (saveMessageView) {
							ManageTVServiceUI.subDataChanged = true;
						}
						if (lastUIId == BaseUI.UI_INITIAL_STATE) {
							if (needParamForRequiresUpsell) {
								Hashtable param = new Hashtable();
								param.put("requiresUpsell", "false");
								needParamForRequiresUpsell = false;
								CyoDataManager.getInstance().request(URLType.VALIDATION, param);
							} else {
								CyoDataManager.getInstance().request(URLType.VALIDATION, null);
							}
						} else {
							dispose();
							CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
						}
					} else {
						channelInfoBoolean = false;
					}
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					repaint();
				} else {
					ManageTVServiceUI.subDataChanged = true;
					if (lastUIId == BaseUI.UI_INITIAL_STATE) {
						if (lastRequestedURLType == URLType.MODIFY_A_LA_CARTE) {
							if (needParamForRequiresUpsell) {
								Hashtable param = new Hashtable();
								param.put("requiresUpsell", "false");
								needParamForRequiresUpsell = false;
								CyoDataManager.getInstance().request(URLType.VALIDATION, param);
							} else {
								CyoDataManager.getInstance().request(URLType.VALIDATION, null);
							}
						} else {
							dispose();
							CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
						}
					} else {
						dispose();
						CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
					}
				}
			}
		} else if (pop.equals(validationUpSellPopupUI)) {
			Boolean result = (Boolean) msg;

			Log.printDebug("SelfServeUI, popupClosed, validationUpSellPopupUI, result=" + result);

			if (result.booleanValue()) {
				Message message = validationUpSellPopupUI.getMessage();

				DataCenter.getInstance().put("UPSELL_MESSAGE", message);

				if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_PACKAGE) || message
						.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {

					CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);

				} else if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_A_LA_CARTE)) {
					// NOTHING;
					Log.printDebug("SelfServeUI, popupClosed, optimizationType is"
							+ " OPTIMIZATION_TYPE_A_LA_CARTE");
				}

			} else {
				needParamForRequiresUpsell = true;
				CyoDataManager.getInstance().setNeedParamForRequiresUpsell(true);
				requestSaveAction();
			}
		}
		repaint();
	}

	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("SelfServeUI: receiveCyoData(): type = " + type + ", state = " + state + ", obj = " + obj);
		}
		
		lastRequestedURLType = type;
		lastObject = obj;

		if (type == URLType.LOAD_A_LA_CARTE) {
			if (obj.getSuccess().equals("1")) {

				if (obj instanceof Services) {
					services = (Services) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug("SelfServeUI: receiveCyoData: cyoData length = "
								+ services.getCyoObject().length);
					}
					CyoObject[] groups = services.getCyoObject();
					
					Vector groupVec = new Vector();
					CyoObject[] groupData = null;
					Message message = null;
					
					if (groups != null) {
						for (int i = 0; i < groups.length; i++) {
							if (groups[i] instanceof Group) {
								groupVec.add(groups[i]);
							} else if (groups[i] instanceof Message) {
								message = (Message) groups[i];
								if (Log.DEBUG_ON) {
									Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: found message");
								}
							}
						}
						groupData = new CyoObject[groupVec.size()];
						groupVec.toArray(groupData);
					}
					
					
					if (groupData[0] instanceof Group) {
						// custom channels
						loadedGroup = (Group) groupData[0];

						if (Log.DEBUG_ON) {
							Log.printDebug("SelfServeUI: receiveCyoData: groupData length = " + groupData.length);
							Log.printDebug("SelfServeUI: receiveCyoData: groupData[0].getCyoObject length = "
									+ groupData[0].getCyoObject().length);
						}

						Vector vData = new Vector();

						if (groupData != null) {
							CyoObject[] data = groupData[0].getCyoObject();
							Vector sortData = new Vector();

							for (int a = 0; data != null && a < data.length; a++) {
								if (((Subscription) data[a]).getSubscribedInAccount() != ((Subscription) data[a])
										.getSubscribedInSession()) {
									// update SubscribedInSession
									String modifiable = ((Subscription) data[a]).getModifiable();
									if (modifiable.equals("0")) {
										// changed the one more.
										((Subscription) data[a]).setSubscribedInSession(!((Subscription) data[a])
												.getSubscribedInSession());
										
										updateSubscribed(data[a]);
									}
									ManageTVServiceUI.subDataChanged = true;
								}

								if (startLoad) {
									sortData.add(data[a]);
								}
							}

							Hashtable addHash = new Hashtable();
							Hashtable removeHash = new Hashtable();
							Log.printDebug("--------------a33111--");
							for (int a = 0; a < addVector.size(); a++) {
								Subscription ss = (Subscription) addVector.elementAt(a);
								String key = ss instanceof Package || ss instanceof Product ? ((Subscription) ss).getNo() : ((Subscription) ss)
										.getId();
								Log.printDebug("key = " + key);
								addHash.put(key, ss);
							}

							for (int a = 0; a < removeVector.size(); a++) {
								Subscription ss = (Subscription) removeVector.elementAt(a);
								String key = ss instanceof Package  || ss instanceof Product ? ((Subscription) ss).getNo() : ((Subscription) ss)
										.getId();
								Log.printDebug("re ss.getNo() = " + key);
								removeHash.put(key, ss);
							}

							CyoObject[] cur = customList.getAllCyoObject();
							Hashtable orignalHash = new Hashtable();
							Log.printDebug("--------------a111--");
							for (int a = 0; cur != null && a < cur.length; a++) {
								Subscription ss = (Subscription) cur[a];
								if (ss != null) {
									Log.printDebug("ss.getNo() " + ss.getNo() + " " + ss.getId());
									if (ss instanceof Package || ss instanceof Product ) {
										if (ss.getNo() != null) {
											orignalHash.put(ss.getNo(), ss);
										}
									} else if (ss instanceof Channel) {
										if (ss.getId() != null) {
											orignalHash.put(ss.getId(), ss);
										}
									}

								}
							}
							Log.printDebug("--------------a12211--");
							for (int a = 0; data != null && a < data.length; a++) {
								String key = data[a] instanceof Package  || data[a] instanceof Product ? ((Subscription) data[a]).getNo()
										: ((Subscription) data[a]).getId();

								Subscription cyoObj = (Subscription) orignalHash.get(key);
								Log.printDebug("--------------cyoObj--" + cyoObj);
								if (cyoObj == null)
									continue;

								cyoObj.setSequenceNo(((Subscription) data[a]).getSequenceNo());

								Log.printDebug("--------------key--" + key);
								Log.printDebug("--------------cyoObj--" + cyoObj);

								if (addHash.contains(key)) {
									Log.printDebug("pellos1 = " + key);
									updateSubscribed(cyoObj);
								} else if (removeHash.contains(key)) {
									Log.printDebug("pellos2 = " + key);
									updateSubscribed(cyoObj);
								}

								if (!startLoad) {
									sortData.add(cyoObj);
								}
							}

							Collections.sort(sortData, CyoObjectComparator.comparator);

							for (int b = 0; b < sortData.size(); b++) {
								CyoObject coj = (CyoObject) sortData.elementAt(b);

								if (coj instanceof Package) {
									Subscription ss = (Subscription) coj;
									ss.setSubscribedOrginalInSession(String.valueOf(ss.getSubscribedInSession()));
									vData.add(coj);
									CyoObject[] channels = coj.getCyoObject();
									Vector vChannelData = new Vector();
									for (int c = 0; channels != null && c < channels.length; c++) {
										vChannelData.add(channels[c]);
									}
									Collections.sort(vChannelData, CyoObjectComparator.comparator);
//									for (int c = 0; c < vChannelData.size(); c++) {
//										Channel ch = (Channel) vChannelData.elementAt(c);
//										vData.add(ch);
//										// Log.printInfo("Channel sort getSequenceNo = "
//										// + ch.getSequenceNo());
//									}
									CyoObject[] channelArr = new CyoObject[vChannelData.size()];
									vChannelData.copyInto(channelArr);
									coj.setCyoObject(channelArr);
									vChannelData.clear();
								} else if (coj instanceof Channel) {
									Subscription ss = (Subscription) coj;
									ss.setSubscribedOrginalInSession(String.valueOf(ss.getSubscribedInSession()));
									vData.add(coj);
								}
							}
						}

						loadGroupTitle = loadedGroup.getTitle();

						CyoObject[] rearrangeData = new CyoObject[vData.size()];
						vData.copyInto(rearrangeData);

						customList.setCyoObject(rearrangeData);
						customList.setHasFocus(true);
						customList.start(true);
						currentList = customList;
						((SelfServeRenderer)renderer).update(this);
						vData.clear();
						// buildDataForList(groups[0].getCyoObject());

						// sync with myPicks of Modify.
						if (startLoad) {
							
							Vector customMyPicks = (Vector) DataCenter.getInstance().get("ADDED_CUSTOM_CHANNELS");

							Log.printDebug("SelfServeUI: receiveCyoData: ADDED_CUSTOM_CHANNELS :" + customMyPicks);

							if (customMyPicks != null) {
								Log.printDebug("SelfServeUI: receiveCyoData: ADDED_CUSTOM_CHANNELS size :"
										+ customMyPicks.size());
								for (int i = 0; i < customMyPicks.size(); i++) {
									Subscription sub = (Subscription) customMyPicks.get(i);
									
									for (int j = 0; j < rearrangeData.length; j++) {
										Subscription newSub = (Subscription) rearrangeData[j];
										
										if (Log.DEBUG_ON) {
											Log.printDebug("SelfServeUI: receivedCyoData: newSub.getNo()=" + newSub.getNo());
											Log.printDebug("SelfServeUI: receivedCyoData: sub.getNo()=" + sub.getNo());
										}
										
										if (sub.getNo().equals(newSub.getNo()) && newSub.getModifiable().equals("0")) {
											
//											addVector.remove(sub);
//											addVector.add(newSub);
//											removeVector.remove(sub);
											removeInVector(addVector, sub.getNo());
											addVector.add(newSub);
											removeInVector(removeVector, sub.getNo());
										} else if (sub.getNo().equals(newSub.getNo()) && !newSub.getModifiable().equals("0")) {
											removeInVector(addVector, sub.getNo());
											removeInVector(removeVector, sub.getNo());
										}
									}
								}
								if (Log.DEBUG_ON) {
									Log.printDebug("SelfServeUI: receivedCyoData: addVector size=" + addVector.size());
								}
							}

						}
						
						if (message != null) {
							if (Log.DEBUG_ON) {
								Log.printDebug("SelfServeUI: receivedCyoData: found message");
							}
							if (needDisplay5CPopup) {
								setPopup(infoPopup);
								infoPopup.show(this, message);
								needDisplay5CPopup = false;
							}
						}
					}
				} else if (obj instanceof Group) {
					Vector dataVector = new Vector();
					Group group = (Group) obj;
					CyoObject[] data = group.getCyoObject();

					addCyoObject(dataVector, data);

					CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
					dataVector.copyInto(rearrangeData);

					dataVector.clear();

					customList.setCyoObject(rearrangeData);
					customList.setHasFocus(true);
					customList.start(true);
					renderer.prepare(this);
				}
				
				// zestyman initialize removeVector becuase we just focus on addVector to sync, don't need to focus on removeVecotor.
				removeVector.clear();
				
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					lastMessage = msg;
					// Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
					notiPop.setExplain(msg.getText());
					String[] proccessedMsg = notiPop.getExplain();
					
					if (msg.getType().equals("3")) {
						// show CYO_13
						if (msg.getId().equals("35")) {
							// VDTRMASTER-5747
							notiPop.stop();
							
							vPop.setIcon(true);
							setPopup(vPop);
							vPop.show(this, msg.getText());
						} else if (msg.getId().equals("312")) {
							// VDTRMASTER-5747
							notiPop.stop();
							
							setPopup(infoPopup);
							infoPopup.show(this, msg);
							notiPop.setExplain(null);
						} else if (proccessedMsg == null) {
							// VDTRMASTER-5747
							notiPop.stop();
							
							vPop.setIcon(false);
							setPopup(vPop);
							vPop.show(this, msg.getText());
						} else {
							setPopup(notiPop);
							notiPop.setExplain(msg.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
					} else if ((msg.getType()).equals("5")) {
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						Log.printInfo("msg.getType() = " + msg.getType());
						expired = true;
					} else {
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						Log.printInfo("msg.getType() = " + msg.getType());
					}
				}
			}

			repaint();
		} else if (type == URLType.MODIFY_A_LA_CARTE) {
			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: receiveCyoData: MODIFY_A_LA_CARTE success = " + obj.getSuccess());
			}
			Messages messages = (Messages) obj;

			if (obj.getSuccess().equals("1")) {
				CyoObject[] subObjs = messages.getCyoObject();

				if (subObjs != null) {
					for (int i = 0; i < subObjs.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug("SelfServeUI: receiveCyoData: subObjs[" + i + "] = "
									+ subObjs[i]);
						}
						if (subObjs[i] instanceof Message) {
							messagePopupVector.add(subObjs[i]);
						}
					}

					if (messagePopupVector != null && messagePopupVector.size() > 0) {
						StringBuffer sf = new StringBuffer();
						Message message = (Message) messagePopupVector.remove(0);
						lastMessage = message;
						Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());
						if (message.getId().equals("307_MEILLEURE_OFFRE")) {
							setPopup(notiPop);

							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
						} else if (message.getType().equals("0")) {// &&
							// message.getId().equals("140")
							sf.append(message.getText());
							sf.append("\n");
							if (!saveMessageView) {
								saveMessageView = true;
								setPopup(notiPop);
								notiPop.setExplain(sf.toString());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else if (message.getType().equals("1")) {
							if (message.getOptimizationType() != null) {
								if (Log.DEBUG_ON) {
									Log.printDebug("message.optimizationType=" + message.getOptimizationType());
									Log.printDebug("message.optimizationCode=" + message.getOptimizationCode());
								}
								needParamForRequiresUpsell = false;
								setPopup(validationUpSellPopupUI);
								validationUpSellPopupUI.show(this, message);
							} else {
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
							}
						} else {
							ManageTVServiceUI.subDataChanged = true;
							if (lastUIId == BaseUI.UI_INITIAL_STATE) {
								if (needParamForRequiresUpsell) {
									Hashtable param = new Hashtable();
									param.put("requiresUpsell", "false");
									needParamForRequiresUpsell = false;
									CyoDataManager.getInstance().request(URLType.VALIDATION, param);
								} else {
									CyoDataManager.getInstance().request(URLType.VALIDATION, null);
								}
							} else {
								dispose();
								CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
							}
						}
					}
				} else {
					ManageTVServiceUI.subDataChanged = true;

					if (lastUIId == BaseUI.UI_INITIAL_STATE) {
						CyoDataManager.getInstance().request(URLType.VALIDATION, null);
					} else {
						dispose();
						CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
					}

				}
			} else {
				CyoObject[] subObjs = obj.getCyoObject();
				if (subObjs != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("SelfServeUI: receiveCyoData: subObjs = " + subObjs);
					}
					for (int i = 0; i < subObjs.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug("SelfServeUI: receiveCyoData: subObjs[" + i + "] = "
									+ subObjs[i]);
						}

						if (subObjs[i] instanceof Message) {
							messagePopupVector.add(subObjs[i]);
						}
					}
					if (messagePopupVector != null && messagePopupVector.size() > 0) {
						Message message = (Message) messagePopupVector.remove(0);
						lastMessage = message;
						if (Log.DEBUG_ON) {
							Log.printDebug("SelfServeUI: receiveCyoData: message = " + message);
							Log.printDebug("SelfServeUI: receiveCyoData: message = " + message.getId());
						}

						// Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
						notiPop.setExplain(message.getText());
						String[] proccessedMsg = notiPop.getExplain();
						
						if (message.getType().equals("3")) {
							// show CYO_13
							if (message.getId().equals("35")) {
								// VDTRMASTER-5747
								notiPop.stop();
								
								vPop.setIcon(true);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else if (message.getId().equals("312")) {
								// VDTRMASTER-5747
								notiPop.stop();
								
								setPopup(infoPopup);
								infoPopup.show(this, message);
								notiPop.setExplain(null);
							} else  if (proccessedMsg == null) {
								// VDTRMASTER-5747
								notiPop.stop();
								
								vPop.setIcon(false);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else {
								setPopup(notiPop);
								//notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else if ((message.getType()).equals("5")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							expired = true;
						} else if ((message.getType()).equals("1")) {
							setPopup(notiPop);
//							notiPop.setExplain(services.getMessage()[0].getText());
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
						} else {
							// show CYO_14
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
					}
					repaint();
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("SelfServeUI: receiveCyoData: subObjs is null");
					}
				}
			}
		} else if (type == URLType.VALIDATION) {
			if (Log.DEBUG_ON) {
				Log.printDebug("SelfServeUI: receiveCyoData: VALIDATION success = " + obj.getSuccess());
			}
			if (obj instanceof Services) {
				Services services = (Services) obj;
				if (services.getSuccess().equals("1")) {
					stop();
					if (services.getMessage() != null) {
						setPopup(notiPop);
						notiPop.setExplain(services.getMessage()[0].getText());
						notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
					} else {
						DataCenter.getInstance().put("VALIDATION", obj);
						CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
					}
				} else {
					CyoObject[] subObjs = services.getCyoObject();
					if (subObjs != null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("SelfServeUI: receiveCyoData: subObjs = " + subObjs);
						}
						for (int i = 0; i < subObjs.length; i++) {
							if (Log.DEBUG_ON) {
								Log.printDebug(
										"SelfServeUI: receiveCyoData: subObjs[" + i + "] = " + subObjs[i]);
							}

							if (subObjs[i] instanceof Message) {
								messagePopupVector.add(subObjs[i]);
							}
						}
						if (messagePopupVector != null && messagePopupVector.size() > 0) {
							Message message = (Message) messagePopupVector.remove(0);

							if (Log.DEBUG_ON) {
								Log.printDebug("SelfServeUI: receiveCyoData: message = " + message);
								Log.printDebug(
										"SelfServeUI: receiveCyoData: message = " + message.getId());
							}

							notiPop.setExplain(message.getText());
							String[] proccessedMsg = notiPop.getExplain();

							if (message.getType().equals("3")) {
								// show CYO_13
								if (message.getId().equals("35")) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(true);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
									// VDTRMASTER-5747
									notiPop.stop();

									setPopup(infoPopup);
									infoPopup.show(this, message);
									notiPop.setExplain(null);
								} else if (proccessedMsg == null) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(false);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else {
									setPopup(notiPop);
									//notiPop.setExplain(message.getText());
									notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								}
							} else if ((message.getType()).equals("5")) {
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								expired = true;
							} else if (message.getId().equals("1")) {
								setPopup(notiPop);
								notiPop.setExplain(services.getMessage()[0].getText());
								notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
							} else {
								// show CYO_14
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						}
						repaint();
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("SelfServeUI: receiveCyoData: subObjs is null");
						}
					}
				}
			} else if (obj instanceof Messages) {
				// Validataion big popup
				Messages msgs = (Messages) obj;

				if (msgs.getSuccess().equals("1")) {
					if (msgs.getCyoObject() != null && msgs.getCyoObject()[0] instanceof Message) {
						Message msg = (Message) msgs.getCyoObject()[0];
						if (Log.DEBUG_ON) {
							Log.printDebug(
									"SelfServeUI, msg.getOptimizationType()=" + msg.getOptimizationType());
						}
						if (msg.getOptimizationType() != null) {
							needParamForRequiresUpsell = false;
							setPopup(validationUpSellPopupUI);
							validationUpSellPopupUI.show(this, msg);
						}
					}
				}
			}
		}
	}

	public void canceled() {
		// TODO Auto-generated method stub

	}

	public void selected(MenuItem item) {

		// sort
		if (currentList.equals(currentList)) {
			Hashtable param = new Hashtable();
			param.put("groupNo", loadedGroup.getNo());

			boolean checkSort = false;
			if (item.equals(nSORT)) {
				param.put("sort", Param.SORT_NUMBER);
				checkSort = sort == Param.SORT_NUMBER ? true : false;
				sort = Param.SORT_NUMBER;
			} else {
				param.put("sort", Param.SORT_ALPHABETICAL);
				checkSort = sort == Param.SORT_ALPHABETICAL ? true : false;
				sort = Param.SORT_ALPHABETICAL;
			}
			if (!checkSort) {
				startLoad = false;
				
				if (addVector.size() > 0) {
					clonedAddVector = (Vector) addVector.clone();
					if (Log.DEBUG_ON) {
						Log.printDebug("SelfServeUI: requestSaveAction: addVector size = " + addVector.size());
						Log.printDebug("SelfServeUI: requestSaveAction: clonedAddVector size = " + clonedAddVector.size());
						
					}
					DataCenter.getInstance().put("ADDED_CUSTOM_CHANNELS", clonedAddVector);
				}
				CyoDataManager.getInstance().request(URLType.LOAD_A_LA_CARTE, param);
			}
		}
	}

	public void receiveCyoError(int type) {
		if (type == URLType.LOAD_GROUP) {
			sort = sort == Param.SORT_NUMBER ? Param.SORT_ALPHABETICAL : Param.SORT_NUMBER;
		}
	}

	public boolean getChanged() {
		if (addVector.size() > 0 || removeVector.size() > 0) {
			for (int a = 0; a < addVector.size(); a++) {
				CyoObject obj = (CyoObject) addVector.elementAt(a);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("add getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("add getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}

			for (int a = 0; a < removeVector.size(); a++) {
				CyoObject obj = (CyoObject) removeVector.elementAt(a);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("remove getSubscribedOrginalInSession() " + " " + sobj.getNo() + " = "
							+ sobj.getSubscribedOrginalInSession());
					Log.printDebug("remove getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}
		}

		return false;
	}

	private void removeInVector(Vector vec, String no) {
		
		if (no == null || no.length() < 1) {
			return;
		}
		
		for (int i = 0; i < vec.size(); i++) {
			Subscription sub = (Subscription) vec.get(i);
			
			if (sub.getNo().equals(no)) {
				vec.remove(i);
				return;
			}
		}
	}
}

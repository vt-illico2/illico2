/**
 * @(#)GameUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.Game;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.data.obj.Messages;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Subscription;
import com.videotron.tvi.illico.cyo.gui.GameRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class GameUI extends BaseUI implements CyoDataListener {
	public static final int STATE_LIST = 0x0;
	public static final int STATE_BUTTON = 0x1;

	private ListUI gameList = new GameListUI();
	private ClickingEffect clickEffect;
	private Package curPackage;

	private Vector addVector = new Vector();
	private Vector removeVector = new Vector();

	private int btnFocus = 0;

	private ListUI currentList;

	private boolean canSelect;
	private int btnLimit;

	private boolean gotoHelp = false;

	private boolean isSuccessed = false;
	
	private boolean isModified = false;

	public GameUI() {
		renderer = new GameRenderer();
		setRenderer(renderer);
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);
		if (!back) {
			currentList = null;
			CyoObject obj = (CyoObject) DataCenter.getInstance().get("GamePackage");
			Group parentObj = (Group) obj.getParentObj();

			curPackage = (Package) obj;

			addVector.clear();
			removeVector.clear();
			// TV-Web or Game Plus
			if (parentObj.getNo().equals("1541") || curPackage.getNo().equals("460")) {
				// if (obj instanceof Package) {
				canSelect = false;
				btnLimit = 2;
				btnFocus = 1;
				gameList.setState(0);
				buildDataForList(obj.getCyoObject());

			} else {// if (obj instanceof Game) {
				// curPackage = (Package) obj.getParentObj();
				canSelect = true;
				btnLimit = 2;
				btnFocus = 0;
				gameList.setState(1);
				// buildDataForList(obj.getParentObj().getCyoObject());
				buildDataForList(curPackage.getCyoObject());
			}

			prepare();
		} else {
			((GameListUI) gameList).startGetImage();
		}
		super.start(back);
		gotoHelp = false;
		isModified = false;
	}

	public void prepare() {
		state = STATE_LIST;
		focus = 0;
		setFooter();
		super.prepare();
	}

	public void stop() {
		clearPopup();

		((GameListUI) currentList).gotoHelp(gotoHelp);
		currentList.stop();

		super.stop();

	}

	public void dispose() {
		clickEffect = null;
		super.dispose();
	}

	public void reset() {
		for (int a = 0; a < addVector.size(); a++) {
			Game gg = (Game) addVector.elementAt(a);
			String key = gg.getNo();
			Log.printDebug("key = " + key);
			for (int b = 0; b < currentList.getCyoObjectLength(); b++) {
				Game obj = (Game) currentList.getCyoObject(b);
				if (obj != null && obj.getNo().equals(key)) {
					obj.setSubscribedInSession(!obj.getSubscribedInSession());

				}
			}
		}

		for (int a = 0; a < removeVector.size(); a++) {
			Game gg = (Game) removeVector.elementAt(a);
			String key = gg.getNo();
			for (int b = 0; b < currentList.getCyoObjectLength(); b++) {
				Game obj = (Game) currentList.getCyoObject(b);
				if (obj != null && obj.getNo().equals(key)) {
					obj.setSubscribedInSession(!obj.getSubscribedInSession());
				}
			}
		}

		addVector.clear();
		removeVector.clear();
	}

	public boolean handleKey(int code) {

		if (getPopup() != null) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case OCRcEvent.VK_PAGE_UP:
			pageUp();
			return true;
		case OCRcEvent.VK_PAGE_DOWN:
			pageDown();
			return true;
		case OCRcEvent.VK_ENTER:
			keyEnter();
			return true;
		case KeyCodes.LAST:
			footer.clickAnimation(0);
			// reset();
			// CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES,
			// true);
			
			if (Log.DEBUG_ON) {
				Log.printDebug("GameUI: handleKey(): canSelect =" + canSelect);
				Log.printDebug("GameUI: handleKey(): isModified =" + isModified);
				Log.printDebug("GameUI: handleKey(): isChanged() =" + isChanged());
			}
			if (canSelect && isModified) {
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			} else {
				if (canSelect) {
					reset();
				}
				CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
			}
			repaint();
			return true;
		case KeyCodes.COLOR_B:
			// deselect
			if (canSelect) {
				footer.clickAnimation(2);
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL_CANCEL,
						BaseUI.getMenuText(KeyNames.UNSELECT_GAME_TITLE),
						BaseUI.getExplainText(KeyNames.UNSELECT_GAME));
				repaint();
			}
			return true;
		default:
			return false;
		}
	}

	private void keyLeft() {
		if (state == STATE_BUTTON && currentList.getCyoObjectLength() > 0) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
		} else if (state == STATE_BUTTON) {
			state = STATE_LIST;
			btnFocus = 0;
		}
		repaint();

	}

	private void keyRight() {
		if (state == STATE_LIST) {
			state = STATE_BUTTON;
			currentList.setHasFocus(false);
			if (!isChanged() && canSelect) {
				btnFocus = 1;
			} else if (canSelect) {
				btnFocus = 0;
			}
			repaint();
		}
	}

	private void keyUp() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_UP);
		} else if (state == STATE_BUTTON) {
			if (canSelect) {
				if (--btnFocus < 0 && isChanged()) {
					btnFocus = 0;
				} else if (!isChanged()){
					btnFocus = 1;
				}
			} else {
				if (--btnFocus < 1) {
					btnFocus = 1;
				}
			}

		}
		repaint();
	}

	private void keyDown() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_DOWN);
		} else if (state == STATE_BUTTON) {
			Log.printDebug("btnFocus = " + btnFocus + " btnLimit = " + btnLimit);
			if (++btnFocus > btnLimit) {
				btnFocus = btnLimit;
			}
			
			
		}
		repaint();
	}

	private void pageUp() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_UP);
			repaint();
		}
	}

	private void pageDown() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
			repaint();
		}
	}

	private void keyEnter() {
		if (state == STATE_LIST) {
			CyoObject curObj = currentList.getCurrentCyoObject();
			updateSubscribed(curObj);
			needExitPopup = true;
		} else if (state == STATE_BUTTON) {
			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}

			if (canSelect) {

				if (btnFocus == 0) { // Confirm
					if (Log.DEBUG_ON) {
						Log.printDebug("GameUI: curPackage is " + curPackage.getNo());
					}
					clickEffect.start(717, 346, 180, 35);
					if (isModified) {
						DataCenter.getInstance().put("GAME_PACKAGES_UPDATED", new Boolean(true));
						requestSaveAction();
					} else {
						CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
					}
				} else if (btnFocus == 1) { // Cancel
					CyoObject curObj = gameList.getCurrentCyoObject();
					clickEffect.start(717, 346 + 37, 180, 35);
					if (curObj == null) {
						CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
					} else {
						setPopup(notiPop);
						notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
					}
				} else if (btnFocus == 2) { // Help
					gotoHelp = true;
					DataCenter.getInstance().put("MODE", "HELP");
					clickEffect.start(717, 346 + 37 + 37, 180, 35);
					CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
				}
			} else {
				if (btnFocus == 1) { // back
					if (Log.DEBUG_ON) {
						Log.printDebug("GameUI: curPackage is " + curPackage);
					}
					clickEffect.start(717, 346 + 37, 180, 35);
					CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
				} else if (btnFocus == 2) { // Help
					gotoHelp = true;
					clickEffect.start(717, 346 + 37 + 37, 180, 35);
					DataCenter.getInstance().put("MODE", "HELP");
					CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
				}
			}

		}
		repaint();
	}

	private void setFooter() {
		footer.reset();
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
		if (canSelect) {
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_B),
					BaseUI.getMenuText(KeyNames.BTN_DESELECT_ALL));
		}
	}

	public Package getPackage() {
		return curPackage;
	}

	public String getPackageName() {
		if (curPackage != null) {
			if (canSelect) {
				return " | " + curPackage.getPrice() + BaseUI.getPriceFreq(curPackage.getPricefreq());
			} else {
				return curPackage.getTitle() + " | " + curPackage.getPrice() + BaseUI.getPriceFreq(curPackage.getPricefreq());
			}

		}
		return TextUtil.EMPTY_STRING;
	}

	public boolean canSelect() {
		return canSelect;
	}

	private void updateSubscribed(CyoObject obj) {
		if (obj instanceof Game) {
			Game curGame = ((Game) obj);
			String modiable = curGame.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("GameUI: updateSubscribed: curGame modiable = " + modiable);
			}
			curGame.setSubscribedInSession(!curGame.getSubscribedInSession());

			if (curGame.getSubscribedInSession()) {
				addCyoObject(curGame);
			} else {
				removeCyoObject(curGame);
			}
			
			CyoObject parentObj = curPackage.getParentObj();
			
			CyoObject[] subObjs = parentObj.getCyoObject();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("GameUI: updateSubscribed: subObjs length = " + subObjs.length);
			}
			
			for (int i = 0; i < subObjs.length; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug("GameUI: updateSubscribed: subObjs[" + i + "] = " + subObjs[i]);
				}
				if (!subObjs[i].equals(curPackage)) {
					if (Log.DEBUG_ON) {
						Log.printDebug("GameUI: updateSubscribed: subObjs's SubscribedInSession set false");
					}
					((Subscription) subObjs[i]).setSubscribedInSession(false);
					removeCyoObject(subObjs[i]);
				}
			}
			isModified = true;
		}
	}

	public int getButtonFocus() {
		return btnFocus;
	}

	public ListUI getCurrentList() {
		return currentList;
	}

	
	private void requestSaveAction() {
		String addParams = null, removeParams = null;
		Hashtable param = new Hashtable();

		addParams = getNumbers(addVector);
		removeParams = getNumbers(removeVector);

		if (addParams != null) {
			param.put("add", addParams);
		}
		if (removeParams != null) {
			param.put("remove", removeParams);
		}

		if (addParams != null || removeParams != null) {
			CyoDataManager.getInstance().request(URLType.SAVE_GAME, param);
		}
	}

	private String getNumbers(Vector vec) {
		if (vec.size() == 0) {
			return null;
		}

		StringBuffer sf = new StringBuffer();
		if (vec.equals(addVector)) {
			sf.append(curPackage.getNo());
			sf.append(",");
		}
		
		for (int i = 0; i < vec.size(); i++) {
			if (i > 0) {
				sf.append(",");
			}
			Subscription obj = (Subscription) vec.get(i);
			sf.append(obj.getNo());
			
		}

		return sf.toString();
	}

	private void buildDataForList(CyoObject[] data) {
		
		for (int i = 0; i < data.length; i++) {
			if (((Subscription)data[i]).getSubscribedInSession()) {
				addCyoObject(data[i]);
			} else {
				removeCyoObject(data[i]);
			}
		}

		gameList.setCyoObject(data);
		gameList.setHasFocus(true);
		gameList.start(true);

		currentList = gameList;

	}

	private void addCyoObject(Vector vec, CyoObject[] data) {
		Log.printInfo("data = " + data.length);
		for (int i = 0; i < data.length; i++) {
			vec.add(data[i]);
		}
	}

	private void addCyoObject(CyoObject obj) {
		if (addVector.contains(obj)) {
			addVector.remove(obj);
			addVector.add(obj);
			removeVector.remove(obj);
		} else {
			addVector.add(obj);
			removeVector.remove(obj);
		}
	}

	private void removeCyoObject(CyoObject obj) {
		if (removeVector.contains(obj)) {
			addVector.remove(obj);
			removeVector.remove(obj);
			removeVector.add(obj);
		} else {
			addVector.remove(obj);
			removeVector.add(obj);
		}
	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);
		removePopup(pop);
		if (gotoHotKey) {
			gotoHotKey = false;
			super.popupClosed(pop, msg);
			return;
		}

		if (expired) {
			cyoReStart();
		} else if (pop instanceof ValidationErrorPopupUI) {
			repaint();
		} else if (pop instanceof NotificationPopupUI) {
			if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
				Boolean result = (Boolean) msg;

				if (result.booleanValue()) {
					if (Log.DEBUG_ON) {
						Log.printDebug("GameUI: isModified is " + isChanged());
					}
					resetGames();
					CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL_CANCEL) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					for (int a = 0; a < gameList.getCyoObjectLength(); a++) {
						CyoObject obj = gameList.getCyoObject(a);

						Game ch = (Game) obj;
						if ((ch.getModifiable().equals("0")) && ch.getSubscribedInSession()) {
							Log.printInfo("2getid = " + ch.getId());

							if (addVector.contains(obj)) {
								addVector.remove(obj);
							}
							if (!removeVector.contains(obj) && ch.getNo() != null) {
								removeVector.add(obj);
							}
							ch.setSubscribedInSession(false);
						}
					}
					addVector.clear();
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL && isSuccessed) {
				DataCenter.getInstance().put("GAME_PACKAGES_UPDATED", new Boolean(true));
				CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
			}
			repaint();
		}
	}
	
	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("GameUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = " + obj);

		}

		if (type == URLType.SAVE_GAME) {
			if (obj.getSuccess().equals("1")) {
				isSuccessed = true;
				Messages messages = (Messages) obj;

				CyoObject[] subObjs = messages.getCyoObject();

				if (subObjs != null) {
					for (int i = 0; i < subObjs.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: subObjs[" + i + "] = "
									+ subObjs[i]);
						}
					}

					Message message = (Message) subObjs[0];
					Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());
					if (message.getType().equals("1")) {
						setPopup(notiPop);
						notiPop.setExplain(message.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					} else {
						DataCenter.getInstance().put("GAME_PACKAGES_UPDATED", new Boolean(true));
						CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
					}
				} else {
					DataCenter.getInstance().put("GAME_PACKAGES_UPDATED", new Boolean(true));
					CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, true);
				}

			} else {
				isSuccessed = false;
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					setPopup(notiPop);
					notiPop.setExplain(msg.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

					if ((msg.getType()).equals("5")) {
						expired = true;
					}
				}
			}
		}
	}

	public void receiveCyoError(int type) {
	}

	public ListUI getGameListUI() {
		return gameList;
	}
	
	public boolean isChanged() {
		CyoObject[] gamesObj = curPackage.getCyoObject();
		for (int i = 0; i < gamesObj.length; i++) {
			Subscription game = (Subscription) gamesObj[i];
			
			if (game.getSubscribedInSession() != game.getSubscribedOrginalInSession()) {
				return true;
			}
		}
		
		return false;
	}
	
	private void resetGames() {
		CyoObject[] gamesObj = curPackage.getCyoObject();
		for (int i = 0; i < gamesObj.length; i++) {
			Subscription game = (Subscription) gamesObj[i];
			game.setSubscribedInSession(game.getSubscribedOrginalInSession());
		}
		
	}
}

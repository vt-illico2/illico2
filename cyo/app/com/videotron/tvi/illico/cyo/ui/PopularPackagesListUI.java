/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.debug.DebugScreenManager;
import com.videotron.tvi.illico.cyo.gui.PopularPackagesListRenderer;
import com.videotron.tvi.illico.log.Log;

/**
 * ListUI display a list of service, group, package, channel etc.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopularPackagesListUI extends ListUI {

    public static final int FOCUS_NONE = 0;
    public static final int FOCUS_ON_PARENT = 1;
    public static final int FOCUS_ON_CHILD = 2;
    public static final int FOCUS_ON_ME = 3;

    public PopularPackagesListUI() {
        super(false);
        renderer = new PopularPackagesListRenderer();
        setRenderer(renderer);
    }

    public void start(boolean back) {
        prepare();
        startIdx = 0;
        focus = 0;

        super.start(back);
    }

    public void stop() {
        super.stop();
    }

    public boolean handleKey(int code) {
        Log.printDebug("pellos handleKey");
        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
            super.handleKey(code);
            repaint();
            return true;
        default:
            return false;
        }
    }

    private void keyUp() {
        if (objs != null) {
            if (startIdx + focus >= objs.length) {
                startIdx--;
            } else if (startIdx + focus > 0) {
                if (focus > 3) {
                    focus--;
                } else if (startIdx > 0) {
                    if (focus < 4) {
                        startIdx--;
                    } else {
                        focus--;
                    }
                } else {
                    focus--;
                }
            }
        }
    }

    private void keyDown() {
        if (objs != null) {
            if (startIdx + focus < objs.length - 1) {
                if (focus < 2) {
                    focus++;
                } else if (startIdx + 7 < objs.length) {
                    if (focus > 2) {
                        startIdx++;
                    } else {
                        focus++;
                    }
                } else {
                    if (startIdx + focus > objs.length - 7) {
                        focus++;
                    } else {
                        startIdx++;
                        focus++;
                        if (startIdx + focus >= objs.length) {
                            startIdx--;
                        }
                    }
                }
            }
        }
    }

    public int getStartIdx() {
        return startIdx;
    }

    public CyoObject getCyoObject(int idx) {
        if (objs != null && idx < objs.length && objs[idx] != null) {
            DebugScreenManager.getInstance().addData(objs[idx]);
            return objs[idx];
        }

        return null;
    }

    public CyoObject getCurrentCyoObject() {
        int idx = startIdx + focus;
        return getCyoObject(idx);
    }

    public int getCyoObjectLength() {
        if (objs != null) {
            return objs.length;
        } else {
            return 0;
        }
    }

    public void setCyoObject(CyoObject[] cyoObjs) {
        this.objs = cyoObjs;
    }

    public boolean hasFocus() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    public boolean onFocus(int idx) {
        if (startIdx + focus == idx) {
            return true;
        }

        return false;
    }

    public int getFocusType(int idx) {

        if ((startIdx + focus) == idx) {
            return FOCUS_ON_ME;
        } else {
            return FOCUS_NONE;
        }
    }
}

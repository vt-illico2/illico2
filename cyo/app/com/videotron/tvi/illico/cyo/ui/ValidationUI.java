/**
 * @(#)InitialStateUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.text.DecimalFormat;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Fee;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.data.obj.Services;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.gui.ValidationRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ValidationUI extends BaseUI implements CyoDataListener {

    public static final int STATE_MENU = 0;
    public static final int STATE_BUTTON = 1;

    private ValidateListUI list = new ValidateListUI();
    private ClickingEffect clickEffect;

    private String priceDollarStr = "0";
    private String priceCentStr = ".00 $";

    private String afterPrice;
    private String beforePrice;

    private int btnFocus = 0;

    public ValidationUI() {
        renderer = new ValidationRenderer();
        setRenderer(renderer);
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), getMenuText(KeyNames.BTN_BACK));
    }

    public void start(final boolean back) {
        CyoDataManager.getInstance().addCyoDataListener(this);
        if (!back) {
            prepare();
            CyoObject obj = (CyoObject) DataCenter.getInstance().get("VALIDATION");

            Services services = (Services) obj;
            CyoObject[] objs = services.getCyoObject();

            // objs - group + fee
            buildDataForList(objs);

            CyoDataManager.getInstance().request(URLType.COMPARE_SERVICES, null);
        }
        super.start(back);
    }

    public void prepare() {
        btnFocus = 0;
        // VDTRMASTER-6036
        focus = 1;

        priceDollarStr = "0";
        priceCentStr = ".00 $";
        // VDTRMASTER-6036
        state = STATE_MENU;
        super.prepare();
    }

    public void stop() {
        super.stop();
    }

    public boolean handleKey(final int code) {

        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (state == STATE_MENU) {
                if (focus > 0) {
                    focus--;
                    repaint();
                }
            } else if (state == STATE_BUTTON) {
                if (--btnFocus < 0) {
                    btnFocus = 0;
                    state = STATE_MENU;
                    focus = 1;
                }
                repaint();
            }
            return true;
        case OCRcEvent.VK_DOWN:
            if (state == STATE_MENU) {
                if (focus == 1) {
                    state = STATE_BUTTON;
                    btnFocus = 0;
                } else {
                    focus++;
                }
                repaint();
            } else if (state == STATE_BUTTON) {
                if (btnFocus < 2) {
                    btnFocus++;
                    repaint();
                }
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (state == STATE_MENU) {
                
                if (clickEffect == null) {
                    clickEffect = new ClickingEffect(this, 5);
                }

                clickEffect.start(642, 227 + focus * 42, 252, 36);
                if (focus == 0) {
                    // goto CYO_034
                    CyoController.getInstance().showUI(BaseUI.UI_SEE_DETAILS, false);
                } else if (focus == 1) {
                    CyoController.getInstance().showUI(BaseUI.UI_CHANNEL_LIST_NEW, false);
                }
            } else if (state == STATE_BUTTON) {
                if (clickEffect == null) {
                    clickEffect = new ClickingEffect(this, 5);
                }

                clickEffect.start(624, 341 + btnFocus * 39, 284, 43);

                if (btnFocus == 0) {
                    // goto CYO_038
                    CyoDataManager.getInstance().request(URLType.CONFIRMATION, null);
                    //                    CyoController.getInstance().setNeedToNotify(true);
                    //                     CyoController.getInstance().showUI(BaseUI.UI_CONFIRMATION, false);
                } else if (btnFocus == 1) {
                    // goto CYO_0005
                    CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
                } else {
                    setPopup(notiPop);
                    notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
                    repaint();
//                    CyoController.getInstance().showUI(lastUIId, true);
                }
            }
            return true;
//        case OCRcEvent.VK_PAGE_UP:
//        case OCRcEvent.VK_PAGE_DOWN:
//            if (list != null) {
//                footer.clickAnimation(1);
//                list.handleKey(code);
//                repaint();
//            }
//            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0); 
            //CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
//            CyoController.getInstance().showUI(lastUIId, true);
            setPopup(notiPop);
            notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
            repaint();
            return true;
        default:
            return false;
        }
    }

    public int getBtnFocus() {
        return btnFocus;
    }

    public String getPriceDollar() {
        return priceDollarStr;
    }

    public String getPriceCent() {
        return priceCentStr;
    }

    public ValidateListUI getList() {
        return list;
    }

    private void buildDataForList(CyoObject[] data) {

        Vector dataVector = new Vector();

        addCyoObject(dataVector, data);

        CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
        dataVector.copyInto(rearrangeData);

        list.setCyoObject(rearrangeData);

        if (list.needScroll()) {
            footer.removeButton(getMenuText(KeyNames.BTN_SCROLL));
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), getMenuText(KeyNames.BTN_SCROLL));
        }

        list.start(true);

        dataVector.clear();

    }

    private void addCyoObject(Vector vec, CyoObject[] data) {

        for (int i = 0; i < data.length; i++) {
            vec.add(data[i]);

            if (data[i].getCyoObject() != null) {
                makeDescription(data[i]);
            }
        }
    }

    private void makeDescription(CyoObject data) {
        StringBuffer sf = new StringBuffer();


        for (int i = 0; i < data.getCyoObject().length; i++) {
            if (data.getCyoObject()[i] instanceof Fee) {
                if (sf.length() > 0) {
                    sf.append(", ");
                }
                sf.append(((Fee)data.getCyoObject()[i]).getTitle());
            }
        }

        if (data instanceof Group) {
            ((Group) data).setDesc(sf.toString());
        }
    }

    public void popupClosed(BaseUI pop, Object msg) {
        super.popupClosed(pop, msg);
        removePopup(pop);
        if(gotoHotKey) {
            gotoHotKey = false;
            super.popupClosed(pop, msg);
            return;
        }
        if(expired) {
            cyoReStart();
        } else if (pop instanceof NotificationPopupUI) {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("ValidationUI: pop instanceof NotificastionPopupUI");
        		Log.printDebug("ValidationUI: pop state = " + pop.getState());
        		
        	}
	        if (pop.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
	            Boolean result = (Boolean) msg;
	            if (result.booleanValue()) {
	                repaint();
	            } else {
	                CyoController.getInstance().setNeedToNotify(true);
	                CyoController.getInstance().showUI(BaseUI.UI_CONFIRMATION, false);
	            }
	        } else if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
                Boolean result = (Boolean) msg;

                if (result.booleanValue()) {
                    CyoController.getInstance().showUI(lastUIId, true);
                }
            }
        }
        repaint();
    }

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.INFO_ON) {
            Log.printInfo("ValidationUI: receiveCyoData(): type = " + type + "\t state = " + state
                    + "\t obj = " + obj);

        }
        
       if (type == URLType.CONFIRMATION) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ValidationUI: receiveCyoData(): CONFIRMATION success = " + obj.getSuccess());
            }

            if (obj.getSuccess().equals("1")) {
                DataCenter.getInstance().put(KeyNames.KEY_CONFIRMATION, obj);
            	CyoObject[] subObjs = obj.getCyoObject();
                if (subObjs != null) {
                    Message message = (Message) subObjs[0];
                    Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());
                    if (message.getType().equals("1")) {
                    	setPopup(notiPop);

                        notiPop.setExplain(message.getText());
                        notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
                    } else {
                    	CyoController.getInstance().setNeedToNotify(true);
                        CyoController.getInstance().showUI(BaseUI.UI_CONFIRMATION, false);
                    }
                } else {
                	CyoController.getInstance().setNeedToNotify(true);
                    CyoController.getInstance().showUI(BaseUI.UI_CONFIRMATION, false);
                }
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                    
                    Log.printInfo("msg.getType() = " + msg.getType());
                    if((msg.getType()).equals("5")) {
                        expired = true;
                    }
                }
            }
        } else if (type == URLType.COMPARE_SERVICES) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ValidationUI: receiveCyoData(): COMPARE_SERVICES success = " + obj.getSuccess());
            }
            DataCenter.getInstance().put("COMPARE", obj);
            if (obj.getSuccess().equals("1")) {
                CyoObject[] data = obj.getCyoObject();
                Group currentService = (Group) data[0];
                Group newService = (Group) data[1];

                beforePrice = currentService.getPricetotal();
                afterPrice = newService.getPricetotal();

//                int dotIdx = afterPrice.indexOf('.');
//
//                if (dotIdx > 0) {
//                    afterDollar = afterPrice.substring(0, dotIdx);
//                    afterCent = afterPrice.substring(dotIdx);
//                }
//
//                dotIdx = beforePrice.indexOf('.');
//
//                if (dotIdx > 0) {
//                    beforeDollar = beforePrice.substring(0, dotIdx);
//                    beforeCent = beforePrice.substring(dotIdx);
//                }

                float beforePriceFloat = Float.parseFloat(beforePrice);
                float afterPriceFloat = Float.parseFloat(afterPrice);

                float gapPriceFloat = afterPriceFloat - beforePriceFloat;

                if (Log.DEBUG_ON) {
                    Log.printDebug("ValidationUI: receiveCyoData(): beforePriceFloat=" + beforePriceFloat);
                    Log.printDebug("ValidationUI: receiveCyoData(): afterPriceFloat=" + afterPriceFloat);
                    Log.printDebug("ValidationUI: receiveCyoData(): gapPriceFloat=" + gapPriceFloat);
                }

                DecimalFormat decFormat = new DecimalFormat("#.00");
                String priceStr = decFormat.format(gapPriceFloat);

                if (Log.DEBUG_ON) {
                    Log.printDebug("ValidationUI: receiveCyoData(): beforePriceFloat=" + beforePriceFloat);
                    Log.printDebug("ValidationUI: receiveCyoData(): afterPriceFloat=" + afterPriceFloat);
                    Log.printDebug("ValidationUI: receiveCyoData(): priceStr=" + priceStr);
                }

                int dotIdx = priceStr.indexOf('.');
                if (gapPriceFloat > 0) {
                    priceDollarStr = "+" + priceStr.substring(0, dotIdx);
                } else if (gapPriceFloat < 0) {
                    priceDollarStr = priceStr.substring(0, dotIdx);
                } else {
                    priceDollarStr = "0";
                }
                priceCentStr = priceStr.substring(dotIdx) + " $";

            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    Log.printInfo("msg.getType() = " + msg.getType());
                    if((msg.getType()).equals("5")) {                        
                        setPopup(notiPop);
                        notiPop.setExplain(msg.getText());
                        notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                        expired = true;
                    }
                }      
            }
        }
        repaint();
    }
    
    public void receiveCyoError(int type) {
        
    }
}

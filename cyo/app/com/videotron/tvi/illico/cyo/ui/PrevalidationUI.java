/**
 * @(#)InitialStateUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.gui.PrevalidationRenderer;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class display CYO_015.
 * @author Woojung Kim
 * @version 1.1
 */
public class PrevalidationUI extends BaseUI  implements CyoDataListener {
    private ClickingEffect clickEffect;
    
    public PrevalidationUI() {
        renderer = new PrevalidationRenderer();
        setRenderer(renderer);
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), getMenuText(KeyNames.BTN_BACK));
    }

    public void start(final boolean back) {
        CyoDataManager.getInstance().addCyoDataListener(this);
        if (!back) {
            prepare();
        }

        super.start(back);
    }

    public void stop() {
        super.stop();
    }

    public boolean handleKey(final int code) {

        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_LEFT:
            if (focus == 1) {
                focus = 0;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (focus == 0) {
                focus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }
            if (focus == 0) {
                // confirmation and goto CYO_020
                clickEffect.start(187, 378, 162, 40);
                CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);                
//                CyoDataManager.getInstance().request(URLType.CONFIRMATION, null);
            } else {
                // goto CYO_036
                clickEffect.start(618, 378, 162, 40);
                CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
            }
            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0);
            CyoController.getInstance().showUI(BaseUI.UI_CHANGE_CHANNEL_SELECTION, true);
            return true;
        default:
            return false;
        }
    }
    
    public void popupClosed(BaseUI pop, Object msg) {
        super.popupClosed(pop, msg);
        removePopup(pop);
        if(gotoHotKey) {
            gotoHotKey = false;
            super.popupClosed(pop, msg);
            return;
        }
        if(expired) {
            cyoReStart();
        } else if (pop instanceof NotificationPopupUI) {
            repaint();
        }
    }

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.INFO_ON) {
            Log.printInfo("PrevalidationUI: receiveCyoData(): type = " + type + "\t state = " + state
                    + "\t obj = " + obj);

        }
        if (type == URLType.CONFIRMATION) {
            if (Log.DEBUG_ON) {
                Log.printDebug("PrevalidationUI: receiveCyoData(): CONFIRMATION success = "
                        + obj.getSuccess());
            }

            if (obj.getSuccess().equals("1")) {
                CyoController.getInstance().setNeedToNotify(true);
                CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                    
                    Log.printInfo("msg.getType() = " + msg.getType());
                    if((msg.getType()).equals("5")) {
                        expired = true;
                    }
                }
            }
        }
    }
    public void receiveCyoError(int type) {
    }
}

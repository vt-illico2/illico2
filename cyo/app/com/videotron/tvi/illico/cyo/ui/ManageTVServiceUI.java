/**
 * @(#)ManageTVServiceUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.cyo.data.obj.*;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.gui.ManageTVServiceRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ManageTVServiceUI extends BaseUI implements CyoDataListener {
    public static final int STATE_LIST = 0;
    public static final int STATE_MENU = 1;
    public static final int STATE_BUTTON = 2;

    public static boolean subDataChanged = false;
    public boolean buttonClick = false;

    private ClickingEffect clickEffect;

    private ValidationErrorPopupUI vPop = new ValidationErrorPopupUI();

    private ManageTVServiceListUI listUI = new ManageTVServiceListUI();
    private Services services;

    private int btnFocus = 0;
    private String dollar = "00";
    private String cent = "00";
    private Vector messagePopupVector = new Vector();
    private String lastListIndex = null;
    
    private boolean isBack = false;

    public ManageTVServiceUI() {
        renderer = new ManageTVServiceRenderer();
        setRenderer(renderer);

        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
    }

    public void start(boolean back) {
    	isBack = back;
        if (!back) {
            prepare();
            DataCenter.getInstance().remove("BasicService");
        } else {
            super.prepare();
            lastUIId = CyoController.getInstance().getCurrentId();
//            lastUIId = getLastUIId();
        }
        if (messagePopupVector == null) {
            messagePopupVector = new Vector();
        }

        // if(lastUIId == BaseUI.UI_PREVALIDATION) {
        // subDataChanged = true;
        // }

        CyoDataManager.getInstance().addCyoDataListener(this);
        CyoDataManager.getInstance().request(URLType.LOAD_OVERVIEW, null);
        super.start(back);
        Log.printInfo("state = " + state + " focus = " + focus);
        repaint();
    }

    public void prepare() {
        btnFocus = 0;
        focus = 0;
        state = STATE_LIST;
        dollar = "00";
        cent = "00";
        super.prepare();
    }

    public void stop() {
        clearPopup();
        if (messagePopupVector != null) {
            messagePopupVector.clear();
        }
        Log.printInfo("stop ");
        listUI.setCyoObject(null);

        super.stop();
    }

    public void deleteData() {
        services = null;

        vPop.stop();
        listUI.stop();
    }

    public void dispose() {
        messagePopupVector = null;
        clickEffect = null;
        services = null;

        vPop.stop();
        listUI.stop();

        super.dispose();
    }

    public boolean handleKey(int code) {

        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case OCRcEvent.VK_LEFT:
            keyLeft();
            return true;
        case OCRcEvent.VK_RIGHT:
            keyRight();
            return true;
        case OCRcEvent.VK_ENTER:
            keyEnter();
            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0);
            Log.printInfo("subDataChanged " + subDataChanged);
            if (subDataChanged || lastUIId == BaseUI.UI_VALIDATION) {
                setPopup(notiPop);
                notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
            } else {
                subDataChanged = false;
                lastListIndex = null;
                CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
            }
            repaint();
            // setPopup(notiPop);
            // notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
            return true;
        default:
            return false;
        }
    }

    private void keyLeft() {
        if (state == STATE_MENU && listUI.getCyoObjectLength() > 0) {
            state = STATE_LIST;
            listUI.setHasFocus(true);
            repaint();
        } else if (state == STATE_BUTTON && listUI.getCyoObjectLength() > 0) {
            state = STATE_LIST;
            listUI.setHasFocus(true);
            btnFocus = 0;
            repaint();
        }

    }

    private void keyRight() {
        if (state == STATE_LIST) {
            state = STATE_BUTTON;
            btnFocus = 0;
            listUI.setHasFocus(false);
            repaint();
        }
    }

    private void keyUp() {
        if (state == STATE_LIST) {
            listUI.handleKey(OCRcEvent.VK_UP);
            repaint();
        } else if (state == STATE_MENU) {
            if (--focus < 0) {
                focus = 0;
            }
            repaint();
        } else if (state == STATE_BUTTON) {
            if (--btnFocus < 0) {
                btnFocus = 0;
                focus = 1;
                state = STATE_MENU;
            }

            repaint();
        }
    }

    private void keyDown() {
        if (state == STATE_LIST) {
            listUI.handleKey(OCRcEvent.VK_DOWN);
            repaint();
        } else if (state == STATE_MENU) {

            if (++focus > 1) {
                focus = 1;
                state = STATE_BUTTON;
            }
            repaint();

        } else if (state == STATE_BUTTON) {
            if (++btnFocus > 2) {
                btnFocus = 2;
            }
            repaint();
        }
    }

    private void keyEnter() {
        if (state == STATE_LIST) {
            CyoObject curObj = listUI.getCurrentCyoObject();

            if (curObj instanceof Group) {

                lastListIndex = ((Group) curObj).getNo();

                int selfIdx = ((Group) curObj).getUrl().indexOf("LoadSelfServe");
                listUI.keyEnter();
                if (selfIdx > -1) { // service de base
                    Log.printInfo("SelfServe =");
                    DataCenter.getInstance().put("SelfServe", curObj);
                    CyoController.getInstance().showUI(BaseUI.UI_SELF_SERVE, false);
                } else {
                    int gameIdx = ((Group) curObj).getUrl().indexOf("LoadGames");
                    if (gameIdx > -1) {
                        String str = DataCenter.getInstance().getString("gotogame");

                        if (str == null || str.equals("false")) {
                            setPopup(notiPop);
                            notiPop.setExplain(BaseUI.getExplainText(KeyNames.NOT_GOTO_GAME));
                            notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                        } else {
                            DataCenter.getInstance().put("Game", curObj);
                            CyoController.getInstance().showUI(BaseUI.UI_GAME_PACKAGES, false);
                        }
                    // VDTRMASTER-5734
                    } else if (((Group) curObj).getNo().equals("1516") || ((Group) curObj).getNo().equals("1553")) { // CFC said, 1516 is Popular Packages.
                        //                    } else if (((Group) curObj).getNo().equals("1516")) { // CFC said, 1516 is Popular Packages.
                        // popular packages
                        DataCenter.getInstance().put("PopularPackages", curObj);
                        CyoController.getInstance().showUI(BaseUI.UI_POPULAR_PACKAGES, false);
                    } else if (((Group) curObj).getNo().equals("1565")) {

                        DataCenter.getInstance().put("CHANNEL_BLOC", curObj);
                        CyoController.getInstance().showUI(BaseUI.UI_CHANNEL_BLOC_LIST, false);
                    } else if (((Group) curObj).getNo().equals("1564")) {
                        CyoController.getInstance().showUI(BaseUI.UI_TV_PACKAGE_LIST, false);
                    } else {
                        Log.printInfo("LoadGroup");

                        if ((((Group) curObj).getNo()).equals("1514") && "false".equals(((Group)curObj).getModifiable())) {
							DataCenter.getInstance().put("BasicService", curObj);
							CyoController.getInstance().showUI(BaseUI.UI_CHANNEL_LIST_BASIC, false);
						} else {
							DataCenter.getInstance().put("LoadGroup", curObj);
							CyoController.getInstance().showUI(BaseUI.UI_MANAGE_GROUP, false);
						}
						
                        
//                        DataCenter.getInstance().put("LoadGroup", curObj);
//                        CyoController.getInstance().showUI(BaseUI.UI_MANAGE_GROUP, false);
                    }
                }

            }
        } else if (state == STATE_MENU) {
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }
            clickEffect.start(642, 227 + focus * 42, 252, 36);
            lastListIndex = null;
            if (focus == 0) { // See details
                CyoController.getInstance().showUI(BaseUI.UI_SEE_DETAILS, false);
            } else if (focus == 1) { // List of channels
                CyoController.getInstance().showUI(BaseUI.UI_CHANNEL_LIST_NEW, false);
            }
        } else if (state == STATE_BUTTON) {
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }
            clickEffect.start(624, 341 + btnFocus * 39, 292, 40);
            Log.printDebug("lastUIId = " + lastUIId);
            if (btnFocus == 0) { // Confirm
                buttonClick = true;
                if (CyoDataManager.getInstance().needParamForRequiresUpsell()) {
                    Hashtable param = new Hashtable();
                    param.put("requiresUpsell", "false");
                    CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                    CyoDataManager.getInstance().request(URLType.VALIDATION, param);
                } else {
                    CyoDataManager.getInstance().request(URLType.VALIDATION, null);
                }
            } else if (btnFocus == 1) { // Cancel
                if (subDataChanged || lastUIId == BaseUI.UI_VALIDATION) {
                    setPopup(notiPop);
                    notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
                } else {
                    subDataChanged = false;
                    lastListIndex = null;
                    CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                    CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
                }
            } else if (btnFocus == 2) { // Help
                DataCenter.getInstance().put("MODE", "HELP");
                CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
            }
        }
        repaint();
    }

    public void popupClosed(BaseUI pop, Object msg) {
        super.popupClosed(pop, msg);
        removePopup(pop);
        repaint();
        
        if(gotoHotKey) {
            gotoHotKey = false;
            super.popupClosed(pop, msg);
            return;
        }
        if (messagePopupVector != null && messagePopupVector.size() > 0) {
            Message message = (Message) messagePopupVector.remove(0);

            if (message.getType().equals("3")) {
            	if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5696
					setPopup(infoPopup);
					infoPopup.show(this, message);
					notiPop.setExplain(null);
				} else if (message.getId().equals("62")) {
					setPopup(infoPopup);
					infoPopup.show(this, message);
				} else {
	                // show CYO_13
	                if (message.getId().equals("35")) {
	                    vPop.setIcon(true);
	                }
	                setPopup(vPop);
	                vPop.show(this, message.getText());
				}
            } else if ((message.getType()).equals("5")) {
                setPopup(notiPop);
                notiPop.setExplain(message.getText());
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                expired = true;
            } else {
                // show CYO_14
                setPopup(notiPop);
                notiPop.setExplain(message.getText());
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
            }
            return;
        }

        if (pop instanceof NotificationPopupUI) {
            if (expired) {
                cyoReStart();
            } else if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
                Boolean result = (Boolean) msg;

                if (result.booleanValue()) {
                    subDataChanged = false;
                    CyoDataManager.getInstance().request(URLType.LOGIN, null);
                } else {
                    // CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
                }
            } else if (pop.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
                Integer result = (Integer) msg;
                if (result.intValue() == 0) {
                    buttonClick = false;
                    if (CyoDataManager.getInstance().needParamForRequiresUpsell()) {
                        Hashtable param = new Hashtable();
                        param.put("requiresUpsell", "false");
                        CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                        CyoDataManager.getInstance().request(URLType.VALIDATION, param);
                    } else {
                        CyoDataManager.getInstance().request(URLType.VALIDATION, null);
                    }
                } else if (result.intValue() == 1) {
                    deleteData();
                    subDataChanged = false;
                    lastListIndex = null;
                    CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                    CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
                } else if (result.intValue() == 2) {
                }
            }
        } else if (pop.equals(validationUpSellPopupUI)) {
            Boolean result = (Boolean) msg;
            if (result.booleanValue()) {
                Message message = validationUpSellPopupUI.getMessage();

                DataCenter.getInstance().put("VALIDATION_MESSAGE", message);

                if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_PACKAGE) || message
                        .getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {

                    CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);

                } else if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_A_LA_CARTE)) {
                    // NOTHING;
                }

            } else {
                Hashtable param = new Hashtable();
                param.put("requiresUpsell", "false");
                CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                CyoDataManager.getInstance().request(URLType.VALIDATION, param);
            }
        }
        repaint();
        // super.popupClosed(pop, msg);
    }

    public int getBtnFocus() {
        return btnFocus;
    }

    public String getPriceDollar() {
        return dollar;
    }

    public String getPriceCent() {
        return cent;
    }

    public ManageTVServiceListUI getList() {
        return listUI;
    }

    /** The object of Service de base package */
    private CyoObject baseObj;

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.INFO_ON) {
            Log.printInfo("ManageTVServiceUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
                    + obj);
        }

        if (type == URLType.LOGIN) {
            InitialStateUI.shortcuts = (Shortcuts) obj;

            if (InitialStateUI.shortcuts.getSuccess().equals("1")) {
                lastListIndex = null;
                CyoController.getInstance().disposeScene(BaseUI.UI_CHANGE_CHANNEL_SELECTION);
                CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
                deleteData();
            } else {
                // POPUP
                if (InitialStateUI.shortcuts.getCyoObject() != null) {
                    Message msg = (Message) InitialStateUI.shortcuts.getCyoObject()[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                }
            }
            CyoController.getInstance().hideLoadingAnimation();
        } else if (type == URLType.LOAD_FEES) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ManageTVServiceUI: receiveCyoData(): LOAD_FEES success = " + obj.getSuccess());
            }

            if (obj.getSuccess().equals("1")) {
                Services services = (Services) obj;
                CyoObject[] objs = services.getCyoObject();

                baseObj = objs[0].getCyoObject()[0];
                DataCenter.getInstance().put("Package", baseObj);
                CyoController.getInstance().showUI(BaseUI.UI_CHANNEL_LIST_BASIC, false);
                repaint();
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

                    Log.printInfo("msg.getType() = " + msg.getType());
                    if ((msg.getType()).equals("5")) {
                        expired = true;
                    }
                }
            }
        } else if (type == URLType.LOAD_OVERVIEW) {
            if (obj.getSuccess().equals("1")) {

                DataCenter.getInstance().put("LOAD_OVERVIEW", obj);

                services = (Services) obj;

//                String group1Title = BaseUI.getMenuText(KeyNames.YOUR_TV_SERVICE);
//                String group2Title = BaseUI.getMenuText(KeyNames.ADD_NEW_PACKAGE_CHANNELS);

                CyoObject[] objs = services.getCyoObject();

//                ((Group) objs[0]).setTitle(group1Title);
//                ((Group) objs[1]).setTitle(group2Title);

                Vector dataVec = new Vector();

//                dataVec.add(((Group) objs[0]));
                CyoObject[] sub1Objs = objs[0].getCyoObject();

                if (sub1Objs != null) {
                    for (int i = 0; i < sub1Objs.length; i++) {
                        dataVec.add(sub1Objs[i]);
                    }
                }

                CyoObject[] sub2Objs = objs[1].getCyoObject();
                Log.printInfo("sub2Objs =" + sub2Objs);
//                if (sub2Objs != null && sub2Objs.length > 0) {
//                    dataVec.add(((Group) objs[1]));
//                    Log.printInfo("add sub2Objs =");
//                    listUI.setDownList(true);
//                } else {
//                    listUI.setDownList(false);
//                }

                if (sub2Objs != null) {
                    Log.printInfo("sub2Objs length=" + sub2Objs.length);
                    for (int i = 0; i < sub2Objs.length; i++) {
                        dataVec.add(sub2Objs[i]);
                    }
                }

                CyoObject[] dataObj = new CyoObject[dataVec.size()];
                dataVec.copyInto(dataObj);

                listUI.setCyoObject(dataObj);

                // VDTRMASTER-5969
                if (!isBack || this.state == STATE_LIST) {
                	listUI.setHasFocus(true);
                	this.state = STATE_LIST;
                }
                listUI.start(false);

                int dotIdx = services.getPricetotalM().indexOf(".");
                dollar = services.getPricetotalM().substring(0, dotIdx);
                cent = services.getPricetotalM().substring(dotIdx) + " $";

                if (lastListIndex != null) {
                    Log.printInfo("lastListIndex =" + lastListIndex);
                    for (int a = 0; a < dataObj.length; a++) {
                        if (dataObj[a] instanceof Group && ((Group) dataObj[a]).getNo() != null
                                && ((Group) dataObj[a]).getNo().equals(lastListIndex)) {
                            repaint();
                            break;
                        } else {
                            listUI.keyDown();
                        }
                    }
                }
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                    Log.printInfo("msg.getType() = " + msg.getType());
                    if ((msg.getType()).equals("5")) {
                        expired = true;
                    }
                }
            }

            repaint();
        } else if (type == URLType.VALIDATION) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ManageTVServiceUI: receiveCyoData(): VALIDATION success = " + obj.getSuccess());
            }

            if (obj.getSuccess().equals("1")) {
                if (obj instanceof Messages) {
                    Messages msgs = (Messages) obj;
                    if (msgs.getCyoObject() != null && msgs.getCyoObject()[0] instanceof Message) {
                        Message msg = (Message) msgs.getCyoObject()[0];
                        if (Log.DEBUG_ON) {
                            Log.printDebug(
                                    "ManageTVServiceUI, msg.getOptimizationType()=" + msg.getOptimizationType());
                        }
                        if (msg.getOptimizationType() != null) {
                            CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                            setPopup(validationUpSellPopupUI);
                            validationUpSellPopupUI.show(this, msg);
                        } else {
                            setPopup(notiPop);
                            notiPop.setExplain(msg.getText());
                            notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                        }
                    }
                } else {
                    lastListIndex = null;
                    if (buttonClick) {
                        DataCenter.getInstance().put("VALIDATION", obj);
                        CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                        CyoDataManager.getInstance().request(URLType.COMPARE_SERVICES, null);
                    } else {
                        lastListIndex = null;
                        CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
                        CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
                    }
                    subDataChanged = false;
                }
            } else {
                CyoObject[] subObjs = obj.getCyoObject();
                if (subObjs != null) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ManageTVServiceUI: receiveCyoData: subObjs = " + subObjs);
                    }
                    for (int i = 0; i < subObjs.length; i++) {
                        if (subObjs[i] instanceof Message) {
                            messagePopupVector.add(subObjs[i]);
                        }
                    }
                    if (messagePopupVector != null && messagePopupVector.size() > 0) {
                        Message message = (Message) messagePopupVector.remove(0);

                        if (message.getType().equals("3")) {
                        	if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5696
            					setPopup(infoPopup);
            					infoPopup.show(this, message);
            					notiPop.setExplain(null);
            				} else if (message.getId().equals("62")) {
            					setPopup(infoPopup);
            					infoPopup.show(this, message);
            				} else {
								// show CYO_13
	                        	if (message.getId().equals("35")) {
									vPop.setIcon(true);
								} else {
									vPop.setIcon(false);
								}
								setPopup(vPop);
								vPop.show(this, message.getText());
            				}
						} else if ((message.getType()).equals("5")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							expired = true;
							
						} else if (message.getType().equals("1") && message.getId().startsWith("307")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
						} else if (message.getId().equals("1")) {
							notiPop.setExplain(message.getText());
							String[] proccessedMsg = notiPop.getExplain();
							
							if (Log.DEBUG_ON) {
								if (proccessedMsg != null) {
									Log.printDebug("ManageTVServiceUI: receiveCyoData: proccessedMsg length : " + proccessedMsg.length);
								} else {
									Log.printDebug("ManageTVServiceUI: receiveCyoData: proccessedMsg is null");
								}
							}
							
							if (proccessedMsg == null) {
								vPop.setIcon(false);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else {
								setPopup(notiPop);
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else {
							// show CYO_14
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
                    }
                } else {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ManageTVServiceUI: receiveCyoData: subObjs is null");
                    }
                }
            }

            repaint();
        } else if (type == URLType.COMPARE_SERVICES) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ManageTVServiceUI: receiveCyoData(): COMPARE_SERVICES success = " + obj.getSuccess());
            }

            if (obj.getSuccess().equals("1")) {
                DataCenter.getInstance().put("COMPARE", obj);
                // comment out by VDTRMASTER-5176 [CQ] [PO1/2/3][ECLR][R4][CYO] In CYO-"Consult or manage your TV service plan" changes are not being saved if the customer hit the back button
//                subDataChanged = false;
                if (this.state == STATE_MENU) {
                    CyoController.getInstance().showUI(BaseUI.UI_COMPARE_MY_SERVICES, false);
                } else {
                    lastListIndex = null;
                    CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
                }
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);

                    Log.printInfo("msg.getType() = " + msg.getType());
                    if ((msg.getType()).equals("5")) {
                        expired = true;
                    }
                }
            }
            repaint();
        }
    }

    public void receiveCyoError(int type) {
    }

}

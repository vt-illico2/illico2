package com.videotron.tvi.illico.cyo.ui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Enumeration;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.cyo.gui.InformationRenderer;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

public class InfoPopupUI extends BaseUI {

	public final String FIVE_C_ICON_MODIFIED = "[5_icon]";
	private Message message;
	private ClickingEffect clickEffect;
	private BaseUI parent;
	
	public InfoPopupUI() {
		setRenderer(new InforPopupRenderer());
	}
	
	public void show(BaseUI parent, Message message) {
		this.message = message;
		this.parent = parent;
		
		prepare();
		
		super.start();
	}
	
	public void stop() {
		message = null;
		clickEffect = null;
        super.stop();
    }
	
	public void dispose() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                dataCenter.removeImage(key);
            }
            imageKeys.clear();
        }
    }
	
	public boolean handleKey(int code) {
		Log.printDebug("InfoPopupUI, handleKey code= " + code);
        switch (code) {
        case OCRcEvent.VK_ENTER:
        case OCRcEvent.VK_EXIT:
        	stop();
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }
            clickEffect.start(403, 328, 162, 40);
            parent.popupClosed(this, Boolean.TRUE);
            return true;
        }
		
        return true;
	}
	
	
	class InforPopupRenderer extends BaseRenderer {

		private Image buttonImg, icon5CImg;
		private Color bgColor = new Color(44, 44, 44);
		private Color lineColor = new Color(57, 57, 57);
		private Color exColor = new Color(229, 229, 229);
		
		private String btnOKStr;
		
		
		public Rectangle getPreferredBounds(UIComponent c) {
			return Constants.SCREEN_BOUNDS;
		}

		public void prepare(UIComponent c) {
			loadImages((BaseUI)c);
			
			btnOKStr = BaseUI.getMenuText(KeyNames.BTN_OK);
		}

		protected void paint(Graphics g, UIComponent c) {
			g.setColor(dimmedBgColor);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			
			// popup bg
			g.setColor(bgColor);
			g.fillRect(296, 133, 370, 244);
			g.setColor(lineColor);
			g.fillRect(317, 171, 330, 1);
			
			if (message != null) {
				// title
				g.setColor(priceColor);
				g.setFont(FontResource.BLENDER.getBoldFont(24));
				GraphicUtil.drawStringCenter(g, message.getTitle(), 481, 159);
				
				// explain text
				g.setFont(FontResource.BLENDER.getFont(18));
				g.setColor(exColor);
				String messageStr = TextUtil.replace(message.getText(), FIVE_C_ICON, FIVE_C_ICON_MODIFIED);
				String[] explain = TextUtil.split(messageStr, g.getFontMetrics(), 305);
				
				if (explain.length < 4) {
					 explain = TextUtil.split(messageStr, g.getFontMetrics(), 290);
				}
				
				int y = 248 - explain.length / 2 * 20 + (explain.length%2==1? 10 : 0);
				int fiveCPos = 0;
				FontMetrics fm = g.getFontMetrics();
				for (int i = 0; i < explain.length; i++) {
					int yPos = y + i * 20;
					if (( fiveCPos = explain[i].indexOf(FIVE_C_ICON_MODIFIED)) > -1) {
						String pre = explain[i].substring(0, fiveCPos);
						String sub = explain[i].substring(fiveCPos + FIVE_C_ICON_MODIFIED.length());
						int preWidth = fm.stringWidth(pre);
						int subWidth = fm.stringWidth(sub);
						int tWidth = preWidth + subWidth + 25;
						g.drawString(pre, 480 - tWidth/2, yPos);
						g.drawString(sub, 480 + tWidth/2 - subWidth, yPos);
						
						g.drawImage(icon5CImg, 480 - tWidth/2 + preWidth + 4, yPos - 12, c);
					} else {
						GraphicUtil.drawStringCenter(g, explain[i], 480, yPos);
					}
				}
				
			}
			// button
			g.drawImage(buttonImg, 403, 328, c);
			g.setColor(btnColor);
			g.setFont(FontResource.BLENDER.getFont(18));
			GraphicUtil.drawStringCenter(g, btnOKStr, 481, 349);
		}

		protected void loadImages(BaseUI ui) {
			buttonImg = getImage("05_focus.png");
			icon5CImg = getImage("icon_5.png");
			FrameworkMain.getInstance().getImagePool().waitForAll();
		}
		
	}
}

/**
 * @(#)ChangeChannelSelectionUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.gui.PopularPackagesRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class PopularPackagesUI extends BaseUI implements CyoDataListener {
	public static final int STATE_LIST = 0x0;
	public static final int STATE_MENU = 0x1;
	public static final int STATE_BUTTON = 0x2;

	private ClickingEffect clickEffect;

	private ListUI popularList = new PopularPackagesListUI();
	private ValidationErrorPopupUI vPop = new ValidationErrorPopupUI();

	private Group loadedGroup;
	private Package curPackage;
	private Package lastPackage;

	private Vector addVector = new Vector();

	private int btnFocus = 0;
	private Services services;

	private ListUI currentList;

	private Package noPackage;

	private Vector messagePopupVector = new Vector();

	// public boolean changed = false;
	private boolean saveMessageView = false;
	private String groupModifiable = null;

	public PopularPackagesUI() {
		renderer = new PopularPackagesRenderer();
		setRenderer(renderer);
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);

		if (!back) {
			prepare();
			currentList = null;
			CyoObject obj = (CyoObject) DataCenter.getInstance().get("PopularPackages");
			Group group = (Group) obj;
			Hashtable param = new Hashtable();
			param.put("url", group.getUrl());
			CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
			groupModifiable = null;
		}
		if (messagePopupVector == null) {
			messagePopupVector = new Vector();
		}
		saveMessageView = false;
		super.start(back);
	}

	public void prepare() {
		state = STATE_LIST;
		btnFocus = 0;
		focus = 0;
		setFooter();
		super.prepare();
	}

	public void stop() {
		// popularList.setCyoObject(null);

		if (messagePopupVector != null) {
			messagePopupVector.clear();
			messagePopupVector = null;
		}

		clearPopup();
		super.stop();
	}

	public void dispose() {
		clickEffect = null;
		if (messagePopupVector != null) {
			messagePopupVector.clear();
			messagePopupVector = null;
		}
		addVector.clear();
		popularList.stop();
		vPop.stop();
		super.dispose();
	}

	public boolean handleKey(int code) {

		if (getPopup() != null && getPopup().isVisible()) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case OCRcEvent.VK_PAGE_UP:
			pageUp();
			return true;
		case OCRcEvent.VK_PAGE_DOWN:
			pageDown();
			return true;
		case OCRcEvent.VK_ENTER:
			keyEnter();
			return true;
		case KeyCodes.LAST:
			// Cancel
			footer.clickAnimation(0);
			if (!getChanged()) {
				CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
			} else {
				setPopup(notiPop);
				// notiPop.show(this, NotificationPopupUI.STATE_CANCEL,
				// BaseUI.getMenuText(KeyNames.CANCEL_VALIDATE_TITLE), BaseUI
				// .getExplainText(KeyNames.CANCEL_VALIDATE_EXPLAIN));
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			}
			repaint();
			return true;
		default:
			return false;
		}
	}

	private void keyLeft() {
		if (state == STATE_BUTTON && currentList.getCyoObjectLength() > 0) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
		} else if (state == STATE_MENU) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
		} else if (state == STATE_BUTTON) {
			state = STATE_LIST;
			btnFocus = 0;
		}
		repaint();

	}

	private void keyRight() {
		CyoObject curObj = currentList.getCurrentCyoObject();
		Package curPackage = ((Package) curObj);
		if (curPackage.getSelection() == null) {// &&
												// !curPackage.getSelection().equals(KeyNames.SINGLE)
			if (state == STATE_LIST) {
				state = STATE_MENU;
				currentList.setHasFocus(false);
			}
		} else {
			state = STATE_BUTTON;
			currentList.setHasFocus(false);
			btnFocus = getChanged() ? 0 : 1;
		}
		repaint();
	}

	private void keyUp() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_UP);
		} else if (state == STATE_BUTTON) {
			CyoObject curObj = currentList.getCurrentCyoObject();
			Package curPackage = ((Package) curObj);
			--btnFocus;
			if (curPackage.getSelection() != null) {
				if (btnFocus < 1) {
					btnFocus = getChanged() ? 0 : 1;
				}
			} else {
				if (getChanged()) {
					if (btnFocus < 0) {
						btnFocus = 0;
						state = STATE_MENU;
					}
				} else {
					if (btnFocus < 1) {
						btnFocus = 0;
						state = STATE_MENU;
					}
				}
			}
		}
		repaint();
	}

	private void keyDown() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_DOWN);
		} else if (state == STATE_MENU) {
			state = STATE_BUTTON;
			// if(popularList.getCurrentCyoObject() == null) {
			btnFocus = getChanged() ? 0 : 1;
		} else if (state == STATE_BUTTON) {
			if (++btnFocus > 2) {
				btnFocus = 2;
			}
		}
		repaint();
	}

	private void pageUp() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_UP);
			repaint();
		}
	}

	private void pageDown() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
			repaint();
		}
	}

	private void keyEnter() {
		if (state == STATE_LIST) {

			// remove with VDTRMASTER-3947 & VDTRMASTER-3948
			// if (groupModifiable != null && groupModifiable.equals("false")) {
			// return;
			// }

			CyoObject curObj = currentList.getCurrentCyoObject();
			updateSubscribed(curObj);
			needExitPopup = true;
			if (getChanged()) {
				currentList.setHasFocus(false);
				state = STATE_BUTTON;
				btnFocus = 0;
			}
		} else if (state == STATE_MENU) {
			CyoObject curObj = currentList.getCurrentCyoObject();
			Package curPackage = ((Package) curObj);
			// if (curPackage.getSelection() != null) {// &&
			// !curPackage.getSelection().equals(KeyNames.SINGLE)
			// noPackage.setSubscribedInSession(false);
			// } else {
			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}
			clickEffect.start(718, 237, 172, 30);

			DataCenter.getInstance().put("Package", currentList.getCurrentCyoObject());
			CyoController.getInstance().showUI(BaseUI.UI_VIEW_CHANNELS, false);
			// }
		} else if (state == STATE_BUTTON) {
			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}

			clickEffect.start(717, 346 + btnFocus * 37, 180, 35);

			if (btnFocus == 0) { // Confirm
				if (Log.DEBUG_ON) {
					Log.printDebug("PopularPackagesUI: curPackage is " + curPackage);
				}
				if (curPackage != null && lastPackage == null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("PopularPackagesUI: lastPackage is null.");
					}
					// VDTRMASTER-5958
					CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
					requestSaveAction();
				} else if (curPackage != null && lastPackage != null && !curPackage.equals(lastPackage)) {
					if (Log.DEBUG_ON) {
						Log.printDebug("PopularPackagesUI: curPackage not equals lastPackage.");
					}
					// VDTRMASTER-5958
					CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
					requestSaveAction();
				} else {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else if (btnFocus == 1) { // Cancel
				// if (!changed) {
				if (getChanged()) {
					setPopup(notiPop);
					notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
				} else {
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else if (btnFocus == 2) { // Help
				DataCenter.getInstance().put("MODE", "HELP");
				CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
			}
		}
		repaint();
	}

	private void setFooter() {
		footer.reset();
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
	}

	public String getGroupNo() {
		if (loadedGroup != null) {
			return loadedGroup.getNo();
		}
		return TextUtil.EMPTY_STRING;
	}

	public Group getGroup() {
		return loadedGroup;
	}

	private void updateSubscribed(CyoObject obj) {
		if (obj instanceof Package) {
			Package curPackage = ((Package) obj);
			String modiable = curPackage.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("PopularPackagesUI: Package modiable = " + modiable);
			}
			if (modiable.equals("0")) {
				// Package
				if (curPackage.getSubscribedInSession()) { // &&
															// curPackage.getSelection().equals(KeyNames.SINGLE))
															// {
					return;
				}

				curPackage.setSubscribedInSession(!curPackage.getSubscribedInSession());
				addVector.remove(curPackage);
				addVector.add(curPackage);

				if (curPackage.getSubscribedInSession()) {
					this.curPackage = curPackage;
					CyoObject[] subObj = loadedGroup.getCyoObject();

					if (curPackage.getSelection() == null) {//
						noPackage.setSubscribedInSession(false);
					}

					for (int i = 0; subObj != null && i < subObj.length; i++) {
						if (subObj[i] instanceof Package) {
							if (!subObj[i].equals(curPackage)) {

//								if (((Package) subObj[i]).getSubscribedInSession()) {
//									lastPackage = (Package) subObj[i];
//								}

								((Package) subObj[i]).setSubscribedInSession(false);
								addVector.remove(subObj[i]);
							}
						}
					}
				} else {
					this.curPackage = null;
				}
			}
		}
	}

	public int getButtonFocus() {
		return btnFocus;
	}

	public ListUI getCurrentList() {
		return currentList;
	}

	private void requestSaveAction() {
		String groupNo = null;
		Hashtable param = new Hashtable();

		groupNo = loadedGroup.getNo();

		if (curPackage != null && curPackage.getSelection() == null) {
			param.put("add", curPackage.getNo());
		}
		if (lastPackage != null) {
			param.put("remove", lastPackage.getNo());
		}
		param.put("groupNo", groupNo);

		if (curPackage != null || lastPackage != null) {
			saveMessageView = false;
			CyoDataManager.getInstance().request(URLType.SAVE_GROUP, param);
		}
	}

	private void buildDataForList(CyoObject[] data) {
		Vector dataVector = new Vector();
		addCyoObject(dataVector, data);

		CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
		dataVector.copyInto(rearrangeData);

		popularList.setCyoObject(rearrangeData);
		popularList.setHasFocus(true);
		popularList.start(true);

		dataVector.clear();

		currentList = popularList;

	}

	private void addCyoObject(Vector vec, CyoObject[] data) {

		noPackage = new Package();
		noPackage.setTitle(BaseUI.getMenuText(KeyNames.NO_PACKAGE));
		noPackage.setModifiable("0");
		noPackage.setSubscribedInAccount(true);
		noPackage.setSubscribedInSession(true);
		noPackage.setSelection(KeyNames.SINGLE);
		noPackage.setSubscribedOrginalInSession(String.valueOf(noPackage.getSubscribedInSession()));
		vec.add(noPackage);
		for (int i = 0; i < data.length; i++) {
			vec.add(data[i]);

			if (data[i] instanceof Package) {
				if (((Package) data[i]).getSubscribedInSession()) {
					lastPackage = (Package) data[i];
					lastPackage.setSubscribedOrginalInSession(String.valueOf(lastPackage.getSubscribedInSession()));
					if (lastPackage.getSubscribedInSession()) {
						noPackage.setSubscribedInAccount(false);
						noPackage.setSubscribedInSession(false);
						noPackage.setSubscribedOrginalInSession(String.valueOf(false));
					}

					if (Log.DEBUG_ON) {
						Log.printDebug("PopularPackagesUI: find last Package : " + lastPackage.getTitle());
					}
				} else {
					Package temp = (Package) data[i];
					temp.setSubscribedOrginalInSession(String.valueOf(temp.getSubscribedInSession()));
				}
			}
		}
	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);
		removePopup(pop);
		if (gotoHotKey) {
			gotoHotKey = false;
			super.popupClosed(pop, msg);
			return;
		}
		if (messagePopupVector != null && messagePopupVector.size() > 0) {
			Message message = (Message) messagePopupVector.remove(0);

			if (message.getType().equals("3")) {
				if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5696
					setPopup(infoPopup);
					infoPopup.show(this, message);
					notiPop.setExplain(null);
				} else if (message.getId().equals("62")) {
					setPopup(infoPopup);
					infoPopup.show(this, message);
				} else {
					// show CYO_13
					if (message.getId().equals("35")) {
						vPop.setIcon(true);
					} else {
						vPop.setIcon(false);
					}
					setPopup(vPop);
					vPop.show(this, message.getText());
				}
			} else if ((message.getType()).equals("5")) {
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				expired = true;
			} else if (message.getType().equals("1") && message.getType().startsWith("307")) {
				// show CYO_14
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
			} else {
				notiPop.setExplain(message.getText());
				String[] proccessedMsg = notiPop.getExplain();
				
				if (proccessedMsg == null) {
					vPop.setIcon(false);
					setPopup(vPop);
					vPop.show(this, message.getText());
				} else {
					setPopup(notiPop);
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
			}
			repaint();
			return;
		}

		if (expired) {
			cyoReStart();
		} else if (pop instanceof ValidationErrorPopupUI) {
			if (getChanged()) {
				ManageTVServiceUI.subDataChanged = true;
			}
			dispose();
			CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
			repaint();
		} else if (pop instanceof NotificationPopupUI) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PopularPackagesUI: pop instanceof NotificastionPopupUI");
				Log.printDebug("PopularPackagesUI: pop state = " + pop.getState());

			}
			if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					dispose();
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
				Integer result = (Integer) msg;
				if (result.intValue() == 0) {
					requestSaveAction();
				} else if (result.intValue() == 1) {
					dispose();
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				} else if (result.intValue() == 2) {

				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL) {
				if (saveMessageView) {
					ManageTVServiceUI.subDataChanged = true;
				}
				dispose();
				CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
			} else if (pop.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
				ManageTVServiceUI.subDataChanged = true;
				dispose();
				CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
			}

			repaint();
		} else if (pop.equals(validationUpSellPopupUI)) {
			Boolean result = (Boolean) msg;
			if (result.booleanValue()) {
				Message message = validationUpSellPopupUI.getMessage();

				DataCenter.getInstance().put("VALIDATION_MESSAGE", message);

				if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_PACKAGE) || message
						.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {

					CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);

				} else if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_A_LA_CARTE)) {
					// NOTHING;
				}

			} else {
				CyoDataManager.getInstance().setNeedParamForRequiresUpsell(true);
				requestSaveAction();
			}
		}
	}

	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo("PopularPackagesUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
					+ obj);

		}

		if (type == URLType.LOAD_GROUP) {
			if (obj instanceof Services) {
				if (obj.getSuccess().equals("1")) {
					services = (Services) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug("PopularPackagesUI: receiveCyoData: cyoData length = "
								+ services.getCyoObject().length);
					}
					CyoObject[] groups = services.getCyoObject();
					if (groups[0] instanceof Group) {
						loadedGroup = (Group) groups[0];
						groupModifiable = loadedGroup.getModifiable();
						renderer.prepare(this);
						if (Log.DEBUG_ON) {
							Log.printDebug("PopularPackagesUI: receiveCyoData: groups length = " + groups.length);
							Log.printDebug("PopularPackagesUI: receiveCyoData: groups modifiable = " + groupModifiable);
							Log.printDebug("PopularPackagesUI: receiveCyoData: groups[0].getCyoObject length = "
									+ groups[0].getCyoObject().length);
						}
						buildDataForList(groups[0].getCyoObject());
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("PopularPackagesUI: receiveCyoData: obj = " + obj);
						}
					}
				} else {
					CyoObject[] objs = obj.getCyoObject();

					if (objs != null && objs[0] != null) {
						Message msg = (Message) objs[0];
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						Log.printInfo("msg.getType() = " + msg.getType());
						if ((msg.getType()).equals("5")) {
							expired = true;
						}
					}
				}
			}

			repaint();
		} else if (type == URLType.SAVE_GROUP) {
			if (obj.getSuccess().equals("1")) {

				CyoObject[] subObjs = obj.getCyoObject();
				if (subObjs != null) {

					for (int i = 0; i < subObjs.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug("PopularPackagesUI: receiveCyoData: subObjs[" + i + "] = "
									+ subObjs[i]);
						}

						if (subObjs[i] instanceof Message) {
							messagePopupVector.add(subObjs[i]);
						}
					}

					StringBuffer sf = new StringBuffer();

					if (messagePopupVector != null && messagePopupVector.size() > 0) {
						Message message = (Message) messagePopupVector.remove(0);
						Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());

						if (Log.DEBUG_ON) {
							Log.printDebug("PopularPackagesUI: receiveCyoData: message = " + message);
							Log.printDebug("PopularPackagesUI: receiveCyoData: message = " + message.getId());
						}

						if (message.getType().equals("0")) {// &&
															// message.getId().equals("140")
							// if (message.getType().equals("0") &&
							// message.getId().equals("140")) {
							sf.append(message.getText());
							sf.append("\n");
							if (!saveMessageView) {
								saveMessageView = true;
								setPopup(notiPop);
								notiPop.setExplain(sf.toString());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else if (message.getType().equals("3")) {
							Log.printDebug("message id = " + message.getId());
							if (message.getId().equals("62")) {
								setPopup(infoPopup);
								infoPopup.show(this, message);
							} else {
								if (message.getId().equals("35")) {
									vPop.setIcon(true);
								} else {
									vPop.setIcon(false);
								}
								setPopup(vPop);
								vPop.show(this, message.getText());
							}
						} else if ((message.getType()).equals("5")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							expired = true;
						} else if (message.getType().equals("1") && message.getId().startsWith("307")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
						} else if (message.getType().equals("1")) {
							notiPop.setExplain(message.getText());
							String[] proccessedMsg = notiPop.getExplain();
							
							if (Log.DEBUG_ON) {
								if (proccessedMsg != null) {
									Log.printDebug("PopularPackagesUI: receiveCyoData: proccessedMsg length : " + proccessedMsg.length);
								} else {
									Log.printDebug("PopularPackagesUI: receiveCyoData: proccessedMsg is null");
								}
							}
							
							if (proccessedMsg == null) {
								vPop.setIcon(false);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else {
								setPopup(notiPop);
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else {
							dispose();
							ManageTVServiceUI.subDataChanged = true;
							CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
						}
					}

				} else {
					dispose();
					ManageTVServiceUI.subDataChanged = true;
					CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
				}
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null) {

					for (int i = 0; i < objs.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug("PopularPackagesUI: receiveCyoData: subObjs[" + i + "] = "
									+ objs[i]);
						}

						if (objs[i] instanceof Message) {
							messagePopupVector.add(objs[i]);
						}
					}

					StringBuffer sf = new StringBuffer();

					if (messagePopupVector != null && messagePopupVector.size() > 0) {
						Message message = (Message) messagePopupVector.remove(0);
						Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());

						if (Log.DEBUG_ON) {
							Log.printDebug("PopularPackagesUI: receiveCyoData: message = " + message);
							Log.printDebug("PopularPackagesUI: receiveCyoData: message = " + message.getId());
						}

						if (message.getType().equals("0")) {// &&
															// message.getId().equals("140")
							// if (message.getType().equals("0") &&
							// message.getId().equals("140")) {
							sf.append(message.getText());
							sf.append("\n");
							if (!saveMessageView) {
								saveMessageView = true;
								setPopup(notiPop);
								notiPop.setExplain(sf.toString());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else if (message.getType().equals("3")) {
							Log.printDebug("message id = " + message.getId());
							if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5696
								setPopup(infoPopup);
								infoPopup.show(this, message);
								notiPop.setExplain(null);
							} else if (message.getId().equals("62")) {
								setPopup(infoPopup);
								infoPopup.show(this, message);
							} else {
								if (message.getId().equals("35")) {
									vPop.setIcon(true);
								} else {
									vPop.setIcon(false);
								}
								setPopup(vPop);
								vPop.show(this, message.getText());
							}
						} else if ((message.getType()).equals("5")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							expired = true;
						} else if (message.getType().equals("1") && message.getId().startsWith("307")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
						} else if (message.getType().equals("1")) {
							notiPop.setExplain(message.getText());
							String[] proccessedMsg = notiPop.getExplain();
							
							if (Log.DEBUG_ON) {
								if (proccessedMsg != null) {
									Log.printDebug("PopularPackagesUI: receiveCyoData: proccessedMsg length : " + proccessedMsg.length);
								} else {
									Log.printDebug("PopularPackagesUI: receiveCyoData: proccessedMsg is null");
								}
							}

							if (Log.DEBUG_ON) {
								Log.printDebug(
										"PopularPackagesUI, message.getOptimizationType()=" + message.getOptimizationType());
							}
							if (message.getOptimizationType() != null) {
								CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
								setPopup(validationUpSellPopupUI);
								validationUpSellPopupUI.show(this, message);
							} else if (proccessedMsg == null) {
								vPop.setIcon(false);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else {
								setPopup(notiPop);
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
					}

				}
			}
		} else if (type == URLType.VALIDATION) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ManageGroupUI: receiveCyoData: VALIDATION success = " + obj.getSuccess());
			}
			if (obj instanceof Services) {
				Services services = (Services) obj;
				if (services.getSuccess().equals("1")) {
					stop();
					if (services.getMessage() != null) {
						setPopup(notiPop);
						notiPop.setExplain(services.getMessage()[0].getText());
						notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
					} else {
						DataCenter.getInstance().put("VALIDATION", obj);
						CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
					}
				} else {
					CyoObject[] subObjs = services.getCyoObject();
					if (subObjs != null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ManageGroupUI: receiveCyoData: subObjs = " + subObjs);
						}
						for (int i = 0; i < subObjs.length; i++) {
							if (Log.DEBUG_ON) {
								Log.printDebug(
										"ManageGroupUI: receiveCyoData: subObjs[" + i + "] = " + subObjs[i]);
							}

							if (subObjs[i] instanceof Message) {
								messagePopupVector.add(subObjs[i]);
							}
						}
						if (messagePopupVector != null && messagePopupVector.size() > 0) {
							Message message = (Message) messagePopupVector.remove(0);

							if (Log.DEBUG_ON) {
								Log.printDebug("ManageGroupUI: receiveCyoData: message = " + message);
								Log.printDebug(
										"ManageGroupUI: receiveCyoData: message = " + message.getId());
							}

							notiPop.setExplain(message.getText());
							String[] proccessedMsg = notiPop.getExplain();

							if (message.getType().equals("3")) {
								// show CYO_13
								if (message.getId().equals("35")) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(true);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
									// VDTRMASTER-5747
									notiPop.stop();

									setPopup(infoPopup);
									infoPopup.show(this, message);
									notiPop.setExplain(null);
								} else if (proccessedMsg == null) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(false);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else {
									setPopup(notiPop);
									//notiPop.setExplain(message.getText());
									notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								}
							} else if ((message.getType()).equals("5")) {
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								expired = true;
							} else if (message.getId().equals("1")) {
								setPopup(notiPop);
								notiPop.setExplain(services.getMessage()[0].getText());
								notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
							} else {
								// show CYO_14
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						}
						repaint();
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("ManageGroupUI: receiveCyoData: subObjs is null");
						}
					}
				}
			} else if (obj instanceof Messages) {
				// Validataion big popup
				Messages msgs = (Messages) obj;

				if (msgs.getSuccess().equals("1")) {
					if (msgs.getCyoObject() != null && msgs.getCyoObject()[0] instanceof Message) {
						Message msg = (Message) msgs.getCyoObject()[0];
						if (Log.DEBUG_ON) {
							Log.printDebug(
									"ManageGroupUI, msg.getOptimizationType()=" + msg.getOptimizationType());
						}
						if (msg.getOptimizationType() != null) {
							CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
							setPopup(validationUpSellPopupUI);
							validationUpSellPopupUI.show(this, msg);
						} else {
							setPopup(notiPop);
							notiPop.setExplain(msg.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
					}
				}
			}
		}
	}

	public void receiveCyoError(int type) {
	}

	public ListUI getPopularList() {
		return popularList;
	}

	public boolean getChanged() {
		Log.printDebug(" addVector.size() " + addVector.size());
		for (int a = 0; a < addVector.size(); a++) {
			CyoObject obj = (CyoObject) addVector.elementAt(a);
			if (obj instanceof Subscription) {
				Subscription sobj = (Subscription) obj;
				Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
				Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
				if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
					return true;
				}
			}
		}

		return false;
	}
}

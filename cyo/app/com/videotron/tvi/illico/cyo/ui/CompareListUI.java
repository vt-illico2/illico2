/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.gui.CompareListRenderer;
import com.videotron.tvi.illico.log.Log;

/**
 * ListUI display a list of service, group, package, channel etc.
 * @author Woojung Kim
 * @version 1.1
 */
public class CompareListUI extends BaseUI {

    private int startIdx;
    private boolean showScroll;

    public CompareListUI(boolean scroll) {
        renderer = new CompareListRenderer();
        setRenderer(renderer);
        setShowScroll(scroll);
    }

    /** The current items of List. */
    private CyoObject[] objs;

    public void start(boolean back) {
        prepare();
        startIdx = 0;
        focus = 0;
        super.start(back);
    }

    public void stop() {
        super.stop();
    }

    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_PAGE_UP:
            if (needScroll()) {
                if (--startIdx < 0) {
                    startIdx = 0;
                }
            }
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            if (needScroll()) {
                if (++startIdx == objs.length - 6) {
                    startIdx--;
                }
            }
            return true;
        default:
            return false;
        }
    }

    public int getStartIdx() {
        return startIdx;
    }

    public CyoObject getCyoObject(int idx) {
        if (objs != null && idx < objs.length && objs[idx] != null) {
            return objs[idx];
        }

        return null;
    }

    public CyoObject getCurrentCyoObject() {
        int idx = startIdx + focus;
        if (objs != null && idx < objs.length && objs[idx] != null) {
            return objs[idx];
        }
        return null;
    }

    public int getCyoObjectLength() {
        if (objs != null) {
            return objs.length;
        } else {
            return 0;
        }
    }

    public void setCyoObject(CyoObject[] cyoObjs) {
        this.objs = cyoObjs;
    }

    private void setShowScroll(boolean show) {
        showScroll = show;
    }

    public int getScrollGap() {
        if (startIdx == 0) {
            return 0;
        } else {
            int gap = 102 * (startIdx * 100 / (objs.length - 7)) / 100;
            return gap;
        }
    }
    
    public boolean showScroll() {
        return showScroll;
    }

    public boolean needScroll() {
        if (!showScroll) {
            return false;
        } else if (objs != null && objs.length > 6) {
            return true;
        }
        return false;
    }
}

/**
 * @(#)Base.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.awt.Image;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The BaseUI component is a parent class of All UI class.
 * @author Woojung Kim
 * @version 1.1
 */
public class BaseUI extends UIComponent implements ClockListener {

    public static DataCenter dataCenter = DataCenter.getInstance();
    protected BaseRenderer renderer;

    public static final int UI_INITIAL_STATE = 0;
    public static final int UI_CHANGE_CHANNEL_SELECTION = 1;
    public static final int UI_PREVALIDATION = 2;
    public static final int UI_MANAGE_TV_SERVICE = 3;
    public static final int UI_SELF_SERVE = 4;
    public static final int UI_MANAGE_GROUP = 5;
    public static final int UI_POPULAR_PACKAGES = 6;
    public static final int UI_VIEW_CHANNELS = 7;
    public static final int UI_GAME_PACKAGES = 8;
    public static final int UI_VIEW_GAMES = 9;
    public static final int UI_SELECT_GAMES = 10;
    public static final int UI_VALIDATION = 11;
    public static final int UI_CONFIRMATION = 12;
    public static final int UI_SEE_DETAILS = 13;
    public static final int UI_CHANNEL_LIST_BASIC = 14;
    public static final int UI_CHANNEL_LIST_NEW = 15;
    public static final int UI_COMPARE_MY_SERVICES = 16;
    public static final int UI_INFORMATION = 17;
    public static final int UI_TV_PACKAGE_LIST = 18;
    public static final int UI_PACKAGE_CHANNEL_LIST = 19;
    public static final int UI_CHANNEL_BLOC_LIST = 20;

    public static final String BTN_BACK = KeyNames.BTN_BACK;
    
    public static final String FIVE_C_ICON = "[5 icon]"; // VDTRMASTER-5667

    protected Vector imageKeys = new Vector();

    protected Footer footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
    public static NotificationPopupUI notiPop = new NotificationPopupUI();
    protected static NotificationPopupUI exitPop = new NotificationPopupUI();
    protected static ValidationErrorPopupUI vPop = new ValidationErrorPopupUI();

    private static Hashtable footerImgTable;

    protected BaseUI popup;

    private static String clockString;

    private static boolean isAddedClockListener = false;

    protected int lastUIId;

    protected static int keyCode;

    protected static boolean gotoHotKey = false;

    protected static boolean expired = false;

    /** Indicate to need or not a popup for exit. */
    protected static boolean needExitPopup;
    
    // R5-SM5
    protected static InfoPopupUI infoPopup= new InfoPopupUI();
    protected static boolean needDisplay5CPopup = true;

    // Bundling
    protected static IncludedChannelsPopupUI includedChannelsPopup = new IncludedChannelsPopupUI();
    protected static ValidationUpSellPopupUI validationUpSellPopupUI = new ValidationUpSellPopupUI();

    /**
     *
     */
    public BaseUI() {
    }

    public void start(boolean back) {
        start();
    }

    public final void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("BaseUI: start().");
        }

        if (notiPop == null) {
            notiPop = new NotificationPopupUI();
        }

        if (exitPop == null) {
            exitPop = new NotificationPopupUI();
        }

        if (vPop == null) {
            vPop = new ValidationErrorPopupUI();
        }
        
        // R5-SM5
        if (infoPopup == null) {
        	infoPopup = new InfoPopupUI();
        }

        // Bundling
        if (includedChannelsPopup == null) {
            includedChannelsPopup = new IncludedChannelsPopupUI();
        }

        if (validationUpSellPopupUI == null) {
            validationUpSellPopupUI = new ValidationUpSellPopupUI();
        }

        addClockListener();
        add(footer);
        setVisible(true);
    }

    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("BaseUI: stop().");
        }

        if (isAddedClockListener) {
            Clock.getInstance().removeClockListener(this);
            isAddedClockListener = false;
        }
        remove(footer);
        setVisible(false);
    }

    public void dispose() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                dataCenter.removeImage(key);
            }
            imageKeys.clear();
        }

        if (notiPop != null) {
            notiPop.stop();
            notiPop.dispose();
            notiPop = null;
        }

        if (exitPop != null) {
            exitPop.stop();
            exitPop.dispose();
            exitPop = null;
        }

        if (vPop != null) {
            vPop.stop();
            vPop.dispose();
            vPop = null;
        }
        
        // R5-SM5
        if (infoPopup != null) {
        	infoPopup.stop();
        	infoPopup.dispose();
        	infoPopup = null;
        }

        // Bundling
        if (includedChannelsPopup != null) {
            includedChannelsPopup.stop();
            includedChannelsPopup = null;
        }

        if (validationUpSellPopupUI != null) {
            validationUpSellPopupUI.stop();
            validationUpSellPopupUI = null;
        }
        
        footerImgTable = null;
    }

    public final Image getImage(String key) {
        Image img = dataCenter.getImage(key);

        if (img != null && !imageKeys.contains(key)) {
            imageKeys.add(key);
        }

        return img;
    }

    public final Image getFooterImage(String key) {
        Image img;

        if (footerImgTable == null) {
            footerImgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        }

        img = (Image) footerImgTable.get(key);

        return img;
    }

    public void addClockListener() {
        Formatter formatter = Formatter.getCurrent();
        clockString = formatter.getLongDate();
        Log.printDebug("BaseUI: clockString = " + clockString);

        if (!isAddedClockListener) {
            isAddedClockListener = true;
            Clock.getInstance().addClockListener(this);
        }
    }

    public void clockUpdated(Date date) {
        Formatter formatter = Formatter.getCurrent();
        clockString = formatter.getLongDate();
        repaint();
    }

    public String getClockString() {
        return clockString;
    }

    /**
     * @return the popup
     */
    public BaseUI getPopup() {
        return popup;
    }

    /**
     * @param p the popup to set
     */
    public void setPopup(BaseUI p) {
        popup = p;
        if (popup != null) {
            add(popup, 0);
        }
    }

    public void removePopup(BaseUI p) {
        remove(p);
        popup = null;
    }

    public void clearPopup() {
        if (popup != null) {
            remove(popup);
            popup = null;
        }
    }

    public void showExitPopup(int keyNumber) {
        int uiId = CyoController.getInstance().getCurrentId();
        keyCode = keyNumber;
        if (uiId != BaseUI.UI_CONFIRMATION) {// needExitPopup &&
            gotoHotKey = true;
            if (exitPop == null) {
                exitPop = new NotificationPopupUI();
            }
            setPopup(exitPop);
            exitPop.show(this, NotificationPopupUI.STATE_NORMAL_CANCEL, BaseUI.getMenuText(KeyNames.EXIT_CYO_TITLE),
                    BaseUI.getExplainText(KeyNames.EXIT_CYO));

            repaint();
        } else {
            CyoController.getInstance().exit();
        }
    }

    public void popupClosed(BaseUI pop, Object msg) {
        Log.printInfo("BaseUI popupClosed");
        if (pop.equals(exitPop)) {
            removePopup(exitPop);
            Boolean result = (Boolean) msg;

            if (result.booleanValue()) {
                // CyoController.getInstance().showUI(BaseUI.UI_CONFIRMATION, false);
                gotoNextApplication(keyCode);
            }
            repaint();
        } else {
            repaint();
        }
    }

    public void gotoNextApplication(int keyCode) {
        CyoController.getInstance().stop();
        Log.printInfo("gotoNextApplication keyCode = " + keyCode);
        CyoController.getInstance().startUnboundApplication(keyCode);
    }

    /**
     * Get a explain text of name(or id) in explain.txt.
     * @param name
     * @return
     */
    public static String getExplainText(String name) {
        String text = dataCenter.getString("explain." + name);
        if (Log.DEBUG_ON) {
            Log.printDebug("BaseUI: getExplainText: text : " + text);
        }
        if (text == null) {
            text = TextUtil.EMPTY_STRING;
        }

        return text;
    }

    /**
     * Get a name of menu according to name(or id) in menu.txt.
     * @param name
     * @return
     */
    public static String getMenuText(String name) {
        String text = dataCenter.getString("menu." + name);
        if (Log.DEBUG_ON) {
            Log.printDebug("BaseUI: getMenuText: text : " + text);
        }
        return text;
    }

    public static String getPriceFreq(String freq) {
        if (freq == null || freq.equals("m")) {
            return " " + BaseUI.getMenuText(KeyNames.LIST_MONTH);
        } else if (freq.equals("y")) {
            return " " + BaseUI.getMenuText(KeyNames.LIST_YEAR);
        } else if (freq.equals("o")) {
            return " " + BaseUI.getMenuText(KeyNames.LIST_ONCE);
        }

        return TextUtil.EMPTY_STRING;
    }

    public static String getGamePriceFreq(String freq) {
        if (freq == null || freq.equals("m")) {
            return " " + BaseUI.getMenuText(KeyNames.LIST_GAME_MONTH);
        } else if (freq.equals("y")) {
            return " " + BaseUI.getMenuText(KeyNames.LIST_GAME_YEAR);
        } else if (freq.equals("o")) {
            return " " + BaseUI.getMenuText(KeyNames.LIST_GAME_ONCE);
        }

        return TextUtil.EMPTY_STRING;
    }

    /**
     * Return a id of last UI.
     * @return
     */
    public int getLastUIId() {
        return lastUIId;
    }

    /**
     * Set a id of last UI.
     * @param id
     */
    public void setLastUIId(int id) {
        lastUIId = id;
    }

    public void cyoReStart() {
        String[] msg = new String[] {BaseUI.getExplainText(KeyNames.PIN_MESSAGE_1),
                BaseUI.getExplainText(KeyNames.PIN_MESSAGE_2)};
        CyoController.getInstance().showPinEnabler(msg);
    }
}

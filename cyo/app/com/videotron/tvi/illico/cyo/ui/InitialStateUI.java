/**
 * @(#)InitialStateUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Vector;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.controller.UserEventManager;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.gui.InitialStateRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class InitialStateUI extends BaseUI implements CyoDataListener {

    public static Shortcuts shortcuts;
    private boolean successLogin;
    private ClickingEffect clickEffect;

    public InitialStateUI() {
        renderer = new InitialStateRenderer();
        setRenderer(renderer);
    }

    public void start(final boolean back) {
        if (!back) {
            needExitPopup = false;
            successLogin = false;
            expired = false;
            gotoHotKey = false;
            needDisplay5CPopup = true;
            prepare();
        }
        super.start(back);
        DataCenter.getInstance().remove("ADDED_CUSTOM_CHANNELS");
        if (!back) {
            CyoDataManager.getInstance().addCyoDataListener(this);
            CyoDataManager.getInstance().request(URLType.LOGIN, null);
        } else {
        	expired = true;
        	CyoDataManager.getInstance().addCyoDataListener(this);
            CyoDataManager.getInstance().request(URLType.LOGIN, null);
        }
    }

    public void stop() {
    	Vector customMyPicks = (Vector) DataCenter.getInstance().get("ADDED_CUSTOM_CHANNELS");
    	if (customMyPicks != null) {
    		customMyPicks.clear();
    	}
        super.stop();
    }

    public boolean handleKey(final int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        // case OCRcEvent.VK_UP:
        // cyoReStart();
        // return true;

        case OCRcEvent.VK_LEFT:
            if (focus != 0) {
                focus--;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (focus < shortcuts.getCyoObject().length - 1) {
                focus++;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (successLogin) {
                if (clickEffect == null) {
                    clickEffect = new ClickingEffect(this, 5);
                }
//                if (focus == 0) {
//                    Log.printDebug("shortcusts " + shortcuts);
//                    Group group = (Group) shortcuts.getCyoObject()[0];
//                    Shortcut shortcut = (Shortcut) group.getCyoObject()[0];
//                    String url = shortcut.getUrl();
//                    CyoDataManager.getInstance().pushUrl(url);
//                    clickEffect.start(187, 378, 162, 40);
//                    if (shortcuts.getCyoObject().length == 2) {
//                        CyoController.getInstance().showUI(BaseUI.UI_CHANGE_CHANNEL_SELECTION, false);
//                    } else {
//                        CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
//                    }
//                } else {
//                    Group group = (Group) shortcuts.getCyoObject()[1];
//                    Shortcut shortcut = (Shortcut) group.getCyoObject()[0];
//                    String url = shortcut.getUrl();
//                    clickEffect.start(618, 378, 162, 40);
//                    CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
//                }

                int menuLength = shortcuts.getCyoObject().length;
                Group group = (Group) shortcuts.getCyoObject()[focus];
				Shortcut shortcut = (Shortcut) group.getCyoObject()[0];
                String url = shortcut.getUrl();
                CyoDataManager.getInstance().pushUrl(url);

                if (Log.DEBUG_ON) {
                    Log.printDebug("InitialStateUI, handleKey, shortcuts=" + shortcuts);
					Log.printDebug("InitialStateUI, handleKey, group.shortcut url=" + shortcut.getUrl());
                }

                if (shortcut.getUrl().indexOf("LoadSelfServe.action") > -1) {

                    CyoController.getInstance().showUI(BaseUI.UI_SELF_SERVE, false);
                } else if (shortcut.getUrl().indexOf("LoadCatalog.action") > -1) {
                     // in the case to have premium package
                     // go to Package channel list
                    Package myPackage = CyoDataManager.getInstance().getCurrentPackageInPackageList();
                    DataCenter.getInstance().put("TV_PACKAGE", myPackage);
                    CyoController.getInstance().showUI(BaseUI.UI_PACKAGE_CHANNEL_LIST, false);

                } else if (shortcut.getUrl().indexOf("LoadGroup.action") > -1) {
//                    if (menuLength == 3 && focus == 0) {
//                        // Custom channel
//                        CyoController.getInstance().showUI(BaseUI.UI_CHANGE_CHANNEL_SELECTION, false);
//                    } else if ((menuLength == 3 && focus == 1) || (menuLength == 2 && focus == 0)) {
//                        // a la carte
//                        CyoController.getInstance().showUI(BaseUI.UI_TV_PACKAGE_LIST, false);
//                    }

                    // decide with id base.
                    if (shortcut.getUrl().indexOf("groupNo=1533") > -1) {
                        // Custom channel
                        CyoController.getInstance().showUI(BaseUI.UI_CHANGE_CHANNEL_SELECTION, false);
                    } else {
                        // a la carte
                        DataCenter.getInstance().remove("LoadGroup");
                        CyoController.getInstance().showUI(BaseUI.UI_MANAGE_GROUP, false);
                    }

                } else if (shortcut.getUrl().indexOf("LoadOverview.action") > -1) {
                     // Manager TV service
                     CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, false);
                 }

            }
            return true;

        default:
            return false;
        }
    }

    public boolean successLogin() {
        return successLogin;
    }

    public void popupClosed(BaseUI pop, Object msg) {
        super.popupClosed(pop, msg);
        removePopup(pop);
        if(gotoHotKey) {
        	gotoHotKey = false;
            return;
        }
        Log.printInfo("initalStateUI popupClosed");
        if (expired) {
            cyoReStart();
        } else if (!pop.equals(exitPop)) {
            //CyoController.getInstance().stop();
            CyoController.getInstance().exit();
        }
        repaint();
    }

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (obj instanceof Message && ((Message) obj).getType().equals("5")) {
            Message msg = (Message) obj;
            setPopup(notiPop);
            notiPop.setExplain(msg.getText());
            notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
            expired = true;
        } else if (type == URLType.LOGIN) {
            shortcuts = (Shortcuts) obj;// DataCenter.getInstance().get(Integer.toString(type));

            if (shortcuts.getSuccess().equals("1")) {
                // order - FOR-CYO-0360
                for (int i = 0; i < shortcuts.getCyoObject().length - 1; i++) {
                    String no1 = ((Shortcut)((Group)shortcuts.getCyoObject()[i]).getCyoObject()[0]).getNo();
                    String no2 = ((Shortcut)((Group)shortcuts.getCyoObject()[i + 1]).getCyoObject()[0]).getNo();
                    if (no1.compareTo(no2) > 0) {
                        CyoObject[] objs = shortcuts.getCyoObject();
                        Group tempGroup = (Group) objs[i];
                        objs[i] = (Group)objs[i + 1];
                        objs[i + 1] = tempGroup;
                        shortcuts.setGroup(objs);
                    }
                }

                if(expired) {
                    successLogin = true;
                    expired = false;
                    CyoController.getInstance().hideLoadingAnimation();
                } else  {
                    CyoDataManager.getInstance().request(URLType.LOAD_CHANNEL_INFO, null);
                    UserEventManager.getInstance().start();
                }
            } else {
                successLogin = false;
                // POPUP
                if (shortcuts.getCyoObject() != null) {
                    Message msg = (Message) shortcuts.getCyoObject()[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                }
                CyoController.getInstance().hideLoadingAnimation();
            }
        } else if (type == URLType.LOAD_CHANNEL_INFO) {
            //            successLogin = true;
            //            CyoDataManager.getInstance().removeCyoDataListener();
            CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_OFFER, null);
        } else if (type == URLType.LOAD_PACKAGE_OFFER) {
            successLogin = true;
            CyoDataManager.getInstance().removeCyoDataListener();

            if (CyoController.getInstance().getRedirectionCode() != CyoController.REDIRECTION_CODE_NONE) {
                if (CyoController.getInstance().getRedirectionCode() == CyoController.REDIRECTION_CODE_PACAKGE
                        && CyoController.getInstance().getTargetCallLetter() != null) {
                    if (CyoDataManager.getInstance().getCurrentPackageInPackageList() != null) {
                        CyoController.getInstance().showUI(BaseUI.UI_CHANGE_CHANNEL_SELECTION, false);
                    } else {
                        CyoController.getInstance().resetRedirectionCode();
                        repaint();
                    }
                } else if (CyoController.getInstance().getRedirectionCode() == CyoController.REDIRECTION_CODE_PACAKGE
                        && CyoController.getInstance().getTargetCallLetter() == null) {
                    CyoController.getInstance().showUI(BaseUI.UI_TV_PACKAGE_LIST, false);
                } else if (CyoController.getInstance().getRedirectionCode() == CyoController.REDIRECTION_CODE_A_LA_CARTE) {
                    CyoController.getInstance().showUI(BaseUI.UI_MANAGE_GROUP, false);
                }
            }

        } else if (type == URLType.VALIDATION) {
            // DataCenter.getInstance().put("PRE_LOAD_VALIDATION", obj);
            CyoDataManager.getInstance().removeCyoDataListener();
        }
    }

    public void receiveCyoError(int type) {
    }
}

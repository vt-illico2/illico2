/**
 * @(#)ManageTVServiceUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.awt.Color;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.gui.InformationRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class InformationUI extends BaseUI {
    public static final int STATE_LIST = 0;
    public static final int STATE_MENU = 1;
    public static final int STATE_BUTTON = 2;

    public static final int MODE_HELP = 0;
    public static final int MODE_RULES = 1;
    private ClickingEffect clickEffect;
    private ScrollTexts scroll = new ScrollTexts();
    
    private String helpStr;
    private String rulesStr;

    private int mode;

    public InformationUI() {
        renderer = new InformationRenderer();
        setRenderer(renderer);

        if(scroll == null) {
            scroll = new ScrollTexts();
        }
        
        scroll.setForeground(new Color(224, 224, 224));
        scroll.setRowHeight(17);
        scroll.setFont(FontResource.DINMED.getFont(14));
        scroll.setInsets(10, 8, 13, 0);
        scroll.setBounds(78, 150, 484, 296);
        scroll.setRows(15);
//        scroll.setBottomMaskImage(getImage("17_list_sha02.png"));
        scroll.setBottomMaskImagePosition(66, 418);
        scroll.setVisible(true);
        this.add(scroll);
        
        String tmp = BaseUI.getExplainText(KeyNames.HELP);
        helpStr = tmp.replace('|', '\n');
        
        tmp = BaseUI.getExplainText(KeyNames.RULES);
        rulesStr = tmp.replace('|', '\n');
    }

    public void start(boolean back) {
        String modeStr = DataCenter.getInstance().getString("MODE");
        footer.reset();
        if (modeStr.equals("HELP")) {
            mode = MODE_HELP;
            scroll.setContents(helpStr);
        } else {
            mode = MODE_RULES;
            scroll.setContents(rulesStr);
        }

        if (scroll.hasMultiplePages()) {
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
        } else {
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
        }

        prepare();
        super.start(back);
    }

    public void prepare() {
        focus = 0;
        state = STATE_MENU;
        super.prepare();
    }

    public void stop() {        
        super.stop();
    }

    public void dispose() {
        scroll.stop();
        scroll = null;
        clickEffect = null;
        helpStr = null;
        rulesStr = null;
        
        super.dispose();
    }

    public boolean handleKey(int code) {
        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }
        
        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case OCRcEvent.VK_ENTER:
            keyEnter();
            return true;
        case OCRcEvent.VK_PAGE_UP:
            footer.clickAnimation(1);
            scroll.showPreviousPage();
            repaint();
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            footer.clickAnimation(1);
            scroll.showNextPage();
            repaint();
            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0);
//            CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
            CyoController.getInstance().showUI(lastUIId, true);
            return true;
        default:
            return false;
        }
    }

    private void keyUp() {
        if (state == STATE_BUTTON) {
            state = STATE_MENU;
            repaint();
        }
    }

    private void keyDown() {
        if (state == STATE_MENU) {
            state = STATE_BUTTON;
            repaint();
        }
    }

    private void keyEnter() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
        }
        
        if (state == STATE_MENU) {
            clickEffect.start(622, 352, 292, 50);
            
            footer.reset();
            
            if (mode == MODE_HELP) {
                mode = MODE_RULES;
                scroll.setContents(rulesStr);
                footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
                footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
                renderer.prepare(this);
            } else {
                mode = MODE_HELP;
                scroll.setContents(helpStr);
                footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
                renderer.prepare(this);
            }
            repaint();
        } else if (state == STATE_BUTTON) {
            clickEffect.start(624, 419, 292, 50);
            CyoController.getInstance().showUI(lastUIId, true);
//            CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
        }
    }

    public int getMode() {
        return mode;
    }
}

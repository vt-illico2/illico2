/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Enumeration;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.debug.DebugScreenManager;
import com.videotron.tvi.illico.cyo.gui.GamePackagesListRenderer;

/**
 * ListUI display a list of service, group, package, channel etc.
 * @author Woojung Kim
 * @version 1.1
 */
public class GamePackagesListUI extends ListUI {

    public static final int FOCUS_NONE = 0;
    public static final int FOCUS_ON_PARENT = 1;
    public static final int FOCUS_ON_CHILD = 2;
    public static final int FOCUS_ON_ME = 3;

    public GamePackagesListUI() {
        super(false);
        renderer = new GamePackagesListRenderer();
        setRenderer(renderer);
    }

    public void start(boolean back) {
        prepare();
        if (!back) {
            startIdx = 0;
            focus = 0;
            descStartIdx = 0;
            super.start(back);
            if (getCyoObject(0) instanceof Group) {
                focus = 1;
            }
        }        
    }

    public void stop() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                dataCenter.removeImage(key);
            }
            imageKeys.clear();
        }
        super.stop();
    }

    public boolean handleKey(int code) {
        CyoObject curObj;

        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();
            // handle a case of group
            curObj = getCyoObject(startIdx + focus);
            if (curObj instanceof Group) {
                if (startIdx == 0 && focus == 0) {
                    keyDown();
                } else {
                    keyUp();
                }
            }
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();

            // handle a case of group
            curObj = getCyoObject(startIdx + focus);
            if (curObj instanceof Group) {
                keyDown();
            }
            return true;
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
            super.handleKey(code);
            repaint();
            return true;
        default:
            return false;
        }
    }

    private void keyUp() {
        if (objs != null) {
            if (startIdx + focus >= objs.length) {
                startIdx--;
            } else if (startIdx + focus > 0) {
                if (focus > 3) {
                    focus--;
                } else if (startIdx > 0) {
                    if (focus < 4) {
                        startIdx--;
                    } else {
                        focus--;
                    }
                } else {
                    focus--;
                }
            }
        }
    }

    private void keyDown() {
        if (objs != null) {
            if (startIdx + focus < objs.length - 1) {
                if (focus < 2) {
                    focus++;
                } else if (startIdx + 7 < objs.length) {
                    if (focus > 2) {
                        startIdx++;
                    } else {
                        focus++;
                    }
                } else {
                    if (startIdx + focus > objs.length - 7) {
                        focus++;
                    } else {
                        startIdx++;
                        focus++;
                        if (startIdx + focus >= objs.length) {
                            startIdx--;
                        }
                    }
                }
            }
        }
    }

    public int getStartIdx() {
        return startIdx;
    }

    public CyoObject getCyoObject(int idx) {
        if (objs != null && idx < objs.length && objs[idx] != null) {
            DebugScreenManager.getInstance().addData(objs[idx]);
            return objs[idx];
        }

        return null;
    }

    public CyoObject getCurrentCyoObject() {
        int idx = startIdx + focus;
        return getCyoObject(idx);
    }

    public int getCyoObjectLength() {
        if (objs != null) {
            return objs.length;
        } else {
            return 0;
        }
    }

    public void setCyoObject(CyoObject[] cyoObjs) {
        this.objs = cyoObjs;
    }

    public boolean hasFocus() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    public boolean onFocus(int idx) {
        if (startIdx + focus == idx) {
            return true;
        }

        return false;
    }

    public int getFocusType(int idx) {
        CyoObject obj = getCyoObject(idx);
        CyoObject currentObj = getCyoObject(startIdx + focus);

        if (obj instanceof Package) {

            if (obj == currentObj.getParentObj()) {
                return FOCUS_ON_CHILD;
            } else if (obj == currentObj) {
                return FOCUS_ON_ME;
            } else {
                return FOCUS_NONE;
            }

        } else {
            if (obj.getParentObj() == currentObj) {
                return FOCUS_ON_PARENT;
            } else if (obj == currentObj) {
                return FOCUS_ON_ME;
            } else if (obj.getParentObj() == currentObj.getParentObj()) {
                return FOCUS_ON_CHILD;
            } else {
                return FOCUS_NONE;
            }
        }
    }
}

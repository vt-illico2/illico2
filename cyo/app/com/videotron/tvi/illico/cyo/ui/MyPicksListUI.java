/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Enumeration;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.gui.MyPicksListRenderer;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;

/**
 * ListUI display a list of service, group, package, channel etc.
 * @author Woojung Kim
 * @version 1.1
 */
public class MyPicksListUI extends ListUI {

    public static final int FOCUS_NONE = 0;
    public static final int FOCUS_ON_PARENT = 1;
    public static final int FOCUS_ON_CHILD = 2;
    public static final int FOCUS_ON_ME = 3;

    private ClickingEffect clickEffect;

    public MyPicksListUI() {
        super(false);
        renderer = new MyPicksListRenderer();
        setRenderer(renderer);
    }

    public void start(boolean back) {
        prepare();
        startIdx = 0;
        focus = 0;
        if (getCyoObject(0) instanceof Group) {
            focus = 1;
        }

        super.start(back);
    }

    public void stop() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                dataCenter.removeImage(key);
            }
            imageKeys.clear();
        }
        
        super.stop();
    }

    public boolean handleKey(int code) {
        CyoObject curObj;

        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();

            // handle a case of group
            curObj = getCyoObject(startIdx + focus);
            if (curObj instanceof Group) {
                if (startIdx == 0 && focus == 0) {
                    keyDown();
                } else {
                    keyUp();
                }
            }
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();

            // handle a case of group
            curObj = getCyoObject(startIdx + focus);
            if (curObj instanceof Group) {
                keyDown();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            return true;
        default:
            return false;
        }
    }

    private void keyUp() {
        if (objs != null) {
            if (startIdx + focus >= objs.length) {
                startIdx--;
            } else if (startIdx + focus > 0) {
                if (focus > 3) {
                    focus--;
                } else if (startIdx > 0) {
                    if (focus < 4) {
                        startIdx--;
                    } else {
                        focus--;
                    }
                } else {
                    focus--;
                }
            }
        }
    }

    private void keyDown() {
        if (objs != null) {
            if (startIdx + focus < objs.length - 1) {
                if (focus < 3) {
                    focus++;
                } else if (startIdx + 9 < objs.length) {
                    if (focus > 2) {
                        startIdx++;
                    } else {
                        focus++;
                    }
                } else {
                    if (startIdx + focus > objs.length - 9) {
                        focus++;
                    } else {
                        startIdx++;
                        focus++;
                        if (startIdx + focus >= objs.length) {
                            startIdx--;
                        }
                    }
                }
            }
        }
    }

    public int getStartIdx() {
        return startIdx;
    }

    public CyoObject getCyoObject(int idx) {
        if (objs != null && idx < objs.length && objs[idx] != null) {
            return objs[idx];
        }

        return null;
    }

    public CyoObject getCurrentCyoObject() {
        int idx = startIdx + focus;
        if (objs != null && idx < objs.length && objs[idx] != null) {
            return objs[idx];
        }
        return null;
    }

    public int getCyoObjectLength() {
        if (objs != null) {
            return objs.length;
        } else {
            return 0;
        }
    }

    public void setCyoObject(CyoObject[] cyoObjs) {
        this.objs = cyoObjs;
    }

    public boolean hasFocus() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    public boolean onFocus(int idx) {
        if (startIdx + focus == idx) {
            return true;
        }

        return false;
    }

    public int getFocusType(int idx) {

        if ((startIdx + focus) == idx) {
            return FOCUS_ON_ME;
        } else {
            return FOCUS_NONE;
        }
    }
    
    public void setClickEffect() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
        }
        clickEffect.start(547, 172 + focus * 32, 131, 26);

    }

    public void removeCurrentCyoObjcet() {
        Vector dataVec = new Vector();
        int curIdx = startIdx + focus;

        for (int i = 0; i < objs.length; i++) {
            if (i != curIdx) {
                dataVec.add(objs[i]);
            }
        }
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("MyPicksListUI: removeCurrentCyoObjcet: dataVec size = " + dataVec.size());
        }

        // remove empty group
        if (dataVec.size() > 1) {
            for (int i = 0; i < dataVec.size(); i++) {
                if (i < dataVec.size() - 1) {
                    CyoObject curObj = (CyoObject) dataVec.get(i);
                    CyoObject nextObj = (CyoObject) dataVec.get(i + 1);

                    if (curObj instanceof Group && nextObj instanceof Group) {
                        dataVec.remove(i);
                    }
                } else {
                    CyoObject curObj = (CyoObject) dataVec.get(i);
                    if (curObj instanceof Group) {
                        dataVec.remove(i);
                    }
                }
            }
        } else if (dataVec.size() == 1) {
            CyoObject curObj = (CyoObject) dataVec.get(0);

            if (curObj instanceof Group) {
                objs = null;
                focus = 0;
                return;
            }
        }

        CyoObject[] newObjs = new CyoObject[dataVec.size()];
        dataVec.copyInto(newObjs);

        objs = null;
        objs = newObjs;

        if (focus > 0 && curIdx == objs.length) {
            focus--;
        } else if (focus > 0 && curIdx > objs.length) {
            focus -= 2;
        }
    }

    public boolean isDisplayBottomDeco() {
        if (objs == null) {
            return false;
        }

        if (objs != null && startIdx < objs.length - 9) {
            return true;
        }

        return false;
    }

}

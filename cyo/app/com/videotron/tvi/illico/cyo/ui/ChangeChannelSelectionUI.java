/**
 * @(#)ChangeChannelSelectionUI.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.cyo.data.obj.*;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.gui.ChangeChannelSelectionRenderer;
import com.videotron.tvi.illico.cyo.popup.MenuItem;
import com.videotron.tvi.illico.cyo.popup.MenuListener;
import com.videotron.tvi.illico.cyo.popup.OptionScreen;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class display CYO_005.
 *
 * @author Woojung Kim
 * @version 1.1
 */
public class ChangeChannelSelectionUI extends BaseUI implements CyoDataListener, MenuListener {
	public static final int STATE_LIST = 0x0;
	public static final int STATE_BUTTON = 0x1;

	private ChangeChannelSelectionRenderer renderer = new ChangeChannelSelectionRenderer();
	private ClickingEffect clickEffect;

	private ListUI listUI;
	private Group loadedGroup;

	private Vector addVector;
	private Vector removeVector;

	private Vector messagePopupVector = new Vector();

	private boolean isModified;

	private int customIdx = -1;
	//private int specialIdx = -1;

	private int btnFocus = 0;
	private String url;
	private Services services;

	private ListUI currentList;

	private String sort;

	public OptionScreen optScr;
	public static MenuItem sortOption = new MenuItem("menu.sort");
	public static MenuItem nSORT = new MenuItem("menu.sorted.number");
	public static MenuItem aSORT = new MenuItem("menu.sorted.alphabetical");
	public boolean buttonClick = false;

	private boolean channelInfoBoolean = false;

	private boolean backButton = false;

	private boolean isRequestedValidate = false;

	public static final String CUSTOM_GROUP_NO = "1529";
	public static final String SELF_SERVE_GROUP_NO = "1528";
	//	public static final String MOVIE_GROUP_NO = "1530";
	//	public static final String SPECIAL_GROUP_NO = "1531";
	//	public static final String HD_GROUP_NO = "1532";
	//	public static final String INTERNATIONAL_GROUP_NO = "1538";

	private boolean comeFromBack;
	// 서버에 저장 했는지 확인
	private Vector savedParam = new Vector();

	private Vector removedParam = new Vector();

	private int lastRequestedURLType = URLType.LOAD_GROUP;
	private CyoObject lastObject;

	private boolean needParamForRequiresUpsell = false;
	private boolean isFromISA;
	private Catalog loadedCatalog;
	private int numberOfChannels;

	public ChangeChannelSelectionUI() {
		setRenderer(renderer);
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);

		Log.printInfo("start = " + back);
		comeFromBack = back;
		needParamForRequiresUpsell = false;
		if (!back) {
			isRequestedValidate = false;
			isFromISA = false;
			clearAddAndRemoveVectors();

			// VDTRMASTER-6001 & VDTRMASTER-6002
			loadedCatalog = null;
			loadedGroup = null;

            numberOfChannels = 0;

			prepare();
			customIdx = -1;
			//specialIdx = -1;
			sort = Param.SORT_ALPHABETICAL;
			currentList = null;
			url = null;
			try {
				url = CyoDataManager.getInstance().popUrl();
			} catch (EmptyStackException ese) {
				ese.printStackTrace();
			}

			if (url != null) {
				Hashtable param = new Hashtable();
				param.put("url", url);
				CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
			} else {
				// from isa
				isFromISA = true;

				if (Log.DEBUG_ON) {
					Log.printDebug("ChangeChannelSelectionUI, start, come from ISA, isFromISA=" + isFromISA);
				}

				// found catalog
				Package currentPackage = CyoDataManager.getInstance().getCurrentPackageInPackageList();
				if (Log.DEBUG_ON) {
					Log.printDebug("ChangeChannelSelectionUI, start, currentPackage=" + currentPackage);
				}

				if (currentPackage != null && currentPackage.getPackageCatalogs() != null) {
					PackageCatalog nonPremiumCatalog = currentPackage.getNonPremiumPacakgeCatalog();
					Catalog catalog = nonPremiumCatalog.getCatalog();

                    numberOfChannels = nonPremiumCatalog.getNumberOfChannelForInt();
					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI, start, nonPremiumCatalog=" + nonPremiumCatalog);
						Log.printDebug("ChangeChannelSelectionUI, start, catalog=" + catalog);
					}

					if (catalog != null && catalog.getUrl() != null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI, start, catalog.getUrl()=" + catalog.getUrl());
						}
						url = catalog.getUrl();
						Hashtable param = new Hashtable();
						param.put("url", url);
						CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_CATALOG, param);
					} else if (currentPackage.getUrl() != null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI, start, currentPackage.getUrl()=" + currentPackage
									.getUrl());
						}
						url = currentPackage.getUrl();
						Hashtable param = new Hashtable();
						param.put("url", url);
						CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_CATALOG, param);
					}
				} else {
					// VDTRMASTER-6001 & VDTRMASTER-6002
					isFromISA = true;

					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI, start, come from ISA but no subscribed TV package"
								+ ", isFromISA=" + isFromISA);
					}

					Hashtable param = new Hashtable();
					param.put("groupNo", SELF_SERVE_GROUP_NO);

					CyoDataManager.getInstance().request(URLType.LOAD_A_LA_CARTE, param);
				}
			}
		} else {

			if (listUI != null) {
				listUI.prepare();
			}

			state = STATE_LIST;
			focus = 0;
			currentList = listUI;
			currentList.setHasFocus(true);
			// myPicksList.start(true);
			setFooter();

			//resetData();
			CyoDataManager.getInstance().request(URLType.LOGIN, null);
			// Help 인경우 ...
			// if(!gotoHelp) {
			// Hashtable param = new Hashtable();
			// param.put("url", url);
			// CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
			// }
		}
		if (messagePopupVector == null) {
			messagePopupVector = new Vector();
		} else {
			messagePopupVector.clear();
		}
		backButton = back;
		channelInfoBoolean = false;
		buttonClick = false;
		sortOption.clear();
		sortOption.add(aSORT);
		sortOption.add(nSORT);

		super.start(back);

		repaint();
	}

	public void prepare() {
		state = STATE_LIST;
		btnFocus = 0;
		focus = 0;
		state = STATE_LIST;
		setFooter();

		super.prepare();
	}

	public void stop() {
		if (optScr != null) {
			optScr.stop();
			optScr.dispose();
		}
		if (messagePopupVector != null) {
			messagePopupVector.clear();
			messagePopupVector = null;
		}
		optScr = null;

		// if(!gotoHelp) {
		// customAddVector.clear();
		// specialAddVector.clear();
		// movieAddVector.clear();
		// hdAddVector.clear();
		//
		// customRemoveVector.clear();
		// specialRemoveVector.clear();
		// movieRemoveVector.clear();
		// hdRemoveVector.clear();
		// }

		clearPopup();
		super.stop();
	}

	public void dispose() {
		clickEffect = null;
		if (messagePopupVector != null) {
			messagePopupVector.clear();
			messagePopupVector = null;
		}
		clearAddAndRemoveVectors();

		if (listUI != null) {
			listUI.stop();
		}
		super.dispose();
	}

	public boolean handleKey(int code) {
		if (getPopup() != null && getPopup().isVisible()) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case OCRcEvent.VK_PAGE_UP:
			pageUp();
			return true;
		case OCRcEvent.VK_PAGE_DOWN:
			pageDown();
			return true;
		case OCRcEvent.VK_ENTER:
			keyEnter();
			return true;
		case KeyCodes.LAST:
			footer.clickAnimation(0);
			if (backButton) {
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			} else if (getChanged()) {
				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
			} else {
				CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
			}
			repaint();
			return true;
		case KeyCodes.COLOR_A:
			if (loadedGroup != null || loadedCatalog != null) {

				footer.clickAnimation(2);

				if (optScr == null) {
					optScr = new OptionScreen();
				}

				if (sort == Param.SORT_NUMBER) {
					nSORT.setChecked(true);
					aSORT.setChecked(false);
				} else {
					nSORT.setChecked(false);
					aSORT.setChecked(true);
				}

				optScr.start(sortOption, this);
			}

			return true;
		case KeyCodes.COLOR_B:
			if (loadedGroup != null || loadedCatalog != null) {
				footer.clickAnimation(3);

				setPopup(notiPop);
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL_CANCEL,
						BaseUI.getMenuText(KeyNames.UNSELECT_CHANNEL_TITLE),
						BaseUI.getExplainText(KeyNames.UNSELECT_CHANNEL));
				repaint();
			}
			return true;
		default:
			return false;
		}
	}

	private void keyLeft() {
		if (state == STATE_BUTTON) {
			state = STATE_LIST;
			currentList.setHasFocus(true);
			btnFocus = 0;
			repaint();
		}
	}

	private void keyRight() {
		if (state == STATE_LIST) {
			state = STATE_BUTTON;
			currentList.setHasFocus(false);
			if (!getChanged()) {
				btnFocus = 1;
			} else {
				btnFocus = 0;
			}
			repaint();
		}
	}

	private void keyUp() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_UP);
			repaint();
		} else if (state == STATE_BUTTON) {
			--btnFocus;
			if (!getChanged() && btnFocus < 1) {
				btnFocus = 1;
			} else if (btnFocus < 0) {
				btnFocus = 0;
			}

			repaint();
		}
	}

	private void keyDown() {
		if (state == STATE_LIST) {
			currentList.handleKey(OCRcEvent.VK_DOWN);
			repaint();
		} else if (state == STATE_BUTTON) {
			if (++btnFocus > 2) {
				btnFocus = 2;
			}
			repaint();
		}
	}

	private void pageUp() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_UP);
			repaint();
		}
	}

	private void pageDown() {
		if (state == STATE_LIST) {
			footer.clickAnimation(1);
			currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
			repaint();
		}
	}

	private void keyEnter() {
		if (state == STATE_LIST) {
			CyoObject curObj = currentList.getCurrentCyoObject();
			if (curObj instanceof Package && currentList.isFocusOnViewChanenlIncluded()) {
				// show included channels popup
				if (Log.DEBUG_ON) {
					Log.printDebug("ChangeChannelSelectionUI,, keyEnter, show included channels popup");
				}

				setPopup(includedChannelsPopup);
				includedChannelsPopup.show(this, (Package) curObj);
			} else {
				updateSubscribed(curObj);
				needExitPopup = true;
			}
		} else if (state == STATE_BUTTON) {

			if (clickEffect == null) {
				clickEffect = new ClickingEffect(this, 5);
			}
			clickEffect.start(717, 346 + btnFocus * 37, 180, 35);

			if (btnFocus == 0) {
				buttonClick = true;
				isRequestedValidate = true;
				// VDTRMASTER-5958
				CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);
				setData();
			} else if (btnFocus == 1) { // Cancel
				if (backButton) {
					setPopup(notiPop);
					notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
				} else if (checkAddVector()) {
					setPopup(notiPop);
					notiPop.show(this, NotificationPopupUI.STATE_CANCEL);
				} else {
					if (isRequestedValidate) {
						String titleStr = BaseUI.getMenuText(KeyNames.CANCEL_CHANGE_TITLE);
						String explainStr = BaseUI.getExplainText(KeyNames.CANCEL_CHANGE);
						setPopup(notiPop);
						notiPop.show(this, NotificationPopupUI.STATE_CANCEL_CHANGE, titleStr, explainStr);
					} else {
						CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
					}
				}
			} else if (btnFocus == 2) { // Help
				DataCenter.getInstance().put("MODE", "HELP");
				CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
			}
		}
		repaint();
	}

	private boolean checkAddVector() {

		if (addVector != null) {
			if (addVector != null && addVector.size() > 0) {
				return true;
			}
		}

		return false;
	}

	private boolean checkRemoveVector() {
		if (removeVector != null) {
			if (removeVector != null && removeVector.size() > 0) {
				return true;
			}
		}
		return false;
	}

	public void setData() {
		// Confirm
		dataParam.clear();
		addNumber = null;
		addNumber = new StringBuffer();
		removeNumber = null;
		removeNumber = new StringBuffer();

		if (addVector != null) {
			if (addVector.size() > 0 || removeVector.size() > 0) {
				// VDTRMASTER-5888
				requestSaveAction();
			}
		}

		sendServerSaveAction();
	}

	public void resetData() {
		if (Log.INFO_ON) {
			Log.printInfo("ChangeChannelSelectionUI: resetData()");
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ChangeChannelSelectionUI: resetData(): dataParam size = " + dataParam.size());
		}

		if (dataParam.size() > 0) {
			Hashtable resetParam = new Hashtable();

			String addParams = (String) dataParam.get("add");

			if (addParams != null) {
				resetParam.put("remove", addParams);
			}

			String removeParams = (String) dataParam.get("remove");

			if (removeParams != null) {
				resetParam.put("add", removeParams);
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("ChangeChannelSelectionUI: resetData(): resetParam " + resetParam);
			}

			if (isFromISA) {
				CyoDataManager.getInstance().request(URLType.SAVE_SELF_SERVE, resetParam);
			} else {
				CyoDataManager.getInstance().request(URLType.SAVE_GROUP, resetParam);
			}
		}
	}

	private void clearAddAndRemoveVectors() {
		if (Log.INFO_ON) {
			Log.printInfo("ChangeChannelSelectionUI: clearAddAndRemoveVectors()");
		}

		if (addVector != null) {
			addVector.clear();
		}

		if (removeVector != null) {
			removeVector.clear();
		}
	}

	private void setFooter() {
		footer.reset();
		if (loadedGroup != null) {
			// VDTRMASTER-5923
			if (loadedGroup != null) {
				footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK),
						BaseUI.getMenuText(KeyNames.BTN_BACK));
				footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE),
						BaseUI.getMenuText(KeyNames.BTN_SCROLL));
				footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A),
						BaseUI.getMenuText(KeyNames.BTN_SORTING));
				footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_B),
						BaseUI.getMenuText(KeyNames.BTN_DESELECT_ALL));
			} else {
				footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK),
						BaseUI.getMenuText(KeyNames.BTN_BACK));
				footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE),
						BaseUI.getMenuText(KeyNames.BTN_SCROLL));
			}
		} else if (loadedCatalog != null) {
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK),
					BaseUI.getMenuText(KeyNames.BTN_BACK));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE),
					BaseUI.getMenuText(KeyNames.BTN_SCROLL));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A),
					BaseUI.getMenuText(KeyNames.BTN_SORTING));
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_B),
					BaseUI.getMenuText(KeyNames.BTN_DESELECT_ALL));
		} else {
			footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK),
					BaseUI.getMenuText(KeyNames.BTN_BACK));
		}
	}

	public String getGroupName() {
		if (loadedGroup != null) {
			return loadedGroup.getTitle();
		}
		return TextUtil.EMPTY_STRING;
	}

	public String getGroupNo(int idx) {
		if (loadedGroup != null) {
			return loadedGroup.getNo();
		}
		return TextUtil.EMPTY_STRING;
	}

	private void updateSubscribed(CyoObject obj) {
		if (Log.DEBUG_ON) {
			if (obj instanceof Subscription) {
				Log.printDebug("ChangeChannelSelectionUI: obj no = " + ((Subscription) obj).getNo());
			}
		}
		if (obj instanceof Package) {
			Package curPackage = ((Package) obj);
			String modiable = curPackage.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("ChangeChannelSelectionUI: Package modiable = " + modiable);
			}
			if (modiable.equals("0")) {
				// Package
				curPackage.setSubscribedInSession(!curPackage.getSubscribedInSession());
				CyoObject[] childObjs = curPackage.getCyoObject();

				for (int i = 0; childObjs != null && i < childObjs.length; i++) {
					Subscription child = (Subscription) childObjs[i];
					if (child.getModifiable().equals("1")) {
						child.setSubscribedInSession(curPackage.getSubscribedInSession());
					}
				}

				if (curPackage.getSubscribedInSession()) {
					addCyoObjectInMyPicks(obj);
				} else {
					removeCyoObjectInMyPicks(obj);
				}

				if (loadedGroup != null) {
					isModified = true;
				}
			} else if (modiable.equals("1")) {
				Log.printInfo("UNSELECT_MESSAGE_1");
				channelInfoBoolean = true;
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else if (modiable.equals("2")) {
				Log.printInfo("UNSELECT_MESSAGE_2");
				channelInfoBoolean = true;
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else if (modiable.equals("3")) {
				Log.printInfo("UNSELECT_MESSAGE_3");
				channelInfoBoolean = true;
				setPopup(notiPop);
				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			}

		} else if (obj instanceof Subscription) {
			Subscription subObj = ((Subscription) obj);
			String modiable = subObj.getModifiable();
			if (Log.DEBUG_ON) {
				Log.printDebug("ChangeChannelSelectionUI: Channel modiable = " + modiable);
			}
			Log.printDebug("ChangeChannelSelectionUI: subObj = " + subObj);
			CyoObject parentObj = subObj.getParentObj();
			Log.printDebug("ChangeChannelSelectionUI: parentObj = " + parentObj);
			if (parentObj instanceof Package) {
				Package parentPackage = (Package) parentObj;
				String pModiable = parentPackage.getModifiable();
				if (Log.DEBUG_ON) {
					Log.printDebug("ChangeChannelSelectionUI: Parent Package modiable = " + pModiable);
				}
				if (pModiable.equals("0") && modiable.equals("0")) {
					/*
					 * Package = 0; Channel = 0: The user can add / remove a
					 * package, as well as channels in it. In this case, the
					 * user can do anything (add/remove a package, add/remove a
					 * channel from within the package with no consequences)
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(obj);
					} else {
						removeCyoObjectInMyPicks(obj);
					}
					if (loadedGroup != null) {
						isModified = true;
					}
				} else if (pModiable.equals("0") && !modiable.equals("0")) {
					/*
					 * Package = 0; Channel 1: The user can remove/add the
					 * package but not individual channels. If the user unchecks
					 * a channel, the whole package is automatically unchecked
					 * with all other channels in it.
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());
					parentPackage.setSubscribedInSession(subObj.getSubscribedInSession());
					CyoObject[] childObjs = parentPackage.getCyoObject();

					for (int i = 0; childObjs != null && i < childObjs.length; i++) {
						Subscription child = (Subscription) childObjs[i];
						if (child.getModifiable().equals("1")) {
							child.setSubscribedInSession(parentPackage.getSubscribedInSession());
						}
					}

					if (parentPackage.getSubscribedInSession()) {
						addCyoObjectInMyPicks(parentPackage);
					} else {
						removeCyoObjectInMyPicks(parentPackage);
					}

					if (loadedGroup != null) {
						isModified = true;
					}
				} else if (!pModiable.equals("0") && modiable.equals("0")) {
					/*
					 * Package = 1; Channel 0: The user cannot add/remove the
					 * package, but he can change individual channels in it. If
					 * the user checks/unckecks a channel, it has an effect only
					 * on that channel.
					 */
					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(subObj);
					} else {
						removeCyoObjectInMyPicks(subObj);
					}
					if (loadedGroup != null) {
						isModified = true;
					}
				} else if (modiable.equals("1")) {
					Log.printInfo("UNSELECT_MESSAGE_1");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("2")) {
					Log.printInfo("UNSELECT_MESSAGE_2");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("3")) {
					Log.printInfo("UNSELECT_MESSAGE_3");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
				//			} else if (!(subObj instanceof Package || subObj instanceof Channel) && !subObj.getNo().equals("926")) {
				////				if (subObj.getNo().equals("926")) {
				////					Log.printInfo("This is HD Network Fees");
				////					setPopup(notiPop);
				////					notiPop.setExplain(BaseUI.getExplainText(KeyNames.HD_NETWORK_FEES));
				////					notiPop.show(this, NotificationPopupUI.STATE_HD_NETWORK_FEES);
				////				} else {
				////					Log.printInfo("UNSELECT_MESSAGE_4");
				////					channelInfoBoolean = true;
				////					setPopup(notiPop);
				////					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_4));
				////					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				////				}
				//
				//				Log.printInfo("UNSELECT_MESSAGE_4");
				//				channelInfoBoolean = true;
				//				setPopup(notiPop);
				//				notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_4));
				//				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			} else {
				Log.printDebug("ChangeChannelSelectionUI: subObj no = " + subObj.getNo());
				Log.printDebug(
						"ChangeChannelSelectionUI: subObj.getSubscribedInAccount = " + subObj.getSubscribedInAccount());
				Log.printDebug(
						"ChangeChannelSelectionUI: subObj.getSubscribedInSession = " + subObj.getSubscribedInSession());
				Log.printDebug("ChangeChannelSelectionUI: subObj.getSubscribedOrginalInSession = " + subObj
						.getSubscribedOrginalInSession());

				if (subObj.getNo().equals("926") && !modiable.equals("0") && subObj.getSubscribedInAccount()) {
					Log.printInfo("This is HD Network Fees");
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.HD_NETWORK_FEES));
					notiPop.show(this, NotificationPopupUI.STATE_HD_NETWORK_FEES);
				} else if (modiable.equals("0")) {

					subObj.setSubscribedInSession(!subObj.getSubscribedInSession());

					if (subObj.getSubscribedInSession()) {
						addCyoObjectInMyPicks(subObj);
					} else {
						removeCyoObjectInMyPicks(subObj);
					}
					if (loadedGroup != null) {
						isModified = true;
					}
				} else if (subObj.isSubscriptionTypeOfALaCarte()) {
					Log.printDebug("UNSELECT_MESSAGE_FOR_A_LA_CARTE");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_FOR_A_LA_CARTE));
					notiPop.show(this, NotificationPopupUI.STATE_UNSELECT_MESSAGE_FOR_A_LA_CARTE);
				} else if (modiable.equals("1")) {
					Log.printDebug("UNSELECT_MESSAGE_1");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_1));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("2")) {
					Log.printDebug("UNSELECT_MESSAGE_2");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_2));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				} else if (modiable.equals("3")) {
					Log.printDebug("UNSELECT_MESSAGE_3");
					channelInfoBoolean = true;
					setPopup(notiPop);
					notiPop.setExplain(BaseUI.getExplainText(KeyNames.UNSELECT_MESSAGE_3));
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}

			}
		}

		renderer.update(this);
	}

	public int getButtonFocus() {
		return btnFocus;
	}

	public ListUI getCurrentList() {
		return currentList;
	}

	Hashtable dataParam = new Hashtable();
	StringBuffer addNumber = new StringBuffer();
	StringBuffer removeNumber = new StringBuffer();

	private void requestSaveAction() {
		if (Log.INFO_ON) {
			Log.printInfo("requestSaveAction()");
		}

		String addParams = null, removeParams = null;

		//		if (no.equals(INTERNATIONAL_GROUP_NO) && specialIdx != -1) {
		//			if (Log.DEBUG_ON) {
		//				Log.printDebug("ChangeChannelSelectionUI: requestSaveAction(): international group");
		//			}
		//
		//			addParams = getNumbers(addVectorArray[specialIdx], addNumber);
		//			removeParams = getNumbers(removeVectorArray[specialIdx], removeNumber);
		//			groupNo = INTERNATIONAL_GROUP_NO;
		//		} else {
		//			addParams = getNumbers(addVectorArray[idx], addNumber);
		//			removeParams = getNumbers(removeVectorArray[idx], removeNumber);
		//			groupNo = no;
		//		}
		//
		addParams = getNumbers(addVector, addNumber);
		removeParams = getNumbers(removeVector, removeNumber);

		if (addParams != null) {
			dataParam.put("add", addParams);
		}
		if (removeParams != null) {
			dataParam.put("remove", removeParams);
		}

		if (needParamForRequiresUpsell) {
			dataParam.put("requiresUpsell", "false");
		}

		// param.put("groupNo", groupNo);
		if (Log.DEBUG_ON) {
			Log.printDebug("addParams = " + addParams);
			Log.printDebug("removeParams = " + removeParams);
			Log.printDebug("loadGroup.no = " + (loadedGroup != null ? loadedGroup.getNo() : null));
			Log.printDebug("loadCatalog.catalogId =" + (loadedCatalog != null ? loadedCatalog.getCatalogId() : null));
			Log.printDebug("needParamForRequiresUpsell = " + needParamForRequiresUpsell);
		}
	}

	public void sendServerSaveAction() {
		if (dataParam.size() > 0) {
			if (isFromISA) {
				CyoDataManager.getInstance().request(URLType.SAVE_SELF_SERVE, dataParam);
			} else {
				CyoDataManager.getInstance().request(URLType.SAVE_GROUP, dataParam);
			}
		} else if (savedParam.size() == 0 && removedParam.size() == 0) {
			CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
		}
	}

	private String getNumbers(Vector vec, StringBuffer sb) {
		if (vec.size() == 0) {
			return null;
		}

		if (sb.length() > 0) {
			sb.append(",");
		}

		for (int i = 0; i < vec.size(); i++) {
			if (i > 0) {
				sb.append(",");
			}
			Subscription obj = (Subscription) vec.get(i);
			sb.append(obj.getNo());
		}
		return sb.toString();
	}

	private void buildDataForList(CyoObject[] groups) {

		loadedGroup = null;
		loadedCatalog = null;
		clearAddAndRemoveVectors();

		CyoObject[] data = null;

		// VDTRMASTER-6001 & VDTRMASTER-6002
		if (groups[0] instanceof Catalog) {
			loadedCatalog = (Catalog) groups[0];
			data = loadedCatalog.getCyoObject();
		} else {
			loadedGroup = (Group) groups[0];
			data = loadedGroup.getCyoObject();
		}
		isModified = false;

		addVector = new Vector();
		removeVector = new Vector();
		listUI = new ListUI(true);

		CyoObject[] rearrangeData;
		Vector dataVector = new Vector();

		addCyoObject(dataVector, data);
		data = new CyoObject[dataVector.size()];
		dataVector.copyInto(data);
		dataVector.clear();

		if (data != null) {
			// CyoObject[] data = groups[0].getCyoObject();
			Vector sortData = new Vector();
			for (int a = 0; data != null && a < data.length; a++) {
				if (data[a] instanceof Subscription) {
					if (((Subscription) data[a]).getSubscribedInAccount() != ((Subscription) data[a])
							.getSubscribedInSession()) {
						// changed the one more.
						((Subscription) data[a])
								.setSubscribedInSession(!((Subscription) data[a]).getSubscribedInSession());
						// update SubscribedInSession
						String modifiable = ((Subscription) data[a]).getModifiable();
						if (modifiable.equals("0")) {
							updateSubscribed(data[a]);
						}
					}
				}

				sortData.add(data[a]);
			}

			Collections.sort(sortData, CyoObjectComparator.comparator);

			data = (CyoObject[]) sortData.toArray(data);
		}

		//		addCyoObject(dataVector, data);
		//		rearrangeData = new CyoObject[dataVector.size()];
		//		dataVector.copyInto(rearrangeData);

		listUI.setCyoObject(data);
		listUI.start(true);
		currentList = listUI;

		dataVector.clear();

		listUI.setHasFocus(true);

		if (CyoController.getInstance().getRedirectionCode() != CyoController.REDIRECTION_CODE_NONE) {
			processRedirect();
		}
	}

	// Bundling
	private void processRedirect() {
		CyoController.getInstance().resetRedirectionCode();
		boolean found = false;
		String targetCallLetter = CyoController.getInstance().getTargetCallLetter();
		String sisterCallLetter = null;
		try {
			EpgService eService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
			if (eService != null) {
				TvChannel channel = eService.getChannel(targetCallLetter);
				if (channel != null && channel.getSisterChannel() != null) {
					sisterCallLetter = channel.getSisterChannel().getCallLetter();
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ChangeChannelSelectionUI, processRedirect, targetCallLetter=" + targetCallLetter);
			Log.printDebug("ChangeChannelSelectionUI, processRedirect, sisterCallLetter=" + sisterCallLetter);
		}

		for (int i = 0; i < listUI.getCyoObjectLength(); i++) {
			if (listUI.getCurrentCyoObject() instanceof Channel) {
				Channel ch = (Channel) listUI.getCurrentCyoObject();

				// VDTRMASTER-5880
				Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(ch.getId());
				if (chInfo.getCallLetters() != null && (chInfo.getCallLetters().equals(targetCallLetter) || chInfo
						.getCallLetters().equals(sisterCallLetter))) {
					updateSubscribed(ch);
					needExitPopup = true;
					found = true;
					break;
				}
			} else if (listUI.getCurrentCyoObject() instanceof Package) {
				Package p = (Package) listUI.getCurrentCyoObject();
				if (p.getCyoObject() != null) {
					for (int pIdx = 0; pIdx < p.getCyoObject().length; pIdx++) {
						Channel ch = (Channel) p.getCyoObject()[pIdx];

						// VDTRMASTER-5880
						Channel chInfo = CyoDataManager.getInstance().getChannelInfoById(ch.getId());
						if (chInfo.getCallLetters() != null && (chInfo.getCallLetters().equals(targetCallLetter)
								|| chInfo.getCallLetters().equals(sisterCallLetter))) {
							updateSubscribed(p);
							needExitPopup = true;
							found = true;
							break;
						}
					}

					if (found) {
						break;
					}
				}
			}
			listUI.handleKey(OCRcEvent.VK_DOWN);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ChangeChannelSelectionUI, processRedirect, found=" + found);
		}

		if (!found) {
			listUI.start(true);
		}
	}

	private void addCyoObject(Vector vec, CyoObject[] data) {
		for (int i = 0; i < data.length; i++) {
			vec.add(data[i]);
			if (data[i] instanceof Package) {
				Package p = (Package) data[i];
				p.setSubscribedOrginalInSession(String.valueOf(p.getSubscribedInSession()));
			} else if (data[i] instanceof Channel) {
				Channel p = (Channel) data[i];
				p.setSubscribedOrginalInSession(String.valueOf(p.getSubscribedInSession()));
			}

			if (data[i] instanceof Group && data[i].getCyoObject() != null) {
				addCyoObject(vec, data[i].getCyoObject());
			}
		}
	}

	//	private int findGroupIdx(String no) {
	//
	//		for (int i = 0; i < loadedGroups.length; i++) {
	//			if (loadedGroups[i].getNo().equals(no)) {
	//				return i;
	//			}
	////			else if (no.equals(INTERNATIONAL_GROUP_NO) && loadedGroups[i].getNo().equals(SPECIAL_GROUP_NO)) {
	////				return i;
	////			}
	//		}
	//
	//		return -1;
	//	}

	private void addCyoObjectInMyPicks(CyoObject obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("addCyoObjectInMyPicks = " + obj);
			Log.printDebug("currentList = " + currentList);
		}

		Log.printDebug("addCyoObjectInMyPicks-1 size " + addVector.size());
		Log.printDebug("addCyoObjectInMyPicks-1 size " + removeVector.size());
		Log.printDebug("addCyoObjectInMyPicks-1 removedParam " + removedParam);
		Log.printDebug("addCyoObjectInMyPicks-1 savedParam " + savedParam);

		removeVector.remove(obj);
		removedParam.remove(((Subscription) obj).getNo());

		if (addVector.contains(obj)) {
			addVector.remove(obj);
			savedParam.remove(((Subscription) obj).getNo());
		}

		addVector.add(obj);
		savedParam.add(((Subscription) obj).getNo());

		Log.printDebug("addCyoObjectInMyPicks-2 size " + addVector.size());
		Log.printDebug("addCyoObjectInMyPicks-2 size " + removeVector.size());
		Log.printDebug("addCyoObjectInMyPicks-2 removedParam " + removedParam);
		Log.printDebug("addCyoObjectInMyPicks-2 savedParam " + savedParam);
	}

	private void removeCyoObjectInMyPicks(CyoObject obj) {
		Log.printInfo("removeCyoObjectInMyPicks = " + obj);
		Log.printDebug("currentList = " + currentList);

		Log.printDebug("removeCyoObjectInMyPicks " + obj);
		Log.printDebug("removeCyoObjectInMyPicks-1 size " + addVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-1 size " + removeVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-1 removedParam " + removedParam);
		Log.printDebug("removeCyoObjectInMyPicks-1 savedParam " + savedParam);

		addVector.remove(obj);
		savedParam.remove(((Subscription) obj).getNo());

		if (removeVector.contains(obj)) {
			removeVector.remove(obj);
			removedParam.remove(((Subscription) obj).getNo());
		}

		removeVector.add(obj);
		removedParam.add(((Subscription) obj).getNo());

		Log.printDebug("removeCyoObjectInMyPicks-2 size " + addVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-2 size " + removeVector.size());
		Log.printDebug("removeCyoObjectInMyPicks-2 removedParam " + removedParam);
		Log.printDebug("removeCyoObjectInMyPicks-2 savedParam " + savedParam);
	}

	public String getSortString() {
		if (sort.equals(Param.SORT_ALPHABETICAL)) {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_ALPHABETICAL);
		} else {
			return BaseUI.getMenuText(KeyNames.SORTED_BY_CHANNEL_NUMBER);
		}
	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);
		removePopup(pop);

		if (gotoHotKey) {
			gotoHotKey = false;
			super.popupClosed(pop, msg);
			return;
		}

		if (messagePopupVector != null && messagePopupVector.size() > 0) {
			Message message = (Message) messagePopupVector.remove(0);

			// Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
			notiPop.setExplain(message.getText());
			String[] proccessedMsg = notiPop.getExplain();

			if (message.getType().equals("3")) {
				// show CYO_13
				if (message.getId().equals("35")) {
					// VDTRMASTER-5747
					notiPop.stop();

					vPop.setIcon(true);
					setPopup(vPop);
					vPop.show(this, message.getText());
				} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
					// VDTRMASTER-5747
					notiPop.stop();

					setPopup(infoPopup);
					infoPopup.show(this, message);
				} else if (proccessedMsg == null) {
					// VDTRMASTER-5747
					notiPop.stop();

					vPop.setIcon(false);
					setPopup(vPop);
					vPop.show(this, message.getText());
				} else {
					setPopup(notiPop);
					//notiPop.setExplain(message.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				}
			} else if (message.getType().equals("5")) {
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
				expired = true;
			} else if (message.getType().equals("1")) {
				// show CYO_14
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
			} else {
				setPopup(notiPop);
				notiPop.setExplain(message.getText());
				notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
			}
			repaint();
			return;
		}

		if (expired) {
			cyoReStart();
		} else if (pop.equals(vPop)) {
			repaint();
		} else if (pop.equals(notiPop)) {
			if (pop.getState() == NotificationPopupUI.STATE_CANCEL) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					// if (backButton) {
					// CyoDataManager.getInstance().request(URLType.LOGIN,
					// null);
					// } else {
					// //resetData();
					// dispose();
					// savedParam.clear();
					// removedParam.clear();
					// CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE,
					// true);
					// }

					CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_VALIDATE_CANCEL) {
				Integer result = (Integer) msg;
				if (result.intValue() == 0) {
					buttonClick = false;
					setData();
				} else if (result.intValue() == 1) {
					// resetData();
					dispose();
					savedParam.clear();
					removedParam.clear();
					CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
				} else if (result.intValue() == 2) {
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_KEEP_SELECTIION) {
				Boolean result = (Boolean) msg;
				if (result.booleanValue()) {
					focus = 0;
					currentList = listUI;
					setFooter();
					state = STATE_LIST;
				} else {
					if (lastRequestedURLType == URLType.SAVE_GROUP || lastRequestedURLType == URLType.SAVE_SELF_SERVE) {
						if (needParamForRequiresUpsell) {
							Hashtable param = new Hashtable();
							param.put("requiresUpsell", "false");
							needParamForRequiresUpsell = false;
							CyoDataManager.getInstance().request(URLType.VALIDATION, param);
						} else {
							CyoDataManager.getInstance().request(URLType.VALIDATION, null);
						}
					} else {
						CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
					}
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL_CANCEL) {
				Boolean result = (Boolean) msg;

				// VDTRMASTER-5923
				if (result.booleanValue()) {

					for (int a = 0; a < listUI.getCyoObjectLength(); a++) {
						CyoObject obj = listUI.getCyoObject(a);

						if (obj instanceof Package) {
							if (obj.getCyoObject() != null) {
								CyoObject[] subObj = obj.getCyoObject();
								for (int j = 0; j < subObj.length; j++) {
									Subscription ssubObj = (Subscription) subObj[j];
									if (((Package) obj).getSubscribedInSession() && ((Package) obj).getModifiable()
											.equals("0") && (ssubObj.getModifiable().equals("1") || ssubObj
											.getModifiable().equals("0"))) {
										Log.printInfo("1getid = " + ssubObj.getId());
										if (addVector.contains(ssubObj)) {
											addVector.remove(ssubObj);
										}

										if (!removeVector.contains(ssubObj) && ssubObj.getNo() != null) {
											removeVector.add(ssubObj);
										}
										ssubObj.setSubscribedInSession(false);
									}
								}

								if (addVector.contains(obj)) {
									addVector.remove(obj);
								}

								if (!removeVector.contains(obj) && obj != null) {
									removeVector.add(obj);
								}

								if (((Package) obj).getModifiable().equals("0")) {
									((Package) obj).setSubscribedInSession(false);
								}
							}
						} else if (obj instanceof Channel) {
							Channel ch = (Channel) obj;
							if ((ch.getModifiable().equals("0")) && ch.getSubscribedInSession()) {
								Log.printInfo("2getid = " + ch.getId());

								if (addVector.contains(obj)) {
									addVector.remove(obj);
								}
								if (!removeVector.contains(obj) && ch.getNo() != null) {
									removeVector.add(obj);
								}
								ch.setSubscribedInSession(false);
							}
						}
					}
					addVector.clear();
					renderer.update(this);
				}
			} else if (pop.getState() == NotificationPopupUI.STATE_NORMAL) {
				// if (saveMessageView) {
				// saveMessageView = false;

				if ((lastRequestedURLType == URLType.SAVE_GROUP || lastRequestedURLType == URLType.SAVE_SELF_SERVE)
						&& lastObject.getSuccess().equals("1")) {
					if (!comeFromBack) {
						if (!channelInfoBoolean) {
							if (needParamForRequiresUpsell) {
								Hashtable param = new Hashtable();
								param.put("requiresUpsell", "false");
								needParamForRequiresUpsell = false;
								CyoDataManager.getInstance().request(URLType.VALIDATION, param);
							} else {
								CyoDataManager.getInstance().request(URLType.VALIDATION, null);
							}
						} else {
							channelInfoBoolean = false;
						}
					} else {
						comeFromBack = false;
					}
				}
				// }
			} else if (pop.getState() == NotificationPopupUI.STATE_CANCEL_CHANGE) {

				Integer result = (Integer) msg;
				if (result.intValue() == 0) {
					CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
				}
			}
			repaint();
		} else if (pop.equals(validationUpSellPopupUI)) {
			Boolean result = (Boolean) msg;
			if (result.booleanValue()) {
				Message message = validationUpSellPopupUI.getMessage();

				DataCenter.getInstance().put("UPSELL_MESSAGE", message);

				if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_PACKAGE) || message
						.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_BLOCK)) {

					CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);

				} else if (message.getOptimizationType().equals(Message.OPTIMIZATION_TYPE_A_LA_CARTE)) {
					// NOTHING;
					Log.printDebug("ChangeChannelSelectionUI, popupClosed, optimizationType is"
							+ " OPTIMIZATION_TYPE_A_LA_CARTE");
				}

			} else {
				// VDTRMASTER-6010
				if (isFromISA) {
					CyoDataManager.getInstance().request(URLType.VALIDATION, null);
				} else {
					needParamForRequiresUpsell = true;
					CyoDataManager.getInstance().setNeedParamForRequiresUpsell(true);
					setData();
				}
			}
		}
	}

	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.INFO_ON) {
			Log.printInfo(
					"ChangeChannelSelectionUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
							+ obj);
		}

		lastRequestedURLType = type;
		lastObject = obj;

		if (type == URLType.LOGIN) {

			if (!comeFromBack) {
				InitialStateUI.shortcuts = (Shortcuts) obj;

				if (InitialStateUI.shortcuts.getSuccess().equals("1")) {
					dispose();
					savedParam.clear();
					removedParam.clear();
					CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
				} else {
					// POPUP
					if (InitialStateUI.shortcuts.getCyoObject() != null) {
						Message msg = (Message) InitialStateUI.shortcuts.getCyoObject()[0];
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					}
				}
			} else {
				comeFromBack = false;
			}
			CyoController.getInstance().hideLoadingAnimation();
		} else if (type == URLType.LOAD_GROUP) {
			if (obj.getSuccess().equals("1")) {
				if (obj instanceof Services) {
					services = (Services) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: cyoData length = " + services
								.getCyoObject().length);
					}
					CyoObject[] groups = services.getCyoObject();

					Message message = null;

					if (groups[0] instanceof Message) {
						message = (Message) groups[0];
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: found message");
						}
					} else if (groups[0] instanceof Group) {
						buildDataForList(groups);
						setFooter();
						renderer.update(this);

					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: obj = " + obj);
						}
					}

					if (message != null && needDisplay5CPopup) {
						setPopup(infoPopup);
						infoPopup.show(this, message);
						needDisplay5CPopup = false;
					}
				} else if (obj instanceof Group) {
					Vector dataVector = new Vector();
					Group group = (Group) obj;
					CyoObject[] data = group.getCyoObject();

					addCyoObject(dataVector, data);

					CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
					dataVector.copyInto(rearrangeData);

					dataVector.clear();

					listUI.setCyoObject(rearrangeData);
					listUI.setHasFocus(true);
					listUI.start(true);
				}
				repaint();
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					setPopup(notiPop);
					notiPop.setExplain(msg.getText());
					notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					Log.printInfo("msg.getType() = " + msg.getType());
					if ((msg.getType()).equals("5")) {
						expired = true;
					}
				}
			}
		} else if (type == URLType.LOAD_A_LA_CARTE) {

			if (obj.getSuccess().equals("1")) {
				if (obj instanceof Services) {
					Vector vData = new Vector();

					services = (Services) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: cyoData length = " + services
								.getCyoObject().length);
					}

					CyoObject[] groups = services.getCyoObject();

					Vector groupVec = new Vector();
					CyoObject[] groupData = null;
					Message message = null;

					if (groups != null) {
						for (int i = 0; i < groups.length; i++) {
							if (groups[i] instanceof Group) {
								groupVec.add(groups[i]);
							} else if (groups[i] instanceof Message) {
								message = (Message) groups[i];
								if (Log.DEBUG_ON) {
									Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: found message");
								}
							}
						}
						groupData = new CyoObject[groupVec.size()];
						groupVec.toArray(groupData);
					}

					if (addVector == null) {
						addVector = new Vector();
					}

					if (removeVector == null) {
						removeVector = new Vector();
					}

					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI: customIdx = " + customIdx);
						Log.printDebug("ChangeChannelSelectionUI: addVectorArray[customIdx]: size = " + addVector.size());
						Log.printDebug("ChangeChannelSelectionUI: removeVectorArray[customIdx]: size = " + removeVector.size());

						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: groups = " + groups);
					}

					Hashtable addHashtable = new Hashtable();
					Hashtable removeHashtable = new Hashtable();

					for (int a = 0; a < addVector.size(); a++) {
						Subscription ss = (Subscription) addVector.elementAt(a);
						String key = ss instanceof Package || ss instanceof Product ? ((Subscription) ss).getNo() : ((Subscription) ss).getId();
						addHashtable.put(key, ss);
					}

					for (int a = 0; a < removeVector.size(); a++) {
						Subscription ss = (Subscription) removeVector.elementAt(a);
						String key = ss instanceof Package  || ss instanceof Product ? ((Subscription) ss).getNo() : ((Subscription) ss).getId();
						removeHashtable.put(key, ss);
					}

					if (listUI == null) {
						listUI = new ListUI(true);
					}

					CyoObject[] cur = listUI.getAllCyoObject();
					Hashtable orignalHash = new Hashtable();

					for (int a = 0; cur != null && a < cur.length; a++) {
						Subscription ss = (Subscription) cur[a];
						if (ss != null) {
							if (ss instanceof Package || ss instanceof Product ) {
								if (ss.getNo() != null) {
									orignalHash.put(ss.getNo(), ss);
								}
							} else if (ss instanceof Channel) {
								if (ss.getId() != null) {
									orignalHash.put(ss.getId(), ss);
								}
							}

						}
					}

					if (groupData != null) {
						// VDTRMASTER-5923
						loadedGroup = (Group) groupData[0];

						CyoObject[] data = groupData[0].getCyoObject();
						Vector sortData = new Vector();
						for (int a = 0; data != null && a < data.length; a++) {
							String key = data[a] instanceof Package || data[a] instanceof Product ?
									((Subscription) data[a]).getNo() :
									((Subscription) data[a]).getId();

							Subscription cyoObj = (Subscription) orignalHash.get(key);
							if (cyoObj != null) {
								cyoObj.setSequenceNo(((Subscription) data[a]).getSequenceNo());
								if (addHashtable.contains(key)) {
									updateSubscribed(cyoObj);
								} else if (removeHashtable.contains(key)) {
									updateSubscribed(cyoObj);
								}
								sortData.add(cyoObj);
							} else {
								sortData.add(data[a]);
							}
						}

						Collections.sort(sortData, CyoObjectComparator.comparator);

						for (int b = 0; b < sortData.size(); b++) {
							CyoObject coj = (CyoObject) sortData.elementAt(b);
							if (coj == null)
								continue;

							if (coj instanceof Package) {
								vData.add(coj);
								CyoObject[] channels = coj.getCyoObject();
								Vector vChannelData = new Vector();
								for (int c = 0; channels != null && c < channels.length; c++) {
									String key = channels[c] instanceof Package ?
											((Subscription) channels[c]).getNo() :
											((Subscription) channels[c]).getId();

									Subscription cyoObj = (Subscription) orignalHash.get(key);
									if (cyoObj != null) {

										cyoObj.setSequenceNo(((Subscription) channels[c]).getSequenceNo());

										if (addHashtable.contains(key)) {
											updateSubscribed(cyoObj);
										} else if (removeHashtable.contains(key)) {
											updateSubscribed(cyoObj);
										}

										vChannelData.add(cyoObj);
									} else {
										vChannelData.add(channels[c]);
									}
								}

								Collections.sort(vChannelData, CyoObjectComparator.comparator);
								//								for (int c = 0; c < vChannelData.size(); c++) {
								//									Channel ch = (Channel) vChannelData.elementAt(c);
								//									vData.add(ch);
								//								}
								CyoObject[] channelArr = new CyoObject[vChannelData.size()];
								vChannelData.copyInto(channelArr);
								coj.setCyoObject(channelArr);
								vChannelData.clear();
							} else if (coj instanceof Channel) {
								vData.add(coj);
							}
						}
					}

					CyoObject[] rearrangeData = new CyoObject[vData.size()];
					vData.copyInto(rearrangeData);

					listUI.setCyoObject(rearrangeData);
					listUI.setHasFocus(true);
					listUI.start(true);

					currentList = listUI;
					setFooter();
					renderer.update(this);

					// VDTRMASTER-5924 & VDTRMASTER-5880
					if (CyoController.getInstance().getRedirectionCode() != CyoController.REDIRECTION_CODE_NONE) {
						processRedirect();
					}

					if (message != null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("SelfServeUI: receivedCyoData: found message");
						}
						if (needDisplay5CPopup) {
							setPopup(infoPopup);
							infoPopup.show(this, message);
							needDisplay5CPopup = false;
						}
					}
				}
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					Log.printInfo("msg.getType() = " + msg.getType());
					if ((msg.getType()).equals("5")) {
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						expired = true;
					}
				}
			}

		} else if (type == URLType.SAVE_GROUP || type == URLType.SAVE_SELF_SERVE) {
			String typeStr = type == URLType.SAVE_GROUP ? "SAVE_GROUP" : "SAVE_SELF_SERVE";

			if (Log.DEBUG_ON) {
				Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: " + typeStr + " success = " + obj.getSuccess());
			}

			Messages messages = (Messages) obj;

			if (messages.getSuccess().equals("1")) {
				CyoObject[] subObjs = messages.getCyoObject();

				if (subObjs != null) {
					messagePopupVector.clear();
					for (int i = 0; i < subObjs.length; i++) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: subObjs[" + i + "] = "
									+ subObjs[i]);
						}

						if (subObjs[i] instanceof Message) {
							messagePopupVector.add(subObjs[i]);
						}
					}
					StringBuffer sf = new StringBuffer();
					Message message = (Message) messagePopupVector.remove(0);
					if (Log.DEBUG_ON) {
						Log.printDebug("message.getType() " + message.getType() + " message getId() " + message.getId());
					}
					if (message.getType().equals("0")) {// &&
						// message.getId().equals("140")
						sf.append(message.getText());
						sf.append("\n");
						setPopup(notiPop);
						notiPop.setExplain(sf.toString());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
					} else if (message.getType().equals("1")) {

						if (message.getOptimizationType() != null) {
							if (Log.DEBUG_ON) {
								Log.printDebug("message.optimizationType=" + message.getOptimizationType());
								Log.printDebug("message.optimizationCode=" + message.getOptimizationCode());
							}
							needParamForRequiresUpsell = false;
							setPopup(validationUpSellPopupUI);
							validationUpSellPopupUI.show(this, message);
						} else {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
						}
					} else {

						if (needParamForRequiresUpsell) {
							Hashtable param = new Hashtable();
							param.put("requiresUpsell", "false");
							needParamForRequiresUpsell = false;
							CyoDataManager.getInstance().request(URLType.VALIDATION, param);
						} else {
							CyoDataManager.getInstance().request(URLType.VALIDATION, null);
						}
					}
				} else {
					if (needParamForRequiresUpsell) {
						Hashtable param = new Hashtable();
						param.put("requiresUpsell", "false");
						needParamForRequiresUpsell = false;
						CyoDataManager.getInstance().request(URLType.VALIDATION, param);
					} else {
						CyoDataManager.getInstance().request(URLType.VALIDATION, null);
					}
				}
			} else {
				CyoObject[] subObjs = obj.getCyoObject();

				if (subObjs != null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: subObjs = " + subObjs);
					}
					for (int i = 0; i < subObjs.length; i++) {
						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: subObjs = " + subObjs[i]);
						if (subObjs[i] instanceof Message) {
							messagePopupVector.add(subObjs[i]);
						}
					}

					if (messagePopupVector != null && messagePopupVector.size() > 0) {
						Message message = (Message) messagePopupVector.remove(0);

						// Message line length를 파악하여서 작은 팝업으로 보여 줄 수 있는지 확인
						notiPop.setExplain(message.getText());
						String[] proccessedMsg = notiPop.getExplain();

						if (message.getType().equals("3")) {
							// show CYO_13
							if (message.getId().equals("35")) {
								// VDTRMASTER-5747
								notiPop.stop();

								vPop.setIcon(true);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
								// VDTRMASTER-5747
								notiPop.stop();

								setPopup(infoPopup);
								infoPopup.show(this, message);
								notiPop.setExplain(null);
							} else if (proccessedMsg == null) {
								// VDTRMASTER-5747
								notiPop.stop();

								vPop.setIcon(false);
								setPopup(vPop);
								vPop.show(this, message.getText());
							} else {
								setPopup(notiPop);
								//notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						} else if ((message.getType()).equals("5")) {
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							expired = true;
						} else {
							// show CYO_14
							setPopup(notiPop);
							notiPop.setExplain(message.getText());
							notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						}
					}
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: subObjs is null");
					}
				}
			}
			repaint();
		} else if (type == URLType.VALIDATION) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: VALIDATION success = " + obj.getSuccess());
			}
			savedParam.clear();
			removedParam.clear();
			if (obj instanceof Services) {
				Services services = (Services) obj;
				if (services.getSuccess().equals("1")) {
					stop();
					if (services.getMessage() != null) {
						setPopup(notiPop);
						notiPop.setExplain(services.getMessage()[0].getText());
						notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
					} else {
						if (buttonClick) {
							// goto CYO_015
							if (btnFocus == 0) {
								DataCenter.getInstance().put("VALIDATION", obj);
								CyoController.getInstance().showUI(BaseUI.UI_VALIDATION, false);
							}
						} else {
							CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, true);
						}
					}
				} else {
					CyoObject[] subObjs = services.getCyoObject();
					if (subObjs != null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: subObjs = " + subObjs);
						}
						for (int i = 0; i < subObjs.length; i++) {
							if (Log.DEBUG_ON) {
								Log.printDebug(
										"ChangeChannelSelectionUI: receiveCyoData: subObjs[" + i + "] = " + subObjs[i]);
							}

							if (subObjs[i] instanceof Message) {
								messagePopupVector.add(subObjs[i]);
							}
						}
						if (messagePopupVector != null && messagePopupVector.size() > 0) {
							Message message = (Message) messagePopupVector.remove(0);

							if (Log.DEBUG_ON) {
								Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: message = " + message);
								Log.printDebug(
										"ChangeChannelSelectionUI: receiveCyoData: message = " + message.getId());
							}

							notiPop.setExplain(message.getText());
							String[] proccessedMsg = notiPop.getExplain();

							if (message.getType().equals("3")) {
								// show CYO_13
								if (message.getId().equals("35")) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(true);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else if (message.getText().indexOf(FIVE_C_ICON) > -1) { // VDTRMASTER-5667
									// VDTRMASTER-5747
									notiPop.stop();

									setPopup(infoPopup);
									infoPopup.show(this, message);
									notiPop.setExplain(null);
								} else if (proccessedMsg == null) {
									// VDTRMASTER-5747
									notiPop.stop();

									vPop.setIcon(false);
									setPopup(vPop);
									vPop.show(this, message.getText());
								} else {
									setPopup(notiPop);
									//notiPop.setExplain(message.getText());
									notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								}
							} else if ((message.getType()).equals("5")) {
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
								expired = true;
							} else if (message.getId().equals("1")) {
								setPopup(notiPop);
								notiPop.setExplain(services.getMessage()[0].getText());
								notiPop.show(this, NotificationPopupUI.STATE_KEEP_SELECTIION);
							} else {
								// show CYO_14
								setPopup(notiPop);
								notiPop.setExplain(message.getText());
								notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
							}
						}
						repaint();
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: subObjs is null");
						}
					}
				}
			}
		} else if (type == URLType.LOAD_PACKAGE_CATALOG) {
			if (obj.getSuccess().equals("1")) {
				if (obj instanceof Services) {
					Vector vData = new Vector();

					services = (Services) obj;
					if (Log.DEBUG_ON) {
						Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: cyoData length = " + services
								.getCyoObject().length);
					}

					Log.printDebug("ChangeChannelSelectionUI: customIdx = " + customIdx);

					CyoObject[] catalogs = services.getCyoObject();
					Log.printDebug("ChangeChannelSelectionUI: receiveCyoData: catalogs = " + catalogs);
					if (catalogs != null) {
						Log.printDebug(
								"ChangeChannelSelectionUI: receiveCyoData: catalogs.length = " + catalogs.length);
					}

					if (listUI == null) {
						buildDataForList(catalogs);
						setFooter();
					} else {
						// VDTRMASTER-5923
						loadedCatalog = (Catalog) catalogs[0];

						Hashtable addHashtable = new Hashtable();
						Hashtable removeHashtable = new Hashtable();

						if (addVector != null) {
							Log.printDebug(
									"ChangeChannelSelectionUI: addVectorArray[customIdx]: size = " + addVector.size());

							for (int a = 0; a < addVector.size(); a++) {
								Subscription ss = (Subscription) addVector.elementAt(a);
								String key = ss instanceof Package || ss instanceof Product ?
										((Subscription) ss).getNo() :
										((Subscription) ss).getId();
								addHashtable.put(key, ss);
							}

						} else {
							Log.printDebug("ChangeChannelSelectionUI: addVectorArray[customIdx] is null");
						}

						if (removeVector != null) {
							Log.printDebug(
									"ChangeChannelSelectionUI: removeVectorArray[customIdx]: size = " + removeVector
											.size());
							for (int a = 0; a < removeVector.size(); a++) {
								Subscription ss = (Subscription) removeVector.elementAt(a);
								String key = ss instanceof Package || ss instanceof Product ?
										((Subscription) ss).getNo() :
										((Subscription) ss).getId();
								removeHashtable.put(key, ss);
							}
						} else {
							Log.printDebug("ChangeChannelSelectionUI: removeVectorArray[customIdx] is null");
						}

						CyoObject[] cur = listUI.getAllCyoObject();
						Hashtable orignalHash = new Hashtable();

						for (int a = 0; cur != null && a < cur.length; a++) {
							if (cur[a] instanceof Subscription) {
								Subscription ss = (Subscription) cur[a];
								if (ss != null) {
									if (ss instanceof Package || ss instanceof Product) {
										if (ss.getNo() != null) {
											orignalHash.put(ss.getNo(), ss);
										}
									} else if (ss instanceof Channel) {
										if (ss.getId() != null) {
											orignalHash.put(ss.getId(), ss);
										}
									}

								}
							}
						}

						if (catalogs != null) {
							loadedCatalog = (Catalog) catalogs[0];
							CyoObject[] data = catalogs[0].getCyoObject();
							Vector dataVector = new Vector();
							addCyoObject(dataVector, data);
							data = new CyoObject[dataVector.size()];
							dataVector.copyInto(data);
							dataVector.clear();

							Vector sortData = new Vector();
							for (int a = 0; data != null && a < data.length; a++) {
								if (data[a] instanceof Subscription) {
									String key = data[a] instanceof Package || data[a] instanceof Product ?
											((Subscription) data[a]).getNo() :
											((Subscription) data[a]).getId();

									Subscription cyoObj = (Subscription) orignalHash.get(key);
									cyoObj.setSequenceNo(((Subscription) data[a]).getSequenceNo());

									if (addHashtable.containsKey(key)) {
										updateSubscribed(data[a]);
										addVector.remove(cyoObj);
										removeVector.remove(cyoObj);
									} else if (removeHashtable.containsKey(key)) {
										updateSubscribed(data[a]);
										addVector.remove(cyoObj);
										removeVector.remove(cyoObj);
									}
									sortData.add(cyoObj);
								} else {
									sortData.add(data[a]);
								}

							}

							Collections.sort(sortData, CyoObjectComparator.comparator);

							for (int b = 0; b < sortData.size(); b++) {
								CyoObject coj = (CyoObject) sortData.elementAt(b);
								if (coj == null)
									continue;

								if (coj instanceof Package) {
									vData.add(coj);
									CyoObject[] channels = coj.getCyoObject();
									Vector vChannelData = new Vector();
									for (int c = 0; channels != null && c < channels.length; c++) {
										String key = channels[c] instanceof Package || channels[c] instanceof Product ?
												((Subscription) channels[c]).getNo() :
												((Subscription) channels[c]).getId();

										Subscription cyoObj = (Subscription) orignalHash.get(key);
										if (cyoObj == null)
											continue;

										cyoObj.setSequenceNo(((Subscription) channels[c]).getSequenceNo());

										if (addHashtable.containsKey(key)) {
											updateSubscribed(cyoObj);
											addVector.remove(cyoObj);
											removeVector.remove(cyoObj);
										} else if (removeHashtable.containsKey(key)) {
											updateSubscribed(cyoObj);
											addVector.remove(cyoObj);
											removeVector.remove(cyoObj);
										}

										vChannelData.add(cyoObj);
									}

									Collections.sort(vChannelData, CyoObjectComparator.comparator);
									for (int c = 0; c < vChannelData.size(); c++) {
										Channel ch = (Channel) vChannelData.elementAt(c);
										vData.add(ch);
									}
									vChannelData.clear();
								} else if (coj instanceof Channel) {
									vData.add(coj);
								} else {
									vData.add(coj);
								}
							}
						}

						CyoObject[] rearrangeData = new CyoObject[vData.size()];
						vData.copyInto(rearrangeData);

						listUI.setCyoObject(rearrangeData);
						listUI.setHasFocus(true);
						listUI.start(true);

						currentList = listUI;

						renderer.update(this);
						setFooter();

						// VDTRMASTER-5924 & VDTRMASTER-5880
						if (CyoController.getInstance().getRedirectionCode() != CyoController.REDIRECTION_CODE_NONE) {
							processRedirect();
						}
					}
				}
			} else {
				CyoObject[] objs = obj.getCyoObject();

				if (objs != null && objs[0] != null) {
					Message msg = (Message) objs[0];
					Log.printInfo("msg.getType() = " + msg.getType());
					if ((msg.getType()).equals("5")) {
						setPopup(notiPop);
						notiPop.setExplain(msg.getText());
						notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
						expired = true;
					}
				}
			}
		}
	}

	public void receiveCyoError(int type) {
		if (type == URLType.LOAD_GROUP) {
			sort = sort == Param.SORT_NUMBER ? Param.SORT_ALPHABETICAL : Param.SORT_NUMBER;
		}
	}

	public void selected(MenuItem item) {
		// sort
		Hashtable param = new Hashtable();
		// VDTRMASTER-6001 & VDTRMASTER-6002
		if (loadedCatalog != null) {
			param.put("catalogId", loadedCatalog.getCatalogId());
		} else {
			param.put("groupNo", loadedGroup.getNo());
		}

		boolean checkSort = false;

		if (item.equals(nSORT)) {
			param.put("sort", Param.SORT_NUMBER);
			checkSort = sort == Param.SORT_NUMBER ? true : false;
			sort = Param.SORT_NUMBER;
		} else {
			param.put("sort", Param.SORT_ALPHABETICAL);
			checkSort = sort == Param.SORT_ALPHABETICAL ? true : false;
			sort = Param.SORT_ALPHABETICAL;
		}

		if (!checkSort) {
			// VDTRMASTER-6001 & VDTRMASTER-6002
			if (loadedCatalog != null) {
				CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_CATALOG, param);
			} else {
				CyoDataManager.getInstance().request(URLType.LOAD_A_LA_CARTE, param);
			}
		}
	}

	public void canceled() {
	}

	public boolean getChanged() {
		Log.printDebug("ChangeChannelSelectionUI: getChanged()");

		if (checkAddVector() || checkRemoveVector()) {
			for (int j = 0; j < addVector.size(); j++) {
				CyoObject obj = (CyoObject) addVector.elementAt(j);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				} else if (obj instanceof Package) {
					Package sobj = (Package) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}

			for (int j = 0; j < removeVector.size(); j++) {
				CyoObject obj = (CyoObject) removeVector.elementAt(j);
				if (obj instanceof Subscription) {
					Subscription sobj = (Subscription) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				} else if (obj instanceof Package) {
					Package sobj = (Package) obj;
					Log.printDebug("getSubscribedOrginalInSession() " + sobj.getSubscribedOrginalInSession());
					Log.printDebug("getSubscribedInSession() " + sobj.getSubscribedInSession());
					if (sobj.getSubscribedOrginalInSession() != sobj.getSubscribedInSession()) {
						return true;
					}
				}
			}
			return false;
		} else {
			return false;
		}
	}

	// VDTRMASTER-6118
    public int getNumberOfChannels() {
	    return numberOfChannels;
    }
}

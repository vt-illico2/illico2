/**
 * @(#)InitialStateUI.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.gui.ConfirmationRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ConfirmationUI extends BaseUI {
    private ClickingEffect clickEffect;
    private CyoObject confirmCyoObj;

    public ConfirmationUI() {
        renderer = new ConfirmationRenderer();
        setRenderer(renderer);
    }

    public void start(final boolean back) {
        prepare();

        confirmCyoObj = (CyoObject) DataCenter.getInstance().get(KeyNames.KEY_CONFIRMATION);

        super.start(back);
    }

    public void stop() {
        confirmCyoObj = null;
        super.stop();
    }

    public boolean handleKey(final int code) {

        switch (code) {
            case OCRcEvent.VK_ENTER:
                if (clickEffect == null) {
                    clickEffect = new ClickingEffect(this, 5);
                }
                clickEffect.start(402, 381, 162, 40);
//            MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

                if (gotoHotKey) {
                    gotoNextApplication(keyCode);
                } else if ("VOD".equalsIgnoreCase(CyoController.getInstance().getTargetApp())) {
                    CyoController.getInstance().startUnboundApplication("VOD");
                } else {
                    gotoNextApplication(OCRcEvent.VK_MENU);
                }
//            if (mService != null) {
//                try {
//                    mService.startUnboundApplication("Menu", null);
//                } catch (RemoteException e) {
//                    Log.printWarning(e);
//                }
//            }
                return true;
            default:
                return false;
        }
    }

    // R7.3 - DDC-003
    public String[] getConfirmationMessge() {
        if (Log.DEBUG_ON) {
            Log.printDebug("ConfirmationUI, getConfirmationMessage, confirmCyoObj=" + confirmCyoObj);
        }

        String[] messages = null;
        if (confirmCyoObj != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ConfirmationUI, getConfirmationMessage, confirmCyoObj.getCyoObject()="
                        + confirmCyoObj.getCyoObject());
            }
            if (confirmCyoObj.getCyoObject() != null) {

                CyoObject[] cyoObjs = confirmCyoObj.getCyoObject();

                for (int i = 0; i < cyoObjs.length; i++) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ConfirmationUI, getConfirmationMessage, cyoObjs[" + i
                                + "]=" + cyoObjs[i]);
                    }
                    if (cyoObjs[i] instanceof Message) {
                        Message msg = (Message) cyoObjs[i];
                        if (Log.DEBUG_ON) {
                            Log.printDebug("ConfirmationUI, getConfirmationMessage, msg.type=" + msg.getType()
                                    + ", id=" + msg.getId());
                        }

                        if ("0".equals(msg.getType())
                                && ("157_DEMANDE_COMPLETEE".equals(msg.getId()) || "157_APPELER_SAC".equals(msg.getId()))) {
                            String message = msg.getText();

                            if (Log.DEBUG_ON) {
                                Log.printDebug("ConfirmationUI, getConfirmationMessage, message=" + message);
                            }

                            if (message != null) {
                                messages = TextUtil.split(message, FontResource.BLENDER.getFontMetrics(FontResource.BLENDER.getFont(22)), 640, 4);
                            }
                            break;
                        }
                    }
                }
            }
        }

//        if (Environment.EMULATOR) {
////            messages =  new String[] {"Videotron goes green !"};
////            messages =  new String[] {"Videotron goes green !", "A summary of these changes will be sent by email."};
////            messages =  new String[] {"Videotron goes green !", "A summary of these changes will be sent by email.",
////                    "Be sure to add your email address to your client account within 24 hours:",};
//            messages =  new String[] {"Videotron goes green !", "A summary of these changes will be sent by email.",
//                    "Be sure to add your email address to your client account within 24 hours:",
//                    "videotron.com/customercentre"};
//        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ConfirmationUI, getConfirmationMessage, messages=" + messages);
        }

        return messages;
    }
}

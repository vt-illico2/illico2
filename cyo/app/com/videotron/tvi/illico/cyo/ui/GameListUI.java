/**
 * @(#)ListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Game;
import com.videotron.tvi.illico.cyo.debug.DebugScreenManager;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.cyo.gui.GameListRenderer;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * ListUI display a list of service, group, package, channel etc.
 * @author Woojung Kim
 * @version 1.1
 */
public class GameListUI extends ListUI {

    private CyoObject lastObject;

    private Image gameImg;
    private Thread imgThread;
    
    private boolean gotoHelp = false;

    MediaTracker tracker = new MediaTracker(this);

    public GameListUI() {
        super(false);
        renderer = new GameListRenderer();
        setRenderer(renderer);

    }

    public void start(boolean back) {
        prepare();
        startIdx = 0;
        focus = 0;
        startGetImage();
        super.start(back);
    }

    public void stop() {
        if (gameImg != null) {
            tracker.removeImage(gameImg);
            gameImg.flush();
            gameImg = null;
        }

        if (imgThread != null) {
            imgThread.interrupt();
            imgThread = null;
        }
        Log.printDebug("gotoHelp = " + gotoHelp);
        if(!gotoHelp) {
            super.stop();
        }
    }
    
    public void gotoHelp(boolean b) {
        gotoHelp = b;
    }

    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_UP:
            keyUp();
            startGetImage();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            startGetImage();
            return true;
        case OCRcEvent.VK_PAGE_UP:
            if (currentDesc != null) {
                if (--descStartIdx < 0) {
                    descStartIdx = 0;
                }
            }
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            if (currentDesc != null) {
                if (++descStartIdx > currentDesc.length - 3) {
                    descStartIdx--;
                }
            }
            return true;
        default:
            return false;
        }
    }

    private void keyUp() {
        if (objs != null) {
            if (startIdx + focus >= objs.length) {
                startIdx--;
            } else if (startIdx + focus > 0) {
                if (focus > 3) {
                    focus--;
                } else if (startIdx > 0) {
                    if (focus < 4) {
                        startIdx--;
                    } else {
                        focus--;
                    }
                } else {
                    focus--;
                }
            }
        }
    }

    private void keyDown() {
        if (objs != null) {
            if (startIdx + focus < objs.length - 1) {
                if (focus < 2) {
                    focus++;
                } else if (startIdx + 7 < objs.length) {
                    if (focus > 2) {
                        startIdx++;
                    } else {
                        focus++;
                    }
                } else {
                    if (startIdx + focus > objs.length - 7) {
                        focus++;
                    } else {
                        startIdx++;
                        focus++;
                        if (startIdx + focus >= objs.length) {
                            startIdx--;
                        }
                    }
                }
            }
        }
    }

    public int getStartIdx() {
        return startIdx;
    }

    public CyoObject getCyoObject(int idx) {
        if (objs != null && idx < objs.length && objs[idx] != null) {
            DebugScreenManager.getInstance().addData(objs[idx]);
            return objs[idx];
        }

        return null;
    }

    public CyoObject getCurrentCyoObject() {
        int idx = startIdx + focus;
        return getCyoObject(idx);
    }

    public int getCyoObjectLength() {
        if (objs != null) {
            return objs.length;
        } else {
            return 0;
        }
    }

    public void setCyoObject(CyoObject[] cyoObjs) {
        this.objs = cyoObjs;
    }

    public boolean hasFocus() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    public boolean onFocus(int idx) {
        if (startIdx + focus == idx) {
            return true;
        }

        return false;
    }

    public int getFocusType(int idx) {

        if ((startIdx + focus) == idx) {
            return FOCUS_ON_ME;
        } else {
            return FOCUS_NONE;
        }
    }

    public String[] getDesc() {
        if (Log.INFO_ON) {
            Log.printInfo("ListUI: getDesc: startIdx = " + startIdx + "\t focus = " + focus);
        }
        String src = null;
        CyoObject currentObj = getCyoObject(startIdx + focus);

        if (lastObject == null || lastObject != currentObj) {
            descStartIdx = 0;
            src = ((Game) currentObj).getDesc();
            lastObject = currentObj;

            if (src != null) {
                currentDesc = TextUtil.split(src, BaseRenderer.fm14d, 410);
                return currentDesc;
            } else {
                currentDesc = null;
            }
        } else if (currentDesc != null) {
            return currentDesc;
        }

        return null;
    }

    public Image getGameImage() {
        return gameImg;
    }

    public void startGetImage() {
        if (imgThread != null) {
            imgThread.interrupt();
            imgThread = null;
        }

        if (gameImg != null) {
            tracker.removeImage(gameImg);
            gameImg.flush();
            gameImg = null;
        }

        imgThread = new ImageUpdater();
        imgThread.start();
    }

    class ImageUpdater extends Thread {

        public void run() {
            try {
                Thread.yield();
                Game game = (Game) getCurrentCyoObject();
                URL url = new URL(game.getLogo());
                URLConnection con = url.openConnection();
                if (Log.INFO_ON) {
                    if (con instanceof HttpURLConnection) {
                        HttpURLConnection hc = (HttpURLConnection) con;
                        Log.printInfo("URLRequestor.getBytes response = " + hc.getResponseCode());
                        Log.printInfo("URLRequestor.getBytes : " + hc.getResponseMessage());
                    }
                }

                BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                int c;
                while ((c = bis.read(buf)) != -1) {
                    baos.write(buf, 0, c);
                    Thread.sleep(50);
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug("URLRequestor.getBytes read bytes = " + baos.size());
                }
                if (con instanceof HttpURLConnection) {
                    ((HttpURLConnection) con).disconnect();
                }
                bis.close();
                gameImg = Toolkit.getDefaultToolkit().createImage(baos.toByteArray());
                tracker.addImage(gameImg, 0);
                tracker.waitForAll();
                FrameworkMain.getInstance().getHScene().repaint();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}

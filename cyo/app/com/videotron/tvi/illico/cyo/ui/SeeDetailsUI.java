/**
 * @(#)InitialStateUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.data.obj.Services;
import com.videotron.tvi.illico.cyo.gui.SeeDetailsRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class SeeDetailsUI extends BaseUI implements CyoDataListener {

    private SeeDetailsListUI list = new SeeDetailsListUI();

    /** The object of Service de base package */
    private CyoObject baseObj;
    private ClickingEffect clickEffect;

    private String priceDollarStr = "0", priceCentStr = ".00 $";
    
    public SeeDetailsUI() {
        renderer = new SeeDetailsRenderer();
        setRenderer(renderer);
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), getMenuText(KeyNames.BTN_BACK));
    }

    public void start(final boolean back) {
        CyoDataManager.getInstance().addCyoDataListener(this);
        if (!back) {
            prepare();
            CyoDataManager.getInstance().request(URLType.LOAD_FEES, null);
        } else {
        	if (list.needScroll()) {
                footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), getMenuText(KeyNames.BTN_SCROLL));
            }
        }
        super.start(back);
    }

    public void prepare() {
        focus = 1;
        priceDollarStr = "0";
        priceCentStr = ".00 $";
        super.prepare();
    }

    public void stop() {
        footer.removeButton(getMenuText(KeyNames.BTN_SCROLL));
        super.stop();
    }
    
    public void dispose() {
        list.stop();
    }

    public boolean handleKey(final int code) {
        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }
        
        switch (code) {
        case OCRcEvent.VK_UP:
            if (focus == 1) {
                focus = 0;
            }
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (++focus > 1) {
                focus = 1;
            }
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }

            if (focus == 0) {
                clickEffect.start(622, 352, 292, 50);
                // goto channel list basic

                Log.printInfo("SeeDetailsUI baseObj = " + baseObj);
                DataCenter.getInstance().put("Package", baseObj);
                CyoController.getInstance().showUI(BaseUI.UI_CHANNEL_LIST_NEW, false);
            } else {
                // back
                clickEffect.start(624, 419, 292, 50);
                CyoController.getInstance().showUI(lastUIId, true);
            }
            return true;
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
            if (list != null) {
                footer.clickAnimation(1);
                list.handleKey(code);
                repaint();
            }
            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0);
            CyoController.getInstance().showUI(lastUIId, true);
            return true;
        default:
            return false;
        }
    }

    public SeeDetailsListUI getList() {
        return list;
    }

    public String getPriceDollar() {
        return priceDollarStr;
    }

    public String getPriceCent() {
        return priceCentStr;
    }

    private void buildDataForList(CyoObject[] data) {

        Vector dataVector = new Vector();
        addCyoObject(dataVector, data);

        CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
        dataVector.copyInto(rearrangeData);

        list.setCyoObject(rearrangeData);

        if (list.needScroll()) {
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), getMenuText(KeyNames.BTN_SCROLL));
        }

        list.start(true);

        dataVector.clear();
    }

    private void addCyoObject(Vector vec, CyoObject[] data) {

        for (int i = 0; i < data.length; i++) {
            if (!(data[i] instanceof Channel)) {
                vec.add(data[i]);
            }

            if (data[i].getCyoObject() != null) {
                addCyoObject(vec, data[i].getCyoObject());
            }
        }
    }

    public void popupClosed(BaseUI pop, Object msg) {
        super.popupClosed(pop, msg);

        removePopup(pop);
        if(gotoHotKey) {
            gotoHotKey = false;
            super.popupClosed(pop, msg);
            return;
        }
        if(expired) {
            cyoReStart();
        } else if (pop instanceof NotificationPopupUI) {
            repaint();
        }
    }

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.INFO_ON) {
            Log.printInfo("SeeDetailsUI: receiveCyoData(): type = " + type + "\t state = " + state
                    + "\t obj = " + obj);

        }
        
        if (type == URLType.LOAD_FEES) {
            if (Log.DEBUG_ON) {
                Log.printDebug("SeeDetailsUI: receiveCyoData(): LOAD_FEES success = " + obj.getSuccess());
            }

            if (obj.getSuccess().equals("1")) {
                Services services = (Services) obj;
                CyoObject[] objs = services.getCyoObject();

                baseObj = objs[0].getCyoObject()[0];

                String priceStr = services.getPricetotalM();
                Log.printDebug("SeeDetailsUI: receiveCyoData(), priceStr=" + priceStr);

                if (priceStr != null) {
                    int dotIdx = priceStr.indexOf('.');
                    priceDollarStr = priceStr.substring(0, dotIdx);
                    priceCentStr = priceStr.substring(dotIdx) + " $";
                }

                // objs - group + fee
                buildDataForList(objs);

                repaint();
            } else {
                CyoObject[] objs = obj.getCyoObject();

                if (objs != null && objs[0] != null) {
                    Message msg = (Message) objs[0];
                    setPopup(notiPop);
                    notiPop.setExplain(msg.getText());
                    notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
                    
                    Log.printInfo("msg.getType() = " + msg.getType());
                    if((msg.getType()).equals("5")) {
                        expired = true;
                    }
                }
            }
        }
    }
    
    public void receiveCyoError(int type) {
    }
}

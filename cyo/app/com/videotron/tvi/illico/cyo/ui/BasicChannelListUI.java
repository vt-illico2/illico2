/**
 * @(#)ChangeChannelSelectionUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.Param;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.Channel;
import com.videotron.tvi.illico.cyo.data.obj.CyoObjectComparator;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Services;
import com.videotron.tvi.illico.cyo.gui.BasicChannelListRenderer;
import com.videotron.tvi.illico.cyo.popup.MenuItem;
import com.videotron.tvi.illico.cyo.popup.MenuListener;
import com.videotron.tvi.illico.cyo.popup.OptionScreen;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class BasicChannelListUI extends BaseUI implements CyoDataListener, MenuListener {
    public static final int STATE_LIST = 0x0;
    public static final int STATE_BUTTON = 0x1;

    private ListUI chList = new ChannelListUI();

    private Package curPackage;

    private int btnFocus = 0;

    private String sort;

    public OptionScreen optScr;
    public static MenuItem sortOption = new MenuItem("menu.sort");
    public static MenuItem nSORT = new MenuItem("menu.sorted.number");
    public static MenuItem aSORT = new MenuItem("menu.sorted.alphabetical");
    private ClickingEffect clickEffect;
    private ListUI currentList;

    private Services services;

    public BasicChannelListUI() {
        renderer = new BasicChannelListRenderer();
        setRenderer(renderer);
    }

    public void start(boolean back) {
        Log.printInfo("BasicChannelListUI Start = " + back);
        CyoDataManager.getInstance().addCyoDataListener(this);
        if (!back) {            
            sort = Param.SORT_ALPHABETICAL;

            sortOption.clear();
            sortOption.add(aSORT);
            sortOption.add(nSORT);

//            curPackage = (Package) DataCenter.getInstance().get("Package");
//            Log.printInfo("BasicChannelListUI curPackage = " + curPackage);
//            if (curPackage != null) {
//                buildDataForList(curPackage.getCyoObject());
//            }
            prepare();
            chList.setHasFocus(false);
            chList.setCyoObject(null);

            CyoObject curObj = (CyoObject) DataCenter.getInstance().get("BasicService");
            
            if (curObj != null && curObj instanceof Group) {
            	Group group = (Group) curObj;
                Hashtable param = new Hashtable();
                param.put("url", group.getUrl());
                CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
            } else {
            	Group group = (Group) curObj;
                Hashtable param = new Hashtable();
                param.put("groupNo", "1514");
                CyoDataManager.getInstance().request(URLType.LOAD_GROUP, param);
            }
            

        }
        super.start(back);
    }

    public void prepare() {
        state = STATE_BUTTON;
        btnFocus = 0;
        focus = 0;
        setFooter();
        super.prepare();
    }

    public void stop() {
        if (optScr != null) {
            optScr.stop();
            optScr.dispose();
        }
        optScr = null;
        //chList.setCyoObject(null);
        super.stop();
    }

    public void dispose() {
    	stop();
        clickEffect = null;
        super.dispose();
    }

    public boolean handleKey(int code) {
        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }
        
        switch (code) {
        case OCRcEvent.VK_LEFT:
            keyLeft();
            return true;
        case OCRcEvent.VK_RIGHT:
            keyRight();
            return true;
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case OCRcEvent.VK_PAGE_UP:
            pageUp();
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            pageDown();
            return true;
        case OCRcEvent.VK_ENTER:
            keyEnter();
            return true;
        case KeyCodes.COLOR_A:
            footer.clickAnimation(2); 
            if (optScr == null) {
                optScr = new OptionScreen();
            }

            if (sort == Param.SORT_NUMBER) {
                nSORT.setChecked(true);
                aSORT.setChecked(false);
            } else {
                nSORT.setChecked(false);
                aSORT.setChecked(true);
            }
            optScr.start(sortOption, this);

            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0);
            if(getLastUIId() == BaseUI.UI_SEE_DETAILS) {
                CyoController.getInstance().showUI(BaseUI.UI_SEE_DETAILS, true);
            } else {
                CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
            }
            return true;
        default:
            return false;
        }
    }

    private void keyLeft() {
        if (state == STATE_BUTTON && currentList.getCyoObjectLength() > 0) {
            state = STATE_LIST;
            currentList.setHasFocus(true);
        } else if (state == STATE_BUTTON) {
            state = STATE_LIST;
            btnFocus = 0;
        }
        repaint();

    }

    private void keyRight() {
        if (state == STATE_LIST) {
            state = STATE_BUTTON;
            currentList.setHasFocus(false);
            repaint();
        }
    }

    private void keyUp() {
        if (state == STATE_LIST) {
            currentList.handleKey(OCRcEvent.VK_UP);
        } else if (state == STATE_BUTTON) {
            if (--btnFocus < 0) {
                btnFocus = 0;
            }
        }
        repaint();
    }

    private void keyDown() {
        if (state == STATE_LIST) {
            currentList.handleKey(OCRcEvent.VK_DOWN);
        } else if (state == STATE_BUTTON) {
            if (++btnFocus > 1) {
                btnFocus = 1;
            }
        }
        repaint();
    }

    private void pageUp() {
        if (state == STATE_LIST) {
            footer.clickAnimation(1); 
            currentList.handleKey(OCRcEvent.VK_PAGE_UP);
            repaint();
        }
    }

    private void pageDown() {
        if (state == STATE_LIST) {
            footer.clickAnimation(1);
            currentList.handleKey(OCRcEvent.VK_PAGE_DOWN);
            repaint();
        }
    }

    private void keyEnter() {
        if (state == STATE_BUTTON) {
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }
            
            clickEffect.start(717, 383 + btnFocus * 37, 180, 35);
            
            if (btnFocus == 0) { // Back
                if(getLastUIId() == BaseUI.UI_SEE_DETAILS) {
                    CyoController.getInstance().showUI(BaseUI.UI_SEE_DETAILS, true);
                } else {
                    CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
                }
            } else if (btnFocus == 1) { // Help
                // help
                DataCenter.getInstance().put("MODE", "HELP");
                CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
            }
        }
        repaint();
    }

    public String getSortString() {
        if (sort.equals(Param.SORT_ALPHABETICAL)) {
            return BaseUI.getMenuText(KeyNames.SORTED_BY_ALPHABETICAL);
        } else {
            return BaseUI.getMenuText(KeyNames.SORTED_BY_CHANNEL_NUMBER);
        }
    }

    private void setFooter() {
        footer.reset();
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), BaseUI.getMenuText(KeyNames.BTN_SCROLL));
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_A), BaseUI.getMenuText(KeyNames.BTN_SORTING));
    }

    public int getButtonFocus() {
        return btnFocus;
    }

    public ListUI getCurrentList() {
        return currentList;
    }

    public Package getCurrentPackage() {
        return curPackage;
    }
    
//    private void buildDataForList(CyoObject[] data) {
//        Vector dataVector = new Vector();
//        addCyoObject(dataVector, data);
//        Log.printInfo("BasicChannelListUI dataVector = " + dataVector.size());
//        CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
//        dataVector.copyInto(rearrangeData);
//
//        chList.setCyoObject(rearrangeData);
//        // chList.setHasFocus(true);
//        chList.start(true);
//
//        dataVector.clear();
//        currentList = chList;
//    }
    
    private void buildDataForList(Group g) {
        Vector dataVector = new Vector();
        
//        for (int i = 0; i < g.getCyoObject().length; i++) {
//        	CyoObject obj = g.getCyoObject()[i];
//        	
//        	if (obj instanceof Package) {
//        		for (int j = 0; j < obj.getCyoObject().length; j++) {
//        			CyoObject child = obj.getCyoObject()[j];
//        			
//        			if (child instanceof Channel) {
//        				dataVector.add(child);
//        			}
//        		}
//        	} else if (obj instanceof Channel) {
//        		dataVector.add(obj);
//        	}
//        }
//        
//        //addCyoObject(dataVector, data);
//        Log.printInfo("BasicChannelListUI dataVector = " + dataVector.size());
//        CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
//        dataVector.copyInto(rearrangeData);

        if (Log.DEBUG_ON) {
            Log.printDebug("BasicChannelListUI, buildDataForList, data length="
                    + g.getCyoObject()[0].getCyoObject().length);
        }
        chList.setCyoObject(g.getCyoObject()[0].getCyoObject());
        // chList.setHasFocus(true);
        chList.start(true);

        dataVector.clear();
        currentList = chList;
        
        state = STATE_LIST;
        currentList.setHasFocus(true);
    }

    private void addCyoObject(Vector vec, CyoObject[] data) {

        for (int i = 0; i < data.length; i++) {
            vec.add(data[i]);
        }
    }

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.INFO_ON) {
            Log
                    .printInfo("BasicChannelListUI: receiveCyoData(): type = " + type + "\t state = " + state + "\t obj = "
                            + obj);
        }

        if (type == URLType.LOAD_GROUP) {
            if (obj instanceof Services) {
                Services services = (Services) obj;
                if (Log.DEBUG_ON) {
                    Log.printDebug("BasicChannelListUI: receiveCyoData: cyoData length = "
                            + services.getCyoObject().length);
                }
                CyoObject[] groups = services.getCyoObject();
                if (groups[0] instanceof Group) {
                    renderer.prepare(this);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("BasicChannelListUI: receiveCyoData: groups length = " + groups.length);
                        Log.printDebug("BasicChannelListUI: receiveCyoData: groups[0].getCyoObject length = "
                                + groups[0].getCyoObject().length);
                    }

                    //CyoObject[] packages = groups[0].getCyoObject();

                    //curPackage = (Package) groups[0].getCyoObject()[0];
                    //buildDataForList(curPackage.getCyoObject());
                    
                    buildDataForList((Group)groups[0]);
                } else {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("BasicChannelListUI: receiveCyoData: obj = " + obj);
                    }
                }
            }
        } else if (type == URLType.LOAD_A_LA_CARTE) {

            if (obj.getSuccess().equals("1")) {
                if (obj instanceof Services) {
                    Vector vData = new Vector();

                    services = (Services) obj;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("BasicChannelListUI: receiveCyoData: cyoData length = "
                                + services.getCyoObject().length);
                    }

                    CyoObject[] groups = services.getCyoObject();
                    Log.printDebug("BasicChannelListUI: receiveCyoData: groups = " + groups);

//                    if (groups != null) {
//                        CyoObject[] data = groups[0].getCyoObject();
//                        Vector sortData = new Vector();
//                        for (int a = 0; data != null && a < data.length; a++) {
//                            sortData.add(data[a]);
//                        }
//
//                        Collections.sort(sortData, CyoObjectComparator.comparator);
//
//                        for (int b = 0; b < sortData.size(); b++) {
//                            CyoObject coj = (CyoObject) sortData.elementAt(b);
//                            if (coj instanceof Package) {
////                                vData.add(coj);
//                                CyoObject[] channels = coj.getCyoObject();
//                                Vector vChannelData = new Vector();
//                                for (int c = 0; channels != null && c < channels.length; c++) {
//                                    vChannelData.add(channels[c]);
//                                }
//                                Collections.sort(vChannelData, CyoObjectComparator.comparator);
//                                for (int c = 0; c < vChannelData.size(); c++) {
//                                    Channel ch = (Channel) vChannelData.elementAt(c);
//                                    vData.add(ch);
////                                    Log.printInfo("Channel sort getSequenceNo = " + ch.getSequenceNo());
//                                }
//                                vChannelData.clear();
//                            } else if (coj instanceof Channel) {
//                                vData.add(coj);
//                            }
//                        }
//                    }
//
//                    CyoObject[] rearrangeData = new CyoObject[vData.size()];
//                    vData.copyInto(rearrangeData);
//
//                    chList.setCyoObject(rearrangeData);
//                    chList.start(true);
//                    currentList = chList;
//                    renderer.prepare(this);
//                    vData.clear();
                    buildDataForList((Group)groups[0]);
                }
            }

        }
    }

    public void receiveCyoError(int type) {
        if (type == URLType.LOAD_GROUP) {
            sort = sort == Param.SORT_NUMBER ? Param.SORT_ALPHABETICAL : Param.SORT_NUMBER;
        }
    }

    public void canceled() {

    }

    public void selected(MenuItem item) {
        // sort
//        Group parentGroup = (Group) curPackage.getParentObj();
//        Hashtable param = new Hashtable();
//        String no = parentGroup.getNo();
//        if (no == null) {
//            Log.printDebug("BasicChannelListUI: handleKey(): group no is null, so use 1514 for Basic Service");
//            no = "1514";
//        }
    	
    	Hashtable param = new Hashtable();
    	String no = "1514"; 
        param.put("groupNo", no);
        boolean checkSort = false;
        if (item.equals(nSORT)) {
            param.put("sort", Param.SORT_NUMBER);
            checkSort = sort == Param.SORT_NUMBER ? true : false;
            sort = Param.SORT_NUMBER;
        } else {
            param.put("sort", Param.SORT_ALPHABETICAL);
            checkSort = sort == Param.SORT_ALPHABETICAL ? true : false;
            sort = Param.SORT_ALPHABETICAL;
        }
        if (!checkSort) {
            CyoDataManager.getInstance().request(URLType.LOAD_A_LA_CARTE, param);
        }
    }
}

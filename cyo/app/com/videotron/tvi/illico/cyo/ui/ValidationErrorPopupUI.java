/**
 * @(#)ValidationErrorPopupUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.awt.Color;
import java.util.Enumeration;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.cyo.gui.ValidationErrorRenderer;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ValidationErrorPopupUI extends BaseUI {

    private ScrollTexts scrollTexts = new ScrollTexts();
    private ClickingEffect clickEffect;
    private BaseUI parent;
    
    public boolean iconViewing = false;

    public ValidationErrorPopupUI() {
        renderer = new ValidationErrorRenderer();
        setRenderer(renderer);
        scrollTexts.setForeground(Color.white);
        scrollTexts.setRowHeight(20);
        scrollTexts.setFont(FontResource.BLENDER.getFont(18));
        scrollTexts.setInsets(0, 0, 0, 0);
        scrollTexts.setBounds(240, 173, 481, 203);
        scrollTexts.setRows(9);
        scrollTexts.setVisible(true);
        footer.setBounds(306, 370, 306, 24);
        this.add(scrollTexts);
    }

    public void show(BaseUI parent, String text) {
        prepare();
        this.parent = parent;
        String str = TextUtil.replace(text, "\\n", "\n");
        scrollTexts.setContents(str);

        Log.printInfo("scrollTexts.getPageCount()" + scrollTexts.getPageCount());
        if(scrollTexts.getPageCount() > 1 ){
            footer.setVisible(true);
        } else {
            footer.setVisible(false);
        }
        
        super.start(false);
    }
    
    public void setIcon(boolean b){
        iconViewing = b;
    }

    public void stop() {
        iconViewing = false;
        super.stop();
    }

    public void dispose() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                dataCenter.removeImage(key);
            }
            imageKeys.clear();
        }
    }

    public boolean handleKey(int code) {
        switch (code) {
        case OCRcEvent.VK_PAGE_UP:
            if(scrollTexts.isVisible()) {
                footer.clickAnimation(0);
                scrollTexts.showPreviousPage();
            }
            return true;
        case OCRcEvent.VK_PAGE_DOWN:
            if(scrollTexts.isVisible()) {
                footer.clickAnimation(0);
                scrollTexts.showNextPage();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }
            
            clickEffect.start(400, 414, 162, 40);
            
            parent.popupClosed(this, Boolean.TRUE);
            return true;
        default:
            return true;
        }
    }
}

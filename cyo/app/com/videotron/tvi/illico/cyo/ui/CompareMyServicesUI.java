/**
 * @(#)InitialStateUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.ui;

import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Fee;
import com.videotron.tvi.illico.cyo.data.obj.Group;
import com.videotron.tvi.illico.cyo.data.obj.Services;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.gui.CompareMyServicesRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class CompareMyServicesUI extends BaseUI {

    private CompareListUI rightList = new CompareListUI(true);
    private CompareListUI leftList = new CompareListUI(false);
    private ClickingEffect clickEffect;
    private String afterPrice;
    private String beforePrice;

    public CompareMyServicesUI() {
        renderer = new CompareMyServicesRenderer();
        setRenderer(renderer);
        footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), getMenuText(KeyNames.BTN_BACK));
    }

    public void start(final boolean back) {
        if (!back) {
            prepare();
            CyoObject obj = (CyoObject) DataCenter.getInstance().get("COMPARE");

            Services services = (Services) obj;
            CyoObject[] objs = services.getCyoObject();

            // objs - group + fee
            buildDataForList(objs);
        }
        super.start(back);
    }

    public void prepare() {
        focus = 2;
        leftList.setCyoObject(null);
        rightList.setCyoObject(null);
        super.prepare();
    }

    public void stop() {
        super.stop();
    }

    public boolean handleKey(final int code) {
        if (getPopup() != null && getPopup().isVisible()) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }
        
        switch (code) {
        case OCRcEvent.VK_UP:
            focus = 0;
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (focus == 0) {
                focus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_LEFT:
            if (focus == 2) {
                focus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (focus == 1) {
                focus = 2;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }

            
            if (focus == 0) {
                clickEffect.start(486, 380, 414, 35);
                CyoController.getInstance().showUI(BaseUI.UI_CHANNEL_LIST_NEW, false);
            } else if (focus == 1) {
                // goto help
                clickEffect.start(487, 419, 206, 40);
                DataCenter.getInstance().put("MODE", "HELP");
                CyoController.getInstance().showUI(BaseUI.UI_INFORMATION, false);
            } else if (focus == 2) {
                // back
                clickEffect.start(694, 419, 206, 40);
                CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
            }
            return true;
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
            footer.clickAnimation(1);
            rightList.handleKey(code);
            repaint();
            return true;
        case KeyCodes.LAST:
            footer.clickAnimation(0);
            CyoController.getInstance().showUI(BaseUI.UI_MANAGE_TV_SERVICE, true);
            return true;
        default:
            return false;
        }
    }

    public CompareListUI getLeftList() {
        return leftList;
    }

    public CompareListUI getRightList() {
        return rightList;
    }

    public String getAfterPrice() {
        return afterPrice;
    }

    public String getCurrentPrice() {
        return beforePrice;
    }

    private void buildDataForList(CyoObject[] data) {
        Group currentService = (Group) data[0];
        Group newService = (Group) data[1];

        beforePrice = currentService.getPricetotal();
        afterPrice = newService.getPricetotal();

        // left list
        Vector dataVector = new Vector();
//        Group group = new Group();
//        group.setTitle(BaseUI.getMenuText(KeyNames.COMPARE_GROUP_TITLE));
//        dataVector.add(group);
                
        addCyoObject(dataVector, currentService.getCyoObject());
        CyoObject[] currentData = new CyoObject[dataVector.size()];
        dataVector.copyInto(currentData);

        leftList.setCyoObject(currentData);

        // right list
        dataVector.clear();
        addCyoObject(dataVector, newService.getCyoObject());

        CyoObject[] rearrangeData = new CyoObject[dataVector.size()];
        dataVector.copyInto(rearrangeData);

        rightList.setCyoObject(rearrangeData);

        if (rightList.needScroll()) {
            footer.removeButton(getMenuText(KeyNames.BTN_SCROLL));
            footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_PAGE), getMenuText(KeyNames.BTN_SCROLL));
        } else {
            footer.removeButton(getMenuText(KeyNames.BTN_SCROLL));
        }

        leftList.start(true);
        rightList.start(true);

        dataVector.clear();
    }

    private void addCyoObject(Vector vec, CyoObject[] data) {
        for (int i = 0; i < data.length; i++) {
            vec.add(data[i]);
            
            if (data[i].getCyoObject() != null) {
                addCyoObject(vec, data[i].getCyoObject());
            }
        }
    }
}

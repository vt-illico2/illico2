package com.videotron.tvi.illico.cyo.ui;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.cyo.gui.BaseRenderer;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.*;
import org.ocap.ui.event.OCRcEvent;

import java.awt.*;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-08-18.
 */
public class TVPackageDownGradePopupUI extends BaseUI {

	private ClickingEffect clickEffect;

	private BaseUI parentUI;
	private Message message;

	public TVPackageDownGradePopupUI() {
		setRenderer(new TVPackageDownGradeRenderer());

		clickEffect = new ClickingEffect(this, 5);
	}

	public void show(BaseUI parent, Message message) {
		this.parentUI = parent;
		this.message = message;

		prepare();

		start();
	}

	public boolean handleKey(int code) {

		switch (code) {
		case OCRcEvent.VK_LEFT:
			if (focus != 0) {
				focus = 0;
				repaint();
			}
			return true;
		case OCRcEvent.VK_RIGHT:
			if (focus != 1) {
				focus = 1;
				repaint();
			}
			return true;

		case OCRcEvent.VK_ENTER:

			clickEffect.start(223 + focus * 263, 355, 255, 32);

			if (focus == 0) {
				parentUI.popupClosed(this, Boolean.TRUE);
			} else if (focus == 1) {
				parentUI.popupClosed(this, Boolean.FALSE);
			}

			return true;

		case KeyCodes.LAST:
		case OCRcEvent.VK_EXIT:
			parentUI.popupClosed(this, Boolean.FALSE);
			return true;
		}

		return super.handleKey(code);
	}

	private class TVPackageDownGradeRenderer extends BaseRenderer {

		private String titleStr, explainStr, questionStr, yesBtnStr, noBtnStr;

		private Image notiIconImg;

		protected void loadImages(BaseUI ui) {
			notiIconImg = ui.getImage("icon_noti_or.png");

			FrameworkMain.getInstance().getImagePool().waitForAll();
		}

		public Rectangle getPreferredBounds(UIComponent c) {
			return Constants.SCREEN_BOUNDS;
		}

		public void prepare(UIComponent c) {

			titleStr = BaseUI.getMenuText(KeyNames.NOTIFICATION);
			questionStr = BaseUI.getMenuText(KeyNames.TV_PACKAGE_DOWNGRADE_QUESTION);

			if (message != null) {
				explainStr = message.getText();
			} else if (Environment.EMULATOR) {
				explainStr = "The package you selected no longer includes Sports or Premium channels.";
			}

			yesBtnStr = BaseUI.getMenuText(KeyNames.BTN_YES);
			noBtnStr = BaseUI.getMenuText(KeyNames.BTN_NO_CANCEL);

		}

		protected void paint(Graphics g, UIComponent c) {
			// dimmed bg
			g.setColor(dimmedBgColor);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

			// popup bg
			g.setColor(C044044044);
			g.fillRect(240, 113, 485, 299);
			g.setColor(C066066066);
			g.fillRect(260, 152, 445, 1);

			// title
			g.setFont(f24);
			g.setColor(popupTitleColor);
			int x = GraphicUtil.drawStringCenter(g, titleStr, 494, 140);
			g.drawImage(notiIconImg, x - 31, 119, c);

			// explain
			int startY = 217;
			int stepY = 20;
			g.setFont(f19);
			g.setColor(dimmedMenuColor);
			String[] msgStr = TextUtil.split(explainStr, g.getFontMetrics(), 400);

			if (msgStr.length % 2 == 0) {
				startY -= (msgStr.length / 2 + 10);
			} else {
				startY -= (msgStr.length / 2);
			}

			for (int i = 0; i < msgStr.length; i++) {
				GraphicUtil.drawStringCenter(g, msgStr[i], 482, startY + i * stepY);
			}

			// question
			GraphicUtil.drawStringCenter(g, questionStr, 482, 312);

			// button
			if (focus == 0) {
				g.setColor(C255191000);
				g.fillRect(273, 355, 205, 32);
				g.setColor(C157157157);
				g.fillRect(486, 355, 205, 32);
			} else {
				g.setColor(C157157157);
				g.fillRect(273, 355, 205, 32);
				g.setColor(C255191000);
				g.fillRect(486, 355, 205, 32);
			}
			g.setFont(f18);
			g.setColor(btnColor);
			GraphicUtil.drawStringCenter(g, yesBtnStr, 377, 377);
			GraphicUtil.drawStringCenter(g, noBtnStr, 589, 377);
		}
	}
}

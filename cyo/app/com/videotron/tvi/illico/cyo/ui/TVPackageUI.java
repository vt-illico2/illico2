package com.videotron.tvi.illico.cyo.ui;

import com.videotron.tvi.illico.cyo.KeyNames;
import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.data.URLType;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Package;
import com.videotron.tvi.illico.cyo.data.obj.PackageList;
import com.videotron.tvi.illico.cyo.gui.TVPackageRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import org.ocap.ui.event.OCRcEvent;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zestyman on 2016-08-08.
 */
public class TVPackageUI extends BaseUI implements CyoDataListener{

	private PackageList packageList;
	private Package[] packageArr;

	private int startIdx;
	private boolean isMovingToFirst;

	private TVPackageDownGradePopupUI tvDownGradePopup = new TVPackageDownGradePopupUI();

	public TVPackageUI() {
		setRenderer(new TVPackageRenderer());
	}

	public void prepare() {
		isMovingToFirst = false;
		focus = 0;
		startIdx = 0;
		super.prepare();
	}

	public void start(boolean back) {
		CyoDataManager.getInstance().addCyoDataListener(this);
		if (!back) {
			prepare();
		}
		CyoController.getInstance().resetRedirectionCode();
		CyoDataManager.getInstance().request(URLType.LOAD_PACKAGE_OFFER, null);

		footer.reset();
		footer.addButtonWithLabel(getFooterImage(PreferenceService.BTN_BACK), BaseUI.getMenuText(KeyNames.BTN_BACK));

		super.start();
	}

	public void stop() {
		super.stop();
	}

	public void dispose() {
		super.dispose();
	}

	public boolean handleKey(int code) {

		if (getPopup() != null && getPopup().isVisible()) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				Log.printInfo("TVPackageUI code = " + code);
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_LEFT:
			if (focus == 3) {
				focus = 2;
				repaint();
			} else if (startIdx > 0 && focus == 2) {
				startIdx--;
				repaint();
			} else if (focus > 0) {
				focus--;
				if (focus == 0) {
					isMovingToFirst = false;
				}
				repaint();
			}

			return true;
		case OCRcEvent.VK_RIGHT:
			if (focus < 2) {
				focus++;
				repaint();
			} else if (startIdx + 4 < packageArr.length) {
				startIdx++;
				repaint();
			} else {
				focus = 3;
				isMovingToFirst = true;
				repaint();
			}
			return true;
		case OCRcEvent.VK_ENTER:
			Package selectedPackage = packageArr[startIdx + focus];
			DataCenter.getInstance().put("TV_PACKAGE", selectedPackage);
			CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);

//			Package currentPackage = CyoDataManager.getInstance().getCurrentPackageInPackageList();
//			Package selectedPackage = packageArr[startIdx + focus];
//
//			if (Log.DEBUG_ON) {
//				Log.printDebug("PackageChannelListUI, comparePackages, currentPackage.no=" + currentPackage.getNo());
//				Log.printDebug("PackageChannelListUI, comparePackages, selectedPacakge.no=" + selectedPackage.getNo());
//			}
//
//			if (!selectedPackage.getNo().equals(currentPackage.getNo())) {
//				Hashtable param = new Hashtable();
//
//				param.put("add", selectedPackage.getNo());
//				param.put("remove", currentPackage.getNo());
//
//				if (Log.DEBUG_ON) {
//					Log.printDebug("TVPackageUI, handleKey, param=" + param);
//				}
//
//				CyoDataManager.getInstance().request(URLType.SAVE_GROUP, param);
//			} else {
//
//			}

			return true;
		case KeyCodes.LAST:
			CyoController.getInstance().showUI(lastUIId, true);
			return true;

		}

		return super.handleKey(code);
	}

	public Package[] getPackageData() {
		return packageArr;
	}

	public int getStartIdx() {
		return startIdx;
	}

	public boolean isMovingToFirst() {
		return isMovingToFirst;
	}

	public void popupClosed(BaseUI pop, Object msg) {
		super.popupClosed(pop, msg);

		remove(pop);
		pop.stop();

		// remove by VT
//		if (pop instanceof TVPackageDownGradePopupUI) {
//			Boolean result = (Boolean) msg;
//			if (result.booleanValue()) {
//				Package selectedPackage = packageArr[startIdx + focus];
//				DataCenter.getInstance().put("TV_PACKAGE", selectedPackage);
//				CyoController.getInstance().showUI(UI_PACKAGE_CHANNEL_LIST, false);
//			}
//		}
	}

	public void receiveCyoData(int type, int state, CyoObject obj) {
		if (Log.DEBUG_ON) {
			Log.printDebug("TvPackageUI, receiveCyoData, type=" + type + ", obj=" + obj);
		}

		if (type == URLType.LOAD_PACKAGE_OFFER) {
			packageList = (PackageList) obj;
			packageArr = packageList.getPackages();

			if (Log.DEBUG_ON) {
				Log.printDebug("TvPackageUI, receiveCyoData, type=" + type + ", packageArr=" + packageArr);
				if (packageArr != null) {
					Log.printDebug("TvPackageUI, receiveCyoData, packageArr.length=" + packageArr.length);
				}
			}
		}
	}

	public void receiveCyoError(int type) {

	}
}

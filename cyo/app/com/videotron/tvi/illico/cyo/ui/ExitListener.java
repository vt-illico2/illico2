package com.videotron.tvi.illico.cyo.ui;

import com.videotron.tvi.illico.cyo.controller.CyoController;
import com.videotron.tvi.illico.cyo.data.CyoDataListener;
import com.videotron.tvi.illico.cyo.data.obj.CyoObject;
import com.videotron.tvi.illico.cyo.data.obj.Message;
import com.videotron.tvi.illico.log.Log;

public class ExitListener extends BaseUI implements CyoDataListener{

    public void receiveCyoData(int type, int state, CyoObject obj) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ExitListener: receiveCyoData(): CONFIRMATION success = " + obj.getSuccess());
        }

        if (obj.getSuccess().equals("1")) {
            CyoController.getInstance().setNeedToNotify(true);
            CyoController.getInstance().showUI(BaseUI.UI_CONFIRMATION, false);
        } else {
            CyoObject[] objs = obj.getCyoObject();

            if (objs != null && objs[0] != null) {
                Message msg = (Message) objs[0];
                setPopup(notiPop);
                notiPop.setExplain(msg.getText());
                notiPop.show(this, NotificationPopupUI.STATE_NORMAL);
            }
        }
    }

    public void receiveCyoError(int type) {
        
    }

}

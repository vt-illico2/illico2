/**
 * @(#)CommunicationManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.communication;

import java.rmi.Remote;
import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;

/**
 * The CommunicationManager class lookup a remote Service of Other Applcation.
 * @author Woojung Kim
 * @version 1.1
 */
public class CommunicationManager {

    /** The XletContext of UPP Application. */
    private XletContext xletContext;
    /** The instance of CommunicationManager. */
    private static CommunicationManager instance = new CommunicationManager();

    /**
     * Instantiates a new CommunicationManager.
     * @return Singleton instance of this class.
     */
    public static CommunicationManager getInstance() {
        return instance;
    }

    /**
     * Start a lookups.
     * @param xContext XletContext for CYO application.
     */
    public void start(XletContext xContext) {
        if (Log.INFO_ON) {
            Log.printInfo("CommunicationManager: start()");
        }
        this.xletContext = xContext;

        lookUpService("/1/1/", MonitorService.IXC_NAME);
        lookUpService("/1/2030/", PreferenceService.IXC_NAME);
        lookUpService("/1/2110/", NotificationService.IXC_NAME);
        lookUpService("/1/2075/", LoadingAnimationService.IXC_NAME);
        lookUpService("/1/2025/", ErrorMessageService.IXC_NAME);
        lookUpService("/1/2100/", StcService.IXC_NAME);
        lookUpService("/1/2085/", VbmService.IXC_NAME);
        lookUpService("/1/2026/", EpgService.IXC_NAME);
    }

    /**
     * Dispose a resources.
     */
    public void dispose() {
    }

    /**
     * Look up a Service to communicate.
     * @param path The path of Service.
     * @param name The Key name of Service.
     */
    private void lookUpService(String path, final String name) {
        final String ixcName = path + name;
        Thread thread = new Thread("CYO.lookUp." + name) {
            public void run() {
                Remote remote = null;
                while (remote == null) {
                    try {
                        remote = IxcRegistry.lookup(xletContext, ixcName);
                    } catch (Exception ex) {
                        if (Log.WARNING_ON) {
                            Log.printWarning("CommunicationManager: not bound - " + name);
                        }
                    }
                    if (remote != null) {
                        if (Log.INFO_ON) {
                            Log.printInfo("CommunicationManager: found - " + name);
                        }
                        DataCenter.getInstance().put(name, remote);
                        return;
                    }
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        Log.printError(e);
                    }
                }
            }
        };
        thread.start();
    }
}

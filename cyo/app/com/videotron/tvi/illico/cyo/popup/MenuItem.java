package com.videotron.tvi.illico.cyo.popup;

import java.util.Vector;
import java.util.Enumeration;

/**
 * This class represents the menu item. Each item may have a child items.
 *
 * @version $Revision: 1.8 $ $Date: 2017/01/09 19:50:01 $
 * @author June Park
 */
public class MenuItem {

    /** menu key. */
    protected String key;

    /** children menu items. */
    protected Vector items = new Vector();
    /** parent menu item. */
    protected MenuItem parent;
    /** the data that specific on menu */
    protected Object menuData;

    protected boolean checked;

    /** Create menu item with specified key. */
    public MenuItem(String key) {
        this.key = key;
    }

    /** Create menu item with specified key and children. */
    public MenuItem(String key, Vector children) {
        this.key = key;
        Enumeration en = children.elements();
        while (en.hasMoreElements()) {
            add((MenuItem) en.nextElement());
        }
    }

    /** Create menu item with specified key and children. */
    public MenuItem(String key, MenuItem[] children) {
        this.key = key;
        for (int i = 0; i < children.length; i++) {
            add(children[i]);
        }
    }

    /** Return the key of this menu item. */
    public String getKey() {
        return key;
    }

    /** Clear all child menu items. */
    public void clear() {
        items.clear();
    }

    /** Adds the child menu item. */
    public void add(MenuItem item) {
        if (item != null) {
            item.parent = this;
            items.addElement(item);
        }
    }

    /** Adds the child menu item. */
    public void setItemAt(MenuItem item, int index) {
        item.parent = this;
        items.setElementAt(item, index);
    }

    /** Returns the child menu item. */
    public MenuItem getItemAt(int index) {
        return (MenuItem) items.elementAt(index);
    }

    /** Returns the index of child menu item. */
    public int indexOf(MenuItem item) {
        return items.indexOf(item);
    }

    /** Returns parent menu item. */
    public MenuItem getParent() {
        return parent;
    }

    /** Returns the size of child menu item. */
    public int size() {
        return items.size();
    }

    /** Checks this item has child or not. */
    public boolean hasChild() {
        return items.size() > 0;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean check) {
        this.checked = check;
    }

    /** Set the menu data. */
    public void setData(Object data) {
        menuData = data;
    }

    /** Returns the menu data. */
    public Object getData() {
        return menuData;
    }

}

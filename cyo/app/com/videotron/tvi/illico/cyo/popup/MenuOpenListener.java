package com.videotron.tvi.illico.cyo.popup;

/**
 * Defines a listener for menu selection.
 *
 * @version $Revision: 1.8 $ $Date: 2017/01/09 19:50:01 $
 * @author June Park
 */
public interface MenuOpenListener {

    void opened(MenuItem parent);

}

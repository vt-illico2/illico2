/**
 * @(#)CyoController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.controller;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.cyo.ui.*;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.cyo.App;
import com.videotron.tvi.illico.cyo.communication.CommunicationManager;
import com.videotron.tvi.illico.cyo.data.CyoDataManager;
import com.videotron.tvi.illico.cyo.debug.DebugScreenManager;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * CyoController is a core of CYO application.
 * @author Woojung Kim
 * @version 1.1
 */
public class CyoController implements KeyListener, DataUpdateListener, PreferenceListener, ErrorMessageListener {

    /** singleton object. */
    private static CyoController instance;
    /** Key name for data file from MS. */
    private static final String IB_DATA = "CYO_SERVER";
    /** The Remote Object of EpgService. */
    private EpgService epgService;
    /** The remote object of MonitorService. */
    private MonitorService monitorService;
    /** The Listener to return a confirmation to launch Screen Saver. */
    private ScreenSaverConfirmationListenerImpl sListener;
    /** The implement class for InbandDataListener. */
    private InbandDataListenerImpl inbandListner;
    /** The implement class for LogLevelListener. */
    private LogLevelAdapter logListener;
    /** The current version number. */
    private int currentVersion;
    /** object of current UI. */
    private BaseUI currentUI;
    /** Hastable to have a created UIs. */
    private Hashtable uiTable;
    /** id of current UI. */
    private int currentId;
    private int curIdForLoading;
    /** Indicate need or not to notify package is updated. */
    private boolean needToNotify;
    
    private String[] startParameter = null;
    
    private boolean isStarted;

    // Bundling
    public static int REDIRECTION_CODE_NONE = 0;
    public static int REDIRECTION_CODE_PACAKGE = 1;
    public static int REDIRECTION_CODE_A_LA_CARTE = 2;
    public int redirectionCode;
    public String targetCallLetter;
    public String targetApp;

    /** private constructor. create a Hashtable for UI. */
    private CyoController() {
        uiTable = new Hashtable();
    }

    /**
     * Return a singleton object.
     * @return singleton object.
     */
    public static CyoController getInstance() {
        if (instance == null) {
            instance = new CyoController();
        }

        return instance;
    }

    /**
     * Initialize.
     * @param ctx XletContext of CYO application.
     */
    public void init(XletContext ctx) {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: init()");
        }
        inbandListner = new InbandDataListenerImpl();
        sListener = new ScreenSaverConfirmationListenerImpl();
        logListener = new LogLevelAdapter();

        DataCenter.getInstance().addDataUpdateListener(IB_DATA, inbandListner);
        DataCenter.getInstance().addDataUpdateListener(PreferenceService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(MonitorService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(EpgService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(StcService.IXC_NAME, this);
        DataCenter.getInstance().addDataUpdateListener(VbmService.IXC_NAME, this);
        CommunicationManager.getInstance().start(ctx);
    }

    /**
     * Start a CyoController.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: start()");
        }
        isStarted = true;

        CyoDataManager.getInstance().setNeedParamForRequiresUpsell(false);

        String appName = FrameworkMain.getInstance().getApplicationName();
        if (monitorService != null) {
            try {
                monitorService.addScreenSaverConfirmListener(sListener, appName);
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
        DebugScreenManager.getInstance();
        CyoDataManager.changedData = false;
        curIdForLoading = 0;
        needToNotify = false;
        CyoDataManager.getInstance().start();
        startParameter = getStartParameter();
        VbmController.getInstance().writeStartLog(startParameter);

        redirectionCode = REDIRECTION_CODE_NONE;
        targetCallLetter = null;
        targetApp = null;

        if (startParameter != null) {
            if (Log.DEBUG_ON && startParameter != null) {
                for (int i = 0; i < startParameter.length; i++) {
                    Log.printDebug("CyoController, start(), startParameter[" + i + "]=" + startParameter[i]);
                }
            }

            if (startParameter.length > 2) {
                // come from ISA
                // format : {parent app, targetApp, package or aLaCarte, optional:callLetter}}
                targetApp = startParameter[1];

                if (startParameter[2].equals("package")) {
                    redirectionCode = REDIRECTION_CODE_PACAKGE;
                    if (startParameter.length > 3) {
                        targetCallLetter = startParameter[3];
//                        showUI(BaseUI.UI_CHANGE_CHANNEL_SELECTION, false);
                    } else {
                        targetCallLetter = null;
//                        showUI(BaseUI.UI_TV_PACKAGE_LIST, false);
                    }
                } else if (startParameter[2].equals("aLaCarte")) {
                    redirectionCode = REDIRECTION_CODE_A_LA_CARTE;
                    targetCallLetter = startParameter[3];
//                    showUI(BaseUI.UI_MANAGE_GROUP, false);
                }
            }
            showUI(BaseUI.UI_INITIAL_STATE, false);
        } else {
            showUI(BaseUI.UI_INITIAL_STATE, false);
        }
    }

    /**
     * Stop a CyoController.
     */
    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: stop()");
        }
        hideLoadingAnimation();
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("CyoController: isStarted=" + isStarted);
        }
        
        if (isStarted) {
        	isStarted = false;
	        DebugScreenManager.getInstance().hide();
	        hideUI();
	        UserEventManager.getInstance().dispose();
	        CyoDataManager.getInstance().stop();
	        VbmController.getInstance().writeEndLog();
	        String appName = FrameworkMain.getInstance().getApplicationName();
	        if (monitorService != null) {
	            try {
	                monitorService.removeScreenSaverConfirmListener(sListener, appName);
	            } catch (RemoteException e) {
	                Log.print(e);
	            }
	        }
	
	        if (needToNotify && epgService != null) {
	            try {
	                epgService.notifyChannelPackageUpdated();
	            } catch (RemoteException e) {
	                Log.printError(e);
	            }
	        }
	//         DataCenter.getInstance().dispose();

            CyoDataManager.getInstance().clearUrlStack();
        }

    }

    /**
     * Dispose a CyoController.
     */
    public void dispose() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: dispose()");
        }
        stop();
    }
    
    public void disposeScene(int uiId) {
        BaseUI requestUI;
        if (uiTable.containsKey(String.valueOf(uiId))) {
            requestUI = (BaseUI) uiTable.get(String.valueOf(uiId));  
            if(requestUI != null) {
                Log.printInfo("CyoController: dispose() Scene id = " + uiId);
                requestUI.dispose();
            }
        }  
    }

    /**
     * Show a loading animation.
     */
    public void showLoadingAnimation() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: showLoadingAnimation()");
        }
        
        if (CyoDataManager.getInstance().mode == CyoDataManager.MODE_START) {
	        LoadingAnimationService lService = (LoadingAnimationService) DataCenter.getInstance().get(
	                LoadingAnimationService.IXC_NAME);
	
	        Log.printInfo("curIdForLoading = " + curIdForLoading);
	        if (lService != null) {
	            try {
	                if (curIdForLoading == BaseUI.UI_INITIAL_STATE || curIdForLoading == BaseUI.UI_PREVALIDATION
                            || curIdForLoading == BaseUI.UI_TV_PACKAGE_LIST) {
	                    lService.showNotDelayLoadingAnimation(new Point(480, 270));
	                } else if (curIdForLoading == BaseUI.UI_CHANNEL_LIST_NEW) {
	                    lService.showNotDelayLoadingAnimation(new Point(380, 280));
	                } else if (curIdForLoading == BaseUI.UI_CHANGE_CHANNEL_SELECTION
	                        || curIdForLoading == BaseUI.UI_GAME_PACKAGES || curIdForLoading == BaseUI.UI_MANAGE_GROUP
	                        || curIdForLoading == BaseUI.UI_POPULAR_PACKAGES || curIdForLoading == BaseUI.UI_SELF_SERVE
	                        || curIdForLoading == BaseUI.UI_CHANNEL_LIST_BASIC || curIdForLoading == BaseUI.UI_PACKAGE_CHANNEL_LIST) {
	                    lService.showNotDelayLoadingAnimation(new Point(380, 300));
	                } else {
	                    lService.showNotDelayLoadingAnimation(new Point(320, 280));
	                }
	            } catch (RemoteException e) {
	                e.printStackTrace();
	            }
	        } else {
	            if (Log.DEBUG_ON) {
	                Log.printDebug("CyoController: showLoadingAnimation: LoadingAnimationService is null");
	            }
	        }
        } else {
        	if (Log.DEBUG_ON) {
                Log.printDebug("CyoController: CYO visible is false");
            }
        }
    }

    /**
     * Hide a loading animation.
     */
    public void hideLoadingAnimation() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: hideLoadingAnimation()");
        }

        if (FrameworkMain.getInstance().getHScene().isShowing()) {
            LoadingAnimationService lService = (LoadingAnimationService) DataCenter.getInstance().get(
                    LoadingAnimationService.IXC_NAME);
            if (lService != null) {
                try {
                    lService.hideLoadingAnimation();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("CyoController: showLoadingAnimation: LoadingAnimationService is null");
                }
            }
        } else {
        	if (Log.DEBUG_ON) {
                Log.printDebug("CyoController: CYO visible is false");
            }
        }
    }

    /**
     * Show a UI.
     * @param uiId a id of wanted UI.
     * @param back true if back, false otherwise.
     */
    public void showUI(int uiId, boolean back) {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: showUIComponent()");
        }
        BaseUI requestUI;

        if (uiTable.containsKey(String.valueOf(uiId))) {
            requestUI = (BaseUI) uiTable.get(String.valueOf(uiId));
        } else {
            requestUI = createUI(uiId);
            if (requestUI == null) {
                if (Log.INFO_ON) {
                    Log.printInfo("CyoController: requestUIComponent is null.");
                }
                return;
            }
            uiTable.put(String.valueOf(uiId), requestUI);
        }

        if (currentUI != null) {
            FrameworkMain.getInstance().removeComponent(currentUI);
            currentUI.removeKeyListener(this);
            currentUI.stop();
        }
        curIdForLoading = uiId;
        
        currentUI = requestUI;
        currentUI.start(back);
        FrameworkMain.getInstance().addComponent(currentUI);
        currentUI.addKeyListener(this);
        currentUI.requestFocus();
       
        if (!back) {
            currentUI.setLastUIId(currentId);
        }
        currentId = uiId;
    }

    /**
     * Create a UI.
     * @param uiId id of UI.
     * @return UI to be created.
     */
    private BaseUI createUI(int uiId) {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: createUI()");
        }
        if (uiId == BaseUI.UI_INITIAL_STATE) {
            return new InitialStateUI();
        } else if (uiId == BaseUI.UI_CHANGE_CHANNEL_SELECTION) {
            return new ChangeChannelSelectionUI();
        } else if (uiId == BaseUI.UI_PREVALIDATION) {
            return new PrevalidationUI();
        } else if (uiId == BaseUI.UI_MANAGE_TV_SERVICE) {
            return new ManageTVServiceUI();
        } else if (uiId == BaseUI.UI_SELF_SERVE) {
            return new SelfServeUI();
        } else if (uiId == BaseUI.UI_MANAGE_GROUP) {
            return new ManageGroupUI();
        } else if (uiId == BaseUI.UI_POPULAR_PACKAGES) {
            return new PopularPackagesUI();
        } else if (uiId == BaseUI.UI_VIEW_CHANNELS) {
            return new ViewChannelUI();
        } else if (uiId == BaseUI.UI_GAME_PACKAGES) {
            return new GamePackagesUI();
        } else if (uiId == BaseUI.UI_VIEW_GAMES) {
            return new GameUI();
        } else if (uiId == BaseUI.UI_VALIDATION) {
            return new ValidationUI();
        } else if (uiId == BaseUI.UI_CONFIRMATION) {
            return new ConfirmationUI();
        } else if (uiId == BaseUI.UI_SEE_DETAILS) {
            return new SeeDetailsUI();
        } else if (uiId == BaseUI.UI_CHANNEL_LIST_BASIC) {
            return new BasicChannelListUI();
        } else if (uiId == BaseUI.UI_CHANNEL_LIST_NEW) {
            return new NewChannelUI();
        } else if (uiId == BaseUI.UI_COMPARE_MY_SERVICES) {
            return new CompareMyServicesUI();
        } else if (uiId == BaseUI.UI_INFORMATION) {
            return new InformationUI();
        } else if (uiId == BaseUI.UI_TV_PACKAGE_LIST) {
            return new TVPackageUI();
        } else if (uiId == BaseUI.UI_PACKAGE_CHANNEL_LIST) {
            return new PackageChannelListUI();
        } else if (uiId == BaseUI.UI_CHANNEL_BLOC_LIST) {
            return new ChannelBlocUI();
        }

        return null;
    }

    /**
     * Hide a UI.
     */
    private void hideUI() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: hideUI()");
        }
        if (currentUI != null) {
            FrameworkMain.getInstance().removeComponent(currentUI);
            currentUI.removeKeyListener(this);
            currentUI.stop();
        }

        Enumeration enums = uiTable.elements();
        Log.printInfo("uiTable size " + uiTable.size());

         while (enums.hasMoreElements()) {
            BaseUI ui = (BaseUI) enums.nextElement();
            if (ui != null) {
                ui.stop();
                ui.dispose();
                ui = null;
            }
        }

        currentUI = null;
        uiTable.clear();
    }

    /**
     * Get a id of current UI.
     * @return id of current UI.
     */
    public int getCurrentId() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: getCurrentId(): currentId = " + currentId);
        }
        return currentId;
    }

    /**
     * Set a needToNotify.
     * @param need true if need to notify a package is updated, false otherwise.
     */
    public void setNeedToNotify(boolean need) {
        needToNotify = need;
    }

    /**
     * Show a error message acoording to code.<br>
     * <p>
     * CYO500 Cannot communicate with the CYO Service HTTP Server Error Error Message Displayed<br>
     * CYO501 Cannot get the server information from the Management System Error Error Message Displayed<br>
     * CYO502 Cannot parse the server information from the Management System Error Error Message Displayed<br>
     * CYO503 Cannot parse the XML response of the CYO Server Error Error Message Displayed
     * @param code a error code.
     */
    public void showErrorMessage(String code) {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: showErrorMessage(): code = " + code);
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("CyoContoller: showErrorMessage: isShowing = "
                    + FrameworkMain.getInstance().getHScene().isShowing());
        }

        if (FrameworkMain.getInstance().getHScene().isShowing()) {
            CyoController.getInstance().hideLoadingAnimation();
            ErrorMessageService eService = (ErrorMessageService) DataCenter.getInstance().get(
                    ErrorMessageService.IXC_NAME);

            if (eService != null) {
                try {
                    eService.showErrorMessage(code, this);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     * request a exit to MonitorService.
     */
    public void exit() {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: exit()");
        }
        MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

        if (mService != null) {
            try {
                mService.exitToChannel();
            } catch (RemoteException e1) {
                Log.print(e1);
            }
        } else {
            hideUI();
        }
    }

    public void startUnboundApplication(int keyCode) {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: exit()" + keyCode);
        }
        MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

        if (mService != null) {
            try {
                mService.startUnboundAppWithHotKey(keyCode);
                if (keyCode == OCRcEvent.VK_EXIT) {
                    CyoController.getInstance().exit();
                }
            } catch (RemoteException e1) {
                Log.print(e1);
            }
        }
    }

    public void startUnboundApplication(String appName) {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: startUnboundApplication, appName=" + appName);
        }
        MonitorService mService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

        if (mService != null) {
            try {
                mService.startUnboundApplication(appName, new String[] {"CYO"});
            } catch (RemoteException e1) {
                Log.print(e1);
            }
        }
    }

    /**
     * Handle if data is removed in DataCenter.
     * @param key a key of data in DataCenter.
     */
    public void dataRemoved(String key) {

    }

    /**
     * Handle if data is updated in DataCenter.
     * @param key a key of updated data in DataCenter.
     * @param old last data.
     * @param value new data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("CyoController: dataUpdated(): key = " + key + "\t old = " + old + "\t value = " + value);
        }

        if (key.equals(EpgService.IXC_NAME)) {
            epgService = (EpgService) value;
            if (Log.INFO_ON) {
                Log.printInfo("MonitorService Lookup completed");
            }
        } else if (key.equals(MonitorService.IXC_NAME)) {
            monitorService = (MonitorService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                monitorService.addInbandDataListener(inbandListner, appName);

                CyoDataManager.getInstance().init();
                if (Log.INFO_ON) {
                    Log.printInfo("CyoController: MonitorService Lookup completed");
                }
            } catch (RemoteException e) {
                Log.print(e);
            }

        } else if (key.equals(PreferenceService.IXC_NAME)) {
            PreferenceService pService = (PreferenceService) value;
            try {
                String[] values = pService.addPreferenceListener(this,
                        FrameworkMain.getInstance().getApplicationName(), new String[] {PreferenceNames.LANGUAGE,
                                Log.LOG_CAPTURE_APP_NAME}, // Logging tools, Bundling
                        new String[] {Definitions.LANGUAGE_FRENCH, ""});

                if (Log.DEBUG_ON) {
                    for (int i = 0; values != null && i < values.length; i++) {
                        Log.printDebug("CyoController: dataUpdated(): values = " + values[i]);
                    }
                }

                DataCenter.getInstance().put(PreferenceNames.LANGUAGE, values[0]);
            } catch (RemoteException e) {
                Log.print(e);
            }
        } else if (key.equals(StcService.IXC_NAME)) {
            StcService stcService = (StcService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                int currentLevel = stcService.registerApp(appName);
                Log.printDebug("CyoController: stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            Object o = SharedMemory.getInstance().get("stc-" + appName);
            if (o != null) {
                DataCenter.getInstance().put("LogCache", o);
            }
            try {
                stcService.addLogLevelChangeListener(appName, logListener);
            } catch (RemoteException ex) {
                Log.print(ex);
            }

        } else if (key.equals(VbmService.IXC_NAME)) {
            VbmController.getInstance().init((VbmService) value);            
        }
    }

    public void keyPressed(KeyEvent event) {
        keyPressed(event.getKeyCode());
    }

    /**
     * Handle a event of key pressed.
     * @param keyCode keyCode of key event.
     */
    public void keyPressed(int keyCode) {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: keyPressed()");
        }

        if (FrameworkMain.getInstance().getHScene().isShowing()) {
            Log.printInfo("CyoController: keyPressed() currentUI.notiPop " + currentUI.getPopup());

            if ((keyCode == OCRcEvent.VK_EXIT || keyCode == HRcEvent.VK_GUIDE || keyCode == OCRcEvent.VK_MENU
                    || keyCode == OCRcEvent.VK_ON_DEMAND || keyCode == OCRcEvent.VK_LIST
                    || keyCode == KeyCodes.SETTINGS || keyCode == OCRcEvent.VK_LIVE || keyCode == KeyCodes.PIP
                    || keyCode == KeyCodes.WIDGET)
                    && currentUI.getPopup() == null) {
                Log.printInfo("CyoController: keyPressed() exit ");

                // if (CyoDataManager.changedData) {
                Log.printInfo("showExitPopup = " + keyCode);
                hideLoadingAnimation();
                currentUI.showExitPopup(keyCode);
                // } else {
                // currentUI.gotoNextApplication(keyCode);
                // }
            } else if (currentUI != null) {
                Log.printInfo("CyoController: keyPressed() currentUI " + currentUI);
                currentUI.handleKey(keyCode);
            }
        } else if (Log.DEBUG_ON) {
            Log.printDebug("CyoController: keyPressed: CYO is not showing.");
        }
    }

    /**
     * Do nothing just implements.
     */
    public void keyReleased(KeyEvent event) {
    }

    /**
     * Do nothing just implements.
     */
    public void keyTyped(KeyEvent event) {
    }

    /**
     * Receive a updated preference from Profile application.
     * @param preferenceName a name of updated preference.
     * @param value a updated value.
     * @throws RemoteException remote exception.
     */
    public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("CyoController: receiveUpdatedPreference()");
        }
        DataCenter.getInstance().put(preferenceName, value);
    }

    /**
     * Implements ScreenSaverConfirmationListener.
     * @author Woojung Kim
     * @version 1.1
     */
    class ScreenSaverConfirmationListenerImpl implements ScreenSaverConfirmationListener {

        /**
         * Confirm to launch Screen Saver.
         * @return Return true if Galaxie is not showing, false otherwise.
         * @throws RemoteException The Remote Exception
         */
        public boolean confirmScreenSaver() throws RemoteException {
            if (Log.DEBUG_ON) {
                Log.printDebug("ScreenSaverConfirmationListenerImpl: confirmScreenSaver: isShowing = "
                        + FrameworkMain.getInstance().getHScene().isShowing());
            }
            return true;
        }
    }

    /**
     * Receives a log level from STC application and apply.
     * @author Woojung Kim
     * @version 1.1
     */
    class LogLevelAdapter implements LogLevelChangeListener {

        /**
         * Receives a log level from STC application and apply.
         * @param logLevel logLevel to be updated.
         * @throws RemoteException throws exception
         */
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    /**
     * Implements a InbandDataListener.
     * @author Woojung Kim
     * @version 1.1
     */
    class InbandDataListenerImpl implements InbandDataListener, DataUpdateListener {

        /**
         * Update a data via in-band when Monitor tuned a In-band channel for Data.
         * @param locator The locator for in-band data channel.
         */
        public void receiveInbandData(String locator) throws RemoteException {
            if (Log.DEBUG_ON) {
                Log.printDebug("InbandDataListenerImpl: receiveInbandData: locator = " + locator);
            }

            int newVersion = monitorService.getInbandDataVersion(MonitorService.cyo);

            if (Log.DEBUG_ON) {
                Log.printDebug("InbandDataListenerImpl: receiveInbandData: currentVersion = " + currentVersion);
                Log.printDebug("InbandDataListenerImpl: receiveInbandData: newVersion = " + newVersion);
            }

            if (currentVersion != newVersion) {
                currentVersion = newVersion;
                DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
            }

            try {
                monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
            } catch (RemoteException e) {
                Log.printError(e);
            }
        }

        public void dataRemoved(String key) {
        }

        /**
         * Process a updated data.
         * @param key The key of update data.
         * @param old The old value
         * @param value The new value
         */
        public void dataUpdated(String key, Object old, Object value) {
            if (Log.DEBUG_ON) {
                Log.printDebug("InbandDataListenerImpl: dataUpdated: key = " + key);
                Log.printDebug("InbandDataListenerImpl: dataUpdated: value = " + value);
            }
            if (key.equals(IB_DATA) && value != null) {
                CyoDataManager.getInstance().updateInformationData((File) value);
            } else if (key.equals(IB_DATA) && value == null) {
                CyoController.getInstance().showErrorMessage("CYO501");
            }
        }
    }

    public void actionPerformed(int buttonType) throws RemoteException {
        Log.printInfo("actionPerformed " + buttonType);
        stop();
        exit();
    }

    class PinEnabler implements PinEnablerListener {
        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            Log.printDebug("result = " + response + " detail = " + detail);
            if (PreferenceService.RESPONSE_SUCCESS == response) {
                CyoController.getInstance().showUI(BaseUI.UI_INITIAL_STATE, false);
            } else {
                // hidePinEnabler();
                actionPerformed(-1);
            }
        }
    }

    public boolean showPinEnabler(String[] msg) {
        boolean result = false;
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService == null) {
            return result;
        } else {
            result = true;;
        }

        if (msg == null) {
            msg = new String[] {"Enter Your PIN"};
        }
        try {
            preferenceService.showPinEnabler(new PinEnabler(), msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void hidePinEnabler() {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.hidePinEnabler();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Gets the start parameter.
     *
     * @return the start parameter
     */
    private String[] getStartParameter() {
        if (monitorService != null) {
            try {
                String[] startParm = monitorService.getParameter(App.NAME);
                Log.printInfo("getStartParameter()-startParm : " + startParm);
                return startParm;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public int getRedirectionCode() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CyoController, getRedirectionCode, redirectionCode=" + redirectionCode);
        }
        return redirectionCode;
    }

    public void resetRedirectionCode() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CyoController, resetRedirectionCode");
        }
        redirectionCode = REDIRECTION_CODE_NONE;
    }

    public String getTargetApp() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CyoController, getTargetApp, targetApp=" + targetApp);
        }
        return targetApp;
    }

    public String getTargetCallLetter() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CyoController, getTargetApp, targetCallLetter=" + targetCallLetter);
        }
        return targetCallLetter;
    }
}

/**
 * @(#)UserEvenetManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo.controller;

import java.awt.event.KeyEvent;

import org.davic.resources.ResourceClient;
import org.davic.resources.ResourceProxy;
import org.davic.resources.ResourceStatusEvent;
import org.davic.resources.ResourceStatusListener;
import org.dvb.event.EventManager;
import org.dvb.event.UserEvent;
import org.dvb.event.UserEventAvailableEvent;
import org.dvb.event.UserEventListener;
import org.dvb.event.UserEventRepository;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class handles the hot key that is input by user.
 */
public class UserEventManager implements ResourceClient, ResourceStatusListener {

    /** a repository for register hot key event. */
    private UserEventRepository repositoryHotkey = new UserEventRepository("SettingsHotkey");
    /** a repository for register numeric key event. */
    private UserEventRepository repositoryNumber = new UserEventRepository("SettingsNumber");

    /** UserEventApdater for numeric keys. */
    private UserEventAdapter numKeyAdapter = new UserEventAdapter();
    /** a UserEventAdapter for hot keys. */
    private UserEventAdapter hotKeyAdapter = new UserEventAdapter();

    /** a object of UserEventManager for singleton. */
    private static UserEventManager instance;
    
    private boolean staredUserEvent = false;

    /**
     * Constructor. call a init().
     */
    private UserEventManager() {
        init();
    }

    /**
     * Get a singleton object of UserEventManager.
     * @return singleton object.
     */
    public static UserEventManager getInstance() {
        if (instance == null) {
            instance = new UserEventManager();
        }
        return instance;
    }

    /**
     * Initialize a manager.
     */
    private void init() {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager: init()");
        }
        repositoryNumber.addAllNumericKeys();
        repositoryHotkey.addKey(KeyCodes.SETTINGS);
        repositoryHotkey.addKey(KeyCodes.WIDGET);
        repositoryHotkey.addKey(OCRcEvent.VK_EXIT);
        repositoryHotkey.addKey(OCRcEvent.VK_GUIDE);
        repositoryHotkey.addKey(OCRcEvent.VK_MENU);
        repositoryHotkey.addKey(OCRcEvent.VK_ON_DEMAND);
        repositoryHotkey.addKey(OCRcEvent.VK_LIST);
        repositoryHotkey.addKey(OCRcEvent.VK_LIVE);
        
        repositoryHotkey.addKey(KeyCodes.PIP);
        repositoryHotkey.addKey(OCRcEvent.VK_CHANNEL_UP);
        repositoryHotkey.addKey(OCRcEvent.VK_CHANNEL_DOWN);
    }

    /**
     * Start a mananger.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager: start()");
        }
        
        if(staredUserEvent){
            return;
        }
        staredUserEvent = true;
        EventManager.getInstance().addResourceStatusEventListener(this);
        reserveHotKey();
        reserveNumericKeys();
    }

    /**
     * Reserve a numeric keys.
     */
    public void reserveNumericKeys() {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager: reserveNumericKeys()");
        }
        EventManager.getInstance().addUserEventListener(numKeyAdapter, this, repositoryNumber);
    }

    /**
     * Reserve a hot key.
     */
    public void reserveHotKey() {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager: reserveHotKey()");
        }
        EventManager.getInstance().addUserEventListener(hotKeyAdapter, this, repositoryHotkey);
    }

    /**
     * Release a numberic keys.
     */
    public void releaseNumericKeys() {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager: releaseNumericKeys()");
        }
        EventManager.getInstance().removeUserEventListener(numKeyAdapter);
    }

    /**
     * Release a hot key.
     */
    public void releaseHotKey() {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager: releaseHotKey()");
        }
        EventManager.getInstance().removeUserEventListener(hotKeyAdapter);
    }

    /**
     * Dispose a resource.
     */
    public void dispose() {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager: dispose()");
        }
        staredUserEvent = false;
        EventManager.getInstance().removeResourceStatusEventListener(this);
        releaseNumericKeys();
        releaseHotKey();
    }

    /**
     * A call to this operation notifies the ResourceClient that proxy has lost access to a resource.
     * @param rProxy the ResourceProxy representing the scarce resource to the application
     */
    public void notifyRelease(ResourceProxy rProxy) {

    }

    /**
     * A call to this operation informs the ResourceClient that proxy is about to lose access to a resource.
     * @param rProxy the ResourceProxy representing the scarce resource to the application
     */
    public void release(ResourceProxy rProxy) {

    }

    /**
     * A call to this operation informs the ResourceClient that another application has requested the resource accessed
     * via the proxy parameter.
     * @param rProxy the ResourceProxy representing the scarce resource to the application
     * @param requestData application specific data
     * @return If the ResourceClient decides to give up the resource following this call, it should terminate its usage
     *         of proxy and return True, otherwise False.
     */
    public boolean requestRelease(ResourceProxy rProxy, Object requestData) {
        return false;
    }

    /**
     * implements ResourceStatusListener.
     * @param event ResourceStateusEvent.
     */
    public void statusChanged(ResourceStatusEvent event) {
        if (Log.INFO_ON) {
            Log.printInfo("UserEventManager statusChanged()");
        }
        if (event instanceof UserEventAvailableEvent) {
            Object o = ((UserEventAvailableEvent) event).getSource();
            UserEventRepository rep = (UserEventRepository) o;
            UserEvent[] events = rep.getUserEvent();
            boolean channelKey = false;
            boolean numKey = false;
            for (int i = 0; i < events.length; i++) {
                switch (events[i].getCode()) {
                case OCRcEvent.VK_CHANNEL_UP:
                case OCRcEvent.VK_CHANNEL_DOWN:
                    channelKey = true;
                    break;
                case OCRcEvent.VK_0:
                    numKey = true;
                    break;
                default:
                    break;
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("UserEventManager statusChanged " + channelKey + " , " + numKey);
            }
            if (channelKey) {
                reserveHotKey();
            }
            if (numKey) {
                reserveNumericKeys();
            }
        }
    }

    /**
     * Implements the UserEventListener interface. It receive the user event and process the hot key.
     */
    public class UserEventAdapter implements UserEventListener {

        /**
         * Receives a UserEvent.
         * @param e UserEvent.
         */
        public void userEventReceived(UserEvent e) {
            if (Log.INFO_ON) {
                Log.printInfo("UserEventManager: userEventReceived()");
            }
            if (e.getType() != KeyEvent.KEY_PRESSED) {
                return;
            }
            int keyCode = e.getCode();
            switch (keyCode) {
            case OCRcEvent.VK_EXIT :
            case HRcEvent.VK_GUIDE :
            case OCRcEvent.VK_MENU :
            case OCRcEvent.VK_ON_DEMAND :
            case OCRcEvent.VK_LIST :
            case KeyCodes.SETTINGS :
            case OCRcEvent.VK_LIVE :
            case KeyCodes.PIP:
            case KeyCodes.WIDGET:
                CyoController.getInstance().keyPressed(e.getCode());
                break;
            case HRcEvent.VK_0:
            case HRcEvent.VK_1:
            case HRcEvent.VK_2:
            case HRcEvent.VK_3:
            case HRcEvent.VK_4:
            case HRcEvent.VK_5:
            case HRcEvent.VK_6:
            case HRcEvent.VK_7:
            case HRcEvent.VK_8:
            case HRcEvent.VK_9:
//            case OCRcEvent.VK_EXIT:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                break;
            default:
                break;
            }
        }
    }
}
package com.videotron.tvi.illico.cyo.controller;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.cyo.App;
import com.videotron.tvi.illico.ixc.vbm.*;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

import java.rmi.*;
import java.util.*;

public class VbmController implements LogCommandTriggerListener {

    /** The Constant MID_APP_START. */
    private static final long MID_APP_START = 1009000001L;
    /** The Constant MID_APP_EXIT. */
    private static final long MID_APP_EXIT = 1009000002L;
    /** The Constant MID_PARENT_APP. */
    private static final long MID_PARENT_APP = 1009000003L;

    protected static VbmController instance = new VbmController();

    /** The session id. */
    private String sessionId = null;

    public static VbmController getInstance() {
        return instance;
    }

    public static boolean ENABLED = false;

    protected VbmService vbmService;
    protected HashSet idSet = new HashSet();
    protected Vector vbmLogBuffer;

    protected VbmController() {
    }

    public void init(VbmService service) {
        Log.printInfo("VbmController.init");
        vbmService = service;
        long[] ids = null;
        try {
            ids = vbmService.checkLogCommand(App.NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        try {
            vbmService.addLogCommandChangeListener(App.NAME, this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }

        if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmController: ids[" + i + "] = " + ids[i]);
                }
                idSet.add(new Long(ids[i]));
            }
        }

        vbmLogBuffer = (Vector) SharedMemory.getInstance().get(VbmService.VBM_SM_INTF);
        checkEmpty();
        if (vbmLogBuffer != null) {
            Log.printDebug("VbmController: found vbmLogBuffer");
        } else {
            Log.printDebug("VbmController: not found vbmLogBuffer.");
        }
    }

    public void dispose() {
        try {
            vbmService.removeLogCommandChangeListener(App.NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

    protected void checkEmpty() {
        ENABLED = vbmLogBuffer != null && !idSet.isEmpty();
    }

    public boolean write(long id, String[] value) {
        if (!ENABLED) {
            return false;
        }
        if (!idSet.contains(new Long(id))) {
            return false;
        }
        String str = Long.toString(id) + VbmService.SEPARATOR + "-1";
        if (value != null) {
            StringBuffer buffer = new StringBuffer(100);
            buffer.append(str);
            for (int i = 0; i < value.length; i++) {
                buffer.append(VbmService.SEPARATOR);
                buffer.append(value[i]);
            }
            str = buffer.toString();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("VbmController.write: " + str);
        }
        vbmLogBuffer.addElement(str);
        return true;
    }

    /**
     * event invoked when logLevel data updated. it is only applied to action measurements
     * @param Measurement to notify measurement an frequencies
     * @param command - true to start logging, false to stop logging
     * @throws RemoteException - occur during the execution of a remote method call
     */
    public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
        Log.printInfo("VbmController.logCommandChanged: " + measurementId + ", " + command);
        if (command) {
            idSet.add(new Long(measurementId));
        } else {
            idSet.remove(new Long(measurementId));
        }
        checkEmpty();
    }

    /**
     * event invoked when log of trigger measurement should be collected
     * @param measurementId trigger measurement
     * @throws RemoteException - occur during the execution of a remote method call
     */
    public void triggerLogGathering(long measurementId) {
        Log.printInfo("VbmController.triggerLogGathering: " + measurementId);
    }

    /**
     * Write.
     * @param id the id
     * @param str the str
     * @return true, if successful
     */
    private boolean write(long id, String str) {
        return write(id, new String[] {str});
    }

    /**
     * Write start log.
     * @param startParam the start param
     */
    public void writeStartLog(String[] startParam) {
    	String parentAppName = TextUtil.EMPTY_STRING;
        if (startParam != null && startParam.length > 0) {
            parentAppName = startParam[0];
        }
        write(MID_PARENT_APP, parentAppName);
        
        sessionId = Long.toString(System.currentTimeMillis());
        write(MID_APP_START, sessionId);
    }

    /**
     * Write end log.
     */
    public void writeEndLog() {
    	if (sessionId != null) {
	        write(MID_APP_EXIT, sessionId);
	        sessionId = null;
    	}
    }
}

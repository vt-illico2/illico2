/**
 * @(#)KeyNames.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.cyo;

/**
 * This class has a key to get a name of title or preference etc according to Language.
 * @author Woojung Kim
 * @version 1.1
 */
public class KeyNames {

    /** The key name of Change your option. */
    public static final String CHANGE_YOUR_OPTION = "cyo";

    /** The key name of Sub title of initial State Screen. */
    public static final String CUSTOMIZE_YOUR_TV_PROGRAMMING = "customize.your.tv.programming";
    public static final String CHANGE_YOUR_CHANNEL_SELECTION = "change.your.channel.selection";
    public static final String MANAGE_YOUR_TV_SERVICE = "manage.your.tv.service";
    public static final String FOR_CUSTOM_PACKAGE_ONLY = "for.custom.package.only";
    public static final String MY_PICKS = "my.picks";

    public static final String CHANGE_CUSTOM_MYPICKS = "change.custom.mypicks";
    public static final String CHANGE_CUSTOM_CHANNELS = "change.custom.channels";
    public static final String CHANGE_NORMAL_GROUPS = "change.normal.groups";
//    public static final String CHANGE_SPECIALTY_CHANNELS = "change.specialty.channels";
//    public static final String CHANGE_MOVIE_SERIES_CHANNELS = "change.movie.series.channels";
//    public static final String CHANGE_HD_CHANNELS = "change.hd.channel";
    public static final String CHANGE_MY_PICKS = "change.my.picks";
    public static final String DELETE = "delete";
    public static final String TAX = "tax";
    
    public static final String FUND = "contribution.fund";

    public static final String EXPLAIN = "explain";
    public static final String EXPLAIN_MYPICKS = "explain.mypick";

    public static final String GAME_EXPLAIN = "game.explain";
    public static final String GAME_PLUS = "game.plus";
    
    public static final String SELECTED_CHANNELS = "selected.channels";

    // Prevalidation
    public static final String PREVALIDATION_TITLE = "prevalidation.title";
    public static final String PREVALIDATION_LEFT = "prevalidation.left";
    public static final String PREVALIDATION_RIGHT = "prevalidation.right";

    // Manage TV Service
    public static final String MANAGE_TV_SERVICE = "manage.tv.service";
    public static final String MANAGE_TV_SERVICE_EXPLAIN = "manage.tv.service.explain";
    public static final String SEE_DETAILS = "see.details";
    public static final String LIST_OF_CHANNELS = "list.of.channels";
    public static final String COMPARE_YOUR_SERVICES = "compare.your.services";
    public static final String YOUR_TV_SERVICE = "your.tv.service";
    public static final String ADD_NEW_PACKAGE_CHANNELS = "add.new.package.channels";
    public static final String TOTAL = "total";

    // Popular Packages
    public static final String POPULAR_PACKAGES = "popular.packages";
    public static final String CUSTOM_PACKAGES = "custom.packages";
    public static final String MY_PICKS_PACKAGES = "my.picks.packages";
    public static final String SPECIALTY_PACKAGES = "specialty.packages";
    public static final String SPECIALTY_PACKAGES_EXPLAIN = "specialty.packages.explain";
    public static final String MOVIE_SERIES_PACKAGES = "movie.series.packages";
    public static final String MOVIE_SERIES_PACKAGES_EXPLAIN = "movie.series.packages.explain";
    public static final String HD_CHANNEL_PACAKGES = "hd.channel.packages";
    public static final String HD_CHANNEL_PACAKGES_EXPLAIN = "hd.channel.packages.explain";
    public static final String THEME_PACKAGES = "theme.packages";
    public static final String GAME_PACKAGES = "game.packages";
    public static final String NO_PACKAGE = "popular.nopackage";    
    public static final String SINGLE = "single";
    
    public static final String GAME_A_LA_CARTE = "game.a.la.carte";
    
    public static final String LIST_OF_GAMES = "list.of.games";
    public static final String SELECT_GAMES = "select.games";
    public static final String SELECTED_GAMES = "selected.games";

    // Validation
    public static final String VALIDATION_TITLE = "validation.title";
    public static final String VALIDATION_EXPLAIN = "validation.explain";
    public static final String VIEW_CONDITIONS = "view.conditions";

    // Confirmation
    public static final String CONFIRMATION_MAIN = "confirmation.main";
    public static final String CONFIRMATION_EXPLAIN = "confirmation.explain";
    public static final String EXIT = "exit";
    
    
    // See Details
    public static final String SEE_DETAILS_TITLE = "see.details.title";
    public static final String SEE_DETAILS_EXPLAIN = "see.details.explain";
    public static final String LIST_OF_CHANNELS_BASIC_BTN = "list.of.channels.basic.btn";

    // Basic channel list
    public static final String BASIC_CHANNEL_LIST_TITLE = "basic.channel.list.title";
    public static final String BASIC_CHANNEL_LIST_EXPLAIN = "basic.channel.list.explain";
    
    // New channel list
    public static final String NEW_CHANNEL_LIST_TITLE = "new.channel.list.title";
    public static final String NEW_CHANNEL_LIST_EXPLAIN = "new.channel.list.explain";
    public static final String ADDED_CHANNEL = "added.channels";
    public static final String REMOVED_CHANNEL = "removed.channels";
    public static final String MY_CHANNEL_LIST = "list.mychannelList";
    
    // Compare my services
    public static final String COMPARE_MY_SERVICES_TITLE = "compare.my.services.title";
    public static final String COMPARE_MY_SERVICES_EXPLAIN = "compare.myservices.explain";
    public static final String YOUR_CURRENT_SERVICE = "your.current.service";
    public static final String YOUR_MODIFIED_SERVICE = "your.modified.service";
    public static final String COMPARE_GROUP_TITLE = "compare.group.title";
    
    
    //
    public static final String HELP = "help";
    public static final String RULES = "rules";
    public static final String USEFUL_INFORMATION = "useful.information";
    public static final String INFORMATION_CONTACT = "information.contact";
    public static final String INFOMATION_SUB_TITLE = "information.sub.title";
    public static final String BTN_RULES = "btn.rules";
    public static final String BTN_VIEW_HELP = "btn.view.help";
    
    // Common
    public static final String SORTED_BY_CHANNEL_NUMBER = "sorted.by.channel.number";
    public static final String SORTED_BY_ALPHABETICAL = "sorted.by.alphabetical";
    public static final String MONTH = "month";
    public static final String YEAR = "year";
    public static final String ONCE = "once";
    public static final String MODIFY = "modify";
    public static final String VIEW = "btn.view";
    public static final String ADD = "add";
    public static final String AFTER = "after";
    public static final String BEFORE = "before";
    public static final String CHANNELS = "channels";

    // PopCallerIDUI
    public static final String NOTIFICATION = "notification";
    public static final String ICON_CANADIAN = "icon.canadian";
    public static final String CANCEL_YOUR_SELECTION_TITLE = "cancel.your.selection.title";
    public static final String CANCEL_VALIDATE_TITLE = "cancel.validate.title";    
    public static final String CANCEL_VALIDATE_EXPLAIN = "cancel.validate.explain"; 
    
    public static final String CANCEL_YOUR_SELECTION = "cancel.your.selection";
    public static final String OTHER_SUGGESTION_TITLE = "other.suggestion.title";
    public static final String ADD_REMOVE_CHANNELS = "add.remove.channel";
    public static final String KEEP_MY_SELECTION = "keep.my.selection";
    public static final String HD_NETWORK_FEES = "hd.network.fees";
    public static final String UNSELECT_CHANNEL_TITLE = "unselect.channel.title";
    public static final String UNSELECT_CHANNEL = "unselect.channel";
    public static final String UNSELECT_GAME_TITLE = "unselect.game.title";
    public static final String UNSELECT_GAME = "unselect.game";
    public static final String EXIT_CYO_TITLE = "exit.cyo.title";
    public static final String EXIT_CYO = "exit.cyo";

    public static final String BTN_YES = "btn.yes";
    public static final String BTN_NO = "btn.no";
    public static final String BTN_CONFIRM = "btn.confirm";
    public static final String BTN_HELP = "btn.help";
    public static final String BTN_MODIFY = "btn.modify";
    public static final String BTN_MANAGE = "btn.manage";
    public static final String BTN_CONFIRM2 = "btn.confrim2";
    public static final String BTN_CONFIRM3 = "btn.confrim3";
    
    public static final String BTN_SCROLL = "btn.scroll";
    public static final String BTN_SORTING = "btn.sorting";
    public static final String BTN_DESELECT_ALL = "btn.deselect.all";
    public static final String BTN_LIST_MODIFIED_CHANNELS = "btn.list.modified.channels";
    
    /** The key name of Back button. */
    public static final String BTN_BACK = "btn.back";
    /** The key name of Cancel button. */
    public static final String BTN_CANCEL = "btn.cancel";
    public static final String BTN_OK = "btn.ok";
    
public static final String BTN_EXIT = "btn.exit";
    
    // game temp
    public static final String NOT_GOTO_GAME = "game.notList";
    
    public static final String UNSELECT_MESSAGE_1 = "unselect.message.1";
    public static final String UNSELECT_MESSAGE_2 = "unselect.message.2";
    public static final String UNSELECT_MESSAGE_3 = "unselect.message.3";
    public static final String UNSELECT_MESSAGE_4 = "unselect.message.4";
    
    public static final String PIN_MESSAGE_1 = "msgUnblockPIN1";
    public static final String PIN_MESSAGE_2 = "msgUnblockPIN2";
    
    public static final String LIST_MONTH = "list.month";
    public static final String LIST_YEAR = "list.year";
    public static final String LIST_ONCE = "list.once";
    
    public static final String LIST_GAME_MONTH = "list.game.month";
    public static final String LIST_GAME_YEAR = "list.game.year";
    public static final String LIST_GAME_ONCE = "list.game.once";
   
    public static final String CANCEL_CHANGE = "cancel.change";
    public static final String CANCEL_CHANGE_TITLE = "cancel.change.title";
    
    // R5-SM5
    public static final String CUSTOM_5PACKGE = "custom.5package";

    // Bundling
    public static final String A_LA_CARTE_CHANNELS = "a.la.carte.channels";
    public static final String CHANGE_YOUR_BILL = "change.your.bill";
    public static final String BTN_MAKE_OTHER_CHANGES = "btn.make.other.changes";

    public static final String CHANGE_CUSTOM_CHANNELS_ADD_SINGLE = "change.custom.channels.add.single";
    public static final String CHANGE_CUSTOM_CHANNELS_ADD_MULTI = "change.custom.channels.add.multi";
    public static final String CHANGE_CUSTOM_CHANNELS_REMOVE_SINGLE = "change.custom.channels.remove.single";
    public static final String CHANGE_CUSTOM_CHANNELS_REMOVE_MULTI = "change.custom.channels.remove.multi";
    public static final String TV_PACKAGE_LIST = "tv.package.list";
    public static final String TV_PACKAGE_LIST_PRODUCT = "tv.package.list.product";
    public static final String HD_BASIC = "hd.basic";
    public static final String PREMIUM = "premium";
    public static final String CHOOSE_MY_CHANNELS = "choose.my.channels";
    public static final String MODIFY_MY_CHANNELS = "modify.my.channels";
    public static final String BTN_NEXT_STEP = "btn.next.step";
    public static final String BTN_PREVIOUS_STEP = "btn.previous.step";
    public static final String PACKAGE_CHANNEL_LIST_TITLE = "package.channel.list.title";
    public static final String PACKAGE_CHANNEL_LIST_ADD_SINGLE = "package.channel.list.add.single";
    public static final String PACKAGE_CHANNEL_LIST_ADD_MULTI = "package.channel.list.add.multi";
    public static final String BTN_VIEW_CHANNEL_INCLUDED = "btn.view.channel.included";

    public static final String UNSELECT_MESSAGE_FOR_A_LA_CARTE = "unselect.message.for.a.la.carte";
    public static final String UNSELECT_SUBFIX_FOR_A_LA_CARTE = "unselect.subfix.for.a.la.carte";
    public static final String BTN_CLOSE = "btn.close";
    public static final String BTN_SCROLL_CHANNELS = "btn.scroll.channels";
    public static final String VALIDATION_UPSELL_TITLE = "validation.upsell.title";
    public static final String VALIDATION_UPSELL_QUESTION = "validation_upsell.question";
    public static final String BTN_MODIFY_MY_PACKAGE = "btn.modify.my.package";
    public static final String BTN_ADD_BLOCK_CHANNELS = "btn.add.block.channels";
    public static final String BTN_CONTINUE_TRANSACTION = "btn.continue.transaction";
    public static final String TV_PACKAGE_DOWNGRADE_QUESTION = "tv.package.downgrade.question";
    public static final String BTN_NO_CANCEL = "btn.no.cancel";
    public static final String MODIFIED_CHANNEL_LIST = "modified.channel.list";
    public static final String VIEW_MODIFIED_CHANNELS = "view.modified.channels";
    public static final String NO_BLOC = "no.bloc";
    public static final String CHANNEL_BLOC_LIST = "channel.bloc.list";
    public static final String REDIRECTION_EXPLAIN = "redirection.explain";
    public static final String MONTH2 = "month2";
    public static final String SEE_DETAIL2 = "see.detail2";
    public static final String LIST_OF_CHANNELS2 = "list.of.channels2";

    // VDTRMASTER-5967
    public static final String MAIN_MENU = "main.menu";
    public static final String MAIN_EXPLAIN = "main.explain";

    // R7.3
    public static final String KEY_CONFIRMATION = "key.confirmation";
}

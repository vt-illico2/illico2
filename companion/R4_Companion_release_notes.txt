﻿This is Companion App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version 4K.2.1
    - Release App file Name
      : Companion.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2015.6.29

***
version 4K.2.1 - 2015.6.29
    - New Feature   
        + DDC-107
    - Resolved Issues
        + VDTRMASTER-5478	[CQ][UHD][R6][Companion] Wrong response result when playing a UHD content

***
version 4K.1.0 - 2015.4.10
    - New Feature   
        + [4K] Initial draft for periscope features.
    - Resolved Issues
        N/A
***
version R4_1.2.1 - 2014.11.21
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5286  [Companion] State logging is not stopped on the companion app when backend server returns specific result code -122 (E22 stop logging) 

***
version R4_1.1.0 - 2014.10.31
    - New Feature
        + [R4.1] pushKeyEvent added
    - Resolved Issues
        N/A

***
version 1.4.0.1 - 2014.04.01
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5072  [R4][PO3][Companion] Set-top box unstable (freezes, MOT500 errors, slowness) after sending multiple InitiateLogging (with Companion API) 

***
version 1.3.0.0 - 2013.12.23
    - New Feature
        + Merged PO1 and 3 to R3 HEAD branch.

***
version 1.2.5.1_PO3 - 2013.12.05
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4961  [CQ][PO3][Compagnon] Authentication screen shall be disappeard when showing a VOD content without control limit.  

***
version 1.2.4.2_PO3 - 2013.11.25
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4936  [PO3 Companion][CQ][R4] - Wrong error code value in case of initiateLogging
        + VDTRMASTER-4955  [PO3 Companion][CQ][R4] - Show/PlayVODContent from Search doesn't work as expected  

***
version 1.2.3.1_PO3 - 2013.11.08
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4912  [PO3 Companion] State logging: Pressing "Pause" button on live tv generates 2 logs

***
version 1.2.2.3_PO3 - 2013.10.31
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4912  [PO3 Companion] State logging: Pressing "Pause" button on live tv generates 2 logs  
        + VDTRMASTER-4905  [PO3 Companion] State logging: States are returned even if they are not in the original request.  
        + VDTRMASTER-4903  [PO3 Companion] State logging dwell time is not taken into account  

***
version 1.2.1.1_PO3 - 2013.10.21
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4896 [PO3 Companion] State logging payload should return the CableCard Mac for Samsung STB instead of Hostmac

***
version 1.2.0.0_PO3 - 2013.09.27
    - New Feature
        + First release including the followings
        + UC-EPG01B - Tune to a particular channel
        + UC-EPG02B - Focus on a future program
        + UC-REM01B - Apply remote control subset to STB
        + UC-STA01B - Receive the current state of the STB
        + UC-PVR01B - Schedule a recording
        + UC-PVR02B - Delete a scheduled recording
        + UC-PVR03B - Retrieve a list of recordings and scheduled recordings
        + UC-PVR04B - Start a PVR playback
        + UC-VOD01B - Access a specific content page in the VOD application with a specific context
        + Play VOD content from the Companion application
        + Change settings with Close caption and Zoom mode 
    - Resolved Issues
        + N/A

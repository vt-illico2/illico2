package com.videotron.tvi.illico.companion;

public interface ResultCodes {

    public static final int E00_SUCCESS = 0;
    public static final int E01_GENERIC = -1;
    public static final int E02_UNAVAILABLE_VOD_CONTENT_ID = -102;
    public static final int E03_PARTIALLY_SUCCESSFUL = -103;
    public static final int E04_NO_VOD_CONTENT = -104;
    public static final int E05_LIST_IS_EMPTY = -105;
    public static final int E06_INVALID_CHANNEL = -106;
    public static final int E07_CHANNEL_IS_UNSUBSCRIBED = -107;
    public static final int E08_NO_TUNERS_AVAILABLE = -108;
    public static final int E09_INVALID_START_TIME_FORMAT = -109;
    public static final int E10_START_TIME_OUT_OF_RANGE = -110;
    public static final int E11_UNAVAILABLE_PVR_CONTENT_ID_LOCAL = -111;
    public static final int E12_UNAVAILABLE_PVR_CONTENT_ID_REMOTE = -112;
    public static final int E13_INVALID_SETTING_ID = -113;
    public static final int E14_INVALID_SETTING_VALUE = -114;
    public static final int E15_UNABLE_TO_CHANGE_THE_SPECIFIED_SETTING = -115;
    public static final int E16_INVALID_REMOTE_CONTROL_KEY_EVENT_ID = -116;
    public static final int E17_UNABLE_TO_PROCESS_THE_SPECIFIED_REMOTE_CONTROL_KEY = -117;
    public static final int E18_INVALID_SERVER_STRING = -118;
    public static final int E19_INVALID_RSKEY_FORMAT = -119;
    public static final int E20_INVALID_DWELL_TIME = -120;
    public static final int E21_INVALID_LOG_STATE = -121;
    public static final int E22_STOP_LOGGING = -122;
    public static final int E23_MISSING_MANDATORY_PARAMETER = -123;
    public static final int E24_REQUEST_RENDERING_TIMEOUT = -124;
    public static final int E25_RIGHTS_MISSING_TO_ACCESS_CONTENT = -125;
    public static final int E26_PIN_REQUIRED_ON_STB_TO_ACCESS_CONTENT = -126;
    public static final int E27_RIGHTS_MISSING_TO_PLAY_CONTENT = -127;
    public static final int E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT = -128;
    public static final int E29_LAST_VIDEO_IN_THE_PLAY_LIST = -129;
    public static final int E30_INVALID_UDN = -130;
    public static final int E31_STB_IN_STAND_BY_MODE = -131;
    public static final int E32_STB_UNABLE_TO_STOP_THE_LOGGING = -132;
    public static final int E33_INCOMPATIBLE_DEFINITION_TO_PLAY_CONTENT_ON_STB = -133;
    //->Kenneth[2015.6.25] DDC-107 : 실제 얘는 PVR 앱에서 사용.
    public static final int E34_WRONG_HDMI_FOR_UHD = -134;
    //<-

}

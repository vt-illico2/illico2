package com.videotron.tvi.illico.companion.json;

import java.util.Map;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.AbstractList;
import java.io.Writer;
import java.io.StringWriter;
import java.io.IOException;
import com.videotron.tvi.illico.log.Log;

public class JSONEncoder {

    private static void write(Hashtable table, Writer w) throws IOException {
        w.write('{');
        w.write(' ');

        Map.Entry entry;
        Iterator it = table.entrySet().iterator();
        while (it.hasNext()) {
            entry = (Map.Entry) it.next();
            w.write('\"');
            w.write(entry.getKey().toString());
            w.write('\"');

            w.write(' ');
            w.write(':');
            w.write(' ');

            write(entry.getValue(), w);
            if (it.hasNext()) {
                w.write(',');
                w.write(' ');
            }
        }
        w.write(' ');
        w.write('}');
    }

    private static void write(AbstractList list, Writer w) throws IOException {
        w.write('[');
        w.write(' ');

        Iterator it = list.iterator();
        while (it.hasNext()) {
            Object item = it.next();
            write(item, w);

            if (it.hasNext()) {
                w.write(',');
                w.write(' ');
            }
        }
        w.write(' ');
        w.write(']');
    }

    private static void write(Number number, Writer w) throws IOException {
        write(number.toString(), w);
    }

    private static void write(Boolean b, Writer w) throws IOException {
        write(b.toString(), w);
    }

    private static void write(Object o, Writer w) throws IOException {
        if (o instanceof Hashtable) {
            write((Hashtable) o, w);
        } else if (o instanceof AbstractList) {
            write((AbstractList) o, w);
        } else if (o instanceof Number) {
            write((Number) o, w);
        } else if (o instanceof Boolean) {
            write((Boolean) o, w);
        } else {
            w.write('\"');
            write(o.toString(), w);
            w.write('\"');
        }
    }

    /** Hashtable to string (JSON object). */
    public static String encode(Hashtable table) {
        StringWriter sw = new StringWriter();
        try {
            write(table, sw);
        } catch (IOException ex) {
            Log.print(ex);
        }
        return sw.toString();
    }

    /** String to JSON string */
    public static String encode(String s) {
        StringWriter sw = new StringWriter(s.length() * 2);
        try {
            write(s, sw);
        } catch (IOException ex) {
            Log.print(ex);
        }
        return sw.toString();
    }

    public static void write(String s, Writer w) throws IOException {
        int sLen = s.length();
        for (int i = 0; i < sLen; i++) {
            char c = s.charAt(i);
            switch (c) {
            case '\\':
                w.write("\\\\");
                break;
            case '"':
                w.write("\\\"");
                break;
            case '\b':
                w.write("\\b");
                break;
            case '\f':
                w.write("\\f");
                break;
            case '\n':
                w.write("\\n");
                break;
            case '\r':
                w.write("\\r");
                break;
            case '\t':
                w.write("\\t");
                break;
            default:
                if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.BASIC_LATIN) {
                    w.write(c);
                } else {
                    String hex = Integer.toHexString(c);
                    w.write('\\');
                    w.write('u');
                    for (int k = 4 - hex.length(); k > 0 ; k--) {
                        w.write('0');
                    }
                    w.write(hex);
                }
                break;
            }
        }
    }
}

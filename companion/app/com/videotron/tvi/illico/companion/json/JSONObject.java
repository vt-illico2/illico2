package com.videotron.tvi.illico.companion.json;

import java.util.Hashtable;

public class JSONObject {

    public Hashtable table;

    public JSONObject() {
        this(new Hashtable());
    }

    public JSONObject(Hashtable hashtable) {
        this.table = hashtable;
    }

    public void put(String key, Object value) {
        table.put(key, value);
    }

}

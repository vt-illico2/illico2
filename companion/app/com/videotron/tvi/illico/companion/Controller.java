package com.videotron.tvi.illico.companion;

import javax.tv.xlet.XletContext;
import java.rmi.RemoteException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.ixc.monitor.VideoContextListener;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.companion.communication.CommunicationManager;
import com.videotron.tvi.illico.companion.log.LogController;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.upp.*;

/**
 * This is a main class of Companion agent application.
 *
 * @author June Park
 */
public class Controller implements DataUpdateListener, PreferenceListener {//4K : SUPPORT_UHD

    private static final String SETTING_KEY_VALUES = "SETTING_KEY_VALUES";
    private static final String SETTING_VALUE_OVERLAY = "SETTING_VALUE_OVERLAY";

    private SettingKeyValues settingKeyValues = new SettingKeyValues();
    private Properties settingValueOverlay = new Properties();

    private LogLevelAdapter logListener = new LogLevelAdapter();
    private STBModeChangeAdapter stbModeListener = new STBModeChangeAdapter();
    private VideoContextAdapter videoContextListener = new VideoContextAdapter();
    private String videoContext = null;

    /////////////////////////////////////////////////////////////////////////
    // DataUpdateListener
    /////////////////////////////////////////////////////////////////////////

    private static Controller instance = new Controller();

    private Controller() {
    }

    public static Controller getInstance() {
        return instance;
    }

    public void init(XletContext context) {
        CommunicationManager.getInstance().start(context, this);
        DataCenter.getInstance().addDataUpdateListener(SETTING_KEY_VALUES, this);
        DataCenter.getInstance().addDataUpdateListener(SETTING_VALUE_OVERLAY, this);
    }

    public void dispose() {
    }


    /**
     * Request to Monitor to start unbound application.
     * @param appName application name.
     * @param param parameter
     */
    public void startUnboundApplication(String appName, String[] param) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.startUnboundApplication("+appName+", "+param+")");
        try {
            ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME)).startUnboundApplication(appName, param);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /**
     * Checks validity of key and value of setting.
     * @param key key of setting.
     * @param value value to be set for the setting.
     * @return result code.
     */
    public int checkSettingParameters(String key, String value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.checkSettingParameters("+key+", "+value+")");
        if (!settingKeyValues.isValidKey(key)) {
            return ResultCodes.E13_INVALID_SETTING_ID;
        }
        if (!settingKeyValues.isValidEntry(key, value)) {
            return ResultCodes.E14_INVALID_SETTING_VALUE;
        }
        return ResultCodes.E00_SUCCESS;
    }

    public String getSettingValue(String key, String value) {
        String newValue = settingValueOverlay.getProperty(key + '/' + value);
        Log.printDebug("getSettingValue = " + key + " " + value + " " + newValue);
        if (newValue != null) {
            return newValue;
        }
        return value;
    }

    public void resetScreenSaverTimer() {
        try {
            ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME)).resetScreenSaverTimer();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void stopSearch() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.stopSearch()");
        try {
            SearchService ss = (SearchService) DataCenter.getInstance().get(SearchService.IXC_NAME);
            if (ss != null) {
                ss.stopSearchApplication();
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void hidePinEnabler() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.hidePinEnabler()");
        try {
            PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
            if (ps != null) {
                ps.hidePinEnabler();
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public String getVideoContextName() {
        return videoContext;
    }

    public boolean isTerminalBlockedTime() {
        try {
            return ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME)).isTerminalBlockedTime();
        } catch (Exception ex) {
            Log.print(ex);
        }
        return false;
    }

    /////////////////////////////////////////////////////////////////////////
    // DataUpdateListener
    /////////////////////////////////////////////////////////////////////////

    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.dataUpdated("+key+", "+value+")");
        if (key.equals(DaemonService.IXC_NAME)) {
            DaemonService daemonService = (DaemonService) value;
            String hostName = DataCenter.getInstance().getString("DAEMON_HOST_NAME");
            try {
                daemonService.addListener(hostName, RemoteRequestHandler.getInstance());
            } catch (RemoteException ex) {
                Log.print(ex);
            }
        } else if (key.equals(StcService.IXC_NAME)) {
            StcService stcService = (StcService) value;
            try {
                int currentLevel = stcService.registerApp(App.NAME);
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            Object o = SharedMemory.getInstance().get("stc-" + App.NAME);
            if (o != null) {
                DataCenter.getInstance().put("LogCache", o);
            }
            try {
                stcService.addLogLevelChangeListener(App.NAME, logListener);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
        } else if (key.equals(MonitorService.IXC_NAME)) {
            MonitorService monitorService = (MonitorService) value;
            try {
                monitorService.addSTBModeChangeListener(stbModeListener, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                stbModeListener.modeChanged(monitorService.getSTBMode());
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                monitorService.addVideoContextListener(videoContextListener, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                videoContextListener.videoModeChanged(monitorService.getVideoContextName());
            } catch (RemoteException ex) {
                Log.print(ex);
            }
        } else if (key.equals(SETTING_KEY_VALUES)) {
            settingKeyValues.load((File) value);
        } else if (key.equals(SETTING_VALUE_OVERLAY)) {
            try {
                settingValueOverlay.load(new FileInputStream((File) value));
            } catch (IOException ex) {
                Log.print(ex);
            }
            Log.printDebug("getSettingValue = " + settingValueOverlay);
        } else if (key.equals(PreferenceService.IXC_NAME)) {
            //->Kenneth[2015.2.21] 4K : SUPPORT_UHD 값을 얻기 위해서
            String[] prefKeys = {MonitorService.SUPPORT_UHD};
            PreferenceService preferenceService = (PreferenceService) value;
            String[] values = null;
            try {
                values = preferenceService.addPreferenceListener(this,
                    FrameworkMain.getInstance().getApplicationName(), prefKeys, null);
                for (int i = 0; i < prefKeys.length; i++) {
                    if (values != null && values[i] != null) {
                        Log.printInfo("Controller: " + prefKeys[i] + " = " + values[i]);
                        if (MonitorService.SUPPORT_UHD.equals(prefKeys[i])) {
                            if ("true".equals(value)) {
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.dataUpdated() : set SUPPORT_UHD = true");
                                Environment.SUPPORT_UHD = true;
                            } else {
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.dataUpdated() : set SUPPORT_UHD = false");
                                Environment.SUPPORT_UHD = false;
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public void dataRemoved(String key) {
    }

    /** PreferenceListener. */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.receiveUpdatedPreference("+name+", "+value+")");
        if (value != null) {
            //->Kenneth [2015.2.21] 4K
            if ("SUPPORT_UHD".equals(name)) {
                if ("true".equals(value)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.receiveUpdatedPreference() : set SUPPORT_UHD = true");
                    Environment.SUPPORT_UHD = true;
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Controller.receiveUpdatedPreference() : set SUPPORT_UHD = false");
                    Environment.SUPPORT_UHD = true;
                }
            }
        }
    }

    class LogLevelAdapter implements LogLevelChangeListener {
    	public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    class STBModeChangeAdapter implements STBModeChangeListener {
        public void modeChanged(int newMode) throws RemoteException {
            if (Log.DEBUG_ON) {
                Log.printDebug("STBModeChangeAdapter.modeChanged = " + newMode);
            }
            RemoteRequestHandler.getInstance().setEnabled(newMode == MonitorService.STB_MODE_NORMAL);
        }
    }

    class VideoContextAdapter implements VideoContextListener {
        public void videoModeChanged(String videoName) throws RemoteException {
            Log.printDebug("VideoContextListener.videoModeChanged = " + videoName);
            videoContext = videoName;
            LogController.getInstance().videoModeChanged(videoContext);
        }
    }

}

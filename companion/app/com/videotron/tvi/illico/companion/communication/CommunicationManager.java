package com.videotron.tvi.illico.companion.communication;

import java.rmi.Remote;

import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.screensaver.ScreenSaverService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.IxcLookupWorker;
import com.videotron.tvi.illico.util.Environment;

public class CommunicationManager {

    private XletContext xletContext;
    private static CommunicationManager instance = new CommunicationManager();

    public static CommunicationManager getInstance() {
        return instance;
    }

    IxcLookupWorker lookupWorker;

    public void start(XletContext xletContext, DataUpdateListener l) {
        this.xletContext = xletContext;
        lookupWorker = new IxcLookupWorker(xletContext);
        lookupWorker.lookup("/1/1/", MonitorService.IXC_NAME, l);
        lookupWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, l);
        lookupWorker.lookup("/1/2026/", EpgService.IXC_NAME, l);
        lookupWorker.lookup("/1/2100/", StcService.IXC_NAME, l);
        lookupWorker.lookup("/1/2040/", DaemonService.IXC_NAME, l);
        lookupWorker.lookup("/1/1020/", VODService.IXC_NAME, l);
        lookupWorker.lookup("/1/20A0/", PvrService.IXC_NAME, l);
        lookupWorker.lookup("/1/2080/", SearchService.IXC_NAME, l);
    }

    public void dispose() {
    }

}

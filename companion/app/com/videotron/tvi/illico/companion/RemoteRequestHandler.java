package com.videotron.tvi.illico.companion;

import java.util.Hashtable;
import java.util.Vector;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.rmi.RemoteException;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.daemon.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.vod.*;
import com.videotron.tvi.illico.companion.log.LogController;

public class RemoteRequestHandler implements RemoteRequestListener, ResultCodes {

    private static final String DATE_FORMAT = "dd-MM-yyyy:HH:mm:ss";

    private static final int NO_VALUE = Integer.MIN_VALUE;

    private static RemoteRequestHandler instance = new RemoteRequestHandler();

    public static RemoteRequestHandler getInstance() {
        return instance;
    }

    private RemoteRequestHandler() {
    }

    private boolean enabled = false;

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Pass request to host application.
     * @param path path to the operation to execute by host application.
     * @param body parameters(parma1=value1&param2=value2..)
     * @return response to request
     * @throws RemoteException during the execution of a remote method call
     */
    public byte[] request(String path, byte[] body) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemoteRequestHandler.request("+path+", "+body+")");
        if (path == null) {
            if (body == null) {
                Log.printWarning(App.LOG_HEADER+"RemoteRequestHandler.request : no path, no bytes");
            } else {
                Log.printWarning(App.LOG_HEADER+"RemoteRequestHandler.request : no path, " + body.length + " bytes");
            }
            return null;
        }
        String ret = null;
        if (enabled) {
            try {
                ret = process(path, body);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemoteRequestHandler.request() : process() returns "+ret);
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else {
            Log.printWarning("RemoteRequestHandler: STB is not enabled.");
        }
        if (ret == null) {
            ret = "result=-1";
        }
        return ret.getBytes();
    }

    private String process(String path, byte[] body) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemoteRequestHandler.process()");
        int ret;
        Parameters param = new Parameters(body);
        if ("tuneChannel".equalsIgnoreCase(path)) {
            ret = tuneChannel(param);
        } else if ("showEpgContent".equalsIgnoreCase(path)) {
            ret = showEpgContent(param);
        } else if ("changeSetting".equalsIgnoreCase(path)) {
            ret = changeSetting(param);
        } else if ("applyRcKeyEvent".equalsIgnoreCase(path)) {
            ret = applyRcKeyEvent(param);
        //->Kenneth [R4.1] : Add applyOcapRcKeyEvent
        } else if ("applyOcapRcKeyEvent".equalsIgnoreCase(path)) {
            ret = applyOcapRcKeyEvent(param);
        //<-
        } else if ("showVodContent".equalsIgnoreCase(path)) {
            ret = showVodContent(param);
        } else if ("playVodContent".equalsIgnoreCase(path)) {
            return playVodContent(param);
        } else if ("playPvrContent".equalsIgnoreCase(path)) {
            ret = playPvrContent(param);
        } else if ("initiateLogging".equalsIgnoreCase(path)) {
            ret = initiateLogging(param);
        } else if ("stopLogging".equalsIgnoreCase(path)) {
            ret = stopLogging(param);
        } else {
            return null;
        }
        return "result=" + ret;
    }

    /**
     * Tune to a specific channel.
     */
    //->Kenneth[2015.6.25] DDC-107
    private int tuneChannel(Parameters param) {
        int number = param.getInt("channel_id");
        if (number == NO_VALUE) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (es == null) {
            return E01_GENERIC;
        }

        if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
            return E31_STB_IN_STAND_BY_MODE;
        }

        if (Controller.getInstance().isTerminalBlockedTime()) {
            return E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        }

        Controller.getInstance().stopSearch();
        Controller.getInstance().hidePinEnabler();
        Controller.getInstance().resetScreenSaverTimer();
        int ret = 0;
        try {
            ret = es.requestTune(number);
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
            return E06_INVALID_CHANNEL;
        } catch (IllegalStateException ex) {
            Log.print(ex);
            return E08_NO_TUNERS_AVAILABLE;
        } catch (Exception ex) {
            Log.print(ex);
            return E01_GENERIC;
        }
        return ret;
    }
    /*
    private int tuneChannel(Parameters param) {
        int number = param.getInt("channel_id");
        if (number == NO_VALUE) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (es == null) {
            return E01_GENERIC;
        }

        if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
            return E31_STB_IN_STAND_BY_MODE;
        }

        if (Controller.getInstance().isTerminalBlockedTime()) {
            return E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        }

        Controller.getInstance().stopSearch();
        Controller.getInstance().hidePinEnabler();
        Controller.getInstance().resetScreenSaverTimer();
        boolean subscribed = false;
        try {
            subscribed = es.requestTune(number);
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
            return E06_INVALID_CHANNEL;
        } catch (IllegalStateException ex) {
            Log.print(ex);
            return E08_NO_TUNERS_AVAILABLE;
        } catch (Exception ex) {
            Log.print(ex);
            return E01_GENERIC;
        }
        if (subscribed) {
            return E00_SUCCESS;
        } else {
            return E07_CHANNEL_IS_UNSUBSCRIBED;
        }
    }
    */
    //<-

    /**
     * Show the details page of a EPG program or PPV event.
     */
    private int showEpgContent(Parameters param) {
        long start;
        String startStr = param.getString("start");
        if (startStr == null) {
            start = System.currentTimeMillis();
        } else {
            start = getDateFromString(startStr);
            if (start == 0) {
                return E09_INVALID_START_TIME_FORMAT;
            }
        }

        int number = param.getInt("channel_id");
        if (number == NO_VALUE) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (es == null) {
            return E01_GENERIC;
        }
        TvChannel ch = null;
        try {
            ch = es.getChannelByNumber(number);
        } catch (Exception ex) {
            return E01_GENERIC;
        }
        if (ch == null) {
            return E06_INVALID_CHANNEL;
        }

        if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
            return E31_STB_IN_STAND_BY_MODE;
        }

        if (Controller.getInstance().isTerminalBlockedTime()) {
            return E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        }

        int result = E00_SUCCESS;
        Controller.getInstance().stopSearch();
        Controller.getInstance().hidePinEnabler();
        Controller.getInstance().resetScreenSaverTimer();
        try {
            result = es.showProgramDetails(
                new String[] { App.NAME, ch.getCallLetter(), String.valueOf(start) });
        } catch (Exception ex) {
            Log.print(ex);
            result = E01_GENERIC;
        }
        return result;
    }

    /**
     * Change a setting.
     */
    private int changeSetting(Parameters param) {
        String key = param.getString("setting_id");
        String value = param.getString("value");
        if (key == null || value == null) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }

        int code = Controller.getInstance().checkSettingParameters(key, value);
        if (code != E00_SUCCESS) {
            return code;
        }

        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        if (ps == null) {
            return E01_GENERIC;
        }
        if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
            return E31_STB_IN_STAND_BY_MODE;
        }

        value = Controller.getInstance().getSettingValue(key, value);

        try {
            boolean result = ps.setPreferenceValue(key, value);
            if (result) {
                Controller.getInstance().resetScreenSaverTimer();
                return E00_SUCCESS;
            } else {
                return E15_UNABLE_TO_CHANGE_THE_SPECIFIED_SETTING;
            }
        } catch (Exception ex) {
            Log.print(ex);
            return E15_UNABLE_TO_CHANGE_THE_SPECIFIED_SETTING;
        }
    }


    /**
     * 7.3.7 Applying remote control key events to the STB.
     */
    private int applyRcKeyEvent(Parameters param) {
        int uniqueId = param.getInt("rck_id", -1);
        if (uniqueId == NO_VALUE) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        if (Host.getInstance().getPowerMode() != Host.FULL_POWER && uniqueId != 1 && uniqueId != 11) {
            return E31_STB_IN_STAND_BY_MODE;
        }

        int result = E00_SUCCESS;
        try {
            result = processKey(uniqueId);
            if (result == E00_SUCCESS) {
                Controller.getInstance().resetScreenSaverTimer();
            }
        } catch (Exception ex) {
            result = E01_GENERIC;
        }
        return result;
    }

    //->Kenneth [R4.1] 
    private int applyOcapRcKeyEvent(Parameters param) {
        int keyCode = param.getInt("rck_id", -1);
        Log.printDebug("Kenneth : applyOcapRcKeyEvent("+keyCode+")");
        if (keyCode == NO_VALUE) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }

        int result = E00_SUCCESS;
        try {
            Class c = Class.forName("com.alticast.event.InputEventGenerator");
            java.lang.reflect.Method m = c.getMethod("pushKeyEvent", new Class[] { int.class, int.class });
            Log.printDebug("Kenneth : Call pushKeyEvent(KEY_PRESSED)");
            m.invoke(null, new Object[] { new Integer(java.awt.event.KeyEvent.KEY_PRESSED), new Integer(keyCode) });
            Log.printDebug("Kenneth : Call pushKeyEvent(KEY_RELEASED)");
            m.invoke(null, new Object[] { new Integer(java.awt.event.KeyEvent.KEY_RELEASED), new Integer(keyCode) });
        } catch (Exception ex) {
            Log.print(ex);
            result = E01_GENERIC;
        }
        return result;
    }
    //<-

    private int processKey(int uniqueId) throws Exception {
        switch (uniqueId) {
            case 0: // MUTE
                EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
                es.toggleMute();
                return E00_SUCCESS;
            case 1: // POWER
            {
                Host host = Host.getInstance();
                if (host.getPowerMode() == Host.FULL_POWER) {
                    host.setPowerMode(Host.LOW_POWER);
                } else {
                    host.setPowerMode(Host.FULL_POWER);
                }
                return E00_SUCCESS;
            }
            case 11: // POWER_ON
            {
                Host host = Host.getInstance();
                if (host.getPowerMode() != Host.FULL_POWER) {
                    host.setPowerMode(Host.FULL_POWER);
                }
                return E00_SUCCESS;
            }
            case 12: // POWER_OFF
            {
                Host host = Host.getInstance();
                if (host.getPowerMode() != Host.LOW_POWER) {
                    host.setPowerMode(Host.LOW_POWER);
                }
                return E00_SUCCESS;
            }
            default:
                return processVideoKey(uniqueId);
        }
    }

    private int processVideoKey(int uniqueId) throws Exception {
        MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (ms == null) {
            return E01_GENERIC;
        }
        String vcName = Controller.getInstance().getVideoContextName();

        if (vcName == null || "EPG".equals(vcName) || "PVR".equals(vcName)) {
            PvrService ps = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
            if (ps == null) {
                return E17_UNABLE_TO_PROCESS_THE_SPECIFIED_REMOTE_CONTROL_KEY;
            }
            int keyCode = getVideoControlKeyCode(uniqueId);
            if (keyCode == -1) {
                return E16_INVALID_REMOTE_CONTROL_KEY_EVENT_ID;
            }
            boolean result = ps.pvrPlayControl(keyCode);
            if (result) {
                return E00_SUCCESS;
            } else {
                return E17_UNABLE_TO_PROCESS_THE_SPECIFIED_REMOTE_CONTROL_KEY;
            }
        } else if ("VOD".equals(vcName)) {
            // VOD
            VODService vs = (VODService) DataCenter.getInstance().get(VODService.IXC_NAME);
            if (vs == null) {
                return E17_UNABLE_TO_PROCESS_THE_SPECIFIED_REMOTE_CONTROL_KEY;
            }
            int keyCode = getVideoControlKeyCode(uniqueId);
            if (keyCode == -1) {
                return E16_INVALID_REMOTE_CONTROL_KEY_EVENT_ID;
            }
            boolean result = vs.vodPlayControl(keyCode);
            if (result) {
                return E00_SUCCESS;
            } else {
                return E17_UNABLE_TO_PROCESS_THE_SPECIFIED_REMOTE_CONTROL_KEY;
            }
        }
        return E17_UNABLE_TO_PROCESS_THE_SPECIFIED_REMOTE_CONTROL_KEY;
    }

    private int getVideoControlKeyCode(int uniqueId) {
        switch (uniqueId) {
            case 3:
                return OCRcEvent.VK_PLAY;
            case 4:
                return OCRcEvent.VK_PAUSE;
            case 5:
                return OCRcEvent.VK_STOP;
            case 6:
                return OCRcEvent.VK_FAST_FWD;
            case 7:
                return OCRcEvent.VK_REWIND;
            case 8:
                return OCRcEvent.VK_FORWARD;
            case 9:
                return OCRcEvent.VK_BACK;
//            case 10:
//                return OCRcEvent.VK_EXIT;
            default:
                return -1;
        }
    }

    /**
     * Show the asset page of a VOD content or Bundle.
     */
    private int showVodContent(Parameters param) {
        VODService vs = (VODService) DataCenter.getInstance().get(VODService.IXC_NAME);
        if (vs == null) {
            return E01_GENERIC;
        }
        String id = param.getString("content_id");
        if (id == null) {
            return E23_MISSING_MANDATORY_PARAMETER;
        } else if (id.length() == 0) {
            return E02_UNAVAILABLE_VOD_CONTENT_ID;
        }
        if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
            return E31_STB_IN_STAND_BY_MODE;
        }
        if (Controller.getInstance().isTerminalBlockedTime()) {
            return E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        }
        Controller.getInstance().stopSearch();
        Controller.getInstance().hidePinEnabler();
        Controller.getInstance().resetScreenSaverTimer();
        try {
            int ret = vs.showContent(id);
            
            if (ret != E00_SUCCESS) {
            	return ret;
            }
        } catch (IllegalStateException ex) {
            Log.print(ex);
            return E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        } catch (SecurityException ex) {
            Log.print(ex);
            return E27_RIGHTS_MISSING_TO_PLAY_CONTENT;
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
            return E02_UNAVAILABLE_VOD_CONTENT_ID;
        } catch (UnsupportedOperationException ex) {
            Log.print(ex);
            return E24_REQUEST_RENDERING_TIMEOUT;
        } catch (Exception ex) {
            Log.print(ex);
            return E01_GENERIC;
        }
        return E00_SUCCESS;
    }

    /**
     * Play a single or a list of free VOD contents.
     */
    private String playVodContent(Parameters param) {
        VODService vs = (VODService) DataCenter.getInstance().get(VODService.IXC_NAME);
        if (vs == null) {
            return "result=" + E01_GENERIC;
        }
        int count = param.getInt("count", -1);
        if (count == NO_VALUE) {
            return "result=" + E23_MISSING_MANDATORY_PARAMETER;
        } else if (count <= 0) {
            return "result=" + E05_LIST_IS_EMPTY;
        }
        int index = 1;
        Vector entries = param.entries;

        String contentId = null;
        String language = null;
        String definition = null;
        String trailer = null;

        Vector vodParams = new Vector();

        while (index < entries.size()) {
            String[] array = (String[]) entries.elementAt(index);
            if ("content_id".equals(array[0])) {
                if (contentId != null && trailer != null) {
                    boolean ok = putVodParams(vodParams, contentId, trailer, language, definition);
                    if (!ok) {
                        return "result=" + E23_MISSING_MANDATORY_PARAMETER;
                    }
                    trailer = null;
                    contentId = null;
                    language = null;
                    definition = null;
                } else {
                    contentId = array[1];
                }
            } else if ("trailer".equals(array[0])) {
                if (contentId != null && trailer != null) {
                    boolean ok = putVodParams(vodParams, contentId, trailer, language, definition);
                    if (!ok) {
                        return "result=" + E23_MISSING_MANDATORY_PARAMETER;
                    }
                    trailer = null;
                    contentId = null;
                    language = null;
                    definition = null;
                } else {
                    trailer = array[1];
                }
            } else if ("language".equals(array[0])) {
                language = array[1];
            } else if ("definition".equals(array[0])) {
                definition = array[1];
            }
            index++;
        }
        if (contentId != null && trailer != null) {
            boolean ok = putVodParams(vodParams, contentId, trailer, language, definition);
            if (!ok) {
                return "result=" + E23_MISSING_MANDATORY_PARAMETER;
            }
        }

        int size = vodParams.size();
        if (size < count) {
            return "result=" + E23_MISSING_MANDATORY_PARAMETER;
        }

        if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
            return "result=" + E31_STB_IN_STAND_BY_MODE;
        }

        if (Controller.getInstance().isTerminalBlockedTime()) {
            return "result=" + E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        }

        String[] s1 = new String[size];
        boolean[] b = new boolean[size];
        String[] s3 = new String[size];
        String[] s4 = new String[size];
        for (int i = 0; i < size; i++) {
            Object[] content = (Object[]) vodParams.elementAt(i);
            s1[i] = (String) content[0];
            b[i] = ((Boolean) content[1]).booleanValue();
            s3[i] = (String) content[2];
            s4[i] = (String) content[3];
        }

        try {
            PvrService ps = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
            if (!ps.checkTuner(0)) {
                return "result=" + E08_NO_TUNERS_AVAILABLE;
            }

            Controller.getInstance().stopSearch();
            Controller.getInstance().hidePinEnabler();
            Controller.getInstance().resetScreenSaverTimer();
            int ret = vs.playContent(s1, b, s3, s4);
            
            if (ret != E00_SUCCESS) {
            	return "result=" + ret;
            }
        } catch (SecurityException ex) {
            Log.print(ex);
            return "result=" + E27_RIGHTS_MISSING_TO_PLAY_CONTENT;
        } catch (IllegalStateException ex) {
            Log.print(ex);
            return "result=" + E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
            return "result=" + E04_NO_VOD_CONTENT;
        } catch (UnsupportedOperationException ex) {
            Log.print(ex);
            return "return=" + E24_REQUEST_RENDERING_TIMEOUT;
        } catch (Exception ex) {
            Log.print(ex);
            return "result=" + E01_GENERIC;
        }
        return "result=" + E00_SUCCESS;
    }

    private boolean putVodParams(Vector vodParams, String contentId, String trailer, String language, String definition) {
        boolean trailerValue;
        if ("1".equals(trailer)) {
            trailerValue = true;
        } else if ("0".equals(trailer)) {
            trailerValue = false;
        } else {
            return false;
        }
        vodParams.addElement(new Object[] { contentId, Boolean.valueOf(trailerValue), language, definition });
        return true;
    }

    /**
     * Play a PVR content.
     */
    private int playPvrContent(Parameters param) {
        PvrService ps = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
        if (ps == null) {
            return E01_GENERIC;
        }
        int id = param.getInt("content_id", NO_VALUE);
        int startPoint = param.getInt("start_point", NO_VALUE);
        if (id == NO_VALUE || startPoint == NO_VALUE) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        String udn = param.getString("udn");
        if (id == 0) {
            return (udn == null) ? E11_UNAVAILABLE_PVR_CONTENT_ID_LOCAL : E12_UNAVAILABLE_PVR_CONTENT_ID_REMOTE;
        }
        if (udn != null) {
            udn = TextUtil.urlDecode(udn);
        }

        if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
            return E31_STB_IN_STAND_BY_MODE;
        }
        if (Controller.getInstance().isTerminalBlockedTime()) {
            return E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        }
        
        /* [2015.6.25] DDC-107 companion 에서 판단하지 말고 PVR 에서 판단하게 하자.
        try {
        	PvrContent pContent = ps.getContent(String.valueOf(id));
        	int definition = ((Integer)pContent.getAppData(AppDataKeys.DEFINITION)).intValue();
        	if (Log.DEBUG_ON) {
        		Log.printDebug("RemoteRequestHandler, playPvrContent, definition=" + definition + ", SUPPORT_UHD="
        				+ Environment.SUPPORT_UHD);
        	}
        	if (definition == 2 && !Environment.SUPPORT_UHD) {
        		return E33_INCOMPATIBLE_DEFINITION_TO_PLAY_CONTENT_ON_STB;
        	}
        } catch (Exception ex) {
        	Log.printDebug(ex.getMessage());
        	ex.printStackTrace();
        }
        */

        Controller.getInstance().stopSearch();
        Controller.getInstance().hidePinEnabler();
        Controller.getInstance().resetScreenSaverTimer();
        try {
            //Kenneth->[2015.6.25] DDC-107 
            int ret = ps.playContent(id, startPoint, udn);
            return ret;
        } catch (IllegalStateException ex) {
            Log.print(ex);
            return E28_PIN_REQUIRED_ON_STB_TO_PLAY_CONTENT;
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
            return E30_INVALID_UDN;
        } catch (Exception ex) {
            Log.print(ex);
            return E01_GENERIC;
        }
    }

    /**
     * Initiate the State logging on the STB.
     */
    private int initiateLogging(Parameters param) {
        String server = param.getString("server");
        if (server == null) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        server = TextUtil.urlDecode(server);
        String proxy = param.getString("proxy");
        int port = param.getInt("port", 0);
        String rskey = param.getString("rskey");
        if (rskey == null) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        String logState = param.getString("log_state");
        if (logState == null) {
            return E23_MISSING_MANDATORY_PARAMETER;
        }
        rskey = TextUtil.urlDecode(rskey);

        try {
            LogController.getInstance().reset(server, proxy, port, rskey, logState);
        } catch (MalformedURLException ex) {
            Log.print(ex);
            return E18_INVALID_SERVER_STRING;
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
            return E20_INVALID_DWELL_TIME;
        } catch (IllegalStateException ex) {
            Log.print(ex);
            return E21_INVALID_LOG_STATE;
        }
        return E00_SUCCESS;
    }

    private int stopLogging(Parameters param) {
        LogController.getInstance().stop();
        return E00_SUCCESS;
    }

    /////////////////////////////////////////////////////////////////////////
    // Utility APIs
    /////////////////////////////////////////////////////////////////////////

    private long getDateFromString(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        try {
            return dateFormat.parse(date).getTime();
        } catch (Exception ex) {
            Log.print(ex);
        }
        return 0;
    }


    /** Request parameters. */
    class Parameters {
        Hashtable table = new Hashtable();
        Vector entries = new Vector();

        public Parameters(byte[] body) {
            if (body == null || body.length == 0) {
                return;
            }
            String[] params = TextUtil.tokenize(new String(body), '&');
            for (int i = 0; params != null && i < params.length; i++) {
                String s = params[i];
                int pos = s.indexOf('=');
                if (pos != -1) {
                    String key = s.substring(0, pos);
                    String value = s.substring(pos + 1);
                    entries.addElement(new String[] { key, value });
                    Object o = table.get(key);
                    if (o == null) {
                        table.put(key, value);
                    } else if (o instanceof Vector) {
                        Vector v = (Vector) o;
                        v.addElement(value);
                    } else {
                        Vector v = new Vector();
                        v.addElement(o);
                        v.addElement(value);
                        table.put(key, v);
                    }
                }
            }
        }

        public Object get(String key) {
            return table.get(key);
        }

        public String getString(String key) {
            return (String) table.get(key);
        }

        public int getInt(String key) {
            return getInt(key, 0);
        }

        public int getInt(String key, int defaultValue) {
            Object value = table.get(key);
            if (value == null) {
                return NO_VALUE;
            }
            try {
                return Integer.parseInt((String) value);
            } catch (Exception ex) {
                return defaultValue;
            }
        }

    }
}

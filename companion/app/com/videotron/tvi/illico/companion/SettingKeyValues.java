package com.videotron.tvi.illico.companion;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Hashtable;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.framework.io.TextReader;

public class SettingKeyValues {

    Hashtable table = new Hashtable();

    public SettingKeyValues() {
    }

    public void load(File file) {
        Log.printInfo("SettingKeyValues : " + file);
        if (file.exists()) {
            String[] lines = TextReader.read(file);
            for (int i = 0; lines != null && i < lines.length; i++) {
                String s = lines[i];
                if (s == null || s.startsWith("#") || s.startsWith("!")) {
                    continue;
                }
                int pos = s.indexOf('=');
                if (pos < 0) {
                    continue;
                }
                String key = s.substring(0, pos).trim();
                String value = s.substring(pos + 1).trim();
                String[] values = TextUtil.tokenize(value, '|');
                HashSet valueSet = new HashSet();
                for (int j = 0; j < values.length; j++) {
                    valueSet.add(values[j]);
                }
                Log.printDebug("SettingKeyValues: key = " + key + ", values = " + valueSet);
                table.put(key, valueSet);
            }
        }
    }

    public boolean isValidKey(String key) {
        return table.containsKey(key);
    }

    public boolean isValidEntry(String key, String value) {
        HashSet set = (HashSet) table.get(key);
        if (set == null) {
            return false;
        }
        return set.contains(value);
    }

}


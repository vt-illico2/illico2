package com.videotron.tvi.illico.companion;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;


/**
 * This class is the initial class of FlipBar.
 *
 * @version $Revision: 1.9 $ $Date: 2017/01/12 19:01:22 $
 * @author June Park
 */
public class App implements Xlet, ApplicationConfig {

	/** App name. */
	public static final String NAME = "Companion";
	/** Version. */
    //->Kenneth [R4.1] new version
	//public static final String VERSION = "1.4.1.1";
	//public static final String VERSION = "R4_1.1.0";
	//public static final String VERSION = "R4_1.2.1";
    //->Kenneth [2015.2.21] 4K
	//public static final String VERSION = "4K.1.0";
	public static final String VERSION = "4K.2.1";//[2015.6.29] VT 에 릴리즈 : DDC-107, VDTRMASTER-5478
    public static final String LOG_HEADER = "T_COMPANION : ";

	/** XletContext. */
	private XletContext xletContext;

	/**
	 * Signals the Xlet to initialize itself and enter the Paused state.
	 *
	 * @param ctx
	 *            The XletContext of this Xlet.
	 * @throws XletStateChangeException
	 *             If the Xlet cannot be initialized.
	 */
	public void initXlet(XletContext ctx) throws XletStateChangeException {
		this.xletContext = ctx;
        FrameworkMain.getInstance().init(this);
    }


	/**
	 * Signals the Xlet to start providing service and enter the Active state.
	 *
	 * @throws XletStateChangeException
	 *             is thrown if the Xlet cannot start providing service.
	 */
	public void startXlet() throws XletStateChangeException {
		FrameworkMain.getInstance().start();
	}

	/**
	 * Signals the Xlet to stop providing service and enter the Paused state.
	 */
	public void pauseXlet() {
		FrameworkMain.getInstance().pause();
	}

	/**
	 * Signals the Xlet to terminate and enter the Destroyed state.
	 *
	 * @param unconditional
	 *            If unconditional is true when this method is called, requests
	 *            by the Xlet to not enter the destroyed state will be ignored.
	 * @throws XletStateChangeException
	 *             is thrown if the Xlet wishes to continue to execute (Not
	 *             enter the Destroyed state). This exception is ignored if
	 *             unconditional is equal to true.
	 */
	public void destroyXlet(boolean unconditional)
			throws XletStateChangeException {
		FrameworkMain.getInstance().destroy();
	}



	////////////////////////////////////////////////////////////////////////////
	// ApplicationConfig implementation
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the version of Application.
	 *
	 * @return version string.
	 */
	public String getVersion() {
		return VERSION;
	}

	/**
	 * Returns the name of Application.
	 *
	 * @return application name.
	 */
	public String getApplicationName() {
		return NAME;
	}

	/**
	 * Returns the Application's XletContext.
	 *
	 * @return application's XletContext.
	 */
	public XletContext getXletContext() {
		return xletContext;
	}

    /**
     * Initialize this application.
     */
	public void init() {
        Controller.getInstance().init(xletContext);
	}

}

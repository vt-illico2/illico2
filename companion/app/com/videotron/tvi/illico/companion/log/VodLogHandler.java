package com.videotron.tvi.illico.companion.log;

import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;

public class VodLogHandler extends LogHandler {

    public VodLogHandler(LogSession session) {
        super(session);
        stateName = LOG_STATE_VOD;
    }

    public void enableLogStateListener(boolean enabled) {
        VODService vs = (VODService) DataCenter.getInstance().get(VODService.IXC_NAME);
        try {
            if (enabled) {
                vs.setLogStateListener(this);
            } else {
                vs.setLogStateListener(null);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

}

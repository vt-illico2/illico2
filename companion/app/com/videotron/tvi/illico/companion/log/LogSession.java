package com.videotron.tvi.illico.companion.log;

import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import java.util.LinkedList;
import java.util.Enumeration;
import java.text.SimpleDateFormat;
import com.videotron.tvi.illico.util.Worker;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.companion.json.JSONEncoder;
import com.videotron.tvi.illico.companion.ResultCodes;
import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;
import javax.tv.util.TVTimerWentOffListener;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimer;

public class LogSession implements PowerModeChangeListener {

    public static final String KEY_HOSTMAC = "hostmac";
    public static final String KEY_RSKEY = "rskey";
    public static final String KEY_TIMESTAMP = "timestamp";
    public static final String KEY_LOG = "log";
    public static final String KEY_LOG_TYPE = "log_type";

    public static final String DATE_FORMAT = "dd-MM-yyyy:HH:mm:ss";

    //JIRA(VDTRMASTER-5286) : 정준이 수정했던 것을 R4.1 에 merge 함.
    private static String STOP_LOGGING_RESULT = "result=" + ResultCodes.E22_STOP_LOGGING;

    private static int createCount = 0;

    private final URL url;
    private final String hostMac;
    private final String rskey;

    private Worker uploader;

    private Hashtable appTable = new Hashtable();
    private Hashtable logHandlerTable = new Hashtable();
    private LogHandler currentVideoLogHandler;

    private StateLog lastAppLog = null;
    private StateLog lastStbLog = null;
    private StateLog lastKeysLog = null;

    private LinkedList debugLogs;
    private String threadName;

    private boolean stbEnabled = false;
    private boolean keysEnabled = false;

    private Vector lastSentStateLogs = null;

    private boolean firstSent = false;

    private DwellTimer dwellTimer = new DwellTimer();

    public LogSession(String server, String proxy, int port, String rskey, String logState) throws MalformedURLException {
        createCount++;
        if (proxy != null) {
            url = new URL("HTTP", proxy, port, server);
        } else {
            url = new URL(server);
        }

        this.hostMac = findHostMac();
        this.rskey = rskey;

        if (logState == null) {
            throw new IllegalStateException("log_state is null");
        }

        // log_state
        String[] apps = TextUtil.tokenize(logState, ',');
        if (apps == null || apps.length == 0) {
            throw new IllegalStateException("empty apps");
        }

        AppLogSetting[] appSettings = new AppLogSetting[apps.length];
        for (int i = 0; i < apps.length; i++) {
            AppLogSetting als = new AppLogSetting(apps[i]);
            appSettings[i] = als;
            String appName = als.appName;
            if (!LogController.getInstance().isValidState(appName)) {
                throw new IllegalStateException("not supporting app = " + appName);
            }
            Object oldSetting = appTable.put(appName, als);
            if (oldSetting != null) {
                throw new IllegalStateException("duplicated app = " + appName);
            }
            LogHandler handler = createLogHandler(appName);
            if (handler != null) {
                logHandlerTable.put(appName, handler);
            }
        }

        stbEnabled = appTable.containsKey("STB");
        keysEnabled = appTable.containsKey("KEYS");

        threadName = "LogSession-" + createCount;
        uploader = new Worker(threadName, true);
        debugLogs = (LinkedList) SharedMemory.getInstance().get(SharedDataKeys.COMPANION_DEBUG_LOGS);
        debugLogs.addFirst(threadName + " created = " + logState);
    }

    private String findHostMac() {
        if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO) {
            String macAddr = Host.getInstance().getReverseChannelMAC();
            if (Log.DEBUG_ON) {
                Log.printDebug("LogSession: getReverseChannelMAC = " + macAddr);
            }
            StringBuffer sb = new StringBuffer(15);
            int len = macAddr.length();
            int pos = 0;
            for (int i = 0; i < 10; i++) {
                if (pos + 2 <= len) {
                    sb.append(macAddr.substring(pos, pos+2));
                    pos += 3;
                } else {
                    break;
                }
            }
            return sb.toString();
        } else {
            String ret = "null";
            MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            if (ms != null) {
                try {
                    StringBuffer sb = new StringBuffer();
                    byte[] mac = ms.getCableCardMacAddress();
                    for (int i = 0; i < mac.length; i++) {
                        String str = Integer.toHexString(((int) mac[i]) & 0xff);
                        if (str.length() == 1) {
                            sb.append('0');
                        }
                        sb.append(str);
                    }
                    return sb.toString();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            return ret;
        }
    }

    public synchronized void start(String currentVideo) {
        setCurrentVideo(currentVideo);
        if (stbEnabled) {
            Host.getInstance().addPowerModeChangeListener(this);
        }
    }

    public synchronized void dispose() {
        Host.getInstance().removePowerModeChangeListener(this);
        uploader.dispose();
        dwellTimer.dispose();

        LogHandler handler = currentVideoLogHandler;
        if (handler != null) {
            handler.dispose();
        }
        Enumeration en = logHandlerTable.elements();
        while (en.hasMoreElements()) {
            handler = (LogHandler) en.nextElement();
            handler.dispose();
        }
        currentVideoLogHandler = null;

        lastAppLog = null;
        lastStbLog = null;
        lastKeysLog = null;
        debugLogs.addFirst(threadName + " stopped");
    }

    private LogHandler createLogHandler(String appName) {
        if ("EPG".equals(appName)) {
            return new EpgLogHandler(this);
        } else if ("PVR".equals(appName)) {
            return new PvrLogHandler(this);
        } else if ("VOD".equals(appName)) {
            return new VodLogHandler(this);
        } else {
            return null;
        }
    }

    public void setCurrentVideo(final String currentVideo) {
        Thread t = new Thread("LogSession.setCurrentVideo") {
            public void run() {
                synchronized (LogSession.this) {
                    if (Log.INFO_ON) {
                        Log.printInfo("LogSession.setCurrentVideo = " + currentVideo);
                    }
                    if (currentVideoLogHandler != null) {
                        currentVideoLogHandler.enableLogStateListener(false);
                        currentVideoLogHandler = null;
                    }
                    LogHandler handler = (LogHandler) logHandlerTable.get(currentVideo);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("LogSession: LogHandler for new videoContext = " + handler);
                    }
                    if (handler != null) {
                        handler.enableLogStateListener(true);
                        currentVideoLogHandler = handler;
                    }
                }
            }
        };
        t.start();
    }

    public String getTimestamp() {
        return new SimpleDateFormat(DATE_FORMAT).format(new Date());
    }


    public void send(String str) {
        if (str == null || str.length() == 0) {
            return;
        }
        try {
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.getOutputStream();

            OutputStream os = con.getOutputStream();
            os.write(str.getBytes());
            os.flush();
            os.close();

            int code = con.getResponseCode();
            String msg = con.getResponseMessage();

            //JIRA(VDTRMASTER-5286) : 정준이 수정했던 것을 R4.1 에 merge 함.
            if (Log.DEBUG_ON) {
                Log.printDebug("response = " + code + ", " + msg);
            }
            con.disconnect();

            if (STOP_LOGGING_RESULT.equalsIgnoreCase(msg)) {
                LogController.getInstance().stop();
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // PowerModeChangeListener
    ///////////////////////////////////////////////////////////////////////

    public void powerModeChanged(int newPowerMode) {
        uploader.push(new PowerModeChangedEvent());
    }


    ///////////////////////////////////////////////////////////////////////
    // Log Queue APIs
    ///////////////////////////////////////////////////////////////////////

    public void pushAppStateLog(Hashtable logs) {
        uploader.push(new AppStateChangedEvent(logs));
    }

    public void pushNoVideoLog() {
        if (stbEnabled) {
            uploader.push(new NoVideoStateEvent());
        }
    }

    public void pushPausedLog(boolean paused) {
        if (keysEnabled) {
            uploader.push(new PausedEvent(paused));
        }
    }

    public void pushSendEvent() {
        uploader.push(new SendEvent());
    }

    ///////////////////////////////////////////////////////////////////////
    // Log Event classes
    ///////////////////////////////////////////////////////////////////////

    abstract class LogEvent implements Runnable {

        protected String appName;

        protected abstract void prepareLog();

        public final void run() {
            prepareLog();

            long dwellTime = 0L;
            if (firstSent && appName != null) {
                AppLogSetting as = (AppLogSetting) appTable.get(appName.toUpperCase());
                if (as != null) {
                    dwellTime = as.dwellTime;
                }
            }

            if (dwellTime > 0) {
                dwellTimer.start(appName, dwellTime);
            } else {
                dwellTimer.stop();
                pushSendEvent();
            }
            firstSent = true;
        }
    }

    class AppStateChangedEvent extends LogEvent {
        private StateLog appLog;

        public AppStateChangedEvent(Hashtable logs) {
            if (logs != null) {
                this.appName = (String) logs.get(LogHandler.KEY_STATE);
                this.appLog = new StateLog("log", logs);
            } else {
                this.appName = "stb";
            }
        }

        public void prepareLog() {
            if (appLog == null) {
                if (stbEnabled) {
                    lastStbLog = StbLogHandler.getLog(false);
                }
                if (keysEnabled) {
                    lastKeysLog = KeysLogHandler.getLog(false);
                }
            } else {
                if (stbEnabled) {
                    lastStbLog = StbLogHandler.getLog(true);
                }
//                if (keysEnabled) {
//                    lastKeysLog = KeysLogHandler.getLog(false);
//                }
            }
            lastAppLog = appLog;
        }
    }


    class NoVideoStateEvent extends AppStateChangedEvent {
        public NoVideoStateEvent() {
            super(null);
        }
    }


    class PausedEvent extends LogEvent {
        boolean paused;
        public PausedEvent(boolean paused) {
            this.paused = paused;
            appName = "keys";
        }

        public void prepareLog() {
            lastKeysLog = KeysLogHandler.getLog(paused);    // paused
        }

    }

    class PowerModeChangedEvent extends LogEvent {

        public PowerModeChangedEvent() {
            appName = "stb";
        }

        public void prepareLog() {
            StateLog stbLog;
            if (lastAppLog != null) {
                stbLog = StbLogHandler.getLog(true);   // has video
            } else {
                stbLog = StbLogHandler.getLog(false);   // no video
            }
            lastStbLog = stbLog;
        }
    }


    class SendEvent implements Runnable {

        public void run() {
            StateLog stbLog = stbEnabled ? lastStbLog : null;
            StateLog keysLog = keysEnabled ? lastKeysLog : null;
            StateLog appLog = lastAppLog;

            Vector states = new Vector();

            if (stbLog != null) {
                states.addElement(stbLog);
            }
            if (appLog != null && (stbLog == null || stbLog.get(StbLogHandler.KEY_NO_VIDEO).equals(Boolean.FALSE))) {
                // with app
                states.addElement(appLog);
            } else {
                lastAppLog = null;
            }
            if (keysLog != null) {
                states.addElement(keysLog);
            }

            if (states.equals(lastSentStateLogs)) {
                Log.printInfo("LogSession: skipped to send same log states");
                return;
            }
            lastSentStateLogs = states;

            Hashtable table = new Hashtable();
            table.put(KEY_HOSTMAC, hostMac);
            table.put(KEY_RSKEY, rskey);
            table.put(KEY_TIMESTAMP, getTimestamp());
            table.put(KEY_LOG_TYPE, "full");

            Enumeration en = states.elements();
            while (en.hasMoreElements()) {
                StateLog sl = (StateLog) en.nextElement();
                table.put(sl.stateName, sl.table);
            }

            String str = JSONEncoder.encode(table);
            send(str);
            Log.printDebug(str);
            debugLogs.addFirst(str);
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Dwell time
    ///////////////////////////////////////////////////////////////////////

    class DwellTimer implements TVTimerWentOffListener {

        TVTimer timer = TVTimer.getTimer();
        TVTimerSpec spec = new TVTimerSpec();

        public DwellTimer() {
            spec.addTVTimerWentOffListener(this);
            spec.setRepeat(false);
        }

        public synchronized void start(String appName, long dwellTime) {
            Log.printInfo("DwellTimer.start = " + appName + ", " + dwellTime);
            startTimerSpec(dwellTime);
        }

        private synchronized void startTimerSpec(long dwellTime) {
            timer.deschedule(spec);
            spec.setDelayTime(dwellTime);
            try {
                spec = timer.scheduleTimerSpec(spec);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }

        public synchronized void stop() {
            timer.deschedule(spec);
        }

        public void dispose() {
            stop();
            spec.removeTVTimerWentOffListener(this);
        }

        public synchronized void timerWentOff(TVTimerWentOffEvent e) {
            Log.printInfo("DwellTimer.timerWentOff");
            pushSendEvent();
        }
    }


}


/** Application name and dwell time. */
class AppLogSetting {
    /** Stored to upper case. */
    public final String appName;
    public final long dwellTime;

    public AppLogSetting(String string) throws IllegalArgumentException, IllegalStateException {
        int pos = string.indexOf('!');
        if (pos < 0) {
            this.appName = string.toUpperCase();
            this.dwellTime = 0L;
        } else if (pos == 0) {
            throw new IllegalStateException("missing app name = " + string);
        } else if (pos == string.length() - 1) {
            throw new IllegalArgumentException("missing dwell time = " + string);
        } else {
            try {
                this.appName = string.substring(0, pos).toUpperCase();
                this.dwellTime = Integer.parseInt(string.substring(pos + 1)) * Constants.MS_PER_SECOND;
            } catch (Exception ex) {
                Log.print(ex);
                throw new IllegalArgumentException("parse error = " + ex);
            }
        }
        Log.printInfo("AppLogSetting : " + appName + ", " + dwellTime);
    }
}


package com.videotron.tvi.illico.companion.log;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;

public class EpgLogHandler extends LogHandler {

    public EpgLogHandler(LogSession session) {
        super(session);
        stateName = LOG_STATE_EPG;
    }

    public void enableLogStateListener(boolean enabled) {
        EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        try {
            if (enabled) {
                es.setLogStateListener(this);
            } else {
                es.setLogStateListener(null);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

}

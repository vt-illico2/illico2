package com.videotron.tvi.illico.companion.log;

import org.ocap.hardware.Host;
import java.util.Hashtable;

public class StbLogHandler extends LogHandler {
    // boolean
    public static final String KEY_POWER_ON = "power_on";
    // boolean
    public static final String KEY_NO_VIDEO = "no_video";

    public StbLogHandler(LogSession session) {
        super(session);
        stateName = LOG_STATE_STB;
    }

    public void enableLogStateListener(boolean enabled) {
        // Does nothing. always enabled.
    }

    public static StateLog getLog(boolean hasVideo) {
        StateLog table = new StateLog(LOG_STATE_STB);
//        table.put(KEY_LOG_STATE, LOG_STATE_STB);
        boolean powerOn = Host.getInstance().getPowerMode() == Host.FULL_POWER;
        table.put(KEY_POWER_ON, Boolean.valueOf(powerOn));
        table.put(KEY_NO_VIDEO, Boolean.valueOf(!powerOn || !hasVideo));
        return table;
    }

}

package com.videotron.tvi.illico.companion.log;

import java.net.URL;
import java.util.Hashtable;
import java.util.HashSet;
import java.util.Enumeration;
import java.net.MalformedURLException;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.companion.*;

public class LogController {

    private static LogController instance = new LogController();

    public static LogController getInstance() {
        return instance;
    }

    private LogSession session;
    private String videoContext;

    private HashSet validStates = new HashSet();

    private LogController() {
        validStates.add("EPG");
        validStates.add("PVR");
        validStates.add("VOD");
        validStates.add("STB");
        validStates.add("KEYS");
    }

    public boolean isValidState(String state) {
        return validStates.contains(state);
    }

    public synchronized void reset(String server, String proxy, int port, String rskey, String logState) throws MalformedURLException {
        Log.printInfo("LogController.reset : " + server + ", " + proxy + ", " + port + ", " + rskey + ", " + logState);
        stop();
        // server
        LogSession newSession = new LogSession(server, proxy, port, rskey, logState);
        this.session = newSession;
        session.start(getCurrentVideoApplication());
    }

    public synchronized void stop() {
        if (session != null) {
            session.dispose();
        }
        session = null;
    }

    public void videoModeChanged(String videoName) {
        String oldContext = videoContext;
        videoContext = videoName;
        synchronized (this) {
            if (session != null) {
                session.setCurrentVideo(getCurrentVideoApplication());
            }
        }
    }

    private String getCurrentVideoApplication() {
        String currentContext = videoContext;
        if (currentContext == null) {
            return "EPG";
        } else {
            return currentContext;
        }
    }

}

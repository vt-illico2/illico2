package com.videotron.tvi.illico.companion.log;

import java.util.Hashtable;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;

public abstract class LogHandler implements LogStateListener {

    protected String stateName;
    protected LogSession session;

    public LogHandler(LogSession logSession) {
        this.session = logSession;
    }

    public abstract void enableLogStateListener(boolean enabled);

    public void dispose() {
        enableLogStateListener(false);
    }

    /**
     * Invoked when the logs are sent from application.
     * @param logs logs from application.
     */
    public void logStateChanged(Hashtable logs) {
        session.pushAppStateLog(logs);
    }

    /**
     * Invoked when content is paused or content's rate changed from paused state.
     * @param paused truen when the content is paused.
     */
    public void playbackPaused(boolean paused) {
        session.pushPausedLog(paused);
//        LogController.getInstance().sendLog(KeysLogHandler.getLog(paused));
    }

    /**
     * Invoked when content is stopped and goes to "no_video" state.
     */
    public void playbackStopped() {
        session.pushNoVideoLog();
//        LogController.getInstance().sendLog(StbLogHandler.getLog(false));
    }

}

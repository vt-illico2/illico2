package com.videotron.tvi.illico.companion.log;

import java.util.Hashtable;

public class KeysLogHandler extends LogHandler {
    // boolean
    public static final String KEY_PAUSED = "paused";

    private static StateLog LOG_PAUSED = new StateLog("keys");
    private static StateLog LOG_NOT_PAUSED = new StateLog("keys");

    static {
//        LOG_PAUSED.put(KEY_LOG_STATE, LOG_STATE_KEYS);
        LOG_PAUSED.put(KEY_PAUSED, Boolean.TRUE);

//        LOG_NOT_PAUSED.put(KEY_LOG_STATE, LOG_STATE_KEYS);
        LOG_NOT_PAUSED.put(KEY_PAUSED, Boolean.FALSE);
    }

    public KeysLogHandler(LogSession session) {
        super(session);
        stateName = LOG_STATE_KEYS;
    }

    public void enableLogStateListener(boolean enabled) {
        // Does nothing. always enabled.
    }

    public static StateLog getLog(boolean paused) {
        return paused ? LOG_PAUSED : LOG_NOT_PAUSED;
    }

}

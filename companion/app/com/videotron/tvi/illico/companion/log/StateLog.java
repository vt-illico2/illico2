package com.videotron.tvi.illico.companion.log;

import java.util.Hashtable;

public class StateLog {

    String stateName;
    Hashtable table;

    public StateLog(String stateName, Hashtable table) {
        this.stateName = stateName;
        this.table = table;
    }

    public StateLog(String stateName) {
        this(stateName, new Hashtable());
    }

    public Object put(String key, Object value) {
        return table.put(key, value);
    }

    public Object get(String key) {
        return table.get(key);
    }

    public boolean equals(Object o) {
        if (o instanceof StateLog) {
            StateLog s = (StateLog) o;
            if (!stateName.equals(s.stateName)) {
                return false;
            }
            return table.equals(s.table);
        }
        return false;
    }

}

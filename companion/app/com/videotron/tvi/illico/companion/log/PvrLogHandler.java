package com.videotron.tvi.illico.companion.log;

import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;

public class PvrLogHandler extends LogHandler {

    public PvrLogHandler(LogSession session) {
        super(session);
        stateName = LOG_STATE_PVR;
    }

    public void enableLogStateListener(boolean enabled) {
        PvrService ps = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
        try {
            if (enabled) {
                ps.setLogStateListener(this);
            } else {
                ps.setLogStateListener(null);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

}

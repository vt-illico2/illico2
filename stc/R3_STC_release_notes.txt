This is STC App Release Notes.
GENERAL RELEASE INFORMATION
- Release Details
	- Release name
  	  : version 1.4.0.0
	- Release App file Name
  	  : stc.zip
	- Release type (official, debug, test,...)
  	  : debug
	- Release date
	  : 2014.06.17

***
version  1.4.0.0 - 2014.06.17
    - New Feature
        + update a logic to lookup.
        + start a LogUploader thread after received a server information.
    - Resolved Issues
        N/A
***
version  1.0.11.1 - 2014.04.23
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4972 Applications frozen
***
version  1.0.10.0 - 2014.01.16
    - New Feature
        N/A
    - Resolved Issues
        + corrected wrong key in LogJobQueue.
***
version  1.0.9.0
    - New Feature
        + change logUploader and logTrigger to be a single thread with queue.
    - Resolved Issues
        N/A
***
version  1.0.8.0
    - New Feature
	+  added TimerWrapper class
    - Resolved Issues
***
version  1.0.7.1
    - New Feature
	+  N/A
    - Resolved Issues
	+ VDTRMASTER-4547	Debug Level of Applications doesn't respect last expiration date & time
***
version  1.0.6.0
    - New Feature
	   + change getMacAddress not to use recursive call
    - Resolved Issues
	   + N/A
***
version  1.0.5.0
    - New Feature
	+ changed sequence to get MAC after initXlet
    - Resolved Issues
	+ N/A

***
version  1.0.4.1
    - New Feature
	+ N/A
    - Resolved Issues
	+ VDTRSUPPORT-74     [regression][STC] The setLogLevel service does not work accordingly to specification
***
version  1.0.3.0
    - New Feature
	+ advanced method to read MAC in Misc.getMacAddress
    - Resolved Issues

***
version  1.0.2.0
    - New Feature
	added upload.retry.count and upload.retry.count
	added feature when buffer size is 1, do not write failed log into buffer.
    - Resolved Issues


***
version  1.0.1.1
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3761) Standalone function does not work

***
version  1.0.0.0
    - New Feature

    - Resolved Issues
***
version  0.2.19.1
    - New Feature
	+ N/A
    - Resolved Issues
	+ VDTRSUPPORT-74     [regression][STC] The setLogLevel service does not work accordingly to specification
***
version  0.2.18.0
    - New Feature
	+ advanced method to read MAC in Misc.getMacAddress
    - Resolved Issues


***
version  0.2.17.1
    - New Feature

    - Resolved Issues
	+ (VDTRSUPPORT-3) The STC syslogs are not sent instantaneously even when buffer size is set to 1.
***
version  0.2.16.1
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3761) Standalone function does not work

***
version  0.2.15.1
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3761) Standalone function does not work

***
version  0.2.14.1
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3743) Incorrect answer format for getLogLevel when changing STC log level, again.  

***
version  0.2.14.1
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3743) Incorrect answer format for getLogLevel when changing STC log level, again.


***
version  0.2.13.1
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3554) Set default log level to a value in application properties. Corrected module to process expiration date again. Initialize configuration stored in flash.

***
version  0.2.12.8
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3554) Set default log level to a value in application properties. Corrected module to process expiration date again.

***
version  0.2.12.7
    - New Feature

    - Resolved Issues
	+ (VDTRMASTER-3554) Set default log level to a value in application properties. Corrected module to process expiration date.

***
version  0.2.12.6
    - New Feature
	+ (VDTRMASTER-3554) Set default log level to a value in application properties.
    - Resolved Issues


 ***
version  0.2.12.5
    - New Feature

    - Resolved Issues
    	+ (VDTRMASTER-2759) Syslog output format is different with CISCO and SAMSUNG STBs
	+ (VDTRMASTER-2760) SYSLOG - Invalid timestamp in the SYSLOG messages


 ***
version  0.2.12.4
    - New Feature
    - Resolved Issues
    	+ VDTRMASTER-2237



 ***
version  0.2.12.3
    - New Feature
	+ applied changes in framework library

    - Resolved Issues



 ***
version  0.2.12.2
    - New Feature
	+ applied changes in framework library

    - Resolved Issues


 ***
version  0.2.12.1
    - New Feature
	+ added checker for input parameter

    - Resolved Issues


 ***
version  0.2.11
    - New Feature
	+ added checker for input parameter

    - Resolved Issues

 ***
version  0.2.10
    - New Feature

    - Resolved Issues
    	+ VDTRMASTER-1599  Logs are still activated after expiration date

 ***
version  0.2.9
    - New Feature

    - Resolved Issues
    	+ VDTRMASTER-1304  When a /stc/setLogLevel sets a log for an application that does not exist, false application name is added to getLogLevel list
    	+ VDTRMASTER-1178  http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0
 ***
version  0.2.8
    - New Feature

    - Resolved Issues
    	+ VDTRMASTER-1248  The expire parameter does not work 
 ***
version  0.2.7
    - New Feature

    - Resolved Issues
    	+ VDTRMASTER-1178 http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0
          --> changed return duration
 ***
version  0.2.6
    - New Feature
   - Resolved Issues
    	+ VDTRMASTER-1178 http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0
          --> changed return duration
 
 ***
version  0.2.5
    - New Feature
    - Resolved Issues
    	+ VDTRMASTER-1178  http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0 : added debug code, not resolved yet

 ***
version  0.2.4 
     - New Feature
    	N/A
     - Resolved Issues
        + VDTRMASTER-1164  STC is not sending log to syslog server configured in management system
***








- Release Details

- New Feature
	+  added TimerWrapper class
- Resolved Issues

- Release name
  : version 1.0.9.0_R3
- Release App file Name
  : stc.r3.v.1.0.9.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.08.25

------------------------------------------------------------------------------



- Release Details

- New Feature
	+  added TimerWrapper class
- Resolved Issues

- Release name
  : version 1.0.8.0_R3
- Release App file Name
  : stc.r3.v.1.0.8.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.04.25

------------------------------------------------------------------------------




- Release Details

- New Feature

- Resolved Issues
	+ VDTRMASTER-4547	Debug Level of Applications doesn't respect last expiration date & time
- Release name
  : version 1.0.7.1_R3
- Release App file Name
  : stc.r3.v.1.0.7.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.06.15

------------------------------------------------------------------------------


- Release Details

- New Feature
	+ changed sequence to get MAC after initXlet
- Resolved Issues

- Release name
  : version 1.0.6.0_R3
- Release App file Name
  : stc.r3.v.1.0.6.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.06.15

------------------------------------------------------------------------------


- Release Details

- New Feature
	+ changed sequence to get MAC after initXlet
- Resolved Issues

- Release name
  : version 1.0.5.0_R3
- Release App file Name
  : stc.r3.v.1.0.5.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.05.15

------------------------------------------------------------------------------

- Release Details

- New Feature
	VDTRSUPPORT-74     [regression][STC] The setLogLevel service does not work accordingly to specification
- Resolved Issues

- Release name
  : version 1.0.4.1_R3
- Release App file Name
  : stc.r3.v.1.0.4.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.05.05

------------------------------------------------------------------------------

- Release Details

- New Feature
	added upload.retry.count and upload.retry.count
	added feature when buffer size is 1, do not write failed log into buffer.
- Resolved Issues

- Release name
  : version 1.0.3.0_R3
- Release App file Name
  : stc.r3.v.1.0.3.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.04.10

------------------------------------------------------------------------------


- Release Details

- New Feature
	added upload.retry.count and upload.retry.count
	added feature when buffer size is 1, do not write failed log into buffer.
- Resolved Issues

- Release name
  : version 1.0.2.0_R3
- Release App file Name
  : stc.r3.v.1.0.2.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.03.4

------------------------------------------------------------------------------

- Release Details

- New Feature

- Resolved Issues
	+ (VDTRMASTER-3743) Incorrect answer format for getLogLevel when changing STC log level

- Release name
  : version 0.2.15.1
- Release App file Name
  : stc.v.0.2.15.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.02.14

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+ (VDTRMASTER-3743) Incorrect answer format for getLogLevel when changing STC log level

- Release name
  : version 0.2.14.1
- Release App file Name
  : stc.v.0.2.14.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.02.14

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+ (VDTRMASTER-3554) Set default log level to a value in application properties. Corrected module to process expiration date again.

- Release name
  : version 0.2.13.1
- Release App file Name
  : stc.v.0.2.13.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.02.14

------------------------------------------------------------------------------

- Release Details

- New Feature

- Resolved Issues
	+ (VDTRMASTER-3554) Set default log level to a value in application properties. Corrected module to process expiration date again.

- Release name
  : version 0.2.12.8
- Release App file Name
  : stc.v.0.2.12.8.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.02.14

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+ (VDTRMASTER-3554) Set default log level to a value in application properties. Corrected module to process expiration date.

- Release name
  : version 0.2.12.7
- Release App file Name
  : stc.v.0.2.12.7.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.02.05

------------------------------------------------------------------------------


- Release Details

- New Feature
	+ (VDTRMASTER-3554) Set default log level to a value in application properties.
- Resolved Issues


- Release name
  : version 0.2.12.6
- Release App file Name
  : stc.v.0.2.12.6.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.02.03

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
 	+ (VDTRMASTER-2759) Syslog output format is different with CISCO and SAMSUNG STBs
	+ (VDTRMASTER-2760) SYSLOG - Invalid timestamp in the SYSLOG messages

- Release name
  : version 0.2.12.5
- Release App file Name
  : stc.v.0.2.12.15.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.08.10

------------------------------------------------------------------------------


- Release Details


- New Feature

- Resolved Issues
	+ VDTRMASTER-2237  interface with syslog (set local address to UDP port)

- Release name
  : version 0.2.12.4
- Release App file Name
  : stc.v.0.2.12.4.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.08.10

------------------------------------------------------------------------------



- Release Details


- New Feature
     + applied changes of framework

- Resolved Issues
	+ VDTRMASTER-2194  [STC] Certain log levels cannot be set on Cisco and Samsung STBs (both PVR and non-PVR)

- Release name
  : version 0.2.12.3
- Release App file Name
  : stc.v.0.2.12.3.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.08.07

------------------------------------------------------------------------------




- Release Details

- New Feature
     + applied changes of framework

- Resolved Issues

- Release name
  : version 0.2.12
- Release App file Name
  : stc.v.0.2.12.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.06.30

------------------------------------------------------------------------------



- Release Details

- New Feature

- Resolved Issues
     + VDTRMASTER-1599  Logs are still activated after expiration date

- Release name
  : version 0.2.11
- Release App file Name
  : stc.v.0.2.11.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.05.26

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
     + VDTRMASTER-1599  Logs are still activated after expiration date

- Release name
  : version 0.2.10
- Release App file Name
  : stc.v.0.2.10.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.05.19

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
     + VDTRMASTER-1304  When a /stc/setLogLevel sets a log for an application that does not exist, false application name is added to getLogLevel list
     + VDTRMASTER-1178  http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0
- Release name
  : version 0.2.9
- Release App file Name
  : stc.v.0.2.9.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.04.20

------------------------------------------------------------------------------


- Release Details
  : new features
        + VDTRMASTER-1178  http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0 : added debug code, not resolved yet
  : fixed bugs

- Release name
  : version 0.2.8
- Release App file Name
  : stc.v.0.2.8.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.04.15
------------------------------------------------------------------------------



- Release Details
  : new features

  : fixed bugs
        + VDTRMASTER-1164  STC is not sending log to syslog server configured in management system

- Release name
  : version 0.2.7
- Release App file Name
  : stc.v.0.2.7.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.04.06
------------------------------------------------------------------------------




- Release Details
  : new features

  : fixed bugs
        + VDTRMASTER-1178 http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0
          --> changed return duration, added debug code, not resolved yet
- Release name
  : version 0.2.6
- Release App file Name
  : stc.v.0.2.5.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.04.02
------------------------------------------------------------------------------


- Release Details
  : new features

  : fixed bugs
    	+ VDTRMASTER-1178  http request /stc/getLogLevel returns incorrect response for applications that have log level different than 0
- Release name
  : version 0.2.5
- Release App file Name
  : stc.v.0.2.5.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.03.15
------------------------------------------------------------------------------



- Release Details
  : new features

  : fixed bugs
        + VDTRMASTER-1164  STC is not sending log to syslog server configured in management system

- Release name
  : version 0.2.4
- Release App file Name
  : stc.v.0.2.4.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.03.11
------------------------------------------------------------------------------



- Release Details
  : new features

  : fixed bugs
        + VDTRMASTER-1164  STC is not sending log to syslog server configured in management system

- Release name
  : version 0.2.3
- Release App file Name
  : stc.v.0.2.3.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.03.28
------------------------------------------------------------------------------


- Release Details
  : new features
        + changed to use Inband OC
  : fixed bugs

- Release name
  : version 0.2.2
- Release App file Name
  : stc.v.0.2.2.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.03.11
------------------------------------------------------------------------------

- Release Details
  : new features
        + set default log level into off in debug
	+ change return code of setLogLevel when there is no app registered
  : fixed bugs
        + VDTRMASTER-1088  Daemon getLogLevel request to Support Application returns empty xml
- Release name
  : version 0.2.1
- Release App file Name
  : stc.v.0.2.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.03.10
------------------------------------------------------------------------------


- Release Details
  : new features
        + appliied new framework library
	+ update notification center ixc interface
  : fixed bugs
- Release name
  : version 0.2.0
- Release App file Name
  : stc.v.0.2.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2011.03.09
------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+ STC initial version for R3

- Release name
  : version 1.0.0.0
- Release App file Name
  : stc.v.1.0.0.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.02.14

------------------------------------------------------------------------------


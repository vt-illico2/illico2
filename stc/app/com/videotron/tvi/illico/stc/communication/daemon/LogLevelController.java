package com.videotron.tvi.illico.stc.communication.daemon;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.config.ApplicationConfig;
import com.videotron.tvi.illico.stc.controller.DaemonController;
import com.videotron.tvi.illico.stc.util.StcCode;
import com.videotron.tvi.illico.stc.util.StcInfo;
import com.videotron.tvi.illico.stc.util.StringUtils;
import com.videotron.tvi.illico.stc.util.StcDuration;


/**
 * method called by LogLevelController.
 * @author thorn
 *
 */
public class LogLevelController {
	/** application parameter name. optional parameter. */
	private String application = "application";
	/** level parameter name. mandatory parameter. */
	private String level = "level";
	/** expires parameter name. optional parameter. */
	private String expires = "expires";
	/** buffer parameter name. optional parameter. */
	private String buffer = "buffer";
	/** notify parameter name. optional parameter. */
	private String notify = "notify";

	/**
	 * @param request - the request from daemon
	 * @param response - the response to daemon
	 */
	public void getLogLevel(DaemonRequest request, DaemonResponse response) {

		if (Log.DEBUG_ON) {
			Log.printDebug("called getLogLevel");
		}

		ApplicationConfig[] config = null;
		try {
			config = new DaemonController().getLogConfig(request.getParameter(application));
		} catch (Exception e) {
			response.setResultCode(DaemonRequestDispatcher.STC009);
			return;
		}

		if (config == null) {
			response.setResultCode(DaemonRequestDispatcher.STC000);
			return;
		}

		ArrayList list = new ArrayList();
		for (int i = 0, size = config.length; i < size; i++) {
			list.add(config[i]);
		}
		response.setContents(list);
		response.setResultCode(DaemonRequestDispatcher.STC000);
	}

	public boolean checkValidRequest(DaemonRequest request){

		if (Log.DEBUG_ON) {
			Log.printDebug("check validation of request parameter");
		}

		boolean valid = true;

		Vector keys = request.getParameterKeys();
        int j = 0;
        int checkSize = StcCode.KEYS.length;
        int keySize = keys.size();

        for(int i=0; i<keySize; i++){
			for(j=0; j<checkSize; j++){
				if(((String)keys.get(i)).equals(StcCode.KEYS[j])){
					break;
				}
			}
			// if j is equals to checkSize, there is no KEYS equal to input param
			// thus it should be return STC002
			if(j == checkSize) {
				valid = false;
			}
		}

		return valid;
	}


	/**
	 * @param request - the request from daemon
	 * @param response - the response to daemon
	 */
	public void setLogLevel(DaemonRequest request, DaemonResponse response) {

		if (Log.DEBUG_ON) {
			Log.printDebug("called setLogLevel");
		}

		if(!checkValidRequest(request)){
			response.setResultCode(DaemonRequestDispatcher.STC002);
			return;
		}

		String appName = request.getParameter(application);
		int logLevel = -1;
		int bufferSize = -1;
		String expDuration = request.getParameter(expires);
		boolean notifyFlag = false;


		// check initialization and validation

		try {
			logLevel = Integer.parseInt(request.getParameter(level));

			if (StringUtils.hasText(request.getParameter(buffer))) {
				bufferSize = Integer.parseInt(request.getParameter(buffer));
			}

			if (StringUtils.hasText(request.getParameter(notify))) {
				// check whether notify parameter is valid
				if (!((StcCode.LOG_TRUE.equals(request.getParameter(notify).toUpperCase())) ||
					(StcCode.LOG_FALSE.equals(request.getParameter(notify).toUpperCase())))) {
						response.setResultCode(DaemonRequestDispatcher.STC003);
						return;
					}
				notifyFlag = Boolean.valueOf(request.getParameter(notify)).booleanValue();
			}
		} catch (Exception e) {
			response.setResultCode(DaemonRequestDispatcher.STC003);
			return;
		}


		/*
		// check logLevel valid
		if (!((logLevel == StcCode.LOG_DEBUG) ||  (logLevel == StcCode.LOG_INFO)
				|| (logLevel == StcCode.LOG_WARNING) || (logLevel == StcCode.LOG_ERROR)
				|| (logLevel == StcCode.LOG_OFF))) {
			response.setResultCode(DaemonRequestDispatcher.STC003);
			return;
		}
		*/

		if((logLevel > StcCode.LOG_DEBUG) || (logLevel < StcCode.LOG_OFF)) {
			response.setResultCode(DaemonRequestDispatcher.STC003);
			return;
		}

		// changed spec by VT, 2011.08.07
		// set log level as requested.
		/*
		// log level 1 and 2 are note defined. it is cascaded into 0, off.
		if((logLevel == StcCode.LOG_1) || (logLevel == StcCode.LOG_2)){
			logLevel = StcCode.LOG_OFF;
		}
		// log level 5 is not defined. it is cascaded into 4, warning
		if(logLevel == StcCode.LOG_5) {
			logLevel = StcCode.LOG_WARNING;
		}

		*/
		// check expiration duration valid
		// it can be optional, that is it can be null
		if (expDuration != null) {

			if (!new StcDuration().checkDateValidity(expDuration)) {
				response.setResultCode(DaemonRequestDispatcher.STC003);
				return;
			}

			SimpleDateFormat formatter = new SimpleDateFormat(StcInfo.DATE_FORMAT);

			// evaluate foramt of exp duration. for example, hour 4 into 04.
			try{
				Date exp = formatter.parse(expDuration);
				expDuration = formatter.format(exp);
			} catch(Exception e){
				if(Log.DEBUG_ON) {
					Log.printDebug("[STC] format of expiration date is wrong. so return false");
				}

				response.setResultCode(DaemonRequestDispatcher.STC003);
				return;
			}

			String now = formatter.format(new Date());
			if (now.compareTo(expDuration) > 0) {
				if(Log.DEBUG_ON) {
					Log.printDebug("[STC] expiration date is passed. so return false");
				}

				response.setResultCode(DaemonRequestDispatcher.STC003);
				return;
			}
		}


		// check buffer size valid
		if (bufferSize < -1) {
			response.setResultCode(DaemonRequestDispatcher.STC003);
			return;
		}


		ApplicationConfig config = new ApplicationConfig();
		config.setName(appName);
		config.setBufferSize(bufferSize);

		// if expiration is NEVER, then do not expire this task

		if((logLevel == 0) && ( expDuration == null)){
			if(Log.DEBUG_ON) {
				Log.printDebug("[STC] log level is 0 and expiration is null, so expiration is set to NEVER");
			}
			expDuration = StcCode.EXP_NEVER;
		}

		config.setExpires(expDuration);

		config.setLogLevel(logLevel);
		config.setNotify(notifyFlag);
		if(Log.DEBUG_ON) {
			Log.printDebug("[appName] " + appName
					+ "[bufferSize]" + bufferSize
					+ "[expDuration]" + expDuration
					+ "[logLevel]" + logLevel
					+ "[notifyFlag]" + notifyFlag);
		}

		try {
			//set
			int ret = new DaemonController().setLogConfig(config);
			if (Log.DEBUG_ON) {
				Log
				.printDebug("changed log level with return " + ret);
			}
			response.setResultCode(ret);
		} catch (Exception e) {
			if (Log.DEBUG_ON) {
				Log
				.printDebug("Exception occurred when changing log level");
			}
			response.setResultCode(DaemonRequestDispatcher.STC007);
			return;
		}
	}
}

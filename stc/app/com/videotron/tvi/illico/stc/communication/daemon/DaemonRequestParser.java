package com.videotron.tvi.illico.stc.communication.daemon;

import java.io.IOException;

import com.videotron.tvi.illico.stc.util.StringUtils;
import com.videotron.tvi.illico.util.TextUtil;
/**
 * @author thorn
 */
public final class DaemonRequestParser {
	
	/** delimiter of name&value pairs. */
	public static final String NAMEVALUE_PAIR_DELIM = "&";
	/** delimiter of name and value. */
	public static final String PARAMETER_DELIM = "=";
	
	/**
	 * Constructs a instance.
	 */
	private DaemonRequestParser() {
	}
	
	/**
	 * 
	 * @param aInput - a string to parse
	 * @return DaemonRequest object
	 */
	protected static DaemonRequest parseParameters(final String aInput) {
		DaemonRequest request = new DaemonRequest();
		if (!StringUtils.hasText(aInput)) {
			return request;
		}
		
		String[] namevaluePairs = TextUtil.tokenize(aInput, NAMEVALUE_PAIR_DELIM);
		String[] namevalue;
		for (int i = 0, size = namevaluePairs.length; i < size; i++) {
			namevalue = TextUtil.tokenize(namevaluePairs[i], PARAMETER_DELIM);
			request.setParameter(namevalue[0], namevalue[1]);
		}
		return request;
	}
	
	/**
	 * @param aInput - a string to parse
	 * @return DaemonRequest object
	 * @throws IOException - I/O error occurs.
	 */
	public static DaemonRequest parse(final String aInput) throws IOException {
		if (aInput == null) {
            throw new IllegalStateException("input is null.");
        }
		
		return parseParameters(aInput);
	}
}

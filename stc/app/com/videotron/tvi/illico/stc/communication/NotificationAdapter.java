package com.videotron.tvi.illico.stc.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.notification.NotificationOption;

/**
 * The methods in this class return a default value. 
 * This class exists as convenience for creating notificationOption objects. 
 * @author thorn
 *
 */
public class NotificationAdapter implements NotificationOption {
	
	/** the application name. */
	private String name = "STC";
	
	/**
     * Gets the application name.
     * @return the application name
     * @throws RemoteException the remote exception
     */
	public String getApplicationName() throws RemoteException {
		return name;
	}
	
	
	/**
     * Sets the Application name.
     * @param aName the application name
     * @throws RemoteException the remote exception
     */
	public void setApplicationName(String aName) throws RemoteException {
		this.name = aName;
	}
	
	/** the categories. */
	private int categories = 0;
	
	/**
     * get the categories.
     * @return the categories
     * @throws RemoteException the remote exception
     */
	public int getCategories() throws RemoteException {
		return categories;
	}
	
	/**
     * Sets the categories.
     * @param aCategories the new categories
     * @throws RemoteException  the remote exception
     */
	public void setCategories(int aCategories) throws RemoteException {
		this.categories = aCategories;
	}
	
	/** the create data. */
	private long createData = 0;
	
	/**
     * get the create data.
     * @return the createData
     * @throws RemoteException the remote exception
     */
	public long getCreateDate() throws RemoteException {
		return createData;
	}
	
	/**
     * set a one value of create data parameters.
     * @param aCreateData the new CreateData
     * @throws RemoteException  the remote exception
     */
	public void setCreateDate(long aCreateData) throws RemoteException {
		this.createData = aCreateData;
		
	}
	
	/** the display count. */
	private int count = 0;
	
	/**
     * get the display count.
     * @return the createData
     * @throws RemoteException the remote exception
     */
	public int getDisplayCount() throws RemoteException {
		return count;
	}
	
	/**
     * Sets the display count.
     * @param aCount the new CreateData
     * @throws RemoteException  the remote exception
     */
	public void setDisplayCount(int aCount) throws RemoteException {
		this.count = aCount;
	}
	
    /** the display time. */
	private long displayTime = 0;
	
	/**
     * Gets the display time.
     * @return the display time.
     * @throws RemoteException the remote exception
     */
	public long getDisplayTime() throws RemoteException {
		if (displayTime == 0) {
			return System.currentTimeMillis();
		}
		return displayTime;
	}

	
    /**
     * Sets the display time.
     * @param aTime the new display time. Value is a milliseconds. 
     * @throws RemoteException the remote exception
     */
	public void setDisplayTime(long aTime) throws RemoteException {
		this.displayTime = aTime;
	}
	
    /** Gets the message. */
	private String message = "Your settings have been modified.";
	
    /**
     * Gets the message.
     *
     * @return the message
     * @throws RemoteException the remote exception
     */
	public String getMessage() throws RemoteException {
		return message;
	}
	
	/**
     * Sets the message.
     * @param aMessage the new message
     * @throws RemoteException the remote exception
     */
	public void setMessage(String aMessage) throws RemoteException {
		this.message = aMessage;
	}
	
	/** a message ID. */
	private String messageID = null;
	/**
     * get a message ID.
     * @return message.
     * @throws RemoteException the remote exception
     */
	public String getMessageID() throws RemoteException {
		return messageID;
	}
	
	/**
     * set a message ID. 
     * If you want to delete the event, sets the message ID.
     * @param aMessageID message is a message to notification.
     * @throws RemoteException the remote exception
     */
	public void setMessageID(String aMessageID) throws RemoteException {
		this.messageID = aMessageID;
		
	}
	
	/** the notification action. */
	private int action = ACTION_NONE;
	
	/**
     * Gets the notification action.
     * @return the notification action
     * @throws RemoteException the remote exception
     */
	public int getNotificationAction() throws RemoteException {
		return action;
	}
	
	/**
     * Sets the notification action.
     * set a one value of upper action parameter.
     * @param aAction the new notification action
     * @throws RemoteException the remote exception
     */
	public void setNotificationAction(int aAction) throws RemoteException {
		this.action = aAction;
	}
	
	/** the notification action data. */
	private String[] param = null;
	
	/**
     * Gets the notification action data.
     * @return the notification action data
     * @throws RemoteException the remote exception
     */
	public String[] getNotificationActionData() throws RemoteException {
		return param;
	}
	
	/**
     * Sets the notification action data.
     * 0 application name, 1~n data.
     * The parameters of action.
     * @param aParam the new notification action data
     * @throws RemoteException the remote exception
     */
	public void setNotificationActionData(String[] aParam) throws RemoteException {
		this.param = aParam;
	}
	
	/** the notification popup type. */
	private int status = NotificationOption.STC_POPUP;
	
	/**
     * Gets the notification popup type.
     * @return the notification status
     * @throws RemoteException the remote exception
     */
	public int getNotificationPopupType() throws RemoteException {
		return status;
	}
	
	/**
     * Sets the notification popup type.
     * set a one value of upper  popup type parameters
     * @param aStatus the new notification status
     * @throws RemoteException the remote exception
     */
	public void setNotificationPopupType(int aStatus) throws RemoteException {
		this.status = aStatus;
	}
	
	/** the notification type. */
	private int type = 0;
	
	/**
     * Gets the notification type.
     * @return the notification type
     * @throws RemoteException the remote exception
     */
	public int getNotificationType() throws RemoteException {
		return type;
	}
	
	/**
     * set a one value of upper type parameters.
     * @param aType the new notification type
     * @throws RemoteException the remote exception
     */
	public void setNotificationType(int aType) throws RemoteException {
		this.type = aType;
	}
	
	/** the reminder delay. */
	private long delay = 0;
	
	/**
     * Gets the reminder delay.
     * @return the reminder delay. Value is a milliseconds. 
     * @throws RemoteException the remote exception
     */
	public long getRemainderDelay() throws RemoteException {
		return delay;
	}
	
	/**
     * Sets the reminder delay.
     * @param aDelay the new reminder delay. Value is a integer.
     * @throws RemoteException the remote exception
     */
	public void setRemainderDelay(long aDelay) throws RemoteException {
		this.delay = aDelay;
		
	}
	
	/** the reminder time. */
	private int time = 0;
	
	 /**
     * Gets the reminder time.
     * @return the reminder time. count.
     * @throws RemoteException the remote exception
     */
	public int getRemainderTime() throws RemoteException {
		return time;
	}

	/**
     * Sets the reminder time.
     * @param aTime the new reminder time
     * @throws RemoteException the remote exception
     */
	public void setRemainderTime(int aTime) throws RemoteException {
		this.time = aTime;
		
	}
	
	/** view flag. */
	private boolean viewed = false;
	
	/**
	 * Return true if it viewed.
	 * @return view flag.
     * @throws RemoteException the remote exception
	 */
	public boolean isViewed() throws RemoteException {
		return viewed;
	}
	
	/**
	 * Sets the view.
	 * @param isViewed - view flag
     * @throws RemoteException the remote exception
	 */
	public void setViewed(boolean isViewed) throws RemoteException {
		this.viewed = isViewed;
	}

	/**
     * Gets large button Info.
     * @return string multi array.
     * @throws RemoteException the remote exception
     */
	public String[][] getButtonNameAction() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
     * Gets Large Popup Message.
     * @return message.
     * @throws RemoteException the remote exception
     */
	public String getLargePopupMessage() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
     * Gets Large Popup sub Message.
     * @return message.
     * @throws RemoteException the remote exception
     */
	public String getLargePopupSubMessage() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
     * Gets the sub_message.
     *
     * @return the message
     * @throws RemoteException the remote exception
     */
	public String getSubMessage() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
     * Is the countDwon.
     * @return true if count
     * @throws RemoteException the remote exception
     */
	public boolean isCountDown() throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	 /**
     * [0][0] is a button Name.
     * [0][1] is ActionType.
     * [0][2] is application name or channel name, 
     * [0][n] is data. 
     * @param aAction string multi array
     * @throws RemoteException the remote exception
     */
	public void setButtonNameAction(String[][] aAction) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	/**
     * Sets the countDown.
     * @param b boolean
     * @throws RemoteException the remote exception
     */
	public void setCountDown(boolean b) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	 /**
     * Sets Large Popup Message.
     * @param aMessage string.
     * @throws RemoteException the remote exception
     */
	public void setLargePopupMessage(String aMessage) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	/**
     * Sets Large Popup sub Message.
     * @param aMessage string
     * @throws RemoteException - the remote exception
     */
	public void setLargePopupSubMessage(String aMessage) throws RemoteException {
		// TODO Auto-generated method stub
	}

	/**
     * Sets the sub_message.
     * @param aMessage the new message
     * @throws RemoteException the remote exception
     */
	public void setSubMessage(String aMessage) throws RemoteException {
		// TODO Auto-generated method stub
	}
}

package com.videotron.tvi.illico.stc.communication;

/**
 * SysLogServerDataUpdateListener interface.
 * @author sccu
 */
public interface SysLogServerDataUpdateListener {

    /**
     * Invoked when DataUpdateEvent occurs.
     * @param event - a SysLogServerDataUpdateEvent instance.
     */
    void onDataUpdateEvent(SysLogServerDataUpdateEvent event);
}

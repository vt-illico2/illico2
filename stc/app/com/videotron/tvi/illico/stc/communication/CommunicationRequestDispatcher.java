/*
 *  CommunicationRequestDispatcher.java    $Revision: 1.3 $ $Date: 2014/06/17 13:09:37 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.stc.communication;

import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.communication.daemon.DaemonRequestDispatcher;
import com.videotron.tvi.illico.stc.uploader.LogServerConfig;
import com.videotron.tvi.illico.stc.util.concurrent.Scheduler;
import com.videotron.tvi.illico.util.IxcLookupWorker;
/**
 *
 * @author thorn
 *
 */
public class CommunicationRequestDispatcher implements DataUpdateListener {

	private static CommunicationRequestDispatcher instance;

	/** xlet context. */
	private XletContext xletContext;

	/** Scheduler instance. */
	private Scheduler scheduler;
	/** StcServiceImpl instance. */
	private StcServiceImpl stcService;
	/** DaemonService instance. */
	private DaemonService daemonService;
	/** NotificationService instance. */
	private NotificationService notificationService;
	/** MonitorService instance. */
	private MonitorService monitorService;
	/** DaemonRequestDispatcher instance. */
	private DaemonRequestDispatcher daemonRequestDispatcher;

	IxcLookupWorker ixcWorker;

	public static synchronized CommunicationRequestDispatcher getInstance() {
		if (instance == null) {
			instance = new CommunicationRequestDispatcher();
		}

		return instance;
	}

	private CommunicationRequestDispatcher() {

	}

	/**
	 * Sets an XletContext instance.
	 *
	 * @param context
	 *            - an XletContext instance.
	 */
	public synchronized void setXletContext(XletContext context) {
		xletContext = context;
	}

	/**
	 * Initiates an instance.
	 *
	 * @param sch
	 *            - scheduler instance.
	 */
	public synchronized void setScheduler(Scheduler sch) {
		if (sch == null) {
			throw new IllegalArgumentException("Argument should not be null.");
		}
		scheduler = sch;
	}

	/**
	 * start interface of STC.
	 */
	public synchronized void start() {
		if (xletContext == null) {
			throw new IllegalStateException("XletContext should not be null.");
		}

		bind();

        this.ixcWorker = new IxcLookupWorker(xletContext);

        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME, this);
        ixcWorker.lookup("/1/2040/", DaemonService.IXC_NAME, this);
        ixcWorker.lookup("/1/2110/", NotificationService.IXC_NAME, this);

	}

	private void bind() {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationRequestDispatcher: bind()");
		}
		stcService = new StcServiceImpl();
		IxcRegistry.rebind(xletContext, StcService.IXC_NAME, stcService);
		DataCenter.getInstance().put(StcService.IXC_NAME, stcService);
	}

	/**
	 * destroy.
	 */
	public void destroy() {
		if (xletContext == null) {
			throw new IllegalStateException("XletContext should not be null.");
		}
		try {
			IxcRegistry.unbind(xletContext, StcService.IXC_NAME);

			if (daemonService != null) {
				daemonService.removeListener(StcService.IXC_NAME);
			}

		} catch (Exception e) {
            Log.print(e);
		}
	}

	/**
     * listener is registered to notification Center.
     */
	private void addNotificationListener() {
		try {
			notificationService.setRegisterNotificaiton(StcService.IXC_NAME, null);
		} catch (RemoteException e) {
            Log.print(e);
		}
	}


	/**
     * listener is registered to notification Center.
     */
	private void addInbandDataListener() {
		try {
			InbandDataListenerImpl inbandListener = new InbandDataListenerImpl();
			LogServerConfig logServerConfig = LogServerConfig.getInstance();

			if (Log.DEBUG_ON) {
				Log.printDebug("added stc inband data listner to monitor");
			}
			monitorService.addInbandDataListener(inbandListener,
					FrameworkMain.getInstance().getApplicationName());

			if (Log.DEBUG_ON) {
				Log.printDebug("added stc inband data listner to data center");
			}
			DataCenter.getInstance().addDataUpdateListener(logServerConfig.getFileName(), inbandListener);

		} catch (RemoteException e) {
            Log.print(e);
		}
	}

	public void dataRemoved(String key) {
		// TODO Auto-generated method stub

	}

	public void dataUpdated(String key, Object old, Object value) {
		if (Log.DEBUG_ON) {
			Log.printDebug("CommunicationRequestDispatcher: dateUpdated: key=" + key + ", old=" + old + ", value=" + value);
		}

		if (key.equals(MonitorService.IXC_NAME)) {
            monitorService = (MonitorService) value;
			addInbandDataListener();
		} else if (key.equals(DaemonService.IXC_NAME)) {
            daemonService = (DaemonService) value;
			daemonRequestDispatcher = new DaemonRequestDispatcher();
			daemonRequestDispatcher.setScheduler(scheduler);
			daemonRequestDispatcher.init(ApplicationProperties.getClassName());
			try {
				daemonService.addListener(StcService.IXC_NAME, daemonRequestDispatcher);
			} catch (RemoteException e) {
				Log.print(e);
			}
		} else if (key.equals(NotificationService.IXC_NAME)) {
            notificationService = (NotificationService) value;
			addNotificationListener();
		}
	}

}

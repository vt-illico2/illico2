package com.videotron.tvi.illico.stc.communication;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.stc.util.StcCode;

/**
 * @author thorn
 */
public final class ApplicationProperties {
	
	/** notification variable name in application.prop. */
	private static final String KEY_REMOTE_OPERATIONS_NOTIFICATION = "remote.opertaion.notification";
	
	 /** notification variable name in application.prop. */
	private static final String KEY_DEVELOPER_LOG_BUFFER_SIZE = "developer.log.buffer.size";
	
	 /** system line separator. */
	private static final String KEY_LOG_LEVEL_EXPIRATION = "loglevel.default.expiration.duration";
	
	 /** system line separator. */
	private static final String KEY_CONTROLLER_CLASS_NAME = "controller.classname";
	
	/** default log level. */
	private static final String KEY_DEFAULT_LOG_LEVEL = "loglevel.default.level";
	
	/** log file name. */
	private static final String KEY_LOG_FILE_NAME = "log.file.name";
	
	
	/** upload retry count. */
	private static final String KEY_UPLOAD_RETRY_COUNT = "upload.retry.count";
	
	/** upload retry interval */
	private static final String KEY_UPLOAD_RETRY_INTERVAL = "upload.retry.interval";
	
	
	/** 
	 *  Constructs a instance. 
	 */
	private ApplicationProperties() {
	}
	
	/**
	 * read variable remote.opertaions.notification.
	 * /resource/application.prop
	 * @return default notification value
	 */
    public static boolean getDefaultRemoteOperationsNotification() {
    	return Boolean.getBoolean(DataCenter.getInstance().getString(KEY_REMOTE_OPERATIONS_NOTIFICATION));
    	
    }
    
    /**
     * read variable developer.log.buffer.size.
	 * /resource/application.prop
     * @return default log buffer size value
     */
    public static int getDefaultDeveloperLogBufferSize() {
    	
    	return DataCenter.getInstance().getInt(KEY_DEVELOPER_LOG_BUFFER_SIZE);
    }
    
    /**
     * read variable loglevel.default.expiration.duration.
	 * /resource/application.prop
     * @return default log level expiration value
     */
    public static String getDefaultLogLevelExpDuration() {
    	String expDuration =  DataCenter.getInstance().getString(KEY_LOG_LEVEL_EXPIRATION);
    	
    	if(expDuration == null) return StcCode.EXP_NEVER;    	
    	if(expDuration.length() < 1) return StcCode.EXP_NEVER;    	
    	return expDuration;
    	
    }
    

    /**
     * read variable loglevel.default.level.
	 * /resource/application.prop
     * @return default log level
     */
    public static int getDefaultLogLevel() {
    	
    	return DataCenter.getInstance().getInt(KEY_DEFAULT_LOG_LEVEL);
    }
    
    
    /**
     * read variable loglevel.default.expiration.duration.
	 * /resource/application.prop
     * @return default log level expiration value
     */
    public static String getLogFileName() {
    	String expDuration =  DataCenter.getInstance().getString(KEY_LOG_FILE_NAME);
    	
    	if(expDuration == null) return StcCode.EXP_NEVER;    	
    	if(expDuration.length() < 1) return StcCode.EXP_NEVER;    	
    	return expDuration;
    	
    }
    
    /**
     * read variable upload.retry.count
	 * /resource/application.prop
     * @return upload.retry.count
     */
    public static int getUploadRetryCount() {
    	
    	return DataCenter.getInstance().getInt(KEY_UPLOAD_RETRY_COUNT);
    }
    
    
    
    /**
     * read variable upload.retry.interval
	 * /resource/application.prop
     * @return upload.retry.interval
     */
    public static int getUploadRetryInterval() {
    	
    	return DataCenter.getInstance().getInt(KEY_UPLOAD_RETRY_INTERVAL);
    }
    
    
    
    /**
     * read variable daemon controller.classname.
	 * /resource/application.prop
     * @return daemon controller class name
     */
    public static String getClassName() {
    	return DataCenter.getInstance().getString(KEY_CONTROLLER_CLASS_NAME);
    }
}

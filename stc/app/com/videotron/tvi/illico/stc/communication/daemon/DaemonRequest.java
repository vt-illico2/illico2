package com.videotron.tvi.illico.stc.communication.daemon;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * @author thorn
 */
public class DaemonRequest {
	
	/** Map instance. */
	private Map parameters;
	
	/**
	 * Constructs DaemonRequest.
	 */
	public DaemonRequest() {
		parameters = new HashMap();
	}
	
	/**
	 * Associates the specified value of a request parameter with the specified name.
	 * @param name  - a String specifying the name of the parameter
	 * @param value - the value of a request parameter  
	 */
	public void setParameter(String name, String value) {
		parameters.put(name , value);
	}
	/**
	 * Returns the value of a request parameter as a String.
	 * @param name - a String specifying the name of the parameter
	 * @return a String representing the single value of the parameter
	 */
	public String getParameter(String name) {
		return (String) parameters.get(name);
	}
	
	public Vector getParameterKeys(){
		Vector keys = new Vector();
		
		Set set = parameters.entrySet();
		Iterator it = set.iterator();

        while(it.hasNext())
        {
            Map.Entry m =(Map.Entry)it.next();

            keys.add((String)m.getKey());

        }
		return keys;
	}
}

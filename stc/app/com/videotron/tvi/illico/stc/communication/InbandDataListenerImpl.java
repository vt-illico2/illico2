package com.videotron.tvi.illico.stc.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.uploader.LogServerConfig;
import com.videotron.tvi.illico.stc.uploader.LogUploader;


/**
 * Inband data listner implementation class.
 * this gathers configuration from inband and generates syslog server and port.
 *
 *
 * @author swkim
 */
public class InbandDataListenerImpl implements InbandDataListener,
		DataUpdateListener {

	/**
	 * implementation of abstract method.
	 *  it is executed when Inband OC data is received.
	 *
	 *  @param locator	service locator. Monitor fill in this value
	 *  @throws RemoteException RMI exception
	 */
	public void receiveInbandData(String locator) throws RemoteException {
		if (Log.INFO_ON) {
			Log
					.printInfo("InbandDataListenerImpl: receiveInbandData: locator = "
							+ locator);
		}

		LogServerConfig logServerConfig = LogServerConfig.getInstance();

		// MonitorService monitorService =
		// CommunicationManager.getInstance().getMonitorService();
		MonitorService monitorService = (MonitorService) DataCenter
				.getInstance().get(MonitorService.IXC_NAME);
		int newVersion = monitorService
				.getInbandDataVersion(MonitorService.stc_server);

		if (Log.DEBUG_ON) {
			Log.printDebug("InbandDataListenerImpl: receiveInbandData: ");
			Log.printDebug(" stc code currentVersion = " + logServerConfig.getFileVersion());

			Log
					.printDebug("InbandDataListenerImpl: receiveInbandData: stc code newVersion = "
							+ newVersion);
		}

		if (logServerConfig.getFileVersion() != newVersion) {
			logServerConfig.setFileVersion(newVersion);
			DataAdapterManager.getInstance().getInbandAdapter()
					.asynchronousLoad(locator);
		}

		try {
			monitorService.completeReceivingData(FrameworkMain.getInstance()
					.getApplicationName());
		} catch (RemoteException e) {
			Log.printError(e);
		}
	}


	/**
	 * implementation of abstract class.
	 * Monitor uses this method.
	 *
	 * @param key key of inband data
	 *
	 */
	public void dataRemoved(String key) {
		// do nothing;
	}

	/**
	 * Process a updated data.
	 *
	 * @param key
	 *            The key of update data.
	 * @param old
	 *            The old value
	 * @param value
	 *            The new value
	 */
	public void dataUpdated(String key, Object old, Object value) {
		if (Log.INFO_ON) {
			Log.printInfo("STC InbandDataListenerImpl: dataUpdated: key = " + key);
		}

		LogServerConfig logServerConfig = LogServerConfig.getInstance();

		if (key.equals(logServerConfig.getFileName()) && value != null) {
			byte[] data = BinaryReader.read((java.io.File) value);

			if (!logServerConfig.parse(data)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("STC: in-band server configuration is null.");
				}
			} else {
				LogUploader uThread = new LogUploader();
		        Thread uploadThread = new Thread(uThread);
		        uploadThread.start();
			}
		}
	}
}

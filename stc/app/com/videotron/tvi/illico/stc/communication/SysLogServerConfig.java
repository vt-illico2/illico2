/*
 *  SysLogServerConfig.java    $Revision: 1.1 $ $Date: 2014/04/03 14:02:34 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.stc.communication;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.util.Misc;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 25.
 */
public final class SysLogServerConfig {

	/** ip port value delimiter. */
	private static final String DELIM = ":";
	/** config version value. */
	private final byte configVersion;
	/** syslog server hostname. */
	private final String hostname;
	/** syslog server port. */
	private final int port;

	/**
	 * @return config version value.
	 */
	public byte getConfigVersion() {
		return configVersion;
	}

	/**
	 * @return syslog server hostname
	 */
	public String getHostname() {
		return hostname;
	}
	/**
	 * @return syslog server port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Creates a SysLogServerConfig object from the specified input stream.
	 * 
	 * @param stream
	 *            - input stream.
	 * @return a SysLogServerConfig object.
	 * @throws IOException
	 *             if fails to read data from the input stream.
	 */
	public static SysLogServerConfig create(InputStream stream)
			throws IOException {

		DataInputStream dis = new DataInputStream(new BufferedInputStream(
				stream));

		byte configVersion = dis.readByte();

		byte length = dis.readByte();

		byte[] b = new byte[length];

		dis.read(b, 0, b.length);

		String[] pairs = TextUtil.tokenize(new String(b), DELIM);

		if (pairs[1] != null) {
			pairs[1] = pairs[1].trim();
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("--------------------------------------");
    		Log.printDebug("In SysLogServerConfig, "
    				+ "server Ip = " + pairs[0] 
    				+ ", port = " + pairs[1]);
    		Log.printDebug("--------------------------------------");
    	}
		return new SysLogServerConfig(configVersion, pairs[0], Integer
				.parseInt(pairs[1]));
	}

	/**
	 * @param configVer
	 *            - config version.
	 * @param aHostname
	 *            - syslog server hostname.
	 * @param aPort
	 *            - syslog server port.
	 */
	private SysLogServerConfig(final byte configVer, final String aHostname,
			final int aPort) {
		this.configVersion = configVer;
		this.hostname = aHostname;
		this.port = aPort;
	}

	/**
	 * @param newConfig
	 *            - the SysLogServerConfig object to be compared with this
	 *            SysLogServerConfig object.
	 * @return <code>true</code> if there is any change between this
	 *         SysLogServerConfig object and the newConfig object.
	 */
	public boolean isChanged(SysLogServerConfig newConfig) {
		return newConfig == null
				|| getConfigVersion() != newConfig.getConfigVersion();
	}
	/**
	 * @return the string
	 */
	public String toString() {
		return "ConfigVersion:" + Misc.byteToUbyte(configVersion);
	}
}

package com.videotron.tvi.illico.stc.communication.daemon;

import java.io.IOException;
import java.util.List;

import com.videotron.tvi.illico.log.Log;
/**
 * @author thorn
 */
public final class DaemonResponseWriter {
	/**
	 * Constructs a instance.
	 */
	private DaemonResponseWriter() {
	}
	
	/**
	 * Write a entity of response to byte array.
	 * @param response - daemon response
	 * @return byte array
	 * @throws IOException - I/O error occurs.
	 */
	protected static byte[] writeEntity(final DaemonResponse response) throws IOException {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<root result=\"").append(response.getResultCode()).append("\">");
		List list = response.getContents();
		
		for (int i = 0, size = list.size(); i < size; i++) {
			buffer.append(list.get(i).toString());
		}
		buffer.append("</root>");
		
		if (Log.DEBUG_ON) {
			Log.printDebug(buffer.toString());
		}
		return buffer.toString().getBytes();
	}
	/**
	 * 
	 * @param response - write response to byte array.
	 * @return byte array.
	 * @throws IOException - I/O error occurs.
	 */
	public static byte[] write(DaemonResponse response) throws IOException {
		if (Log.DEBUG_ON) {
			Log.printDebug("byte[] write(DaemonResponse response)");
		}
		return writeEntity(response);
	}
}

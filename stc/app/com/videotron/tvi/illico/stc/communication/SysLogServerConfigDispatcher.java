/*
 *  SysLogServerConfigDispatcher.java    $Revision: 1.1 $ $Date: 2014/04/03 14:02:34 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.stc.communication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.dvb.dsmcc.DSMCCObject;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.util.concurrent.Scheduler;
import com.videotron.tvi.illico.stc.util.concurrent.Task;

/**
 * Provides access to the contents of configuration files in OC.
 * It notifies modification of configuration via
 * DataUpdateListener.
 * 
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 25.
 */
public class SysLogServerConfigDispatcher {

	/** config file name. */
	private static final String CONFIG_FILE = "stc_server.txt";

	/** Scheduler instance. */
	private Scheduler scheduler;

	/** a list of listeners. */
	private final List listeners = new ArrayList(1);

	/** OcApdater instance. */
	private final OcAdapter ocAdapter = new OcAdapter();

	/**
	 * @author sccu
	 */
	private class OcAdapter implements DataUpdateListener {

		/** service string. */
		private static final String DATA_OOB = "STC_DataOOB";

		/** config file key. */
		private static final String CONFIG_FILE_KEY = "STC_CONFIG";

		/**
		 * Starts to listen a config file.
		 */
		public void startToListen() {
			DataCenter.getInstance().addDataUpdateListener(CONFIG_FILE_KEY,
					this);
		}

		/**
		 * Stops to listen a config file.
		 */
		public void stopToListen() {
			DataCenter.getInstance().removeDataUpdateListener(CONFIG_FILE_KEY,
					this);
		}

		/**
		 * Retrieves a version file from OC.
		 * 
		 * @return a version file.
		 * @throws IOException
		 *             if fails to load from Oc.
		 */
		public File getSysLogServerConfigFile() throws IOException {
			if (Log.DEBUG_ON) {
				Log.printDebug("Entering getVersionFile().");
			}

			File file = (File) DataCenter.getInstance().get(CONFIG_FILE_KEY);
			if (file != null) {
				if (Log.DEBUG_ON) {
					Log
							.printDebug("Loads version.dat via DataCenter.");
				}
				return file;
			}

			file = getOcFile(CONFIG_FILE);
			if (file != null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("Loads version.dat via DSMCC.");
				}

				return file;
			}

			throw new FileNotFoundException("Fails to load Version from OC.");
		}

		/**
		 * Retrieves a file from OC.
		 * 
		 * @param fileName
		 *            - file name.
		 * @return the specified File instance.
		 * @throws IOException
		 *             if fails to load the specified file.
		 */
		public File getOcFile(String fileName) throws IOException {
			if (Log.DEBUG_ON) {
				Log.printDebug("Entering getOcFile() to load "
						+ fileName + ".");
			}

			DSMCCObject mountPoint = DataAdapterManager.getInstance()
					.getOobAdapter().getMountPoint();

			if (mountPoint == null) {
				throw new IOException("getMountPoint() returns null.");
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("canon path="
						+ mountPoint.getCanonicalPath());
			}

			DSMCCObject dsmcc = new DSMCCObject(mountPoint, DATA_OOB + "/"
					+ fileName);
			dsmcc.setRetrievalMode(DSMCCObject.FROM_STREAM_ONLY);
			dsmcc.synchronousLoad();

			if (Log.DEBUG_ON) {
				Log.printDebug("Loads " + fileName + " succesfully.");
			}

			return dsmcc;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.videotron.tvi.illico.framework.DataUpdateListener#dataRemoved
		 * (java.lang.String)
		 */
		/**
		 * Should not be called.
		 * 
		 * @param fileName
		 *            - file name.
		 */
		public void dataRemoved(String fileName) {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.videotron.tvi.illico.framework.DataUpdateListener#dataUpdated
		 * (java.lang.String, java.lang.Object, java.lang.Object)
		 */
		/**
		 * Invoked when a version file on OC is updated.
		 * 
		 * @param fileName
		 *            - file name
		 * @param oldFile
		 *            - old File instance.
		 * @param newFile
		 *            - new File instance.
		 */
		public void dataUpdated(String fileName, Object oldFile, Object newFile) {
			if (Log.INFO_ON) {
				Log.printInfo("Entering OcAdapter.dataUpdate().");
			}
			onOcUpdate((File) newFile);
		}
	}
	
	/**
     * Starts to listen a syslogserver config file.
     */
	public synchronized void start() {
		ocAdapter.startToListen();
	}
	
	/**
     * Stops to listen a syslogserver config file.
     */
	public void stop() {
		ocAdapter.stopToListen();
	}

	/**
	 * Initiates an instance.
	 * 
	 * @param sch
	 *            - scheduler instance.
	 */
	public synchronized void setScheduler(Scheduler sch) {
		if (sch == null) {
			throw new IllegalArgumentException("Argument should not be null.");
		}
		scheduler = sch;
	}

	/**
	 * @param configFile
	 *            - the updated syslogserver config file.
	 */
	private synchronized void onOcUpdate(File configFile) {
		scheduler.execute(new DataUpdateNotificationTask(configFile));

	}
	
	/**
     * Implements Task interface to call notifyDataUpdate().
     */
	private class DataUpdateNotificationTask implements Task {

		/** configFile. */
		private final File configFile;

		/**
		 * @param aConfigFile
		 *            - updated syslogserver config file.
		 */
		public DataUpdateNotificationTask(final File aConfigFile) {
			this.configFile = aConfigFile;
		}

		/**
		 * Invokes notifyDataUpdate().
		 */
		public void run() {
			try {
				notifyDataUpdate(configFile);
			} catch (IOException e) {
				if (Log.ERROR_ON) {
					Log.print(e);
				}
			}
		}
		
		/**
         * Returns the task name.
         * @return the string "DataUpdateNotificationTask".
         */
		public String getName() {
			return "DataUpdateNotificationTask";
		}
	};

	/**
	 * Adds a listener to the list of listeners.
	 * 
	 * @param listener
	 *            - a listener to be added.
	 */
	public synchronized void addListener(SysLogServerDataUpdateListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes a listener from the list of listeners.
	 * 
	 * @param listener
	 *            - a listener to be removed.
	 */
	public synchronized void removeListener(
			SysLogServerDataUpdateListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notifies the listeners of a data update event.
	 * 
	 * @param configFile
	 *            - sysLogServer config file.
	 * @throws IOException
	 *             if fails to read a version from OOB-OC.
	 */
	private void notifyDataUpdate(File configFile) throws IOException {
		InputStream is = new FileInputStream(configFile);
		SysLogServerConfig config = null;
		try {
			config = SysLogServerConfig.create(is);
		} finally {
			is.close();
		}

		if (Log.DEBUG_ON) {
			Log
					.printDebug("stc_file.dat file is successfully loaded from OC.");
		}

		synchronized (this) {
			for (int x = 0; x < listeners.size(); x++) {
				((SysLogServerDataUpdateListener) listeners.get(x))
						.onDataUpdateEvent(new SysLogServerDataUpdateEvent(
								config));
			}
		}
	}

	/**
	 * Retrieves Version object from OOB-OC.
	 * 
	 * @return the Version object retrieved from OOB-OC.
	 * @throws Exception
	 *             if fails to read a file from OOB-OC.
	 */
	public SysLogServerConfig getSysLogServerConfigFromOc() throws Exception {
		if (Log.DEBUG_ON) {
			Log.printDebug("Entering getSysLogServerConfigFromOc().");
		}

		InputStream is = new FileInputStream(ocAdapter
				.getSysLogServerConfigFile());

		try {
			return SysLogServerConfig.create(is);
		} finally {
			is.close();
		}
	}
}

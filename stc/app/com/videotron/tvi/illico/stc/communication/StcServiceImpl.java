/*  StcServiceImpl.java    $Revision: 1.1 $ $Date: 2014/04/03 14:02:34 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.stc.communication;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.controller.GenAppController;
import com.videotron.tvi.illico.stc.util.StringUtils;

/**
 * 
 * @author thorn
 * 
 */
public class StcServiceImpl implements StcService {
	/** a list of listeners. */
	private static Map listeners = null;

	/**
	 * Constructs a instance.
	 */
	public StcServiceImpl() {
		listeners = new HashMap();
	}
	/**
	 * Assert that appName has actual text. If it does not an IllegalArgumentException is thrown.
	 * @param appName - application name
	 */
	protected void assertAppName(String appName) {
		if (!StringUtils.hasText(appName)) {
			throw new IllegalArgumentException("appName should not be empty.");
		}
	}

	/**
	 * Asserts that a listener isn't null. If it is an IllegalArgumentException is thrown. 
	 * @param listener - a listener to check or null
	 */
	protected void assertListener(LogLevelChangeListener listener) {
		if (listener == null) {
			throw new IllegalArgumentException("Listener should not be null.");
		}
	}

	/**
	 * @param appName - application name to register
	 * @return current log level of application
	 * @throws RemoteException occur during the execution of a remote method call
	 */
	public int registerApp(String appName) throws RemoteException {

		assertAppName(appName);
		return new GenAppController().registerLog(appName);
	}

	 /**
	 * Adds a listener to the map of listeners.
	 * @param appName - application name to register
	 * @param listener - a listener to be added.
	 * @throws RemoteException - - occur during the execution of a remote method call
	 */
	public synchronized void addLogLevelChangeListener(String appName,
			LogLevelChangeListener listener) throws RemoteException {

		assertAppName(appName);
		assertListener(listener);
		listeners.put(appName, listener);
		if (Log.INFO_ON) {
			Log.printInfo("called addLogLevelChangeListener("
					+ appName + "," + listener.getClass() + ")");
		}
	}
	
	/**
     * Removes a listener from the list of listeners.
     * @param appName - app name of a listener to be removed.
     * @throws RemoteException - occur during the execution of a remote method call
     */
	public synchronized void removeLogLevelChangeListener(String appName)
			throws RemoteException {

		assertAppName(appName);
		listeners.remove(appName);
		if (Log.INFO_ON) {
			Log.printInfo("called removeLogLevelChangeListener("
					+ appName + ")");
		}
	}
	
	/**
	 * Removes all of the listeners from listeners map. 
	 */
	public synchronized void removeLogLevelChangeListener() {
		listeners.clear();
	}
	
	/**
	 * 
	 * @param appName - application name
	 * @param logLevel - log level to change
	 * @return true if, and only if, any exception does not occurs.
	 */
	public synchronized boolean notifyLogLevelChange(String appName,
			int logLevel) {

		assertAppName(appName);
		
		if (Log.INFO_ON) {
			Log.printInfo("notifyLogLevelChange(" + appName + "," + logLevel + ")");
		}
		try {
			LogLevelChangeListener listener = (LogLevelChangeListener) listeners
					.get(appName);
			this.assertListener(listener);
			listener.logLevelChanged(logLevel);
		} catch (Exception e) {
			if (Log.ERROR_ON) {
				Log.print(e);
			}
			return false;
		}
		return true;
	}
	
	/**
	 * @param logLevel - log level to change
	 * @return true if, and only if, any exception does not occurs.
	 */
	public synchronized boolean notifyLogLevelChange(int logLevel) {
		try {
			LogLevelChangeListener listener;
			Iterator iterator = listeners.entrySet().iterator();
			while (iterator.hasNext()) {
				listener = (LogLevelChangeListener) iterator.next();
				listener.logLevelChanged(logLevel);
			}
		} catch (Exception e) {
			if (Log.ERROR_ON) {
				Log.print(e);
			}
			return false;
		}
		return true;
	}
}

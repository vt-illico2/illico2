package com.videotron.tvi.illico.stc.config;


import com.videotron.tvi.illico.log.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.stc.communication.ApplicationProperties;
import com.videotron.tvi.illico.stc.util.StcCode;
import com.videotron.tvi.illico.util.TextUtil;


/**
 * data manager in persistent storage.
 * it reads and write configuration in persistent storage.
 * 
 * @author swkim
 *
 */
public class PersistentStorageManager {

	/**
	 * application config element count.
	 */
	private static final int ELE_COUNT = 4;
	/** 
	 * application configuration index for name.
	 */
	private static final int NAME_IDX = 0;
	/**
	 * application configuration index for level.
	 */
	private static final int LEVEL_IDX = 1;
	/**
	 * application configuration index for buffer size.
	 */
	private static final int SIZE_IDX = 2;
	/**
	 * application configuration index for expiration date.
	 */
	private static final int EXP_IDX = 3;

	/**
	 * root key name for persistent storage.
	 */
    private static final String PERSISTENT_ROOT_KEY = "dvb.persistent.root";
    /**
     * default persistent path.
     */
    private static final String PERSISTENT_ROOT = System.getProperty(PERSISTENT_ROOT_KEY);
    
    /**
     * actual STC path.
     */
    private static final String STC_PATH = PERSISTENT_ROOT + "/stc/";
    //private static final String STC_PATH = "C:/TEMP/";
    
    // there was an error in configuration, so do not use old configuration, list.cfg.
    // if there is an old config file, then remove it and use new one, stc.cfg
    
    /**
     * stc configuration file name.
     */
    private static final String CONFIG_FILE_NAME = ApplicationProperties.getLogFileName();

   
    
    /**
     * separator to separates attributes in one configuration.
     */
	private static final String ELE_SEP = "\t";
	/**
	 * separator to separates configurations.
	 */
	private static final String LINE_SEP = System.getProperty("line.separator");

	
	/**
	 * creator of persistent storage manager.
	 * it checks path and create new one if there is not.
	 * 
	 */
    public PersistentStorageManager() {
    	if (Log.DEBUG_ON) {
    		Log.printDebug("called persistent storage manager.");
    	}
        File stcFolder = new File(STC_PATH);
        if (!stcFolder.exists()) {
            stcFolder.mkdir();
            if (Log.DEBUG_ON) {
        		Log.printInfo(STC_PATH + " is created successfully.");
            }
        } else {
        	if (Log.DEBUG_ON) {
        		Log.printDebug(STC_PATH + "is existing. so read file that file");
        	}
        }
    }

    
    

    /**
     * Writes data into the specified file on flash memory.
     * @param fileName - the specified file path.
     * @param data - the data to be written.
     * @return flag to write data succeeded or not.
     */
    public boolean writeToFile(String fileName, byte[] data)  {
    	
    	boolean result = true;
    	
    	if (data == null) {
            if (Log.DEBUG_ON) {
        		Log.printError("Write " + fileName + " into persistent storage but data is null." 
        				+ "so do not write and return false");
            }
            return false;
    	}
    	
        if (Log.DEBUG_ON) {
    		Log.printInfo("Write " + fileName + " into persistent storage with data." 
    				+ data.toString());
        }
    	FileOutputStream fos = null;
    	
    	try {
    		fos = new FileOutputStream(STC_PATH + fileName);
	        fos.write(data);
	        fos.close();
    	} catch (IOException e) {
    		result = false;
    		if (Log.DEBUG_ON) {
        		Log.printError("Error writing configuration into persistent memeory ");
    		}
    	} finally {
    		try { 
    			fos.close(); 
			} catch (Exception e) {
				if (Log.DEBUG_ON) {
	        		Log.printError("Error closing fos in finally. ");
	    		}
			}
    		try { 
    			fos = null; 
			} catch (Exception e) {
				if (Log.DEBUG_ON) {
	        		Log.printError("Error setting fos null in finally. ");
	    		}
			}
    	}
  
        return result;
    }

    /**
     * Returns InputStream instance of the file specified by the filePath parameter. It returns <code>null</code> if the
     * specified file is not found on persistent storage.
     * @param fileName - the file path.
     * @return an <code>InputStream</code> instance of the specified file, if the file exists on flash memory;
     * <code>null</code>, otherwise.
     */
    public String readFromFile(String fileName) {
    	if (Log.DEBUG_ON) {
    		Log.printDebug("Entering readFromFile() to load " + fileName + ".");
        }

    	FileInputStream fis = null;
    	StringBuffer contentBuffer = new StringBuffer("");
    	int ch = 0;
    	
        File file = new File(STC_PATH + fileName);
        
        if (Log.DEBUG_ON) {
    		Log.printDebug("readFromFile(), full path=" + STC_PATH + fileName);
        }
        
        if (!file.exists()) {
            if (Log.DEBUG_ON) {
        		Log.printInfo("File is not existing. So return null.");
            }
            return null;
        }

        try {
            fis = new FileInputStream(file);
            while ((ch = fis.read()) != -1) {
            	contentBuffer.append((char) ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (Log.DEBUG_ON) {
        		Log.printError("Error reading data from file + " + fileName);
            }
            return null;
        } finally {
        	try { 
    			fis.close(); 
			} catch (Exception e) {
				if (Log.DEBUG_ON) {
	        		Log.printError("Error closing fis in finally. ");
	    		}
			}
    		try { 
    			fis = null; 
			} catch (Exception e) {
				if (Log.DEBUG_ON) {
	        		Log.printError("Error setting fis null in finally. ");
	    		}
			}
        }
        if (contentBuffer == null) {
	        return null; 
        } else {
        	return contentBuffer.toString();
        }
    }
    
    

    /*
     * Removes the specified file.
     * it will be used when a file should be removed.
     * @param fileName - the file to be removed.
     */
    
    private void removeFile(String fileName) {
    	
    	/*
        File file = new File(STC_PATH + fileName);

        if (Log.DEBUG_ON) {
    		Log.developer.printDebug("[STC] checking file to remove old STC configuration file " + fileName);
        }
  
        if (file.exists()) {
        	
        	String aFile = file.getName();
        	if(aFile != null) {
        		if(!aFile.equals(fileName)){
        			if (Log.DEBUG_ON) {
                		Log.developer.printDebug("[STC] there is an old STC configuration file, so delete this one.");
                    }
        			file.delete();
        		}
        	}
        }
        */
    	
    	
    	File dir = new File(STC_PATH);
		File file = null;
		File[] files = dir.listFiles();
	    for (int i=0; i<files.length; i++) {
	    	file = files[i];
	    	if((file == null) || (file.getName() == null)) continue;	
	    	
	    	// if there are old configuration files and whose name is different from current one, then remove old one.
	    	// if configuration file exists and whose name is same as current one, then do not remove and use it.
	        if(!fileName.equals(file.getName().trim())) {
	        	file.delete();
	        	if (Log.DEBUG_ON) {
            		Log.printDebug("[STC] there is an old STC configuration file, so delete this one." + file.getName());
                }
	        }
	    }
	    
    }

    
    /**
     * convert a configuration string into application config object.
     * @param row a configuration string
     * @return	application configuration object
     */
	private ApplicationConfig convertToApplicationConfig(String row) {
		ApplicationConfig config = new ApplicationConfig();
		
		String [] elements =  TextUtil.tokenize(row, ELE_SEP);
	
		if (elements == null) {
	        if (Log.DEBUG_ON) {
	    		Log.printError("application configuration parsing error");
	        }
	        return null;
	  
		}
		if (elements.length != ELE_COUNT) {
			if (Log.DEBUG_ON) {
	    		Log.printError("application configuration parsing error");
			}
			return null;
		}
		config.setName(elements[NAME_IDX]);
		config.setLogLevel(Integer.parseInt(elements[LEVEL_IDX]));
		config.setBufferSize(Integer.parseInt(elements[SIZE_IDX]));
		config.setExpires(checkExpires(elements[EXP_IDX]));
				
		return config;
	}
	
	private String checkExpires(String inStr){
		if(inStr == null) return ApplicationProperties.getDefaultLogLevelExpDuration();
		if(StcCode.NULL_STRING.equals(inStr)) return ApplicationProperties.getDefaultLogLevelExpDuration();
		if(StcCode.EMPTY_STRING.equals(inStr)) return ApplicationProperties.getDefaultLogLevelExpDuration();
		return inStr;
	}
	/**
	 * convert an application config into a string.
	 * @param config log configuration for an application
	 * @return a string for that application configuration.
	 */
	private String convertToString(ApplicationConfig config) {
		String row = null;
		
		if (config == null) {
			return null;
		}
		
		row = config.getName() + ELE_SEP + String.valueOf(config.getLogLevel()) 
		+ ELE_SEP + String.valueOf(config.getBufferSize()) + ELE_SEP + config.getExpires();
		
		return row;
	}
	
	/**
	 * read configuration stored in persistent memory.
	 * 
	 * @return all application's configuration in persistent storage.
	 */
	public synchronized ApplicationConfig [] readConfiguration() {
		ApplicationConfig [] configs = null;
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("readConfiguration called from persistent storage. ");
    	}
		
		String storedString = readFromFile(CONFIG_FILE_NAME);
		
		
		if (storedString == null) {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("there is no configuration stored in persistent storage. ");
	    	}
			return null;
		} else {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("configuration in flash : [" + storedString + "]");
	    	}
		}
		
		String [] configData = TextUtil.tokenize(storedString, LINE_SEP);
		if (configData == null) {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("there is no configuration stored in persistent storage. ");
	    	}
			return null;
		}
		
		configs = new ApplicationConfig[configData.length];
		for (int i = 0; i < configData.length; i++) {
			configs[i] = convertToApplicationConfig(configData[i]);
		}
		
		return configs;
	}

	/**
	 * writes all application's configurations in memory into persistent storage.
	 * @param configs all application's configurations stored in memory
	 * @return status flag that it succeeded to write or not.
	 */
    public synchronized boolean writeConfiguration(ApplicationConfig [] configs) {
    	
    	if (Log.DEBUG_ON) {
    		Log.printInfo("writeConfiguration called. Stores " 
    				+ configs.length + " configuration");
    	}
    	
    	if ((configs == null) || (configs.length == 0)) {
    		if (Log.DEBUG_ON) {
	    		Log.printDebug("in writeConfiguration called. "
	    				+ "But configs is null. So do not store anything");
	    	}
    	}
    	
    	String configLine = "";

    	for (int i = 0; i < configs.length; i++) {
    		configLine += convertToString(configs[i]) + LINE_SEP;
    	}
    	
    	if (configLine == null) {
    		if (Log.DEBUG_ON) {
	    		Log.printDebug("in writeConfiguration cofiguration data is finally null. "
	    				+ "so return false");
	    	}
    	}
    	
    	if (configLine.length() > LINE_SEP.length()) {
    		configLine = configLine.substring(0, configLine.length() - LINE_SEP.length());
    	}
    	
    	return writeToFile(CONFIG_FILE_NAME, configLine.getBytes());
    	
    }
    
    public synchronized void initializeFile(){
    	removeFile(CONFIG_FILE_NAME);
    }
}

package com.videotron.tvi.illico.stc.config;

import com.videotron.tvi.illico.log.Log;

import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Vector;


/**
 * data store of application's configuration.
 * it keeps application's configuration in memory and use them in STC application.
 * 
 * @author swkim
 *
 */
public class LogConfig {


	/**
	 * data store for application configuration.
	 */
	private static Hashtable configs = null;	
	/**
	 * instance of LogConfig.
	 * Log config is static singleton object.
	 */
	private static LogConfig instance = null;
	
	
	/**
	 * flag to check config initialization.
	 */
	private static boolean initializedConfig = false;
	
	/**
	 * 
	 * constructor : currently it only creates hashtable.
	 * in AppConfigManager, it should call initialLogConfig to fill in LogConfig
	 * if initLogConfig is not called, you should use this method here 
	 * - it reads persistent storage manager and put configuration in memory
	 * - it is a singleton class, thus it should be called only once.
	 */
	protected LogConfig() {
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("LogConfig is created.");
    	}
		
		
		configs = new Hashtable();
		
		/*
		PersistentStorageManager psManager = new PersistentStorageManager();
		ApplicationConfig [] appConfigs = psManager.readConfiguration();
		
		if(appConfigs != null){
			for(int i=0; i<appConfigs.length; i++){
				configs.put(appConfigs[i].getName(), appConfigs[i]);
			}
		}
		*/
	}
	
	
	/**
	 * single method to provide instance of LogConfig.
	 * 
	 * @return LogConfig instance.
	 */
	public static LogConfig getInstance() {
		if (instance == null) {
			instance = new LogConfig();
		}
		return instance;
	}
	
	/**
	 * provides count of configuration stored in LogCofnig.
	 * 
	 * @return count of configurations.
	 */
	public synchronized int getConfigCount() {
		return configs.size();
	}
	
	
	/**
	 * add new configuration for an application.
	 * it is called when registered or modified.
	 * 
	 * @param appName	application's name
	 * @param aConfig	application's configuration.
	 */
	public synchronized void addConfiguration(String appName, ApplicationConfig aConfig) {
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("addConfiguration " + appName 
    				+ " called, and put configuration in LogConfig memory");
    	}
		
		configs.put(appName, aConfig);
	}
	
	/**
	 * check whether there is a configuration in stored memory
	 * it is called before app stores configuration.
	 * 
	 * @param appName	application's name
	 * 
	 * @return true if configuration exists, false otherwise.
	 */
	
	public synchronized boolean existConfiguration(String appName) {
		boolean existing = false;
		
		Object obj = configs.get(appName);
		
		if (obj != null) {
			existing = true;
		}
		
		return existing; 
	}
	/**
	 * provide one configuration of a target application.
	 * 
	 * @param appName	target application's name
	 * @return	configuration for an application targeted.
	 */
	public synchronized ApplicationConfig getConfiguration(String appName) {
		if (appName == null) {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("appConfiguration called. " 
	    				+ "but appName is null. So return null.");
	    	}
			return null;
		}
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("getConfiguratoin " + appName + " called");
    	}
		return (ApplicationConfig) configs.get(appName);
	}
	
	
	/**
	 * Provides all configuration stored in this LogConfig object.
	 * they are application's configuration registered to STC.
	 * 
	 * @return all configurations in LogConfig
	 */
	public synchronized ApplicationConfig [] getAllConfiguration() {


		Vector appConfigVector = new Vector();
		
		Enumeration enums = configs.elements();
		
		while (enums.hasMoreElements()) {		
			appConfigVector.add((ApplicationConfig) enums.nextElement());
		}
		
		int configSize = appConfigVector.size();
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("getAllConfiguration called. There are  " + configSize  + " configurations.");
    	}
		
		if (configSize == 0) {
			return null;
		}
		
		ApplicationConfig [] appConfigs = new ApplicationConfig [configSize];
		
		for (int i = 0; i < configSize; i++) {
			appConfigs[i] = (ApplicationConfig) appConfigVector.get(i);
		}
		
		return appConfigs; 
	}
	
	
	
	/*
	public synchronized ApplicationConfig [] getAllConfiguration() {
		
		int configCount = configs.size();
		
		if (configCount == 0) {
			return null;
		}
		
		ApplicationConfig [] appConfigs = new ApplicationConfig [configCount];
		Enumeration enums = configs.elements();
		int i = 0;
		
		while (enums.hasMoreElements()) {
			if (i < configCount) {
				appConfigs[i++] = (ApplicationConfig) enums.nextElement();
			} else {
				if (Log.DEBUG_ON) {
		    		Log.developer.printDebug("Exception to get configuration more than confgis.sies.");
		    	}
			}
		}
		
		if (Log.DEBUG_ON) {
    		Log.developer.printDebug("getAllConfiguration called. There are  " + i + " configurations.");
    	}
		
		return appConfigs; 
	}
	*/
	
	
	/**
	 * check whether restoration of initial parameter is done of not.
	 */
	public synchronized void setInitialzed() {
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("setInitialzed configuration method called.");
    	}
		
		initializedConfig = true;
	}
	
	
	/**
	 * set restoration of initial parameter done.
	 * @return boolean of restoration
	 */
	public synchronized boolean checkInitialized() {
		if (Log.DEBUG_ON) {
    		Log.printDebug("checkInitialized called. returned " + initializedConfig);
    	}
		
		return initializedConfig;
	}
	
}

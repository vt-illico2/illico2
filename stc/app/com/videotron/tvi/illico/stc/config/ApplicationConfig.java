package com.videotron.tvi.illico.stc.config;


/**
 * ApplicationConfig is data format object to share data.
 * it contains application name, log level, expire date, buffer size and notify.
 * 
 * @author swkim
 *
 */
public class ApplicationConfig {

	/**
	 * log level defined in STC. Error is 3
	 */
	public static final int ERROR = 3;
	/**
	 * log level defined in STC. WARNING is 4
	 */
	public static final int WARNING = 4;
	/**
	 * log level defined in STC. INFO is 6
	 */
	public static final int INFO = 6;
	/**
	 * log level defined in STC. DEBUG is 7
	 */
	public static final int DEBUG = 7;
	/**
	 * log level defined in STC. Off is 0
	 */
	public static final int OFF = 0;

	/**
	 * application name of an application.
	 */
	private String name;
	
	/**
	 * log level of an application. 
	 */
	private int logLevel;
	
	/**
	 * expired date for an application to be stopped logging.
	 */
	private String expires;
	
	/**
	 * buffer size for an application in STB.
	 */
	private int bufferSize;
	/**
	 * flag to set notification center to show message.
	 */
	private boolean notify = false;

	
	/**
	 * getter for an application name.
	 * @return	String application name
	 */
	public String getName() {
		return name;
	}

	
	/**
	 * setter for an application name.
	 * @param appName	application's name
	 */
	public void setName(String appName) {
		this.name = appName;
	}

	
	/**
	 * getter for an application's log level.
	 * @return	log level.
	 */
	public int getLogLevel() {
		return logLevel;
	}

	
	/**
	 * setter for an application's log level.
	 * @param stcLogLevel	log level for an application to be set.
	 */
	public void setLogLevel(int stcLogLevel) {
		this.logLevel = stcLogLevel;
	}

	
	/**
	 * setter of expired data for an application.
	 * @return	expiration date for an application to stop logging.
	 */
	public String getExpires() {
		return expires;
	}

	
	/**
	 * set expiration date.
	 * @param expiresDate	expiration date.
	 */
	public void setExpires(String expiresDate) {
		this.expires = expiresDate;
	}

	
	/**
	 * getter for an applciation's buffer size.
	 * @return	buffer size of an application.
	 */
	public int getBufferSize() {
		return bufferSize;
	}

	
	/**
	 * set buffer size for an application.
	 * @param bufferLen	buffer size.
	 */
	public void setBufferSize(int bufferLen) {
		this.bufferSize = bufferLen;
	}

	// Override
	/**
	 * generates application's configuration into xml format.
	 * @return String application configuration in xml format.
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer("<l");
		buffer.append(" ap=\"");
		buffer.append(getName());
		buffer.append("\" lv=\"");
		buffer.append(getLogLevel());

		if (getLogLevel() >= 0) {
			buffer.append("\" ex=\"");
			buffer.append(getExpires());
		}

		buffer.append("\"/>");

		return buffer.toString();
	}

	/**
	 * return a value to set notification is needed or not.
	 * @return	<code> true </code> when request from STCC is set notify
	 */
	public boolean isNotify() {
		return notify;
	}

	/**
	 * set a value whether notification is needed or not.
	 * @param setNotify <code> true </code> when request from STCC is set notify
	 */
	public void setNotify(boolean setNotify) {
		this.notify = setNotify;
	}

}

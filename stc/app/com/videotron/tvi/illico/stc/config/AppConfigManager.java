package com.videotron.tvi.illico.stc.config;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.communication.ApplicationProperties;
import com.videotron.tvi.illico.stc.util.StcCode;
import com.videotron.tvi.illico.stc.util.StcDuration;

/**
 * This class manages configuration information used in STC application. 
 * it is an interface for config package. So if there is a request to manage
 * configuration in STC, use this class to manage applcation's logging
 * configuration.
 * 
 * initially general application registers itself to STC to put logs STC
 * check previous registration and registers to register it register
 * configuration and create share object
 */
public class AppConfigManager {

	/**
	 * LogConfiguration to store application's configuration.
	 */
	private LogConfig logConfig = null;
	// LogConfig is static singleton object

	/**
	 * Creator of AppConfgiManager.
	 * logConfig is a static singleton object so create in this function
	 */
	public AppConfigManager() {
		logConfig = LogConfig.getInstance();
	}

	/**
	 * 
	 * called from GenAppController to register an application in configuration.
	 * 
	 * @param appName application name to be registered . it can not be null.
	 * @param logLevel	log level for an application to be set in config manager
	 * @return log level of an application. if return is negative, then it is
	 *         STC Error code
	 *  
	 */
	public int registerApp(String appName, int logLevel) {
		// int logLevel = ApplicationConfig.Off;

		if (appName == null) {
			if (Log.DEBUG_ON) {
				Log.printError("Error : AppConfigManager.registerApp is called. "
						+ "appName is null.");
			}
			return StcCode.STC003;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("AppConfigManager.registerApp(" + appName
					+ ") is called");
		}

		ApplicationConfig config = logConfig.getConfiguration(appName);
		String durationConfig = null;
		// this method should be called that config is not set, but to be
		// conformed to check this.
		// if there is a performance issue, you can remote to check this
		// confirmation.
		// if config is null, that means there is no configuration registered.
		// So create new configuration
		if (config == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("create new appName, " + appName
						+ ", configuration");
			}
			config = new ApplicationConfig();
			config.setName(appName);
			config.setBufferSize(ApplicationProperties.getDefaultDeveloperLogBufferSize());
			durationConfig = ApplicationProperties.getDefaultLogLevelExpDuration();
			
			
			if (Log.DEBUG_ON) {
				Log.printDebug("[STC] before, duration of appName, " + appName + " is " + durationConfig);
			}
			if (durationConfig == null ){
				durationConfig = StcCode.EXP_NEVER;
			} else if((durationConfig.equals(StcCode.EMPTY_STRING)) || (durationConfig.equals(StcCode.NULL_STRING))|| (durationConfig.length() < 1)) {
				durationConfig = StcCode.EXP_NEVER;
			} else {
				durationConfig = ((new StcDuration()).parseDuration(durationConfig));
			}
			
			if (Log.DEBUG_ON) {
				Log.printDebug("[STC] after, duration of appName, " + appName + " is " + durationConfig);
			}
			
			config.setExpires(durationConfig);
			
			config.setLogLevel(logLevel);

			logConfig.addConfiguration(appName, config);

		} else {
			logLevel = config.getLogLevel();
		}
		// if config is existed, then it means there is already configuration,
		// thus we use this as configuration

		return logLevel;
	}

	/**
	 * In controller, app name should be checked. 
	 * In this method, app name
	 * should no be null. it update configuration of one application.
	 * 
	 * @param appConfig
	 *            Application configuration
	 * @return logging level, if return is negative, then it is STC Error
	 * 
	 */

	public int updateLogConfig(ApplicationConfig appConfig) {

		if (appConfig == null) {
			if (Log.DEBUG_ON) {
				Log.printError("appConfig is null, do not update configuration and return");
			}
			return StcCode.STC003;
		}
		
		// if appName is null, then configuration is not updated
		if (appConfig.getName() == null) {
			if (Log.DEBUG_ON) {
				Log.printError("appName is null, do not update configuration and return");
			}
			return StcCode.STC003;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("update configuration of, "
					+ appConfig.getName() + ", " + appConfig.getBufferSize()
					+ "," + appConfig.getExpires() + ", "
					+ appConfig.getLogLevel());
		}
		logConfig.addConfiguration(appConfig.getName(), appConfig);

		return appConfig.getLogLevel();
	}

	/**
	 * 
	 * it is public interface to get all log configuration. logging
	 * configuration is in LogConfig
	 * 
	 * @return returns all application logging configuration
	 */
	public ApplicationConfig[] getAllLogConfig() {

		if (Log.DEBUG_ON) {
			Log.printDebug("getAllLogConfig called. ");
		}

		return checkMultiAppsExpiredDate(logConfig.getAllConfiguration());
	}

	/**
	 * returns on logging configuration of targeted application.
	 * 
	 * @param appName
	 *            application. it should not be null
	 * @return ApplicationConfiguration of targeted application
	 */
	public ApplicationConfig getOneLogConfig(String appName) {
		
		if (Log.DEBUG_ON) {
			Log.printDebug("getOneLogConfig called");
		}
		
		if (appName == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("getOneLogConfig called but appname is null. "
						+ "So return null object");
			}
		}
		
		return checkOneAppsExpiredDate(logConfig.getConfiguration(appName));
	}

	
	/**
	 * check applications' configuration whether they have duration in expired date.
	 * if there is a duration, then it converts duration into date
	 * @param configs list of application configs
	 * @return converted applications' config
	 */
	private ApplicationConfig [] checkMultiAppsExpiredDate(ApplicationConfig [] configs) {
		ApplicationConfig [] newConfigs = null;
		
		if (configs == null) {
			return null;
		}
		if (configs.length == 0) {
			return null;
		}
		
		newConfigs = new ApplicationConfig [configs.length];
		
		for (int i = 0; i < configs.length; i++) {
			newConfigs[i] = checkOneAppsExpiredDate(configs[i]);
		}
		
		return newConfigs;
	}
	
	/**
	 * check expired date in application config and convert if config is duration.
	 * @param config application's configuration
	 * @return new application config.
	 */
	private ApplicationConfig checkOneAppsExpiredDate(ApplicationConfig config) {
		
		if (config == null) {
			return null;
		}
		
		ApplicationConfig newConfig = new ApplicationConfig();
		
		newConfig.setBufferSize(config.getBufferSize());
		newConfig.setLogLevel(config.getLogLevel());
		newConfig.setName(config.getName());
		newConfig.setExpires(config.getExpires());
		
		// if expires is not "NEVER", then do expire
		if(config.getExpires() == null) {
			
			if(ApplicationProperties.getDefaultLogLevelExpDuration() == null) {
				newConfig.setExpires(StcCode.EXP_NEVER);
			} else {
				newConfig.setExpires(ApplicationProperties.getDefaultLogLevelExpDuration());
			}

		}		
		else {
			// expire is not null
			if(config.getExpires().equals(StcCode.EXP_NEVER)){	
				newConfig.setExpires(config.getExpires());
			} else {
				String exp = checkExpiredDate(config.getExpires());
				// convert duration into time
				if (exp == null) {
					if(ApplicationProperties.getDefaultLogLevelExpDuration() == null)
						newConfig.setExpires(StcCode.EXP_NEVER);
					else 
						newConfig.setExpires(ApplicationProperties.getDefaultLogLevelExpDuration());
				} else {
					newConfig.setExpires(exp);
				}
			} 
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug(config.getName() + " converting exp date from " + config.getExpires()
					+ " to " + newConfig.getExpires());
		}
		return newConfig;
	}
	
	
	/**
	 * check whether expired date is duration or time.
	 * if expired date is duration, it converts duration into time.
	 * 
	 * @param expiredDate expired date in application config
	 * @return expired date
	 */
	private String checkExpiredDate(String expiredDate) {
		// check null first
		
		if(expiredDate.equals(StcCode.EXP_NEVER)) return expiredDate;
		
		return (new StcDuration()).parseDuration(expiredDate);		
	}
	
	
	
	/**
	 * initialization method.
	 * initially read configuration stored in persistent storage and put
	 * configurations in memory
	 * 
	 * @return ApplicationConfig [] those are all configuration stored.
	 */
	public ApplicationConfig[] getIniticalLogConfig() {

		if (Log.DEBUG_ON) {
			Log.printDebug("getInitialLogConfig called");
		}

		ApplicationConfig[] allLogConfig = null;
		PersistentStorageManager psManager = new PersistentStorageManager();

		// read configuration from persistent storage
		// put stored configuration into current configuration in memory

		try {
			psManager.initializeFile();
			allLogConfig = psManager.readConfiguration();
		} catch (NullPointerException e) {
		    if (Log.DEBUG_ON) {
	    		Log.printError("Error reading configuration. " 
	    				+ e.getMessage());
		    }
		}


		// set true for flash initialization
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("setInitialzed configuration method called in ConfigManager.");
    	}
		
		logConfig.setInitialzed();
		
		
		if ((allLogConfig == null) || (allLogConfig.length == 0)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("there are no log config in persistent storage.");
			}

			return null;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("there are " + allLogConfig.length
					+ " log config already");
		}

		// store configurations read from persistent memory into LogConfig
		if (allLogConfig.length > 0) {

			for (int i = 0; i < allLogConfig.length; i++) {
				if (allLogConfig[i] == null || allLogConfig[i].getName() == null) {
					continue;
				}
				if (Log.DEBUG_ON) {
					Log.printDebug("initialize "
							+ allLogConfig[i].getName()
							+ " stored configuration");
				}
				logConfig.addConfiguration(allLogConfig[i].getName(),
						allLogConfig[i]);
			}

		}
		
		return allLogConfig;
	}

	/**
	 * stores logging configuration in memory to persistent storage.
	 * 
	 */
	public void storeConfig() {

		if (Log.DEBUG_ON) {
			Log
					.printDebug("try to store configuration in persisitent storage");
		}

		PersistentStorageManager psManager = new PersistentStorageManager();

		
		/*
		 * wait until flash initialization is done.
		 * until flash initialized, changes only stored in memory
		 */
		if (logConfig.checkInitialized()) {
			psManager.writeConfiguration(logConfig.getAllConfiguration());
		} else {
			if (Log.DEBUG_ON) {
				Log
						.printDebug("not store in flash because flash is not initialized.");
			}
		}

	}
}

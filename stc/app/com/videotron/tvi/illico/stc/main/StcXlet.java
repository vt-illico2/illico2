/*
 *  StcXlet.java    $Revision: 1.4 $ $Date: 2014/06/17 07:00:20 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.stc.main;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.stc.communication.CommunicationRequestDispatcher;
//import com.videotron.tvi.illico.stc.communication.SysLogServerConfigDispatcher;
import com.videotron.tvi.illico.stc.controller.StcController;
import com.videotron.tvi.illico.stc.schedule.LogTrigger;
import com.videotron.tvi.illico.stc.uploader.LogUploader;
import com.videotron.tvi.illico.stc.util.StcInfo;
import com.videotron.tvi.illico.stc.util.concurrent.Scheduler;
import com.videotron.tvi.illico.log.Log;
//import com.videotron.tvi.illico.stc.uploader.SysLogServerConfigManager;

/**
 * @author sccu
 * @version v 1.0, $Revision: 1.4 $
 * @since sccu, 2010. 1. 22.
 */
public class StcXlet implements Xlet, ApplicationConfig {


    /** xlet context. */
    private XletContext xletContext;

    /** */
    private StcController controller;

    // swkim changed not to use OOB OC. 2011.03.10
    /** the SysLogServerConfigDispatcher instance. */
    //private SysLogServerConfigDispatcher sysLogServerConfigDispatcher;

    /** */
    private CommunicationRequestDispatcher requestDispatcher;

    // swkim changed not to use OOB OC. 2011.03.10
    /** */
    //private SysLogServerConfigManager sysLogServerConfigManager;

    /** the Scheduler instance. */
    private Scheduler scheduler;

    /**
     * Initialize the xlet.
     * @param context - the XletContext instance.
     * @throws XletStateChangeException - If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext context) throws XletStateChangeException {
        xletContext = context;
        FrameworkMain.getInstance().init(this);
        if (Log.DEBUG_ON) {
            //Log.developer.printDebug("STC HOST MAC:" + Misc.getMacAddress());
            Log.printDebug("started building components");
        }

    }


    /**
     * init method for the xlet.
     */
    public void init() {
        try {

        	buildComponets();

        } catch (Exception e) {
        	if (Log.ERROR_ON) {
        		Log.printError("Exception occurs on buildComponents().");
        		e.printStackTrace();
        	}
        }

        if (Log.DEBUG_ON) {
    		Log.printDebug("Leaving initXlet() successfully.");
    	}

    }

    /**
     * Constructs, initialize and starts components.
     * @throws Exception if methods of the components raise an exception during constructing, initializing and starting
     * components.
     */
    private void buildComponets() throws Exception {


    	// swkim changed execution sequence of initialization. 2011.5.19
    	// it is better to recover configuration first than to release external interface.

    	try {
	        controller = new StcController();
	    	controller.initApp(xletContext);
    	} catch (Exception e) {
    		Log.printError("buildComponents, can NOT create or init a StcController");
    		e.printStackTrace();
    	}

    	try {
	    	scheduler = new Scheduler();
	        scheduler.start();
	
			requestDispatcher = CommunicationRequestDispatcher.getInstance();
			
			Log.printDebug("STC : started CommunicationRequestDispatcher");

			requestDispatcher.setXletContext(xletContext);
			requestDispatcher.setScheduler(scheduler);
			requestDispatcher.start();
    	} catch (Exception e) {
    		Log.printError("buildComponents, can NOT create or start a CommunicationRequestDispatcher");
    		e.printStackTrace();
    	}



		// 	swkim changed not to use OOB OC 2011.03.10
		// so removed this SysLogLogServerConfigDispatcher and SysLogServerConfigManger
		// rather added Inband OC in CommunicatinoRequestDispatcher to get Inband OC
		// refer to CommunicationRequestDispatcher


		/*
		sysLogServerConfigDispatcher = new SysLogServerConfigDispatcher();
        sysLogServerConfigDispatcher.setScheduler(scheduler);
        sysLogServerConfigDispatcher.start();
//        SysLogServerConfig config = sysLogServerConfigDispatcher.getSysLogServerConfigFromOc();
//        if(Log.DEBUG_ON) {
//        	Log.developer.printDebug("config = " + config.getHostname() + ":" + config.getPort());
//        }
        SysLogServerConfigManager sysLogServerConfigManager = new SysLogServerConfigManager();
		sysLogServerConfigDispatcher.addListener(sysLogServerConfigManager);
		 */



//		LogUploader uThread = new LogUploader();
//        Thread uploadThread = new Thread(uThread);
//        uploadThread.start();


        LogTrigger tThread = new LogTrigger();
        Thread triggerThread = new Thread(tThread);
        triggerThread.start();


    }


    /**
     * Start the Xlet.
     * @throws XletStateChangeException - is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        FrameworkMain.getInstance().start();
    }

    /**
     * Pause the Xlet.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
    }

    /**
     * Destroys the Xlet.
     * @param unconditional - true if the instance should be destroyed immediately without any condition.
     * @throws XletStateChangeException - is thrown if the Xlet wishes to continue to execute (Not enter
     * the Destroyed state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
    	if (Log.DEBUG_ON) {
    		Log.printDebug("destroyXlet() called. StcXlet:" + this);
    	}
    	clearComponents();

    	FrameworkMain.getInstance().destroy();
    }

    /**
     * Clears components.
     */
    private void clearComponents() {

    	controller.destroyApp();

    	// swkim removed SysLogServerConfigurationDispatcher
    	// 2011.03.11
    	//sysLogServerConfigDispatcher.stop();
    	//sysLogServerConfigDispatcher.removeListener(sysLogServerConfigManager);
    	requestDispatcher.destroy();

    	Log.printDebug("STC : stopped CommunicationRequestDispatcher");


        scheduler.stop();

        requestDispatcher = null;
        // swkim changed 2011.03.11
        //sysLogServerConfigDispatcher = null;
        scheduler = null;
        controller = null;

    }
    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getApplicationName()
     */
    /**
     * @return application name
     */
    public String getApplicationName() {
        return StcInfo.NAME;
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getVersion()
     */
    /**
     * @return application version
     */
    public String getVersion() {
        return StcInfo.VERSION;
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getXletContext()
     */
    /**
     * @return xlet context
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

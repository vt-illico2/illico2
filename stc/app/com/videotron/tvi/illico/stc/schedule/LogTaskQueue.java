package com.videotron.tvi.illico.stc.schedule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.util.StcInfo;

public class LogTaskQueue {

	private static Hashtable taskQueue = null;

	private static LogTaskQueue instance = null;

	public static LogTaskQueue getInstance(){
		if (instance == null ){
			instance = new LogTaskQueue();

		}

		return instance;
	}

    private DateFormat dateFormat = new SimpleDateFormat(StcInfo.DATE_FORMAT);

	protected LogTaskQueue(){
		taskQueue = new Hashtable();
	}

	public synchronized void addLogJob(String schedule, Object buffer){
		if(schedule == null) {
	    	schedule = dateFormat.format(new Date());
		}
		taskQueue.put(schedule, buffer);
	}

	public synchronized ArrayList getJobs(String now){
		ArrayList aList = new ArrayList();

		Enumeration enu = taskQueue.keys();
		String schedule = null;

		while (enu.hasMoreElements()){
			schedule = enu.nextElement().toString();
			//System.out.println("jobSchedule = " + schedule);

			if(now.compareTo(schedule) >=0) {
				aList.add(taskQueue.get(schedule));
				taskQueue.remove(schedule);

				if (Log.DEBUG_ON) {
		    		Log.printInfo(" LogTaskQueue : getJobs : schedule = " + schedule + " size = " + taskQueue.size());
				}
			}
		}

		return aList;
	}
}
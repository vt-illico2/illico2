package com.videotron.tvi.illico.stc.schedule;

import com.videotron.tvi.illico.stc.config.AppConfigManager;
import com.videotron.tvi.illico.stc.config.ApplicationConfig;
import com.videotron.tvi.illico.stc.config.LogConfig;
import com.videotron.tvi.illico.stc.controller.buffer.AppBufferManager;
import com.videotron.tvi.illico.stc.communication.ApplicationProperties;
import com.videotron.tvi.illico.stc.communication.StcServiceImpl;
import com.videotron.tvi.illico.stc.util.StcCode;
//import com.videotron.tvi.illico.stc.communication.ApplicationProperties;

import com.videotron.tvi.illico.stc.util.StcInfo;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.log.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

/**
 * Schedule Job to trigger a command's end.
 * 
 * @author swkim
 *
 */
public class LogTrigger extends TimerTask  {

	/**
	 * task onwer's name. it is an application.
	 */
	private String appName = null;
	
	/**
	 * constructor.
	 * creates new Log Trigger per an application  for a command
	 * @param ownerApp	owner of this LogTrigger.
	 */
	public LogTrigger() {
		// to use this class, check appName and schedule is not null and valid
		//this.appName = ownerApp;
	
		
		if (Log.INFO_ON) {
    		Log.printInfo(" STC, LogTrigger thread started.");
		}
		
	}
	
	/**
	 * start action of LogTrigger.
	 */

	public void run() {


		LogTaskQueue queue = LogTaskQueue.getInstance();
    	DateFormat dateFormat = new SimpleDateFormat(StcInfo.DATE_FORMAT);
    	Calendar cal = null;
    	ArrayList aList = null;
        
		while(true){
		
        	cal = Calendar.getInstance();
        	aList = queue.getJobs(dateFormat.format(cal.getTime()));    	
        
        	for (int i=0; i<aList.size(); i++){
        		trigger(aList.get(i).toString());
        	}
        	
			try{
				Thread.sleep(StcCode.CHECK_SEC);				
			} catch(Exception e){
				if (Log.DEBUG_ON) {
		    		Log.printError(" STC, LogTrigger thread sleep failed with error code " + e.getMessage());
				}
		    }				
		
		}
		
	}
		
		
		
	private void trigger(String appName){	
		final int validZone = 1; // minutes
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("trigger for " + appName + " triggered ");
		}
		
		ApplicationConfig config = LogConfig.getInstance().getConfiguration(appName);
		String expires = config.getExpires();
		
		SimpleDateFormat df1 = new SimpleDateFormat(StcInfo.DATE_FORMAT);
		
						
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, 0-validZone); // from -1 min
		String min = df1.format(cal.getTime());
		
		cal.add(Calendar.MINUTE, 3*validZone); // to 2 min
		String max = df1.format(cal.getTime());
		
		// trigger expires can be updated. only the last trigger should work.
		// so check expired time with thread executed time. 
		// the time can be somewhat changeable between valid zone, so min and max value is considered.
		
		if((expires.compareTo(max) >0) || ( expires.compareTo(min)<0)){

			if (Log.DEBUG_ON) {
	    		Log.printDebug("[STC] " + appName + "'s trigger is updated. Current expiration time is " + expires);
	    		Log.printDebug("[STC] max = " + max + ", min = " + min +  ". So discarding this trigger.");
	    	}

		} else {		
		
			if (Log.DEBUG_ON) {
	    		Log.printDebug("[STC] " + appName + "'s log level is set to default " + ApplicationProperties.getDefaultLogLevel());
	    	}
	
			// change app name's log level off
			//changeAppLogLevel(appName, ApplicationConfig.OFF);
			int logLevel = ApplicationProperties.getDefaultLogLevel();
			
			if(logLevel == ApplicationConfig.OFF) {
				String expDate = LogConfig.getInstance().getConfiguration(appName).getExpires();
				if((expDate != null)&& StcCode.EXP_NEVER.equals(expDate)){
					logLevel = ApplicationConfig.OFF;
					if (Log.DEBUG_ON) {
			    		Log.printDebug("[STC] " + appName + "'s log level was off, so it is off now");
			    	}
				}
			}
			
			changeAppLogLevel(appName, logLevel);
				
			// change STC configuration of app name into off
			//setAppConfig(appName, ApplicationConfig.OFF);
			setAppConfig(appName, logLevel);
			
			// upload buffer of app name 
			uploadBuffer(appName);
			
		}
		
	}
	
	
	/**
	 * set final log level when it is triggered.
	 * @param ownerApp	target application's name
	 * @param finalLevel	final log level when it is triggered.
	 */
	private void setAppConfig(String ownerApp, int finalLevel) {

		// set config manager to set log level off
		AppConfigManager configManager = new AppConfigManager();
		ApplicationConfig appConfig = configManager.getOneLogConfig(ownerApp);
		// if there is no configuration, then create error and return;
		if (appConfig == null) {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("There is no configuration for " + appName 
	    				+ " in LogConfig. Exceptional case!!!");
	    	}
			return;
		}
		appConfig.setLogLevel(finalLevel);
		int ret = configManager.updateLogConfig(appConfig);
		
		if (ret < ApplicationConfig.OFF) {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("Log trigger failed to change " + appName + "'s log level into Off.");
	    	}
		} else {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("Log trigger changed " + appName + "'s configuration into Off.");
	    	}
		}
		
		// after change log config, store configuration into memory.
		configManager.storeConfig();
		
	}
	
	/**
	 * really upload temporary buffer into uploader.
	 * 
	 * @param ownerApp	owner application's name
	 */
	private void uploadBuffer(String ownerApp) {
		
		AppBufferManager bufferManager = new AppBufferManager();
		
		bufferManager.updateLogConfig(ownerApp);
		if (Log.DEBUG_ON) {
    		Log.printDebug("uploaded buffer of  " + appName + " in log trigger");
    	}
	}
	
	
	/**
	 * change log level of owner application.
	 * @param ownerApp owner application's name
	 * @param finalLevel final log level when this job is triggered.
	 * @return return result of change application's log level
	 */
	private boolean changeAppLogLevel(String ownerApp, int finalLevel) {
		
		// even if configuration is not changed, it changes gen app's log level into off to not to collect logs
		// set gen application's log level into off
		//CommunicationRequestDispatcher comDispatcher = CommunicationRequestDispatcher.getInstance();
		//comDispatcher.notifyLogLevelChange(ownerApp, finalLevel);
		
		// change not to used communicationrequestdispatcher.
		
		boolean ret = false;
		
		StcServiceImpl stcServiceImpl = (StcServiceImpl) DataCenter.getInstance().get(StcService.IXC_NAME);
		try {
			ret = stcServiceImpl.notifyLogLevelChange(ownerApp, finalLevel);
		} catch (Exception e) {
			if (Log.DEBUG_ON) {
				Log.printError("unknown error : failed to change log level. " 
						+ e.getMessage());
			}
		}
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("Log trigger changed " + appName + "'s log level into Off.");
    	}
		
		return ret;
	}
}

package com.videotron.tvi.illico.stc.schedule;

import java.util.Calendar;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.util.StcCode;
import com.videotron.tvi.illico.stc.util.StcDuration;
import com.videotron.tvi.illico.stc.util.StcInfo;
import com.videotron.tvi.illico.stc.util.TimerWrapper;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * LogScheudler is a schedule manager for many schedule tasks.
 * it is a singleton object.
 *
 * @author swkim
 *
 */
public class LogScheduler {

	/**
	 * date format used in scheduler.
	 * it is yyyy-MM-dd HH:mm:ss
	 */
	private static final String DATE_FORMAT = StcInfo.DATE_FORMAT;

	/**
	 * instance to be used in this application.
	 */
	private static LogScheduler instance = null;


	/**
	 * Constructor.
	 * it is not public but used in this class only.
	 */
	protected LogScheduler() {
		// do nothing
	}


	/**
	 * method to get instance of LogScheduler.
	 *
	 * @return logScheduler instance.
	 */
	public static LogScheduler getInstance() {
		if (instance == null) {
			instance = new LogScheduler();
		}
		return instance;
	}


	/**
	 * set new schedule and create new LogTrigger object.
	 *
	 * @param appName	owner of buffer
	 * @param schedule	time to be finished of collecting logs
	 * @return	status of adding schedule.
	 */
	public synchronized int addSchedule(String appName, String schedule) {

		if (appName == null) {

			if (Log.DEBUG_ON) {
	    		Log.printInfo("trigger added but appName is null");

	    	}

			return StcCode.STC003;
		}

		if (schedule == null) {

			if (Log.DEBUG_ON) {
	    		Log.printError("trigger is not added because schedule is null");

	    	}

			return StcCode.STC003;
		}

		if (schedule.equals(StcCode.EXP_NEVER)) {
			if (Log.DEBUG_ON) {
	    		Log.printError("[STC] expiration of " + appName + " is never. so do not expired");

	    	}

			return StcCode.STC000;
		}

		// if schedule is duration format, change into date format.
		if (schedule.charAt(0) == 'P') {
			schedule = (new StcDuration()).parseDuration(schedule);
		}

		Date triggerDate = null;

		// swkim changed time format
		// VT chose time format local time.

		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		//df.setTimeZone(TimeZone.getTimeZone("GMT"));
		//df.setTimeZone(TimeZone.getTimeZone(StcInfo.getTimeZone()));

        try {
        	// replace T in the middle
        	// use time format yyyy-MM-dd'T'HH:mm:ss, so no need to change T into ' '
        	//schedule = schedule.replace('T', ' ');
        	// remove last Z
        	if (schedule.charAt(schedule.length() - 1) == 'Z') { // last character
        		schedule = schedule.substring(0, schedule.length() - "Z".length());
        	}
        	triggerDate = df.parse(schedule);
        } catch (Exception e) {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("trigger schedule " + schedule + " is invalid");
        	}
        	return StcCode.STC003;
		}

//        if (Log.DEBUG_ON) {
//    		Log.developer.printDebug("create scheduler thread with " + appName + ", " + schedule);
//   	}

		// trigger task :
		// triggerDate.getTime() - now.getTime() is time difference between now and scheduled date in long type
		// schedule is GMT, but now is local time. But in SimpleDateFormatter, schedule is set to GMT.
		// thus schedule(GMT) - now(local) is valid.

		if (Log.DEBUG_ON) {
    		Log.printDebug("created 'Timer' scheduler thread.");
    	}

		Calendar cal = Calendar.getInstance();
		//cal.setTimeZone(TimeZone.getTimeZone(StcInfo.getTimeZone()));
		Date now = cal.getTime();


		if (Log.DEBUG_ON) {
			Log.printDebug("now : request = " + triggerDate.getTime()  + ",  " + triggerDate);
    		Log.printDebug("now : Canada  = " + now.getTime()  + ",  " + now);
    	}


		long delay = triggerDate.getTime() - now.getTime();

		if (Log.DEBUG_ON) {
			Log.printDebug("fire delay = " + delay);
    	}

		if(delay < 0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("fire delay is negative. so trigger it now!");
	    	}
			schedule = null;

		} else {

	        //TimerWrapper trigger = new TimerWrapper();
			//trigger.schedule(new LogTrigger(appName), delay);

			if (Log.DEBUG_ON) {
	    		Log.printDebug("set schedule to Timer thread at " + schedule);
	    	}

		}


		LogTaskQueue queue = LogTaskQueue.getInstance();
		queue.addLogJob(schedule, appName);



		return StcCode.STC000;
	}
}

package com.videotron.tvi.illico.stc.schedule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.util.StcInfo;

public class LogJobQueue {

	private static Hashtable jobQueue = null;

	private static LogJobQueue instance = null;

	public static LogJobQueue getInstance(){
		if (instance == null ){
			instance = new LogJobQueue();

		}

		return instance;
	}

    private DateFormat dateFormat = new SimpleDateFormat(StcInfo.DATE_FORMAT); // yyyy-MM-dd'T'HH:mm:ss

	protected LogJobQueue(){
		jobQueue = new Hashtable();
	}

	public synchronized void addLogJob(String schedule, Object buffer){
		if(schedule == null) {
	    	schedule = dateFormat.format(new Date());

			if (Log.DEBUG_ON) {
	    		Log.printInfo(" LogJobQueue : addLogJob : schedule is null.");
			}
		}
		jobQueue.put(schedule, buffer);

		if (Log.DEBUG_ON) {
    		Log.printInfo(" LogJobQueue : addLogJob : schedule =." + schedule + " size = " + jobQueue.size());
		}


	}

	public synchronized ArrayList getJobs(String now){
		ArrayList aList = new ArrayList();

		Enumeration enu = jobQueue.keys();
		String schedule = null;

		if (Log.EXTRA_ON) {
    		Log.printDebug(" LogJobQueue : getJobs : before size = " + jobQueue.size());
		}
		
		while (enu.hasMoreElements()){
			schedule = enu.nextElement().toString();
			//System.out.println("jobSchedule = " + schedule);

			if(now.compareTo(schedule) >=0) {
				aList.add(jobQueue.get(schedule));
				jobQueue.remove(schedule);


				if (Log.DEBUG_ON) {
		    		Log.printDebug(" LogJobQueue : getJobs : schedule = " + schedule + " size = " + jobQueue.size());
				}

			}
		}
		
		if (Log.EXTRA_ON) {
    		Log.printDebug(" LogJobQueue : getJobs : after size = " + jobQueue.size());
		}

		return aList;
	}
}

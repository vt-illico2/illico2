/*
 *  Task.java    $Revision: 1.1 $ $Date: 2014/04/03 14:02:34 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.stc.util.concurrent;


/**
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 25.
 */
public interface Task extends Runnable {
    /**
     * @return task name.
     */
    String getName();
}

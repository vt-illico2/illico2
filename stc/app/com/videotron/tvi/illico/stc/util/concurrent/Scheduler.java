/*
 *  Scheduler.java    $Revision: 1.1 $ $Date: 2014/04/03 14:02:34 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.stc.util.concurrent;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.log.Log;


/**
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 25.
 */
public class Scheduler {

    /** the Map instance containing the relation between the task's name and its schedule. */
    private final Map jobMap = new HashMap();

    /** the Thread instance which executes the tasks in the task queue. */
    private final SingleThreadedExecutor executor = new SingleThreadedExecutor();

    /** the TVTimer instance. */
    private final TVTimer tvTimer;

    /**
     * This class implements {@link TVTimerWentOffListener} for Scheduler.
     * @author sccu
     */
    class ScheduledJob implements Runnable, TVTimerWentOffListener {

        /** the task instance. */
        private final Task task;

        /** the TaskSchedule instance. */
        private final TaskSchedule schedule;

        /** the TVTimerSpec instance. */
        private TVTimerSpec timerSpec;

        /**
         * Constructs the TimerListener object with the specified task instance.
         * @param taskInstance - a task instance that will be executed at the scheduled time.
         * @param taskSchedule - delay in milliseconds.
         * @throws Exception if fails to invoke {@link TVTimer#scheduleTimerSpec(TVTimerSpec)}.
         */
        public ScheduledJob(final Task taskInstance, final TaskSchedule taskSchedule) throws Exception {
            if (taskInstance == null) {
                throw new IllegalArgumentException();
            }

            if (taskSchedule == null) {
                throw new IllegalArgumentException();
            }

            this.task = taskInstance;
            this.schedule = taskSchedule;

            TVTimerSpec spec = new TVTimerSpec();
            spec.addTVTimerWentOffListener(this);
            spec.setAbsoluteTime(schedule.getFirstFireTime().getTime());
            tvTimer.scheduleTimerSpec(spec);
        }

        /**
         * Implements the call-back method of TVTimerWentOffListener. This method put the its task into the task queue.
         * The task will be executed in its turn within the task queue.
         * @param event - the {@link TVTimerWentOffEvent} object.
         */
        public void timerWentOff(TVTimerWentOffEvent event) {
            executor.execute(this);
        }

        /**
         */
        public synchronized void clear() {
            if (timerSpec != null) {
                tvTimer.deschedule(timerSpec);
                timerSpec.removeTVTimerWentOffListener(this);
                timerSpec = null;
            }

        }

        /*
         * (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        /**
         * Overrides {@link java.lang.Runnable#run()} to execute the scheduled task.
         */
        public void run() {
            try {
                task.run();
            } finally {
                if (schedule.isRepeat()) {
                    tvTimer.deschedule(timerSpec);

                    timerSpec.setAbsoluteTime(schedule.getFirstFireTimeAfter(new Date()).getTime());
                    try {
                        tvTimer.scheduleTimerSpec(timerSpec);
                    } catch (TVTimerScheduleFailedException e) {
                    	if (Log.ERROR_ON) {
                    		Log.printError("TVTimerScheduleFailedException occurs");
                    	}
                    }
                } else {
                    jobMap.remove(task.getName());
                    clear();
                }
            }
        }
    }

    /**
     * @return the executor instance.
     */
    protected SingleThreadedExecutor getExecutor() {
        return executor;
    }

    /**
     * Constructs the Scheduler instance.
     */
    public Scheduler() {
        this.tvTimer = TVTimer.getTimer();
    }

    /**
     * Prepares the instance. After this method returns, the instance starts the service. This method should be called
     * first among the starts() methods of all modules.
     */
    public synchronized void start() {
    	if (Log.INFO_ON) {
    		Log.printInfo("Scheduler started.");
    	}
    }

    /**
     * Stops the service of the instance. This method stops the service of the instance.
     */
    public synchronized void stop() {
    	if (Log.INFO_ON) {
    		Log.printInfo("Scheduler stopped.");
    	}
    }

    /**
     * Execute the task as asynchronously. This method will exit without executing task after stop() method is called.
     * @param task task.
     */
    public void execute(Task task) {
    	if (Log.DEBUG_ON) {
    		Log.printDebug("Puts a task: " + task.getName() + " into a queue.");
    	}
        executor.execute(task);
    }

    /**
     * Schedules the specified task. If the task with the same name has already been scheduled, the latter will be
     * ignored.
     * @param task - the task instance.
     * @param schedule - the schedule instance of the task.
     */
    public synchronized void scheduleIfNotExists(Task task, TaskSchedule schedule) {
        if (!jobMap.containsKey(task.getName())) {
            schedule(task, schedule);
        } else {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("Already scheduled task: " + task.getName());
        	}
        }
    }

    /**
     * @param task task
     * @param schedule - the schedule when the task will be executed.
     */
    public synchronized void schedule(Task task, TaskSchedule schedule) {
        if (task == null) {
            throw new IllegalArgumentException("Task should not be null.");
        }

        ScheduledJob prevJob = null;
        try {
            prevJob = (ScheduledJob) jobMap.put(task.getName(), new ScheduledJob(task, schedule));
        } catch (Exception e) {
        	if (Log.ERROR_ON) {
        		Log.printError("TVTimerScheduleException occurred in ScheduledJob.");
        	}
        }

        if (prevJob != null) {
            prevJob.clear();
        }
    }

    /**
     * Reschedule the specified task with the specified schedule. If the task is already scheduled, the previous
     * schedule will be canceled. Otherwise, the result is the same as a method {@link #schedule(Task, TaskSchedule)}.
     * @param task - the task already scheduled.
     * @param schedule - the schedule when the task will be executed.
     */
    public synchronized void reschedule(Task task, TaskSchedule schedule) {
        deschedule(task);
        schedule(task, schedule);
    }

    /**
     * Cancels the specified task that was already scheduled. If the task has already executed, nothing happens. This
     * method calls {@link #deschedule(String)} internally.
     * @param task the task already scheduled.
     */
    public synchronized void deschedule(Task task) {
        deschedule(task.getName());
    }

    /**
     * Cancels the task with the specified name. If the task has already executed, nothing happens.
     * @param taskName the name of the task already scheduled.
     */
    public synchronized void deschedule(String taskName) {
    	if (Log.DEBUG_ON) {
    		Log.printDebug("Entering deschedule(\"" + taskName + "\").");
    	}

        ScheduledJob job = (ScheduledJob) jobMap.remove(taskName);
        if (job != null) {
            job.clear();
        }
    }
}

package com.videotron.tvi.illico.stc.util;

/**
 * code used in STC.
 * it is shared with STC Client.
 * 
 * @author swkim
 *
 */
public class StcCode {

	/**
	 * constructor for utility class. do nothing.
	 */
	protected StcCode() {
		// do nothing.
	}
/**
 * STC Code. code for success
 */
	public static final int STC000 = 0; // success
	/**
	 * STC Code. code for invalid method name.
	 */
	public static final int STC001 = -1; // Invalid method name.
	/**
	 * STC Code. code for invalid parameter.
	 */
	public static final int STC002 = -2; // Unknown parameter(s) specified.
	/**
	 * STC Code. Code for invalid value.
	 */
	public static final int STC003 = -3; // at least one of the parameter values is invalid
	
	/**
	 * STC Code. code for invalid PIN id.
	 */
	
	public static final int STC004 = -4; // Invalid PIN identifier specified.
	/**
	 * STC Code. code for invalid PIN specifiied
	 */
	public static final int STC005 = -5; // Invalid PIN specified.
	/**
	 * STC Code. code for invalid UPP
	 */
	public static final int STC006 = -6; // UPP
	/**
	 * STC Code. code for failure updating logging level
	 */
	public static final int STC007 = -7; // updating logging level failed
	/**
	 * STC Code. code for failure setting
	 */
	public static final int STC008 = -8; // setting
	/**
	 * STC Code. code for failure retrieving
	 */
	public static final int STC009 = -9; // could not retrieve logging level
	

	/**
	 * STC Code. log level of off
	 */
	public static final int LOG_OFF = 0; 
	

	/**
	 * STC Code. log level 1
	 */
	public static final int LOG_1 = 1; 

	/**
	 * STC Code. log level 2
	 */
	public static final int LOG_2 = 2; 
	
	
	/**
	 * STC Code. log level of error
	 */
	public static final int LOG_ERROR = 3; 
	
	/**
	 * STC Code. log level of warning
	 */
	public static final int LOG_WARNING = 4; 
	
	/**
	 * STC Code. log level 5
	 */
	public static final int LOG_5 = 5; 
	
	/**
	 * STC Code. log level of info
	 */
	public static final int LOG_INFO = 6; 
	
	/**
	 * STC Code. log level of debug
	 */
	public static final int LOG_DEBUG = 7; 
	
	/**
	 * STC Code. true for notify
	 */
	public static final String LOG_TRUE = "TRUE"; 
	
	/**
	 * STC Code. false for notify
	 */
	public static final String LOG_FALSE = "FALSE"; 
	
	/**
	 * hexa decimal value for parsing byte array into int.
	 */
	public static final int SHIFT_HEXA = 0x000000FF; 
	
	/**
	 * Sleep time to get configuration .
	 */
	public static final int SLEEP_TIME = 1000; 
							
	
	/**
	 * daemon input parameter.
	 */
	public static final String [] KEYS = {"application", "level", "expires", "buffer", "notify"};
	
	/**
	 * STC Code. expiration never
	 */
	public static final String EXP_NEVER = "NEVER"; 
	

	/**
	 * STC Code. empty string
	 */
	public static final String EMPTY_STRING = ""; 

	/**
	 * STC Code. empty string
	 */
	public static final String NULL_STRING = "null";
	
	
	
	/**
	 * milisecond .
	 */
	public static final long MILISEC = 100l; 
	
	
	
	/**
	 * milisecond .
	 */
	public static final long CHECK_SEC = 1000L; 
	
	
	
}



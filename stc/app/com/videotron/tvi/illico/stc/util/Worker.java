package com.videotron.tvi.illico.stc.util;

import com.videotron.tvi.illico.log.Log;
import java.util.*;

public class Worker extends Vector implements Runnable {

    Thread thread;
    boolean keepThread;
    String threadName;

    public Worker(String name) {
        this(name, true);
    }

    public Worker(String name, boolean keepThread) {
        super(20);
        this.threadName = name;
        this.keepThread = keepThread;
        if (keepThread) {
            startNewThread();
        }
    }

    private synchronized void startNewThread() {
        Thread t = new Thread(this, threadName);
        t.start();
        thread = t;
    }

    public synchronized void push(Runnable r) {
        if (contains(r)) {
            Log.printInfo("Worker: already added = " + r);
            return;
        }
        addElement(r);
        if (Log.INFO_ON) {
            Log.printInfo("Worker: added = " + r);
        }
        if (keepThread) {
            notify();
        } else {
            if (thread == null) {
                startNewThread();
            }
        }
    }

    public void run() {
        Runnable r;
        while (true) {
            synchronized (this) {
                while (size() <= 0) {
                    if (keepThread) {
                        try {
                            this.wait();
                        } catch (InterruptedException ignored) {
                        }
                    } else {
                        thread = null;
                        return;
                    }
                }
                r = (Runnable) remove(0);
            }
            if (Log.INFO_ON) {
                Log.printInfo("Worker: processing " + r);
            }
            try {
                r.run();
            } catch (Throwable t) {
                Log.print(t);
            }
        }
    }
}

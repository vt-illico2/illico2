package com.videotron.tvi.illico.stc.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.TimeZone;


/**
 * Utility class to convert Duration into Date.
 *
 * @author swkim
 *
 */
public class StcDuration {

	/**
	 * data format used in STC.
	 */
	private static final String DATE_FORMAT = StcInfo.DATE_FORMAT;
	// but stores date with yyyy-MM-dd hh:mm:ssZ format to present GMT

	/**
	 * validator for small number.
	 */
	private final int minHundred = 100;
	/**
	 * year defined in Duration.
	 */
	private int year;
    /**
     * month defined in Duration.
     */
    private int month;
    /**
     * day defined in Duration.
     */
    private int day;
    /**
     * hour defined in Duration.
     */
    private int hour;
    /**
     * minute defined in Duration.
     */
    private int minute;
    /**
     * second defined in Duration.
     */
    private int second;


    /**
     * utility function to change duration format into real date.
     *
     * @param duration	duration used in a command.
     * @return	validity of duration.
     */
    public boolean checkDurationValidity(String duration) {

    	// if duration is null, then it means duration is not set,
    	// it can not be checked in this method.
    	// thus it returns true;

    	if (duration == null) {
    		return false;
    	}
    	if (duration.equals("")) {
    		return false;
    	}

    	// check the case the duration is set and the value is duration
    	// if it is duration, it should be checked later
    	// thus it returns true;
    	if (duration.charAt(0) == 'P') {
    		return true;
    	}

    	SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);

    	try {
    		df.parse(duration);
    	} catch (Exception e) {
    		return false;
    	}

    	return true;
    }



    /**
     * utility function to change duration format into real date.
     *
     * @param date	duration used in a command.
     * @return	validity of date.
     */
    public boolean checkDateValidity(String date) {

    	String cDate = date;
    	// date should be null;
    	// date format  yyyy-MM-dd'T'HH:mm:ss or yyyy-MM-dd'T'HH:mm:ssZ. thus date.length size is predefined.
    	if((date.length() + 1) == StcInfo.DATE_FORMAT.length()){

    		// date should contain 'z' or 'Z' at last position of date string
    		if((date.charAt(date.length()-1) == 'Z') || (date.charAt(date.length()-1) == 'z')){
    			cDate = date.substring(0, date.length()-1);
    		} else {
    			return false;
    		}

    	}else if((date.length() + 2) == StcInfo.DATE_FORMAT.length()) {

    		// it does not contains 'z' or 'Z' at last position of date string
    		cDate = date;

    	} else {
    		return false;
    	}

    	SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);

    	try {
    		df.parse(cDate);
    	} catch (Exception e) {
    		return false;
    	}

    	return true;
    }


    /**
     * utility function to change duration format into real date.
     *
     * @param duration	duration used in a command.
     * @return	converted date.
     */
    public String parseDuration(String duration) {

    	year = 0;
    	month = 0;
    	day = 0;
    	hour = 0;
    	minute = 0;
    	second = 0;

    	if (duration == null) {
    		return null;
    	}
    	if (duration.length() < 1) {
    		return null;
    	}
    	// it is date format, so do not parse and return
    	if (duration.charAt(0) != 'P') {
    		return duration;
    	}

    	if (!parseDurationString(duration)) {
    		return null;
    	}


		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);

		// 2011.04.11
		// changed time format into Canada/Eastern

		//formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		//return formatter.format(new Date(calculateLong())) + "Z";

		// changed to use default time zone
		//formatter.setTimeZone(TimeZone.getTimeZone(StcInfo.getTimeZone()));
		return formatter.format(new Date(calculateLong()));
    }


    /**
     * parse duration string into and understand.
     *
     * @param duration	input value defined in a command or default setting.
     * @return	whether is is parsed correctly or not.
     */
    private boolean parseDurationString(String duration) {
        int position = 1;
        int timePosition = duration.indexOf("T");

        if (duration.indexOf("P") == -1 || duration.equals("P")) {
            return false;
        }
        if (duration.lastIndexOf("T") == duration.length() - 1) {
            return false;
        }
        if (timePosition != -1) {
            if (!parseTime(duration.substring(timePosition + 1))) {
            	return false;
            }
        } else {
            timePosition = duration.length();
        }

        if (position != timePosition) {
            if (!parseDate(duration.substring(position, timePosition))) {
            	return false;
            }
        }

        return true;
    }

    /**
     * parse time format defined in duration.
     *
     * @param time time value defined in duration.
     * @return	whether time is parsed correctly or not.
     */
    private boolean parseTime(String time) {
        if (time.length() == 0 || time.indexOf("-") != -1) {
        	return false;
        }
        if (!time.endsWith("H") && !time.endsWith("M") && !time.endsWith("S")) {
            return false;
        }

        int start = 0;

        int end = time.indexOf("H");
        if (start == end) {
        	return false;
        }
        if (end != -1) {
            hour = Integer.parseInt(time.substring(0, end));
            start = end + 1;
        }
        end = time.indexOf("M");
        if (start == end) {
        	return false;
        }

        if (end != -1) {
            minute = Integer.parseInt(time.substring(start, end));
            start = end + 1;
        }
        end = time.indexOf("S");
        if (start == end) {
        	return false;
        }

        if (end != -1) {
            this.second = (int) (Math.round(Double.parseDouble(time.substring(start, end)) * minHundred) / minHundred);
        }

        return true;
    }

    /**
     * parse date string in duration.
     *
     * @param date	date string in duration
     * @return	whether date string is parsed correctly or not.
     */
    private boolean parseDate(String date) {

    	if (date.length() == 0 || date.indexOf("-") != -1) {
            return false;
        }

        if (!date.endsWith("Y") && !date.endsWith("M") && !date.endsWith("D")) {
            return false;
        }

        int start = 0;
        int end = date.indexOf("Y");

        if (start == end) {
        	return false;
        }
        if (end != -1) {
            year = Integer.parseInt(date.substring(0, end));
            start = end + 1;
        }

        end = date.indexOf("M");
        if (start == end) {
        	return false;
        }
        if (end != -1) {
            month = Integer.parseInt(date.substring(start, end));
            start = end + 1;
        }

        end = date.indexOf("D");

        if (start == end) {
        	return false;
        }
        if (end != -1) {
            day = Integer.parseInt(date.substring(start, end));
        }

        return true;
    }


    /**
     * convert temporary date values into real time value in long format.
     *
     * @return	actual date in long format.
     */
    private long calculateLong() {

    	Calendar cal = Calendar.getInstance();
    	cal.setTimeZone(TimeZone.getTimeZone(StcInfo.TIME_ZONE));

    	cal.add(Calendar.YEAR, year);
    	cal.add(Calendar.MONTH,  month);
    	cal.add(Calendar.DATE, day);
    	cal.add(Calendar.HOUR, hour);
    	cal.add(Calendar.MINUTE, minute);
    	cal.add(Calendar.SECOND, second);

    	return cal.getTimeInMillis();
    }

}

package com.videotron.tvi.illico.stc.util;

import org.ocap.hardware.Host;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author sccu
 */
public final class Misc {

    /** maximum unsigned byte value. */
    private static final int MAX_UBYTE = 255;
    
    /** maximum unsigned byte value. */
    private static final int SIZE_OCT = 256;
    
    /** byte mask value. */
    private static final int BYTE_MASK = 0xFF;

    /** MAC size. */
    private static final int LENGTH_HOST = 6;
    
    /** MAC size. */
    private static final int LOC_MAC = 6;
    
    /** MAC size. */
    private static final int SIZE_BYTE = 6;
    
    /** MAC size. */
    private static final int BASE_INT = 6;
    
    
    /** system line separator. */

    /** submission-format line separator. */
    public static final String SUBMISSION_LINE_SEPARATOR = "\r\n";
    
    /** host mac. */
    private static String mac = null;
   
    /**
     *
     */
    private Misc() {
    }

    /** host mac array. */
    
    
    private static final byte[] HOSTMAC = null;
    /*    
    static {
    	HOSTMAC = new byte[LENGTH_HOST];
        try {
        		String hostMacString = Host.getInstance().getReverseChannelMAC();
                for (int x = 0; x < LENGTH_HOST; x++) {
                    HOSTMAC[x] = ubyteToByte(Integer.parseInt(hostMacString.substring(x * LOC_MAC, 
                    		x * LOC_MAC + SIZE_BYTE), BASE_INT));
                }
        } catch (Exception e) {
        	if (Log.WARNING_ON) {
				Log.developer.print(e);
			}
        }
    }
    */
    
    /**
     * Get host mac string.
     * @return host mac string.
     */
    
    /*
    public static String getHostMacString() {
    	if (Log.DEBUG_ON) {
            Log.developer.printDebug("getHostMacString");
        }
    	if (mac == null) {
    		try {
    			if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
    					|| Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
    				mac = toHexString(getHostMac());
    			} else {
    				MonitorService monitor = 
    					(MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
    				if (monitor != null) {
    					byte[] addr = monitor.getCableCardMacAddress();
    					if (Log.DEBUG_ON) {
        		            Log.developer.printDebug("get monitor " + addr.length);
        		        }
    					mac = toHexString(addr);
    					if (Log.DEBUG_ON) {
        		            Log.developer.printDebug("MAC " +  mac);
        		        }
    				}
    			}
    		} catch (Exception e) {
    			if (Log.WARNING_ON) {
    				Log.developer.print(e);
    			}
    			return "";
    		}
    	}
    	return mac;
    }

    
    */
    

    /**
     * set MacAddress.
     * 2012.05.15 changed not to make recursive call
     */
    
    
    public static String getMacAddress() {

    	// 2012.04.16
    	// added coded to remove duplicated request to get MAC
        if (!(mac == null || mac.length() == 0)) {
            return mac.toUpperCase();            
        }
    	
    	
    	if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
                || Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
            mac = Host.getInstance().getReverseChannelMAC();
            String[] token = TextUtil.tokenize(mac, ":");
            StringBuffer address = new StringBuffer();
            for (int i = 0; i < token.length; i++) {
                address.append(token[i]);
            }
            mac = address.toString();
        } else {
        	MonitorService monitor = null;
        	boolean gotValue = false;
            do {
            	try {
            		if (monitor == null) {
		                monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            		}
            		
	                if (monitor != null) {
	                    byte[] addr = monitor.getCableCardMacAddress();
	                    //mac = Util.getStringBYTEArray(addr);
	                    mac = toHexString(addr);
	                
		                if((mac != null) && (mac.length() > 0)) {
		                	gotValue = true;
		                } else {
		                	Thread.sleep(StcCode.SLEEP_TIME);
		                }
	                }
	                
	            } catch (Exception e) {
	                if (Log.WARNING_ON) {
	                	Log.print(e);
	                }
	            } finally {
	            	if(gotValue) monitor = null;
	            }
            }while(!gotValue);
        	
        }
    	
    	
        return mac.toUpperCase();            

    }
    
    
/*
    public static String getMacAddress() {

    	// 2012.04.16
    	// added coded to remove duplicated request to get MAC
        if (!(mac == null || mac.length() == 0)) {
            return mac.toUpperCase();            
        }
    	
    	
    	if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
                || Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
            mac = Host.getInstance().getReverseChannelMAC();
            String[] token = TextUtil.tokenize(mac, ":");
            StringBuffer address = new StringBuffer();
            for (int i = 0; i < token.length; i++) {
                address.append(token[i]);
            }
            mac = address.toString();
        } else {
        	MonitorService monitor = null;
            try {
                monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
                if (monitor != null) {
                    byte[] addr = monitor.getCableCardMacAddress();
                    //mac = Util.getStringBYTEArray(addr);
                    mac = toHexString(addr);
                    
                }
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                	Log.developer.print(e);
                }
            } finally {
            	monitor = null;
            }
        }

        if (mac == null || mac.length() == 0) {
            try {
                Thread.sleep(StcCode.SLEEP_TIME);
            } catch (Exception e) {
            	Log.developer.print(e);
            }
            return getMacAddress();
        } else {
            return mac.toUpperCase();            
        }
    }
*/    
    
    /**
     * Gets host mac array.
     * @return host mac array.
     */
    private static byte[] getHostMac() {
        return HOSTMAC;
    }

    /**
     * Converts byte array to hex String.
     * @param byteArray byte array.
     * @return host mac represented as a String object.
     */
    public static String toHexString(final byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }

        StringBuffer buf = new StringBuffer(byteArray.length);

        for (int i = 0; i < byteArray.length; i++) {
            String hex = Integer.toHexString(byteArray[i] & BYTE_MASK).toUpperCase();
            if (hex.length() == 1) {
                buf.append('0').append(hex);
            } else {
                buf.append(hex);
            }
        }

        return buf.toString();
    }

    /**
     * @param array the array to be copied.
     * @return a copy of the original array.
     */
    public static int[] copyOf(int[] array) {
        int[] arr = new int[array.length];
        for (int x = 0; x < array.length; x++) {
            arr[x] = array[x];
        }
        return arr;
    }

    /**
     * Converts signed byte into unsigned byte.
     * @param b - signed byte
     * @return unsigned byte value
     */
    public static int byteToUbyte(byte b) {
        return (SIZE_OCT + b) % SIZE_OCT;
    }

    /**
     * Converts an unsigned byte into signed byte.
     * @param ubyteValue - unsigned byte value.
     * @return byte value.
     */
    public static byte ubyteToByte(int ubyteValue) {
        if (ubyteValue < 0 || MAX_UBYTE < ubyteValue) {
            throw new IllegalArgumentException("version value is out of range:" + ubyteValue);
        }
        if (ubyteValue > Byte.MAX_VALUE) {
            ubyteValue -= SIZE_OCT;
        }
        return (byte) ubyteValue;
    }
}

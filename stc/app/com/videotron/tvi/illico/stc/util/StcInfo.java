package com.videotron.tvi.illico.stc.util;

/**
 * general code class used in STC.
 *
 * @author swkim
 *
 */
public class StcInfo {

	/**
	 * constructor.
	 * do nothing.
	 */
	protected StcInfo() {

	}
	/**
	 * STC Application Name.
	 * @return	application name
	 */
	public static String NAME = "STC";

	/**
	 * STC application version.
	 * @return	application version
	 */
	public static final String VERSION = "1.4.0.0";

	/**
	 * prefix used in STC for shared memory.
	 * @return prefix in STC
	 */
    public static final String PREFIX = "stc-";

	/**
	 * date format used in STC.
	 * @return	date format
	 */
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	/**
	 * simple date format used in STC.
	 * @return	date format
	 */
	public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * simple time format used in STC.
	 * @return	time format
	 */
	public static final String SIMPLE_TIME_FORMAT = "HH:mm:ss";

	/**
	 * time zone used in STC.
	 * @return	time zone
	 */
	public static final String TIME_ZONE = "America/Montreal";
}

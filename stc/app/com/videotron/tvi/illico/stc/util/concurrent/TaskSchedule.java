/*
 *  TaskSchedule.java    $Revision: 1.1 $ $Date: 2014/04/03 14:02:34 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.stc.util.concurrent;

import java.util.Date;

/**
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 25.
 */
public interface TaskSchedule {

    /**
     * @return true if the specification is repeated.
     */
    boolean isRepeat();

    /**
     * @return the remaining time in Date format.
     */
    Date getFirstFireTime();

    /**
     * @param origin - the origin time.
     * @return the remaining time to the next triggering after the specified time, in Date format.
     */
    Date getFirstFireTimeAfter(Date origin);
}

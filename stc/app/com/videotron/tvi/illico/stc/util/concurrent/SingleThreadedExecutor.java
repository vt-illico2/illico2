/**
 */
package com.videotron.tvi.illico.stc.util.concurrent;

import com.videotron.tvi.illico.log.Log;

/**
 * @author sccu
 */
public class SingleThreadedExecutor {

	/** the BlockingQueue instance containing tasks. */
	private final BlockingQueue taskQueue = new BlockingQueue(Integer.MAX_VALUE);

	/** the Thread instance which executes the tasks in the task queue. */
	private Thread thread;

	/**
	 * Puts the task into the internal executor. The task will be executed by
	 * the internal executor's thread.
	 * 
	 * @param task
	 *            - the task to be executed.
	 */
	public synchronized void execute(Runnable task) {
		try {
			taskQueue.put(task);
		} catch (InterruptedException e) {
			throw new IllegalStateException("TaskQueue shutdowned.");
		}

		if (thread == null) {
			thread = createExecutorThread();
			thread.start();
		}
	}

	/**
	 * Creates an executor thread.
	 * 
	 * @return new executor thread.
	 */
	private Thread createExecutorThread() {
        return new Thread() {
            public void run() {
            	if (Log.INFO_ON) {
            		Log.printInfo("scheduler thread starts.");
            	}

                Runnable task = null;
                while (!taskQueue.isShutdown()) {
                    try {
                        synchronized (SingleThreadedExecutor.this) {
                            task = (Runnable) taskQueue.takeNoWait();
                            if (task == null) {
                                thread = null;
                                break;
                            }
                        }
                        
                        if (Log.DEBUG_ON) {
                    		Log.printDebug("Starts task:" + task.toString());
                    	}

                        task.run();
                        
                        if (Log.DEBUG_ON) {
                    		Log.printDebug("Ends task:" + task.toString());
                    	}
                        
                    } catch (Exception e) {
                        if (task != null) {
                        	if (Log.ERROR_ON) {
                        		Log.printDebug("Exception occurs during [" + task.toString() 
                        				+ "]: " + e.getMessage());
                        		Log.print(e);
                        	}
                        }
                    }
                } // end of while
                
                if (Log.INFO_ON) {
            		Log.printInfo("scheduler thread exits.");
            	}
            }
        };
    }
	/**
	 * @return true if the running thread exists.
	 */
	public synchronized boolean isRunning() {
		return thread != null && thread.isAlive();
	}

}

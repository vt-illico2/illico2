package com.videotron.tvi.illico.stc.util;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.config.ApplicationConfig;
import com.videotron.tvi.illico.util.TextUtil;

import java.text.StringCharacterIterator;


/**
 * utility class to parse log message into syslog format.
 *
 */
public class BufferUtil {

	/**
	 * elements in a log string for syslog.
	 */
	private static final int ELE_LEN = 5;
	/**
	 * elements to be checked in log string.
	 * they are 0,1 and 2.
	 */
	private static final int CHECK_LEN = 3; // tmpArr 0, 1, 2.
	/**
	 * syslog protocol version.
	 */
	private static final String VERSION = "1";
	/**
	 * parameter name for syslog format.
	 */
	private static final String PARAM_NAME = "APP_VER";
	/**
	 * index for data in parsed log string.
	 */
	private static final int IDX_DATE = 0;
	/**
	 * index for data in parsed log string.
	 */
	private static final int IDX_LEVEL = 1;
	/**
	 * index for data in parsed log string.
	 */
	private static final int IDX_NAME = 2;
	/**
	 * index for data in parsed log string.
	 */
	private static final int IDX_VERSION = 3;
	/**
	 * index for data in parsed log string.
	 */
	private static final int IDX_MSG = 4;

	/**
	 * token for parse log string.
	 */
	private static final String TOKEN = " ";

	/**
	 * facility code defined in syslog server for STC.
	 */
	private static final int FACILITY_CODE = 19;
	/**
	 * log type defined in syslog.
	 */
	private static final int LOG_KINDS = 8;
	/**
	 * added parameter used in syslog.
	 */
	private static final String SD_ID = StcInfo.NAME;

	/**
	 * separator defined in syslog format.
	 */
	private static final String SP = " ";

	/**
	 * nil value defined in syslog format.
	 */
	private static final String NILVALUE = "-";
	/**
	 * start index for checking parsed string.
	 */
	private static final int INIT_SIMPLE_PARSE = 3;

	/**
	 * bom defined in syslog format.
	 */
	private static final byte[] BOM = new byte[] {(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};


	/**
	 * public method to change logs in STC to syslog format.
	 * @param src	source log string in STC
	 * @param mac	machine uniq id
	 * @param index	log index
	 * @return	converted string for syslog server
	 */
	public synchronized String changeSysLogFormat(String src, String mac, long index) {
		// log format would be :

		String[] elements = parseSimpleLog(src);
		// elements 0 is timestamp, 1 is log level, 2 is app name, 3 is app
		// version
		// 4 is method name and 5 is message

		if (elements == null) {
			return null;
		}
		if (elements.length < ELE_LEN) {
			return null; // false message format
		}

		int prival = FACILITY_CODE * LOG_KINDS + getLevel(elements[IDX_LEVEL]);
		// rule in http://tools.ietf.org/html/rfc5424#page-8
		String header = "<" + prival + ">" + // PRIVAL
				VERSION + SP + elements[IDX_DATE] + SP + // elements 0 is timestamp
				utftoascii(mac) + SP + // mac is hostname
				utftoascii(elements[IDX_NAME]) + SP + // elements 2 is app name
				//utftoascii(elements[4]) + SP + // elements 4 is method name
				NILVALUE + SP + // PROCID is set to NILVALUE  --> changed to PROCID
				utftoascii(String.valueOf(index));

		String structuredData = "[" + utftoascii(SD_ID) + SP
		+ utftoascii(PARAM_NAME) + "=" + elements[IDX_VERSION] + "]"; // elements 3 is app version

		String outString = header + SP + structuredData + SP + BOM + elements[IDX_MSG];
		// element 5 is a message, --> 4 is a message
		if (Log.DEBUG_ON) {
    		Log.printDebug("changeSysLogFormat : " + outString);
    	}
		return outString;
	}

	/**
	 *
	 * simple parser for stc log.
	 *
	 * log string : 2010112231010203Z #<EPG 0.2.25> FieldedTextReader.read :
	 * resource/text/menu.txt
	 *
	 * PRIVAL = FACILITY CODE + SEVERITY LEVEL // I want to use “19” for
	 * facility code and concatenate 1 digit for severity level HOSTNAME = MAC
	 * APP-NAME = application unique name // such as EPG, VOD and so on PROCID =
	 * method name MSGID = unique msgid (I think buffer size would be in integer
	 * format, thus it should be 0 .. 2^31 – positive integer) PARAM-NAME =
	 * “APP_VER” PARAM-VALUE = application version // such as 1.0.3 MSG =
	 * MSG-UTF8
	 *
	 * @param src	source log string in STC
	 * @return list of string separated with white space.
	 *
	 */
	private String[] parseSimpleLog(String src) {
		if (src == null) {
			if (Log.DEBUG_ON) {
	    		Log.printError("log string is null");
			}
	    	return null;
		}

		String[] tmpArr = TextUtil.tokenize(src, TOKEN);


		// there should be at least 4 tmpArr elements.
		if (tmpArr.length < CHECK_LEN) {
			if (Log.DEBUG_ON) {
	    		Log.printError("log string is not valid. " + src);
			}
			return null;
		}

		// it can be possible to get error message with null.
		// so we check tmpArr 0, 1, 2, and 3
		for (int i = 0; i < CHECK_LEN; i++) {
			if (tmpArr[i] == null) {
				if (Log.DEBUG_ON) {
		    		Log.printError("log string is not valid. " + src);
				}
				return null;
			}
		}

		String[] elements = new String[ELE_LEN];
		elements[IDX_DATE] = tmpArr[0]; // date and time
		elements[IDX_LEVEL] = tmpArr[1].substring(0, 1); // log level, #<EPG
		elements[IDX_NAME] = tmpArr[1].substring(2, tmpArr[1].length());
		// app name, first #<
		elements[IDX_VERSION] = tmpArr[2].substring(0, tmpArr[2].length() - 1);
		// app version, remove last >
		// elements[4] = tmpArr[3].substring(0, tmpArr[3].length() -1);
		//  // method name, remove last :
		elements[IDX_MSG] = "";
		for (int i = INIT_SIMPLE_PARSE; i < tmpArr.length; i++) {
			// strings from tmpArr[2] are logs messages
			elements[IDX_MSG] += tmpArr[i] + TOKEN;
		}
		// remove final SP
		if (elements[IDX_MSG].length() > SP.length()) {
			elements[IDX_MSG] = elements[IDX_MSG].substring(0, elements[IDX_MSG].length() - SP.length());
			// remove final TOKEN
		}
		return elements;
	}


	/**
	 * Transferring logging level in log string into logging value.
	 * @param logChar	logging level character in log.
	 * @return	logging level.
	 */
	private int getLevel(String logChar) {
		int level = ApplicationConfig.OFF;
		// each Debug, Info, Warning, Error match #, @, ?, !
		if (logChar.equals("#")) {
			level = ApplicationConfig.DEBUG;
		} else if (logChar.equals("@")) {
			level = ApplicationConfig.INFO;
		} else if (logChar.equals("?")) {
			level = ApplicationConfig.WARNING;
		} else if (logChar.equals("!")) {
			level = ApplicationConfig.ERROR;
		}
		return level;
	}


	/**
	 * changing UTF-8 string into USASCII.
	 *
	 * @param simpleStr	simple UTF-8 string.
	 * @return	changed USASCII string
	 */
	private String utftoascii(String simpleStr) {
		final StringBuffer sb = new StringBuffer(simpleStr.length() * 2);

		final StringCharacterIterator iterator = new StringCharacterIterator(simpleStr);

		char ch = iterator.current();

		while (ch != StringCharacterIterator.DONE) {
			if (Character.getNumericValue(ch) >= 0) {
				sb.append(ch);
			} else {
				boolean f = false;
				if (Character.toString(ch).equals("Ê")) {
					sb.append("E");
					f = true;
				}
				if (Character.toString(ch).equals("È")) {
					sb.append("E");
					f = true;
				}
				if (Character.toString(ch).equals("ë")) {
					sb.append("e");
					f = true;
				}
				if (Character.toString(ch).equals("é")) {
					sb.append("e");
					f = true;
				}
				if (Character.toString(ch).equals("è")) {
					sb.append("e");
					f = true;
				}
				if (Character.toString(ch).equals("è")) {
					sb.append("e");
					f = true;
				}
				if (Character.toString(ch).equals("Â")) {
					sb.append("A");
					f = true;
				}
				if (Character.toString(ch).equals("ä")) {
					sb.append("a");
					f = true;
				}
				if (Character.toString(ch).equals("ß")) {
					sb.append("ss");
					f = true;
				}
				if (Character.toString(ch).equals("Ç")) {
					sb.append("C");
					f = true;
				}
				if (Character.toString(ch).equals("Ö")) {
					sb.append("O");
					f = true;
				}
				if (Character.toString(ch).equals("º")) {
					sb.append("");
					f = true;
				}
				if (Character.toString(ch).equals("Ó")) {
					sb.append("O");
					f = true;
				}
				if (Character.toString(ch).equals("ª")) {
					sb.append("");
					f = true;
				}
				if (Character.toString(ch).equals("º")) {
					sb.append("");
					f = true;
				}
				if (Character.toString(ch).equals("Ñ")) {
					sb.append("N");
					f = true;
				}
				if (Character.toString(ch).equals("É")) {
					sb.append("E");
					f = true;
				}
				if (Character.toString(ch).equals("Ä")) {
					sb.append("A");
					f = true;
				}
				if (Character.toString(ch).equals("Å")) {
					sb.append("A");
					f = true;
				}
				if (Character.toString(ch).equals("ä")) {
					sb.append("a");
					f = true;
				}
				if (Character.toString(ch).equals("Ü")) {
					sb.append("U");
					f = true;
				}
				if (Character.toString(ch).equals("ö")) {
					sb.append("o");
					f = true;
				}
				if (Character.toString(ch).equals("ü")) {
					sb.append("u");
					f = true;
				}
				if (Character.toString(ch).equals("á")) {
					sb.append("a");
					f = true;
				}
				if (Character.toString(ch).equals("Ó")) {
					sb.append("O");
					f = true;
				}
				if (Character.toString(ch).equals("É")) {
					sb.append("E");
					f = true;
				}
				if (Character.toString(ch).equals("_")) {
					sb.append("_");
					f = true;
				}
				if (!f) {
					sb.append("?");
				}
			}
			ch = iterator.next();
		}
		return sb.toString();
	}
/*
	public static void main(String args[]){
		BufferUtil bufferUtil = new BufferUtil();
		long idx = (new Date()).getTime() * 100000 + 10034;

		System.out.println(String.valueOf(idx));
		System.out.println(bufferUtil.utftoascii(String.valueOf(idx)));
	}
*/
}

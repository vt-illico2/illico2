package com.videotron.tvi.illico.stc.uploader;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;

import com.videotron.tvi.illico.stc.communication.ApplicationProperties;
import com.videotron.tvi.illico.stc.schedule.LogJobQueue;
import com.videotron.tvi.illico.stc.util.BufferUtil;
import com.videotron.tvi.illico.stc.util.Misc;
import com.videotron.tvi.illico.stc.util.StcCode;
import com.videotron.tvi.illico.stc.util.StcInfo;
import com.alticast.util.SharedMemory;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import java.io.IOException;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 *
 * LogUploader for one buffer.
 * it uploads buffer per an application for one command.
 * it tries to upload, buff if if failed then throw away logs.
 * and then put a single failure logs into log buffer.
 *
 *
 * @author swkim
 *
 */
public class LogUploader extends Thread {


	/**
	 * STC prefix for shared memory.
	 */
	private static final String STC_PREF = StcInfo.PREFIX;  // STC Prefix


	/**
	 * define max size of buffer.
	 * max msgid  = 63 bits long. 9223372036854775807
	 * if max buffer size is 100000, then it can cover next two hundred years.
	 * if max buffer size is 10000, then it can cover next thousand years.
	 */
	private static final int MAX_MSG_LEN = 100000;

	/** default syslog server Ip. */
	//private final String defaultSysLogIp = "10.247.196.165";

	/** default syslog server port. */
	//private final int defaultSysLogPort = 514;

	/**
	 * syslog server connection for UDP.
	 */
	private InetAddress server = null;
	/**
	 * syslog server port.
	 */
	private int port = 0;

	/**
	 * Owner application name.
	 */
	private String appName;

	/**
	 * STB MAC in log.
	 */
	private String mac;

	/**
	 * socket to connect to syslog with UDP.
	 */
	private DatagramSocket socket;

	/**
	 * retry count.
	 */
	private int retryCount;


	/**
	 * constructor.
	 *
	 * in this class, I do not check parameter.
	 * so to use this class, check appName, address and port !!!
	 *
	 * @param ownerApp	owner application's name.
	 */
	//public LogUploader(final String ownerApp) {
	public LogUploader(final String ownerApp, final int retryCount) {

		if (Log.DEBUG_ON) {
    		Log.printInfo("LogUploader created for " + ownerApp + " with retry count " + retryCount);
    	}
		this.appName = ownerApp;
		this.retryCount = retryCount;

	}

	/**
	 * constructor.
	 *
	 * in this class, I do not check parameter.
	 * so to use this class, check appName, address and port !!!
	 *
	 * @param ownerApp	owner application's name.
	 */
	//public LogUploader(final String ownerApp) {
	//public LogUploader(final String ownerApp, final int retryCount, String[] aBuffer) {
	public LogUploader() {

		/*
		if (Log.DEBUG_ON) {
    		Log.printInfo("LogUploader created for " + ownerApp + " with retry count " + retryCount + " with buffer size = " + aBuffer.length);
    	}
		this.appName = ownerApp;
		this.retryCount = retryCount;
		this.buffer = aBuffer;
		*/

		if (Log.INFO_ON) {
    		Log.printInfo("LogUploader thread started.");
    	}
	}

	/**
	 * setting buffer to be uploaded.
	 *
	 * @param aBuffer logs to be uploaded to syslog server.
	 *
	 */

	/*
	public void setBuffer(String[] aBuffer) {
		this.buffer = aBuffer;

		if (Log.DEBUG_ON) {
    		if (buffer == null) {
    			Log.printInfo("buffer was set but it is null. ");
    		} else {
    			Log.printInfo("buffer was set with size " + buffer.length);
    		}
    	}


		if (buffer == null) {
			if (Log.DEBUG_ON) {
				Log.printWarning("in LogUploader, null buffer is set!");
			}
		}

	}
	 */


	/*
	 * return socket connected to syslog server with UDP.
	 *
	 * @return	socket connected to syslog server
	 */
	/*
	private DatagramSocket getSocket() {
		return this.socket;
	}
*/

	/**
	 * get connection information.
	 * it checks syslog server info and MAC
	 *
	 * @return result of setting before connection.
	 */
	private boolean getConnectionInfo() {


		// 2012.02.28
		// check STB's standalone. if it is stand alone, do not upload logs

		MonitorService monitor =
			(MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
		if (monitor == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogUploader : do not get MonitorService. So do not upload logs");
			}
			return false;
		} else {
			try{
				if(monitor.getSTBMode() == MonitorService.STB_MODE_STANDALONE) {
					if (Log.EXTRA_ON) {
						Log.printDebug(" LogUploader : STB is in standalone mode. So do not upload logs");
					}
					return false;
				} else {
					if (Log.EXTRA_ON) {
						Log.printDebug(" LogUploader : STB is not in standalone mode. So keep uploading logs");
					}
				}
			}catch(RemoteException e){
				if (Log.EXTRA_ON) {
					Log.printDebug(" LogUploader : Error checking standalone mode. So do not upload logs");
					return false;
				}
			}
		}

		// set server ip and port from broadcasting
		// if it failed to get address and port, then it returns false;
		LogServerConfig serverConfig = LogServerConfig.getInstance();

		String serverIp = serverConfig.getServerIp();
		this.port = serverConfig.getPort(); // temporary

		if (Log.EXTRA_ON) {
			Log.printDebug("server Ip = " + serverIp
    				+ ", port = " + this.port);
    	}

		/*
		// when used in testbed, set default syslog config

		serverIp = defaultSysLogIp;
		this.port = defaultSysLogPort;

		if (Log.DEBUG_ON) {
			Log.developer.printDebug("in testbed set server Ip = " + serverIp
    				+ ", port = " + this.port);
    	}
    	*/

		//mac = Misc.getHostMacString();

		// 2011.09.15
		// swkim changed code to get CISCO MAC
		mac = Misc.getMacAddress();

		// mac should be set before upload to notify syslog of unique STB id
		if (mac == null) {
			String message = "fail to upload buffer because it can not read MAC. MAC is null";
			if (Log.DEBUG_ON) {
	    		Log.printDebug(message);
	    	}
			return false;
		}

		try {
			server = InetAddress.getByName(serverIp);
		} catch (UnknownHostException e) {
			String message = "syslog server IP is not valid";
			putErrorMesage(message);
			if (Log.DEBUG_ON) {
	    		Log.printError(message);
	    	}
			return false;
		}

		return true;
	}



	/**
	 * put error message in current collecting buffer.
	 *
	 * @param message	error message to be put in collecting buffer.
	 */
	private void putErrorMesage(String message) {
		if (Log.DEBUG_ON) {
    		Log.printDebug("putErrorMessage called. put cache " + appName + " with message " + message);
    	}


//		// put error log into shared object
//		SharedMemory sm = SharedMemory.getInstance();
//		Vector logCache = (Vector) sm.get(STC_PREF + appName);
//
//		// check buffer size and if there is only one buffer, then do not write logs.
//		if(logCache.size() > 1) {
//			logCache.addElement("!<" + StcInfo.NAME + " " + StcInfo.VERSION
//				+ "> LogUploader: failed to upload buffer. " + message);
//		}
//		// release shared object
//		logCache = null;
	}

	/**
	 * start thread to upload to syslog server.
	 *
	 */
	public void run() {
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("LogUploader, thread start");
		}

    	LogJobQueue queue = LogJobQueue.getInstance();
    	DateFormat dateFormat = new SimpleDateFormat(StcInfo.DATE_FORMAT);
    	Calendar cal = null;
    	ArrayList aList = null;
        int count = 0;

		while(true){

        	cal = Calendar.getInstance();
        	aList = queue.getJobs(dateFormat.format(cal.getTime()));

        	try {
        		upload(aList, 0);
        	} catch (Exception e) {
        		Log.printWarning("LogUploader, fail to upload.");
        		e.printStackTrace();
        	}

			try{
				Thread.sleep(StcCode.CHECK_SEC);
			} catch(Exception e){
				if (Log.DEBUG_ON) {
		    		Log.printError(" STC, LogUpload thread sleep failed with error code " + e.getMessage());
				}
		    }
			
			if (Log.EXTRA_ON) {
				Log.printDebug("LodUploader: thread is live.");
			}

		}

	}


	private void upload(ArrayList aList, int retryCount){

		if (Log.EXTRA_ON) {
    		Log.printDebug("upload called. start to upload to syslog server");
    	}

		if(retryCount >= ApplicationProperties.getUploadRetryCount()) {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("retry count is exceeded. it stopped uploading. retry count = " + retryCount);
	    	}
			return;
		}

		if (!getConnectionInfo()) {

			if (Log.DEBUG_ON) {
	    		Log.printDebug("fail to upload buffer "
	    				+ "because it does not get proper connection info.");
	    	}
			return;
		}


		if (aList == null || aList.size() == 0) {
			if (Log.EXTRA_ON) {
				String message = "aList is null or size is zero. so do not upload buffer";
	    		putErrorMesage(message);
				Log.printDebug(message);
	    	}
			return;
		}


		DatagramPacket output = null;
		BufferUtil bufferUtil = new BufferUtil();
		String tmpLog = null;
		long baseMsgId = (new Date()).getTime() * MAX_MSG_LEN;
		// base msg id is set with current time in milisec

		boolean uploaded = false;
		try {
			socket = new DatagramSocket();

			if (Log.DEBUG_ON) {
				//socket.bind(null); // set local address for UDP
				Log.printDebug("local address of UDP =" + socket.getLocalAddress());
	    	}

			socket.connect(server, port);

			int count = 0;
			for (int i = 0; i < aList.size(); i++) {

				String[] buffer = (String[]) aList.get(i);
				
				if (buffer == null) {
					if (Log.DEBUG_ON) {
						String message = "buffer set is null. so do not upload buffer";
			    		putErrorMesage(message);
						Log.printDebug(message);
			    	}
					continue;
				}
				
				for (int j = 0; j < buffer.length; j++) {
					tmpLog = bufferUtil.changeSysLogFormat(buffer[j], mac, baseMsgId + count);
					if (tmpLog == null) {
						continue;
					}
					byte[] data = tmpLog.getBytes();
					output = new DatagramPacket(data, data.length, server, port);
					socket.send(output);
					if (Log.DEBUG_ON) {
						Log.printDebug("sent a data, size=" + data.length);
					}
					count++;
				}

			}

			uploaded = true;
			//socket.disconnect();
			// disconnect in finally
		} catch (IOException e) {
			String message = "upload failed : " + e.getMessage();
			putErrorMesage(message);
			if (Log.DEBUG_ON) {
	    		Log.printError(message);
	    	}
		} finally {
			try {
				socket.disconnect();
				} catch (Exception e) {
					if (Log.DEBUG_ON) {
			    		Log.printError("Error when disconnecting socket in finally");
			    	}
				}
			try {
				socket = null;
			} catch (Exception e) {
				if (Log.DEBUG_ON) {
		    		Log.printError("Error when make socket null in finally.");
		    	}
			}
		}

		if(!uploaded) {

			try {

				upload(aList, ++retryCount);

				//uploader = new LogUploader(appName, ++retryCount);

				//Thread.sleep(ApplicationProperties.getUploadRetryInterval() * StcCode.MILISEC);

				//uploader.setBuffer(buffer);
				//uploader.start();


			} catch (Exception e) {
				if (Log.DEBUG_ON) {
		    		Log.printError("error uploading buffer." + e.getMessage());
		    	}
			}
		}

		if (Log.DEBUG_ON) {
    		Log.printDebug("upload finished");
    	}
	}
}

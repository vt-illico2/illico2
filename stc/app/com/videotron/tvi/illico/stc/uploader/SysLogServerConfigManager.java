package com.videotron.tvi.illico.stc.uploader;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.communication.SysLogServerConfig;
import com.videotron.tvi.illico.stc.communication.SysLogServerDataUpdateEvent;
import com.videotron.tvi.illico.stc.communication.SysLogServerDataUpdateListener;

/**
 * Configuration Manager to get OOB data.
 * to get syslog server ip and port.
 * 
 * @author swkim
 *
 */
public class SysLogServerConfigManager implements SysLogServerDataUpdateListener {

	/**
	 * setter for data update event.
	 * @param event data update event
	 */
	public synchronized void  onDataUpdateEvent(SysLogServerDataUpdateEvent event) {
				
		LogServerConfig serverConfig = LogServerConfig.getInstance();
		SysLogServerConfig ocServerConfig = event.getSysLogServerConfig();
		
		serverConfig.setServerIp(ocServerConfig.getHostname());
		serverConfig.setPort(ocServerConfig.getPort());
		

		if (Log.DEBUG_ON) {
			Log.printDebug("--------------------------------------");
			Log.printInfo("server Ip = " + serverConfig.getServerIp() 
    				+ ", port = " + serverConfig.getPort());
			Log.printDebug("--------------------------------------");
    	}
		
	}
}

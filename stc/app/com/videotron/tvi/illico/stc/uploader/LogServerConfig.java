package com.videotron.tvi.illico.stc.uploader;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.util.StcCode;


/**
 * data store for broadcasted OOB data.
 * it keeps server ip and port.
 * 
 * @author swkim
 *
 */
public class LogServerConfig {

	/** config file version. */
	private static int fileVersion = 0;
	/** config file name. */
	private static final String CONFIG_FILE = "stc_server.txt";


	/** version location in inband data. */
	//private static final int VERSION_LOCATION = 0; // 8 bits, first byte
	
	/** length location in inband data. */
	private static final int LENGTH_LOCATION = 1;  // 8 bits, second byte
	
	/** server ip location in inband data. */
	private static final int SERVER_IP_LOCATION = 2;  // 8 bits, third byte
		
	/** port size location in inband data. */
	private static final int SIZE_PORT = 2;  // 2 bytes
	
	/** size of byte in inband data. */
	private static final int SIZE_BYTE = 8;  // 8 bytes
	
	

	/**
	 * syslog server ip.
	 */
	private static String serverIp = null;
	/**
	 * port of syslog server.
	 */
	private static int port = 0;

	/** 
	 * instance shared to keep syslog server connection info.
	 */
	private static LogServerConfig instance = null;
	
	
	/**
	 * 
	 * get instance method in singleton pattern.
	 * 
	 * @return	static LogServerConfig object.
	 */
	public static LogServerConfig getInstance() {
		if (instance == null) {
			instance = new LogServerConfig();
		}
		return instance;
	}
	
	
	/**
	 * constructor.
	 * 
	 */
	protected LogServerConfig() {
		// do nothing.
	}

	/**
	 * @return file name.
	 */
	public String getFileName() {
		return CONFIG_FILE;
	}
	

	/**
	 * @return file version value.
	 */
	public int getFileVersion() {
		return fileVersion;
	}
	
	
	/**
	 * set file version of inband.
	 * 
	 * @param newFileVersion set new file version of inband
	 */
	public void setFileVersion(int newFileVersion) {
		this.fileVersion = newFileVersion;
	}
		
	
	/**
	 * getter of syslog server ip.
	 * @return syslog server ip
	 */
	public static String getServerIp() {
		return serverIp;
	}

	
	/**
	 * setter of syslog server ip.
	 * 
	 * @param sysLogIp	syslog server ip.
	 */
	public static void setServerIp(String sysLogIp) {
		
		serverIp = sysLogIp;
		if (Log.DEBUG_ON) {
    		Log.printDebug("server IP is set to " + serverIp);
		}
	}

	/**
	 * getter of syslog server port.
	 * 
	 * @return syslog server port.
	 */
	public static int getPort() {
		return port;
	}

	
	/**
	 * setter of syslog server port.
	 * 
	 * @param sysLogport	syslog server port.
	 */
	public static void setPort(int sysLogport) {

		port = sysLogport;
		if (Log.DEBUG_ON) {
    		Log.printDebug("server port is set to " + port);
		}
	}

	
	/**
	 * parse inband data.
	 * 
	 * @param inbandData	configuration data from broadcasting server.
	 * @return true / false whether it is completed.
	 */
	public static boolean parse(byte [] inbandData) {


		if (Log.DEBUG_ON) {
            Log.printDebug("start to parse STC Server Configuartion inband data.");
		}
		
		if (inbandData == null) {
			if (Log.DEBUG_ON) {
                Log.printDebug("Server Configuartion inband data is null.");
			}
			return false;
		}
		
		if (inbandData.length == 0) {
			if (Log.DEBUG_ON) {
                Log.printDebug("Server Configuartion inband data has no data.");
			}
			return false;
		}	
		
		int ipLength = inbandData[LENGTH_LOCATION];
		
		if (ipLength <= 0) {
			if (Log.DEBUG_ON) {
                Log.printError("Server Configuartion is invalid. Server IP Length is " + ipLength);
			}
			return false;
		}
		
		// server ip in configuration
		byte [] serverIp = new byte[ipLength];
		
		for (int i = 0; i < ipLength; i++) {
			serverIp[i] = inbandData[i + SERVER_IP_LOCATION]; 
		}

		byte [] serverPort = new byte[SIZE_PORT];
		
		for (int i = 0; i < SIZE_PORT; i++) {
			serverPort[i] = inbandData[SERVER_IP_LOCATION + ipLength + i ];
		}

		setServerIp(new String(serverIp));
		setPort(byteArrayToInt(serverPort));

		if (Log.DEBUG_ON) {
            Log.printDebug("server Ip =  " + new String(serverIp));
            Log.printDebug("server Port =  " + byteArrayToInt(serverPort));
		}
		
		return true;
		
	}


	/**
	 * parse byte array into integer. 
	 * 
	 * @param b	byte array
	 * @return integer value parsed byte array
	 */
	private static int byteArrayToInt(byte[] b) {
        int value = 0;
        
        for (int i = 0; i < SIZE_PORT; i++) {
            int shift = (SIZE_PORT - 1 - i) * SIZE_BYTE;
            value += (b[i] & StcCode.SHIFT_HEXA) << shift;
        }
        return value;
    }
}

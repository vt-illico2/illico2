package com.videotron.tvi.illico.stc.controller;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.log.Log;

import com.videotron.tvi.illico.stc.config.ApplicationConfig;
import com.videotron.tvi.illico.stc.config.AppConfigManager;
import com.videotron.tvi.illico.stc.controller.buffer.AppBufferManager;
import com.videotron.tvi.illico.stc.schedule.LogScheduler;
import com.videotron.tvi.illico.stc.util.StcDuration;
import com.videotron.tvi.illico.stc.util.StcInfo;


/**
 * controller for STC application to use inner method.
 * it provides initXlet and destoryXlet.
 *
 * @author swkim
 *
 */
public class StcController {


	/**
	 * initialize STC application.
	 * 1. check configuration in persistent storage
	 * 2. set configuration and buffer if something is stored in persistent storage
	 * 3. publish STC Service
	 *
	 * @param context XletContext of this application.
	 */
	public void initApp(XletContext context) {
		AppConfigManager configManager = new AppConfigManager();
		AppBufferManager buffManager = new AppBufferManager();


		SimpleDateFormat df = new SimpleDateFormat(StcInfo.DATE_FORMAT);
		String currDate = df.format(new Date());
		String appDate = "";

		if (Log.DEBUG_ON) {
    		Log.printInfo("initApp callled. Current Time = " + currDate);
    	}

		// read all configuration
		ApplicationConfig [] appConfigs = configManager.getIniticalLogConfig();

		// initially application checks persistent storage for stored application configuration
		// if there are configurations, then register application buffers

		StcDuration stcDuration = new StcDuration();

		if (appConfigs == null) {

			if (Log.DEBUG_ON) {
	    		Log.printDebug("there is no aplicaiton configuration stored in persistent storage.");
	    	}
		} else {
			LogScheduler scheduler = LogScheduler.getInstance();

			for (int i = 0; i < appConfigs.length; i++) {
				if (appConfigs[i] != null) {
					if (Log.DEBUG_ON) {
			    		Log.printDebug("configuration of  "
			    				+ appConfigs[i].getName() + " is managed.");
			    		Log.printDebug(appConfigs[i].getName() + ", ");
			    		Log.printDebug(appConfigs[i].getBufferSize() + ", ");
			    		Log.printDebug(appConfigs[i].getExpires() + ", ");
			    		Log.printDebug(appConfigs[i].getLogLevel() + ".");
			    	}
					buffManager.registerApp(appConfigs[i].getName(), appConfigs[i].getBufferSize());

					appDate = stcDuration.parseDuration(appConfigs[i].getExpires());
					// check first character of expiration. if it is duration than do not trigger
					if ((appConfigs[i].getExpires() != null)
							&& (appConfigs[i].getExpires().charAt(0) != 'P')
							&& (appDate.compareTo(currDate) > 0)) {
						if (Log.DEBUG_ON) {
				    		Log.printDebug(appConfigs[i].getName() + "s is later than now.");
				    		Log.printDebug("expired = " + appConfigs[i].getExpires());
				    		Log.printDebug(". now = " + currDate);
				    		Log.printDebug("creating LogTrigger for " + appConfigs[i].getName());
						}

				    	scheduler.addSchedule(appConfigs[i].getName(), appConfigs[i].getExpires());
					}
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("appConfigs[" + i + "] is null.");
					}
				}
			}
		}
		if (Log.DEBUG_ON) {
    		Log.printDebug("started STCService.");
    	}
	}

	/**
	 *
	 * destroy STC application.
	 * 1. upload logs into syslog servers
	 * 2. store configuration into persistent storage
	 *
	 */
	public void destroyApp() {
		AppConfigManager configManager = new AppConfigManager();
		AppBufferManager buffManager = new AppBufferManager();

		if (Log.DEBUG_ON) {
    		Log.printDebug("destroyApp called. "
    				+ "configuration is set into persistent storage. close stc service");
    	}

		// read configs with null appName, then it reads all configs in LogManager
		ApplicationConfig [] configs = configManager.getAllLogConfig();
		for (int i = 0; i < configs.length; i++) {
			buffManager.updateLogConfig(configs[i].getName(), configs[i].getBufferSize());
		}
		configManager.storeConfig();
	}
}

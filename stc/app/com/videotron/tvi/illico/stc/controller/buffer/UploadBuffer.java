package com.videotron.tvi.illico.stc.controller.buffer;


import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.schedule.LogJobQueue;
import com.videotron.tvi.illico.stc.schedule.LogTrigger;
import com.videotron.tvi.illico.stc.uploader.LogUploader;


/**
 * 
 * upload buffer to temporarily store logs.
 * it puts buffers into uploader after.
 * 
 * @author swkim
 *
 */
public class UploadBuffer {

	// change UploadBuffer from static into normal
	// because for each instance upload buffer has a different object
	// uploading of logs do not retry not keep data temporarily
	// thus it tries to upload and if it fails then throws data.
	// that means if it failed, then this object is removed from memory.
	// so when it is invoked, it creates uploader, put buffer into uploader and execute upload method in uploader
	// after that, it is removed.


	/**
	 * application name for this upload buffer.
	 */
	private String appName = null;
	
	/**
	 * data store for buffer.
	 */
	private String [] buffer = null;
	
	/**
	 * constructor.
	 * set uploading buffer with owner application name.
	 * @param applicationName upload buffer's owner application name.
	 */
	public UploadBuffer(final String applicationName) {

		if (Log.DEBUG_ON) {
    		Log.printDebug("created new UploadBuffer to upload caches for " + applicationName);
    	}
		
		this.appName = applicationName;
		
	}
	
	
	/**
	 * set logs into uploading buffer.
	 * 
	 * @param aBuffer logs stored in LogCache.
	 */
	public synchronized void addBuffer(String[] aBuffer) {
		
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("put log buffers into upload buffer");
    	}
		
		buffer = aBuffer;
		if (buffer == null) {
			if (Log.DEBUG_ON) {
				Log.printWarning("in UploadBuffer, null buffer is set!");
			}
		}


	}
	
	
	/**
	 * upload current buffer into uploader.
	 * 
	 */
	public synchronized void upload() {
	
		if (Log.DEBUG_ON) {
    		Log.printDebug("upload called. start to upload to syslog server");
    	}
		
		if (appName == null) {
			if (Log.DEBUG_ON) {
	    		Log.printError("appName is not set to upload. So do not upload");
	    	}
			return;
		}
		
		
		LogUploader uploader = null;
		try {
			// retry count is added, it's initial value is 0
			
			// swkim 2013.05.05 changed single thread with queue for LogUploader
			/*
			uploader = new LogUploader(appName, 0);			
			uploader.setBuffer(buffer);
			uploader.start();
			*/
			
			LogJobQueue queue = LogJobQueue.getInstance();			
			queue.addLogJob(null, buffer);
			
			
		} catch (Exception e) {
			if (Log.DEBUG_ON) {
	    		Log.printError("error uploading buffer." + e.getMessage());
	    	}
		}
		
	}
}

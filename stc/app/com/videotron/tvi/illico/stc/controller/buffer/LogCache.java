package com.videotron.tvi.illico.stc.controller.buffer;

import com.videotron.tvi.illico.log.Log;
//import com.videotron.tvi.illico.stc.uploader.LogServerConfig;
import com.videotron.tvi.illico.stc.util.StcInfo;

import java.util.Vector;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

// implement this class

/**
 * Buffer for each application's log.
 *
 * LogCache overrides Vector class for other application to use simple vector interface.
 *
 */
public class LogCache extends Vector {


	/**
	 * LogCache extends Vector so it should have a UID.
	 */
	  private static final long serialVersionUID = -2767605614048989439L;

	/**
	 * separator in log format.
	 */
	private static final String SP = " ";  // separator for syslog server
	/**
	 * collecting buffer to store String arrays.
	 */
	private String [] collectingBuffer = null;
	/**
	 * log count.
	 */
	private int elementCount = 0;
	/**
	 * size of buffer.
	 */
	private int bufferSize = 0;
	/**
	 * target application name.
	 */
	private String appName = null;
	/**
	 * upload buffer to transfer to uploader.
	 */
	private UploadBuffer uploadBuffer = null;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                StcInfo.SIMPLE_DATE_FORMAT + "\'T\'" + StcInfo.SIMPLE_TIME_FORMAT);


	/**
	 * actually it is not used, because default buffer size not defined.
	 * to use this creator, call set size and set appName with contains method after that.
	 * in setSize method, memory will be allocated
	 */
    public LogCache() {

    	// do not create string array.
    	// to use log cache, setSize should be called to set buffer size and create string array with that.

    	if (Log.DEBUG_ON) {
    		Log.printInfo("LogCache is created with buffer size " + bufferSize);
    	}
    }

    /**
     * actual creator of this class.
     * it is defined with initial buffer size.
     * @param initialCapacity	initial size of buffer
     */
    public LogCache(final int initialCapacity) {

    	bufferSize = initialCapacity;
    	elementCount = 0;
    	collectingBuffer = new String[bufferSize];

    	if (Log.DEBUG_ON) {
    		Log.printInfo("LogCache is created with initial buffer size " + bufferSize);
    	}
    }


    /**
     * method to flush current buffer and recreate new buffer.
     *
     */
    private void recreateBuffer() {
    	collectingBuffer = new String[bufferSize];
		elementCount = 0;
    }


    /**
     * upload collecting buffer to new uploading buffer
     * and create new memory for collecting buffer.
     */
    private void flushBuffer() {

    	if (Log.DEBUG_ON) {
    		Log.printDebug("flushing buffer, elementCount=" + elementCount + ", collectionBuffer=" + collectingBuffer);
    	}

    	// if element count is 0, then there is no logs in buffer.
    	// So just flush buffer, and do not upload
    	if (elementCount == 0) {
    		recreateBuffer();
    		return;
    	}

    	// if collecting buffer is null, then it can be a problem.
    	// but anyway in this case, we regenerate collecting buffer again
    	if (collectingBuffer == null) {
    		recreateBuffer();
    		return;
    	}

    	// if there is no logs collected, then return
    	if (collectingBuffer.length == 0) {
    		recreateBuffer();
    		return;
    	}

    	if (Log.DEBUG_ON) {
    		Log.printDebug("there are logs in logcache, so it moves into uploading buffer with "
    				+ collectingBuffer.length + " logs");
    	}

    	if (appName == null) {
    		if (Log.DEBUG_ON) {
        		Log.printError("appName is not set. Thus it can not create upload buffer.");
        	}
    		recreateBuffer();
    		return;
    	}

    	if (uploadBuffer == null) {
			uploadBuffer = new UploadBuffer(appName);
		}

		uploadBuffer.addBuffer(collectingBuffer);
		recreateBuffer();

		if (Log.DEBUG_ON) {
    		Log.printInfo("uploading logs in uploading buffer");
    	}

		// to test LogCache operation only, comment out below line.
		uploadBuffer.upload();
    }


    /**
     *
     * set log of a general application into collecting buffer.
     * if collecting buffer is full, then it moves collecting buffer into a uploading buffer
     * and create new memory
     *
     * @param arg0	log string to be added in buffer
     */
	public synchronized void addElement(Object arg0) {

		String currLog = getCurrTime() + SP + (String) arg0;

		if (Log.DEBUG_ON) {
    		Log.printDebug("one log added : " + currLog);
    	}

		collectingBuffer[elementCount++] = currLog;


		if (elementCount >= bufferSize) {
			flushBuffer();
		}

	}


	/**
	 * get STB's time to be added in logs.
	 * @return system time
	 */
	private String getCurrTime() {
        return simpleDateFormat.format(new Date());
/*
		Date date = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat(StcInfo.SIMPLE_DATE_FORMAT);
		//dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		SimpleDateFormat timeFormatter = new SimpleDateFormat(StcInfo.SIMPLE_TIME_FORMAT);
		//timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

		  // Set GMT date and time
		  //return dateFormatter.format(date) + "T" + timeFormatter.format(date) + "Z";
		  // add Z referring to http://www.timeanddate.com/library/abbreviations/timezones/military/z.html


		//swkim changed not to use GMT but to use montreal local time
		return dateFormatter.format(date) + "T" + timeFormatter.format(date);
*/
	}

	/**
	 * setter of buffer's owner.
	 * set appName to start uploader, appName should be set!!!
	 * @param elem application name who owned this buffer
	 * @return result of setting name.
	 */
	public boolean contains(Object elem) {

		appName = (String) elem;

		return true;
	}

	/**
	 *
	 * set buffer's size.
	 * set initial buffer size and create memory
	 *
	 * @param newSize size of new buffer
	 *
	 */
    public synchronized void setSize(int newSize) {

    	bufferSize = newSize;

    	if (Log.DEBUG_ON) {
    		Log.printInfo("LogCache is set with buffer size " + newSize + ", and created");
    	}

    	flushBuffer();
    }


    /**
     * upload current buffer and recreate new buffer.
     *
     */
    public synchronized void clear() {

    	if (Log.DEBUG_ON) {
    		Log.printDebug("LogCache is flushed");
    	}

        flushBuffer();
    }

    /**
     * returns buffer size not element count.
     *
     * @return size of current buffer.
     *
     */
    public synchronized int size() {
    	return bufferSize;
    }

    /**
     * make buffer empty, not to create new buffer.
     *
     *@return checks whether there are logs or not.
     */
    public synchronized boolean isEmpty() {
    	return elementCount == 0;
    }

    /**
     * override Vector's toString to make simple job internally.
     *
     * @return return simple overrided method.
     */
    public synchronized java.lang.String toString() {
    	return appName + " LogCache processed";
    }
}

package com.videotron.tvi.illico.stc.controller;


import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.config.ApplicationConfig;

import com.videotron.tvi.illico.stc.config.AppConfigManager;
import com.videotron.tvi.illico.stc.controller.buffer.AppBufferManager;
import com.videotron.tvi.illico.stc.communication.ApplicationProperties;

//import com.videotron.tvi.illico.stc.util.StcDuration;

/**
 * controller for general applications in STB.
 * it provides methods for general applications to use STC
 * 
 * @author swkim
 *
 */
public class GenAppController {

	/**
	 * 	 *
	 * general application call stc to register. 
	 * STC check whether there is a previous configuration or not
	 * if there is a previous configuration, then it keeps previous configuration
	 * 
	 * 
	 * @param appName	targeted application name
	 * @return		returns logging level of application
	 */
	public int registerLog(String appName) {
		
		AppConfigManager configManager = new AppConfigManager();
		AppBufferManager bufferManager = new AppBufferManager();

		int logLevel = ApplicationProperties.getDefaultLogLevel(); // default log level is ERROR, changed 2012.01.25
		
		if (Log.DEBUG_ON) {
    		Log.printInfo("registerLog " + appName 
    				+ " is called. in debug mode");
    		// set default log level into Debug if it is debug model
    		//logLevel = ApplicationConfig.DEBUG;
    		
    		Log.printDebug("[STC] default log level is " + ApplicationProperties.getDefaultLogLevel());
		}		 
		
		if (appName == null) {
		
			if (Log.DEBUG_ON) {
	    		Log.printError("registerLog appName is null. So do not register.");
			}
			return ApplicationProperties.getDefaultLogLevel(); // set log level into off --> error as above
		}
		
		ApplicationConfig config = configManager.getOneLogConfig(appName);
		
		// if there is a already registered log configuration, then set configuration as set before
		// if not, register new configuration
		
		if (config == null) {
			
			if (Log.DEBUG_ON) {
	    		Log.printDebug("there is no confuguration for  " 
	    				+ appName + ". So create new configuration.");
			}
			// there is no configuration in LogConfig, so register new app
			// temporary buffer size value to use default buffer size
			configManager.registerApp(appName, logLevel);
			bufferManager.registerApp(appName, ApplicationProperties.getDefaultDeveloperLogBufferSize());
			// store configuration into persistent memory
			configManager.storeConfig();
			
		} else {
			if (Log.DEBUG_ON) {
	    		Log.printDebug("There is already a configuration for " + appName);
			}
			// if there is already a log configuration, 
			//then set general application to log level in configuration
			logLevel = config.getLogLevel();
			// update configuration of log config.
			// 2011.05.10. swkim, don't need to update log config
			//configManager.updateLogConfig(config);
			// update log buffer
			bufferManager.updateLogConfig(appName,  config.getBufferSize());
			// store configuration into persistent memory
			// 2011.05.10. swkim. don't need to store config
			//configManager.storeConfig();
			
		}
		
		// Scheduler Test
		// after test remove this code.
		//if (Log.DEBUG_ON) {
    	//	Log.developer.printDebug("STC -------------- added schedule ");
		//}
		//StcDuration stcDuration = new StcDuration();
		//config.setExpires(stcDuration.parseDuration("PT1M"));
		//DaemonController dc = new DaemonController();
		//dc.setLogConfig(config);
		
		return logLevel;
		
	}
	
	
}

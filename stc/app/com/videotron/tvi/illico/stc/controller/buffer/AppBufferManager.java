package com.videotron.tvi.illico.stc.controller.buffer;


import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.stc.util.StcInfo;
import com.alticast.util.SharedMemory;
import java.util.Vector;
import java.util.Hashtable;

/**
 * manager who manages application's buffer in STB.
 * AppBufferManager creates buffer for each applications
 * and manages upload buffer to send logs into syslog server
 *
 *
 * @author swkim
 *
 */
public class AppBufferManager {


	/**
	 * prefix to be put in shared memory.
	 * key for shared memory is stc- + appName
	 */
	private static final String STC_PREF = StcInfo.PREFIX;



	/**
	 * register application buffer .
	 * and put shared object in shared memory
	 *
	 * @param appName	should not be null
	 * @param buffSize	initial buffer size for an application
	 */
	public synchronized void registerApp(String appName, int buffSize) {

		if (appName == null) {
			if (Log.DEBUG_ON) {
	    		Log.printError("cannot registerApp with null application name. appName is null now");
	    	}
			return;
		}

		if (Log.DEBUG_ON) {
    		Log.printDebug("registerApp called. " + appName + " 's LogCache is created");
    	}


		Hashtable sm = (Hashtable) SharedMemory.getInstance();
		Vector cache = (Vector) sm.get(STC_PREF + appName);

		// check me!
		// check object exist later
		if ((cache == null) || (cache.size() == 0)) {

			if (Log.DEBUG_ON) {
	    		Log.printDebug("LogCache of " + STC_PREF + appName + " is null. So create new one.");
	    	}

			cache = new LogCache(buffSize);
			cache.contains(appName); // LogCache should have a name!!
			sm.put(STC_PREF + appName, cache);

		} else {

			if (Log.DEBUG_ON) {
	    		Log.printDebug("thre is already log cache. so flush it for " + appName);
	    	}

			if (cache.size() > 0) {
				cache.clear();
			}
		}

		cache = null; // release LogCache local memory

	}

	/**
	 * change of buffer configuration.
	 * move current buffer if any into uploading buffer,
	 * and recreate new collecting buffer
	 * if buffer size is changed, then new buffersize should be applied.
	 * if buffSize is -1, that means there is no changes of buffSize;
	 *
	 * @param appName	targeted application name
	 * @param buffSize 	buffer size to be changed
	 */
	public void updateLogConfig(String appName, int buffSize) {


		if (Log.DEBUG_ON) {
    		Log.printDebug("updateLogConfig called of " + appName + " with buffer size " + buffSize);
    	}

		if (appName == null) {
			if (Log.DEBUG_ON) {
	    		Log.printError("cannot updateLogConfig with null application name.");
	    	}
			return;
		}

		Hashtable sm = (Hashtable) SharedMemory.getInstance();
		Vector cache = (Vector) sm.get(STC_PREF + appName);

		if (cache == null) {
			registerApp(appName, buffSize);
		} else {
			// set buffer size
			if (buffSize >= 0) {
				cache.setSize(buffSize);
			}

			// when buffer is renewed, new buffer size is applied
			// setSize and clear is one operation. So added clear method into setSize
			//cache.clear();

			cache = null;
		}
	}



	/**
	 * upload current buffer and create new one.
	 * it does not change buffer size.
	 *
	 *
	 * @param appName target application name
	 */
	public void updateLogConfig(String appName) {


		if (Log.DEBUG_ON) {
    		Log.printDebug("updateLogConfig called of " + appName + " not changing buffer size");
    	}

		if (appName == null) {
			if (Log.DEBUG_ON) {
	    		Log.printError("cannot update log config with null application name.");
	    	}
			return;
		}

		Hashtable sm = (Hashtable) SharedMemory.getInstance();
		Vector cache = (Vector) sm.get(STC_PREF + appName);

		// set buffer size with before
		// check below two lines of codes are necessary, cause bufferSize in LogCache is not changed
		int buffSize = cache.size();
		if (buffSize >= 0) {
			cache.setSize(buffSize);
		}

		// when buffer is renewed, new buffer size is applied. merged into setSize
		//cache.clear();

		cache = null;
	}
}

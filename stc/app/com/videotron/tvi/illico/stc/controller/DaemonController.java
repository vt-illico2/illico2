package com.videotron.tvi.illico.stc.controller;

import com.videotron.tvi.illico.log.Log;
import java.rmi.RemoteException;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.stc.config.ApplicationConfig;
import com.videotron.tvi.illico.stc.config.AppConfigManager;
import com.videotron.tvi.illico.stc.config.LogConfig;
import com.videotron.tvi.illico.stc.controller.buffer.AppBufferManager;
import com.videotron.tvi.illico.stc.communication.ApplicationProperties;
import com.videotron.tvi.illico.stc.communication.StcServiceImpl;
import com.videotron.tvi.illico.stc.communication.NotificationAdapter;
import com.videotron.tvi.illico.stc.schedule.LogScheduler;
import com.videotron.tvi.illico.stc.util.Misc;
import com.videotron.tvi.illico.stc.util.StcCode;
import com.videotron.tvi.illico.stc.util.StcDuration;


/**
 * DaemonController is a business logic processor of requests from DAEMON.
 * Requests from DAEMON is also come from STCC and return the final results in STB.
 * 
 * @author swkim
 *
 */
public class DaemonController {

	/**
	 * logScheduler is scheduler for triggering final signal for a command from STCC.
	 */
	private LogScheduler logScheduler = null;
	/**
	 * stcService is interface to other general application.
	 */
	private StcServiceImpl stcService = null;
	
	/**
	 * DaemonController's creator.
	 * it creates logScheduler and stcService with static singleton object.
	 */
	public DaemonController() {
		logScheduler = LogScheduler.getInstance();
		stcService = (StcServiceImpl) DataCenter.getInstance().get(StcService.IXC_NAME);
		// stcService is static singleton
		
	}

	/**
	 * process request from DAEMON.
	 * in setLogConfig, it checks target application lists.
	 * And for each target application
	 * - update configuration
	 * - update buffer
	 * - update schedule
	 * - update notification
	 * 
	 * @param requestConfig	object from DAEMON containing parameter value.
	 * @return	logging level of target application
	 */
	public int setLogConfig(ApplicationConfig requestConfig) {

		if (Log.DEBUG_ON) {
			Log.printDebug("setLogConfig " + requestConfig.getName() + " is called");
		}
		
		
		int stcReturn = StcCode.STC000;
		
		// 2011.09.15
		// swkim changed to use getMacAddress to support CISCO MAC
		//if (Misc.getHostMacString() == null) {
		if (Misc.getMacAddress() == null) {
			if (Log.DEBUG_ON) {
				Log.printError("ERROR : MAC is not set. So do not proess set log config.");
			}
		
			return StcCode.STC008; // parameter exception
		}
		
		
		if (requestConfig == null) {
		
			if (Log.DEBUG_ON) {
				Log.printError("setLogConfig called.  but requestConfig is null, so return.");
			}

			return StcCode.STC003; // parameter exception
		}
		
		if (Log.DEBUG_ON) {
			Log.printInfo("setLogConfig called. " + requestConfig.getName() + "," 
					+ requestConfig.getBufferSize() + "," 
					+ requestConfig.getExpires() + "," + requestConfig.getLogLevel());
		}

		
		// if buffer size is 0, it is parameter exception
		// added 2011.05.19 by Guy's request
		if (requestConfig.getBufferSize() == 0) {
			return StcCode.STC003;
		}
		
		if (requestConfig.getBufferSize() == -1) {
			requestConfig.setBufferSize(ApplicationProperties.getDefaultDeveloperLogBufferSize());
		}
		
		if(((requestConfig.getExpires() != null) && (requestConfig.getExpires().equals(StcCode.EXP_NEVER))) // expire == "NEVER"
				|| ((requestConfig.getExpires() != null) && (requestConfig.getExpires().equals(StcCode.EMPTY_STRING))) // expire == ""
				|| (requestConfig.getExpires() == null)){ // expire == NULL
			// initial configuration. do not expires

			String defExpDur = null;
			
			if(requestConfig.getLogLevel() == ApplicationConfig.OFF) {
				defExpDur = StcCode.EXP_NEVER;
			} else {
				defExpDur = (new StcDuration()).parseDuration(ApplicationProperties.getDefaultLogLevelExpDuration());
			}
			
			requestConfig.setExpires(defExpDur);
		
		
			if (Log.DEBUG_ON) {
				Log.printDebug(requestConfig.getName()  + "'s expiration is " + defExpDur);
			}
		
		}
		
		if (requestConfig.getLogLevel() < ApplicationConfig.OFF) {
			requestConfig.setLogLevel(ApplicationConfig.OFF);
		}
		
		// check application name is null or not
		// Application name is null in this DaemonController, 
		// so AppConfigManager and AppBufferManager do not get null application name.
		if (requestConfig.getName() == null) {
			stcReturn = processMultiApplicationConfig(requestConfig);			
		} else {
			stcReturn = processOneApplicationConfig(requestConfig);
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("return value afater processing muliple application config " 
					+ stcReturn);
		}
		
		// if stcReturn is negative, that means there is error to process application config.
		// so return stcReturn if negative
		// but if it is positive, it returns log level
		
		if (stcReturn < StcCode.STC000) {
			return stcReturn;
		}


		// set notification message with NotificationCenter
		if (requestConfig.isNotify()) {
			stcReturn = setNotify();
		}

		return stcReturn;
	}

	
	/**
	 * setNotify calls notification center to show a message for STC.
	 * 
	 * @return	int	execution result for notification center to send message. 
	 * 				it does not mean notification center showed message.
	 */
	private int setNotify() {
		
		if (Log.DEBUG_ON) {
			Log.printInfo("callinig notification center for showing message. ");
		}
		int ret = StcCode.STC000;
	
		NotificationAdapter notificationAdaptor = new NotificationAdapter();
		
		NotificationService notifyService = 
			(NotificationService) DataCenter.getInstance().get(NotificationService.IXC_NAME);
		
		try {
			notifyService.setNotify(notificationAdaptor);
		} catch (RemoteException e) {
			if (Log.DEBUG_ON) {
				Log.printError("failed to call notification center. " + e.getMessage());
			}
			ret = StcCode.STC007;
		} catch (Exception e) {
			if (Log.DEBUG_ON) {
				Log.printError("unknown error : failed to call notification center. " 
						+ e.getMessage());
			}
			ret = StcCode.STC007;
		}
		
		return ret;
	}
	
	
	/**
	 * addSchedule add schedule to new schedule.
	 * schedule is a task to finalize collecting logs for one application.
	 * 
	 * @param appName	target application name to be added with schedule
	 * @param schedule	time for an application to stop logging
	 * @return			final status of logging. value is defined in STCCode
	 */
	private int addSchedule(String appName, String schedule, int logLevel) {
		
		if (Log.DEBUG_ON) {
			Log.printInfo("called add schedule for " + appName + " with schedule " + schedule);
		}
		
		// checking input parameter
		if (appName == null) {
			if (Log.DEBUG_ON) {
				Log.printError("failed to add scheudle because appName is null");
			}
			return StcCode.STC003;
		}
		
		if (schedule == null) {
			
			if(logLevel == ApplicationConfig.OFF){
				schedule = StcCode.EXP_NEVER;
			}  else {
				schedule = ApplicationProperties.getDefaultLogLevelExpDuration();
			}
			if (Log.DEBUG_ON) {
				Log.printError("[STC]" + appName + "'s schedule is null, so schedule is set to  " + schedule);
			}			
		}
			
		if(StcCode.EXP_NEVER.equals(schedule)){
			if (Log.DEBUG_ON) {
				Log.printError("Expiration of Log Trigger is NEVER. So do not trigger.");
			}
			
			return StcCode.STC000;
		}
		
		// put schedule into scheduler to stop logging!!
		// if schedule's format is duration, change into date format
		String scheduleDate = (new StcDuration()).parseDuration(schedule);
		
		if (scheduleDate == null) {
			return StcCode.STC003;
		}
		
		int addRes = logScheduler.addSchedule(appName, scheduleDate);
		
		if (addRes  < StcCode.STC000) {
			if (Log.DEBUG_ON) {
				Log.printError("failed to added scheduler for " + appName + "," + schedule);
			}
			
			return addRes;
			
		} else {
			
			if (Log.DEBUG_ON) {
				Log.printDebug("trigger schedule added for " + appName + "," + schedule);
			}
			
			return StcCode.STC000;
		}
		
	}
	
	
	/**
	 * change log level for an application.
	 * 
	 * @param appName	target application name.
	 * @param logLevel	log level to be set. LogLevel is defined in <code> StcCode </code>.
	 * @return	int		returns final status of setting log levels for each application. 
	 * 					it is <code> StcCode </code>.
	 */
	private int changeLogLevel(String appName, int logLevel) {
		
		// checking input parameter
		if (appName == null) {
			if (Log.DEBUG_ON) {
				Log.printError("failed to change log level because appName is null");
			}
			return StcCode.STC003;
		}
		
		int ret = StcCode.STC000;
				
		boolean changed = stcService.notifyLogLevelChange(appName, logLevel);
		if (!changed) {
			ret = StcCode.STC007;
		}
		
		if (Log.DEBUG_ON) {
			if (changed) {
				Log.printInfo(appName 
						+ " application's log level is changed into " + logLevel);
			} else {
				Log.printWarning("failed to change " 
						+ appName + " application's log level into " + logLevel);
			}
		}
		
		return ret;
	}
	
	
	/**
	 * make actions for each application to set log levels.
	 * 
	 * @param configManager	component to manage configuration for each application.
	 * @param bufferManager	component to manage buffer for each application.
	 * @param config		configuration of each application.
	 * @return	int			returns final status of setting log levels for each application. 
	 * 						it is <code> StcCode </code>.
	 */
	private int processChange(AppConfigManager configManager, AppBufferManager bufferManager,
			ApplicationConfig config) {
		
		if (Log.DEBUG_ON) {
			Log.printDebug("processChange " + config.getName() + " is called");
		}
		
		// checking input parameter
		if (configManager == null) {
			if (Log.DEBUG_ON) {
				Log.printError("failed to process because configManager is null");
			}
			return StcCode.STC003;
		}
		
		// checking input parameter
		if (bufferManager == null) {
			if (Log.DEBUG_ON) {
				Log.printError("failed to process because bufferManager is null");
			}
			return StcCode.STC003;
		}
		
		// checking input parameter
		if (config == null) {
			if (Log.DEBUG_ON) {
				Log.printError("failed to process because config is null");
			}
			return StcCode.STC003;
		}

		
		int ret = configManager.updateLogConfig(config);
		if (ret < StcCode.STC000) {
			return ret;
		} else {
			ret = StcCode.STC000;
		}
		
		bufferManager.updateLogConfig(config.getName(), config.getBufferSize());
		
		// change target applicaiton's log level
		ret = changeLogLevel(config.getName(), config.getLogLevel());
		if (ret < StcCode.STC000) {
			return ret;
		}
		
		// add schedule
		ret = addSchedule(config.getName(), config.getExpires(), config.getLogLevel());
		if (ret < StcCode.STC000) {
			return ret;
		}
		
		configManager.storeConfig();
		
		return StcCode.STC000;
	}

	
	/**
	 * change configuration for only one application.
	 * 
	 * @param requestConfig	configuration for a target application
	 * @return	int	returns final status of setting log levels for each application. 
	 * 					it is <code> StcCode </code>.
	 */
	private int processOneApplicationConfig(ApplicationConfig requestConfig) {
		AppConfigManager configManager = new AppConfigManager();
		AppBufferManager buffManager = new AppBufferManager();

		
		int ret = StcCode.STC000;
		if (Log.DEBUG_ON) {
			Log.printDebug("processOneApplicatoinConfig called.  "
					+ requestConfig.getName() + " 's configuration is set");
		}
		
		if (!LogConfig.getInstance().existConfiguration(requestConfig.getName())) {
			if (Log.DEBUG_ON) {
				Log.printDebug("no application for.  "
						+ requestConfig.getName() + " registered.");
			}
			return StcCode.STC003;
		}
			
		// check format of expired date
		// if format of expired date is invalid, then it returns invalid parameter error, STC003
		if ((requestConfig.getExpires() != null) && !StcCode.EXP_NEVER.equals(requestConfig.getExpires()) ) {
			if (!(new StcDuration()).checkDurationValidity(requestConfig.getExpires())) {
				return StcCode.STC003;
			}
		}
		
		ret = processChange(configManager, buffManager, requestConfig);
		
		configManager.storeConfig();
		
		return ret;
	}
	

	
	/**
	 * process multiple application's configuration.
	 * 
	 * @param requestConfig	application's configuration. there is no application name.
	 * 			but other configuration is all the same for all applications
	 * @return	int returns final status of setting log levels for each application. 
	 * 					it is <code> StcCode </code>.
	 */
	private int processMultiApplicationConfig(ApplicationConfig requestConfig) {
		
		if (Log.DEBUG_ON) {
			Log.printDebug("processMultiApplicationConfig is called : " + requestConfig.getBufferSize() + " : " 
					+ requestConfig.getExpires() + " : " + requestConfig.getLogLevel());
		}
		
		
		/*
		if (Log.DEBUG_ON) {
			Log.developer.printDebug("processMultiApplicationConfig called. ");
		}
		*/
		String[] appNames = getAllAppNames();

		if ((appNames == null) || (appNames.length == 0)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("there is no configuration stored. "
						+ "so do not update anything.");
			}
			return StcCode.STC000;
		}
		
		AppConfigManager configManager = new AppConfigManager();
		AppBufferManager buffManager = new AppBufferManager();
		ApplicationConfig appConfig = null;
		int ret = StcCode.STC000;
		
		for (int i = 0; i < appNames.length; i++) {
			appConfig = new ApplicationConfig();
			appConfig.setName(appNames[i]);
			appConfig.setBufferSize(requestConfig.getBufferSize());
			appConfig.setExpires(requestConfig.getExpires());
			appConfig.setLogLevel(requestConfig.getLogLevel());

			if (Log.DEBUG_ON) {
				Log.printDebug("updating configuration of  " + appConfig.getName() 
						+ "," + appConfig.getBufferSize());
			}
			
			ret = processChange(configManager, buffManager, appConfig);
		}
		
		return ret;
	}

	
	/**
	 * get all application's name managed in configuration manager.
	 * @return	String []	application's names
	 */
	private String[] getAllAppNames() {
		String[] appList = null;
		ApplicationConfig[] appConfigs = null;

		AppConfigManager configManager = new AppConfigManager();
		
		appConfigs = configManager.getAllLogConfig();
		
		if (appConfigs == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("count of configuration stored is null");
			}
			return null;
		}
		
		int appCount = appConfigs.length;
			
		if (Log.DEBUG_ON) {
			Log.printDebug("count of configuration stored is " + appCount);
		}
		
		if (appCount > 0) {
			appList = new String[appCount];
			for (int i = 0; i < appCount; i++) {
				appList[i] = appConfigs[i].getName();
			}
		}

		return appList;
	}

	
	/**
	 * process request from DAEMON to get all configuration in STC.
	 * @param appName	if appName is null, it returns all configuration. 
	 * 					if not null, targeted applciation's configuration is returned
	 * @return ApplcationConfig [] for one or all applications
	 */
	public ApplicationConfig[] getLogConfig(String appName) {

		ApplicationConfig[] appConfigs = null;

		if (Log.DEBUG_ON) {
			Log.printInfo("getLogConfig is called. appName is " + appName);
		}

		// check application name is null or not, so AppConfigManager do not get null application name.
		AppConfigManager configManager = new AppConfigManager();

		if (appName == null) {
			appConfigs = configManager.getAllLogConfig();
		} else {
			ApplicationConfig config = configManager.getOneLogConfig(appName);
			if (config != null) {
				
				appConfigs = new ApplicationConfig[1];
				appConfigs[0] = config;
			}
		}

		if (Log.DEBUG_ON) {
			if (appConfigs == null) {
				Log.printDebug("there is no appConfig stored");
			} else {
				Log.printDebug(" thre are " + appConfigs.length + " configurations.");
			}
		}
		return appConfigs;

	}

	
}

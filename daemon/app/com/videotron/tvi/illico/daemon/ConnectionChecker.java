package com.videotron.tvi.illico.daemon;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.videotron.tvi.illico.daemon.http.HttpConfig;
import com.videotron.tvi.illico.daemon.http.HttpRequest;
import com.videotron.tvi.illico.daemon.http.HttpServer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.log.Log;

/**
 * This class responsible for checking the validation for connection. Performs security check, pool size check, and
 * duplicate check.
 * @author Jinjoo Ok
 */
public class ConnectionChecker {

    /** This class instance. */
    private static ConnectionChecker instance = new ConnectionChecker();

    /** Number of maximum connection. */
    private int maxConnection;
    /** Number of current connection. */
    private int curConnection;
    /** List of trustable clients. */
    private String[] trustableClients;

    /**
     * Timeout for duplication detection. Only reject a request when the Daemon receives an identical request n time
     * within m seconds. seconds : DUPLICATE_DETECTION_TIMEOUT.
     */
    private int duplicateTimeout;
    /**
     * Only reject a request when the Daemon receives an identical request n time within m seconds. n time :
     * MAX_DUPLICATE m seconds : DUPLICATE_DETECTION_TIMEOUT.
     */
    private int maxDuplicate;

    /** The number of current duplicated connection. */
    // private int duplicateCount;
    // private Hashtable lastConnectioneTable;
    /** Constructor. */
    public ConnectionChecker() {
        maxConnection = DataCenter.getInstance().getInt("CONNECTION_POOL_SIZE");
        duplicateTimeout = DataCenter.getInstance().getInt("DUPLICATE_DETECTION_TIMEOUT");
        maxDuplicate = DataCenter.getInstance().getInt("MAX_DUPLICATE");
        // lastConnectioneTable = new Hashtable();
    }

    /**
     * Get instance.
     * @return ConnectionChecker
     */
    public static synchronized ConnectionChecker getInstance() {
        return instance;
    }

    /**
     * Increase connection.
     */
    public void increaseConnection() {
        curConnection++;
        if (Log.DEBUG_ON) {
            Log.printDebug("increaseConnection(), curConnection: " + curConnection);
        }
    }

    /**
     * Decrease connection.
     */
    public void decreaseConnection() {
        if (curConnection > 0) {
            curConnection--;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("decreaseConnection(), curConnection: " + curConnection);
        }
    }

    /**
     * Check whether the connection exceed maximum connection.
     * @return if current connection is smaller than maximum, return True.
     */
    public boolean checkExceedConnectionLimit() {
        if (Log.DEBUG_ON) {
            Log.printDebug("checkExceedConnectionLimit() maxConnection :" + maxConnection
                    + "curConnection : " + curConnection);
        }
        if (maxConnection == 0 || curConnection < maxConnection) {
            return false;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("Exceed ConnectionLimit!!");
        }
        return true;
    }

    /**
     * Get number of current connection.
     * @return connection number
     */
    public int getCurConnection() {
        if (Log.DEBUG_ON) {
            Log.printDebug("getCurConnection()  " + curConnection);
        }
        return curConnection;
    }

    /**
     * The client IP must belongs to trustable client list.
     * @param ip ip trying to connect STB
     * @return true/false
     */
    public boolean checkClient(String ip) {
        if (Environment.EMULATOR) {
            return true;
        }
        if (trustableClients == null || trustableClients.length == 0) {
            return false;
        }
        for (int i = 0; i < trustableClients.length; i++) {
            if (ip.equals(trustableClients[i])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check whether comming request is duplicated or not. Only reject a request when the Daemon receives an identical
     * request n time within m seconds.
     * @param request HttpRequest
     * @param context context name
     * @param currentTime current time(millisecond)
     * @return if the request is duplicate case return true
     */
    public boolean checkDuplicate(HttpRequest request, String context, long currentTime) {
        if (Log.DEBUG_ON) {
            Log.printDebug("checkDuplicate()");
        }
        HttpRequest lastRequest = null;
        if (request != null && context != null) {
            if (HttpServer.getInstance().checkContext(context)) {
                HttpConfig config = HttpServer.getInstance().getHttpConfig(context);
                lastRequest = config.getLastRequest();
                boolean samePath = false;
                if (lastRequest != null) {
                    samePath = request.getPathString().equals(lastRequest.getPathString());
                   if (samePath) {
                        long checkDuplicatedTime = getTimeout(lastRequest.getConnectedTime(),
                                duplicateTimeout);
                        long formatCurrentTime = Long.parseLong(DaemonMain.getDateTime(currentTime));
                        if (Log.DEBUG_ON) {
                            Log.printDebug("checkDuplicate() currentTime " + formatCurrentTime + ", timeout: "
                                    + checkDuplicatedTime + " last connectTime : "
                                    + Long.parseLong(DaemonMain.getDateTime(lastRequest.getConnectedTime())));
                        }

                        if (formatCurrentTime <= checkDuplicatedTime) {
                            int count = config.getDuplicateCount();
                            if (Log.DEBUG_ON) {
                                Log.printDebug("checkDuplicate() maxDuplicate: " + maxDuplicate
                                        + ", duplicateCount : " + count);
                            }
                            if (maxDuplicate > count) {
                                config.setDuplicateCount(++count);
                                Log.printDebug(config.getApplicationName() + " config.getDuplicateCount() "
                                        + config.getDuplicateCount());
                                if (maxDuplicate == config.getDuplicateCount()) {
                                    if (Log.DEBUG_ON) {
                                        Log.printDebug("checkDuplicate(), it's a duplicate case!!");
                                    }
                                    return true;
                                }
                            } else {
                                if (Log.DEBUG_ON) {
                                    Log.printDebug("checkDuplicate(), it's a duplicate case!!");
                                }
                                return true;
                            }
                        }
                    }
                } else {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("checkDuplicate(), lastRequest of " + context + " is null");
                    }
                }
            }
        }
        // boolean samePath = false;
        // if (lastRequest != null) {
        // samePath = request.getPathString().equals(lastRequest.getPathString());
        // }
        // if (lastRequest == null || !samePath) {
        // if (Log.DEBUG_ON) {
        // Log.printDebug("checkDuplicate(), lastRequest is null or the path is different"
        // + "so, it's not a duplicate case");
        // }
        // duplicateCount = 0;
        // return false;
        // }
        // long currentTime = Long.parseLong(current);
        // long checkDuplicatedTime = (Long.parseLong(lastRequest.getConnectedTime()) + duplicateTimeout);
        // if (Log.DEBUG_ON) {
        // Log.printDebug("checkDuplicate() currentTime " + currentTime + " timeout " + checkDuplicatedTime);
        // Log.printDebug("checkDuplicate() maxDuplicate: "
        // + maxDuplicate + ", duplicateCount : " + duplicateCount
        // + " samePath ? " + samePath);
        // }
        // if (currentTime < checkDuplicatedTime && samePath) {
        // if (maxDuplicate > duplicateCount) {
        // duplicateCount++;
        // }
        // if (maxDuplicate == duplicateCount) {
        // if (Log.DEBUG_ON) {
        // Log.printDebug("checkDuplicate(), it's a duplicate case!!");
        // }
        // return true;
        // }
        // } else {
        // duplicateCount = 0;
        // }
        return false;
    }
    /**
     * Returns the time of timeout .
     * @param date added date
     * @param timeout time(second)
     * @return timeout calculated time(millisecond)
     */
    private static long getTimeout(long date, int timeout) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(date));
        cal.add(Calendar.SECOND, timeout);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return Long.parseLong(sdf.format(cal.getTime()));
    }

    /**
     * Initialize trustable clients list.
     * @param size trustable client list size
     */
    public void initTrustableClients(int size) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ConnectionChecker.initTrustableClients = " + size);
        }
        trustableClients = new String[size];
    }

    /**
     * @param i index of list
     * @param client ip
     */
    public void setTrustableClients(int i, String client) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ConnectionChecker.setTrustableClients[" + i + "] = " + client);
        }
        trustableClients[i] = client;
    }

    // /**
    // * Save Last client.
    // *
    // * @param request HttpReqeust.
    // */
    // public void setLastRequest(HttpRequest request) {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("ConnectionChecker.setLastRequest[" + request);
    // }
    // this.lastRequest = request;
    // }

    // public void setLastRequest(String app, HttpRequest request) {
    // lastConnectioneTable.put(app, request);
    // }

}

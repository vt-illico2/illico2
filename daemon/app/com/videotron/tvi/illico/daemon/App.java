package com.videotron.tvi.illico.daemon;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
/**
 * Daemon main class.
 * @author Jinjoo Ok
 *
 */
public class App implements Xlet, ApplicationConfig {

    /** XletContext.*/
    private XletContext context;

    /** Application name. */
    public static final String APP_NAME = "Daemon";
    /** Version. */
    public static final String VERSION = "R4_1.1.0";

    /**
     * called by Monitor when STB booted.
     * @param arg0 XletContext
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext arg0) throws XletStateChangeException {
        this.context = arg0;
        DaemonMain.getInstance();
        FrameworkMain.getInstance().init(this);
        if (Log.DEBUG_ON) {
            Log.printInfo("Daemon App.initXlet");
        }
    }

    /**
     * called by Monitor when STB booted.
     * @throws XletStateChangeException s thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
    }

    /**
     * called when application destroyed.
     * @param arg0 XletContext
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *                 state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean arg0) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();
    }

    /**
     * called when application paused.
     */
    public void pauseXlet() {
    }

    /**
     * Returns name of application.
     * @return name
     */
    public String getApplicationName() {
        return APP_NAME;
    }
    /**
     * Returns version of application.
     * @return version
     */
    public String getVersion() {
        return VERSION;
    }
    /**
     * Returns XletContext.
     * @return XletContext
     */
    public XletContext getXletContext() {
        return context;
    }

	public void init() {
		DaemonMain.getInstance().start(context);
	}

}

package com.videotron.tvi.illico.daemon;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.daemon.communication.CommunicationManager;
import com.videotron.tvi.illico.daemon.http.HttpConfig;
import com.videotron.tvi.illico.daemon.http.HttpServer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.daemon.RemoteRequestListener;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * Main class of STB Daemon Application.
 * @author Jinjoo Ok
 *
 */
public final class DaemonMain implements DaemonService {

    /** Instance of this class. */
    private static DaemonMain instance = new DaemonMain();
    /** OOB data path. */
    private static final String DA_KEY_OOB = "DMN_OOB";
    /** Inband data path. */
//    private static final String DA_KEY_INBAND = "INBAND";
    /**
     * The set of host application to receive request from Daemon, registered listener to Daemon by their application
     * name.
     */
    private Hashtable listeners = new Hashtable();
    /** The version of confing file. */
    private int versionDaemonConfig;

    /** The monitor service. * */
//    private MonitorService monitorService;
    /** XletContext. */
    private XletContext xContext;
    /** Listener for Receiving oob data. */
    private DaemonDataUpdateListener dataUpdateListener = new DaemonDataUpdateListener();

    /** The index of context root. */
    private static final int INDEX_CONTEXT = 0;
    /** The index of host application name. */
    private static final int INDEX_APP_NAME = 1;
    /** The index of timeout. */
    private static final int INDEX_TIMEOUT = 2;
    /** The index of whether application active or not. */
    private static final int INDEX_ACTIVE = 3;
    /** Listener for log level change. */
    private LogLevelAdapter logListener = new LogLevelAdapter();
    /**
     * Get Daemon instance.
     *
     * @return DaemonMain
     */
    public static DaemonMain getInstance() {
        return instance;
    }
    /**
     * Constructor.
     */
    private DaemonMain() {
        DataCenter.getInstance().addDataUpdateListener(DA_KEY_OOB, dataUpdateListener);
        DataCenter.getInstance().addDataUpdateListener(StcService.IXC_NAME, dataUpdateListener);
        if (Log.DEBUG_ON) {
            Log.printDebug("addDataUpdateListener()");
        }
    }

    /**
     * Start.
     * @param ctx XletContext
     */
    public void start(XletContext ctx) {
        xContext = ctx;
        bindToIxc();
        CommunicationManager.getInstance().start(ctx);
        new HttpServer(readHTTPProperty()).start(DataCenter.getInstance().getInt("HTTP_PORT"));
//        startUdpServer();
    }
//    /**
//     * Starts UDP server.
//     */
//    private void startUdpServer() {
//        HashMap map = readUDPProperty();
//        Iterator iterator = map.values().iterator();
//        Object obj = null;
//        while (iterator.hasNext()) {
//            obj = iterator.next();
//            UdpServer s = new UdpServer((UdpConfig) obj);
//            s.start();
//        }
//    }
//    /**
//     * Reads UDP property.
//     * @return HashMap
//     */
//    private HashMap readUDPProperty() {
//        HashMap configs = new HashMap();
//        UdpConfig config = null;
//        String platform = null;
//        String[] strArray = null;
//        String active = "active";
//
//        String[] keys = new String[] {"DAEMON_UDP_CALLER"};
//        for (int i = 0; i < keys.length; i++) {
//            platform = DataCenter.getInstance().getString(keys[i]);
//            strArray = TextUtil.tokenize(platform, "/");
//            if ((strArray[INDEX_ACTIVE]).equals(active)) {
//                config = new UdpConfig();
//                try {
//                    config.setPort(Integer.parseInt(strArray[INDEX_CONTEXT]));
//                } catch (Exception ex) {
//                    Log.print(ex);
//                }
//                config.setApplicationName(strArray[INDEX_APP_NAME]);
//                config.setTimeout(Integer.parseInt(strArray[INDEX_TIMEOUT]));
//                configs.put(strArray[INDEX_APP_NAME], config);
//            }
//        }
//        return configs;
//    }
    /**
     * Read property from application property file.
     *
     * @return Hashtable configuration
     */
    private HashMap readHTTPProperty() {
        HashMap configs = new HashMap();
        HttpConfig config = null;
        String[] platformList = TextUtil.tokenize(DataCenter.getInstance().getString("APP_LIST"), "|");
        StringBuffer name = new StringBuffer();
        String platform = null;
        String[] strArray = null;
        String active = "active";
        String[] context = null;
        for (int i = 0; i < platformList.length; i++) {
            if (platformList[i].equals("PVR") && !Environment.SUPPORT_DVR) {
                continue;
            }
            name.setLength(0);
            name.append("DAEMON_HTTP_");
            name.append(platformList[i]);
            platform = DataCenter.getInstance().getString(name.toString());
            strArray = TextUtil.tokenize(platform, "/");
            context = TextUtil.tokenize(strArray[INDEX_CONTEXT], ".");
            if ((strArray[INDEX_ACTIVE]).equals(active)) {
                for (int j = 0; j < context.length; j++) {
                    config = new HttpConfig();
                    config.setContextRoot(context[j]);
                    config.setApplicationName(strArray[INDEX_APP_NAME]);
                    config.setTimeout(Integer.parseInt(strArray[INDEX_TIMEOUT]));
                    configs.put(context[j], config);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("make config app name : " + strArray[INDEX_APP_NAME]);
                    }
                }
            }
        }

        return configs;
    }

    /**
     * Get date and time in form of yyyyMMddHHmmss.
     * @param time time
     * @return yyyyMMddHHmmss
     */
    public static String getDateTime(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(new Date(time));
    }


    /**
     * Bind to IXC Registry.
     */
    private void bindToIxc() {
        try {
            if (Log.INFO_ON) {
                Log.printInfo("DaemonService: bind to IxcRegistry");
            }
            IxcRegistry.bind(xContext, DaemonService.IXC_NAME, this);
            if (Log.INFO_ON) {
                Log.printInfo("DaemonService: bind to IxcRegistry end!!");
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * Read File.
     *
     * @param file File
     */
    private void parseFile(File file) {

        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            fis = new FileInputStream(file);
            baos = new ByteArrayOutputStream();
            int len = 0;
            while ((len = fis.read()) != -1) {
                baos.write(len);
            }
            byte[] arr = baos.toByteArray();
            len = arr.length;
            int index = 0;
            int version = oneByteToInt(arr, index++);
            if (Log.INFO_ON) {
                Log.printInfo("Daemon parseData() version = " + version);
            }
            if (version != versionDaemonConfig) {
                versionDaemonConfig = version;
                len = oneByteToInt(arr, index++);
                if (Log.DEBUG_ON) {
                    Log.printDebug("trustable_client_list_size " + len);
                }
                ConnectionChecker.getInstance().initTrustableClients(len);
                String ip = null;
                for (int i = 0; i < len; i++) {
                    ip = new String(arr, index, 15);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("trustable_client_list_ip " + ip);
                    }
                    ConnectionChecker.getInstance().setTrustableClients(i, ip.trim());
                    index += 15;
                }
            }

        } catch (Exception e) {
            Log.print(e);
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }
    }

    /**
     * Change one byte to integer.
     * @param data byte array
     * @param offset index
     * @return integer
     */
    private static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]) & 0xFF);
    }

    /**
     * Get listener.
     * @param appName Host application
     * @return RemoteRequestListener
     */
    public RemoteRequestListener getListener(String appName) {

//        if (Log.DEBUG_ON) {
//            Log.printDebug(" listeners " + listeners.size() + " name : " + appName);
//            Log.printDebug(" Daemon contains key ? " + listeners.containsKey(appName));
//        }
        return (RemoteRequestListener) listeners.get(appName);
    }

    public void stop() {

    }

    /**
     * Add listener.
     * @param appName host application name
     * @param i RemoteRequestListener
     */
    public void addListener(String appName, RemoteRequestListener i) {
        if (i == null) {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("addListener: appName = " + appName + ", RemoteRequestListener == null");
        	}
        } else {
        	if (Log.DEBUG_ON) {
        		Log.printDebug("addListener: appName = " + appName + ", RemoteRequestListener = " + i);
        	}
            listeners.put(appName, i);
        }
    }

    /**
     * Remove listener.
     * @param appName host application name
     */
    public void removeListener(String appName) {
        listeners.remove(appName);
    }

//    public void sendRequestByUDP(DatagramPacket packet) throws Exception{
     //   try {
//            udpServer.sendRequest(packet);
//        } catch (IOException e){
//
//        }
//    }

    /**
     * DataUpdate listener.
     */
    class DaemonDataUpdateListener implements DataUpdateListener {
        /**
         * Called when data has been removed.
         *
         * @param key key of data.
         */
        public void dataRemoved(String key) {
        }

        /**
         * Called when data has been updated.
         *
         * @param key key of data.
         * @param old old value of data.
         * @param value new value of data.
         */
        public void dataUpdated(String key, Object old, Object value) {
            if (key.equals(DA_KEY_OOB) && value != null) {
                parseFile((File) value);
            } else if (key.equals(StcService.IXC_NAME) && value != null) {
            	try {
                    int currentLevel = ((StcService) value).registerApp(App.APP_NAME);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("stc log level : " + currentLevel);
                    }
                    Log.setStcLevel(currentLevel);
                    Object o = SharedMemory.getInstance().get("stc-" + App.APP_NAME);
                    if (o != null) {
                        DataCenter.getInstance().put("LogCache", o);
                    }
                    try {
                        ((StcService) value).addLogLevelChangeListener(App.APP_NAME, logListener);
                    } catch (RemoteException ex) {
                        Log.print(ex);
                    }

                 } catch (Exception e) {
                   Log.print(e);
                 }
            }
        }
    }
    /**
     * Log level change listener.
     * @author Jinjoo Ok
     *
     */
    class LogLevelAdapter implements LogLevelChangeListener {
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    /**
     * for test.
     * @param arg
     * @throws Exception
     */
//     public static void main(String[] arg) throws Exception {
//
////     new HttpServer(null).start(8080);
//         UdpConfig con = new UdpConfig();
//         con.setApplicationName("Caller");
//         con.setPort(10101);
//         con.setTimeout(0);
//         new UdpServer(con);
//
//     }
}

/**
 * @(#)CommunicationManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.daemon.communication;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * The CommunicationManager class lookup a remote Service of Other Applcation.
 * @author Woojung Kim
 * @version 1.1
 */
public class CommunicationManager {

    /** The XletContext of DAEMON Application. */
    private XletContext xletContext;
    /** The instance of CommunicationManager. */
    private static CommunicationManager instance = new CommunicationManager();

    /** The PreferenceService to communicate with Profile App. */
    PreferenceService pService;

    IxcLookupWorker ixcWorker;

    /**
     * Instantiates a new CommunicationManager.
     */
    public static CommunicationManager getInstance() {
        return instance;
    }

    /**
     * Start a lookups.
     * @param xletContext
     */
    public void start(XletContext xletContext) {
        this.xletContext = xletContext;
        this.ixcWorker = new IxcLookupWorker(xletContext);

        ixcWorker.lookup("/1/2030/", PreferenceService.IXC_NAME);
        ixcWorker.lookup("/1/2100/", StcService.IXC_NAME);
    }

    /**
     * Dispose a resources.
     */
    public void dispose() {
    	pService = null;
    }

    /**
     * Return a PreferenceService.
     * @return The MonitorService object to be lookup.
     */
    public PreferenceService getPreferenceService() {
        if (pService == null) {
        	pService = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        }
        return pService;
    }
}

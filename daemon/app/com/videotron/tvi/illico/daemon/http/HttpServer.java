package com.videotron.tvi.illico.daemon.http;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

/**
 * Create a ServerSocket with specified port to receive connection from client. Create HttpRequest when connection with
 * client is established.
 *
 * @author Jinjoo Ok
 *
 */
public class HttpServer extends Thread {

    /** The response code "OK". */
    public static final int RESPONSE_OK = 200;
    /** The URL format is invalid. */
    public static final int ERROR_FORMAT_INVALID = 400;
    /** Client is not a Trustable Client. */
    public static final int ERROR_NOT_TRUSTABLE_CLIENT = 401;
    /** The request is a duplicate. */
    public static final int ERROR_DUPLICATE = 401;
    /** Connection is full. */
    public static final int ERROR_CONNECTION_FULL = 403;
    /** No host application is mapped to it. */
    public static final int ERROR_NO_SERVICE_FOUND = 404;
    /** HTTP header is missing. */
    public static final int ERROR_HEADER_MISSING = 411;
    /** Host application is not available. */
    public static final int ERROR_APPLICATION_NOT_AVAILABLE = 502;
    /** Service timeout. */
    public static final int ERROR_TIMEOUT = 504;
    /** Internal server error. */
    public static final int ERROR_INTERNAL_SERVER = 500;
    /** Error code - bad request. */
    public static final String ERROR_CODE_BAD_REQUEST = "DMN500";
    /** Error code - Unauthorized IP. */
    public static final String ERROR_CODE_UNAUTHORIZED_IP = "DMN501";
    /** Error code - The Request is Duplicated. */
    public static final String ERROR_CODE_DUPLICATED = "DMN502";
    /** Error code - Too many Connections. */
    public static final String ERROR_CODE_CONNECTION_FULL = "DMN503";
    /** Error code - Service Not Found. */
    public static final String ERROR_CODE_NO_SERVICE_FOUND = "DMN504";
    /** Error code - Connection Timeout. */
    public static final String ERROR_CODE_TIMEOUT = "DMN505";
    /** Error code - Internal Server Error. */
    public static final String ERROR_CODE_INTERNAL_SERVER = "DMN506";
    /** Error code - Header Missing. */
    public static final String ERROR_CODE_HEADER_MISSING = "DMN507";
    /** Error code - Service Not Available. */
    public static final String ERROR_CODE_APPLICATION_NOT_AVAILABLE = "DMN508";


    /** HttpServer. */
    private static HttpServer instance;
    /** ServerSocket. */
    private ServerSocket serverSocket;
    /** Set of configuration data. */
    private HashMap configs;
    /** Server port. */
    private int port;
    /**
     * Constructor.
     *
     * @param config Set of HttpConfig
     */
    public HttpServer(final HashMap config) {
        this.configs = config;
        instance = this;
    }

    /**
     * Get HttpServer.
     * @return HttpServer
     */
    public static HttpServer getInstance() {
        return instance;
    }

    /**
     * Start Server.
     */
    private void startServer() {
           try {
               serverSocket = new ServerSocket(port);
           } catch (Exception e) {
               FrameworkMain.getInstance().printSimpleLog("" + e);
               Log.print(e);
           }
            FrameworkMain.getInstance().printSimpleLog("created ServerSocket with port = " + port);

            Log.printDebug("created ServerSocket with port = " + port);

            Socket socket = null;
            HttpRequest request = null;
            int requestID = 0;
            while (true) {
                try {
                    socket = serverSocket.accept();
                    Log.printDebug("accepted socket = " + socket);

                    // code to check socket state.
//                    if (requestID == 3) {
//                    	serverSocket.close();
//
//                    	if (Log.DEBUG_ON) {
//                    		Log.printDebug("serverSocket closed forcely");
//                    	}
//                    }

                    InetAddress clientInet = socket.getInetAddress();
                    FrameworkMain.getInstance().printSimpleLog("client = " + clientInet);
                    requestID++;
                    if (requestID > 10000) {
                        requestID = 0;
                    }
                    if (Log.INFO_ON) {
                        Log.printInfo(clientInet.getHostAddress() + " access to Daemon.");
                    }
                    request = new HttpRequest(socket, requestID);
                    request.start();
                } catch (SocketTimeoutException se) {
                    //Log.printDebug("ServerSocket accept time out - Waiting..");
                } catch (IOException e) {
                	FrameworkMain.getInstance().printSimpleLog("" + e);
                    Log.print(e);

                    try {
                        Thread.sleep(1000L);
                    } catch (Exception ex) {
                    }

                    if (Log.DEBUG_ON) {
                    	if (serverSocket != null) {
                    		Log.printDebug("serverSocket isClosed = " + serverSocket.isClosed());
                    	} else {
                    		Log.printDebug("serverSocket is null");
                    	}
                    }

                    if (serverSocket == null || serverSocket.isClosed()) {
                    	if (Log.DEBUG_ON) {
                        	Log.printDebug("serverSocket recreate");
                        }
                    	try {
                    		serverSocket = new ServerSocket(port);
						} catch (IOException e1) {
							Log.printWarning("serverSocket close error");
							Log.print(e1);
						}
                    }
                }

            }

    }
    /**
     * This class start.
     *
     * @param serverPort port number
     */
    public void start(int serverPort) {
        this.port = serverPort;
        this.start();

    }

    /**
     * Check whether configuration file include context key.
     *
     * @param context context root
     * @return True or false.
     */
    public boolean checkContext(String context) {
        if (Log.DEBUG_ON) {
            Log.printDebug("checkContext " + context);
        }
        if (configs == null) {
            return false;
        }

        return configs.containsKey(context);
    }

    /**
     * Get HttpConfig.
     *
     * @param context appName of URL request.
     * @return HttpConfig HttpConfig
     */
    public HttpConfig getHttpConfig(String context) {
        return (HttpConfig) configs.get(context);
    }

    /**
     * run() of Thread class.
     */
    public void run() {
        startServer();
    }
}

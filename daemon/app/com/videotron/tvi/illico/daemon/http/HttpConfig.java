package com.videotron.tvi.illico.daemon.http;

/**
 * This class representing the configuration per HTTP Service. Only one HttpServer is created therefore multiple
 * HttpConfig[] are specified.
 *
 * @author Jinjoo Ok
 *
 */
public class HttpConfig {

    /**
     * Context root included in URL. Host application may have multiple context root.
     */
    private String contextRoot;

    /** Host Application. */
    private String applicationName;

    /** Socket timeout. timeout 0 means socket has no timeout. */
    private int timeout;
    /** The last request. */
    private HttpRequest lastRequest;
    /** The number of duplicate request before timeout. */
    private int duplicateCount;

    /**
     * Get context root.
     *
     * @return name of context root
     */
    public String getContextRoot() {
        return contextRoot;
    }

    /**
     * Set context root.
     *
     * @param context Remote service mapping
     */
    public void setContextRoot(String context) {
        this.contextRoot = context;
    }

    /**
     * Get application name.
     *
     * @return name of application.
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Set application name.
     *
     * @param name host application name
     */
    public void setApplicationName(String name) {
        this.applicationName = name;
    }

    /**
     * Get timeout.
     *
     * @return seconds of time out
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Set timeout.
     *
     * @param time number of timeout
     */
    public void setTimeout(int time) {
        this.timeout = time;
    }

   /**
   * Save Last client.
   * @param req HttpReqeust.
   */
    public void setLastHttpRequest(HttpRequest req) {
        lastRequest = req;
        duplicateCount = 0;
    }

    /**
     * Gets last request.
     * @return last request
     */
    public HttpRequest getLastRequest() {
        return lastRequest;
    }

    /**
     * Gets the number of duplicate request before timeout.
     * @return duplicate count
     */
    public int getDuplicateCount() {
        return duplicateCount;
    }
    /**
     * Sets the number of duplicate request before timeout.
     * @param count duplicate count
     */
    public void setDuplicateCount(int count) {
        duplicateCount = count;
    }
}

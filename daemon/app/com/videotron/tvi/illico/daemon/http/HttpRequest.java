package com.videotron.tvi.illico.daemon.http;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.daemon.ConnectionChecker;
import com.videotron.tvi.illico.daemon.DaemonMain;
import com.videotron.tvi.illico.daemon.communication.CommunicationManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.daemon.RemoteRequestListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * HttpRequest parse HTTP message. Passes a parsed message to specified application, and receives response from
 * specified application and build appropriate HTTP response. Received response is sent back to client. Allow multiple
 * clients to connect simultaneously by extending thread class.
 * @author Jinjoo Ok
 */
public class HttpRequest extends Thread implements TVTimerWentOffListener {

    /** Socket. */
    private Socket socket;
    /** Timer for checking timeout. */
    private TVTimerSpec timerSpec;
    /** Time stamp. */
    private long connectedTime;
    /** Time stamp. */
    private long sentTime;
    /** Path string of HTTP request. */
    private String pathString;
    /** Date Format used HTTP response. */
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");

    /** Client Ip. */
    private String ip;
    /** this server ip & port. */
    private String serverIpPort;
    /** The number of connection timeout. */
    private int connectionTimeout;
    /** Count for checking timeout. */
    private int timerCount;
    /** The number of timeout. */
    private int serviceTimeout;
    /** Response status code. */
    private int status;
    /** Buffer size. */
    private static final int BUFFER_SIZE = 1024;
    /** it is incremented on every request received. Maximum is 10000. */
    private int requestId;

    /**
     * Constructor.
     * @param soc Socket
     * @param id id incremented on every request received. Maximum is 10000.
     */
    public HttpRequest(final Socket soc, final int id) {
        this.socket = soc;
        connectionTimeout = DataCenter.getInstance().getInt("CONNECTION_TIMEOUT");
        timerSpec = new TVTimerSpec();
        requestId = id;
    }

    /**
     * Read path and parameters from Inputstream of socket.
     * @return URL path except IP, port
     * @throws IOException I/O error
     */
    private String readRequestPath() throws IOException {
        BufferedInputStream bi = new BufferedInputStream(socket.getInputStream());
        int size = bi.available();
        if (size == 0) {
            size = BUFFER_SIZE;
        }
        byte[] tempArray = new byte[size];
        bi.read(tempArray);
        String[] s = TextUtil.tokenize(new String(tempArray).trim(), "\r\n");
        StringBuffer firstLine = new StringBuffer(s[0]);
        String host = "host";
        if (firstLine.toString().startsWith("GET")) {
            int index = firstLine.indexOf(" ");
            for (int i = 0; i < s.length; i++) {
                s[i] = s[i].toLowerCase();
                if (s[i].indexOf(host) != -1) {
                    serverIpPort = s[i].substring(host.length() + 1).trim();
                    break;
                }
            }
            return firstLine.substring(index + 1, firstLine.indexOf("HTTP") - 1);
        } else if (firstLine.toString().startsWith("POST")) {
            boolean detectConncetion = false;
            boolean detectType = false;
            boolean detectLength = false;
            String connection = "connection";
            String type = "content-type";
            String length = "content-length";
            for (int i = 0; i < s.length; i++) {
                s[i] = s[i].toLowerCase();
                if (s[i].indexOf(connection) != -1) {
                    detectConncetion = true;
                } else if (s[i].indexOf(type) != -1) {
                    detectType = true;
                } else if (s[i].indexOf(length) != -1) {
                    detectLength = true;
                } else if (s[i].indexOf(host) != -1) {
                    serverIpPort = s[i].substring(host.length() + 1).trim();
                }
            }

            if (!detectConncetion || !detectType || !detectLength) {
                String msg = null;
                if (!detectType) {
                    msg = "<Content-Type> Required";
                } else if (!detectLength) {
                    msg = "<Content-Length> Required";
                } else if (!detectConncetion) {
                    msg = "<Connection> Required";
                }
                receiveLog(msg);
                sendErrorResponse(HttpServer.ERROR_HEADER_MISSING, msg);
                responseLog(msg);
                if (Log.ERROR_ON) {
                    Log.printError(HttpServer.ERROR_CODE_HEADER_MISSING + " : " + msg);
                }
                return null;
            }
            int index = firstLine.indexOf(" ");
            StringBuffer tempPath = null;
            // body including full url
            tempPath = new StringBuffer(firstLine.substring(index + 1, firstLine.indexOf("HTTP") - 1));
            tempPath.append("?");
            tempPath.append(s[s.length - 1]);
            return tempPath.toString();
        }
        return null;

    }

    /**
     * Start timer.
     */
    private void startTimer() {

        synchronized (timerSpec) {
            stopTimer();
            timerCount = 0;
            try {
                timerSpec.setDelayTime(Constants.MS_PER_SECOND);
                timerSpec.setRepeat(true);
                timerSpec.addTVTimerWentOffListener(this);
                TVTimer.getTimer().scheduleTimerSpec(timerSpec);
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }
    }

    /**
     * Stop timer.
     */
    private void stopTimer() {

        synchronized (timerSpec) {
            timerCount = 0;
            timerSpec.removeTVTimerWentOffListener(this);
            TVTimer.getTimer().deschedule(timerSpec);
        }
    }

    /**
     * Send response to client.
     * @param message message will be sent to client
     * @param result message form of byte array received by host application
     * @throws IOException I/O error
     */
    private void sendResponse(String message, byte[] result) throws IOException {
        int length = message.length() + result.length;
        OutputStream out = null;
        try {
            out = sendResponseHeaders(message, length);
            out.write(result);
            out.flush();
            if (Log.INFO_ON) {
                Log.printInfo("sendResponse msg: " + new String(result));
            }
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Write Header.
     * @param message message
     * @param len message length
     * @return OutputStream
     * @throws IOException I/O error
     */
    private OutputStream sendResponseHeaders(String message, long len) throws IOException {

        StringBuffer response = new StringBuffer();
        response.append("HTTP/1.1 ");
        response.append(status).append(" ").append(message).append("\r\n");
        response.append("Date: ").append(DATE_FORMAT.format(new Date())).append("\r\n");
        response.append("Client IP: ").append(ip).append("\r\n");
        response.append("Content-Type: text/plain").append("\r\n");
        response.append("Content-Length: ").append(len).append("\r\n\r\n");

        OutputStream out = socket.getOutputStream();
        out.write(response.toString().getBytes());
        sentTime = System.currentTimeMillis();

        return out;
    }

    /**
     * Send Error Response to client.
     * @param code error code
     * @param message error message
     * @throws IOException when an I/O error occur.
     */
    private void sendErrorResponse(int code, String message) throws IOException {
        // if (Log.DEBUG_ON) {
        // Log.printDebug("sendErrorResponse !! " + message);
        // }
        status = code;
        OutputStream out = null;
        try {
            out = sendResponseHeaders(message, message.length());
            out.write(message.getBytes());
            out.flush();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Check validation of string array.
     * @param arr String array
     * @return true or false
     */
    private boolean isValid(String[] arr) {
        if (arr == null || arr.length < 2) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == null || arr[i].length() == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * run() of Thread.
     */
    public void run() {
        InetAddress inet = socket.getInetAddress();
        // String host = inet.getHostName();
        String[] element = null;
        String msg = null;
        HttpConfig config = null;
        boolean increased = false;
        try {
            connectedTime = System.currentTimeMillis();
            if (connectionTimeout > 0) {
                socket.setSoTimeout(connectionTimeout);
            }
            ip = inet.getHostAddress();
            pathString = readRequestPath();
            if (pathString != null) {
                StringBuffer path = new StringBuffer(pathString);
                element = TextUtil.tokenize(path.substring(1), "/");

                if (!ConnectionChecker.getInstance().checkClient(ip)) {
                    msg = "Unauthorized";
                    receiveLog(msg);
                    sendErrorResponse(HttpServer.ERROR_NOT_TRUSTABLE_CLIENT, msg);
                    if (Log.WARNING_ON) {
                        Log.printWarning(HttpServer.ERROR_CODE_UNAUTHORIZED_IP + " : " + ip);
                    }
                } else if (path.toString() == null || !isValid(element)) {
                    msg = "Bad Request";
                    receiveLog(msg);
                    sendErrorResponse(HttpServer.ERROR_FORMAT_INVALID, msg);
                    if (Log.ERROR_ON) {
                        Log.printError(HttpServer.ERROR_CODE_BAD_REQUEST + " : " + msg);
                    }
                } else if (ConnectionChecker.getInstance()
                        .checkDuplicate(this, element[0].toLowerCase(), connectedTime)) {
                    msg = "Request Duplicated";
                    receiveLog(msg);
                    sendErrorResponse(HttpServer.ERROR_DUPLICATE, msg);
                    if (Log.WARNING_ON) {
                        Log.printWarning(HttpServer.ERROR_CODE_DUPLICATED + " : " + msg);
                    }
                } else if (ConnectionChecker.getInstance().checkExceedConnectionLimit()) {
                    msg = "Too Many Connections";
                    receiveLog(msg);
                    sendErrorResponse(HttpServer.ERROR_CONNECTION_FULL, msg);
                    if (Log.ERROR_ON) {
                        Log.printError(HttpServer.ERROR_CODE_CONNECTION_FULL + " : " + msg);
                    }
                } else {
                    String context = element[0].toLowerCase();
                    if (HttpServer.getInstance().checkContext(context)) {
                        config = HttpServer.getInstance().getHttpConfig(context);
                        String destinationApp = config.getApplicationName();
                        RemoteRequestListener listener = DaemonMain.getInstance().getListener(destinationApp);
                        if (listener == null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("listener {" + destinationApp + "} is null");
                            }
                            msg = "Service Unavailable";
                            receiveLog(msg);
                            sendErrorResponse(HttpServer.ERROR_APPLICATION_NOT_AVAILABLE, msg);
                            if (Log.ERROR_ON) {
                                Log.printError(HttpServer.ERROR_CODE_APPLICATION_NOT_AVAILABLE + " : path=" + pathString);
                            }
                        } else {
                            receiveLog("OK");
                            int timeout = config.getTimeout();
                            if (timeout != 0) {
                                serviceTimeout = timeout;
                            } else {
                                serviceTimeout = DataCenter.getInstance().getInt("SERVICE_TIMEOUT");
                            }
                            startTimer();
                            ConnectionChecker.getInstance().increaseConnection();
                            increased = true;
                            byte[] response = null;
                            path.setLength(0);
                            path.append(element[1]);
                            int index = path.indexOf("?");
                            if (index != -1) {
                                response = listener.request(path.substring(0, index),
                                        path.substring(index + 1, path.length()).getBytes());
                            } else {
                                response = listener.request(path.toString(), TextUtil.EMPTY_STRING.getBytes());
                            }
                            status = HttpServer.RESPONSE_OK;
                            sendResponse("OK", response);
                            stopTimer();
                            
                            if (Log.DEBUG_ON) {
                            	Log.printDebug("HttpRequest, run(), destinationApp=" + destinationApp);
                            }
                            
                            if (destinationApp.equals("EPG") || destinationApp.equals("PVR") || destinationApp.equals("VOD")
                            		|| destinationApp.equals("COMP")) {
                            	
                            	PreferenceService pService = CommunicationManager.getInstance().getPreferenceService();
                            	
                            	if (Log.DEBUG_ON) {
                                	Log.printDebug("HttpRequest, run(), pService=" + pService);
                                }
                            	
                            	if (pService != null) {
                            		pService.resetAPDTimer();
                            	}
                            }
                        }
                    } else {
                        msg = "Service Not Found.";
                        receiveLog("OK");
                        sendErrorResponse(HttpServer.ERROR_NO_SERVICE_FOUND, msg);
                        if (Log.ERROR_ON) {
                            Log.printError(HttpServer.ERROR_CODE_NO_SERVICE_FOUND + " : path=" + pathString);;
                        }
                    }
                }
            }
            FrameworkMain.getInstance().printSimpleLog("Daemon url path : " + serverIpPort + pathString);
        } catch (SocketException se) {
            try {
                msg = "Connection Timeout";
                sendErrorResponse(HttpServer.ERROR_TIMEOUT, msg);
                Log.print(se);
            } catch (Exception ee) {
                Log.print(ee);
            }
        } catch (IOException e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("IOException requestId " + requestId);
            }
            try {
                msg = "Internal Server Error";
                sendErrorResponse(HttpServer.ERROR_INTERNAL_SERVER, msg);
                Log.print(e);
            } catch (Exception ee) {
                Log.print(ee);
            }
        } catch (Exception except) {
            Log.print(except);
        } finally {
            if (increased) {
                ConnectionChecker.getInstance().decreaseConnection();
            }
            if (status == HttpServer.RESPONSE_OK) {
                if (config != null) {
                    config.setLastHttpRequest(this);
                }
            }
            if (pathString != null) {
                responseLog(msg);
            }
            try {
                if (!socket.isClosed()) {
                    socket.close();
                }
            } catch (Exception e) {
                Log.print(e);
            }

            if (Log.DEBUG_ON) {
                Log.printDebug(status + "<- staus,, Socket close !! , curConnection: "
                        + ConnectionChecker.getInstance().getCurConnection() + "requestID : " + requestId);
            }
        }
    }

    /**
     * Log for sent response.
     * @param msg description
     */
    private void responseLog(String msg) {

        String time = DaemonMain.getDateTime(sentTime);
        if (status == HttpServer.RESPONSE_OK) {
            if (Log.INFO_ON) {
                Log.printInfo("ResponseLog requestID : " + requestId);
                Log.printInfo("ResponseLog client ip : " + ip);
                Log.printInfo("ResponseLog sent time  : " + time);
                Log.printInfo("ResponseLog status : " + status + ", Success");
            }
        } else if (Log.WARNING_ON) {
            Log.printWarning("ResponseLog requestID : " + requestId);
            Log.printWarning("ResponseLog full url : " + serverIpPort + pathString);
            Log.printWarning("ResponseLog client ip : " + ip);
            Log.printWarning("ResponseLog sent time  : " + time);
            Log.printWarning("ResponseLog status : " + status + ", " + msg);
        }

    }

    /**
     * Log for received request.
     * @param msg description
     */
    private void receiveLog(String msg) {
        if (Log.INFO_ON) {
            Log.printInfo("-------------------------------------");
            Log.printInfo("ReceiveLog requestID : " + requestId);
            Log.printInfo("ReceiveLog full url : " + serverIpPort + pathString);
            Log.printInfo("ReceiveLog client ip : " + ip);
            Log.printInfo("ReceiveLog receivec time : " + DaemonMain.getDateTime(connectedTime));
            Log.printInfo("ReceiveLog status : " + msg);
        }
    }

    /**
     * Timer.
     * @param arg0 Timer event
     */
    public void timerWentOff(TVTimerWentOffEvent arg0) {
        timerCount++;
        if (timerCount >= serviceTimeout && status == 0) {
            stopTimer();
            try {
                sendErrorResponse(HttpServer.ERROR_TIMEOUT, "Service Timeout");
                responseLog("Service Timeout");
            } catch (Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError(e);
                }
            }
        }
    }

    /**
     * Get request path.
     * @return path
     */
    public String getPathString() {
        return pathString;
    }

    /**
     * Get the time connected to Daemon.
     * @return time
     */
    public long getConnectedTime() {
        return connectedTime;
    }

}

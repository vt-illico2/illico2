package com.videotron.tvi.illico.daemon.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import com.videotron.tvi.illico.daemon.ConnectionChecker;
import com.videotron.tvi.illico.daemon.DaemonMain;
import com.videotron.tvi.illico.ixc.daemon.RemoteRequestListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * UdpServer: send received UDP packet to application and,
 * optionally, receives response from it and send it back to client as a UDP message.
 * @author Jinjoo Ok
 *
 */
public class UdpServer extends Thread {
    /** configuration data. */
    private UdpConfig config;
    /** Server socket. */
    private DatagramSocket socket;
    /** it is incremented on every request received. Maximum is 10000. */
    private int requestId;
    /** Client Ip. */
    private String ip;
    /** Time stamp. */
    private long connectedTime;
    /** Time stamp. */
    private long sentTime;
    /** Response status code. */
    private String status;
    /**
     * Constructor.
     * @param udp UdpConfig
     */
    public UdpServer(final UdpConfig udp) {
        this.config = udp;
    }

    /**
     * Send response.
     * @param inet InetAddress
     * @param port port number
     * @param packet data
     * @throws IOException IOException I/O error
     */
    public void sendResponse(InetAddress inet, int port, byte[] packet)  throws IOException {
        if (Log.DEBUG_ON) {
            Log.printDebug("sendResponse====port " + port + " packet len: " + packet.length);
        }
        DatagramPacket out = new DatagramPacket(packet, packet.length, inet, port);
        socket.send(out);
    }

    public void sendRequest(DatagramPacket packet)  throws IOException {
        if (Log.DEBUG_ON) {
            Log.printDebug("sendRequest====port " + packet.getPort());
        }
        socket.connect(packet.getAddress(), packet.getPort());
        socket.send(packet);
    }

    /**
     * Start Thread.
     */
    public void start() {
        try {
            if (Log.DEBUG_ON) {
                Log.printDebug("Start UdpServer " + config.getApplicationName() + " listening port "
                        + config.getPort());
            }
            socket = new DatagramSocket(config.getPort());
        } catch (Exception e) {
            Log.print(e);
        }
        super.start();
    }
    /**
     * Log for received request.
     * @param msg description
     */
    private void receiveLog(String msg) {
        if (Log.INFO_ON) {
            Log.printInfo("ReceiveLog requestID : " + requestId);
            Log.printInfo("ReceiveLog client ip : " + ip);
            Log.printInfo("ReceiveLog receivec time : " + DaemonMain.getDateTime(connectedTime));
            Log.printInfo("ReceiveLog status : " + msg);
        }
    }
    /**
     * Log for sent response.
     * @param msg description
     */
    private void responseLog(String msg) {

        String time = DaemonMain.getDateTime(sentTime);
        if (msg.equals("OK")) {
            if (Log.INFO_ON) {
                Log.printInfo("ResponseLog requestID : " + requestId);
                Log.printInfo("ResponseLog client ip : " + ip);
                Log.printInfo("ResponseLog sent time  : " + time);
                Log.printInfo("ResponseLog status : " + status);
            }
        } else if (Log.WARNING_ON) {
            Log.printWarning("ResponseLog requestID : " + requestId);
            Log.printWarning("ResponseLog client ip : " + ip);
            Log.printWarning("ResponseLog sent time  : " + time);
            Log.printWarning("ResponseLog status : " + status + ", " + msg);
        }

    }
    /**
     * run() of Thread class.
     */
    public void run() {

        byte[] buffer = new byte[2048];
        try {
            DatagramPacket in = null;
            InetAddress inet = null;
            if (Log.INFO_ON) {
                Log.printInfo("UDP server run start : ");
            }
            if (config.getTimeout() > 0) {
                if (Log.INFO_ON) {
                    Log.printInfo("timeout : " + config.getTimeout());
                }
                socket.setSoTimeout(config.getTimeout());
            }
             while (true) {
                requestId++;
                if (requestId > 10000) {
                    requestId = 0;
                }
                in = new DatagramPacket(buffer, 2048);
                socket.receive(in);
                inet = in.getAddress();
                ip = inet.getHostAddress();
                connectedTime = System.currentTimeMillis();
                if (Log.INFO_ON) {
                    Log.printInfo("HostAddress : " + inet.getHostAddress() + " " +in.getPort());
                }
                if (!ConnectionChecker.getInstance().checkClient(ip)) {
                    if (Log.INFO_ON) {
                        Log.printInfo("Unauthorized ip : " + ip);
                    }
                    receiveLog("Unauthorized");
                } else if (ConnectionChecker.getInstance().checkExceedConnectionLimit()) {
                    if (Log.INFO_ON) {
                        Log.printInfo("Over ConnectionLimit ");
                    }
                    receiveLog("Over ConnectionLimit");
                } else {

                    RemoteRequestListener listener = DaemonMain.getInstance().getListener(
                            config.getApplicationName());
                    if (listener != null) {
                        ConnectionChecker.getInstance().increaseConnection();
                        byte[] buf = in.getData();
                        byte[] actualBuf = new byte[in.getLength()];
                        if (Log.DEBUG_ON) {
                            Log.printInfo("in.getLength() " + in.getLength());
                        }
                        System.arraycopy(buf, 0, actualBuf, 0, actualBuf.length);
                        if (Log.DEBUG_ON) {
                            Log.printInfo("msg " + new String(actualBuf));
                        }
                        byte[] response = listener.request(null, actualBuf);
                        if (response != null) {
                            sendResponse(inet, in.getPort(), response);
                        }
                        ConnectionChecker.getInstance().decreaseConnection();
                    } else {
                        if (Log.INFO_ON) {
                            Log.printInfo("RemoteRequestListener is null ");
                        }
                        byte[] d = in.getData();
                        socket.send(new DatagramPacket(d, d.length, inet, in.getPort()));
                        responseLog("OK");
                    }
                }
                try {
                    Thread.sleep(Constants.MS_PER_SECOND);
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        } catch (Exception e) {
            responseLog(e.getMessage());
          Log.print(e);
        } finally {
            if (socket != null) {
                socket.close();
                socket = null;
            }
        }

    }

}

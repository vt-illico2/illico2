package com.videotron.tvi.illico.daemon.udp;
/**
 * UdpConfig: class representing configuration per UDP Service.
 * Unlike HTTP, one UDP port is mapped to one application and a number of UdpService can be
 *  generated depending on the configuration.  Each UdpService will have one UdpConfig
 * @author Jinjoo Ok
 *
 */
public class UdpConfig {
    /** port.    */
    private int port;
    /** name of application. */
    private String applicationName;
    /** number of timeout. */
    private int timeout;
    /**
     * Gets port.
     * @return port
     */
    public int getPort() {
        return port;
    }
    /**
     * Sets port.
     * @param p port
     */
    public void setPort(int p) {
        this.port = p;
    }
    /**
     * Gets application name.
     * @return application name.
     */
    public String getApplicationName() {
        return applicationName;
    }
    /**
     * Sets application name.
     * @param name application name
     */
    public void setApplicationName(String name) {
        this.applicationName = name;
    }

    /**
     * Gets timeout.
     * @return number of timeout
     */
    public int getTimeout() {
        return timeout;
    }
    /**
     * Sets timeout.
     * @param time timeout
     */
    public void setTimeout(int time) {
        this.timeout = time;
    }
}

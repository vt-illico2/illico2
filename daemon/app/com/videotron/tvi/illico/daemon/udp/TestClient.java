package com.videotron.tvi.illico.daemon.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.Date;

import com.videotron.tvi.illico.daemon.ConnectionChecker;
import com.videotron.tvi.illico.daemon.DaemonMain;
import com.videotron.tvi.illico.daemon.http.HttpServer;
import com.videotron.tvi.illico.ixc.daemon.RemoteRequestListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

public class TestClient {
    static StringBuffer sb;

    public static void main(String[] arg) {
        DatagramSocket socket = null;
        DatagramPacket out = null;
        DatagramPacket in = null;
        InetAddress serverInet = null;
        // byte[] packet = new byte[1024];
        try {

            // sb = new StringBuffer("454249460400020000160b");
            // sb.append("48cfff00011d0000650000b8000002060000140000004f0001001200000008ff050200000c040002c2020006c2020008");
            // sb.append("0502000a0502000c0502000e0502001000070000000300150027003900490074");
            // sb.append("00b10001000500000002ff0e03000005020003ff7fff00000005332e342e31000c303130313032303230333033020400");
            // sb.append("0300030000082300000821000007210204000300030000008500000085000000");
            // sb.append("85000e7777777c466f76656f6e7c5e3438002928363032292034353634353637");
            // sb.append("7c283630322920343536343536377c28363032292034353634353637003b3230");
            // sb.append("3130204d41522030342030343a3332414d7c32303130204d4152203034203034");
            // sb.append("3a3136414d7c32303130204a414e2031312030383a3137414d0005317c317c310d0a");
            // serverInet = InetAddress.getByName("10.247.16.134");
            Server server = new Server();
            server.start();
            serverInet = InetAddress.getByName("192.168.11.2");
            socket = new DatagramSocket();
             socket.connect(serverInet, 9275);
            out = new DatagramPacket(
                    //"<Ack>000E35463FA1</>".getBytes(), "<Reg>000E35463FA1</>".getBytes().length,
                    "<NewCall>000E35463FA1|15|(514) 759-2159|RECEPTION|(514) 902-0000</>".getBytes(),
                    "<NewCall>000E35463FA1|15|(514) 759-2159|RECEPTION|(514) 902-0000</>".getBytes().length,
                    serverInet, 9275);
            // in = new DatagramPacket(packet, 1024);
            socket.send(out);
//            System.out.println(socket.isConnected());
            // String word = null;
            // while (true) {
            // socket.receive(in);
            // word = new String(in.getData(), 0, in.getLength()).trim();
            // if ((word = new String(in.getData(), 0, in.getLength()).trim())== null) {
            // break;
            // }
            // System.out.print(word);
            // }
            // Server s = new Server(sb.toString());
            // s.start();
            // // Socket socket = new Socket();
            // // socket.

            // con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // con.setRequestProperty("Content-Length", "admin=1111&privileged=0000".getBytes().length+"");
            // con.setDoOutput(true);
            // con.setDoInput(true);
            // con.setRequestMethod("POST");
            // OutputStream out = con.getOutputStream();
            // out.write("admin=1111&privileged=0000".getBytes());
            // out.flush();
            // int code = con.getResponseCode();
            // System.out.println(con.getResponseMessage());
            // System.out.println("code " + code);
            // InputStream is = con.getInputStream();
            // byte[] data = new byte[1024];
            // is.read(data);
            // System.out.println(con.getContentLength());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

class Server extends Thread {
    private ServerSocket serverSocket;
    private DatagramSocket socket = null;
    String path;
    public Server() {

    }
    public Server(String path) {
        this.path = path;
    }

    // private OutputStream sendResponseHeaders(String message, long len) throws IOException {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("sendResponseHeaders !!");
    // }
    // StringBuffer response = new StringBuffer();
    // response.append("HTTP/1.1 ");
    // response.append(200).append(" ").append(message).append("\r\n");
    // response.append("Content-Type: text/plain").append("\r\n");
    // response.append("Content-Length: ").append(len).append("\r\n\r\n");
    //
    // OutputStream out = socket.getOutputStream();
    // out.write(response.toString().getBytes());
    // // out.flush();
    // if (Log.DEBUG_ON) {
    // Log.printDebug(response.toString());
    // }
    // return out;
    // }

    public static byte[] hexStringToByteArray(String input) {
        System.out.println("hexStringToByteArray " + input.length());
        if (input.length() % 2 != 0) {
            System.out.println("hexStringToByteArray return null : " + input.length());
            return null;
        }
        byte[] result = new byte[input.length() / 2];
        for (int i = 0; i < input.length(); i += 2) {
            result[i / 2] = (byte) Integer.parseInt(input.substring(i, i + 2), 16);
        }
        return result;
    }

    // private String readRequestPath() throws IOException {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("readRequestPath");
    // }
    //
    // BufferedInputStream bi = new BufferedInputStream(socket.getInputStream());
    //
    // int size = bi.available();
    // System.out.println("size " + size);
    // if (size == 0) {
    // size = 1024;
    // }
    // byte[] tempArray = new byte[size];
    // bi.read(tempArray);
    // String[] s = TextUtil.tokenize(new String(tempArray).trim(), "\r\n");
    // if (Log.DEBUG_ON) {
    // for (int i = 0; i < s.length; i++) {
    // Log.printDebug("input s " + s[i]);
    // }
    // }
    // StringBuffer firstLine = new StringBuffer(s[0]);
    // if (firstLine.toString().startsWith("GET")) {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("readRequestPath GET");
    // }
    // int index = firstLine.indexOf(" ");
    // return firstLine.substring(index + 1, firstLine.indexOf("HTTP") - 1);
    // } else if (firstLine.toString().startsWith("POST")) {
    //
    // int index = firstLine.indexOf(" ");
    // StringBuffer tempPath = null;
    // // body including full url
    // if (Log.DEBUG_ON) {
    // Log.printDebug("readRequestPath s[s.length - 1] " + s[s.length - 1]);
    // }
    // tempPath = new StringBuffer(firstLine.substring(index + 1, firstLine.indexOf("HTTP") - 1));
    // tempPath.append("?");
    // tempPath.append(s[s.length - 1]);
    // if (Log.DEBUG_ON) {
    // Log.printDebug("readRequestPath POST " + tempPath.toString());
    // }
    // return tempPath.toString();
    // }
    // return null;
    // }
    public void run() {
        // try {
        // serverSocket = new ServerSocket(1234);
        // while (true) {
        // // socket = serverSocket.accept();
        // // System.out.println(socket.getInetAddress().getHostAddress());
        // // InputStream in = socket.getInputStream();
        // // System.out.println(readRequestPath()+":::");
        // // byte[] bb = hexStringToByteArray(path);
        // // System.out.println(new String(bb));
        // // OutputStream out = sendResponseHeaders("OK", bb.length);
        // //
        // // out.write(bb);
        // // System.out.println("==="+bb.length);
        // // out.flush();
        // }
        // } catch (Exception e) {
        //
        // }
        byte[] buffer = new byte[2048];
        try {
            DatagramPacket in = null, out = null;
            InetAddress inet = null;
            String ip = null;
            System.out.println("UDP server run start : ");
            socket = new DatagramSocket(9275);
            while (true) {
                in = new DatagramPacket(buffer, 1024);
                socket.receive(in);
                inet = in.getAddress();
                ip = inet.getHostAddress();
                System.out.println("HostAddress : " + inet.getHostAddress() + " " + in.getPort());

                byte[] buf = in.getData();
                byte[] actualBuf = new byte[in.getLength()];
                System.out.println("in.getLength() " + in.getLength());
                System.arraycopy(buf, 0, actualBuf, 0, actualBuf.length);
                String data = new String(actualBuf);
                System.out.println("msg " + data);
                out = new DatagramPacket(data.getBytes(),data.getBytes().length, inet,  in.getPort());
                socket.send(out);
                try {
                    Thread.sleep(Constants.MS_PER_SECOND);
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        } catch (Exception e) {
        } finally {
            if (socket != null) {
                socket.close();
                socket = null;
            }
        }
    }
}

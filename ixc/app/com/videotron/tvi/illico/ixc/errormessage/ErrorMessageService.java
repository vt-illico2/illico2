package com.videotron.tvi.illico.ixc.errormessage;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Hashtable;

/**
 * This class is represents the Error Message Service.
 * 
 * @author Sangjoon Kwon
 */
public interface ErrorMessageService extends Remote {
    /** The IXC name of error message application. */
    String IXC_NAME = "ErrorMessageService";

    /**
     * Shows error message pop-up about error code.
     * 
     * @param Error code.
     * @param Error message listener.
     */
    void showErrorMessage(String errorCode, ErrorMessageListener l) throws RemoteException;
    
    /**
     * Shows error message pop-up about error code using by dynamic tag.
     * 
     * @param Error code.
     * @param Error message listener.
     * @param Dynamic tag hashtable.
     */
    void showErrorMessage(String errorCode, ErrorMessageListener l, Hashtable dth) throws RemoteException;
    
    /**
     * Shows error message pop-up about error code using by ErrorMessage object.
     * 
     * @param Error code.
     * @param Error message.
     * @param Error message listener.
     */
    void showErrorMessage(String errorCode, ErrorMessage message, ErrorMessageListener l) throws RemoteException;

    /**
     * Hides error message pop-up.
     */
    void hideErrorMessage() throws RemoteException;
    
    /**
     * Gets error message about error code.
     * 
     * @param Error code.
     * @return Error message.
     */
    ErrorMessage getErrorMessage(String errorCode) throws RemoteException;
    
    /**
     * Shows a popup of common message.
     * @param commonCode not used. because currently has only incompatibility popup.
     * @throws RemoteException
     */
    void showCommonMessage(String commonCode) throws RemoteException;
    
    /**
     * Shows a popup of common message.
     * @param commonCode not used. because currently has only incompatibility popup.
     * @param l notify to close a popup.
     * @throws RemoteException
     */
    void showCommonMessage(String commonCode, ErrorMessageListener l) throws RemoteException;
    
    /**
     * Gets a common message about common code.
     * @param commonCode not used. because currently has only incompatibility popup.
     * @return CommonMessage has all texts.
     * @throws RemoteException
     */
    CommonMessage getCommonMessage(String commonCode) throws RemoteException;
}

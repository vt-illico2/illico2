package com.videotron.tvi.illico.ixc.errormessage;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class represents the common message
 * 
 * @author zestyman
 *
 */
public interface CommonMessage extends Remote {

	/**
	 * Returns a title according to current language.
	 * @return title 
	 * @throws RemoteException
	 */
	String getTitle() throws RemoteException;
	
	/**
	 * Returns a functional text according to current language.
	 * @return a functional text
	 * @throws RemoteException
	 */
	String getFunctionalText() throws RemoteException;
	/**
	 * Returns a short functional Text according to current language.
	 * @return a short functional Text
	 * @throws RemoteException
	 */
	String getShortFunctionalText() throws RemoteException;
	
	/**
	 * Returns a action button text according to current lanaguage.
	 * @return a action button text
	 * @throws RemoteException
	 */
	String getActionButtonText() throws RemoteException;
	
	/**
	 * Returns a titles
	 * @return a String array for titles. first is for French. Second is for english.
	 * @throws RemoteException
	 */
	String[] getTitles() throws RemoteException;
	
	/**
	 * Return a functional Texts.
	 * @return a String array for functional Texts. first is for French. Second is for English.
	 * @throws RemoteException
	 */
	String[] getFunctionalTexts() throws RemoteException;
	
	/**
	 * Returns a short functional Texts.
	 * @return a String array for functional texts. first is for French, second is for English.
	 * @throws RemoteException
	 */
	String[] getShortFunctionalTexts() throws RemoteException;
	
	/**
	 * Returs a action button texts.
	 * @return a String array for action button texts, first is for French, second is for English.
	 * @throws RemoteException
	 */
	String[] getActionButtonTexts() throws RemoteException;
}

package com.videotron.tvi.illico.ixc.errormessage;

import java.io.Serializable;

public class ErrorMessageButton implements Serializable {
    private static final long serialVersionUID = -8397879640764621857L;
    public static final String BUTTON_TYPE_APPLICATION = "O";
    public static final String BUTTON_TYPE_CHANNEL = "T";
    public static final String BUTTON_TYPE_CLOSE = "C";
    public static final String BUTTON_TYPE_REBOOT = "R";
    
    private String buttonNameEn;
    private String buttonNameFr;
    private String buttonType;
    private String targetAppName;
    private String[] targetAppParamList;
    private String targetCallLetter;
    
    public String getEnglishButtonName() {
        return buttonNameEn;
    }
    public void setEnglishButtonName(String buttonNameEn) {
        this.buttonNameEn = buttonNameEn;
    }
    public String getFrenchButtonName() {
        return buttonNameFr;
    }
    public void setFrenchButtonName(String buttonNameFr) {
        this.buttonNameFr = buttonNameFr;
    }
    public String getButtonType() {
        return buttonType;
    }
    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }
    public String getTargetApplicationName() {
        return targetAppName;
    }
    public void setTargetApplicationName(String appName) {
        this.targetAppName = appName;
    }
    public String[] getTargetApplicationParameterList() {
        return targetAppParamList;
    }
    public void setTargetApplicationParameterList(String[] targetAppParamList) {
        this.targetAppParamList = targetAppParamList;
    }
    public String getTargetCallLetter() {
        return targetCallLetter;
    }
    public void setTargetCallLetter(String callLetter) {
        this.targetCallLetter = callLetter;
    }
}

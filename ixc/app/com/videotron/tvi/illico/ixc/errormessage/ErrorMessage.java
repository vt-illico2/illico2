package com.videotron.tvi.illico.ixc.errormessage;

import java.io.Serializable;

/**
 * This class is represents the Error Message.
 * 
 * @author Sangjoon Kwon
 */
public class ErrorMessage implements Serializable {
    private static final long serialVersionUID = 7579208340321165023L;
    /** Error message type. - Alert */
    public static final int TYPE_ALERT = 1;
    /** Error message type. - Warning */
    public static final int TYPE_WARNING = 2;
    
    private int errMsgId;
    private int errMsgType;
    private String reqAppName;
    private String enTitle;
    private String frTitle;
    private String enContent;
    private String frContent;
    private String iconName;
    private ErrorMessageButton[] errMsgButtonList;
    
    public void setErrorMessageId(int errMsgId) {
        this.errMsgId = errMsgId;
    }
    public int getErrorMessageId() {
        return errMsgId;
    }

    public void setErrorMessageType(int errMsgType) {
        this.errMsgType = errMsgType;
    }
    public int getErrorMessageType() {
        return errMsgType;
    }
    
    public void setRequestApplicationName(String reqAppName) {
        this.reqAppName = reqAppName;
    }
    public String getRequestApplicationName() {
        return reqAppName;
    }
    
    public void setEnglishTitle(String enTitle) {
        this.enTitle = enTitle;
    }
    public String getEnglishTitle() {
        return enTitle;
    }
    
    public String getFrenchTitle() {
        return frTitle;
    }
    public void setFrenchTitle(String frTitle) {
        this.frTitle = frTitle;
    }
    
    public void setEnglishContent(String enMsg) {
        this.enContent = enMsg;
    }
    public String getEnglishContent() {
        return enContent;
    }
    
    public void setFrenchContent(String frMsg) {
        this.frContent = frMsg;
    }
    public String getFrenchContent() {
        return frContent;
    }
    
    public void setIconName(String iconName) { 
        this.iconName = iconName;
    }
    public String getIconName() {
        return iconName;
    }
    
    public void setErrorMessageButtonList(ErrorMessageButton[] errMsgButtonList) {
        this.errMsgButtonList = errMsgButtonList;
    }
    public ErrorMessageButton[] getErrorMessageButtonList() {
        return errMsgButtonList;
    }
}

package com.videotron.tvi.illico.ixc.errormessage;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ErrorMessageListener extends Remote {
    int BUTTON_TYPE_LAUNCH_APP = 0;
    int BUTTON_TYPE_SELECT_CHANNEL = 1;
    int BUTTON_TYPE_CLOSE = 2;
    int BUTTON_TYPE_EXIT = 3;
    
    void actionPerformed(int buttonType) throws RemoteException;
}

package com.videotron.tvi.illico.ixc.vbm;

import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 * @author swkim
 */
public interface VbmService extends Remote {
	/**
	 * The IXC name of VBM application.
	 */
	String IXC_NAME = "VBM";

    String VBM_SM_INTF = "VBM";

    String SEPARATOR = "///";

	/**
	 *
	 * add listener of appName
	 *
	 * @param appName - application name
	 * @param listener - a listener to be added
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	void addLogCommandChangeListener(String appName, LogCommandTriggerListener listener) throws RemoteException;

	/**
	 * remove listener of appName
	 *
	 * @param appName
	 * @throws RemoteException
	 */
	void removeLogCommandChangeListener(String appName) throws RemoteException;

	/**
	 * remove all listeners
	 */
	void removeLogCommandChangeListener() throws RemoteException;


	/**
	 *
	 * request for list of measurements for appName
	 *
	 * @param appName target application name to get measurements
	 *
	 * @return measurement list of target application
	 *
	 */
	long [] checkLogCommand(String appName) throws RemoteException;

}

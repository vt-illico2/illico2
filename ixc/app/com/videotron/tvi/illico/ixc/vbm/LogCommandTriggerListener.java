package com.videotron.tvi.illico.ixc.vbm;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * 
 * @author thorn
 *
 */
public interface LogCommandTriggerListener extends Remote {
	
	/**
	 * event invoked when logLevel data updated.
	 * it is only applied to action measurements
	 * 
	 * 
	 * @param Measurement to notify measurement an frequencies
	 * @param command - true to start logging, false to stop logging
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	void logCommandChanged(long measurementId, boolean command) throws RemoteException;
	
	
	/**
	 * event invoked when log of trigger measurement should be collected
	 * 
	 * @param measurementId trigger measurement
	 * 
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	void triggerLogGathering(long measurementId) throws RemoteException;
}

package com.videotron.tvi.illico.ixc.notification;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * NotificationService class is a interface to communication with Notification Center application.
 * @author pellos
 *
 */
public interface NotificationService extends Remote {

    /** The IXC name of Notification application. */
    String IXC_NAME = "Notification";
    
    /**
     * Start to Notification event.
     * @param o NotificationOption
     * @return messageID.
     * @throws RemoteException
     */
    String setNotify(NotificationOption o) throws RemoteException;
    
    /**
     * 
     * @param o NotificationOption
     * @param time millisecond.
     * @return
     * @throws RemoteException
     */
    String setNotifyAndTime(NotificationOption o, long time) throws RemoteException;
    
    /**
     * Stop the notification event.
     * @param messageID
     * @throws RemoteException
     */
    void removeNotify(String messageID) throws RemoteException;
    
    /**
     * Application is registered to notification Center.
     * Already Application is registered, massageID List is returned.   
     * @param appName Application name.
     * @param l NotificationListener
     * @return messageID list.
     * @throws RemoteException
     */
    String[] setRegisterNotificaiton(String appName, NotificationListener l) throws RemoteException;
    
    /**
     * set display priority.
     * @param id
     * 0 Displays on top of a currently displayed notification, closing the latter by default.
     * 1 Waits until the currently displayed notification is closed (including the detailed notification)  
     * @throws RemoteException
     */
    void setPriority(int id) throws RemoteException;
    
    /**
     * Gets the Notification event detail.
     * @param id messageID
     * @return NotificationOption.
     * @throws RemoteException
     */
    NotificationOption getNotificationDetail(String id) throws RemoteException;
    
    /**
     * Gets the default display time of Notificaiton.
     * @return
     * @throws RemoteException
     */
    long getDisplayTime() throws RemoteException;
}

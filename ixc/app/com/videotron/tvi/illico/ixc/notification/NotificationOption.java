/*
 *
 */
package com.videotron.tvi.illico.ixc.notification;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The Interface NotificationOption.
 */
public interface NotificationOption extends Remote {

    /** It is define that the message saved period. */
    /** The Constant SHORT_TERM_TYPE. */
    public static final int SHORT_TERM_TYPE = 0;
    /** The Constant LONG_TERM_TYPE. */
    public static final int LONG_TERM_TYPE = 1;
    /** The Constant PERMANENT_TYPE. */
    public static final int PERMANENT_TYPE = 2;

    /** categories. */
    /** The Constant All_Application_CATEGORIES. */
    public static final int All_Application_CATEGORIES = 1;
    /** The Constant EXCEPTION_VOD_PPV_GAMES_CATEGORIES. */
    public static final int NOT_PPV_CATEGORIES = 2;
//    /** The Constant ONLY_VIDEO_CATEGORIES. */
//    public static final int EXCEPTION_VOD_PPV_GAMES_CATEGORIES = 3;
//    /** The Constant ONLY_NOTIFICATION_CENTER_CATEGORIES. */
//    public static final int ONLY_NOTIFICATION_CENTER_CATEGORIES = 4;

    /** It is define that is a popup type. */
    /** The Constant PPV_POPUP. */
    public static final int PPV_REMINDER_POPUP = 0;
    /** The Constant STC_POPUP. */
    public static final int STC_POPUP = 1;
    /** The Constant REMINDER_POPUP. */
    public static final int PROGRAM_REMINDER_POPUP = 2;
    /** The Constant SLEEP_TIMER_REMAINER_POPUP. */
    public static final int SLEEP_TIMER_REMAINER_POPUP = 3;
    /** The Constant PARENTAL_CONTROLS_POPUP. */
    public static final int PARENTAL_CONTROLS_POPUP = 4;
    /** The Constant NOTIFICAION_APPNAME_POPUP. */
    public static final int NOTIFICAION_APPNAME_POPUP = 5;
    /** The Constant NOTIFICAION_TIMEBased_POPUP. */
    public static final int TIMEBASED_RESTRICTIONS_POPUP = 6;
    /** The Constant NOTIFICAION_CALLER_ID_POPUP. */
    public static final int CALLER_ID_POPUP = 7;
    /** The Constant PPV_ORDER_REMINDER_POPUP */
    public static final int PPV_ORDER_REMINDER_POPUP = 8;
    /** The constant Automatic Power Down Popup */
    public static final int APD_POPUP = 9;

    /** It is define that is acted by a user. */
    /** The Constant ACTION_APPLICATION. */
    public static final int ACTION_APPLICATION = 1;
    /** The Constant ACTION_TUNE_CHANNEL. */
    public static final int ACTION_TUNE_CHANNEL = 2;
    /** The Constant ACTION_SLEEP_TIMER. */
    public static final int ACTION_SLEEP_TIMER = 3;
    /** The Constant ACTION_LARGE_POPUP. */
    public static final int ACTION_LARGE_POPUP = 4;
    /** The Constant ACTION_LARGE_POPUP. */
    public static final int ACTION_PIN_POPUP = 5;
    /** The Constant ACTION_NONE. */
    public static final int ACTION_NONE = 0;

    /**
     * Gets the ApllicationName
     * @return
     * @throws RemoteException
     */
    String getApplicationName() throws RemoteException;

    /**
     * Gets the create date of Notification message.
     * @return
     * @throws RemoteException
     */
    long getCreateDate() throws RemoteException;

    /**
     * Whether or not.
     * @return
     * @throws RemoteException
     */
    boolean isViewed() throws RemoteException;

    /**
     * Gets display count. disable... .
     * @return count.
     * @throws RemoteException
     */
    int getDisplayCount() throws RemoteException;

    /**
     * Gets a message ID.
     * @return message.
     * @throws RemoteException the remote exception
     */
    String getMessageID() throws RemoteException;

    /**
     * Gets the categories.
     * {@link NotificationOption#All_Application_CATEGORIES},
     * {@link NotificationOption#NOT_PPV_CATEGORIES}
     * @return the categories
     * @throws RemoteException the remote exception
     */
    int getCategories() throws RemoteException;

    /**
     * Gets the display time.
     * @return the display time.
     * @throws RemoteException the remote exception
     */
    long getDisplayTime() throws RemoteException;

    /**
     * Gets the reminder viewing Sum.
     * @return the reminder time. count.
     * @throws RemoteException the remote exception
     */
    int getRemainderTime() throws RemoteException;

    /**
     * Gets the reminder interval time.
     * @return the reminder delay. Value is a milliseconds.
     * @throws RemoteException the remote exception
     */
    long getRemainderDelay() throws RemoteException;

    /**
     * Is the countDwon.
     * @return
     * @throws RemoteException
     */
    boolean isCountDown() throws RemoteException;

    /**
     * Gets the message.
     *
     * @return the message
     * @throws RemoteException the remote exception
     */
    String getMessage() throws RemoteException;

    /**
     * Gets the sub_message.
     *
     * @return the message
     * @throws RemoteException the remote exception
     */
    String getSubMessage() throws RemoteException;


    /**
     * Gets the notification popup type.
     * {@link NotificationOption#PPV_REMINDER_POPUP},
     * {@link NotificationOption#STC_POPUP},
     * {@link NotificationOption#PROGRAM_REMINDER_POPUP},
     * {@link NotificationOption#SLEEP_TIMER_REMAINER_POPUP},
     * {@link NotificationOption#PARENTAL_CONTROLS_POPUP},
     * {@link NotificationOption#NOTIFICAION_APPNAME_POPUP},
     * {@link NotificationOption#TIMEBASED_RESTRICTIONS_POPUP},
     * {@link NotificationOption#CALLER_ID_POPUP}
     * @return the notification status
     * @throws RemoteException the remote exception
     */
    int getNotificationPopupType() throws RemoteException;

    /**
     * Gets the notification action.
     * {@link NotificationOption#ACTION_NONE},
     * {@link NotificationOption#ACTION_APPLICATION},
     * {@link NotificationOption#ACTION_SLEEP_TIMER},
     * {@link NotificationOption#ACTION_LARGE_POPUP},
     * {@link NotificationOption#ACTION_PIN_POPUP},
     * {@link NotificationOption#ACTION_TUNE_CHANNEL}
     * @return the notification action
     * @throws RemoteException the remote exception
     */
    int getNotificationAction() throws RemoteException;

    /**
     * Gets the notification action data.
     * [0] is application name or channel name,
     * [1] is data.
     * The parameters of action.
     * @return the notification action data
     * @throws RemoteException the remote exception
     */
    String[] getNotificationActionData() throws RemoteException;

    /**
     * Gets Large Popup Message.
     * @return message.
     * @throws RemoteException
     */
    String getLargePopupMessage() throws RemoteException;

    /**
     * Gets Large Popup sub Message.
     * @return message.
     * @throws RemoteException
     */
    String getLargePopupSubMessage() throws RemoteException;

    /**
     * Gets large button Info.
     * [0][0] is a button Name.
     * [0][1] is ActionType.
     * [0][2] is application name or channel name,
     * [0][n] is data.
     * @return
     * @throws RemoteException
     */
    String[][] getButtonNameAction() throws RemoteException;


}

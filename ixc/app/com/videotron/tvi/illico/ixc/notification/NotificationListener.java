package com.videotron.tvi.illico.ixc.notification;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NotificationListener extends Remote {
    /**
     * Event is started. 
     * @param id messageID.
     * @throws RemoteException
     */
    void action(String id) throws RemoteException;
    
    /**
     * Event is canceled.
     * @param id
     * @throws RemoteException
     */
    void cancel(String id) throws RemoteException;
    
    /**
     * Currently running application is to run the next job. 
     * @param platform is the Application name.
     * @param action is the data.
     * @throws RemoteException
     */
    void directShowRequested(String platform, String[] action) throws RemoteException;
}

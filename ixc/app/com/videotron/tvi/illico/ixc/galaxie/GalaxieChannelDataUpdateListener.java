/**
 * @(#)GalaxieChannelDataUpdateListener.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.galaxie;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class apply a interface to receive a notification that the data of Galaxie channel is updated.
 * @author Woojung Kim
 * @version 1.1
 */
public interface GalaxieChannelDataUpdateListener extends Remote {

    /**
     * Notify a updated data for Galaxie channel.
     * @param data To include a information 
     * @throws RemoteException Remote Exception.
     */
    void notifyGalaxieChannelDataUpdated(GalaxieChannelData data) throws RemoteException;
    
    /**
     * Notify a updated cover data for Galaxie channel.
     * @param img byte array for image.
     * @throws RemoteException Remote Exception.
     */
    void notifyCoverImageUpdated(byte[] img) throws RemoteException;
}

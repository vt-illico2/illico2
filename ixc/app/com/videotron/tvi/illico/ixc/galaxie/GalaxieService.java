/**
 * @(#)GalaxieService.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.galaxie;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class provides Galaxie functionality for other applications through IXC.
 * @author Woojung Kim
 * @version 1.1
 */
public interface GalaxieService extends Remote {
    /** Indicates the IXC name. */
    String IXC_NAME = "GalaxieService";

    /**
     * Get a Galaxie data.
     * @param chNo a number of galaxie channel which want to know.
     * @return {@link GalaxieChannelData}
     * @throws RemoteException
     */
    GalaxieChannelData getGalaxieData(int chNo) throws RemoteException;

    /**
     * Add a listener to update a data of Galaxie channel.
     * @param l listener to receive a updated data.
     * @throws RemoteException Remote Exception.
     */
    void addGalaxieChannelDataUpdatedListener(GalaxieChannelDataUpdateListener l) throws RemoteException;

    /**
     * Remove a listener.
     * @throws RemoteException Remote Exception.
     */
    void removeGalaxieChannelDataUpdatedListener() throws RemoteException;
    
    
}

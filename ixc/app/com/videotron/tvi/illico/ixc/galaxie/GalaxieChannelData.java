/**
 * @(#)GalaxieChannelData.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.galaxie;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class provides a information for Galaxie channel.
 * @author Woojung Kim
 * @version 1.1
 */
public interface GalaxieChannelData extends Remote {

    /**
     * Get a title of current song.
     * @return title of current song or null if can't find.
     */
    String getCurrentTitle() throws RemoteException;

    /**
     * Get a artist name of current song.
     * @return artist name of current song or null if can't find.
     */
    String getCurrentArtist() throws RemoteException;

    /**
     * Get a album name of current song.
     * @return album name of current song or null if can't find.
     */
    String getCurrentAlbum() throws RemoteException;

    /**
     * Get a title of next song.
     * @return title of next song or null if can't find.
     */
    String getNextTitle() throws RemoteException;

    /**
     * Get a artist name of next song.
     * @return artist name of next song or null if can't find.
     */
    String getNextArtist() throws RemoteException;

    /**
     * Get a album name of next song.
     * @return album name of next song or null if can't find.
     */
    String getNextAlbum() throws RemoteException;

    /**
     * Get a url to get a cover image for current song.
     * @return url or null if can't find.
     */
    String getCoverUrl() throws RemoteException;

    /**
     * Get a IP array of Proxy.
     * @return IP array of Proxy.
     */
    String[] getProxyIPs() throws RemoteException;

    /**
     * Get a port array of Proxy.
     * @return Port array of Proxy.
     */
    int[] getProxyPorts() throws RemoteException;
    
    /**
     * Get a default cover.
     * @return byte array for image.
     * @throws RemoteException
     */
    byte[] getDefaultCover() throws RemoteException;
}

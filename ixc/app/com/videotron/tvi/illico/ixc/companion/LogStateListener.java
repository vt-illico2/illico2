package com.videotron.tvi.illico.ixc.companion;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Hashtable;

/**
 * Applications can send logs to companion agent.
 *
 * @version $Revision: 1.4 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface LogStateListener extends Remote {

    String KEY_STATE = "state";

    String KEY_CONTENT_ID = "content_id";
    String KEY_LENGTH = "length";
    String KEY_TITLE = "title";
    String KEY_LANGUAGE = "language";
    String KEY_DEFINITION = "definition";

    String KEY_REC_LIST_VERSION = "rec_list_version";
    String KEY_SCH_LIST_VERSION = "sch_list_version";
    String KEY_UDN = "udn";

    String KEY_CHANNEL_ID = "channel_id";
    String KEY_PROGRAM_ID = "program_id";
    String KEY_START_TIME = "start_time";
    String KEY_END_TIME = "end_time";
    String KEY_TIMESHIFT = "timeshift";
    String KEY_RECORDING = "recording";

    String LOG_STATE_EPG = "epg";
    String LOG_STATE_PVR = "pvr";
    String LOG_STATE_VOD = "vod";
    String LOG_STATE_STB = "stb";
    String LOG_STATE_KEYS = "keys";


    /**
     * Invoked when the logs are sent from application.
     * @param logs logs from application.
     */
    void logStateChanged(Hashtable logs) throws RemoteException;

    /**
     * Invoked when content is paused or content's rate changed from paused state.
     * @param paused truen when the content is paused.
     */
    void playbackPaused(boolean paused) throws RemoteException;

    /**
     * Invoked when content is stopped and goes to "no_video" state.
     */
    void playbackStopped() throws RemoteException;

}

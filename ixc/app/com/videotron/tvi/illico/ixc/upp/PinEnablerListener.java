/**
 * @(#)PinEnablerListener.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The listener interface for receiving a result of PinEnablerUI.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public interface PinEnablerListener extends Remote {

    /**
     * Receive a response and detail from PinEnablerUI. In case of the only error, It receive a detail.
     * 
     * @param response
     *            The one of {@link PreferenceService#RESPONSE_SUCCESS}, {@link PreferenceService#RESPONSE_FAIL},
     *            {@link PreferenceService#RESPONSE_ERROR}, {@link PreferenceService#RESPONSE_CANCEL}
     * @param detail
     *            The detail message of error.
     * @throws RemoteException
     *             The remote exception
     */
    void receivePinEnablerResult(int response, String detail) throws RemoteException;
}

/**
 * @(#)RightFlterListener.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The RightFilterListener class provides a interface to receive a response to check that has a rights or filters.
 * @author Woojung Kim
 * @version 1.1
 */
public interface RightFilterListener extends Remote {
    /**
     * Invoked when UPP application has been finished to check Rights and Filters.
     * 
     * @param response
     *            The one of {@link PreferenceService#RESPONSE_SUCCESS}, {@link PreferenceService#RESPONSE_FAIL},
     *            {@link PreferenceService#RESPONSE_ERROR}, {@link PreferenceService#RESPONSE_CANCEL}.
     * @throws RemoteException
     *             The remote exception
     */
    void receiveCheckRightFilter(int response) throws RemoteException;
    
    /**
     * Return a explain string to use in PinEnablerUI. if anonymous don't have a access right or filter, UPP will show a
     * PinEnablerUI for User. a inputed PIN via PinEnablerUI will be use to decide whether User has a Right or Filter. 
     * @return The explain string for PinEnablerUI.
     * @throws RemoteException The remote exception
     */
    String[] getPinEnablerExplain() throws RemoteException;
}

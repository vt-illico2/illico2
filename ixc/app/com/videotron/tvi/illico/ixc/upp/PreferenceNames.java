/**
 * @(#)PreferenceNames.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

/**
 * Define a Preference names for UPP.
 * @author Woojung Kim
 * @version 1.1
 */
public final class PreferenceNames {

    /** a constructor for Utils class. */
    private PreferenceNames() {
    }

    /** The name of language. */
    public static final String LANGUAGE = "language";
    /** The name of menu. */
    public static final String MENU = "menu";
    /** The name of Audio Feedback. */
    public static final String AUDIO_FEEDBACK = "audio.feedback";
    /** The name of Postal Code. */
    public static final String POSTAL_CODE = "postal.code";
    /** 
     * The name of Dashboard menu displayed by default. 
     * @Deprecated remove from R5 - TANK
     * */
    public static final String DASHBOARD_MENU_DISPLAY = "dashboard.menu.display";
    /** The name of menu displayed by default. */
    public static final String DISPLAY_MENU_AT_POWER_ON = "display.menu.at.power.on";
    /** The name of Setup wizard. */
    public static final String SETUP_WIZARD = "setup.wizard";

    // Program Guide
    /** The name of Power On Channel. */
    public static final String POWER_ON_CHANNEL = "power.on.channel";
    /** The name of Favourites CHANNEL. */
    public static final String FAVOURITE_CHANNEL = "favourite.channel";
    /** The name of Favourites Channel Filter. */
    public static final String FAVOURITE_CHANNEL_FILTER = "favourite.channel.filter";
    /** The name of Channel Guide Format. */
    public static final String GRID_FORMAT = "grid.format";
    /** The name of PIP size. */
    public static final String PIP_SIZE = "pip.size";
    /** The name of PIP Favourite Layout and Position. */
    public static final String PIP_POSITION = "pip.position";
    /** The name of Automatically show description in mini-guide. */
    public static final String AUTO_SHOW_DESCIRIPTION = "auto.show.description";
    // R7
    public static final String SD_HD_CHANNELS_GROUPING = "sd.hd.channels.grouping";

    // Caller ID
    /** The name of Caller ID Display. */
    public static final String CALLER_ID_DISPLAY = "caller.id.display";
    /** The name of Caller ID Notification Duration. */
    public static final String CALLER_ID_NOTIFICATION_DURATION = "caller.id.notification.duration";
    /** The name of Caller ID Phone Line Selection. */
    public static final String CALLER_ID_PHONE_LINE_SELECTION = "caller.id.phone.line.selection";
    /** The name of Caller ID Phone Line List. */
    public static final String CALLER_ID_PHONE_LINE_LIST = "caller.id.phone.line.list";

    // Shortcut
    /**
     * The name of shortcut menu display. The value is {@link Definitions#OPTION_VALUE_ENABLE} or
     * {@link Definitions#OPTION_VALUE_DISABLE} .
     */
    public static final String SHORTCUT_MENU_DISPLAY = "shortcut.menu.display";
    /**
     * The name of shortcut menu list. The value is a shorcut data list that is separated by
     * {@link PreferenceService#PREFERENCE_DELIMETER}.
     */
    public static final String SHORTCUT_MENU_LIST = "shortcut.menu.list";

    // PVR
    /**
     * The name of Record buffer. The value is a one of {@link Definitions#RECORD_BUFFER_2MIN},
     * {@link Definitions#RECORD_BUFFER_5MIN}, {@value Definitions#RECORD_BUFFER_10MIN},
     * {@link Definitions#RECORD_BUFFER_15MIN}, {@link Definitions#RECORD_BUFFER_30MIN}.
     */
    public static final String RECORD_BUFFER = "record.buffer";
    /**
     * The name of Save Time Default. The value is a one of {@link Definitions#DEFAULT_SAVE_TIME_UNTIL_I_ERASE},
     * {@link Definitions#DEFAULT_SAVE_TIME_AFTER_7_DAYS}, {@link Definitions#DEFAULT_SAVE_TIME_AFTER_14_DAYS}.
     */
    public static final String DEFAULT_SAVE_TIME = "default.save.time";
    /**
     * Then name of Default save time (repeated recording)
     */
    public static final String DEFAULT_SAVE_TIME_REPEAT = "default.save.time.repeat";
    /** The name of Terminal name. */
    public static final String TERMINAL_NAME = "terminal.name";
    /** The name of Terminal other name. */
    public static final String TERMINAL_OTHER_NAME = "terminal.other.name";

    // VOD
    /** The name of Wishlist display. */
    public static final String WISHLIST_DISPLAY = "wishlist.display";
    /** The name of Preferences on format. */
    public static final String PREFERENCES_ON_FORMAT = "preferences.on.format";
    /** The name of Preference on language. */
    public static final String PREFERENCES_ON_LANGUAGE = "preferences.on.language";
    /** The name of VOD Wish List. */
    public static final String VOD_WISH_LIST = "vod.wish.list";
    /** The name of VOD Favourites. */
    public static final String VOD_FAVOURITES = "vod.favourites";

    // Widget
    /**
     * The name of Display of Widgets when Power ON. The value is {@link Definitions#OPTION_VALUE_YES} or
     * {@link Definitions#OPTION_VALUE_NO}.
     */
    public static final String WIDGETS_DISPLAY = "widgets.display";
    /** The name of Widgets List. */
    public static final String WIDGETS_LIST = "widgets.list";

    // Equipment setup
    /** The name of STB Sleep time. */
    public static final String STB_SLEEP_TIME = "stb.sleep.time";
    /** The name of STB Wake up time. */
    public static final String STB_WAKE_UP_TIME = "stb.wake.up.time";

    /** The name of TV picture format. */
    public static final String VIDEO_TV_PICTURE_FORMAT = "tv.picture.format";
    /** The name of zoom mode. */
    public static final String VIDEO_ZOOM_MODE = "zoom.mode";
    /** The name of TV Picture Stretch. */
    public static final String VIDEO_TV_PICTURE_STRETCH = "tv.picture.stretch";
    /** The name of 1080P support on VOD. */
    public static final String VOD_1080P_SUPPORT = "vod.1080p.support";
    /** The name of Audio volume control. */
    public static final String AUDIO_VOLUME_CONTROL = "audio.volume.control";
    /** The name of Remote Control Type. */
    public static final String REMOTE_CONTROL_TYPE = "remote.control.type";
    /** The name of Display Power Off. */
    public static final String DISPLAY_POWER_OFF = "display.power.off";
    /** The name of Terminal Power Outlet. */
    public static final String TERMINAL_POWER_OUTLET = "terminal.power.outlet";
    /** The name of Audio output 
     *  R7.4 audio output VDTRMASTER-5277
     **/
    public static final String AUDIO_OUTPUT = "audio.output";
    
    // Accessibility
    /** The name of SAP language audio. */
    public static final String SAP_LANGUAGE_AUDIO = "sap.language.audio";
    /** The name of cc display. */
    public static final String CC_DISPLAY = "cc.display";
    /** The name of cc language. */
    public static final String CC_LANGUAGE = "cc.language";
    /** The name of cc analog source. */
    public static final String CC_ANALOG_SOURCE = "cc.analog.source";
    /** The name of cc digital source. */
    public static final String CC_DIGITAL_SOURCE = "cc.digital.source";
    /** The name of cc style. */
    public static final String CC_STYLE = "cc.style";
    /** The name of cc characters size. */
    public static final String CC_CHARACTERS_SIZE = "cc.characters.size";
    /** The name of cc charaters shadings. */
    public static final String CC_CHARACTERS_SHADINGS = "cc.characters.shadings";
    /** The name of cc charaters color. */
    public static final String CC_CHARACTERS_COLOR = "cc.characters.color";
    /** The name of cc background shadings. */
    public static final String CC_BACKGROUND_SHADINGS = "cc.background.shadings";
    /** The name of cc background color. */
    public static final String CC_BACKGROUND_COLOR = "cc.background.color";
    /** The name of Described audio display. */
    public static final String DESCRIBED_AUDIO_DISPLAY = "described.audio.display";
    
    // Automatic Power Down
    public static final String AUTOMATIC_POWER_DOWN = "automatic.power.down";
    public static final String AUTOMATIC_POWER_DOWN_TIME = "automatic.power.down.time";
    
    // 4K
    public static final String BYPASS_TERMINAL_CONVERSION = "bypass.terminal.conversion";
    public static final String AUTO_DETECTED_TV_PICTURE_FORMAT = "auto.detected.tv.picture.format";
}

/**
 * @(#)RightFilter.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

/**
 * The RightFilter class has a name of Access Rights and Filters.
 * @author Woojung Kim
 * @version 1.1
 */
public final class RightFilter {

    /** The name of Games for Application Access. */
    public static final String GAMES = "GAMES";

    /** The name of PC to TV. */
    public static final String PC_TO_TV = "PC_TO_TV";

    /** The name of STB Settings for Application Access. */
    public static final String STB_SETTINGS = "Profile";

    /** The name of Parental control for Application Filter. */
    public static final String PARENTAL_CONTROL = "parental.control";
    
    /** The name of Parental control for Application Filter. */
    public static final String PARENTAL_CONTROL_TIME = "parental.control.time";
    
    /** The name of suspend default time for Parental control. */
    public static final String SUSPEND_DEFAULT_TIME = "suspend.default.time";
    
    /** The name of channel restriction for Access Filter. */
    public static final String CHANNEL_RESTRICTION = "channel.restriction";

    /** The name of block periods and limits. to use for menu. */
    public static final String TIME_BASED_RESTRICTIONS = "time.based.restrictions";
    /** The name of block by ratings. */
    public static final String BLOCK_BY_RATINGS = "block.by.ratings";
    /** The name of block by ratings display. */
    public static final String BLOCK_BY_RATINGS_DISPLAY = "block.by.ratings.display";
    /** The name of PIN code unblocks channel. */
    public static final String PIN_CODE_UNBLOCKS_CHANNEL = "pin.code.unblocks.channel";

    /** The name of PIN code unblocks channel time. */
    public static final String PIN_CODE_UNBLOCKS_CHANNEL_TIME = "pin.code.unblocks.time";

    /** The name of hide adult content. */
    public static final String HIDE_ADULT_CONTENT = "hide.adult.content";

    /** The name of protect PVR recording. */
    public static final String PROTECT_PVR_RECORDINGS = "protect.pvr.recordings";

    /** The name of protect settings modification. */
    public static final String PROTECT_SETTINGS_MODIFICATIONS = "protect.settings.modifications";

    /** The name of ALLOW_PURCHASE. TO define for family user. */
    public static final String ALLOW_PURCHASE = "allow.purchase";

    public static final String ALLOW_BYPASS_BLOCKED_CHANNEL = "allow.bypass.blocked.channel";
    public static final String ALLOW_BYPASS_BLOCKED_TIME_PERIOD = "allow.bypass.blocked.time.period";
    public static final String ALLOW_ADULT_CATEGORY = "allow.adult.category";
}

/**
 * @(#)PreferenceListener.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The listener interface for receiving updated Preference and result of checking Rights.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public interface PreferenceListener extends Remote {

    /**
     * invoked when the value of the preference has changed.
     * 
     * @param preferenceName
     *            Updated Preference Name
     * @param value
     *            Updated value of Preference
     * @throws RemoteException
     *             The remote exception
     */
    void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException;
}

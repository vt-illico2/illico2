/**
 * @(#)RightFlterListener.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The RightFilterListener class provides a interface to receive a response to check that has a rights or filters.
 * @author Woojung Kim
 * @version 1.1
 */
public interface BlockTimeUpdateListener extends Remote {
    
    void updateBlockTimeData(BlockTime[] times) throws RemoteException;
}

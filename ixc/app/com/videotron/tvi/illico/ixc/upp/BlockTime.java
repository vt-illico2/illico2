/**
 * @(#)BlockTime.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

import java.io.Serializable;

/**
 * This class define a interface to know a information for Block time.
 * @author Woojung Kim
 * @version 1.1
 */
public class BlockTime implements Serializable, Cloneable {

    private String startTime;
    private String endTime;
    private String repeat;

    /**
     * Return a start time of Block time.
     * @return The start time that format is hhmm.
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Return a end time of Block time.
     * @return The end time that format is hhmm.
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * Return a repeat of Block time.
     * @return The repeat that format is SUN|MON|TUSE|WEN|THUS|FRI|SAT
     * @throws RemoteException
     */
    public String getRepeat() {
        return repeat;
    }

    public void setStartTime(String time) {
        startTime = time;
    }

    public void setEndTime(String time) {
        endTime = time;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }
}

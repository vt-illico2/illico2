/**
 * @(#)Definitions.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

/**
 * Defines a value and name of field for UPP.
 * @author Woojung Kim
 * @version 1.1
 */
public final class Definitions {

    /** a constructor for Util class. */
    private Definitions() {
    }

    /** Indicate a french language. */
    public static final String LANGUAGE_FRENCH = "Français";
    /** Indicate a english language. */
    public static final String LANGUAGE_ENGLISH = "English";
    /** Indicate a spanish language. */
    public static final String LANGUAGE_SPANISH = "Spanish";
    /** Indicate a Virtual Keyboard Standard type. */
    public static final String VIRTUAL_KEYBOARD_STANDARD = "Alphanumeric";
    /** Indicate a Virtual Keyboard MultiTap type. */
    public static final String VIRTUAL_KEYBOARD_MULTI_TAP = "Multi-tap";

    /** Indicate a value of Yes. */
    public static final String OPTION_VALUE_YES = "Yes";
    /** Indicate a value of No. */
    public static final String OPTION_VALUE_NO = "No";
    /** Indicate a value of On. */
    public static final String OPTION_VALUE_ON = "On";
    /** Indicate a value of Off. */
    public static final String OPTION_VALUE_OFF = "Off";

    public static final String OPTION_VALUE_ON_MUTE = "On when pressing on \"mute\"";
    /** Indicate a value of Enable. */
    public static final String OPTION_VALUE_ENABLE = "Enable";
    /** Indicate a value of Disable. */
    public static final String OPTION_VALUE_DISABLE = "Disable";

    public static final String OPTION_VALUE_SUSPENDED_FOR_4_HOURS = "Suspended for 4 hours";
    public static final String OPTION_VALUE_SUSPENDED_FOR_8_HOURS = "Suspended for 8 hours";

    /** Indication a value of Last Channel which will be use a Power On Channel. */
    public static final String LAST_CHANNEL = "Last Channel";

    public static final String UNTIL_CHANNEL_IS_CHANGED = "Until channel is changed";
    public static final String FOR_4_HOURS = "For 4 hours";
    public static final String FOR_8_HOURS = "For 8 hours";

    public static final String RATING_G = "G";
    public static final String RATING_8 = "8+";
    public static final String RATING_13 = "13+";
    public static final String RATING_16 = "16+";
    public static final String RATING_18 = "18+";

    public static final String GRID_FORMAT_NORMAL = "Normal(with TV feed)";
    public static final String GRID_FORMAT_FULL = "Full Grid";
    public static final String PIP_SIZE_OVERLAY = "Overlay";
    public static final String PIP_SIZE_SIDE_BY_SIDE = "Side-by-Side";
    public static final String PIP_POSITION_TOP_LEFT = "Top left";
    public static final String PIP_POSITION_TOP_RIGHT = "Top right";
    public static final String PIP_POSITION_BOTTOM_LEFT = "Bottom left";
    public static final String PIP_POSITION_BOTTOM_RIGHT = "Bottom right";

    /** Indicate a channel filter of all channels. */
    public static final String FAVOURITES_CHANNEL_FILTER_ALL_CHANNELS = "All channels";
    /** Indicate a channel filter of subscribed channels. */
    public static final String FAVOURITES_CHANNEL_FILTER_SUBSCRIBED_CHANNELS = "Subscribed channels";
    /** Indicate a channel filter of my favourites. */
    public static final String FAVOURITES_CHANNEL_FILTER_MY_FAVOURITES = "My favourites";
    /** Indicate a channel filter of entertainment. */
    public static final String FAVOURITES_CHANNEL_FILTER_ENTERTAINMENT = "Entertainment";
    /** Indicate a channel filter of life style. */
    public static final String FAVOURITES_CHANNEL_FILTER_LIFESTYLE = "Lifestyle";
    /** Indicate a channel filter of sports. */
    public static final String FAVOURITES_CHANNEL_FILTER_SPORTS = "Sports";
    /** Indicate a channel filter of news. */
    public static final String FAVOURITES_CHANNEL_FILTER_NEWS = "News";
    /** Indicate a channel filter of movies. */
    public static final String FAVOURITES_CHANNEL_FILTER_MOVIES = "Movies";
    /** Indicate a channel filter of documentaries. */
    public static final String FAVOURITES_CHANNEL_FILTER_DOCUMENTARIES = "Documentaries";
    /** Indicate a channel filter of family. */
    public static final String FAVOURITES_CHANNEL_FILTER_FAMILY = "Family";
    /** Indicate a channel filter of kids. */
    public static final String FAVOURITES_CHANNEL_FILTER_KIDS = "Kids";
    /** Indicate a channel filter of music. */
    public static final String FAVOURITES_CHANNEL_FILTER_MUSIC = "Music";
    /** Indicate a channel filter of french channel. */
    public static final String FAVOURITES_CHANNEL_FILTER_FRENCH_CHANNEL = "French channel";
    /** Indicate a channel filter of english channel. */
    public static final String FAVOURITES_CHANNEL_FILTER_ENGLISH_CHANNEL = "English channel";
    /** Indicate a channel filter of HD channels. */
    public static final String FAVOURITES_CHANNEL_FILTER_HD_CHANNELS = "HD channels";
    /** Indicate a channel filter of PPV chanels. */
    public static final String FAVOURITES_CHANNEL_FILTER_PPV_CHANNELS = "PPV channels";
    /** Indicate a channel filter of Radio channels. */
    public static final String FAVOURITES_CHANNEL_FILTER_GALAXIE_RADIO_CHANNELS = "Galaxie radio channels";
    /** Indicate a channel filter of sports max channels. */
    public static final String FAVOURITES_CHANNEL_FILTER_SPORTSMAX_CHANNELS = "SportsMax channels";
    /** Indicate a channel filter of technitian. */
    public static final String FAVOURITES_CHANNEL_FILTER_TECHNICIAN = "Technician";

    // PVR
    public static final String RECORD_BUFFER_0MIN = "0 min";
    public static final String RECORD_BUFFER_1MIN = "1 min";
    public static final String RECORD_BUFFER_2MIN = "2 min";
    public static final String RECORD_BUFFER_5MIN = "5 min";
    public static final String RECORD_BUFFER_10MIN = "10 min";
    public static final String RECORD_BUFFER_15MIN = "15 min";


    public static final String DEFAULT_SAVE_TIME_UNTIL_I_ERASE = "Until I erase";
    public static final String DEFAULT_SAVE_TIME_AFTER_7_DAYS = "After 7 days";
    public static final String DEFAULT_SAVE_TIME_AFTER_14_DAYS = "After 14 days";

    public static final String DEFAULT_SAVE_TIME_KEEP_ALL = "Keep All";
    public static final String DEFAULT_SAVE_TIME_LAST_3 = "Last 3 occurences";
    public static final String DEFAULT_SAVE_TIME_LAST_5 = "Last 5 occurences";
    
    public static final String PVR_MAIN = "PVR Main";
    public static final String PVR_BEDROOM = "PVR Bedroom";
    public static final String PVR_LOUNGE = "PVR Lounge";
    public static final String PVR_BASEMENT = "PVR Basement";
    public static final String PVR_KITCHEN = "PVR Kitchen";
    public static final String PVR_OTHER_NAME = "Other name";

    // VOD
    public static final String WISHLIST_DISPLAY_ALPHABETICAL = "Alphabetical";
    public static final String WISHLIST_DISPLAY_NEWEST_SELECTED_ON_TOP = "Newest selected on top";
    public static final String PREFERENCES_ON_FORMAT_HD = "HD";
    public static final String PREFERENCES_ON_FORMAT_STANDARD = "Standard";
    public static final String PREFERENCES_ON_LANGUAGE_FRENCH = "Français";
    public static final String PREFERENCES_ON_LANGUAGE_ENGLISH = "English";

    // Equipment setup
    public static final String VIDEO_TV_PICTURE_FORMAT_480I = "480i";
    public static final String VIDEO_TV_PICTURE_FORMAT_480P = "480p";
    public static final String VIDEO_TV_PICTURE_FORMAT_720P = "720p";
    public static final String VIDEO_TV_PICTURE_FORMAT_1080I = "1080i";
    public static final String VIDEO_TV_PICTURE_FORMAT_1080P = "1080p";
    public static final String VIDEO_TV_PICTURE_STRETCH_DO_NOT_STRETCH = "Do not stretch";
    public static final String VIDEO_TV_PICTURE_STRETCH_STRETCH_SD_PROGRAM = "Stretch SD programs";
    public static final String VIDEO_ZOOM_MODE_NORMAL = "Normal";
    public static final String VIDEO_ZOOM_MODE_WIDE = "Wide";
    public static final String VIDEO_ZOOM_MODE_ZOOM = "Zoom";
    public static final String AUDIO_VOLUME_CONTROL_TV = "Control volume on TV";
    public static final String AUDIO_VOLUME_CONTROL_STB = "Control volume on STB";
    public static final String REMOTE_CONTROL_TYPE_A = "Type A";
    public static final String REMOTE_CONTROL_TYPE_B = "Type B";
    public static final String DISPLAY_POWER_OFF_TIME = "Time";
    public static final String DISPLAY_POWER_OFF_BLANK = "Blank";

    // CC
    public static final String CC_SOURCE_ANALOG_CC1 = "Std CC1";
    public static final String CC_SOURCE_ANALOG_CC2 = "Std CC2";
    public static final String CC_SOURCE_ANALOG_CC3 = "Std CC3";
    public static final String CC_SOURCE_ANALOG_CC4 = "Std CC4";
    public static final String CC_SOURCE_ANALOG_T1 = "Std T1";
    public static final String CC_SOURCE_ANALOG_T2 = "Std T2";
    public static final String CC_SOURCE_ANALOG_T3 = "Std T3";
    public static final String CC_SOURCE_ANALOG_T4 = "Std T4";
    public static final String CC_SOURCE_DIGITAL_1 = "Digital 1";
    public static final String CC_SOURCE_DIGITAL_2 = "Digital 2";
    public static final String CC_SOURCE_DIGITAL_3 = "Digital 3";
    public static final String CC_SOURCE_DIGITAL_4 = "Digital 4";
    public static final String CC_SOURCE_DIGITAL_5 = "Digital 5";
    public static final String CC_SOURCE_DIGITAL_6 = "Digital 6";

    /** The value that can customize a attribute. */
    public static final String CC_STYLE_SET_BY_VIEWER = "Set by viewer";
    /** The value that set automatically a attribute by default. */
    public static final String CC_STYLE_SET_BY_PROGRAM = "Set by program";
    public static final String CC_CHARACTER_SIZE_STANDARD = "Standard text";
    public static final String CC_CHARACTER_SIZE_SMALL = "Small text";
    public static final String CC_CHARACTER_SIZE_LARGE = "Large text";
    public static final String CC_COLOR_DEFAULT = "Default";
    public static final String CC_COLOR_YELLOW = "Yellow";
    public static final String CC_COLOR_BLACK = "Black";
    public static final String CC_COLOR_WHITE = "White";
    public static final String CC_COLOR_RED = "Red";
    public static final String CC_COLOR_GREEN = "Green";
    public static final String CC_COLOR_BLUE = "Blue";
    public static final String CC_COLOR_CYAN = "Cyan";
    public static final String CC_COLOR_MAGENTA = "Magenta";
    public static final String CC_SHADINGS_SLIGHT = "Slight";
    public static final String CC_SHADINGS_TRANSPARENT = "Transparent";
    public static final String CC_SHADINGS_OPAQUE = "Opaque";

    // Caller ID
    public static final String CALLER_ID_DURATION_10SEC = "10 seconds";
    public static final String CALLER_ID_DURATION_15SEC = "15 seconds";
    public static final String CALLER_ID_DURATION_30SEC = "30 seconds";
    public static final String CALLER_ID_DURATION_60SEC = "60 seconds";

    public static final String CALLER_ID_LINE_1 = "Line 1";
    public static final String CALLER_ID_LINE_2 = "Line 2";
    public static final String CALLER_ID_LINE_3 = "Line 3";
    public static final String CALLER_ID_LINE_4 = "Line 4";
    public static final String CALLER_ID_LINE_5 = "Line 5";

    // Time
    public static final String TIME_AM = "am";
    public static final String TIME_PM = "pm";
    
    // Automatic power down
    public static final String ONE_HOUR = "1 hour";
    public static final String TWO_HOURS = "2 hours";
    public static final String THREE_HOURS = "3 hours";
    public static final String FOUR_HOURS = "4 hours";
    
    // 4K
    public static final String VIDEO_TV_PICTURE_FORMAT_AUTO = "Auto";
    public static final String VIDEO_TV_PICTURE_FORMAT_UHD = "UHD/4K";
    
    // R7
    public static final String SD_HD_CHANNEL_GROUPED = "grouped";
    public static final String SD_HD_CHANNEL_UNGROUPED = "ungrouped";
    
    //R7.4 sykim VDTRMASTER-5277
    public static final String AUDIO_OUTPUT_DEFAULT = "Default";
    public static final String AUDIO_OUTPUT_PCM = "PCM";
    public static final String AUDIO_OUTPUT_DOLBY = "Dolby Digital";
}

/**
 * @(#)PreferenceService.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.upp;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * <code>PreferenceService</code> provides a interface to communicate between UPP and other Application.
 * @author Woojung Kim
 * @version 1.1
 */
public interface PreferenceService extends Remote {

    /** The Constant RMI_APP_NAME. */
    String IXC_NAME = "PreferenceService";

    /** The Constant PREFERENCE_DELIMETER. */
    String PREFERENCE_DELIMETER = "|";

    /** Indicate that a response is a fail. */
    int RESPONSE_FAIL = 0x00;
    /** Indicate that a response is a success. */
    int RESPONSE_SUCCESS = 0x01;
    /** Indicate that a response is a cancel. */
    int RESPONSE_CANCEL = 0x02;
    /** Indicate that a response is a error. */
    int RESPONSE_ERROR = 0x03;

    /** Main menu use this field for parameter that want to come into Shortcut menu settings. */
    String SHORTCUT_MENU = "Shortcut menu";
    /** EPG use this field for parameter that want to come into Program guide settings. */
    String PROGRAM_GUIDE = "Program guide";
    /** VOD use this field for parameter that want to come into VOD settings. */
    String VOD = "VOD";
    /** Other application user this field to want to come into parental control. */
    String PARENTAL_CONTROLS = "Parental Controls";
    /** Caller ID user this field for parameter that wnat to come into Caller ID settings. */
    String CALLER_ID = "Caller ID";
    /** APD uses this field as parameter for going to Equipment Settings*/
    String EQUIPMENT = "Equipment";

    /** The key name of Hashtable for Footer images in SharedMemory. */
    String FOOTER_IMG = "Footer Img";
    /** The key name of button A image for Footer in Hashtable of SharedMemory. */
    String BTN_A = "btn_a";
    /** The key name of button B image for Footer in Hashtable of SharedMemory. */
    String BTN_B = "btn_b";
    /** The key name of button C image for Footer in Hashtable of SharedMemory. */
    String BTN_C = "btn_c";
    /** The key name of button D image for Footer in Hashtable of SharedMemory. */
    String BTN_D = "btn_d";
    /** The key name of button big B image for Footer in Hashtable of SaredMemory. */
    String BTN_B_BIG = "btn_b_big";
    /** The key name of button arrow image for Footer in Hashtable of SharedMemory. */
    String BTN_ARROW = "btn_arrow";
    /** The key name of button back image for Footer in Hashtable of SharedMemory. */
    String BTN_BACK = "btn_back";
    /** The key name of button exit image for Footer in Hashtable of SharedMemory. */
    String BTN_EXIT = "btn_exit";
    /** The key name of button guide image for Footer in Hashtable of SharedMemory. */
    String BTN_GUIDE = "btn_guide";
    /** The key name of button info image for Footer in Hashtable of SharedMemory. */
    String BTN_INFO = "btn_info";
    /** The key name of button list image for Footer in Hashtable of SharedMemory. */
    String BTN_LIST = "btn_list";
    /** The key name of button menu image for Footer in Hashtable of SharedMemory. */
    String BTN_MENU = "btn_menu";
    /** The key name of button OK image for Footer in Hashtable of SharedMemory. */
    String BTN_OK = "btn_ok";
    /** The key name of button page up / down image for Footer in Hashtable of SharedMemory. */
    String BTN_PAGE = "btn_page";
    /** The key name of button profile(user) image for Footer in Hashtable of SharedMemory. */
    String BTN_PRO = "btn_pro";
    /** The key name of button search image for Footer in Hashtable of SharedMemory. */
    String BTN_SEARCH = "btn_search";
    /** The key name of button skip image for Footer in Hashtable of SharedMemory. */
    String BTN_SKIP = "btn_skip";
    /** The key name of button widget image for Footer in Hashtable of SharedMemory. */
    String BTN_WIDGET = "btn_wid";
    /** the key name of button widget image for Footer in Hashtable of Sharedmemory. */
    String BTN_POUND = "btn_sharp";

    /**
     * Request to save the value for preference name.
     * @param preferenceName The preference name to save
     * @param value the value to saved
     * @return the result to save the value;
     * @throws RemoteException The remote exception
     */
    boolean setPreferenceValue(String preferenceName, String value) throws RemoteException;

    /**
     * Return a value of Preference.
     * @param preferenceName preference name to know.
     * @return current value of preference.
     * @throws RemoteException the remote exception
     */
    String getPreferenceValue(String preferenceName) throws RemoteException;
    
    /**
     * Adds the specified preference listener to receive notify of updated preference.
     * @param pListener the preference listener that receive notification for updated preference.
     * @param appId a Application ID
     * @param preferenceNames the preference names to want values
     * @param defaultValues the preference value for default if preference not existed.
     * @return the preference values
     * @throws RemoteException the remote exception
     */
    String[] addPreferenceListener(PreferenceListener pListener, String appId, String[] preferenceNames,
            String[] defaultValues) throws RemoteException;

    /**
     * Removes the specified preference listener.
     * @param pListener the preference listener
     * @throws RemoteException the remote exception
     */
    void removePreferenceListener(String appId, PreferenceListener pListener) throws RemoteException;

    /**
     * Requests to show PinEnablerUI.
     * @param listener The listener to receive a result of PinEnablerUI
     * @param explain The explain string to be used
     * @throws RemoteException the remote exception
     */
    void showPinEnabler(PinEnablerListener listener, String[] explain) throws RemoteException;

    /**
     * Requests to check that has a Rights and Filter or not.
     * @param l The listener to receive a response.
     * @param appName Application name to be check a access right.
     * @param filters the filter list
     * @param callLetter if {@link RightFilter#BLOCKED_CHANNELS} be used, UPP needs a current call letter to be used to
     *            compare.
     * @param rating if (@link {@link RightFilter#BLOCKED_RATING} be used, UPP needs a current rating to be used to
     *            compare.
     * @throws RemoteException the remote exception
     */
    void checkRightFilter(RightFilterListener l, String appName, String[] filters, String callLetter, String rating)
            throws RemoteException;

    /**
     * Requests to check that has a Right or not.
     * @param l The listener to receive a response.
     * @param appName Application name to be check a access right.
     * @throws RemoteException the remote exception
     */
    void checkRight(RightFilterListener l, String appName) throws RemoteException;

    /**
     * requests to show Settings Menu to UPP.
     * @param parameters The parameters to know a request from and a menu to be display.
     * @throws RemoteException the remote exception
     */
    void showSettingsMenu(String[] parameters) throws RemoteException;

    /**
     * requests to hide Settings Menu.
     * @throws RemoteException the remote exception
     */
    void hideSettingsMenu() throws RemoteException;

    /**
     * Requests to hide PinEnablerUI.
     * @throws RemoteException the remote exception
     */
    void hidePinEnabler() throws RemoteException;

    /**
     * Return a result to check whether exist a Admin PIN or not.
     * @return true if Admin PIN exist, false otherwise.
     * @throws RemoteException the remote exception
     */
    boolean isExistedAdminPIN() throws RemoteException;

    /**
     * Set a Admin PIN.
     * @param pin a PIN string to set for Admin PIN.
     * @throws RemoteException the remote exception
     */
    void setAdminPIN(String pin) throws RemoteException;

    /**
     * Return whether inputed PIN string equals Admin PIN or not.
     * @param pin 4-digit string which want to compare with Admin PIN.
     * @return true if equals Admin PIN, false otherwise.
     * @throws RemoteException The remote exception.
     */
    boolean equalsWithAdminPIN(String pin) throws RemoteException;

    /**
     * Return a result to check weawhetherther exist a Admin PIN or not.
     * @return true if Admin PIN exist, false otherwise.
     * @throws RemoteException the remote exception
     */
    boolean isExistedFamilyPIN() throws RemoteException;

    /**
     * Set a Family PIN.
     * @param pin a PIN String for Family PIN
     * @throws RemoteException The remote exception;
     */
    void setFamilyPIN(String pin) throws RemoteException;

    /**
     * Return whether inputed PIN string equals Family PIN or not.
     * @param pin 4-digit string witch want to compare with Family PIN.
     * @return true if equals Family PIN. false otherwise.
     * @throws RemoteException The remote exception
     */
    boolean equalsWithFamilyPIN(String pin) throws RemoteException;

    /**
     * add a listener to receive a event to occur when Block time is changed and return a current block times.
     * @param l listener to receive a event to occur when Block time is changed
     * @return a current block times.
     * @throws RemoteException The remote exception.
     */
    BlockTime[] addBlockTimeUpdateListener(BlockTimeUpdateListener l) throws RemoteException;

    /**
     * Set a postal code.
     * @param postalCode a string of postal code want to save.
     * @throws RemoteException The remote exception.
     */
    void setPostalCode(String postalCode) throws RemoteException;

    /**
     * Get a Postal code.
     * @return a postal code string to be saved.
     * @throws RemoteException The remote exception.
     */
    String getPostalCode() throws RemoteException;

    /**
     * Save a current zoom mode.
     * @see {@link #restoreZoomMode()}, {@link #setNormalZoomMode()}
     * @throws RemoteException
     */
    void saveZoomMode() throws RemoteException;
    
    /**
     * Set a Normal for Zoom mode.
     * @see {@link #restoreZoomMode()}
     * @throws RemoteException
     */
    void setNormalZoomMode() throws RemoteException;
    
    /**
     * Restore a last zoom mode which is before call {@link #setNormalZoomMode()};
     * @throws RemoteException
     */
    void restoreZoomMode() throws RemoteException;
    
    /**
     * Save a current TV Picture format.
     * @throws RemoteException
     */
    void saveTVPictureFormat() throws RemoteException;
    
    /**
     * Restore a last TV Picture format
     * @throws RemoteException
     */
    void restoreTVPictureFormat() throws RemoteException;
    
    /**
     * Set a 1080p for TV Picture Format
     * @throws RemoteException
     */
    void setFullHDTVPictureFormat() throws RemoteException;
    
    /**
     * add a callLetter into block channel list and include a sister channel of it.
     * @param callLetter be added into block channel list.
     * @throws RemoteException
     */
    void addBlockChannel(String callLetter) throws RemoteException;
    
    /**
     * remove a callLetter from block channel list and include a sister channel of it.
     * @param callLetter be removed from block channel list.
     * @throws RemoteException
     */
    void removeBlockChannel(String callLetter) throws RemoteException;
    
	/**
	 * Reset the timer of Automatic Power Down.
	 * @return true if showing Notification. false otherwise.
	 * @throws RemoteException The remote exception
	 */
	boolean resetAPDTimer() throws RemoteException;
	
	/**
	 * Reset the timer of Automatic Power Down 
	 * and send a keycode as a parameter
	 * @return true if Notification is shown and pressed D key
	 * @throws RemoteException the remote exception
	 */
	boolean resetAPDTimerWithKeycode(int keycode) throws RemoteException;
	
	
	/**
	 * add a callLetter into Favorite channel list and include a sister channel of it.
	 * @param callLetter be added into favorite channel list
	 * @throws RemoteException
	 */
	void addFavoriteChannel(String callLetter) throws RemoteException;
	
	/**
	 * remove a callLetter from favorite channel list and include a sister channel of it.
	 * @param callLetter be removed from favorite channel list.
	 * @throws RemoteException
	 */
	void removeFavoriteChannel(String callLetter) throws RemoteException;
	
	
	void addBlockChannels(String[] callLetters) throws RemoteException;
	void removeBlockChannels(String[] callLetters) throws RemoteException;
	
	void addFavoriteChannels(String[] callLetters) throws RemoteException;
	void removeFavoriteChannels(String[] callLetters) throws RemoteException;
}

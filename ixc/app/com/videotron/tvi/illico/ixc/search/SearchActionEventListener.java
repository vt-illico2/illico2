package com.videotron.tvi.illico.ixc.search;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class defined listener in each Platform that notifies Action. The
 * results of Action performed by each Platform are returned through the
 * Listener.
 *
 * @author Jinjoo Ok
 *
 */
public interface SearchActionEventListener extends Remote {
    /**
     * Inform the platform that Search application has closed.
     */
    String SEARCH_CLOSED = "CLOSED";
     /**
     * Request to the platform with Action.
     *
     * @param platform
     *                platform
     * @param action
     *                action
     * @throws RemoteException
     *                 The remote exception
     */
    void actionRequested(String platform, String[] action) throws RemoteException;
}

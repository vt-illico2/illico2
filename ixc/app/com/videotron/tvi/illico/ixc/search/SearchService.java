package com.videotron.tvi.illico.ixc.search;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The interface SearchService. provides API to launch Search Application for
 * each Platform.
 *
 * @author Jinjoo Ok
 * @version 1.1
 */
public interface SearchService extends Remote {
    /** Indicates the IXC name. */
    String IXC_NAME = "SearchService";
    /** Indicates platform type is EPG. * */
    String PLATFORM_EPG = "EPG";
    /** Indicates platform type is VOD. * */
    String PLATFORM_VOD = "VOD";
    /** Indicates platform type is PPV. * */
    String PLATFORM_PPV = "PPV";
    /** Indicates platform type is PVR. * */
    String PLATFORM_PVR = "PVR";
    /** Indicates platform type is MONITOR. */
    String PLATFORM_MONITOR = "MONITOR";
    /** Indicates Karaoke search. */
    String PLATFORM_KARAOKE = "KARAOKE";
    
    /**
     * SharedMemory key for Search URL.
     */
    String KEY_SEARCH_URL = "SEARCH_URL";
    
    /**
     * SharedMemory key for Search Proxy IP array String[].
     */
    String KEY_SEARCH_PROXY_IP = "SEARCH_PROXY_IP";
    
    /**
     * SharedMemory key for Search Proxy Port array int[].
     */
    String KEY_SEARCH_PROXY_PORT = "SEARCH_PROXY_PORT";

    /**
     * Requests to Search Application start.
     *
     * @param platform
     *                platform
     * @param init
     *                type whether search will be initialized or not
     * @throws RemoteException
     *                 the remote exception
     */
    void launchSearchApplication(String platform, boolean init)
            throws RemoteException;

    /**
     * R7.3 Sangyong
     * Requests to Search Application start.
     *
     * @param platform
     *                platform
     * @param init
     *                type whether search will be initialized or not
     *		  param
     *				  additional parameter(for measurement via d-option)  
     * @throws RemoteException
     *                 the remote exception
     */
    void launchSearchApplicationWithParam(String platform, boolean init, String param)
            throws RemoteException;

    /**
     * Requests to Search Application to stop.
     *@throws RemoteException
     *                 the remote exception
     */
    void stopSearchApplication() throws RemoteException;
    /**
     * Adds SearchActionEventListener to receive Action. The Action will be
     * passed to platform only when platform launched search and platform
     * receive Action are the same. In different case, Action passed by Monitor.
     *
     * @param platform
     *                platform
     * @param i
     *                SearchActionEventListener
     * @throws RemoteException
     *                 the remote exception
     */
    void addSearchActionEventListener(String platform,
            SearchActionEventListener i) throws RemoteException;

    /**
     * Removes registered Listener.
     * @param platform
     *                platform
     * @throws RemoteException
     *                 the remote exception
     */
    void removeSearchAcitonEventListener(String platform) throws RemoteException;
}

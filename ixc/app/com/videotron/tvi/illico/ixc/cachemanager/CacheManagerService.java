package com.videotron.tvi.illico.ixc.cachemanager;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CacheManagerService extends Remote {
    /** The IXC name of cache manager service. */
    String IXC_NAME = "CacheManagerService";
    
    /**
     * Gets cached data from cache manager service.
     * 
     * @param Application name
     * @param Cached Key
     * @return File Cached data.
     * @throws RemoteException
     */
    File getCachedFile(String appName, String cachedKey) throws RemoteException;

    /**
     * Puts cache data to cache manager service.
     * 
     * @param Application name
     * @param Cache key
     * @param Next update time milliseconds
     * @param File Cache data.
     * @throws RemoteException
     */
    void putCacheFile(String appName, String cacheKey, long nextUpdateTimeMillis, File cacheFile) throws RemoteException;
}

/**
 * 
 */
package com.videotron.tvi.illico.ixc.isa;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class provides ISA functionality for other applications through IXC.
 * 
 * @author zestyman
 * 
 */
public interface ISAService extends Remote {

	/** Indicates the IXC name. */
	String IXC_NAME = "ISAService";

	/**
	 * Indicate a channel type for ISA service.
	 */
	public static final String ISA_SERVICE_TYPE_CHANNEL = "channel";

	/**
	 * Indicate a freePreview type for ISA service.
	 */
	public static final String ISA_SERVICE_TYPE_FREE_PREVIEW = "freePreview";
	/**
	 * Indicate a illicoService type for ISA service.
	 */
	public static final String ISA_SERVICE_TYPE_ILLICO_SERVICE = "illicoService";

	/**
	 * Indicate a entry point of EPG grid (full or normal) .
	 */
	public static final String ENTRY_POINT_EPG_E01 = "epg_E01";

	/**
	 * Indicate a entry point of EPG show details screen.
	 */
	public static final String ENTRY_POINT_EPG_E02 = "epg_E02";

	/**
	 * Indicate a entry point of Unsubscribed channel screen.
	 */
	public static final String ENTRY_POINT_EPG_E03 = "epg_E03";

	/**
	 * Indicate a entry point of Channel environnent page.
	 */
	public static final String ENTRY_POINT_VOD_E01 = "vod_E01";

	/**
	 * Indicate a entry point of Movies/special description page.
	 */
	public static final String ENTRY_POINT_VOD_E02 = "vod_E02";

	/**
	 * Indicate a entry point of Channel environnent episodes page.
	 */
	public static final String ENTRY_POINT_VOD_E03 = "vod_E03";

	/**
	 * Indicate a entry point of Club unlimited description page with video.
	 */
	public static final String ENTRY_POINT_VOD_E04 = "vod_E04";

	/**
	 * Indicate a entry point of Club unlimited episodes page.
	 */
	public static final String ENTRY_POINT_VOD_E05 = "vod_E05";

	/**
	 * Indicate a entry point of Movie description page.
	 */
	public static final String ENTRY_POINT_VOD_E06 = "vod_E06";

	/**
	 * Indicate a entry point of Movie description page - Hybrid.
	 */
	public static final String ENTRY_POINT_VOD_E07 = "vod_E07";

	/**
	 * Indicate a entry point of Channel short description page.
	 */
	public static final String ENTRY_POINT_FPR_E01 = "fpr_E01";

	/**
	 * Indicate a entry point of Channel details page.
	 */
	public static final String ENTRY_POINT_FPR_E02 = "fpr_E02";

	/**
	 * Indicate a entry point of PASTILLE
	 */
	public static final String ENTRY_POINT_PASTILLE = "pastille";

	/**
	 * Indicate a requestLocale for french.
	 */
	public static final String LOCALE_FR_CA = "fr_CA";

	/**
	 * Indicate a requestLocale for english.
	 */
	public static final String LOCALE_EN_CA = "en_CA";

	/**
	 * Return ISA service list with ISA Service Type.
	 * @param one of {@link #ISA_SERVICE_TYPE_CHANNEL}, {@link #ISA_SERVICE_TYPE_FREE_PREVIEW}, {@link #ISA_SERVICE_TYPE_ILLICO_SERVICE
	 * @return a String array consist of serviceid for isaServiceType. Call Letter in the case of channel or free preview channel, Service number in the case of illico service (e.g. 33 for Club Unlimited).
	 */
	String[] getISAServiceList(String isaServiceType) throws RemoteException;

	/**
	 * Show a Instance subscription popup.
	 * 
	 * @param serviceId
	 *            Call Letter in the case of channel or free preview channel,
	 *            Service number in the case of illico service (e.g. 33 for Club
	 *            Unlimited).
	 * @param entryPoint
	 *            one of {@link #ENTRY_POINT_EPG_E01},
	 *            {@link #ENTRY_POINT_EPG_E02}, {@link #ENTRY_POINT_EPG_E03},
	 *            {@link #ENTRY_POINT_FRP_E01, {@link #ENTRY_POINT_FRP_E02},
	 *            {@link #ENTRY_POINT_PASTILLE}, {@link #ENTRY_POINT_VOD_E01},
	 *            {@link #ENTRY_POINT_VOD_E02}, {@link #ENTRY_POINT_VOD_E03,
	 *            {@link #ENTRY_POINT_VOD_E04}, {@link #ENTRY_POINT_VOD_E05},
	 *            {@link #ENTRY_POINT_VOD_E06} {@link #ENTRY_POINT_VOD_E07}
	 */
	void showSubscriptionPopup(String serviceId, String entryPoint) throws RemoteException;
	
	/**
	 * Hide a showing instance subscription popup.
	 * 
	 * @throws RemoteException
	 */
	void hideSubscriptionPopup() throws RemoteException;
	
	/**
	 * Add a {@link ISAServiceDataListener} to receive a updated data of ISA Service.
	 * @param l a object to implement {@link ISAServiceDataListener}.
	 * @param a appName of owner who implements {@link ISAServiceDataListener} in parameter.
	 */
	void addISAServiceDataListener(ISAServiceDataListener l, String appName) throws RemoteException;
	
	/**
	 * Remove a added {@link ISAServiceDataListener) via {@link #addISAServiceDataListener(ISAServiceDataListener, String)}.
	 * @param appName a appName of owner who implements {@link ISAServiceDataListener} in parameter.
	 */
	void removeISAServiceDataListener(String appName) throws RemoteException;

	/**
	 *
	 * @return array include the callLetters
	 * @throws RemoteException
	 */
	String[] getExcludedChannels() throws RemoteException;

	int[] getExcludedChannelTypes() throws RemoteException;
}

package com.videotron.tvi.illico.ixc.isa;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISAServiceDataListener extends Remote {

	/**
	 * Notify to be updated a ISAService list.
	 * 
	 * @param isaServiceType
	 *            one of {@link ISAService#ISA_SERVICE_TYPE_CHANNEL},
	 *            {@link ISAService#ISA_SERVICE_TYPE_FREE_PREVIEW},
	 *            {@link ISAService#ISA_SERVICE_TYPE_ILLICO_SERVICE}
	 * @param serviceList a String array consist of serviceId for each ISA service type.
	 */
	void updateISAServiceList(String isaServiceType, String[] serviceList) throws RemoteException;

    //->Kenneth[2016.8.12] ISA/CYO Bundling
	// Notify to be updated the excluded channel/type list.
	void updateISAExcludedChannelsAndTypes(String[] excludedChannels, int[] excludedTypes) throws RemoteException;
    //<-
}

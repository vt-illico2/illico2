package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * TODO.
 *
 * @version $Revision: 1.6 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface RemoteIterator extends Remote {

    int size() throws RemoteException;

    boolean hasNext() throws RemoteException;

    Remote next() throws RemoteException;

    void reset() throws RemoteException;
}

package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class is represents the TV Channel.
 *
 * @author  June Park
 */
public interface TvChannel extends Remote {

    /** Unknown Channel type. Constant for getType(). */
    short TYPE_UNKNOWN    = -1;
    /** Abstract Channel type. Constant for getType(). */
    short TYPE_ABSTRACT   = -2;
    /** Abstract Channel type. Constant for getType(). */
    short TYPE_HIDDEN     = -3;
    /** Normal Channel type. Constant for getType(). */
    short TYPE_NORMAL     = 0;
    /** Radio Channel type. Constant for getType(). */
    short TYPE_RADIO      = 1;
    /** Galaxie Channel type. Constant for getType(). */
    short TYPE_GALAXIE    = 2;
    /** PPV Channel type. Constant for getType(). */
    short TYPE_PPV        = 3;
    /** SportsMax Channel type. Constant for getType(). */
    short TYPE_SPORTSMAX  = 4;
    /** Virtual Channel type. Constant for getType(). */
    short TYPE_VIRTUAL    = 5;
    /** Technical Channel type. Constant for getType(). */
    short TYPE_TECH       = 6;
    /** Data Channel type. Constant for getType(). */
    short TYPE_DATA       = 7;
    /** SDV Channel type. Constant for getType(). */
    short TYPE_SDV        = 8;
    /** Not offered type. Constant for getType(). */
    short TYPE_NOT_OFFERED = 100;
    //->Kenneth[2015.4.15] : R5 : No Program type 추가됨.
    short TYPE_NO_PROGRAM = 9;
    //->Kenneth[2017.8.24] : R7.4 : Camera type 추가
    short TYPE_CAMERA = 10;

    /** TODO
    0	All
1	News
2	Movies
3	Family/Kids
4	Sports
5	Music
*/

    byte GENRE_ALL           = 0;
    byte GENRE_ENTERTAINMENT = 1;
    byte GENRE_LIFESTYLES    = 2;
    byte GENRE_SPORTS        = 3;
    byte GENRE_NEWS          = 4;
    byte GENRE_MOVIE         = 5;
    byte GENRE_DOCUMENTARIES = 6;
    byte GENRE_FAMILY        = 7;
    byte GENRE_KIDS          = 8;
    byte GENRE_MUSIC         = 9;

	//->Kenneth : 4K by June
    int DEFINITION_SD = 0;
    int DEFINITION_HD = 1;
    int DEFINITION_4K = 2;

    /**
     * Gets a call letter of Channel.
     * @return call letter.
     */
    String getCallLetter() throws RemoteException;

    /**
     * Gets a full name of Channel.
     * @return full name.
     */
    String getFullName() throws RemoteException;

    /**
     * Gets a type of Channel.
     * @return Channel type.
     */
    short getType() throws RemoteException;

    /**
     * Gets a number of Channel.
     * @return Channel number.
     */
    int getNumber() throws RemoteException;

    /**
     * Gets a OCAP source ID of Channel.
     * @return OCAP source ID.
     */
    int getSourceId() throws RemoteException;

    /**
     * Gets an unique identifier of Channel.
     * @return Normally, the ID will be same with source ID. But for the
     *      virtual channel, the id will be different.
     */
    int getId() throws RemoteException;


    /**
     * Determines whether this channel is HD or not.
     * @return true if this Channel is HD; false otherwise.
     */
    boolean isHd() throws RemoteException;

	//->Kenneth : 4K by June
    int getDefinition() throws RemoteException;

    /**
     * Determines whether this channel is subscribed or not.
     * @return true if this Channel is subscribed; false otherwise.
     */
    boolean isSubscribed() throws RemoteException;

    /**
     * Determines whether this channel has package but not subscribed.
     * @return true if this Channel has package from free preview.
     */
    boolean isAuthorizedByFreePreview() throws RemoteException;

    /**
     * Determines whether this channel is recordable or not.
     * @return true if this Channel is recordable; false otherwise.
     */
    boolean isRecordable() throws RemoteException;

    /**
     * Gets a genre of this Channel.
     * @return genre.
     */
    byte getGenre() throws RemoteException;

    /**
     * Gets a rating of this Channel.
     * @return rating.
     */
    int getRating() throws RemoteException;

    /**
     * Gets a language of this Channel.
     * @return language.
     */
    String getLanguage() throws RemoteException;

    String getDescription() throws RemoteException;

    /**
     * Gets A string-based representation of this channel's locator.
     * @return string of locator.
     */
    String getLocatorString() throws RemoteException;

    TvChannel getSisterChannel() throws RemoteException;


    //->Kenneth[2017.3.23] R7.3 : APD disabled 
    boolean apdDisabled() throws RemoteException;
    //<-
}

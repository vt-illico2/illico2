package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashSet;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;

/**
 * This class provides EPG functionality for other applications through IXC.
 *
 * @author  June Park
 */
public interface EpgService extends Remote {

    int STATE_NONE  = 0;
    int STATE_GUIDE = 1;
    int STATE_PIP   = 2;

    /** Indicates the IXC name. */
    String IXC_NAME = "EpgService";

    /**
     * SharedMemory key for channels. (value = Object[][9]) 
     * number (Integer), name (String), logo (Image), recordable (Boolean),
     * type (Integer), channel id (Integer), full name(String), sister name (String), definition (Integer)
     */
    String DATA_KEY_CHANNELS = "Epg.Channels";
    /**
     * SharedMemory key for Channel CA Map. (value = Hashtable<Integer, Boolean>)
     */
    String DATA_KEY_CA_MAP = "Epg.CaMap";

    void changeState(int state) throws RemoteException;

    ChannelContext getChannelContext(int tunerIndex) throws RemoteException;

    boolean isPpvWatching() throws RemoteException;

    boolean isAdultBlocked(TvProgram p) throws RemoteException;

    TvChannel getDataChannel() throws RemoteException;

    /**
     * Gets the Channel object with unique identifier.
     * @param id unique identifier of Channel.
     */
    TvChannel getChannel(int id) throws RemoteException;

    /**
     * Gets the Channel object with channel call letter.
     * @param callLetter Channel call letter (name).
     */
    TvChannel getChannel(String callLetter) throws RemoteException;

    TvChannel getChannelByNumber(int number) throws RemoteException;

    /**
     * Gets the Program.
     * @param id channel id.
     * @param time start time of program.
     */
    TvProgram getProgram(int id, long time) throws RemoteException;

    TvProgram getProgram(TvChannel ch, long time) throws RemoteException;

    TvProgram getProgram(String callLetter, long time) throws RemoteException;

    TvProgram requestProgram(String callLetter, long time) throws RemoteException;

    /**
     * Gets the Program.
     * @param id channel number.
     * @param time start time of program.
     */
    TvProgram getProgramByChannelNumber(int number, long time) throws RemoteException;

    TvProgram getCurrentProgram(int id) throws RemoteException;

    RemoteIterator requestProgramsByTitle(String callLetter, String title, long from) throws RemoteException;

    RemoteIterator findProgramsByTitle(String callLetter, String title, long from, long to) throws RemoteException;

    RemoteIterator requestPrograms(String callLetter, String programId, String seriesId, long from) throws RemoteException;

    RemoteIterator findPrograms(String callLetter, String programId, String seriesId, long from, long to) throws RemoteException;

    RemoteIterator findEpisodes(String callLetter, String seriesId, String programId) throws RemoteException;

    TvProgram getHighestRatingProgram(String callLetter, long from, long to) throws RemoteException;

    VideoController getVideoController() throws RemoteException;

    void selectSdvHomeChannel() throws RemoteException;

    ////////////////////////////////////////////////////////////////////////
    // Channel Context APIs
    ////////////////////////////////////////////////////////////////////////

    void changeChannel(int id) throws RemoteException;

    void changeChannel(TvChannel ch) throws RemoteException;

    void changeChannelByNumber(int number) throws RemoteException;

    void changeChannelByRecordingChange(int recording, HashSet selectedChannels) throws RemoteException;
    //->Kenneth[2017.10.13] : VDTRMASTER-6218 : 채널 요청시 message 를 보낼 수 있는 API 추가
    void changeChannel(int id, String msg) throws RemoteException;
    //<-Kenneth

    void stopChannel() throws RemoteException;

    TvChannel getCurrentChannel() throws RemoteException;

    /**
     * Registers a Listener that will be used to receive Channel Change Event.
     * @param l ChannelEventListener to be informed when channel changed.
     */
    void addChannelEventListener(ChannelEventListener l) throws RemoteException;

    /**
     * Removes registered ChannelEventListener.
     * @param l ChannelEventListener to be removed.
     */
    void removeChannelEventListener(ChannelEventListener l) throws RemoteException;


    void addEpgDataListener(EpgDataListener l) throws RemoteException;

    void removeEpgDataListener(EpgDataListener l) throws RemoteException;

    void notifyChannelPackageUpdated() throws RemoteException;

    String requestSdvRecording(int sourceId) throws RemoteException;

    void notifyRequestedSdvRecordingFailed(int sourceId) throws RemoteException;

    void addCaUpdateListener(CaUpdateListener l) throws RemoteException;

    void removeCaUpdateListener(CaUpdateListener l) throws RemoteException;
    int showProgramDetails(String[] param) throws RemoteException;

    void toggleMute() throws RemoteException;

    //->Kenneth[2015.6.25] DDC-107
    int requestTune(int channelNumber) throws RemoteException, IllegalArgumentException, IllegalStateException;
    //boolean requestTune(int channelNumber) throws RemoteException, IllegalArgumentException, IllegalStateException;

    void setLogStateListener(LogStateListener l) throws RemoteException;

    //->Kenneth[2017.9.27] R7.4
    void sdvErrorPopupClosed(String errorCode) throws RemoteException;
    //<-
}

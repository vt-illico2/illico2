package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class is represents the TV Channel.
 *
 * @version $Revision: 1.14 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface TvProgram extends Remote {

    /** Unknown type. Constant for getType(). */
    int TYPE_UNKNOWN   = -1;
    /** Generic type. Constant for getType(). */
    int TYPE_GENERIC   = 0;
    /** Program type. Constant for getType(). */
    int TYPE_PROGRAM   = 1;
    /** Movie type. Constant for getType(). */
    int TYPE_MOVIE     = 2;
    /** Event type. Constant for getType(). */
    int TYPE_EVENT     = 3;
    /** Series type. Constant for getType(). */
    int TYPE_SERIES    = 4;

    int AUDIO_UNKNOWN             = 0;
    int AUDIO_STEREO              = 1;
    int AUDIO_SURROUND            = 2;
    int AUDIO_STEREO_AND_SURROUND = 3;

	//->Kenneth : 4K by June
    int DEFINITION_SD = 0;
    int DEFINITION_HD = 1;
    int DEFINITION_4K = 2;

    /**
     * Returns the Channel of this program.
     * @return Channel.
     */
    TvChannel getChannel() throws RemoteException;

    /**
     * Gets a call letter of Channel.
     * @return call letter.
     */
    String getCallLetter() throws RemoteException;

    int getChannelId() throws RemoteException;

    long getStartTime() throws RemoteException;

    long getEndTime() throws RemoteException;

    long getDuration() throws RemoteException;

    /**
     * Gets a type of Program.
     * @return Program type.
     */
    int getType() throws RemoteException;

    /**
     * Determines whether this program is HD or not.
     * @return true if this Program is HD; false otherwise.
     */
    boolean isHd() throws RemoteException;

	//->Kenneth : 4K by June
    int getDefinition() throws RemoteException;

    /**
     * Determines whether this program is provided with closed caption.
     * @return true if closed captioned; false otherwise.
     */
    boolean isCaptioned() throws RemoteException;

    boolean isLive() throws RemoteException;

    boolean isInteractive() throws RemoteException;

    int getStarRating() throws RemoteException;

    /**
     * Gets a language of this Program.
     * @return language.
     */
    String getLanguage() throws RemoteException;


    String getTitle() throws RemoteException;

    /**
     * Gets an unique identifier of Program.
     * @return program id.
     */
    String getId() throws RemoteException;

    /**
     * Gets an audio type of Program.
     * @return audio type.
     */
    int getAudioType() throws RemoteException;

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    int getCategory() throws RemoteException;

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    int getSubCategory() throws RemoteException;

    /**
     * Gets a rating of this Channel.
     * @return rating.
     */
    String getRating() throws RemoteException;

    boolean isSapEnabled() throws RemoteException;

    String getProductionYear() throws RemoteException;

    String getCountry() throws RemoteException;

    String getFullDescription() throws RemoteException;

    String getActors() throws RemoteException;

    String getSeriesId() throws RemoteException;

    String getEpisodeTitle() throws RemoteException;

    /** Returns EID of PPV program. 0 for the non-PPV program. (Long) */
    long getPpvEid() throws RemoteException;

}

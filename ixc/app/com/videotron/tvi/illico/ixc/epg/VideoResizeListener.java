package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * TODO.
 *
 * @version $Revision: 1.4 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface VideoResizeListener extends Remote {

    void mainScreenResized(int x, int y, int w, int h) throws RemoteException;

}

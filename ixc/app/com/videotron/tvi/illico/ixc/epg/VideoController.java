package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * VideoController.
 *
 * @version $Revision: 1.5 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface VideoController extends Remote {

    /** do not show info bar. */
    short DO_NOT_SHOW    = 0;
    /** show info bar and do not hide. */
    short SHOW           = 1;
    /** show info bar and hide after few seconds . */
    short SHOW_AND_HIDE  = 2;

    /** show program name in info bar . */
    byte PROGRAM_INFO = 0;
    /** show channel name in info bar . */
    byte CHANNEL_INFO = 1;

    void resize(int x, int y, int w, int h, short display) throws RemoteException;

    void resize(int x, int y, int w, int h, short display, byte info) throws RemoteException;

    void maximize() throws RemoteException;

    void addVideoResizeListener(VideoResizeListener l) throws RemoteException;

    void removeVideoResizeListener(VideoResizeListener l) throws RemoteException;

}

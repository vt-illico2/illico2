package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class provides EPG functionality for other applications through IXC.
 *
 * @version $Revision: 1.4 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface ChannelContext extends Remote {

    byte STOPPED         = -1;
    byte CLEAR            = 0;
    byte BLOCK_BY_RATING  = 1;
    byte BLOCK_BY_CHANNEL = 2;

    byte getState() throws RemoteException;

    void changeChannel(int id) throws RemoteException;

    void changeChannel(TvChannel ch) throws RemoteException;

    void changeChannelByNumber(int number)  throws RemoteException;

    void stopChannel() throws RemoteException;

    TvChannel getCurrentChannel() throws RemoteException;

    /**
     * Registers a Listener that will be used to receive Channel Change Event.
     * @param l ChannelEventListener to be informed when channel changed.
     */
    void addChannelEventListener(ChannelEventListener l) throws RemoteException;

    /**
     * Removes registered ChannelEventListener.
     * @param l ChannelEventListener to be removed.
     */
    void removeChannelEventListener(ChannelEventListener l) throws RemoteException;

}

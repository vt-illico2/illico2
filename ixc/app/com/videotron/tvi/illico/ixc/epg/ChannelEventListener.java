package com.videotron.tvi.illico.ixc.epg;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class provides EPG functionality for other applications through IXC.
 *
 * @version $Revision: 1.4 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface ChannelEventListener extends Remote {

    void selectionRequested(int id, short type) throws RemoteException;

    void selectionBlocked(int id, short type) throws RemoteException;

}

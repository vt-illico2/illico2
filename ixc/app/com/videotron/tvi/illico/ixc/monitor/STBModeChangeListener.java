package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface notifies the STB mode changed.
 * @author Woosik Lee
 */
public interface STBModeChangeListener extends Remote {

    /**
     * It notices that the STB mode is changed. {@see MonitorService#STB_MODE_BOOTING},{@see
     * MonitorService#STB_MODE_NORMAL}, {@see MonitorService#STB_MODE_STANDALONE}.
     * @param newMode the new mode.
     * @throws RemoteException the exception.
     */
    void modeChanged(int newMode) throws RemoteException;
}

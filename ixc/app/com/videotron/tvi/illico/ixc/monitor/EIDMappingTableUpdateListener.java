package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface notifies the updating package id/entitlement id table.
 * @author Woosik Lee
 */
public interface EIDMappingTableUpdateListener extends Remote {

    /**
     * It notifies the updating package id/entitlement id table.
     * @throws RemoteException The exception.
     */
    void tableUpdated() throws RemoteException;
}

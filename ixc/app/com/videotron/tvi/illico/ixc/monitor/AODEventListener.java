// $Header: /home/cvs/illico2/ixc/app/com/videotron/tvi/illico/ixc/monitor/AODEventListener.java,v 1.4 2017/01/12 19:28:39 zestyman Exp $

/*
 *  AodEventListener.java	$Revision: 1.4 $ $Date: 2017/01/12 19:28:39 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface AODEventListener extends Remote {

	/**
	 * Notifies the result of requested AOD.
	 *
	 * @param result
	 *            {@link MonitorService#AOD_REQUEST_SUCCESS},
	 *            {@link MonitorService#AOD_ERROR_APP_NAME_NOT_FOUND},
	 *            {@link MonitorService#AOD_ERROR_CONFIGURATION_FILE},
	 *            {@link MonitorService#AOD_ERROR_STARTING_TIMER},
	 *            {@link MonitorService#AOD_ERROR_STOPPING_TIMER},
	 *            {@link MonitorService#AOD_ERROR_TIME_OUT_DELAY_EXPIRED},
	 *            {@link MonitorService#AOD_ERROR_UNABLE_TO_UNZIP_FILE},
	 *            {@link MonitorService#AOD_ERROR_XAIT_FILE_NOT_FOUND},
	 *            {@link MonitorService#AOD_ERROR_XAIT_PARSING},
	 *            {@link MonitorService#AOD_ERROR_XAIT_SERVER_NOT_REACHABLE},
	 *            {@link MonitorService#AOD_ERROR_ZIP_FILE_NOT_FOUND},
	 *            {@link MonitorService#AOD_ERROR_ZIP_FILE_SERVER_NOT_REACHABLE}
	 *            .
	 * @throws RemoteException
	 *             The remote exception.
	 */
	void notifyAodEvent(int result) throws RemoteException;

}

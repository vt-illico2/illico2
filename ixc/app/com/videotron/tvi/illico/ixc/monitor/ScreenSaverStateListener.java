package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The interface notifies screen-saver's life cycle.
 * @author Woosik Lee
 *
 */
public interface ScreenSaverStateListener extends Remote {

    /**
     * Notifies that the screen-saver will be started. 
     * @throws RemoteException  the exception.
     */
    void startScreenSaver() throws RemoteException;
    /**
     * Notifies that the screen-saver will be stopped.
     * @throws RemoteException the exception.
     */
    void stopScrenSaver() throws RemoteException;
}

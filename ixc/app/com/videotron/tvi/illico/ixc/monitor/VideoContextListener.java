package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface notifies change of current video mode.
 * @author WSLEE1
 *
 */
public interface VideoContextListener extends Remote {

	/**
	 * Notify change of current video mode.
	 * @param videoName special video name or default video mode {@see MonitorService#DEFAULT_VIDEO_NAME}.
	 * @throws RemoteException remote exception.
	 */
	public void videoModeChanged(String videoName) throws RemoteException;
}

package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface notifies the state transition event.
 * <P>
 * @author Woosik Lee
 */
public interface ServiceStateListener extends Remote {

    /**
     * Notify service state changes.
     * <P>
     * When the service state changed, notifies the event this interface.
     * @param newState new service state see the
     *            {@link com.videotron.tvi.illico.ixc.monitor.MonitorService#TV_VIEWING_STATE} ,
     *            {@link com.videotron.tvi.illico.ixc.monitor.MonitorService#FULL_SCREEN_APP_STATE}.
     * @throws RemoteException the exception.
     */
    void stateChanged(int newState) throws RemoteException;
}

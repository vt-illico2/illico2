package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface notifies change of CA authorization state from CANH.
 * @author WSLEE1
 *
 */
public interface MonitorCAAuthorizationListener extends Remote  {
	/**
	 * Monitor notifies change of CA authorization.
	 * @param targetId the target id.
	 * @param targetType the target type.
	 * @param isAuthorized authorized or not.
	 * @param packageId the package id.
	 * @throws RemoteException the remote exception.
	 */
	void deliverEvent(long targetId, int targetType, boolean isAuthorized, String packageId) throws RemoteException;
}

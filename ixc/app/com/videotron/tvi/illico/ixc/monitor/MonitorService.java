package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class provides Monitor functionality for other applications through IXC.
 *
 * @author Woosik Lee
 */
public interface MonitorService extends Remote {

	/** Indicates the IXC name. */
	String IXC_NAME = "MonitorService";
	/** Indicates the service state that is unknown state. for instance booting. */
	int UNKNOWN_STATE = -1;
	/** Indicates the service state that is a watching TV state. */
	int TV_VIEWING_STATE = 0;
	/** Indicates the service state that is full screen application state. */
	int FULL_SCREEN_APP_STATE = 1;
	/** Indicates the PIP application state. */
	int PIP_STATE = 2;

	/** It is the requested event to launch the application by hot key. */
	String REQUEST_APPLICATION_HOT_KEY = "HOT_KEY";
	/**
	 * It is the requested event to launch the application from the MainMenu
	 * application.
	 */
	String REQUEST_APPLICATION_MENU = "MENU";
	/**
	 * It is the requested event to launch the application by last key from any
	 * Application.
	 */
	String REQUEST_APPLICATION_LAST_KEY = "LAST_KEY";

	/** Indicates the STB mode that is normal mode. */
	int STB_MODE_NORMAL = 101;
	/** Indicates the STB mode is standalone mode. */
	int STB_MODE_STANDALONE = 102;
	/** Indicates the STB mode that is no signal mode. */
	int STB_MODE_NO_SIGNAL = 103;
	/** Indicates the STB mode that is blocked by Mandatory authority. */
	int STB_MODE_MANDATORY_BLOCKED = 104;
	/** Indicates the STB mode is booting. */
	int STB_MODE_BOOTING = -1;

	/** It is the result of checking authorization. */
	int NOT_FOUND_ENTITLEMENT_ID = Integer.MIN_VALUE;
	/** It is the result of checking authorization. */
	int AUTHORIZED = 1;
	/** It is the result of checking authorization. */
	int NOT_AUTHORIZED = 2;
	/** It is the result of checking authorization. */
	int CHECK_ERROR = 3;

	// ////////////////////////////////////////////////
	// Following is the names of Inband data file.
	// /////////////////////////////////////////////////
	/**
	 * If not found the version, returns this value in
	 * {@link MonitorService#getInbandDataVersion(String)} .
	 */
	int VERSION_NOT_FOUND = -1;
	String mini_menu_items = "mini_menu_items";
	String full_menu_items = "full_menu_items";
	String menu_config = "menu_config";
	String channel_logos = "channel_logos";
	String galaxie_wallpapers = "galaxie_wallpapers";
	String epg_logos = "epg_logos";
	String screen_saver_image = "screen_saver_image";
	String loading_animation_config = "loading_animation_config";
	String booting_animation_config = "booting_animation_config";
	String pvr_config = "pvr_config";
	String search_config = "search_config";
	String vod_server_config = "vod_server_config";
	String widget_list = "widget_list";
	String dashboard_config_ib = "dashboard_config_ib";
	String postal_code = "postal_code";
	String notification = "notification";
	String stc_server = "stc_server";
	String dtaa_config = "dtaa_config";
	String general_config = "general_config";
	String error_codes = "error_codes";
	String cache_management = "cache_management";

	String itv_server = "itv_server";
	String cyo = "cyo";
	String callerid_server_config = "callerid_server_config";

	String help_topic = "help_topic";

	String virtual_channels = "virtual_channels";
	String ppv_banner = "ppv_banner";
	String pcv_teaser = "pcs_teaser";
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    // Kenneth [2015.7.17] : MS 의 version.txt 와 불일치 발견. 
    // 현재의 version.txt 와 맞춤
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    String list_behavior = "list_behavior";
    String vod_common = "vod_common";
    String tutorial_images = "tutorial_images";
    String portal_brand_images = "portal_brand_images";
    /*
    //Kenneth [2014.9.26] : R4.1 때문에 추가 
    String list_behavior = "list_behavior";
    String list_behavior_images = "list_behavior_images";

    //->Kenneth[2015.7.15] : Tank : brand image 업데이트 때문에 추가됨
    String portal_brand_images = "portal_brand_images";
    //<-
    */

	/**
	 * The default video name. It is used change video mode.
	 */
	String DEFAULT_VIDEO_NAME = "EPG";

	/**
	 * AOD Error code
	 */
	int AOD_REQUEST_SUCCESS = 0;
	/** XAIT Server not reachable. */
	int AOD_ERROR_XAIT_SERVER_NOT_REACHABLE = 1;
	/** XAIT file not found. */
	int AOD_ERROR_XAIT_FILE_NOT_FOUND = 2;
	/** AOD application name not found. */
	int AOD_ERROR_APP_NAME_NOT_FOUND = 3;
	/** XAIT parsing error. */
	int AOD_ERROR_XAIT_PARSING = 4;
	/** AOD application ZIP file source server not reachable. */
	int AOD_ERROR_ZIP_FILE_SERVER_NOT_REACHABLE = 5;
	/** AOD application ZIP file not found. */
	int AOD_ERROR_ZIP_FILE_NOT_FOUND = 6;
	/** Unable to unzip file. */
	int AOD_ERROR_UNABLE_TO_UNZIP_FILE = 7;
	/** AOD loading/launching timeout delay has expired. */
	int AOD_ERROR_TIME_OUT_DELAY_EXPIRED = 8;
	/** Error starting AOD timer. */
	int AOD_ERROR_STARTING_TIMER = 9;
	/** Error stopping AOD timer. */
	int AOD_ERROR_STOPPING_TIMER = 10;
	/** Error not found AOD configuration file or not found aod application name. */
	int AOD_ERROR_CONFIGURATION_FILE = 11;
	/** Error parsing AOD configuration file. */
	int AOD_ERROR_CONFIGURATION_FILE_PARSING = 12;
	/** Requested AOD is already loaded. */
	int AOD_ALREADY_LOADDED = 100;
	
	/** Key for UHD Support */
	String SUPPORT_UHD = "SUPPORT_UHD";

	void loadAODApplication(String aodAppName, AODEventListener listener) throws RemoteException;

	void startAODApplication(String aodAppName, AODEventListener listener) throws RemoteException;

	void closeAODApplication(String aodAppName, AODEventListener listener) throws RemoteException;

	void addAODApplicationRemoveListener(AODRemoveListener listener, String applicationName) throws RemoteException;

	void removeAODApplicationRemoveListener(AODRemoveListener listener, String applicationName) throws RemoteException;

	/**
	 * Gets the CableCard MAC address.
	 *
	 * @return the mac address.
	 * @throws RemoteException
	 *             the exception.
	 */
	byte[] getCableCardMacAddress() throws RemoteException;

	/**
	 * Gets the correct MAC address to communicate with the appropriate backend.
	 * @return MAC address string.
	 * @throws RemoteException the exception.
	 */
	String getMacAddress() throws RemoteException;

	/**
	 * During the boot process, the unbound applications call available this
	 * method.
	 *
	 * @param applicationName
	 *            the application unique name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void iamReady(String applicationName) throws RemoteException;

	/**
	 * Checks the user has the authorization to execute the application.
	 *
	 * @param applicationName
	 *            the application unique name.
	 * @return {@link MonitorService#NOT_AUTHORIZED},
	 *         {@link MonitorService#AUTHORIZED},
	 *         {@link MonitorService#NOT_FOUND_ENTITLEMENT_ID},
	 *         {@link MonitorService#CHECK_ERROR}.
	 * @throws RemoteException
	 *             the exception.
	 */
	int checkAppAuthorization(String applicationName) throws RemoteException;

	/**
	 * Checks the authorization using packageID.
	 *
	 * @param packageID
	 *            the package ID
	 * @return if authorized package ID, returns
	 *         {@link MonitorService#AUTHORIZED}. Otherwise return
	 *         {@link MonitorService#NOT_AUTHORIZED}. if not found the
	 *         entitlement id using packageID, returns
	 *         {@link MonitorService#NOT_FOUND_ENTITLEMENT_ID}. if error occurs,
	 *         return {@link MonitorService.CHECK_ERROR}.
	 * @throws RemoteException
	 *             the exception.
	 */
	int checkResourceAuthorization(String packageID) throws RemoteException;

	/**
	 * Adds the CAAuthorizationListener.
	 *
	 * @param l
	 *            instance of MonitorCAAuthorizationListener.
	 * @param applicationName
	 *            application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void addCAResourceAuthorizationListener(MonitorCAAuthorizationListener l, String applicationName)
			throws RemoteException;

	/**
	 * Removes the CAAuthorizationListener.
	 *
	 * @param l
	 *            instance of MonitorCAAuthorizationListener.
	 * @param applicationName
	 *            application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void removeCAResourceAuthorizationListener(MonitorCAAuthorizationListener l, String applicationName)
			throws RemoteException;

	/**
	 * Gets the iPPV Deactivation package's authority.
	 *
	 * @return {@link MonitorService#NOT_AUTHORIZED},
	 *         {@link MonitorService#AUTHORIZED},
	 *         {@link MonitorService#NOT_FOUND_ENTITLEMENT_ID},
	 *         {@link MonitorService#CHECK_ERROR}.
	 * @throws RemoteException
	 *             the exception.
	 */
	int getIPPVDeactivationPackageAuthority() throws RemoteException;

	/**
	 * Requests the parameters. It returns the parameters that is
	 * {@link MonitorService#startUnboundApplication(String, String[])} 's
	 * parameter
	 * <P>
	 * The first index of parameter indicates the start event. Following is the
	 * start event variables.
	 *
	 * @link {@link MonitorService#REQUEST_APPLICATION_HOT_KEY},
	 * @link {@link MonitorService#REQUEST_APPLICATION_LAST_KEY},
	 * @link {@link MonitorService#REQUEST_APPLICATION_MENU}.
	 * @param appName
	 *            Application name to request the parameter.
	 * @return parameters it is the parameters by
	 *         {@link MonitorService#startUnboundApplication(String, String[])}
	 * @throws RemoteException
	 *             the exception.
	 */
	String[] getParameter(String appName) throws RemoteException;

	/**
	 * Request to start unbound application. When this method is called, current
	 * running application is destroyed (or paused), and the requested
	 * application is started. All these action is performed asynchronously.
	 *
	 * @param appName
	 *            unbound application name to be started.
	 * @param parameters
	 *            the arguments to be transfered. It can be retrieved by
	 *            {@link MonitorService#getParameter(String)}.
	 * @return if successfully launched the application returns true, otherwise
	 *         returns false.
	 * @throws RemoteException
	 *             the exception.
	 */
	boolean startUnboundApplication(String appName, String[] parameters) throws RemoteException;

	/**
	 * In the special video state, this method stops current overlapping
	 * application.
	 *
	 * @throws RemoteException
	 */
	void stopOverlappingApplication() throws RemoteException;

	/**
	 * Returns current overlapping application name.
	 * Return null, if overlapping application is not exist.
	 * @return
	 * @throws RemoteException
	 */
	String getCurrentOverlappingApplicationName() throws RemoteException;

	/**
	 * Enter to special video mode. It is called by PVR, VOD, PCS.
	 *
	 * @param videoName
	 *            the video name.
	 * @param endingMessage
	 *            Monitor can stop special video mode and shows confirm popup.
	 *            This parameter is confirm popup message. If null, Monitor does
	 *            not show confirm popoup.
	 * @throws RemoteException the exception.
	 */
	void reserveVideoContext(String videoName, String endingMessage) throws RemoteException;

	/**
	 * Exit to normal video mode.
	 * @throws RemoteException the exception.
	 */
	void releaseVideoContext() throws RemoteException;

	/**
	 * Gets the current video context name.
	 * @return video name.
	 * @throws RemoteException the exception.
	 */
	String getVideoContextName() throws RemoteException;

	/**
	 * Whether special video mode or not.
	 * @return If special video mode now, return true otherwise return false.
	 * @throws RemoteException the exception.
	 */
	boolean isSpecialVideoState() throws RemoteException;

	/**
	 * Adds the {@link VideoContextListener} .
	 * @param l	{@link VideoContextListener}.
	 * @throws RemoteException	the exception.
	 */
	void addVideoContextListener(VideoContextListener l, String appName) throws RemoteException;

	void removeVideoContextListener(VideoContextListener l, String appName) throws RemoteException;

	/**
	 * Request to start unbound application.
	 *
	 * @param code
	 *            hot key code
	 * @throws RemoteException
	 *             the exception.
	 */
	void startUnboundAppWithHotKey(int keyCode) throws RemoteException;

	/**
	 * Requests to destroy the current running application When this method is
	 * called, MonitorApplication destroys (or pause) the current running
	 * application, and selects the LCW service. All these action is performed
	 * asynchronously.
	 *
	 * @throws RemoteException
	 *             the exception.
	 */
	void exitToChannel() throws RemoteException;

	/**
	 * Requests to destroy the current running application and moves to the
	 * specified service by sourceId. When this method is called, Monitor
	 * destroys (or pause) the current running application, and selects the
	 * specified service. All these action is performed asynchronously.
	 *
	 * @param sourceId
	 *            The target channel's source id.
	 * @throws RemoteException
	 *             the exception.
	 */
	void exitToChannel(int sourceId) throws RemoteException;

	/**
	 * Gets the service state.
	 *
	 * @return current service state.
	 * @throws RemoteException
	 *             the exception.
	 */
	int getState() throws RemoteException;

	/**
	 * Whether TV_VIEWING_STATE or not.
	 *
	 * @return if TV_VIEWING_STATE, returns true.
	 * @throws RemoteException
	 *             the exception.
	 */
	boolean isTvViewingState() throws RemoteException;

	/**
	 * Adds the service state listener.
	 *
	 * @param l
	 *            The {@link ServiceStateListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void addStateListener(ServiceStateListener l, String applicationName) throws RemoteException;

	/**
	 * Removes the service state listener.
	 *
	 * @param l
	 *            The {@link ServiceStateListener} instance.
	 * @param applicationName
	 *            the application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void removeStateListener(ServiceStateListener l, String applicationName) throws RemoteException;

	/**
	 * Gets the STB mode.
	 *
	 * @return currrent STB mode. {@link MonitorService#STB_MODE_BOOTING},
	 *         {@link MonitorService#STB_MODE_NORMAL},
	 *         {@link MonitorService#STB_MODE_STANDALONE},
	 *         {@link MonitorService#STB_MODE_NO_SIGNAL},
	 *         {@link MonitorService#STB_MODE_MANDATORY_BLOCKED}.
	 * @throws RemoteException
	 *             the exception.
	 */
	int getSTBMode() throws RemoteException;

	/**
	 * Adds the STB mode changed listener.
	 *
	 * @param l
	 *            The {@link STBModeChangeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void addSTBModeChangeListener(STBModeChangeListener l, String applicationName) throws RemoteException;

	/**
	 * Removes the STB mode changed listener.
	 *
	 * @param l
	 *            The {@link STBModeChangeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void removeSTBModeChangeListener(STBModeChangeListener l, String applicationName) throws RemoteException;

	/**
	 * Resets screen saver timer. It is called when the software action occurs.
	 *
	 * @throws RemoteException
	 *             the exception.
	 */
	void resetScreenSaverTimer() throws RemoteException;

	/**
	 * Requests to start screen saver.
	 *
	 * @throws RemoteException
	 */
	void startScreenSaver() throws RemoteException;

	/**
	 * Adds the screen saver confirmation listener.
	 *
	 * @param l
	 *            The {@link ScreenSaverConfirmationListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void addScreenSaverConfirmListener(ScreenSaverConfirmationListener l, String applicationName)
			throws RemoteException;

	/**
	 * Removes the screen saver confirmation listener.
	 *
	 * @param l
	 *            The {@link ScreenSaverConfirmationListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void removeScreenSaverConfirmListener(ScreenSaverConfirmationListener l, String applicationName)
			throws RemoteException;

	/**
	 * Adds the screen saver state listener
	 *
	 * @param l
	 *            the {@link ScreenSaverStateListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void addScreenSaverStateListener(ScreenSaverStateListener l, String applicationName) throws RemoteException;

	/**
	 * Removes the screen saver state listener.
	 *
	 * @param l
	 *            The {@link ScreenSaverStateListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void removeScreenSaverStateListener(ScreenSaverStateListener l, String applicationName) throws RemoteException;

	/**
	 * Adds the Inband data listener.
	 *
	 * @param l
	 *            The {@link InbandDataListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	void addInbandDataListener(InbandDataListener l, String applicationName) throws RemoteException;

	/**
	 * Removes the Inband data listener.
	 *
	 * @param l
	 *            The {@link InbandDataListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	void removeInbandDataListener(InbandDataListener l, String applicationName) throws RemoteException;

	/**
	 * The applications must call this interface when application completes
	 * receiving the Inband data. When all applicatios that receiving the Inband
	 * data call this interface, the Monitor application can perform next
	 * process in boot state.
	 *
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	void completeReceivingData(String applicationName) throws RemoteException;

	/**
	 * Adds the EIDMappingTableUpdateListener. If any application want to get
	 * the event that package id/entitlement id changed, can register event.
	 *
	 * @param l
	 *            EIDMappingTableUpdateListener instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	void addEIDMappingTableUpdateListener(EIDMappingTableUpdateListener l, String applicationName)
			throws RemoteException;

	/**
	 * Removes the {@link EIDMappingTableUpdateListener}.
	 *
	 * @param l
	 *            EIDMappingTableUpdateListener instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	void removeEIDMappingTableUpdateListener(EIDMappingTableUpdateListener l, String applicationName)
			throws RemoteException;

	/**
	 * Starts the monitoring key event to go standby mode automatically.
	 *
	 * @param inactivityTime
	 *            the waiting time for auto standby mode. unit is millisecond.
	 * @throws RemoteException
	 *             The exception.
	 */
	void startAutoStandby(long inactivityTime) throws RemoteException;

	/**
	 * Stops the monitoring key event to go standby mode automatically.
	 *
	 * @throws RemoteException
	 *             The exception.
	 */
	void stopAutoStandby() throws RemoteException;

	/**
	 * Returns the version of the Inband data file.
	 *
	 * @param fileName
	 *            the file name.
	 * @return version. if not found the file name, returns
	 *         {@link MonitorService#VERSION_NOT_FOUND}
	 * @throws RemoteException
	 *             RemoteException The exception.
	 */
	int getInbandDataVersion(String fileName) throws RemoteException;

	/**
	 * Returns the entitlement id in the package_name_eid.txt file.
	 *
	 * @param packageName
	 *            the package name.
	 * @return entitlement id, if not found the package id, returns NULL.
	 * @throws RemoteException
	 *             The exception.
	 */
	String getEidAtPackageNameEidMappingFile(String packageName) throws RemoteException;

	/**
	 * Adds the blocked-time state listener
	 *
	 * @param l
	 *            the {@link BlockedTimeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void addBlockedTimeListener(BlockedTimeListener l, String applicationName) throws RemoteException;

	/**
	 * Removes the blocked-time state listener.
	 *
	 * @param l
	 *            The {@link BlockedTimeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	void removeBlockedTimeListener(BlockedTimeListener l, String applicationName) throws RemoteException;

	/**
	 * Gets the current activated application name in the
	 * {@link MonitorService#FULL_SCREEN_APP_STATE} only. If this method is
	 * called in the {@link MonitorService#TV_VIEWING_STATE}, returns null;
	 *
	 * @return Application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	String getCurrentActivatedApplicationName() throws RemoteException;

	/**
	 * Gets the DNCS name in OOB's Moni_DataOOB/package_name_eid.txt file.
	 *
	 * @return DNCS name.
	 * @throws RemoteException
	 *             the exception.
	 */
	String getDncsName() throws RemoteException;

	/**
	 * Requests to register unbound application.
	 * Monitor have a server ip address and port.
	 * Parameter Example
	 * 1. /xait?appId=0x4011&orgId=0x5F
	 * 2. /appstore/xait.xml/
	 *
	 * @param xaitFilePath The XAIT's file path.
	 * @throws RemoteException The remote exception.
	 */
	void registerUnboundApplication(String xaitFilePath) throws RemoteException;

	/**
	 * Gets the Mandatory package name array.
	 * If the Mandatory package is not present, returns null.
	 * If the Mandatory package is empty, returns empty array (Array is not null and size is 0).
	 * @return Mandatory package name array.
	 * @throws RemoteException The remote exception
	 */
	String[] getMandatoryPackageNames() throws RemoteException;

	/**
	 * Gets the Activation package name array.
	 * If the Activation package is not present, returns null.
	 * If the Activation package is empty, returns empty array (Array is not null and size is 0).
	 * @return Activation package name array.
	 * @throws RemoteException The remote exception
	 */
	String[] getActivationPackageNames() throws RemoteException;

	/**
	 * Gets the authorization of Multiroom package.
	 * @return User has authorization of Multiroom return true.
	 * @throws RemoteException The remote exception
	 */
	boolean isMultiroomEnabled() throws RemoteException;

	/**
	 * Checkes the STB is blocked by "terminal blocked time".
	 *
	 * @return if the STB is blocked, returns true.
	 * @throws RemoteException the exception.
	 */
    boolean isTerminalBlockedTime() throws RemoteException;

    //->Kenneth[2015.6.29] Tank
	/**
	 * Show list behavior popup
	 *
	 * @throws RemoteException the exception.
	 */
    public void showListPressedInNonPvrPopup() throws RemoteException;
    //<-

    /**
     * returns YearShift in Monitor
     */
    public int getYearShift() throws RemoteException;
}

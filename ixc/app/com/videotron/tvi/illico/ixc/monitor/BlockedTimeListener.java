package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * This interface notifies time blocked started and stopped.
 * @author WSLEE1
 *
 */
public interface BlockedTimeListener extends Remote {

    /**
     * Notifies that the blocked-time will be started. 
     * @throws RemoteException  the exception.
     */
    void startBlockedTime() throws RemoteException;
    /**
     * Notifies that the blocked-time will be stoped.
     * @throws RemoteException the exception.
     */
    void stopBlockedTime() throws RemoteException;
}

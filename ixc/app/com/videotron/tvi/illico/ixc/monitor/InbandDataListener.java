package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface notifies Inband data available at booting time or standby.
 * @author Woosik Lee
 */
public interface InbandDataListener extends Remote {

    /**
     * It notifies that receiving the Inband data is available.
     * @param locator the locator string.
     * @throws RemoteException The exception.
     */
    void receiveInbandData(String locator) throws RemoteException;
}

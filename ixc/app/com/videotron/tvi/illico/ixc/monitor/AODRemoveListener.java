// $Header: /home/cvs/illico2/ixc/app/com/videotron/tvi/illico/ixc/monitor/AODRemoveListener.java,v 1.5 2017/01/12 19:28:39 zestyman Exp $

/*
 *  AODRemoveListener.java	$Revision: 1.5 $ $Date: 2017/01/12 19:28:39 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface AODRemoveListener extends Remote {

	void notifyAODRemoved(String aodAppName) throws RemoteException;

	/**
	 * Notify AOD application version is update.
	 * @param aodAppName AOD application name.
	 * @throws RemoteException RemoteException.
	 */
	void notifyAODChanged(String aodAppName) throws RemoteException;
}

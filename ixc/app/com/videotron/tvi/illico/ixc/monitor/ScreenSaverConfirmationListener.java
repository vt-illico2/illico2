package com.videotron.tvi.illico.ixc.monitor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface asks other applications about screen-saver enable or disable.
 * <P>
 * Using this interface, the applications determine activation screen-saver.
 * @author Woosik Lee
 */
public interface ScreenSaverConfirmationListener extends Remote {
    /**
     * As need screen-saver, the monitor asks other applications to confirm.
     * <P>
     * If any application return false, screen-saver is not launching. In other words All applications return true,
     * screen-saver can launch.
     * @return if true, screen-saver will be run, if false, will be cancel.
     * @throws RemoteException the exception.
     */
    boolean confirmScreenSaver() throws RemoteException;
}

package com.videotron.tvi.illico.ixc.screensaver;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 *  This class provides Screen-saver functionality for other applications through IXC.
 * @author Woosik Lee
 *
 */
public interface ScreenSaverService extends Remote {

    /** Indicates the IXC name. */
    public static final String IXC_NAME = "ScreenSaverService";

    /**
     * Shows the screen saver.
     * @param currentRunningApplicationName Current activated application name.
     * @throws RemoteException the exception.
     */
    public void showScreenSaver(String currentRunningApplicationName) throws RemoteException;

    /**
     * Hides the screen saver.
     * @throws RemoteException the exception.
     */
    public void hideScreenSaver() throws RemoteException;
    
    /**
     * Gets the maximun inactivity time.
     * @return the screen-saver's maximum inactivity time. Unit is second.
     * @throws RemoteException the exception.
     */
    public long getMaximulInactivityTime() throws RemoteException;

}

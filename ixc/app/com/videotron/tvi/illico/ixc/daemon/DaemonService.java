package com.videotron.tvi.illico.ixc.daemon;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * Interface that allows other application to register RemoteRequestListener.
 * @author jinjoo Ok
 * @version 1.1
 */
public interface DaemonService extends Remote {

    /** Indicates the IXC name. */
    String IXC_NAME = "DaemonService";

    /**
     * Add listener to DaemonService.
     * @param appName host application
     * @param i RemoteRequestListener
     * @throws RemoteException during the execution of a remote method call
     */
    void addListener(String appName, RemoteRequestListener i)
            throws RemoteException;

    /**
     * Remove listener.
     * @param appName host application
     * @throws RemoteException during the execution of a remote method call
     */
    void removeListener(String appName) throws RemoteException;
//    void sendRequestByUDP(DatagramPacket packet)  throws Exception;
}

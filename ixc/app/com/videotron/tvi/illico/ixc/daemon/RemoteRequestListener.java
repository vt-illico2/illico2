package com.videotron.tvi.illico.ixc.daemon;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * listener interface that can receive HTTP/UDP message.
 * Response for each message is returned as the return value of API
 * @author Jinjoo Ok
 * @version 1.1
 */
public interface RemoteRequestListener extends Remote {
    /**
     * Pass request to host application.
     * @param path path to the operation to execute by host application.
     * @param body parameters(parma1=value1&param2=value2..)
     * @return response to request
     * @throws RemoteException during the execution of a remote method call
     */
    byte[] request(String path, byte[] body) throws RemoteException;
}

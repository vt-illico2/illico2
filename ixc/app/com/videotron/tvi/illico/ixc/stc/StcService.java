package com.videotron.tvi.illico.ixc.stc;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface StcService extends Remote{
	/** 
	 * The IXC name of STC application.
	 */ 
	String IXC_NAME = "STC";
	/**
	 * 
	 * @param appName
	 * @return
	 * @throws RemoteException
	 */
	public int registerApp(String appName) throws RemoteException;

	/**
	 * 
	 * @param appName
	 * @param listener
	 * @throws RemoteException
	 */
	public void addLogLevelChangeListener(String appName, LogLevelChangeListener listener) throws RemoteException;

	/**
	 * 
	 * @param listener
	 * @throws RemoteException
	 */
	public void removeLogLevelChangeListener(String appName) throws RemoteException;
}

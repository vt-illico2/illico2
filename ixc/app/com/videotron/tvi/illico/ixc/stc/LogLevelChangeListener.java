package com.videotron.tvi.illico.ixc.stc;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LogLevelChangeListener extends Remote {
	
	public void logLevelChanged(int logLevel) throws RemoteException;
	
}

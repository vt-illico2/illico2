package com.videotron.tvi.illico.ixc.stc;

import java.rmi.Remote;

public interface LogEvent extends Remote{
	public static int LEVEL_OFF= 0;
	
	public static int LEVEL_ALERT = 1;
	
	public static int LEVEL_CRITICAL = 2;//fatal
	
	public static int LEVEL_ERROR = 3;
	
	public static int LEVEL_WARNING = 4;
	
	public static int LEVEL_NOTICE = 5;
	
	public static int LEVEL_INFO = 6;
	
	public static int LEVEL_DEBUG = 7;
	
	public int getCurrentLogLevel();
	
	public int getPreviousLogLevel();
		
}

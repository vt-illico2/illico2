package com.videotron.tvi.illico.ixc.dashboard;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface DashboardService extends Remote {
    /** Indicates the IXC name.*/
    String IXC_NAME = "DashboardService";

    void showDashboard() throws RemoteException;
    void hideDashboard() throws RemoteException;
}

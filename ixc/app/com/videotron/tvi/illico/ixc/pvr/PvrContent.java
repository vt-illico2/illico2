package com.videotron.tvi.illico.ixc.pvr;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.io.Serializable;

/**
 * This class represents recorded contents by PVR application.
 *
 * @version $Revision: 1.7.6.1 $ $Date: 2017/08/23 20:06:31 $
 * @author  June Park
 */
public interface PvrContent extends Remote {

    short STATE_RECORDED  = 1;
    short STATE_RECORDING = 2;
    short STATE_SCHEDULED = 3;

    /**
     * Gets the recording id of this content.
     * @return recording id.
     */
    String getId() throws RemoteException;

    /**
     * Gets the state of this content.
     * @return state.
     * @see LeafRecordingRequest.getState.
     */
    int getRecordingRequestState() throws RemoteException;

    /**
     * Gets the state of this content.
     * @return state.
     * @see LeafRecordingRequest.getState.
     */
    short getContentState() throws RemoteException;

    /**
     * Gets the title of this content.
     * @return the title of content.
     */
    String getTitle() throws RemoteException;

    //->Kenneth[2017.8.22] R7.4 : title 의 French accents character 들을 English character 로 바꿔준다
    /**
     * Gets the english title.
     * @return the title of content transcoded to english.
     */
    String getEnglishTitle() throws RemoteException;
    //<-

    /**
     * Gets the channel name of this content.
     * @return the channel name.
     */
    String getChannelName() throws RemoteException;

    /**
     * Gets start time of this content.
     * @return start time.
     */
    long getProgramStartTime() throws RemoteException;

    /**
     * Gets end time of this content.
     * @return end time.
     */
    long getProgramEndTime() throws RemoteException;

    /**
     * Gets the channel number of this content.
     * @return the channel number.
     */
    int getChannelNumber() throws RemoteException;

    /**
     * Gets scheduled start time of this content.
     * @return start time.
     */
    long getScheduledStartTime() throws RemoteException;

    /**
     * Gets scheduled duration of this content.
     * @return duration.
     */
    long getScheduledDuration() throws RemoteException;

    /**
     * Returns the app data of this content.
     * @see RecordingRequest#getAppData()
     * @param key of app data.
     * @return value of app data.
     */
    Serializable getAppData(String key) throws RemoteException;


    String getLocation() throws RemoteException;
}

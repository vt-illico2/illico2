package com.videotron.tvi.illico.ixc.pvr;

import java.rmi.Remote;
import java.rmi.RemoteException;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.ChannelContext;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;

/**
 * This class provides PVR functionality for other applications through IXC.
 *
 * @version $Revision: 1.38.6.1 $ $Date: 2017/09/01 16:25:10 $
 * @author  June Park
 */
public interface PvrService extends Remote {

    /** Indicates the IXC name. */
    String IXC_NAME = "PvrService";

    String DATA_KEY_TSB_KEEP_ALIVE_DURATION = "Pvr.TSB_KEEP_ALIVE_DURATION";

    int MENU_PLAY_RECORDING       = -1;

    // recorded list
    int MENU_RECORDING_LIST       = 0;

    // upcoming recording list
    int MENU_SCHEDULED_RECORDINGS = 1;

    // manual recording
    int MENU_CREATE_RECORDING     = 2;

    // management
    int MENU_PVR_MANAGEMENT       = 3;

    int MENU_PREFERENCE           = 4;

    // portal
    int MENU_PVR_PORTAL           = 5;

    int MENU_WIZARD               = 6;

    String getDeviceName() throws RemoteException;

    void setDeviceName(String name) throws RemoteException;

    /**
     * Gets the recorded contents list.
     * @return recorded contents ids list.
     */
    String[] getRecordedContents() throws RemoteException;

    /**
     * Gets the all contents list.
     * @return all contents ids list.
     */
    String[] getContents() throws RemoteException;

    PvrContent getContent(String id) throws RemoteException;

    void addContentsChangedListener(ContentsChangedListener c) throws RemoteException;

    void removeContentsChangedListener(ContentsChangedListener c) throws RemoteException;

    void showMenu(int menuId) throws RemoteException;

    /**
     * Records the requested Program.
     */
    void record(TvChannel ch, TvProgram p) throws RemoteException;

    /**
     * Stop or unschedule the requested Program.
     */
    void cancel(TvProgram p) throws RemoteException;

    boolean confirmSaveBuffer(int tunerIndex, TvChannel current, TvChannel next) throws RemoteException;

    void cancelSaveBuffer(int tunerIndex) throws RemoteException;

    boolean confirmChangeChannel(int tunerIndex, TvChannel current, TvChannel next) throws RemoteException;

    void showChannelAutoTuneNotice(int tunerIndex, int recordingId) throws RemoteException;

    boolean checkTuner(int sourceId) throws RemoteException;

    boolean requestTuner() throws RemoteException;

    boolean requestTuner(int sourceId) throws RemoteException;

    /**
     * Requests a new repeat recording from OcapReady application.
     *
     * @param channel channel number to be recorded.
     * @param title title of recording.
     * @param from recording start time.
     * @param to recording end time
     * @param programStartTime start time of program to be recorded.
     *        set to 0, if the recording is recorded manually.
     * @param repeatOption 7-length string to represent repeat option.
     *        Each char represents days to be recorded. (Monday to Sunday)
     *        For example, 0000011 represents every weekends, 0010000 represents
     *        every Wednesday. And xxxxxxx represents any day & any time slots.
     * @param keepOption number of episodes to be kept.
     *        0 represents all episodes.
     * @return group key of this repeat recording.
     */
    String requestRepeatRecording(int channel, String title, long from, long to,
                long programStartTime, String repeatOption, int keepOption) throws RemoteException;

    boolean pvrPlayControl(int keyCode) throws RemoteException;

    boolean showHideFlipBar() throws RemoteException;

    //->Kenneth[2015.6.25] DDC-107
    int playContent(int id, int startPoint, String udn) throws RemoteException, IllegalStateException, IllegalArgumentException;
    //boolean playContent(int id, int startPoint, String udn) throws RemoteException, IllegalStateException, IllegalArgumentException;

    /**
     *
     */
    void setLogStateListener(LogStateListener l) throws RemoteException;

    //->Kenneth[2015.7.4] Tank
    // This method is called directly by portal application when portal application's visibility is changed.
    void portalVisibilityChanged(boolean visible) throws RemoteException;

    //->Kenneth[2017.8.31] R7.4
    void removeAllUpcomingRecordings(String callLetter) throws RemoteException;
    //<-
}

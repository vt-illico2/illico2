package com.videotron.tvi.illico.ixc.pvr;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class represents recorded contents by PVR application.
 *
 * @version $Revision: 1.6 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface ContentsChangedListener extends Remote {

    void contentsChanged(String[] contents) throws RemoteException;

    void contentsRescheduled(String[] contents) throws RemoteException;

}


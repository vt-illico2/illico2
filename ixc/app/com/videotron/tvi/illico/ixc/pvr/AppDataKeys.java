package com.videotron.tvi.illico.ixc.pvr;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.io.Serializable;

/**
 * This class contains AppData keys.
 *
 * @version $Revision: 1.35.6.1 $ $Date: 2017/08/23 20:06:31 $
 * @author  June Park
 */
public interface AppDataKeys extends Remote {

    // creation methods
    int BY_USER = 1;
    int BY_REMOTE_REQUEST = 2;
    int NEW_SERIES = 4;
    int NEW_EPISODE_FROM_EPG_DATA = 5;
    int NEW_TIMESLOT_BY_CONFLICT = 6;

    /** Channel number. (Integer) */
    String CHANNEL_NUMBER       = "channel_number";
    /** Channel name. (String) */
    String CHANNEL_NAME         = "channel_name";
    /** Source ID. (Integer) */
    String SOURCE_ID            = "source_id";

    /** Program start time. (Long: time stamp) */
    String PROGRAM_START_TIME   = "program_start_time";
    /** Program end time. (Long: time stamp) */
    String PROGRAM_END_TIME     = "program_end_time";

    /** creation time. (Long: time stamp) */
    String CREATION_TIME        = "creation_time";
    /** creation method. (Integer) */
    String CREATION_METHOD        = "creation_method";

    /** old start time - only for the rescheduled recording. (Long: time stamp) */
    /**
     * Recording end time. (Long: time stamp)
     * Note: recording start time can be gotten from RecordedService.
     */
//    String RECORDING_END_TIME     = "recording_end_time";

    /** Title. (String) */
    String TITLE                = "title";

    //->Kenneth[2017.8.22] R7.4 : Search 에서 사용할 keyword 저장
    String ENGLISH_TITLE        = "english_title";
    //<-

    /** Program rating. (String: G, 18+) */
    String PARENTAL_RATING      = "parental_rating";
    /** Program category. (Integer: 0 - 36) */
    String CATEGORY             = "category";
    /** Program subcategory. (Integer: 0 - 36) */
    String SUBCATEGORY          = "subcategory";
    /** Program definition. (Integer: 0=sd, 1=hd) */
    String DEFINITION           = "definition";
    /** Program language. (String: fr, en) */
    String LANGUAGE             = "language";
    /** Program unique ID. (String) */
    String PROGRAM_ID           = "program_id";
    /** Program type. (Integer) */
    String PROGRAM_TYPE         = "program_type";

    /** Series ID. (String) */
    String SERIES_ID            = "series_id";
    /** Episode Title. (String) */
    String EPISODE_TITLE           = "episode_title";
    /** Full description. (String) */
    String FULL_DESCRIPTION     = "full_description";
    /** Production Year. (String) */
    String PRODUCTION_YEAR     = "production_year";

    /** Block. (Boolean) */
    String BLOCK                = "block";
    /** Unset option. (Boolean) */
    String UNSET               = "unset";
//    /** media time. (Long) */
//    String MEDIA_TIME          = "media_time";
    /** view count. (Integer) */
    String VIEW_COUNT          = "view_count";

    /** Star rating. (Integer: 0 - 7) */
    String STAR_RATING          = "star_rating";
    /** Star rating. (Boolean) */
    String CLOSED_CAPTION       = "closed_caption";
    /** Audio type. (Integer) */
    String AUDIO_TYPE           = "audio_type";
    /** SAP. (Boolean) */
    String SAP                  = "sap";
    /** LIVE. (Boolean) */
    String LIVE                 = "live";

    /** EID of PPV program. 0 for the non-PPV program. (Long) */
    String PPV_EID                 = "ppv_eid";

    /**
     * Group key. (String).
     * If this metadata is set, the recording is child of group recording.
     * For the series, this value should be same as SERIES_ID,
     * For the forces series, this value shoule be CALL_LETTER.PROGRAM_ID,
     * For the repeated manual, this value shoule be CALL_LETTER.START_TIME
     * For the single recording, this value should not set.
     */
    String GROUP_KEY            = "group_key";

    /**
     * Failed reason. (Integer).
     * When application stops IN_PROGRESS_WITH_ERROR_STATE, it would have
     * USER_STOP as a failed reason.
     */
    String FAILED_REASON        = "failed_reason";

    /** suppress conflict. (Boolean) */
    String SUPPRESS_CONFLICT    = "suppres_conflict";

    /** Only for the SDV recordings. (Integer) */
    String SDV_STATUS      = "sdv_status";

    /** Written by M/W (Integer) */
    String NI_INDEX             = "NI index";

    /** Only for the remote recordings. (Long[]) */
    String SEGMENT_TIMES        = "segment_times";

    /** Only for the remote recordings. (Long) */
    String SCHEDULED_DURATION        = "scheduled_duration";
    /** Only for the remote recordings. (Long) */
    String SCHEDULED_START_TIME      = "scheduled_start_time";
    /** Only for the remote recordings. (Integer) */
    String KEEP_COUNT                = "keep_count";
    /** Only for the remote recordings. (Integer) */
    String LOCAL_ID                  = "local_id";

    /** DDC-001. (Integer) */
    String YEAR_SHIFT = "year_shift";
}

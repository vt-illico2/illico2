package com.videotron.tvi.illico.ixc.pvr;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class represents recorded contents by PVR application.
 *
 * @version $Revision: 1.5 $ $Date: 2017/01/12 19:28:39 $
 * @author  June Park
 */
public interface RecordedContent extends PvrContent {

    /**
     * Gets the time when the recording was initiated.
     * @see RecordedService#getRecordingStartTime()
     * @return the time when the recording was initiated by the implementation.
     */
    long getRecordingStartTime() throws RemoteException;

    /**
     * Gets the actual duration of the content recorded.
     * @see RecordedService#getRecordedDuration()
     * @return The duration of the recording in milli-seconds.
     */
    long getDuration() throws RemoteException;

}

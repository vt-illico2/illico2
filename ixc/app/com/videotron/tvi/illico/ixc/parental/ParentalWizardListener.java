package com.videotron.tvi.illico.ixc.parental;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ParentalWizardListener extends Remote  {
    void requestCompleted() throws RemoteException;
    void requestCanceled() throws RemoteException;
}

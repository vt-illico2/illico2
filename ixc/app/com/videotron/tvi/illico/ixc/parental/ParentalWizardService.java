package com.videotron.tvi.illico.ixc.parental;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ParentalWizardService extends Remote {
    /** Indicates the IXC name. */
    String IXC_NAME = "ParentalWizardService";
    
    void showParentalWizard(ParentalWizardListener listener)throws RemoteException;
    void hideParentalWizard()throws RemoteException;
}

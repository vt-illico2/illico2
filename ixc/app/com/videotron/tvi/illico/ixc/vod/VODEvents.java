package com.videotron.tvi.illico.ixc.vod;

import java.rmi.Remote;

public interface VODEvents extends Remote {

    public static final int SETUP_SUCCEEDED = 0x41;
    public static final int SETUP_FAILED = 0x42;
    public static final int SETUP_TIMEDOUT = 0x43;
    public static final int RELEASE_SUCCEEDED = 0x44;
    public static final int RELEASE_FAILED = 0x48;
    public static final int RELEASE_TIMEDOUT = 0x49;
    public static final int RELEASE_REQUIRED = 0x50;
    public static final int SERVICEGROUP_SUCCEEDED = 0x60;
    public static final int SERVICEGROUP_FAILED = 0x70;
    public static final int SERVICEGROUP_CHANGED = 0x71;
    public static final int NO_CABLE_CARD_MAC = 0x72;
    
    public static final int STOP = 0;
    public static final int PAUSE = 3;
    public static final int PLAY = 1;
    public static final int RESUME = 5;
    public static final int PLAY_SLOW = 6;
    public static final int STATUS = 98;
    // REW_FF
//    public static final int REW_SCALE_1 = -4;
//    public static final int REW_SCALE_2 = -16;
//    public static final int REW_SCALE_3 = -64;
//    public static final int REW_SCALE_4 = -128;
//    public static final int FF_SCALE_1 = 4;
//    public static final int FF_SCALE_2 = 16;
//    public static final int FF_SCALE_3 = 64;
//    public static final int FF_SCALE_4 = 128;
    public static final int REW_SCALE_1 = -2;
    public static final int REW_SCALE_2 = -4;
    public static final int REW_SCALE_3 = -8;
    public static final int REW_SCALE_4 = -16;
    public static final int FF_SCALE_1 = 2;
    public static final int FF_SCALE_2 = 4;
    public static final int FF_SCALE_3 = 8;
    public static final int FF_SCALE_4 = 16;
    public static final int END_OF_STREAM = 99;
    
}

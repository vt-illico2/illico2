package com.videotron.tvi.illico.ixc.vod;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface VideoController extends Remote {
	
    void sessionSetup(String s)  throws RemoteException;

    void play(String s, int i) throws RemoteException;

    void resume(String s, int i) throws RemoteException;

    void jump(String s, int i) throws RemoteException;

    void pause(String s) throws RemoteException;

    void stop(String s) throws RemoteException;   
}

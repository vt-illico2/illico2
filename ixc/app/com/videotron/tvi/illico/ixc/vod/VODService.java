package com.videotron.tvi.illico.ixc.vod;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.companion.LogStateListener;


/**
 * The Interface VODService.
 */
public interface VODService extends Remote {
	/** Watching a VOD. */
	public static final int APP_WATCHING = 2;
	/** VOD application is running. */
	public static final int APP_STARTED = 1;
	/** VOD application is not running. */
	public static final int APP_PAUSED = 0;
	/** VOD application will be paused soon. */
	public static final int APP_PAUSING = -1;

    /** Indicates the IXC name. */
    String IXC_NAME = "VODService";
    
    /**
     * SharedMemory key for SVOD list.
     */
    String KEY_SVOD_LIST = "SVOD_LIST";

    /**
     * Gets the video controller.
     *
     * @return the video controller
     * @throws RemoteException the remote exception
     */
    VideoController getVideoController() throws RemoteException;

    /**
     * Adds the vod service listener.
     *
     * @param listener the listener
     * @throws RemoteException the remote exception
     */
    void addVODServiceListener (VODServiceListener listener) throws RemoteException;

    /**
     * Removes the vod service listener.
     *
     * @param listener the listener
     * @throws RemoteException the remote exception
     */
    void removeVODServiceListener (VODServiceListener listener) throws RemoteException;

    /**
     * Link to vod.
     *
     * @param CategoryID the category id
     * @param sspID the ssp id
     * @throws RemoteException the remote exception
     */
    void linkToVOD(String CategoryID, String sspID) throws RemoteException;

    /**
     * Gets the VOD state.
     *
     * @return the VOD state
     * @throws RemoteException the remote exception
     */
    int getVODState() throws RemoteException;

    /**
     * Start auto discovery.
     *
     * @throws RemoteException the remote exception
     */
    void startAutoDiscovery() throws RemoteException;

    /**
     * Plays requested video on popup style GUI.
     * @param sspId	video session name to play
     * @param runtimeInSec runtime
     * @param dispTitle title name to display
     * @throws RemoteException the remote exception
     */
    void showPopupVideo(String sspId, int runtimeInSec, String dispTitle) throws RemoteException;

    /**
     * Request to stop the playing.
     * @throws RemoteException
     */
    void requestToStop() throws RemoteException;

    /**
     * Request "play" the current playing content.
     * If not playing, it will be ignored.
     * @throws RemoteException
     */
    void vodPlay() throws RemoteException;

    /**
     * Request "pause" the current playing content.
     * If not playing, it will be ignored.
     * @throws RemoteException
     */
    void vodPause() throws RemoteException;

    /**
     * Request "stop" the current playing content.
     * If not playing, it will be ignored.
     * @throws RemoteException
     */
    void vodStop() throws RemoteException;

    /**
     * Request "fast forward" the current playing content.
     * It's speed depends on current speed.
     * If not playing, it will be ignored.
     * @throws RemoteException
     */
    void vodFF() throws RemoteException;

    /**
     * Request "rewind" the current playing content.
     * It's speed depends on current speed.
     * If not playing, it will be ignored.
     * @throws RemoteException
     */
    void vodRWD() throws RemoteException;

    /**
     * Request "skip forward" the current playing content.
     * If not playing or reach the end section, it will be ignored.
     * @throws RemoteException
     */
    void vodSkipForward() throws RemoteException;

    /**
     * Request "skip backward" the current playing content.
     * If not playing or reach the first section, it will be ignored.
     * @throws RemoteException
     */
    void vodSkipBackward() throws RemoteException;

    /**
     * Request "skip section" the current playing content.
     * If not playing, it will be ignored.
     * @throws RemoteException
     */
    void vodSkipSection(int section) throws RemoteException;
    /**
     * Request "trick mode control" the current playing content.
     * If not playing, it will be ignored.
     * @param code key code to transfer, it shall be in OCRcEvent.VK_PLAY, VK_PAUSE, VK_STOP, VK_FORWARD, VK_BACK, VK_FAST_FWD, VK_REWIND and VK_EXIT
     * @throws RemoteException
     */
    boolean vodPlayControl(int code) throws RemoteException;

    boolean showHideFlipBar() throws RemoteException;

    /**
     * Request "play next content" the current playing content.
     * If not playing, it will be ignored.
     * @throws RemoteException
     */
    void vodNextContent() throws RemoteException;

    int showContent(String contentId) throws RemoteException;
    int playContent(String[] contentId, boolean[] trailer, String[] lang, String[] definition) throws RemoteException;

    void setLogStateListener(LogStateListener l) throws RemoteException;

    void showCannotIcon() throws RemoteException;

    //->Kenneth[2015.7.4] Tank
    // This method is called directly by portal application when portal application's visibility is changed.
    void portalVisibilityChanged(boolean visible) throws RemoteException;
}

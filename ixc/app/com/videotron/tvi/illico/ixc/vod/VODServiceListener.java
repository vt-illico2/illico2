package com.videotron.tvi.illico.ixc.vod;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface VODServiceListener extends Remote {

    void receiveVODEvent(int event) throws RemoteException;
}

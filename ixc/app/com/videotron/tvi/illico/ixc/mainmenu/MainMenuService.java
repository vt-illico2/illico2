package com.videotron.tvi.illico.ixc.mainmenu;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * A sample processor to check naming conventions are being followed.
 *
 * <h3>How to run this processor from the command line</h3>
 * <ol>
 * <li> Compile this file; for example<br>
 *      {@code javac -d procdir CheckNamesProcessor.java}
 * <li> Use {@code javac} to run the annotation processor on itself:<br>
 *      {@code javac -processorpath procdir -processor CheckNamesProcessor -proc:only CheckNamesProcessor.java}
 * </ol>
 *
 * <h3>Another way to run this processor from the command line</h3>
 * <ol>
 * <li> Compile the processor as before
 *
 * <li> Create a UTF-8 encoded text file named {@code
 * javax.annotation.processing.Processor} in the {@code
 * META-INF/services} directory.  The contents of the file are a list
 * of the binary names of the concrete processor classes, one per
 * line.  This provider-configuration file is used by {@linkplain
 * java.util.ServiceLoader service-loader} style lookup.
 *
 * <li> Create a {@code jar} file with the processor classes and
 * {@code META-INF} information.
 *
 * <li> Such a {@code jar} file can now be used with the <i>discovery
 * process</i> without explicitly naming the processor to run:<br>
 * {@code javac -processorpath procdir -proc:only CheckNamesProcessor.java}
 *
 * </ol>
 * 
 * For some notes on how to run an annotation processor inside
 * NetBeans, see http://wiki.java.net/bin/view/Netbeans/FaqApt.
 *
 * <h3>Possible Enhancements</h3>
 * <ul>
 *
 * <li> Support an annotation processor option to control checking
 * exported API elements ({@code public} and {@code protected} ones)
 * or all elements
 *
 * <li> Print out warnings that are more informative
 *
 * <li> Return a true/false status if any warnings were printed or
 * compute and return name warning count
 *
 * <li> Implement checks of package names
 *
 * <li> Use the Tree API, com.sun.source, to examine names within method bodies
 *
 * <li> Define an annotation type whose presence can indicate a
 * different naming convention is being followed
 *
 * <li> Implement customized checks on elements in chosen packages
 *
 * </ul>
 *
 * @author Joseph D. Darcy
 */
public interface MainMenuService extends Remote {
    /** Indicates the IXC name. */
    String IXC_NAME = "MainMenuService";
    
    String BRAND_IMAGE_KEY = "SMKEY_BRAND_IMAGE";
    
    /** Close All MainMenu UI. **/
    int MAIN_MENU_CLOSE_ALL = 0;
    /** Management System that sets the value. */
    int MAIN_MENU_STATE_MANAGEMENT_SYSTEM_VALUE = 1;
    /** Indicates the service state that is a mini menu state. */
    int MAIN_MENU_STATE_MINI_MENU = 2;
    /** Indicates the service state that is full menu state. */
//    int MAIN_MENU_STATE_FULL_MENU = 3;
    /** Indicates the service state that is dash board state. */
    int MAIN_MENU_STATE_DASHBOARD = 4;

    /**
     * requests to show main menu.
     * 
     * @param mainMenuState the state of the main menu
     * @param parameters Specific Parameters.
     * @throws RemoteException the remote exception
     */
    void showMenu(int mainMenuState, String[] parameters) throws RemoteException;

    /**
     * resumes menu.
     * 
     * @throws RemoteException the remote exception
     */
    void resumeMenu() throws RemoteException;

    /**
     * hides menu.
     * 
     * @throws RemoteException the remote exception
     * @return
     */
    void hideMenu() throws RemoteException;

    /**
     * gets target menu state.
     * 
     * @return int
     * @throws RemoteException the remote exception
     */
    int getTargetMenuState() throws RemoteException;
    
    
    /**
     * whether settop box is activated or not.
     * 
     * @param isDeactivatedSTB true - deactivated settop, false - activated settop
     * @throws RemoteException the remote exception
     */
    void setDeactivatedSTB(boolean isDeactivatedSTB) throws RemoteException;
}

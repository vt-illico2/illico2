package com.videotron.tvi.illico.ixc.loadinganimation;

import java.awt.Point;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LoadingAnimationService extends Remote{
    /** Indicates the IXC name. */
    public static final String IXC_NAME = "LoadingAnimationService";

    /**
     * Shows the LoadingAnimation.
     * @throws RemoteException the exception.
     */
    public void showLoadingAnimation(Point p) throws RemoteException;
    
    /**
     * Shows the LoadingAnimation without delay.
     * @throws RemoteException the exception.
     */
    public void showNotDelayLoadingAnimation(Point p) throws RemoteException;

    /**
     * Hides the LoadingAnimation.
     * @throws RemoteException the exception.
     */
    public void hideLoadingAnimation() throws RemoteException;
    
    public void isKeyMoving(boolean flag) throws RemoteException;
    
}

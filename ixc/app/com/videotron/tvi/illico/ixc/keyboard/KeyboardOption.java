/**
 * @(#)KeyboardOption.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.keyboard;

import java.awt.Point;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class is a interface to use when create a Keyboard. The application to create Keyboard should implements this
 * class and use it when create Keyboard.
 *
 * @see KeyboardService#showPopupKeyboard(KeyboardOption, KeyboardListener)
 * @author Woojung Kim
 * @version 1.1
 */
public interface KeyboardOption extends Remote {

    /** The type of Standard Keyboard(Virtual Keyboard). */
    int TYPE_VIRTUAL = 0;
    /** The type of Extended Keyboard. */
    int TYPE_VIRTUAL_EXTENDED = 1;
    /** The type of Multi-Tab Keyboard. */
    int TYPE_MULTI_TAP = 2;
    /** The type of Extended and Embedded. */
    int TYPE_EMBEDDED_EXTENDED = 3;
    /** The type of default. */
    int TYPE_DEFAULT = 4;
    /** The type of default. */
    int TYPE_ALPHANUMERIC = 5;
    /** The mode of Popup style. */
    short MODE_POPUP = 0;
    /** The mode of Embedded style. */
    short MODE_EMBEDDED = 1;
    
    /** The mode of ALPHANUMERIC style. */
    int ALPHANUMERIC_ALL = 0;
    /** The mode of ALPHANUMERIC style. */
    int ALPHANUMERIC_SAMLL = 1;
    /** The mode of ALPHANUMERIC style. */
    int ALPHANUMERIC_LARGE = 2;
    /** The mode of ALPHANUMERIC style. */
    int ALPHANUMERIC_ACCENT = 3;
    
    /**
     * Get the default text for Keyboard.
     *
     * @return The default text
     * @throws RemoteException
     *             The remote exception
     */
    String getDefaultText() throws RemoteException;
    
    /**
     * Get the default text for Keyboard.
     * 
     * @return  The temp text viewed  
     * @throws RemoteException
     */
    boolean getTempDefaultText() throws RemoteException;

    /**
     * Get the input pattern for Keyboard.
     *
     * @return The input pattern
     * @throws RemoteException
     *             The remote exception
     */
    String getInputPattern() throws RemoteException;

    /**
     * Get the Max length to input a characters.
     *
     * @return The max length
     * @throws RemoteException
     *             The remote exception
     */
    int getMaxChars() throws RemoteException;

    /**
     * Get the echo character for Keyboard.
     *
     * @return The echo character
     * @throws RemoteException
     *             The remote exception
     */
    char getEchoChars() throws RemoteException;

    /**
     * Return true if want to close keyboard to reach a max length.
     *
     * @return true if reach a max length to input, false otherwise.
     * @throws RemoteException
     *             The remote exception
     */
    boolean isClosedWithMaxInput() throws RemoteException;

    /**
     * Get a type of Keyboard.
     *
     * @return a one of {@link #TYPE_ALPHANUMERIC}
     * @throws RemoteException
     *             The remote exception
     */
    int getType() throws RemoteException;

    /**
     * Get a type of Keyboard.
     *
     * @return The one of {@link #MODE_POPUP} and {@link #MODE_EMBEDDED}
     * @throws RemoteException
     *             The remote exception
     */
    short getMode() throws RemoteException;
    
    /**
     * Get a position to paint a keyboard.
     * @return
     * @throws RemoteException
     */
    Point getPosition() throws RemoteException;
    
    /**
     * Gets a title.
     * @return
     * @throws RemoteException
     */
    String getTitle() throws RemoteException;

    /**
     * Gets a Alphanumeric
     * @return The one of {@link #ALPHANUMERIC_ALL} and
     *  {@link #ALPHANUMERIC_SMALL} and
     *  {@link #ALPHANUMERIC_LARGE} and {@link #ALPHANUMERIC_ACCENT}
     * 
     */
    int getAlphanumericType() throws RemoteException;
    
    /**
     * Deactivate changing mode as "A" key.
     * The A key is changed the keyboard pad mode.
     * This option method is only adapted by popup keyboard.
     * false - activate changing mode as "A" kay.
     * true - Deactivate changing mode as "A" kay.
     * default is false.
     * @return
     * @throws RemoteException
     */
    boolean isKeyboardChangedDeactivate() throws RemoteException;
}

/**
 * @(#)KeyboardListener.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.keyboard;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The listener interface for receiving keyboard events. The class that is interested in processing a keyboard event
 * implements this interface, and the object created with that class is registered with a component using the
 * component's <code>addKeyboardListener</code> method. When the keyboard event occurs, that object's appropriate
 * method is invoked.
 * @author Woojung Kim
 * @version 1.1
 */
public interface KeyboardListener extends Remote {

    /**
     * NotifyInput ended.
     * @param text the text
     * @throws RemoteException the remote exception
     */
    void inputEnded(String text) throws RemoteException;

    /**
     * Input canceled.
     * @param text the text
     * @throws RemoteException the remote exception
     */
    void inputCanceled(String text) throws RemoteException;

    /**
     * Cursor moved.
     * @param position the position
     * @throws RemoteException the remote exception
     */
    void cursorMoved(int position) throws RemoteException;

    /**
     * In the case of embedded mode, the caller application shall know to update the text of inputed via Virtual
     * Keyboard. Virtual Keyboard shall notify to update text to caller application.
     * @param ext updated text.
     * @throws RemoteException the remote exception.
     */
    void textChanged(String ext) throws RemoteException;

    /**
     * When keyboard lost focus, let application know the focus.
     * @param direction focus direction
     * @throws RemoteException the remote exception.
     */
    void focusOut(int direction) throws RemoteException;

    /**
     * Notify mode changed.
     * @param mode keyboard mode
     * @throws RemoteException the remote exception.
     */
    void modeChanged(int mode) throws RemoteException;
    /**
     * Notify input text cleared.
     */
    void inputCleared() throws RemoteException;
}

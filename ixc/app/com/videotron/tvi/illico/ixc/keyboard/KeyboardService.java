/**
 * @(#)KeyboardService.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.ixc.keyboard;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * KeyboardService class is a interface to communication with Keyboard application.
 * @author Woojung Kim
 * @version 1.1
 */
public interface KeyboardService extends Remote {

    /** The IXC name of Keyboard application. */
    String IXC_NAME = "Keyboard";

    /**
     * Request to show a keyboard by using KeyboardOption.
     * @param o The {@link KeyboardOption} to use when create keyboard.
     * @param l The {@link KeyboardListerner} to receive a keyboard events.
     * @throws RemoteException The remote exception
     */
    void showPopupKeyboard(KeyboardOption o, KeyboardListener l) throws RemoteException;

    /**
     * Request to hide a keyboard and remove a registered {@link KeyboardListener}.
     * @param l The listener to receive a keyboard events.
     * @throws RemoteException The remote exception
     */
    void hidePopupKeyboard(KeyboardListener l) throws RemoteException;
}

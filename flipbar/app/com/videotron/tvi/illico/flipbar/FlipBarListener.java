package com.videotron.tvi.illico.flipbar;

public interface FlipBarListener {

    void actionPerformed(FlipBarAction action);

    void notifyShown(long time);

    void notifyHidden(boolean selection);

}



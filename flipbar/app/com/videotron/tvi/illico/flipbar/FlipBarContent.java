package com.videotron.tvi.illico.flipbar;

import java.util.Date;
import com.videotron.tvi.illico.util.Constants;

public class FlipBarContent {

    private String name;
    private long startTime;
    private long endTime;
    private int definition;
    private String rating;

    private Date startDate;
    private Date endDate;

    private Object data;

    public FlipBarContent(String name, long startTime, long endTime) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.startDate = new Date(startTime);
        this.endDate = new Date(endTime);
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setResolution(boolean hd) {
        setDefinition(hd ? 1 : 0);
    }

    public void setDefinition(int definition) {
        this.definition = definition;
    }

    public void setData(Object contentData) {
        this.data = contentData;
    }

    public Object getData() {
        return data;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
        this.endDate = new Date(endTime);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public boolean isHd() {
        return definition >= Constants.DEFINITION_HD;
    }

    public int getDefinition() {
        return definition;
    }

    public String getRating() {
        return rating;
    }

}



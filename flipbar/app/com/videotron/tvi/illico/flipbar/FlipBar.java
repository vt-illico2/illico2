package com.videotron.tvi.illico.flipbar;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.EventManager;
import org.dvb.event.UserEvent;
import org.dvb.event.UserEventListener;
import org.dvb.event.UserEventRepository;
import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBColor;
import org.dvb.ui.DVBGraphics;

import com.videotron.tvi.illico.flipbar.FlipBarAction;
import com.videotron.tvi.illico.flipbar.FlipBarContent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class is the FlipBar UI class.
 *
 * @version $Revision: 1.52 $ $Date: 2017/01/12 19:18:40 $
 * @author  June Park
 */
public class FlipBar extends UIComponent implements UserEventListener, TVTimerWentOffListener {
    private static final long serialVersionUID = 7152091012442L;

    private DataCenter dataCenter = DataCenter.getInstance();

    public static final String MODE_VOD = "VoD";
    public static final String MODE_PVR = "PVR";
    public static final String MODE_TV  = "TV";
    public static final String MODE_PPV = "PPV";
    public static final String MODE_TIMESHIFT = "TimeShift";
    public static final String MODE_KARAOKE = "Karaoke";
    public static final String MODE_PCS_VIDEO = "VIDEO";
    public static final String MODE_PCS_PHOTO = "PHOTO";
    public static final String MODE_PCS_PHOTO_SLIDE = "PHOTO_SLIDE";
    public static final String MODE_PCS_PHOTO_ZOOM = "ZOOM";
    public static final String MODE_PCS_MUSIC = "MUSIC";
    public static final String MODE_PCS_GALAXIE = "GALAXIE";

    private Vector listeners = new Vector(2);

    private FlipBarContent[] contents;

    private FlipBarAction[] actions;
    private boolean[] disabled;

    private int contentIndex = 0;

    private String mode;
    private boolean skippable = false;
    private boolean absolute = false;
    //pcs
    private boolean isAlpha = false;

    private long[] indicationTimes;

    private long mediaTime;
    private long bufferEndTime;
    private long bufferStartTime;

    private UserEventRepository eventRepository;

    private static final int SCROLL_DELAY = 20;
    private TVTimerSpec scrollTimer;
    private int scrollCounter;
    private Rectangle scollBounds = new Rectangle(158, 103, 650, 18);

    private ClickingEffect clickEffect;
    private int btnX, btnY, btnW, btnH;

    public FlipBar(final String modeIndicator) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : Contructor("+modeIndicator+")");
        this.mode = modeIndicator;
        setRenderer(new FlipBarRenderer());
        eventRepository = new UserEventRepository("FlipBar.Keys");
        eventRepository.addKey(KeyEvent.VK_ENTER);
        eventRepository.addAllArrowKeys();
        setVisible(false);

        clickEffect = new ClickingEffect(this, 5);
    }

    public void setMode(String modeIndicator) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setMode("+modeIndicator+")");
        this.mode = modeIndicator;
        repaint();
    }

    public void setDisplayActions(FlipBarAction[] flipBarActions) {
        if (actions != null) {
            FlipBarAction[] oldActions = this.actions;
            boolean[] oldDisabled = this.disabled;
            this.actions = flipBarActions;
            this.disabled = new boolean[flipBarActions.length];
            for (int i = 0; i < oldActions.length; i++) {
                setEnabled(oldActions[i], !oldDisabled[i]);
            }
        } else {
            this.actions = flipBarActions;
            this.disabled = new boolean[flipBarActions.length];
        }
        ((FlipBarRenderer) renderer).readyActionImages();
    }

    //for PCS
    public void setAction(FlipBarAction flipBarAction, int actionFocus) {
        if (actions == null || actionFocus < 0 || actionFocus >= actions.length) { return; }
        actions[actionFocus] = flipBarAction;
        ((FlipBarRenderer) renderer).readyActionImages();
    }

    public void setAbsolute(boolean absolute) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setAbsolute("+absolute+")");
        this.absolute = absolute;
    }

    public void setContents(FlipBarContent[] flipBarContents) {
        this.contents = flipBarContents;
        this.contentIndex = 0;
    }

    public void setContent(FlipBarContent flipBarContent) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setContent("+flipBarContent+")");
        setContents(new FlipBarContent[] { flipBarContent });
    }

    public FlipBarContent[] getContents() {
        return contents;
    }

    public FlipBarContent getCurrentContent() {
        int ci = contentIndex;
        if (contents != null && ci >= 0 && ci < contents.length) {
            return contents[ci];
        } else {
            return null;
        }
    }

    public void setFocus(FlipBarAction action) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setFocus("+action+")");
        int index = getActionIndex(action);
        if (index >= 0 && !disabled[index]) {
            setFocus(index);
        }
    }

    public void setEnabled(FlipBarAction action, boolean enable) {
        int index = getActionIndex(action);
        if (index >= 0) {
            setEnabled(index, enable);
        }
    }

    public void setEnabled(int index, boolean enable) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setEnable("+index+", "+enable+")");
        disabled[index] = !enable;
        if (!enable && focus == index) {
            if (focus >= actions.length - 1) {
                handleKey(KeyEvent.VK_LEFT);
            } else {
                handleKey(KeyEvent.VK_RIGHT);
            }
        }
    }

    private int getActionIndex(FlipBarAction action) {
        if (actions != null) {
            for (int i = 0; i < actions.length; i++) {
                if (actions[i] == action) {
                    return i;
                }
            }
        }
        return -1;
    }

    public void setSkippable(boolean flag) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setSkippable("+flag+")");
        this.skippable = flag;
    }

    public boolean isSkippable() {
        return skippable;
    }

    public void setIndicationTimes(long[] times) {
        this.indicationTimes = times;
    }

    public void setMediaTime(long time) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setMediaTime("+time+")");
        this.mediaTime = time;
        updateContentIndex();
        repaint();
    }

    public void setBufferTime(long start, long end) {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : setBufferTime("+start+", "+end+")");
        this.bufferStartTime = start;
        this.bufferEndTime = end;
        repaint();
    }

    /**
     * @return the isAlpha for PCS
     */
    public boolean isAlpha() {
        return isAlpha;
    }

    /**
     * @param isAlphaValue the isAlpha to set for PCS
     */
    public void setAlpha(boolean isAlphaValue) {
        this.isAlpha = isAlphaValue;
    }

    private void updateContentIndex() {
        if (contents == null || contentIndex >= contents.length) {
            return;
        }
        FlipBarContent con = contents[contentIndex];
        if (mediaTime >= con.getEndTime()) {
            // move right
            while ((contentIndex + 1) < contents.length && mediaTime >= contents[++contentIndex].getEndTime());
        } else if (mediaTime < con.getStartTime()) {
            // move left
            while (contentIndex > 0 && mediaTime < contents[--contentIndex].getStartTime());
        }
    }

    public void addListener(FlipBarListener l) {
        synchronized (listeners) {
            if (!listeners.contains(l)) {
                listeners.addElement(l);
            }
        }
    }

    public void removeListener(FlipBarListener l) {
        synchronized (listeners) {
            listeners.removeElement(l);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent evt) {
    	scrollCounter++;
    	if (scrollCounter > SCROLL_DELAY) {
    		repaint(scollBounds.x, scollBounds.y, scollBounds.width, scollBounds.height);
    	}
    }

    public void resetScroll() {
    	scrollCounter = 0;
    }

    public void setupTimer(boolean turnOn) {
    	if (turnOn) {
    		if (scrollTimer == null) {
    			scrollTimer = new TVTimerSpec();
    			scrollTimer.addTVTimerWentOffListener(this);
    			scrollTimer.setDelayTime(100);
    			scrollTimer.setRepeat(true);
    			scrollTimer.setRegular(true);
    			scrollCounter = 0;
    			try {
    				scrollTimer = TVTimer.getTimer().scheduleTimerSpec(scrollTimer);
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    	} else {
    		if (scrollTimer != null) {
    			TVTimer.getTimer().deschedule(scrollTimer);
    			scrollTimer.removeTVTimerWentOffListener(this);
    			scrollTimer = null;
    		}
    	}
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : start()");
        super.start();
        setVisible(true);
        EventManager em = EventManager.getInstance();
        em.addUserEventListener(this, eventRepository);
        setupTimer(true);
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : stop()");
        super.start();
    	setupTimer(false);
    	EventManager em = EventManager.getInstance();
        em.removeUserEventListener(this);
        setVisible(false);
    }

    public void userEventReceived(UserEvent event) {
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return;
        }
        handleKey(event.getCode());
    }

    public boolean handleKey(int code) {
        switch (code) {
            case KeyEvent.VK_LEFT:
                if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : handleKey(VK_LEFT)");
                if (focus > 0) {
                    int i = focus - 1;
                    while (i >= 0 && disabled[i]) {
                    	i--;
                    }
                    if (i >= 0) {
                        focus = i;
                    }
                    repaint();
                }
                return true;
            case KeyEvent.VK_RIGHT:
                if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : handleKey(VK_RIGHT)");
                if (focus < actions.length - 1) {
                    int i = focus + 1;
                    while (i < actions.length && disabled[i]) {
                        i++;
                    }
                    if (i < actions.length) {
                        focus = i;
                    }
                    repaint();
                }
                return true;
            case KeyEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : handleKey(VK_ENTER)");
            	clickEffect.start(btnX, btnY, btnW, btnH);
            	if (actions[focus] == FlipBarAction.REPEAT_ONCE) {
            		actions[focus] = FlipBarAction.REPEAT_ALL;
            		((FlipBarRenderer) renderer).readyActionImages();
            		renderer.prepare(this);
            		repaint();
            	} else if (actions[focus] == FlipBarAction.REPEAT_ALL) {
            		actions[focus] = FlipBarAction.REPEAT_ONE;
            		((FlipBarRenderer) renderer).readyActionImages();
            		renderer.prepare(this);
            		repaint();
            	} else if (actions[focus] == FlipBarAction.REPEAT_ONE) {
            		actions[focus] = FlipBarAction.REPEAT_ONCE;
            		((FlipBarRenderer) renderer).readyActionImages();
            		renderer.prepare(this);
            		repaint();
            	}
            	//pcs
            	if (MODE_PCS_VIDEO.equals(mode)) {
            	    if (actions[focus] == FlipBarAction.SCREEN_FULL) {
            	        actions[focus] = FlipBarAction.SCREEN_SMALL;
            	        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
            	    } else if (actions[focus] == FlipBarAction.SCREEN_SMALL) {
            	        actions[focus] = FlipBarAction.SCREEN_FULL;
            	        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
            	    }
            	} else if (MODE_PCS_PHOTO.equals(mode) || MODE_PCS_MUSIC.equals(mode)
            	        || MODE_PCS_GALAXIE.equals(mode)) {
            	    if (actions[focus] == FlipBarAction.RANDOM_OFF) {
                        actions[focus] = FlipBarAction.RANDOM_ON;
                        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
                    } else if (actions[focus] == FlipBarAction.RANDOM_ON) {
                        actions[focus] = FlipBarAction.RANDOM_OFF;
                        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
                    } else if (actions[focus] == FlipBarAction.REPEAT_OFF) {
                        actions[focus] = FlipBarAction.REPEAT_ON;
                        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
                    } else if (actions[focus] == FlipBarAction.REPEAT_ON) {
                        actions[focus] = FlipBarAction.REPEAT_OFF;
                        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
                    //} else if (MODE_PCS_MUSIC.equals(mode) || MODE_PCS_GALAXIE.equals(mode)) {
                    } else if (actions[focus] == FlipBarAction.PLAY) {
                        actions[focus] = FlipBarAction.PAUSE;
                        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
                    } else if (actions[focus] == FlipBarAction.PAUSE) {
                        actions[focus] = FlipBarAction.PLAY;
                        ((FlipBarRenderer) renderer).readyActionImages();
                        renderer.prepare(this);
                        repaint();
                    }
                    //}
            	}
                synchronized (listeners) {
                    int size = listeners.size();
                    for (int i = 0; i < size; i++) {
                        ((FlipBarListener) listeners.elementAt(i)).actionPerformed(actions[focus]);
                    }
                }
                return true;
        }
        return false;
    }

    private static final int BUTTON_X = 204;
    private static final int BUTTON_X_K = BUTTON_X + 32;
    private static final int BUTTON_Y = 60;
    private static final int BUTTON_X_GAP = 34;
    private static final int SKIP_WIDTH = 28;
    private static final int TOTAL_WIDTH = 28 * 10 - 1;
    private static final Rectangle BOUNDS = new Rectangle(0, 386, 960, 154);
    //pcs
    private static final Rectangle BOUNDS_ZOOM = new Rectangle(36, 335, 175, 175);
    private static final Rectangle BOUNDS_PCS_MUSIC = new Rectangle(432, 410, 261, 68);
    private static final Rectangle BOUNDS_PCS_GALAXIE = new Rectangle(431, 410, 261, 68);

    protected class FlipBarRenderer extends Renderer {

        private Hashtable modeImageMap = new Hashtable();
        private Hashtable actionImageMap = new Hashtable();

        private Image iBackground = dataCenter.getImage("04_flip_bg.png");
//        private Image iEffect = dataCenter.getImage("04_flip_ef.png");


        private Image iButtonBg = dataCenter.getImage("04_flip_bt_01.png");
        private Image iButtonFocus = dataCenter.getImage("04_flip_st_foc_01.png");
        private Image iRecordButtonFocus = dataCenter.getImage("04_flip_st_foc_02.png");

        private Image iNowLong = dataCenter.getImage("04_flip_now01.png");
        private Image iNowShort = dataCenter.getImage("04_flip_now02.png");

        private Image iUhd = dataCenter.getImage("icon_uhd.png");
        private Image iHd = dataCenter.getImage("icon_hd.png");
        private Image iSd = dataCenter.getImage("icon_sd.png");
        private Image[] iRating = new Image[] {
        		null, // this order from Contants
        		dataCenter.getImage("icon_g.png"),
        		dataCenter.getImage("icon_8.png"),
        		dataCenter.getImage("icon_13.png"),
        		dataCenter.getImage("icon_16.png"),
        		dataCenter.getImage("icon_18.png"),
        };
        private Image iIndi = dataCenter.getImage("04_pvr_indi.png");

        private Image modeImage;
        private Image[][] actionImages;
        //pcs
        private Image pcsPhotoBgImg = dataCenter.getImage("09_ph_flip.png");
        private Image photoNowLImg = dataCenter.getImage("09_flip_now_l.png");
        private Image photoNowRImg = dataCenter.getImage("09_flip_now_r.png");
        private Image photoNowMImg = dataCenter.getImage("09_flip_now_m1.png");
        private Image photoNowCenterImg = dataCenter.getImage("09_flip_now_m.png");
        private String[] focusName = new String[8];
        private String stopSlideshowTxt = null;
        private String changeGalChTxt = null;
        private Image photoZoomBgImg = dataCenter.getImage("09_zoombg.png");
        private Image photoZoomArrLImg = dataCenter.getImage("09_zoom_l.png");
        private Image photoZoomArrRImg = dataCenter.getImage("09_zoom_r.png");
        private Image photoZoomArrTImg = dataCenter.getImage("09_zoom_t.png");
        private Image photoZoomArrBImg = dataCenter.getImage("09_zoom_b.png");
        private Image[] photoZoomSizeImg = new Image[4];
        private Image pcsMusicBgImg = dataCenter.getImage("09_mp_flip.png");
        private Image pcsGalaxieBgImg = dataCenter.getImage("09_mp_flip_g.png");

        private Font fTitle = FontResource.BLENDER.getFont(20);
        private Font fTime = FontResource.BLENDER.getFont(16);
        private Font fMode = FontResource.BLENDER.getFont(16);
        private Font fSkip = FontResource.BLENDER.getFont(14);

        private FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);
        private FontMetrics fmTime = FontResource.getFontMetrics(fTime);
        //pcs
        private Font f18 = FontResource.BLENDER.getFont(18);
        private Font f17 = FontResource.BLENDER.getFont(17);
        private Color c0 = new Color(0, 0, 0);
        private Color c224 = new Color(224, 224, 224);

        private Color cTitle = Color.white;
        private Color cTitleShadow = new Color(51, 51, 51);
        private Color cMode = new Color(255, 203, 0);
        private Color cButtonBg = new DVBColor(0, 0, 0, 128);
        private Color cEndTime = new Color(175, 175, 175);

        private Color cBarBorder = Color.black;
        private Color cBarBackground = new Color(101, 101, 101);
        private Color cBarForeground = new Color(255, 203, 0);
        private Color cNow = new Color(38, 38, 38);
        private Color cSkip = new Color(148, 148, 148);

        public FlipBarRenderer() {
            modeImageMap.put(MODE_VOD, "04_flip_vod.png");
            modeImageMap.put(MODE_PVR, "04_flip_pvr.png");
            modeImageMap.put(MODE_TV, "04_flip_tv.png");
            modeImageMap.put(MODE_PPV, "04_flip_ppv.png");
            modeImageMap.put(MODE_TIMESHIFT, "04_flip_timeshift.png");
            modeImageMap.put(MODE_KARAOKE, "04_flip_karaoke.png");
            modeImageMap.put(MODE_PCS_VIDEO, "09_fliptitle_en.png");
            modeImageMap.put(MODE_PCS_PHOTO, "04_flip_photo.png");
            modeImageMap.put(MODE_PCS_PHOTO_SLIDE, "04_flip_Slide.png");

            // icon, focused icon, button, focused button, dimmed button
            actionImageMap.put(FlipBarAction.SKIP_BACKWARD,
                new String[] { "04_flip_st_01.png", "04_flip_st_01_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.REWIND,
                new String[] { "04_flip_st_02.png", "04_flip_st_02_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.STOP,
                new String[] { "04_flip_st_03.png", "04_flip_st_03_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.PAUSE,
                new String[] { "04_flip_st_04.png", "04_flip_st_04_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.PLAY,
                new String[] { "04_flip_st_05.png", "04_flip_st_05_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.FAST_FORWARD,
                new String[] { "04_flip_st_06.png", "04_flip_st_06_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.SKIP_FORWARD,
                new String[] { "04_flip_st_07.png", "04_flip_st_07_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.RECORD,
                new String[] { "04_flip_st_08.png", "04_flip_st_08_foc.png", "04_flip_bt_02.png", "04_flip_st_foc_02.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.REPEAT_ONCE,
                new String[] { "09_flip_st_02_1.png", "09_flip_st_02_1_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.REPEAT_ALL,
                new String[] { "09_flip_st_02_2.png", "09_flip_st_02_2_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.REPEAT_ONE,
                new String[] { "09_flip_st_02_3.png", "09_flip_st_02_3_foc.png", "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            //PCS
            actionImageMap.put(FlipBarAction.SCREEN_FULL,
                    new String[] { "04_flip_st_09.png", "04_flip_st_09_foc.png",
                    "04_flip_bt_01.png","04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.SCREEN_SMALL,
                new String[] { "04_flip_st_10.png", "04_flip_st_10_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.RANDOM_ON,
                    new String[] { "09_flip_st_01_on.png", "09_flip_st_01_on_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.RANDOM_OFF,
                    new String[] { "09_flip_st_01.png", "09_flip_st_01_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.REPEAT_ON,
                    new String[] { "09_flip_st_02_2_on.png", "09_flip_st_02_2_on_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.REPEAT_OFF,
                    new String[] { "09_flip_st_02_2.png", "09_flip_st_02_2_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.ZOOM,
                new String[] { "09_flip_st_03.png", "09_flip_st_03_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.ROTATE,
                    new String[] { "09_flip_st_04.png", "09_flip_st_04_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.SKIP_BACKWARD_MUSIC,
                    new String[] { "04_flip_st_pre.png", "04_flip_st_pre_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.SKIP_FORWARD_MUSIC,
                    new String[] { "04_flip_st_next.png", "04_flip_st_next_foc.png",
                    "04_flip_bt_01.png", "04_flip_st_foc_01.png", "04_flip_bt_01_dim.png"});
            actionImageMap.put(FlipBarAction.GAL_CH_CHANGE,
                    new String[] { "", "", "", "04_flip_st_foc150.png", ""});
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return BOUNDS;
        }

        public void readyActionImages() {
            actionImages = new Image[actions.length][5];
            for (int i = 0; i < actionImages.length; i++) {
                String[] s = (String[]) actionImageMap.get(actions[i]);
                if (s != null) {
                    for (int j = 0; j < s.length; j++) {
                        actionImages[i][j] = dataCenter.getImage(s[j]);
                    }
                }
            }
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
            if (MODE_PCS_PHOTO.equals(mode)) {
                focusName = TextUtil.tokenize(DataCenter.getInstance().getString("flipbar.photoFocusName"), "^");
                stopSlideshowTxt = DataCenter.getInstance().getString("flipbar.stopSlideShow");
            } else if (MODE_PCS_PHOTO_ZOOM.equals(mode)) {
                int[] size = {1, 2, 4, 8};
                for (int i = 0; i < photoZoomSizeImg.length; i++) {
                    photoZoomSizeImg[i] = dataCenter.getImage("09_zoom" + size[i] + "x.png");
                }
            } else if (MODE_PCS_GALAXIE.equals(mode)) {
                changeGalChTxt = DataCenter.getInstance().getString("flipbar.changeGalaxieCh");
            }
        }

        public void paint(Graphics g, UIComponent c) {
            if (MODE_PCS_PHOTO.equals(mode)) {
                paintPcsPhoto(g, c);
                return;
            } else if (MODE_PCS_PHOTO_ZOOM.equals(mode)) {
                paintPcsPhotoZoom(g, c);
                return;
            } else if (MODE_PCS_MUSIC.equals(mode)) {
                paintPcsMusic(g, c);
                return;
            } else if (MODE_PCS_GALAXIE.equals(mode)) {
                paintPcsGalaxie(g, c);
                return;
            }
            Formatter formatter = Formatter.getCurrent();

//            Log.printDebug("paint()-iBackground : " + iBackground);
            g.drawImage(iBackground, 0, 0, c);
            int buttonX = MODE_KARAOKE.equals(mode) ? BUTTON_X_K : BUTTON_X;
            int x = buttonX;

            int actionSize = actions.length;
            //pcs
            if (MODE_PCS_VIDEO.equals(mode)) { actionSize--; }
            g.setColor(cButtonBg);
            g.fillRect(x, BUTTON_Y + 1, actionSize * BUTTON_X_GAP + 1, 34);
            //pcs
            if (MODE_PCS_VIDEO.equals(mode)) { g.fillRect(780, BUTTON_Y + 1, BUTTON_X_GAP + 1, 34); }

            // buttons
            for (int i = 0; i < actionSize; i++) {
                g.drawImage(!disabled[i] ? actionImages[i][2] : actionImages[i][4], x, BUTTON_Y, c);
                g.drawImage(actionImages[i][0], x + 4, BUTTON_Y + 5, c);
                x = x + BUTTON_X_GAP;
            }
            //pcs
            if (MODE_PCS_VIDEO.equals(mode)) {
                x = 780;
                g.drawImage(!disabled[actionSize] ? actionImages[actionSize][2] : actionImages[actionSize][4], x, BUTTON_Y, c);
                g.drawImage(actionImages[actionSize][0], x + 4, BUTTON_Y + 5, c);
            }
//            g.drawImage(iEffect, 146, BUTTON_Y + 1, c);
            // focus
            x = buttonX + focus * BUTTON_X_GAP;
            //pcs
            if (MODE_PCS_VIDEO.equals(mode) && focus == actionSize) { x = 780; }
            g.drawImage(actionImages[focus][3], btnX = x, btnY = BUTTON_Y + 1, c);
            btnW = actionImages[focus][3].getWidth(c);
            btnH = actionImages[focus][3].getHeight(c);
            g.drawImage(actionImages[focus][1], x + 3, BUTTON_Y + 5, c);

            // mode icon
            Image modeImage = dataCenter.getImageByLanguage((String) modeImageMap.get(mode));

            if (modeImage != null) {
                //pcs
                if (MODE_PCS_VIDEO.equals(mode)) {
                    g.drawImage(modeImage, 151, 455 - 386, c);
                } else {
                    g.drawImage(modeImage, MODE_KARAOKE.equals(mode) ? 151 : 146, 60, c);
                }
            } else {
                g.setFont(fMode);
                g.setColor(cMode);
                GraphicUtil.drawStringCenter(g, mode, 174, 84);
            }

            int barLeft = buttonX + actionSize * BUTTON_X_GAP + 13;
            int barRight;
            char[] chars;

            FlipBarContent cur = getCurrentContent();
            if (cur == null) {
                return;
            }
            long startTime = cur.getStartTime();
            long endTime = cur.getEndTime();
            // start & end time
            g.setFont(fTime);
            g.setColor(cEndTime);
            if (absolute) {
                chars = formatter.getTimeChars(cur.getStartDate(), Formatter.TIME_HOUR_MIN);
                g.drawChars(chars, 0, chars.length, barLeft - 5, 83);
                barLeft += fmTime.charsWidth(chars, 0, chars.length);
                String s = new String(formatter.getTimeChars(cur.getEndDate(), Formatter.TIME_HOUR_MIN));
                barRight = GraphicUtil.drawStringRight(g, s, 807, 83) - 6;
            } else {
                //pcs
                if (MODE_PCS_VIDEO.equals(mode)) {
                    barRight = GraphicUtil.drawStringRight(g, formatter.getDuration(endTime), 772, 83) - 6;
                } else {
                    barRight = GraphicUtil.drawStringRight(g, formatter.getDuration(endTime), 807, 83) - 6;
                }
            }
            int barWidth = barRight - barLeft;
            int curWidth = getWidth(mediaTime, startTime, endTime, barWidth);

            g.setColor(cBarBorder);
            g.fillRect(barLeft - 1, 74, barWidth + 2, 8);

            if (bufferStartTime != 0) {
                // time shift
                int bsp = getWidth(bufferStartTime, startTime, endTime, barWidth);
                int bep = getWidth(bufferEndTime, startTime, endTime, barWidth);
                g.setColor(cBarBackground);
                if (bsp <= 0) {
                    bsp = 0;
                } else {
                    // before buffer
                    g.fillRect(barLeft, 75, bsp, 6);
                }
                if (bep == bsp) {
                    bep++;
                }
                g.setColor(cBarForeground);
                // buffer
                if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : paint() : yellow painting width(1) = "+(bep-bsp));
                g.fillRect(barLeft + bsp, 75, bep - bsp, 6);
            } else {
                // normal play
                g.setColor(cBarBackground);
                g.fillRect(barLeft, 75, barWidth, 6);
                // media time
                g.setColor(cBarForeground);
                if (Log.DEBUG_ON) Log.printDebug("FlipBar_log : paint() : yellow painting width(2) = "+(curWidth));
                g.fillRect(barLeft, 75, curWidth, 6);
            }

            if (skippable) {
                g.setFont(fSkip);
                for (int i = 0; i < 10; i++) {
                    x = barLeft + barWidth * i / 10 - 1;
                    g.setColor(cBarBorder);
                    g.fillRect(x, 75, 1, 6);
                    g.setColor(cSkip);
                    GraphicUtil.drawStringCenter(g, Integer.toString(i), x + barWidth / 20, 92);
                }
                g.setFont(fTime);
            }

            // segment indicators
            long[] array = indicationTimes;
            if (array != null) {
                for (int i = 0; i < array.length; i++) {
                    long time = array[i];
                    if (startTime < time && time < endTime) {
                        g.drawImage(iIndi, barLeft + getWidth(time, startTime, endTime, barWidth) - 11, 49, c);
                    }
                }
            }
            // media time
            boolean longType;
            String mt;
            if (absolute) {
                mt = formatter.getTime(mediaTime);
                if (mt.indexOf('m') != -1) { // remove am/pm
                	mt = mt.substring(0, mt.length()-3);
                }
                longType = false;
            } else {
                mt = formatter.getDuration(mediaTime);
                longType = mt.length() > 5;
            }
            g.setColor(cNow);
            if (longType) {
                g.drawImage(iNowLong, barLeft + curWidth - 33, 50, c);
            } else {
                g.drawImage(iNowShort, barLeft + curWidth - 23, 50, c);
            }
            GraphicUtil.drawStringCenter(g, mt, barLeft + curWidth, 64);

            // title and information
            int iconWidth = 0;
            Image ratingImage = null;
            if (cur.getRating() != null) {
            	for (int i = iRating.length-1; i >= 0; i--) {
            		if (Constants.PARENTAL_RATING_NAMES[i].equalsIgnoreCase(cur.getRating())) {
            			ratingImage = iRating[i];
            			iconWidth = 25;
            			break;
            		}
            	}
            }
            Image resImage = null;
            int definition = cur.getDefinition();
            if (definition == Constants.DEFINITION_4K) {
            	resImage = iUhd;
            	iconWidth += iUhd.getWidth(c);
            } else if (definition == Constants.DEFINITION_HD) {   // TODO - 4K
                resImage = iHd;
                iconWidth += iHd.getWidth(c);
            } else if (MODE_VOD == mode) {
            	resImage = iSd;
            	iconWidth += iSd.getWidth(c);
            }
            if (iconWidth > 0) {
                iconWidth += 2;
            }

            String s = cur.getName();
            int availWidth = 646 - iconWidth;
            int titleWidth = fmTitle.stringWidth(cur.getName());
            boolean exeed = titleWidth > availWidth;
            boolean extra = false;
            Rectangle oriClip = null;
            x = 159;
            if (exeed) {
            	if (scrollCounter > SCROLL_DELAY) {
            		int off = (scrollCounter - SCROLL_DELAY) * 2;
            		x -= off;
            		extra = (off > titleWidth - availWidth + 100);
            		if (off >= titleWidth + 100) {
            			resetScroll();
            		}
            	}
            	oriClip = g.getClipBounds();
            	g.clipRect(scollBounds.x, scollBounds.y, availWidth, scollBounds.height);
            } else {
            	setupTimer(false);
            }
            // title
            g.setFont(fTitle);
            g.setColor(cTitleShadow);
            g.drawString(s, x+1, 118);
            if (extra) {
            	g.drawString(s, x+1 + titleWidth + 100, 118);
            }
            g.setColor(cTitle);
            g.drawString(s, x, 117);
            if (extra) {
            	g.drawString(s, x + titleWidth + 100, 117);
            }
            if (exeed) {
            	g.setClip(oriClip);
            }

            x = 159 + Math.min(titleWidth, availWidth) + 5;
            if (ratingImage != null) {
                g.drawImage(ratingImage, x, 105, c);
                x = x + ratingImage.getWidth(c) + 4;
            }
            if (resImage != null) {
                g.drawImage(resImage, x, 105, c);
            }
        }

        /**
         * Paint for PCS photo.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        private void paintPcsPhoto(Graphics g, UIComponent c) {
            g.translate(-BOUNDS.x, -BOUNDS.y);

            g.drawImage(pcsPhotoBgImg, 292, 447, c);
            int buttonX = 393;
            int x = buttonX;
            int actionSize = actions.length;

            int btnYPos = 447;
            // buttons
            x = buttonX;
            for (int i = 0; i < actionSize; i++) {
                g.drawImage(!disabled[i] ? actionImages[i][2] : actionImages[i][4], x, btnYPos, c);
                //if (disabled[i]) { g.drawImage(actionImages[i][4], x, btnYPos, c); }
                int addPos = 0;
                if (actions[i] == FlipBarAction.REPEAT_ON) { addPos = 5; }
                DVBGraphics dvbGraphics = (DVBGraphics) g;
                DVBAlphaComposite baseComposite = DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
                if (disabled[i]) {
                    try {
                        dvbGraphics.setDVBComposite(
                                DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                g.drawImage(actionImages[i][0], x + 4, btnYPos + 5 + addPos, c);
                if (disabled[i]) {
                    try { dvbGraphics.setDVBComposite(baseComposite); } catch (Exception e) { e.printStackTrace(); }
                }
                x = x + BUTTON_X_GAP;
            }
            // focus
            x = buttonX + focus * BUTTON_X_GAP;
            g.drawImage(actionImages[focus][3], x, btnY = btnYPos, c);
            btnX = x - BOUNDS.x;
            btnY = btnYPos - BOUNDS.y;
            btnW = actionImages[focus][3].getWidth(c);
            btnH = actionImages[focus][3].getHeight(c);
            g.drawImage(actionImages[focus][1], x + 3, btnYPos + 5, c);

            FlipBarContent cur = getCurrentContent();
            if (cur == null) { return; }
            // title
            g.setFont(f18);
            g.setColor(cTitle);
            String numTxt = "";
            if (cur.getData() != null) { numTxt = " " + cur.getData(); }
            int numWidth = g.getFontMetrics().stringWidth(numTxt);
            String str = TextUtil.shorten(cur.getName(), g.getFontMetrics(), (653 - 305) - numWidth) + numTxt;
            g.drawString(str, 305, 503);
            //now img
            g.setFont(f17);
            g.setColor(cTitleShadow);
            String btnName = focusName[focus];
            if (actions[focus] == FlipBarAction.PAUSE) {
                btnName = stopSlideshowTxt;
            }
            int nameSize = g.getFontMetrics().stringWidth(btnName);
            int baseSize = 29;
            int addSize = 0;
            if (nameSize > baseSize) { addSize = (nameSize - baseSize + 1) / 2; }
            x = 407 - 19 - addSize + (focus * BUTTON_X_GAP);
            g.drawImage(photoNowLImg, x, 421, c);
            x += 19;
            g.drawImage(photoNowMImg, x, 421, addSize, 21, c);
            x += addSize;
            g.drawImage(photoNowCenterImg, x, 421, c);
            x += 6;
            g.drawImage(photoNowMImg, x, 421, addSize, 21, c);
            x += addSize;
            g.drawImage(photoNowRImg, x, 421, c);
            GraphicUtil.drawStringCenter(g, btnName, 409 + (focus * BUTTON_X_GAP), 435);

            String photoMode = MODE_PCS_PHOTO;
            if (actions[focus] == FlipBarAction.PAUSE) { photoMode = MODE_PCS_PHOTO_SLIDE; }
            Image modeImage = dataCenter.getImageByLanguage((String) modeImageMap.get(photoMode));
            if (modeImage != null) { g.drawImage(modeImage, 299, 455, c); }

            g.translate(BOUNDS.x, BOUNDS.y);
        }

        /**
         * Paint for PCS photo zoom.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        private void paintPcsPhotoZoom(Graphics g, UIComponent c) {
            DVBGraphics dvbGraphics = (DVBGraphics) g;
            if (isAlpha) {
                try {
                    dvbGraphics.setDVBComposite(DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            g.translate(-BOUNDS_ZOOM.x, -BOUNDS_ZOOM.y);
            g.drawImage(photoZoomBgImg, 36, 335, c);
            g.drawImage(photoZoomArrLImg, 83, 415, c);
            g.drawImage(photoZoomArrRImg, 154, 415, c);
            g.drawImage(photoZoomArrTImg, 119, 382, c);
            g.drawImage(photoZoomArrBImg, 119, 452, c);
            g.drawImage(photoZoomSizeImg[focus], 114, 405, c);
            g.translate(BOUNDS_ZOOM.x, BOUNDS_ZOOM.y);
            if (isAlpha) {
                try {
                    dvbGraphics.setDVBComposite(DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Paint for PCS Music.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        private void paintPcsMusic(Graphics g, UIComponent c) {
            Log.printDebug("paintPcsMusic()-c.getBounds() : " + c.getBounds());
            g.translate(-BOUNDS_PCS_MUSIC.x, -BOUNDS_PCS_MUSIC.y);

            g.drawImage(pcsMusicBgImg, 432, 410, c);
            int buttonX = 460;
            int x = buttonX;
            int actionSize = actions.length;
            int btnYPos = 421;
            // buttons
            for (int i = 0; i < actionSize; i++) {
                g.drawImage(!disabled[i] ? actionImages[i][2] : actionImages[i][4], x, btnYPos, c);
                //if (disabled[i]) { g.drawImage(actionImages[i][4], x, btnYPos, c); }
                int addPos = 0;
                if (actions[i] == FlipBarAction.REPEAT_ON) { addPos = 5; }
                DVBGraphics dvbGraphics = (DVBGraphics) g;
                DVBAlphaComposite baseComposite = DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
                if (disabled[i]) {
                    try {
                        dvbGraphics.setDVBComposite(
                                DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                g.drawImage(actionImages[i][0], x + 4, btnYPos + 5 + addPos, c);
                if (disabled[i]) {
                    try { dvbGraphics.setDVBComposite(baseComposite); } catch (Exception e) { e.printStackTrace(); }
                }
                /*if (actionImages[i][0] != null && ((i == 2 || i == 4 || i == 5) || (disabled[i] && (i == 1 || i == 3)))) {
                    g.drawImage(actionImages[i][0], x + 4, btnYPos + 5 + addPos, c);
                }*/
                x = x + BUTTON_X_GAP;
            }
            // focus
            if (actions[focus] == FlipBarAction.REPEAT_OFF) {
                Log.printDebug("paint()-FlipBarAction.REPEAT_OFF");
            } else if (actions[focus] == FlipBarAction.REPEAT_ON) {
                Log.printDebug("paint()-FlipBarAction.REPEAT_ON");
            }
            x = buttonX + focus * BUTTON_X_GAP;
            g.drawImage(actionImages[focus][3], x, btnY = btnYPos, c);
            btnX = x - BOUNDS_PCS_MUSIC.x;
            btnY = btnYPos - BOUNDS_PCS_MUSIC.y;
            btnW = actionImages[focus][3].getWidth(c);
            btnH = actionImages[focus][3].getHeight(c);
            g.drawImage(actionImages[focus][1], x + 3, btnYPos + 5, c);

            g.translate(BOUNDS_PCS_MUSIC.x, BOUNDS_PCS_MUSIC.y);
        }

        /**
         * Paint for PCS Galaxie.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        private void paintPcsGalaxie(Graphics g, UIComponent c) {
            g.translate(-BOUNDS_PCS_GALAXIE.x, -BOUNDS_PCS_GALAXIE.y);

            g.drawImage(pcsGalaxieBgImg, 431, 410, c);
            int buttonX = 453;
            int x = buttonX;
            int actionSize = actions.length;
            int btnYPos = 421;
            // buttons
            for (int i = 0; i < actionSize; i++) {
                if (actionImages[i][0] != null) {
                    g.drawImage(actionImages[i][0], x + 4, btnYPos + 5, c);
                }
                x = x + BUTTON_X_GAP;
            }
            // focus
            x = buttonX + focus * BUTTON_X_GAP;
            g.drawImage(actionImages[focus][3], x, btnY = btnYPos, c);
            btnX = x - BOUNDS_PCS_GALAXIE.x;
            btnY = btnYPos - BOUNDS_PCS_GALAXIE.y;
            btnW = actionImages[focus][3].getWidth(c);
            btnH = actionImages[focus][3].getHeight(c);
            g.setFont(f17);
            if (focus < 2) {
                g.drawImage(actionImages[focus][1], x + 3, btnYPos + 5, c);
                g.setColor(c224);
            } else { g.setColor(c0); }
            GraphicUtil.drawStringRight(g, changeGalChTxt, 656, 443);

            g.translate(BOUNDS_PCS_GALAXIE.x, BOUNDS_PCS_GALAXIE.y);
        }

        private int getWidth(long time, long from, long to, int totalWidth) {
            long dur = to - from;
            long cur = time - from;
            return Math.min(totalWidth, Math.max(0, (int) (cur * totalWidth / dur)));
        }

    }

}

package com.videotron.tvi.illico.flipbar;

public class FlipBarAction {

    public static final FlipBarAction PLAY = new FlipBarAction("PLAY");
    public static final FlipBarAction STOP = new FlipBarAction("STOP");
    public static final FlipBarAction PAUSE = new FlipBarAction("PAUSE");
    public static final FlipBarAction FAST_FORWARD = new FlipBarAction("FAST_FORWARD");
    public static final FlipBarAction REWIND = new FlipBarAction("REWIND");
    public static final FlipBarAction SKIP_FORWARD = new FlipBarAction("SKIP_FORWARD");
    public static final FlipBarAction SKIP_BACKWARD = new FlipBarAction("SKIP_BACKWARD");
    public static final FlipBarAction RECORD = new FlipBarAction("RECORD");
    public static final FlipBarAction REPEAT_ONCE = new FlipBarAction("REPEAT_ONCE");
    public static final FlipBarAction REPEAT_ALL = new FlipBarAction("REPEAT_ALL");
    public static final FlipBarAction REPEAT_ONE = new FlipBarAction("REPEAT_ONE");
    public static final FlipBarAction SLOW_MOTION = new FlipBarAction("SLOW_MOTION");
    //pcs
    public static final FlipBarAction SCREEN_FULL = new FlipBarAction("SCREEN_FULL");
    public static final FlipBarAction SCREEN_SMALL = new FlipBarAction("SCREEN_SMALL");
    public static final FlipBarAction RANDOM_ON = new FlipBarAction("RANDOM_ON");
    public static final FlipBarAction RANDOM_OFF = new FlipBarAction("RANDOM_OFF");
    public static final FlipBarAction REPEAT_ON = new FlipBarAction("REPEAT_ON");
    public static final FlipBarAction REPEAT_OFF = new FlipBarAction("REPEAT_OFF");
    public static final FlipBarAction ZOOM = new FlipBarAction("ZOOM");
    public static final FlipBarAction ROTATE = new FlipBarAction("ROTATE");
    public static final FlipBarAction SKIP_FORWARD_MUSIC = new FlipBarAction("SKIP_FORWARD_MUSIC");
    public static final FlipBarAction SKIP_BACKWARD_MUSIC = new FlipBarAction("SKIP_BACKWARD_MUSIC");
    public static final FlipBarAction GAL_CH_CHANGE = new FlipBarAction("GAL_CH_CHANGE");

//    public static final FlipBarAction LEFT_RIGHT = new FlipBarAction("LEFT_RIGHT");
//    public static final FlipBarAction UP_DOWN = new FlipBarAction("UP_DOWN");
//    public static final FlipBarAction SLOW_MOTION = new FlipBarAction("SLOW_MOTION");
//    public static final FlipBarAction REPLAY = new FlipBarAction("REPLAY");
//    public static final FlipBarAction CHANGE_CHANNEL = new FlipBarAction("CHANGE_CHANNEL");
//    public static final FlipBarAction TIMELINE = new FlipBarAction("TIMELINE");
//    public static final FlipBarAction SKIP = new FlipBarAction("SKIP");
//    public static final FlipBarAction GO_TO_TIME = new FlipBarAction("GO_TO_TIME");

    private String name;
    private boolean enabled = true;

    public FlipBarAction(String actionName) {
        this.name = actionName;
    }

    public String getName() {
        return name;
    }

    public int hashCode() {
        return name.hashCode();
    }

    public String toString() {
        return name;
    }

    public boolean equals(Object o) {
        if (o instanceof FlipBarAction) {
            return name.equals(((FlipBarAction) o).getName());
        } else {
            return false;
        }
    }

}



package com.videotron.tvi.illico.flipbar;

import com.videotron.tvi.illico.flipbar.FlipBar;
import com.videotron.tvi.illico.flipbar.FlipBarAction;

/**
 * This class defines a factory for the creation of FlipBar objects.
 *
 * @version $Revision: 1.14 $ $Date: 2017/01/12 19:18:40 $
 * @author  June Park
 */
public class FlipBarFactory {

    private static FlipBarFactory instance = new FlipBarFactory();

    public static FlipBarFactory getInstance() {
        return instance;
    }

    private FlipBarFactory() {
    }

    public FlipBar createVodFlipBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_VOD);
        flipBar.setAbsolute(false);
        flipBar.setSkippable(true);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.SKIP_BACKWARD,
            FlipBarAction.REWIND,
            FlipBarAction.STOP,
            FlipBarAction.PAUSE,
            FlipBarAction.PLAY,
            FlipBarAction.FAST_FORWARD,
            FlipBarAction.SKIP_FORWARD,
        };
        flipBar.setFocus(3);
        flipBar.setDisplayActions(actions);
        flipBar.prepare();
        return flipBar;
    }

    public FlipBar createPvrFlipBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_PVR);
        flipBar.setAbsolute(false);
        flipBar.setSkippable(true);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.SKIP_BACKWARD,
            FlipBarAction.REWIND,
            FlipBarAction.STOP,
            FlipBarAction.PAUSE,
            FlipBarAction.PLAY,
            FlipBarAction.FAST_FORWARD,
            FlipBarAction.SKIP_FORWARD,
        };
        flipBar.setFocus(3);
        flipBar.setDisplayActions(actions);
        flipBar.prepare();
        return flipBar;
    }

    public FlipBar createTimeshiftFlipBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_TIMESHIFT);
        flipBar.setAbsolute(true);
        flipBar.setSkippable(false);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.RECORD,
            FlipBarAction.SKIP_BACKWARD,
            FlipBarAction.REWIND,
            FlipBarAction.STOP,
            FlipBarAction.PAUSE,
            FlipBarAction.PLAY,
            FlipBarAction.FAST_FORWARD,
            FlipBarAction.SKIP_FORWARD,
        };
        flipBar.setFocus(4);
        flipBar.setDisplayActions(actions);
        flipBar.setEnabled(FlipBarAction.STOP, false); // STOP is disabled on Timeshift
        flipBar.prepare();
        return flipBar;
    }
    
    public FlipBar createKaraokeFlipBar() {
    	FlipBar flipBar = new FlipBar(FlipBar.MODE_KARAOKE);
    	flipBar.setAbsolute(false);
        flipBar.setSkippable(false);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.SKIP_BACKWARD,
            FlipBarAction.STOP,
            FlipBarAction.PAUSE,
            FlipBarAction.PLAY,
            FlipBarAction.SKIP_FORWARD,
            FlipBarAction.REPEAT_ONCE
        };
        flipBar.setFocus(2);
        flipBar.setDisplayActions(actions);
        flipBar.prepare();
        return flipBar;
    }
    
    public FlipBar createPcsVodFlipBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_PCS_VIDEO);
        flipBar.setAbsolute(false);
        flipBar.setSkippable(false);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.SKIP_BACKWARD,
            FlipBarAction.REWIND,
            FlipBarAction.STOP,
            FlipBarAction.PAUSE,
            FlipBarAction.PLAY,
            FlipBarAction.FAST_FORWARD,
            FlipBarAction.SKIP_FORWARD,
            FlipBarAction.SCREEN_FULL
        };
        flipBar.setFocus(3);
        flipBar.setDisplayActions(actions);
        flipBar.prepare();
        return flipBar;
    }
    
    public FlipBar createPcsPhotoFlipBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_PCS_PHOTO);
        flipBar.setAbsolute(false);
        flipBar.setSkippable(false);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.SKIP_BACKWARD,
            FlipBarAction.STOP,
            FlipBarAction.PLAY,
            FlipBarAction.SKIP_FORWARD,
            FlipBarAction.RANDOM_OFF,
            FlipBarAction.REPEAT_OFF,
            FlipBarAction.ZOOM,
            FlipBarAction.ROTATE
        };
        flipBar.setFocus(2);
        flipBar.setDisplayActions(actions);
        flipBar.prepare();
        return flipBar;
    }
    
    public FlipBar createPcsPhotoZoomBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_PCS_PHOTO_ZOOM);
        flipBar.setAbsolute(false);
        flipBar.setSkippable(false);
        /*FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.SKIP_BACKWARD,
            FlipBarAction.STOP,
            FlipBarAction.PLAY,
            FlipBarAction.SKIP_FORWARD,
            FlipBarAction.RANDOM_OFF,
            FlipBarAction.REPEAT_OFF,
            FlipBarAction.ZOOM,
            FlipBarAction.ROTATE
        };
        flipBar.setFocus(2);
        flipBar.setDisplayActions(actions);*/
        flipBar.prepare();
        return flipBar;
    }
    
    public FlipBar createPcsMusicFlipBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_PCS_MUSIC);
        flipBar.setAbsolute(false);
        flipBar.setSkippable(false);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.STOP,
            FlipBarAction.SKIP_BACKWARD_MUSIC,
            FlipBarAction.PAUSE,
            FlipBarAction.SKIP_FORWARD_MUSIC,
            FlipBarAction.RANDOM_OFF,
            FlipBarAction.REPEAT_OFF
        };
        flipBar.setFocus(2);
        flipBar.setDisplayActions(actions);
        flipBar.prepare();
        return flipBar;
    }
    
    public FlipBar createPcsGalaxieFlipBar() {
        FlipBar flipBar = new FlipBar(FlipBar.MODE_PCS_GALAXIE);
        flipBar.setAbsolute(false);
        flipBar.setSkippable(false);
        FlipBarAction[] actions = new FlipBarAction[] {
            FlipBarAction.STOP,
            FlipBarAction.PAUSE,
            FlipBarAction.GAL_CH_CHANGE
        };
        flipBar.setFocus(1);
        flipBar.setDisplayActions(actions);
        flipBar.prepare();
        return flipBar;
    }

}
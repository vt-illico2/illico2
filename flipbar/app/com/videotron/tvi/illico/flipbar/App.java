package com.videotron.tvi.illico.flipbar;

import java.io.File;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.log.Log;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import org.ocap.system.RegisteredApiManager;

/**
 * This class is the initial class of FlipBar.
 *
 * @version $Revision: 1.43 $ $Date: 2017/01/12 19:18:40 $
 * @author June Park
 */
public class App implements Xlet, ApplicationConfig {
	/** Version. */
	public static final String VERSION = "4K.2.0";
	/** App name. */
	private static final String APP_NAME = "FlipBar";
	/** Registered API name. */
	private static final String REGISTERED_API_NAME = "reg_api_flipbar";
	/** Priority. */
	private static final short REGISTERED_API_STORAGE_PRIOR = 123;
	/** XletContext. */
	private XletContext xletContext;

	/**
	 * Signals the Xlet to initialize itself and enter the Paused state.
	 *
	 * @param ctx
	 *            The XletContext of this Xlet.
	 * @throws XletStateChangeException
	 *             If the Xlet cannot be initialized.
	 */
	public void initXlet(XletContext ctx) throws XletStateChangeException {
		xletContext = ctx;
		FrameworkMain.getInstance().init(this);
	}

	/**
	 * Signals the Xlet to start providing service and enter the Active state.
	 *
	 * @throws XletStateChangeException
	 *             is thrown if the Xlet cannot start providing service.
	 */
	public void startXlet() throws XletStateChangeException {
		FrameworkMain.getInstance().start();
	}

	/**
	 * Signals the Xlet to stop providing service and enter the Paused state.
	 */
	public void pauseXlet() {
		FrameworkMain.getInstance().pause();
	}

	/**
	 * Signals the Xlet to terminate and enter the Destroyed state.
	 *
	 * @param unconditional
	 *            If unconditional is true when this method is called, requests
	 *            by the Xlet to not enter the destroyed state will be ignored.
	 * @throws XletStateChangeException
	 *             is thrown if the Xlet wishes to continue to execute (Not
	 *             enter the Destroyed state). This exception is ignored if
	 *             unconditional is equal to true.
	 */
	public void destroyXlet(boolean unconditional)
			throws XletStateChangeException {
		FrameworkMain.getInstance().destroy();
	}

	// //////////////////////////////////////////////////////////////////////////
	// ApplicationConfig implementation
	// //////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the version of Application.
	 *
	 * @return version string.
	 */
	public String getVersion() {
		return VERSION;
	}

	/**
	 * Returns the name of Application.
	 *
	 * @return application name.
	 */
	public String getApplicationName() {
		return APP_NAME;
	}

	/**
	 * Returns the Application's XletContext.
	 *
	 * @return application's XletContext.
	 */
	public XletContext getXletContext() {
		return xletContext;
	}

    /**
     * Initialize this application.
     */
	public void init() {
		try {
			RegisteredApiManager rm = RegisteredApiManager.getInstance();
			rm.register(REGISTERED_API_NAME, VERSION, new File(
					"./scdf.xml").getAbsoluteFile(),
					REGISTERED_API_STORAGE_PRIOR);
			String[] rAPINames = rm.getNames();
			if (rAPINames != null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[FlipBar.registerAPI]Registered API Name Count : "
									+ rAPINames.length);
				}
				for (int i = 0; i < rAPINames.length; i++) {
					String rAPIName = rAPINames[i];
					if (rAPIName == null) {
						continue;
					}
					String rAPIVersion = rm.getVersion(rAPIName);
					if (Log.DEBUG_ON) {
						Log.printDebug("[FlipBar.registerAPI]" + i
								+ "-Name[" + rAPIName + "], Versiont["
								+ rAPIVersion + "]");
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Log.DEBUG_ON) {
				Log.printDebug("[FlipBar.registerAPI]Exception occurred");
			}
			return;
		}
        FrameworkMain.getInstance().printSimpleLog(
            "vendor_id = \"" + System.getProperty("ocap.hardware.vendor_id") + "\", (" + Environment.VENDOR_NAME + ")" +
            ", " + (Environment.SUPPORT_DVR ? "DVR" : "Non-DVR") );
    }

}
